<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['cookie_prefix']	= 'tk1001_';
$config['cookie_domain']	= '.toko1001.id';
$config['ftp_server']           = '';
$config['ftp_user_name']	= '';
$config['ftp_password'] 	= '';
$config['cdn_image']            = 'https://toko1001.id/';
$config['file_server']          = '/home/toko1001/public_html/';