<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0755);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
  |--------------------------------------------------------------------------
  | Display Debug backtrace
  |--------------------------------------------------------------------------
  |
  | If set to TRUE, a backtrace will be displayed along with php errors. If
  | error_reporting is disabled, the backtrace will not display, regardless
  | of this setting
  |
 */
define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
  |--------------------------------------------------------------------------
  | Exit Status Codes
  |--------------------------------------------------------------------------
  |
  | Used to indicate the conditions under which the script is exit()ing.
  | While there is no universal standard for error codes, there are some
  | broad conventions.  Three such conventions are mentioned below, for
  | those who wish to make use of them.  The CodeIgniter defaults were
  | chosen for the least overlap with these conventions, while still
  | leaving room for others to be defined in future versions and user
  | applications.
  |
  | The three main conventions used for determining exit status codes
  | are as follows:
  |
  |    Standard C/C++ Library (stdlibc):
  |       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
  |       (This link also contains other GNU-specific conventions)
  |    BSD sysexits.h:
  |       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
  |    Bash scripting:
  |       http://tldp.org/LDP/abs/html/exitcodes.html
  |
 */
define('EXIT_SUCCESS', 0); // no errors
define('EXIT_ERROR', 1); // generic error
define('EXIT_CONFIG', 3); // configuration error
define('EXIT_UNKNOWN_FILE', 4); // file not found
define('EXIT_UNKNOWN_CLASS', 5); // unknown class
define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
define('EXIT_USER_INPUT', 7); // invalid user input
define('EXIT_DATABASE', 8); // database error
define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/*
 * Folder Live View
 */
define("LIVEVIEW", "v1/");

/*
 * Route Agent
 */
define("ROUTE_AGENT", "agent/");
define('TEMP_CAPTCHA', 'temp/');


/*
 * SYSTEM BASE
 */
define("CONTROLLER_BASE_ADMIN", APPPATH . "system/admin/controller_base_admin.php");
define("CONTROLLER_BASE_MEMBER", APPPATH . "system/member/controller_base_member.php");
define("CONTROLLER_BASE_MERCHANT", APPPATH . "system/" . LIVEVIEW . "merchant/controller_base_merchant.php");
define("CONTROLLER_BASE_HOME", APPPATH . "system/" . LIVEVIEW . "home/controller_base_home.php");
define("CONTROLLER_BASE_AGENT", APPPATH . "system/" . LIVEVIEW . ROUTE_AGENT . "controller_base_agent.php");
define("CONTROLLER_BASE_PARTNER", APPPATH . "system/" . "partner/controller_base_partner.php");

define("MODEL_BASE", APPPATH . "system/model_base.php");
define("VIEW_BASE", APPPATH . "system/view_base.php");
define("VIEW_BASE_ADMIN", APPPATH . "system/admin/view_base_admin.php");
define("VIEW_BASE_MEMBER", APPPATH . "system/member/view_base_member.php");
define("VIEW_BASE_MERCHANT", APPPATH . "system/" . LIVEVIEW . "merchant/view_base_merchant.php");
define("VIEW_BASE_HOME", APPPATH . "system/" . LIVEVIEW . "home/view_base_home.php");
define("VIEW_BASE_AGENT", APPPATH . "system/" . LIVEVIEW . ROUTE_AGENT . "view_base_agent.php");
define("VIEW_BASE_PARTNER", APPPATH . "system/" . "partner/view_base_partner.php");

/*
 * MESSAGE
 */
define("SAVE_DATA_SUCCESS", "Data telah berhasil di simpan !");
define("CHANGE_PASSWORD_SUCCESS", "Password anda sudah berhasil diperbaharui !");
define("LOGIN_FACEBOOK_FAIL", "Login Facebook gagal");

/*
 * VALIDATION MESSAGE
 */
define("ERROR_VALIDATION_MUST_FILL", "Harus diisi !");
define("PRODUCT_MINIMAL_STOCK", "minimal harus 3 !");
define("ERROR_VALIDATION_LENGTH_MAX", "Maximal karakter harus ");
define("ERROR_VALIDATION_MUST_NUMERIC", "Harus disi dengan angka !");
define("ERROR_VALIDATION_MUST_GREATER_THAN_ZERO", "Harus lebih besar dari 0");
define("ERROR_VALIDATION_MUST_PERCENTAGE", "Harus diantara 0 s/d 100");
define("ERROR_VALIDATION_HAVE_NEW_REQUEST", "Tidak dapat menambah data jika masih ada status yang belum disetujui !");
define("ERROR_VALIDATION_MUST_FILL_ITEM", "Harus diisi dengan 1 gambar !");
define("ERROR_VALIDATION_FILL_EMAIL_FORMAT", "Format email yang anda masukan salah. Silahkan masukan kembali !");
define("ERROR_VALIDATION_MUST_FILL_EMAIL", "E-mail harus Diisi !");
define("ERROR_VALIDATION_FILL_PASSWORD_LENGTH", "Password harus antara 8 s/d 20 digit !");
define("ERROR_VALIDATION_UPLOAD_TYPE_DOESNT_MATCH", "Tipe data yang di unggah salah silahkan unggah kembali.");
define("ERROR_INVALID_PASSWORD_AND_USER_NAME", "Password atau User yang anda masukan salah !");
define("ERROR_INVALID_PASSWORD_AND_EMAIL", "Email atau Password yang dimasukkan salah !");
define("ERROR_ACCOUNT_NOT_ACTIVE", "ID anda belum aktif, cek kembali verifikasi di email anda / hubungi Administrator kami.");
define("ERROR_VALIDATION_CANT_ADD_NO_ACCESS_FORM", "CANNOT ADD, NO ACCESS PERMISSION");
define("ERROR_VALIDATION_CANT_EDIT_NO_ACCESS_FORM", "CANNOT UPDATE, NO ACCESS PERMISSION");
define("ERROR_INVALID_OLD_PASSWORD", "Masukkan password yang lama dengan benar");
define("ERROR_REQUIRED_MERCHANT_SKU", "Masukkan SKU Merchant");
define("ERROR_VALIDATION_WITHDRAW_APPROVED", "Nominal tarik dana melebihi Saldo");
define("ERROR_VALIDATION_WITHDRAW_STATUS", "Gagal, Status transaksi tidak sesuai !");
define("ERROR_VALIDATION_PHONE_NUMBER_LENGTH", "No HP yang anda masukan harus 8 sampai 13 character !");
define("ERROR_PRODUCT_STOCK_QTY_EXCEEDED", "Qty yang anda butuhkan tidak tersedia");
define("ERROR_DEPOSIT_EXCEDEED", "Nilai deposit kurang untuk nilai transaksi");
define("ERROR_DEPOSIT", "Nominal yang anda masukkan lebih dari saldo");
define("ERROR_STATUS_INVOICE", "Gagal.. Status faktur sudah tidak dapat diubah !");
define("ERROR_AWB_NO", "Gagal.. No resi sudah pernah terpakai !");
define("ERROR_DATE_VALIDATION", "Tanggal tidak sesuai !");
define("ERROR_DATE_INVALID", "Format tanggal tidak sesuai !");
define("ERROR_DATE_RECEIVED", "Tanggal terima tidak boleh lebih kecil dari pengajuan !");
define("ERROR_DATE_SENT", "Tanggal kirim tidak boleh lebih kecil dari pengajuan !");
define("ERROR_DATE_SENT_FROM_MERCHANT", "Tanggal kirim tidak boleh lebih kecil dari tanggal terima !");
define("ERROR_UPLOAD", "Gagal, Belum memilih file !");
define("ERROR_TEMPLATE", "Format Template tidak sesuai");
define("ERROR_APPROVED", "Gagal, Tidak dapat disetujui !");
define("ERROR_APPROVED_AMOUNT", "Gagal, saldo tidak mencukupi untuk melakukan penarikan");
define("ERROR_APPROVED_STATUS", "Gagal, status sudah tidak dapat disetujui");
define("ERROR_REJECT", "Gagal, status sudah tidak dapat ditolak");
define("ERROR_DELETE", "Gagal, Tidak dapat dihapus");
define("ERROR_DELETE_SLIDE_SHOW", "Gagal, Slide show tidak bisa di non-aktifkan karena status bukan disetujui");
define("ERROR_APPROVED_IMAGE", "Gagal, Tidak dapat disetujui karena tidak ada gambar");
define("ERROR_UPDATE", 'Gagal, Tidak dapat diubah, karena status tidak sesuai !');
define("ERROR_DELETE_STATUS", "Gagal, Tidak dapat dihapus, karena status tidak sesuai !");
define("ERROR_REDEEM_STATUS", "Gagal, Status periode pembayaran sudah tidak \"Baru\". Silahkan mengubah periode pembayaran. ");
define("ERROR_APPROVED_INVOICE", "Gagal, Status tagihan sudah tidak \"Baru\" !");
define("ERROR_REQUEST", "Gagal, Status sudah tidak \"Baru\" !");
define("ERROR_APPROVED_INVOICE_DETAIL", "Gagal, Detail tagihan masih kosong !");
define("ERROR_CONFIRM", "Pengajuan perubahan data menunggu konfirmasi !");
define("ERROR_DISCOUNT", "Harga produk tidak boleh lebih kecil dari harga jual");
define("ERROR_TOTAL", "Pembayaran tidak boleh lebih kecil dari total pembayaran");
define("ERROR_RECEIVED_DATE", "Tanggal tidak sesuai");
define("ERROR_SEND_EMAIL", "Email tidak berhasil dikirim");
define("ERROR_MAX_LIMIT_IMAGE", "Gambar tidak boleh melebih 2 MB");
define("ERROR_DIMENSION_IMAGE", "Ukuran gambar tidak sesuai dengan ketentuan.");
define("ERROR_CAPTCHA_CHECK", "Captcha Salah !");
define("ERROR_PLEASE_TRY_AGAIN", "Error silahkan coba lagi.");
define("ERROR_ACCESS_NOT_VALID", "Tidak dapat diakses !");
define("ERROR_IP_NOT_ALLOWED", "Tidak dapat diakses !");
define("ERROR_GENDER", "Cek kembali unggah jenis kelamin !");

define("ERROR_VALIDATION_FORGOT_PASSWORD", "Permintaan password anda sudah di proses, silahkan login menggunakan password anda !");
define("ERROR_FORGOT_PASSWORD_EXPIRED", "Permintaan password baru anda tidak dapat di proses karena sudah melebihi 2 hari !");
define("ERROR_PAYMENT_STATUS", "Status pembayaran anda sudah berubah silahkan diperiksa kembali !");
define("ERROR_PAYMENT_LESS_THAN_TOTAL_PAYMENT", "Jumlah setoran tidak boleh lebih kecil dari total bayar");
define("ERROR_PAYMENT_LESS_THAN_TOTAL_DP", "Jumlah setoran tidak boleh lebih kecil dari total pembayaran pertama");
define("ERROR_EMAIL_CHECK", "Email telah terdaftar, Silahkan menggunakan email lain");
define("ERROR_PASSWORD", "Password yang anda masukan tidak sama");
define("ERROR_REQUIREMENT", "Anda belum mensetujui syarat dan ketentuan");
define("ERROR_VOUCHER_CANT_USE", "Voucher yang anda gunakan sudah tidak valid.");
define("ERROR_COUPON_CANT_USE", "Kupon yang anda gunakan sudah tidak valid.");
define("ERROR_COUPON_REACH_LIMIT", "Kupon yang anda gunakan sudah mencapai batas pemakain per hari.");
define("ERROR_SAVE", 'Gagal, Tidak dapat diubah!');
define("ERROR_SAVE_ADD", 'Gagal, Tidak dapat ditambah!');
define("ERROR_VERIFICATION_DATE_DEPOSIT", 'Verifikasi deposit anda telah melewati batas waktu !');
define("ERROR_DEPOSIT_NOT_FOUND", 'Data Tidak Ditemukan !');
define("ERROR_DEPOSIT_EXIST", 'Anda telah verifikasi sebelumnya!');
define("ERROR_DEPOSIT_NOMINAL", 'Nominal harus lebih besar dari 0');
define("ERROR_DEPOST_TOPUP_NOMINAL", 'Nominal topup harus lebih besar dari 0');
define("ERROR_DEPOSIT_NOMINAL_MUST_FILL", "Nilai nominal harus diisi !");
define("ERROR_DEPOSIT_TOPUP_NOMINAL_MUST_FILL", "Nilai nominal topup harus diisi !");
define("ERROR_INPUT_PERSONAL_INFO_FIRST", 'Silahkan mengisi data penerima terlebih dahulu.');
define("ERROR_INPUT_PAYMENT_INFO_FIRST", 'Silahkan mengisi data pembayaran terlebih dahulu.');
define("ERROR_INPUT_PRODUCT_INFO_FIRST", 'Silahkan memilih produk yang ingin dibeli terlebih dahulu.');
define("ERROR_EMAIL_NOT_EXISTS", 'Email tidak terdaftar silahkan masukan email anda.');
define("ERROR_DOCUMENT_NOT_FOUND", 'Document tidak ditemukan.');
define("ERROR_RATE_NOT_FOUND", 'Ekspedisi tidak mendukung pengiriman ke alamat tersebut.');
define("ERROR_QTY_TOO_MUCH", 'Qty yg anda masukan melebih qty seharusnya');
define("ERROR_VOUCHER_WAS_USED", 'Voucher telah digunakan untuk transaksi');
define("ERROR_VALIDATION_MUST_BIRTHDAY", 'Tanggal Lahir Harus Disi !');
define("ERROR_VALIDATION_COUPON", 'Kode yang anda masukan tidak valid silahkan masukan kembali  !');
define("ERROR_VALIDATION_NOMINAL_COUPON", 'Nominal voucher / coupon yang anda masukan melebihi nilai belanja  !');
define("ERROR_VALIDATION_NOMINAL_ORDER_COUPON", 'Total order anda tidak mencukupi untuk memakai kode voucher / coupon !');
define("ERROR_CREDIT_SEQ", "Data kredit tidak valid");
define("ERROR_CREDIT_PRODUCT", 'Untuk dapat melakukan pembayaran dengan cicilan, per order hanya diperbolehkan membeli 1 jenis produk');
define("ERROR_BANK_NOT_EXISTS", "Silahkan mengisi data bank anda terlebih dahulu");
define("ERROR_DOMAIN_NOT_EXISTS", "Domain yang anda masukan tidak memenuhi format");
define("ERROR_DOMAIN_EXISTS", "Domain yang anda masukan sudah digunakan");

define("SUCCESS_DEPOSIT_IS_FOUND", 'Verfikasi deposit telah berhasil');
define("SUCCESS_SAVE_DEPOSIT", 'Data berhasil di simpan, silahkan cek email anda !');
define("SUCCESS_FORGOT_PASSWORD", 'Permintaan password baru anda sedang diproses silahkan cek email anda');
define("SUCCESS_REGISTRATION_MEMBER", 'Terima kasih telah mendaftar sebagai Member di Toko1001, harap cek email anda untuk verifikasi');
define("SUCCESS_VERIFY_FORGOT_PASSWORD", "Password anda sudah di ubah silahkan cek email anda untuk password baru !");
define("ERROR_PROCESS", "Gagal, Tidak dapat diproses!");
define("ERROR_PROMO_HIGHER_THAN_PRODUCT", "Harga Promo Lebih Besar Dari Harga Produk");

define("ERROR_ADIRA_PAYMENT_QTY", "Pembayaran Menggunakan ADIRA Hanya Bisa Digunakan Untuk 1 Quantity");
define("ERROR_ADIRA_PAYMENT_COUNT_PRODUCT", "Pembayaran Menggunakan ADIRA Hanya Bisa Digunakan Untuk 1 Produk");
define('ADIRA_PAYMENT_STATUS', 'adira_payment_status');


define("ERROR_VALUE_LEAST_THAN_10", "Tidak Boleh Lebih Dari 10 Karakter");
define("ERROR_VALUE_MUST_LARGE_THAN_5", "Minimal terdiri dari 5 Karakter");
define("ERROR_FACEBOOK_LOGIN_EMAIL", "Gagal Login Email Tidak Terverifikasi");

define('CHECK_OPEN_ADN_CLOSE_STORE_DATE', 'periksa kembali tanggal tutup dan tanggal buka toko anda');
define('ERROR_NO_DATA_SELECTED', 'Tidak Ada Data yang dipilih');
define('ERROR_EMPTY_DATETIME', 'Tanggal tidak boleh dikosongkan');
define('CHECK_EXCEL_COLUMN', 'Periksa file excel anda pada kolom');
define('ERROR_EMPTY_PRODUCT_ON_CART', 'Tidak Ada Barang Didalam Keranjang Belanja Anda');

define('ERROR_PARTNER_PRODUCT_SEQ_NOT_EXIST', 'Partner Produk Tidak Ditemukan');
define('ERROR_PRODUCT_NOT_EXIST', 'Produk Tidak Boleh Kosong');
define('ERROR_PARTNER_COMMISSION_NOT_EXIST', 'Komisi Partner Tidak Boleh Dikosongkan.');
define('ERROR_PARTNER_GROUP_SEQ', 'Partner Group Tidak Boleh Kosong');
define('AGENT_KOMISI_NOT_EXIST', 'Komisi Agent Tidak Boleh Kosong');

/*
 * MERCHANT CAPTION INFO
 */

define('CAPTION_INFO_PRODUCT_NAME_CATEGORI', 'Setelah konfirmasi produk, Nama produk tidak akan bisa di ganti kembali.');
define('CAPTION_INFO_PRODUCT_CHOOSE_CATEGORI', 'Pilih kategori dengan benar, selanjutnya kategori tidak dapat dirubah.');
define('CAPTION_INFO_PRODUCT_SPESIFICATION', 'change this message on constant.php line 235');


/*
 * TYPE VALIDATOR
 */
define("NUMERIC_VALIDATOR", "num_val");
define("QTY_VALIDATOR", "qty_val");
define("FILL_VALIDATOR", "fil_val");
define("PERCENTAGE_VALIDATOR", "pct_val");
define("STOCK_VALIDATOR", "stk_val");
define("DATE_VALIDATOR", "dte_val");
define("GENDER_VALIDATOR", "gen_val");
define("ZIP_CODE_VALIDATOR", "zip_code");
define("EMAIL_VALIDATOR", "email_val");

/*
 * LOGIN TYPE
 */
define("MERCHANT_LOGIN", "mer_log");
define("MEMBER_LOGIN", "mem_log");
define("ADMIN_LOGIN", "adm_log");
define("AGENT_LOGIN", "age_log");
define("PARTNER_LOGIN", "par_log");

/*
 * Library
 */
define("CSV", "csv_reader");

/*
 * URL UPLOAD
 */
define("UPLOADID", "userfile");
define("CREDIT_IMG_FULL_PATH", "assets/img/cicilantoko1001.png");
define("AGENT_UPLOAD_IMAGE", CDN_IMAGE . "assets/img/" . ROUTE_AGENT);
define("AGENT_UPLOAD_IMAGE_TOPUP_DEPOSIT", CDN_IMAGE . "assets/img/" . ROUTE_AGENT . 'topup_deposit/');
define("AGENT_UPLOAD_EXCEL_CUSTOMER_DATA", CDN_IMAGE . "assets/csv/agent/customer_data/");
define("EXPEDITION_UPLOAD_IMAGE", CDN_IMAGE . "assets/img/admin/ekspedisi/");
define("ORDER_UPLOAD_IMAGE", CDN_IMAGE . "assets/img/member/");
define("SLIDE_TEMP_UPLOAD_IMAGE", CDN_IMAGE . "assets/img/admin/tmp_slide/");
define("SLIDE_UPLOAD_IMAGE", CDN_IMAGE . "assets/img/admin/slide/");
define("MERCHANT_LOGO", CDN_IMAGE . "assets/img/merchant/logo/");
define("PARTNER_LOGO", CDN_IMAGE . "assets/img/partner/logo/");
define("UPLOADPATH", CDN_IMAGE . "assets/csv/");
define("FILENAME", "Upload_CSV");
define("UPLOAD_PATH", "upload_path");

define("MAX_LENGTH_DEFAULT", "50");
define("MAX_HEIGHT", "max_height");
define("MAX_PHONE_LENGTH", "50");
define("MAX_EMAIL_LENGTH", "100");
define("MAX_NAME_LENGTH", "50");
define("MAX_BANK_LENGTH", "25");
define("MAX_BANK_BRANCE_LENGTH", "50");
define("MAX_POSTAL_CODE_LENGTH", "10");
define("MAX_MEMBER_MESSAGE_LENGTH", "100");
define("MAX_ADDRESS_LENGTH", "200");
define("MAX_WIDTH", "max_width");
define("MIN_HEIGHT", "min_height");
define("QUALITY", "quality");
define("MIN_WIDTH", "min_width");
define("MAX_SIZE", "max_size");
define("TEMP_NAME", "tmp_name");

define("ALLOWED_TYPES", "allowed_types");
define("IMAGE_TYPE", "jpg|png|jpeg|JPG|JPEG|PNG");

define("IMAGE_DIMENSION_LOGO", serialize(array("max_height" => 200, "min_height" => 150, "max_width" => 200, "min_width" => 150)));
define("IMAGE_DIMENSION_EXPEDITION", serialize(array("max_height" => 5000, "min_height" => 1, "max_width" => 5000, "min_width" => 1)));
define("IMAGE_DIMENSION_PAYMENT_GATEWAY", serialize(array("max_height" => 200, "min_height" => 70, "max_width" => 200, "min_width" => 70)));
define("IMAGE_DIMENSION_BANK", serialize(array("max_height" => 300, "min_height" => 70, "max_width" => 300, "min_width" => 70)));
define("IMAGE_DIMENSION_PRODUCT_CATEGORY_MAIN", serialize(array("max_height" => 520, "min_height" => 520, "max_width" => 488, "min_width" => 488)));
define("IMAGE_DIMENSION_PRODUCT_CATEGORY_ADDITIONAL", serialize(array("max_height" => 250, "min_height" => 250, "max_width" => 234, "min_width" => 234)));
define("IMAGE_DIMENSION_PRODUCT_CATEGORY_BANNER", serialize(array("max_height" => 284, "min_height" => 284, "max_width" => 1140, "min_width" => 1140)));
define("IMAGE_DIMENSION_PRODUCT_CATEGORY_HOME", serialize(array("max_height" => 520, "min_height" => 520, "max_width" => 488, "min_width" => 488)));
define("IMAGE_DIMENSION_PRODUCT_CATEGORY_HOME_ADDITIONAL", serialize(array("max_height" => 230, "min_height" => 230, "max_width" => 200, "min_width" => 200)));
define("IMAGE_DIMENSION_PRODUCT_CATEGORY_PROMO", serialize(array("max_height" => 160, "min_height" => 160, "max_width" => 600, "min_width" => 600)));

define("IMAGE_DIMENSION_SLIDE_SHOW", serialize(array("max_height" => 433, "min_height" => 433, "max_width" => 1170, "min_width" => 1170)));
define("M_DIMENSION_BANNER", serialize(array("width" => 585, "height" => 217)));
define("IMAGE_DIMENSION_BANNER", serialize(array("max_height" => 300, "min_height" => 200, "max_width" => 1200, "min_width" => 1000)));
define("IMAGE_DIMENSION_PROMO", serialize(array("max_height" => 300, "min_height" => 200, "max_width" => 1200, "min_width" => 1000)));
define("IMAGE_DIMENSION_PRODUCT", serialize(array("max_height" => 1500, "min_height" => 850, "max_width" => 1500, "min_width" => 850)));
define("IMAGE_DIMENSION_TRANSFER", serialize(array("max_height" => 5000, "min_height" => 10, "max_width" => 5000, "min_width" => 10)));

define("IMAGE_PRODUCT_INFO", "<span class=\"require\">*</span>&nbsp;upload gambar dengan ukuran min 850px dan max 1500px.");
define("IMAGE_TYPE_UPLOAD", "image/gif,image/jpeg,image/png");
define("CSV_FILE", "csv");
define("FILE_NAME", "file_name");
define("TEMP_PRODUCT_UPLOAD_IMAGE", CDN_IMAGE . "assets/img/temp_product/");
define("TEMP_PAYMENT_UPLOAD_IMAGE", CDN_IMAGE . "assets/img/admin/tmp_payment/");
define("ADV_TMP_UPLOAD_IMAGE", CDN_IMAGE . "assets/img/admin/tmp_floor/");
define("ADV_UPLOAD_IMAGE", CDN_IMAGE . "assets/img/admin/floor/");
define("PRODUCT_UPLOAD_IMAGE", CDN_IMAGE . "assets/img/product/");
define("PROFILE_IMAGE", CDN_IMAGE . "assets/img/member/");
define("PROFILE_AGENT_IMAGE", CDN_IMAGE . "assets/img/agent/");
define("PROFILE_IMAGE_MERCHANT", CDN_IMAGE . "assets/img/merchant/logo");
define("PROFILE_IMAGE_PARTNER", "assets/img/partner/logo");
define("PROFILE_IMAGE_MEMBER", CDN_IMAGE . "assets/img/member");
define("BANK_UPLOAD_IMAGE", CDN_IMAGE . "assets/img/admin/bank_logo/");
define("PROVIDER_UPLOAD_IMAGE", CDN_IMAGE . "assets/img/admin/provider_logo/");
define("RESI_UPLOAD_IMAGE", CDN_IMAGE . "assets/img/resi/");
define("CAPTCHA_IMAGE", CDN_IMAGE . "assets/img/captcha/");
define("PROMO_IMAGE", CDN_IMAGE . "assets/img/promo/");
define("CAMPAIGN_CELEBRATE_YOUR_GRADUATION_IMAGE", CDN_IMAGE . "assets/img/campaign/celebrate_your_graduation/");
define("EMAIL_IMAGE_LOGO", CDN_IMAGE . "assets/img/");

//define('IMAGE_BANK_PAYMENT', 'payment_70px/TMP_VIA_BANK.jpg');

define("XS_DIMENSION", serialize(array("width" => 55, "height" => 55)));
define("S_DIMENSION", serialize(array("width" => 175, "height" => 175)));
define("M_DIMENSION", serialize(array("width" => 350, "height" => 350)));
define("L_DIMENSION", serialize(array("width" => 550, "height" => 550)));
define("XL_DIMENSION", serialize(array("width" => 850, "height" => 850)));

define("XS_IMAGE_UPLOAD", "xs-img/");
define("S_IMAGE_UPLOAD", "s-img/");
define("M_IMAGE_UPLOAD", "m-img/");
define("L_IMAGE_UPLOAD", "l-img/");
define("XL_IMAGE_UPLOAD", "xl-img/");

/*
 * UPLOAD IMAGE
 */
define("LOGO_IMG", "nlogo_img");

/*
 * STATUS
 */
define("APPROVE_STATUS_CODE", "A");
define("REJECT_STATUS_CODE", "R");
define("WEB_STATUS_CODE", "W");
define("NEW_STATUS_CODE", "N");
define("READY_STATUS_CODE", "R");
define("PROCESS_STATUS_CODE", "S");
define("OPEN_STATUS_CODE", "O");
define("CLOSED_STATUS_CODE", "C");
define("REFUND_CODE", "RFD");
define("VERIFIED", "V");
define("UNVERIFIED", "U");
define("SUCCESS_STATUS", "success");
define("FAILED_STATUS", "failed");
define("NOT_FOUND_STATUS", "notfound");
define("VALUE", 'value');
define("REFUND_STATUS_CODE", "R");
define("LIVE_STATUS_CODE", "L");
define("CHANGE_STATUS", "C");
define("TOPUP_TRX_TYPE", "TPU");
define("TOPUP_STATUS_CODE", "T");
/*
 * PAYMENT STATUS
 */
define("PAYMENT_UNPAID_STATUS_CODE", "U");
define("PAYMENT_WAIT_CONFIRM_STATUS_CODE", "W");
define("PAYMENT_WAIT_CONFIRM_ADIRA_APPROVAL", "A");
define("PAYMENT_CONFIRM_STATUS_CODE", "C");
define("PAYMENT_PAID_STATUS_CODE", "P");
define("PAYMENT_FAILED_STATUS_CODE", "F");
define("PAYMENT_CANCEL_BY_ADMIN_STATUS_CODE", "X");
define("PAYMENT_WAIT_CONFIRM_THIRD_PARTY", "T");

define("PAYMENT_TYPE_CASH", "C");
define("PAYMENT_TYPE_TRANSFER", "T");
define('PAYMENT_HEAD', 'payment_head');
/*
 * FACEBOOK
 */
define("FACEBOOK_ID", "FBID");
define("FACEBOOK_EMAIL", "EMAIL");
define("FACEBOOK_NAME", "FULLNAME");
define("FACEBOOK_GENDER", "GENDER");
define("FACEBOOK_IMAGE", "IMAGE");

/*
 * ORDER STATUS
 */
define("ORDER_PREORDER_STATUS_CODE", "P");
define("ORDER_NEW_ORDER_STATUS_CODE", "N");
define("ORDER_READY_TO_SHIP_STATUS_CODE", "R");
define("ORDER_SHIPPING_STATUS_CODE", "S");
define("ORDER_DELIVERED_STATUS_CODE", "D");
define("ORDER_CANCEL_BY_MERCHANT_STATUS_CODE", "X");
define("ORDER_CANCEL_BY_PARTNER_STATUS_CODE", "X");
define("ORDER_CANCEL_BY_SYSTEM_STATUS_CODE", "Y");
define("ORDER_REQUEST_STATUS_CODE", "R");
define("ORDER_SUCCESS_STATUS_CODE", "S");
define("ORDER_FAIL_STATUS_CODE", "F");

/*
 * PRODUCT STATUS
 */
define("PRODUCT_READY_STATUS_CODE", "R");
define("PRODUCT_CANCEL_BY_MERCHANT_STATUS_CODE", "X");
define("PRODUCT_CANCEL_BY_PARTNER_STATUS_CODE", "X");
define('PRODUCT_PRICE', 'product_price');
/*
 * PRODUCT VARIANT
 */
define("ALL_PRODUCT_VARIANT", "1");
/*
 * PRODUCT CATEGORY
 */
define("PRODUCT_CATEGORY_HANDPHONE", "1");
define("PRODUCT_CATEGORY_ELECTRONIC_AUDIO", "8");
define("PRODUCT_CATEGORY_COMPUTER", "7");
define("PRODUCT_CATEGORY_FASHION", "3");
define("PRODUCT_CATEGORY_HEALTH_AND_COSMETIC", "4");
define("PRODUCT_CATEGORY_CAMERA", "6");
define("PRODUCT_CATEGORY_HOUSEHOLD", "9");
define("PRODUCT_CATEGORY_BABY_AND_TOY", "10");
define("PRODUCT_CATEGORY_BAG", "11");
define("PRODUCT_CATEGORY_MUSIC_AND_SPORT", "2");
define("PRODUCT_CATEGORY_OTOMOTIF", "12");
define("PRODUCT_CATEGORY_FOOD_AND_BEVERAGE", "13");
define("PRODUCT_CATEGORY_STATIONARY", "14");

/*
 * SEO FOLDER XML
 */
define("SEO_PRODUCT_CATEGORY_HANDPHONE", "sitemap-handphone-tablet.xml");
define("SEO_PRODUCT_CATEGORY_ELECTRONIC_AUDIO", "sitemap-electronic-audio.xml");
define("SEO_PRODUCT_CATEGORY_COMPUTER", "sitemap-computer.xml");
define("SEO_PRODUCT_CATEGORY_FASHION", "sitemap-fashion.xml");
define("SEO_PRODUCT_CATEGORY_HEALTH_AND_COSMETIC", "sitemap-health-cosmetic.xml");
define("SEO_PRODUCT_CATEGORY_CAMERA", "sitemap-camera.xml");
define("SEO_PRODUCT_CATEGORY_HOUSEHOLD", "sitemap-household.xml");
define("SEO_PRODUCT_CATEGORY_BABY_AND_TOY", "sitemap-baby-toy.xml");
define("SEO_PRODUCT_CATEGORY_BAG", "sitemap-bag.xml");
define("SEO_PRODUCT_CATEGORY_MUSIC_AND_SPORT", "sitemap-music-sport.xml");
define("SEO_PRODUCT_CATEGORY_OTOMOTIF", "sitemap-otomotif");
define("SEO_PRODUCT_CATEGORY_FOOD_AND_BEVERAGE", "sitemap-food-beverage.xml");
define("SEO_PRODUCT_CATEGORY_STATIONARY", "sitemap-stationary.xml");

define("SEO_HEADER_CONTENT", '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">');
define("SEO_FOOTER_CONTENT", '</urlset>');



/*
 * DEFAULT
 */
define("DEFAULT_PASSWORD", "********");
define("DEFAULT_RECORD_PER_PAGE", "5");
define("DEFAULT_QTY_PRODUCT", "1");
define("DEFAULT_EXPEDITION_SEQ_RATE", "1");
define("DEFAULT_EXPEDITION_SEQ_MERCHANT", "0");
define("DEFAULT_ERROR_404", "error_404.jpg");
define("DEFAULT_ERROR_404_XS", "error_404_xs.jpg");
define("DEFAULT_ERROR_404_SM", "error_404_sm.jpg");
define("DEFAULT_EXPEDITION_SERVICE_RATE", "REG");
define("DEFAULT_VALUE_VARIANT_SEQ", "1");
define("DEFAULT_PRODUCT_CATEGORY_HOME", "0");
define("DEFAULT_NAME", "Toko1001");
define("DEFAULT_SLOGAN", "Toko1001.id Aman Cepat Nyaman");
define("DEFAULT_TITLE", "Toko1001.id:Pusat Belanja Online Aman Cepat Nyaman untuk Handphone,Tablet,Komputer,Kamera,Fashion dan banyak lagi");
define("DEFAULT_KEYWORD", "");
define("DEFAULT_DESCRIPTION", "Pusat belanja online dengan konsep \"Aman Cepat Nyaman\" untuk produk Handphone, Tablet, Komputer, Kamera, Fashion dan banyak lagi.");
define("DEFAULT_WEBSITE", "Toko1001.id");
define("DEFAULT_MALE_GENDER", "M");
define("DEFAULT_FEMALE_GENDER", "F");
define("DEFAULT_MAX_LIMIT_IMAGE", "2097152");
define("CURL_CONNECTION_TIMEOUT", 30);
define("OPERATION_TIMEOUT", 30);
define("CAPTCHA_POOL", 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890');
define("JNE_CTC_REG_CODE", 'CTC15');
define("JAKARTA_PROVINCE_SEQ", '11');
define("PAGINATION_LIMIT", 10);
define("COOKIE_TIME", 2592000);
define("DEFAULT_EXPEDITION_COMMISSION", 15);

define("DEFAULT_SETTLED_OFF","0");
define("DEFAULT_SETTLED_ON","1");


/*
 * EXPEDITION STATUS
 */
define("DELIVERED_EXPEDITION_STATUS", "DELIVERED");
define("ON_PROCCESS_EXPEDITION_STATUS", "ONPROCESS");


/*
 * TASK TYPE
 */
define("TASK_TYPE_ADD", "add_type");
define("TASK_TYPE_DELETE", "del_type");
define("TASK_TYPE_CHECKOUT", "checkout_type");
define("TASK_TYPE_ADD_QTY", "add_qty");
define("TASK_FIRST_STEP", "1");
define("TASK_SECOND_STEP", "2");
define("TASK_FINAL_STEP", "3");
define("TASK_PAYMENT_STEP", "fin");
define("TASK_PROVINCE_CHANGE", "prov_change");
define("TASK_CUSTOMER_SEARCH", "customer_search");
define("TASK_CUSTOMER_DATA", "customer_data");
define("TASK_LOAN_SIMULATION", "simulation");
define("TASK_CITY_CHANGE", "city_change");
define("TASK_PROVIDER_CHANGE", "provider_change");
define("TASK_PROVIDER_SERVICE_CHANGE", "provider_srv_change");
define("TASK_PHONE_NUMBER_CHANGE", "phone_no_change");
define("TASK_PROVIDER_SAVE", "pro_save");
define("TASK_PROVIDER_PROCESS", "pro_proc");
define("TASK_SAVE_PROVIDER", "pro_save_sess");
define("TASK_UPDATE_CC_LOG", "update_log_cc");
define("TASK_UPDATE_DOCU_LOG", "update_log_docu");
define("TASK_MEMBER_ADDRESS_CHANGE", "member_address");
define("TASK_GET_COUPON", "get_coupon");
define("TASK_CLEAR_COUPON", "clear_coupon");
define("TASK_MENU_CHANGE", "menu_change");
define("TASK_NEXT_STEP", "next_step");

define("TASK_TYPE_SET_PRODUCT_ACTIVE", "prod_actv");
define("TASK_TYPE_SET_PRODUCT_STOCK", "prod_stock");
define("TASK_TYPE_SET_PRODUCT_PRICE", "prod_price");
define("TASK_TYPE_SET_PRODUCT_SELL_PRICE", "prod_sell_price");

define("TASK_GET_BANK", "get_bank");
define("TASK_GET_BANK_MONTH", "get_bank_month");
define("TASK_GET_ONE_CREDIT", "get_one_credit");
define("TASK_GET_VALUE_CREDIT", "get_value_cr");
define("DEFAULT_CREDIT_DESCRIPTION", "Untuk dapat melakukan pembayaran dengan cicilan, per order hanya diperbolehkan membeli 1 jenis produk. Apakah anda akan menghapus produk yang lainnya ?");

define("TASK_AGENT_ADDRESS_CHANGE", "agent_address");

/*
 * GET TYPE
 */
define("GET_DROP_DOWN_MERCHANT_BY_ORDER", "get_dropdown_merchant_by_order");
define("GET_DROP_DOWN_PARTNER_BY_ORDER", "get_dropdown_partner_by_order");

/*
 * TAB TYPE
 */
define("TAB_BASIC", "basic");
define("TAB_ADDRESS", "address");
define("TAB_PAYMENT", "payment");
define("TAB_DEPOSIT", "deposit");
define("TAB_CONFIG", "configuration");
define("TAB_ORDER", "order");
define("TAB_REVIEW", "review");
define("TAB_MESSAGE", "message");

/*
 * EXPEDITION CODE
 */

define("JNE_EXPEDITION_CODE", "JNE");
define("RAJA_KIRIM_EXPEDITION_CODE", "RK");
define("TIKI_EXPEDITION_CODE", "TIKI");
define("PANDU_EXPEDITION_CODE", "PANDU");
define("RPX_EXPEDITION_CODE", "RPX");
define("AWB_EXPEDITION_ERROR", "Koneksi ke Expedisi tidak berhasil dilakukan, silakan kontak administrator untuk dapat melanjutkan proses pengiriman");
define("EXPEDITION_CHECK", "'" . JNE_EXPEDITION_CODE . "','" . RAJA_KIRIM_EXPEDITION_CODE . "','" . RPX_EXPEDITION_CODE . "'");
/*
 * WEB FUNCTION SERVICE
 */
define("FUNCTION_GET_JNE_RATE_EXPEDITION", "get_jne_expedition_rate");
define("FUNCTION_GET_PANDU_RATE_EXPEDITION", "get_pandu_expedition_rate");
define("FUNCTION_GET_TIKI_RATE_EXPEDITION", "get_tiki_expedition_rate");
define("FUNCTION_GET_RAJA_KIRIM_RATE_EXPEDITION", "get_raja_kirim_expedition_rate");

/*
 * WEB SERVICE TYPE
 */
define("GET_RATE_TYPE", "RAT");

/*
 * MUTATION TYPE
 */
define("IN_MUTATION_TYPE", "I");
define("OUT_MUTATION_TYPE", "O");
define("CREDIT_MUTATION_TYPE", "C");
define("DEBET_MUTATION_TYPE", "D");

/*
 * PAYMENT TYPE
 */
define("PAYMENT_TYPE_BANK", "BNK");
define("PAYMENT_TYPE_DEPOSIT", "DPT");
define("PAYMENT_TYPE_CREDIT_CARD", "CC");
define("PAYMENT_TYPE_BCA_KLIKPAY", "BKP");
define("PAYMENT_TYPE_MANDIRI_KLIKPAY", "MKP");
define("PAYMENT_TYPE_MANDIRI_ECASH", "MEC");
define("PAYMENT_TYPE_DOCU_ATM", "ATM");
define("PAYMENT_TYPE_DOCU_ALFAMART", "ALF");
define("PAYMENT_TYPE_QR_CODE", "QR");
define("CURENCY_CODE", "IDR");
define("PAYMENT_DESC", "Pembayaran atas Pembelian Produk di toko1001.id");
define("RP", "Rp. ");
define("PAYMENT_TYPE_AGENT", "ADIRA");
define("PAYMENT_TYPE_CREDIT", "CRE");
define("PAYMENT_TYPE_ADIRA_DEPOSIT", "ADIRA");
define("PAYMENT_TYPE_ADIRA_BANK", "ADIRATB");

/*
 * PAYMENT SEQ
 */
define("PAYMENT_SEQ_BANK", "1");
define("PAYMENT_SEQ_DEPOSIT", "2");
define("PAYMENT_SEQ_CREDIT_CARD", "3");
define("PAYMENT_SEQ_BCA_KLIKPAY", "4");
define("PAYMENT_SEQ_MANDIRI_KLIKPAY", "5");
define("PAYMENT_SEQ_MANDIRI_ECASH", "6");
define("PAYMENT_SEQ_DOCU_ATM", "8");
define("PAYMENT_SEQ_DOCU_ALFAMART", "7");
define("PAYMENT_SEQ_QR_CODE", "9");
define("PAYMENT_SEQ_CREDIT", "12");
define("PAYMENT_SEQ_ADIRA", "13");
define("PAYMENT_SEQ_ADIRA_DEPOSIT", "10");
define("PAYMENT_SEQ_ADIRA_HEAD", "7");
define("PAYMENT_CREDIT_HEAD", "9");


/*
 * REK NO
 */
define("BCA_REK_NO", "0192321321354313");
define("MANDIRI_REK_NO", "0192321321354313");
define("MANDIRI_REK_CUSTOMER", "Toko1001");
define("BCA_REK_CUSTOMER", "Toko1001");


/*
 * STOCK TRX TYPE
 */
define("STOCK_ADJUSTMENT_TYPE", "ADJ");
define("STOCK_ORDER_MEMBER_TYPE", "ORD");
define("STOCK_CANCEL_ORDER_MEMBER_TYPE", "CNL");

/*
 * DEPOSIT ACCOUNT TYPE
 */
define("CANCEL_ACCOUNT_TYPE", "CNL");
define("WITHDRAW_ACCOUNT_TYPE", "WDW");
define("CASH_ACCOUNT_TYPE", "CSH");
define("ORDER_ACCOUNT_TYPE", "ORD");
define("RETUR_ACCOUNT_TYPE", "RTR");
define("DEPOSIT_ACCOUNT_TYPE", json_encode(array("CNL" => "Batal Order", "WDW" => "Tarik Dana", "ORD" => "Order", "RTR" => "Retur", "TPU" => "Top Up Deposit", "CSH" => 'Cashback')));
define("DEPOSIT_ACCOUNT_TYPE_FOR_REFUND_ADMIN", json_encode(array("CNL" => "Batal Order", "RTR" => "Retur")));

/*
 * PROMO VOUCHER TYPE
 */
define("VOUCHER_TYPE_AUTOMATIC", 'A');

/*
 * EMAIL CODE
 */
define("MEMBER_REG_CODE", "MEMBER_REG");
define("MEMBER_REG_PROMO", "MEMBER_REG_PROMO");
define("MEMBER_REG_CODE_SUCCESS", "MEMBER_REG_SUCCESS");
define("MEMBER_REG_FROM_FACEBOOK", "MEMBER_REG_FACEBOOK_SUCCESS");
define("MEMBER_FORGOT_PASSWORD", "MEMBER_FORGOT_PASSWORD");
define("MERCHANT_FORGOT_PASSWORD", "MERCHANT_FORGOT_PASSWORD");
define("PARTNER_FORGOT_PASSWORD", "PARTNER_FORGOT_PASSWORD");
define("MERCHANT_FORGOT_PASSWORD_VERIFICATION", "MERCHANT_FORGOT_PASSWORD_VERIFICATION");
define("PARTNER_FORGOT_PASSWORD_VERIFICATION", "PARTNER_FORGOT_PASSWORD_VERIFICATION");
define("MEMBER_FORGOT_PASSWORD_VERIFICATION", "MEMBER_FORGOT_PASSWORD_VERIFICATION");
define("MERCHANT_REG_CODE", "MERCHANT_REG");
define("MERCHANT_REG_ADMIN_CODE", "MERCHANT_REG_ADMIN");
define("MERCHANT_CANCEL_ORDER", "MERCHANT_CANCEL_ORDER");
define("PARTNER_CANCEL_ORDER", "PARTNER_CANCEL_ORDER");
define("MERCHANT_STOCK_EXCEEDED", "MERCHANT_STOCK_EXCEEDED");
define("ORDER_SENT_CODE", "ORDER_SENT");
define("ORDER_INFO", "ORDER_INFO");
define("ORDER_NEW_PARTNER_TO_MERCHANT", "ORDER_NEW_PARTNER_TO_MERCHANT");
define("ORDER_NEW_PARTNER_TO_PARTNER", "ORDER_NEW_PARTNER_TO_PARTNER");
define("REJECT_ORDER_BY_PARTNER", "REJECT_ORDER_BY_PARTNER");
define("REJECT_ORDER_BY_PARTNER_TO_AGENT_AND_CUSTOMER", "REJECT_ORDER_BY_PARTNER_TO_AGENT_AND_CUSTOMER");
define("APPROVE_ORDER_BY_PARTNER", "APPROVE_ORDER_BY_PARTNER");
define("ORDER_INFO_PARTNER", "ORDER_INFO_PARTNER");
define("GENERATE_PASSWORD_MERCHANT", "RESET_PASSWORD_MERCHANT");
define("GENERATE_PASSWORD_PARTNER", "RESET_PASSWORD_PARTNER");
define("GENERATE_PASSWORD_AGENT", "RESET_PASSWORD_AGENT");
define("E_FACEBOOK_ICON", "facebook-email.png");
define("E_TWITTER_ICON", "twitter-email.png");
define("E_INSTAGRAM_ICON", "instagram-email.png");
define("SYSTEM_CANCEL_ORDER_TO_AGENT", "SYSTEM_CANCEL_ORDER_TO_AGENT");


define("ORDER_NEW_CODE", "ORDER_NEW");
define("ORDER_NEW_PARTNER", "ORDER_NEW_PARTNER");
define("ORDER_PAY_CONFIRM_CANCEL_CODE", "ORDER_PAY_CONFIRM_CANCEL");
define("ORDER_PAY_CONFIRM_FAIL_CODE", "ORDER_PAY_CONFIRM_FAIL");
define("ORDER_PAY_CONFIRM_SUCCESS_CODE", "ORDER_PAY_CONFIRM_SUCCESS");
define("ORDER_PAY_CONFIRM_SUCCESS_WITH_AXA_CODE", "ORDER_PAY_CONFIRM_SUCCESS_WITH_AXA");
define("ORDER_RETURN_REJECT", "ORDER_RETURN_REJECT");
define("ORDER_RETURN_MERCHANT_REFUND", "ORDER_RETURN_MERCHANT_REFUND");
define("ORDER_RETURN_MERCHANT_APPROVE", "ORDER_RETURN_MERCHANT_APPROVE");
define("MEMBER_WITHDRAW_CODE", "MEMBER_WITHDRAW");
define("MEMBER_WITHDRAW_SUCCESS", "MEMBER_WITHDRAW_SUCCESS");
define("MEMBER_WITHDRAW_FAIL", "MEMBER_WITHDRAW_FAIL");
define("MEMBER_REFUND_SUCCESS_CODE", "MEMBER_REFUND_SUCCESS");
define("MEMBER_REFUND_FAIL_CODE", "MEMBER_REFUND_FAIL");
define("MEMBER_REG_SUCCESS_CODE", "MEMBER_REG_SUCCESS");
define("MEMBER_REG_SUCCESS_CODE_VOUCHER", "MEMBER_REG_SUCCESS_VOUCHER");
define("MEMBER_CONFIRM_PAYMENT", "MEMBER_CONFIRM_PAYMENT");


define("AGENT_WITHDRAW_CODE", "AGENT_WITHDRAW");
define("AGENT_TOPUP_DEPOSIT", "AGENT_TOPUP_DEPOSIT");
define("AGENT_WITHDRAW_SUCCESS", "AGENT_WITHDRAW_SUCCESS");
define("AGENT_WITHDRAW_FAIL", "AGENT_WITHDRAW_FAIL");
define("AGENT_REFUND_SUCCESS_CODE", "AGENT_REFUND_SUCCESS");
define("AGENT_REFUND_FAIL_CODE", "AGENT_REFUND_FAIL");
define("AGENT_REG_SUCCESS_CODE", "AGENT_REG_SUCCESS");
define("AGENT_REG_SUCCESS_CODE_VOUCHER", "AGENT_REG_SUCCESS_VOUCHER");
define("AGENT_CONFIRM_PAYMENT", "AGENT_CONFIRM_PAYMENT");
define("AGENT_FORGOT_PASSWORD", "AGENT_FORGOT_PASSWORD");
define("AGENT_FORGOT_PASSWORD_VERIFICATION", "AGENT_FORGOT_PASSWORD_VERIFICATION");

define('AGENT_COMMISSION_SUCCESS', 'COMMISSION_CASH_AGENT');
/*
 * NODE CODE
 */
define("NODE_REGISTRATION_MEMBER", "REG");
define("NODE_GIFT_MEMBER", "GFT");
define("NODE_SHOPPING_MEMBER", "BUY");
define("NODE_COUPON_MEMBER", "COU");

/*
 * SHIP STATUS
 */
define("SHIP_FROM_MEMBER", "M");
define("SHIP_TO_MERCHANT", "S");
define("SHIP_TO_MEMBER", "T");
define("RECEIVED_BY_MERCHANT", "R");
define("RECEIVED_BY_MEMBER", "C");
define("RECEIVED_BY_ADMIN", "A");
define("RETURN_ITEM_BY_MERCHANT", "I");
define("REFUND_BY_MERCHANT", "F");
define("REJECT_BY_MERCHANT", "R");
define("REJECT_BY_PARTNER", "R");

/*
 * TIME
 */
define("DEFAULT_DATETIME", "0000-00-00 00:00:00");
define("DEFAULT_DATE", "0000-00-00");

/*
 * STATUS TYPE
 */
define("STATUS_MEMBER", json_encode(array("N" => "Baru", "A" => "Disetujui", "S" => "Di-non-aktifkan", "U" => "Unggah")));
define("STATUS_AGENT", json_encode(array("A" => "Disetujui", "S" => "Di-non-aktifkan")));
define("STATUS_RECEIPT", json_encode(array("O" => "Baru", "V" => "Disetujui", "C" => "Selesai")));
define("STATUS_ANR", json_encode(array("A" => "Disetujui", "N" => "Baru", "R" => "Ditolak")));
define("STATUS_ANRL", json_encode(array("A" => "Disetujui", "N" => "Pengajuan", "R" => "Ditolak", "L" => "Data Akhir")));
define("STATUS_FNR", json_encode(array("F" => "Perbaikan", "N" => "Baru", "R" => "Ditolak")));
define("STATUS_NRAV", json_encode(array("N" => "Baru", "R" => "Ditolak", "A" => "Disetujui", "V" => "Terverifikasi")));
define("STATUS_OPC", json_encode(array("O" => "Periode Terbuka", "P" => "Proses", "C" => "Periode Selesai")));
define("STATUS_WNR", json_encode(array("W" => "Dari Web", "N" => "Baru", "R" => "Ditolak")));
define("STATUS_UPAID", json_encode(array("U" => "Belum Dibayar", "P" => "Sudah Dibayar")));
define("STATUS_REDEEM_AGENT", json_encode(array("U" => "Belum Dibayar", "P" => "Sudah Dibayar", "T" => "Sudah Dibayar ke Merchant")));
define("STATUS_PAYMENT", json_encode(array("A" => "Menunggu Approval Adira", "U" => "Belum Dibayar", "P" => "Lunas", "F" => "Gagal Bayar", "X" => "Batal", "W" => "Menunggu Pembayaran Member", "C" => "Menunggu Konfirmasi Toko1001", "T" => "Menunggu Konfirmasi Pembayaran")));
define("STATUS_PAY", json_encode(array("P" => "Lunas Dengan Nominal", "F" => "Gagal Bayar", "X" => "Batal Order")));
define("STATUS_ORDER", json_encode(array("N" => "Baru", "R" => "Siap Kirim", "S" => "Proses Kirim", "D" => "Terkirim", "X" => "Batal Kirim", "Y" => "Gagal Kirim", "P" => "Pending")));
define("STATUS_VERIFIED", json_encode(array("U" => "Belum di verifikasi", "V" => "Sudah di verifikasi")));
define("STATUS_XR", json_encode(array("X" => "Dibatalkan oleh Merchant", "R" => "Siap")));
define("STATUS_WITHDRAW", json_encode(array("W" => "Pengajuan", "N" => "Baru", "A" => "Disetujui", "R" => "Ditolak", "T" => "Top Up Baru")));
define("STATUS_WITHDRAW_NO_T", json_encode(array("W" => "Pengajuan", "N" => "Baru", "A" => "Disetujui", "R" => "Ditolak")));
define("STATUS_LC", json_encode(array("L" => "Tayang", "C" => "Perubahan")));
define("REDEEM_TYPE", json_encode(array("ORD" => "ORDER", "EXP" => "EKSPEDISI", "CFA" => "Commision Fee Partner")));
define("VOUCHER_TYPE", json_encode(array("A" => "Auto", "M" => "Manual")));
define("STATUS_SHIPMENT", json_encode(array("M" => "Dikirim ke Toko1001", "A" => "Diterima Toko1001", "S" => "Dikirim ke Merchant", "R" => "Diterima Merchant", "T" => "Dikirim ke Member", "C" => "Diterima Member")));
define("STATUS_SHIPMENT_BY_MERCHANT", json_encode(array("S" => "Dikirim ke Merchant", "R" => "Diterima Merchant", "T" => "Dikirim ke Member", "C" => "Diterima Member")));
define("STATUS_RETURN", json_encode(array("A" => "Disetujui", "R" => "Ditolak", "N" => "Pengajuan", "I" => "Pengembalian Barang", "F" => "Pengembalian Dana")));
define("STATUS_RETURN_BY_MERCHANT", json_encode(array("R" => "Ditolak", "F" => "Pengembalian Dana", "I" => "Pengembalian Barang")));
define("STATUS_GENDER", json_encode(array("M" => "Pria", "F" => "Wanita")));
define("STATUS_ORDER_VOUCHER", json_encode(array('R' => ' Permintaan', 'F' => 'Gagal', 'S' => 'Berhasil')));
define("STATUS_MESSAGE", json_encode(array('U' => 'Belum Dilihat', 'R' => 'Sudah Dilihat')));
define("STATUS_LOAN", json_encode(array("N" => "Baru", "A" => "Approved by Partner", "P" => "Paid by Customer", "F" => "Pembayaran full (apporval admin)", "D" => "Pembayaran full oleh partner", "R" => "Reject by Partner")));
define("STATUS_PRODUCT_MERCHANT", json_encode(array("C" => "Perubahan", "L" => "Tayang")));

/* HOME PAGE: NUMBER OF CATEGORY DROPDOWN COLUMN */
define("ASSET_IMG_HOME", 'assets/img/home/');
define("NUMBER_OF_CATEGORY_COLUMN", 4);

define("ICON_HANDPHONE_TABLET_BLACK", 'icon icon-handphone-tablet');
define("ICON_KOMPUTER_BLACK", 'icon icon-komputer-1');
define("ICON_ELEKTRONIK_AUDIO_BLACK", 'icon icon-elektronik-audio');
define("ICON_FASHION_BLACK", 'icon icon-fashion');
//define("ICON_FASHION_PRIA",'fashion%20pria.png');
//define("ICON_FASHION_WANITA",'fashion_02.png');
define("ICON_KESEHATAN_KECANTIKAN_BLACK", 'icon icon-kesehatan-kecantikan');
define("ICON_HOBBY_JAM_TANGAN_BLACK", 'icon icon-hobby-jamtangan');
define("ICON_KAMERA_BLACK", 'icon icon-kamera-1');
define("ICON_PERALATAN_RUMAH_TANGGA_BLACK", 'icon icon-peralatan-rumahtangga');
define("ICON_MAINAN_BAYI_BLACK", 'icon icon-mainan-bayi');
define("ICON_TAS_KOPER_BLACK", 'icon icon-tas-koper');
define("ICON_OLAHRAGA_MUSIK_BLACK", 'icon icon-olahraga-musik');
define("ICON_OTOMOTIF_BLACK", 'icon icon-otomotif-1');
define("ICON_MAKANAN_MINUMAN_BLACK", 'icon icon-makanan-minuman');
define("ICON_BUKU_ALAT_TULIS_BLACK", 'icon icon-buku-alat-tulis');
define("ICON_VOUCHER_LAYANAN_BLACK", 'icon icon-voucher');
define('ICON_VOUCHER_PERAWATAN_HEWAN_PELIHARAAN_BLACK','icon icon-perawatan-hewan-peliharaan');

define("ICON_HANDPHONE_TABLET_WHITE", 'icon icon-handphone-tablet');
define("ICON_KOMPUTER_WHITE", 'icon icon-komputer-1');
define("ICON_ELEKTRONIK_AUDIO_WHITE", 'icon icon-elektronik-audio');
define("ICON_FASHION_WHITE", 'icon icon-fashion');
define("ICON_KESEHATAN_KECANTIKAN_WHITE", 'icon icon-kesehatan-kecantikan');
define("ICON_HOBBY_JAM_TANGAN_WHITE", 'icon icon-hobby-jamtangan');
define("ICON_KAMERA_WHITE", 'icon icon-kamera-1');
define("ICON_PERALATAN_RUMAH_TANGGA_WHITE", 'icon icon-peralatan-rumahtangga');
define("ICON_MAINAN_BAYI_WHITE", 'icon icon-mainan-bayi');
define("ICON_TAS_KOPER_WHITE", 'icon icon-tas-koper');
define("ICON_OLAHRAGA_MUSIK_WHITE", 'icon icon-olahraga-musik');
define("ICON_OTOMOTIF_WHITE", 'icon icon-otomotif-1');
define("ICON_MAKANAN_MINUMAN_WHITE", 'icon icon-makanan-minuman');
define("ICON_BUKU_ALAT_TULIS_WHITE", 'icon icon-buku-alat-tulis');
define("ICON_VOUCHER_LAYANAN_WHITE", 'icon icon-voucher');
define("ICON_VOUCHER_PERAWATAN_HEWAN_PELIHARAAN_WHITE","icon icon-perawatan-hewan-peliharaan");
define("FLOOR_MAX", 6);
define("LEFT_MENU_LEVEL2_MAX", 8);

define("START_OFFSET", "mulai");
define("SEARCH", "e"); //search parameter
define("CATEGORY", "b"); //category identifier
define("PRICE_RANGE", "pr"); //category filter price parameter
define("OLD_TO_NEW_PRODUCT", "on"); //category filter order parameter
define("NEW_TO_OLD_PRODUCT", "no"); //category filter order parameter
define("PRICE_EXPENSIVE_TO_CHEAP", "ec"); //category filter order parameter
define("PRICE_CHEAP_TO_EXPENSIVE", "ce"); //category filter order parameter
define("ORDER_CATEGORY", "g");
define("ALL_CATEGORY", "kategori"); //show all category url
define("PARAMETER_CATEGORY_ATTRIBUTE", "attribute_list");
define('SELL_PRICE', 'sell_price');
define("VERIFICATION_MEMBER", "member/verifikasi_registrasi/");
define("VERIFICATION_MEMBER_FORGOT_PASSWORD", "member/verify_password/");
define("VERIFICATION_MERCHANT_FORGOT_PASSWORD", "merchant/verify_password/");
define("VERIFICATION_PARTNER_FORGOT_PASSWORD", "partner/verify_password/");
define("VERIFICATION_AGENT_FORGOT_PASSWORD", "agent/verify_password/");
define("lOGIN_MEMBER", "member/login");
define("MEMBER_CEK_DEPOSIT", "member/");
define("AGENT_CEK_DEPOSIT", "agent/");
define("MEMBER_ORDER_RETURN", "member/product_return");
define("AGENT_ORDER_RETURN", "agent/product_return");

/*
 * TYPE SECURITY PASS
 */
define("CHANGE_PASSWORD_TYPE", "1");
define("FORGOT_PASSWORD_TYPE", "2");

define("CONTINUE_URL", "continue");


/*
 * DATE FORMAT
 */
define("LAST_WEEK", '-7 days');
define("NEXT_WEEK", '+7 days');
define("BLANK_TARGET", '_blank');

/*
 * DOKU PAYMENT CHANNEL
 */
define("CREDIT_CARD_VISA__MASTER_IDR", "01");
define("MANDIRI_CLICKPAY", "02");
define("KLIK_BCA", "03");
define("DOKU_WALLET", "04");
define("ATM_PERMATA_VA_LITE", "05");
define("BRI_E-Pay", "06");
define("ATM_PERMATA_VA", "07");
define("MANDIRI_MULTIPAYMENT_LITE", "08");
define("MANDIRI_MULTIPAYMENT", "09");
define("ATM_MANDIRI_VA_LITE", "10");
define("ATM_MANDIRI_VA", "11");
define("PAYPAL", "12");
define("BNI_DEBIT_ONLINE_(VCN)", "13");
define("ALFAMART", "14");
define("CREDIT_CARD_VISA_MASTER_MULTI_CURRENCY", "15");
define("TOKENIZATION", "16");
define("RECUR", "17");
define("KLICKPAY_BCA", "18");
define("CIMB_CLICKS", "19");
define("PTPOS", "20");
define("SINARMASS_VA_FULL", "21");
define("SINARMASS_VA_LITE", "22");
define("MOTO", "23");
define("MALLID", "2464");
define("SHARED_KEY", "q90R5oQxDqP7");
define("CHAIN_MERCHANT", "NA");
define("DOCU_CURRENCY", 360);


//credit

define("CREDIT_STATUS_ACTIVE", "A");
define("MINIMUM_TOTAL_ORDER_AXA", "500000");
define("MINIMUM_DATE_ORDER_AXA", "2016-08-23"); //YYYY-mm-dd
define("SUCCESS_REGISTRATION_AXA", 'Terima kasih telah mendaftar Member AXA Life Insurance melalui toko1001');

//order history
define("STATUS_HISTORY", json_encode(array("N" => "Pesanan Baru", "C" => "Konfirmasi Pembayaran", "P" => "Verifikasi Pembayaran", "F" => "Gagal Bayar", "X" => "Pembatalan order oleh Sistem", "R" => "Pesanan sedang diproses untuk dikirim", "S" => "Pesanan telah dikirim", "D" => "Pesanan telah diterima pelanggan", "Y" => "Pembatalan order oleh Sistem", "XM" => "Pembatalan order oleh Merchant", "FINISH" => "Selesai", "W" => "")));
define("FINISH_DATE", "7");
define("VIEW_MODE", "view_mode");

//notif
define("PAID_ORDER_MERCHANT", "PAID_ORDER_MERCHANT");
define("PAID_ORDER_PARTNER", "PAID_ORDER_PARTNER");
define("DELIVERED_ORDER_MERCHANT", "DELIVERED_ORDER_MERCHANT");
define("DELIVERED_ORDER_PARTNER", "DELIVERED_ORDER_PARTNER");
define("CANCEL_ORDER_BY_SYSTEM_MERCHANT", "CANCEL_ORDER_BY_SYSTEM_MERCHANT");
define("CANCEL_ORDER_BY_SYSTEM_PARTNER", "CANCEL_ORDER_BY_SYSTEM_PARTNER");
define("CANCEL_ORDER_BY_SYSTEM_ADMIN", "CANCEL_ORDER_BY_SYSTEM_ADMIN");
define("ORDER_UNDELIVERED_MERCHANT", "ORDER_UNDELIVERED_MERCHANT");
define("ORDER_UNDELIVERED_PARTNER", "ORDER_UNDELIVERED_PARTNER");
define("REDEEM_MERCHANT", "REDEEM_MERCHANT");
define("REDEEM_PARTNER", "REDEEM_PARTNER");
define("NEW_ORDER_MEMBER", "NEW_ORDER_MEMBER");
define("NEW_ORDER_AGENT", "NEW_ORDER_AGENT");
define("NEW_ORDER_TRANSFER_BANK_MEMBER", "NEW_ORDER_TRANSFER_BANK_MEMBER");
define("NEW_ORDER_TRANSFER_BANK_AGENT", "NEW_ORDER_TRANSFER_BANK_AGENT");
define("DELIVERED_ORDER_MEMBER ", "DELIVERED_ORDER_MEMBER");
define("DELIVERED_ORDER_AGENT ", "DELIVERED_ORDER_AGENT");
define("CANCEL_ORDER_MEMBER", "CANCEL_ORDER_MEMBER");
define("CANCEL_ORDER_AGENT", "CANCEL_ORDER_AGENT");
define("CANCEL_ORDER_BY_SYSTEM_MEMBER", "CANCEL_ORDER_BY_SYSTEM_MEMBER");
define("CANCEL_ORDER_BY_SYSTEM_AGENT", "CANCEL_ORDER_BY_SYSTEM_AGENT");
define("PAID_ORDER_MEMBER", "PAID_ORDER_MEMBER");
define("PAID_ORDER_AGENT", "PAID_ORDER_AGENT");
define("RATE_PRODUCT_ORDER_MEMBER", "RATE_PRODUCT_ORDER_MEMBER");
define("RATE_PRODUCT_ORDER_AGENT", "RATE_PRODUCT_ORDER_AGENT");
define("REDEEM_REMINDER_ADMIN", "REDEEM_REMINDER_ADMIN");


define("CANCEL_ORDER_BY_PAYMENT_ADMIN", "CANCEL_ORDER_BY_PAYMENT_ADMIN");
define("FAIL_ORDER_BY_PAYMENT_ADMIN", "FAIL_ORDER_BY_PAYMENT_ADMIN");
define("ORDER_IN_SHIPPING_MEMBER", "ORDER_IN_SHIPPING_MEMBER");

define("CANCEL_ORDER_BY_PAYMENT_ADMIN_MEMBER", "CANCEL_ORDER_BY_PAYMENT_ADMIN_MEMBER");
define("CANCEL_ORDER_BY_PAYMENT_ADMIN_AGENT", "CANCEL_ORDER_BY_PAYMENT_ADMIN_AGENT");
define("FAIL_ORDER_BY_PAYMENT_ADMIN_MEMBER", "FAIL_ORDER_BY_PAYMENT_ADMIN_MEMBER");
define("FAIL_ORDER_BY_PAYMENT_ADMIN_AGENT", "FAIL_ORDER_BY_PAYMENT_ADMIN_AGENT");

define("ORDER_IN_SHIPPING_AGENT", "ORDER_IN_SHIPPING_AGENT");
define("MEMBER_CONFIRM_PAYMENT_ADMIN", "MEMBER_CONFIRM_PAYMENT_ADMIN");
define("NEW_MERCHANT_ADMIN", "NEW_MERCHANT_ADMIN");
define("MERCHANT_AWB_ERROR_ADMIN", "MERCHANT_AWB_ERROR_ADMIN");


/*
 * ADIRA REQUIREMENT
 */
define('ADIRA_MINIMUM_CREDIT_PAYMENT', 1500000);
define('AGENT_TOPUP_DEPOSIT_APPROVE', 'AGENT_TOPUP_DEPOSIT_APPROVE');


//STORE STATUS

define('OPEN_STORE', 'O');
define('CLOSE_STORE', 'C');
define('LOAN_PARTNER', 'LOAN_PARTNER');

define("SECURECONNECTION", true);

define('SESSION_PAID', 'error_paid');
define('SUCCESS_PAID', 'succes_paid');
define('SUCCESS_SAVE_PAID', 'Data sudah berhasil dibuat!');
define('SESSION_PAID_CHECKED', 'session_paid_checked');
define('SESSION_PAID_SELECTED', 'session_paid_selected');
define('PAID_DATE_SELECTED', 'paid_date_selected');

//APRIL_WOW
define('APRIL_WOW', 'april_wow');
define('TREE_CATEGORY', 'tree_category');
define('TASK_CATEOGRY', 'category');
define('CAMPAIGN_IMG', 'assets/img/campaign/');
define('TEMP_FOLDER_EXCEL', 'temp');
define('EXCEL_AGENT_DOWNLOAD_TEMPLATE', 'excel_template/master_template/Agent_Register_Template.xlsx');
define('EXCEL_FOLDER_TEMPLATE', 'excel_template');
define('EXCEL_COLUMN_A', 'A');
define('EXCEL_COLUMN_B', 'B');
define('EXCEL_COLUMN_C', 'C');
define('EXCEL_COLUMN_D', 'D');
define('EXCEL_COLUMN_E', 'E');
define('EXCEL_COLUMN_F', 'F');
define('EXCEL_COLUMN_G', 'G');
define('EXCEL_COLUMN_H', 'H');
define('ERROR_EXCEL_FILE_NO_COMPATIBLE', 'Data File Tidak Sesuai, Yang diminta adalah file excel');
define('ERROR_EMAIL_FORMAT', 'Format Email Tidak Sesuai');
define('ERROR_NAME_MUST_FILL', 'Nama Tidak Boleh Dikosongkan');
define('ERROR_COMMISION_MUST_FILL', 'Komisi Tidak Boleh Dikosongkan');
define('EXCEL_EXTENTION_NAME', 'xlsx');
define('QTY', 'qty');
define('IESE_URL', 'promo/iese_2017');
define('IESE_IMG', 'iese_2017');
define('IESE_ALWAYS_ON', 'always_on');
define('IESE_ENJOY_YOUR_TIME', 'enjoy_your_time');
define('IESE_GET_STYLISH', 'get_stylish');
define('IESE_TAKE_THE_MOMMENT', 'take_the_momment');
define('BREADCRUMB_PAGE', 'breadcrumb_pages');
define('IESE_DATA', 'iese');


/*
 * FOR RAMADHAN EVENT
 */

define('RAMADHAN_BERKAH_URL', 'berkah_ramadhan/');
define('RAMADHAN_CATEGORI_SMARTPHONE_EVERCOSS', 'smartphone_evercoss_400_ribuan');
define('RAMADHAN_CATEGORI_TAB_EVERCOSS', 'tablet_evercoss_dibawah_1_jutaan');   
define('RAMADHAN_CATEGORI_TELEVISION', 'televisi_dibawah_2_juta');
define('RAMADHAN_CATEGORI_MUSLIM_WEAR', 'pakaian_muslim_diskon_sampai_20_persen');
define('RAMADHAN_CATEGORI_SARANGE_COSMETICT', 'kosmetik_sarange_diskon_sampai_70_persen');
define('RAMADHAN_CATEGORI_POWER_BANK', 'power_bank_untuk_mudik_mu_diskon_sampai_72_persen');
define('RAMADHAN_CATEGORI_HOUSE_HOLD_EQUIPMENT', 'perangkat_minum_dan_gelas_hemat_hingga_56_persen');
define('RAMADHAN_CATEGORI_SHOES_SPORT', 'sepatu_olahraga_hemat_hingga_20_persen');
define('RAMADHAN_CATEGORI_WATCH', 'aneka_jam_tangan_modis');
define('RAMADHAN_CATEGORY_LUNA', 'luna_smartphone');

define('BREADCRUMB_PAGES','breadcrumb_pages');
define('DATA_RAMADHAN','data_ramadhan');
//function name

define('FUNCTION_SMARTPHONE_EVERCOSS','smartphone_evercoss');
define('FUNCTION_TAB_EVERCOSS','tablet_evercoss');
define('FUNCTION_TELEVISION','television');
define('FUNCTION_MUSLIM_WEAR','muslim_wear');
define('FUNCTION_SARANGE','sarange_kosmetik');
define('FUNCTION_POWER_BANK','powerbank');
define('FUNCTION_HOUSE_HOLD_EQUIPMENT','household_equipment');
define('FUNCTION_SHOES_SPORT','shoes_sport');
define('FUNCTION_WATCH','watch'); 
define('FUNCTION_LUNA', 'luna'); 