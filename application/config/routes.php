<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */

/*
 * ADMIN ROUTE
 */ 
$route['(?i)admin/login'] = 'admin/C_login_admin';
$route['(?i)admin/login/sign_in'] = 'admin/C_login_admin/sign_in';
$route['(?i)admin/main_page'] = 'admin/C_main_page_admin';
$route['(?i)admin/profile/change_password'] = 'admin/profile/C_change_password';

/*
 * ADMIN MASTER ROUTE
 */
$route['(?i)logout/admin/login'] = 'admin/C_main_page_admin/sign_out';
$route['(?i)admin/master/province'] = 'admin/master/C_province_admin';
$route['(?i)admin/master/province/filter'] = 'admin/master/C_province_admin/get_filter_form';
$route['(?i)admin/master/city'] = 'admin/master/C_city_admin';
$route['(?i)admin/master/district'] = 'admin/master/C_district_admin';
$route['(?i)admin/master/setting'] = 'admin/master/C_setting_admin';
$route['(?i)admin/master/setting_type'] = 'admin/master/C_setting_type_admin';
$route['(?i)admin/master/attribute_category'] = 'admin/master/C_attribute_category_admin';
$route['(?i)admin/master/bank'] = 'admin/master/C_bank_admin';
$route['(?i)admin/master/expedition'] = 'admin/master/C_expedition_admin';
$route['(?i)admin/master/expedition_detail/(:num)'] = 'admin/master/C_expedition_admin_detail';
$route['(?i)admin/master/expedition_awb/(:num)'] = 'admin/master/C_expedition_admin_awb';
$route['(?i)admin/master/variant_product'] = 'admin/master/C_variant_product_admin';
$route['(?i)admin/master/product_attribute'] = 'admin/master/C_product_attribute_admin';
$route['(?i)admin/master/user'] = 'admin/master/C_user_admin';
$route['(?i)admin/master/user_group'] = 'admin/master/C_user_group_admin';
$route['(?i)admin/master/menu'] = 'admin/master/C_menu_admin';
$route['(?i)admin/master/promo_free_fee'] = 'admin/master/C_promo_free_fee_admin';
$route['(?i)admin/master/product_category'] = 'admin/master/C_product_category_admin';
$route['(?i)admin/master/image'] = 'admin/master/C_image_admin';
$route['(?i)admin/master/image_slide_show'] = 'admin/master/C_image_slide_show_admin';
$route['(?i)admin/master/variant_detail/(:num)'] = 'admin/master/C_variant_product_admin_detail';
$route['(?i)admin/master/product_attribute_detail/(:num)'] = 'admin/master/C_product_attribute_admin_detail';
$route['(?i)admin/master/user'] = 'admin/master/C_user_admin';
$route['(?i)admin/master/user_group'] = 'admin/master/C_user_group_admin';
$route['(?i)admin/master/menu'] = 'admin/master/C_menu_admin';
$route['(?i)admin/master/user_admin'] = 'admin/master/C_user_admin';
$route['(?i)admin/master/promo_free_fee'] = 'admin/master/C_promo_free_fee_admin';
$route['(?i)admin/master/product_category'] = 'admin/master/C_product_category_admin';
$route['(?i)admin/master/gambar'] = 'admin/master/C_gambar_admin';
$route['(?i)admin/master/promo_free_fee'] = 'admin/master/C_promo_free_fee_admin';
$route['(?i)admin/master/promo_voucher_periode'] = 'admin/master/C_promo_voucher_periode_admin';
$route['(?i)admin/master/promo_voucher_periode_detail/(:num)'] = 'admin/master/C_promo_voucher_periode_admin_detail';
$route['(?i)admin/master/gambar'] = 'admin/master/C_gambar_admin';
$route['(?i)admin/master/promo_free_fee_detail/(:num)'] = 'admin/master/C_promo_free_fee_admin_detail';
$route['(?i)admin/master/product_category'] = 'admin/master/C_product_category_admin';
$route['(?i)admin/master/expedition_city'] = 'admin/master/C_expedition_city_admin';
$route['(?i)admin/master/merchant'] = 'admin/master/C_merchant_admin';
$route['(?i)admin/master/payment_gateway'] = 'admin/master/C_payment_gateway_admin';
$route['(?i)admin/master/payment_gateway_detail/(:num)'] = 'admin/master/C_payment_gateway_admin_detail';
$route['(?i)admin/master/merchant_detail/(:num)'] = 'admin/master/C_merchant_admin_detail';
$route['(?i)admin/master/product_new'] = 'admin/master/C_product_new_admin';
$route['(?i)admin/master/user_group_menu/(:num)'] = 'admin/master/C_user_group_admin_menu';
$route['(?i)admin/master/merchant_log'] = 'admin/master/C_merchant_log_admin';
$route['(?i)admin/master/merchant_log_detail/(:any)'] = 'admin/master/C_merchant_log_admin_detail';
$route['(?i)admin/master/member'] = 'admin/master/C_member_admin';
$route['(?i)admin/master/member/save_approve'] = 'admin/master/C_member_admin/save_approve_all';
$route['(?i)admin/master/product_change'] = 'admin/master/C_product_change_admin';
$route['(?i)admin/master/promo_free_fee_merchant/(:num)'] = 'admin/master/C_promo_free_fee_admin_merchant';
$route['(?i)admin/master/partner_live'] = 'admin/master/C_partner_live';
$route['(?i)admin/master/partner_loan'] = 'admin/master/C_partner_loan';
$route['(?i)admin/master/partner_down_payment'] = 'admin/master/C_partner_down_payment';
$route['(?i)admin/master/merchant_live'] = 'admin/master/C_merchant_live';
$route['(?i)admin/master/merchant_live_commission/(:num)'] = 'admin/master/C_merchant_live_commission';
$route['(?i)admin/product_list'] = 'admin/master/C_product_list_admin';
$route['(?i)admin/master/product_non_active_admin'] = 'admin/master/C_product_non_active_admin';
$route['(?i)admin/master/member_review'] = 'admin/master/C_member_review_admin';
$route['(?i)admin/master/coupon'] = 'admin/master/C_coupon_admin';
$route['(?i)admin/master/coupon_detail/(:any)'] = 'admin/master/C_coupon_admin_detail';
$route['(?i)admin/master/coupon_transaction/(:any)'] = 'admin/master/C_coupon_admin_trx';
$route['(?i)admin/master/promo_subscribe'] = 'admin/master/C_image_promo_subscribe_admin';
$route['(?i)admin/master/promo_credit'] = 'admin/master/C_promo_credit_admin';
$route['(?i)admin/master/promo_credit_product/(:any)'] = 'admin/master/C_promo_credit_admin_product';
$route['(?i)admin/master/promo_credit_bank/(:any)'] = 'admin/master/C_promo_credit_admin_bank';
$route['(?i)admin/master/bank_credit'] = 'admin/master/C_bank_credit_admin';
$route['(?i)admin/master/bank_credit_detail/(:any)'] = 'admin/master/C_bank_credit_admin_detail';
$route['(?i)admin/master/provider'] = 'admin/master/C_provider_admin';
$route['(?i)admin/master/provider_service/(:any)'] = 'admin/master/C_provider_service_admin_detail';
$route['(?i)admin/master/provider_service_nominal/(:any)'] = 'admin/master/C_provider_service_nominal_admin_detail';
$route['(?i)admin/master/provider_phone_code/(:any)'] = 'admin/master/C_provider_phone_code_detail';
$route['(?i)admin/master/message_log'] = 'admin/master/C_message_log_admin';
$route['(?i)admin/master/agent'] = 'admin/master/C_agent_admin';
$route['(?i)admin/master/agent_top_up'] = 'admin/master/C_agent_top_up_admin';
$route['(?i)admin/master/agent/refund'] = 'admin/master/C_agent_refund';
$route['(?i)admin/master/agent/refund_detail/(:any)'] = 'admin/master/C_agent_refund_detail';
$route['(?i)admin/master/agent_withdraw_admin'] = 'admin/master/C_agent_withdraw_admin';

/*
 * ADMIN TRANSACTION ROUTES
 */
$route['(?i)admin/transaction/order'] = 'admin/transaction/C_order_admin';
$route['(?i)admin/transaction/order_merchant'] = 'admin/transaction/C_order_merchant_admin';
$route['(?i)admin/transaction/order_detail/(:any)'] = 'admin/transaction/C_order_admin_detail';
$route['(?i)admin/transaction/expedition_order'] = 'admin/transaction/C_expedition_order';
$route['(?i)admin/transaction/member_withdraw_admin'] = 'admin/transaction/C_member_withdraw_admin';
$route['(?i)admin/transaction/redeem_period'] = 'admin/transaction/C_redeem_period_admin';
$route['(?i)admin/transaction/redeem_period_merchant/(:num)'] = 'admin/transaction/C_redeem_period_merchant_admin';
$route['(?i)admin/transaction/expedition_receipt'] = 'admin/transaction/C_expedition_receipt_admin';
$route['(?i)admin/transaction/expedition_receipt_detail/(:num)'] = 'admin/transaction/C_expedition_receipt_admin_detail';
$route['(?i)admin/transaction/member/refund'] = 'admin/transaction/C_member_refund';
$route['(?i)admin/transaction/member/refund_detail/(:any)'] = 'admin/transaction/C_member_refund_detail';
$route['(?i)admin/transaction/order_return_admin'] = 'admin/transaction/C_order_return_admin';
$route['(?i)admin/transaction/order_return_admin_received/(:any)'] = 'admin/transaction/C_order_return_admin_received';
$route['(?i)admin/transaction/ship_to_member/(:any)'] = 'admin/transaction/C_order_return_admin_ship_to_member';
$route['(?i)admin/transaction/ship_to_merchant/(:any)'] = 'admin/transaction/C_order_return_admin_ship_to_merchant';
$route['(?i)admin/transaction/member_get_axa'] = 'admin/transaction/C_member_get_axa';
$route['(?i)admin/transaction/order_voucher'] = 'admin/transaction/C_order_voucher_admin';
$route['(?i)admin/transaction/member_get_axa'] = 'admin/transaction/C_member_get_axa';
$route['(?i)admin/transaction/redeem_agent'] = 'admin/transaction/C_redeem_agent_admin';
$route['(?i)admin/transaction/redeem_period_agent/(:num)'] = 'admin/transaction/C_redeem_period_agent_admin';
$route['(?i)admin/transaction/invoicing_period'] = 'admin/transaction/C_invoicing_period_admin';
$route['(?i)admin/transaction/invoicing_period_partner/(:num)'] = 'admin/transaction/C_invoicing_period_partner_admin';

$route['(?i)admin/master/partner_group'] = 'admin/master/C_partner_group';
$route['(?i)admin/master/partner_group_detail/(:num)'] = 'admin/master/C_partner_group_detail';

$route['admin/master/commission_product_partner'] = 'admin/master/C_commission_product_partner';
$route['admin/master/commission_product_partner_detail/(:any)'] = 'admin/master/C_commission_product_partner_detail';
$route['admin/master/commission_product_partner_detail'] = 'admin/master/C_commission_product_partner_detail';

/*
 * MERCHANT ROUTES
 */
//$route['(?i)logout/merchant'] = LIVEVIEW . 'merchant/C_main_page_merchant/sign_out';
$route['(?i)merchant/sign_up'] = LIVEVIEW . 'merchant/master/C_signup_merchant';
$route['(?i)merchant/sign_up/register_merchant_check'] = LIVEVIEW . 'merchant/master/C_signup_merchant/get_check';
$route['(?i)merchant/forgot_password'] = LIVEVIEW . 'merchant/master/C_forgot_password_merchant';
$route['(?i)merchant/verify_password/(:any)'] = LIVEVIEW . 'merchant/master/C_verify_password_merchant';
$route['(?i)merchant/login/sign_in'] = LIVEVIEW . 'merchant/C_login_merchant/sign_in';
$route['(?i)merchant/main_page'] = LIVEVIEW . 'merchant/C_main_page_merchant';
$route['(?i)merchant/profile/(:any)/(:any)'] = LIVEVIEW . 'home/C_redirect_merchant_to_landing_page_merchant';
//$route['(?i)merchant/master/info_merchant'] = LIVEVIEW . 'merchant/master/C_info_merchant'; 
$route['(?i)merchant/login'] = LIVEVIEW . 'merchant/C_login_merchant';
$route['(?i)merchant/profile/change_password_merchant'] = LIVEVIEW . 'merchant/profile/C_change_password_merchant';
$route['(?i)merchant/profile/change_old_password_merchant'] = LIVEVIEW . 'merchant/profile/C_change_old_password_merchant';
$route['(?i)merchant/profile/(:any)/(:any)'] = LIVEVIEW . 'home/C_redirect_merchant_to_landing_page_merchant';
$route['(?i)merchant/product_new'] = LIVEVIEW . 'merchant/master/C_product_new_merchant';
$route['(?i)merchant/master/product_stock_merchant'] = LIVEVIEW . 'merchant/master/C_product_stock_merchant';
$route['(?i)merchant/master/product_stock_merchant/(:any)'] = LIVEVIEW . 'merchant/master/C_product_stock_merchant';
$route['(?i)merchant/master/product_merchant'] = LIVEVIEW . 'merchant/master/C_product_merchant';
$route['(?i)merchant/master/product_non_active_merchant'] = LIVEVIEW . 'merchant/master/C_product_non_active_merchant';
$route['(?i)merchant/master/logo_banner_merchant'] = LIVEVIEW . 'merchant/master/C_merchant_logo'; //jk
$route['(?i)merchant/transaction/order'] = LIVEVIEW . 'merchant/transaction/C_order_merchant';
$route['(?i)merchant/transaction/order_list_detail/(:num)'] = LIVEVIEW . 'merchant/transaction/C_order_merchant_list_detail';
$route['(?i)merchant/transaction/merchant_delivery'] = LIVEVIEW . 'merchant/transaction/C_merchant_delivery';
$route['(?i)merchant/product_list'] = LIVEVIEW . 'merchant/master/C_product_list_merchant';
$route['(?i)merchant/transaction/redeem_merchant'] = LIVEVIEW . 'merchant/transaction/C_redeem_merchant';
$route['(?i)merchant/transaction/return_merchant'] = LIVEVIEW . 'merchant/transaction/C_order_return_merchant';
$route['(?i)merchant/transaction/return_merchant/received/(:any)'] = LIVEVIEW . 'merchant/transaction/C_order_return_received_merchant';
$route['(?i)merchant/transaction/return_merchant/ship_to_member/(:any)'] = LIVEVIEW . 'merchant/transaction/C_order_return_ship_to_member_merchant';
$route['(?i)merchant/transaction/return_merchant/change_status/(:any)'] = LIVEVIEW . 'merchant/transaction/C_order_return_change_status_merchant';
$route['(?i)merchant/transaction/notification'] = LIVEVIEW . 'merchant/transaction/C_notification_merchant';
$route['(?i)merchant-backend'] = LIVEVIEW . 'merchant/C_login_merchant';
$route['(?i)merchant/report'] = LIVEVIEW . 'merchant/transaction/C_report_merchant';
$route['(?i)merchant/message'] = LIVEVIEW . 'merchant/master/C_message_merchant';
$route['(?i)merchant/master/store_decoration'] = LIVEVIEW . 'merchant/master/C_store_decoration';
$route['(?i)merchant/master/info_merchant'] = LIVEVIEW . 'merchant/master/C_info_merchant';
$route['(?i)merchant/forgot_password'] = LIVEVIEW . 'merchant/master/C_forgot_password_merchant';
$route['(?i)logout/merchant'] = LIVEVIEW . 'merchant/C_main_page_merchant/sign_out';

/*
 * MEMBER ROUTES
 */
$route['(?i)member'] = 'member/profile/C_member';
$route['(?i)member/verifikasi/(:any)'] = 'member/profile/C_member';
$route['(?i)member/address'] = 'member/master/C_member_address';
$route['(?i)member/address/edt'] = 'member/master/C_member_address/get_edit';
$route['(?i)member/address/save_add'] = 'member/master/C_member_address/save_add';
$route['(?i)member/address/save_update'] = 'member/master/C_member_address/save_update';
$route['(?i)member/address/del'] = 'member/master/C_member_address/save_delete';
$route['(?i)member/update'] = 'member/profile/C_member/save_update';
$route['(?i)member/search'] = 'member/profile/C_member/search';
$route['(?i)member/main_page'] = 'member/C_main_page_member';
$route['(?i)member/shopping_cart'] = 'member/C_shopping_cart_member';
$route['(?i)member/login'] = 'member/C_login_member';
$route['(?i)member/login_fb'] = 'member/C_login_member_fb';
$route['(?i)member/sign_up'] = 'member/C_signup_member';
$route['(?i)member/login/sign_out'] = 'member/C_login_member/sign_out';
$route['(?i)member/login/sign_in'] = 'member/C_login_member/sign_in';
$route['(?i)member/profile/profile_member'] = 'member/profile/C_profile_member';
$route['(?i)member/profile/order_list_detail/(:any)'] = 'member/profile/C_order_list_detail';
$route['(?i)member/profile/payment_retry/(:any)'] = 'member/profile/C_payment_retry_detail';
$route['(?i)member/profile/change_password_member'] = 'member/profile/C_change_password_member';
$route['(?i)member/profile/change_old_password_member'] = 'member/profile/C_change_old_password_member';
$route['(?i)member/verifikasi_registrasi/(:any)'] = 'member/master/C_verifikasi_signup_member';
$route['(?i)member/forgot_password'] = 'member/master/C_forgot_password_member';
$route['(?i)member/verify_password/(:any)'] = 'member/master/C_verify_password';
$route['(?i)registration'] = 'member/profile/C_registration_member';
$route['(?i)member/checkout'] = 'member/C_checkout_member';
$route['(?i)member/profile/verifikasi_deposit/(:any)'] = 'member/profile/C_profile_member';
$route['(?i)member/payment/proccess/identify'] = 'member/C_payment_proccess_member/save_docu_payment_code';
$route['(?i)member/payment/proccess?(:any)'] = 'member/C_payment_proccess_member';
$route['(?i)member/payment/proccess/(:any)'] = 'member/C_payment_proccess_member';
$route['(?i)member/payment/inquiry?(:any)'] = 'member/C_payment_inquiry_member';
$route['(?i)member/payment/inquiry/(:any)'] = 'member/C_payment_inquiry_member';
$route['(?i)member/payment/proccess'] = 'member/C_payment_proccess_member';
$route['(?i)member/payment/(:any)'] = 'member/C_payment_member';
$route['(?i)member/wishlist'] = 'member/C_wishlist_member';
$route['(?i)member/product_return'] = 'member/C_order_return_member';
$route['(?i)member/print_payment'] = 'member/C_print_payment';
$route['(?i)thankyou/(:any)'] = 'member/C_payment_thankyou_member';

/*
 * PARTNER
 */
$route['(?i)partner'] = 'partner/C_login_partner';
$route['(?i)partner/sign_in'] = 'partner/C_login_partner/sign_in';
$route['(?i)partner/logout'] = 'partner/C_main_partner/sign_out';
$route['(?i)partner/forgot_password'] = 'partner/C_forgot_password_partner';
$route['(?i)partner/verify_password/(:any)'] = 'partner/C_verify_password_partner';
$route['(?i)partner/main_page'] = 'partner/C_main_partner';
$route['(?i)partner/profile'] = 'partner/profile/C_profile_partner';
$route['(?i)partner/login'] = 'partner/C_login_partner';
$route['(?i)partner/change_password'] = 'partner/profile/C_change_password_partner';
$route['(?i)partner/agent_data'] = 'partner/master/C_agent_data_partner';
$route['(?i)partner/customer_agent_data'] = 'partner/master/C_customer_agent_data_partner';
$route['(?i)partner/agent_address'] = 'partner/master/C_agent_address_partner';
$route['(?i)partner/agent_order'] = 'partner/transaction/C_agent_order_partner';
$route['(?i)partner/agent_order/download_excel'] = 'partner/transaction/C_agent_order_partner';
$route['(?i)partner/redeem_partner'] = 'partner/transaction/C_redeem_partner';
$route['(?i)partner/redeem_detail_partner'] = 'partner/transaction/C_redeem_detail_partner';
$route['(?i)partner/partner_loan'] = 'partner/loan/C_partner_loan_partner';
$route['(?i)partner/partner_down_payment'] = 'partner/loan/C_partner_down_payment_partner';
$route['(?i)partner/change_picture'] = 'partner/profile/C_change_picture_partner';
$route['(?i)partner/checkout'] = 'partner/C_checkout_partner';
$route['(?i)partner/invoicing_period'] = 'partner/transaction/C_invoicing_period_partner';
$route['(?i)partner/group_agent'] = 'partner/master/C_group_agent_partner';
/*
 * AGENT ROUTES
 */
$route['(?i)agent'] = LIVEVIEW . 'agent/profile/C_agent';
$route['(?i)agent/verifikasi/(:any)'] = LIVEVIEW . 'agent/profile/C_agent';
$route['(?i)agent/address'] = LIVEVIEW . 'agent/master/C_agent_address';
$route['(?i)agent/address/edt'] = LIVEVIEW . 'agent/master/C_agent_address/get_edit';
$route['(?i)agent/address/save_add'] = LIVEVIEW . 'agent/master/C_agent_address/save_add';
$route['(?i)agent/address/save_update'] = LIVEVIEW . 'agent/master/C_agent_address/save_update';
$route['(?i)agent/address/del'] = LIVEVIEW . 'agent/master/C_agent_address/save_delete';
$route['(?i)agent/update'] = LIVEVIEW . 'agent/profile/C_agent/save_update';
$route['(?i)agent/search'] = LIVEVIEW . 'agent/profile/C_agent/search';
$route['(?i)agent/main_page'] = LIVEVIEW . 'agent/C_main_page_agent';
$route['(?i)agent/login'] = LIVEVIEW . 'agent/C_login_agent';
$route['(?i)agent/sign_up'] = LIVEVIEW . 'agent/C_signup_agent';
$route['(?i)agent/login/sign_out'] = LIVEVIEW . 'agent/C_login_agent/sign_out';
$route['(?i)agent/login/sign_in'] = LIVEVIEW . 'agent/C_login_agent/sign_in';
$route['(?i)agent/profile/profile_agent'] = LIVEVIEW . 'agent/profile/C_profile_agent';
$route['(?i)agent/profile/order_list_detail/(:any)'] = LIVEVIEW . 'agent/profile/C_order_list_detail';
$route['(?i)agent/profile/change_password_agent'] = LIVEVIEW . 'agent/profile/C_change_password_agent';
$route['(?i)agent/forgot_password'] = LIVEVIEW . 'agent/master/C_forgot_password_agent';
$route['(?i)agent/verify_password/(:any)'] = LIVEVIEW . 'agent/master/C_verify_password';
$route['(?i)agent/product_return'] = LIVEVIEW . 'agent/C_order_return_agent';
$route['(?i)agent/checkout'] = LIVEVIEW . 'agent/C_checkout_agent';


/*
 * HOME
 */
$route['(?i)produk-terbaru((/.*)?)'] = 'home/C_product/new_product';
$route['(?i)produk/cicilan'] = 'home/C_credit_product';
$route['(?i)produk/promo((/.*)?)'] = 'home/C_product/discount';
$route['(?i)april-wow'] = 'promo/C_april_wow';
$route['(?i)april-wow/tas-wanita'] = 'promo/C_april_wow/woman_bag';
$route['(?i)april-wow/pakaian-pria'] = 'promo/C_april_wow/t_shirt';
$route['(?i)april-wow/jam-tangan'] = 'promo/C_april_wow/wristwatch';
$route['(?i)april-wow/mainan-buah-hati'] = 'promo/C_april_wow/toys';
$route['(?i)april-wow/elektronik'] = 'promo/C_april_wow/electronic';


/*
 * BERKAH RAMADHAN
 */

$route['(?i)berkah_ramadhan'] = 'promo/C_promo_ramadhan';
$route['(?i)berkah_ramadhan/(:any)'] = 'promo/C_promo_ramadhan';

//$route['(?i)promo/iese_2017'] = 'promo/C_iese_2017';
//$route['(?i)promo/iese_2017/(:any)'] = 'promo/C_iese_2017';

//$route['(?i)cari((/.*)?)'] = 'home/C_search';
// $route['(?i)cari/(.*)'] = 'home/C_search';
//$route['(?i)kategori((/.*)?)'] = 'home/C_category';
$route['error_404'] = 'home/C_error_404';
//$route['404_override'] = 'C_error_404';

$route['(?i)cart'] = 'home/C_cart';
$route['(?i)home/c_detail/product_review'] = 'home/C_detail/product_review';
$route['(?i)merchant/(:any)'] = 'home/C_merchant';
$route['(?i)merchant/(:any)/(:any)'] = 'home/C_merchant';

/*
 * SCHEDULER ROUTES
 */
$route['(?i)scheduler/track_awb'] = 'admin/scheduler/C_track_awb';
$route['(?i)scheduler/cancel_order'] = 'admin/scheduler/C_scheduler_order_admin';
$route['(?i)scheduler/rate_expedition'] = 'admin/scheduler/C_scheduler_rate_expedition_admin';
$route['(?i)scheduler/sitemap'] = 'admin/scheduler/C_scheduler_sitemap';
$route['(?i)scheduler/resend_email'] = 'admin/scheduler/C_scheduler_resend_email';
$route['(?i)scheduler/refund_order'] = 'admin/scheduler/C_scheduler_refund_order';
$route['(?i)scheduler/track_awb_non_partner'] = 'admin/scheduler/C_track_awb_non_partner';
$route['(?i)scheduler/cashback'] = 'admin/scheduler/C_scheduler_cashback';
//$route['(?i)scheduler/coba_email'] = 'admin/scheduler/C_test_email';

/*
 * DEFAULT ROUTES
 */
$route['default_controller'] = 'C_main';
$route['translate_uri_dashes'] = FALSE;
//$route['(?i)merchant/master/logo_banner_merchant'] = 'merchant/master/C_merchant_logo';
$route['(?i)info/(:any)'] = 'home/C_info';
$route['(?i)email/(:any)'] = 'home/C_email';
$route['(?i)pulsa'] = 'home/C_prepaid_voucher';


/*
 * ROUTE SEMENTARA
 */
$route['(?i)admin/login'] = 'admin/C_login_admin';
//$route['(?i)merchant/login'] = 'merchant/C_login_merchant';

/* take it to last page */
$route['(?i)(:any)-b(:num)((/.*)?)'] = 'home/C_category';
$route['(?i)kategori'] = 'home/C_category';
$route['(?i)(:any)(:num)((/.*)?)'] = 'home/C_detail';
$route['(?i)campaign/iese_super_great_sale'] = 'promo/C_iese_super_great_sale';
//$route['(?i)campaign/celebrate_your_graduation'] = 'promo/C_celebrate_your_graduation';
$route['(?i)campaign/prosperity_promo'] = 'promo/C_prosperity';



/* redirect old 1001 to new 1001 */
$route['(?i)product/detail/(:num)((/.*)?)'] = 'home/C_redirecting_old_toko1001_to_new_toko1001';

