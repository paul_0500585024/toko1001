<?php

defined('BASEPATH') OR exit('No direct script access allowed');

define("URL_TIKI_API", "http://203.77.231.130");

define("URL_JNE_API", "http://api.jne.co.id:8889");
define("SUB_URL_JNE_API", "dms/generateCnoteTraining");
define("API_USERNAME_JNE", "DMS");
define("API_KEY_JNE", "a68591589a0b0160749ee15fb5d3cbfa");
define("JNE_CUST_ID", "11041400");
define("URL_JNE_API_TRACE", "http://api.jne.co.id:8889/tracing/");
define("GET_AWB_TYPE", "AWB");
define("FUNCTION_GET_JNE_AWB_EXPEDITION", "get_jne_expedition_awb");
define("FUNCTION_GET_JNE_AWB_TRACKING", "get_jne_tracking_awb");

define("URL_PANDU_API", "http://202.152.56.230:8080/soap/deltamas/index.php");
define("API_KEY_PANDU", "988211222");
define("PANDU_ACCOUNT_NO", "1500400497");

define("URL_RAJA_KIRIM_API", "http://api.rajakirim.co.id/pda_api.php");
define("API_USER_RAJA_KIRIM", "TK1");
define("API_KEY_RAJA_KIRIM", "a394559246e982c35816a2d03a8765ea");
define("URL_RAJA_KIRIM_API_CHECK", "http://primatama.dyndns.info/api/pda_api.php");
define("FUNCTION_GET_RK_AWB_TRACKING", "pda_json_cek_status");

define("URL_RPX_API", "http://api.rpxholding.com/wsdl/rpxwsdl.php?wsdl");
define("API_USER_RPX", "demo");
define("API_PASSWORD_RPX", "demo");
define("API_ACCOUNT_RPX", "234098705"); //   800056762

define("MANDIRI_ECASH_WSDL", "wsdl/ecommgwws_dev.wsdl");
define("MANDIRI_ECASH_PAYMENT_URL", "http://mandiri-ecash.com:19443/ecommgateway/payment.html");
define("MANDIRI_ECASH_VALIDATION_URL", "http://mandiri-ecash.com:19443/ecommgateway/validation.html");
define("MANDIRI_ECASH_MERCHANT_ID", "Toko1001");
define("MANDIRI_ECASH_USERNAME", "toko1001");
define("MANDIRI_ECASH_PASSWORD", "pass123456");

define("PGS_URL", "https://dvlp.payment2go.com:443/payment/PaymentWindow.jsp");
define("PGS_URL_TRACE", "https://dvlp.payment2go.com:443/payment/PaymentInterface.jsp");
define("PGS_TXN_PASSWORD", "oyplx");
define("PGS_MERCHANT_ID", "demo_Toko1001");
define("PAYMENT_METHOD", "1");
define("PGS_CREDIT_PAYMENT_IND", "card_range_ind");
define("PGS_CREDIT_PAYMENT_CRITERIA", "card_bin_bca");
// credit bank
// BCA 3 bulan
define("BCA_PERIOD_3_SEQ", "1");
define("BCA_PERIOD_3_MID", "demo_Toko1001_03"); //000453694	,   demo_toko1001_03
define("BCA_PERIOD_3_PASSWORD", "gokqv");
// BCA 6 bulan
define("BCA_PERIOD_6_SEQ", "2");
define("BCA_PERIOD_6_MID", "demo_Toko1001_06"); //000453695	,	demo_toko1001_06
define("BCA_PERIOD_6_PASSWORD", "yifdc");
//4617007700000039
define("MANDIRI_CLICKPAY_MERCHANT_ID", "TOKO1001");
define("MANDIRI_CLICKPAY_MERCHANT_PWD", "8605O1Ao1");
define("MANDIRI_CLICKPAY_INSERTPAYMENT", "https://mitrapay.mitracomm.com/mandiri_clickpay_dev/insertpay");
define("MANDIRI_CLICKPAY_REDIRECT_URL", "https://mitrapay.mitracomm.com/mandiri_clickpay_dev/redirect");
define("MANDIRI_CLICKPAY_CHECKPAYMENT", "https://mitrapay.mitracomm.com/mandiri_clickpay_dev/checkpay");
define("MANDIRI_CLICKPAY_REVERSAL", "https://mitrapay.mitracomm.com/mandiri_clickpay_dev/reversal");

//KlikPay BCA
define("BCA_KLIKPAY_CODE", "dvlptk1001");
define("BCA_KLIKPAY_CLEARKEY", "KlikPayDevMrchnt");
define("BCA_KLIKPAY_P2GOID", "toko1001_p2go");
//define("BCA_KLIKPAY_P2GO_URL", "http://202.152.30.60/payment2go/payment/klikpay");
// BY JA, URL Changed by GSP
define("BCA_KLIKPAY_P2GO_URL", "http://202.152.30.60/payment2go/direct/klikpay/directAPI");

/*
 * CC DEVELOPMENT
 */
define("PYMT_IND_VALUE", "card_range_ind");
define("PYMT_CRITERIA_VALUE", "card_bin_indnoncorp");

/*
 * DOKU DEVELOPMENT
 */
define("URL_PAYMENT_REQUEST", 'https://staging.doku.com/Suite/Receive');
define("URL_CHECK_STATUS", 'https://staging.doku.com/Suite/CheckStatus');
define("URL_VOID", 'https://staging.doku.com/Suite/VoidRequest');

/*
 * EMAIL BASE
 */
define("SMTP_HOST", "tokosiana.com");
define("SMTP_USERNAME", "no-reply@tokosiana.com");
define("SMTP_PASSWORD", "dez3FehejA5&dr-");
define("SMTP_PORT", 465);
define("SMTP_FROM_EMAIL", "no-reply@tokosiana.com");
define("SMTP_FROM_NAME", "Tokosiana");

/*
 * EMAIL AND NAME ADMIN
 */
define("ADMIN_TOKO1001_EMAIL", "albertnagawan@gmail.com, arvin@evercoss.co.id");
define("ADMIN_TOKO1001_NAME", "admin");

define("RETUR_OFFICE", "Tim Retur Toko1001");
define("RETUR_ADDRESS", "PT. DELTAMAS MANDIRI SEJAHTERA<br>Jl. Indokarya Barat I Blok D No. 1<br>Papanggo, Sunter<br>Jakarta Utara 14330, Indonesia ");
define("RETUR_PHONE", "021-6514224/5");

/*
 * SMTEL CONFIG
 */
define("SMTEL_CHANNEL_ID", "TOKO1001");
define("SMTEL_STORE_ID", "099");
define("SMTEL_POS_ID", "1");
define("SMTEL_SERVER_SECRET_KEY", "123456");
define("SMTEL_SERVER_PIN", "1234");
define("SMTEL_URL_REQUEST", "http://183.91.68.19:7357/h2h.php");

/* FACEBOOK API
 * 
 */
define("FACEBOOK_API", "421079861618141");
define("FACEBOOK_DEV_API", "1185840741473785");

/* API GPLUS */
define("GOOGLE_API", "213656510999-5dl7l1031qljj3v4t7jvpr4ue3vj0tsl.apps.googleusercontent.com");

/* DIMO */
define('URL_DIMO_API', 'https://sandbox.dimo.co.id/');
define('DIMO_API_KEY', '11e8872fe766d7f5b08c7091618c53532fe3107b');
define('DIMO_PIN_CODE', '1234');
define('DIMO_IP_ADDRESS', json_encode(array("0" => "36.37.88.37")));

/*
 * STATIC CONTENT
 */
define('CDN_IMAGE', 'http://192.168.50.16/toko1001_id/');
define('IMAGE_BANK_PAYMENT', 'payment_70px/TMP_VIA_BANK.jpg');
define('GSP_BCA_IP_ADDRESS', json_encode(array("0" => "202.152.30.60")));


define('URL_BCA', "http://202.152.30.60/payment2go/direct/klikpay/directAPI");
define('BCA_CLEARKEY', 'ClearKeyDevToko1');
define('BCA_KLIKPAYCODE', '03DELT0521');
//define('BCA_CALLBACK', "https://tokosiana.com/thankyou/");
define('BCA_CALLBACK', "https://tokosiana.com/member/payment/proccess/");
define('BCA_PAYTYPE', "01");
define('BCA_CURRENCY', "IDR");
define('URL_BCA_REDIRECT', "http://202.152.30.60/klikpay_dummy/index/test_payment");
//define('URL_BCA_REDIRECT', "https://202.6.215.230:8081/purchasing/purchase.do?action=loginRequest");


/*
 * PUSH NOTIFICATION
 */

define('SEND_NOTIF_URL', "https://fcm.googleapis.com/fcm/send");
define('SEND_NOTIF_MEMBER_KEY', "AAAA6gg79_E:APA91bESmdt5w4mlC6siU-fDZtBWVVe5ZaUf1gcsSfhIij973RDGpBMXl-7A4fAtn8teM-nSZMAAStztu4vCkN_2mCqM58Wm4ozl6sRVOv4ddSroKRzhn9Q4bq8KgAsMUasOg16FGcuA");
define('SEND_NOTIF_AGENT_KEY', "AAAAphYezBY:APA91bHTwgCuuJBOQWa96vv7W0mKalbO3IpFHAcP9egxamd1faB6a8qnl9Paqfode8TiTomcsy_UfajpxZsHoPMqSUTJvhMqNLep_vjYLRlkvpNLp1yeaXTs82ypz_Tqu3uozIhyreNy");


