<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['cookie_prefix']	= 'tokosiana_';
$config['cookie_domain']	= '.tokosiana.com';
$config['ftp_server']           = '';
$config['ftp_user_name']	= '';
$config['ftp_password'] 	= '';
$config['cdn_image']            = 'https://tokosiana.com/';
$config['file_server']          = '/home/tokosiana/public_html/';
