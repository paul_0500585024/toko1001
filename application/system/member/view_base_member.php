<?php

require_once APPPATH . 'system/view_base.php';

/*
 * URL
 */
function get_css_url() {
    return get_base_url() . "assets/css/";
}

function get_js_url() {
    return get_base_url() . "assets/js/";
}

function get_img_url() {
    return CDN_IMAGE . "assets/img/";
}

function get_config_url() {
    return get_base_url() . "application/config/";
}

function get_content_url() {
    return VIEWPATH . "member/content/";
}

function get_component_url() {
    return VIEWPATH . "member/component/";
}

function get_include_content_member_header() {
    return get_content_url() . "header.php";
}

function get_include_content_member_top_navigation() {
    return get_content_url() . "top_nav.php";
}

function get_include_content_member_left_navigation() {
    return get_content_url() . "left_nav.php";
}

function get_include_content_member_right_navigation() {
    return get_content_url() . "right_nav.php";
}

function get_include_content_member_footer() {
    return get_content_url() . "footer.php";
}

function get_include_content_member_top_page_navigation() {
    return get_content_url() . "top_page.php";
}

function get_include_content_member_bottom_page_navigation() {
    return get_content_url() . "bottom_page.php";
}

function get_include_page_list_member_content_header() {
    return get_content_url() . "page_list_header.php";
}

function get_include_page_list_member_content_footer() {
    return get_content_url() . "page_list_footer.php";
}

?>