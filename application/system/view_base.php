<?php

require_once APPPATH . 'system/base.php';

/*
 * URL
 */

function get_current_url() {
    return htmlspecialchars(current_url());
}

function get_base_url() {
    return htmlspecialchars(base_url());
}

/*
 * TITLE
 */

function get_title_list($title) {
    return "Daftar " . $title;
}

function get_title_add($title) {
    return "Tambah " . $title;
}

function get_title_edit($title) {
    return "Ubah " . $title;
}

function get_image_location() {
    return CDN_IMAGE;
}

/*
 * SECURITY
 */

function get_csrf_admin_token() {
    if (isset($_SESSION[SESSION_ADMIN_CSRF_TOKEN])) {
        return "<input type= \"hidden\" name=\"" . CONTROL_ADMIN_CSRF_NAME . "\" value=\"" . $_SESSION[SESSION_ADMIN_CSRF_TOKEN] . "\" style=\"display:none;\"/>";
    } else {
        return "";
    };
}

function get_csrf_merchant_token() {
    if (isset($_SESSION[SESSION_MERCHANT_CSRF_TOKEN])) {
        return "<input type= \"hidden\" name=\"" . CONTROL_MERCHANT_CSRF_NAME . "\" value=\"" . $_SESSION[SESSION_MERCHANT_CSRF_TOKEN] . "\" style=\"display:none;\"/>";
    } else {
        return "";
    };
}

function get_csrf_member_token() {
    if (isset($_SESSION[SESSION_MEMBER_CSRF_TOKEN])) {
        return "<input type= \"hidden\" name=\"" . CONTROL_MEMBER_CSRF_NAME . "\" value=\"" . $_SESSION[SESSION_MEMBER_CSRF_TOKEN] . "\" style=\"display:none;\"/>";
    } else {
        return "";
    };
}

function get_csrf_agent_token() {
    if (isset($_SESSION[SESSION_AGENT_CSRF_TOKEN])) {
        return "<input type= \"hidden\" name=\"" . CONTROL_AGENT_CSRF_NAME . "\" value=\"" . $_SESSION[SESSION_AGENT_CSRF_TOKEN] . "\" style=\"display:none;\"/>";
    } else {
        return "";
    };
}

function get_csrf_partner_token() {
    if (isset($_SESSION[SESSION_PARTNER_CSRF_TOKEN])) {
        return "<input type= \"hidden\" name=\"" . CONTROL_PARTNER_CSRF_NAME . "\" value=\"" . $_SESSION[SESSION_PARTNER_CSRF_TOKEN] . "\" style=\"display:none;\"/>";
    } else {
        return "";
    };
}

function get_search_button() {
    return "<input type= \"button\" name=\"" . CONTROL_SEARCH_NAME . "\" id = \"search\" style=\"width: 100%\" value =\"Cari\" class=\"btn btn-google-plus\">";
}

function get_edit_button() {
    return "<button type= \"submit\" name=\"" . CONTROL_EDIT_NAME . "\" value=\"\" style=\"width: 100%;\" class=\"btn btn-google-plus\">Ubah</button>";
}

function get_save_add_button() {
    return "<button type= \"submit\" name=\"" . CONTROL_SAVE_ADD_NAME . "\" value=\"\" style=\"width: 100%;\" class=\"btn btn-google-plus\"><i class=\"fa fa-save\"></i>&nbsp;Simpan</button>";
}

function get_additional() {
    return "<button type= \"submit\" name=\"" . CONTROL_ADDITIONAL_NAME . "\" value=\"\" style=\"width: 100%;\" class=\"btn btn-google-plus\"><i class=\"fa fa-save\"></i>&nbsp;Simpan</button>";
}

function get_save_edit_button() {
//    return "<button type= \"submit\" name=\"" . CONTROL_SAVE_EDIT_NAME . "\" id = \"save_edit\" value=\"\" style=\"width: 100%;\" class=\"btn btn-google-plus\"><i class=\"fa fa-floppy\"></i>&nbsp;Simpan Ubah</button>";
    return "<button type= \"submit\" name=\"" . CONTROL_SAVE_EDIT_NAME . "\" id = \"save_edit\" value=\"\" style=\"width: 100%;\" class=\"btn btn-success\"><i class=\"fa fa-save\"></i>&nbsp;Simpan</button>";
}

function get_cancel_button() {
    return "<button type= \"submit\" name=\"" . CONTROL_CANCEL_NAME . "\" id = \"cancel\" onclick=\"button = this.id\" style=\"width: 100%;\" class=\"btn btn-warning\" value=\"Batal\"><i class=\"fa fa-times-circle\"></i>&nbsp;Batal</input>";
}

function get_back_button() {
    return "<button type= \"submit\" name=\"" . CONTROL_BACK_NAME . "\" id = \"back\" onclick=\"button = this.id\" style=\"width: 100%;\" class=\"btn btn-google-plus\" value=\"Kembali\"><i class=\" fa fa-arrow-circle-left\"></i>&nbsp;Kembali</input>";
}

function get_save_register_button() {
    return "<button type= \"submit\" name=\"" . CONTROL_SAVE_REGISTER_NAME . "\" value=\"\" class=\"btn btn-success btn-lg\" id=\"daftar\">Daftar</button>";
}

function get_cancel_link($url) {
    return "<a href='" . $url . "' class='btn btn-google-plus' style='width: 100%;'>Batal</a>";
}

function get_display_value($value) {
    if (isset($value) && $value != "") {
        $value = htmlspecialchars($value);
    }
    return $value;
}
function get_display_status($result, $def) {
    $data = json_decode($def, true);
    return ($data[$result]);
}

function cdef($result) {
    return htmlspecialchars($result);
}

function display_gender($gender = '', $form_name = "gender") {
    $view = '';
    $gender = $gender == '' ? DEFAULT_MALE_GENDER : $gender;
    foreach ((json_decode(STATUS_GENDER)) as $key => $each) {
        $view .= '<label>' . '<input type="radio" name="' . ($form_name != "" ? $form_name : "") .
                '" value="' . $key . '" '
                . ($key == $gender ? "checked" : "") . '>&nbsp;' . $each .
                '</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    }
    return $view;
}

?>