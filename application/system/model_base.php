<?php

require_once APPPATH . 'system/base.php';

class ModelBase extends CI_Model {

    public function __construct() {
        parent::__construct();

        $this->initialize();
    }

    public function __destruct() {
// do nothing, Cannot close connection in here.
// $this->db->close();
    }

    protected function initialize() {
//mysqli_report(MYSQLI_REPORT_ALL); // Traps all mysqli error -- TOO MANY WARNING, don't use this.
        mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

// Open Connection
        try {
            $this->load->database();
            $this->load->library('MyDB');
//
        } catch (Exception $ex) {       // } catch (mysqli_sql_exception $ex) {
//TODO log error connection and go to custom error page
//            echo "ERR initialize_database : " . var_dump($ex) . "<hr/>";
        }

        /*
          try {
          $this->load->database();
          $this->load->library('mydb');
          //
          } catch (Exception $ex) {       // } catch (mysqli_sql_exception $ex) {
          //TODO log error connection and go to custom error page
          echo "ERR initialize_database : " . var_dump($ex) . "<hr/>";
          //
          }
         *
         */

// Ensure reporting is setup correctly
// mysqli_report(MYSQLI_REPORT_ALL); // Traps all mysqli error -- TOO MANY WARNING, don't use this.
// mysqli_report(MYSQLI_REPORT_STRICT);
// mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

        /*
         * NOTES TRY CATCH:
         * - Harus ada command sebelum open connection mysqli_report(MYSQLI_REPORT_ALL);
         *   supaya error bisa di catch
         *
         * NOTES DATABASE CONNECTION:
         * - SERVERNAME TIDAK KETEMU localhost diisi sembarang, tetap ada error, walaupun sudah di catch,
         *   jadi error  "mysqli::real_connect(): php_network_getaddresses:..." di php dan
         *   di catch juga ada error, solusinya apabila error di php, masuk ke custom page error
         * - USER TIDAK KETEMU, ketangkap di catch
         * - PASSWORD SALAH, ketangkap di catch
         * - DATABASE TIDAK KETEMU, ketangkap di catch
         */

        /*
         *
          try {
          $db = new mysqli('localhost', 'roota', '', 'tk181188_dev_2');
          // Check connection
          if ($db->connect_error) {
          die("Connection failed: " . $db->connect_error);
          }
          echo "Connected successfully";
          } catch (mysqli_sql_exception $ex) {
          echo "ERR: " . $ex->getMessage();
          }
         */

        /*
         *  PDO
          try {
          $conn = new PDO("mysql:host=localhosta;dbname=tk181188_dev_2", 'root', '');
          // set the PDO error mode to exception
          $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          echo "Connected successfully";
          } catch (PDOException $e) {
          echo "Connection failed: " . $e->getMessage();
          }
         */
    }

    protected function handle_database_error($ex) {

//TODO -- log error to database: ip, datetime, err
        switch ($ex->getCode()) {
            case 1062: // DUPLICATE
                throw new BusisnessException("Data sudah ada di database !", $ex->getCode(), $ex);
                break;
            case 1451: // FOREIGN KEY
                throw new BusisnessException("Data sudah dipakai oleh data lain !", $ex->getCode(), $ex);
                break;
            case 1264: // FOREIGN KEY
                throw new BusisnessException("Nilai data melebihi nilai yang seharusnya !", $ex->getCode(), $ex);
                break;
            default:
                throw new TechnicalException("ERROR, TRY AGAIN " . $ex->getMessage() . " - " . $ex->getCode(), $ex->getCode(), $ex);
//            default:
//                throw new TechnicalException("ERROR, TRY AGAIN ");
        }
    }

    private function get_parameters($params_assoc) {
        $params = "";
        $val = "";
        $length = count($params_assoc);

        for ($idx = 0; $idx < $length; $idx++) {
            switch ($params_assoc[$idx][1]) {
                case TYPE_STRING:
                    $val = $this->db->escape($params_assoc[$idx][0]);
                    break;

                case TYPE_NUMERIC:
                    if (is_numeric($params_assoc[$idx][0])) {
                        $val = $params_assoc[$idx][0];
                    } else {
                        $val = 0;
                    }
                    break;

                case TYPE_DATE: {
                        if ($params_assoc[$idx][0] == '0000-00-00') {
                            $val = "'0000-00-00'";
                        } else {
                            $date = date_create($params_assoc[$idx][0]);
                            $val = $this->db->escape(date_format($date, "Ymd"));
                        }
                    } break;

                case TYPE_DATETIME: {
                        $date = date_create($params_assoc[$idx][0]);
                        $val = $this->db->escape(date_format($date, "Y-m-d h:m:s"));
                    } break;

                case TYPE_NEW_DATE: {
                        if ($params_assoc[$idx][0] == '0000-00-00') {
                            $val = "'0000-00-00'";
                        } else {
                            $date = date_create($params_assoc[$idx][0]);
                            $val = $this->db->escape(date_format($date, "Y-m-d"));
                        }
                    } break;


                case TYPE_BOOLEAN;
                    $val = ($params_assoc[$idx][0] == 'on' ? 1 : 0);
            }
            if ($params != "") {
                $params = $params . ",";
            }
            $params = $params . $val;
        }

        return $params;
    }

    protected function execute_sp_single_query($sp_command, $params_assoc = "") {
        try {
            $sql_command = "call " . $sp_command;

            if ($params_assoc != "") {
                $sql_command = $sql_command . "("
                        . $this->get_parameters($params_assoc)
                        . ")";
            }
            
            $query = $this->db->query($sql_command, $params_assoc);

            if ($query->num_rows() > 0) {
                $result = $query->result();
            }
            $query->next_result();
            $query->free_result();
            if (isset($result)) {
                return $result;
            } else {
                return;
            }
        } catch (Exception $ex) {
            throw new mysqli_sql_exception($sql_command, $ex->getCode(), $ex);
        }
    }

    protected function execute_sp_multi_query($sp_command, $params_assoc = "") {
        try {
            $sql_command = "call " . $sp_command;

            if ($params_assoc != "") {
                $sql_command = $sql_command . "("
                        . $this->get_parameters($params_assoc)
                        . ")";
            }

            return $this->mydb->get_multi_results($sql_command);
        } catch (Exception $ex) {
            throw new mysqli_sql_exception($sql_command, $ex->getCode(), $ex);
        }
    }

    protected function execute_non_query($sp_command, $params_assoc) {
        try {
            $sql_command = "call " . $sp_command . "("
                    . $this->get_parameters($params_assoc)
                    . ")";
            $this->db->query($sql_command);
        } catch (Exception $ex) {
            throw new mysqli_sql_exception($sql_command, $ex->getCode(), $ex);
        }
    }

    public function trans_begin() {
        $this->db->trans_begin();
    }

    public function trans_commit() {
        $this->db->trans_commit();
    }

    public function trans_rollback() {
        $this->db->trans_rollback();
    }

    public function trans_status() {
        $this->db->trans_status();
    }

}

?>
