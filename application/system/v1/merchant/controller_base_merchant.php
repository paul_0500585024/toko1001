<?php

require_once APPPATH . 'system/base.php';
require_once APPPATH . 'system/controller_base.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_controller_base
 *
 * @author Jartono
 */
class controller_base_merchant extends ControllerBase {

    public function __construct($form_cd = "", $form_url = "", $check_logged_in = true) {
        return parent::__construct($form_cd, $form_url, $check_logged_in, "merchant/login", MERCHANT_LOGIN);
    }

    protected function get_merchant_user_id() {
        if (isset($_SESSION[SESSION_MERCHANT_UID])) {
            return $this->session->userdata(SESSION_MERCHANT_UID);
        } else {
            return "";
        }
    }

    protected function get_merchant_email() {
        if (isset($_SESSION[SESSION_MERCHANT_EMAIL])) {
            return $this->session->userdata(SESSION_MERCHANT_EMAIL);
        } else {
            return "";
        }
    }

    protected function get_merchant_user_seq() {
        if (isset($_SESSION[SESSION_MERCHANT_SEQ])) {
            return $this->session->userdata(SESSION_MERCHANT_SEQ);
        } else {
            return "";
        }
    }

    protected function get_merchant_user_name() {
        if (isset($_SESSION[SESSION_MERCHANT_UNAME])) {
            return $this->session->userdata(SESSION_MERCHANT_UNAME);
        } else {
            return "";
        }
    }

    protected function create_barcode($text = '', $height = 50, $widthScale = 1) {
        if ($text != '') {
            $code39 = array('0' => 'bwbwwwbbbwbbbwbw', '1' => 'bbbwbwwwbwbwbbbw',
                '2' => 'bwbbbwwwbwbwbbbw', '3' => 'bbbwbbbwwwbwbwbw',
                '4' => 'bwbwwwbbbwbwbbbw', '5' => 'bbbwbwwwbbbwbwbw',
                '6' => 'bwbbbwwwbbbwbwbw', '7' => 'bwbwwwbwbbbwbbbw',
                '8' => 'bbbwbwwwbwbbbwbw', '9' => 'bwbbbwwwbwbbbwbw',
                'A' => 'bbbwbwbwwwbwbbbw', 'B' => 'bwbbbwbwwwbwbbbw',
                'C' => 'bbbwbbbwbwwwbwbw', 'D' => 'bwbwbbbwwwbwbbbw',
                'E' => 'bbbwbwbbbwwwbwbw', 'F' => 'bwbbbwbbbwwwbwbw',
                'G' => 'bwbwbwwwbbbwbbbw', 'H' => 'bbbwbwbwwwbbbwbw',
                'I' => 'bwbbbwbwwwbbbwbw', 'J' => 'bwbwbbbwwwbbbwbw',
                'K' => 'bbbwbwbwbwwwbbbw', 'L' => 'bwbbbwbwbwwwbbbw',
                'M' => 'bbbwbbbwbwbwwwbw', 'N' => 'bwbwbbbwbwwwbbbw',
                'O' => 'bbbwbwbbbwbwwwbw', 'P' => 'bwbbbwbbbwbwwwbw',
                'Q' => 'bwbwbwbbbwwwbbbw', 'R' => 'bbbwbwbwbbbwwwbw',
                'S' => 'bwbbbwbwbbbwwwbw', 'T' => 'bwbwbbbwbbbwwwbw',
                'U' => 'bbbwwwbwbwbwbbbw', 'V' => 'bwwwbbbwbwbwbbbw',
                'W' => 'bbbwwwbbbwbwbwbw', 'X' => 'bwwwbwbbbwbwbbbw',
                'Y' => 'bbbwwwbwbbbwbwbw', 'Z' => 'bwwwbbbwbbbwbwbw',
                '-' => 'bwwwbwbwbbbwbbbw', '.' => 'bbbwwwbwbwbbbwbw',
                ' ' => 'bwwwbbbwbwbbbwbw', '*' => 'bwwwbwbbbwbbbwbw',
                '$' => 'bwwwbwwwbwwwbwbw', '/' => 'bwwwbwwwbwbwwwbw',
                '+' => 'bwwwbwbwwwbwwwbw', '%' => 'bwbwwwbwwwbwwwbw');

            if (!preg_match('/^[A-Z0-9-. $+\/%]+$/i', $text)) {
                throw new Exception('Invalid text input.');
            }
            $text = '*' . strtoupper($text) . '*'; // *UPPERCASE TEXT*
            $length = strlen($text);
            $barcode = imageCreate($length * 16 * $widthScale, $height);
            $bg = imagecolorallocate($barcode, 255, 255, 0); //sets background to yellow
            imagecolortransparent($barcode, $bg); //makes that yellow transparent
            $black = imagecolorallocate($barcode, 0, 0, 0); //defines a color for black
            $chars = str_split($text);
            $colors = '';
            foreach ($chars as $char) {
                $colors .= $code39[$char];
            }
            foreach (str_split($colors) as $i => $color) {
                if ($color == 'b') {
// imageLine($barcode, $i, 0, $i, $height-13, $black);
                    imageFilledRectangle($barcode, $widthScale * $i, 0, $widthScale * ($i + 1) - 1, $height - 13, $black);
                }
            }

//16px per bar-set, halved, minus 6px per char, halved (5*length)
// $textcenter = $length * 5 * $widthScale;
            $textcenter = ($length * 8 * $widthScale) - ($length * 3);
            imageString($barcode, 2, $textcenter, $height - 13, $text, $black);
            ob_start();
            imagepng($barcode);
            $imagedata = ob_get_contents();
            imagedestroy($barcode);
            ob_end_clean();
            return $imagedata;
        }
    }

    protected function btn_edit($idkey) {
        return " <a href='javascript:edit(" . $idkey . ")' class='btn btn-xs btn-default-edit' title='Edit'><i class = 'fa fa-pencil'></i> Edit</a> ";
    }

    protected function btn_view($idkey) {
        return " <a href='javascript:view(" . $idkey . ")' class='btn btn-xs btn-default-view' title='Lihat'><i class = 'fa fa-eye'></i> Lihat</a> ";
    }

    protected function display_page($base_url, $start, $total_row, $perpage = '10') {
        $config['query_string_segment'] = START_OFFSET;
        $config['base_url'] = $base_url;
        $config['page_query_string'] = TRUE;
//        $config['use_page_numbers'] = TRUE;

        $config['per_page'] = $perpage;
        $config['total_rows'] = $total_row;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = "</ul>";

        $config['num_tag_open'] = '<li class="pagging-number">';
        $config['num_tag_close'] = '</li>';
//        $config['display_pages'] = FALSE;
        $config['use_page_numbers'] = FALSE;
        $config['cur_tag_open'] = "<li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";

        $config['next_tag_open'] = "<li class='next-pagging'>";
        $config['next_link'] = '<i class="fa fa-chevron-right"></i>';
        $config['next_tagl_close'] = "</li>";

        $config['prev_tag_open'] = "<li class='prev-pagging'>";
        $config['prev_link'] = '<i class="fa fa-chevron-left"></i>';
        $config['prev_tagl_close'] = "</li>";

        $config['first_tag_open'] = "<li style='display:none'>";
        $config['first_tagl_close'] = "</li>";

        $config['last_tag_open'] = "<li style='display:none'>";
        $config['last_tagl_close'] = "</li>";

        $this->pagination->initialize($config);
    }

    protected function get_breadcrumb($data) {
        $breadcrumb = "<div class='row reset-margin h-40px box-brcrum-merchant'>
                            <ul class='col-xs-12 brcrum-merchant'>
                                <li><a href='" . base_url("merchant/main_page") . "' style='font-size:20px !important;'><span class='fa fa-home'></span></a></li>
                                <li><img src='" . CDN_IMAGE . "assets/img/breadcrumb.png'></li>";

        if ($data[DATA_AUTH][FORM_AUTH][FORM_DETAIL] === "1"){
            $breadcrumb .="<li class='brcrum-merchant-active'><a href='" . base_url($data[DATA_AUTH][FORM_AUTH][FORM_PARENT_URL]) . "'>" . $data[DATA_AUTH][FORM_AUTH][FORM_PARENT_TITLE] . "</a></li>
                          <li><img src='" . CDN_IMAGE . "assets/img/breadcrumb.png'></li>
                          <li class='brcrum-merchant-active'><a href='" . $_SERVER['REQUEST_URI'] . "'>" . $data[DATA_AUTH][FORM_AUTH][FORM_TITLE] . "</a></li>
                ";
        }else{  
            $breadcrumb .="<li class='brcrum-merchant-active'><a href='" . base_url($data[DATA_AUTH][FORM_URL]) . "'>" . $data[DATA_AUTH][FORM_AUTH][FORM_TITLE] . "</a></li>";
        }
                                


        if ($data[DATA_AUTH][FORM_ACTION] !== ACTION_SEARCH) {
            $breadcrumb .="<li><img src='" . CDN_IMAGE . "assets/img/breadcrumb.png'></li>
                           <li class='brcrum-merchant-active'><a href='" . $_SERVER['REQUEST_URI'] . "'>" . $data[DATA_AUTH][FORM_ACTION_TITLE] . $data[DATA_AUTH][FORM_AUTH][FORM_TITLE] . "</a></li>";
        }

        $breadcrumb .= "</ul>
                       </div>";

        return $breadcrumb;
    }

    protected function is_status_inarray($value, $data) {
        return in_array($value, array_keys(json_decode($data, true)));
    }
    
    protected function sendnotif_member_or_agent($notif) {
        if (!empty($notif->agent_seq)) {
            $this->sendnotif_agent($notif);
        }
        if (!empty($notif->member_seq)) {
            $this->sendnotif_member($notif);
        }
    }

    protected function sendnotif_member($notif) {
        $notif->user_group_seq = '4';
        $notif->ip_address = parent::get_ip_address();
        $notif->code = $notif->member_code;
        $url_detail = base_url() . 'member/payment/' . $notif->order_no;
        $notif->order_no = "<a href='" . $url_detail . "' target='_blank'>" . $notif->order_no . "</a>";
        parent::notif_template($notif);
    }

    protected function sendnotif_agent($notif) {        
        $notif->user_group_seq = '5';
        $notif->ip_address = parent::get_ip_address();
        $notif->code = $notif->agent_code;
        $url_detail = base_url() . 'member/payment/' . $notif->order_no;
        $notif->order_no = "<a href='" . $url_detail . "' target='_blank'>" . $notif->order_no . "</a>";
        parent::notif_template($notif);
    }



}

?>