<?php

require_once APPPATH . 'system/base.php';
require_once APPPATH . 'system/controller_base.php';



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of agent_controller_base
 *
 * @author Jartono
 */
class controller_base_agent extends ControllerBase {

    public function __construct($form_cd = "", $form_url = "", $check_logged_in = true, $params = "") {
        return parent::__construct($form_cd, $form_url, $check_logged_in, "agent/login", AGENT_LOGIN, $params);
    }

    protected function get_agent_user_id() {
        if (isset($_SESSION[SESSION_AGENT_UID])) {
            return $this->session->userdata(SESSION_AGENT_UID);
        } else {
            return "";
        }
    }

    protected function get_agent_email() {
        if (isset($_SESSION[SESSION_AGENT_EMAIL])) {
            return $this->session->userdata(SESSION_AGENT_EMAIL);
        } else {
            return "";
        }
    }

    protected function get_agent_user_seq() {
        if (isset($_SESSION[SESSION_AGENT_SEQ])) {
            return $this->session->userdata(SESSION_AGENT_SEQ);
        } else {
            return "";
        }
    }
    
    protected function get_agent_name() {
        if (isset($_SESSION[SESSION_AGENT_UNAME])) {
            return $this->session->userdata(SESSION_AGENT_UNAME);
        } else {
            return "";
        }
    }

    protected function get_tree_category($with_icon = false) {
        $this->load->model('component/m_tree_view');
        $result = $this->m_tree_view->get_menu_product_category();
        if ($with_icon) {
            $result['icon']['black']['handphone-tablet'] = ICON_HANDPHONE_TABLET_BLACK;
            $result['icon']['black']['komputer'] = ICON_KOMPUTER_BLACK;
            $result['icon']['black']['elektronik-audio'] = ICON_ELEKTRONIK_AUDIO_BLACK;
            $result['icon']['black']['fashion'] = ICON_FASHION_BLACK;
            $result['icon']['black']['kesehatan-kecantikan'] = ICON_KESEHATAN_KECANTIKAN_BLACK;
            $result['icon']['black']['hobby-jam-tangan-perhiasan'] = ICON_HOBBY_JAM_TANGAN_BLACK;
            $result['icon']['black']['kamera'] = ICON_KAMERA_BLACK;
            $result['icon']['black']['peralatan-rumah-tangga'] = ICON_PERALATAN_RUMAH_TANGGA_BLACK;
            $result['icon']['black']['mainan-bayi'] = ICON_MAINAN_BAYI_BLACK;
            $result['icon']['black']['tas-koper'] = ICON_TAS_KOPER_BLACK;
            $result['icon']['black']['olahraga-musik'] = ICON_OLAHRAGA_MUSIK_BLACK;
            $result['icon']['black']['otomotif'] = ICON_OTOMOTIF_BLACK;
            $result['icon']['black']['makanan-minuman'] = ICON_MAKANAN_MINUMAN_BLACK;
            $result['icon']['black']['buku-alat-tulis'] = ICON_BUKU_ALAT_TULIS_BLACK;
            $result['icon']['black']['voucher-layanan'] = ICON_VOUCHER_LAYANAN_BLACK;
            $result['icon']['black']['perawatan-hewan-peliharaan'] = ICON_VOUCHER_PERAWATAN_HEWAN_PELIHARAAN_BLACK;
            
            $result['icon']['white']['handphone-tablet'] = ICON_HANDPHONE_TABLET_WHITE;
            $result['icon']['white']['komputer'] = ICON_KOMPUTER_WHITE;
            $result['icon']['white']['elektronik-audio'] = ICON_ELEKTRONIK_AUDIO_WHITE;
            $result['icon']['white']['fashion'] = ICON_FASHION_WHITE;
            $result['icon']['white']['kesehatan-kecantikan'] = ICON_KESEHATAN_KECANTIKAN_WHITE;
            $result['icon']['white']['hobby-jam-tangan-perhiasan'] = ICON_HOBBY_JAM_TANGAN_WHITE;
            $result['icon']['white']['kamera'] = ICON_KAMERA_WHITE;
            $result['icon']['white']['peralatan-rumah-tangga'] = ICON_PERALATAN_RUMAH_TANGGA_WHITE;
            $result['icon']['white']['mainan-bayi'] = ICON_MAINAN_BAYI_WHITE;
            $result['icon']['white']['tas-koper'] = ICON_TAS_KOPER_WHITE;
            $result['icon']['white']['olahraga-musik'] = ICON_OLAHRAGA_MUSIK_WHITE;
            $result['icon']['white']['otomotif'] = ICON_OTOMOTIF_WHITE;
            $result['icon']['white']['makanan-minuman'] = ICON_MAKANAN_MINUMAN_WHITE;
            $result['icon']['white']['buku-alat-tulis'] = ICON_BUKU_ALAT_TULIS_WHITE;
            $result['icon']['white']['voucher-layanan'] = ICON_VOUCHER_LAYANAN_WHITE;
            $result['icon']['white']['perawatan-hewan-peliharaan'] = ICON_VOUCHER_PERAWATAN_HEWAN_PELIHARAAN_WHITE;
        }
        return $result;
    }

    function startsWith($haystack, $needle) {
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
    }

    function endsWith($haystack, $needle) {

        return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
    }

    protected function display_page($base_url, $start, $total_row, $perpage = '18') {
        $config['query_string_segment'] = START_OFFSET;
        $config['base_url'] = $base_url;
        $config['page_query_string'] = TRUE;
//        $config['use_page_numbers'] = TRUE;

        $config['per_page'] = $perpage;
        $config['total_rows'] = $total_row;

        $config['full_tag_open'] = "<ul class='pagination pull-right'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";

        $this->pagination->initialize($config);
    }
    
        protected function is_agent() {
        $status = false;
/*        if (parent::get_input_get(VIEW_MODE)) {
            $status = true;
        }
        if (isset($_COOKIE[VIEW_MODE])) {
            if ($_COOKIE[VIEW_MODE] == AGENT) {
                $status = true;
            }
        }*/
        if (isset($_SESSION[SESSION_AGENT_CSRF_TOKEN])) {
            $status = true;
        }
        return $status;
    }
    
    protected function is_agent_view() {
        $status = false;
/*        if (parent::get_input_get(VIEW_MODE)) {
            $status = true;
        }*/
        if (isset($_COOKIE[VIEW_MODE])) {
            if ($_COOKIE[VIEW_MODE] == AGENT) {
                $status = true;
            }
        }
        return $status;
    }
    
    protected function sendnotif_member_or_agent($notif) {
        if (!empty($notif->agent_seq)) {
            $this->sendnotif_agent($notif);
        }
        if (!empty($notif->member_seq)) {
            $this->sendnotif_member($notif);
        }
    }

    protected function sendnotif_member($notif) {
        $notif->user_group_seq = '4';
        $notif->ip_address = parent::get_ip_address();
        $notif->code = $notif->member_code;
        $url_detail = base_url() . 'member/payment/' . $notif->order_no;
        $notif->order_no = "<a href='" . $url_detail . "' target='_blank'>" . $notif->order_no . "</a>";
        parent::notif_template($notif);
    }

    protected function sendnotif_agent($notif) {        
        $notif->user_group_seq = '5';
        $notif->ip_address = parent::get_ip_address();
        $notif->code = $notif->agent_code;
        $url_detail = base_url() . 'member/payment/' . $notif->order_no;
        $notif->order_no = "<a href='" . $url_detail . "' target='_blank'>" . $notif->order_no . "</a>";
        parent::notif_template($notif);
    }

}

?>