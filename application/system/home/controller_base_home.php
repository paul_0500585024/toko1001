<?php

require_once APPPATH . 'system/base.php';
require_once APPPATH . 'system/controller_base.php';



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_controller_base
 *
 * @author Jartono
 */
class controller_base_home extends ControllerBase {

    public function __construct($form_cd = "", $form_url = "", $check_logged_in = true, $params="") {
            return parent::__construct($form_cd, $form_url, $check_logged_in, "member/login", MEMBER_LOGIN, $params);
    }

    protected function get_member_user_id() {
        if (isset($_SESSION[SESSION_MEMBER_UID])) {
            return $this->session->userdata(SESSION_MEMBER_UID);
        } else {
            return "";
        }
    }
    
    protected function get_member_email() {
        if (isset($_SESSION[SESSION_MEMBER_EMAIL])) {
            return $this->session->userdata(SESSION_MEMBER_EMAIL);
        } else {
            return "";
        }
    }

    protected function get_member_user_seq() {
        if (isset($_SESSION[SESSION_MEMBER_SEQ])) {
            return $this->session->userdata(SESSION_MEMBER_SEQ);
        } else {
            return "";
        }
    }
    
    protected function get_member_name() {
        if (isset($_SESSION[SESSION_MEMBER_UNAME])) {
            return $this->session->userdata(SESSION_MEMBER_UNAME);
        } else {
            return "";
        }
    }
    
    protected function get_tree_category($with_icon=false) {
        $this->load->model('component/m_tree_view');
        $result = $this->m_tree_view->get_menu_product_category();
        if($with_icon){
            $result['icon']['black']['handphone-tablet'] = ICON_HANDPHONE_TABLET_BLACK;
            $result['icon']['black']['komputer'] = ICON_KOMPUTER_BLACK;
            $result['icon']['black']['elektronik-audio'] = ICON_ELEKTRONIK_AUDIO_BLACK;
            $result['icon']['black']['fashion'] = ICON_FASHION_BLACK;
            $result['icon']['black']['kesehatan-kecantikan'] = ICON_KESEHATAN_KECANTIKAN_BLACK;
            $result['icon']['black']['hobby-jam-tangan-perhiasan'] = ICON_HOBBY_JAM_TANGAN_BLACK;
            $result['icon']['black']['kamera'] = ICON_KAMERA_BLACK;
            $result['icon']['black']['peralatan-rumah-tangga'] = ICON_PERALATAN_RUMAH_TANGGA_BLACK;
            $result['icon']['black']['mainan-bayi'] = ICON_MAINAN_BAYI_BLACK;
            $result['icon']['black']['tas-koper'] = ICON_TAS_KOPER_BLACK;
            $result['icon']['black']['olahraga-musik'] = ICON_OLAHRAGA_MUSIK_BLACK;
            $result['icon']['black']['otomotif'] = ICON_OTOMOTIF_BLACK;
            $result['icon']['black']['makanan-minuman'] = MAKANAN_MINUMAN_BLACK;
            $result['icon']['black']['buku-alat-tulis'] = BUKU_ALAT_TULIS_BLACK;
            $result['icon']['white']['handphone-tablet'] = ICON_HANDPHONE_TABLET_WHITE;
            $result['icon']['white']['komputer'] = ICON_KOMPUTER_WHITE;
            $result['icon']['white']['elektronik-audio'] = ICON_ELEKTRONIK_AUDIO_WHITE;
            $result['icon']['white']['fashion'] = ICON_FASHION_WHITE;
            $result['icon']['white']['kesehatan-kecantikan'] = ICON_KESEHATAN_KECANTIKAN_WHITE;
            $result['icon']['white']['hobby-jam-tangan-perhiasan'] = ICON_HOBBY_JAM_TANGAN_WHITE;
            $result['icon']['white']['kamera'] = ICON_KAMERA_WHITE;
            $result['icon']['white']['peralatan-rumah-tangga'] = ICON_PERALATAN_RUMAH_TANGGA_WHITE;
            $result['icon']['white']['mainan-bayi'] = ICON_MAINAN_BAYI_WHITE;
            $result['icon']['white']['tas-koper'] = ICON_TAS_KOPER_WHITE;
            $result['icon']['white']['olahraga-musik'] = ICON_OLAHRAGA_MUSIK_WHITE;
            $result['icon']['white']['otomotif'] = ICON_OTOMOTIF_WHITE;
            $result['icon']['white']['makanan-minuman'] = MAKANAN_MINUMAN_WHITE;
            $result['icon']['white']['buku-alat-tulis'] = BUKU_ALAT_TULIS_WHITE;
        }
        return $result;
    }    
    
    protected function display_page($base_url,$start,$total_row,$perpage='18'){
        $config['query_string_segment'] = START_OFFSET;
        $config['base_url'] = $base_url;
        $config['page_query_string'] = TRUE;
//        $config['use_page_numbers'] = TRUE;

        $config['per_page'] = $perpage; 
        $config['total_rows'] = $total_row; 

        $config['full_tag_open'] = "<ul class='pagination pull-right'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";        

        $this->pagination->initialize($config);    
    }
    
    protected function ajax_display_page($base_url,$start,$total_row,$perpage='18'){
        $config['query_string_segment'] = 'start';
        $config['base_url'] = $base_url;
        $config['page_query_string'] = TRUE;
//        $config['use_page_numbers'] = TRUE;

        $config['per_page'] = $perpage; 
        $config['total_rows'] = $total_row; 

        $config['full_tag_open'] = "<ul class='pagination pull-right' id='pagination_review'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";        

        $this->pagination->initialize($config);    
    }    

    protected function get_category_seq_self_child(&$categories, $datas, $parent = 0, $p_level = 0, $get_self_seq =TRUE) {
        $parent_level = "";
        if($get_self_seq){
            $categories[] = $parent;
        }
        if (isset($datas[$parent])) {
            foreach ($datas[$parent] as $vals) {
                if ($vals->level == 1) {
                    $parent_level = $vals->seq;
                    $p_level = $vals->seq;
                }
                $categories[] = $vals->seq;
                $this->get_category_seq_self_child($categories, $datas, $vals->seq, $parent_level,FALSE);
            }
//            return $seq;
        }
    }
    
    protected function get_category_seq_self_parent(&$categories, $datas, $seq, $parent=0, $p_level=0, $get_self_seq =TRUE){
        if($get_self_seq){
            $categories[] = $seq;
        }
        foreach($datas as $each_datas){
            foreach($each_datas as $each_datas_detail){
                if(isset($each_datas_detail->seq)){
                    if($each_datas_detail->seq == $seq){
                        if($each_datas_detail->parent_seq != 0)
                            $categories[] = $each_datas_detail->parent_seq;
                            $this->get_category_seq_self_parent($categories, $datas, $each_datas_detail->parent_seq, 0,0,FALSE);
                        return false;
                    }
                }
            }
        }
    }

    
    protected function _get_category_seq_self_parent_with_name(&$categories, $datas, $seq, $parent=0, $p_level=0){
        foreach($datas as $each_datas){
            foreach($each_datas as $each_datas_detail){
                if(isset($each_datas_detail->seq)){
                    if($each_datas_detail->seq == $seq){
                        if($each_datas_detail->parent_seq != 0){
                            $categories[$each_datas_detail->seq] = $each_datas_detail->name;
                            $this->get_category_seq_self_parent_with_name($categories, $datas, $each_datas_detail->parent_seq, 0,0);                        
                        }else{
                            $categories[$each_datas_detail->seq] = $each_datas_detail->name;
                        }
                        return false;
                    }
                }
            }
        }
    }
    protected function get_category_seq_self_parent_with_name(&$categories, $datas, $seq, $parent=0, $p_level=0,$get_self_seq =TRUE){
        if($get_self_seq){
            $this->_get_category_seq_self_parent_with_name($categories, $datas, $seq, $parent,$p_level);
        }else{
            foreach($datas as $each_datas){
                foreach($each_datas as $each_datas_detail){
                    if(isset($each_datas_detail->seq)){
                        if($each_datas_detail->seq == $seq){
                            if($each_datas_detail->parent_seq != 0){
                                $this->_get_category_seq_self_parent_with_name($categories, $datas, $each_datas_detail->parent_seq, $parent,$p_level);
                            }
                        }
                    }
                }
            }
        }
    }
    
    protected function split_filter_category($attribute_seq_attribute_value='',$exploder_parameter=',',$exploder_parent_child = '-'){
        $value = array();
        $attribute_seq_attribute_value_array = explode($exploder_parameter,$attribute_seq_attribute_value);
        foreach($attribute_seq_attribute_value_array as $each_attribute_seq_attribute_value){
            $split_attribute_value_array = explode($exploder_parent_child,$each_attribute_seq_attribute_value);
            if($split_attribute_value_array[1] != '0'){
                $value[] = $split_attribute_value_array[1];
            }
        }
        return (count($value)>0)?("'" . implode("','",$value) . "'"):'';
    }
        
    protected function current_url_with_query_string($removed_parameter=array(),$added_parameter = array(),$rawurlencode=true){
        $ret_url = current_url();
        $result_query_string = '';
        $counter = 0;
        parse_str($_SERVER['QUERY_STRING'],$query_string);
        foreach($query_string as $key=>$each_query_string){
            if(!in_array($key, $removed_parameter)){
                if($counter > 0){
                    $result_query_string .= '&';
                }
                if($rawurlencode == true){
                    $result_query_string .= $key.'='.rawurlencode($each_query_string);
                }else{
                    $result_query_string .= $key.'='.$each_query_string;
                }                
                $counter++;
            }
        }
        $ret_url .= ($counter>0)?('?'.$result_query_string):$result_query_string;
        foreach($added_parameter as $parameter_name=>$parameter_value){
            if($counter>0 != ''){
                $ret_url .= '&';
            }else{
                $ret_url .= '?';
            }
            if($rawurlencode == true){
                $ret_url .= $parameter_name.'='.rawurlencode($parameter_value);
            }else{
                $ret_url .= $parameter_name.'='.$parameter_value;
            }            
        }
        return $ret_url;        
    }
    protected function generate_combinations(array $data, array &$all = array(), array $group = array(), $value = null, $i = 0)
    {
        $keys = array_keys($data);
        if (isset($value) === true) {
            array_push($group, $value);
        }

        if ($i >= count($data)) {
            array_push($all, $group);
        } else {
            $currentKey     = $keys[$i];
            $currentElement = $data[$currentKey];
            foreach ($currentElement as $val) {
                $this->generate_combinations($data, $all, $group, $val, $i + 1);
            }
        }

        return $all;
    }
    protected function add_remove_attribute_in_tags($search = array(), $replace = array(), $subject=''){
        if(count($search) != count($replace)) return $subject;
        $total_replace = count($search);
        
        for($i=0;$i<$total_replace;$i++){
            $subject = str_replace($search[$i], $replace[$i], $subject);
        }
        
        return $subject;
    }
    protected function get_file_exists($image_file = "",$no_image_file='no_image.png') {
        $ret_url_image = '';
        if (is_dir($image_file)) {
            $ret_url_image = base_url() . ASSET_IMG_HOME . "/" . $no_image_file;
            return $ret_url_image;
        }
        if (file_exists($image_file)) {
            $ret_url_image = base_url() . $image_file;
        } else {
            $ret_url_image = base_url() . ASSET_IMG_HOME . "/" . $no_image_file;
        }
        return $ret_url_image;
    }
    protected function get_variant_value($variant_seq, $variant_value, $separator = "") {
        $retval = "";
        switch ($variant_seq) {
            case "1": //all product
                $retval = "";
                break;
            default:
                $retval = $separator . $variant_value;
        }
        return $retval;
    }
    protected function get_data_product_category_seq($category_tree,$product_category_seq = ''){
        $data_product_category_seq = array();
        foreach($category_tree as $each_category_tree){
            foreach($each_category_tree as $each_data_category_tree){
                if(isset($each_data_category_tree->seq)){
                    if($each_data_category_tree->seq == $product_category_seq){
                        $data_product_category_seq = $each_data_category_tree;
                        break 2;
                    }
                }
            }
        }
        return $data_product_category_seq;
    }
    protected function get_data_product_category_seq_array($category_tree,$product_category_seq = array()){
        $data_product_category_seq = array();
        foreach($category_tree as $each_category_tree){
            foreach($each_category_tree as $each_data_category_tree){
                if(isset($each_data_category_tree->seq)){
                    if(in_array($each_data_category_tree->seq,$product_category_seq)){
                        $data_product_category_seq[$each_data_category_tree->seq] = $each_data_category_tree;
                    }
                }
            }
        }
        return $data_product_category_seq;
    }
    
    
    protected function create_title($name = '', $with_slogan = true ,$separator = '|'){
        if($with_slogan){
            $title = $name.' '.$separator.' '.DEFAULT_WEBSITE;
        }else{
            $title = $name;
        }
        return $title;
    }
    
    protected function sort_level_category($group_of_category,$order = 'ASC'){
        $new_group_of_category_index = array();
        $new_group_of_category_index_level = array();
        
        foreach($group_of_category as $key=>$each_group_of_category){
            $new_group_of_category_index_level[$each_group_of_category->level] = $each_group_of_category;
        }
        if($order == 'ASC'){
            ksort($new_group_of_category_index_level);
        }elseif($orde == 'DESC'){
            krsort($new_group_of_category_index_level);
        }else{
            exit('Unknown order');
        }
        foreach($new_group_of_category_index_level as $each_new_group_of_category_index_level){
            $new_group_of_category_index[$each_new_group_of_category_index_level->seq] = $each_new_group_of_category_index_level;
        }
        return $new_group_of_category_index;
    }
    
    protected function get_percentage_discount($product_price = 0, $sell_price = 0) {
        return ($product_price != 0) ? (sprintf("%d%%", ceil(($product_price - $sell_price) / ($product_price) * 100))) : '0%';
    }
    
}
?>