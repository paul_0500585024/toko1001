<?php

require_once APPPATH . 'system/view_base.php';

//function get_current_url() {
//    return htmlspecialchars(current_url());
//}
//
//function get_base_url() {
//    return htmlspecialchars(base_url());
//}
/*
 * URL
 */

function get_css_url() {
    return get_base_url() . "assets/css/home/";
}

function get_js_url() {
    return get_base_url() . "assets/js/home/";
}

function get_img_url() {
    return get_base_url() . "assets/img/home/";
}

function get_template_url() {
    return VIEWPATH . "home/template/";
}

function get_content_url() {
    return VIEWPATH . "home/content/";
}

function get_component_url() {
    return VIEWPATH . "home/component/";
}

function get_include_template_home_head() {
    return get_template_url() . "segment_main/html_head.php";
}

function get_include_template_home_script() {
    return get_template_url() . "segment_main/html_script.php";
}

function get_include_template_home_top_navigation() {
    return get_template_url() . "top_nav.php";
}

function get_include_template_home_left_navigation() {
    return get_template_url() . "left_nav.php";
}

function get_include_template_home_right_navigation() {
    return get_template_url() . "right_nav.php";
}

function get_include_template_home_footer() {
    return get_template_url() . "footer.php";
}

function get_include_template_home_top_page_navigation() {
    return get_template_url() . "top_page.php";
}

function get_include_template_home_bottom_page_navigation() {
    return get_template_url() . "bottom_page.php";
}

function get_include_page_list_home_content_footer() {
    return get_content_url() . "page_list_footer.php";
}

function get_include_page_list_home_template_header() {
    return get_template_url() . "page_list_header.php";
}

function get_include_page_list_home_template_footer() {
    return get_template_url() . "page_list_footer.php";
}

function get_cannot_view_home_template() {
    return "<button class = \"btn btn-flat bg-orange \" id = \"refresh\" data-toggle = \"tooltip\" title = \"Cari\" type='button'><i class = \"fa fa-refresh\"></i></button>";
}

function cnum($result, $decimal = 0) {
    return number_format($result, $decimal);
}

function get_percentage_discount($product_price = 0, $sell_price = 0) {
    return ($product_price != 0) ? (sprintf("%d%%", ceil(($product_price - $sell_price) / ($product_price) * 100))) : '0%';
}

function get_variant_value($variant_seq, $variant_value, $separator = "") {
    $retval = "";
    switch ($variant_seq) {
	case "1": //all product
	    $retval = "";
	    break;
	default:
	    $retval = $separator . $variant_value;
    }
    return $retval;
}

function get_file_exists($image_file = "", $no_image_file = 'no_image.png') {
    $ret_url_image = '';
    $image_file = str_replace(CDN_IMAGE, "",$image_file);
    if (is_dir($image_file)) {
	$ret_url_image = get_image_location() . ASSET_IMG_HOME . "/" . $no_image_file;
	return $ret_url_image;
    }
    if (file_exists($image_file)) {
	$ret_url_image = get_image_location() . $image_file;
    } else {
	$ret_url_image = get_image_location() . ASSET_IMG_HOME . "/" . $no_image_file;
    }
    return $ret_url_image;
}

function is_get_file_exists($image_file = "") {
    $ret_url_image = '';
    $image_file = str_replace(CDN_IMAGE, "",$image_file);
    if (is_dir($image_file)) {
	return false;
    }
    if (file_exists($image_file)) {
	return true;
    } else {
	return false;
    }
    return false;
}

//start function to create category menu 3 level
function get_menu_3level_xs($datas, $parent = 0, $extra_row = '1', $color_icon = 'white') {
    $html = '';
    $counter = 1;
    if ($extra_row == '1') {
	$html .= '<a id="promo_xs" onclick="window.location = \'' . base_url() . 'produk/promo' . '\'" href="#tree_0" class="list-group-item list-roup-item-success"  data-toggle="collapse" data-parent="#menu_0"  >';
	$html .= '<img style="width:20px;" src="' . get_image_location() . 'assets/img/home/discount_256x256.png">';
	$html .= '<span>&nbsp; Promo </span>';
	$html .= '</a>';
	$extra_row = '0';
    }
  if (isset($datas[$parent])) {
        foreach ($datas[$parent] as $each_datas) {
            $seq = $each_datas->seq;
            $parent_seq = $each_datas->parent_seq;
            $lvl = $each_datas->level;
            if ($parent == 0) {
                $html .= '<a href="#tree_' . $seq . '" class="list-group-item list-group-item-success"  data-toggle="collapse" data-parent="#menu_' . $seq . '">';
                $html .= '<i class="' . $datas['icon'][$color_icon][url_title(strtolower($each_datas->name))] . ' text-white" ></i>';
                $html .= '&nbsp; ' . $each_datas->name;
                $html .= '</a>';
            } else {
                $html .= '<a href="#tree_' . $seq . '" class=""  data-toggle="collapse" data-parent="#menu_' . $seq . '">';
                $html .= '<span onclick="window.location = \'' . base_url() . url_title(strtolower($each_datas->name)) . "-" . CATEGORY . ($each_datas->seq) . '\'">';
                $html .= '&nbsp; ' . $each_datas->name;
                $html .= '</span>';
                if ($lvl < 3) {
                    $html .= '<i class="fa fa-caret-down pull-right"></i>';
                }
                $html .= '</a>';
            }
            $html .= '<div class="collapse" id="tree_' . $seq . '">';
            if ($parent == 0) {
                $html .= '<a href="' . base_url() . url_title(strtolower($each_datas->name)) . "-" . CATEGORY . ($each_datas->seq) . '" class="list-group-item" data-parent="#tree_' . $seq . '">Semua ' . $each_datas->name . '</a>';
            }
            $html .= get_menu_3level_xs($datas, $seq, $extra_row);
            if ($parent == 0) {
                
            }
            $html .= '</div>';
//            get_menu_3level_xs($datas)

            if ($parent == 0) {
                $counter++;
            }
        }
    }
    return $html;
}

//echo htmlentities (get_menu_3level_xs($tree_category));exit();

function get_menu_3level($datas, $parent = 0, $color_icon = 'black') {
    $html = "<ul>";
    $counter = 1;
    if (isset($datas[$parent])) {
	foreach ($datas[$parent] as $each_datas) {
	    if (count($each_datas) > 0) {
		if ($counter % 12 == 0) {
		    $warna = $counter;
		} else {
		    $warna = $counter % 12;
		}
                $html .= '<li class="li-' . url_title(strtolower($each_datas->name)) . '">';
                $html .= "<a href='" . base_url() . url_title(strtolower($each_datas->name)) . "-" . CATEGORY . ($each_datas->seq) . "'><i class='" . $datas['icon'][$color_icon][url_title(strtolower($each_datas->name))] . "'></i>&nbsp;&nbsp;&nbsp;" . $each_datas->name . "</a>";
                $html .= '<ul class="submenu">';
                $html .= '<li class="big_li">';
                $html .= '<div class="box box-' . url_title(strtolower($each_datas->name)) . ' no-shadow submenu fixed-template">';
                $html .= '<div class="box-header">';
                $html .= '<a href="' . base_url() . url_title(strtolower($each_datas->name)) . "-" . CATEGORY . ($each_datas->seq) . '">';
                $html .= '<h3 class="box-title">Semua ' . $each_datas->name . '</h3>';
                $html .= '</a>';
                $html .= '</div>';
                $html .= '<div class="box-body no-padding">';
                $html .= '<table class="table-menu">';
                $html .= '<tbody>';
                $html .= '<tr>';
                $html .= set_html_for_child_level2n3_of_menu_3level($each_datas, $datas);
                $html .= '</tr>';
                $html .= '</tbody>';
                $html .= '</table>';
                $html .= '</div>';
                $html .= '</div>';
                $html .= '</li>';
                $html .= '</ul>';
                $html .= '</li>';
                $counter++;
	    }
	}
    }
    $html .= "</ul>";
    echo $html;
}

function get_discount() {
    $html = "";
    $html .= "<li>";
    $html .= "<a href='" . base_url() . "produk/promo'>Promo</a>";
    $html .= "</li>";
    return $html;
}

function get_promo_iese() {
    $html = "";
    $html .= "<li>";
    $html .= "<a href='" . base_url() . "campaign/iese_super_great_sale'>IESE</a>";
    $html .= "</li>";
    return $html;
}

function get_menu_level1_top_n($datas, $parent = 0, $color_icon = 'black', $n = 4) {
    $html = "";
    $counter = 0;
    if (isset($datas[$parent])) {
	foreach ($datas[$parent] as $each_datas) {
	    if ($counter < $n) {
		$html .= "<li>";
		$html .= "<a href='" . base_url() . url_title(strtolower($each_datas->name)) . "-" . CATEGORY . ($each_datas->seq) . "'>" . $each_datas->name . "</a>";
		$html .= "</li>";
	    }
	    $counter++;
	}
    }
    return $html;
}

function set_html_for_child_level2n3_of_menu_3level($each_datas, $datas) {
    $html = "";
    $parent1 = $each_datas->seq;
    if (isset($datas[$parent1])) {

	$num_of_column = NUMBER_OF_CATEGORY_COLUMN;
	$num_of_category = count($datas[$parent1]);
	$array_td_break = get_each_num_category_per_column($num_of_category, $num_of_column);

	$data_child2_3 = array();
	$number_child2_3 = 0;
	foreach ($datas[$parent1] as $each_datas1) {
	    $data_child2_3[$number_child2_3] = "<a href='" . base_url() . url_title(strtolower($each_datas1->name)) . "-" . CATEGORY . ($each_datas1->seq) . "'><strong>" . $each_datas1->name . "</strong></a>";
	    $parent2 = $each_datas1->seq;
	    if (isset($datas[$parent2])) {
		foreach ($datas[$parent2] as $each_datas2) {
		    $data_child2_3[$number_child2_3] .= "<a href='" . base_url() . url_title(strtolower($each_datas2->name)) . "-" . CATEGORY . ($each_datas2->seq) . "'>" . $each_datas2->name . "</a>";
		}
	    }
	    $number_child2_3++;
	}

	$cur_pointer = 0;
	for ($i = 0; $i < $num_of_column; $i++) {
	    $send_cat = array();
	    for ($j = 0; $j < $array_td_break[$i]; $j++) {
		array_push($send_cat, $data_child2_3[$cur_pointer]);
		$cur_pointer++;
	    }
	    $html .= generate_td_in_each_category($send_cat);
	}
    }
    return $html;
}

function generate_td_in_each_category($data) {
    $retval = "<td>";
    foreach ($data as $each_data) {
	$retval .= $each_data . "<br>";
    }
    $retval .= "</td>";
    return $retval;
}

function get_each_num_category_per_column($num_of_category, $num_of_coloumn = 4) {

    for ($i = 0; $i < $num_of_coloumn; $i++) {
	$num_data[$i] = 0;
    }

    for ($i = 0; $i < $num_of_category; $i++) {
	$array_num = $i % $num_of_coloumn;
	$num_data[$array_num] += 1;
    }
    return $num_data;
}

function old_get_menu($datas, $parent = 0, $p_level = 0) {
    static $i = 1;
    $parent_level = "";
    $tab = str_repeat(" ", $i);
    if (isset($datas[$parent])) {
	$html = "<ul>";
	$i++;
	foreach ($datas[$parent] as $vals) {
	    if ($vals->level == 1) {
		$parent_level = $vals->seq;
		$p_level = $vals->seq;
	    }
	    $child = old_get_menu($datas, $vals->seq, $parent_level);
	    $html .= "$tab";
	    $html.='<li><a href="javascript:pilihmenu(\'' . $vals->seq . '~' . $vals->level . '~' . $vals->name . '~' . $p_level . '\')">' . $vals->name . '</a>';
	    if ($child) {
		$i++;
		$html .= $child;
		$html .= "$tab";
	    }
	    $html .= '</li>';
	}
	$html .= "$tab</ul>";
	return $html;
    } else {
	return false;
    }
}

//end function to create category menu 3 level


function get_color($color_num) {
    $color_value = 'undefined';
    switch ($color_num) {
	case 1 :
	    $color_value = '#85CCBA';
	    break;
	case 2 :
	    $color_value = '#9F6BB3';
	    break;
	case 3 :
	    $color_value = '#F38679';
	    break;
	case 4 :
	    $color_value = '#03B2E7';
	    break;
	case 5 :
	    $color_value = '#E50052';
	    break;
	case 6 :
	    $color_value = '#005E80';
	    break;
	case 7 :
	    $color_value = '#2AA5AD';
	    break;
	case 8 :
	    $color_value = '#F4A610';
	    break;
	case 9 :
	    $color_value = '#A60006';
	    break;
	case 10 :
	    $color_value = '#93BF50';
	    break;
	case 11 :
	    $color_value = '#F1763B';
	    break;
	case 12 :
	    $color_value = '#6EC8D0';
	    break;
    }
    return $color_value;
}

function display_product_home($product = array()) {
    $total_product = count($product);
    $html = '';
    if ($total_product > 0) {
	foreach ($product as $key => $each_product) {
	    if ($key % 6 == 0) {
		$html .='<div class="container ">';
		$html .= '<div style="margin-top:0px;" >';
	    }
	    $html .= '<div class="col-md-3-display col-sm-3-display col-lg-3-display col-xs-6" id="display-' . $key . '">';
	    $html .= '<div class="panel panel-default panel-produk-baru height_fix_product prodDiv">';
	    if ($each_product->stock == '0') {
		$html .= '<div class="sold-out-produk-baru clearfix">';
		$html .= '<span class="sold-out-box"><p>SOLD OUT</p></span>';
		$html .= '</div>';
	    } else {
		if (!(trim($each_product->product_price) == '' OR trim($each_product->product_price) == trim($each_product->sell_price))) {
		    $html .= '<div class="diskon-produk-baru clearfix">';
		    $html .= '<span class="disc-box">';
		    $html .= get_percentage_discount($each_product->product_price, $each_product->sell_price);
		    $html .= '</span>';
		    $html .= '</div>';
		}
	    }
	    $html .= '<div class="panel-body" itemscope itemtype="http://schema.org/Product">';
	    $html .= '<div class="hover01">';
	    $html .= '<a href="' . base_url() . strtolower(url_title($each_product->name . get_variant_value($each_product->variant_seq, $each_product->variant_value, " "))) . '-' . $each_product->product_variant_seq . '">';
	    $html .= '<img class="img-terbaru img-responsive center-block full-img load_img"';
	    $html .= ' data-src="' . get_file_exists(PRODUCT_UPLOAD_IMAGE . $each_product->merchant_seq . '/' . S_IMAGE_UPLOAD . $each_product->image) . '"';
	    $html .= ' data-src-retina="' . get_file_exists(PRODUCT_UPLOAD_IMAGE . $each_product->merchant_seq . "/" . S_IMAGE_UPLOAD . $each_product->image) . '"';
	    $html .= ' src=' . get_image_location() . ASSET_IMG_HOME . 'blank.png';
	    $html .= '>';
	    $html .= '<div class="title-produk-box line-clamp">';
	    $html .= '<p class="title-produk"><b itemprop="name">' . $each_product->name . get_variant_value($each_product->variant_seq, $each_product->variant_value, "-") . '</b></p>';
	    $html .= '</div>';
	    $html .= '</a>';
	    if (trim($each_product->product_price) == '' OR trim($each_product->product_price) == trim($each_product->sell_price)) {
		$html .= '<p class="no-old-price">&nbsp;</p>';
	    } else {
		$html .= '<p class="old-price">' . RP . cnum($each_product->product_price) . '</p>';
	    }
	    $html .= '<p class="new-price">' . RP . cnum($each_product->sell_price) . '</p>';
	    $html .= '</div>';
	    $html .= '</div>';
	    $html .= '</div>';
	    $html .= '</div>';
//            if ($key % 6 == 1 || $key % 6 == 3) {
//                $html .= '<div class="clearfix visible-xs-block"></div>';
//            }
//            if ($key % 6 == 2) {
//                $html .= '<div class="clearfix visible-sm-block"></div>';
//            }
	    if ($key % 6 == 5 || $key == $total_product - 1) {
		$html .= '</div>';
		$html .='</div>';
	    }
	}
    }
    return $html;
}

function display_product($product = array()) {
    $total_product = count($product);
    $html = '';
    if ($total_product > 0) {
	$html .='<div class="container ">';
	foreach ($product as $key => $each_product) {

	    $html .= '<div style="margin-top:0px;" >';

	    $html .= '<div class="col-md-3-display col-sm-3 col-lg-3-sm col-xs-6" >';
	    $html .= '<div class="panel panel-default panel-produk-baru height_fix_product prodDiv">';
	    if ($each_product->stock == '0') {
		$html .= '<div class="sold-out-produk-baru clearfix">';
		$html .= '<span class="sold-out-box"><p>SOLD OUT</p></span>';
		$html .= '</div>';
	    } else {
		if (!(trim($each_product->product_price) == '' OR trim($each_product->product_price) == trim($each_product->sell_price))) {
		    $html .= '<div class="diskon-produk-baru clearfix">';
		    $html .= '<span class="disc-box">';
		    $html .= get_percentage_discount($each_product->product_price, $each_product->sell_price);
		    $html .= '</span>';
		    $html .= '</div>';
		}
	    }
	    $html .= '<div class="panel-body">';
	    $html .= '<div class="hover01">';
	    $html .= '<a href="' . base_url() . strtolower(url_title($each_product->name . get_variant_value($each_product->variant_seq, $each_product->variant_value, " "))) . '-' . $each_product->product_variant_seq . '">';
	    $html .= '<img class="img-terbaru img-responsive center-block full-img load_img"';
	    $html .= ' data-src="' . get_file_exists(PRODUCT_UPLOAD_IMAGE . $each_product->merchant_seq . '/' . S_IMAGE_UPLOAD . $each_product->image) . '"';
	    $html .= ' data-src-retina="' . get_file_exists(PRODUCT_UPLOAD_IMAGE . $each_product->merchant_seq . "/" . S_IMAGE_UPLOAD . $each_product->image) . '"';
	    $html .= ' src=' . get_image_location() . ASSET_IMG_HOME . 'blank.png';
	    $html .= '>';
	    $html .= '<div class="title-produk-box line-clamp">';
	    $html .= '<p class="title-produk"><b>' . $each_product->name . get_variant_value($each_product->variant_seq, $each_product->variant_value, "-") . '</b></p>';
	    $html .= '</div>';
	    $html .= '</a>';
	    if (trim($each_product->product_price) == '' OR trim($each_product->product_price) == trim($each_product->sell_price)) {
		$html .= '<p class="no-old-price">&nbsp;</p>';
	    } else {
		$html .= '<p class="old-price">' . RP . cnum($each_product->product_price) . '</p>';
	    }
	    $html .= '<p class="new-price">' . RP . cnum($each_product->sell_price) . '</p>';
	    $html .= '</div>';
	    $html .= '</div>';
	    $html .= '</div>';
	    $html .= '</div>';

//            if ($key % 6 == 1 || $key % 6 == 3) {
//                $html .= '<div class="clearfix visible-xs-block"></div>';
//            }
//            if ($key % 6 == 2) {
//                $html .= '<div class="clearfix visible-sm-block"></div>';
//            }
//            if ($key % 5 == 4 || $key == $total_product - 1) {
	    $html .= '</div>';

//            }
	}
	$html .='</div>';
    }
    return $html;
}

function display_product_slide($product = array()) {
    $total_product = count($product);
    $html = '';
    if ($total_product > 0) {
	$counter = 0;
	foreach ($product as $key => $each_product) {
	    $counter++;
	    $html .= '<li><div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">';
	    $html .= '<div class="panel panel-default panel-produk-baru height_fix_product prodDiv thumbnail" style="min-height:250px; margin-bottom:0px;" >';
	    if ($each_product->stock == '0') {
		$html .= '<div class="sold-out-produk-baru clearfix">';
		$html .= '<span class="sold-out-box"><p>SOLD OUT</p></span>';
		$html .= '</div>';
	    } else {
		if (!(trim($each_product->product_price) == '' OR trim($each_product->product_price) == trim($each_product->sell_price))) {
		    $html .= '<div class="diskon-produk-baru clearfix">';
		    $html .= '<span class="disc-box">';
		    $html .= get_percentage_discount($each_product->product_price, $each_product->sell_price);
		    $html .= '</span>';
		    $html .= '</div>';
		}
	    }
	    $html .= '<div class="panel-body">';
	    $html .= '<div class="hover01">';
	    $html .= '<a href="' . base_url() . strtolower(url_title($each_product->name . get_variant_value($each_product->variant_seq, $each_product->variant_value, " "))) . '-' . $each_product->product_variant_seq . '">';
	    $html .= '<img class="img-terbaru img-responsive center-block full-img load_img"';
	    $html .= ' data-src="' . get_file_exists(PRODUCT_UPLOAD_IMAGE . $each_product->merchant_seq . '/' . S_IMAGE_UPLOAD . $each_product->image) . '"';
	    $html .= ' data-src-retina="' . get_file_exists(PRODUCT_UPLOAD_IMAGE . $each_product->merchant_seq . "/" . S_IMAGE_UPLOAD . $each_product->image) . '"';
	    $html .= ' src=' . get_file_exists(PRODUCT_UPLOAD_IMAGE . $each_product->merchant_seq . '/' . S_IMAGE_UPLOAD . $each_product->image);
	    $html .= '>';
	    $html .= '<div class="title-produk-box line-clamp">';
	    $html .= '<p class="title-produk"><b>' . $each_product->name . get_variant_value($each_product->variant_seq, $each_product->variant_value, "-") . '</b></p>';
	    $html .= '</div>';
	    $html .= '</a>';
	    if (trim($each_product->product_price) == '' OR trim($each_product->product_price) == trim($each_product->sell_price)) {
		$html .= '<p class="no-old-price">&nbsp;</p>';
	    } else {
		$html .= '<p class="old-price">' . RP . cnum($each_product->product_price) . '</p>';
	    }
	    $html .= '<p class="new-price">' . RP . cnum($each_product->sell_price) . '</p>';
	    $html .= '</div>';
	    $html .= '</div>';
	    $html .= '</div></li>';
	}
    }
    return $html;
}

function get_category_option($datas, $parent = 0, $selected = '-1') {
    $html = '<option value="-1" data-url="' . base_url() . '" ' . (($selected == '-1' || $selected == "") ? 'selected' : '') . '>- Pilih Kategori -</option>';
    $html .='<option value="0" data-url="' . base_url() . ALL_CATEGORY . '" ' . (($selected == '0') ? 'selected' : '') . '>Semua Kategori</option>';
    $counter = 1;
    if (isset($datas[$parent])) {
	foreach ($datas[$parent] as $each_datas) {
	    $html .= '<option value="' . $each_datas->seq . '" ' . (($selected == $each_datas->seq) ? 'selected' : '') . ' data-url="' . base_url() . url_title(strtolower($each_datas->name)) . "-" . CATEGORY . ($each_datas->seq) . '">' . $each_datas->name . '</option>';
	    $counter++;
	}
    }
    return $html;
}

function cdate($result, $type = "") {
    switch ($type) {
	case 1: //date time
	    if (!isset($result) || $result == "0000-00-00 00:00:00") {
		$return = "";
	    } else {
		$return = date("d-M-Y H:i:s", strtotime($result));
	    }
	    break;
	case 2: //date time
	    if (!isset($result) || $result == "0000-00-00 00:00:00") {
		$return = "";
	    } else {
		$return = date("d-M-y H:i:s", strtotime($result));
	    }
	    break;
	default:
	    if (!isset($result) || $result == "0000-00-00") {
		$return = "";
	    } else {
		$return = date("d-M-Y", strtotime($result));
	    }
    }
    return $return;
}

function combostatus($datas, $id = "") {
    $return = '';
    foreach ($datas as $option => $val) {
	$return.="<option value='" . $option . "'" . ($option == $id ? ' selected' : '') . ">" . $val . "</option>";
    }
    return $return;
}

function display_filter_name($attribute_name = '', $attribute_display_name = '') {
    return ($attribute_display_name == '' OR $attribute_display_name == '-') ? $attribute_name : $attribute_display_name;
}

function add_current_url_with_query_string_special($value = '', $parameter_name = PARAMETER_CATEGORY_ATTRIBUTE, $exploder_parameter = ',', $exploder_parent_child = '-') {
    //this function is not only add the parameter but it also remove parent data previous
    $output_query_string = '';
    $output = '';
    $query_string_parameter_name = array();
    if ($value != '') {
	$exploder_for_parent_child_param = explode($exploder_parent_child, $value);
	$parent_param = $exploder_for_parent_child_param[0];
	$child_param = $exploder_for_parent_child_param[1];
	$query_string = $_SERVER['QUERY_STRING'];
	parse_str($query_string, $query_string_array);
	if (isset($query_string_array[$parameter_name])) {
	    if ($query_string_array[$parameter_name] == "") {
		redirect(current_url_with_query_string(array(PARAMETER_CATEGORY_ATTRIBUTE)));
	    }
	    $parameter_value_list = explode($exploder_parameter, $query_string_array[$parameter_name]);
	    //query_string_attribute_cleaning;
	    foreach ($parameter_value_list as $each_parameter_value_list) {
		$exploder_for_parent_child = explode($exploder_parent_child, $each_parameter_value_list);
		$parent_uri = $exploder_for_parent_child[0];
		$child_uri = $exploder_for_parent_child[1];
		$query_string_parameter_name[$parent_uri][] = $child_uri;
	    }
	    //make query_string_unique for double value
	    foreach ($query_string_parameter_name as $parent_uri => $each_query_string_parameter_name) {
		$query_string_parameter_name[$parent_uri] = array_unique($query_string_parameter_name[$parent_uri]);
	    }
	    //if uri has child value 0 then set parent url value to 0
	    foreach ($query_string_parameter_name as $parent_uri => $each_query_string_parameter_name) {
		if (in_array('0', $query_string_parameter_name[$parent_uri])) {
		    $query_string_parameter_name[$parent_uri] = array('0');
		}
	    }
	    if ($child_param == 0) {
		foreach ($query_string_parameter_name as $parent_uri => $each_query_string_parameter_name) {
		    foreach ($each_query_string_parameter_name as $child_uri) {
			if ($parent_param != $parent_uri) {
			    $output_query_string .= $parent_uri . $exploder_parent_child . $child_uri . $exploder_parameter;
			}
		    }
		}
		$output_query_string .= $parent_param . $exploder_parent_child . $child_param;
	    } else {
		$is_parent_uri_equal_parent_param_n_child_uri_equal_child_param = false; //unset for child same
		foreach ($query_string_parameter_name as $parent_uri => $each_query_string_parameter_name) {
		    foreach ($each_query_string_parameter_name as $child_uri) {
			if ($parent_param != $parent_uri) {
			    $output_query_string .= $parent_uri . $exploder_parent_child . $child_uri . $exploder_parameter;
			} else {
			    if ($child_uri != '0') {
				if ($child_uri != $child_param) {
				    $output_query_string .= $parent_uri . $exploder_parent_child . $child_uri . $exploder_parameter;
				} else {
				    $is_parent_uri_equal_parent_param_n_child_uri_equal_child_param = true;
				}
			    }
			}
		    }
		}
		if (!$is_parent_uri_equal_parent_param_n_child_uri_equal_child_param) {
		    $output_query_string .= $parent_param . $exploder_parent_child . $child_param;
		} else {
		    $output_query_string = rtrim($output_query_string, $exploder_parameter);
		}
	    }
	    if ($output_query_string != "") {
		$output .= current_url_with_query_string(array($parameter_name), array($parameter_name => $output_query_string));
	    } else {
		$output .= current_url_with_query_string(array($parameter_name));
	    }
	    /*            $parameter_value_list = explode($exploder_parameter,$query_string_array[$parameter_name]);
	      foreach($parameter_value_list as $each_parameter_value_list){
	      $exploder_for_parent_child = explode($exploder_parent_child,$each_parameter_value_list);
	      $parent_uri = $exploder_for_parent_child[0];
	      $child_uri = $exploder_for_parent_child[1];
	      if($parent_param != $parent_uri){
	      $output_query_string .= $parent_uri.$exploder_parent_child.$child_uri;
	      }else{
	      $output_query_string .= $parent_param.$exploder_parent_child.$child_param;
	      }
	      }
	      $output = $output_query_string; */
//            $output .= current_url_with_query_string(array($parameter_name),array($parameter_name=>$output_query_string));
	} else {
	    $output = current_url_with_query_string();
	    $output .= ($_SERVER['QUERY_STRING'] != '') ? '&' : '?';
	    $output .= $parameter_name . '=' . $value;
	}
    } else {
	$output .= current_url_with_query_string();
    }
    return $output;
}

/*
  function add_current_url_with_query_string_special($value='',$parameter_name=PARAMETER_CATEGORY_ATTRIBUTE,$exploder_parameter=',',$exploder_parent_child = '-'){
  //this function is not only add the parameter but it also remove parent data previous
  $url = current_url();
  $return_attribute = array();
  $output = '';
  if($value != ''){
  $exploder_for_parent_child_param = explode($exploder_parent_child,$value);
  $parent_param = $exploder_for_parent_child_param[0];
  $child_param = $exploder_for_parent_child_param[1];
  $query_string = $_SERVER['QUERY_STRING'].urlencode($exploder_parameter.$value);
  parse_str($query_string, $query_string_array);
  if(isset($query_string_array[$parameter_name])){
  $parameter_value_list = explode($exploder_parameter,$query_string_array[$parameter_name]);
  foreach($parameter_value_list as $each_parameter_value_list){
  $exploder_for_parent_child = explode($exploder_parent_child,$each_parameter_value_list);
  $parent_uri = $exploder_for_parent_child[0];
  $child_uri = $exploder_for_parent_child[1];
  if($parent_param != $parent_uri){
  $return_attribute[$parent_uri] = $child_uri;
  }else{
  $return_attribute[$parent_param] = $child_param;
  }
  }
  $output_query_string = implode(',', array_map(function ($v, $k) { return $k . '-' . $v; }, $return_attribute, array_keys($return_attribute)));
  $output .= current_url_with_query_string(array($parameter_name),array($parameter_name=>$output_query_string));
  //            echo $output_query_string;exit();
  //            $output_query_string = urlencode($output_query_string);
  //            $new_query_string = str_replace($parameter_name.'='.$_GET[$parameter_name], $parameter_name.'='.$output_query_string, urldecode($_SERVER['QUERY_STRING']));
  //            $output .= isset($new_query_string)?('?'.$new_query_string):'';
  }else{
  $output = current_url_with_query_string();
  $output .= ($_SERVER['QUERY_STRING'] != '') ? '&' : '?';
  $output .= $parameter_name.'='.$value;
  }
  }else{
  $output .= current_url_with_query_string();
  }
  return $output;
  }
 */

function current_url_with_query_string($removed_parameter = array(), $added_parameter = array(), $rawurlencode = true) {
    $ret_url = current_url();
    $result_query_string = '';
    $counter = 0;
    parse_str($_SERVER['QUERY_STRING'], $query_string);
    foreach ($query_string as $key => $each_query_string) {
	if (!in_array($key, $removed_parameter)) {
	    if ($counter > 0) {
		$result_query_string .= '&';
	    }
	    if ($rawurlencode == true) {
		$result_query_string .= $key . '=' . rawurlencode($each_query_string);
	    } else {
		$result_query_string .= $key . '=' . $each_query_string;
	    }
	    $counter++;
	}
    }
    $ret_url .= ($counter > 0) ? ('?' . $result_query_string) : $result_query_string;
    foreach ($added_parameter as $parameter_name => $parameter_value) {
	if ($counter > 0 != '') {
	    $ret_url .= '&';
	} else {
	    $ret_url .= '?';
	}
	if ($rawurlencode == true) {
	    $ret_url .= $parameter_name . '=' . rawurlencode($parameter_value);
	} else {
	    $ret_url .= $parameter_name . '=' . $parameter_value;
	}
    }
    return $ret_url;
}

function url_with_query_string($url = '', $removed_parameter = array(), $added_parameter = array(), $rawurlencode = true) {
    $ret_url = ($url == '') ? current_url() : $url;
    $result_query_string = '';
    $counter = 0;
    parse_str($_SERVER['QUERY_STRING'], $query_string);
    foreach ($query_string as $key => $each_query_string) {
	if (!in_array($key, $removed_parameter)) {
	    if ($counter > 0) {
		$result_query_string .= '&';
	    }
	    if ($rawurlencode == true) {
		$result_query_string .= $key . '=' . rawurlencode($each_query_string);
	    } else {
		$result_query_string .= $key . '=' . $each_query_string;
	    }
	    $counter++;
	}
    }
    $ret_url .= ($counter > 0) ? ('?' . $result_query_string) : $result_query_string;
    foreach ($added_parameter as $parameter_name => $parameter_value) {
	if ($counter > 0 != '') {
	    $ret_url .= '&';
	} else {
	    $ret_url .= '?';
	}
	if ($rawurlencode == true) {
	    $ret_url .= $parameter_name . '=' . rawurlencode($parameter_value);
	} else {
	    $ret_url .= $parameter_name . '=' . $parameter_value;
	}
    }
    return $ret_url;
}

function get_selected_category($attribute_category_attribute_seq = '', $attribute_value_seq = '', $attribute_seq_attribute_value = '', $exploder_parameter = ',', $exploder_parent_child = '-') {

    $collection_attribute_category_attribute_seq = array();

    $attribute_seq_attribute_value_array = explode($exploder_parameter, $attribute_seq_attribute_value);
    foreach ($attribute_seq_attribute_value_array as $each_attribute_seq_attribute_value) {
	$split_attribute_value_array = explode($exploder_parent_child, $each_attribute_seq_attribute_value);
	$collection_attribute_category_attribute_seq[] = $split_attribute_value_array[0];
	if ($split_attribute_value_array[0] == $attribute_category_attribute_seq AND $split_attribute_value_array[1] == $attribute_value_seq) {
	    return TRUE;
	}
    }

    //if same value between attribute_value_seq and category_attribute_seq
    //if no default to semua
    if (!in_array($attribute_category_attribute_seq, $collection_attribute_category_attribute_seq)) {
	if ($attribute_value_seq == '0') {
	    return TRUE;
	}
    }
    return FALSE;
}

function long_text_with_elipsis($text = '', $n_char = 10) {
    if (strlen($text) > $n_char) {
	return substr($text, 0, $n_char) . '...';
    } else {
	return substr($text, 0, $n_char);
    }
}

function add_remove_attribute_in_tags($search = array(), $replace = array(), $subject = '') {
    if (count($search) != count($replace))
	return $subject;
    $total_replace = count($search);

    for ($i = 0; $i < $total_replace; $i++) {
	$subject = str_replace($search[$i], $replace[$i], $subject);
    }

    return $subject;
}

function create_star($num_star = 0, $total_star = 5) {
    $star_created = '';
    if ($num_star >= 0 AND $total_star > 0 AND $num_star <= $total_star) {
	for ($counter = 0; $counter < $num_star; $counter++) {
	    $star_created .= '<span class="fa fa-star"></span>';
	}
	for ($counter = $num_star; $counter < $total_star; $counter++) {
	    $star_created .= '<span class="fa fa-star-o"></span>';
	}
    }
    return $star_created;
}

//
?>