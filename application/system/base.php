<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * USER
 */
define("ADMIN", "admin");
define("MERCHANT", "merchant");
define("MEMBER", "member");
define("AGENT", "agent");
define("PARTNER", "partner");
/*
 * FORM
 */
define("CONTROL_ADMIN_CSRF_NAME", "txtAdminSID");
define("CONTROL_MERCHANT_CSRF_NAME", "txtMerchantnSID");
define("CONTROL_PARTNER_CSRF_NAME", "txtPartnernSID");
define("CONTROL_MEMBER_CSRF_NAME", "txtMemberSID");
define("CONTROL_AGENT_CSRF_NAME", "txtAgentSID");
define("CONTROL_COMMAND_NAME", "command_name");
define("CONTROL_SEARCH_NAME", "btnSearch");
define("CONTROL_EDIT_NAME", "btnEdit");
define("CONTROL_VIEW_NAME", "btnView");
define("CONTROL_ADD_NAME", "btnAdd");
define("CONTROL_SAVE_ADD_NAME", "btnSaveAdd");
define("CONTROL_SAVE_EDIT_NAME", "btnSaveEdit");
define("CONTROL_SAVE_DELETE_NAME", "btnSaveDel");
define("CONTROL_CANCEL_NAME", "btnCancel");
define("CONTROL_BACK_NAME", "btnBack");
define("CONTROL_SAVE_REGISTER_NAME", "btnSaveRegister");
define("CONTROL_GET_TYPE", "act");

define("CONTROL_ADDITIONAL_NAME", "btnAdditional");
define("CONTROL_APPROVE_NAME", "btnApprove");
define("CONTROL_REJECT_NAME", "btnReject");


define("FORM_CD", "form_cd");
define("FORM_PARENT_CD", "form_parent_cd");
define("FORM_PARENT_TITLE", "form_parent_title");
define("FORM_PARENT_URL", "form_parent_url");
define("FORM_DETAIL", "form_detail");
define("FORM_NAME", "form_name");
define("FORM_TITLE", "title");
define("FORM_AUTH_VIEW", "view");
define("FORM_AUTH_ADD", "add");
define("FORM_AUTH_EDIT", "edit");
define("FORM_AUTH_DELETE", "delete");
define("FORM_AUTH_APPROVE", "approve");
define("FORM_AUTH_PRINT", "print");
define("FORM_AUTH_SEARCH", "search");
define("FORM_AUTH", "form_auth");
define("FORM_URL", "form_url");
define("FORM_ACTION", "form_act");
define("FORM_ACTION_TITLE", "form_act_title");
define("FORM_POST", "post");
define("FORM_GET", "get");

define("ACTION_SEARCH", "act_sch");
define("ACTION_EDIT", "act_edt");
define("ACTION_ADD", "act_add");
define("ACTION_VIEW", "act_view");
define("ACTION_SAVE_ADD", "act_s_add");
define("ACTION_SAVE_UPDATE", "act_s_edt");
define("ACTION_SAVE_DELETE", "act_s_del");
define("ACTION_ADDITIONAL", "act_s_adt");
define("ACTION_APPROVE", "act_s_approve");
define("ACTION_REJECT", "act_s_reject");
define("ACTION_SAVE_REGISTER", "act_s_register");

define("SESSION_IP_ADDR", "ip_addr");
//define("SESSION_CSRF_TOKEN", "csrf_token");
define("SESSION_DATA", "data");
define("SESSION_PROVIDER", "provider");
define("SESSION_TEMP_DATA", "temp_data");

/*
 * USER PROFILE
 */
define("USER_NAME", "u_name");
define("USER_GROUP", "u_group");
define("USER_EMAIL", "u_email");
define("USER_IMAGE", "u_image");
define("USER_SEQ", "u_seq");
define("LOGIN_TYPE", "login_type");
define("LAST_LOGIN", "last_login");


/*
 * MEMBER SESSION KEY
 */

define("MEMBER_SESSION", "member_session");
define("SESSION_MEMBER_UID", "member_id");
define("SESSION_MEMBER_UNAME", "member_name");
define("SESSION_MEMBER_EMAIL", "member_email");
define("SESSION_MEMBER_SEQ", "member_seq");
define("SESSION_MEMBER_IMAGE", "profile_img");
define("SESSION_MEMBER_ADDRESS", "district_seq");
define("SESSION_MEMBER_USER_GROUP", "member_ug");
define("SESSION_MEMBER_LAST_URL", "member_url");
define("SESSION_MEMBER_CSRF_TOKEN", "csrf_member_token");
define("SESSION_MEMBER_FORM_AUTH", "member_form_auth");
define("SESSION_PRODUCT_INFO", "prod_info");
define("SESSION_PERSONAL_INFO", "person_info");
define("SESSION_PAYMENT_INFO", "pg_info");
define("SESSION_MEMBER_LAST_LOGIN", "member_last_login");
define("SESSION_TYPE_LOGIN", "type_login");
define("SESSION_CUSTOMER_DATA", "customer_data");
define("SESSION_DATA_SIMULATION", "data_simulation");





/*
 * AGENT SESSION KEY
 */

define("AGENT_SESSION", "agent_session");
define("SESSION_AGENT_UID", "agent_id");
define("SESSION_AGENT_UNAME", "agent_name");
define("SESSION_AGENT_EMAIL", "agent_email");
define("SESSION_AGENT_SEQ", "agent_seq");
define("SESSION_AGENT_IMAGE", "profile_img");
define("SESSION_AGENT_ADDRESS", "district_seq");
define("SESSION_AGENT_USER_GROUP", "agent_ug");
define("SESSION_AGENT_LAST_URL", "agent_url");
define("SESSION_AGENT_CSRF_TOKEN", "csrf_agent_token");
define("SESSION_AGENT_FORM_AUTH", "agent_form_auth");
define("SESSION_AGENT_LAST_LOGIN", "agent_last_login");

/*
 * ADMIN SESSION KEY
 */
define("SESSION_ADMIN_UID", "admin_id");
define("SESSION_ADMIN_UNAME", "admin_name");
define("SESSION_ADMIN_CSRF_TOKEN", "csrf_admin_token");
define("SESSION_ADMIN_FORM_AUTH", "admin_form_auth");
define("SESSION_ADMIN_USER_GROUP", "admin_ug");
define("SESSION_ADMIN_LEFT_NAV", "admin_left_nav");
define("SESSION_ADMIN_LAST_LOGIN", "admin_last_login");

/*
 * MERCHANT SESSION KEY
 */
define("SESSION_MERCHANT_UID", "merchant_id");
define("SESSION_MERCHANT_SEQ", "merchant_seq");
define("SESSION_MERCHANT_UNAME", "merchant_name");
define("SESSION_MERCHANT_EMAIL", "merchant_email");
define("SESSION_MERCHANT_IMAGE", "logo_img");
define("SESSION_MERCHANT_FORM_AUTH", "merchant_form_auth");
define("SESSION_MERCHANT_CSRF_TOKEN", "csrf_merchant_token");
define("SESSION_MERCHANT_USER_GROUP", "merchant_ug");
define("SESSION_MERCHANT_LEFT_NAV", "merchant_left_nav");
define("SESSION_MERCHANT_LAST_LOGIN", "merchant_last_login");

/*
 * PARTNER SESSION KEY
 */
define("SESSION_PARTNER_UID", "partner_id");
define("SESSION_PARTNER_SEQ", "partner_seq");
define("SESSION_PARTNER_NAME", "partner_name");
define("SESSION_PARTNER_UNAME", "partner_name");
define("SESSION_PARTNER_EMAIL", "partner_email");
define("SESSION_PARTNER_IMAGE", "logo_img");
define("SESSION_PARTNER_FORM_AUTH", "partner_form_auth");
define("SESSION_PARTNER_CSRF_TOKEN", "csrf_partner_token");
define("SESSION_PARTNER_USER_GROUP", "partner_ug");
define("SESSION_PARTNER_LEFT_NAV", "partner_left_nav");
define("SESSION_PARTNER_LAST_LOGIN", "partner_last_login");
/*
 * TITLE
 */

define("VIEW", "Lihat ");
define("LIST", "Daftar ");
define("EDIT", "Ubah ");
define("ADD", "Tambah ");

/*
 * TYPE LOGIN FOR MEMBER
 */
define("STANDARD_LOGIN", "standard_login");
define("GOOGLE_PLUS_LOGIN", "google_login");
define("FACEBOOK_LOGIN", "facebook_login");

/*
 * ERROR
 */
define("ERROR", "err");
define("ERROR_MESSAGE", "err_msg");
define("ERROR_CODE", "err_code");

/*
 * SUCCESS
 */
define("SUCCESS", "succ");
define("SUCCESS_MESSAGE", "succ_msg");

/*
 * DATA
 */

define("DATA_AUTH", "data_auth");
define("DATA_ERROR", "data_err");
define("DATA_SUCCESS", "data_suc");
define("DATA_LIST", "data_list");
define("DATA_SEARCH", "data_sch");
define("DATA_ACTION", "data_act");
define("DATA_SELECTED", "data_sel");
define("DATA_HEADER", "data_head");
define("DATA_PROD", "data_prod");
define("DATA_INIT", "data_init");
define("DATA_TEMP", "data_temp");
define("DATA_USER_PROFILE", "data_prof");
define("DATA_LEFT_NAV", "data_left");
define("DATA_STEP", "data_step");
define("DATA_ORDER", "data_ord");
define("DATA_BANK", "data_bank");
define("DATA_SIGNATURE", "data_sign");
define("DATA_ORDER_INFO", "data_ord_info");
define("DATA_MESSAGE_SUCCESS", "data_message_success");
define("DATA_MESSAGE_ERROR", "data_message_error");
define("DATA_CREDIT_MID", "data_credit_mid");
define("DATA_CREDIT_PAS", "data_credit_pas");
define("DATA_CREDIT_INFO", "data_credit_info");
define("DATA_PAYMENT", "data_paymnet");
define("DATA_BCA", "data_bca");
define("DATA_BNI", "data_bni");

define("LIST_DATA", "list_data");
define("LIST_RECORD", "list_rec");
define("FILTER", "filter");

define("HEADER_INFO", "h_info");

/*
 * SEQ
 */
define("DATA_ADDRESS", "data_address");
define("ADDRESS_SEQ", "address_seq");

/*
 * DATA DROPDOWN
 */
define("PROVINCE_NAME", "province_name");
define("CITY_NAME", "city_name");
define("DISTRICT_NAME", "district_name");
define("KEY", "key");
define("TYPE_NAME", "type_name");
define("CATEGORY_NAME", "category_name");
define("ATTRIBUTE_NAME", "attribute_name");
define("MODULE_NAME", "module_name");
define("USER_GROUP_NAME", "user_group_name");
define("VARIANT", "variant");
define("MEMBER_INFO", "member_info");
define("CUSTOMER_INFO", "customer_info");
define("PAYMENT_NAME", "payment_name");
define("PAYMENT_INFO", "payment_info");
define("VOUCHER_NAME", "voucher_name");
define("COUPON", "coupon");
define("PRODUCT_SEQ_STRING", "product_variant_string");
define("BANK_NAME", "bank_name");
define("MERCHANT_NAME", "merchant_name");
define("PARTNER_NAME", "partner_name");
define("PARTNER_SEQ", "partner_seq");
define("CATEGORY_LOAN", "category_loan");
define("TENOR_LOAN", "tenor_loan");
define("MERCHANT_INFO_SEQ", "merchant_info_seq");
define("PARTNER_INFO_SEQ", "partner_info_seq");
define("PRODUCT_CATEGORY", "product_category");
define("ATTRIBUTE_LIST", "attribute_list");
define("NODE_NAME", "node_name");
define("EXPEDITION_NAME", "exp_name");
define("REDEEM_PERIOD", "redeem_period");
define("EXPEDITION_DISTRICT_CODE", "expedition_district_code");
define("EXPEDITION_SERVICE_NAME", "expedition_service_name");
define("MERCHANT_LIST", "merchant_name");
define("PARTNER_LIST", "partner_name");
define("PRODUCT_LIST", "merchant_name");
define("MEMBER_EMAIL", "email");
define("AGENT_EMAIL", "agent");
define("MEMBER_NAME", "member_name_email");
define("PRODUCT_NAME", "product");
define("PROVIDER_NAME", "provider_name");
define("ORDER_NO", "order_no");
define("ORDER_SEQ", "order_seq");
define("AGENT_INFO", "agent_info");

define("IMAGE", "image");
define("CAPTCHA", "captcha");
define("WORD", "word");
define("AWB_SEQ", "awb_seq");

/*
 * DATA TREEVIEW
 */
define("TREE_MENU", "tree_menu");
define("TREE_PAYMENT_GATEWAY", "pg_menu");
define("ROOT", "ROOT");
define("DEFAULT_ROOT", "0");
define("DEFAULT_MODULE_SEQ", "1");

/*
 * DATA TYPE
 */

define("TYPE_STRING", "str");
define("TYPE_NUMERIC", "num");
define("TYPE_DATE", "dte");
define("TYPE_DATETIME", "dti");
define("TYPE_BOOLEAN", "bool");
define("TYPE_NEW_DATE", "dtei");

/*
 * DATA TABLE HEADER
 */
define("TH_CREATED_BY", "Dibuat Oleh");
define("TH_CREATED_DATE", "Tanggal Buat");
define("CREATED_DATE", "Tanggal Pengajuan");
define("TH_MODIFIED_BY", "Diubah Oleh");
define("TH_MODIFIED_DATE", "Tanggal Ubah");

/*
 * DATA IMAGE PLACEHOLDER
 */
define("IMG_BLANK_100", CDN_IMAGE."assets/img/home/no_image.png");
define("IMG_NO_LOGO", CDN_IMAGE."assets/img/home/img_page.jpg");
define("IMG_NO_BANNER", CDN_IMAGE."assets/img/home/landing_page.jpg");
define("IMG_ADMIN_PROFILE", CDN_IMAGE."toko1001_ico.jpg");
/*
 * DASHBOARD
 */
define("ORDER_NEED_CONFIRMATION", "order_need_confirmation");
define("WITHDRAW_NEED_CONFIRMATION", "withdraw_need_confirmation");
define("NEW_MERCHANT", "new_merchant");
define("NEW_PARTNER", "new_partner");
define("NEW_PRODUCT", "new_products");
define("PENDING_REFUND", "pending_refund");
define("PENDING_DELIVERY", "pending_delivery");
define("PENDING_NEW_PRODUCT", "pending_new_product");
define("PENDING_PRODUCT_CHANGE", "pending_product_change");
define("OUT_OF_STOCK", "out_of_stock");
define("UNPRINTED", "unprinted");

//media social image

define("MEDSOS_INSTAGRAM", "instagram-email.png");
define("MEDSOS_FACEBOOK", "facebook-email.png");
define("MEDSOS_TWITTER", "twitter-email.png");

class BusisnessException extends Exception {

}

class TechnicalException extends Exception {

}

function validate_input($param) {

}

?>
