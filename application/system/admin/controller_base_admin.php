<?php

require_once APPPATH . 'system/base.php';
require_once APPPATH . 'system/controller_base.php';



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_controller_base
 *
 * @author Jartono
 */
class controller_base_admin extends ControllerBase {

    public function __construct($form_cd = "", $form_url = "", $check_logged_in = true, $params="") {
        return parent::__construct($form_cd, $form_url, $check_logged_in, "admin/login", ADMIN_LOGIN, $params);
    }

    protected function get_admin_user_id() {
        if (isset($_SESSION[SESSION_ADMIN_UID])) {
            return $this->session->userdata(SESSION_ADMIN_UID);
        } else {
            return "";
        }
    }

}

?>