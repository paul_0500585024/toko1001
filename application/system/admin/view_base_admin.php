<?php

require_once APPPATH . 'system/view_base.php';

/*
 * URL
 */

function get_css_url() {
    return get_base_url() . "assets/css/";
}

function get_js_url() {
    return get_base_url() . "assets/js/";
}

function get_img_url() {
    return get_base_url() . "assets/img/";
}

function get_content_url() {
    return VIEWPATH . "admin/content/";
}

function get_component_url() {
    return VIEWPATH . "admin/component/";
}

function get_include_content_admin_header() {
    return get_content_url() . "header.php";
}

function get_include_content_admin_top_navigation() {
    return get_content_url() . "top_nav.php";
}

function get_include_content_admin_left_navigation() {
    return get_content_url() . "left_nav.php";
}

function get_include_content_admin_right_navigation() {
    return get_content_url() . "right_nav.php";
}

function get_include_content_admin_footer() {
    return get_content_url() . "footer.php";
}

function get_include_content_admin_top_page_navigation() {
    return get_content_url() . "top_page.php";
}

function get_include_content_admin_bottom_page_navigation() {
    return get_content_url() . "bottom_page.php";
}

function get_include_page_list_admin_content_header() {
    return get_content_url() . "page_list_header.php";
}

function get_include_page_list_admin_content_footer() {
    return get_content_url() . "page_list_footer.php";
}

function get_cannot_view_admin_content() {
    return "<button class = \"btn btn-flat bg-orange \" id = \"refresh\" data-toggle = \"tooltip\" title = \"Cari\" type='button'><i class = \"fa fa-refresh\"></i></button>";
}

function combostatus($datas, $id = "") {
    $return = '';
    foreach ($datas as $option => $val) {
        $return.="<option value='" . $option . "'" . ($option == $id ? ' selected' : '') . ">" . $val . "</option>";
    }
    return $return;
}

function cnum($result, $decimal = 0) {
    return number_format($result, $decimal);
}

function status($result, $def) {
    $data = json_decode($def, true);
    return ($data[$result]);
}

function cdate($result, $type = "") {
    switch ($type) {
        case 1: //date time
            if (!isset($result) || $result == "0000-00-00 00:00:00") {
                $return = "";
            } else {
                $return = date("d-M-Y H:i:s", strtotime($result));
            }
            break;
        default:
            if (!isset($result) || $result == "0000-00-00") {
                $return = "";
            } else {
                $return = date("d-M-Y", strtotime($result));
            }
    }
    return $return;
}

function cstdes($result, $def) {
    $data = json_decode($def, true);
    return ($data[$result]);
}

?>