<?php

require_once APPPATH . 'system/base.php';

class ControllerBase extends CI_Controller {

    public function __construct($form_cd = "", $form_url = "", $check_logged_in = true, $login_form = "", $type_login = "", $params = "") {

        parent::__construct();

        $ajax_data = array(
            'url' => base_url($login_form),
            'message' => 'login redirection due to unauthorization',
            'code' => '401',
        );

        if ($type_login == MEMBER_LOGIN) {
            if (isset($_SESSION[SESSION_AGENT_UID])) {
                $type_login = AGENT_LOGIN;
                $login_form = "agent/login";
            }
        }

        if ($check_logged_in) {
            switch ($type_login) {
                case ADMIN_LOGIN : {
                        if (isset($_SESSION[SESSION_ADMIN_CSRF_TOKEN])) {
                            if ($this->session->userdata(SESSION_ADMIN_CSRF_TOKEN) == "") {
                                if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                                    echo json_encode($ajax_data);
                                    exit();
                                } else {
                                    redirect(base_url($login_form) . "?url=" . $form_url);
                                }
                            } else {
                                $user_profile = [
                                    USER_NAME => $this->session->userdata(SESSION_ADMIN_UNAME),
                                    USER_GROUP => $this->session->userdata(SESSION_ADMIN_USER_GROUP),
                                    LAST_LOGIN => $this->session->userdata(SESSION_ADMIN_LAST_LOGIN),
                                    LOGIN_TYPE => ADMIN_LOGIN
                                ];
                            }
                        } else {
                            if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                                echo json_encode($ajax_data);
                                exit();
                            } else {
                                redirect(base_url($login_form) . "?url=" . $form_url . "/" . $params);
                            }
                        }

                        $data[DATA_LEFT_NAV] = $_SESSION[SESSION_ADMIN_LEFT_NAV];
                    }break;
                case MERCHANT_LOGIN : {
                        if (isset($_SESSION[SESSION_MERCHANT_CSRF_TOKEN])) {
                            if ($this->session->userdata(SESSION_MERCHANT_CSRF_TOKEN) == "") {
                                if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                                    echo json_encode($ajax_data);
                                    exit();
                                } else {
                                    redirect(base_url($login_form));
                                }
                            } else {
                                $user_profile = [
                                    USER_NAME => $this->session->userdata(SESSION_MERCHANT_UNAME),
                                    USER_GROUP => $this->session->userdata(SESSION_MERCHANT_USER_GROUP),
                                    USER_EMAIL => $this->session->userdata(SESSION_MERCHANT_EMAIL),
                                    USER_IMAGE => $this->session->userdata(SESSION_MERCHANT_IMAGE),
                                    USER_SEQ => $this->session->userdata(SESSION_MERCHANT_SEQ),
                                    LAST_LOGIN => $this->session->userdata(SESSION_MERCHANT_LAST_LOGIN),
                                    LOGIN_TYPE => MERCHANT_LOGIN
                                ];
                            }
                        } else {
                            if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                                echo json_encode($ajax_data);
                                exit();
                            } else {
                                redirect(base_url($login_form) . "?url=" . $form_url);
                            }
                        }
                        $data[DATA_LEFT_NAV] = $_SESSION[SESSION_MERCHANT_LEFT_NAV];
                    }break;
                case MEMBER_LOGIN : {
                        if (isset($_SESSION[SESSION_MEMBER_CSRF_TOKEN])) {
                            if ($this->session->userdata(SESSION_MEMBER_CSRF_TOKEN) == "") {
                                if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                                    echo json_encode($ajax_data);
                                    exit();
                                } else {
                                    redirect(base_url($login_form) . "?url=" . $form_url . $params);
                                }
                            } else {
                                $user_profile = [
                                    USER_NAME => $this->session->userdata(SESSION_MEMBER_UNAME),
                                    USER_GROUP => $this->session->userdata(SESSION_MEMBER_USER_GROUP),
                                    LAST_LOGIN => $this->session->userdata(SESSION_MEMBER_LAST_LOGIN),
                                    LOGIN_TYPE => MEMBER_LOGIN
                                ];
                                $member_url[SESSION_MEMBER_LAST_URL] = $form_url;
                                $this->session->set_userdata($member_url);
                            }
                        } else {
                            if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                                echo json_encode($ajax_data);
                                exit();
                            } else {
                                redirect(base_url($login_form) . "?url=" . $form_url . "/" . $params);
                            }
                        }
                    }break;
                case AGENT_LOGIN : {
                        if (isset($_SESSION[SESSION_AGENT_CSRF_TOKEN])) {
                            if ($this->session->userdata(SESSION_AGENT_CSRF_TOKEN) == "") {
                                if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                                    echo json_encode($ajax_data);
                                    exit();
                                } else {
                                    redirect(base_url($login_form) . "?url=" . $form_url . $params);
                                }
                            } else {
                                $user_profile = [
                                    USER_NAME => $this->session->userdata(SESSION_AGENT_UNAME),
                                    USER_GROUP => $this->session->userdata(SESSION_AGENT_USER_GROUP),
                                    PARTNER_SEQ => $this->session->userdata(SESSION_PARTNER_SEQ),
                                    PARTNER_NAME => $this->session->userdata(SESSION_PARTNER_NAME),
                                    LAST_LOGIN => $this->session->userdata(SESSION_AGENT_LAST_LOGIN),
                                    LOGIN_TYPE => AGENT_LOGIN
                                ];
                                $agent_url[SESSION_AGENT_LAST_URL] = $form_url;
                                $this->session->set_userdata($agent_url);
                            }
                        } else {
                            if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                                echo json_encode($ajax_data);
                                exit();
                            } else {
                                redirect(base_url($login_form) . "?url=" . $form_url . "/" . $params);
                            }
                        }
                    }break;
                case PARTNER_LOGIN : {
                        if (isset($_SESSION[SESSION_PARTNER_CSRF_TOKEN])) {
                            if ($this->session->userdata(SESSION_PARTNER_CSRF_TOKEN) == "") {
                                if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                                    echo json_encode($ajax_data);
                                    exit();
                                } else {
                                    redirect(base_url($login_form));
                                }
                            } else {
                                $user_profile = [
                                    USER_NAME => $this->session->userdata(SESSION_PARTNER_UNAME),
                                    USER_GROUP => $this->session->userdata(SESSION_PARTNER_USER_GROUP),
                                    USER_EMAIL => $this->session->userdata(SESSION_PARTNER_EMAIL),
                                    USER_IMAGE => $this->session->userdata(SESSION_PARTNER_IMAGE),
                                    USER_SEQ => $this->session->userdata(SESSION_PARTNER_SEQ),
                                    LAST_LOGIN => $this->session->userdata(SESSION_PARTNER_LAST_LOGIN),
                                    LOGIN_TYPE => PARTNER_LOGIN
                                ];
                            }
                        } else {
                            if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                                echo json_encode($ajax_data);
                                exit();
                            } else {
                                redirect(base_url($login_form) . "?url=" . $form_url);
                            }
                        }
                        $data[DATA_LEFT_NAV] = $_SESSION[SESSION_PARTNER_LEFT_NAV];
                    }break;
            }
            $data[DATA_USER_PROFILE] = $user_profile;
        } else {
            
        }
        if ($form_cd != "") {
            $auth = [
                FORM_URL => $form_url,
                FORM_ACTION => "",
                FORM_CD => $form_cd,
                FORM_AUTH => $this->get_form_auth($form_cd, $type_login)
            ];
            if ($this->input->method(false) == FORM_POST) {
                if ($this->input->post(CONTROL_SEARCH_NAME) !== null) {
                    $auth[FORM_ACTION] = ACTION_SEARCH;
                } elseif ($this->input->post(CONTROL_SAVE_ADD_NAME) !== null) {
                    $auth[FORM_ACTION] = ACTION_SAVE_ADD;
                    $auth[FORM_ACTION_TITLE] = ADD;
                } elseif ($this->input->post(CONTROL_SAVE_EDIT_NAME) !== null) {
                    $auth[FORM_ACTION] = ACTION_SAVE_UPDATE;
                    $auth[FORM_ACTION_TITLE] = EDIT;
                } elseif ($this->input->post(CONTROL_SAVE_DELETE_NAME) !== null) {
                    $auth[FORM_ACTION] = ACTION_SAVE_DELETE;
//                } elseif ($this->input->post(ACTION_SAVE_DELETE) !== null) {
//                    $auth[FORM_ACTION] = ACTION_EDIT;
                } elseif ($this->input->post(CONTROL_ADD_NAME) !== null) {
                    $auth[FORM_ACTION] = ACTION_ADD;
                    $auth[FORM_ACTION_TITLE] = ADD;
                } elseif ($this->input->post(CONTROL_VIEW_NAME) !== null) {
                    $auth[FORM_ACTION] = ACTION_VIEW;
                    $auth[FORM_ACTION_TITLE] = VIEW;
                } elseif ($this->input->post(CONTROL_EDIT_NAME) !== null) {
                    $auth[FORM_ACTION] = ACTION_EDIT;
                    $auth[FORM_ACTION_TITLE] = EDIT;
                } elseif ($this->input->post(CONTROL_ADDITIONAL_NAME) !== null) {
                    $auth[FORM_ACTION] = ACTION_ADDITIONAL;
                } elseif ($this->input->post(CONTROL_APPROVE_NAME) !== null) {
                    $auth[FORM_ACTION] = ACTION_APPROVE;
                } elseif ($this->input->post(CONTROL_REJECT_NAME) !== null) {
                    $auth[FORM_ACTION] = ACTION_REJECT;
                };
            } else if ($type_login == MERCHANT_LOGIN || $type_login == PARTNER_LOGIN) {
                switch ($this->input->get(CONTROL_GET_TYPE)) {
                    case ACTION_ADD : {
                            $auth[FORM_ACTION] = ACTION_ADD;
                            $auth[FORM_ACTION_TITLE] = ADD;
                        }break;
                    case ACTION_EDIT : {
                            $auth[FORM_ACTION] = ACTION_EDIT;
                            $auth[FORM_ACTION_TITLE] = EDIT;
                        }break;
                    case ACTION_VIEW : {
                            $auth[FORM_ACTION] = ACTION_VIEW;
                            $auth[FORM_ACTION_TITLE] = VIEW;
                        }break;
                    case ACTION_ADDITIONAL : {
                            $auth[FORM_ACTION] = ACTION_ADDITIONAL;
                            $auth[FORM_ACTION_TITLE] = '';
                        }break;
                    default : {
                            $auth[FORM_ACTION] = ACTION_SEARCH;
                        }break;
                }
            }

            $err = [
                ERROR => false,
                ERROR_MESSAGE => ""
            ];

            $succ = [
                SUCCESS => false,
                SUCCESS_MESSAGE => ""
            ];

            $list = [
                LIST_DATA => null,
                LIST_RECORD => null
            ];
            $sel = [
                LIST_DATA => null
            ];
            $search = [
                LIST_DATA => null,
                LIST_RECORD => null
            ];

            $action = [
                ACTION_SEARCH => "",
                ACTION_EDIT => "",
                ACTION_SAVE_ADD => "",
                ACTION_SAVE_UPDATE => "",
                ACTION_SAVE_DELETE => "",
                ACTION_APPROVE => "",
                ACTION_REJECT => ""
            ];

            $data[DATA_AUTH] = $auth;
            $data[DATA_ERROR] = $err;
            $data[DATA_SUCCESS] = $succ;
            $data[DATA_LIST] = $list;
            $data[DATA_SELECTED] = $sel;
            $data[DATA_SEARCH] = $search;
            $data[DATA_ACTION] = $action;
            $data[DATA_INIT] = true;
        }
        if (isset($_SESSION[SESSION_DATA])) {
            $data = $_SESSION[SESSION_DATA];
            $data[DATA_INIT] = false;
            unset($_SESSION[SESSION_DATA]);
        }
        return (isset($data) ? $data : "");
    }

    protected function register_event(&$data, $action, $function_name) {
        switch ($action) {
            case ACTION_SEARCH:
                $data[DATA_ACTION][ACTION_SEARCH] = $function_name;
                break;
            case ACTION_EDIT:
                $data[DATA_ACTION][ACTION_EDIT] = $function_name;
                $data[DATA_ACTION][ACTION_VIEW] = $function_name;
                break;
            case ACTION_SAVE_ADD:
                $data[DATA_ACTION][ACTION_SAVE_ADD] = $function_name;
                break;
            case ACTION_SAVE_UPDATE:
                $data[DATA_ACTION][ACTION_SAVE_UPDATE] = $function_name;
                break;
            case ACTION_SAVE_DELETE:
                $data[DATA_ACTION][ACTION_SAVE_DELETE] = $function_name;
                break;
            case ACTION_ADDITIONAL:
                $data[DATA_ACTION][ACTION_ADDITIONAL] = $function_name;
                break;
            case ACTION_APPROVE:
                $data[DATA_ACTION][ACTION_APPROVE] = $function_name;
                break;
            case ACTION_REJECT:
                $data[DATA_ACTION][ACTION_REJECT] = $function_name;
                break;
            default:
                unset($data);
                throw new TechnicalException("ACTION NOT FOUND");
        }
        unset($data);
    }

    protected function fire_event(&$data) {
        switch ($data[DATA_AUTH][FORM_ACTION]) {
            case ACTION_SEARCH:
                If ($data[DATA_ACTION][ACTION_SEARCH] != "") {
                    call_user_func(array($this, $data[DATA_ACTION][ACTION_SEARCH]));
                }
                break;

            case ACTION_SAVE_ADD:
                If ($data[DATA_ACTION][ACTION_SAVE_ADD] != "") {
                    if ($this->auth_form($data, FORM_AUTH_ADD)) {
                        call_user_func(array($this, $data[DATA_ACTION][ACTION_SAVE_ADD]));
                    } else {
                        $this->set_error($data, new TechnicalException("CANNOT ADD NO ACCESS PERMISSION"));
                        call_user_func(array($this, $data[DATA_ACTION][ACTION_SAVE_ADD]));
                    }
                }
                break;

            case ACTION_SAVE_UPDATE:
                If ($data[DATA_ACTION][ACTION_SAVE_UPDATE] != "") {
                    if ($this->auth_form($data, FORM_AUTH_EDIT)) {
                        call_user_func(array($this, $data[DATA_ACTION][ACTION_SAVE_UPDATE]));
                    } else {
                        $this->set_error($data, new TechnicalException("CANNOT UPDATE NO ACCESS PERMISSION"));
                        call_user_func(array($this, $data[DATA_ACTION][ACTION_SAVE_UPDATE]));
                    }
                }
                break;

            case ACTION_SAVE_DELETE:
                If ($data[DATA_ACTION][ACTION_SAVE_DELETE] != "") {
                    call_user_func(array($this, $data[DATA_ACTION][ACTION_SAVE_DELETE]));
                }
                break;

            case ACTION_EDIT:
                If ($data[DATA_ACTION][ACTION_EDIT] != "") {
                    call_user_func(array($this, $data[DATA_ACTION][ACTION_EDIT]));
                }
                break;

            case ACTION_VIEW:
                If ($data[DATA_ACTION][ACTION_VIEW] != "") {
                    call_user_func(array($this, $data[DATA_ACTION][ACTION_VIEW]));
                }
                break;

//            case ACTION_ADD:
//                If ($data[DATA_ACTION][ACTION_ADD] != "") {
//                    call_user_func(array($this, $data[DATA_ACTION][ACTION_ADD]));
//                }
//                break;

            case ACTION_ADDITIONAL:
                If ($data[DATA_ACTION][ACTION_ADDITIONAL] != "") {
                    call_user_func(array($this, $data[DATA_ACTION][ACTION_ADDITIONAL]));
                }
                break;
            case ACTION_APPROVE:
                If ($data[DATA_ACTION][ACTION_APPROVE] != "") {
                    call_user_func(array($this, $data[DATA_ACTION][ACTION_APPROVE]));
                }
                break;
            case ACTION_REJECT:
                If ($data[DATA_ACTION][ACTION_REJECT] != "") {
                    call_user_func(array($this, $data[DATA_ACTION][ACTION_REJECT]));
                }
                break;

            default:
                $this->auth_form($data, FORM_AUTH_SEARCH);
        }
        unset($data);
    }

    private function get_form_auth($form_cd, $type_cd) {
        switch ($type_cd) {
            case ADMIN_LOGIN: {
                    if (array_key_exists($form_cd, $this->session->userdata(SESSION_ADMIN_FORM_AUTH))) {
                        return $this->session->userdata(SESSION_ADMIN_FORM_AUTH)[$form_cd];
                    } else {
                        return "";
                    }
                }break;

            case MERCHANT_LOGIN : {
                    if (array_key_exists($form_cd, $this->session->userdata(SESSION_MERCHANT_FORM_AUTH))) {
                        return $this->session->userdata(SESSION_MERCHANT_FORM_AUTH)[$form_cd];
                    } else {
                        return "";
                    }
                }break;

            case MEMBER_LOGIN : {
                    return "";
                }break;
            case PARTNER_LOGIN : {
                    if (array_key_exists($form_cd, $this->session->userdata(SESSION_PARTNER_FORM_AUTH))) {
                        return $this->session->userdata(SESSION_PARTNER_FORM_AUTH)[$form_cd];
                    } else {
                        return "";
                    }
                }break;
        }
    }

    protected function get_input_post($ctrl_name, $validate = false, $type = "", $label = "", &$data = "") {
        $value = trim($this->input->post($ctrl_name, true));
        if ($validate == true) {
            $this->validate_form($type, $value, $label, $data);
        }
        return $value;
    }

    protected function get_input_get($ctrl_name, $validate = false, $type = "", $label = "", &$data = "") {
        $value = trim($this->input->get($ctrl_name, true));
        if ($validate == true) {
            $this->validate_form($type, $value, $label, $data);
        }
        return $value;
    }

    protected function validate_form($type, $value, $label, &$data) {
        switch ($type) {
            case FILL_VALIDATOR : {
                    if ($value == "") {
                        $data[DATA_ERROR][ERROR] = true;
                        $data[DATA_ERROR][ERROR_MESSAGE][] = "ERROR : " . $label . " " . ERROR_VALIDATION_MUST_FILL;
                    }
                } break;
            case NUMERIC_VALIDATOR : {
                    if (!is_numeric($value)) {
                        $data[DATA_ERROR][ERROR] = true;
                        $data[DATA_ERROR][ERROR_MESSAGE][] = "ERROR : " . $label . " " . ERROR_VALIDATION_MUST_NUMERIC;
                    }
                } break;
            case PERCENTAGE_VALIDATOR : {
                    if (!is_numeric($value)) {
                        $data[DATA_ERROR][ERROR] = true;
                        $data[DATA_ERROR][ERROR_MESSAGE][] = "ERROR : " . $label . " " . ERROR_VALIDATION_MUST_NUMERIC;
                    } elseif ($value > 100 || $value < 0) {
                        $data[DATA_ERROR][ERROR] = true;
                        $data[DATA_ERROR][ERROR_MESSAGE][] = "ERROR : " . $label . " " . ERROR_VALIDATION_MUST_PERCENTAGE;
                    }
                }break;
            case QTY_VALIDATOR : {
                    if (!is_numeric($value)) {
                        $data[DATA_ERROR][ERROR] = true;
                        $data[DATA_ERROR][ERROR_MESSAGE][] = "ERROR : " . $label . " " . ERROR_VALIDATION_MUST_NUMERIC;
                    } elseif ($value <= 0) {
                        $data[DATA_ERROR][ERROR] = true;
                        $data[DATA_ERROR][ERROR_MESSAGE][] = "ERROR : " . $label . " " . ERROR_VALIDATION_MUST_GREATER_THAN_ZERO;
                    }
                } break;
            case STOCK_VALIDATOR : {
                    if (!is_numeric($value)) {
                        $data[DATA_ERROR][ERROR] = true;
                        $data[DATA_ERROR][ERROR_MESSAGE][] = "ERROR : " . $label . " " . ERROR_VALIDATION_MUST_NUMERIC;
                    } elseif ($value < 0) {
                        $data[DATA_ERROR][ERROR] = true;
                        $data[DATA_ERROR][ERROR_MESSAGE][] = "ERROR : " . $label . " " . ERROR_VALIDATION_MUST_GREATER_THAN_ZERO;
                    }
                } break;
            case DATE_VALIDATOR : {
                    if ($value == "0000-00-00") {
                        $data[DATA_ERROR][ERROR] = true;
                        $data[DATA_ERROR][ERROR_MESSAGE][] = "ERROR : " . $label . " " . ERROR_DATE_VALIDATION;
                    } elseif ($value == "") {
                        $data[DATA_ERROR][ERROR] = true;
                        $data[DATA_ERROR][ERROR_MESSAGE][] = "ERROR : " . $label . " " . ERROR_VALIDATION_MUST_FILL;
                    } elseif ($value != $this->validateDate($value)) {
                        $data[DATA_ERROR][ERROR] = true;
                        $data[DATA_ERROR][ERROR_MESSAGE][] = "ERROR : " . $label . " " . ERROR_DATE_INVALID;
                    }
//                    } elseif ($value < "01-Jan-2010") {
//                        $data[DATA_ERROR][ERROR] = true;
//                        $data[DATA_ERROR][ERROR_MESSAGE][] = "ERROR : " . $label . " " . ERROR_DATE_INVALID;
//                    }
                } break;
            case GENDER_VALIDATOR : {
                    if (!(strtoupper($value) == 'M' OR strtoupper($value) == 'F')) {
                        $data[DATA_ERROR][ERROR] = true;
                        $data[DATA_ERROR][ERROR_MESSAGE][] = "ERROR : " . $label . " " . ERROR_GENDER;
                    }
                } break;
            case EMAIL_VALIDATOR : {
                    if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                        $data[DATA_ERROR][ERROR] = true;
                        $data[DATA_ERROR][ERROR_MESSAGE][] = "ERROR : " . $label . " " . ERROR_EMAIL_FORMAT;
                    }
                } break;
        }
    }

    protected function validateDate($date) {
        $d = DateTime::createFromFormat('d-M-Y', $date);
        return $d && $d->format('d-M-Y') == $date;
    }

    protected function set_list_data(&$data, $list_data) {
        $data[DATA_LIST][LIST_RECORD] = $list_data[0];
        if (count($list_data) == 2) {
            $data[DATA_LIST][LIST_DATA] = $list_data[1];
        }

        unset($data);
    }

    protected function set_data(&$data, $sel_data) {
        if (isset($data[DATA_SELECTED][LIST_DATA])) {
            unset($data[DATA_SELECTED][LIST_DATA]);
        }
        $data[DATA_SELECTED][LIST_DATA] = $sel_data;
        unset($data);
    }

    protected function set_data_header(&$data, $sel_data) {
        if (isset($data[DATA_HEADER][LIST_DATA])) {
            unset($data[DATA_HEADER][LIST_DATA]);
        }
        $data[DATA_HEADER][LIST_DATA] = $sel_data;
        unset($data);
    }

    protected function set_error(&$data, $ex) {
        $data[DATA_ERROR][ERROR] = true;
        $data[DATA_ERROR][ERROR_MESSAGE][] = "ERROR: " . ($ex->getMessage());
        $data[DATA_ERROR][ERROR_CODE] = $ex->getCode();

        unset($data);
    }

    protected function set_json_error($ex, $err_msg = "") {
        if ($err_msg == "") {
            $output = array("error" => TRUE, "message" => $ex->getMessage());
            echo json_encode($output);
            die();
        } else {
            $output = array("error" => TRUE, "message" => $err_msg);
            echo json_encode($output);
            die();
        }
    }

    protected function set_json_success() {
        $output = array("error" => FALSE, "message" => "");
        echo json_encode($output);
        die();
    }

    private function auth_form(&$data, $action) {
        $auth_message = $this->check_auth_form($data[DATA_AUTH], $action, $data[DATA_USER_PROFILE][LOGIN_TYPE]);
        unset($data);

        if ($auth_message != "") {
            $this->set_error($data, new BusisnessException($auth_message));
            return false;
        } else {
            return true;
        }
    }

    protected function check_auth_form($form_assoc, $action, $type_login) {        
        if ($form_assoc[FORM_AUTH] === "") {
            switch ($type_login) {
                case ADMIN_LOGIN : redirect(base_url("admin/main_page"));
                    break;
                case MERCHANT_LOGIN : redirect(base_url("merchant/main_page"));
                    break;
//                case MEMBER_LOGIN: redirect(base_url("member/main_page"));
//                    break;
            }
        } elseif ($form_assoc[FORM_AUTH][$action] === false) {
            return "TIDAK ADA HAK AKSES";
        } elseif ($type_login == ADMIN_LOGIN && ($this->get_input_post(CONTROL_ADMIN_CSRF_NAME) != $this->session->userdata(SESSION_ADMIN_CSRF_TOKEN))) {
            return "TIDAK VALID";
        } elseif ($type_login == MERCHANT_LOGIN && ($this->get_input_post(CONTROL_MERCHANT_CSRF_NAME) != $this->session->userdata(SESSION_MERCHANT_CSRF_TOKEN))) {
            return "TIDAK VALID";
        } elseif ($type_login == MEMBER_LOGIN && ($this->get_input_post(CONTROL_MEMBER_CSRF_NAME) != $this->session->userdata(SESSION_MEMBER_CSRF_TOKEN))) {
            return "TIDAK VALID";
        } else {
            return "";
        }
// CEK HARUS SUBMIT DARI WEBSITE SENDIRI, TIDAK BOLEH DARI WEBSITE ORANG LAIN
    }

    protected function get_ip_address() {
        if (isset($_SESSION[SESSION_IP_ADDR])) {
            return $this->session->userdata(SESSION_IP_ADDR);
        } else {
            return "";
        }
    }

    public function upload_file($upload_id, $upload_path, $file_name, $file_type, &$data, $dimension_type = "", $error_name = "") {
        $return_value = new stdClass;
        $delete = false;
        $file_delete = "";
//var_dump($_FILES[);exit();
        $real_name = $_FILES[$upload_id]["name"];
        $ext = pathinfo($real_name, PATHINFO_EXTENSION);
        if (preg_match("/\.(" . $file_type . ")$/", $real_name)) {
            if ($_FILES[$upload_id][TEMP_NAME]) {
                if (is_uploaded_file($_FILES[$upload_id][TEMP_NAME])) {
                    if ($file_name == "") {
                        $file_name = $real_name;
                    }

                    if ($file_type == IMAGE_TYPE) {
                        $dimension = (object) unserialize($dimension_type);
                        $config[MAX_HEIGHT] = $dimension->max_height;
                        $config[MAX_WIDTH] = $dimension->max_width;
                        $config[MIN_WIDTH] = $dimension->min_width;
                        $config[MIN_HEIGHT] = $dimension->min_height;
                        $config[MAX_SIZE] = DEFAULT_MAX_LIMIT_IMAGE;
                    }


                    $config[UPLOAD_PATH] = $upload_path;
                    $config[ALLOWED_TYPES] = $file_type;
                    $config[FILE_NAME] = $file_name;
                    $file_url = $upload_path . $file_name;

                    if (count(glob($file_url . ".*")) > 0) {
                        foreach (glob($file_url . ".*") as $filename) {
                            $file_delete = $filename;
                            $delete = true;
                            break;
                        }
                    } else {
                        if (!file_exists($upload_path)) {
                            mkdir($upload_path, 0777, true);
                        }
                    }

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

                    if ($data[DATA_ERROR][ERROR] == false) {
                        if (!$this->upload->do_upload($upload_id)) {
                            $return_value->error = true;
                            $return_value->file_name = $file_name;
                            $return_value->url_file = $upload_path . $file_name;
                            $data[DATA_ERROR][ERROR] = true;
                            $data[DATA_ERROR][ERROR_MESSAGE][] = $error_name . $this->upload->display_errors('', '');
                        } else {
                            if ($delete) {
                                
                            }
                        }

                        $img = $this->upload->data();
                        $logo_img = $img['file_name'];

                        if ($delete) {
                            if (file_exists($upload_path . $logo_img)) {
                                rename($upload_path . $logo_img, $upload_path . $file_name . "." . $ext);
                            }
                            $logo_img = $file_name . "." . $ext;
                        }

                        $return_value->error = false;
                        $return_value->file_name = $logo_img;
                        $return_value->url_file = $upload_path . $file_name;
                    }
                } else {
                    $return_value->error = false;
                    $return_value->file_name = $file_name;
                    $return_value->url_file = $upload_path . $file_name;
                }
            } else {
                $return_value->error = true;
                $return_value->file_name = $file_name;
                $return_value->url_file = $upload_path . $file_name;
            }
        } else {
            $return_value->error = true;
            $return_value->file_name = $file_name;
            $return_value->url_file = $upload_path . $file_name;
            if ($_FILES[$upload_id][TEMP_NAME]) {
                $data[DATA_ERROR][ERROR] = true;
                $data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_VALIDATION_UPLOAD_TYPE_DOESNT_MATCH;
            }
        }
        return $return_value;
    }

    protected function get_file_name($upload_id, $default_name = "") {
        return ($_FILES[$upload_id]["name"] != "") ? $_FILES[$upload_id]["name"] : $default_name;
    }

    protected function upload_product($number, $img, $upload_path, $file_name, &$data) {

        $dimension = (object) unserialize(IMAGE_DIMENSION_PRODUCT);
        $return_msg = "";

        list($width, $height) = getimagesize($_FILES['ifile' . $number]['tmp_name'][$img]);

        if ($_FILES['ifile' . $number]['size'][$img] > DEFAULT_MAX_LIMIT_IMAGE) {
            $data[DATA_ERROR][ERROR] = true;
            $data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_MAX_LIMIT_IMAGE;
        } elseif ($width > $dimension->max_width || $width < $dimension->min_width || $height > $dimension->max_height || $height < $dimension->min_height || $height != $width) {
            $data[DATA_ERROR][ERROR] = true;
            $data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_DIMENSION_IMAGE;
        } else {
            $idfile = $_FILES['ifile' . $number][TEMP_NAME][$img];
            move_uploaded_file($idfile, $upload_path . '/' . $file_name);
            $return_msg = $file_name;
        }
        return $return_msg;
    }

    protected function upload_new_variant_product($number, $img, $upload_path, $file_name, &$data) {

        $dimension = (object) unserialize(IMAGE_DIMENSION_PRODUCT);
        $return_msg = "";

        list($width, $height) = getimagesize($_FILES['new_ifile' . $number]['tmp_name'][$img]);

        if ($_FILES['new_ifile' . $number]['size'][$img] > DEFAULT_MAX_LIMIT_IMAGE) {
            $data[DATA_ERROR][ERROR] = true;
            $data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_MAX_LIMIT_IMAGE;
        } elseif ($width > $dimension->max_width || $width < $dimension->min_width || $height > $dimension->max_height || $height < $dimension->min_height) {
            $data[DATA_ERROR][ERROR] = true;
            $data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_DIMENSION_IMAGE;
        } else {
            $idfile = $_FILES['new_ifile' . $number][TEMP_NAME][$img];
            move_uploaded_file($idfile, $upload_path . '/' . $file_name);
            $return_msg = $file_name;
        }
        return $return_msg;
    }

    protected function set_img_size($source_path, $dest_path, $width, $height, &$data) {

        if (!file_exists($dest_path)) {
            mkdir($dest_path, 0777, true);
        }

        $config_manip = array(
            'image_library' => 'gd2',
            'source_image' => $source_path,
            'new_image' => $dest_path,
            'maintain_ratio' => FALSE,
            'width' => $width,
            'height' => $height
        );

        $this->load->library('image_lib', $config_manip);
        $this->image_lib->initialize($config_manip);

        if (!$this->image_lib->resize()) {
            $data[DATA_ERROR][ERROR] = true;
            $data[DATA_ERROR][ERROR_MESSAGE][] = $this->image_lib->display_errors('', '');
        }

        $this->image_lib->clear();
    }

    public function set_all_image($source_path, $dest_path, $file_name, &$data) {

        $xs_size = (object) unserialize(XS_DIMENSION);
        $s_size = (object) unserialize(S_DIMENSION);
        $m_size = (object) unserialize(M_DIMENSION);
        $l_size = (object) unserialize(L_DIMENSION);
        $xl_size = (object) unserialize(XL_DIMENSION);

        $this->set_img_size($source_path, $dest_path . XS_IMAGE_UPLOAD, $xs_size->width, $xs_size->height, $data);
        $this->set_img_size($source_path, $dest_path . S_IMAGE_UPLOAD, $s_size->width, $s_size->height, $data);
        $this->set_img_size($source_path, $dest_path . M_IMAGE_UPLOAD, $m_size->width, $m_size->height, $data);
        $this->set_img_size($source_path, $dest_path . L_IMAGE_UPLOAD, $l_size->width, $l_size->height, $data);
        $this->set_img_size($source_path, $dest_path . XL_IMAGE_UPLOAD, $xl_size->width, $xl_size->height, $data);
    }

    public function generate_random_text($length = 10, $is_capital_num = false) {

        $par1 = range('A', 'Z');
        $par2 = range('a', 'z');
        $par3 = range('1', '9');
        if (!$is_capital_num) {
            $final_array = array_merge($par1, $par2, $par3);
        } else {
            $final_array = array_merge($par1, $par3);
        }
        $password = '';
        while ($length--) {
            $key = array_rand($final_array);
            $password .= $final_array[$key];
        }
        sleep(1);
        return $password;
    }

    public function generate_random_alnum($length = 8, $is_capital_num = false) {
        $random = "";
        srand((double) microtime() * 1000000);
        if (!$is_capital_num) {
            $data = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $data .= "0123456789";
        } else {
            $data = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $data .= "0123456789";
        }

        for ($i = 0; $i < $length; $i++) {
            $random .= substr($data, (rand() % (strlen($data))), 1);
        }
        return $random;
    }

    public function generate_random_num($length = 20, $is_capital_num = false) {
        $random = "";
        srand((double) microtime() * 1000000);
        $data = "0123456789";

        for ($i = 0; $i < $length; $i++) {
            $random .= substr($data, (rand() % (strlen($data))), 1);
        }
        return $random;
    }

    public function generate_random_alphabet($length = 12, $is_capital_num = false) {
        $character1 = range('A', 'Z');
        $character2 = range('a', 'z');

        if (!$is_capital_num) {
            $final_array = array_merge($character2);
        } else {
            $final_array = array_merge($character1);
        }
        $alphabet = '';
        while ($length--) {
            $key = array_rand($final_array);
            $alphabet .= $final_array[$key];
        }
        return $alphabet;
    }

    protected function generate_member_refund($data) {
        /* sample $data
          $selected->user_id = parent::get_merchant_user_id();
          $selected->ip_address = parent::get_ip_address();
          $selected->member_seq = Member Seq;
          $selected->mutation_type = 'N';   // C atau N
          $selected->pg_method_seq = seq pembayaran;
          $selected->trxno = Referensi Nomor Transaksi;
          $selected->depositamount = nilai refund deposit;
          $selected->nondepositamount = nilai refund non deposit;
          parent::generate_member_refund($selected);
         */
        $this->load->model("component/M_voucher");
        $this->M_voucher->save_add_refund($data);
    }

    protected function generate_voucher_refund($data) {
        /* sample $data
          $selected->user_id = parent::get_merchant_user_id();
          $selected->ip_address = parent::get_ip_address();
          $selected->voucherseq = Old Voucher Seq;
          parent::generate_voucher_refund($selected);
         */
        $this->load->model("component/M_voucher");
        $selected = new stdClass();
        $selected->user_id = $data->user_id;
        $selected->ip_address = $data->ip_address;
        $selected->voucherseq = $data->voucherseq;
        $selected->newvoucher = REFUND_CODE . $this->generate_random_alnum(17, true); // create voucher baru;
        $newvoucherseq = $this->M_voucher->save_voucher_refund($selected);
        $selected->newvoucherseq = $newvoucherseq[0]->new_seq;
        return $selected;
    }

    protected function generate_voucher($data, $total_order = 0) {
        /* sample $data
         * $params->user_id = parent::get_member_user_id();
         * $params->ip_address = parent::get_ip_address();
         * $params->node_cd = NODE_REGISTRATION_MEMBER OR NODE_SHOPPING_MEMBER;
         * $params->member_seq = parent::get_member_user_seq();
         * parent::generate_voucher($params);
         */
        $this->load->model("component/M_voucher");
        $params = new stdClass();
        $params->user_id = $data->user_id;
        $params->ip_address = $data->ip_address;
        $params->node_cd = $data->node_cd; //* node_cd = REG OR BUY*//
        $params->total_order = $total_order;

        $promo = $this->M_voucher->get_promo($params);

        if (isset($promo)) {
            $promo_seq = $promo[0]->seq;
            $active_date = date('d-M-Y');
            $exp_days = $promo[0]->exp_days;
            $date = date('d-M-Y', strtotime('+' . $exp_days . 'days', strtotime($active_date)));

            $select = new stdClass();
            $select->seq = $promo_seq;
            $select->member_seq = $data->member_seq;
            $select->active_date = $active_date;
            $select->exp_date = $date;

            if ($promo[0]->type == VOUCHER_TYPE_AUTOMATIC) {
                $voucher = $this->generate_random_text(17, true);
                $select->code = NODE_REGISTRATION_MEMBER . $voucher;
                $this->M_voucher->save_add_voucher($select);
                return $voucher;
            } else {
                $this->M_voucher->save_update_voucher($select);
            }
        }
    }

    protected function email_template($data) {

//sample $data
//	$params = new stdClass;
//	$params->user_id = parent::get_member_user_id();
//	$params->ip_address = parent::get_ip_address();
//	$params->code = "ORDER_SENT";
//	$params->to_email = "alamat@emailtujuan.com";
//	$params->email_code = "kode email untuk log kalau ada";
//	$params->ORDER_NO = "[ORDER_NO]";
//	$params->RECIPIENT_NAME = "[RECIPIENT_NAME]";
//	$params->RESI_NO = "[RESI_NO]";
//	$params->ORDER_ITEMS = array("Baju", "Sepatu", "Handphone", "Tas", "Kacamata");
//	$params->RECIPIENT_DATE = "02-12-2015";
//	die(parent::email_template($params));

        $this->load->model("component/M_email");
        $params = new stdClass();
        $params->user_id = $data->user_id;
        $params->ip_address = $data->ip_address;
        $params->email_cd = $data->code;
        $replaced = '';
        $finded = '';
        $findpass = '';
        // get subject email template
        $template = $this->M_email->get_email_template($params);
        $string = $template[0]->subject;

        foreach ($data as $key => $val) {
            if (is_array($val)) {
                $finded = $key;
                foreach ($val as $key1 => $val1) {
                    $replaced .= $val1 . '<br>';
                }
                $string = str_ireplace("[" . strtoupper($finded) . "]", $replaced, $string);
                $replaced = '';
                $finded = '';
            } else {
                $string = str_ireplace("[" . strtoupper($key) . "]", $val, $string);
            }
        }
        $subject = $string;
        $replaced = '';
        $finded = '';
        // get content email template
        $string = $template[0]->content;
        foreach ($data as $key => $val) {
            if (is_array($val)) {
                $finded = $key;
                foreach ($val as $key1 => $val1) {
                    $replaced .= $val1 . '<br>';
                }
                $string = str_ireplace("[" . strtoupper($finded) . "]", $replaced, $string);
                $replaced = '';
                $finded = '';
            } else {
                if ("[" . strtoupper($key) . "]" == "[PASSWORD]") {
                    $findpass = $val;
                }
                $string = str_ireplace("[" . strtoupper($key) . "]", $val, $string);
            }
        }
        $content = $string;
        $to_email = $data->to_email;
        // get master email template layout
        $params->email_cd = "EMAIL_CONTENT";
        $template = $this->M_email->get_email_template($params);
        $content = str_ireplace("[EMAIL_CONTENT]", $content, $template[0]->content);
        // get setting email Website Name [WEB_TITLE]
        $template = $this->M_email->get_email_setting($params);
        $webtitle = $template[0]->value;
        $subject = str_ireplace("[WEB_TITLE]", $webtitle, $subject);
        $content = str_ireplace("[WEB_TITLE]", $webtitle, $content);
        // get setting email Website URL [VIEW_URL]
        if (isset($data->email_code)) {
            $params->email_code = $data->email_code;
        } else {
            $params->email_code = time() . $this->generate_random_text(10);
        }
        $template = $this->M_email->get_email_setting($params, 3);
        $content = str_ireplace("[VIEW_URL]", "<a href='" . base_url() . "email/" . $params->email_code . "'>disini</a>", $content);
        // get setting email Website Logo Color [LOGO_URL]
        $template = $this->M_email->get_email_setting($params, 4);
        $content = str_ireplace("[LOGO_URL]", "<img src='" . base_url() . ASSET_IMG_HOME . $template[0]->value . "' alt='" . $webtitle . "'>", $content);
        $content = str_ireplace("[BASE_URL]", base_url(), $content);
        // send email
        $params->name = $data->RECIPIENT_NAME;
        $params->email = $to_email;
        $params->subject = $subject;
        $params->contentsaved = $content;
        $stemail = $this->send_email($to_email, SMTP_FROM_EMAIL, $subject, $content, SMTP_FROM_NAME);
        // save email log
        $params->email_cd = $data->code;
        $params->stemail = $stemail;
        if ($findpass != "") {
            $content = str_ireplace($findpass, "[PASSWORD]", $content);
        }
        $params->contentsaved = $content;
        $this->M_email->save_email_log($params);
    }

    protected function send_email($to_email, $from_email, $subject, $content, $from_name = '') {
        $ci = get_instance();
        $ci->load->library('email');

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => SMTP_HOST,
            'smtp_port' => SMTP_PORT,
            'smtp_user' => SMTP_USERNAME,
            'smtp_pass' => SMTP_PASSWORD,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE,
            'newline' => "\r\n"
        );

        $ci->email->initialize($config);
        $ci->email->from($from_email, $from_name);
        $ci->email->to($to_email);
        $ci->email->subject($subject);
        $ci->email->message($content);
        if (!$ci->email->send()) {
//	    echo $ci->email->print_debugger();
            $data[DATA_ERROR][ERROR] = true;
            $data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_SEND_EMAIL;
            return '0';
        } else {
            return '1';
        }
    }

    protected function Unlink_file($result) {
        if (count(glob($result . ".*")) > 0) {
            foreach (glob($result . ".*") as $filename) {
                unlink(realpath($filename));
            }
        }
    }

    protected function cdate($result, $type = "") {
        switch ($type) {
            case 1: //date time
                if (!isset($result) || $result == "0000-00-00 00:00:00") {
                    $return = "";
                } else {
                    $return = date("d-M-Y H:i:s", strtotime($result));
                }
                break;
            default:
                if (!isset($result) || $result == "0000-00-00") {
                    $return = "";
                } else {
                    $return = date("d-M-Y", strtotime($result));
                }
        }
        return $return;
    }

    protected function cnum($result, $decimal = 0) {
        return number_format($result, $decimal);
    }

    protected function cstat($result) {
        return (($result == '1') ? '<i class="fa fa-check-square-o fa-lg"></i>' : '<i class="fa fa-square-o fa-lg"></i>');
    }

    protected function cdef($result) {
        return htmlspecialchars($result);
    }

    protected function cgend($result) {
        return $result == DEFAULT_MALE_GENDER ? '<i class="fa fa-mars fa-lg text-blue"></i>' : '<i class="fa fa-venus fa-lg text-red"></i>';
    }

    protected function cstdes($result, $def) {
        $data = json_decode($def, true);
        return ($data[$result]);
    }

    protected function clength($value, $length = MAX_LENGTH_DEFAULT, &$data = "") {
        if (strlen($value) > $length) {
            $data[DATA_ERROR][ERROR] = true;
            $data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_VALIDATION_LENGTH_MAX . " " . $length;
        }
        return $value;
    }

    function get_display_value($value) {
        if (isset($value) && $value != "") {
            $value = htmlspecialchars($value);
        }
        return $value;
    }

    function get_trim_text($input, $length, $ellipses = true, $strip_html = true) {
        if ($strip_html)
            $input = strip_tags($input);
        if (strlen($input) <= $length)
            return $input;
        $last_space = strrpos(substr($input, 0, $length), ' ');
        $trimmed_text = substr($input, 0, $last_space);
        if ($ellipses)
            $trimmed_text .= ' ... ';
        return $trimmed_text;
    }

    protected function is_member_get_axa($order_info) {
        if (!$this->is_agent()) {
            $this->load->model('member/M_order_member');
            $order_date = $order_info[0][0]->order_date;
            $total_order = $order_info[0][0]->total_order;
            $order_seq = $order_info[0][0]->seq;
            $status = $order_info[0][0]->payment_status;
            $member_seq = $order_info[0][0]->member_seq;

            $params = new stdClass();
            $params->user_id = $member_seq;
            $params->ip_address = $this->get_ip_address();
            $params->order_seq = $order_seq;
            $params->member_seq = $member_seq;
            $member_axa = $this->M_order_member->get_axa_member($params);
            if (count($member_axa) == 0 &&
                    $status == 'P' &&
                    $total_order >= MINIMUM_TOTAL_ORDER_AXA &&
                    $order_date >= MINIMUM_DATE_ORDER_AXA) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    protected function set_all_image_banner($source_path, $dest_path, $file_name, &$data) {

        $m_size = (object) unserialize(M_DIMENSION_BANNER);

        $this->set_img_size($source_path, $dest_path . M_IMAGE_UPLOAD, $m_size->width, $m_size->height, $data);
    }

    protected function is_agent() {
        $status = false;
        /*        if (parent::get_input_get(VIEW_MODE)) {
          $status = true;
          }
          if (isset($_COOKIE[VIEW_MODE])) {
          if ($_COOKIE[VIEW_MODE] == AGENT) {
          $status = true;
          }
          } */
        if (isset($_SESSION[SESSION_AGENT_CSRF_TOKEN])) {
            $status = true;
        }
        return $status;
    }

    protected function is_agent_view() {
        $status = false;
        /*        if (parent::get_input_get(VIEW_MODE)) {
          $status = true;
          } */
        if (isset($_COOKIE[VIEW_MODE])) {
            if ($_COOKIE[VIEW_MODE] == AGENT) {
                $status = true;
            }
        }
        return $status;
    }

    protected function get_customer_simulation_agent($params) {
        $this->load->model(LIVEVIEW . 'agent/M_agent_customer');
        return $this->M_agent_customer->get_simulation($params);
    }

    protected function get_customer_info($params) {
        $this->load->model(LIVEVIEW . 'agent/M_agent_customer');
        return $this->M_agent_customer->get_customer_data($params);
    }

    protected function create_table_customer_info($params) {
        $result = $this->get_customer_info($params);
        if ($result != NULL) {
            $table = '<div>';
            foreach ($result as $key => $each) {

                $table .= '<div class="col-xs-12 box-header-cart">';
                $table .= '<h4>Customer Data</h4>';
                $table .= '</div>';

                //DIV FOR UANG DOWN PAYMENT
                $table .= '<div class="col-xs-6 simulation-field odd no-pad">';
                $table .= '<div class="col-xs-5">';
                $table .= 'Nama Customer';
                $table .= '</div>';
                $table .= '<div class="col-xs-7 text-right">';
                $table .= (isset($each->pic_name) ? $each->pic_name : "-");
                $table .= '</div>';
                $table .= '</div>';

                //DIV FOR UANG DOWN PAYMENT
                $table .= '<div class="col-xs-6 simulation-field odd no-pad">';
                $table .= '<div class="col-xs-5">';
                $table .= 'No Indentitas';
                $table .= '</div>';
                $table .= '<div class="col-xs-7 text-right">';
                $table .= (isset($each->identity_no) ? $each->identity_no : "-");
                $table .= '</div>';
                $table .= '</div>';

                //DIV FOR UANG DOWN PAYMENT
                $table .= '<div class="col-xs-6 simulation-field no-pad">';
                $table .= '<div class="col-xs-5">';
                $table .= 'Email';
                $table .= '</div>';
                $table .= '<div class="col-xs-7 text-right">';
                $table .= (isset($each->email_address) ? $each->email_address : "-");
                $table .= '</div>';
                $table .= '</div>';

                //DIV FOR UANG DOWN PAYMENT
                $table .= '<div class="col-xs-6 simulation-field no-pad">';
                $table .= '<div class="col-xs-5">';
                $table .= 'Tanggal Lahir';
                $table .= '</div>';
                $table .= '<div class="col-xs-7 text-right">';
                $table .= (isset($each->birthday) ? $this->cdate($each->birthday) : "-");
                $table .= '</div>';
                $table .= '</div>';

                //DIV FOR UANG DOWN PAYMENT
                $table .= '<div class="col-xs-6 simulation-field no-pad odd">';
                $table .= '<div class="col-xs-5">';
                $table .= 'No Telephone ';
                $table .= '</div>';
                $table .= '<div class="col-xs-7 text-right">';
                $table .= (isset($each->phone_no) ? $each->phone_no : "-");
                $table .= '</div>';
                $table .= '</div>';

                //DIV FOR UANG DOWN PAYMENT
                $table .= '<div class="col-xs-6 simulation-field no-pad odd">';
                $table .= '<div class="col-xs-5">';
                $table .= 'Alamat Sesuai KTP';
                $table .= '</div>';
                $table .= '<div class="col-xs-7 text-right">';
                $table .=(isset($each->identity_address) ? $each->identity_address : "-");
                $table .= '</div>';
                $table .= '</div>';


                //DIV FOR UANG DOWN PAYMENT
                $table .= '<div class="col-xs-6 simulation-field no-pad">';
                $table .= '<div class="col-xs-5">';
                $table .= 'Provinsi';
                $table .= '</div>';
                $table .= '<div class="col-xs-7 text-right">';
                $table .=(isset($each->province_name) ? $each->province_name : "-");
                $table .= '</div>';
                $table .= '</div>';


                //DIV FOR UANG DOWN PAYMENT
                $table .= '<div class="col-xs-6 simulation-field no-pad">';
                $table .= '<div class="col-xs-5">';
                $table .= 'Kabupaten';
                $table .= '</div>';
                $table .= '<div class="col-xs-7 text-right">';
                $table .=(isset($each->city_name) ? $each->city_name : "-");
                $table .= '</div>';
                $table .= '</div>';


                //DIV FOR UANG DOWN PAYMENT
                $table .= '<div class="col-xs-6 simulation-field no-pad">';
                $table .= '<div class="col-xs-5">';
                $table .= 'Kecamatan';
                $table .= '</div>';
                $table .= '<div class="col-xs-7 text-right">';
                $table .= (isset($each->district_name) ? $each->district_name : "-");
                $table .= '</div>';
                $table .= '</div>';


                //DIV FOR UANG DOWN PAYMENT
                $table .= '<div class="col-xs-6 simulation-field no-pad">';
                $table .= '<div class="col-xs-5">';
                $table .= 'Kelurahan';
                $table .= '</div>';
                $table .= '<div class="col-xs-7 text-right">';
                $table .= (isset($each->sub_district) ? $each->sub_district : "");
                $table .= '</div>';
                $table .= '</div>';



                //DIV FOR UANG DOWN PAYMENT
                $table .= '<div class="col-xs-6 simulation-field no-pad">';
                $table .= '<div class="col-xs-5">';
                $table .= 'Kode Pos';
                $table .= '</div>';
                $table .= '<div class="col-xs-7 text-right">';
                $table .= (isset($each->zip_code) ? $each->zip_code : "");
                $table .= '</div>';
                $table .= '</div>';
            }
            $table .= '</div>';
        } else {
            $table = '';
        }
        return $table;
    }

    protected function create_table_customer_simulation_agent($params) {
        
        $result = $this->get_customer_simulation_agent($params);
        if ($result != NULL) {
            $table = '<div class="row m-bottom-20px">';
            foreach ($result as $key => $each) {

                $table .= '<div class="col-xs-6">'; // left side perhitungan angsuran
                $table .= '<div class="col-xs-12 box-header-cart">'; //header
                $table .= '<h4>Perhitungan Angsuran</h4>'; //header
                $table .= '</div>'; // end header
                $table .= '<div class="col-xs-12">';

                //DIV FOR HARGA BARANG
                $table .= '<div class="simulation-field no-pad">';
                $table .= '<div class="col-xs-5">';
                $table .= 'Harga Barang';
                $table .= '</div>';
                $table .= '<div class="col-xs-1">';
                $table .= 'Rp.';
                $table .= '</div>';
                $table .= '<div class="col-xs-6 text-right">';
                $table .= number_format($each->product_price);
                $table .= '</div>';
                $table .= '</div>';

                //DIV FOR UANG DOWN PAYMENT
                $table .= '<div class="simulation-field odd no-pad">';
                $table .= '<div class="col-xs-5">';
                $table .= 'Uang Muka (DP)';
                $table .= '</div>';
                $table .= '<div class="col-xs-1">';
                $table .= 'Rp.';
                $table .= '</div>';
                $table .= '<div class="col-xs-6 text-right">';
                $table .= number_format($each->dp);
                $table .= '</div>';
                $table .= '</div>';

                //DIV FOR LAON
                $table .= '<div class="simulation-field no-pad">';
                $table .= '<div class="col-xs-5">';
                $table .= 'Pokok Hutang';
                $table .= '</div>';
                $table .= '<div class="col-xs-1">';
                $table .= 'Rp.';
                $table .= '</div>';
                $table .= '<div class="col-xs-6 text-right">';
                $table .= number_format($each->total_loan);
                $table .= '</div>';
                $table .= '</div>';

                //DIV FOR LOAN INTEREST
                $table .= '<div class="simulation-field no-pad odd">';
                $table .= '<div class="col-xs-5">';
                $table .= 'Tenor';
                $table .= '</div>';
                $table .= '<div class="col-xs-1">';
                $table .= '';
                $table .= '</div>';
                $table .= '<div class="col-xs-6 text-right">';
                $table .= number_format($each->tenor) . " Bulan";
                $table .= '</div>';
                $table .= '</div>';


                //DIV FOR LOAN Bunga
//                $table .= '<div class="simulation-field no-pad">';
//                $table .= '<div class="col-xs-5">';
//                $table .= 'Bunga';
//                $table .= '</div>';
//                $table .= '<div class="col-xs-1">';
//                $table .= '';
//                $table .= '</div>';
//                $table .= '<div class="col-xs-6 text-right">';
//                $table .= number_format($each->loan_interest) . "%";
//                $table .= '</div>';
//                $table .= '</div>';
                //DIV FOR LOAN Anggsuran
                $table .= '<div class="simulation-field no-pad">';
                $table .= '<div class="col-xs-5">';
                $table .= 'Angsuran';
                $table .= '</div>';
                $table .= '<div class="col-xs-1">';
                $table .= 'Rp.';
                $table .= '</div>';
                $table .= '<div class="col-xs-6  text-right">';
                $table .= number_format($each->installment);
                $table .= '</div>';
                $table .= '</div>';
                $table .= '</div>';
                $table .= '</div>'; // end left side perhitungan angsuran





                $table .= '<div class="col-xs-6">'; // right side perhitungan angsuran
                $table .= '<div class="col-xs-12 box-header-cart">'; //header
                $table .= '<h4>Pembayaran Pertama</h4>'; //header
                $table .= '</div>'; // end header
                $table .= '<div class="col-xs-12">';

                //DIV FOR HARGA BARANG
                $table .= '<div class="simulation-field no-pad">';
                $table .= '<div class="col-xs-5">';
                $table .= 'Uang Muka (DP)';
                $table .= '</div>';
                $table .= '<div class="col-xs-1">';
                $table .= 'Rp.';
                $table .= '</div>';
                $table .= '<div class="col-xs-6 text-right">';
                $table .= number_format($each->dp);
                $table .= '</div>';
                $table .= '</div>';

                //DIV FOR HARGA BARANG
                $table .= '<div class="simulation-field no-pad odd">';
                $table .= '<div class="col-xs-5">';
                $table .= 'Angsuran I';
                $table .= '</div>';
                $table .= '<div class="col-xs-1">';
                $table .= 'Rp.';
                $table .= '</div>';
                $table .= '<div class="col-xs-6 text-right">';
                $table .= number_format($each->installment);
                $table .= '</div>';
                $table .= '</div>';


                //DIV FOR ADMINISTRASI
                $table .= '<div class="simulation-field no-pad">';
                $table .= '<div class="col-xs-5">';
                $table .= 'Administrasi';
                $table .= '</div>';
                $table .= '<div class="col-xs-1">';
                $table .= 'Rp.';
                $table .= '</div>';
                $table .= '<div class="col-xs-6 text-right">';
                $table .= number_format($each->admin_fee);
                $table .= '</div>';
                $table .= '</div>';


                //DIV FOR ONGKOS KIRIM
                $table .= '<div class="simulation-field no-pad odd">';
                $table .= '<div class="col-xs-5">';
                $table .= 'Ongkos Kirim';
                $table .= '</div>';
                $table .= '<div class="col-xs-1">';
                $table .= 'Rp.';
                $table .= '</div>';
                $table .= '<div class="col-xs-6 text-right">';
                $table .= number_format($each->total_ship_charged);
                $table .= '</div>';
                $table .= '</div>';

                //DIV FOR TOTAL PEMBAYARAN
                $table .= '<div class="simulation-field no-pad odd" style="visibility:hidden">';
                $table .= '<div class="col-xs-5">';
                $table .= '';
                $table .= '</div>';
                $table .= '<div class="col-xs-1">';
                $table .= '';
                $table .= '</div>';
                $table .= '<div class="col-xs-6 text-right">';
                $table .= '';
                $table .= '</div>';
                $table .= '</div>';


                $table .= '<div class="simulation-field no-pad odd">';
                $table .= '<div class="col-xs-5 font-20px text-bold">';
                $table .= 'Total Pembayaran I';
                $table .= '</div>';
                $table .= '<div class="col-xs-1">';
                $table .= 'Rp.';
                $table .= '</div>';
                $table .= '<div class="col-xs-6 text-right font-color-red font-20px text-bold">';
                $table .= number_format($each->total_installment);
                $table .= '</div>';
                $table .= '</div>';


                $table .= '</div>';
                $table .= '</div>'; // end left side perhitungan angsuran
//                $table .= '<tr>';
//                $table .= '<th colspan="2"><hr/>Perhitungan Angsuran</th>';
//                $table .= '</tr>';
//                $table .= '<tr>';
//                $table .= '<td>Harga Barang</td>';
//                $table .= '<td>Rp. <span class="pull-right text-bold">' . number_format($each->product_price) . '</span></td>';
//                $table .= '</tr>';
//                $table .= '<tr>';
//                $table .= '<td>Uang Muka (DP)</td>';
//                $table .= '<td>Rp. <span class="pull-right text-bold">' . number_format($each->dp) . '</span></td>';
//                $table .= '</tr>';
//                $table .= '<tr><td colspan="3"><span id="min_dp" class="text-red pull-right"></span></td></tr>';
//                $table .= '<tr>';
//                $table .= '<td>Pokok Hutang</td>';
//                $table .= '<td>Rp. <span class="pull-right text-bold">' . number_format($each->total_loan) . '</span></td>';
//                $table .= '</tr>';
//                $table .= '<tr>';
//                $table .= '<td>Tenor</td>';
//                $table .= '<td>' . $each->tenor . ' bulan</td>';
//                $table .= '</tr>';
//                $table .= '<tr>';
//                $table .= '<td>Bunga</td>';
//                $table .= '<td class="pull-right text-bold">' . number_format($each->loan_interest) . ' %</td>';
//                $table .= '</tr>';
//                $table .= '<tr>';
//                $table .= '<td>Angsuran</td>';
//                $table .= '<td>Rp. <span class="pull-right text-bold">' . number_format($each->installment) . '</span></td>';
//                $table .= '</tr>';
//                $table .= '<tr>';
//
//
//
//
//
//                $table .= '<th colspan="3"><hr/>Pembayaran Pertama</th>';
//                $table .= '</tr>';
//                $table .= '<tr>';
//                $table .= '<td>Uang Muka (DP)</td>';
//                $table .= '<td>Rp. <span class="pull-right text-bold">' . number_format($each->dp) . '</span></td>';
//                $table .= '</tr>';
//                $table .= '<tr>';
//                $table .= '<td>Angsuran I</td>';
//                $table .= '<td>Rp. <span class="pull-right text-bold">' . number_format($each->installment) . '</span></td>';
//                $table .= '</tr>';
//                $table .= '<tr>';
//                $table .= '<td>Administrasi</td>';
//                $table .= '<td>Rp. <span class="pull-right text-bold">' . number_format($each->admin_fee) . '</span></td>';
//                $table .= '</tr>';
//                $table .= '<tr>';
//                $table .= '<td>Ongkos Kirim</td>';
//                $table .= '<td>Rp. <span class="pull-right text-bold">' . number_format($each->total_ship_charged) . '</span></td>';
//                $table .= '</tr>';
//                $table .= '<tr>';
//                $table .= '<td>Total Pembayaran Pertama </td>';
//                $table .= '<td>Rp. <span id="total_pertama" class="pull-right text-bold">' . number_format($each->total_installment) . '</span></td>';
//                $table .= '</tr>';
            }
            $table .= '</div>';
        } else {
            $table = '';
        }
        return $table;
    }

}

?>
