<?php

require_once APPPATH . 'system/base.php';
require_once APPPATH . 'system/controller_base.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_controller_base
 *
 * @author Jartono
 */
class controller_base_merchant extends ControllerBase {

    public function __construct($form_cd = "", $form_url = "", $check_logged_in = true) {
	return parent::__construct($form_cd, $form_url, $check_logged_in, "merchant/login", MERCHANT_LOGIN);
    }

    protected function get_merchant_user_id() {
	if (isset($_SESSION[SESSION_MERCHANT_UID])) {
	    return $this->session->userdata(SESSION_MERCHANT_UID);
	} else {
	    return "";
	}
    }

    protected function get_merchant_email() {
	if (isset($_SESSION[SESSION_MERCHANT_EMAIL])) {
	    return $this->session->userdata(SESSION_MERCHANT_EMAIL);
	} else {
	    return "";
	}
    }

    protected function get_merchant_user_seq() {
	if (isset($_SESSION[SESSION_MERCHANT_SEQ])) {
	    return $this->session->userdata(SESSION_MERCHANT_SEQ);
	} else {
	    return "";
	}
    }

    protected function get_merchant_user_name() {
	if (isset($_SESSION[SESSION_MERCHANT_UNAME])) {
	    return $this->session->userdata(SESSION_MERCHANT_UNAME);
	} else {
	    return "";
	}
    }

    protected function create_barcode($text = '', $height = 50, $widthScale = 1) {
	if ($text != '') {
	    $code39 = array('0' => 'bwbwwwbbbwbbbwbw', '1' => 'bbbwbwwwbwbwbbbw',
		'2' => 'bwbbbwwwbwbwbbbw', '3' => 'bbbwbbbwwwbwbwbw',
		'4' => 'bwbwwwbbbwbwbbbw', '5' => 'bbbwbwwwbbbwbwbw',
		'6' => 'bwbbbwwwbbbwbwbw', '7' => 'bwbwwwbwbbbwbbbw',
		'8' => 'bbbwbwwwbwbbbwbw', '9' => 'bwbbbwwwbwbbbwbw',
		'A' => 'bbbwbwbwwwbwbbbw', 'B' => 'bwbbbwbwwwbwbbbw',
		'C' => 'bbbwbbbwbwwwbwbw', 'D' => 'bwbwbbbwwwbwbbbw',
		'E' => 'bbbwbwbbbwwwbwbw', 'F' => 'bwbbbwbbbwwwbwbw',
		'G' => 'bwbwbwwwbbbwbbbw', 'H' => 'bbbwbwbwwwbbbwbw',
		'I' => 'bwbbbwbwwwbbbwbw', 'J' => 'bwbwbbbwwwbbbwbw',
		'K' => 'bbbwbwbwbwwwbbbw', 'L' => 'bwbbbwbwbwwwbbbw',
		'M' => 'bbbwbbbwbwbwwwbw', 'N' => 'bwbwbbbwbwwwbbbw',
		'O' => 'bbbwbwbbbwbwwwbw', 'P' => 'bwbbbwbbbwbwwwbw',
		'Q' => 'bwbwbwbbbwwwbbbw', 'R' => 'bbbwbwbwbbbwwwbw',
		'S' => 'bwbbbwbwbbbwwwbw', 'T' => 'bwbwbbbwbbbwwwbw',
		'U' => 'bbbwwwbwbwbwbbbw', 'V' => 'bwwwbbbwbwbwbbbw',
		'W' => 'bbbwwwbbbwbwbwbw', 'X' => 'bwwwbwbbbwbwbbbw',
		'Y' => 'bbbwwwbwbbbwbwbw', 'Z' => 'bwwwbbbwbbbwbwbw',
		'-' => 'bwwwbwbwbbbwbbbw', '.' => 'bbbwwwbwbwbbbwbw',
		' ' => 'bwwwbbbwbwbbbwbw', '*' => 'bwwwbwbbbwbbbwbw',
		'$' => 'bwwwbwwwbwwwbwbw', '/' => 'bwwwbwwwbwbwwwbw',
		'+' => 'bwwwbwbwwwbwwwbw', '%' => 'bwbwwwbwwwbwwwbw');

	    if (!preg_match('/^[A-Z0-9-. $+\/%]+$/i', $text)) {
		throw new Exception('Invalid text input.');
	    }
	    $text = '*' . strtoupper($text) . '*'; // *UPPERCASE TEXT*
	    $length = strlen($text);
	    $barcode = imageCreate($length * 16 * $widthScale, $height);
	    $bg = imagecolorallocate($barcode, 255, 255, 0); //sets background to yellow
	    imagecolortransparent($barcode, $bg); //makes that yellow transparent
	    $black = imagecolorallocate($barcode, 0, 0, 0); //defines a color for black
	    $chars = str_split($text);
	    $colors = '';
	    foreach ($chars as $char) {
		$colors .= $code39[$char];
	    }
	    foreach (str_split($colors) as $i => $color) {
		if ($color == 'b') {
// imageLine($barcode, $i, 0, $i, $height-13, $black);
		    imageFilledRectangle($barcode, $widthScale * $i, 0, $widthScale * ($i + 1) - 1, $height - 13, $black);
		}
	    }

//16px per bar-set, halved, minus 6px per char, halved (5*length)
// $textcenter = $length * 5 * $widthScale;
	    $textcenter = ($length * 8 * $widthScale) - ($length * 3);
	    imageString($barcode, 2, $textcenter, $height - 13, $text, $black);
	    ob_start();
	    imagepng($barcode);
	    $imagedata = ob_get_contents();
	    imagedestroy($barcode);
	    ob_end_clean();
	    return $imagedata;
	}
    }

}
?>

