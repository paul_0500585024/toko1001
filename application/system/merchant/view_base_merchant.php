<?php

require_once APPPATH . 'system/view_base.php';

/*
 * URL
 */
function get_css_url() {
    return get_base_url() . "assets/css/";
}

function get_js_url() {
    return get_base_url() . "assets/js/";
}

function get_img_url() {
    return CDN_IMAGE . "assets/img/";
}

function get_content_url() {
    return VIEWPATH . "merchant/content/";
}

function get_component_url() {
    return VIEWPATH . "merchant/component/";
}

function get_include_content_merchant_header() {
    return get_content_url() . "header.php";
}

function get_include_content_merchant_top_navigation() {
    return get_content_url() . "top_nav.php";
}

function get_include_content_merchant_left_navigation() {
    return get_content_url() . "left_nav.php";
}

function get_include_content_merchant_right_navigation() {
    return get_content_url() . "right_nav.php";
}

function get_include_content_merchant_footer() {
    return get_content_url() . "footer.php";
}

function get_include_content_merchant_top_page_navigation() {
    return get_content_url() . "top_page.php";
}

function get_include_content_merchant_bottom_page_navigation() {
    return get_content_url() . "bottom_page.php";
}

function get_include_page_list_merchant_content_header() {
    return get_content_url() . "page_list_header.php";
}

function get_include_page_list_merchant_content_footer() {
    return get_content_url() . "page_list_footer.php";
}

function status($result, $def) {
    $data = json_decode($def, true);
    return ($data[$result]);
}

function c_date($result, $type = "") {
    switch ($type) {
	case 1: //date time
	    if (!isset($result) || $result == "0000-00-00 00:00:00") {
		$return = "";
	    } else {
		$return = date("d-M-Y H:i:s", strtotime($result));
	    }
	    break;
	default:
	    if (!isset($result) || $result == "0000-00-00") {
		$return = "";
	    } else {
		$return = date("d-M-Y", strtotime($result));
	    }
    }
    return $return;
}

function combostatus($datas, $id = "") {
    $return = '';
    foreach ($datas as $option => $val) {
	$return.="<option value='" . $option . "'" . ($option == $id ? ' selected' : '') . ">" . $val . "</option>";
    }
    return $return;
}

?>