<?php

require_once APPPATH . 'system/view_base.php';

/*
 * URL
 */

function get_css_url() {
    return get_base_url() . "assets/css/";
}

function get_js_url() {
    return get_base_url() . "assets/js/";
}

function get_img_url() {
    return CDN_IMAGE . "assets/img/";
}

function get_content_url() {
    return VIEWPATH . "partner/content/";
}

function get_component_url() {
    return VIEWPATH . "partner/component/";
}

function get_include_content_partner_header() {
    return get_content_url() . "header.php";
}

function get_include_content_partner_top_navigation() {
    return get_content_url() . "top_nav.php";
}

function get_include_content_partner_left_navigation() {
    return get_content_url() . "left_nav.php";
}

function get_include_content_partner_right_navigation() {
    return get_content_url() . "right_nav.php";
}

function get_include_content_partner_footer() {
    return get_content_url() . "footer.php";
}

function get_include_content_partner_js() {
    return get_content_url() . 'javascript_file.php';
}

function get_include_content_partner_top_page_navigation() {
    return get_content_url() . "top_page.php";
}

function get_include_content_partner_bottom_page_navigation() {
    return get_content_url() . "bottom_page.php";
}

function get_include_page_list_partner_content_header() {
    return get_content_url() . "page_list_header.php";
}

function get_include_page_list_partner_content_footer() {
    return get_content_url() . "page_list_footer.php";
}

function status($result, $def) {
    $data = json_decode($def, true);
    return ($data[$result]);
}

function c_date($result, $type = "") {
    switch ($type) {
        case 1: //date time
            if (!isset($result) || $result == "0000-00-00 00:00:00") {
                $return = "";
            } else {
                $return = date("d-M-Y H:i:s", strtotime($result));
            }
            break;
        default:
            if (!isset($result) || $result == "0000-00-00") {
                $return = "";
            } else {
                $return = date("d-M-Y", strtotime($result));
            }
    }
    return $return;
}

function get_variant_value($variant_seq, $variant_value, $separator = "") {
    $retval = "";
    switch ($variant_seq) {
        case "1": //all product
            $retval = "";
            break;
        default:
            $retval = $separator . $variant_value;
    }
    return $retval;
}

function combostatus($datas, $id = "") {
    $return = '';
    foreach ($datas as $option => $val) {
        $return.="<option value='" . $option . "'" . ($option == $id ? ' selected' : '') . ">" . $val . "</option>";
    }
    return $return;
}

function cnum($result, $decimal = 0) {
    return number_format($result, $decimal);
}

function get_search_filter() {
    return '<button type="button" class="btn btn-default" data-toggle="collapse" data-target="#filter-panel" title = "Pencarian">
    		<span class="fa fa-search"></span>
    	    </button>';
}

function cstdes($result, $def) {
    $data = json_decode($def, true);
    return ($data[$result]);
}

function product_partner($data = null, $new_product = false) {
    $combomaxby = '';
    for ($i = 9; $i > 0; $i--) {
        $combomaxby.='<option value=' . $i . ' ' . (!empty($data) && $data->max_buy == $i ? "selected" : "") . '>' . $i . '</option>';
    }
    $fromvariant = '<div id="' . (!empty($data) ? $data->variant_value_seq : "{varval}" ) . '" class="well box-variant">
                        <h4>
                        <div class="row">
                        <div class="col-xs-12">
                        <div class="variant-tittle">' .
            (!empty($data) ? $data->value : "{vartext}" ) .
            '</div><div id="titlevarian">' . (((empty($data->seq) || $data->seq == 0) && !$new_product) ? "<button type=\"button\" class = \"pull-right btn btn-danger btn-flat variant-del-btn\" onclick=\"remove(" . (!empty($data) ? $data->variant_value_seq : "{varval}" ) . ")\"> Hapus </button>" : "") . '
                        </div>
                        </div>
                        <input name="value[]" id="value" value="' . (!empty($data) ? $data->value : "{vartext}" ) . '" type="hidden">
                        <input name="variant_seq[]" id="variant_seq" value="' . (!empty($data->seq) ? $data->seq : 0 ) . '" type="hidden">
                        <input type="hidden" name="variant_value_seq[]" value="' . (!empty($data) ? $data->variant_value_seq : "{varval}") . '">
                     </div>
               </h4>
               <div class="row">
                  <div class="col-xs-2">
                    <div class ="form-group">
                        <label class ="col-xs-12">Harga Produk</label>
                        <div class ="col-xs-12">
                            <input class="form-control auto_int aRight" value="' . (!empty($data) ? $data->product_price : 0 ) . '" id="product_price' . (!empty($data) ? $data->dtimg : "{dtimg}") . '"  name="product_price[]" type="text" onchange="prodpc(this,' . (!empty($data) ? $data->dtimg : "{dtimg}") . ')" maxlength="50">
                        </div>
                    </div>
                  </div>
                  
<div class="col-xs-2">
                   <div class ="form-group">
                        <label class ="col-xs-12">Harga Promo</label>
                        <div class ="col-xs-12">
                            <input class="form-control auto_int aRight" value="' . (!empty($data) ? $data->sell_price : 0) . '" id="sell_price' . (!empty($data) ? $data->dtimg : "{dtimg}") . '" name="sell_price[]" type="text" onchange="salepc(this,' . (!empty($data) ? $data->dtimg : "{dtimg}") . ')" maxlength="50">
                        </div>
                   </div>
                  </div>
                  <div class="col-xs-2">
                    <div class ="form-group">
                        <label class ="col-xs-12">Diskon %</label>
                        <div class ="col-xs-12">
                            <input class="form-control aRight" value="' . (!empty($data) ? $data->disc_percent : 0) . '" id="disc_percent' . (!empty($data) ? $data->dtimg : "{dtimg}") . '" name="disc_percent[]" type="text" readonly>
                        </div>
                    </div>
                  </div>
                <div class="col-xs-2">
                    <div class ="form-group">
                        <label class ="col-xs-12">Max. Pembelian</label>
                        <div class ="col-xs-12">
                            <select class="form-control" name="max_buy[]" id="max_buy">' . $combomaxby . '</select>
                        </div>
                    </div>
                </div>';

    if ($new_product) {

        $fromvariant .= '<div class="col-xs-2">
                    <div class ="form-group">
                        <label class ="col-xs-12">SKU</label>
                        <div class ="col-xs-12">
                            <input class="form-control aLeft" value="' . (!empty($data) && isset($data->partner_sku) ? $data->partner_sku : '') . '" name="partner_sku[]" type="text">
                        </div>
                    </div>
                  </div>
                <div class="col-xs-2">
                    <div class ="form-group">
                        <label class ="col-xs-12">Stock</label>
                        <div class ="col-xs-12">
                            <input class="form-control auto_int aRight" value="' . (!empty($data) && isset($data->stock) ? $data->stock : '') . '" name="stock[]" type="text">
                        </div>
                    </div>
                  </div>';
    }




    $fromvariant.='</div>
            <div class = "row">
            <div class = "col-xs-2">
            <div class = "slim height-img" data-ratio = "1:1" data-min-size = "850,850" data-max-file-size = "2" data-jpeg-compression = "60" number = "1" image = "' . (!empty($data) ? $data->variant_value_seq : 0) . '">
            <input test type = "file" accept = "' . IMAGE_TYPE_UPLOAD . '" name = "ifile1[]" ><br>
            <img ' . ((!empty($data) && is_file(PRODUCT_UPLOAD_IMAGE . $data->partner_seq . '/' . $data->pic_1_img)) ?
                    ' src = "' . get_base_url() . PRODUCT_UPLOAD_IMAGE . $data->partner_seq . '/' . $data->pic_1_img . '"' :
                    ((!empty($data) && is_file(TEMP_PRODUCT_UPLOAD_IMAGE . $data->partner_seq . '/' . $data->pic_1_img)) ?
                            ' src = "' . get_base_url() . TEMP_PRODUCT_UPLOAD_IMAGE . $data->partner_seq . '/' . $data->pic_1_img . '"' : "")) .
            ' alt = "Gambar Utama">
            </div>
            <center>Gambar Utama</center>
            </div>
            <div class = "col-xs-2">
            <div class = "slim height-img" data-ratio = "1:1" data-min-size = "850,850" data-max-file-size = "2" number = "2" image = "' . (!empty($data) ? $data->variant_value_seq : 0) . '">
            <input type = "file" accept = "' . IMAGE_TYPE_UPLOAD . '" name = "ifile2[]" ><br>
            <img ' . ((!empty($data) && is_file(PRODUCT_UPLOAD_IMAGE . $data->partner_seq . '/' . $data->pic_2_img)) ?
                    ' src = "' . get_base_url() . PRODUCT_UPLOAD_IMAGE . $data->partner_seq . '/' . $data->pic_2_img . '"' :
                    ((!empty($data) && is_file(TEMP_PRODUCT_UPLOAD_IMAGE . $data->partner_seq . '/' . $data->pic_2_img)) ?
                            ' src = "' . get_base_url() . TEMP_PRODUCT_UPLOAD_IMAGE . $data->partner_seq . '/' . $data->pic_2_img . '"' : "")) .
            ' alt = "Gambar 2">
            </div>
            <center>Gambar 2</center>
            </div>
            <div class = "col-xs-2">
            <div class = "slim height-img" data-ratio = "1:1" data-min-size = "850,850" data-max-file-size = "2" number = "3" image = "' . (!empty($data) ? $data->variant_value_seq : 0) . '">
            <input type = "file" accept = "' . IMAGE_TYPE_UPLOAD . '" name = "ifile3[]" ><br>
            <img ' . ((!empty($data) && is_file(PRODUCT_UPLOAD_IMAGE . $data->partner_seq . '/' . $data->pic_3_img)) ?
                    ' src = "' . get_base_url() . PRODUCT_UPLOAD_IMAGE . $data->partner_seq . '/' . $data->pic_3_img . '"' :
                    ((!empty($data) && is_file(TEMP_PRODUCT_UPLOAD_IMAGE . $data->partner_seq . '/' . $data->pic_3_img)) ?
                            ' src = "' . get_base_url() . TEMP_PRODUCT_UPLOAD_IMAGE . $data->partner_seq . '/' . $data->pic_3_img . '"' : "")) .
            ' alt = "Gambar 3" />
            </div>
            <center>Gambar 3</center>
            </div>
            <div class = "col-xs-2">
            <div class = "slim height-img" data-ratio = "1:1" data-min-size = "850,850" data-max-file-size = "2" number = "4" image = "' . (!empty($data) ? $data->variant_value_seq : 0) . '">
            <input type = "file" accept = "' . IMAGE_TYPE_UPLOAD . '" name = "ifile4[]" ><br>
            <img ' . ((!empty($data) && is_file(PRODUCT_UPLOAD_IMAGE . $data->partner_seq . '/' . $data->pic_4_img)) ?
                    ' src = "' . get_base_url() . PRODUCT_UPLOAD_IMAGE . $data->partner_seq . '/' . $data->pic_4_img . '"' :
                    ((!empty($data) && is_file(TEMP_PRODUCT_UPLOAD_IMAGE . $data->partner_seq . '/' . $data->pic_4_img)) ?
                            ' src = "' . get_base_url() . TEMP_PRODUCT_UPLOAD_IMAGE . $data->partner_seq . '/' . $data->pic_4_img . '"' : "")) .
            ' alt = "Gambar 4" />
            </div>
            <center>Gambar 4</center>
            </div>
            <div class = "col-xs-2">
            <div class = "slim height-img" data-ratio = "1:1" data-min-size = "850,850" data-max-file-size = "2" number = "5" image = "' . (!empty($data) ? $data->variant_value_seq : 0) . '">
            <input type = "file" accept = "' . IMAGE_TYPE_UPLOAD . '" name = "ifile5[]" ><br>
            <img ' . ((!empty($data) && is_file(PRODUCT_UPLOAD_IMAGE . $data->partner_seq . '/' . $data->pic_5_img)) ?
                    ' src = "' . get_base_url() . PRODUCT_UPLOAD_IMAGE . $data->partner_seq . '/' . $data->pic_5_img . '"' :
                    ((!empty($data) && is_file(TEMP_PRODUCT_UPLOAD_IMAGE . $data->partner_seq . '/' . $data->pic_5_img)) ?
                            ' src = "' . get_base_url() . TEMP_PRODUCT_UPLOAD_IMAGE . $data->partner_seq . '/' . $data->pic_5_img . '"' . "test" : "")) .
            ' alt = "Gambar 5" />
            </div>
            <center>Gambar 5</center>
            </div>
            <div class = "col-xs-2">
            <div class = "slim height-img" data-ratio = "1:1" data-min-size = "850,850" data-max-file-size = "2" number = "6" image = "' . (!empty($data) ? $data->variant_value_seq : 0) . '">
            <input type = "file" accept = "' . IMAGE_TYPE_UPLOAD . '" name = "ifile6[]" ><br>
            <img ' . ((!empty($data) && is_file(PRODUCT_UPLOAD_IMAGE . $data->partner_seq . '/' . $data->pic_6_img)) ?
                    ' src = "' . get_base_url() . PRODUCT_UPLOAD_IMAGE . $data->partner_seq . '/' . $data->pic_6_img . '"' :
                    ((!empty($data) && is_file(TEMP_PRODUCT_UPLOAD_IMAGE . $data->partner_seq . '/' . $data->pic_6_img)) ?
                            ' src = "' . get_base_url() . TEMP_PRODUCT_UPLOAD_IMAGE . $data->partner_seq . '/' . $data->pic_6_img . '"' : "")) .
            ' alt = "Gambar 6" />
            </div>
            <center>Gambar 6</center>
            </div>
            </div>
            </div>';

    if (!empty($data)) {
        $fromvariant .='<input type = "hidden" value = "' . $data->pic_1_img . '" name = oldfile1[] id = "oldfile1_' . $data->variant_value_seq . '">
            <input type = "hidden" value = "' . $data->pic_2_img . '" name = oldfile2[] id = "oldfile2_' . $data->variant_value_seq . '">
            <input type = "hidden" value = "' . $data->pic_3_img . '" name = oldfile3[] id = "oldfile3_' . $data->variant_value_seq . '">
            <input type = "hidden" value = "' . $data->pic_4_img . '" name = oldfile4[] id = "oldfile4_' . $data->variant_value_seq . '">
            <input type = "hidden" value = "' . $data->pic_5_img . '" name = oldfile5[] id = "oldfile5_' . $data->variant_value_seq . '">
            <input type = "hidden" value = "' . $data->pic_6_img . '" name = oldfile6[] id = "oldfile6_' . $data->variant_value_seq . '">                                    ';
    }

    return $fromvariant;
}

function get_order_filter($date_field = "", $search_field = "", $data_filter = "", $sort, $column_sort) {
    $order_filter = '';
    switch ($column_sort) {
        case $date_field : {
                switch ($sort) {
                    case "asc" : $string = "Terlama ke terbaru";
                        break;
                    case "desc" : $string = "Terbaru ke terlama";
                        break;
                    default : $string = "Urutkan";
                }
            }break;
        case $search_field : {
                switch ($sort) {
                    case "asc" : $string = "<i><span class='fa fa-sort-alpha-asc'></i>&nbsp;&nbsp;&nbsp;Alphabet";
                        break;
                    case "desc" : $string = "<i><span class='fa fa-sort-alpha-desc'></i>&nbsp;&nbsp;&nbsp;Alphabet";
                        break;
                    default : $string = "Urutkan";
                }
            }break;
        default : $string = "Urutkan";
    }

    $order_filter = "<div class = 'col-xs-6 no-pad'>
            <button class = 'btn dropdown-toggle btn-default btn-block pull-right' data-toggle = 'dropdown'>" . $string . "</button>
            <ul class = 'dropdown-menu btn-normal full-width'>";
    if ($date_field !== "") {
        $order_filter .=
                "<li><a href = '" . $data_filter . "&sort=desc&column_sort=" . $date_field . "'>Terbaru ke terlama</a></li>
            <li><a href = '" . $data_filter . "&sort=asc&column_sort=" . $date_field . "'>Terlama ke terbaru</a></li>";
    }
    if ($search_field !== "") {
        $order_filter .=
                "<li><a href = '" . $data_filter . "&sort=asc&column_sort=" . $search_field . "'><i><span class='fa fa-sort-alpha-asc'></i>&nbsp;&nbsp;&nbsp;Alphabet</a></li>
            <li><a href = '" . $data_filter . "&sort=desc&column_sort=" . $search_field . "'><i><span class='fa fa-sort-alpha-desc'></i>&nbsp;&nbsp;&nbsp;Alphabet</a></li>";
    }
    $order_filter .= "</ul></div>";

    return $order_filter;
}

function get_include_page_list_agent_new_content_header() {
    return get_content_url() . "page_list_header.php";
}

function cdate($result, $type = "") {
    switch ($type) {
        case 1: //date time
            if (!isset($result) || $result == "0000-00-00 00:00:00") {
                $return = "";
            } else {
                $return = date("d-M-Y H:i:s", strtotime($result));
            }
            break;
        default:
            if (!isset($result) || $result == "0000-00-00") {
                $return = "";
            } else {
                $return = date("d-M-Y", strtotime($result));
            }
    }
    return $return;
}

//, $gender = "M", $man_image="avatar_male.jpg", $female_image="avatar_female.jpg"

/* $assetspath = realpath("assets");
  $assetspath = $assetspath . "\img\agent";
  if (isset($images) && $images != "") {
  $img_status = file_exists($assetspath . $images);
  if($img_status){
  return "ada";
  }else{
  return "gak ada";
  }
  } */


function get_image_exist($real_img_name, $gender = "M", $img_folder_name) {
    $image_name = (isset($real_img_name) && $real_img_name != '' ? $real_img_name : '');
    if ($image_name == '') {
        $image_name = ($gender == 'M' ? 'avatar_male.jpg' : 'avatar_female.jpg');
    }
    return get_img_url() . 'agent/' . $image_name;
}

?>