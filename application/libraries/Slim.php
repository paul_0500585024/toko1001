<?php

abstract class SlimStatus {

    const Failure = 'failure';
    const Success = 'success';

}

class Slim {

    private $ci;
    private static $ftp_server;
    private static $ftp_user;
    private static $ftp_password;
    private static $cdn_image;
    private static $file_server;

    public function __construct() {
        $this->ci = & get_instance();
        self::$ftp_server = $this->ci->config->item('ftp_server');
        self::$ftp_user = $this->ci->config->item('ftp_user_name');
        self::$ftp_password = $this->ci->config->item('ftp_password');
        self::$cdn_image = $this->ci->config->item('cdn_image');
        self::$file_server = $this->ci->config->item('file_server');
    }

    public static function getImages($inputName = 'slim') {

        $values = Slim::getPostData($inputName);

        // test for errors
        if ($values === false) {
            return false;
        }

        // determine if contains multiple input values, if is singular, put in array
        $data = array();
        if (!is_array($values)) {
            $values = array($values);
        }

//        var_dump($values);die();
        // handle all posted fields
        foreach ($values as $value) {
            $inputValue = Slim::parseInput($value);
            if ($inputValue) {
                array_push($data, $inputValue);
            } else {
                array_push($data, array());
            }
        }

        // return the data collected from the fields
        return $data;
    }

    // $value should be in JSON format
    private static function parseInput($value) {

        // if no json received, exit, don't handle empty input values.
        if (empty($value)) {
            return null;
        }

        // The data is posted as a JSON String so to be used it needs to be deserialized first
        $data = json_decode($value);

        // shortcut
        $input = null;
        $actions = null;
        $output = null;
        $meta = null;

        if (isset($data->input)) {
            $inputData = isset($data->input->image) ? Slim::getBase64Data($data->input->image) : null;
            $input = array(
                'data' => $inputData,
                'name' => $data->input->name,
                'type' => $data->input->type,
                'size' => $data->input->size,
                'width' => $data->input->width,
                'height' => $data->input->height,
            );
        }

        if (isset($data->output)) {
            $outputData = isset($data->output->image) ? Slim::getBase64Data($data->output->image) : null;
            $output = array(
                'data' => $outputData,
                'width' => $data->output->width,
                'height' => $data->output->height
            );
        }

        if (isset($data->actions)) {
            $actions = array(
                'crop' => $data->actions->crop ? array(
                    'x' => $data->actions->crop->x,
                    'y' => $data->actions->crop->y,
                    'width' => $data->actions->crop->width,
                    'height' => $data->actions->crop->height,
                    'type' => $data->actions->crop->type
                        ) : null,
                'size' => $data->actions->size ? array(
                    'width' => $data->actions->size->width,
                    'height' => $data->actions->size->height
                        ) : null
            );
        }

        if (isset($data->meta)) {
            $meta = $data->meta;
        }

        // We've sanitized the base64data and will now return the clean file object
        return array(
            'input' => $input,
            'output' => $output,
            'actions' => $actions,
            'meta' => $meta
        );
    }

    // $path should have trailing slash
    public static function saveFile($data, $name, $path = 'tmp/', $uid = true) {

        // Add trailing slash if omitted
        if (substr($path, -1) !== '/') {
            $path .= '/';
        }

        // Test if directory already exists
//        if (!is_dir($path)) {
//            mkdir($path, 0755);
//        }
        // Let's put a unique id in front of the filename so we don't accidentally overwrite older files
        if ($uid) {
            $name = uniqid() . '_' . $name;
        }
        $path = $path . $name;

        // store the file
        Slim::save($data, $path);

        // return the files new name and location
        return array(
            'name' => $name,
            'path' => $path
        );
    }

    public static function outputJSON($status, $fileName = null, $filePath = null) {

        header('Content-Type: application/json');

        if ($status !== SlimStatus::Success) {
            echo json_encode(array('status' => $status));
            return;
        }

        echo json_encode(
                array(
                    'status' => $status,
                    'name' => $fileName,
                    'path' => $filePath
                )
        );
    }

    /**
     * Gets the posted data from the POST or FILES object. If was using Slim to upload it will be in POST (as posted with hidden field) if not enhanced with Slim it'll be in FILES.
     * @param $inputName
     * @return array|bool
     */
    private static function getPostData($inputName) {

        $values = array();

        if (isset($_POST[$inputName])) {
            $values = $_POST[$inputName];
        } else if (isset($_FILES[$inputName])) {
            // Slim was not used to upload this file
            return false;
        }

        return $values;
    }

    /**
     * Saves the data to a given location
     * @param $data
     * @param $path
     */
    private static function save($data, $path) {
        $url_file = explode("/", $path);

        if (self::$ftp_server !== "") {
            $file_name = "temp/" . end($url_file);
            file_put_contents($file_name, $data);
        } else {
            $file_name = $path;
            $file_name = str_replace(self::$cdn_image, "", $file_name);
            $file_dir = str_replace(end($url_file), "", $file_name);
            if (!is_dir($file_dir)) {
                mkdir($file_dir, 0777, true);
            }
            file_put_contents($file_name, $data);
        }

        $image = getimagesize($file_name);
        switch ($image["mime"]) {
            case "image/jpeg" : {
                    $image_jpeg = imagecreatefromjpeg($file_name);
                    imagejpeg($image_jpeg, $file_name, 60);
                }break;
            case "image/png" : {
                    Slim::compress_png($file_name);
                }break;
        }

        if (self::$ftp_server !== "") {
            $ftp_server = self::$ftp_server;
            $file_path = str_replace(self::$cdn_image, "", $path);
            $ftp_conn = ftp_connect($ftp_server) or die("Couldn't connect to $ftp_server");
            $login = ftp_login($ftp_conn, self::$ftp_user, self::$ftp_password);

            ftp_pasv($ftp_conn, true);
            $file_dir = str_replace(end($url_file), "", $file_path);
            if (!@ftp_chdir($ftp_conn, $file_dir)) {                
                ftp_mkdir($ftp_conn, $file_dir);
            }

            ftp_put($ftp_conn, end($url_file), $file_name, FTP_BINARY);
            ftp_close($ftp_conn);
            unlink($file_name);
        }
    }

    private static function compress_png($path_to_png_file, $max_quality = 60) {
        if (!file_exists($path_to_png_file)) {
            throw new Exception("File does not exist: $path_to_png_file");
        }

        // guarantee that quality won't be worse than that.
        $min_quality = 60;

        // '-' makes it use stdout, required to save to $compressed_png_content variable
        // '<' makes it read from the given file path
        // escapeshellarg() makes this safe to use with any path
        $compressed_png_content = shell_exec(getcwd() . "\pngquant --quality=" . $min_quality . "-" . $max_quality . " " . getcwd() . "/" . $path_to_png_file . " --output " . getcwd() . "/" . $path_to_png_file . " -f");
        return $compressed_png_content;
    }

    /**
     * Strips the "data:image..." part of the base64 data string so PHP can save the string as a file
     * @param $data
     * @return string
     */
    private static function getBase64Data($data) {
        return base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data));
    }

}