<?php
class KeyGenerator{
	public function genKeyId($clearKey)
	{
		return strtoupper(bin2hex($this->str2bin($clearKey)));
	}
	
	public function genSignature($klikPayCode, $transactionDate, $transactionNo, $amount, $currency, $keyId)
	{
		/*
		 * Signature Step 1
		*/
		$tempKey1 = $klikPayCode . $transactionNo . $currency . $keyId;
		$hashKey1 = $this->getHash($tempKey1);
		 
		 /*
		 * Signature Step 2
		 * transactionDate format : 'dd/mm/yyyy hh:mm:ss' (19 char)
		 * e.g. 06/02/1990 02:02:02
		*/
		$expDate = explode("/",substr($transactionDate,0,10));
		$strDate = $this->intval32bits($expDate[0] . $expDate[1] . $expDate[2]);
		$amt = $this->intval32bits($amount);
		$tempKey2 = $strDate + $amt;
		$hashKey2 = $this->getHash((string)$tempKey2);
		
		
		
		/*
		 * Generate Key Step 3
		*/
		$signature = abs($hashKey1 + $hashKey2);
		
		return $signature; 
	}
	
	public function genAuthKey($klikPayCode, $transactionNo, $currency, $transactionDate, $keyId)
	{
		/*
		 * Step 1 - Padding
		*/
		$klikPayCode = str_pad($klikPayCode, 10, "0", STR_PAD_RIGHT);
		$transactionNo = str_pad($transactionNo, 18, "A", STR_PAD_RIGHT);
		$currency = str_pad($currency, 5, "1", STR_PAD_RIGHT);
		$transactionDate = str_pad($transactionDate, 19, "C", STR_PAD_LEFT);
		$keyId = str_pad($keyId, 16, "E", STR_PAD_RIGHT);
	   
		/*
		 * Step 2
		*/
		$value_1 = $klikPayCode . $transactionNo . $currency . $transactionDate . $keyId;
		
		/*
		* Step 3
		*/
		
		$hash_value_1 = strtoupper(md5($value_1));
		/*
		* Step 4
		*/
		
		if (strlen($keyId) == 32)
			$key = $keyId . substr($keyId,0,16);
		else if (strlen($keyId) == 48)
			$key = $keyId;
		
		// hex encode the return value
		return strtoupper(bin2hex(mcrypt_encrypt(MCRYPT_3DES, $this->hex2bin($key), $this->hex2bin($hash_value_1), MCRYPT_MODE_ECB)));
	}

	private function hex2bin($data)
	{
		$len = strlen($data);
		return pack("H" . $len, $data);
	}
	
	private function str2bin($data)
	{
		$len = strlen($data);
		return pack("a" . $len, $data);
	}
	
	private function intval32bits($value)
    {
        if ($value > 2147483647)
            $value = ($value - 4294967296);
		else if ($value < -2147483648)
            $value = ($value + 4294967296);
		
        return intval($value);
    }
	
	private function getHash($value)
	{
		$h = 0;
		for ($i = 0;$i < strlen($value);$i++)
		{
			$h = $this->intval32bits($this->add31T($h) + ord($value{$i}));
		}
		return $h;
	}
	
	private function add31T($value)
	{
		$result = 0;
		for($i=1;$i <= 31;$i++)
		{
			$result = $this->intval32bits($result + $value);
		}
		
		return $result;
	}
}
?>