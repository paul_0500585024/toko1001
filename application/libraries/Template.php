<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Template {

    public function view($view_name = '', $input = array(), $template_name = 'main', $add_header = '') {
	$CI = & get_instance();
	if ($view_name != '') {
	    $data['_content_'] = $CI->load->view(LIVEVIEW . $view_name, $input, true);
	    $data['_add_header_'] = $add_header;
	} else {
	    $data['_content_'] = '';
	}
	$CI->load->view(LIVEVIEW . 'home/template/' . $template_name, $data);
    }

}
