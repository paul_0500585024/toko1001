<?php

require_once MODEL_BASE;

class M_main_page_admin extends ModelBase {

    public function get_data($data) {

        $params[] = array($data->payment_status, TYPE_STRING);
        $params[] = array($data->trx_type, TYPE_STRING);
        $params[] = array($data->withdraw_status, TYPE_STRING);
        $params[] = array($data->new_merchant, TYPE_STRING);
        $params[] = array($data->product_status, TYPE_STRING);
        $params[] = array($data->cancel_trx_type, TYPE_STRING);
        $params[] = array($data->new_status_code, TYPE_STRING);
        $params[] = array($data->order_status, TYPE_STRING);

        try {
            return parent::execute_sp_multi_query("sp_main_page_admin", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function charts_member($data) {

        $params[] = array($data->january_from, TYPE_DATE);
        $params[] = array($data->december_to, TYPE_DATE);

        try {
            return parent::execute_sp_single_query("sp_get_chart_member", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function charts_merchant($data) {

        $params[] = array($data->from_date, TYPE_DATE);
        $params[] = array($data->to_date, TYPE_DATE);

        try {
            return parent::execute_sp_single_query("sp_get_chart_merchant", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>