<?php

require_once MODEL_BASE;

class M_member_refund_admin extends ModelBase {

    public function get_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->trx_no, TYPE_STRING);
        $params[] = array($filter->pgmethod, TYPE_NUMERIC);
        $params[] = array($filter->trx_type, TYPE_STRING);
        $params[] = array($filter->member_email, TYPE_STRING);
        $params[] = array($filter->status, TYPE_STRING);

        try {
            return parent::execute_sp_multi_query("sp_member_refund_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_return_product_detail($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->trx_no, TYPE_STRING);

        try {
            return parent::execute_sp_multi_query("sp_member_refund_get_product_return", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_member_account_info($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->trx_no, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_member_refund_get_deposit_trx", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data_refund($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->trx_no, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_member_refund_get_trx_type", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function approve($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->member_seq, TYPE_NUMERIC);
        try {
            parent::execute_non_query("sp_member_refund_approve", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function reject($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->member_seq, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_member_refund_reject", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function add_deposit_member($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->member_seq, TYPE_NUMERIC);
        $params[] = array($data->new_deposit, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_member_add_deposit", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_refund_data($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->member_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_member_refund_data", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_list_product_detail($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->order_seq, TYPE_STRING);
        $params[] = array($filter->merchant_info_seq, TYPE_STRING);

        try {
            return parent::execute_sp_multi_query("sp_member_refund_detail_product_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_voucher_nominal($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->order_seq, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_member_refund_get_voucher_nominal", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }
    public function get_data_refund_cancel($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->trx_no, TYPE_STRING);
        $params[] = array($filter->cancel_type, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_member_refund_get_data_cancel", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>