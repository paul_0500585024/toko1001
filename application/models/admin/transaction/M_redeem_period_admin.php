<?php

require_once MODEL_BASE;

class M_redeem_period_admin extends ModelBase {

    public function get_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        if ($filter->from_date == "") {
            $params[] = array($filter->from_date, TYPE_STRING);
        } else {
            $params[] = array($filter->from_date, TYPE_DATE);
        }
        if ($filter->to_date == "") {
            $params[] = array($filter->to_date, TYPE_STRING);
        } else {
            $params[] = array($filter->to_date, TYPE_DATE);
        }
        $params[] = array($filter->status, TYPE_STRING);

        try {
            return parent::execute_sp_multi_query("sp_redeem_period_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_redeem_period_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_list_detail($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_multi_query("sp_redeem_merchant_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_list_detail_status($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_redeem_merchant_status", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data_detail($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->seq, TYPE_NUMERIC);
        $params[] = array($filter->merchant_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_redeem_merchant_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_last_date($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_redeem_period_get_lastdate", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_delete($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->seq, TYPE_NUMERIC);
        try {
            parent::execute_non_query("sp_redeem_period_delete", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function status_update($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->seq, TYPE_NUMERIC);
        $params[] = array($filter->status_new, TYPE_STRING);
        $params[] = array($filter->status_old, TYPE_STRING);
        try {
            parent::execute_non_query("sp_redeem_period_status_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->from_date, TYPE_DATE);
        $params[] = array($data->to_date, TYPE_DATE);
        $params[] = array($data->expedition_commission, TYPE_NUMERIC);
        try {
            parent::execute_non_query("sp_redeem_period_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->from_date, TYPE_DATE);
        $params[] = array($data->to_date, TYPE_DATE);
        $params[] = array($data->expedition_commission, TYPE_NUMERIC);
        try {
            parent::execute_non_query("sp_redeem_period_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_detail($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->redeem_seq, TYPE_NUMERIC);
        $params[] = array($data->merchant_seq, TYPE_NUMERIC);
        $params[] = array($data->paid_date, TYPE_DATE);
        try {
            parent::execute_non_query("sp_redeem_merchant_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_redeem_component($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->type, TYPE_STRING);
        $params[] = array($data->mutation_type, TYPE_STRING);
        for ($i = 0; $i < count($data->dataset); $i++) {
            $total_redim = 0;
            $params[] = array($data->dataset[$i]->merchant_seq, TYPE_NUMERIC);
            $total_redim = ($data->dataset[$i]->total_barang + ($data->dataset[$i]->total_ekpedisi* (100 - $data->expedition_commission) / 100 )) - $data->dataset[$i]->total_komisi ;
            $params[] = array($total_redim, TYPE_NUMERIC);
            try {
                if ($data->dataset[$i]->merchant_seq != '')
                    parent::execute_non_query("sp_redeem_component_add", $params);
            } catch (Exception $ex) {
                parent::handle_database_error($ex);
            }
            array_pop($params);
            array_pop($params);
        }
    }

    public function save_update_order_merchant($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->to_date, TYPE_DATE);
        try {
            parent::execute_non_query("sp_redeem_merchant_update_order", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_order_merchant($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->to_date, TYPE_DATE);
        try {
            return parent::execute_sp_single_query("sp_redeem_merchant_get_order", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_expedition_invoice($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_redeem_merchant_get_expedition_invoice", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_redeem_component($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_redeem_component_sum_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add_redeem_merchant($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        for ($i = 0; $i < count($data->dataset); $i++) {
            $params[] = array($data->dataset[$i]->merchant_seq, TYPE_NUMERIC);
            $params[] = array($data->dataset[$i]->merchant_info_seq, TYPE_NUMERIC);
            $params[] = array($data->dataset[$i]->total, TYPE_NUMERIC);
            $params[] = array($data->dataset[$i]->status, TYPE_STRING);
            $params[] = array($data->dataset[$i]->paid_date, TYPE_DATE);
            try {
//		die(print_r($data->dataset));
                parent::execute_non_query("sp_redeem_merchant_add", $params);
            } catch (Exception $ex) {
                parent::handle_database_error($ex);
            }
            array_pop($params);
            array_pop($params);
            array_pop($params);
            array_pop($params);
            array_pop($params);
        }
    }

    public function save_update_merchant($data, $operasi = "") {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        for ($i = 0; $i < count($data->dataset); $i++) {
            $totals = $data->dataset[$i]->total;
            if ($operasi != "")
                $totals = ($totals * -1);
            $params[] = array($data->dataset[$i]->merchant_seq, TYPE_NUMERIC);
            $params[] = array($totals, TYPE_NUMERIC);
            try {
                parent::execute_non_query("sp_merchant_update_ap_amt", $params);
            } catch (Exception $ex) {
                parent::handle_database_error($ex);
            }
            array_pop($params);
            array_pop($params);
        }
    }

    public function get_redeem_merchant_status($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_redeem_merchant_list_status", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_redeem_component_by_merchant($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->merchant_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_redeem_component_by_seq_merchant", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_order_merchant_by_redeem($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->merchant_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_order_merchant_by_redeem", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_expedition_merchant_by_redeem($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->merchant_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_expedition_merchant_by_redeem", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_redeem_detail_order($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->order_no, TYPE_STRING);
        $params[] = array($data->redeem_seq, TYPE_NUMERIC);
        $params[] = array($data->merchant_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_order_merchant_detail_by_redeem", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>