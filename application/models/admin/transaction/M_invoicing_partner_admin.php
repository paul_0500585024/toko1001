<?php

require_once MODEL_BASE;

class M_invoicing_partner_admin extends ModelBase {

    public function get_list_detail($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_multi_query("sp_invoicing_partner_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }
    
    public function get_data_detail($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->seq, TYPE_NUMERIC);
        $params[] = array($filter->partner_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_invoicing_partner_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }
    
    public function get_invoicing_component_by_partner($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->partner_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_invoicing_component_by_seq_partner", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }    

    public function get_order_agent_by_loan($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->partner_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_invoicing_order_agent_partner_by_loan", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }
    
    public function get_order_period_partner_by_loan($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->key, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_invoicing_period_partner_by_collect_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }
    
    public function save_update_detail($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->collect_seq, TYPE_NUMERIC);
        $params[] = array($data->partner_seq, TYPE_NUMERIC);
        $params[] = array($data->paid_date, TYPE_DATE);
        try {
            parent::execute_non_query("sp_invoicing_agent_update_by_admin", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_invoicing_period_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }
    
    public function status_update_agent_period($filter) {
	$params[] = array($filter->user_id, TYPE_STRING);
	$params[] = array($filter->ip_address, TYPE_STRING);
	$params[] = array($filter->seq, TYPE_NUMERIC);
	$params[] = array($filter->status_new, TYPE_STRING);
	$params[] = array($filter->status_old, TYPE_STRING);
	try {
	    parent::execute_non_query("sp_invoicing_period_status_update", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }
    
    
}

?>