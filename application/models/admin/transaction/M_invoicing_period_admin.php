<?php

require_once MODEL_BASE;

class M_invoicing_period_admin extends ModelBase {

    public function get_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        if ($filter->from_date == "") {
            $params[] = array($filter->from_date, TYPE_STRING);
        } else {
            $params[] = array($filter->from_date, TYPE_DATE);
        }
        if ($filter->to_date == "") {
            $params[] = array($filter->to_date, TYPE_STRING);
        } else {
            $params[] = array($filter->to_date, TYPE_DATE);
        }
        $params[] = array($filter->status, TYPE_STRING);

        try {
            return parent::execute_sp_multi_query("sp_invoicing_period_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_invoicing_period_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_last_date($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_invoicing_period_get_lastdate", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function status_update_collect_period($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->seq, TYPE_NUMERIC);
        $params[] = array($filter->status_new, TYPE_STRING);
        $params[] = array($filter->status_old, TYPE_STRING);
        try {
            parent::execute_non_query("sp_invoicing_period_status_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->from_date, TYPE_DATE);
        $params[] = array($data->to_date, TYPE_DATE);
        try {
            parent::execute_non_query("sp_invoicing_period_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_order_loan($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->to_date, TYPE_DATE);
        try {
            parent::execute_non_query("sp_invoicing_update_order_loan", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_order_loan($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->to_date, TYPE_DATE);
        try {
            return parent::execute_sp_single_query("sp_invoicing_period_get_order", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add_invoicing_component_by_partner_seq($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->type, TYPE_STRING);
        $params[] = array($data->mutation_type, TYPE_STRING);
        $params[] = array($data->partner_seq, TYPE_NUMERIC);
        $params[] = array($data->total_payment, TYPE_NUMERIC);
        $params[] = array($data->total_installment, TYPE_NUMERIC);
        try {
            parent::execute_non_query("sp_invoicing_component_add_by_partner_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }
    
}

?>