<?php

require_once MODEL_BASE;

class M_expedition_receipt_admin extends ModelBase {

    public function get_list($filter) {

        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_STRING);
        $params[] = array($filter->length, TYPE_STRING);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->expedition_seq, TYPE_NUMERIC);
        $params[] = array($filter->invoice_no, TYPE_STRING);
        $params[] = array($filter->status, TYPE_STRING);

        try {
            return parent::execute_sp_multi_query("sp_expedition_receipt_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_edit($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->exp_seq, TYPE_NUMERIC);
        $params[] = array($data->seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_expedition_receipt_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->key, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_get_data_expedition_receipt", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->expedition_seq, TYPE_NUMERIC);
        $params[] = array($data->invoice_no, TYPE_STRING);
        $params[] = array($data->invoice_date, TYPE_DATE);
        $params[] = array($data->period_seq, TYPE_NUMERIC);
        $params[] = array($data->status, TYPE_STRING);

        try {
            parent::execute_non_query("sp_expedition_receipt_save_add", $params);
        } catch (Exeption $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_status_receipt($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_expedition_receipt_status", $params);
        } catch (Exeption $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->expedition_seq, TYPE_NUMERIC);
        $params[] = array($data->invoice_no, TYPE_STRING);
        $params[] = array($data->invoice_date, TYPE_DATE);
        $params[] = array($data->period_seq, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_expedition_receipt_save_update", $params);
        } catch (Exeption $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_delete($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_expedition_receipt_save_delete", $params);
        } catch (Exeption $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data_for_approve($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->exp_seq, TYPE_NUMERIC);
        $params[] = array($data->seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_expedition_receipt_get_data_for_approve", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_approve($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->exp_seq, TYPE_NUMERIC);
        $params[] = array($data->seq, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_expedition_receipt_save_approve", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

//    public function save_reject($data) {
//        $params[] = array($data->user_id, TYPE_STRING);
//        $params[] = array($data->ip_address, TYPE_STRING);
//        $params[] = array($data->exp_seq, TYPE_NUMERIC);
//        $params[] = array($data->seq, TYPE_NUMERIC);
//
//        try {
//            parent::execute_non_query("sp_expedition_receipt_save_reject", $params);
//        } catch (Exception $ex) {
//            parent::handle_database_error($ex);
//        }
//    }

    public function get_list_detail($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_STRING);
        $params[] = array($filter->length, TYPE_STRING);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->hseq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_multi_query("sp_expedition_receipt_list_detail", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add_detail($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->invoice_seq, TYPE_NUMERIC);
        $params[] = array($data->awb_no, TYPE_STRING);
        $params[] = array($data->awb_date, TYPE_DATE);
        $params[] = array($data->destination, TYPE_STRING);
        $params[] = array($data->ship_price, TYPE_NUMERIC);
        $params[] = array($data->ins_price, TYPE_NUMERIC);
        $params[] = array($data->order_seq, TYPE_NUMERIC);
        $params[] = array($data->merchant_seq, TYPE_NUMERIC);
        $params[] = array($data->status, TYPE_STRING);
//        var_dump($params); die();
        try {
            return parent::execute_sp_single_query("sp_expedition_receipt_save_add_detail", $params);
            die();
        } catch (Exeption $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function order_merchant_update($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->order_seq, TYPE_NUMERIC);
        $params[] = array($data->merchant_seq, TYPE_NUMERIC);
        $params[] = array($data->invoice_seq, TYPE_NUMERIC);
        $params[] = array($data->awb_seq, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_update_order_merchant", $params);
        } catch (Exeption $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_detail($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->invoice_seq, TYPE_NUMERIC);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->awb_no, TYPE_STRING);
        $params[] = array($data->awb_date, TYPE_DATE);
        $params[] = array($data->destination, TYPE_STRING);
        $params[] = array($data->ship_price, TYPE_NUMERIC);
        $params[] = array($data->ins_price, TYPE_NUMERIC);
        $params[] = array($data->order_seq, TYPE_NUMERIC);
        $params[] = array($data->merchant_seq, TYPE_NUMERIC);
        $params[] = array($data->status, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_expedition_receipt_save_update_detail", $params);
        } catch (Exeption $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function order_merchant_update_detail($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->invoice_seq, TYPE_NUMERIC);
        $params[] = array($data->order_seq, TYPE_NUMERIC);
        $params[] = array($data->merchant_seq, TYPE_NUMERIC);
        $params[] = array($data->old_order_seq, TYPE_NUMERIC);
        $params[] = array($data->old_merchant_info_seq, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_update_order_merchant_detail", $params);
        } catch (Exeption $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data_detail($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_expedition_receipt_get_data_detail", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_invoice_status($data) {
        $params[] = array($data->invoice_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_get_invoice_status", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_delete_detail($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->key, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_expedition_receipt_detail_save_delete", $params);
        } catch (Exeption $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_check_order($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->invoice_seq, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_expedition_receipt_check", $params);
        } catch (Exeption $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data_awb($data) {
        $params[] = array($data->awb_no, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_get_data_awb_no", $params);
        } catch (Exeption $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>
