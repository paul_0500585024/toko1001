<?php

require_once MODEL_BASE;

class M_order_return_admin extends ModelBase {

    public function get_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->return_no, TYPE_STRING);
        $params[] = array($filter->member_email, TYPE_STRING);
        $params[] = array($filter->date_search, TYPE_STRING);
        $params[] = array($filter->return_date_from, TYPE_DATE);
        $params[] = array($filter->return_date_to, TYPE_DATE);
        $params[] = array($filter->return_status, TYPE_STRING);

        try {
            return parent::execute_sp_multi_query("sp_order_return_list_admin", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_multi_query("sp_order_return_get_data", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data_order_return($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->key, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_order_return_get_data_header", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_received($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->return_no, TYPE_STRING);
        $params[] = array($data->shipment_status, TYPE_STRING);
        $params[] = array($data->status, TYPE_STRING);

        try {
            return parent::execute_sp_multi_query("sp_order_return_save_received", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_ship_to_merchant($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->return_no, TYPE_STRING);
        $params[] = array($data->exp_seq_to_merchant, TYPE_NUMERIC);
        $params[] = array($data->awb_admin_no, TYPE_STRING);
        $params[] = array($data->review_admin, TYPE_STRING);
        $params[] = array($data->shipment_status, TYPE_STRING);
        $params[] = array($data->status, TYPE_STRING);

        try {
            return parent::execute_sp_multi_query("sp_order_return_save_ship_to_merchant", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_ship_to_member($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->return_no, TYPE_STRING);
        $params[] = array($data->exp_seq_to_member, TYPE_NUMERIC);
        $params[] = array($data->awb_admin_no, TYPE_STRING);
        $params[] = array($data->review_admin, TYPE_STRING);
        $params[] = array($data->shipment_status, TYPE_STRING);
        $params[] = array($data->status, TYPE_STRING);

        try {
            return parent::execute_sp_multi_query("sp_order_return_save_ship_to_member", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>
