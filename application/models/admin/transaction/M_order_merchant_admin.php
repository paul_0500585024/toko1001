<?php

require_once MODEL_BASE;

class M_order_merchant_admin extends ModelBase {

    public function get_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->order_no, TYPE_STRING);
        $params[] = array($filter->pSDate, TYPE_DATE);
        $params[] = array($filter->pEDate, TYPE_DATE);
        $params[] = array($filter->pg_method_seq, TYPE_STRING);
        $params[] = array($filter->merchant_info_seq, TYPE_NUMERIC);
        $params[] = array($filter->order_status, TYPE_STRING);
        $params[] = array($filter->paymnet_status, TYPE_STRING);

        try {
            return parent::execute_sp_multi_query("sp_order_merchant_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->order_seq, TYPE_NUMERIC);
        $params[] = array($selected->merchant_info_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_order_merchant_by_order", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_produk($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->order_seq, TYPE_NUMERIC);
        $params[] = array($selected->merchant_info_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_order_merchant_by_merchant", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->order_seq, TYPE_NUMERIC);
        $params[] = array($data->order_status, TYPE_STRING);
        $params[] = array($data->received_date, TYPE_DATE);
        $params[] = array($data->merchant_info_seq, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_order_merchant_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>
