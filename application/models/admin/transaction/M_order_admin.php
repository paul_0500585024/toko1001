<?php

require_once MODEL_BASE;

class M_order_admin extends ModelBase {

    public function get_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->order_no, TYPE_STRING);
        $params[] = array($filter->pSDate, TYPE_NEW_DATE);
        $params[] = array($filter->pEDate, TYPE_NEW_DATE);
        $params[] = array($filter->payment_status, TYPE_STRING);
        $params[] = array($filter->pg_method_seq, TYPE_STRING);

        try {
            return parent::execute_sp_multi_query("sp_order_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_order_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data_merchant($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->order_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_order_merchant_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data_m_merchant($selected) {

        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->merchant_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_merchant_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_produk($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_order_product_by_order", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    function get_member_seq($data) {
        $params[] = array($data->email, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_member_email_check", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    function get_member_log($sel) {
        $params[] = array($sel->user_id, TYPE_STRING);
        $params[] = array($sel->ip_address, TYPE_STRING);
        $params[] = array($sel->mseq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_member_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->key, TYPE_NUMERIC);
        $params[] = array($data->payment_status, TYPE_STRING);
        $params[] = array($data->paid_date, TYPE_DATE);
        $params[] = array($data->conf_pay_amt_admin, TYPE_NUMERIC);
        $params[] = array($data->order_status, TYPE_STRING);

        try {
            parent::execute_sp_single_query("sp_order_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_new_voucher($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->new_voucher_seq, TYPE_NUMERIC);
        $params[] = array($data->seq, TYPE_NUMERIC);
        try {
            parent::execute_non_query("sp_order_save_new_voucher", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data_detail($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_STRING);

        try {
            return parent::execute_sp_multi_query("sp_order_by_order_no", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    function get_agent_seq($data) {
        $params[] = array($data->email, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_agent_email_check", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    function get_order_info_loan_admin($selected) {
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->order_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_get_info_order_loan_admin", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>
