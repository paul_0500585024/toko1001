<?php

require_once MODEL_BASE;

class M_redeem_agent_admin extends ModelBase {

    public function get_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        if ($filter->from_date == "") {
            $params[] = array($filter->from_date, TYPE_STRING);
        } else {
            $params[] = array($filter->from_date, TYPE_DATE);
        }
        if ($filter->to_date == "") {
            $params[] = array($filter->to_date, TYPE_STRING);
        } else {
            $params[] = array($filter->to_date, TYPE_DATE);
        }
        $params[] = array($filter->status, TYPE_STRING);

        try {
            return parent::execute_sp_multi_query("sp_redeem_agent_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_redeem_agent_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_list_detail($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_multi_query("sp_redeem_partner_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_list_detail_status($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_redeem_agent_status", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data_detail($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->seq, TYPE_NUMERIC);
        $params[] = array($filter->partner_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_redeem_partner_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_last_date($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_redeem_agent_get_lastdate", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_delete($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->seq, TYPE_NUMERIC);
        try {
            parent::execute_non_query("sp_redeem_period_delete", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function status_update_agent_period($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->seq, TYPE_NUMERIC);
        $params[] = array($filter->status_new, TYPE_STRING);
        $params[] = array($filter->status_old, TYPE_STRING);
        try {
            parent::execute_non_query("sp_redeem_agent_period_status_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->from_date, TYPE_DATE);
        $params[] = array($data->to_date, TYPE_DATE);
        try {
            parent::execute_non_query("sp_redeem_agent_period_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->from_date, TYPE_DATE);
        $params[] = array($data->to_date, TYPE_DATE);
        try {
            parent::execute_non_query("sp_redeem_agent_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_detail($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->redeem_seq, TYPE_NUMERIC);
        $params[] = array($data->partner_seq, TYPE_NUMERIC);
        $params[] = array($data->paid_date, TYPE_DATE);
        try {
            parent::execute_non_query("sp_redeem_agent_update_by_partner", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_redeem_commision_component($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->type, TYPE_STRING);
        $params[] = array($data->mutation_type, TYPE_STRING);
        $params[] = array($data->total_partner, TYPE_NUMERIC);
        try {
            parent::execute_non_query("sp_redeem_agent_commission_component_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_redeem_order_component($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->type, TYPE_STRING);
        $params[] = array($data->mutation_type, TYPE_STRING);
        $params[] = array($data->payment_code, TYPE_STRING);
        $params[] = array($data->to_date, TYPE_DATE);
        try {
            parent::execute_non_query("sp_redeem_agent_order_component_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_order_merchant($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->to_date, TYPE_DATE);
        try {
            parent::execute_non_query("sp_redeem_agent_update_order", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_order_agent($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->to_date, TYPE_DATE);
        try {
            return parent::execute_sp_single_query("sp_redeem_agent_get_order", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add_redeem_agent($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->agent_seq, TYPE_NUMERIC);
        $params[] = array($data->total, TYPE_NUMERIC);
        try {
            parent::execute_non_query("sp_redeem_agent_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_redeem_component_by_merchant($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->partner_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_redeem_component_by_seq_partner", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_order_agent_pg_by_redeem($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->payment_code, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->partner_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_order_agent_partner_by_redeem", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_redeem_agent_by_partner($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->partner_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_redeem_agent_by_partner", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_redeem_detail_order($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->redeem_seq, TYPE_NUMERIC);
        $params[] = array($data->agent_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_order_agent_detail_by_redeem", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_redeem_agent_partner_list($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_redeem_agent_partner_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_total_parner($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->to_date, TYPE_DATE);
        $params[] = array($data->seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_redeem_agent_total_partner_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>