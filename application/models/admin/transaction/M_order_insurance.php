<?php

require_once MODEL_BASE;

class M_order_insurance extends ModelBase {

    public function get_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);

        $params[] = array($filter->order_no, TYPE_STRING);
        $params[] = array($filter->date_reg_start, TYPE_DATE);
        $params[] = array($filter->date_reg_end, TYPE_DATE);
        $params[] = array($filter->member_name, TYPE_STRING);
        $params[] = array($filter->phone_number, TYPE_STRING);
        $params[] = array($filter->email_address, TYPE_STRING);
        $params[] = array($filter->gender, TYPE_STRING);
        try {
            return parent::execute_sp_multi_query("sp_order_insurance", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}
?>
