<?php

require_once MODEL_BASE;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of tracking AWB
 *
 * @author Jartono
 */
class M_track_awb extends ModelBase {

    public function get_data($selected) {
	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->key, TYPE_STRING);
	try {
	    return parent::execute_sp_single_query("sp_scheduler_awb_list", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }
    
    public function get_data_awb($selected) {
	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	try {
	    return parent::execute_sp_single_query("sp_scheduler_awb_non_partner_list", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_update($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->order_seq, TYPE_NUMERIC);
	$params[] = array($data->merchant_info_seq, TYPE_NUMERIC);
	$params[] = array($data->order_status, TYPE_STRING);
	$params[] = array($data->ship_date, TYPE_DATE);
	$params[] = array($data->received_date, TYPE_DATETIME);
	$params[] = array($data->received_by, TYPE_STRING);
	try {
	    parent::execute_non_query("sp_scheduler_order_merchant_update", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

}

?>