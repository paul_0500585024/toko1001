<?php


require_once MODEL_BASE;

class M_scheduler_refund_order extends ModelBase {

    public function get_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_refund_order_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }    

    public function save_update_by_system($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->merchant_seq, TYPE_NUMERIC);
        $params[] = array($data->order_status, TYPE_STRING);
        try {
            parent::execute_non_query("sp_refund_order_update_merchant_by_system", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }
    
    public function save_update_product_by_system($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->merchant_seq, TYPE_NUMERIC);
        $params[] = array($data->product_status, TYPE_STRING);
        try {
            parent::execute_non_query("sp_refund_order_update_product_by_system", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }
    
    
    
}
