<?php

require_once MODEL_BASE;

class M_scheduler_cashback extends ModelBase {

    public function get_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->order_status, TYPE_STRING);
        $params[] = array($filter->settled, TYPE_NUMERIC);
        $params[] = array($filter->product_status, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_scheduler_cashback_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_product($data) {
        $params[] = array($data->order_seq, TYPE_NUMERIC);
        $params[] = array($data->product_variant_seq, TYPE_NUMERIC);
        $params[] = array($data->settled_update, TYPE_NUMERIC);

        try {
            return parent::execute_non_query("sp_scheduler_cashback_product_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>
