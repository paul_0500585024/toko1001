<?php

require_once MODEL_BASE;

class M_scheduler_order_admin extends ModelBase {

    public function get_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_order_list_cancel", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_produk($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_order_product_by_order", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->key, TYPE_NUMERIC);
        $params[] = array($data->payment_status, TYPE_STRING);
        $params[] = array($data->paid_date, TYPE_DATE);
        $params[] = array($data->conf_pay_amt_admin, TYPE_NUMERIC);
        $params[] = array($data->order_status, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_order_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_new_voucher($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->new_voucher_seq, TYPE_NUMERIC);
        $params[] = array($data->seq, TYPE_NUMERIC);
        try {
           parent::execute_sp_single_query("sp_order_save_new_voucher", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>
