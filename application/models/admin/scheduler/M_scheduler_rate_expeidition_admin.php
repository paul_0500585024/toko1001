<?php

require_once MODEL_BASE;

class M_scheduler_rate_expeidition_admin extends ModelBase {

    public function get_rate_expedition_list() {
        try {
            return parent::execute_sp_single_query("sp_get_rate_expedition_list");
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}
?>
