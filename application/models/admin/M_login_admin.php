<?php

require_once MODEL_BASE;

class M_login_admin extends ModelBase {

    public function get_list($data) {
        $login[] = array($data->user_id, TYPE_STRING);
        $login[] = array($data->encrypt_password, TYPE_STRING);
        $login[] = array($data->ip_address, TYPE_STRING);
        try {
            return parent::execute_sp_multi_query("sp_login_admin", $login);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_left_nav_admin($user_id) {

        $login[] = array($user_id, TYPE_STRING);

        try {
            $data = parent::execute_sp_single_query("sp_get_left_nav_admin", $login);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
        if (isset($data)) {
            foreach ($data as $row) {
                $response[$row->parent_menu_cd][] = $row;
            }
            return $response;
        }
    }

}

?>