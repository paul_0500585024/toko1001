<?php

require_once MODEL_BASE;

class M_member_admin extends ModelBase {

    public function get_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->email, TYPE_STRING);
        $params[] = array($filter->name, TYPE_STRING);
        $params[] = array($filter->status, TYPE_STRING);


        try {
            return parent::execute_sp_multi_query("sp_member_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_member_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data_voucher($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_promo_voucher_by_member", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_upload_member($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->email, TYPE_STRING);
        $params[] = array($data->name, TYPE_STRING);
        $params[] = array($data->birthday, TYPE_DATE);
        $params[] = array($data->gender, TYPE_STRING);
        $params[] = array($data->mobile_phone, TYPE_STRING);
        $params[] = array($data->status, TYPE_STRING);

        try {
            parent::execute_non_query("sp_member_upload", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_approve($selected) {

        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);
        $params[] = array($selected->new_password, TYPE_STRING);

        try {
            parent::execute_non_query("sp_member_approve_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function update_voucher($selected) {
        $params[] = array($selected->member_seq, TYPE_NUMERIC);
        $params[] = array($selected->active_date, TYPE_DATE);
        $params[] = array($selected->exp_date, TYPE_DATE);

        try {
            parent::execute_non_query("sp_promo_voucher_update_by_member", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>