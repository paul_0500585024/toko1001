<?php

require_once MODEL_BASE;

class M_partner_live extends ModelBase {

    public function get_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->code, TYPE_STRING);
        $params[] = array($filter->partner_name, TYPE_STRING);
        $params[] = array($filter->partner_email, TYPE_STRING);
        $params[] = array($filter->admin_fee, TYPE_STRING);

        try {
            return parent::execute_sp_multi_query("sp_partner_live_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($selected) {

        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_partner_live_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->email, TYPE_STRING);
        $params[] = array($data->name, TYPE_STRING);
        $params[] = array($data->mobile_phone, TYPE_STRING);
        $params[] = array($data->admin_fee, TYPE_STRING);
        $params[] = array($data->bank_name, TYPE_STRING);
        $params[] = array($data->bank_branch_name, TYPE_STRING);
        $params[] = array($data->bank_acct_no, TYPE_STRING);
        $params[] = array($data->bank_acct_name, TYPE_STRING);
        $params[] = array($data->profile_img, TYPE_STRING);

        try {
            parent::execute_non_query("sp_partner_live_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_delete($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        try {
            parent::execute_non_query("sp_partner_live_delete", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function check_user_exist($email) {
        $params[] = array($email->email, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_check_partner_exist", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }

        return true;
    }

    public function save_add($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->email, TYPE_STRING);
        $params[] = array($data->name, TYPE_STRING);
        $params[] = array($data->password, TYPE_STRING);
        $params[] = array($data->mobile_phone, TYPE_STRING);
        $params[] = array($data->admin_fee, TYPE_STRING);
        $params[] = array($data->status, TYPE_STRING);
        $params[] = array($data->bank_name, TYPE_STRING);
        $params[] = array($data->bank_branch_name, TYPE_STRING);
        $params[] = array($data->bank_acct_no, TYPE_STRING);
        $params[] = array($data->bank_acct_name, TYPE_STRING);        
        $params[] = array($data->profile_img, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_partner_add_signup", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }
    
    
}

?>