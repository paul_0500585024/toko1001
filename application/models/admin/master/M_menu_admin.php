<?php

require_once MODEL_BASE;

class M_menu_admin extends ModelBase {

    public function get_list($filter) {

        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->module_seq, TYPE_NUMERIC);
        $params[] = array($filter->menu_cd, TYPE_STRING);
        $params[] = array($filter->menu_name, TYPE_STRING);
        $params[] = array($filter->active, TYPE_BOOLEAN);

        try {
            return parent::execute_sp_multi_query("sp_menu_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->menu_cd, TYPE_STRING);
        $params[] = array($selected->module_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_menu_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->module_seq, TYPE_NUMERIC);
        $params[] = array($data->parent_menu_cd, TYPE_STRING);
        $params[] = array($data->menu_cd, TYPE_STRING);
        $params[] = array($data->name, TYPE_STRING);
        $params[] = array($data->title_name, TYPE_STRING);
        $params[] = array($data->url, TYPE_STRING);
        $params[] = array($data->icon, TYPE_STRING);
        $params[] = array($data->order, TYPE_NUMERIC);
        $params[] = array($data->active, TYPE_BOOLEAN);
        $params[] = array($data->detail, TYPE_BOOLEAN);
        
        try {
            parent::execute_non_query("sp_menu_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->old_menu_cd, TYPE_STRING);
        $params[] = array($data->old_module_seq, TYPE_NUMERIC);
        $params[] = array($data->module_seq, TYPE_NUMERIC);
        $params[] = array($data->parent_menu_cd, TYPE_STRING);
        $params[] = array($data->menu_cd, TYPE_STRING);
        $params[] = array($data->name, TYPE_STRING);
        $params[] = array($data->title_name, TYPE_STRING);
        $params[] = array($data->url, TYPE_STRING);
        $params[] = array($data->icon, TYPE_STRING);
        $params[] = array($data->order, TYPE_NUMERIC);
        $params[] = array($data->active, TYPE_BOOLEAN);
        $params[] = array($data->detail, TYPE_BOOLEAN);

        try {
            parent::execute_non_query("sp_menu_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_delete($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->key, TYPE_STRING);

        try {
            parent::execute_non_query("sp_menu_delete", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>