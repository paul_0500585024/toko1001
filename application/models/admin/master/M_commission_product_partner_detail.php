<?php

require_once MODEL_BASE;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of M_commission_product_partner_detail
 *
 * @author Jaka
 */
class M_commission_product_partner_detail extends ModelBase {

    public function get_list($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_multi_query("sp_get_partner_product_commission_by_partner_product_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_product_by_name($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->product_name, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_get_partner_product_commission_dropdown_product_by_name", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_group_partner() {
        try {
            return parent::execute_sp_single_query("sp_get_group_partner_commission_list");
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_product_commission($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->partner_product_seq, TYPE_NUMERIC);
        $params[] = array($selected->product_seq, TYPE_NUMERIC);
        $params[] = array($selected->partner_group_seq, TYPE_NUMERIC);
        $params[] = array($selected->nominal_commission_partner, TYPE_NUMERIC);
        $params[] = array($selected->nominal_commission_agent, TYPE_NUMERIC);
        try {
            return parent::execute_non_query("sp_save_add_partner_product_commission", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function delete_partner_product($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->partner_product_seq, TYPE_NUMERIC);
        try {
            return parent::execute_non_query("sp_delete_all_partner_product_commission_by_partner_product_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}
