<?php

require_once MODEL_BASE;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of M_commission_product_partner
 *
 * @author Jaka
 */
class M_commission_product_partner extends ModelBase {

    public function get_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->period_from, TYPE_DATE);
        $params[] = array($filter->period_to, TYPE_DATE);
        try {
            return parent::execute_sp_multi_query("sp_commission_product_partner_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_commission_product_partner_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->period_from, TYPE_DATETIME);
        $params[] = array($selected->period_to, TYPE_DATETIME);
        $params[] = array($selected->notes, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_commission_product_partner_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->seq, TYPE_STRING);
        $params[] = array($selected->period_from, TYPE_DATETIME);
        $params[] = array($selected->period_to, TYPE_DATETIME);
        $params[] = array($selected->notes, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_commission_product_partner_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }
    
    public function save_delete($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);
        try {
            return parent::execute_non_query("sp_commission_product_partner_delete", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}
