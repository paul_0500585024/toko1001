<?php

require_once MODEL_BASE;

class M_promo_credit extends ModelBase {

    public function get_list($filter) {

        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->promo_credit_name, TYPE_STRING);
        $params[] = array($filter->promo_credit_period_from, TYPE_DATE);
        $params[] = array($filter->promo_credit_period_to, TYPE_DATE);
        $params[] = array($filter->status, TYPE_STRING);

        try {
            return parent::execute_sp_multi_query("sp_promo_credit_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_promo_credit_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->promo_credit_name, TYPE_STRING);
        $params[] = array($data->promo_credit_period_from, TYPE_DATE);
        $params[] = array($data->promo_credit_period_to, TYPE_DATE);
        $params[] = array($data->minimum_nominal, TYPE_NUMERIC);
        $params[] = array($data->maximum_nominal, TYPE_NUMERIC);
        $params[] = array($data->status, TYPE_STRING);

        try {
            parent::execute_non_query("sp_promo_credit_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->promo_credit_name, TYPE_STRING);
        $params[] = array($data->promo_credit_period_from, TYPE_DATE);
        $params[] = array($data->promo_credit_period_to, TYPE_DATE);
        $params[] = array($data->minimum_nominal, TYPE_NUMERIC);
        $params[] = array($data->maximum_nominal, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_promo_credit_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_delete($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->key, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_promo_credit_delete", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_approve($selected) {

        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_promo_credit_approve_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_reject($selected) {

        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_promo_credit_reject_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_list_detail($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->hseq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_multi_query("sp_coupon_product_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add_detail($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->promo_credit_seq, TYPE_NUMERIC);
        $params[] = array($data->product_variant_seq, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_promo_credit_product_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_detail($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->coupon_seq, TYPE_NUMERIC);
        $params[] = array($data->product_seq, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_coupon_product_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data_detail($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_promo_credit_product_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_delete_detail($data, $type = "") {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->promo_credit_seq, TYPE_NUMERIC);

        try {
            if ($type != PRODUCT_CATEGORY) {
                parent::execute_non_query("sp_promo_credit_product_delete", $params);
            } else {
                parent::execute_non_query("sp_promo_credit_category_delete", $params);
            }
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_thumbs_new_products_no_limit($filter) {

        $params[] = array($filter->where, TYPE_STRING);
        $params[] = array($filter->order, TYPE_STRING);

        try {
            $result['product'] = parent::execute_sp_single_query("sp_thumbs_new_products_no_limit", $params);
            $result['row'] = $this->db->query("SELECT FOUND_ROWS() AS total_row");
            return $result;
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add_bank($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->promo_seq, TYPE_NUMERIC);
        $params[] = array($data->bank_seq, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_promo_credit_bank_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_delete_bank($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->promo_seq, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_promo_credit_bank_delete", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data_credit_bank($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->promo_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_promo_credit_bank_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data_category_detail($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_promo_credit_category_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add_category($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->promo_credit_seq, TYPE_NUMERIC);
        $params[] = array($data->category_seq, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_promo_credit_category_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>