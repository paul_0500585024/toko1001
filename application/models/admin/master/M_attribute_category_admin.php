<?php

require_once MODEL_BASE;

class M_attribute_category_admin extends ModelBase {

    public function get_list($filter) {

        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->category_seq, TYPE_NUMERIC);
        $params[] = array($filter->attribute_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_multi_query("sp_attribute_category_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_attribute_category_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->parent_seq, TYPE_NUMERIC);

        $this->save_delete_detail($data);
        foreach ($data->attribute_seq as $item) {
            $params[] = array($item, TYPE_NUMERIC);
            try {
                parent::execute_non_query("sp_attribute_category_add", $params);
            } catch (Exception $ex) {
                parent::handle_database_error($ex);
            }
            array_pop($params);
        }
    }

    public function save_delete_detail($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->parent_seq, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_attribute_category_delete_detail", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_delete($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->category_seq, TYPE_NUMERIC);
        $params[] = array($data->attribute_seq, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_attribute_category_delete", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>