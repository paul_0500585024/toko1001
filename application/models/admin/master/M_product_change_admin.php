<?php

require_once MODEL_BASE;

class M_product_change_admin extends ModelBase {

    public function get_list($filter) {
	$params[] = array($filter->user_id, TYPE_STRING);
	$params[] = array($filter->ip_address, TYPE_STRING);
	$params[] = array($filter->start, TYPE_NUMERIC);
	$params[] = array($filter->length, TYPE_NUMERIC);
	$params[] = array($filter->order, TYPE_STRING);
	$params[] = array($filter->column, TYPE_STRING);
	$params[] = array($filter->name, TYPE_STRING);	
	$params[] = array($filter->merchant_seq, TYPE_NUMERIC);
	$params[] = array($filter->merchant_name, TYPE_STRING);
        $params[] = array($filter->status, TYPE_STRING);        
	try {
	    return parent::execute_sp_multi_query("sp_product_change_list", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    
      public function get_log_product($selected) {

	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->seq, TYPE_NUMERIC);

	try {
	    return parent::execute_sp_single_query("sp_get_log_product_by_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }
    
    public function get_log_data($selected) {

	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->seq, TYPE_NUMERIC);

	try {
	    return parent::execute_sp_single_query("sp_get_logdata_product_by_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }
    
     public function get_log_status($selected) {
	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->log_seq, TYPE_NUMERIC);                
	try {
	    return parent::execute_sp_single_query("sp_get_logstatus_product_by_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }
public function get_product_log_info_by_product_seq($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->product_seq, TYPE_NUMERIC);
        $params[] = array($data->log_seq, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_product_log_info_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }
     public function get_product_attribute_by_product_seq($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        
        try {
            return parent::execute_sp_single_query("sp_product_all_attribute_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }
    public function get_product_spec_by_product_seq($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        
        try {
            return parent::execute_sp_single_query("sp_product_spec_by_product_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }
     public function get_product_variant_by_product_seq($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_product_variant_by_product_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }
    public function get_product_log_attribute_by_product_seq($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->product_seq, TYPE_NUMERIC);
        $params[] = array($data->log_seq, TYPE_NUMERIC);
        
        try {
            return parent::execute_sp_single_query("sp_product_log_attribute_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }
    public function get_product_log_spec_by_product_seq($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->product_seq, TYPE_NUMERIC);
        $params[] = array($data->log_seq, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_product_log_spec_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }
    public function get_product_log_variant_by_product_seq($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->product_seq, TYPE_NUMERIC);
        $params[] = array($data->log_seq, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_product_log_variant_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }
    public function save_add($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->merchant_seq, TYPE_NUMERIC);
	$params[] = array($data->name, TYPE_STRING);
	$params[] = array($data->include_ins, TYPE_BOOLEAN);
	$params[] = array($data->category_l2_seq, TYPE_NUMERIC);
	$params[] = array($data->category_ln_seq, TYPE_NUMERIC);
	$params[] = array($data->notes, TYPE_STRING);
	$params[] = array($data->description, TYPE_STRING);
	$params[] = array($data->content, TYPE_STRING);
	$params[] = array($data->warranty_notes, TYPE_STRING);
	$params[] = array($data->p_weight_kg, TYPE_NUMERIC);
	$params[] = array($data->p_length_cm, TYPE_NUMERIC);
	$params[] = array($data->p_width_cm, TYPE_NUMERIC);
	$params[] = array($data->p_height_cm, TYPE_NUMERIC);
	$params[] = array($data->b_weight_kg, TYPE_NUMERIC);
	$params[] = array($data->b_length_cm, TYPE_NUMERIC);
	$params[] = array($data->b_width_cm, TYPE_NUMERIC);
	$params[] = array($data->b_height_cm, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_product_new_add", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_update($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	$params[] = array($data->ge, TYPE_NUMERIC);
	$params[] = array($data->name, TYPE_STRING);
	$params[] = array($data->include_ins, TYPE_BOOLEAN);
	$params[] = array($data->notes, TYPE_STRING);
	$params[] = array($data->description, TYPE_STRING);
	$params[] = array($data->content, TYPE_STRING);
	$params[] = array($data->warranty_notes, TYPE_STRING);
	$params[] = array($data->p_weight_kg, TYPE_NUMERIC);
	$params[] = array($data->p_length_cm, TYPE_NUMERIC);
	$params[] = array($data->p_width_cm, TYPE_NUMERIC);
	$params[] = array($data->p_height_cm, TYPE_NUMERIC);
	$params[] = array($data->b_weight_kg, TYPE_NUMERIC);
	$params[] = array($data->b_length_cm, TYPE_NUMERIC);
	$params[] = array($data->b_width_cm, TYPE_NUMERIC);
	$params[] = array($data->b_height_cm, TYPE_NUMERIC);
	try {
	    parent::execute_non_query("sp_product_new_update", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_add_attribute($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->product_seq, TYPE_NUMERIC);
	$this->save_delete_attribute($data);
	foreach ($data->attribut as $attributeval) {
	    if ($attributeval != '') {
		$params[] = array($attributeval, TYPE_NUMERIC);
		try {
		    parent::execute_non_query("sp_product_attribute_new_add", $params);
		} catch (Exception $ex) {
		    parent::handle_database_error($ex);
		}
		array_pop($params);
	    }
	}
    }

    public function save_delete_attribute($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->product_seq, TYPE_NUMERIC);
	try {
	    parent::execute_non_query("sp_product_attribute_new_delete", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_add_spec($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->product_seq, TYPE_NUMERIC);
	$this->save_delete_spec($data);
	for ($i = 0; $i < count($data->spekname); $i++) {
	    if ($data->spekname[$i] != '') {
		$params[] = array($data->spekname[$i], TYPE_STRING);
		$params[] = array($data->spekval[$i], TYPE_STRING);
		try {
		    parent::execute_non_query("sp_product_spec_new_add", $params);
		} catch (Exception $ex) {
		    parent::handle_database_error($ex);
		}
		array_pop($params);
		array_pop($params);
	    }
	}
    }

    public function save_delete_spec($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->product_seq, TYPE_NUMERIC);
	try {
	    parent::execute_non_query("sp_product_spec_new_delete", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_reject($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->product_seq, TYPE_NUMERIC);
        $params[] = array($data->log_seq, TYPE_NUMERIC);
	try {
	    parent::execute_non_query("sp_product_change_reject", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_delete($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);

	try {
	    parent::execute_non_query("sp_product_new_delete", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_product_category() {
	try {
	    return parent::execute_sp_single_query("sp_get_tree_view_category"); // sp_product_category_merchant_by_parent
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
//	foreach ($data as $row) {
//	    $response[$row->parent_seq][] = $row;
//	}
//	return $response;
    }

    public function get_product_category_by_child($data) {
//	$params[] = array($data->user_id, TYPE_STRING);
//	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_product_category_by_child", $params); // sp_product_category_merchant_by_parent
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_merchant_lvl1($data) {
	$params[] = array($data, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_merchant_trx_fee_by_seq", $params); // sp_product_category_merchant_by_parent
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }
 public function get_attribute_value($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_STRING);
	try {
	    return parent::execute_sp_single_query("sp_attribute_value_by_attribute_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }
    public function get_attribute_by_category($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->idh, TYPE_STRING);        
	try {
	    return parent::execute_sp_single_query("sp_attribute_category_by_category", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_merchant_by_name($selected) {
	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->merchant_name, TYPE_STRING);
        
	try {
	    return parent::execute_sp_single_query("sp_merchant_by_name", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }
    
     public function get_merchant_by_product_seq($selected) {

	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->product_seq, TYPE_STRING);

	try {
	    return parent::execute_sp_single_query("sp_get_merchant_by_product_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }


    public function get_variant_by_category($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->idh, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_variant_product_by_category", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_log_product_attribute_by_product_seq($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_get_logattribute_product_by_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_log_product_spec_by_product_seq($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_get_logspec_product_by_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_log_product_variant_by_product_seq($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_get_logvariant_product_by_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }
    
    public function get_alllog_product($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_multi_query("sp_get_all_log_product", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_max_seq($data) {
	$params[] = array($data, TYPE_STRING);
	try {
	    return parent::execute_sp_single_query("sp_get_table_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_add_variant($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->product_seq, TYPE_NUMERIC);
	for ($i = 0; $i < count($data->variant_value_seq); $i++) {
	    if ($data->product_price[$i] != '' && $data->sale_price[$i] != '') {
		$params[] = array($data->variant_value_seq[$i], TYPE_NUMERIC);
		$params[] = array($data->product_price[$i], TYPE_NUMERIC);
		$params[] = array($data->disc_percent[$i], TYPE_NUMERIC);
		$params[] = array($data->sale_price[$i], TYPE_NUMERIC);
		$params[] = array($data->order[$i], TYPE_NUMERIC);
		$params[] = array($data->max_buy[$i], TYPE_NUMERIC);
		$params[] = array($data->file_name1[$i], TYPE_STRING);
		$params[] = array($data->file_name2[$i], TYPE_STRING);
		$params[] = array($data->file_name3[$i], TYPE_STRING);
		$params[] = array($data->file_name4[$i], TYPE_STRING);
		$params[] = array($data->file_name5[$i], TYPE_STRING);
		$params[] = array($data->file_name6[$i], TYPE_STRING);
		if ($data->variant_seq[$i] > 0) {
		    $params[] = array($data->variant_seq[$i], TYPE_STRING);
		}
		try {
		    if ($data->variant_seq[$i] > 0) {
			parent::execute_non_query("sp_product_variant_new_update", $params);
		    } else {
			parent::execute_non_query("sp_product_variant_new_add", $params);
		    }
		} catch (Exception $ex) {
		    parent::handle_database_error($ex);
		}
		if ($data->variant_seq[$i] > 0) {
		    array_pop($params); //variant_seq
		}
		array_pop($params); //variant_value_seq
		array_pop($params); //prod_price
		array_pop($params); //dis_price
		array_pop($params); //sal_price
		array_pop($params); //order
		array_pop($params); //max buy
		array_pop($params); //im1
		array_pop($params); //im2
		array_pop($params); //im3
		array_pop($params); //im4
		array_pop($params); //im5
		array_pop($params); //im6
	    }
	}
    }

    public function save_delete_variant($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->product_seq, TYPE_NUMERIC);
	try {
	    parent::execute_non_query("sp_product_variant_new_delete", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_copy_spec($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->product_seq, TYPE_NUMERIC);
	$params[] = array($data->log_seq, TYPE_NUMERIC);
	try {
	    parent::execute_non_query("sp_product_logspec_new_copy", $params);	    
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_copy_attribute($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->product_seq, TYPE_NUMERIC);
	$params[] = array($data->log_seq, TYPE_NUMERIC);
	try {
	    parent::execute_non_query("sp_product_logattribute_new_copy", $params);
	    
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_copy_variant($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->product_seq, TYPE_NUMERIC);
	$params[] = array($data->log_seq, TYPE_NUMERIC);
        $params[] = array($data->product_variant_seq, TYPE_NUMERIC);
        $params[] = array($data->new_variant_value_seq, TYPE_NUMERIC);                  
	try {
	    parent::execute_non_query("sp_product_logvariant_new_copy", $params);
	    
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }
    public function save_copy_variant_new($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->product_seq, TYPE_NUMERIC);
	$params[] = array($data->log_seq, TYPE_NUMERIC);
        $params[] = array($data->new_variant_value_seq, TYPE_NUMERIC);                  
        $params[] = array($data->new_img_1, TYPE_STRING);
        $params[] = array($data->new_img_2, TYPE_STRING);
        $params[] = array($data->new_img_3, TYPE_STRING);
        $params[] = array($data->new_img_4, TYPE_STRING);
        $params[] = array($data->new_img_5, TYPE_STRING);
        $params[] = array($data->new_img_6, TYPE_STRING);
//        /echo '<pre>';print_r($params);die();
	try {
	    parent::execute_non_query("sp_product_logvariantnew_new_copy", $params);
	    
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_variant($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->log_seq, TYPE_NUMERIC);
        
        
	try {
	    return parent::execute_sp_single_query("sp_product_log_variant_by_product", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_img_variant($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->newvarseq, TYPE_NUMERIC);
	$params[] = array($data->img1, TYPE_STRING);
	$params[] = array($data->img2, TYPE_STRING);
	$params[] = array($data->img3, TYPE_STRING);
	$params[] = array($data->img4, TYPE_STRING);
	$params[] = array($data->img5, TYPE_STRING);
	$params[] = array($data->img6, TYPE_STRING);
	try {
	    parent::execute_non_query("sp_product_variant_img_update", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

     public function change_log_status($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->log_seq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_multi_query("sp_product_change_log_status", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }
    
    public function save_copy_product($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->product_seq, TYPE_NUMERIC);
	$params[] = array($data->log_seq, TYPE_NUMERIC);
        
	try {
	    parent::execute_non_query("sp_product_logdata_new_copy", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

}

?>