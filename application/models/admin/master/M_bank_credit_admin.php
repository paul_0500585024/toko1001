<?php

require_once MODEL_BASE;

class M_bank_credit_admin extends ModelBase {

    public function get_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        try {
            return parent::execute_sp_multi_query("sp_bank_list_admin", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_bank_by_seq_admin", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->bank_name, TYPE_STRING);
        $params[] = array($data->description, TYPE_STRING);

        try {
            parent::execute_non_query("sp_bank_add_admin", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->bank_name, TYPE_STRING);
        $params[] = array($data->description, TYPE_STRING);

        try {
            parent::execute_non_query("sp_bank_update_admin", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_delete($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->key, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_bank_delete_admin", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_list_detail($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->hseq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_multi_query("sp_bank_credit_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add_detail($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->bank_seq, TYPE_NUMERIC);
        $params[] = array($data->credit_month, TYPE_NUMERIC);
        $params[] = array($data->rate, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_bank_credit_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data_detail($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_bank_credit_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_detail($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->credit_month, TYPE_NUMERIC);
        $params[] = array($data->rate, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_bank_credit_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_delete_detail($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->key, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_bank_credit_delete", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>
