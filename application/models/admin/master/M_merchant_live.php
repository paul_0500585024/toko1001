<?php

require_once MODEL_BASE;

class M_merchant_live extends ModelBase {

    public function get_list($filter) {
	$params[] = array($filter->user_id, TYPE_STRING);
	$params[] = array($filter->ip_address, TYPE_STRING);
	$params[] = array($filter->start, TYPE_NUMERIC);
	$params[] = array($filter->length, TYPE_NUMERIC);
	$params[] = array($filter->order, TYPE_STRING);
	$params[] = array($filter->column, TYPE_STRING);
	$params[] = array($filter->code, TYPE_STRING);
	$params[] = array($filter->merchant_seq, TYPE_NUMERIC);

	try {
	    return parent::execute_sp_multi_query("sp_merchant_live_list", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_data($selected) {

	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->seq, TYPE_NUMERIC);

	try {
	    return parent::execute_sp_single_query("sp_merchant_live_by_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_commission_log_list($selected) {

	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->seq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_merchant_trx_fee_log_list", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_update($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	$params[] = array($data->exp_fee_percent, TYPE_NUMERIC);
	$params[] = array($data->ins_fee_percent, TYPE_NUMERIC);
	$params[] = array($data->notes, TYPE_STRING);

	try {
	    parent::execute_non_query("sp_merchant_live_update", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_data_commission($selected) {

	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->seq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_merchant_live_commission_by_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_data_commission_log($selected) {

	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->seq, TYPE_NUMERIC);
	$params[] = array($selected->log_date, TYPE_STRING);
	try {
	    return parent::execute_sp_single_query("sp_merchant_trx_fee_log_by_seq_date", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_update_commission($data) {
	$level1 = array();
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	$this->save_add_commission_log($data);
	$this->save_delete_commission($data);
	for ($i = 0; $i < count($data->hseq); $i++) {
	    $params[] = array($data->hseq[$i], TYPE_NUMERIC);
	    $params[] = array(NULL, TYPE_NUMERIC);
	    $params[] = array(0, TYPE_NUMERIC);
	    $level1[] = $data->hseq[$i];
	    try {
		parent::execute_non_query("sp_merchant_live_commission_add", $params);
	    } catch (Exception $ex) {
		parent::handle_database_error($ex);
	    }
	    array_pop($params);
	    array_pop($params);
	    array_pop($params);
	}
	for ($i = 0; $i < count($data->lvlcat); $i++) {
	    $level = explode("~", $data->lvlcat[$i]);
	    $params[] = array($level[2], TYPE_NUMERIC);
	    $params[] = array($level[0], TYPE_NUMERIC);
	    $params[] = array($data->mfee[$level[1]], TYPE_NUMERIC);
	    try {
		if (false !== array_search($level[2], $level1))
		    parent::execute_non_query("sp_merchant_live_commission_add", $params);
	    } catch (Exception $ex) {
		parent::handle_database_error($ex);
	    }
	    array_pop($params);
	    array_pop($params);
	    array_pop($params);
	}
    }

    public function save_add_commission_log($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	try {
	    parent::execute_non_query("sp_merchant_trx_fee_log_add", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_delete_commission($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	try {
	    parent::execute_non_query("sp_merchant_live_commission_delete", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_info($selected) {
	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->seq, TYPE_NUMERIC);
	try {
	    parent::execute_non_query("sp_merchant_log_info_add", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

}

?>