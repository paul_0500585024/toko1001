<?php

require_once MODEL_BASE;

class M_user_admin extends ModelBase {

    public function get_list($filter) {

        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->account_id, TYPE_STRING);
        $params[] = array($filter->user_name, TYPE_STRING);
        $params[] = array($filter->user_group_seq, TYPE_NUMERIC);
        $params[] = array($filter->active, TYPE_BOOLEAN);

        try {
            return parent::execute_sp_multi_query("sp_user_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_user_by_id", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->account_id, TYPE_STRING);
        $params[] = array($data->user_name, TYPE_STRING);
        $params[] = array($data->user_group_seq, TYPE_STRING);
        $params[] = array($data->password, TYPE_STRING);
        $params[] = array($data->email, TYPE_STRING);
        $params[] = array($data->description, TYPE_STRING);
        $params[] = array($data->active, TYPE_BOOLEAN);

        try {
            parent::execute_non_query("sp_user_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->old_account_id, TYPE_STRING);
        $params[] = array($data->account_id, TYPE_STRING);
        $params[] = array($data->user_name, TYPE_STRING);
        $params[] = array($data->user_group_seq, TYPE_NUMERIC);
        $params[] = array($data->password, TYPE_STRING);
        $params[] = array($data->email, TYPE_STRING);
        $params[] = array($data->description, TYPE_STRING);
        $params[] = array($data->active, TYPE_BOOLEAN);
        try {
            parent::execute_non_query("sp_user_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_delete($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->key, TYPE_STRING);

        try {
            parent::execute_non_query("sp_user_delete", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>