<?php

require_once MODEL_BASE;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_province
 *
 * @author Jartono
 */
class M_image_admin extends ModelBase {

    public function get_list($filter) {

        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->category_seq, TYPE_NUMERIC);
        $params[] = array($filter->status, TYPE_STRING);
        $params[] = array($filter->active, TYPE_BOOLEAN);

        try {
            return parent::execute_sp_multi_query("sp_image_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_check() {
        try {
            return parent::execute_sp_single_query("sp_image_check_max_seq");
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($selected) {

        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->category_seq, TYPE_NUMERIC);
        $params[] = array($selected->seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_image_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function cek_status_by_seq($selected) {

        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->category_seq, TYPE_NUMERIC);
        $params[] = array($selected->seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_image_check_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function cek_status() {

        try {
            return parent::execute_sp_single_query("sp_image_check_status");
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_approve($selected) {

        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->category_seq, TYPE_NUMERIC);
        $params[] = array($selected->seq, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_image_approve_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_reject($selected) {

        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->category_seq, TYPE_NUMERIC);
        $params[] = array($selected->seq, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_image_reject_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->category_seq, TYPE_NUMERIC);
        $params[] = array($data->img_banner, TYPE_STRING);
        $params[] = array($data->img_banner_url, TYPE_STRING);
        $params[] = array($data->img_1, TYPE_STRING);
        $params[] = array($data->img_1_url, TYPE_STRING);
        $params[] = array($data->img_2, TYPE_STRING);
        $params[] = array($data->img_2_url, TYPE_STRING);
        $params[] = array($data->img_3, TYPE_STRING);
        $params[] = array($data->img_3_url, TYPE_STRING);
        $params[] = array($data->img_4, TYPE_STRING);
        $params[] = array($data->img_4_url, TYPE_STRING);
        $params[] = array($data->img_5, TYPE_STRING);
        $params[] = array($data->img_5_url, TYPE_STRING);
        $params[] = array($data->img_6, TYPE_STRING);
        $params[] = array($data->img_6_url, TYPE_STRING);
        $params[] = array($data->img_7, TYPE_STRING);
        $params[] = array($data->img_7_url, TYPE_STRING);
        $params[] = array($data->status, TYPE_STRING);


        try {
            return parent::execute_sp_single_query("sp_image_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_img($data) {
        $params[] = array($data->category_seq, TYPE_NUMERIC);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->banner, TYPE_STRING);
        $params[] = array($data->img_1, TYPE_STRING);
        $params[] = array($data->img_2, TYPE_STRING);
        $params[] = array($data->img_3, TYPE_STRING);
        $params[] = array($data->img_4, TYPE_STRING);
        $params[] = array($data->img_5, TYPE_STRING);
        $params[] = array($data->img_6, TYPE_STRING);
        $params[] = array($data->img_7, TYPE_STRING);


        try {
            parent::execute_non_query("sp_image_update_image_name", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->category_seq, TYPE_NUMERIC);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->img_banner, TYPE_STRING);
        $params[] = array($data->img_banner_url, TYPE_STRING);
        $params[] = array($data->img_1, TYPE_STRING);
        $params[] = array($data->img_1_url, TYPE_STRING);
        $params[] = array($data->img_2, TYPE_STRING);
        $params[] = array($data->img_2_url, TYPE_STRING);
        $params[] = array($data->img_3, TYPE_STRING);
        $params[] = array($data->img_3_url, TYPE_STRING);
        $params[] = array($data->img_4, TYPE_STRING);
        $params[] = array($data->img_4_url, TYPE_STRING);
        $params[] = array($data->img_5, TYPE_STRING);
        $params[] = array($data->img_5_url, TYPE_STRING);
        $params[] = array($data->img_6, TYPE_STRING);
        $params[] = array($data->img_6_url, TYPE_STRING);
        $params[] = array($data->img_7, TYPE_STRING);
        $params[] = array($data->img_7_url, TYPE_STRING);

        try {
            parent::execute_non_query("sp_image_update_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_delete($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->key, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_image_delete", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>
