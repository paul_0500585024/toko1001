<?php

require_once MODEL_BASE;

class M_product_category_admin extends ModelBase {

    public function get_list($filter) {
	$params[] = array($filter->user_id, TYPE_STRING);
	$params[] = array($filter->ip_address, TYPE_STRING);
	$params[] = array($filter->start, TYPE_NUMERIC);
	$params[] = array($filter->length, TYPE_NUMERIC);
	$params[] = array($filter->order, TYPE_STRING);
	$params[] = array($filter->column, TYPE_STRING);
	$params[] = array($filter->name, TYPE_STRING);
	$params[] = array($filter->parent_name, TYPE_STRING);
	$params[] = array($filter->active, TYPE_BOOLEAN);
	$params[] = array($filter->level, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_multi_query("sp_product_category_list", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_data($selected) {
	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->key, TYPE_NUMERIC);

	try {
	    return parent::execute_sp_single_query("sp_product_category_by_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_add($data) {

	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->name, TYPE_STRING);
	$params[] = array($data->parent_seq, TYPE_NUMERIC);
	$params[] = array($data->level, TYPE_NUMERIC);
	$params[] = array($data->order, TYPE_NUMERIC);
	$params[] = array($data->include_ins, TYPE_BOOLEAN);
	$params[] = array($data->exp_method, TYPE_STRING);
	$params[] = array($data->variant_seq, TYPE_NUMERIC);
	$params[] = array($data->trx_fee_percent, TYPE_NUMERIC);
	$params[] = array($data->active, TYPE_BOOLEAN);

	try {
	    parent::execute_non_query("sp_product_category_add", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_update($data) {

	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	$params[] = array($data->name, TYPE_STRING);
	$params[] = array($data->parent_seq, TYPE_NUMERIC);
	$params[] = array($data->level, TYPE_NUMERIC);
	$params[] = array($data->order, TYPE_NUMERIC);
	$params[] = array($data->include_ins, TYPE_BOOLEAN);
	$params[] = array($data->exp_method, TYPE_STRING);
	$params[] = array($data->variant_seq, TYPE_NUMERIC);
	$params[] = array($data->trx_fee_percent, TYPE_NUMERIC);
	$params[] = array($data->active, TYPE_BOOLEAN);

	try {
	    parent::execute_non_query("sp_product_category_update", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_delete($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->key, TYPE_NUMERIC);

	try {
	    parent::execute_non_query("sp_product_category_delete", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

}

?>