<?php

require_once MODEL_BASE;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Merchant New
 *
 * @author Jartono
 */
class M_merchant_admin extends ModelBase {

    public function get_list($filter) {
	$params[] = array($filter->user_id, TYPE_STRING);
	$params[] = array($filter->ip_address, TYPE_STRING);
	$params[] = array($filter->start, TYPE_NUMERIC);
	$params[] = array($filter->length, TYPE_NUMERIC);
	$params[] = array($filter->order, TYPE_STRING);
	$params[] = array($filter->column, TYPE_STRING);
	$params[] = array($filter->name, TYPE_STRING);
	$params[] = array($filter->email, TYPE_STRING);
	$params[] = array($filter->status, TYPE_STRING);

	try {
	    return parent::execute_sp_multi_query("sp_merchant_list", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_data($selected) {

	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->seq, TYPE_NUMERIC);

	try {
	    return parent::execute_sp_single_query("sp_merchant_new_by_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_add($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->email, TYPE_STRING);
	$params[] = array($data->name, TYPE_STRING);
	$params[] = array($data->address, TYPE_STRING);
	$params[] = array($data->district_seq, TYPE_NUMERIC);
	$params[] = array($data->zip_code, TYPE_STRING);
	$params[] = array($data->phone_no, TYPE_STRING);
	$params[] = array($data->fax_no, TYPE_STRING);
	$params[] = array($data->pic1_name, TYPE_STRING);
	$params[] = array($data->pic1_phone_no, TYPE_STRING);
	$params[] = array($data->pic2_name, TYPE_STRING);
	$params[] = array($data->pic2_phone_no, TYPE_STRING);
	$params[] = array($data->expedition_seq, TYPE_NUMERIC);
	$params[] = array($data->pickup_addr_eq, TYPE_BOOLEAN);
	$params[] = array($data->pickup_addr, TYPE_STRING);
	$params[] = array($data->pickup_district_seq, TYPE_NUMERIC);
	$params[] = array($data->pickup_zip_code, TYPE_STRING);
	$params[] = array($data->return_addr_eq, TYPE_BOOLEAN);
	$params[] = array($data->return_addr, TYPE_STRING);
	$params[] = array($data->return_district_seq, TYPE_NUMERIC);
	$params[] = array($data->return_zip_code, TYPE_STRING);
	$params[] = array($data->exp_fee_percent, TYPE_NUMERIC);
	$params[] = array($data->ins_fee_percent, TYPE_NUMERIC);
	$params[] = array($data->notes, TYPE_STRING);
	$params[] = array("N", TYPE_STRING);
	try {
	    parent::execute_non_query("sp_merchant_add", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_update($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	$params[] = array($data->name, TYPE_STRING);
	$params[] = array($data->address, TYPE_STRING);
	$params[] = array($data->district_seq, TYPE_NUMERIC);
	$params[] = array($data->zip_code, TYPE_STRING);
	$params[] = array($data->phone_no, TYPE_STRING);
	$params[] = array($data->fax_no, TYPE_STRING);
	$params[] = array($data->pic1_name, TYPE_STRING);
	$params[] = array($data->pic1_phone_no, TYPE_STRING);
	$params[] = array($data->pic2_name, TYPE_STRING);
	$params[] = array($data->pic2_phone_no, TYPE_STRING);
	$params[] = array($data->expedition_seq, TYPE_NUMERIC);
	$params[] = array($data->pickup_addr_eq, TYPE_BOOLEAN);
	$params[] = array($data->pickup_addr, TYPE_STRING);
	$params[] = array($data->pickup_district_seq, TYPE_NUMERIC);
	$params[] = array($data->pickup_zip_code, TYPE_STRING);
	$params[] = array($data->return_addr_eq, TYPE_BOOLEAN);
	$params[] = array($data->return_addr, TYPE_STRING);
	$params[] = array($data->return_district_seq, TYPE_NUMERIC);
	$params[] = array($data->return_zip_code, TYPE_STRING);
	$params[] = array($data->exp_fee_percent, TYPE_NUMERIC);
	$params[] = array($data->ins_fee_percent, TYPE_NUMERIC);
	$params[] = array($data->notes, TYPE_STRING);

	try {
	    parent::execute_non_query("sp_merchant_update", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_reject($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	try {
	    parent::execute_non_query("sp_merchant_reject", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_delete($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);

	try {
	    parent::execute_non_query("sp_merchant_delete", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_val_pcd($data) {
	$params[] = array($data, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_get_val_district_city_province", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_val_email($data) {
	$params[] = array($data, TYPE_STRING);
	try {
	    return parent::execute_sp_single_query("sp_merchant_by_email", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_copy($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	$params[] = array($data->password, TYPE_STRING);
	$params[] = array($data->pCode, TYPE_STRING);
	try {
	    return parent::execute_sp_single_query("sp_merchant_copy", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_data_detail($selected) {

	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->seq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_merchant_detail_by_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_detail_trx_fee($selected) {

	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->seq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_merchant_detail_by_merchant", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_update_detail($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	//die(print_r($data->lvlcat));
	$this->save_delete_detail($data);
	for ($i = 0; $i < count($data->hseq); $i++) {
	    $params[] = array($data->hseq[$i], TYPE_NUMERIC);
	    $params[] = array(NULL, TYPE_NUMERIC);
	    $params[] = array(0, TYPE_NUMERIC);
	    try {
		parent::execute_non_query("sp_merchant_detail_add", $params);
	    } catch (Exception $ex) {
		parent::handle_database_error($ex);
	    }
	    array_pop($params);
	    array_pop($params);
	    array_pop($params);
	}
	for ($i = 0; $i < count($data->lvlcat); $i++) {
	    $level = explode("~", $data->lvlcat[$i]);
	    $params[] = array($level[2], TYPE_NUMERIC);
	    $params[] = array($level[0], TYPE_NUMERIC);
	    $params[] = array($data->mfee[$level[1]], TYPE_NUMERIC);
	    try {
		parent::execute_non_query("sp_merchant_detail_add", $params);
	    } catch (Exception $ex) {
		parent::handle_database_error($ex);
	    }
	    array_pop($params);
	    array_pop($params);
	    array_pop($params);
	}
    }

    public function save_delete_detail($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	try {
	    parent::execute_non_query("sp_merchant_detail_delete", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_copy_detail($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	$params[] = array($data->seq_merchant, TYPE_NUMERIC);
	try {
	    parent::execute_non_query("sp_merchant_detail_copy", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_add_info($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq_merchant, TYPE_NUMERIC);
	if ($data->pickup_addr_eq == 1) {
	    $params[] = array($data->address, TYPE_STRING);
	    $params[] = array($data->district_seq, TYPE_NUMERIC);
	    $params[] = array($data->zip_code, TYPE_STRING);
	} else {
	    $params[] = array($data->pickup_addr, TYPE_STRING);
	    $params[] = array($data->pickup_district_seq, TYPE_NUMERIC);
	    $params[] = array($data->pickup_zip_code, TYPE_STRING);
	}
	$params[] = array($data->exp_fee_percent, TYPE_NUMERIC);
	$params[] = array($data->ins_fee_percent, TYPE_NUMERIC);
	try {
	    parent::execute_non_query("sp_merchant_info_add", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_add_log_sec($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq_merchant, TYPE_NUMERIC);
	$params[] = array(1, TYPE_NUMERIC);
	$params[] = array($data->email_code, TYPE_STRING);
	$params[] = array($data->password, TYPE_STRING);
	try {
	    parent::execute_non_query("sp_merchant_log_security_add", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

}

?>