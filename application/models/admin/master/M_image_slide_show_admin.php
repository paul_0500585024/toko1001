<?php

require_once MODEL_BASE;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_province
 *
 * @author Jartono
 */
class M_image_slide_show_admin extends ModelBase {

    public function get_list($filter) {

        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->status, TYPE_STRING);
        $params[] = array($filter->order_slide_show, TYPE_NUMERIC);
        $params[] = array($filter->active, TYPE_BOOLEAN);

        try {
            return parent::execute_sp_multi_query("sp_image_slide_show_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($selected) {

        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_image_slide_show_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_max_seq() {
        try {
            return parent::execute_sp_single_query("sp_image_slide_show_max_seq");
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_status($selected) {

        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);
        $params[] = array($selected->status, TYPE_STRING);

        try {
            parent::execute_non_query("sp_image_slide_show_update_status", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->img, TYPE_STRING);
        $params[] = array($data->img_url, TYPE_STRING);
        $params[] = array($data->order, TYPE_NUMERIC);
        $params[] = array($data->active, TYPE_BOOLEAN);
        $params[] = array($data->status, TYPE_STRING);
        try {
            parent::execute_non_query("sp_image_slide_show_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->img, TYPE_STRING);
        $params[] = array($data->img_url, TYPE_STRING);
        $params[] = array($data->order, TYPE_NUMERIC);
        $params[] = array($data->active, TYPE_BOOLEAN);

        try {
            parent::execute_non_query("sp_image_slide_show_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function check_status_image_slide_show_by_seq($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->key, TYPE_NUMERIC);
        
        try {
            return parent::execute_sp_single_query("sp_image_slide_show_check_status", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
        
    }

    public function save_delete($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->key, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_image_slide_show_delete", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>