<?php

require_once MODEL_BASE;

class M_expedition_city_admin extends ModelBase {

    public function get_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->expedition_seq, TYPE_NUMERIC);
        $params[] = array($filter->province_seq, TYPE_NUMERIC);
        $params[] = array($filter->city_seq, TYPE_NUMERIC);
        $params[] = array($filter->district_seq, TYPE_NUMERIC);
        $params[] = array($filter->exp_district_code, TYPE_STRING);
        $params[] = array($filter->active, TYPE_BOOLEAN);
        try {
            return parent::execute_sp_multi_query("sp_expedition_city_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->exp_seq, TYPE_NUMERIC);
        $params[] = array($selected->district_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_expedition_city_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_upload($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->exp_seq, TYPE_NUMERIC);
        $params[] = array($data->district_seq, TYPE_NUMERIC);
        $params[] = array($data->exp_district_code, TYPE_STRING);

        try {
            parent::execute_non_query("sp_expedition_city_upload", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->exp_seq, TYPE_NUMERIC);
        $params[] = array($data->district_seq, TYPE_NUMERIC);
        $params[] = array($data->exp_district_code, TYPE_STRING);

        try {
            parent::execute_non_query("sp_expedition_city_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>