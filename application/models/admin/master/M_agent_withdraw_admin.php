<?php

require_once MODEL_BASE;

class M_agent_withdraw_admin extends ModelBase {

    public function get_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->agent_email, TYPE_STRING);
        $params[] = array($filter->status, TYPE_STRING);

        try {
            return parent::execute_sp_multi_query("sp_admin_withdraw_list_agent", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($filter) {

        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->seq, TYPE_NUMERIC);
        $params[] = array($filter->agent_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_admin_get_data_withdraw_agent", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_approve($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->agent_seq, TYPE_NUMERIC);
        $params[] = array($data->deposit_trx_amt, TYPE_NUMERIC);
        $params[] = array($data->non_deposit_trx_amt, TYPE_NUMERIC);
        $params[] = array($data->deposit_amt, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_admin_withdraw_save_approve_agent", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_reject($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->agent_seq, TYPE_STRING);
        
        try {
            parent::execute_non_query("sp_admin_withdraw_save_reject_agent", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>