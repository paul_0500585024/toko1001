<?php

require_once MODEL_BASE;

class M_promo_voucher_periode_admin extends ModelBase {

    public function get_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->name, TYPE_STRING);
        $params[] = array($filter->code, TYPE_STRING);
        $params[] = array($filter->node_cd, TYPE_STRING);
        $params[] = array($filter->active, TYPE_BOOLEAN);
        $params[] = array($filter->status, TYPE_STRING);
        $params[] = array($filter->date_from, TYPE_DATE);
        $params[] = array($filter->date_to, TYPE_DATE);

        try {
            return parent::execute_sp_multi_query("sp_promo_voucher_period_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_promo_voucher_period_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->date_from, TYPE_DATE);
        $params[] = array($data->date_to, TYPE_DATE);
        $params[] = array($data->name, TYPE_STRING);
        $params[] = array($data->code, TYPE_STRING);
        $params[] = array($data->node_cd, TYPE_STRING);
        $params[] = array($data->type, TYPE_STRING);
        $params[] = array($data->voucher_count, TYPE_STRING);
        $params[] = array($data->nominal, TYPE_NUMERIC);
        $params[] = array($data->exp_days, TYPE_NUMERIC);
        $params[] = array($data->trx_get_amt, TYPE_NUMERIC);
        $params[] = array($data->trx_use_amt, TYPE_NUMERIC);
        $params[] = array($data->notes, TYPE_STRING);
        $params[] = array($data->active, TYPE_BOOLEAN);


        try {
            parent::execute_non_query("sp_promo_voucher_period_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add_voucher($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->key, TYPE_NUMERIC);
        $params[] = array($data->nominal, TYPE_STRING);
        $params[] = array($data->trx_use_amt, TYPE_NUMERIC);

        foreach ($data->code as $item) {
            $params[] = array($item, TYPE_STRING);
            try {
                parent::execute_non_query("sp_promo_voucher_add", $params);
            } catch (Exception $ex) {
                parent::handle_database_error($ex);
            }
            array_pop($params);
        }
    }

    public function get_approve($selected) {

        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_promo_voucher_approve_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_reject($selected) {

        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);

        try {
            return parent::execute_non_query("sp_promo_voucher_period_reject_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->date_from, TYPE_DATE);
        $params[] = array($data->date_to, TYPE_DATE);
        $params[] = array($data->name, TYPE_STRING);
        $params[] = array($data->code, TYPE_STRING);
        $params[] = array($data->node_cd, TYPE_STRING);
        $params[] = array($data->type, TYPE_STRING);
        $params[] = array($data->voucher_count, TYPE_STRING);
        $params[] = array($data->nominal, TYPE_NUMERIC);
        $params[] = array($data->exp_days, TYPE_NUMERIC);
        $params[] = array($data->trx_get_amt, TYPE_NUMERIC);
        $params[] = array($data->trx_use_amt, TYPE_NUMERIC);
        $params[] = array($data->notes, TYPE_STRING);
        $params[] = array($data->active, TYPE_BOOLEAN);
        try {
            parent::execute_non_query("sp_promo_voucher_period_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_detail($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->member_seq, TYPE_NUMERIC);
        $params[] = array($data->active_date, TYPE_DATE);
        $params[] = array($data->exp_date, TYPE_DATE);

        try {
            parent::execute_non_query("sp_promo_voucher_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_delete($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->key, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_promo_voucher_period_delete", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_list_detail($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->hseq, TYPE_NUMERIC);
        $params[] = array($filter->pFrom, TYPE_NUMERIC);
        $params[] = array($filter->pTo, TYPE_NUMERIC);

        try {
            return parent::execute_sp_multi_query("sp_promo_voucher_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data_detail($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_promo_voucher_by_voucher_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>
