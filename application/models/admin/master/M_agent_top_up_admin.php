<?php

require_once MODEL_BASE;

class M_agent_top_up_admin extends ModelBase {

    public function get_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->agent_seq, TYPE_STRING);
        $params[] = array($filter->status, TYPE_STRING);
        try {
            return parent::execute_sp_multi_query("sp_admin_topup_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->seq, TYPE_NUMERIC);
        $params[] = array($selected->agent_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_admin_topup_list_by_agent_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_approve($selected) {

        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->agent_seq, TYPE_NUMERIC);
        $params[] = array($selected->deposit_trx_amt, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_deposit_agent_addtract", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->agent_seq, TYPE_NUMERIC);
        $params[] = array($data->mutation_type, TYPE_STRING);
        $params[] = array($data->trx_type, TYPE_STRING);
        $params[] = array($data->pg_method_seq, TYPE_NUMERIC);
        $params[] = array($data->trx_no, TYPE_STRING);
        $params[] = array($data->deposit_trx_amt, TYPE_NUMERIC);
        $params[] = array(0, TYPE_NUMERIC);
        $params[] = array("", TYPE_STRING);
        $params[] = array("", TYPE_STRING);
        $params[] = array("", TYPE_STRING);
        $params[] = array("", TYPE_STRING);
        $params[] = array($data->status, TYPE_STRING);
        try {
            parent::execute_non_query("sp_admin_topup_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_STRING);
        $params[] = array($data->trx_no, TYPE_STRING);
        $params[] = array($data->deposit_trx_amt, TYPE_NUMERIC);
        try {
            parent::execute_non_query("sp_admin_topup_edit", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_delete($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->agent_seq, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_admin_topup_delete", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_status($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->agent_seq, TYPE_NUMERIC);
        $params[] = array($data->status, TYPE_STRING);

        try {
            parent::execute_non_query("sp_admin_topup_update_status", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>