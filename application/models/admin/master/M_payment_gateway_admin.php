<?php

require_once MODEL_BASE;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_payment_gateway
 *
 * @author Jartono
 */
class M_payment_gateway_admin extends ModelBase {

    public function get_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->name, TYPE_STRING);
        $params[] = array($filter->active, TYPE_BOOLEAN);

        try {
            return parent::execute_sp_multi_query("sp_payment_gateway_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_payment_gateway_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->name, TYPE_STRING);
        $params[] = array($data->order, TYPE_NUMERIC);
        $params[] = array($data->notes, TYPE_STRING);
        $params[] = array($data->active, TYPE_BOOLEAN);

        try {
            parent::execute_non_query("sp_payment_gateway_add", $params);
            //
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->name, TYPE_STRING);
        $params[] = array($data->order, TYPE_NUMERIC);
        $params[] = array($data->notes, TYPE_STRING);
        $params[] = array($data->active, TYPE_BOOLEAN);

        try {
            parent::execute_non_query("sp_payment_gateway_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_delete($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->key, TYPE_NUMERIC);
        try {
            parent::execute_non_query("sp_payment_gateway_delete", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_list_detail($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->hseq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_multi_query("sp_payment_gateway_method_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data_detail($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_payment_gateway_method_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add_detail($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->pg_seq, TYPE_NUMERIC);
        $params[] = array($data->name, TYPE_STRING);
        $params[] = array($data->code, TYPE_STRING);
        $params[] = array($data->logo_img, TYPE_STRING);
        $params[] = array($data->order, TYPE_NUMERIC);
        $params[] = array($data->notes, TYPE_STRING);
        $params[] = array($data->active, TYPE_BOOLEAN);

        try {
            parent::execute_non_query("sp_payment_gateway_method_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_detail($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->name, TYPE_STRING);
        $params[] = array($data->code, TYPE_STRING);
        $params[] = array($data->logo_img, TYPE_STRING);
        $params[] = array($data->order, TYPE_NUMERIC);
        $params[] = array($data->notes, TYPE_STRING);
        $params[] = array($data->active, TYPE_BOOLEAN);

        try {
            parent::execute_non_query("sp_payment_gateway_method_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_delete_detail($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->key, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_payment_gateway_method_delete", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>