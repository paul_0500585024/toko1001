<?php

require_once MODEL_BASE;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Bank Account
 *
 * @author Jartono
 */
class M_partner_down_payment extends ModelBase {

    public function get_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->category_loan, TYPE_STRING);
        $params[] = array($filter->tenor_loan, TYPE_STRING);
        $params[] = array($filter->partner_seq, TYPE_NUMERIC);
        $params[] = array($filter->product_category_seq, TYPE_NUMERIC);
        $params[] = array($filter->partner_loan_seq, TYPE_NUMERIC);
        $params[] = array($filter->active, TYPE_BOOLEAN);

        try {
            return parent::execute_sp_multi_query("sp_partner_down_payment_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_partner_down_payment_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->partner_loan_seq, TYPE_NUMERIC);
        $params[] = array($data->minimal_loan, TYPE_STRING);
        $params[] = array($data->maximal_loan, TYPE_STRING);
        $params[] = array($data->min_dp_percent, TYPE_STRING);
        $params[] = array($data->active, TYPE_BOOLEAN);

        try {
            parent::execute_non_query("sp_partner_down_payment_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->partner_loan_seq, TYPE_NUMERIC);
        $params[] = array($data->minimal_loan, TYPE_STRING);
        $params[] = array($data->maximal_loan, TYPE_STRING);
        $params[] = array($data->min_dp_percent, TYPE_STRING);
        $params[] = array($data->active, TYPE_BOOLEAN);

        try {
            parent::execute_non_query("sp_partner_down_payment_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_delete($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->key, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_partner_down_payment_delete", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

//    public function get_bank_list() {
//        try {
//            return parent::execute_sp_single_query("sp_get_bank_list");
//        } catch (Exception $ex) {
//            parent::handle_database_error($ex);
//        }
//    }

}

?>