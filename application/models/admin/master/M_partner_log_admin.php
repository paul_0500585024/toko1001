<?php

require_once MODEL_BASE;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_partner_log
 *
 * @author Jartono
 */
class M_partner_log_admin extends ModelBase {

    public function get_list($filter) {

        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->partner_seq, TYPE_NUMERIC);
        $params[] = array($filter->status, TYPE_STRING);


        try {
            return parent::execute_sp_multi_query("sp_partner_log_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->partner_seq, TYPE_NUMERIC);
        $params[] = array($selected->seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_partner_log_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_approve($selected) {

        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->partner_seq, TYPE_NUMERIC);
        $params[] = array($selected->seq, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_partner_log_approve_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_reject($selected) {

        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->partner_seq, TYPE_NUMERIC);
        $params[] = array($selected->seq, TYPE_NUMERIC);

        try {
            return parent::execute_non_query("sp_partner_log_reject_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->partner_seq, TYPE_NUMERIC);
        $params[] = array($selected->seq, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_partner_log_data_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_info($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->partner_seq, TYPE_NUMERIC);


        try {
            parent::execute_non_query("sp_partner_log_info_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data_detail($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->partner_seq, TYPE_NUMERIC);
        $params[] = array($selected->log_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_partner_log_data_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>