<?php

require_once MODEL_BASE;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of M_partner_group
 *
 * @author Jaka
 */
class M_partner_group extends ModelBase {

    public function get_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->name, TYPE_STRING);

        try {
            return parent::execute_sp_multi_query("sp_partner_group_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_partner_group_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->partner_group_name, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_partner_group_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->partner_group_name, TYPE_STRING);
        $params[] = array($selected->partner_group_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_partner_group_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_delete($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);
        try {
            return parent::execute_non_query("sp_partner_group_delete", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_commission_list($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_partner_group_commission_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_detail($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        
        $this->save_delete_detail($data);
        
        for ($i = 0; $i < count($data->lvlcat); $i++) {
            $level = explode("~", $data->lvlcat[$i]);
            $params[] = array($level[2], TYPE_NUMERIC);
            $params[] = array($level[0], TYPE_NUMERIC);
            $params[] = array($data->mfee[$level[1]], TYPE_NUMERIC);
            try {
                parent::execute_non_query("sp_partner_group_commission_detail_update", $params);
            } catch (Exception $ex) {
                parent::handle_database_error($ex);
            }
            array_pop($params);
            array_pop($params);
            array_pop($params);
        }
    }

    public function save_delete_detail($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        try {
            parent::execute_non_query("sp_partner_group_commission_detail_delete", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}
