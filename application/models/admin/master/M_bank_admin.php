<?php

require_once MODEL_BASE;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Bank Account
 *
 * @author Jartono
 */
class M_bank_admin extends ModelBase {

    public function get_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->bank_name, TYPE_STRING);
        $params[] = array($filter->bank_branch_name, TYPE_STRING);
        $params[] = array($filter->bank_acct_no, TYPE_STRING);
        $params[] = array($filter->bank_acct_name, TYPE_STRING);
        $params[] = array($filter->active, TYPE_BOOLEAN);
        try {
            return parent::execute_sp_multi_query("sp_bank_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_bank_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->bank_name, TYPE_STRING);
        $params[] = array($data->bank_branch_name, TYPE_STRING);
        $params[] = array($data->bank_acct_no, TYPE_STRING);
        $params[] = array($data->bank_acct_name, TYPE_STRING);
        $params[] = array($data->active, TYPE_BOOLEAN);
        $params[] = array($data->logo_img, TYPE_STRING);

        try {
            parent::execute_non_query("sp_bank_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->bank_name, TYPE_STRING);
        $params[] = array($data->bank_branch_name, TYPE_STRING);
        $params[] = array($data->bank_acct_no, TYPE_STRING);
        $params[] = array($data->bank_acct_name, TYPE_STRING);
        $params[] = array($data->active, TYPE_BOOLEAN);
        $params[] = array($data->logo_img, TYPE_STRING);

        try {
            parent::execute_non_query("sp_bank_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_delete($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->key, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_bank_delete", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

//    public function get_bank_list() {
//        try {
//            return parent::execute_sp_single_query("sp_get_bank_list");
//        } catch (Exception $ex) {
//            parent::handle_database_error($ex);
//        }
//    }

}

?>