<?php

require_once MODEL_BASE;

class M_agent_admin extends ModelBase {

    public function get_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->email, TYPE_STRING);
        $params[] = array($filter->name, TYPE_STRING);
        $params[] = array($filter->status, TYPE_STRING);


        try {
            return parent::execute_sp_multi_query("sp_agent_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_agent_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_approve($selected) {

        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);
        $params[] = array($selected->new_password, TYPE_STRING);

        try {
            parent::execute_non_query("sp_agent_approve_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->email, TYPE_STRING);
        $params[] = array($data->name, TYPE_STRING);
        $params[] = array($data->birthday, TYPE_DATE);
        $params[] = array($data->gender, TYPE_STRING);
        $params[] = array($data->mobile_phone, TYPE_STRING);
        $params[] = array($data->password, TYPE_STRING);
        $params[] = array($data->profile_img, TYPE_STRING);

        try {
            parent::execute_non_query("sp_agent_add_signup", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->name, TYPE_STRING);
        $params[] = array($data->gender, TYPE_STRING);
        $params[] = array($data->birthday, TYPE_DATE);
        $params[] = array($data->mobile_phone, TYPE_STRING);
        try {
            parent::execute_non_query("sp_agent_update_contact", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function check_user_exist($email) {
        $params[] = array($email->email, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_check_agent_exist", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }

        return true;
    }

    public function save_delete($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->key, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_agent_delete", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_settings($data) {
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->agent_seq, TYPE_NUMERIC);
        $params[] = array($data->encrypt_new_password, TYPE_STRING);

        try {
            return parent::execute_non_query("sp_agent_update_settings", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_profile($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->agent_seq, TYPE_NUMERIC);
        $params[] = array($filter->profile_img, TYPE_STRING);
        try {
            parent::execute_non_query("sp_agent_update_profile_image", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_status($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->key, TYPE_NUMERIC);
        $params[] = array($data->status, TYPE_STRING);

        try {
            parent::execute_non_query("sp_agent_update_status", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>