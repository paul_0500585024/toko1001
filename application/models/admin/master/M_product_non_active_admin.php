<?php

require_once MODEL_BASE;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Product New
 *
 * @author Jartono
 */
class M_product_non_active_admin extends ModelBase {

    public function get_list($filter) {
	$params[] = array($filter->user_id, TYPE_STRING);        
	$params[] = array($filter->ip_address, TYPE_STRING);
	$params[] = array($filter->start, TYPE_NUMERIC);
	$params[] = array($filter->length, TYPE_NUMERIC);
	$params[] = array($filter->order, TYPE_STRING);
	$params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->merchant_seq, TYPE_NUMERIC);
	$params[] = array($filter->name, TYPE_STRING);
	$params[] = array($filter->status, TYPE_STRING);
        $params[] = array($filter->active, TYPE_BOOLEAN);                
	try {
	    return parent::execute_sp_multi_query("sp_product_non_active_list_all", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_data($selected) {
	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->seq, TYPE_NUMERIC);          
	try {
	    return parent::execute_sp_single_query("sp_product_non_active_by_variant_seq_admin", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }
     public function get_merchant_by_name($selected) {
	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->merchant_name, TYPE_STRING);
        
	try {
	    return parent::execute_sp_single_query("sp_merchant_by_name", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

}

?>