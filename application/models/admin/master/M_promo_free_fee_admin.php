<?php

require_once MODEL_BASE;

class M_promo_free_fee_admin extends ModelBase {

    public function get_list($filter) {
	$params[] = array($filter->user_id, TYPE_STRING);
	$params[] = array($filter->ip_address, TYPE_STRING);
	$params[] = array($filter->start, TYPE_NUMERIC);
	$params[] = array($filter->length, TYPE_NUMERIC);
	$params[] = array($filter->order, TYPE_STRING);
	$params[] = array($filter->column, TYPE_STRING);
	$params[] = array($filter->notes, TYPE_STRING);
	$params[] = array($filter->active, TYPE_BOOLEAN);
	$params[] = array($filter->status, TYPE_STRING);
	try {
	    return parent::execute_sp_multi_query("sp_promo_free_fee_list", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_data($selected) {
	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->key, TYPE_NUMERIC);

	try {
	    return parent::execute_sp_single_query("sp_promo_free_fee_by_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_add($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->date_from, TYPE_DATE);
	$params[] = array($data->date_to, TYPE_DATE);
	$params[] = array($data->all_origin_city, TYPE_BOOLEAN);
	$params[] = array($data->all_destination_city, TYPE_BOOLEAN);
	$params[] = array($data->notes, TYPE_STRING);
	$params[] = array($data->active, TYPE_BOOLEAN);

	try {
	    parent::execute_non_query("sp_promo_free_fee_add", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_update($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	$params[] = array($data->date_from, TYPE_DATE);
	$params[] = array($data->date_to, TYPE_DATE);
	$params[] = array($data->all_origin_city, TYPE_BOOLEAN);
	$params[] = array($data->all_destination_city, TYPE_BOOLEAN);
	$params[] = array($data->notes, TYPE_STRING);
	$params[] = array($data->active, TYPE_BOOLEAN);

	try {
	    parent::execute_non_query("sp_promo_free_fee_update", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_status($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	$params[] = array($data->status, TYPE_STRING);
	try {
	    parent::execute_non_query("sp_promo_free_fee_status", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_delete($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->key, TYPE_NUMERIC);

	try {
	    parent::execute_non_query("sp_promo_free_fee_delete", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_list_detail($filter) {
	$params[] = array($filter->user_id, TYPE_STRING);
	$params[] = array($filter->ip_address, TYPE_STRING);
	$params[] = array($filter->hseq, TYPE_NUMERIC);

	try {
	    return parent::execute_sp_multi_query("sp_promo_free_fee_city_list", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_add_detail($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->promo_seq, TYPE_NUMERIC);
	$this->save_delete_detail($data);
	$params[] = array("O", TYPE_STRING);
	foreach ($data->city_seq_o as $item) {
	    $params[] = array($item, TYPE_NUMERIC);
	    try {
		parent::execute_non_query("sp_promo_free_fee_city_add", $params);
	    } catch (Exception $ex) {
		parent::handle_database_error($ex);
	    }
	    array_pop($params);
	}
	array_pop($params);
	$params[] = array("D", TYPE_STRING);
	foreach ($data->city_seq_d as $item) {
	    $params[] = array($item, TYPE_NUMERIC);
	    try {
		parent::execute_non_query("sp_promo_free_fee_city_add", $params);
	    } catch (Exception $ex) {
		parent::handle_database_error($ex);
	    }
	    array_pop($params);
	}
    }

    public function save_delete_detail($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->promo_seq, TYPE_NUMERIC);

	try {
	    parent::execute_non_query("sp_promo_free_fee_city_delete", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_list_merchant($filter) {
	$params[] = array($filter->user_id, TYPE_STRING);
	$params[] = array($filter->ip_address, TYPE_STRING);
	$params[] = array($filter->hseq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_promo_free_fee_merchant_list", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_add_merchant($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->promo_seq, TYPE_NUMERIC);
	$this->save_delete_merchant($data);
	foreach ($data->merchant_seq as $item) {
	    $params[] = array($item, TYPE_NUMERIC);
	    try {
		parent::execute_non_query("sp_promo_free_fee_merchant_add", $params);
	    } catch (Exception $ex) {
		parent::handle_database_error($ex);
	    }
	    array_pop($params);
	}
    }

    public function save_delete_merchant($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->promo_seq, TYPE_NUMERIC);

	try {
	    parent::execute_non_query("sp_promo_free_fee_merchant_delete", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

}

?>