<?php

require_once MODEL_BASE;

class M_image_promo_subscribe_admin extends ModelBase {

    public function get_list($filter) {
	$params[] = array($filter->user_id, TYPE_STRING);
	$params[] = array($filter->ip_address, TYPE_STRING);
	$params[] = array($filter->start, TYPE_NUMERIC);
	$params[] = array($filter->length, TYPE_NUMERIC);
	$params[] = array($filter->order, TYPE_STRING);
	$params[] = array($filter->column, TYPE_STRING);
	$params[] = array($filter->promo, TYPE_STRING);
	if ($filter->promo_img_from_date == "") {
	    $params[] = array($filter->promo_img_from_date, TYPE_STRING);
	} else {
	    $params[] = array($filter->promo_img_from_date, TYPE_DATE);
	}
	if ($filter->promo_img_to_date == "") {
	    $params[] = array($filter->promo_img_to_date, TYPE_STRING);
	} else {
	    $params[] = array($filter->promo_img_to_date, TYPE_DATE);
	}
	$params[] = array($filter->status, TYPE_STRING);

	try {
	    return parent::execute_sp_multi_query("sp_image_promo_subscribe_list", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_data($filter) {
	$params[] = array($filter->user_id, TYPE_STRING);
	$params[] = array($filter->ip_address, TYPE_STRING);
	$params[] = array($filter->seq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_image_promo_subscribe_by_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_delete($filter) {
	$params[] = array($filter->user_id, TYPE_STRING);
	$params[] = array($filter->ip_address, TYPE_STRING);
	$params[] = array($filter->seq, TYPE_NUMERIC);
	try {
	    parent::execute_non_query("sp_image_promo_subscribe_delete", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_status($filter) {
	$params[] = array($filter->user_id, TYPE_STRING);
	$params[] = array($filter->ip_address, TYPE_STRING);
	$params[] = array($filter->seq, TYPE_NUMERIC);
	$params[] = array($filter->status, TYPE_STRING);
	try {
	    parent::execute_non_query("sp_image_promo_subscribe_status_update", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_add($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->promo_name, TYPE_STRING);
	$params[] = array($data->promo_img_from_date, TYPE_DATE);
	$params[] = array($data->promo_img_to_date, TYPE_DATE);
	$params[] = array($data->promo_img_text, TYPE_STRING);
	try {
	    parent::execute_non_query("sp_image_promo_subscribe_add", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_update($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	$params[] = array($data->promo_name, TYPE_STRING);
	$params[] = array($data->promo_img_from_date, TYPE_DATE);
	$params[] = array($data->promo_img_to_date, TYPE_DATE);
	$params[] = array($data->promo_img_text, TYPE_STRING);
	try {
	    parent::execute_non_query("sp_image_promo_subscribe_update", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

}

?>