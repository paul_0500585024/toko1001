<?php

require_once MODEL_BASE;

class M_member_review_admin extends ModelBase {

    public function get_list($filter) {
	$params[] = array($filter->user_id, TYPE_STRING);
	$params[] = array($filter->ip_address, TYPE_STRING);
	$params[] = array($filter->start, TYPE_NUMERIC);
	$params[] = array($filter->length, TYPE_NUMERIC);
	$params[] = array($filter->order, TYPE_STRING);
	$params[] = array($filter->column, TYPE_STRING);
//	$params[] = array($filter->product, TYPE_STRING);
//	$params[] = array($filter->order_no, TYPE_STRING);
//	$params[] = array($filter->user, TYPE_STRING);
	$params[] = array($filter->status, TYPE_STRING);
	$params[] = array(0, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_multi_query("sp_review_product_list_admin", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_data($selected) {
	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->order_seq, TYPE_NUMERIC);
	$params[] = array($selected->product_variant_seq, TYPE_NUMERIC);

	try {
	    return parent::execute_sp_single_query("sp_review_product_admin_by_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_update($selected) {
	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->order_seq, TYPE_NUMERIC);
	$params[] = array($selected->product_variant_seq, TYPE_NUMERIC);
	$params[] = array($selected->review_admin, TYPE_STRING);
	try {
	    parent::execute_non_query("sp_review_product_admin_update", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_status($selected) {
	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->order_seq, TYPE_NUMERIC);
	$params[] = array($selected->product_variant_seq, TYPE_NUMERIC);
	$params[] = array($selected->status, TYPE_STRING);
	$params[] = array($selected->review_admin, TYPE_STRING);
	try {
	    parent::execute_non_query("sp_review_product_admin_status", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_rate($selected) {
	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->product_variant_seq, TYPE_NUMERIC);
	$params[] = array($selected->colom, TYPE_STRING);
	try {
	    parent::execute_non_query("sp_review_product_admin_rate", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

}

?>