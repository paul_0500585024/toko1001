<?php

require_once MODEL_BASE;

class M_change_password extends ModelBase {

    public function get_info($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->old_password, TYPE_STRING);
        $params[] = array($data->new_password, TYPE_STRING);
        $params[] = array($data->encrypt_old_password, TYPE_STRING);
        $params[] = array($data->encrypt_new_password, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_get_info_password", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->encrypt_new_password, TYPE_STRING);
        try {
            parent::execute_non_query("sp_change_password", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>