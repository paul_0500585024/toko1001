<?php

require_once MODEL_BASE;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Merchant New
 *
 * @author Jartono
 */
class M_logo_merchant extends ModelBase {

    public function get_data($selected) {

	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->key, TYPE_NUMERIC);

	try {
	    return parent::execute_sp_single_query("sp_logo_merchant_by_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_update($data) {
	$params[] = array($data->userid, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	$params[] = array($data->banner_img, TYPE_STRING);
	$params[] = array($data->logo_img, TYPE_STRING);
	$params[] = array($data->description, TYPE_STRING);

	try {
	    parent::execute_non_query("sp_logo_merchant_update", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

}

?>