<?php

require_once MODEL_BASE;

class M_profile_merchant extends ModelBase {

    public function get_data_merchant($data) {
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->merchant_seq, TYPE_NUMERIC);

	try {
	    return parent::execute_sp_multi_query("sp_get_data_merchant", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

//    public function get_merchant_info($data) {
//        $params[] = array($data->merchant_seq, TYPE_NUMERIC);
//
//        try {
//            return parent::execute_sp_single_query("sp_get_merchant_info", $params);
//        } catch (Exception $ex) {
//            parent::handle_database_error($ex);
//        }
//    }
//
//    public function get_main_page_merchant($data){
//        $params[] = array($data->merchant_seq, TYPE_NUMERIC);
//        $params[] = array($data->order_status, TYPE_STRING);
//
//        try {
//            return parent::execute_sp_multi_query("sp_main_page_merchant", $params);
//        } catch (Exception $ex) {
//            parent::handle_database_error($ex);
//        }
//    }
}

?>
