<?php

require_once MODEL_BASE;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Product New
 *
 * @author Jartono
 */
class M_order_merchant extends ModelBase {

    public function get_list($filter) {
	$params[] = array($filter->user_id, TYPE_STRING);
	$params[] = array($filter->ip_address, TYPE_STRING);
	$params[] = array($filter->start, TYPE_NUMERIC);
	$params[] = array($filter->length, TYPE_NUMERIC);
	$params[] = array($filter->order, TYPE_STRING);
	$params[] = array($filter->column, TYPE_STRING);
	$params[] = array($filter->mt_seq, TYPE_NUMERIC);
	$params[] = array($filter->order_no_f, TYPE_STRING);
	if ($filter->order_date_s == "") {
	    $params[] = array($filter->order_date_s, TYPE_STRING);
	} else {
	    $params[] = array($filter->order_date_s, TYPE_DATE);
	}
	if ($filter->order_date_e == "") {
	    $params[] = array($filter->order_date_e, TYPE_STRING);
	} else {
	    $params[] = array($filter->order_date_e, TYPE_DATE);
	}
	$params[] = array($filter->receiver_name_f, TYPE_STRING);
	$params[] = array($filter->status_order_f, TYPE_STRING);
	$params[] = array($filter->status_payment_f, TYPE_STRING);
	try {
	    return parent::execute_sp_multi_query("sp_merchant_order_list", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_order_list_detail($filter) {

	$params[] = array($filter->user_id, TYPE_STRING);
	$params[] = array($filter->ip_address, TYPE_STRING);
	$params[] = array($filter->merchant_seq, TYPE_NUMERIC);
	$params[] = array($filter->start, TYPE_NUMERIC);
	$params[] = array($filter->length, TYPE_NUMERIC);
	$params[] = array($filter->order, TYPE_STRING);
	$params[] = array($filter->column, TYPE_STRING);
	$params[] = array($filter->hseq, TYPE_STRING);

	try {
	    return parent::execute_sp_multi_query("sp_merchant_order_list_detail", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_data($selected) {

	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->seq, TYPE_NUMERIC);

	try {
	    return parent::execute_sp_single_query("sp_product_by_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_order_info($selected) {
	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->key, TYPE_NUMERIC);

	try {
	    return parent::execute_sp_single_query("sp_member_order_list_by_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

}

?>