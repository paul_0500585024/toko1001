<?php

require_once MODEL_BASE;

class M_trans_log_merchant extends ModelBase {

    public function save_merchant_change_password_log($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->merchant_seq, TYPE_NUMERIC);
	$params[] = array($data->old_password, TYPE_STRING);

	try {
	    parent::execute_non_query("sp_merchant_change_password_log", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_merchant_stock_log($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->product_variant_seq, TYPE_NUMERIC);
	$params[] = array($data->variant_value_seq, TYPE_NUMERIC);
	$params[] = array($data->qty, TYPE_NUMERIC);
	$params[] = array($data->mutation_type, TYPE_STRING);
	$params[] = array($data->trx_type, TYPE_STRING);
	$params[] = array($data->trx_no, TYPE_STRING);

	try {
	    parent::execute_non_query("sp_product_stock_log_add", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

}

?>