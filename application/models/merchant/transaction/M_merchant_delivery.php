<?php

require_once MODEL_BASE;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Merchant Delivery
 *
 * @author Jartono
 */
class M_merchant_delivery extends ModelBase {

    public function get_list($filter) {
	$params[] = array($filter->user_id, TYPE_STRING);
	$params[] = array($filter->ip_address, TYPE_STRING);
	$params[] = array($filter->start, TYPE_NUMERIC);
	$params[] = array($filter->length, TYPE_NUMERIC);
	$params[] = array($filter->order, TYPE_STRING);
	$params[] = array($filter->column, TYPE_STRING);
	$params[] = array($filter->merchant_id, TYPE_STRING);
	$params[] = array($filter->order_no_f, TYPE_STRING);
	if ($filter->order_date_s == "") {
	    $params[] = array($filter->order_date_s, TYPE_STRING);
	} else {
	    $params[] = array($filter->order_date_s, TYPE_DATE);
	}
	if ($filter->order_date_e == "") {
	    $params[] = array($filter->order_date_e, TYPE_STRING);
	} else {
	    $params[] = array($filter->order_date_e, TYPE_DATE);
	}
	$params[] = array($filter->status_order_f, TYPE_STRING);
	try {
	    return parent::execute_sp_multi_query("sp_merchant_delivery_list", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_data($selected) {

	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->seq, TYPE_NUMERIC);
	$params[] = array($selected->merchant_seq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_multi_query("sp_merchant_delivery_by_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_data_order_merchant($selected) {

	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->seq, TYPE_NUMERIC);
	$params[] = array($selected->merchant_seq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_merchant_delivery_order_by_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_dropdown_expedition() {
	try {
	    return parent::execute_sp_single_query("sp_get_dropdown_expedition");
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_location($selected) {

	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->pseq, TYPE_NUMERIC);
	$params[] = array($selected->cseq, TYPE_NUMERIC);
	$params[] = array($selected->dseq, TYPE_NUMERIC);

	try {
	    return parent::execute_sp_single_query("sp_get_location", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_merchant($selected) {

	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->merchant_seq, TYPE_NUMERIC);

	try {
	    return parent::execute_sp_single_query("sp_merchant_by_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_data_product($selected, $status = '') {

	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->seq, TYPE_NUMERIC);
	$params[] = array($selected->merchant_seq, TYPE_NUMERIC);
	$params[] = array($status, TYPE_STRING);

	try {
	    return parent::execute_sp_single_query("sp_merchant_delivery_product_by_order", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_update_merchant_awb_local($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	$params[] = array($data->merchant_seq, TYPE_NUMERIC);
	$params[] = array($data->new_awb_seq, TYPE_NUMERIC);
	$params[] = array($data->new_awb, TYPE_STRING);
	try {
	    parent::execute_non_query("sp_merchant_deliver_update_awb", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_update_voucher_status($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	$params[] = array($data->newvoucherseq, TYPE_NUMERIC);
	try {
	    parent::execute_non_query("sp_merchant_delivery_update_voucher_status", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_update_coupon_status($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	try {
	    parent::execute_non_query("sp_merchant_delivery_update_coupon_status", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_update_order_merchant_print($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	$params[] = array($data->merchant_seq, TYPE_NUMERIC);
	try {
	    parent::execute_non_query("sp_merchant_delivery_update_print_status", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_update($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	$params[] = array($data->merchant_seq, TYPE_NUMERIC);
	$params[] = array($data->ship_notes, TYPE_STRING);
	$params[] = array($data->order_status, TYPE_STRING);
	try {
	    parent::execute_non_query("sp_merchant_delivery_update", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_update_order_merchant($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	$params[] = array($data->merchant_seq, TYPE_NUMERIC);
	$params[] = array($data->new_sell_price, TYPE_NUMERIC);
	$params[] = array($data->new_ship_price_real, TYPE_NUMERIC);
	$params[] = array($data->new_ship_price_charged, TYPE_NUMERIC);
	$params[] = array($data->new_ins_rate_percent, TYPE_NUMERIC);
	try {
	    parent::execute_non_query("sp_merchant_delivery_order_merchant_update", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_update_product($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	$params[] = array($data->merchant_seq, TYPE_NUMERIC);
	for ($i = 0; $i < count($data->product_variant_seq); $i++) {
	    if ($data->product_variant_seq[$i] != '') {
		$params[] = array($data->product_variant_seq[$i], TYPE_NUMERIC);
		if ($data->product_status[$i] == "R") {
		    $prdsts = "R";
		} else {
		    $prdsts = "X";
		}
		$params[] = array($prdsts, TYPE_STRING);
		try {
		    parent::execute_non_query("sp_merchant_delivery_product_update", $params);
		} catch (Exception $ex) {
		    parent::handle_database_error($ex);
		}
		array_pop($params);
		array_pop($params);
	    }
	}
    }

    public function save_resi($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	$params[] = array($data->merchant_seq, TYPE_NUMERIC);
	$params[] = array($data->awb_no, TYPE_STRING);
	$params[] = array($data->ship_by, TYPE_STRING);
	$params[] = array($data->ship_by_exp_seq, TYPE_NUMERIC);
	$params[] = array($data->ship_date, TYPE_DATE);
	$params[] = array($data->ship_note_file, TYPE_STRING);
	try {
	    parent::execute_non_query("sp_merchant_deliver_save_awb_no", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_setting($selected) {

	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->settingseq, TYPE_NUMERIC);

	try {
	    return parent::execute_sp_single_query("sp_setting_by_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_expedition($selected) {

	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->seq, TYPE_NUMERIC);
	$params[] = array($selected->merchant_seq, TYPE_NUMERIC);

	try {
	    return parent::execute_sp_single_query("sp_merchant_delivery_ekspedisi_merchant", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_location_dtl($distrikid) {
	$params[] = array($distrikid, TYPE_NUMERIC);

	try {
	    return parent::execute_sp_single_query("sp_get_name_district_city_province", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_local_awb($selected) {
	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->exp_seq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_merchant_delivery_get_local_awb", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_data_promo_voucher($selected) {
	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->voucherseq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_promo_voucher_by_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_data_promo_coupon($selected) {
	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->seq, TYPE_NUMERIC);
	$params[] = array($selected->couponseq, TYPE_NUMERIC);
	$params[] = array($selected->statusproduct, TYPE_STRING);
	$params[] = array($selected->merchant_seq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_merchant_delivery_check_coupon", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_data_product_status($selected, $status = '') {

	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->seq, TYPE_NUMERIC);
	$params[] = array($selected->merchant_seq, TYPE_NUMERIC);
	$params[] = array($status, TYPE_STRING);

	try {
	    return parent::execute_sp_single_query("sp_merchant_delivery_product_by_order_status", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_expedition_info($data) {

	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->merchant_seq, TYPE_NUMERIC);
	$params[] = array($data->district_seq, TYPE_NUMERIC);
	$params[] = array($data->exp_seq, TYPE_NUMERIC);

	try {
	    return parent::execute_sp_single_query("sp_get_expedition_info", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_total_product($data) {

	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	$params[] = array($data->merchant_seq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_merchant_delivery_product_sum_by_merchant", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_update_awb($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	$params[] = array($data->merchant_seq, TYPE_NUMERIC);
	$params[] = array($data->awb_no, TYPE_STRING);
	try {
	    parent::execute_non_query("sp_merchant_deliver_update_api_awb_no", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_data_member($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->member_seq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_member_by_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }
    
     public function get_data_agent($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->agent_seq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_agent_by_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_data_voucher($data) {

	$params[] = array($data->old_voucher, TYPE_NUMERIC);
	$params[] = array($data->new_voucher, TYPE_NUMERIC);

	try {
	    return parent::execute_sp_single_query("sp_get_data_voucher", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

}

?>