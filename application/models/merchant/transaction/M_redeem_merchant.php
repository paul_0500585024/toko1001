<?php

require_once MODEL_BASE;

class M_redeem_merchant extends ModelBase {

    public function get_list($filter) {
	$params[] = array($filter->user_id, TYPE_STRING);
	$params[] = array($filter->ip_address, TYPE_STRING);
	$params[] = array($filter->start, TYPE_NUMERIC);
	$params[] = array($filter->length, TYPE_NUMERIC);
	$params[] = array($filter->order, TYPE_STRING);
	$params[] = array($filter->column, TYPE_STRING);
	$params[] = array($filter->merchant_id, TYPE_STRING);
	if ($filter->from_date == "") {
	    $params[] = array($filter->from_date, TYPE_STRING);
	} else {
	    $params[] = array($filter->from_date, TYPE_DATE);
	}
	if ($filter->to_date == "") {
	    $params[] = array($filter->to_date, TYPE_STRING);
	} else {
	    $params[] = array($filter->to_date, TYPE_DATE);
	}
	try {
	    return parent::execute_sp_multi_query("sp_merchant_redeem_list", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_data($filter) {
	$params[] = array($filter->user_id, TYPE_STRING);
	$params[] = array($filter->ip_address, TYPE_STRING);
	$params[] = array($filter->seq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_redeem_period_by_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_list_detail($filter) {
	$params[] = array($filter->user_id, TYPE_STRING);
	$params[] = array($filter->ip_address, TYPE_STRING);
	$params[] = array($filter->start, TYPE_NUMERIC);
	$params[] = array($filter->length, TYPE_NUMERIC);
	$params[] = array($filter->order, TYPE_STRING);
	$params[] = array($filter->column, TYPE_STRING);
	$params[] = array($filter->seq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_multi_query("sp_redeem_merchant_list", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_list_detail_status($filter) {
	$params[] = array($filter->user_id, TYPE_STRING);
	$params[] = array($filter->ip_address, TYPE_STRING);
	$params[] = array($filter->seq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_redeem_merchant_status", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_data_detail($filter) {
	$params[] = array($filter->user_id, TYPE_STRING);
	$params[] = array($filter->ip_address, TYPE_STRING);
	$params[] = array($filter->seq, TYPE_NUMERIC);
	$params[] = array($filter->merchant_seq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_redeem_merchant_by_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_last_date($filter) {
	$params[] = array($filter->user_id, TYPE_STRING);
	$params[] = array($filter->ip_address, TYPE_STRING);
	try {
	    return parent::execute_sp_single_query("sp_redeem_period_get_lastdate", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_order_merchant($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->to_date, TYPE_DATE);
	try {
	    return parent::execute_sp_single_query("sp_redeem_merchant_get_order", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_expedition_invoice($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_redeem_merchant_get_expedition_invoice", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_redeem_component($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_redeem_component_sum_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_redeem_merchant_status($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_STRING);
	try {
	    return parent::execute_sp_single_query("sp_redeem_merchant_list_status", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_redeem_component_by_merchant($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	$params[] = array($data->merchant_seq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_redeem_component_by_seq_merchant", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_order_merchant_by_redeem($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	$params[] = array($data->merchant_seq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_order_merchant_by_redeem", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_expedition_merchant_by_redeem($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	$params[] = array($data->merchant_seq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_expedition_merchant_by_redeem", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

}

?>