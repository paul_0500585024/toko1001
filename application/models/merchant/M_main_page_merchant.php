<?php

require_once MODEL_BASE;

class M_main_page_merchant extends ModelBase {

    public function get_main_page_merchant($data) {

        $params[] = array($data->new_product, TYPE_STRING);
        $params[] = array($data->product_change, TYPE_STRING);
        $params[] = array($data->merchant_seq, TYPE_NUMERIC);
        $params[] = array($data->order_status, TYPE_STRING);

        try {
            return parent::execute_sp_multi_query("sp_main_page_merchant", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }
    
    public function charts_order($data) {

        $params[] = array($data->merchant_seq, TYPE_NUMERIC);
        $params[] = array($data->january_from, TYPE_DATE);
        $params[] = array($data->january_to, TYPE_DATE);
        $params[] = array($data->february_from, TYPE_DATE);
        $params[] = array($data->february_to, TYPE_DATE);
        $params[] = array($data->march_from, TYPE_DATE);
        $params[] = array($data->march_to, TYPE_DATE);
        $params[] = array($data->april_from, TYPE_DATE);
        $params[] = array($data->april_to, TYPE_DATE);
        $params[] = array($data->may_from, TYPE_DATE);
        $params[] = array($data->may_to, TYPE_DATE);
        $params[] = array($data->june_from, TYPE_DATE);
        $params[] = array($data->june_to, TYPE_DATE);
        $params[] = array($data->july_from, TYPE_DATE);
        $params[] = array($data->july_to, TYPE_DATE);
        $params[] = array($data->august_from, TYPE_DATE);
        $params[] = array($data->august_to, TYPE_DATE);
        $params[] = array($data->septebmer_from, TYPE_DATE);
        $params[] = array($data->septebmer_to, TYPE_DATE);
        $params[] = array($data->october_from, TYPE_DATE);
        $params[] = array($data->october_to, TYPE_DATE);
        $params[] = array($data->november_from, TYPE_DATE);
        $params[] = array($data->november_to, TYPE_DATE);
        $params[] = array($data->december_from, TYPE_DATE);
        $params[] = array($data->december_to, TYPE_DATE);

        try {
            return parent::execute_sp_multi_query("sp_get_chart_order", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>