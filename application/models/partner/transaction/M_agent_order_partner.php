<?php

require_once MODEL_BASE;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of partner Delivery
 *
 * @author Jartono
 */
class M_agent_order_partner extends ModelBase {

    public function get_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->sort, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->partner_seq, TYPE_NUMERIC);
        $params[] = array($filter->agent_seq, TYPE_NUMERIC);
        $params[] = array($filter->order_no_f, TYPE_STRING);
        if ($filter->order_date_s == "") {
            $params[] = array($filter->order_date_s, TYPE_STRING);
        } else {
            $params[] = array($filter->order_date_s, TYPE_DATE);
        }
        if ($filter->order_date_e == "") {
            $params[] = array($filter->order_date_e, TYPE_STRING);
        } else {
            $params[] = array($filter->order_date_e, TYPE_DATE);
        }
        $params[] = array($filter->payment_status, TYPE_STRING);

        try {
            return parent::execute_sp_multi_query("sp_partner_order_list_agent", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($selected) {

        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->seq, TYPE_NUMERIC);
        $params[] = array($selected->partner_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_multi_query("sp_partner_order_list_agent_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data_status($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->sort, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->partner_seq, TYPE_NUMERIC);
        $params[] = array($filter->agent_seq, TYPE_NUMERIC);
        $params[] = array($filter->order_no_f, TYPE_STRING);
        if ($filter->order_date_s == "") {
            $params[] = array($filter->order_date_s, TYPE_STRING);
        } else {
            $params[] = array($filter->order_date_s, TYPE_DATE);
        }
        if ($filter->order_date_e == "") {
            $params[] = array($filter->order_date_e, TYPE_STRING);
        } else {
            $params[] = array($filter->order_date_e, TYPE_DATE);
        }
        $params[] = array($filter->payment_status, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_agent_partner_order_status", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_location($selected) {

        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->pseq, TYPE_NUMERIC);
        $params[] = array($selected->cseq, TYPE_NUMERIC);
        $params[] = array($selected->dseq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_get_location", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_expedition() {
        try {
            return parent::execute_sp_single_query("sp_get_dropdown_expedition");
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_agent($selected) {

        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->partner_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_agent_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_order_product($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->partner_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_partner_order_list_agent_product", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_agent_dropdown($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->partner_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_agent_list_partner_no_paging", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function update_status($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->seq, TYPE_NUMERIC);
        $params[] = array($selected->status, TYPE_STRING);
        $params[] = array($selected->po_no, TYPE_STRING);

        try {
            parent::execute_non_query("sp_agent_update_status_order_loan", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_member_order_adira($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_get_member_order_adira", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_partner_order_by_status($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->pg_method_seq, TYPE_STRING);
        $params[] = array($selected->partner_seq, TYPE_NUMERIC);
        $params[] = array($selected->payment_status, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_get_partner_order_by_status", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>