<?php

require_once MODEL_BASE;

class M_redeem_partner extends ModelBase {
    
    public function get_list($filter){
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->partner_seq, TYPE_NUMERIC);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        if ($filter->from_date == "") {
            $params[] = array($filter->from_date, TYPE_STRING);
        } else {
            $params[] = array($filter->from_date, TYPE_DATE);
        }
        if ($filter->to_date == "") {
            $params[] = array($filter->to_date, TYPE_STRING);
        } else {
            $params[] = array($filter->to_date, TYPE_DATE);
        }
        $params[] = array($filter->status, TYPE_STRING);
        $params[] = array($filter->paid_date_month, TYPE_NUMERIC);
        $params[] = array($filter->paid_date_year, TYPE_NUMERIC);
        try {            
            return parent::execute_sp_multi_query("sp_redeem_agent_per_partner_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }        
    }

    public function get_list_excel($filter){
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->partner_seq, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        if ($filter->from_date == "") {
            $params[] = array($filter->from_date, TYPE_STRING);
        } else {
            $params[] = array($filter->from_date, TYPE_DATE);
        }
        if ($filter->to_date == "") {
            $params[] = array($filter->to_date, TYPE_STRING);
        } else {
            $params[] = array($filter->to_date, TYPE_DATE);
        }
        $params[] = array($filter->status, TYPE_STRING);
        $params[] = array($filter->paid_date_month, TYPE_NUMERIC);
        $params[] = array($filter->paid_date_year, TYPE_NUMERIC);

        try {            
            return parent::execute_sp_single_query("sp_redeem_agent_per_partner_list_no_limit", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }        
    }
    
    
    public function get_list_detail($filter) {

        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->partner_seq, TYPE_NUMERIC);
        $params[] = array($filter->redeem_agent_seq, TYPE_NUMERIC);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        try {
            return parent::execute_sp_multi_query("sp_redeem_agent_per_partner_list_by_agent_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }
    
    public function get_list_detail_excel($filter){
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->partner_seq, TYPE_NUMERIC);
        $params[] = array($filter->redeem_agent_seq, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_redeem_agent_per_partner_list_by_agent_seq_no_limit", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }
    
    
    public function get_detail_redeem_per_agent_only($filter)
    {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->partner_seq, TYPE_NUMERIC);
        $params[] = array($filter->redeem_agent_seq, TYPE_NUMERIC);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        try {
            return parent::execute_sp_multi_query("sp_list_redem_agent_by_agent_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }
    
    
    public function save_paid_redeem_to_agent($data){
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->partner_redeem_seq, TYPE_NUMERIC);
        $params[] = array($data->agent_selected, TYPE_NUMERIC);
        $params[] = array($data->paid_date, TYPE_DATE);
        $params[] = array($data->status, TYPE_STRING);
        try {
            parent::execute_non_query("sp_set_redeem_partner_to_agent", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }
    
    
    
}