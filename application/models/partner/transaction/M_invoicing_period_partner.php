<?php
require_once MODEL_BASE;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of partner Delivery
 *
 * @author Jartono
 */
class M_invoicing_period_partner extends ModelBase {

    public function get_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->partner_seq, TYPE_NUMERIC);
        $params[] = array($filter->from_date, TYPE_DATE);
        $params[] = array($filter->to_date, TYPE_DATE);
        $params[] = array($filter->status, TYPE_STRING);
        try {
            return parent::execute_sp_multi_query("sp_invoicing_partner_list_by_partner_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }
    public function get_order_agent_by_loan($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->partner_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_invoicing_order_agent_partner_by_loan", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }
    public function get_data_detail($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->seq, TYPE_NUMERIC);
        $params[] = array($filter->partner_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_invoicing_partner_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_detail($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->collect_seq, TYPE_NUMERIC);
        $params[] = array($data->partner_seq, TYPE_NUMERIC);
        $params[] = array($data->paid_date, TYPE_DATE);
        try {
            parent::execute_non_query("sp_invoicing_agent_update_by_partner", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }
    
}
?>