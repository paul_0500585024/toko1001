<?php
require_once MODEL_BASE;

class M_info_partner extends ModelBase {

    public function get_data($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->partner_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_partner_log_data_by_type", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->partner_seq, TYPE_NUMERIC);
        $params[] = array($data->phone_no, TYPE_STRING);
        $params[] = array($data->bank_name, TYPE_STRING);
        $params[] = array($data->bank_branch_name, TYPE_STRING);
        $params[] = array($data->bank_acct_no, TYPE_STRING);
        $params[] = array($data->bank_acct_name, TYPE_STRING);
        try {
            parent::execute_non_query("sp_partner_log_data_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }    

}

