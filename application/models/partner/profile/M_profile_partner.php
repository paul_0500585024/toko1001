<?php

require_once MODEL_BASE;

class M_profile_partner extends ModelBase {

    public function get_data_partner($data) {
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->partner_seq, TYPE_NUMERIC);

	try {
	    return parent::execute_sp_multi_query("sp_get_data_partner", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }
}

?>
