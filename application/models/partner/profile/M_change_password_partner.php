<?php

require_once MODEL_BASE;

class M_change_password_partner extends ModelBase {

    public function save_update($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->partner_seq, TYPE_NUMERIC);
	$params[] = array($data->new_password, TYPE_STRING);

	try {
	    parent::execute_non_query("sp_change_password_partner", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

}

?>