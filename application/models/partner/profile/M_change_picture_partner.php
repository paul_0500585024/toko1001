<?php

require_once MODEL_BASE;

class M_change_picture_partner extends ModelBase {

    public function get_data($selected) {

        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_logo_partner_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($data) {
        $params[] = array($data->userid, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->name, TYPE_STRING);
        $params[] = array($data->profile_img, TYPE_STRING);

        try {
            parent::execute_non_query("sp_profile_image_partner_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}
