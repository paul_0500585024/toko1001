<?php

require_once MODEL_BASE;

class M_group_agent_partner extends ModelBase {

    public function get_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->partner_seq, TYPE_NUMERIC);
        $params[] = array($filter->agent_group_name, TYPE_STRING);
        try {
            return parent::execute_sp_multi_query("sp_get_group_agent_partner_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->partner_seq, TYPE_NUMERIC);
        $params[] = array($selected->key, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_group_agent_partner_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->partner_seq, TYPE_NUMERIC);
        $params[] = array($data->agent_group_seq, TYPE_NUMERIC);
        $params[] = array($data->agent_group_name, TYPE_STRING);
        $params[] = array($data->commission_fee_percent, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_group_agent_partner_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->partner_seq, TYPE_NUMERIC);
        $params[] = array($data->agent_group_name, TYPE_STRING);
        $params[] = array($data->commission_fee_percent, TYPE_NUMERIC);
        try {
            parent::execute_non_query("sp_group_agent_partner_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_delete($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->partner_seq, TYPE_NUMERIC);
        $params[] = array($data->key, TYPE_NUMERIC);
        try {
            parent::execute_non_query("sp_group_agent_partner_delete", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}
