<?php

require_once MODEL_BASE;

class M_customer_agent_data_partner extends ModelBase {

    public function get_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->identity_no, TYPE_STRING);
        $params[] = array($filter->email_address, TYPE_STRING);
        $params[] = array($filter->partner_seq, TYPE_NUMERIC);
        $params[] = array($filter->active, TYPE_BOOLEAN);;


        try {
            return parent::execute_sp_multi_query("sp_customer_agent_partner_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }
    
    public function get_data($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_customer_agent_partner_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>