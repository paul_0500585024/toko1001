<?php

require_once MODEL_BASE;

class M_agent_data_partner extends ModelBase {

    public function get_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->sort, TYPE_STRING);
        $params[] = array($filter->column_sort, TYPE_STRING);
        $params[] = array($filter->email, TYPE_STRING);
        $params[] = array($filter->name, TYPE_STRING);
        $params[] = array($filter->status, TYPE_STRING);
        $params[] = array($filter->partner_seq, TYPE_NUMERIC);


        try {
            return parent::execute_sp_multi_query("sp_agent_list_partner", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_list_export($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->sort, TYPE_STRING);
        $params[] = array($filter->column_sort, TYPE_STRING);
        $params[] = array($filter->email, TYPE_STRING);
        $params[] = array($filter->name, TYPE_STRING);
        $params[] = array($filter->status, TYPE_STRING);
        $params[] = array($filter->partner_seq, TYPE_NUMERIC);


        try {
            return parent::execute_sp_single_query("sp_agent_list_export_partner", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data_status($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->sort, TYPE_STRING);
        $params[] = array($filter->column_sort, TYPE_STRING);
        $params[] = array($filter->email, TYPE_STRING);
        $params[] = array($filter->name, TYPE_STRING);
        $params[] = array($filter->status, TYPE_STRING);
        $params[] = array($filter->partner_seq, TYPE_NUMERIC);


        try {
            return parent::execute_sp_multi_query("sp_agent_partner_data_status", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_partner_commision_data($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_partner_commision_data", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_agent_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data_address_by_agent_seq($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_agent_address_by_agent_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->info_login_email, TYPE_STRING);
        $params[] = array($data->info_agent_name, TYPE_STRING);
        $params[] = array($data->info_agent_birthday, TYPE_DATE);
        $params[] = array($data->info_agent_gender, TYPE_STRING);
        $params[] = array($data->info_agent_mobile_phone, TYPE_STRING);
        $params[] = array($data->info_login_password, TYPE_STRING);
        $params[] = array($data->profile_img, TYPE_STRING);
        $params[] = array($data->partner_seq, TYPE_NUMERIC);
        $params[] = array($data->info_login_account_status, TYPE_STRING);
        $params[] = array($data->info_bank_name, TYPE_STRING);
        $params[] = array($data->info_bank_branch, TYPE_STRING);
        $params[] = array($data->info_bank_acct_no, TYPE_STRING);
        $params[] = array($data->info_bank_acct_name, TYPE_STRING);
        $params[] = array($data->commision_pro_rate, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_agent_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->info_agent_name, TYPE_STRING);
        $params[] = array($data->info_agent_birthday, TYPE_DATE);
        $params[] = array($data->info_agent_gender, TYPE_STRING);
        $params[] = array($data->info_agent_mobile_phone, TYPE_STRING);
        $params[] = array($data->info_login_password, TYPE_STRING);
        $params[] = array($data->profile_img, TYPE_STRING);
        $params[] = array($data->partner_seq, TYPE_NUMERIC);
        $params[] = array($data->info_login_account_status, TYPE_STRING);
        $params[] = array($data->info_bank_name, TYPE_STRING);
        $params[] = array($data->info_bank_branch, TYPE_STRING);
        $params[] = array($data->info_bank_acct_no, TYPE_STRING);
        $params[] = array($data->info_bank_acct_name, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->commision_pro_rate, TYPE_NUMERIC);
        try {
            parent::execute_non_query("sp_agent_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_delete_agent_address_by_agent_seq($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->agent_seq, TYPE_NUMERIC);
        try {
            parent::execute_non_query("sp_save_delete_agent_address_by_agent_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add_agent_address($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->agent_seq, TYPE_NUMERIC);
        $params[] = array($data->pic_name, TYPE_STRING);
        $params[] = array($data->address, TYPE_STRING);
        $params[] = array($data->district_seq, TYPE_NUMERIC);
        $params[] = array($data->phone_no, TYPE_STRING);
        $params[] = array($data->zip_code, TYPE_STRING);
        $params[] = array($data->alias, TYPE_STRING);
        try {
            parent::execute_non_query("sp_agent_address_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function check_user_exist($email) {
        $params[] = array($email->email, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_check_agent_exist", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }

        return true;
    }

    public function save_add_commission_log($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->agent_seq, TYPE_NUMERIC);
        try {
            parent::execute_non_query("sp_agent_trx_fee_log_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

//    public function save_delete_commission($data) {   
//        $params[] = array($data->user_id, TYPE_STRING);
//        $params[] = array($data->ip_address, TYPE_STRING);
//        $params[] = array($data->agent_seq, TYPE_NUMERIC);
//        try {
//            parent::execute_non_query("sp_agent_commission_delete", $params);
//        } catch (Exception $ex) {
//            parent::handle_database_error($ex);
//        }
//    }

//    public function save_add_commission($data) {
//        $params[] = array($data->user_id, TYPE_STRING);
//        $params[] = array($data->ip_address, TYPE_STRING);
//        $params[] = array($data->agent_seq, TYPE_NUMERIC);
//        $params[] = array($data->cat_lvl1, TYPE_NUMERIC);
//        $params[] = array($data->cat_lvl2, TYPE_NUMERIC);
//        $params[] = array($data->fee, TYPE_NUMERIC);
//        try {
//            parent::execute_non_query("sp_agent_commission_add", $params);
//        } catch (Exception $ex) {
//            parent::handle_database_error($ex);
//        }
//    }

 

}

?>