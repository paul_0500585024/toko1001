<?php

require_once MODEL_BASE;

class M_login_partner extends ModelBase {

    public function get_list($data) {
	$login[] = array($data->user_id, TYPE_STRING);
	$login[] = array($data->password_2, TYPE_STRING);
	$login[] = array($data->ip_address, TYPE_STRING);
	try {
	    return parent::execute_sp_multi_query("sp_login_partner", $login);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_left_nav_partner($user_id) {

	$login[] = array($user_id, TYPE_STRING);

	try {
	    $data = parent::execute_sp_single_query("sp_get_left_nav_partner", $login);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
	if (isset($data)) {
	    foreach ($data as $row) {
		$response[$row->parent_menu_cd][] = $row;
	    }
	    return $response;
	}
    }

    public function get_password($data) {

	$info[] = array($data->partner_seq, TYPE_NUMERIC);

	try {
	    return parent::execute_sp_single_query("sp_get_password_partner", $info);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

}

?>