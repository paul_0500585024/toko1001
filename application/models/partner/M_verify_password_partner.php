<?php

require_once MODEL_BASE;

class M_verify_password_partner extends ModelBase {

    function get_partner_log($sel) {
	$params[] = array($sel->uri, TYPE_STRING);
	$params[] = array($sel->email_cd, TYPE_STRING);

	try {
	    return parent::execute_sp_single_query("sp_email_log_by_code", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    function check_partner_log_security($sel) {
	$params[] = array($sel->uri, TYPE_STRING);

	try {
	    return parent::execute_sp_single_query("sp_partner_log_security_check", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    function update_password($selected) {

	$params[] = array($selected->email, TYPE_STRING);
	$params[] = array($selected->new_pass, TYPE_STRING);
	try {
	    parent::execute_non_query("sp_partner_update_password_by_email", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function add_partner_log_security($selected) {
	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->partner_seq, TYPE_STRING);
	$params[] = array($selected->type, TYPE_STRING);
	$params[] = array($selected->code, TYPE_STRING);
	$params[] = array($selected->old_pass, TYPE_STRING);

	try {
	    parent::execute_non_query("sp_partner_log_security_add", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    function get_partner_seq($data) {
	$params[] = array($data->email, TYPE_STRING);

	try {
	    return parent::execute_sp_single_query("sp_partner_email_check", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

}

?>
