<?php

require_once MODEL_BASE;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Product New
 *
 * @author Jartono
 */
class M_product_new_merchant extends ModelBase {

    public function get_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->search, TYPE_STRING);
        $params[] = array($filter->merchant_seq, TYPE_NUMERIC);
        $params[] = array($filter->status, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        try {
            return parent::execute_sp_multi_query("sp_product_new_list_merchant", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($selected) {

        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_product_new_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->merchant_seq, TYPE_NUMERIC);
        $params[] = array($data->name, TYPE_STRING);
        $params[] = array($data->include_ins, TYPE_BOOLEAN);
        $params[] = array($data->category_l2_seq, TYPE_NUMERIC);
        $params[] = array($data->category_ln_seq, TYPE_NUMERIC);
        $params[] = array($data->notes, TYPE_STRING);
        $params[] = array($data->description, TYPE_STRING);
        $params[] = array($data->content, TYPE_STRING);
        $params[] = array($data->warranty_notes, TYPE_STRING);
        $params[] = array($data->p_weight_kg, TYPE_NUMERIC);
        $params[] = array($data->p_length_cm, TYPE_NUMERIC);
        $params[] = array($data->p_width_cm, TYPE_NUMERIC);
        $params[] = array($data->p_height_cm, TYPE_NUMERIC);
        $params[] = array($data->b_weight_kg, TYPE_NUMERIC);
        $params[] = array($data->b_length_cm, TYPE_NUMERIC);
        $params[] = array($data->b_width_cm, TYPE_NUMERIC);
        $params[] = array($data->b_height_cm, TYPE_NUMERIC);
        $params[] = array($data->specification, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_product_new_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->merchant_seq, TYPE_NUMERIC);
        $params[] = array($data->name, TYPE_STRING);
        $params[] = array($data->include_ins, TYPE_BOOLEAN);
        $params[] = array($data->notes, TYPE_STRING);
        $params[] = array($data->description, TYPE_STRING);
        $params[] = array($data->content, TYPE_STRING);
        $params[] = array($data->warranty_notes, TYPE_STRING);
        $params[] = array($data->p_weight_kg, TYPE_NUMERIC);
        $params[] = array($data->p_length_cm, TYPE_NUMERIC);
        $params[] = array($data->p_width_cm, TYPE_NUMERIC);
        $params[] = array($data->p_height_cm, TYPE_NUMERIC);
        $params[] = array($data->b_weight_kg, TYPE_NUMERIC);
        $params[] = array($data->b_length_cm, TYPE_NUMERIC);
        $params[] = array($data->b_width_cm, TYPE_NUMERIC);
        $params[] = array($data->b_height_cm, TYPE_NUMERIC);
        $params[] = array($data->specification, TYPE_STRING);
        try {
            parent::execute_non_query("sp_product_new_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add_attribute($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->product_seq, TYPE_NUMERIC);
        $this->save_delete_attribute($data);
        foreach ($data->attribut as $attributeval) {
            if ($attributeval != '') {
                $params[] = array($attributeval, TYPE_NUMERIC);
                try {
                    parent::execute_non_query("sp_product_attribute_new_add", $params);
                } catch (Exception $ex) {
                    parent::handle_database_error($ex);
                }
                array_pop($params);
            }
        }
    }

    public function save_delete_attribute($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->product_seq, TYPE_NUMERIC);
        try {
            parent::execute_non_query("sp_product_attribute_new_delete", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add_spec($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->product_seq, TYPE_NUMERIC);
        $this->save_delete_spec($data);
        for ($i = 0; $i < count($data->spekname); $i++) {
            if ($data->spekname[$i] != '') {
                $params[] = array($data->spekname[$i], TYPE_STRING);
                $params[] = array($data->spekval[$i], TYPE_STRING);
                try {
                    parent::execute_non_query("sp_product_spec_new_add", $params);
                } catch (Exception $ex) {
                    parent::handle_database_error($ex);
                }
                array_pop($params);
                array_pop($params);
            }
        }
    }

    public function save_delete_spec($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->product_seq, TYPE_NUMERIC);
        try {
            parent::execute_non_query("sp_product_spec_new_delete", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_delete($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_product_new_delete", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_product_category() {
        try {
            return parent::execute_sp_single_query("sp_get_tree_view_category"); // sp_product_category_merchant_by_parent
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
//	foreach ($data as $row) {
//	    $response[$row->parent_seq][] = $row;
//	}
//	return $response;
    }

    public function get_product_category_by_child($data) {
//	$params[] = array($data->user_id, TYPE_STRING);
//	$params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_product_category_by_child", $params); // sp_product_category_merchant_by_parent
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_merchant_lvl1($data) {
        $params[] = array($data, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_merchant_trx_fee_by_seq", $params); // sp_product_category_merchant_by_parent
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_attribute_by_category($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->idh, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_attribute_category_by_category", $params); // sp_product_category_merchant_by_parent
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_attribute_value($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_attribute_value_by_attribute_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_merchant_by_name($selected) {

        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->merchant_name, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_merchant_by_name", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_variant_by_category($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->idh, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_variant_product_by_category", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_product_attribute_new_by_product_seq($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_product_attribute_new_by_product_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_product_spec_new_by_product_seq($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_product_spec_new_by_product_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_product_variant_new_by_product_seq($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_product_variant_new_by_product_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_max_seq($data) {
        $params[] = array($data, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_get_table_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add_variant($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->product_seq, TYPE_NUMERIC);
        for ($i = 0; $i < count($data->variant_value_seq); $i++) {
            if ($data->varian == "1") {
                if ($data->variant_value_seq[$i] <= "1")
                    continue;
            }
            if ($data->product_price[$i] != '' && $data->sell_price[$i] != '' && $data->product_price[$i] != '0' && $data->sell_price[$i] != '0') {

                $params[] = array($data->variant_value_seq[$i], TYPE_NUMERIC);
                $params[] = array($data->product_price[$i], TYPE_NUMERIC);
                $params[] = array($data->disc_percent[$i], TYPE_NUMERIC);
                $params[] = array($data->sell_price[$i], TYPE_NUMERIC);
                $params[] = array($data->order[$i], TYPE_NUMERIC);
                $params[] = array($data->max_buy[$i], TYPE_NUMERIC);
                $params[] = array($data->merchant_sku[$i], TYPE_STRING);
                $params[] = array($data->stock[$i], TYPE_NUMERIC);
                $params[] = array($data->file_name1[$i], TYPE_STRING);
                $params[] = array($data->file_name2[$i], TYPE_STRING);
                $params[] = array($data->file_name3[$i], TYPE_STRING);
                $params[] = array($data->file_name4[$i], TYPE_STRING);
                $params[] = array($data->file_name5[$i], TYPE_STRING);
                $params[] = array($data->file_name6[$i], TYPE_STRING);
                try {
                    parent::execute_non_query("sp_product_variant_new_add", $params);
                } catch (Exception $ex) {
                    parent::handle_database_error($ex);
                }
                array_pop($params); //variant_value_seq
                array_pop($params); //prod_price
                array_pop($params); //dis_price
                array_pop($params); //sal_price
                array_pop($params); //order
                array_pop($params); //max buy
                array_pop($params); //sku
                array_pop($params); //stock
                array_pop($params); //im1
                array_pop($params); //im2
                array_pop($params); //im3
                array_pop($params); //im4
                array_pop($params); //im5
                array_pop($params); //im6
            }
        }
    }

    public function save_delete_variant($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->product_seq, TYPE_NUMERIC);
        try {
            parent::execute_non_query("sp_product_variant_new_delete", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_variant($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->new_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_product_variant_by_product", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_img_variant($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->newvarseq, TYPE_NUMERIC);
        $params[] = array($data->img1, TYPE_STRING);
        $params[] = array($data->img2, TYPE_STRING);
        $params[] = array($data->img3, TYPE_STRING);
        $params[] = array($data->img4, TYPE_STRING);
        $params[] = array($data->img5, TYPE_STRING);
        $params[] = array($data->img6, TYPE_STRING);
        try {
            parent::execute_non_query("sp_product_variant_img_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>