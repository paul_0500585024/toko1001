<?php

require_once MODEL_BASE;

class M_store_decoration extends ModelBase {

    public function get_data($selected) {

        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_logo_merchant_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($data) {
        $params[] = array($data->userid, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->banner_img, TYPE_STRING);
        $params[] = array($data->logo_img, TYPE_STRING);
        $params[] = array($data->description, TYPE_STRING);
        $params[] = array($data->store_close_start, TYPE_DATE);
        $params[] = array($data->store_close_end, TYPE_DATE);

        try {
            parent::execute_non_query("sp_logo_merchant_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_district_info($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->district_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_district_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function open_store($data) {
        $params[] = array($data->userid, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        try {
            return parent::execute_non_query("sp_open_store", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}
