<?php

require_once MODEL_BASE;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Product New
 *
 * @author Jartono
 */
class M_product_merchant extends ModelBase {

    public function get_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->name, TYPE_STRING);
        $params[] = array($filter->description, TYPE_STRING);
        $params[] = array($filter->merchant_seq, TYPE_NUMERIC);
        $params[] = array($filter->merchant_name, TYPE_STRING);
        $params[] = array($filter->status, TYPE_STRING);
        //var_dump($params);
        try {
            return parent::execute_sp_multi_query("sp_product_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_merchant_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->sort, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->search, TYPE_STRING);
        $params[] = array($filter->merchant_seq, TYPE_STRING);
        $params[] = array($filter->status, TYPE_STRING);
        $params[] = array($filter->start, TYPE_STRING);
        $params[] = array($filter->length, TYPE_STRING);
        try {
            return parent::execute_sp_multi_query("sp_product_merchant_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($selected) {
        $params[] = array($selected->merchant_seq, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_product_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_product_log_seq($selected) {

        $params[] = array($selected->merchant_seq, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_product_get_log_seq_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_product_status($selected) {

        $params[] = array($selected->merchant_seq, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_product_get_status_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_attribute_value($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->seq, TYPE_NUMERIC);


        try {
            return parent::execute_sp_single_query("sp_attribute_value_by_attribute_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_attribute_seq($selected) {

        $params[] = array($selected->merchant_seq, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->new_attribut, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_product_get_attribute_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_old_attribute_val($selected) {

        $params[] = array($selected->merchant_seq, TYPE_STRING);
        $params[] = array($selected->attribute_seq, TYPE_STRING);
        $params[] = array($selected->seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_product_log_old_attribute", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_spec_seq($selected) {

        $params[] = array($selected->merchant_seq, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_product_get_spec_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_spec_old($selected) {
        $params[] = array($selected->merchant_seq, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->oldspecseq, TYPE_NUMERIC);
        $params[] = array($selected->product_seq, TYPE_NUMERIC);


        try {
            return parent::execute_sp_single_query("sp_product_get_spec_seq_old", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function update_product_attribute($data) {
        $params[] = array($data->merchant_seq, TYPE_STRING);
        $params[] = array($data->attribute_seq, TYPE_NUMERIC);
        $params[] = array($data->new_attribut, TYPE_NUMERIC);
        $params[] = array($data->old_attribute_value_seq, TYPE_NUMERIC);
        $params[] = array($data->log_seq, TYPE_NUMERIC);
        $params[] = array($data->seq, TYPE_NUMERIC);
        try {
            parent::execute_non_query("sp_product_update_log_attribute", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function update_product_spec($data) {
        if ($data->name1 != '' or $data->val1 != '') {
            $params[] = array($data->user_id, TYPE_STRING);
            $params[] = array($data->seq, TYPE_NUMERIC);
            $params[] = array($data->log_seq, TYPE_NUMERIC);
            $params[] = array($data->name1, TYPE_STRING);
            $params[] = array($data->val1, TYPE_STRING);
            $params[] = array($data->hid1, TYPE_NUMERIC);
            $params[] = array($data->new_seq, TYPE_NUMERIC);

            try {
                parent::execute_non_query("sp_product_update_log_spec", $params);
            } catch (Exception $ex) {
                parent::handle_database_error($ex);
            }
        }
    }

    public function update_active_product($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->merchant_seq, TYPE_NUMERIC);
        $params[] = array($data->product_variant_seq, TYPE_NUMERIC);
        $params[] = array($data->active, TYPE_STRING);
        try {
            parent::execute_non_query("sp_product_non_active_update_by_variant_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function update_stock_product($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->merchant_seq, TYPE_NUMERIC);
        $params[] = array($data->product_variant_seq, TYPE_NUMERIC);
        $params[] = array($data->stock, TYPE_NUMERIC);
        $params[] = array($data->variant_value_seq, TYPE_NUMERIC);
        $params[] = array($data->merchant_sku, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_product_stock_qty_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function update_product_price($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->merchant_seq, TYPE_NUMERIC);
        $params[] = array($data->product_variant_seq, TYPE_NUMERIC);
        $params[] = array($data->product_price, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_product_variant_update_product_price", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function update_sell_price($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->merchant_seq, TYPE_NUMERIC);
        $params[] = array($data->product_variant_seq, TYPE_NUMERIC);
        $params[] = array($data->sell_price, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_product_variant_update_sell_price", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function add_old_spec($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->product_seq, TYPE_NUMERIC);
        $params[] = array($data->log_seq, TYPE_NUMERIC);
        try {
            parent::execute_non_query("sp_product_log_add_old_spec", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add_spec($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->log_seq, TYPE_NUMERIC);
        for ($i = 0; $i <= count($data->spekname); $i++) {
            if (isset($data->spekname[$i]) and $data->spekname[$i] != '') {
                $params[] = array($data->spekname[$i], TYPE_STRING);
                $params[] = array($data->spekval[$i], TYPE_STRING);
                try {
                    parent::execute_non_query("sp_product_add_log_new_spec", $params);
                } catch (Exception $ex) {
                    parent::handle_database_error($ex);
                }
                array_pop($params);
                array_pop($params);
            }
        }
    }

    public function get_product_category() {
        try {
            return parent::execute_sp_single_query("sp_get_tree_view_category");
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_product_category_by_child($data) {
        $params[] = array($data, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_product_category_by_child", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_merchant_lvl1($data) {
        $params[] = array($data, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_merchant_trx_fee_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_attribute_by_category($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->idh, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_attribute_category_by_category", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_merchant_by_name($selected) {

        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->merchant_name, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_merchant_by_name", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_variant_by_category($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->idh, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_variant_product_by_category", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_product_attribute_by_product_seq($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_product_all_attribute_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_product_log_attribute_by_product_seq($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->log_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_product_log_attribute_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_product_spec_by_product_seq($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_product_spec_by_product_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_product_log_spec_by_product_seq($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->log_seq, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_product_log_spec_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_product_log_info_by_product_seq($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->log_seq, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_product_log_info_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_product_variant_by_product_seq($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_product_variant_by_product_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_product_log_variant_by_product_seq($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->log_seq, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_product_log_variant_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_max_seq() {
        try {
            return parent::execute_sp_single_query("sp_product_log_get_max_variant_seq");
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add_variant($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->log_seq, TYPE_NUMERIC);


        for ($i = 0; $i < count($data->variant_value_seq); $i++) {
            if ($data->product_price[$i] != '' && $data->sell_price[$i] != '' && $data->product_price[$i] != '0' && $data->sell_price[$i] != '0') {
                $params[] = array($data->variant_value_seq[$i], TYPE_NUMERIC);
                $params[] = array($data->product_price[$i], TYPE_NUMERIC);
                $params[] = array($data->disc_percent[$i], TYPE_NUMERIC);
                $params[] = array($data->sell_price[$i], TYPE_NUMERIC);
                $params[] = array($data->order[$i], TYPE_NUMERIC);
                $params[] = array($data->max_buy[$i], TYPE_NUMERIC);
                $params[] = array($data->file_name1[$i], TYPE_STRING);
                $params[] = array($data->file_name2[$i], TYPE_STRING);
                $params[] = array($data->file_name3[$i], TYPE_STRING);
                $params[] = array($data->file_name4[$i], TYPE_STRING);
                $params[] = array($data->file_name5[$i], TYPE_STRING);
                $params[] = array($data->file_name6[$i], TYPE_STRING);

                if ($data->variant_seq[$i] > 0) {
                    $params[] = array($data->variant_seq[$i], TYPE_STRING);
                }
                try {
                    if ($data->variant_seq[$i] > 0) {
                        parent::execute_non_query("sp_product_variant_log_update", $params);
                    } else {
                        parent::execute_non_query("sp_product_variant_log_add", $params);
                    }
                } catch (Exception $ex) {
                    parent::handle_database_error($ex);
                }
                if ($data->variant_seq[$i] > 0) {
                    array_pop($params); //variant_seq
                }
                array_pop($params); //variant_value_seq
                array_pop($params); //prod_price
                array_pop($params); //dis_price
                array_pop($params); //sal_price
                array_pop($params); //order
                array_pop($params); //max buy
                array_pop($params); //im1
                array_pop($params); //im2
                array_pop($params); //im3
                array_pop($params); //im4
                array_pop($params); //im5
                array_pop($params); //im6
            }
        }
    }

    public function save_add_variant_new($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->log_seq, TYPE_NUMERIC);
        for ($i = 1; $i <= count($data->new_variant_value_seq); $i++) {
            if ($data->new_product_price[$i] != '' && $data->new_sell_price[$i] != '') {
                $params[] = array($data->new_variant_value_seq[$i], TYPE_NUMERIC);
                $params[] = array($data->new_product_price[$i], TYPE_NUMERIC);
                $params[] = array($data->new_disc_percent[$i], TYPE_NUMERIC);
                $params[] = array($data->new_sell_price[$i], TYPE_NUMERIC);
                $params[] = array($data->new_order[$i], TYPE_NUMERIC);
                $params[] = array($data->new_max_buy[$i], TYPE_NUMERIC);
                $params[] = array($data->new_file_name1[$i], TYPE_STRING);
                $params[] = array($data->new_file_name2[$i], TYPE_STRING);
                $params[] = array($data->new_file_name3[$i], TYPE_STRING);
                $params[] = array($data->new_file_name4[$i], TYPE_STRING);
                $params[] = array($data->new_file_name5[$i], TYPE_STRING);
                $params[] = array($data->new_file_name6[$i], TYPE_STRING);

                if ($data->new_variant_seq[$i] > 0) {
                    $params[] = array($data->new_variant_seq[$i], TYPE_STRING);
                }
                try {
                    parent::execute_non_query("sp_product_variant_log_add", $params);
                } catch (Exception $ex) {
                    parent::handle_database_error($ex);
                }
                if ($data->new_variant_seq[$i] > 0) {
                    array_pop($params); //variant_seq
                }
                array_pop($params); //variant_value_seq
                array_pop($params); //prod_price
                array_pop($params); //dis_price
                array_pop($params); //sal_price
                array_pop($params); //order
                array_pop($params); //max buy
                array_pop($params); //im1
                array_pop($params); //im2
                array_pop($params); //im3
                array_pop($params); //im4
                array_pop($params); //im5
                array_pop($params); //im6
            }
        }
    }

    public function save_delete_variant($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->product_seq, TYPE_NUMERIC);
        try {
            parent::execute_non_query("sp_product_variant_delete", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function add_product_log($selected) {
        $params[] = array($selected->merchant_seq, TYPE_STRING);
        $params[] = array($selected->m_merchant_seq, TYPE_STRING);
        $params[] = array($selected->seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_multi_query("sp_product_merchant_log_data_check", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function change_product_status($selected) {
        $params[] = array($selected->merchant_seq, TYPE_STRING);
        $params[] = array($selected->seq, TYPE_NUMERIC);


        try {
            return parent::execute_sp_multi_query("sp_product_merchant_change_status", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add_log_data_old($selected) {
        $params[] = array($selected->merchant_seq, TYPE_STRING);
        $params[] = array($selected->log_seq, TYPE_NUMERIC);
        $params[] = array($selected->seq, TYPE_NUMERIC);
        try {
            parent::execute_non_query("sp_product_add_log_data_last", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add_log_data_new($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->log_seq, TYPE_NUMERIC);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->name, TYPE_STRING);
        $params[] = array($data->include_ins, TYPE_BOOLEAN);
        $params[] = array($data->description, TYPE_STRING);
        $params[] = array($data->specification, TYPE_STRING);
        $params[] = array($data->content, TYPE_STRING);
        $params[] = array($data->warranty_notes, TYPE_STRING);
        $params[] = array($data->p_weight_kg, TYPE_NUMERIC);
        $params[] = array($data->p_length_cm, TYPE_NUMERIC);
        $params[] = array($data->p_width_cm, TYPE_NUMERIC);
        $params[] = array($data->p_height_cm, TYPE_NUMERIC);
        $params[] = array($data->b_weight_kg, TYPE_NUMERIC);
        $params[] = array($data->b_length_cm, TYPE_NUMERIC);
        $params[] = array($data->b_width_cm, TYPE_NUMERIC);
        $params[] = array($data->b_height_cm, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_product_add_log_data_new", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

//    public function get_product_attribute_by_new_attribute($selected) {
//	$params[] = array($selected->seq_product_last, TYPE_NUMERIC);
//	$params[] = array($selected->attribut_new, TYPE_NUMERIC);
//	try {
//	    return parent::execute_sp_single_query("sp_product_attribute_by_new_attribute", $params);
//	} catch (Exception $ex) {
//	    parent::handle_database_error($ex);
//	}
//    }

    public function save_add_log_attribute($selected) {

        $params[] = array($selected->merchant_seq, TYPE_STRING);
        $params[] = array($selected->log_seq, TYPE_NUMERIC);
        $params[] = array($selected->seq, TYPE_NUMERIC);
        $params[] = array($selected->attribut_old, TYPE_NUMERIC);
        $params[] = array($selected->attribut, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_product_add_log_attribute_last", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>