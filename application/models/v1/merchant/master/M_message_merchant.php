<?php

require_once MODEL_BASE;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Merchant New
 *
 * @author Jartono
 */
class M_message_merchant extends ModelBase {

    public function get_data($selected) {

        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_logo_merchant_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($data) {
        $params[] = array($data->userid, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->banner_img, TYPE_STRING);
        $params[] = array($data->logo_img, TYPE_STRING);
        $params[] = array($data->description, TYPE_STRING);

        try {
            parent::execute_non_query("sp_logo_merchant_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_message_log_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array(MERCHANT, TYPE_STRING);
        $params[] = array($filter->member_seq, TYPE_NUMERIC);
        $params[] = array($filter->status, TYPE_STRING);
        try {
            return parent::execute_sp_multi_query("sp_message_log_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_insert_message($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->content, TYPE_STRING);
        $params[] = array($filter->prev_message_seq, TYPE_NUMERIC);
        $params[] = array(0, TYPE_NUMERIC);
        $params[] = array($filter->member_seq, TYPE_NUMERIC);
        $params[] = array("", TYPE_STRING);
        try {
            parent::execute_non_query("sp_message_log_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_message_log_list_by_seq($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->seq, TYPE_NUMERIC);
        $params[] = array(MERCHANT, TYPE_STRING);
        $params[] = array($filter->member_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_message_log_list_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>