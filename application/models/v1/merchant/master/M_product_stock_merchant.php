<?php

require_once MODEL_BASE;

class M_product_stock_merchant extends ModelBase {

    public function get_list($filter) {
	$params[] = array($filter->user_id, TYPE_STRING);
	$params[] = array($filter->merchant_seq, TYPE_NUMERIC);
	$params[] = array($filter->ip_address, TYPE_STRING);
	$params[] = array($filter->start, TYPE_NUMERIC);
	$params[] = array($filter->length, TYPE_NUMERIC);
	$params[] = array($filter->order, TYPE_STRING);
	$params[] = array($filter->column, TYPE_STRING);
	$params[] = array($filter->product_name, TYPE_STRING);
	$params[] = array($filter->value, TYPE_STRING);
	$params[] = array($filter->merchant_sku, TYPE_STRING);

	try {
	    return parent::execute_sp_multi_query("sp_product_stock_list", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_list_by_product($filter) {  // version 1
	$params[] = array($filter->user_id, TYPE_STRING);
	$params[] = array($filter->merchant_seq, TYPE_NUMERIC);
	$params[] = array($filter->ip_address, TYPE_STRING);
	$params[] = array($filter->start, TYPE_NUMERIC);
	$params[] = array($filter->length, TYPE_NUMERIC);
	$params[] = array($filter->order, TYPE_STRING);
	$params[] = array($filter->column, TYPE_STRING);
	$params[] = array($filter->product_seq, TYPE_NUMERIC);
	$params[] = array($filter->seq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_multi_query("sp_product_stock_list_by_product", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_data($selected) {
	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->product_variant_seq, TYPE_NUMERIC);
	$params[] = array($selected->variant_value_seq, TYPE_NUMERIC);

	try {
	    return parent::execute_sp_multi_query("sp_product_stock_by_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function update_validate($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->product_variant_seq, TYPE_NUMERIC);
	$params[] = array($data->variant_value_seq, TYPE_NUMERIC);
	$params[] = array($data->merchant_sku, TYPE_STRING);
	$params[] = array($data->stock, TYPE_NUMERIC);
	$params[] = array($data->stock_add, TYPE_NUMERIC);

	try {
	    parent::execute_non_query("sp_product_stock_update", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_update($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->product_variant_seq, TYPE_NUMERIC);
	$params[] = array($data->value_seq, TYPE_NUMERIC);
	$params[] = array($data->merchant_sku, TYPE_STRING);
	$params[] = array($data->stock, TYPE_NUMERIC);
	$params[] = array($data->stock_add, TYPE_NUMERIC);

	try {
	    parent::execute_non_query("sp_product_stock_update", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_subtract_stock($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->product_variant_seq, TYPE_NUMERIC);
	$params[] = array($data->variant_value_seq, TYPE_NUMERIC);
	$params[] = array($data->qty, TYPE_NUMERIC);

	try {
	    parent::execute_non_query("sp_substract_stock_product", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_add_stock($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->product_variant_seq, TYPE_NUMERIC);
	$params[] = array($data->variant_value_seq, TYPE_NUMERIC);
	$params[] = array($data->qty, TYPE_NUMERIC);

	try {
	    parent::execute_non_query("sp_save_add_stock_product", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_latest_product_stock($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->product_variant_seq, TYPE_NUMERIC);
	$params[] = array($data->variant_value_seq, TYPE_NUMERIC);

	try {
	    return parent::execute_sp_single_query("sp_get_latest_product_stock", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

}

?>