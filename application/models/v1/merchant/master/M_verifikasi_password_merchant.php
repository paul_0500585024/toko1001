<?php

require_once MODEL_BASE;

class M_verifikasi_password_merchant extends ModelBase {

    function get_merchant_log($sel) {
	$params[] = array($sel->uri, TYPE_STRING);

	try {
	    return parent::execute_sp_single_query("sp_email_log_by_code", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    function check_merchant_log_security($sel) {
	$params[] = array($sel->uri, TYPE_STRING);

	try {
	    return parent::execute_sp_single_query("sp_merchant_log_security_check", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    function get_date() {

	$query = $this->db->query("select now() as tgl");
	foreach ($query->result()as $row) {
	    $tgl = $row->tgl;
	    return $tgl;
	}
    }

    function update_password($selected) {

	$params[] = array($selected->email, TYPE_STRING);
	$params[] = array($selected->newpass, TYPE_STRING);
	try {
	    parent::execute_non_query("sp_merchant_update_password_by_email", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function add_merchant_log_security($selected) {
	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->merchant_seq, TYPE_STRING);
	$params[] = array($selected->type, TYPE_STRING);
	$params[] = array($selected->code, TYPE_STRING);
	$params[] = array($selected->old_pass, TYPE_STRING);

	//var_dump($params);
	try {
	    parent::execute_non_query("sp_merchant_log_security_add", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
	return "true";
    }

    function get_merchant_seq($merchantparam) {
	$params[] = array($merchantparam->email, TYPE_STRING);

	try {
	    return parent::execute_sp_single_query("sp_merchant_email_check", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function add_mail_log($data) {

	$params[] = array($data->email, TYPE_STRING);
	$params[] = array($data->email_cd, TYPE_STRING);
	$params[] = array($data->recipient_name, TYPE_STRING);
	$params[] = array($data->gencode, TYPE_STRING);
	$params[] = array($data->subject, TYPE_STRING);
	$params[] = array($data->content, TYPE_STRING);
	//var_dump($params);
	try {

	    parent::execute_non_query("sp_merchant_add_forgot_password", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
	return "true";
    }

}

?>
