<?php

require_once MODEL_BASE;

class M_informasi_merchant_admin extends ModelBase {

    public function get_list($filter) {
	$params[] = array($filter->user_id, TYPE_STRING);
	$params[] = array($filter->ip_address, TYPE_STRING);
	$params[] = array($filter->start, TYPE_NUMERIC);
	$params[] = array($filter->length, TYPE_NUMERIC);
	$params[] = array($filter->order, TYPE_STRING);
	$params[] = array($filter->column, TYPE_STRING);
	$params[] = array($filter->name, TYPE_STRING);
	$params[] = array($filter->email, TYPE_STRING);

	try {
	    return parent::execute_sp_multi_query("sp_merchant_list", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_data($selected) {
	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->merchant_seq, TYPE_NUMERIC);

	try {
	    return parent::execute_sp_single_query("sp_merchant_log_data_by_type", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_update($data) {
	$params[] = array($data->user_id, TYPE_NUMERIC);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->merchant_seq, TYPE_NUMERIC);
	$params[] = array($data->log_seq, TYPE_NUMERIC);
	$params[] = array($data->address, TYPE_STRING);
	$params[] = array($data->district_seq, TYPE_NUMERIC);
	$params[] = array($data->zip_code, TYPE_STRING);
	$params[] = array($data->phone_no, TYPE_STRING);
	$params[] = array($data->fax_no, TYPE_STRING);
	$params[] = array($data->pic1_name, TYPE_STRING);
	$params[] = array($data->pic1_phone_no, TYPE_STRING);
	$params[] = array($data->pic2_name, TYPE_STRING);
	$params[] = array($data->pic2_phone_no, TYPE_STRING);
	$params[] = array($data->pickup_addr_eq, TYPE_BOOLEAN);
	$params[] = array($data->pickup_addr, TYPE_STRING);
	$params[] = array($data->pickup_district_seq, TYPE_NUMERIC);
	$params[] = array($data->pickup_zip_code, TYPE_STRING);
	$params[] = array($data->return_addr_eq, TYPE_BOOLEAN);
	$params[] = array($data->return_addr, TYPE_STRING);
	$params[] = array($data->return_district_seq, TYPE_NUMERIC);
	$params[] = array($data->return_zip_code, TYPE_STRING);
	$params[] = array($data->bank_name, TYPE_STRING);
	$params[] = array($data->bank_branch_name, TYPE_STRING);
	$params[] = array($data->bank_acct_no, TYPE_STRING);
	$params[] = array($data->bank_acct_name, TYPE_STRING);
	try {
	    parent::execute_non_query("sp_merchant_log_data_add", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_add_log($data) {

	$params[] = array($data->user_id, TYPE_NUMERIC);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->merchant_seq, TYPE_NUMERIC);

	try {
	    return parent::execute_sp_single_query("sp_merchant_log_add", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_add_auto($data) {

	$params[] = array($data->user_id, TYPE_NUMERIC);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->merchant_seq, TYPE_NUMERIC);
	$params[] = array($data->log_seq, TYPE_NUMERIC);

	try {
	    parent::execute_non_query("sp_merchant_log_data_add_old_data", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

}

?>