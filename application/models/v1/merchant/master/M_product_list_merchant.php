<?php

require_once MODEL_BASE;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Product New
 *
 * @author Jartono
 */
class M_product_list_merchant extends ModelBase {

    public function get_list($filter) {
	$params[] = array($filter->user_id, TYPE_STRING);
	$params[] = array($filter->ip_address, TYPE_STRING);
	$params[] = array($filter->start, TYPE_NUMERIC);
	$params[] = array($filter->length, TYPE_NUMERIC);
	$params[] = array($filter->order, TYPE_STRING);
	$params[] = array($filter->column, TYPE_STRING);
	$params[] = array($filter->name, TYPE_STRING);
	$params[] = array($filter->description, TYPE_STRING);
	$params[] = array($filter->merchant_seq, TYPE_NUMERIC);
	$params[] = array($filter->merchant_name, TYPE_STRING);
	$params[] = array($filter->status, TYPE_STRING);

	try {
	    return parent::execute_sp_multi_query("sp_product_list", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_data($selected) {

	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->seq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_product_by_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_product_category() {
	try {
	    return parent::execute_sp_single_query("sp_get_tree_view_category"); // sp_product_category_merchant_by_parent
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_product_category_by_child($data) {
	$params[] = array($data, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_product_category_by_child", $params); // sp_product_category_merchant_by_parent
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_merchant_lvl1($data) {
	$params[] = array($data, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_merchant_trx_fee_by_seq", $params); // sp_product_category_merchant_by_parent
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_attribute_by_category($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->idh, TYPE_STRING);
	try {
	    return parent::execute_sp_single_query("sp_attribute_category_by_category", $params); // sp_product_category_merchant_by_parent
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_attribute_value($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_STRING);
	try {
	    return parent::execute_sp_single_query("sp_attribute_value_by_attribute_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_merchant_by_name($selected) {

	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->merchant_name, TYPE_STRING);

	try {
	    return parent::execute_sp_single_query("sp_merchant_by_name", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_variant_by_category($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->idh, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_variant_product_by_category", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_product_attribute_new_by_product_seq($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_product_attribute_by_product_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_product_spec_new_by_product_seq($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_product_spec_by_product_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_product_variant_new_by_product_seq($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->seq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_product_variant_by_product_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

}

?>