<?php

require_once MODEL_BASE;

class M_signup_merchant extends ModelBase {

    public function save_add($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->email, TYPE_STRING);
        $params[] = array($data->name, TYPE_STRING);
        $params[] = array($data->phone, TYPE_STRING);
        $params[] = array($data->address, TYPE_STRING);
        $params[] = array($data->code_domain, TYPE_STRING);

        try {
            return parent::execute_non_query("sp_merchant_add_signup", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function check_merchant_exist($email) {
        $params[] = array($email, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_merchant_by_email", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function check_merchant_domain_exist($data) {
        $params[] = array($data->code, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_merchant_code", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>
