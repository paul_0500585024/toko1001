<?php

require_once MODEL_BASE;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Product New
 *
 * @author Jartono
 */
class M_order_return_merchant extends ModelBase {

    public function get_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->merchant_seq, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->no_return, TYPE_STRING);
        $params[] = array($filter->date_search, TYPE_STRING);
        $params[] = array($filter->return_date_from, TYPE_DATE);
        $params[] = array($filter->return_date_to, TYPE_DATE);
        $params[] = array($filter->ship_status, TYPE_STRING);
        $params[] = array($filter->member_email, TYPE_STRING);
        $params[] = array($filter->new_status_code, TYPE_STRING);
        $params[] = array($filter->ship_to_member_status, TYPE_STRING);
        $params[] = array($filter->ship_from_member_status, TYPE_STRING);
        $params[] = array($filter->received_by_member_status, TYPE_STRING);
        $params[] = array($filter->received_by_admin_status, TYPE_STRING);
        $params[] = array($filter->rejected_status, TYPE_STRING);

        try {
            return parent::execute_sp_multi_query("sp_merchant_order_return_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_expedition() {
        try {
            return parent::execute_sp_single_query("sp_get_dropdown_expedition");
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data_detail($selected) {
        $params[] = array($selected->merchant_seq, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->return_no, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_merchant_return_detail", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data_detail_approve($selected) {
        $params[] = array($selected->merchant_seq, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->return_no, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_merchant_return_detail_approve", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data($selected) {
        $params[] = array($selected->merchant_seq, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->return_seq, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_merchant_return_get_data", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data_return_to_refund($selected) {
        $params[] = array($selected->merchant_seq, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->return_no, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_merchant_return_detail_to_refund", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function return_update_received($selected) {
        $params[] = array($selected->merchant_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->return_no, TYPE_STRING);
        $params[] = array($selected->tgl_terima, TYPE_DATE);
        try {
            return parent::execute_non_query("sp_merchant_return_update_received", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function return_update_refund($selected) {
        $params[] = array($selected->merchant_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->return_no, TYPE_STRING);
        $params[] = array($selected->member_seq, TYPE_NUMERIC);
        $params[] = array($selected->pg_method_seq, TYPE_STRING);
        $params[] = array($selected->trx_date, TYPE_STRING);
        $params[] = array($selected->deposit_trx_amt, TYPE_NUMERIC);
        $params[] = array($selected->mutation_type, TYPE_STRING);
        $params[] = array($selected->trx_type, TYPE_STRING);
        $params[] = array($selected->new_status, TYPE_STRING);
        $params[] = array($selected->agent_seq, TYPE_NUMERIC);
//        echo "<pre>";var_dump($params);echo "</pre>";die();
        try {
            return parent::execute_non_query("sp_merchant_return_add_refund", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function return_update_ship_to_member($selected) {
        $params[] = array($selected->merchant_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->return_no, TYPE_STRING);
        $params[] = array($selected->tgl_kirim, TYPE_DATE);
        $params[] = array($selected->expedition_seq, TYPE_STRING);
        $params[] = array($selected->no_awb_merchant, TYPE_STRING);
        $params[] = array($selected->merchant_comment, TYPE_STRING);
        try {
            return parent::execute_non_query("sp_merchant_return_update_ship_to_member", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function return_update_change_status($selected) {
        $params[] = array($selected->merchant_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->return_no, TYPE_STRING);
        $params[] = array($selected->return_status, TYPE_STRING);
        $params[] = array($selected->merchant_comment, TYPE_STRING);
        try {
            return parent::execute_non_query("sp_merchant_return_update_status", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>