<?php

require_once MODEL_BASE;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Report Merchant
 *
 * @author Jartono
 */
class M_report_merchant extends ModelBase {

    public function get_report_order($selected) {

        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->merchant_seq, TYPE_NUMERIC);
        $params[] = array($selected->order, TYPE_STRING);
        $params[] = array($selected->column, TYPE_STRING);
        $params[] = array($selected->order_no_f, TYPE_STRING);
        if ($selected->order_date_s == "") {
            $params[] = array($selected->order_date_s, TYPE_STRING);
        } else {
            $params[] = array($selected->order_date_s, TYPE_DATE);
        }
        if ($selected->order_date_e == "") {
            $params[] = array($selected->order_date_e, TYPE_STRING);
        } else {
            $params[] = array($selected->order_date_e, TYPE_DATE);
        }
        $params[] = array($selected->status_order_f, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_report_order", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_report_redeem($selected) {

        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->merchant_seq, TYPE_NUMERIC);
        $params[] = array($selected->redeem_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_report_redeem", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_report_product($selected) {

        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->merchant_seq, TYPE_NUMERIC);
        $params[] = array($selected->default_value_seq, TYPE_NUMERIC);
        $params[] = array($selected->search, TYPE_STRING);
        $params[] = array($selected->status, TYPE_STRING);       
        
        try {
            return parent::execute_sp_single_query("sp_report_product", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>