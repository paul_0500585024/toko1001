<?php

require_once MODEL_BASE;

class M_agent_customer extends ModelBase {

    public function get_simulation($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->order_seq, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_get_loan_simulation", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_customer_data($data) {
        $param[] = array("", TYPE_STRING);
        $param[] = array($data->user_id, TYPE_STRING);
        $param[] = array($data->customer_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_get_dropdown_customer_search", $param);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>
