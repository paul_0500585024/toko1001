<?php

require_once MODEL_BASE;

class M_order_return_agent extends ModelBase {

    public function get_return_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->agent_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_multi_query("sp_agent_order_product_return_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_return_list_by_no($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->order_no, TYPE_STRING);
        $params[] = array($filter->agent_seq, TYPE_NUMERIC);
        $params[] = array($filter->product_status, TYPE_STRING);
        $params[] = array($filter->order_status, TYPE_STRING);
        $params[] = array($filter->finish_date, TYPE_NUMERIC);

        try {
            return parent::execute_sp_multi_query("sp_agent_order_product_return_by_order_no", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data_return($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->return_no, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_member_order_product_return_get_data", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->return_no, TYPE_STRING);
        $params[] = array($filter->shipment_status, TYPE_STRING);

        try {
            return parent::execute_non_query("sp_member_order_product_return_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add_return($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->return_no, TYPE_STRING);
        $params[] = array($data->order_seq, TYPE_NUMERIC);
        $params[] = array($data->product_variant_seq, TYPE_NUMERIC);
        $params[] = array($data->variant_variant_seq, TYPE_NUMERIC);
        $params[] = array($data->qty, TYPE_NUMERIC);
        $params[] = array($data->return_status, TYPE_STRING);
        $params[] = array($data->shipment_status, TYPE_STRING);
        $params[] = array($data->review_agent, TYPE_STRING);
        $params[] = array($data->awb_agent_no, TYPE_STRING);
        $params[] = array($data->exp_seq_to_admin, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_member_order_product_return_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_order_return_list($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->agent_seq, TYPE_NUMERIC);
        $params[] = array($selected->order_status, TYPE_STRING);
        $params[] = array($selected->finish_date, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_agent_order_list_return_by_agent", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>
