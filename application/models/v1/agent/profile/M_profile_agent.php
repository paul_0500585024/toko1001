<?php

require_once MODEL_BASE;

class M_profile_agent extends ModelBase {

    public function get_order_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->agent_seq, TYPE_NUMERIC);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->order_no, TYPE_STRING);
        $params[] = array($filter->date_search, TYPE_STRING);
        if ($filter->order_date_from == "") {
            $params[] = array(NULL, TYPE_STRING);
        } else {
            $params[] = array($filter->order_date_from, TYPE_DATE);
        }
        if ($filter->order_date_to == "") {
            $params[] = array(NULL, TYPE_STRING);
        } else {
            $params[] = array($filter->order_date_to, TYPE_DATE);
        }
        $params[] = array($filter->payment_status, TYPE_STRING);
        $params[] = array($filter->identity_no, TYPE_STRING);

        try {
            return parent::execute_sp_multi_query("sp_agent_order_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_account_list($filter) {
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->agent_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_multi_query("sp_agent_account_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data_agent($data) {
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->agent_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_multi_query("sp_get_data_agent", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function add_mail_log($selected) {
        $params[] = array($selected->email, TYPE_STRING);
        $params[] = array($selected->email_cd, TYPE_STRING);
        $params[] = array($selected->recipient_name, TYPE_STRING);
        $params[] = array($selected->gencode, TYPE_STRING);
        $params[] = array($selected->subject, TYPE_STRING);
        $params[] = array($selected->content, TYPE_STRING);

        try {
            parent::execute_non_query("sp_merchant_add_forgot_password", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
        return "true";
    }

    public function save_update_contact($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->agent_seq, TYPE_NUMERIC);
        $params[] = array($data->agent_name, TYPE_STRING);
        $params[] = array($data->gender, TYPE_STRING);
        $params[] = array($data->birthday, TYPE_DATE);
        $params[] = array($data->mobile_phone, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_agent_update_contact", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_account($data) {
        $params[] = array($data->trx_no, TYPE_STRING);
        try {
            return parent::execute_non_query("sp_agent_account_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_payment($data) {
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->agent_seq, TYPE_NUMERIC);
        $params[] = array($data->bank_name, TYPE_STRING);
        $params[] = array($data->bank_branch_name, TYPE_STRING);
        $params[] = array($data->bank_acct_no, TYPE_STRING);
        $params[] = array($data->bank_acct_name, TYPE_STRING);

        try {

            return parent::execute_non_query("sp_agent_update_payment", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add_agent_account($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->agent_seq, TYPE_NUMERIC);
        $params[] = array($data->mutation_type, TYPE_STRING);
        $params[] = array($data->trx_type, TYPE_STRING);
        $params[] = array($data->pg_method_seq, TYPE_NUMERIC);
        $params[] = array($data->trx_no, TYPE_STRING);
        $params[] = array($data->deposit_trx_amt, TYPE_NUMERIC);
        $params[] = array($data->non_deposit_trx_amt, TYPE_NUMERIC);
        $params[] = array($data->bank_name, TYPE_STRING);
        $params[] = array($data->bank_branch_name, TYPE_STRING);
        $params[] = array($data->bank_acct_no, TYPE_STRING);
        $params[] = array($data->bank_acct_name, TYPE_STRING);
        $params[] = array($data->status, TYPE_STRING);
        $params[] = array($data->conf_topup_date, TYPE_DATE);
        $params[] = array($data->conf_topup_type, TYPE_STRING);
        $params[] = array($data->conf_topup_file, TYPE_STRING);
        $params[] = array($data->conf_topup_bank_seq, TYPE_NUMERIC);

        try {

            parent::execute_non_query("sp_agent_account_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    function get_date_account() {
        $query = $this->db->query("select now() as tgl");
        foreach ($query->result()as $row) {
            $tgl = $row->tgl;
            return $tgl;
        }
    }

    public function get_info_password($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->old_password, TYPE_STRING);
        $params[] = array($data->new_password, TYPE_STRING);
        $params[] = array($data->encrypt_old_password, TYPE_STRING);
        $params[] = array($data->encrypt_new_password, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_get_info_password_agent", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_settings($data) {
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->user_id, TYPE_NUMERIC);
        $params[] = array($data->encrypt_new_password, TYPE_STRING);

        try {
            return parent::execute_non_query("sp_agent_update_settings", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_address($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->agent_seq, TYPE_NUMERIC);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->payment_status, TYPE_STRING);
        $params[] = array($data->pg_method_seq, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_agent_payment_retry_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_profile($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->agent_seq, TYPE_NUMERIC);
        $params[] = array($filter->profile_img, TYPE_STRING);
        try {
            parent::execute_non_query("sp_agent_update_profile_image", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_order_list_detail($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->agent_seq, TYPE_NUMERIC);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->hseq, TYPE_STRING);

        try {
            return parent::execute_sp_multi_query("sp_agent_order_list_detail", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_payment_retry_detail($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->agent_seq, TYPE_NUMERIC);
        $params[] = array($filter->hseq, TYPE_STRING);

        try {
            return parent::execute_sp_multi_query("sp_agent_order_list_detail_by_order_no", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data_header($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_agent_order_list_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_payment_retry_detail($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->agent_seq, TYPE_NUMERIC);
        $params[] = array($data->order_no, TYPE_STRING);
        $params[] = array($data->payment_status, TYPE_STRING);
        $params[] = array($data->pg_method_seq, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_agent_payment_retry_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_data_account($data) {
        $params[] = array($data->trx_no, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_agent_account_by_trx_no", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_agent_review($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->agent_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_multi_query("sp_review_product_list_agent", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_review($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->product_variant_seq, TYPE_NUMERIC);
        $params[] = array($filter->order_seq, TYPE_NUMERIC);
        $params[] = array($filter->rate, TYPE_NUMERIC);
        $params[] = array($filter->review_text, TYPE_STRING);
        try {
            parent::execute_non_query("sp_review_product_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_review_agent_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array("", TYPE_STRING);
        $params[] = array($filter->agent_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_multi_query("sp_review_product_list_admin", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_message_log_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array(MEMBER, TYPE_STRING);
        $params[] = array($filter->agent_seq, TYPE_NUMERIC);
        $params[] = array($filter->status, TYPE_STRING);
        try {
            return parent::execute_sp_multi_query("sp_message_log_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_insert_message($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->content, TYPE_STRING);
        $params[] = array($filter->prev_message_seq, TYPE_NUMERIC);
        $params[] = array($filter->agent_seq, TYPE_NUMERIC);
        $params[] = array(0, TYPE_NUMERIC);
        $params[] = array("", TYPE_STRING);
        try {
            parent::execute_non_query("sp_message_log_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_message_log_list_by_seq($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->seq, TYPE_NUMERIC);
        $params[] = array(MEMBER, TYPE_STRING);
        $params[] = array($filter->agent_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_message_log_list_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_email_harbolnas($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->email_cd, TYPE_STRING);
        $params[] = array($filter->limit, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_email_harbolnas", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_notification_agent_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->agent_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_multi_query("sp_notification_agent_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_notification_total_by_status($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->status, TYPE_STRING);
        $params[] = array($filter->user_seq, TYPE_NUMERIC);
        $params[] = array($filter->user_group, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_notification_total_by_status", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_notification($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_STRING);
        $params[] = array($data->status, TYPE_STRING);
        $params[] = array($data->table, TYPE_STRING);

        try {
            parent::execute_non_query("sp_notification_save_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_redeem_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->agent_seq, TYPE_NUMERIC);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->payment_status, TYPE_STRING);
        try {
            return parent::execute_sp_multi_query("sp_agent_order_redeem_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_agent_customer_list($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->agent_seq, TYPE_NUMERIC);
        $params[] = array($filter->pic_name, TYPE_STRING);
        $params[] = array($filter->identity_no, TYPE_STRING);
        $params[] = array($filter->email_address, TYPE_STRING);
        try {
            return parent::execute_sp_multi_query("sp_agent_customer_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_redeem_info($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->agent_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_multi_query("sp_agent_order_redeem_info", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_deposit_amt($data) {
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->agent_seq, TYPE_STRING);
        try {
            return parent::execute_sp_multi_query("sp_get_deposit_agent", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>