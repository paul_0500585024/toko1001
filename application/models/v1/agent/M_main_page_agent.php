<?php

require_once MODEL_BASE;

class M_main_page_agent extends ModelBase {

    public function get_list_product($filter) {

        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->page, TYPE_NUMERIC);
        $params[] = array($filter->record_per_page, TYPE_NUMERIC);
        $params[] = array($filter->search, TYPE_STRING);
        $params[] = array($filter->filter, TYPE_STRING);

        try {
            return parent::execute_sp_multi_query("sp_product_list_view", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>
