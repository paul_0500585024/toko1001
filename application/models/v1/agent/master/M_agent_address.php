<?php

require_once MODEL_BASE;

class M_agent_address extends ModelBase {

    public function get_data($selected) {
        $params[] = array($selected->agent_seq, TYPE_NUMERIC);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->address_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_agent_address_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add($data) {
        $params[] = array($data->user_id, TYPE_NUMERIC);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->agent_seq, TYPE_NUMERIC);
        $params[] = array($data->pic_name, TYPE_STRING);
        $params[] = array($data->address, TYPE_STRING);
        $params[] = array($data->district_seq, TYPE_NUMERIC);
        $params[] = array($data->phone_no, TYPE_STRING);
        $params[] = array($data->zip_code, TYPE_STRING);
        $params[] = array($data->alias, TYPE_STRING);

        try {
            parent::execute_non_query("sp_agent_address_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($data) {
        $params[] = array($data->user_id, TYPE_NUMERIC);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->address_seq, TYPE_NUMERIC);
        $params[] = array($data->alias, TYPE_STRING);
        $params[] = array($data->pic_name, TYPE_STRING);
        $params[] = array($data->address, TYPE_STRING);
        $params[] = array($data->district_seq, TYPE_NUMERIC);
        $params[] = array($data->zip_code, TYPE_STRING);
        $params[] = array($data->phone_no, TYPE_STRING);

        try {
            parent::execute_non_query("sp_agent_address_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_delete($data) {
        $params[] = array($data->agent_seq, TYPE_NUMERIC);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->address_seq, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_agent_address_delete", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>