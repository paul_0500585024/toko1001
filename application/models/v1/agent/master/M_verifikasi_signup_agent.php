<?php

require_once MODEL_BASE;

class M_verifikasi_signup_agent extends ModelBase {

    function get_email_by_log($sel) {
        $params[] = array($sel->uri, TYPE_STRING);
        $params[] = array($sel->EmailCd, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_email_log_by_code", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    function get_agent_by_email($sel) {
        $params[] = array($sel->email, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_agent_by_email", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    function save_update_agent($sel) {
        $params[] = array($sel->email, TYPE_STRING);

        try {
            return parent::execute_non_query("sp_agent_approve_by_email", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    function get_promo($data) {
        $params[] = array($data->node_cd, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_promo_voucher_period_by_name", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    function save_update_voucher($data) {
        $params[] = array($data->promo_seq, TYPE_NUMERIC);
        $params[] = array($data->agent_seq, TYPE_NUMERIC);
        $params[] = array($data->active_date, TYPE_DATE);
        $params[] = array($data->exp_date, TYPE_DATE);

        try {
            parent::execute_non_query("sp_update_agent_promo_voucher", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    function save_add_voucher($data) {
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->agent_seq, TYPE_NUMERIC);
        $params[] = array($data->code, TYPE_STRING);
        $params[] = array($data->active_date, TYPE_DATE);
        $params[] = array($data->exp_date, TYPE_DATE);

        try {
            parent::execute_non_query("sp_agent_promo_voucher_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>
