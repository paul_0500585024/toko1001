<?php

require_once MODEL_BASE;

class M_forgot_password_agent extends ModelBase {

    function get_date() {

        $query = $this->db->query("select now() as tgl");
        foreach ($query->result()as $row) {
            $tgl = $row->tgl;
            return $tgl;
        }
    }

    public function save_add_log_email($data) {

        $params[] = array($data->email, TYPE_STRING);
        $params[] = array($data->email_cd, TYPE_STRING);
        $params[] = array($data->name, TYPE_STRING);
        $params[] = array($data->gencode, TYPE_STRING);
        $params[] = array($data->subject, TYPE_STRING);
        $params[] = array($data->content, TYPE_STRING);
        //var_dump($params);
        try {

            parent::execute_non_query("sp_merchant_add_forgot_password", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
        return "true";
    }

}

?>
