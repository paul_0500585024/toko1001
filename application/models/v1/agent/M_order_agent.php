<?php

require_once MODEL_BASE;

class M_order_agent extends ModelBase {

    public function get_order_list_detail($filter) {

        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->agent_seq, TYPE_NUMERIC);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->hseq, TYPE_STRING);

        try {
            return parent::execute_sp_multi_query("sp_agent_order_list_detail", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_order_info($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->hseq, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_agent_order_list_by_header", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_order_info_by_order_no($selected) {

        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->key, TYPE_STRING);

        try {
            return parent::execute_sp_multi_query("sp_agent_order_list_by_order_no", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_order_signature($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->order_seq, TYPE_NUMERIC);
        $params[] = array($data->signature, TYPE_STRING);
        $params[] = array($data->order_no, TYPE_STRING);

        try {
            parent::execute_non_query("sp_order_update_signature", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_payment_code_docu($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->order_no, TYPE_STRING);
        $params[] = array($data->payment_code, TYPE_STRING);

        try {
            parent::execute_non_query("sp_order_update_payment_code", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_credit_info_by_credit_seq($selected) {

        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->promo_credit_seq, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_get_credit_info_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_axa_agent($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->agent_email, TYPE_STRING);
        $params[] = array($data->agent_name, TYPE_STRING);
        $params[] = array($data->agent_birthday, TYPE_DATE);
        $params[] = array($data->gender, TYPE_STRING);
        $params[] = array($data->agent_phone, TYPE_STRING);
        $params[] = array($data->order_seq, TYPE_STRING);

        try {
            parent::execute_non_query("sp_agent_axa_signup", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_axa_agent($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->order_seq, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_get_agent_axa", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>
