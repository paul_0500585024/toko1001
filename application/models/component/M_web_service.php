<?php

require_once MODEL_BASE;

class M_web_service extends ModelBase {

    public function get_rate_cache($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->expedition_seq, TYPE_NUMERIC);
        $params[] = array($data->from_district_code, TYPE_STRING);
        $params[] = array($data->to_district_code, TYPE_STRING);
        $params[] = array($data->exp_service_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_get_rate_cache_expedition", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_rate_cache($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->expedition_seq, TYPE_NUMERIC);
        $params[] = array($data->from_district_code, TYPE_STRING);
        $params[] = array($data->to_district_code, TYPE_STRING);
        $params[] = array($data->exp_service_seq, TYPE_NUMERIC);
        $params[] = array($data->rate, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_expedition_rate_cache_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }
    
        public function save_update_rate_cache($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->expedition_seq, TYPE_NUMERIC);
        $params[] = array($data->from_district_code, TYPE_STRING);
        $params[] = array($data->to_district_code, TYPE_STRING);
        $params[] = array($data->exp_service_seq, TYPE_NUMERIC);
        $params[] = array($data->rate, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_expedition_rate_cache_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_web_service($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->function_name, TYPE_STRING);
        $params[] = array($data->type, TYPE_STRING);
        $params[] = array($data->request_datetime, TYPE_DATETIME);

        try {
            return parent::execute_sp_single_query("sp_web_service_log_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_service($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->web_service_seq, TYPE_STRING);
        $params[] = array($data->request, TYPE_STRING);
        $params[] = array($data->response, TYPE_STRING);
        $params[] = array($data->response_datetime, TYPE_DATETIME);

        try {
            parent::execute_non_query("sp_web_service_log_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }



}
?>


