<?php

require_once MODEL_BASE;

class M_notification extends ModelBase {

    public function get_notif_template($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->code, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_notif_template_by_code", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_notif_log_merchant($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->notif_code, TYPE_STRING);
        $params[] = array($data->merchant_seq, TYPE_STRING);
        $params[] = array($data->content, TYPE_STRING);
        $params[] = array($data->status, TYPE_STRING);

        try {
            parent::execute_non_query("sp_notif_log_add_merchant", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_notif_log_admin($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->notif_code, TYPE_STRING);
        $params[] = array($data->content, TYPE_STRING);
        $params[] = array($data->status, TYPE_STRING);

        try {
            parent::execute_non_query("sp_notif_log_add_admin", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_notif_log_member($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->notif_code, TYPE_STRING);
        $params[] = array($data->member_seq, TYPE_STRING);
        $params[] = array($data->content, TYPE_STRING);
        $params[] = array($data->status, TYPE_STRING);

        try {
            parent::execute_non_query("sp_notif_log_add_member", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_notif_log_agent($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->notif_code, TYPE_STRING);
        $params[] = array($data->agent_seq, TYPE_STRING);
        $params[] = array($data->content, TYPE_STRING);
        $params[] = array($data->status, TYPE_STRING);

        try {
            parent::execute_non_query("sp_notif_log_add_agent", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_list_merchant($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->start, TYPE_NUMERIC);
        $params[] = array($filter->length, TYPE_NUMERIC);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->column, TYPE_STRING);
        $params[] = array($filter->merchant_id, TYPE_NUMERIC);

        try {
            return parent::execute_sp_multi_query("sp_notification_merchant_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->seq, TYPE_STRING);
        $params[] = array($data->status, TYPE_STRING);
        $params[] = array($data->table, TYPE_STRING);

        try {
            parent::execute_non_query("sp_notification_save_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_list_merchant_detail($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->notif_seq, TYPE_NUMERIC);
        $params[] = array($filter->merchant_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_multi_query("sp_notification_merchant_list_detail", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_notification_total_by_status($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->status, TYPE_STRING);
        $params[] = array($filter->user_seq, TYPE_NUMERIC);
        $params[] = array($filter->user_group, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_notification_total_by_status", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_notification_top_list_by_status($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->status, TYPE_STRING);
        $params[] = array($filter->user_seq, TYPE_NUMERIC);
        $params[] = array($filter->top, TYPE_NUMERIC);
        $params[] = array($filter->user_group, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_notification_top_list_by_status", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_notification_merchant_menu($filter) {
        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->user_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_multi_query("sp_notification_merchant_menu", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>