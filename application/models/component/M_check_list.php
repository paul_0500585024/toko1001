<?php

require_once MODEL_BASE;

class M_check_list extends ModelBase {

    public function get_attribute_check_list($filter) {

        $params[] = array($filter->seq, TYPE_NUMERIC);
        $params[] = array($filter->parent_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_get_attribute_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>