<?php

require_once MODEL_BASE;

class M_dropdown_list extends ModelBase {

    public function get_dropdown_province() {

        try {
            return parent::execute_sp_single_query("sp_get_dropdown_province");
            //
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_city() {
        try {
            return parent::execute_sp_single_query("sp_get_dropdown_city");
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_city_by_province($province_seq) {
        try {
            $param[] = array($province_seq, TYPE_NUMERIC);

            return parent::execute_sp_single_query("sp_get_dropdown_city_by_province", $param);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_merchant() {
        try {
            return parent::execute_sp_single_query("sp_get_dropdown_merchant");
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_partner() {
        try {
            return parent::execute_sp_single_query("sp_get_dropdown_partner");
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_member_email() {

        try {
            return parent::execute_sp_single_query("sp_get_dropdown_member_email");
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_member() {

        try {
            return parent::execute_sp_single_query("sp_get_dropdown_member_by_name");
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_product() {

        try {
            return parent::execute_sp_single_query("sp_get_dropdown_product");
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_member_email_return($merchant_seq) {
        $param[] = array($merchant_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_get_dropdown_member_email_return", $param);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_setting_type() {

        try {
            return parent::execute_sp_single_query("sp_get_dropdown_setting_type");
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_payment_gateway_method() {

        try {
            return parent::execute_sp_single_query("sp_get_m_payment_gateway_method");
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_product_category() {

        try {
            return parent::execute_sp_single_query("sp_get_dropdown_product_category_admin");
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_all_product_category() {

        try {
            return parent::execute_sp_single_query("sp_get_dropdown_all_product_category");
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_menu_module() {

        try {
            return parent::execute_sp_single_query("sp_get_dropdown_menu_module");
            //
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_user_group() {

        try {
            return parent::execute_sp_single_query("sp_get_dropdown_user_group");
            //
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_promo_voucher_node() {

        try {
            return parent::execute_sp_single_query("sp_get_dropdown_promo_voucher_node");
            //
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_variant($idSeq = 0, $pTipe = 0) {

        try {
            $param[] = array($idSeq, TYPE_NUMERIC);
            $param[] = array($pTipe, TYPE_NUMERIC);
            return parent::execute_sp_single_query("sp_get_dropdown_variant", $param);
            //
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_expedition() {

        try {
            return parent::execute_sp_single_query("sp_get_dropdown_expedition");
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_expedition_service($expedition_seq) {
        try {
            $param[] = array($expedition_seq, TYPE_NUMERIC);

            return parent::execute_sp_single_query("sp_get_dropdown_expedition_service", $param);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_district() {

        try {
            return parent::execute_sp_single_query("sp_get_dropdown_district");
            //
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_attribute() {

        try {
            return parent::execute_sp_single_query("sp_get_dropdown_attribute");
            //
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_payment_method() {

        try {
            return parent::execute_sp_single_query("sp_get_dropdown_payment_method");
            //
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_district_by_city($idSeq) {
        $param[] = array($idSeq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_get_dropdown_district_by_city", $param);
            //
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

//    public function get_dropdown_district_by_city_and_exp($idSeq, $exp_seq) {
//        $param[] = array($idSeq, TYPE_NUMERIC);
//        $param[] = array($exp_seq, TYPE_STRING);
//
//        try {
//            return parent::execute_sp_single_query("sp_get_dropdown_district_by_city_and_exp", $param);
//        } catch (Exception $ex) {
//            parent::handle_database_error($ex);
//        }
//    }

    public function get_dropdown_redeem_period($data) {

        $param[] = array($data->exp_seq, TYPE_NUMERIC);
        $param[] = array($data->seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_get_dropdown_redeem_period", $param);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_order_no_by_order($data) {
        $params[] = array($data->exp_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_get_dropdown_order_no_by_order", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_order_no_by_order_status($data) {
        $params[] = array($data->exp_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_get_dropdown_order_no_by_order_status", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_merchant_by_order($order_seq) {
        $params[] = array($order_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_get_dropdown_merchant_by_order", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_address_member($member_seq) {
        $params[] = array($member_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_get_dropdown_member_address", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_address_member_by_seq($data) {
        $params[] = array($data->member_seq, TYPE_NUMERIC);
        $params[] = array($data->seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_multi_query("sp_get_dropdown_member_address_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_provider() {
        try {
            return parent::execute_sp_single_query("sp_get_dropdown_provider");
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_provider_service($provider_seq) {

        $params[] = array($provider_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_get_dropdown_provider_service", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_provider_service_nominal($provider_service_seq) {
        $params[] = array($provider_service_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_get_dropdown_provider_service_nominal", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_phone_number_code($phone_number_code) {
        $params[] = array($phone_number_code, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_get_dropdown_phone_number_code", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_bank_credit_by_variant($product_variant_seq) {
        $param[] = array($product_variant_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_get_dropdown_bank_credit_by_product_variant_seq", $param);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_bank_credit_month_by_variant($product_variant_seq, $bankid) {
        $param[] = array($bankid, TYPE_NUMERIC);
        $param[] = array($product_variant_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_get_dropdown_bank_credit_month_by_product_variant_seq", $param);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_bank_credit_month_by_promoseq($promo_credit_seq) {
        $param[] = array($promo_credit_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_get_bank_credit_month_by_promo_credit_seq", $param);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_credit($data) {
        $params[] = array($data->product_variant_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_multi_query("sp_get_dropdown_credit", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_address_agent($agent_seq) {
        $params[] = array($agent_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_get_dropdown_agent_address", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_address_agent_by_seq($data) {
        $params[] = array($data->agent_seq, TYPE_NUMERIC);
        $params[] = array($data->seq, TYPE_NUMERIC);
        try {
                return parent::execute_sp_multi_query("sp_get_dropdown_agent_address_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_agent_email() {

        try {
            return parent::execute_sp_single_query("sp_get_dropdown_agent_email");
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_partner_name() {
        try {
            return parent::execute_sp_single_query("sp_get_dropdown_partner_name");
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_category_name_lvl() {

        try {
            return parent::execute_sp_single_query("sp_get_dropdown_category_name_lvl");
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_partner_loan() {

        try {
            return parent::execute_sp_single_query("sp_get_dropdown_partner_loan");
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_partner_dp_category() {

        try {
            return parent::execute_sp_single_query("sp_get_dropdown_partner_dp_category");
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_partner_dp_tenor_by_category($product_category_seq) {

        try {
            $param[] = array($product_category_seq, TYPE_NUMERIC);

            return parent::execute_sp_single_query("sp_get_dropdown_partner_dp_tenor_by_category", $param);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_customer_search($keyword = "", $agent_id, $customer_seq = 0) {
        $param[] = array($keyword, TYPE_STRING);
        $param[] = array($agent_id, TYPE_STRING);
        $param[] = array($customer_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_multi_query("sp_get_dropdown_customer_search", $param);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_partner_dp_get_partner_name() {

        try {

            return parent::execute_sp_single_query("sp_get_dropdown_partner_dp_get_partner_name");
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_dropdown_partner_dp_category_by_seq($partner_seq) {

        try {
            $param[] = array($partner_seq, TYPE_NUMERIC);
            return parent::execute_sp_single_query("sp_get_dropdown_partner_dp_category_by_seq", $param);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}
?>



