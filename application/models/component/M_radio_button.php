<?php

require_once MODEL_BASE;

class M_radio_button extends ModelBase {

    public function get_bank_list() {
        
        try {
            return parent::execute_sp_single_query("sp_get_bank_list");
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>
