<?php

require_once MODEL_BASE;

class M_voucher extends ModelBase {

    function get_promo($data) {

        $params[] = array($data->node_cd, TYPE_STRING);
        $params[] = array($data->total_order, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_promo_voucher_period_by_name", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    function save_update_voucher($data) {
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->member_seq, TYPE_NUMERIC);
        $params[] = array($data->active_date, TYPE_DATE);
        $params[] = array($data->exp_date, TYPE_DATE);

        try {
            parent::execute_non_query("sp_update_member_promo_voucher", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    function save_add_voucher($data) {
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->member_seq, TYPE_NUMERIC);
        $params[] = array($data->code, TYPE_STRING);
        $params[] = array($data->active_date, TYPE_DATE);
        $params[] = array($data->exp_date, TYPE_DATE);

        try {
            parent::execute_non_query("sp_member_promo_voucher_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add_refund($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        if ($data->agent_seq) {
            $params[] = array($data->agent_seq, TYPE_NUMERIC);
            $params[] = array($data->conf_topup_bank_seq, TYPE_STRING);
        } else {
            $params[] = array($data->member_seq, TYPE_NUMERIC);
        }
        $params[] = array($data->mutation_type, TYPE_STRING);
        $params[] = array($data->pg_method_seq, TYPE_NUMERIC);
        $params[] = array($data->trxno, TYPE_STRING);
        $params[] = array($data->depositamount, TYPE_NUMERIC);
        $params[] = array($data->nondepositamount, TYPE_NUMERIC);
        try {
            if ($data->agent_seq) {
                return parent::execute_non_query("sp_merchant_deliver_refund_agent_add", $params);
            } else {
                return parent::execute_non_query("sp_merchant_deliver_refund_add", $params);
            }
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_voucher_refund($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->voucherseq, TYPE_NUMERIC);
        $params[] = array($data->newvoucher, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_merchant_deliver_voucher_refund", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>
