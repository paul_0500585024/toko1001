<?php

require_once MODEL_BASE;

class M_tree_view extends ModelBase {

    public function get_menu_product_category() {

        try {
            $data = parent::execute_sp_single_query("sp_get_tree_view_category_order");
            
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
        foreach ($data as $row) {
            $response[$row->parent_seq][] = $row;
        }
        return $response;
    }

    public function get_menu_form($module_seq) {

        $params[] = array($module_seq, TYPE_NUMERIC);

        try {
            $data = parent::execute_sp_single_query("sp_get_tree_view_menu", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
        if (isset($data)) {
            foreach ($data as $row) {
                $response[$row->parent_menu_cd][] = $row;
            }
            return $response;
        }
    }

    public function get_payment_gateway() {
        try {
            $data = parent::execute_sp_single_query("sp_get_payment_gateway_list");
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
        foreach ($data as $row) {
            $response[$row->parent_seq][] = $row;
        }
        return $response;
    }

    public function get_menu($datas, $parent = 0, $p_level = 0) {
        static $i = 1;
        $tab = str_repeat(" ", $i);
        if (isset($datas[$parent])) {
            $html = "<ul>";
            $i++;
            foreach ($datas[$parent] as $vals) {

                if ($vals->level == 1) {
                    $this->parent_level = $vals->seq;
                    $p_level = $vals->seq;
                }
                $child = $this->get_menu($datas, $vals->seq, $this->parent_level);
                $html .= "$tab";
                $html.='<li><a href="javascript:pilihmenu(\'' . $vals->seq . '~' . $vals->level . '~' . $vals->name . '~' . $p_level . '\')">' . $vals->name . '</a>';
                if ($child) {
                    $i++;
                    $html .= $child;
                    $html .= "$tab";
                }
                $html .= '</li>';
            }
            $html .= "$tab</ul>";
            return $html;
        } else {
            return false;
        }
    }
    
        //get payment type
    public function get_payment_gateway_type() {
        try {
            $data = parent::execute_sp_single_query("sp_get_payment_gateway_type_list");
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
        return $data;
    }

    public function get_payment_gateway_method_list($data) {
        $params[] = array($data, TYPE_NUMERIC);
        try {
            $data = parent::execute_sp_single_query("sp_get_payment_gateway_method_list", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
        return $data;
    }

}

?>