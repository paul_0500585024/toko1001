<?php

require_once MODEL_BASE;

class M_email extends ModelBase {

    public function get_email_template($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->email_cd, TYPE_STRING);

	try {
	    return parent::execute_sp_single_query("sp_email_template_by_code", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function save_email_log($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->email_cd, TYPE_STRING);
	$params[] = array($data->email_code, TYPE_STRING);
	$params[] = array($data->name, TYPE_STRING);
	$params[] = array($data->email, TYPE_STRING);
	$params[] = array($data->subject, TYPE_STRING);
	$params[] = array($data->contentsaved, TYPE_STRING);
	$params[] = array($data->stemail, TYPE_STRING);

	try {
	    parent::execute_non_query("sp_email_log_add", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_email_setting($data, $type = 1) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($type, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_setting_by_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_email_log($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->email_code, TYPE_STRING);
	try {
	    return parent::execute_sp_single_query("sp_email_log_by_code_only", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

}

?>