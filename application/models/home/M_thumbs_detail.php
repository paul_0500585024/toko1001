<?php

require_once MODEL_BASE;

class M_thumbs_detail extends ModelBase {

    public function get_thumbs_detail($filter) {

        $params[] = array($filter->product_variant_seq, TYPE_NUMERIC);
        $params[] = array($filter->agent, TYPE_STRING);
        $params[] = array($filter->agent_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_multi_query("sp_thumbs_detail", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_thumbs_detail_attrib($filter) {
        $params[] = array($filter->product_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_thumbs_detail_attrib", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_thumbs_detail_spec($filter) {
        $params[] = array($filter->product_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_thumbs_detail_spec", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_thumbs_delivery_promo_city($filter) {
        $params[] = array($filter->merchant_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_thumbs_delivery_promo_city", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_thumbs_product_review($filter) {
        $params[] = array($filter->product_variant_seq, TYPE_STRING);
        $params[] = array($filter->start, TYPE_STRING);
        $params[] = array($filter->limit, TYPE_STRING);
        $params[] = array($filter->where, TYPE_STRING);
        $params[] = array($filter->order, TYPE_STRING);
        try {
            $result['product_review'] = parent::execute_sp_single_query("sp_thumbs_product_review", $params);
            $result['row'] = $this->db->query("SELECT FOUND_ROWS() AS total_row");
            return $result;
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_thumbs_credit($filter) {//ini tambahan 9:43 8/9/2016

        $params[] = array($filter->product_variant_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_multi_query("sp_thumbs_detail_credit", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>