<?php

require_once MODEL_BASE;

class M_credit_product extends ModelBase {

    public function get_product_list($filter) {
        $params[] = array($filter->start, TYPE_STRING);
        $params[] = array($filter->limit, TYPE_STRING);
        $params[] = array($filter->where, TYPE_STRING);
        $params[] = array($filter->order, TYPE_STRING);
        try {
            $result['product'] = parent::execute_sp_single_query("sp_get_credit_product_list", $params);
            $result['row'] = $this->db->query("SELECT FOUND_ROWS() AS total_row");
//            die(var_dump($result['row']));
//            var_dump($result['row']);die();
            return $result;
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }
}
