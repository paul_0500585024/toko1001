<?php

require_once MODEL_BASE;

class M_merchant extends ModelBase {

    public function get_merchant_info($filter) {
	$params[] = array($filter->user_id, TYPE_STRING);
	$params[] = array($filter->ip_address, TYPE_STRING);
	$params[] = array($filter->m_code, TYPE_STRING);

	try {
	    return parent::execute_sp_single_query("sp_merchant_by_code", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_district_info($filter) {
	$params[] = array($filter->user_id, TYPE_STRING);
	$params[] = array($filter->ip_address, TYPE_STRING);
	$params[] = array($filter->district_seq, TYPE_NUMERIC);

	try {
	    return parent::execute_sp_single_query("sp_district_by_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function get_promo_free_fee($filter) {
	$params[] = array($filter->merchant_seq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_get_promo_free_fee_city_by_merchant", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

}

?>