<?php

require_once MODEL_BASE;

class M_product extends ModelBase {

    public function get_product_info($filter) {

        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->product_seq, TYPE_NUMERIC);
        $params[] = array($filter->exp_seq, TYPE_NUMERIC);
        $params[] = array($filter->agent, TYPE_STRING);
        $params[] = array($filter->agent_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_get_product_info_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_product_credit($filter) {

        $params[] = array($filter->user_id, TYPE_STRING);
        $params[] = array($filter->ip_address, TYPE_STRING);
        $params[] = array($filter->product_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_get_product_info_credit_by_seq", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>