<?php

require_once MODEL_BASE;

class M_thumbs_new_products extends ModelBase {

    public function get_thumbs_new_products($filter) {

        $params[] = array($filter->start, TYPE_STRING);
        $params[] = array($filter->limit, TYPE_STRING);
        $params[] = array($filter->where, TYPE_STRING);
        $params[] = array($filter->order, TYPE_STRING);
        $params[] = array($filter->agent, TYPE_STRING);
        $params[] = array($filter->agent_seq, TYPE_NUMERIC);
        try {
            $result['product'] = parent::execute_sp_single_query("sp_thumbs_new_products", $params);
            $result['row'] = $this->db->query("SELECT FOUND_ROWS() AS total_row");
            return $result;
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_search_home($filter) {

        $params[] = array($filter->limit, TYPE_NUMERIC);
        $params[] = array($filter->category, TYPE_NUMERIC);
        $params[] = array($filter->search, TYPE_STRING);
        try {
            return parent::execute_sp_multi_query("sp_search_home", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>