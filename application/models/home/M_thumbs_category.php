<?php

require_once MODEL_BASE;

class M_thumbs_category extends ModelBase {

    public function get_thumbs_category_img($filter) {

        $params[] = array($filter->category_seq, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_thumbs_category_img", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_thumbs_category_filter($filter) {

        $params[] = array($filter->category_seq, TYPE_STRING);

        try {
            $data = parent::execute_sp_multi_query("sp_thumbs_category_filter", $params);
            $filter = isset($data[0]) ? $data[0] : array();
            $filter_detail = isset($data[1]) ? $data[1] : array();
            $return_data['name'] = $filter;
            foreach ($filter_detail as $each_filter_detail) {
                $return_data['detail'][$each_filter_detail->attribute_category_attribute_seq][] = $each_filter_detail;
            }
            return $return_data;
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_product_seq($filter) {
        //to get the real attribute value
        $attribute_value_input = array();
        foreach ($filter as $attribute => $array_attribute_value) {
            foreach ($array_attribute_value as $each_attribute_value) {
                $attribute_value_input[] = $each_attribute_value;
            }
        }
        //get attribute_seq and seq from filter
        $sql = "SELECT attribute_seq,seq FROM m_attribute_value WHERE seq IN (" . implode(',', $attribute_value_input) . ") order by attribute_seq,seq";
        $Rs = $this->db->query($sql);
        $data_attribute = "";
        $data_attribute_seq = array();
        $counter = 0;
        foreach ($Rs->result() as $each_result) {
            if ($data_attribute != $each_result->attribute_seq) {
                $counter++;
            }
            $data_attribute = $each_result->attribute_seq;
            $data_attribute_seq[$counter][] = $each_result->seq;
        }
        //to get product_seq from attribute_value_seq
        $sql = 'SELECT product_seq FROM m_product_attribute ';
        if ($counter > 0) {
            $sql .= ' WHERE 1 ';
            for ($i = 1; $i <= $counter; $i++)
                $sql .= ($i > 1?' OR': ' AND'). ' attribute_value_seq IN (' . implode(',', $data_attribute_seq[$i]) . ')';
			if($counter>1)
				$sql .= ' GROUP BY product_seq HAVING COUNT(*)>1';			
        }
//        die($sql);
        if ($sql != '') {
            $Rs = $this->db->query($sql);
            if (count($Rs->result()) > 0) {
                return $Rs->result();
            }
        }
        return "''";
    }

}

?>