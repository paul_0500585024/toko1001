<?php

require_once MODEL_BASE;

class M_deposit_member extends ModelBase {

    public function save_member_account($data, $tipe = "") {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->member_seq, TYPE_NUMERIC);
        $params[] = array($data->mutation_type, TYPE_STRING);
        $params[] = array($data->account_type, TYPE_STRING);
        $params[] = array($data->payment_method_seq, TYPE_NUMERIC);
        $params[] = array($data->trx_no, TYPE_STRING);
        $params[] = array($data->deposit_amt, TYPE_NUMERIC);
        $params[] = array($data->non_deposit_amt, TYPE_NUMERIC);
        $params[] = array($data->bank_name, TYPE_STRING);
        $params[] = array($data->bank_branch_name, TYPE_STRING);
        $params[] = array($data->bank_account_no, TYPE_STRING);
        $params[] = array($data->bank_account_name, TYPE_STRING);
        $params[] = array($data->status, TYPE_STRING);

        try {
            if ($tipe == AGENT) {
                $params[] = array(date("Y-m-d"), TYPE_DATE);
                $params[] = array($data->topup_tipe, TYPE_STRING);
                $params[] = array("", TYPE_STRING);
                $params[] = array(0, TYPE_NUMERIC);
                parent::execute_non_query("sp_agent_account_add", $params);
            } else {
                parent::execute_non_query("sp_member_account_add", $params);
            }
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_status_member_acoount($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->member_seq, TYPE_NUMERIC);
        $params[] = array($data->seq, TYPE_NUMERIC);
        $params[] = array($data->status, TYPE_STRING);

        try {
            parent::execute_non_query("sp_member_account_update_status", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function add_deposit_member($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->member_seq, TYPE_STRING);
        $params[] = array($data->deposit_amt, TYPE_NUMERIC);

        try {
            parent::execute_non_query("sp_deposit_member_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function substract_deposit_member($data, $tipe = "") {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->member_seq, TYPE_STRING);
        $params[] = array($data->deposit_amt, TYPE_NUMERIC);

        try {
            if ($tipe == AGENT) {
                parent::execute_non_query("sp_deposit_agent_substract", $params);
            } else {
                parent::execute_non_query("sp_deposit_member_substract", $params);
            }
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}
?>

