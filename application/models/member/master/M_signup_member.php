<?php

require_once MODEL_BASE;

class M_signup_member extends ModelBase {

    public function save_add($data) {
	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->email, TYPE_STRING);
	$params[] = array($data->name, TYPE_STRING);
	$params[] = array($data->date, TYPE_DATE);
	$params[] = array($data->gender, TYPE_STRING);
	$params[] = array($data->phone, TYPE_STRING);
	$params[] = array($data->password, TYPE_STRING);

	try {
	    parent::execute_non_query("sp_member_add_signup", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function check_user_exist($email) {
	$params[] = array($email->email, TYPE_STRING);
	try {
	    return parent::execute_sp_single_query("sp_check_member_exist", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}

	return true;
    }

}
?>

