<?php

require_once MODEL_BASE;

class M_prepaid_voucher extends ModelBase {

    public function get_info_provider($data) {
        $params[] = array($data->provider_service_nominal_seq, TYPE_NUMERIC);
        $params[] = array($data->pg_method_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_multi_query("sp_prepaid_voucher_get_info", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add_order_voucher($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->order_no, TYPE_STRING);
        $params[] = array($data->phone_number, TYPE_STRING);
        $params[] = array($data->provider_service_nominal_seq, TYPE_NUMERIC);
        $params[] = array($data->pg_method_seq, TYPE_NUMERIC);
        $params[] = array($data->payment_status, TYPE_STRING);
        $params[] = array($data->order_status, TYPE_STRING);
        $params[] = array($data->type, TYPE_STRING);
        try {
            return parent::execute_non_query("sp_order_voucher_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>
