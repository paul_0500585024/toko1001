<?php

require_once MODEL_BASE;

class M_payment_member extends ModelBase {

    public function save_update_status_order($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->order_seq, TYPE_NUMERIC);
        $params[] = array($data->order_no, TYPE_STRING);
        $params[] = array($data->payment_status, TYPE_STRING);
        $params[] = array($data->order_status, TYPE_STRING);
        $params[] = array($data->paid_date, TYPE_STRING);

        try {
            parent::execute_non_query("sp_order_payment_update_status", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_status_order_voucher($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->order_seq, TYPE_NUMERIC);
        $params[] = array($data->payment_status, TYPE_STRING);

        try {
            parent::execute_non_query("sp_order_voucher_payment_update_status", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_payment_log($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->order_seq, TYPE_NUMERIC);
        $params[] = array($data->order_no, TYPE_STRING);
        $params[] = array($data->payment_method_seq, TYPE_NUMERIC);
        $params[] = array($data->request, TYPE_STRING);
        $params[] = array($data->request_date, TYPE_DATETIME);
        $params[] = array($data->response, TYPE_STRING);
        $params[] = array($data->response_date, TYPE_DATETIME);
        $params[] = array($data->signature, TYPE_STRING);

        try {
            parent::execute_non_query("sp_order_payment_log_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_voucher_log($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->order_seq, TYPE_NUMERIC);
        $params[] = array($data->order_no, TYPE_STRING);
        $params[] = array($data->payment_method_seq, TYPE_NUMERIC);
        $params[] = array($data->request, TYPE_STRING);
        $params[] = array($data->request_date, TYPE_DATETIME);
        $params[] = array($data->response, TYPE_STRING);
        $params[] = array($data->response_date, TYPE_DATETIME);
        $params[] = array($data->signature, TYPE_STRING);

        try {
            parent::execute_non_query("sp_order_voucher_log_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_payment_update_log($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->order_seq, TYPE_NUMERIC);
        $params[] = array($data->order_no, TYPE_STRING);
        $params[] = array($data->payment_method_seq, TYPE_NUMERIC);
        $params[] = array($data->response, TYPE_STRING);
        $params[] = array($data->response_date, TYPE_DATETIME);
        $params[] = array($data->signature, TYPE_STRING);

        try {
            parent::execute_non_query("sp_order_payment_log_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_payment_bank($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->order_no, TYPE_STRING);
        $params[] = array($data->payment_type, TYPE_STRING);
        $params[] = array($data->payment_date, TYPE_DATE);
        $params[] = array($data->payment_amt, TYPE_NUMERIC);
        $params[] = array($data->confirm_pay_note_file, TYPE_STRING);
        $params[] = array($data->confirm_pay_bank_seq, TYPE_NUMERIC);
        $params[] = array($data->status, TYPE_STRING);

        try {
            parent::execute_non_query("sp_order_payment_update_bank", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_order_no_from_signature($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->signature, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_get_order_no_by_signature", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_prepaid_voucher_status($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->order_no, TYPE_STRING);
        $params[] = array($data->order_status, TYPE_STRING);
        try {
            parent::execute_non_query("sp_order_voucher_update_status", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_order_payment_by_order_no_signature($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->order_no, TYPE_STRING);
        $params[] = array($data->signature, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_order_payment_by_order_no_signature", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_update_pg_method($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->key, TYPE_STRING);
        $params[] = array($data->pg_method_seq, TYPE_NUMERIC);
        try {
            parent::execute_non_query("sp_order_update_payment_method", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function update_status_order_loan($selected) {
        $params[] = array($selected->user_id, TYPE_STRING);
        $params[] = array($selected->ip_address, TYPE_STRING);
        $params[] = array($selected->order_seq, TYPE_NUMERIC);
        $params[] = array($selected->status_order, TYPE_STRING);
        $params[] = array($selected->pg_method_seq, TYPE_STRING);

        try {
            parent::execute_non_query("sp_agent_update_status_pg_order_loan", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }
}

?>
