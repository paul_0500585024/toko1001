<?php

require_once MODEL_BASE;

class M_checkout_member extends ModelBase {

    public function get_expedition_info($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->merchant_seq, TYPE_NUMERIC);
        $params[] = array($data->district_seq, TYPE_NUMERIC);
        $params[] = array($data->exp_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_get_expedition_info", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_expedition_merchant_info($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->merchant_seq, TYPE_NUMERIC);
        $params[] = array($data->district_seq, TYPE_NUMERIC);
        $params[] = array($data->exp_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_get_expedition_merchant_info", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_promo_free_fee($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->merchant_seq, TYPE_NUMERIC);
        $params[] = array($data->city_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_check_free_fee_expedition", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add_order($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->member_seq, TYPE_NUMERIC);
        $params[] = array($data->order_no, TYPE_STRING);
        $params[] = array($data->receiver_name, TYPE_STRING);
        $params[] = array($data->receiver_address, TYPE_STRING);
        $params[] = array($data->receiver_district_seq, TYPE_NUMERIC);
        $params[] = array($data->receiver_zip_code, TYPE_STRING);
        $params[] = array($data->receiver_phone_no, TYPE_STRING);
        $params[] = array($data->payment_status, TYPE_STRING);
        $params[] = array($data->payment_method, TYPE_NUMERIC);
        $params[] = array($data->voucher_seq, TYPE_NUMERIC);
        $params[] = array($data->coupon_seq, TYPE_NUMERIC);
        $params[] = array($data->credit_seq, TYPE_NUMERIC);
        $params[] = array($data->agent_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_order_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add_order_merchant($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->order_seq, TYPE_NUMERIC);
        $params[] = array($data->merchant_seq, TYPE_NUMERIC);
        $params[] = array($data->exp_real_service_seq, TYPE_NUMERIC);
        $params[] = array($data->exp_service_seq, TYPE_NUMERIC);
        $params[] = array($data->free_fee_seq, TYPE_NUMERIC);
        $params[] = array($data->order_status, TYPE_STRING);
        $params[] = array($data->member_notes, TYPE_STRING);
        $params[] = array($data->ref_awb_no, TYPE_STRING);

        try {
            parent::execute_non_query("sp_order_merchant_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add_order_product($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        
        $params[] = array($data->commission_partner_percent, TYPE_NUMERIC);
        $params[] = array($data->commission_partner_nominal, TYPE_NUMERIC);
        $params[] = array($data->commission_agent_percent, TYPE_NUMERIC);
        $params[] = array($data->commission_agent_nominal, TYPE_NUMERIC);
        
        
        $params[] = array($data->merchant_seq, TYPE_NUMERIC);
        $params[] = array($data->order_seq, TYPE_NUMERIC);
        $params[] = array($data->product_variant_seq, TYPE_NUMERIC);
        $params[] = array($data->variant_value_seq, TYPE_NUMERIC);
        $params[] = array($data->qty, TYPE_NUMERIC);
        $params[] = array($data->sell_price, TYPE_NUMERIC);
        $params[] = array($data->weight_kg, TYPE_NUMERIC);
        $params[] = array($data->ship_price_real, TYPE_NUMERIC);
        $params[] = array($data->ship_price_charged, TYPE_NUMERIC);
        $params[] = array($data->product_status, TYPE_STRING);
        $params[] = array($data->agent_seq, TYPE_NUMERIC);
        try {
            parent::execute_non_query("sp_order_product_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_promo_voucher_by_member($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->member_seq, TYPE_NUMERIC);
        $params[] = array($data->total_order, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_get_promo_voucher_by_member", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_code_payment_gateway($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->pg_seq, TYPE_NUMERIC);
        try {
            return parent::execute_sp_single_query("sp_get_code_payment_gateway", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_coupon($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->product_variant_seq, TYPE_STRING);
        $params[] = array($data->coupon_code, TYPE_STRING);
        $params[] = array($data->voucher_node, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_get_coupon_info", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function update_data_header_order($order_seq) {

        $params[] = array($order_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_order_total_update", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_valid_voucher($data) {
        if ($data->agent == AGENT) {
            $params[] = array($data->agent_seq, TYPE_NUMERIC);
        } else {
            $params[] = array($data->member_seq, TYPE_NUMERIC);
        }
        $params[] = array($data->voucher_seq, TYPE_NUMERIC);
        $params[] = array($data->voucher_node, TYPE_STRING);
        $params[] = array($data->agent, TYPE_STRING);
        try {
            return parent::execute_sp_single_query("sp_get_valid_voucher", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_valid_coupon($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->coupon_seq, TYPE_NUMERIC);
        $params[] = array($data->order_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_multi_query("sp_get_valid_coupon", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_coupon_period($data) {
        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->product_variant_seq, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_get_coupon_period", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_personal_info($data, $tipe = "") {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->member_seq, TYPE_STRING);
        $params[] = array($data->receiver_name, TYPE_STRING);
        $params[] = array($data->address, TYPE_STRING);
        $params[] = array($data->district_seq, TYPE_NUMERIC);
        $params[] = array($data->phone_no, TYPE_STRING);
        $params[] = array($data->postal_code, TYPE_STRING);
        $params[] = array($data->alias, TYPE_STRING);

        try {
            if ($tipe == AGENT) {
                parent::execute_non_query("sp_agent_address_add", $params);
            } else {
                parent::execute_non_query("sp_member_address_add", $params);
            }
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_valid_credit($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->credit_seq, TYPE_NUMERIC);
        $params[] = array($data->credit_product_variant_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_single_query("sp_get_valid_credit", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_new_customer($data) {

        $params[] = array($data->user_id, TYPE_NUMERIC);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->user_id, TYPE_NUMERIC);
        $params[] = array($data->identity_no, TYPE_STRING);
        $params[] = array($data->identity_address, TYPE_STRING);
        $params[] = array($data->birthday, TYPE_DATE);
        $params[] = array($data->address, TYPE_STRING);
        $params[] = array($data->email_address, TYPE_STRING);
        $params[] = array($data->district_seq, TYPE_NUMERIC);
        $params[] = array($data->sub_district, TYPE_STRING);
        $params[] = array($data->zip_code, TYPE_STRING);
        $params[] = array($data->pic_name, TYPE_STRING);
        $params[] = array($data->phone_no, TYPE_STRING);        
        try {
            return parent::execute_sp_single_query("sp_agent_customer_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_info_loan($product_variant_seq, $total = 0, $tenor = 0, $agent_id = 0) {
        $params[] = array($product_variant_seq, TYPE_NUMERIC);
        $params[] = array($total, TYPE_NUMERIC);
        $params[] = array($tenor, TYPE_NUMERIC);
        $params[] = array($agent_id, TYPE_NUMERIC);
        try {
                return parent::execute_sp_multi_query("sp_get_info_loan", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_add_order_loan($data) {

        $params[] = array($data->user_id, TYPE_NUMERIC);
        $params[] = array($data->ip_address, TYPE_STRING);
        $params[] = array($data->order_loan_seq, TYPE_NUMERIC);
        $params[] = array($data->customer_seq, TYPE_NUMERIC);
        $params[] = array($data->dp, TYPE_NUMERIC);
        $params[] = array($data->admin_fee, TYPE_NUMERIC);
        $params[] = array($data->tenor, TYPE_NUMERIC);
        $params[] = array($data->loan_interest, TYPE_NUMERIC);
        $params[] = array($data->installment, TYPE_NUMERIC);
        $params[] = array($data->total_installment, TYPE_NUMERIC);
        try {
            parent::execute_non_query("sp_order_loan_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>