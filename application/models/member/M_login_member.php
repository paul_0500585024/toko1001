<?php

require_once MODEL_BASE;

class M_login_member extends ModelBase {

    public function get_password($data) {
        $info[] = array($data->user_name, TYPE_STRING);

        try {
            return parent::execute_sp_multi_query("sp_get_password_member", $info);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_list($data) {
        $login[] = array($data->user_id, TYPE_STRING);
        $login[] = array($data->password_2, TYPE_STRING);
        $login[] = array($data->ip_address, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_login_member", $login);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_info_member($data) {
        $login[] = array($data->user_id, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_info_member", $login);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_email_fb($data) {
        $info[] = array($data->email, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_member_by_email", $info);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function save_facebook_member($data) {

        $params[] = array($data->user_id, TYPE_STRING);
        $params[] = array($data->name, TYPE_STRING);
        $params[] = array($data->birthday, TYPE_DATE);
        $params[] = array($data->gender, TYPE_STRING);
        $params[] = array($data->password, TYPE_STRING);
        $params[] = array($data->ip_address, TYPE_STRING);

        try {
            return parent::execute_sp_single_query("sp_facebook_login_save_add", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

    public function get_voucher($data) {
        $params[] = array($data->member_seq, TYPE_NUMERIC);
        $params[] = array($data->voucher_promo_seq, TYPE_NUMERIC);

        try {
            return parent::execute_sp_multi_query("sp_member_get_voucher", $params);
        } catch (Exception $ex) {
            parent::handle_database_error($ex);
        }
    }

}

?>