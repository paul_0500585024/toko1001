<?php

require_once MODEL_BASE;

class M_trans_log_member extends ModelBase {

    public function save_member_change_password_log($data) {

	$params[] = array($data->user_id, TYPE_STRING);
	$params[] = array($data->ip_address, TYPE_STRING);
	$params[] = array($data->member_seq, TYPE_NUMERIC);
	$params[] = array($data->encrypt_old_password, TYPE_STRING);

	try {
	    parent::execute_non_query("sp_member_change_password_log", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

}

?>