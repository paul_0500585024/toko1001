<?php

require_once MODEL_BASE;

class M_wishlist_member extends ModelBase {

    public function get_wishlist($selected) {

	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->member_seq, TYPE_NUMERIC);
	$params[] = array($selected->product_variant_seq, TYPE_NUMERIC);
	try {
	    return parent::execute_sp_single_query("sp_member_wishlist_by_seq", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

    public function update_wishlist($selected) {

	$params[] = array($selected->user_id, TYPE_STRING);
	$params[] = array($selected->ip_address, TYPE_STRING);
	$params[] = array($selected->member_seq, TYPE_NUMERIC);
	$params[] = array($selected->product_variant_seq, TYPE_NUMERIC);
	$params[] = array($selected->pstatus, TYPE_STRING);
	try {
	    parent::execute_non_query("sp_member_wishlist_update", $params);
	} catch (Exception $ex) {
	    parent::handle_database_error($ex);
	}
    }

}

?>