<?php
require_once VIEW_BASE_MERCHANT;

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">

<html>
    <head>
        <title>Toko1001</title>
        <link rel="icon" href="<?php echo get_img_url(); ?>home/icon/pavicon.png"/>
        <link href="<?php echo get_css_url(); ?>bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_css_url(); ?>merchant/toko1001_merchant.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_css_url(); ?>font-awesome.min.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="<?php echo get_js_url(); ?>jQuery-2.1.4.min.js" ></script>
        <script type="text/javascript" src="<?php echo get_js_url(); ?>jquery.validate.js" ></script>
    </head>
    <body class="login-page">
        <div class="login-box">
            <div class="login-box-body" style="border-radius: 16px">
                <div class="form-group text-center">
                    <a href="<?php echo base_url() ?>"><img src='<?php echo get_img_url() ?>logo_184x41.png'></img></a>
                </div>
                <?php if (isset($data_err) && $data_err[ERROR] === true) { ?>
                    <p class="login-box-msg text-red"><?php echo $data_err[ERROR_MESSAGE][0] ?> </p>
                <?php } elseif (isset($data_suc) && $data_suc[SUCCESS] == true) { ?>
                    <p class="login-box-msg"><?php echo $data_suc[SUCCESS_MESSAGE][0]; ?></p>
                <?php } ?>
                <p class="text-right" style="margin: 0px 0px 0px; color: #203F49">Merchant Panel</p>
                <form id="frmlogin" method = "post" action= "<?php echo get_base_url() ?>merchant/login/sign_in<?php echo isset($data_auth[FORM_AUTH][FORM_URL]) ? $data_auth[FORM_AUTH][FORM_URL] : ""; ?>">
                    <div class="form-group has-feedback">
                        <input id="txtUserName" name="email" type="text" placeholder="Email Address" 
                               class="form-control" style="background-color: #999999; color: #FFF"
                               value ="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA]->user_id) : "") ?>"/>
                        <span class="glyphicon glyphicon-envelope form-control-feedback" style="color: #3C3C3C"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input id="txtPassword" name="password" type="password"
                               class="form-control" placeholder="Password" style="background-color: #999999; color: #FFF"
                               value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA]->password : "") ?>"/>
                        <span class="glyphicon glyphicon-lock form-control-feedback" style="color: #3C3C3C"></span>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <button name="submit" type="submit" class="btn btn-block btn-google-plus" 
                                    style=" background-color: #203F49">
                                <i class="fa fa-sign-in"></i>&nbsp; Masuk
                            </button>
                        </div>
                        <div class="col-lg-6 text-right">
                            <a style="color: #203F49" href="<?php echo get_base_url() ?>merchant/forgot_password">Lupa Password ?</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <script>
            $('#frmlogin').validate({
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true
//                        minlength: 8
                    }
                }
            });
            $.validator.messages.required = "Data wajib diisi !";
            $.validator.messages.maxlength = "Harap diisi tidak lebih dari {0} karakter !";
            $.validator.messages.minlength = "Harap diisi dengan minimal {0} karakter !";
            $.validator.messages.email = "Harap diisi dengan alamat email dengan benar !";
            $.validator.messages.date = "Harap diisi dengan tanggal !";
            $.validator.messages.dateiso = "Harap diisi dengan format tanggal yang benar !";
            $.validator.messages.number = "Harap diisi dengan angka !";
        </script>
    </body>
</html>
