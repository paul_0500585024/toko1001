<?php
require_once VIEW_BASE_MERCHANT;
require_once get_include_content_merchant_top_page_navigation();
die('lama');
?>

<div class="row">
    <div class="col-lg-3 col-xs-6">
        <div class="small-box" style="background-color: #f4701e; color: #fff">
            <div class="inner">
                <h3><?php echo $pending_new_product ?></h3>
                <p>Pengajuan Produk</p>
            </div>
            <div class="icon">
                <i class="ion ion-ios-upload-outline"></i>
            </div>
            <a href="<?php echo get_base_url() ?>merchant/product_new" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="small-box" style="background-color: #ca248e; color: #fff">
            <div class="inner">
                <h3><?php echo $pending_product_change ?></h3>
                <p>Perubahan Produk</p>
            </div>
            <div class="icon">
                <i class="ion ion-ios-briefcase-outline"></i>
            </div>
            <a href="<?php echo get_base_url() ?>merchant/master/product_merchant" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="small-box" style="background-color: #5d3380; color: #fff">
            <div class="inner">
                <h3><?php echo $out_of_stock ?></h3>
                <p>Stok Hampir / Sudah Habis</p>
            </div>
            <div class="icon">
                <i class="ion ion-ios-filing-outline"></i>
            </div>
            <a href="<?php echo get_base_url() ?>merchant/master/product_stock_merchant" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="small-box" style="background-color: #CC0000; color: #fff">
            <div class="inner">
                <h3><?php echo $unprinted ?></h3>
                <p>Order Belum Dicetak</p>
            </div>
            <div class="icon">
                <i class="ion ion-ios-paper-outline"></i>
            </div>
            <a href="<?php echo get_base_url() ?>merchant/transaction/merchant_delivery" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="chart">
                    <div id="charts"></div>
                    <table id="datatable" style='display: none'>
                        <thead>
                            <tr>
                                <th></th>
                                <th>Order Rata - Rata Per Bulan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>January</th>
                                <td><?php echo $order_january ?></td>
                            </tr>
                            <tr>
                                <th>February</th>
                                <td><?php echo $order_february ?></td>
                            </tr>
                            <tr>
                                <th>March</th>
                                <td><?php echo $order_march ?></td>
                            </tr>
                            <tr>
                                <th>April</th>
                                <td><?php echo $order_april ?></td>
                            </tr>
                            <tr>
                                <th>May</th>
                                <td><?php echo $order_may ?></td>
                            </tr>
                            <tr>
                                <th>June</th>
                                <td><?php echo $order_june ?></td>
                            </tr>
                            <tr>
                                <th>July</th>
                                <td><?php echo $order_july ?></td>
                            </tr>
                            <tr>
                                <th>August</th>
                                <td><?php echo $order_august ?></td>
                            </tr>
                            <tr>
                                <th>September</th>
                                <td><?php echo $order_september ?></td>
                            </tr>
                            <tr>
                                <th>October</th>
                                <td><?php echo $order_october ?></td>
                            </tr>
                            <tr>
                                <th>November</th>
                                <td><?php echo $order_november ?></td>
                            </tr>
                            <tr>
                                <th>December</th>
                                <td><?php echo $order_december ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('#charts').highcharts({
            data: {
                table: 'datatable'
            },
            chart: {
                type: 'line'
            },
            title: {
                text: 'Perkembangan Order Merchant Toko1001'
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Total Order'
                }}
            //            tooltip: {
            //                formatter: function() {
            //                    return '<b>' + this.series.name + '</b><br/>' +
            //                            this.point.y + ' ' + this.point.name.toLowerCase();
            //                }
//            }
        });
    });

</script>

<?php
require_once get_include_content_merchant_bottom_page_navigation();
?>