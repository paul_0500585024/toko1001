<?php
require_once VIEW_BASE_MERCHANT;
?>

<header class="main-header">
    <a href="<?php echo base_url() ?>merchant/main_page" class="logo" style="padding: 0px; background-color:#074051; ">
        <span class="logo-mini"><img src="<?php echo get_img_url(); ?>50X50.jpg"></span>
        <span class="logo-lg"><img src="<?php echo get_img_url(); ?>230X50.jpg"></span>
    </a>
    <nav class="navbar navbar-static-top" style="background-color: #042E3A" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <?php if (isset($data_prof[USER_IMAGE])) { ?>
                        <a class="dropdown-toggle" href="#" data-toggle="dropdown" style="width: 270px; text-align: center; " id="user-options">
                            <span>Halo, &nbsp; <?php echo $data_prof[USER_NAME] ?> &nbsp;&nbsp;<i class='fa fa-caret-down '></i> </span>                            
                        </a>
                    <?php } else { ?>

                    <?php } ?>
                    <ul class="dropdown-menu" >
                        <li class="user-header" style="height: auto">
                            <img src="<?php echo $data_prof[USER_IMAGE]; ?>" class="" alt="User Image" />
                            <p>
                                <?php echo $data_prof[USER_EMAIL] ?>
                            </p>
                        </li>
                        <li class="user-body">
                            <div class="form-group">
                                <i class="fa fa-pencil"></i>
                                <a href="<?php echo get_base_url(); ?>merchant/profile/change_password_merchant">Ubah Password</a>
                            </div>
                            <div class="form-group">
                                <i class="fa fa-sign-out"></i>
                                <a href="<?php echo get_base_url() ?>logout/merchant">Keluar</a>
                            </div>
                            <div class="form-group">
                                <i class="fa fa-clock-o"></i>
                                <a>Login Terakhir : <?php echo $data_prof[LAST_LOGIN] ?></a>
                            </div>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" data-toggle="control-sidebar" class="control-search"><i class="fa fa-search"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>