<?php
require_once VIEW_BASE_MERCHANT;
?>

<link href="<?php echo get_css_url(); ?>bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_css_url(); ?>font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_css_url(); ?>ionicons.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_css_url(); ?>dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_css_url(); ?>jquery.dataTables.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_css_url(); ?>select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_css_url(); ?>jquery.treeview.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_css_url(); ?>merchant/toko1001_merchant.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_css_url(); ?>skin-blue.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_css_url(); ?>responsive.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_css_url(); ?>buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Ubuntu" />
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">
<script type="text/javascript" src="<?php echo get_js_url(); ?>jQuery-2.1.4.min.js" ></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>app.min.js" ></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>fastclick.min.js" ></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>jquery.dataTables.min.js" ></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>dataTables.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>highcharts/highcharts.js"></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>highcharts/data.js"></script>
<!--<script type="text/javascript" src="<?php echo get_js_url(); ?>highcharts/exporting.js"></script>-->
<script type="text/javascript" src="<?php echo get_js_url(); ?>jquery.validate.js" ></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>select2.full.min.js" ></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>jquery.treeview.js" ></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>autoNumeric-min.js" ></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>dataTables.responsive.min.js" ></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>morris/morris.min.js"></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>raphael-min.js"></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>dataTables.buttons.min.js" ></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>jszip.min.js" ></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>pdfmake.min.js" ></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>vfs_fonts.js" ></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>buttons.html5.min.js" ></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>error_response.js"></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>input-mask/jquery.inputmask.js"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.sidebar-toggle').click(function()
        {
            $('.control-sidebar-dark').removeClass("control-sidebar-open");
        });
        $('.control-search').click(function()
        {
            $('.skin-blue').removeClass("sidebar-open");
        });
        $('#user-options').click(function()
        {
            $('.skin-blue').removeClass("sidebar-open");
            $('.control-sidebar-dark').removeClass("control-sidebar-open");
        });
    })
</script>