<?php
require_once VIEW_BASE_MERCHANT;
?>

<aside class="main-sidebar" style="background-color: #236478">
    <section class="sidebar">
        <!--User side panel-->
        <!--        <div class="user-panel" style="background-color: #0992A5">
                    <div class="pull-left image">
                        <img src="<?php // echo $data_prof[USER_IMAGE];   ?>" class="img-circle" style="" alt="User Image" />
                    </div>
                    <div class="pull-left info">
                        <p><?php // echo $data_prof[USER_NAME]   ?></p>
                    </div>
                </div>-->
        <!--Menu side bar-->
        <ul class="sidebar-menu" style="background-color: #236478">
            <?php echo $data_left; ?>
        </ul>
    </section>
</aside>