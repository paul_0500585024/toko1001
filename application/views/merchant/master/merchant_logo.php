<?php
require_once VIEW_BASE_MERCHANT;

require_once get_include_content_merchant_top_page_navigation();
?>

<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
    <script type="text/javascript" src="<?php echo get_js_url() ?>ckeditor/ckeditor.js"></script>

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo ($data_auth[FORM_ACTION] == ACTION_ADD ? get_title_add($data_auth[FORM_AUTH][FORM_TITLE]) : get_title_edit($data_auth[FORM_AUTH][FORM_TITLE])); ?></h3>
        </div>
        <form class ="form-horizontal" id="frmMain" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>" enctype ="multipart/form-data">
            <div class="box-body">
                <section class="col-md-12">
                    <?php echo get_csrf_merchant_token(); ?>
                    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>                        
                        <input type="hidden"  name="old_logo" value="<?php echo (isset($data_sel[LIST_DATA][0]) || $data_sel[LIST_DATA][0]->old_logo_img == "" ? $data_sel[LIST_DATA][0]->logo_img : $data_sel[LIST_DATA][0]->old_logo_img); ?>">
                        <input type="hidden"  name="old_banner" value="<?php echo (isset($data_sel[LIST_DATA][0]) || $data_sel[LIST_DATA][0]->old_banner_img == "" ? $data_sel[LIST_DATA][0]->banner_img : $data_sel[LIST_DATA][0]->old_banner_img); ?>">
                    <?php } ?>
                    <div class="panel panel-default" style="height:320px">
                        <div class="panel-heading">Banner Merchant (h:1000 - 1200px, w:200 - 300px)</div>
                        <div class="panel-body">
                            <div class ="col-md-12">    
                                <div class ="form-group">  
                                    <input type="file" accept="<?php echo IMAGE_TYPE_UPLOAD; ?>" name="banner_img" class="btn btn-default" onchange ="previewimg(this, 'iprev1')" value="<?php echo isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->banner_img : ""; ?>" <?php echo ($data_auth[FORM_ACTION] == ACTION_ADD ? "validate=\"required[]\"" : ""); ?>/>
                                    <img id="iprev1" src="<?php echo (!isset($data_sel[LIST_DATA])) || $data_sel[LIST_DATA][0]->banner_img == '' ? get_base_url() . IMG_BLANK_100 : get_base_url() . MERCHANT_LOGO . $data_sel[LIST_DATA][0]->seq . "/" . $data_sel[LIST_DATA][0]->banner_img; ?>" height="200" style="max-width:800px"/>                                     
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default" style="height:320px">
                        <div class="panel-heading">Logo Merchant (h:150 - 200px, w:150 - 200px)</div>
                        <div class="panel-body">
                            <div class ="col-md-12">    
                                <div class ="form-group">  
                                    <input type="file" accept="<?php echo IMAGE_TYPE_UPLOAD; ?>" name="logo_img" class="btn btn-default" onchange ="previewimg(this, 'iprev2')" value="<?php echo isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->logo_img : ""; ?>" <?php echo ($data_auth[FORM_ACTION] == ACTION_ADD ? "validate=\"required[]\"" : ""); ?>/>
                                    <img id="iprev2" src="<?php echo (!isset($data_sel[LIST_DATA])) || $data_sel[LIST_DATA][0]->logo_img == '' ? get_base_url() . IMG_BLANK_100 : get_base_url() . MERCHANT_LOGO  . $data_sel[LIST_DATA][0]->seq . "/" . $data_sel[LIST_DATA][0]->logo_img; ?>" height="200" style="max-width:400px"/>                                     
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Perkenalan Toko</label>
                            <textarea class="ckeditor" rows="5" id="editor1" name="description"><?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->description : "" ); ?></textarea>
                            <?php //echo htmlspecialchars(isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->description : "" ); ?>
                        </div>
                    </div>
                    <div class ="form-group">
                        <?php
                        if ($data_auth[FORM_ACTION] == ACTION_VIEW) {
                            ?>
                            <div class ="col-md-6"><?php echo get_back_button(); ?> </div>
                            <?php
                        } else {
                            if ($data_auth[FORM_ACTION] == ACTION_ADD) {
                                ?>
                                <div class ="col-md-6"><?php echo get_save_add_button(); ?> </div>
                            <?php } if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                                <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                            <?php } ?>
                            <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                        <?php } ?>
                    </div> 
                </section>
            </div>
        </form>
    </div>

    <script type="text/javascript">
            function previewimg(thisval, idprev) {
                if (thisval.files && thisval.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#' + idprev + '').attr('src', e.target.result).height(200);
                    };
                    reader.readAsDataURL(thisval.files[0]);
                }
            }
    </script>
    <?php
}
require_once get_include_page_list_merchant_content_footer();
require_once get_include_content_merchant_bottom_page_navigation();
?>