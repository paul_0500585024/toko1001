<?php
require_once VIEW_BASE_MERCHANT;
?>

<form id ="frmSearch"  url= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
    <div class="form-group">
        <label>Nama</label>
        <input class="form-control" name="name" type="input">
    </div>
    <div class="form-group">
        <label>Deskripsi</label>
        <input class="form-control" name="description" type="input">
    </div>
    <div class="form-group">
        <label>Status</label>
        <select class="form-control select2" name="status">
            <option value="">Semua</option>
	    <?php
	    echo combostatus(json_decode(STATUS_FNR));
	    ?>
        </select>
    </div>
    <div class="form-group">
	<?php echo get_search_button(); ?>
    </div>
</form>