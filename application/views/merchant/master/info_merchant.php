<?php
require_once VIEW_BASE_MERCHANT;

require_once get_include_content_merchant_top_page_navigation();
?>

<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
    <?php require_once get_component_url() . '/dropdown/function_drop_down.php'; ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo ($data_auth[FORM_ACTION] == ACTION_ADD ? get_title_add($data_auth[FORM_AUTH][FORM_TITLE]) : get_title_edit($data_auth[FORM_AUTH][FORM_TITLE])); ?></h3>
        </div>
        <form class ="form-horizontal" id="frmMain" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
            <div class="box-body">
                <section class="col-md-12">
                    <?php echo get_csrf_merchant_token(); ?>
                    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                        <input class="form-control" name="old_status" type="hidden" value="<?php echo(isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->status : "") ?>">
                        <input class="form-control" name="old_created_date" type="hidden" value="<?php echo(isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->created_date : "") ?>">
                        <input class="form-control" name="old_modified_date" type="hidden" value="<?php echo(isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->modified_date : "") ?>">
                    <?php } ?>
                    <div>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#t_5" aria-controls="t_5" role="tab" data-toggle="tab">Info Pengajuan</a></li>
                            <li role="presentation"><a href="#t_1" aria-controls="t_1" role="tab" data-toggle="tab">Info Alamat</a></li>
                            <li role="presentation"><a href="#t_2" aria-controls="t_2" role="tab" data-toggle="tab">Info Ekspedisi</a></li>
                            <li role="presentation"><a href="#t_3" aria-controls="t_3" role="tab" data-toggle="tab">Info Retur</a></li>
                            <li role="presentation"><a href="#t_4" aria-controls="t_4" role="tab" data-toggle="tab">Info Bank</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane  active" id="t_5"> <br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class ="form-group">
                                            <label class ="control-label col-md-3">Status</label>
                                            <div class ="col-md-9">
                                                <input class="form-control" name="s_status" type="text" value="<?php echo status($data_sel[LIST_DATA][0]->status, (STATUS_ANRL)); ?>" readonly >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class ="form-group">
                                            <label class ="control-label col-md-3">Tanggal Pengajuan</label>
                                            <div class ="col-md-9">
                                                <input class="form-control" name="s_tgl" type="text" value="<?php echo c_date($data_sel[LIST_DATA][0]->created_date, 1) ?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class ="form-group">
                                            <label class ="control-label col-md-3">Tanggal Verifikasi</label>
                                            <div class ="col-md-9">
                                                <input class="form-control" name="s_tgl_k" value="<?php echo c_date($data_sel[LIST_DATA][0]->modified_date, 1) ?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="t_1"><br />
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class ="form-group">
                                            <label class ="control-label col-md-3">Alamat *</label>
                                            <div class ="col-md-9">
                                                <textarea onblur="cekpr('1')" class="form-control" validate ="required[]" id="address" name="address" style="overflow:auto;resize:none" rows="7"><?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->address) : "") ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class ="control-label col-md-3">Propinsi *</label>
                                            <div class ="col-md-9">
                                                <?php echo drop_down('propinsi', $province_name, '', 'validate ="required[]" onchange="cData(this.value, \'city\', \'city\');"'); ?>
                                            </div>
                                        </div>
                                        <div class ="form-group">
                                            <label class ="control-label col-md-3">Kota *</label>
                                            <div class ="col-md-9">
                                                <?php echo drop_down('city', '', '', 'validate ="required[]" onchange="cData(this.value, \'district_seq\', \'district\');"'); ?>
                                            </div>
                                        </div>
                                        <div class ="form-group">
                                            <label class ="control-label col-md-3">Kecamatan *</label>
                                            <div class ="col-md-9">
                                                <?php echo drop_down('district_seq', '', '', 'validate ="required[]"  onchange="cekpr(\'2\')"'); ?>
                                            </div>
                                        </div>
                                        <div class ="form-group">
                                            <label class ="control-label col-md-3">Kode Pos</label>
                                            <div class ="col-md-9">
                                                <input onblur="cekpr('3')" class="form-control" maxlength="10" id="zip_code" name="zip_code" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->zip_code) : "") ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class ="form-group">
                                            <label class ="control-label col-md-3">No. Telp *</label>
                                            <div class ="col-md-9">
                                                <input class="form-control" data-inputmask="&quot;mask&quot;: &quot;9999 9999 9999&quot;" data-mask="" validate ="required[]" id="phone_no" name="phone_no" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->phone_no) : "") ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class ="form-group">
                                            <label class ="control-label col-md-3">No. Fax</label>
                                            <div class ="col-md-9">
                                                <input class="form-control" maxlength="50" id="fax_no" name="fax_no" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->fax_no) : "") ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class ="form-group">
                                            <label class ="control-label col-md-3">Nama PIC *</label>
                                            <div class ="col-md-9">
                                                <input class="form-control" maxlength="50" validate ="required[]" id="pic1_name" name="pic1_name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->pic1_name) : "") ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class ="form-group">
                                            <label class ="control-label col-md-3">Telp PIC</label>
                                            <div class ="col-md-9">
                                                <input class="form-control" maxlength="50" id="pic1_phone_no" name="pic1_phone_no" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->pic1_phone_no : "") ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class ="form-group">
                                            <label class ="control-label col-md-3">Nama Finance</label>
                                            <div class ="col-md-9">
                                                <input class="form-control" maxlength="50" id="pic2_name" name="pic2_name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->pic2_name) : "") ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">

                                        <div class ="form-group">
                                            <label class ="control-label col-md-3">Telp Finance</label>
                                            <div class ="col-md-9">
                                                <input class="form-control" maxlength="50" id="pic2_phone_no" name="pic2_phone_no" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->pic2_phone_no) : "") ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- end t1 -->
                            <div role="tabpanel" class="tab-pane" id="t_2"><br />
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class ="form-group">
                                            <label class ="control-label col-md-3">Alamat Pengambilan *</label>
                                            <div class ="col-md-9">
                                                <textarea class="form-control" style="overflow:auto;resize:none" rows="3" id="pickup_addr" name="pickup_addr"><?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->pickup_addr) : "") ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label> <input type="checkbox" id ="pickup_addr_eq" name ="pickup_addr_eq" <?php echo (isset($data_sel[LIST_DATA]) ? (($data_sel[LIST_DATA][0]->pickup_addr_eq == "1" OR $data_sel[LIST_DATA][0]->pickup_addr_eq == "on") ? "checked" : "") : "checked") ?> onclick="aSama(this.checked, 'pickup')" /> Sama dengan alamat</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class ="control-label col-md-3">Propinsi</label>
                                            <div class ="col-md-9">
                                                <?php echo drop_down('pickup_propinsi', $province_name, '', 'onchange="cData(this.value, \'city\', \'city\',\'pickup_\');"'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class ="form-group">
                                            <label class ="control-label col-md-3">Kota</label>
                                            <div class ="col-md-9">
                                                <?php echo drop_down('pickup_city', '', '', 'onchange="cData(this.value, \'district_seq\', \'district\',\'pickup_\');"'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class ="form-group">
                                            <label class ="control-label col-md-3">Kecamatan *</label>
                                            <div class ="col-md-9">
                                                <?php echo drop_down('pickup_district_seq'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class ="form-group">
                                            <label class ="control-label col-md-3">Kode Pos Pengambilan</label>
                                            <div class ="col-md-9">
                                                <input maxlength="10" class="form-control" id="pickup_zip_code" name="pickup_zip_code" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->pickup_zip_code) : "") ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- end t2 -->
                            <div role="tabpanel" class="tab-pane" id="t_3"><br />
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class ="form-group">
                                            <label class ="control-label col-md-3">Alamat Pengembalian *</label>
                                            <div class ="col-md-9">
                                                <textarea class="form-control" style="overflow:auto;resize:none" rows="3" id="return_addr" name="return_addr"><?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->return_addr) : "") ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label> <input type="checkbox" id ="return_addr_eq" name ="return_addr_eq" <?php echo (isset($data_sel[LIST_DATA]) ? (($data_sel[LIST_DATA][0]->return_addr_eq == "1" OR $data_sel[LIST_DATA][0]->return_addr_eq == "on") ? "checked" : "") : "checked") ?>  onclick="aSama(this.checked, 'return')"  /> Sama dengan alamat</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class ="control-label col-md-3">Propinsi</label>
                                            <div class ="col-md-9">
                                                <?php echo drop_down('return_propinsi', $province_name, '', 'onchange="cData(this.value, \'city\', \'city\',\'return_\');"'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class ="form-group">
                                            <label class ="control-label col-md-3">Kota</label>
                                            <div class ="col-md-9">
                                                <?php echo drop_down('return_city', '', '', 'onchange="cData(this.value, \'district_seq\', \'district\',\'return_\');"'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class ="form-group">
                                            <label class ="control-label col-md-3">Kecamatan *</label>
                                            <div class ="col-md-9">
                                                <?php echo drop_down('return_district_seq'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class ="form-group">
                                            <label class ="control-label col-md-3">Kode Pos Pengembalian</label>
                                            <div class ="col-md-9">
                                                <input class="form-control" maxlength="10" id="return_zip_code" name="return_zip_code" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->return_zip_code) : "") ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- end t3 -->
                            <div role="tabpanel" class="tab-pane" id="t_4"><br />
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class ="control-label col-md-3">Nama Bank *</label>
                                            <div class="col-md-9">
                                                <input class="form-control" validate ="required[]" name="bank_name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->bank_name : "") ?>"maxlength="25">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class ="col-md-3">Cabang *</label>
                                            <div class="col-md-9">
                                                <input class="form-control" validate ="required[]" name="bank_branch_name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->bank_branch_name : "") ?>" maxlength="50">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class ="col-md-3">No.Rekening *</label>
                                            <div class="col-md-9">
                                                <input class="form-control" validate ="required[]" name="bank_acct_no" type="text"  value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->bank_acct_no : "") ?>"  maxlength="50">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class ="col-md-3">Rekening A/N *</label>
                                            <div class="col-md-9">
                                                <input class="form-control" validate ="required[]" name="bank_acct_name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->bank_acct_name : "") ?>"  maxlength="50">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- end t4 -->
                        </div>
                    </div>
                    <div class ="form-group">
                        <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                        <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                    </div><!-- /.box-footer -->
                </section>
            </div>
        </form>
    </div>
    <script type="text/javascript">
            $("[data-mask]").inputmask();
            function cekpr($iddata) {
                var eqadr_p = '';
                var eqadr_r = '';
                if ($("#pickup_addr_eq").is(':checked'))
                    eqadr_p = '1';
                if ($("#return_addr_eq").is(':checked'))
                    eqadr_r = '1';

                if ($iddata == '1' && eqadr_p == '1')
                    $('#pickup_addr').val($('#address').val());
                if ($iddata == '1' && eqadr_r == '1')
                    $('#return_addr').val($('#address').val());

                if ($iddata == '2' && eqadr_p == '1') {
                    if ($('#district_seq').val() != '')
                        cekdata($('#district_seq').val(), 'pickup_');
                }
                if ($iddata == '2' && eqadr_r == '1') {
                    if ($('#district_seq').val() != '')
                        setTimeout(function() {
                            cekdata($('#district_seq').val(), 'return_');
                        }, 2000)
                }
                if ($iddata == '3' && eqadr_p == '1')
                    $('#pickup_zip_code').val($('#zip_code').val());
                if ($iddata == '3' && eqadr_r == '1')
                    $('#return_zip_code').val($('#zip_code').val());

            }

            function aSama(nilai, tab)
            {
                if (nilai == true || nilai == "1") {
                    $("#" + tab + "_addr").prop('disabled', true);
                    $("#" + tab + "_district_seq").prop('disabled', true);
                    $("#" + tab + "_zip_code").prop('disabled', true);
                    $("#" + tab + "_propinsi").prop('disabled', true);
                    $("#" + tab + "_city").prop('disabled', true);
                    $("#" + tab + "_district_seq").prop('required', false);
                    $("#" + tab + "_addr").prop('required', false);
                } else {
                    $("#" + tab + "_addr").prop('disabled', false);
                    $("#" + tab + "_district_seq").prop('disabled', false);
                    $("#" + tab + "_zip_code").prop('disabled', false);
                    $("#" + tab + "_propinsi").prop('disabled', false);
                    $("#" + tab + "_city").prop('disabled', false);
                    $("#" + tab + "_district_seq").prop('required', true);
                    $("#" + tab + "_addr").prop('required', true);
                }

            }
            var url = "<?php echo get_base_url() . $data_auth[FORM_URL]; ?>";
            function cData(idcombo, iddtl, jenis, grup, nilai) {
                grup = grup || "";
                nilai = nilai || "";
                var list = $("#" + grup + iddtl);
                if (jenis == 'city')
                    $("#" + grup + "district_seq").empty();
                inHTML = "";
                list.empty();
                if (idcombo != '') {
                    $.ajax({
                        url: url,
                        data: {
                            btnAdditional: "act_s_adt", idh: idcombo, tipe: jenis
                        },
                        type: "POST",
                        success: function(response) {
                            if (isSessionExpired(response)) {
                                response_object = json_decode(response);
                                url = response_object.url;
                                location.href = url;
                            } else {
                                if (nilai != '') {
                                    inHTML += '<option value="">-- Pilih --</option>';
                                } else {
                                    inHTML += '<option value="" selected>-- Pilih --</option>';
                                }
                                $.each($.parseJSON(response), function() {
                                    if (nilai != '') {
                                        selected = "";
                                        if (nilai == this.seq)
                                            selected = " selected";
                                        inHTML += '<option value="' + this.seq + '"' + selected + '>' + this.name + '</option>';
                                    } else {
                                        inHTML += '<option value="' + this.seq + '">' + this.name + '</option>';
                                    }
                                });
                                list.append(inHTML);
                                if (nilai == '')
                                    list.select2("val", "");
                            }
                        },
                        error: function(request, error) {
                            alert(error_response(request.status));
                        }
                    });
                }
            }
    <?php if ($data_auth[FORM_ACTION] == ACTION_ADD) { ?>
                function cEmail() {
                    var idemail = $("#email").val();
                    if (idemail == '') {
                        return false;
                    } else {
                        $("#emailerror").html("");
                    }
                    $.ajax({
                        url: url,
                        data: {
                            btnAdditional: "act_s_adt", idh: idemail, tipe: "email"
                        },
                        type: "POST",
                        success: function(data) {
                            if (isSessionExpired(data)) {
                                response_object = json_decode(data);
                                url = response_object.url;
                                location.href = url;
                            } else {
                                if (data.trim() != "OK") {
                                    $("#emailerror").html("Email sudah terdaftar, harap memakai email yang lain !");
                                    $("#email").focus();
                                } else {
                                    $("#emailerror").html("");
                                }
                            }
                        },
                        error: function(request, error) {
                            alert(error_response(request.status));
                        }
                    });
                }
                function cekdata(dtdistrict, grup) {
                    grup = grup || "";
                    var idcity = grup + "city";
                    var iddistrict = grup + "district_seq";
                    if (dtdistrict != '') {
                        $.ajax({
                            url: url,
                            data: {
                                btnAdditional: "act_s_adt", idh: dtdistrict, tipe: 'valpcd'
                            },
                            type: "POST",
                            success: function(response) {
                                if (isSessionExpired(response)) {
                                    response_object = json_decode(response);
                                    url = response_object.url;
                                    location.href = url;
                                } else {
                                    $.each($.parseJSON(response), function() {
                                        idp = this.idp;
                                        idc = this.idc;
                                        idd = this.idd;
                                    });
                                    $("#" + grup + "propinsi").select2("val", idp);
                                    setTimeout(function() {
                                        filldata(idp, idcity, 'city', idc);
                                    }, 500);
                                    setTimeout(function() {
                                        filldata(idc, iddistrict, 'district', idd);
                                    }, 800);
                                }
                            },
                            error: function(request, error) {
                                alert(error_response(request.status));
                            }
                        });
                    }
                }

                function filldata(idcombo, iddtl, jenis, nilai) {
                    nilai = nilai || "";
                    var list = $("#" + iddtl);
                    inHTML = "";
                    list.empty();
                    if (idcombo != '') {
                        $.ajax({
                            url: url,
                            data: {
                                btnAdditional: "act_s_adt", idh: idcombo, tipe: jenis
                            },
                            type: "POST",
                            success: function(response) {
                                if (isSessionExpired(response)) {
                                    response_object = json_decode(response);
                                    url = response_object.url;
                                    location.href = url;
                                } else {
                                    inHTML += "<option value=''>-- Pilih --</option>";
                                    $.each($.parseJSON(response), function() {
                                        if (nilai != '') {
                                            selected = "";
                                            if (nilai == this.seq)
                                                selected = " selected";
                                            inHTML += '<option value="' + this.seq + '"' + selected + '>' + this.name + '</option>';
                                        } else {
                                            inHTML += '<option value="' + this.seq + '">' + this.name + '</option>';
                                        }
                                    });
                                    list.append(inHTML);
                                    list.select2("val", nilai);
                                }
                            },
                            error: function(request, error) {
                                alert(error_response(request.status));
                            }
                        });
                    }
                }
        <?php if (isset($data_sel[LIST_DATA])) { ?>

                    setTimeout(function() {
                        aSama(<?php echo (($data_sel[LIST_DATA][0]->pickup_addr_eq == "1" || $data_sel[LIST_DATA][0]->pickup_addr_eq == "on") ? "'1'" : "''") ?>, 'pickup');
                    }, 500);

                    setTimeout(function() {
                        aSama(<?php echo (($data_sel[LIST_DATA][0]->return_addr_eq == "1" || $data_sel[LIST_DATA][0]->return_addr_eq == "on") ? "'1'" : "''") ?>, 'return');
                    }, 1000);

            <?php
            echo "cekdata('" . $data_sel[LIST_DATA][0]->district_seq . "');";

            //if ($data_sel[LIST_DATA][0]->pickup_addr_eq == "1" || $data_sel[LIST_DATA][0]->pickup_addr_eq == "on") {
            echo "setTimeout(function(){cekdata('" . $data_sel[LIST_DATA][0]->pickup_district_seq . "','pickup_');}, 2000);";
            //}
            //if ($data_sel[LIST_DATA][0]->return_addr_eq == "1" || $data_sel[LIST_DATA][0]->return_addr_eq == "on") {
            echo "setTimeout(function(){cekdata('" . $data_sel[LIST_DATA][0]->return_district_seq . "','return_');}, 3000);";
            //}
            echo "cekpr('1');cekpr('3');";
        } else {
            ?>

                    $("#propinsi").val("");
                    cData("", "city", "city");
                    setTimeout(function() {
                        aSama('1', 'pickup');
                    }, 500);

                    setTimeout(function() {
                        aSama('1', 'return');
                    }, 1000);

            <?php
        }
    } else {
        ?>
                $("#email").prop('required', false);
                $("#email").prop('disabled', true);
                setTimeout(function() {
                    aSama(<?php echo (($data_sel[LIST_DATA][0]->pickup_addr_eq == "1" || $data_sel[LIST_DATA][0]->pickup_addr_eq == "on") ? "'1'" : "''") ?>, 'pickup');
                }, 500);

                setTimeout(function() {
                    aSama(<?php echo (($data_sel[LIST_DATA][0]->return_addr_eq == "1" || $data_sel[LIST_DATA][0]->return_addr_eq == "on") ? "'1'" : "''") ?>, 'return');
                }, 1000);
                function filldata(idcombo, iddtl, jenis, nilai) {
                    nilai = nilai || "";
                    var list = $("#" + iddtl);
                    inHTML = "";
                    list.empty();

                    if (idcombo != '') {
                        $.ajax({
                            url: url,
                            data: {
                                btnAdditional: "act_s_adt", idh: idcombo, tipe: jenis
                            },
                            type: "POST",
                            success: function(response) {
                                if (isSessionExpired(response)) {
                                    response_object = json_decode(response);
                                    url = response_object.url;
                                    location.href = url;
                                } else {
                                    inHTML += "<option value=''>-- Pilih --</option>";
                                    $.each($.parseJSON(response), function() {
                                        if (nilai != '') {
                                            selected = "";
                                            if (nilai == this.seq)
                                                selected = " selected";
                                            inHTML += '<option value="' + this.seq + '"' + selected + '>' + this.name + '</option>';
                                        } else {
                                            inHTML += '<option value="' + this.seq + '">' + this.name + '</option>';
                                        }
                                    });
                                    list.append(inHTML);
                                    list.select2("val", nilai);
                                }
                            },
                            error: function(request, error) {
                                alert(error_response(request.status));
                            }
                        });
                    }
                }
                function cekdata(dtdistrict, grup) {
                    grup = grup || "";
                    var idcity = grup + "city";
                    var iddistrict = grup + "district_seq";
                    if (dtdistrict != '') {
                        $.ajax({
                            url: url,
                            data: {
                                btnAdditional: "act_s_adt", idh: dtdistrict, tipe: 'valpcd'
                            }, type: "POST",
                            success: function(response) {
                                if (isSessionExpired(response)) {
                                    response_object = json_decode(response);
                                    url = response_object.url;
                                    location.href = url;
                                } else {
                                    $.each($.parseJSON(response), function() {
                                        idp = this.idp;
                                        idc = this.idc;
                                        idd = this.idd;
                                    });
                                    $("#" + grup + "propinsi").select2("val", idp);
                                    setTimeout(function() {
                                        filldata(idp, idcity, 'city', idc);
                                    }, 500);
                                    setTimeout(function() {
                                        filldata(idc, iddistrict, 'district', idd);
                                    }, 800);
                                }
                                ;
                            },
                            error: function(request, error) {
                                alert(error_response(request.status));
                            }
                        });
                    }
                }
        <?php
        if (isset($data_sel[LIST_DATA])) {
            echo "cekdata('" . $data_sel[LIST_DATA][0]->district_seq . "');";
//	    if ($data_sel[LIST_DATA][0]->pickup_addr_eq == "1" || $data_sel[LIST_DATA][0]->pickup_addr_eq == "on") {
            echo "setTimeout(function(){cekdata('" . $data_sel[LIST_DATA][0]->pickup_district_seq . "','pickup_');}, 2000);";
//	    }
//	    if ($data_sel[LIST_DATA][0]->return_addr_eq == "1" || $data_sel[LIST_DATA][0]->return_addr_eq == "on") {
            echo "setTimeout(function(){cekdata('" . $data_sel[LIST_DATA][0]->return_district_seq . "','return_');}, 3000);";
//	    }
            echo "cekpr('1');cekpr('3');";
        }
    }
    ?>
    </script>
<?php } else { ?>

    <?php
}
require_once get_include_page_list_merchant_content_footer();
require_once get_include_content_merchant_bottom_page_navigation();
?>