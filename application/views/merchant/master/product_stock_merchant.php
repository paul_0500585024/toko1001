<?php
require_once VIEW_BASE_MERCHANT;
require_once get_include_content_merchant_top_page_navigation();
?>

<?php if ($data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo $data_auth[FORM_ACTION_TITLE] . $data_auth[FORM_AUTH][FORM_TITLE] ?></h3>
        </div>
        <form class ="form-horizontal" id="frmMain" onsubmit ="return validate_form();" method ="post" 
              action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
            <div class="box-body">
                <section class="col-md-12">
                    <?php echo get_csrf_merchant_token(); ?>
                    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
                        <div class ="form-group pull-left col-md-12">
                            <label class ="control-label col-md-1">Produk</label>
                            <div class ="col-md-5">
                                <input class="form-control" name ="product_variant_seq" type="hidden" 
                                       value ="<?php echo $data_sel[LIST_DATA][0][0]->product_variant_seq; ?>">
                                <input class="form-control" name ="product_name" type="text" 
                                       value ="<?php echo $data_sel[LIST_DATA][0][0]->product_name; ?>" readonly>
                            </div>
                        </div>
                        <div class ="form-group pull-left col-md-12">
                            <label class ="control-label col-md-1">Varian</label>
                            <div class ="col-md-5">
                                <input class="form-control" name ="variant_value_seq" type="hidden" 
                                       value ="<?php echo $data_sel[LIST_DATA][0][0]->variant_value_seq; ?>">
                                <input class="form-control" name ="value" type="text" 
                                       value ="<?php echo $data_sel[LIST_DATA][0][0]->value; ?>" readonly>
                            </div>
                        </div>
                        <br><br><br><br><br><br>
                        <table id="tbl" class="diplay table table-bordered table-striped" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <th class="col-md-1"><label class="control-label">Varian</label></th>
                                <th class="col-md-1"><label class="control-label">Merchant SKU</label></th>
                                <th class="col-md-1"><label class="control-label">Stok Sisa</label></th>
                                <th class="col-md-1"><label class="control-label"># Belum Bayar</label></th>
                                <th class="col-md-1"><label class="control-label"># Belum Kirim</label></th>
                                <th class="col-md-1"><label class="control-label">Stok</label></th>
                            </tr>
                            <?php
                            foreach ($data_sel[LIST_DATA][1] as $data) {
                                ?>
                                <tr>
                                <input type ="hidden" name ="value_seq[]" value ="<?php echo $data->value_seq; ?>">
                                <td class="col-md-1"><input class="form-control" name ="size[]" type="text" 
                                                            value ="<?php echo $data->size; ?>" readonly></td>
                                <td class="col-md-1"><input class="form-control" name ="merchant_sku[]" type="text" 
                                                            value ="<?php echo $data->merchant_sku; ?>"></td>
                                <td class="col-md-1"><input class="form-control auto_int" name ="stock[]" type="text" 
                                                            value ="<?php echo $data->stock; ?>" readonly></td>
                                <td class="col-md-1"><input class="form-control" name ="pending_payment[]" type="text" 
                                                            value ="<?php echo $data->pending_payment; ?>" readonly></td>
                                <td class="col-md-1"><input class="form-control" name ="pending_delivery[]" type="text" 
                                                            value ="<?php echo $data->pending_delivery; ?>" readonly></td>
                                <td class="col-md-1"><input class="form-control auto_int" name ="stock_add[]" type="text"
                                                            value ="<?php echo $data->stock; ?>" required=""></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </table>
                    &nbsp;
                    <div class ="form-group">
                        <?php if ($data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
                            <div class ="col-md-6"><?php echo get_back_button(); ?> </div>
                            <?php
                        } else {
                            if ($data_auth[FORM_ACTION] == ACTION_ADD) {
                                ?>
                                <div class ="col-md-6"><?php echo get_save_add_button(); ?> </div>
                            <?php } if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                                <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                            <?php } ?>
                            <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                        <?php } ?>
                    </div>
                </section>
            </div>
        </form>
    </div>

    <script type="text/javascript">
            var isFrmFillValid = $('#frmMain').validate({
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                rules: {
                    stock_add: {
                        required: true
                    },
                }
            });

            $(document).on('submit', '#frmMain', function(e) {
                if (isFrmFillValid.valid()) {
                    $('input[type="text"]').each(function() {
                        if ($(this).hasClass('auto') || $(this).hasClass('auto_int') || $(this).hasClass('auto_dec'))
                        {
                            var v = $(this).autoNumeric('get');
                            $(this).autoNumeric('destroy');
                            $(this).val(v);
                        }
                    });
                }
            });

            $('.auto_int').autoNumeric('init', {vMin: 0, mDec: 0});
    </script>

<?php } else { ?>    
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <?php require_once get_include_page_list_merchant_content_header(); ?>
            <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th column="product_name" style="width: 350px"> Produk </th>
                        <th column="value" style="width: 200px"> Varian </th>
                        <th column="merchant_sku"> Keterangan </th>
                    </tr>
                </thead>
            </table>    
        </div>
    </div>
    <?php
}
require_once get_include_page_list_merchant_content_footer();
require_once get_include_content_merchant_bottom_page_navigation();
?>
