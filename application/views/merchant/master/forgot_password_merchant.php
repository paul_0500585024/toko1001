<?php
require_once VIEW_BASE_MERCHANT;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">

<html>
    <head>
        <link href="<?php echo get_css_url(); ?>bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_css_url(); ?>merchant/toko1001_merchant.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_css_url(); ?>home/style.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="<?php echo get_js_url(); ?>jQuery-2.1.4.min.js" ></script>
        <script type="text/javascript" src="<?php echo get_js_url(); ?>bootstrap.min.js" ></script>
        <script type="text/javascript" src="<?php echo get_js_url(); ?>jquery.validate.js" ></script>
    </head><title>Forgot Password Merchant</title>
    <body class="login-page">   
        <div class="login-box">
            <div class="login-logo">
                <a href="."><b>MERCHANT</b> Toko1001 </a>
            </div>
            <div class="login-box-body">
                <?php if (isset($data_err) && $data_err[ERROR] === true) { ?>
                    <div class="alert alert-error-home">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $data_err[ERROR_MESSAGE][0] ?>
                    </div>
                    <br>
                    <br>
                <?php } elseif (isset($data_suc) && ($data_suc[SUCCESS] === true)) { ?>
                    <div class="alert alert-success-home">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo (isset($data_suc[SUCCESS_MESSAGE][0]) ? $data_suc[SUCCESS_MESSAGE][0] : SAVE_DATA_SUCCESS); ?>
                    </div>           
                    <br>
                    <br>
                <?php } ?>
                   
                <form class="form-horizontal" id="frmMain" action="<?php echo get_base_url() . "merchant/forgot_password"; ?>" method="post">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Email</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="email" placeholder="Email" name="email" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Captcha</label>
                        <div class="col-sm-8">
                            <?php echo $image; ?>
                        </div>
                    </div>

                    <div class="form-group">   
                        <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="security_code" placeholder="Masukkan captcha" validate="required[]">
                        </div>
                        <div class="col-sm-2">
                            <div id="captcha_verify" class="verify"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-offset-2">
                            <div class="col-sm-8">
                                <button type="submit" name="btnSave" value="true" class="btn btn-danger btn-flat" style="width:100%;"><i class="fa fa-save"></i>&nbsp; Lupa Password</button>
                            </div>
                        </div>  
                    </div>
                </form>
            </div>
        </div>
        <script>
            $('#frmMain').validate({
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    security_code: {
                        required: true
                    }
                }
            });

        </script>
    </body>
</html>

