<?php
require_once VIEW_BASE_MERCHANT;
require_once get_include_content_merchant_top_page_navigation();
?>
<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="box box-default">
        <div class="box-header with-border">
    	<h3 class="box-title"><?php echo $data_auth[FORM_ACTION_TITLE] . $data_auth[FORM_AUTH][FORM_TITLE] ?></h3>
        </div>
        <form class="form-horizontal" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>" enctype="multipart/form-data" name="frmMain" id="frmMain">
    	<div class="box-body">
    	    <section class="col-md-12">
		    <?php echo get_csrf_merchant_token(); ?>
		    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
			<input class="form-control"  name ="seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->seq; ?>">
		    <?php } ?>
    		<div>
    		    <!-- Nav tabs -->
    		    <ul class="nav nav-tabs" role="tablist" id="myTab">
    			<li role="presentation" class="active"><a href="#t_1" aria-controls="t_1" role="tab" data-toggle="tab">Informasi Produk</a></li>
    			<li role="presentation" class="hidden" id="t2"><a href="#t_2" aria-controls="t_2" role="tab" data-toggle="tab">Deskripsi Produk</a></li>
    			<li role="presentation" class="hidden" id="t3"><a href="#t_3" aria-controls="t_3" role="tab" data-toggle="tab">Spesifikasi Produk</a></li>
    			<li role="presentation" class="hidden" id="t4"><a href="#t_4" aria-controls="t_5" role="tab" data-toggle="tab">Harga dan Gambar Produk</a></li>
    		    </ul>
    		    <!-- Tab panes -->
    		    <div class="tab-content">
    			<div role="tabpanel" class="tab-pane active" id="t_1"><br />
    			    <div class="row">
    				<div class="col-md-6">
    				    <div class="form-group">
    					<label class="col-md-3 control-label">Nama *</label>
    					<div class="col-md-9">
    					    <div class="input-group">
    						<input type="hidden" name="category_ln_seq" id="category_ln_seq" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->category_ln_seq : ""); ?>">
    						<input class="form-control" validate ="required[]" id="name" name="name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->name) : "") ?>" maxlength="150">
    						<span class="input-group-addon">
    						    <input type="checkbox" name ="include_ins" <?php echo (isset($data_sel[LIST_DATA]) ? (($data_sel[LIST_DATA][0]->include_ins == "1" OR $data_sel[LIST_DATA][0]->include_ins == "on") ? "checked" : "") : "") ?> />
    						    Diasuransi
    						</span>
    					    </div>
    					</div>
    				    </div>
    				    <div class ="form-group">
    					<label class ="control-label col-md-3">Keterangan Garansi</label>
    					<div class="col-md-9">
    					    <input class="form-control" id="warranty_notes" name="warranty_notes" type="text" maxlength="100" value ="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->warranty_notes) : "") ?>">
    					</div>
    				    </div>
    				    <div class ="form-group">
    					<label class ="control-label col-md-3">Dimensi Produk (cm) PxLxT</label>
    					<div class="col-md-3">
    					    <input class="form-control auto" name="p_length_cm" validate="num[]" maxlength="20" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->p_length_cm : "0") ?>">
    					</div>
    					<div class="col-md-3">
    					    <input class="form-control auto" name="p_width_cm" validate="num[]" maxlength="20" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->p_width_cm : "0") ?>">
    					</div>
    					<div class="col-md-3">
    					    <input class="form-control auto" name="p_height_cm" validate="num[]" maxlength="20" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->p_height_cm : "0") ?>">
    					</div>
    				    </div>

    				    <div class="form-group">
    					<label class="col-md-3 control-label">Berat Produk (kg) *</label>
    					<div class="col-md-9">
    					    <input id="p_weight_kg" name="p_weight_kg" validate="num[]" maxlength="20" class="form-control auto" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->p_weight_kg : "0") ?>">
    					</div>
    				    </div>
    				</div>
    				<div class="col-md-6">
    				    <div class="form-group">
    					<div class="box box-warning" style="border:1px solid;width:98%;">
    					    <div class="box-header">
    						<h3 class="box-title">Kategori Produk</h3><input type="hidden" name="catseqval" id="catseqval" value="<?php echo (isset($data_sel[LIST_DATA][0]->catseqval) ? ($data_sel[LIST_DATA][0]->catseqval) : "") ?>">
    					    </div><!-- /.box-header -->

    					    <div class="box-body" id="procat" style="overflow-y:auto;height:500px;">
    					    </div>
    					</div>
    				    </div>
    				</div>
    			    </div>

    			</div><!-- end t1 -->
    			<div role="tabpanel" class="tab-pane" id="t_2"><br />
    			    <div class="row">
    				<div class="col-md-12">
    				    <script type="text/javascript" src="<?php echo get_js_url() ?>ckeditor/ckeditor.js"></script>
    				    <div class ="form-group" style="margin:0 3px 0 3px;">
    					<b>Deskripsi *</b><br />
    					<textarea class="form-control" rows="3" id="description" name="description"><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->description : "") ?></textarea>
    				    </div>
    				    <div class ="form-group" style="margin:15px 3px 10px 3px;">
    					<b>Isi Kemasan</b><br />
    					<textarea class="form-control" rows="3" id="content" name="content" ><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->content : "") ?></textarea>
    				    </div>
    				</div>
    			    </div>
    			</div><!-- end t2 -->
    			<div role="tabpanel" class="tab-pane" id="t_3"><br />
    			    <div class="row">
    				<div class="col-md-6" style="overflow-y:auto;height:400px;">
    				    <div id="spek"></div>
    				    <div id="spesifikasi"></div>
    				</div>
    				<div class="col-md-6" style="overflow-y:auto;height:400px;">
    				    <h3>Spesifikasi Tambahan</h3><input type="hidden" name="spekname[]"><input type="hidden" name="spekval[]">
    				    <table class="table table-striped" id="customFields" width="100%">
    					<tr><td width="45%">Spesifikasi</td><td width="45%">Nilai</td>
    					    <td width="10%"><a href="javascript:void(0);" id="addCF" class="btn btn-info btn-sm">Tambah</a></td>
    					</tr>
					    <?php
					    if (isset($data_sel[LIST_DATA][2])) {
						foreach ($data_sel[LIST_DATA][2] as $each) {
						    ?>
	    					<tr valign="top">
	    					    <td><input type="text" class="form-control" id="spekname" name="spekname[]" value="<?php echo get_display_value($each->name); ?>" maxlength="50" /></td>
	    					    <td><input type="text" class="form-control" id="spekval" name="spekval[]" value="<?php echo get_display_value($each->value); ?>" maxlength="50" /></td>
	    					    <td><a href="javascript:void(0);" class="remCF btn btn-danger btn-sm">Hapus</a></td>
	    					</tr>
						    <?php
						}
					    }
					    ?>
    				    </table>
    				</div>
    			    </div>
    			    <div class="row"><hr style="height: 1px;" />
    				<div class="col-md-12">
    				    <div class ="form-group" style="margin:0 3px 10px 3px;">
    					<h3>Spesifikasi Khusus</h3>
    					<textarea class="form-control" rows="3" id="specification" name="specification"><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->specification : "") ?></textarea>
    				    </div>
    				</div>
    			    </div>
    			</div><!-- end t3 -->
    			<div role="tabpanel" class="tab-pane" id="t_4"><br />
				<?php
				$dtimg = 0;
				$combomaxby = '';
				for ($i = 9; $i > 0; $i--) {
				    $combomaxby.="<option value=\"" . $i . "\">" . $i . "</option>";
				}
				$fromvariant = '<tr><td><h3 id="idv_{varval}"><div id="titlevarian">{vartext}<hr style="width: 100%; color: black; height: 1px;margin: 10px 0px;"><input name="value[]" id="value[]" value="{vartext}" type="hidden"><input name="variant_seq[]" id="variant_seq" value="0" type="hidden"><input type="hidden" name="variant_value_seq[]" id="variant_value_seq" value="{varval}"></h3><div class="row"><div class="col-md-3"><div class ="form-group"><label class ="col-md-5">Harga Produk</label><div class ="col-md-7 input-group"><input class="form-control auto_int" value="0" id="product_price{dtimg}" name="product_price[]" maxlength="50" type="text" onchange="prodpc(this,{dtimg})"></div></div></div><div class="col-md-3"><div class ="form-group"><label class ="col-md-5">Harga Promo</label><div class ="col-md-7 input-group"><input class="form-control auto_int" id="sell_price{dtimg}" maxlength="50" name="sell_price[]" value="0" type="text" onchange="salepc(this,{dtimg})"></div></div></div><div class="col-md-3"><div class ="form-group"><label class ="col-md-5">Diskon %</label><div class ="col-md-5"><input class="form-control" id="disc_percent{dtimg}" name="disc_percent[]" type="text" readonly></div></div></div></div></div><div class="row"><input id="order" name="order[]" type="hidden" value=0><div class="col-md-3"><div class ="form-group"><label class ="col-md-5">Max. Pembelian</label><div class ="col-md-7 input-group"><select class="form-control" name="max_buy[]" id="max_buy">' . $combomaxby . '</select></div></div></div></div><div class="row"><input type="hidden" id="oldfile1_{dtimg}" name="oldfile1[]"><input type="hidden" id="oldfile2_{dtimg}" name="oldfile2[]"><input type="hidden" id="oldfile3_{dtimg}" name="oldfile3[]"><input type="hidden" id="oldfile4_{dtimg}" name="oldfile4[]"><input type="hidden" id="oldfile5_{dtimg}" name="oldfile5[]"><input type="hidden" id="oldfile6_{dtimg}" name="oldfile6[]"><div class="col-md-4"><div class="panel panel-primary"><div class="panel-heading">Gambar Utama <span class="label label-default pull-right">' . IMAGE_PRODUCT_INFO . '</span></div><div class="panel-body"><input type="file" accept="' . IMAGE_TYPE_UPLOAD . '"  id="ifile1" name="ifile1[]" onchange ="previewimg(this, {dtimg}, 1)" /><br /><img id="iprev1_{dtimg}" src="' . get_base_url() . IMG_BLANK_100 . '" alt="Gambar Utama" /><a href="javascript:delimg({dtimg}, 1);" id="linkfile1_{dtimg}" style="display:none;"><i class="fa fa-times"></i>Hapus</a></div></div></div><div class="col-md-4"><div class="panel panel-default"><div class="panel-heading">Gambar 2 <span class="label label-default pull-right">' . IMAGE_PRODUCT_INFO . '</span></div><div class="panel-body"><input type="file" accept="' . IMAGE_TYPE_UPLOAD . '" id="ifile2" name="ifile2[]" onchange ="previewimg(this, {dtimg}, 2)" /><br /><img id="iprev2_{dtimg}" src="' . get_base_url() . IMG_BLANK_100 . '" alt="Gambar 2" /><a href="javascript:delimg({dtimg}, 2);" id="linkfile2_{dtimg}" style="display:none;"><i class="fa fa-times"></i>Hapus</a></div></div></div><div class="col-md-4"><div class="panel panel-default"><div class="panel-heading">Gambar 3 <span class="label label-default pull-right">' . IMAGE_PRODUCT_INFO . '</span></div><div class="panel-body"><input type="file" accept="' . IMAGE_TYPE_UPLOAD . '" id="ifile3" name="ifile3[]" onchange ="previewimg(this, {dtimg}, 3)" /><br /><img id="iprev3_{dtimg}" src="' . get_base_url() . IMG_BLANK_100 . '" alt="Gambar 3" /><a href="javascript:delimg({dtimg}, 3);" id="linkfile3_{dtimg}" style="display:none;"><i class="fa fa-times"></i>Hapus</a></div></div></div><div class="col-md-4"><div class="panel panel-default"><div class="panel-heading">Gambar 4 <span class="label label-default pull-right">' . IMAGE_PRODUCT_INFO . '</span></div><div class="panel-body"><input type="file" accept="' . IMAGE_TYPE_UPLOAD . '" id="ifile4" name="ifile4[]" onchange ="previewimg(this, {dtimg}, 4)" /><br /><img id="iprev4_{dtimg}" src="' . get_base_url() . IMG_BLANK_100 . '" alt="Gambar 4" /><a href="javascript:delimg({dtimg}, 4);" id="linkfile4_{dtimg}" style="display:none;"><i class="fa fa-times"></i>Hapus</a></div></div></div><div class="col-md-4"><div class="panel panel-default"><div class="panel-heading">Gambar 5 <span class="label label-default pull-right">' . IMAGE_PRODUCT_INFO . '</span></div><div class="panel-body"><input type="file" accept="' . IMAGE_TYPE_UPLOAD . '" id="ifile5" name="ifile5[]" onchange ="previewimg(this, {dtimg}, 5)" /><br /><img id="iprev5_{dtimg}" src="' . get_base_url() . IMG_BLANK_100 . '" alt="Gambar 5" /><a href="javascript:delimg({dtimg}, 5);" id="linkfile5_{dtimg}" style="display:none;"><i class="fa fa-times"></i>Hapus</a></div></div></div><div class="col-md-4"><div class="panel panel-default"><div class="panel-heading">Gambar 6 <span class="label label-default pull-right">' . IMAGE_PRODUCT_INFO . '</span></div><div class="panel-body"><input type="file" accept="' . IMAGE_TYPE_UPLOAD . '" name="ifile6[]" onchange ="previewimg(this, {dtimg}, 6)" /><br /><img id="iprev6_{dtimg}" src="' . get_base_url() . IMG_BLANK_100 . '" alt="Gambar 6" /><a href="javascript:delimg({dtimg}, 6);" id="linkfile6_{dtimg}" style="display:none;"><i class="fa fa-times"></i>Hapus</a></div></div></div></div></td>';
				?>
				<?php if (!isset($data_sel[LIST_DATA][3])) { ?>
				    <div class="row">
					<div class="col-md-6">
					    <label class="col-md-3 control-label">Varian :</label>
					    <div class="form-group">
						<div class="radio">
						    <label>
							<input name="varian" id="varian" value="0" checked="" type="radio" onclick="cekvarian(this.value)"> Tidak ada &nbsp; &nbsp;
						    </label>
						    <label>
							<input name="varian" id="varian" value="1" type="radio" onclick="cekvarian(this.value)"> Ada
						    </label>
						</div>
					    </div>
					</div>
					<div class="col-md-6" id="tipevarian"></div>
				    </div>
				    <div id="allvar" >
					<table class="table table-striped">
					    <?php
					    echo str_ireplace('{vartext}', 'Info Produk', str_ireplace('{varval}', '1', str_ireplace('{dtimg}', '0', $fromvariant))) . '</tr>';

					    $fromvariant .='<td valign="top" nowrap><br /><a href="javascript:window.scrollTo(0, 0);" style="margin-right:20px"><i class="fa fa-arrow-up"></i></a> <a href="javascript:void(0);" class="remCV btn btn-danger btn-small" title="{varval}">Hapus</a></td></tr>';
					    ?>
					</table>
				    </div>
				    <div id="variasi" style="display:none;">
					<table id="customVarian" class="table table-striped">

					</table>
				    </div>
				    <?php
				} else {
//				    echo $data_sel[LIST_DATA][3][0]->variant_value_seq;
//				    die(print_r($data_sel[LIST_DATA][3]));
				    $checked0 = '';
				    $checked1 = '';
				    $varshow0 = '';
				    $varshow1 = '';
				    $tabledata0 = '';
				    $tabledata1 = '';
				    $dtimg = 0;
				    $lokasifolder = get_base_url() . TEMP_PRODUCT_UPLOAD_IMAGE . $data_sel[LIST_DATA][0]->merchant_seq . '/';
				    if (isset($data_sel[LIST_DATA][3])) {
					if ($data_sel[LIST_DATA][3][0]->variant_value_seq == "1") {
					    $combomaxbyval = str_ireplace('"' . $data_sel[LIST_DATA][3][0]->max_buy . '"', '"' . $data_sel[LIST_DATA][3][0]->max_buy . '" selected', $combomaxby);
					    $checked0 = " checked";
					    $varshow1 = 'style="display:none;"';
					    $tabledata0 .= '<tr><td><h3><div id="titlevarian">Info Produk <hr style="width: 100%; color: black; height: 1px;margin: 10px 0px;"><input name="value[]" id="value[]" value="Info Produk" type="hidden"><input type="hidden" name="variant_value_seq[]" id="variant_value_seq" value="' . $data_sel[LIST_DATA][3][0]->variant_value_seq . '"></h3>'
						    . '<div class="row"><div class="col-md-3"><div class ="form-group"><label class ="col-md-5">Harga Produk</label><div class ="col-md-7 input-group"><input value="' . $data_sel[LIST_DATA][3][0]->product_price . '" class="form-control auto_int" id="product_price' . $dtimg . '" name="product_price[]" maxlength="40" type="text" onchange="prodpc(this,' . $dtimg . ')" required></div></div></div>'
						    . '<div class="col-md-3"><div class ="form-group"><label class ="col-md-5">Harga Promo</label><div class ="col-md-7 input-group"><input value="' . $data_sel[LIST_DATA][3][0]->sell_price . '" class="form-control auto_int" id="sell_price' . $dtimg . '" name="sell_price[]" maxlength="40" type="text" onchange="salepc(this,' . $dtimg . ')" required></div></div></div>'
						    . '<div class="col-md-3"><div class ="form-group"><label class ="col-md-5">Diskon %</label><div class ="col-md-5"><input value="' . $data_sel[LIST_DATA][3][0]->disc_percent . '" class="form-control" id="disc_percent' . $dtimg . '" name="disc_percent[]" type="text" readonly></div></div></div></div></div>'
						    . '<div class="row"><input value="' . $data_sel[LIST_DATA][3][0]->order . '" id="order" name="order[]" type="hidden">'
						    . '<div class="col-md-3"><div class ="form-group"><label class ="col-md-5">Max. Pembelian</label><div class ="col-md-7 input-group"><select class="form-control" name="max_buy[]" id="max_buy">' . $combomaxbyval . '</select></div></div></div></div>'
						    . '<div class="row">'
						    . '<input type="hidden" name="variant_seq[]" id="variant_seq" value="' . $data_sel[LIST_DATA][3][0]->seq . '">';
					    $gambar = array("", $data_sel[LIST_DATA][3][0]->pic_1_img, $data_sel[LIST_DATA][3][0]->pic_2_img, $data_sel[LIST_DATA][3][0]->pic_3_img, $data_sel[LIST_DATA][3][0]->pic_4_img, $data_sel[LIST_DATA][3][0]->pic_5_img, $data_sel[LIST_DATA][3][0]->pic_6_img);
					    for ($i = 1; $i < 7; $i++) {
						$tabledata0 .='<input type="hidden" name="oldfile' . $i . '[]" id="oldfile' . $i . '_' . $dtimg . '" value="' . $gambar[$i] . '">';
						if ($i == 1) {
						    $tabledata0 .= '<div class = "col-md-4"><div class = "panel panel-primary">';
						    $tabledata0 .= '<div class = "panel-heading">Gambar Utama <span class="label label-default pull-right">' . IMAGE_PRODUCT_INFO . '</span></div>';
						    $tabledata0 .= '<div class = "panel-body"><input type = "file" accept = "' . IMAGE_TYPE_UPLOAD . '" id = "ifile1_' . $dtimg . '" name = "ifile1[]" onchange = "previewimg(this, ' . $dtimg . ', 1)"/><br /><img id = "iprev1_' . $dtimg . '" src = "' . (($gambar[$i] != '') ? $lokasifolder . $gambar[$i] : get_base_url() . IMG_BLANK_100) . '" height = "100" alt = "Gambar Utama" />' . (($data_auth[FORM_ACTION] == ACTION_VIEW) ? '' : '<a href = "javascript:delimg(\'' . $dtimg . '\',\'1\');" id = "linkfile1_' . $dtimg . '" ' . (($gambar[$i] != '') ? "'" : " style = 'display:none;'") . '><i class="fa fa-times"></i>Hapus</a>') . '</div>';
						    $tabledata0 .= '</div></div>';
						} else {
						    $tabledata0 .='<div class="col-md-4"><div class="panel panel-default">';
						    $tabledata0 .= '<div class="panel-heading">Gambar ' . $i . '<span class="label label-default pull-right">' . IMAGE_PRODUCT_INFO . '</span></div>';
						    $tabledata0 .= '<div class="panel-body"><input type="file" accept="' . IMAGE_TYPE_UPLOAD . '" id="ifile' . $i . '_' . $dtimg . '" name="ifile' . $i . '[]" onchange ="previewimg(this, ' . $dtimg . ', ' . $i . ')" /><br /><img id="iprev' . $i . '_' . $dtimg . '" src="' . (($gambar[$i] != '') ? $lokasifolder . $gambar[$i] : get_base_url() . IMG_BLANK_100) . '" height="100" alt="Gambar ' . $i . '" />' . (($data_auth[FORM_ACTION] == ACTION_VIEW) ? '' : '<a href="javascript:delimg(\'' . $dtimg . '\',\'' . $i . '\');" id="linkfile' . $i . '_' . $dtimg . '"' . (($gambar[$i] != '') ? "'" : " style='display:none;'") . '><i class="fa fa-times"></i>Hapus</a>') . '</div>';
						    $tabledata0 .='</div></div>';
						}
					    }
					    $tabledata0 .='</tr>';
					} else {
					    $tabledata0 = str_ireplace('{vartext}', 'Info Produk', str_ireplace('{varval}', '1', str_ireplace('{dtimg}', '0', $fromvariant))) . '</tr>';

					    $checked1 = " checked";
					    $varshow0 = 'style="display:none;"';
					    foreach ($data_sel[LIST_DATA][3] as $each) {
						$ivariant[] = $each->variant_value_seq;
						$dtimg++;
						$gambar = array();
						$combomaxbyval = str_ireplace('"' . $each->max_buy . '"', '"' . $each->max_buy . '" selected', $combomaxby);
						$tabledata1 .= '<tr><td><h3><div id="titlevarian">' . $each->value . '<hr style="width: 100%; color: black; height: 1px;margin: 10px 0px;"><input name="value[]" id="value[]" value="' . $each->value . '" type="hidden"><input type="hidden" name="variant_value_seq[]" id="variant_value_seq" value="' . $each->variant_value_seq . '"></h3>'
							. '<div class="row"><div class="col-md-3"><div class ="form-group"><label class ="col-md-5">Harga Produk</label><div class ="col-md-7 input-group"><input value="' . $each->product_price . '" maxlength="40" class="form-control auto_int" id="product_price' . $dtimg . '" name="product_price[]" type="text" onchange="prodpc(this,' . $dtimg . ')" required></div></div></div>'
							. '<div class="col-md-3"><div class ="form-group"><label class ="col-md-5">Harga Promo</label><div class ="col-md-7 input-group"><input value="' . $each->sell_price . '" maxlength="40" class="form-control auto_int" id="sell_price' . $dtimg . '" name="sell_price[]" type="text" onchange="salepc(this,' . $dtimg . ')" required></div></div></div>'
							. '<div class="col-md-3"><div class ="form-group"><label class ="col-md-5">Diskon %</label><div class ="col-md-5"><input value="' . $each->disc_percent . '" class="form-control" id="disc_percent' . $dtimg . '" name="disc_percent[]" type="text" readonly></div></div></div></div></div>'
							. '<div class="row"><input value="' . $each->order . '" id="order" name="order[]" type="hidden">'
							. '<div class="col-md-3"><div class ="form-group"><label class ="col-md-5">Max. Pembelian</label><div class ="col-md-7 input-group"><select class="form-control" name="max_buy[]" id="max_buy">' . $combomaxbyval . '</select></div></div></div></div>'
							. '<div class="row">'
							. '<input type="hidden" name="variant_seq[]" id="variant_seq" value="' . $each->seq . '">';
						$gambar = array("", $each->pic_1_img, $each->pic_2_img, $each->pic_3_img, $each->pic_4_img, $each->pic_5_img, $each->pic_6_img);
						for ($i = 1; $i < 7; $i++) {
						    $tabledata1 .='<input type="hidden" name="oldfile' . $i . '[]" id="oldfile' . $i . '_' . $dtimg . '" value="' . $gambar[$i] . '">';
						    if ($i == 1) {
							$tabledata1 .= '<div class = "col-md-4"><div class = "panel panel-primary">';
							$tabledata1 .= '<div class = "panel-heading">Gambar Utama <span class="label label-default pull-right">' . IMAGE_PRODUCT_INFO . '</span></div>';
							$tabledata1 .= '<div class = "panel-body"><input type = "file" accept = "' . IMAGE_TYPE_UPLOAD . '" id = "ifile1_' . $dtimg . '" name = "ifile1[]" onchange = "previewimg(this, ' . $dtimg . ', 1)"/><br /><img id = "iprev1_' . $dtimg . '" src = "' . (($gambar[$i] != '') ? $lokasifolder . $gambar[$i] : get_base_url() . IMG_BLANK_100) . '" height = "100" alt = "Gambar Utama" />' . (($data_auth[FORM_ACTION] == ACTION_VIEW) ? '' : '<a href = "javascript:delimg(\'' . $dtimg . '\',\'1\');" id = "linkfile1_' . $dtimg . '" ' . (($gambar[$i] != '') ? "'" : " style = 'display:none;'") . '><i class="fa fa-times"></i>Hapus</a>') . '</div>';
							$tabledata1 .= '</div></div>';
						    } else {
							$tabledata1 .='<div class="col-md-4"><div class="panel panel-default">';
							$tabledata1 .= '<div class="panel-heading">Gambar ' . $i . '<span class="label label-default pull-right">' . IMAGE_PRODUCT_INFO . '</span></div>';
							$tabledata1 .= '<div class="panel-body"><input type="file" accept="' . IMAGE_TYPE_UPLOAD . '" id="ifile_' . $dtimg . '' . $i . '" name="ifile' . $i . '[]" onchange ="previewimg(this, ' . $dtimg . ', ' . $i . ')" /><br /><img id="iprev' . $i . '_' . $dtimg . '" src="' . (($gambar[$i] != '') ? $lokasifolder . $gambar[$i] : get_base_url() . IMG_BLANK_100) . '" height="100" alt="Gambar ' . $i . '" />' . (($data_auth[FORM_ACTION] == ACTION_VIEW) ? '' : '<a href="javascript:delimg(\'' . $dtimg . '\',\'' . $i . '\');" id="linkfile' . $i . '_' . $dtimg . '"' . (($gambar[$i] != '') ? "'" : " style='display:none;'") . '><i class="fa fa-times"></i>Hapus</a>') . '</div>';
							$tabledata1 .='</div></div>';
						    }
						}
						$tabledata1 .= '<td valign="top" nowrap><br /><a href="javascript:window.scrollTo(0, 0);" style="margin-right:20px"><i class="fa fa-arrow-up"></i></a> <a href="javascript:void(0);" class="remCV btn btn-danger btn-small" title="' . $each->variant_value_seq . '">Hapus</a></td></tr>';
					    }
					}
				    }
				    ?>
				    <div class="row">
					<div class="col-md-5">
					    <label class="col-md-3 control-label">Varian :</label>
					    <div class="form-group">
						<div class="radio">
						    <label>
							<input name="varian" id="varian" value="0" type="radio" onclick="cekvarian(this.value)" <?php echo $checked0; ?>> Tidak ada &nbsp; &nbsp;
						    </label>
						    <label>
							<input name="varian" id="varian" value="1" type="radio" onclick="cekvarian(this.value)" <?php echo $checked1; ?>> Ada
						    </label>
						</div>
					    </div>
					</div>
					<div class="col-md-5" id="tipevarian"></div>
				    </div>
				    <div id="allvar" <?php echo $varshow0; ?>>
					<table class="table table-striped">
					    <?php
					    echo $tabledata0;
					    $fromvariant .='<td valign="top" nowrap><br /><a href="javascript:window.scrollTo(0, 0);" style="margin-right:20px"><i class="fa fa-arrow-up"></i></a> <a href="javascript:void(0);" class="remCV btn btn-danger btn-small" title="{varval}">Hapus</a></td></tr>';
					    ?>
					</table>
				    </div>
				    <div id="variasi" <?php echo $varshow1; ?>>
					<table id="customVarian" class="table table-striped">
					    <?php echo $tabledata1; ?>
					</table>
				    </div>
				<?php } ?>
    			</div><!-- end t4 -->
    		    </div>
    		    <div class ="form-group">
			    <?php
			    if ($data_auth[FORM_ACTION] == ACTION_VIEW) {
				?>
				<div class ="col-md-6"><?php echo get_back_button(); ?> </div>
				<?php
			    } else {
				if ($data_auth[FORM_ACTION] == ACTION_ADD) {
				    ?>
	    			<div class ="col-md-6"><?php echo get_save_add_button(); ?> </div>
				<?php } else { ?>
	    			<input name ="status" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->status; ?>">
	    			<div class ="col-md-6"><?php IF ($data_sel[LIST_DATA][0]->status == 'N') echo get_save_edit_button(); ?> </div>
				<?php } ?>
				<div class ="col-md-6"><?php echo get_cancel_button(); ?></div>
			    <?php } ?>
    		    </div><!-- /.box-footer -->
    	    </section>
    	</div>
        </form>
    </div>
    <?php
    $dataatribute = '';
    if (isset($data_sel[LIST_DATA][1])) {
	foreach ($data_sel[LIST_DATA][1] as $each) {
	    $dataatribute.='{' . $each->attribute_value_seq . '}';
	}
    }
    ?>
    <script type="text/javascript">
        var icount = <?php echo $dtimg; ?>;
        var ivariant = [];
        var url = "<?php echo get_base_url() . $data_auth[FORM_URL]; ?>";
        CKEDITOR.replace('description');
        CKEDITOR.replace('content');
        CKEDITOR.replace('specification');

        function previewimg(thisval, thecount, theid) {
    	if (thisval.files && thisval.files[0]) {
    	    var reader = new FileReader();
    	    reader.onload = function (e) {
    		$('#iprev' + theid + '_' + thecount).attr('src', e.target.result).height(100);
    		$('#linkfile' + theid + '_' + thecount).show();
    	    }
    	    reader.readAsDataURL(thisval.files[0]);
    	}
        }
        function cProcat() {
    	$("#procat").html("");
    	$.ajax({
    	    url: url,
    	    data: {btnAdditional: "act_s_adt", tipe: "category"
    	    },
    	    type: "POST",
    	    success: function (response) {
    		if (isSessionExpired(response)) {
    		    response_object = json_decode(response);
    		    url = response_object.url;
    		    location.href = url;
    		} else {
    		    $("#procat").html(response);
    		}
    	    },
    	    error: function (request, error) {
    		alert(error_response(request.status));
    	    }
    	});
        }
    <?php
//    if ($data_auth[FORM_ACTION] == ACTION_ADD) {
    if (!isset($data_sel[LIST_DATA])) {
	?>
	    var notconfirm = true;
	    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		var target = $(e.target).attr("href");
		if (notconfirm) {
		    var r = confirm("Data kategori tidak dapat dirubah kembali. Lanjutkan ?");
		    if (r == true) {
			$(".radiobtn").attr("disabled", true);
			notconfirm = false;
		    } else {
			$('#myTab a:first').tab('show');
		    }
		}

	    });
	    cProcat();
    <?php } ?>


        // cek breadcrumb
        function cekseq(idm) {
    	$("#catseqval").val($('input[name=catseq]:checked', '#frmMain').val());
    	$('#t2').removeClass('hidden');
    	$('#t3').removeClass('hidden');
    	$('#t4').removeClass('hidden');

    	$.ajax({
    	    url: url,
    	    data: {btnAdditional: "act_s_adt", idh: idm, tipe: "breadcrumb"
    	    },
    	    type: "POST",
    	    success: function (response) {
    		if (isSessionExpired(response)) {
    		    response_object = json_decode(response);
    		    url = response_object.url;
    		    location.href = url;
    		} else {
    		    $("#spek").html(response);
    		    cekatribute();
    		}
    	    },
    	    error: function (request, error) {
    		alert(error_response(request.status));
    	    }
    	});
        }
    <?php // }                                                                                                                                                                                                                                                             ?>
        //	cek  attribute
        function cekatribute() {
    	var idm = $("#alcat").val();
    	var nilaiarray = "<?php echo $dataatribute; ?>";
    	$.ajax({
    	    url: url,
    	    data: {btnAdditional: "act_s_adt", idh: idm, atrval: nilaiarray, tipe: "attribute"
    	    },
    	    type: "POST",
    	    success: function (response) {
    		if (isSessionExpired(response)) {
    		    response_object = json_decode(response);
    		    url = response_object.url;
    		    location.href = url;
    		} else {
    		    $("#spesifikasi").html(response);
    		}
    	    },
    	    error: function (request, error) {
    		alert(error_response(request.status));
    	    }
    	});
        }
        $("#addCF").click(function () {
    	$("#customFields").append('<tr valign="top"><td><input type="text" class="form-control" id="spekname" name="spekname[]" value="" maxlength="50" /></td>\n\<td><input type="text" class="form-control" id="spekval" name="spekval[]" value="" maxlength="50" /></td><td><a href="javascript:void(0);" class="remCF btn btn-danger btn-sm">Hapus</a></td></tr>');
        });
        $("#customFields").on('click', '.remCF', function () {
    	$(this).parent().parent().remove();
        });

        function getvarianttitle() {
    	var idm = $("#alcat").val();
    	$.ajax({url: url,
    	    data: {btnAdditional: "act_s_adt", idh: idm, tipe: "variant"},
    	    type: "POST",
    	    success: function (response) {
    		if (isSessionExpired(response)) {
    		    response_object = json_decode(response);
    		    url = response_object.url;
    		    location.href = url;
    		} else {
    		    $("#tipevarian").html(response);
    		}
    	    },
    	    error: function (request, error) {
    		alert(error_response(request.status));
    	    }
    	});
        }
        function cekvarian(nilai) {
    	if (nilai == 0) {
    	    $("#tipevarian").hide("slow");
    	    $("#allvar").show("slow");
    	    $("#variasi").hide();
    	} else {
    	    $("#allvar").hide();
    	    $("#tipevarian").show("slow");
    	    getvarianttitle();
    	}
        }
        function adcvclick() {
    	$("#variasi").show();
    	icount = (icount + 1);
    	var vartext = $("#varianval option:selected").text();
    	var varval = $("#varianval").val();
    	var inaray = ivariant.indexOf(varval);
    	if (inaray != (-1)) {
    	    alert("Data sudah ada");
    	    return;
    	} else {
    	    ivariant.push(varval);
    	}
    	var str = '<?php echo $fromvariant; ?>';
    	var newdata = str.replace(/{vartext}/g, vartext);
    	newdata = newdata.replace(/{varval}/g, varval);
    	newdata = newdata.replace(/{dtimg}/g, icount);
    	$("#customVarian").append(newdata);

    	$('.auto').autoNumeric('init', {vMin: 0});
    	$('.auto_int').autoNumeric('init', {vMin: 0, mDec: 0});
    	$('.auto_dec').autoNumeric('init', {vMin: 0, mDec: 2});
    	$('html, body').animate({
    	    scrollTop: $("#idv_" + varval).offset().top
    	}, 1000);
        }
        $("#customVarian").on('click', '.remCV', function () {
    	var href = $(this).attr('title');
    	var index = ivariant.indexOf(href);
    	if (index > -1) {
    	    ivariant.splice(index, 1);
    	}
    	$(this).parent().parent().remove();
    	return false;
        });
        function cekdiskon(hargap, hargaj, iddisk) {
    	if ($.isNumeric(hargap) && $.isNumeric(hargaj)) {
    	    var nilaidiskon = (hargap - hargaj) / hargap * 100;
    	    iddisk.val(Math.round(nilaidiskon));
    	}
        }
        function prodpc(idproc, nomor) {
    	var $this = $(idproc);
    	$('#sell_price' + nomor).val(idproc.value);
    	var salep = $('#sell_price' + nomor).autoNumeric('get');
    	var prodp = $this.autoNumeric('get');

    	cekdiskon(prodp, salep, $('#disc_percent' + nomor));
        }
        function salepc(idproc, nomor) {
    	var $this = $(idproc);
    	var prodp = $('#product_price' + nomor).autoNumeric('get');
    	var salep = $this.autoNumeric('get');
    	cekdiskon(prodp, salep, $('#disc_percent' + nomor));
        }
        function delimg(idgbr, nomor) {
    	$("#oldfile" + nomor + "_" + idgbr).val("");
    	$("#iprev" + nomor + "_" + idgbr).attr("src", "<?php echo get_base_url() . IMG_BLANK_100; ?>");
    	$("#linkfile" + nomor + "_" + idgbr).hide();
    	$("#ifile" + nomor + "_" + idgbr).val('');

        }
    <?php
    if (isset($data_sel[LIST_DATA])) {
	if (isset($ivariant)) {
	    foreach ($ivariant as $datavariant) {
		echo "ivariant.push('" . $datavariant . "');";
	    }
	} else {
	    echo "$('#tipevarian').hide();";
	}
	?>
	    $('#t2').removeClass('hidden');
	    $('#t3').removeClass('hidden');
	    $('#t4').removeClass('hidden');
	    $("#catseq").prop("disabled", true);
	    $.ajax({
		url: url,
		data: {btnAdditional: "act_s_adt", idh: '<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->category_ln_seq : ""); ?>', tipe: "breadcrumb"
		},
		type: "POST",
		success: function (response) {
		    if (isSessionExpired(response)) {
			response_object = json_decode(response);
			url = response_object.url;
			location.href = url;
		    } else {
			$("#procat").html(response);
			cekatribute();
			getvarianttitle();
		    }
		},
		error: function (request, error) {
		    alert(error_response(request.status));
		}
	    });
	    $("#spek").html("<h3>Spesifikasi Produk</h3>");
	    $(document).ready(function () {
		$('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
		    localStorage.setItem('activeTab', $(e.target).attr('href'));
		});
		var activeTab = localStorage.getItem('activeTab');
		if (activeTab) {
		    $('#myTab a[href="' + activeTab + '"]').tab('show');
		}
	    });
	<?php
    }
    ?>
    </script>
<?php } else {
    ?>
    <div class="box box-default">
        <div class="box-header with-border">
    	<h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
	    <?php require_once get_include_page_list_merchant_content_header(); ?>
    	<table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
    	    <thead>
    		<tr>
    		    <th column="name"> Nama Produk</th>
    		    <th column="status"> Status </th>
    		    <th column="created_date"> <?php echo TH_CREATED_DATE; ?> </th>
    		    <th column="modified_date"> <?php echo TH_MODIFIED_DATE; ?> </th>
    		</tr>
    	    </thead>
    	</table>
        </div>
    </div>
    <?php
}
require_once get_include_page_list_merchant_content_footer();
require_once get_include_content_merchant_bottom_page_navigation();
?>