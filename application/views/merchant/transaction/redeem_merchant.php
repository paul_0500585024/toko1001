<?php
require_once VIEW_BASE_MERCHANT;
require_once get_include_content_merchant_top_page_navigation();
?>
<?php if ($data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="box-body">
        <div class="col-md-8">
    	<dl class="dl-horizontal">

    	    <dt>Merchant :</dt>
    	    <dd><?php echo $data_sel[LIST_DATA][0]->name; ?></dd>
    	    <dt>Total :</dt>
    	    <dd>Rp. <?php echo number_format($data_sel[LIST_DATA][0]->total); ?></dd>
    	    <dt>Bank :</dt>
    	    <dd><?php echo $data_sel[LIST_DATA][0]->bank_name; ?></dd>
    	    <dt>Cabang :</dt>
    	    <dd><?php echo $data_sel[LIST_DATA][0]->bank_branch_name; ?></dd>
    	    <dt>No Akun :</dt>
    	    <dd><?php echo $data_sel[LIST_DATA][0]->bank_acct_no; ?></dd>
    	    <dt>Nama Akun :</dt>
    	    <dd><?php echo $data_sel[LIST_DATA][0]->bank_acct_name; ?></dd>
    	</dl>
        </div>
        <div class="col-md-4">
    	<table class="table table-condensed">
    	    <tr>
    		<th>Tipe</th>
    		<th>Jumlah</th>
    	    </tr>
		<?php
		//die(print_r($data_sel[LIST_DATA]));
		foreach ($data_sel[LIST_DATA][1] as $each) {
		    ?>
		    <tr>
			<td><?php echo status($each->type, REDEEM_TYPE); ?></td>
			<td><?php echo number_format($each->total); ?></td>
		    </tr>
		<?php }
		?>
    	</table>
        </div>
    </div>
    <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
    	<!-- Nav tabs -->
    	<ul class="nav nav-tabs" role="tablist">
    	    <li role="presentation" class="active"><a href="#t_1" aria-controls="t_1" role="tab" data-toggle="tab">Detil Order</a></li>
    	    <li role="presentation"><a href="#t_2" aria-controls="t_2" role="tab" data-toggle="tab">Detil Ekspedisi</a></li>
    	</ul>
    	<!-- Tab panes -->
    	<div class="tab-content">
    	    <div role="tabpanel" class="tab-pane active" id="t_1"><br />
    		<div class="row">
    		    <div class="col-md-12">
    			<table class="table table-striped table-bordered">
    			    <tr>
    				<th>No. Order</th>
    				<th>Nilai Order</th>
    				<th>Nilai Ekpedisi</th>

    				<th>Nilai Komisi</th>
    				<th>Total Redeem</th>
    			    </tr>
				<?php
				if (isset($data_sel[LIST_DATA][2])) {
				    //die(print_r($data_sel[LIST_DATA]));
				    $ttlorder = 0;
				    $ttlexp = 0;
				    $ttlfee = 0;
				    $ttlall = 0;
				    foreach ($data_sel[LIST_DATA][2] as $each) {
//					$totalexpedition = 0;
//					if ($each->free_fee_seq > 0) {
//					    if ($each->real_expedition_service_seq == 0) {
//						$totalexpedition = 0;
//					    } else {
//						$totalexpedition = $each->totalexpedition * $each->exp_fee_percent * (-1) / 100;
//					    }
//					} else {
//					    if ($each->real_expedition_service_seq == 0) {
//						$totalexpedition = $each->totalexpedition * $each->exp_fee_percent / 100;
//					    } else {
//						$totalexpedition = 0;
//					    }
//					}
//					$totalexpedition = $each->totalexpedition;
					?>
	    			    <tr>
	    				<td><?php echo $each->order_no; ?></td>
	    				<td align="right"><?php echo number_format($each->totalorder); ?></td>
	    				<td align="right"><?php echo number_format($each->totalexpedition); ?></td>
	    				<td align="right"><?php echo number_format($each->totalfee); ?></td>
	    				<td align="right"><?php echo number_format($each->totalorder + $each->totalexpedition - $each->totalfee); ?></td>
	    			    </tr>
					<?php
					$ttlorder = ($ttlorder + $each->totalorder);
					$ttlexp = ($ttlexp + $each->totalexpedition );
					$ttlfee = ($ttlfee + $each->totalfee);
					$ttlall = ($ttlall + $each->totalorder + $each->totalexpedition - $each->totalfee);
				    }
				    echo '
					<tr>
					    <td align="right">Total</td>
	    				    <td align="right">' . number_format($ttlorder) . '</td>
	    				    <td align="right">' . number_format($ttlexp) . '</td>
	    				    <td align="right">' . number_format($ttlfee) . '</td>
	    				    <td align="right">' . number_format($ttlall) . '</td>
	    				</tr>';
				}
				?>
    			</table>
    		    </div>
    		</div>
    	    </div>
    	    <div role="tabpanel" class="tab-pane" id="t_2"><br />
    		<div class="row">
    		    <div class="col-md-12">
    			<table class="table table-striped table-bordered">
    			    <tr>
    				<th>No. Order</th>
    				<th>Tujuan</th>
    				<th>Nilai Ekpedisi</th>
    			    </tr>
				<?php
				if (isset($data_sel[LIST_DATA][3])) {
				    $totalexpedition = 0;
				    foreach ($data_sel[LIST_DATA][3] as $each) {
					?>
	    			    <tr>
	    				<td><?php echo $each->order_no; ?></td>
	    				<td align="right"><?php echo $each->destination; ?></td>
	    				<td align="right"><?php echo number_format($each->totalorder); ?></td>
	    			    </tr>
					<?php
					$totalexpedition = ($totalexpedition + $each->totalorder);
				    }
				    echo '
					<tr>
					    <td align="right" colspan=2>Total</td>
	    				    <td align="right">' . number_format($totalexpedition) . '</td>
	    				</tr>';
				}
				?>
    			</table>
    		    </div>
    		</div>
    	    </div>
    	</div>

        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <a href="" class="btn btn-default">Back</a>
<?php } else { ?>
    <div class="box box-default">
        <div class="box-header with-border">
    	<h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
	    <?php require_once get_include_page_list_merchant_content_header(); ?>
    	<table id="tbl" width="100%" class="display table table-bordered table-striped" url="<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
    	    <thead>
    		<tr>
    		    <th column="paid_date"> Tanggal</th>
    		    <th column="total"> Total</th>
    		</tr>
    	    </thead>
    	</table>
        </div>
    </div>
    <?php
}
require_once get_include_page_list_merchant_content_footer();
require_once get_include_content_merchant_bottom_page_navigation();
?>