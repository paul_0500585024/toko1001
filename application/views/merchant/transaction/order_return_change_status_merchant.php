<?php
require_once VIEW_BASE_MERCHANT;
require_once get_include_content_merchant_top_page_navigation();
?>
<link rel="stylesheet" type="text/css" href="<?php echo get_css_url() ?>daterangepicker-bs3.css" />
<script type="text/javascript" src="<?php echo get_js_url() ?>moment.min.js"></script>
<script type="text/javascript" src="<?php echo get_js_url() ?>daterangepicker.js"></script>

<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
    </div>
    <div class="box-body">                    
        <?php /* echo "<pre>";print_r($data_sel);echo "</pre>" */; ?>
        <form class ="form-horizontal" id="frmSearch" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">                    
            <div class="row">
                <section class="col-md-12 col-lg-12">

                    <?php echo get_csrf_merchant_token(); ?>
                    <input type="hidden" name="return_no" class="form-control" value="<?php echo $data_sel[LIST_DATA][0]->return_no; ?>">                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">No. Return</label>
                            <div class="col-md-8"><input type="text" class="form-control" value="<?php echo $data_sel[LIST_DATA][0]->return_no; ?>" readonly></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Tgl. Pengajuan</label>
                            <div class="col-md-8"><input type="text" class="form-control" value="<?php echo c_date($data_sel[LIST_DATA][0]->created_date); ?>" readonly></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">Status Pengembalian</label>
                            <div class="col-md-8 col-sm-8">
                                <select class="form-control select2" name="return_status">                                    
                                    <?php
                                    echo combostatus(json_decode(STATUS_RETURN_BY_MERCHANT));
                                    ?>
                                </select>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2">Komentar</label>
                            <div class="col-md-10 col-sm-10"><textarea class="form-control"name="merchant_comment" style="height: 125px;  max-height: 125px; resize: none;"><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->review_merchant : ""); ?></textarea></div>
                        </div>
                    </div>
                </section>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-md-6" >
                            <?php
                            if ($data_sel[LIST_DATA][0]->shipment_status == RECEIVED_BY_MERCHANT) {
                                echo get_save_edit_button();
                            }
                            ?>
                        </div>
                        <div class="col-md-6">
                            <a href="<?php echo get_base_url(); ?>merchant/transaction/return_merchant/" class="btn btn-google-plus" style="width: 100%;">&nbsp;Kembali</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>


<script  type="text/javascript">
            $('input[name="tgl_kirim"]').daterangepicker({
                format: 'DD-MMM-YYYY',
                singleDatePicker: true,
                showDropdowns: true,
                startDate: "<?php echo date('d-M-Y') ?>"
            });
</script>
<script  type="text/javascript">
    $("#save_edit").click(function() {
        var action = $('.select2 :selected').text();
        return confirm('Anda yakin untuk mengubah status menjadi "' + action + '" ?');
    })
</script>

<?php
require_once get_include_page_list_merchant_content_footer();
require_once get_include_content_merchant_bottom_page_navigation();
?>