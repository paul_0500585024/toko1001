<?php
require_once VIEW_BASE_MERCHANT;
require_once get_include_content_merchant_top_page_navigation();
?>
<?php
if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT or $data_auth[FORM_ACTION] == ACTION_VIEW) {
    $statusorder = $data_sel[LIST_DATA][0][0]->order_status;
    ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo $data_auth[FORM_ACTION_TITLE] . $data_auth[FORM_AUTH][FORM_TITLE] ?></h3>
        </div>
        <form class="form-horizontal" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>" name="form1" id="form1">
            <div class="box-body">
                <section class="col-md-12">
                    <?php echo get_csrf_merchant_token(); ?>
                    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                        <input class="form-control"  name ="seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0][0]->seq; ?>">
                    <?php } ?>
                    <div class="row">
                        <div class="col-sm-4">
                            Nomor Order : <span class="label label-primary"><strong><?php echo $data_sel[LIST_DATA][0][0]->order_no; ?></strong></span><br />
                            Tanggal Order : <?php echo date("d-M-Y", strtotime($data_sel[LIST_DATA][0][0]->order_date)); ?><br />
                            Status Order : <span class="label label-info"><strong><?php echo status($data_sel[LIST_DATA][0][0]->order_status, STATUS_ORDER); ?></strong>
                                <input type="hidden" name="order_status" id="order_status" value="<?php echo $data_sel[LIST_DATA][0][0]->order_status; ?>"></span>
                        </div>
                        <div class="col-sm-5">
                            Penerima<br />
                            <strong><?php echo $data_sel[LIST_DATA][0][0]->receiver_name; ?></strong><br />
                            <?php echo $data_sel[LIST_DATA][0][0]->receiver_address; ?><br />
                            <?php echo $data_sel[LIST_DATA][2][0]->d_name; ?>, <?php echo $data_sel[LIST_DATA][2][0]->c_name; ?><br />
                            <?php echo $data_sel[LIST_DATA][2][0]->p_name; ?>, Kode Pos <?php echo $data_sel[LIST_DATA][0][0]->receiver_zip_code; ?><br />
                            Nomor Telp. : <?php echo $data_sel[LIST_DATA][0][0]->receiver_phone_no; ?><br />

                        </div>
                        <div class="col-sm-3">
                            Pesan Member:<br />
                            <?php echo $data_sel[LIST_DATA][0][0]->member_notes; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <h3>List Product</h3>
                            <table class="table table-striped">
                                <tr><th>#</th><th>Nama Produk</th><th>Qty</th><th>Harga</th><!-- <th>Total</th>--><th>Status Produk</th></tr>
                                <?php
                                if (isset($data_sel[LIST_DATA][1])) {
                                    foreach ($data_sel[LIST_DATA][1] as $each) {
                                        ?>
                                        <tr>
                                            <td align="center" width="110"><?php echo (($each->pic_1_img == "") ? $each->merchant_sku : "<img src='" . base_url(PRODUCT_UPLOAD_IMAGE) . "/" . $each->merchant_seq . "/" . $each->pic_1_img . "' width = '100px'  height='100px' alt='" . $each->merchant_sku . "'><br />" . $each->merchant_sku); ?></td>
                                            <td valign="top"><?php echo $each->name; ?><input type="hidden" name="product_variant_seq[]" id="product_variant_seq" value="<?php echo $each->product_variant_seq; ?>"></td>
                                            <td valign="top"><?php echo number_format($each->qty); ?></td>
                                            <td valign="top"><?php echo number_format($each->sell_price); ?></td>
                                            <?php if ($statusorder == "N") { ?>
                                                <td><select name="product_status[]" id="product_status" class="prdsts">
                                                        <option value="R"<?php echo(($each->product_status == "R") ? " selected" : ""); ?>>Ada</option>
                                                        <option value="X"<?php echo(($each->product_status == "X") ? " selected" : ""); ?>>Tidak Ada</option>
                                                    </select>
                                                </td>
                                            <?php } else { ?>
                                                <td><?php echo(($each->product_status == "R") ? " Ada" : "Tidak Ada"); ?></td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </table>
                            <div class ="form-group">
                                <label class ="control-label col-md-3">Pesan untuk ekspedisi</label>
                                <div class ="col-md-8">
                                    <?php if ($statusorder == "N") { ?>
                                        <input type="text" class="form-control" id="ship_notes" name="ship_notes" value="<?php echo $data_sel[LIST_DATA][0][0]->ship_notes; ?>" maxlength="45">
                                    <?php } else { ?>
                                        <input type="text" class="form-control" id="ship_notes" name="ship_notes" value="<?php echo $data_sel[LIST_DATA][0][0]->ship_notes; ?>" disabled>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    if ($data_auth[FORM_ACTION] == ACTION_VIEW) {
                        if ($statusorder == "S" || $statusorder == "D") {
                            if ($data_sel[LIST_DATA][0][0]->ship_by == 0) {
                                ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class ="form-group">
                                            <label class ="control-label col-md-3">Expedisi *</label>
                                            <div class ="col-md-8">
                                                <select class="form-control" name="ship_by_exp_seq" id="ship_by_exp_seq" disabled>
                                                    <option value=''></option>
                                                    <?php
                                                    if (isset($exp_name)) {
                                                        foreach ($exp_name as $each) {
                                                            ?>
                                                            <option value="<?php echo $each->seq; ?>" <?php echo(($data_sel[LIST_DATA][0][0]->ship_by_exp_seq == $each->seq) ? ' selected' : '') ?>><?php echo $each->name; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                                <div id="merchant_ship" style="<?php echo (($data_sel[LIST_DATA][0][0]->ship_by != '')? : 'display:none') ?>;">
                                                    <input class="form-control" id="ship_by" name="ship_by" type="text" placeholder ="Input Nama Expedisi" value ="<?php echo $data_sel[LIST_DATA][0][0]->ship_by; ?>" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class ="form-group">
                                            <label class ="control-label col-md-3">No Resi *</label>
                                            <div class ="col-md-8">
                                                <input class="form-control" id="awb_no" name="awb_no" type="text" placeholder ="Input No Resi" value ="<?php echo $data_sel[LIST_DATA][0][0]->awb_no; ?>" readonly>
                                            </div>
                                        </div>
                                        <div class ="form-group">
                                            <label class ="control-label col-md-3">Tanggal Resi *</label>
                                            <div class ="col-md-3">
                                                <input date_type="date" readonly class="form-control" id="ship_date" name="ship_date" type="text" placeholder ="Input Tanggal Resi" value ="<?php echo (($data_sel[LIST_DATA][0][0]->ship_date != '0000-00-00') ? date("d-M-Y", strtotime($data_sel[LIST_DATA][0][0]->ship_date)) : '' ); ?>" style="background-color :#fff;">
                                            </div>
                                        </div>
                                        <div class ="form-group">
                                            <label class ="control-label col-md-3">Bukti Kirim *</label>
                                            <div class ="col-md-8">
                                                <?php echo (($data_sel[LIST_DATA][0][0]->ship_note_file != '') ? '<a href="' . base_url() . RESI_UPLOAD_IMAGE . $data_sel[LIST_DATA][0][0]->ship_note_file . ' " target="_blank">Download</a>' : 'Tidak Ada Bukti Kirim') ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                    }
                    ?>
                    <div class ="form-group">
                        <?php if ($data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
                            <div class ="col-md-6"><?php echo get_back_button(); ?></div><div class ="col-md-6">&nbsp;</div>
                        <?php } else { ?>
                            <div class ="col-md-6">
                                <?php if ($statusorder == "N") { ?><input type="hidden" name="btnSaveEdit" id="save_edit"><button type="button" onclick="cekdata()" style="width: 100%;" class="btn btn-google-plus">Simpan </button><?php } ?>
                            </div>
                            <div class ="col-md-6"><button type="button" name="btnCancel" id="cancel" onclick="backlist()" style="width: 100%;" class="btn btn-google-plus">Batal</button></div>
                        <?php } ?>
                    </div><!-- /.box-footer -->
                </section>
            </div>
        </form>
    </div>
    <div class="modal modal-danger" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Konfirmasi Pembatalan Order</h4>
                </div>
                <div class="modal-body">
                    <p>Semua produk yang diorder tidak tersedia.<br />
                        Apakah anda akan membatalkan order ini ?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" id="batal-order">Ya, batalkan order ini !</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <script type="text/javascript">
                                    <!--
        function backlist() {
                                        window.location = '<?php echo get_base_url() . $data_auth[FORM_URL]; ?>';
                                    }
                                    function cekdata() {
                                        status = "";
                                        $(".prdsts").each(function() {
                                            if ($(this).val() == "R") {
                                                status += "ada";

                                            }
                                        });
                                        if (status == "") {
                                            batalorder();
                                            return false;
                                        } else {
                                            $("form#form1").submit();
                                        }
                                    }
                                    ;
                                    function batalorder() {
                                        $('#myModal').modal();
                                    }
                                    $('#batal-order').on(
                                            'click',
                                            function(evt)
                                            {
                                                $("#order_status").val("X");
                                                $("form#form1").submit();
                                                return true;
                                            }
                                    );

                                    //-->
    </script>
    <?php
} elseif ($data_auth[FORM_ACTION] == ACTION_ADDITIONAL) {
    switch ($data_sel[LIST_DATA][0]->tipe) {
        case "bukti_kirim":
//	    die(print_r($data_sel[LIST_DATA]));
            ?>
            <link href="<?php echo get_css_url(); ?>daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
            <script type="text/javascript" src="<?php echo get_js_url(); ?>moment.min.js" ></script>
            <script type="text/javascript" src="<?php echo get_js_url(); ?>daterangepicker.js" ></script>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">UPLOAD BUKTI KIRIM</h3>
                </div>
                <form class="form-horizontal" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>" enctype="multipart/form-data" name="frmMain" id="frmMain" onsubmit ="return validate_form();">
                    <div class="box-body">
                        <section class="col-md-12">
                            <?php echo get_csrf_merchant_token(); ?>
                            <input class="form-control"  name ="seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->seq; ?>">
                            <input type="hidden" name="btnAdditional" value="act_s_adt">
                            <input type="hidden" name="tipe" value="save_resi">
                            <div class="row">
                                <div class="col-sm-4">
                                    Nomor Order : <span class="label label-primary"><strong><?php echo $data_list[LIST_RECORD][0]->order_no; ?></strong></span><br />
                                    Tanggal Order : <?php echo date("d-M-Y", strtotime($data_list[LIST_RECORD][0]->order_date)); ?>
                                </div>
                                <div class="col-sm-4">
                                    Penerima<br />
                                    <strong><?php echo $data_list[LIST_RECORD][0]->receiver_name; ?></strong><br />
                                    <?php echo $data_list[LIST_RECORD][0]->receiver_address; ?><br />
                                    <?php echo $data_sel[LIST_DATA][1][0]->d_name; ?>, <?php echo $data_sel[LIST_DATA][1][0]->c_name; ?><br />
                                    <?php echo $data_sel[LIST_DATA][1][0]->p_name; ?>, Kode Pos <?php echo $data_list[LIST_RECORD][0]->receiver_zip_code; ?><br />
                                    Nomor Telp. : <?php echo $data_list[LIST_RECORD][0]->receiver_phone_no; ?><br />
                                    Pesan :<br />
                                    <?php echo $data_list[LIST_RECORD][0]->member_notes; ?>
                                </div>
                                <div class="col-sm-4">
                                    Status Order : <span class="label label-info"><strong><?php echo status($data_list[LIST_RECORD][0]->order_status, STATUS_ORDER); ?></strong></span>
                                    <br />
                                    <p>Catatan untuk ekspedisi</p>
                                    <?php echo $data_list[LIST_RECORD][0]->ship_notes; ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class ="form-group">
                                        <label class ="control-label col-md-3">Expedisi *</label>
                                        <div class ="col-md-9">
                                            <select class="form-control" name="ship_by_exp_seq" id="ship_by_exp_seq" onchange="cekship(this.value)" validate="required[]">
                                                <?php
                                                if (isset($exp_name)) {
                                                    foreach ($exp_name as $each) {
                                                        ?>
                                                        <option value="<?php echo $each->seq; ?>"><?php echo $each->name; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                            <div id="merchant_ship" style="display:none;">
                                                <input class="form-control" id="ship_by" name="ship_by" type="text" placeholder ="Input Nama Expedisi" value ="<?php echo $data_list[LIST_RECORD][0]->ship_by; ?>" maxlength="50">
                                            </div>
                                        </div>
                                    </div>
                                    <div class ="form-group">
                                        <label class ="control-label col-md-3">No Resi *</label>
                                        <div class ="col-md-9">
                                            <input class="form-control" id="awb_no" name="awb_no" type="text" placeholder ="Input No Resi" value ="<?php echo $data_list[LIST_RECORD][0]->awb_no; ?>" required validate="required[]" maxlength="100">
                                        </div>
                                    </div>
                                    <div class ="form-group">
                                        <label class ="control-label col-md-3">Tanggal Resi *</label>
                                        <div class ="col-md-3">
                                            <input date_type="date" readonly class="form-control" id="ship_date" name="ship_date" type="text" placeholder ="Input Tanggal Resi" value ="<?php echo (($data_list[LIST_RECORD][0]->ship_date != '0000-00-00') ? date("d-M-Y", strtotime($data_list[LIST_RECORD][0]->ship_date)) : date("d-M-Y") ); ?>" required validate="required[]" style="background-color :#fff;">
                                        </div>
                                    </div>
                                    <div class ="form-group">
                                        <label class ="control-label col-md-3">Bukti Kirim *</label>
                                        <div class ="col-md-9">
                                            <input type="hidden" name="ofile" value="<?php echo $data_list[LIST_RECORD][0]->ship_note_file; ?>">
                                            <input type="file" id="ifile" name="ifile" onchange ="previewimg(this)" />
                                            <img id="ship_note_file" name="ship_note_file" src="<?php echo (($data_list[LIST_RECORD][0]->ship_note_file != '') ? base_url() . RESI_UPLOAD_IMAGE . $data_list[LIST_RECORD][0]->ship_note_file : base_url() . IMG_BLANK_100) ?>" height="100" alt="Bukti Kirim" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class ="form-group">
                                <?php if ($data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
                                    <div class ="col-md-6"><?php echo get_back_button(); ?></div>
                                <?php } else {
                                    ?>
                                    <div class ="col-md-6">
                                        <?php
                                        if ($data_list[LIST_RECORD][0]->order_status == READY_STATUS_CODE || $data_list[LIST_RECORD][0]->order_status == PROCESS_STATUS_CODE) {
                                            echo '<input type="submit" value="Simpan" id="btnsaveresi" class="btn btn-google-plus" style="width:100%">'; //get_save_add_button();
                                        }
                                        if ($data_list[LIST_RECORD][0]->order_status == 'N')
                                            echo '<div class="btn btn-danger" style="width:100%;">Data harus di Print dahulu</div>';
                                        ?>
                                    </div>
                                    <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                                <?php } ?>
                            </div><!-- /.box-footer -->
                        </section>
                    </div>
                </form>
            </div>
            <script type="text/javascript">
                                    <!--
                function cekship(expedisi) {
                                        if (expedisi == 0) {
                                            $("#merchant_ship").show();
                                        } else {
                                            $("#merchant_ship").hide();
                                        }
                                    }

                                    function previewimg(thisval) {
                                        if (thisval.files && thisval.files[0]) {
                                            var reader = new FileReader();
                                            reader.onload = function(e) {
                                                $('#ship_note_file').attr('src', e.target.result).height(100);
                                            }
                                            reader.readAsDataURL(thisval.files[0]);
                                        }
                                    }
                                    $('input[date_type="date"]').daterangepicker({
                                        format: 'DD-MMM-YYYY',
                                        singleDatePicker: true,
                                        showDropdowns: true
                                    });
                                    $("#ship_by_exp_seq").val('<?php echo $data_list[LIST_RECORD][0]->ship_by_exp_seq; ?>');
                                    cekship('<?php echo $data_list[LIST_RECORD][0]->ship_by_exp_seq; ?>');
                                    //-->
            </script>

            <?php
            break;
    }
} else {
    $tablesort = 'desc';
    ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <?php require_once get_include_page_list_merchant_content_header(); ?>
            <form method="post" action="" target="_blank" id="frmprint" name="frmprint"><input type="hidden" name="btnAdditional" value="act_s_adt">
                <input type="hidden" id="tipe" name="tipe" value="docprint"><input type="hidden" id="orderid" name="orderid" value="">
                <div class="row">
                    <div class="col-xs-4">
                        <div class="input-group input-group-sm">
                            <span class="input-group-addon">Pilih format</span>
                            <select class="form-control" id="format_type" name="format_type">
                                <option value=""></option>
                                <option value="A1">A4 (Faktur +Resi)</option>
                                <option value="A2">A5 (Faktur +Resi)</option>
                                <option value="A3">A5 (Faktur)</option>
                                <!-- <option value="A4">Stiker Resi</option>-->
                            </select>
                            <span class="input-group-btn">
                                <button class="btn btn-info btn-flat" type="button" id="btnprint" name="btnprint" onclick="docprint()"><i class="fa fa-fw fa-print"></i> Cetak</button>
                            </span>
                        </div>
                    </div>
                </div>
                <br />
                <table id="tbl" class="display table table-bordered table-striped" url="<?php echo get_base_url() . $data_auth[FORM_URL] ?>" width="100%">
                    <thead>
                        <tr>
                            <th column="order_date"> Tanggal Order</th>
                            <th column="order_no"> No. Order</th>
                            <th column="total_merchant"> Total</th>
                            <th column="order_status"> Status Order</th>
                            <th column="awb_no"> No. Resi</th>
                            <th column="member_notes"> Pesan Member</th>
                            <th column="districtname"> Kota Tujuan</th>
                            <th column="print_c">Cetak</th>
                        </tr>
                    </thead>
                </table>
        </div>
    </div></form>
    <script type="text/javascript">
                                    <!--
        var url = "<?php echo get_base_url() . $data_auth[FORM_URL]; ?>";
                                    function cekresi(seq) {
                                        $.ajax({
                                            url: url,
                                            data: {btnAdditional: "act_s_adt", idh: seq, tipe: "prosesresi"
                                            },
                                            type: "POST",
                                            success: function(response) {
                                                if (isSessionExpired(response)) {
                                                    response_object = json_decode(response);
                                                    url = response_object.url;
                                                    location.href = url;
                                                } else {
                                                    alert(response);
                                                }
                                            },
                                            error: function(request, error) {
                                                alert(error_response(request.status));
                                            }
                                        });
                                    }

                                    function bukti_kirim(seq) {
                                        $("#tipe").val("bukti_kirim");
                                        $("#orderid").val(seq);
                                        $("form#frmprint").attr('target', '_self').submit();
                                    }

                                    function docprint() {

                                        status = "";
                                        $("input:checkbox[class=prntorder]").each(function() {
                                            if ($(this).is(":checked"))
                                                status += "ada";
                                        });
                                        if ($("#format_type").val() == '')
                                            status = "";
                                        if (status == "") {
                                            alert("Pilih minimal satu order untuk dicetak !!!");
                                            return false;
                                        } else {
                                            $("form#frmprint").submit();
                                        }
                                    }
                                    //-->
    </script>

    <?php
}
require_once get_include_page_list_merchant_content_footer();
require_once get_include_content_merchant_bottom_page_navigation();
?>