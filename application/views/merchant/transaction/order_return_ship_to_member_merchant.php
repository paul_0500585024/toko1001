<?php
require_once VIEW_BASE_MERCHANT;
require_once get_include_content_merchant_top_page_navigation();
?>
<link rel="stylesheet" type="text/css" href="<?php echo get_css_url() ?>daterangepicker-bs3.css" />
<script type="text/javascript" src="<?php echo get_js_url() ?>moment.min.js"></script>
<script type="text/javascript" src="<?php echo get_js_url() ?>daterangepicker.js"></script>

<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
    </div>
    <div class="box-body">                    
        <?php /* echo "<pre>";print_r($data_head);echo "</pre>" */; ?>
        <section class="col-md-12 col-lg-12">
            <form class ="form-horizontal" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>" name="frmMain" id="frmMain">                    
                <div class="row">
                    <?php echo get_csrf_merchant_token(); ?>
                    <input type="hidden" name="return_no" class="form-control" value="<?php echo $data_head[LIST_DATA][0]->return_no; ?>">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">No. Pengembalian</label>
                            <div class="col-md-8"><input type="text" class="form-control" value="<?php echo $data_head[LIST_DATA][0]->return_no; ?>" readonly></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Tgl. Pengajuan</label>
                            <div class="col-md-8"><input type="text" class="form-control" value="<?php echo c_date($data_head[LIST_DATA][0]->created_date); ?>" readonly></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Tgl. Terima</label>
                            <div class="col-md-8"><input type="text" class="form-control" value="<?php echo c_date($data_head[LIST_DATA][0]->merchant_received_date); ?>" readonly></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Tgl. Kirim</label>
                            <div class="col-md-8">
                                <input validate ="required[]" class="form-control" name="tgl_kirim"  type="input" readonly value="<?php
                                if (!empty($data_head[LIST_DATA][0]->ship_to_member_date)) {
                                    if ($data_head[LIST_DATA][0]->ship_to_member_date == "0000-00-00 00:00:00") {
                                        echo c_date(date('Y-m-d'));
                                    } else {
                                        echo c_date($data_head[LIST_DATA][0]->ship_to_member_date);
                                    }
                                } else {
                                    echo c_date(date('Y-m-d'));
                                }
                                ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Status Pengembalian</label>
                            <div class="col-md-8">
                                <select class="form-control select2" name="return_status" disabled>                                    
                                    <?php
                                    echo combostatus(json_decode(STATUS_RETURN_BY_MERCHANT), $data_head[LIST_DATA][0]->return_status);
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Ekspedisi</label>
                            <div class="col-md-8"><?php require_once get_component_url() . "dropdown/com_drop_expedition_return.php" ?></div>
                        </div>

                    </div>
                    <div class="col-md-6">                        

                        <div class="form-group">
                            <label class="control-label col-md-4">No. Resi</label>
                            <div class="col-md-8"><input type="text" class="form-control" name="no_resi" validate ="required[]" value="<?php echo (isset($data_head[LIST_DATA]) ? $data_head[LIST_DATA][0]->awb_merchant_no : ""); ?>"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Komentar</label>
                            <div class="col-md-8"><textarea class="form-control"name="merchant_comment" readonly style="height: 150px;  max-height: 150px; resize: none;"><?php echo (isset($data_head[LIST_DATA]) ? $data_head[LIST_DATA][0]->review_merchant : ""); ?></textarea></div>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="col-md-6">
                                <?php
                                if ($data_head[LIST_DATA][0]->shipment_status == RECEIVED_BY_MEMBER) {
                                    
                                } else {
                                    echo get_save_edit_button();
                                }
                                ?>

                            </div>
                            <div class="col-md-6">
                                <a href="<?php echo get_base_url(); ?>merchant/transaction/return_merchant/" class="btn btn-google-plus" style="width: 100%;">&nbsp;Kembali</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>
    <script  type="text/javascript">
                $('input[name="tgl_kirim"]').daterangepicker({
                    format: 'DD-MMM-YYYY',
                    singleDatePicker: true,
                    showDropdowns: true,
                    startDate: "<?php echo date('d-M-Y') ?>"
                });
    </script>
<?php
require_once get_include_page_list_merchant_content_footer();
require_once get_include_content_merchant_bottom_page_navigation();
?>