<?php
require_once VIEW_BASE_MERCHANT;
require_once get_include_content_merchant_top_page_navigation();
?>

<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
    </div>
    <div class="box-body">
        <form class ="form-horizontal" id="frmSearch" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
            <div class="box-default col-md-12">
                <div class="form-group col-md-12">
                    <label class ="control-label col-md-2">No. Order</label>
                    <div class="input-group col-md-3">
                        <input class="form-control" value="<?php echo ($data_sel[LIST_DATA][0]->order_no) ?>" readonly>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <label class ="control-label col-md-2">Tanggal Order</label>
                    <div class="input-group col-md-3">
                        <input class="form-control" value="<?php echo date('d-M-Y', strtotime($data_sel[LIST_DATA][0]->order_date)) ?>" readonly>
                    </div>
                </div>
            </div>
            <?php require_once get_include_page_list_merchant_content_header(); ?>
            <br><br><br><br><br>
            <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th column="product_name">Produk</th>
                        <th column="variant_name">Varian</th>                       
                        <th column="p_weight_kg">Berat</th>
                        <th column="p_length_cm">Panjang</th>
                        <th column="p_width_cm">Lebar</th>
                        <th column="p_height_cm">Tinggi</th>
                        <th column="qty">Qty</th>
                        <th column="sell_price">Harga</th>
                        <th column="img_src">Gambar</th>
                    </tr>
                </thead>
            </table>
        </form>
    </div>
</div>

<?php
require_once get_include_page_list_merchant_content_footer();
require_once get_include_content_merchant_bottom_page_navigation();
?>