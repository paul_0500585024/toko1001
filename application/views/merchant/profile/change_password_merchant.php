<?php
require_once VIEW_BASE_MERCHANT;
require_once get_include_content_merchant_top_page_navigation();
?>

<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo get_title_edit($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
    </div>
    <form class ="form-horizontal" id="frmMain" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
        <div class="box-body">
            <section class="col-md-8">
                <?php echo get_csrf_merchant_token(); ?>
                <div class ="form-group">       
                    <label class ="control-label col-md-4">Password Lama *</label>
                    <div class ="col-md-8">
                        <input class="form-control" name ="old_password" type="password" value ="<?php echo (($data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) ? DEFAULT_PASSWORD : "") ?>" autocomplete="off">
                    </div>
                </div>
                <div class ="form-group">       
                    <label class ="control-label col-md-4">Password Baru *</label>
                    <div class ="col-md-8">
                        <input class="form-control" id="newpass" validate="password[]" name ="new_password" type="password">
                    </div>
                </div>
                <div class ="form-group">       
                    <label class ="control-label col-md-4">Konfirmasi Password *</label>
                    <div class ="col-md-8">
                        <input class="form-control" validate ="password_confirm[]" name="password_confirm" type="password">
                    </div>
                </div>
                <div class ="form-group">
                    <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                    <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                </div>
            </section>
        </div>
    </form>
</div>

<script>
        var isValid = $('#frmMain').validate({
            ignore: 'input[type=hidden]',
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            }, unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else if (element.hasClass('select2')) {
                    error.insertAfter(element.next('span'));
                } else {
                    error.insertAfter(element);
                }
            }
        });

        $('input[type="text"], input[type="file"], input[type="password"] ').each(function() {
            var validate = $(this).attr('validate');
            switch (validate) {
                case  "password_confirm[]" :
                    $(this).rules('add', {
                        required: true,
                        equalTo: "#newpass",
                        minlength: 8,
                        maxlength: 20});
                    break;
                case  "password[]" :
                    $(this).rules('add', {
                        required: true,
                        minlength: 8,
                        maxlength: 20});
                    break;
                case  "required[]" :
                    $(this).rules('add', {
                        required: true});
                    break;
            }
            ;
        });

</script>

<?php
require_once get_include_page_list_merchant_content_footer();
require_once get_include_content_merchant_bottom_page_navigation();
?>

