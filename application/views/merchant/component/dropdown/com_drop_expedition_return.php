<select id="expedition_seq" class="form-control select2" style='width : 100%' name="expedition_seq" validate ="required[]">
    <option value="">-- Pilih --</option>
    
    <?php
    
    if (isset($exp_name)) {
	foreach ($exp_name as $each) {
	    ?>
	    <option value="<?php echo $each->seq; ?>" <?php echo (isset($data_head[LIST_DATA]) && $each->seq== $data_head[LIST_DATA][0]->exp_seq_to_member) ? "selected" : "" ?>><?php echo $each->name; ?></option>
	    <?php
	}
    }
    ?>
</select>