<link href="<?php echo get_css_url(); ?>daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo get_js_url(); ?>daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>moment.min.js"></script>

<input class="form-control pull-right" type="text" name ="date_between" id="reservation" readonly="">

<script type="text/javascript">

    $('#reservation').daterangepicker({
        format: 'DD-MMM-YYYY'
    });

</script>