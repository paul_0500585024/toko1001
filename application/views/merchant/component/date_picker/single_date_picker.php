<input class="form-control" id="datepicker" validate ="required[]" name ="date" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? cdate($data_sel[LIST_DATA][0]->date) : "" ); ?>" readonly="">

    <script type="text/javascript">
        $('input[name="date"]').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            format: 'DD-MMM-YYYY'
        });
    </script>

