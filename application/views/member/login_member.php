<?php
require_once VIEW_BASE_MEMBER;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">

<html>
    <head>
        <title>Toko1001</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="<?php echo get_img_url(); ?>home/icon/pavicon.png"/>
        <link href="<?php echo get_css_url(); ?>bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_css_url(); ?>member/toko1001_member.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_css_url(); ?>font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="<?php echo get_css_url(); ?>style.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo get_css_url(); ?>AdminLTE.min.css"/>
        <script type="text/javascript" src="<?php echo get_js_url(); ?>jQuery-2.1.4.min.js" ></script>
        <script type="text/javascript" src="<?php echo get_js_url(); ?>jquery.validate.js" ></script>
        

        <script type="text/javascript">
            (function() {
                var po = document.createElement('script');
                po.type = 'text/javascript';
                po.async = true;
                po.src = 'https://apis.google.com/js/client.js?onload=onLoadCallback';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(po, s);
            })();

            (function(d) {
                var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
                if (d.getElementById(id)) {
                    return;
                }
                js = d.createElement('script');
                js.id = id;
                js.async = true;
                js.src = "//connect.facebook.net/en_US/all.js";
                ref.parentNode.insertBefore(js, ref);
            }(document));
        </script>
    </head>
    <body class="login-page">
        <div class="login-box">
            <div class="login-box-body" style="border-radius: 16px">
                <div class="form-group text-center">
                    <a href="<?php echo base_url() ?>"><img src='<?php echo get_img_url() ?>logo_184x41.png'></img></a>
                </div>
                <div id ="err_msg">
                    <?php if ($data_err[ERROR] === true) { ?>
                        <p  class="login-box-msg text-red"><?php echo $data_err[ERROR_MESSAGE][0] ?> </p>
                    <?php } else { ?>
                        <!--<p class="login-box-msg">Sign in to start your session</p>-->
                    <?php } ?>
                </div>
                <p class="text-right" style="margin: 0px 0px 0px; color: #203F49">Member Panel</p>
                <form id="frmlogin" method = "post" action= "<?php echo get_base_url() ?>member/login/sign_in<?php echo isset($data_auth[FORM_AUTH][FORM_URL]) ? $data_auth[FORM_AUTH][FORM_URL] : ""; ?>">
                    <div class="form-group has-feedback">
                        <input name="type" type="hidden" value="standard_login"/>
                        <input id="txtUserName" name="email" 
                               type="text" class="form-control" placeholder="User Id" 
                               style="background-color: rgba(250, 255, 189, 0.8); color: #000"
                               value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA]->user_id : "") ?>"/> 
                        <span class="glyphicon glyphicon-envelope form-control-feedback" ></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input id="txtPassword" name="password" 
                               type="password" class="form-control" placeholder="Password" 
                               style="background-color: rgba(250, 255, 189, 0.8); color: #000"
                               value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA]->password : "") ?>"/>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        <a href="<?php echo get_base_url() ?>member/forgot_password" 
                           class="btn btn-flat btn-block" style="color: rgba(7, 64, 81, 1); text-align: right">
                            Lupa Password ?
                        </a>
                        <button name="submit" type="submit" class="btn btn-block btn-google-plus">
                            <i class="fa fa-sign-in"></i>&nbsp; Masuk
                        </button>
                        <a href="<?php echo base_url() . "registration" ?> "><button name="submit" type="button" class="btn  btn-block btn-google-plus " style="background-color: #CC5C1D;margin-top: 10px;">
                                <i class="fa fa-user white"></i>&nbsp; &nbsp;Daftar
                            </button></a>
                    </div>
                </form>
                <div class="row">
                    <div class="col-lg-6 col-xs-6 col-md-6">
                        <a id="facebook" class="btn btn-block btn-facebook" 
                           onclick="fb_login();" type="submit"><i class="fa fa-facebook"></i>&nbsp; Facebook
                        </a>
                    </div>
                    <div class="col-lg-6 col-xs-6 col-md-6">
                        <a class="btn btn-block btn-facebook" style="background-color: #D73D32"
                           onclick="gp_login();" type="submit"><i class="fa fa-google-plus"></i>&nbsp; Google+
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">

            function gp_login()
            {
                var myParams = {
                    'clientid': '1005160495089-fn8j24mmkdt1aemgcto51dv55sohq0he.apps.googleusercontent.com',
                    'cookiepolicy': 'single_host_origin',
                    'callback': 'loginCallback',
                    'approvalprompt': 'force',
                    'scope': 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email'
                };
                gapi.auth.signIn(myParams);
            }

            function loginCallback(result)
            {
                if (result['status']['signed_in'])
                {
                    var request = gapi.client.plus.people.get(
                            {
                                'userId': 'me'
                            });
                    request.execute(function(resp)
                    {
                        var email = '';
                        if (resp['emails'])
                        {
                            for (i = 0; i < resp['emails'].length; i++)
                            {
                                if (resp['emails'][i]['type'] == 'account')
                                {
                                    email = resp['emails'][i]['value'];
                                }
                            }
                        }

                        var base_url = '<?php echo base_url(); ?>'
                        $.ajax({
                            url: "<?php echo base_url() ?>member/login_fb",
                            type: "post",
                            data: {
                                "name": resp['displayName'],
                                "email": email,
                                "gender": resp['gender'],
                                "type": "google_login"
                            },
                            success: function(data) {
                                window.location = "<?php echo base_url(); ?>";
                            },
                            error: function(request, error) {
                                alert(error_response(request.status));
                            }
                        });
                    });

                }

            }
            function onLoadCallback()
            {
                gapi.client.load('plus', 'v1', function() {
                });
            }


            $('#frmlogin').validate({
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true
//                                        minlength: 8
                    }
                }
            });
            $.validator.messages.required = "Data wajib diisi !";
            $.validator.messages.maxlength = "Harap diisi tidak lebih dari {0} karakter !";
            $.validator.messages.minlength = "Harap diisi dengan minimal {0} karakter !";
            $.validator.messages.email = "Harap diisi dengan alamat email dengan benar !";

            window.fbAsyncInit = function() {
                FB.init({
                    appId: '672193332925149',
                    status: true,
                    cookie: true,
                    xfbml: true
                });

                FB.Event.subscribe('auth.authResponseChange', function(response)
                {
                    if (response.status === 'connected')
                    {
                        document.getElementById("message").innerHTML += "<br>Connected to Facebook";
                    } else if (response.status === 'not_authorized')
                    {
                        document.getElementById("message").innerHTML += "<br>Failed to Connect";
                    } else
                    {
                        document.getElementById("message").innerHTML += "<br>Logged Out";
                    }
                });

            };

            function fb_login() {
                FB.login(function(response) {
                    if (response.authResponse)
                    {
                        _i();
                    } else
                    {
                        console.log('Batal masuk Facebook, tidak ada otoritas !');
                    }
                }, {scope: 'email'});

            }

            function _i() {
                FB.api('/me?fields=name,email,gender,birthday', function(data) {

                    var base_url = '<?php echo base_url(); ?>'

                    $.ajax({
                        url: "<?php echo base_url() ?>member/login_fb",
                        type: "post",
                        dataType: "json",
                        data: {
                            "name": data.name,
                            "email": data.email,
                            "gender": data.gender,
                            "birthday": data.birthday,
                            "type": "facebook_login"
                        },
                        success: function(data) {
                            if (data.error == false) {
                                window.location = "<?php echo base_url(); ?>";
                            } else {
                                $('#err_msg').html("<p class='login-box-msg text-red'><?php echo ERROR_INVALID_PASSWORD_AND_EMAIL ?></p>");
                            }
                        },
                        error: function(request, error) {
                            alert(error_response(request.status));
                        }
                    });
                })
            }
        </script>
    </body>
</html>