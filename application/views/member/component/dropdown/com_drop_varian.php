<select class="form-control select2" style='width : 100%' name="variant_seq" id="variant_seq">
    <option value="0"> -- Pilih --</option>
    <?php
    if (isset($variant)) {
	foreach ($variant as $each) {
	    ?>
	    <option value="<?php echo $each->seq; ?>" <?php echo (isset($data_sel[LIST_DATA]) && $each->seq == $data_sel[LIST_DATA][0]->variant_seq) ? "selected" : "" ?>><?php echo $each->display_name; ?></option>
	<?php
	}
    }
    ?>
</select>
<script text="javascript">
    $('.select2').select2();
</script>