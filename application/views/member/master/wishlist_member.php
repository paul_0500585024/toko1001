<?php
require_once VIEW_BASE_HOME;
?>
<div class="hidden-xs hidden-lg " style="margin-top:65px;">&nbsp;</div>
<!--    for lg md-->
<div class="hidden-xs hidden-sm hidden-md" style="margin-top:25px;">&nbsp;</div>
<div class="container">
    <div style="margin-top:0px;"><h2 class="page-header">Wishlist</h2></div>
</div>
<?php
$html = display_product($product);
if ($html == '') {
    echo '<div class="container"><div>Tidak ada produk dalam daftar wishlist anda.</div></div>';
} else {
    echo $html;
}
?>
<div class="container" style="margin-top:21px;"><?php echo $this->pagination->create_links(); ?></div>


