<?php
require_once VIEW_BASE_HOME;
?>

<div class="row hidden-xs " style="margin-top:55px;">&nbsp;</div>
<div class="row hidden-xs hidden-lg" style="margin-top:10px;">&nbsp;</div>
<div class="container">
    <?php if (isset($data_err) && $data_err[ERROR] === true) { ?>
        <div class="alert alert-error-home">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $data_err[ERROR_MESSAGE][0] ?>
        </div>
    <?php } elseif (isset($data_suc) && ($data_suc[SUCCESS] === true)) { ?>
        <div class="alert alert-success-home">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo (isset($data_suc[SUCCESS_MESSAGE][0]) ? $data_suc[SUCCESS_MESSAGE][0] : SAVE_DATA_SUCCESS); ?>
        </div>           
    <?php } ?>
    <div class="panel panel-primary">
        <div class="panel-heading">
            Forgot Password Member
        </div>
        <div class="panel-body">
            <center><div id="success"></div></center>
            <form class="form-horizontal" id="frmMain" action="<?php echo get_base_url() . "member/forgot_password"; ?>" method="post">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="email" placeholder="Email" name="email" validate="email[]">
                    </div>
                    <div class="col-sm-2">
                        <div id="usr_verify" class="verify"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Captcha</label>
                    <div class="col-sm-4">
                        <?php echo $image; ?>
                    </div>
                </div>

                <div class="form-group">   
                    <label class="col-sm-2 control-label"></label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="security_code" placeholder="Masukkan captcha" validate="required[]">
                    </div>
                    <div class="col-sm-2">
                        <div id="captcha_verify" class="verify"></div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2">
                        <div class="col-sm-4">
                            <button type="submit" name="btnSave" value="true" class="btn btn-google-plus btn-flat" style="width:100%;"><i class="fa fa-save"></i>&nbsp; Lupa Password</button>
                        </div>
                    </div>  
                </div>
            </form>
        </div>
    </div>
</div>


