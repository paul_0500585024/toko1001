<?php
require_once VIEW_BASE_HOME;
require_once VIEW_BASE_HOME;
?>
<div class='container'>
    <br><br><br><br>
    <div class="panel panel-primary">
        <div class="panel-heading">Verifikasi Password</div>
        <div class="panel-body">
            <?php if (isset($data_err) && $data_err[ERROR] == true) { ?>
                <center><h2><span class="label label-danger"><?php echo $data_err[ERROR_MESSAGE][0]; ?></span></h2></center>
            <?php } elseif (isset($data_suc) && $data_suc[SUCCESS] == true) { ?>
                <center><h2><span class="label label-success"><?php echo $data_suc[SUCCESS_MESSAGE][0]; ?></span></h2></center>
            <?php } ?>
        </div>
    </div>
</div>
