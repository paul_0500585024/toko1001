<?php
require_once VIEW_BASE_HOME;
?>

<div class ="modal fade col-md-12" id="address_modal" role="dialog">
    <div class="modal-dialog">
        <?php if ($data_address === "0") { ?>
            <div class="modal-header col-md-offset-2 col-md-10" style="background-color: #F2F2F2; padding: 2px">
                <button type="button" class="close" data-dismiss="modal" style="margin-top: 5px; margin-right: 15px"> &times;</button>
                <h3 class="modal-title" style="margin: 10px; text-align: center; font-weight: 600">TAMBAH ALAMAT</h3>
            </div>
            <div class="modal-content col-md-offset-2 col-md-10">
                <form class ="form-horizontal" id='frmAddress' method ="post" 
                      action="<?php echo get_base_url() ?>member/address/save_add">
                      <?php } else { ?>
                    <div class="modal-header col-md-offset-2 col-md-10" style="background-color: #F2F2F2; padding: 2px">
                        <button type="button" class="close" data-dismiss="modal" style="margin-top: 5px; margin-right: 15px"> &times;</button>
                        <h3 class="modal-title" style="margin: 10px; text-align: center; font-weight: 600">UBAH ALAMAT</h3>
                    </div>
                    <div class="modal-content col-md-offset-2 col-md-10">
                        <form class ="form-horizontal" id='frmAddress' method ="post" 
                              action="<?php echo get_base_url() ?>member/address/save_update">
                              <?php } ?>
                        <div class="modal-body">
                            <div class="row">
                                <input name="address_seq" type="hidden" value ="<?php echo $address_seq ?>">
                                <div class ="form-group">
                                    <label class ="control-label col-md-4">Alias *</label>
                                    <div class ="col-md-8">
                                        <input class="form-control" name="alias" type="text" maxlength="25" value ="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->alias) : "")?>" >
                                    </div>
                                </div>
                                <div class ="form-group">
                                    <label class ="control-label col-md-4">Penerima *</label>
                                    <div class ="col-md-8">
                                        <input class="form-control" name="pic_name" type="text"
                                               value ="<?php
                                               echo (isset($data_sel[LIST_DATA]) ?
                                                       get_display_value($data_sel[LIST_DATA][0]->pic_name) : "")
                                               ?>">
                                    </div>
                                </div>
                                <div class ="form-group">
                                    <label class ="control-label col-md-4">Alamat *</label>
                                    <div class ="col-md-8">
                                        <input class="form-control" validate ="required[]" name="address" type="text"
                                               value ="<?php
                                               echo (isset($data_sel[LIST_DATA]) ?
                                                       get_display_value($data_sel[LIST_DATA][0]->address) : "")
                                               ?>">
                                    </div>
                                </div>
                                <div class ="form-group">
                                    <label class ="control-label col-md-4">No. Telp</label>
                                    <div class ="col-md-8">
                                        <input class="form-control" validate ="required[]" name="phone_no" type="text"
                                               data-inputmask="&quot;mask&quot;: &quot;9999 9999 9999 999&quot;" data-mask=""
                                               value ="<?php
                                               echo (isset($data_sel[LIST_DATA]) ?
                                                       get_display_value($data_sel[LIST_DATA][0]->phone_no) : "")
                                               ?>">
                                    </div>
                                </div>
                                <div class ="form-group">
                                    <label class ="control-label col-md-4">Propinsi *</label>
                                    <div class ="col-md-8">
                                        <select id="drop_province" validate="required[]" class="form-control" 
                                                name="province_seq" onchange="change_province()">
                                            <option value="">-- Pilih --</option>
                                            <?php
                                            if (isset($province_name)) {
                                                foreach ($province_name as $each) {
                                                    ?>
                                                    <option value="<?php echo $each->seq; ?>"
                                                    <?php
                                                    echo (isset($data_sel[LIST_DATA]) && $each->seq == $data_sel[LIST_DATA][0]->province_seq) ?
                                                            "selected" : ""
                                                    ?>><?php echo get_display_value($each->name); ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                        </select>
                                    </div>
                                </div>
                                <div class ="form-group">
                                    <label class ="control-label col-md-4">Kota / Kabupaten *</label>
                                    <div class ="col-md-8">
                                        <select class="form-control" id ="drop_city" validate ="required[]" 
                                                name="city_seq" type="input" onchange="change_city()">
                                            <option value="">-- Pilih --</option>
                                            <?php
                                            if (isset($city_name)) {
                                                foreach ($city_name as $each) {
                                                    ?>
                                                    <option value="<?php echo $each->seq; ?>"
                                                    <?php
                                                    echo (isset($data_sel[LIST_DATA]) && $each->seq == $data_sel[LIST_DATA][0]->city_seq) ?
                                                            "selected" : ""
                                                    ?>><?php echo $each->name; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                        </select>
                                    </div>
                                </div>
                                <div class ="form-group">
                                    <label class ="control-label col-md-4">Kecamatan *</label>
                                    <div class ="col-md-8">
                                        <select id="drop_district" validate="required[]" class="form-control" name="district_seq">
                                            <option value="">-- Pilih --</option>
                                            <?php
                                            if (isset($district_name)) {
                                                foreach ($district_name as $each) {
                                                    ?>
                                                    <option value="<?php echo $each->seq; ?>" 
                                                    <?php
                                                    echo (isset($data_sel[LIST_DATA]) && $each->seq == $data_sel[LIST_DATA][0]->district_seq) ?
                                                            "selected" : ""
                                                    ?>><?php echo $each->name; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                        </select>
                                    </div>
                                </div>
                                <div class ="form-group">
                                    <label class ="control-label col-md-4">Kode Pos</label>
                                    <div class ="col-md-8">
                                        <input class="form-control" name="zip_code" type="text"
                                               value ="<?php
                                               echo (isset($data_sel[LIST_DATA]) ?
                                                       get_display_value($data_sel[LIST_DATA][0]->zip_code) : "")
                                               ?>">
                                    </div>
                                </div>
                                &nbsp;
                                <div class="form-group">
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <a type="button" class="btn btn-front btn-flat"
                                           data-dismiss="modal" style="background-color: #EC7115; width: 100%">
                                            <i class="fa fa-arrow-left"></i>&nbsp;Kembali</a>
                                    </div>
                                    <?php if ($data_address === "1") { ?>
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <button type="submit" name="btnSaveEdit" 
                                                    class="btn btn-front btn-flat" style="width: 100%">
                                                <i class="fa fa-save"></i>&nbsp;Simpan</button>
                                        </div>
                                    <?php } else if ($data_address === "0") { ?>
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <button type="submit" name="btnSaveAdd" 
                                                    class="btn btn-front btn-flat col-md-12" style="width: 100%">
                                                <i class="fa fa-save"></i>&nbsp;Simpan</button>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </form>

                    <script text="javascript">

                                                    function change_province() {
                                                        var province_seq = $('#drop_province').val();
                                                        $.ajax({
                                                            url: "<?php echo get_base_url() . "member/checkout" ?>",
                                                            type: "POST",
                                                            dataType: "json",
                                                            data: {
                                                                "type": "<?php echo TASK_PROVINCE_CHANGE ?>",
                                                                "province_seq": province_seq
                                                            },
                                                            success: function(data) {
                                                                if(isSessionExpired(data)){
                                                                    response_object = json_decode(data);
                                                                    url = response_object.url;
                                                                    location.href = url;
                                                                }else{                
                                                                    $('#drop_city option').remove();
                                                                    $("#drop_city").append("<option value=\"" + "\">-- Pilih --</option>");
                                                                    $('#drop_district option').remove();
                                                                    $(data).each(function(i) {
                                                                        $("#drop_city").append("<option value=\"" + data[i].seq + "\">" + data[i].name + "</option>")
                                                                    });
                                                                }
                                                            },
                                                            error: function (request, error) {
                                                                alert(error_response(request.status));
                                                            }
                                                        });
                                                    }

                                                    function change_city() {
                                                        var city_seq = $('#drop_city').val();
                                                        $.ajax({
                                                            url: "<?php echo get_base_url() . "member/checkout" ?>",
                                                            type: "POST",
                                                            dataType: "json",
                                                            data: {
                                                                "type": "<?php echo TASK_CITY_CHANGE ?>",
                                                                "city_seq": city_seq
                                                            },
                                                            success: function(data) {
                                                                if(isSessionExpired(data)){
                                                                    response_object = json_decode(data);
                                                                    url = response_object.url;
                                                                    location.href = url;
                                                                }else{                
                                                                    $('#drop_district option').remove();
                                                                    $("#drop_district").append("<option value=\"" + "\">-- Pilih --</option>");
                                                                    $(data).each(function(i) {
                                                                        $("#drop_district").append("<option value=\"" + data[i].seq + "\">" + data[i].name + "</option>")
                                                                    });
                                                                }
                                                            },
                                                            error: function (request, error) {
                                                                alert(error_response(request.status));
                                                            }                                                                    
                                                        });
                                                    }

                                                    $("[data-mask]").inputmask();
                                                    $('#frmAddress').validate({
                                                        highlight: function(element) {
                                                            $(element).closest('.form-group').addClass('has-error');
                                                        },
                                                        unhighlight: function(element) {
                                                            $(element).closest('.form-group').removeClass('has-error');
                                                        },
                                                        errorElement: 'span',
                                                        errorClass: 'help-block',
                                                        errorPlacement: function(error, element) {
                                                            if (element.parent('.input-group').length) {
                                                                error.insertAfter(element.parent());
                                                            } else {
                                                                error.insertAfter(element);
                                                            }
                                                        },
                                                        rules: {
                                                            alias: {
                                                                required: true,
                                                                maxlength: 10
                                                            },
                                                            pic_name: {
                                                                required: true,
                                                            },
                                                            address: {
                                                                required: true,
                                                            },
                                                            province_seq: {
                                                                required: true,
                                                            },
                                                            city_seq: {
                                                                required: true,
                                                            },
                                                            district_seq: {
                                                                required: true,
                                                            }
                                                        }
                                                    });

                    </script>
                </div>
        </div>
    </div>