<?php 
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*save as documentation*/
?>
<?php
require_once VIEW_BASE_HOME;
?>

<div class="row hidden-xs hidden-lg " style="margin-top:65px;">&nbsp;</div>
<div class="row hidden-xs hidden-sm hidden-md" style="margin-top:35px;">&nbsp;</div>
<div class="row">
    <div class="container">
        <div class="box box-default">
            <div class="box-header with-border" style="background-color: #216275">
                <h5 class="box-title" style="color: #fff">Detail Order</h5>
            </div>
            <div class="box-body">
                <section class="col-md-12">
                    <form class ="form-horizontal col-md-12">
                        <div class="form-group">
                            <div class="input-group col-md-5">
                                <label class ="control-label col-md-3" style="text-align: left">No. Order :</label>
                                <label class="control-label" style="text-align: left"><?php echo get_display_value($data_sel[LIST_DATA][0]->order_no) ?></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-md-5">
                                <label class ="control-label col-md-3" style="text-align: left">Alamat :</label>
                                <label class="control-label" style="text-align: left"><?php echo get_display_value($data_sel[LIST_DATA][0]->receiver_address) ?></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-md-5">
                                <label class ="control-label col-md-3" style="text-align: left">Penerima :</label>
                                <label class="control-label" style="text-align: left"><?php echo get_display_value($data_sel[LIST_DATA][0]->receiver_name) ?></label>
                            </div>
                        </div>
                    </form>
                    &nbsp;
                    <form class ="form-horizontal" id="frmSearch" method ="post" 
                          url="<?php echo base_url() . $data_auth[FORM_URL] ?>">
                        <table id="tbl" class="display table table-bordered table-striped" 
                               cellspacing="0" width="100%">
                            <thead>
                                <tr class="success">
                                    <th column="img_src" style="width: 72px">Produk</th>
                                    <th column="sell_price">Harga</th>
                                    <th column="qty">Qty</th>
                                    <th column="p_weight_kg">Berat</th>
                                    <th column="total_ship_charged">Ongkos Kirim</th>
                                    <th column="total_price">Total Harga</th>
                                    <th column="merchant_name">Merchant</th>
                                    <th column="order_status">Status Pengiriman</th>
                                    <th column="awb_no">No. Tracking / Resi</th>
                                    <th column="ship_date">Tgl. Pengiriman</th>
                                </tr>
                            </thead>
                        </table>
                    </form>
                    &nbsp;
                    <div class="form-group">
                        <a type="button" class="btn btn-front btn-flat col-md-2"  href="<?php echo base_url() ?>member" style="background-color: #EC7115"><i class="fa fa-arrow-left"></i>&nbsp;Kembali</a>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>

<script>
//------------------------------------------ frmSearch1 --------------------------------------------------------------------------------------------
    $(document).ready(function() {
        var url = $('#frmSearch').attr('url');
        var columns_detail = [];

        $('#tbl thead th').each(function() {
            var sName = $(this).attr("column");
            var bVisible = $(this).attr("visible");
            var url = $(this).attr("url");
            columns_detail.push({"sName": sName, "bVisible": bVisible, "mData": sName});

        });

        var table = $('#tbl').DataTable({
            "oLanguage": {
                    sProcessing: "<img src='<?php echo base_url().ASSET_IMG_HOME?>loading.gif' />"
            },
            "dom": '<"top"l>rt<"bottom"ip><"clear">',
            responsive: true,
            "processing": true,
            "serverSide": true,
            "aoColumns": columns_detail,
            "ajax": {
                "url": url,
                "type": 'POST',
                "dataType": 'JSON',
                "data": buildAjaxData,
                "error": handleAjaxError
            }
        })
        .on('xhr.dt', function ( e, settings, response, xhr ){
            response = JSON.stringify(response);
            if(isSessionExpired(response)){
                response_object = json_decode(response);
                url = response_object.url;
                location.href = url;
            }
        });

        function handleAjaxError(request, textStatus, error) {
            if (textStatus === 'timeout') {
                alert('The server took too long to send the data.');
            }
            else {
                alert(error_response(request.status));
                $('div#tbl_processing').html(response);
            }
        }

        function handleAjaxSuccess(response) {
            alert(response);
        }

        function buildAjaxData() {
            var settings = $("#tbl").dataTable().fnSettings();
            var search = [];
            search = $('#frmSearch').serializeArray();
            search.push(
                    {name: "draw", value: settings.iDraw},
            {name: "start", value: settings._iDisplayStart},
            {name: "length", value: settings._iDisplayLength},
            {name: "column", value: columns_detail[settings.aaSorting[0][0]].sName},
            {name: "order", value: settings.aaSorting[0][1]},
            {name: "btnSearch", value: "true"}
            );
            return search;
        }

        $('#tbl tbody').on('click', 'tr', function() {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });
    });

</script>