<?php
require_once VIEW_BASE_HOME;
?>

<div class="row hidden-xs hidden-lg " style="margin-top:65px;">&nbsp;</div>
<div class="row hidden-xs hidden-sm hidden-md" style="margin-top:35px;">&nbsp;</div>

<div class="container">
    <?php if (isset($data_auth)) { ?>
        <?php if ($data_err[ERROR] === true) { ?>
            <div class="alert" style="background-color: #ffebe8;border: 1px solid #dd3c10;color: #000000">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $data_err[ERROR_MESSAGE][0] ?>
            </div>
        <?php } ?>
    <?php } ?>
    <!--        <div class="box box-default">
                            <div class="box-header with-border" style="background-color: #216275">
                                <h5 class="box-title" style="color: #fff">Pembayaran Ulang</h5>
                            </div>
                <div class="box-body">
                    <section class="col-md-12">-->
    <!--                    <form class ="form-horizontal col-md-12" action="<?php // echo get_base_url() . $data_auth[FORM_URL]                                                      ?>">
                            <div class="form-group">
                                <div class="input-group col-md-5">
                                    <label class ="control-label col-md-3" style="text-align: left">No. Order :</label>
                                    <label class="control-label" style="text-align: left"><?php // echo (isset($data_sel[LIST_DATA][0][0]) ? $data_sel[LIST_DATA][0][0]->order_no : "" );                                                      ?></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group col-md-5">
                                    <label class ="control-label col-md-3" style="text-align: left">Tgl. Order :</label>
                                    <label class="control-label" style="text-align: left"><?php // echo (isset($data_sel[LIST_DATA][0][0]) ? cdate($data_sel[LIST_DATA][0][0]->order_date) : "");                                                      ?></label>
                                </div>
                            </div>
                        </form>-->
    <!--                    <div class="box-body">
                            <form class ="form-horizontal" action="<?php // echo get_base_url() . $data_auth[FORM_URL]                                                      ?>">
                                <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                                    <thead>
                                        <tr class="success">
                                            <th columns ="display_name">Nama</th>
                                            <th>Jenis</th>
                                            <th># Qty</th>
                                            <th>Harga</th>
                                            <th>Berat (kg)</th>
                                            <th>Ongkos Kirim</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
    <?php // if (isset($data_sel[LIST_DATA][2])) { ?>
    <?php
//                                        foreach ($data_sel[LIST_DATA][2] as $row) {
//                                            echo '<tr><td>' . $row->display_name . '</td>';
//                                            echo '<td>' . $row->value . '</td>';
//                                            echo '<td>' . $row->qty . '</td>';
//                                            echo '<td>' . 'Rp. ' . number_format($row->sell_price) . '</td>';
//                                            echo '<td>' . number_format($row->weight_kg) . '</td>';
//                                            echo '<td>' . 'Rp. ' . number_format($row->ship_price_charged * ceil($row->weight_kg)) . '</td>';
//                                            echo '<td>' . 'Rp. ' . number_format($row->sell_price + $row->ship_price_charged * ceil($row->weight_kg)) . '</td></tr>';
//                                        }
    ?>
    <?php // } else { ?>
                                            <tr>
                                                <td>-</td>
                                                <td>-</td>
                                                <td>-</td>
                                                <td>-</td>
                                                <td>-</td>
                                            </tr>
    <?php // } ?>
                                    </tbody>
                                </table>
                            </form>
                        </div>-->
    <!--                    <form class ="form-horizontal">
                            <div class="row">
                                <div class="form-group col-md-4 pull-right hidden-xs hidden-sm">
                                    <label class ="control-label col-md-6" >Total :</label>
                                    <div class="input-group col-md-6" style="padding-right: 15px">
                                        <label class="control-label" style="text-align: right">Rp. <?php // echo (isset($data_sel[LIST_DATA][0][0]) ? number_format($data_sel[LIST_DATA][0][0]->total_order) : "");                                                      ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group hidden-md hidden-sm hidden-lg col-xs-12">
                                    <label class="control-label col-xs-push-5 col-xs-4" style="text-align:right">Total :</label>
                                    <div class="input-group col-xs-push-5 col-xs-6" style="padding-left: 15px">
                                        <label class="control-label" style="text-align: right">Rp. <?php // echo (isset($data_sel[LIST_DATA][0][0]) ? number_format($data_sel[LIST_DATA][0][0]->total_order) : "");                                                      ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group hidden-lg hidden-xs hidden-md col-sm-12">
                                    <label class="control-label col-sm-push-5 col-sm-4">Total :</label>
                                    <div class="input-group  col-sm-push-5 col-sm-6" style="padding-left: 15px">
                                        <label class="control-label" style="text-align: right">Rp. <?php // echo (isset($data_sel[LIST_DATA][0][0]) ? number_format($data_sel[LIST_DATA][0][0]->total_order) : "");                                                      ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4 pull-right hidden-xs hidden-sm">
                                    <label class ="control-label col-md-6">Voucher :</label>
                                    <div class="input-group col-md-6" style="padding-right: 15px">
                                        <label class="control-label" style="text-align: right">Rp.  <?php // echo (isset($data_sel[LIST_DATA][0][0]) ? number_format($data_sel[LIST_DATA][0][0]->nominal) : "");                                                      ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group hidden-md hidden-sm hidden-lg col-xs-12">
                                    <label class="control-label col-xs-push-5 col-xs-4" style="text-align:right">Voucher :</label>
                                    <div class="input-group col-xs-push-5 col-xs-6" style="padding-left: 15px">
                                        <label class="control-label" style="text-align: right">Rp. <?php // echo (isset($data_sel[LIST_DATA][0][0]) ? number_format($data_sel[LIST_DATA][0][0]->nominal) : "");                                                      ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group hidden-lg hidden-xs hidden-md col-sm-12">
                                    <label class="control-label col-sm-push-5 col-sm-4">Voucher :</label>
                                    <div class="input-group  col-sm-push-5 col-sm-6" style="padding-left: 15px">
                                        <label class="control-label" style="text-align: right">Rp. <?php // echo (isset($data_sel[LIST_DATA][0][0]) ? number_format($data_sel[LIST_DATA][0][0]->nominal) : "");                                                      ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4 pull-right hidden-xs hidden-sm">
                                    <label class ="control-label col-md-6">Total Bayar :</label>
                                    <div class="input-group col-md-6" style="padding-right: 15px">
                                        <label class="control-label" style="text-align: right; color: red">Rp. <?php // echo (number_format($data_sel[LIST_DATA][0][0]->total_payment))                                                      ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group hidden-md hidden-sm hidden-lg col-xs-12">
                                    <label class="control-label col-xs-push-5 col-xs-4" style="text-align:right">Total Bayar :</label>
                                    <div class="input-group col-xs-push-5 col-xs-5" style="padding-left: 15px">
                                        <label class="control-label" style="color: red">Rp. <?php // echo (isset($data_sel[LIST_DATA][0][0]) ? number_format($data_sel[LIST_DATA][0][0]->total_payment) : "");                                                      ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group hidden-lg hidden-xs hidden-md col-sm-12">
                                    <label class="control-label col-sm-push-5 col-sm-4">Total Bayar :</label>
                                    <div class="input-group  col-sm-push-5 col-sm-6" style="padding-left: 15px">
                                        <label class="control-label" style="text-align: right;color: red">Rp. <?php // echo (isset($data_sel[LIST_DATA][0][0]) ? number_format($data_sel[LIST_DATA][0][0]->total_payment) : "");                                                      ?></label>
                                    </div>
                                </div>
                            </div>
                        </form>-->
    <div class="panel panel-primary">
        <div class="panel-heading big_title">No. Order : <?php echo $data_ord[0][0]->order_no ?></div>
        <div class='panel-body'>
            <div class='row'>
                <div class='col-md-12' style="margin-top:-20px;">
                    <p><h3>Detil Order : </h3></p>
                    <div class="well">
                        <div class='row'>
                            <div class='col-md-6'>
                                <p><span class='fa fa-tags fa-lg'></span>&nbsp; No. Order : <?php echo $data_sel[LIST_DATA][0][0]->order_no; ?></p>
                                <p><span class='fa fa-calendar fa-lg'></span>&nbsp; Tanggal Order : <?php echo date_format(date_create($data_sel[LIST_DATA][0][0]->order_date), "d-M-Y"); ?> </p>
                            </div>
                            <div class='col-md-6'>
                                <span class="fa fa-exclamation-circle fa-lg"></span>&nbsp; Status Order : <font style="color: red;font-weight: bold;"> <?php
                                $data = json_decode(STATUS_PAYMENT, true);
                                echo $data[$data_sel[LIST_DATA][0][0]->payment_status];
                                ?></font>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class='row'>
                <div class='col-md-6' style="margin-top:-10px;">
                    <p><h3>Informasi Penerima : </h3></p>
                    <div class="well">
                        <table border='0' class="table-informasi">
                            <style> .table-informasi td{padding: 2px;}
                            </style>
                            <tr>
                                <td valign='top' style=""><span class='fa fa-user fa-lg'></td>
                                <td ><?php echo $data_sel[LIST_DATA][0][0]->receiver_name; ?></td>
                            </tr>
                            <tr>
                                <td valign='top' ><span class='fa fa-home fa-lg'></td>
                                <td valign='top' ><?php echo $data_ord[0][0]->receiver_address . "<br>" . $data_ord[0][0]->province_name . "-" . $data_ord[0][0]->city_name . "-" . $data_ord[0][0]->district_name; ?></td>
                            </tr>
                            <tr>
                                <td><span class='fa fa-phone fa-lg'></td>
                                <td ><?php echo $data_sel[LIST_DATA][0][0]->receiver_phone_no; ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class='col-md-6' style="margin-top:-10px;">
                    <p><h3>Metode Pembayaran : </h3></p>
                    <div class="well">
                        <span class='fa fa-bars fa-lg'></span> &nbsp; <?php echo $data_sel[LIST_DATA][0][0]->payment_name; ?>
                    </div>
                </div>
            </div>
            <div class='row'>
                <div class="col-md-12" style="margin-top:-20px;">
                    <p><h3>Keranjang Belanja : </h3></p>   
                    <!--                        TABLE CART FOR LG,MD,SM-->

                    <table class="field-group-format group_specs table table-bordered  hidden-xs" style="margin-top:-10px;">
                        <tr class="success" id="tr-cart">
                            <th class="field-label" colspan="2" id="tr-cart">Produk</th>
                            <th class="field-label" id="tr-cart">Harga (Rp)</th>
                            <th class="field-label" id="tr-cart">Jumlah</th>
                            <th class="field-label" id="tr-cart">Biaya Kirim (Rp)</th>
                            <th class="field-label" id="tr-cart">Total (Rp)</th>
                        </tr>
                        <?php
                        $total = 0;
                        foreach ($data_ord[1] as $merchant) {
                            ?>
                            <tr>
                                <th colspan="8">                                        
                            <div class='row'>
                                <div class="col-lg-4 col-md-3 col-sm-4"><b><i class = 'material-icons' style='font-size:20px; vertical-align:top;'>store</i>&nbsp; <a href="<?php echo base_url() . "merchant/" . $merchant->merchant_code; ?>"><?php echo $merchant->merchant_name; ?> </a></b></div> 
                                <div class="col-lg-3 col-md-2 col-sm-3 text-left" ><b><i class="fa fa-truck"></i>&nbsp; <?php echo $merchant->expedition_name; ?> </b></div> 
                                <div class="col-lg-5 col-md-7 col-sm-5 text-right" ><i class="fa fa-pencil-square-o" title="Pesan ke <?php echo $merchant->merchant_name; ?>"></i> <?php echo $merchant->member_notes; ?></div>
                            </div>
                            </th>
                            </tr>  
                            <?php
                            foreach ($data_ord[2] as $prod) {
                                if ($merchant->merchant_seq == $prod->merchant_seq) {
                                    ?>
                                    <td class="field-content">
                                        <a href ="<?php echo base_url() . url_title($prod->display_name . " " . $prod->product_seq); ?>"><center><img src="<?php echo get_image_location() . PRODUCT_UPLOAD_IMAGE . $prod->merchant_seq . "/" . S_IMAGE_UPLOAD . $prod->img; ?>" width="100px"></center></a></td>
                                    <td class="field-label"><strong><?php echo $prod->display_name ?></strong>
                                        <br><br>
                                        <table border="0" class="table table-condensed">
                                            <tr>
                                                <td width="20%">Berat</td>  
                                                <td width="5%">:</td>
                                                <td><?php echo $prod->weight_kg ?> Kg</td>
                                            </tr>
                                            <?php if ($prod->value_seq != DEFAULT_VALUE_VARIANT_SEQ) { ?>
                                                <tr>
                                                    <td>Warna</td>
                                                    <td>:</td>
                                                    <td><?php echo $prod->variant_name; ?></td>
                                                </tr>
                                            <?php } ?>
                                            <tr>
                                                <td>Status Kirim</td>
                                                <td>:</td>
                                                <td class="text-red"><?php
                                                    $data = json_decode(STATUS_ORDER, true);
                                                    echo $data[$merchant->order_status];
                                                    ?></td>
                                            </tr>
                                            <tr>
                                                <td>No Resi</td>
                                                <td>:</td>
                                                <td class="text-red"><?php echo $merchant->awb_no; ?></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="field-content" id="tr-cart"><?php echo number_format($prod->sell_price); ?></td>
                                    <td class="field-content" id="tr-cart"><?php echo number_format($prod->qty); ?></td>
                                    <td class="field-content" id="tr-cart"><?php echo $prod->ship_price_charged == 0 ? "<div class='text-green'>Free</div>" : number_format(ceil($prod->weight_kg * $prod->qty) * $prod->ship_price_charged); ?></td>
                                    <td class="field-content" id="tr-cart"><?php
                                        echo number_format($prod->qty * $prod->sell_price + ceil($prod->weight_kg * $prod->qty) * $prod->ship_price_charged);
                                        $total = $total + $prod->qty * $prod->sell_price + ceil($prod->weight_kg * $prod->qty) * $prod->ship_price_charged;
                                        ?></td>
                                    </tr>
                                    <?php
                                }
                            }
                        }
                        ?>
                        <tr>
                        <tr class="font22">
                            <th  colspan="6" style="text-align: left;">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <font size="4px">Kode Voucher : <?php echo $data_ord[0][0]->voucher_code; ?>
                                <br>Voucher Member -  <?php echo number_format($data_ord[0][0]->nominal); ?></font>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6" style="text-align: right; ">
                                <font size="4px">Total Belanja : Rp <?php echo number_format($total); ?></font>
                                <p><font size="4px">Total Bayar : </font><font color="red">Rp <?php echo number_format($data_ord[0][0]->total_payment); ?></font></a>
                            </div>
                        </div>
                        </th>
                        </tr>
                    </table>
                    <!--
                    <!--                        TABLE CART FOR XS-->
                    <div class="hidden-lg hidden-sm hidden-md" >
                        <div class='table-responsive'>
                            <table class="table table-bordered">
                                <tr class="success">
                                    <th class="field-label"><center>Produk</center></th>
                                <th class="field-label">Keterangan</th>
                                </tr>
                                <?php foreach ($data_ord[1] as $merchant) { ?>
                                    <tr>
                                        <td class="field-content" colspan="4">
                                            <div class="row">
                                                <div class="col-xs-12 text-left"><b><i class = 'material-icons' style='font-size:20px; vertical-align:top;'>store</i>&nbsp; <a href="<?php echo base_url() . "merchant/" . $merchant->merchant_code; ?>"><?php echo $merchant->merchant_name; ?> </a></b> &nbsp; </div>
                                                <div class="col-xs-12 text-left"><b><i class="fa fa-truck "></i>&nbsp; <?php echo $merchant->expedition_name; ?> </b></div>
                                                <div class="col-xs-12 text-left" ><i class="fa fa-pencil-square-o"></i> <b>Catatan  : "<?php echo $merchant->member_notes; ?>"</b></div>
                                            </div></td>
                                    </tr>
                                    <?php
                                    foreach ($data_ord[2] as $prod) {
                                        if ($merchant->merchant_seq == $prod->merchant_seq) {
                                            ?>
                                            <tr>
                                                <td>
                                                    <table border="0" class="table table-condensed">
                                                        <a href ="<?php echo base_url() . url_title($prod->display_name . " " . $prod->product_seq); ?>"><center> <img src="<?php echo get_base_url() . PRODUCT_UPLOAD_IMAGE . $prod->merchant_seq . "/" . S_IMAGE_UPLOAD . $prod->img; ?>" alt="<?php echo $prod->img; ?>" class='img' width='100%' height ="100%"></center></a>
                                                        <tr>
                                                            <td>Status Kirim</td>
                                                            <td>:</td>
                                                            <td class="text-red"><?php
                                                                $data = json_decode(STATUS_ORDER, true);
                                                                echo $data[$merchant->order_status];
                                                                ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>No Resi</td>
                                                            <td>:</td>
                                                            <td class="text-red"><?php echo $merchant->awb_no ?></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    <strong><?php echo $prod->display_name ?></strong>
                                                    <br><br>
                                                    <table border="0" class="table table-condensed">
                                                        <tr>
                                                            <td>Berat</td>
                                                            <td>:</td>
                                                            <td><?php echo $prod->weight_kg ?> Kg</td>
                                                        </tr>
                                                        <?php if ($prod->value_seq != DEFAULT_VALUE_VARIANT_SEQ) { ?>
                                                            <tr>
                                                                <td>Warna</td>
                                                                <td>:</td>
                                                                <td><?php echo $prod->variant_name; ?></td>
                                                            </tr>
                                                        <?php } ?>
                                                        <tr>
                                                            <td>Qty</td>
                                                            <td>:</td>
                                                            <td><?php echo number_format($prod->qty); ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Harga</td>
                                                            <td>:</td>
                                                            <td>Rp. <?php echo number_format($prod->sell_price); ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Biaya Kirim</td>
                                                            <td>:</td>
                                                            <td><?php echo $prod->ship_price_charged == 0 ? "<div class='text-green'>Free</div>" : number_format(ceil($prod->weight_kg * $prod->qty) * $prod->ship_price_charged); ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Total</td>
                                                            <td>:</td>
                                                            <td>Rp <?php echo number_format($prod->qty * $prod->sell_price); ?></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                                <tr>
                                    <th class='field-content' colspan='2' ><div class='text-right'><font size="3px"  >Total Belanja : <?php echo number_format($total); ?></font></div></th>
                                </tr>
                                <tr><th colspan="2">
                                <div class="row">
                                    <div class="col-xs-12  text-right">
                                        <font size="3px">Kode Voucher : Voucher Member -  Rp <?php echo number_format($data_ord[0][0]->nominal); ?></font>
                                    </div>
                                </div>
                                </th></tr>
                                <tr>
                                    <th class="field-content" colspan="2">
                                <div class="row">
                                    <div class="col-xs-12" style="text-align: right; ">

                                        <font size="4px">Total Bayar : <font color="red">Rp <?php echo number_format($data_ord[0][0]->total_payment); ?></font></font></a>
                                    </div>
                                </div>
                                </th>
                                </tr>
                            </table>
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade active in" id="metode-pembayaran">
        <br>
        <div class="panel panel-primary">
            <div class="panel-heading big_title">Metode Pembayaran</div>
            <div class="panel-body">
                <form id='frm_payment_info' method="post" action="<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
                    <label>Pilih metode pembayaran : </label><br>
                    <?php $payment_info['payment_seq'] = (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0][0]->pg_method_seq : ""); ?>
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-default payment_type <?php echo!isset($payment_info) || $payment_info["payment_seq"] == PAYMENT_SEQ_BANK || $payment_info["payment_seq"] == PAYMENT_SEQ_DOCU_ATM ? "active" : "" ?>" >
                            <input id="transfer-bank" name="payment" value ="<?php echo PAYMENT_SEQ_BANK; ?>" type="radio" onClick="show_div(this.id)" <?php echo (!isset($payment_info) || $payment_info["payment_seq"] == PAYMENT_SEQ_BANK) || $payment_info["payment_seq"] == PAYMENT_SEQ_DOCU_ATM ? "checked" : ""; ?> >Transfer Bank
                        </label>
                        <label class="btn btn-default   payment_type <?php echo isset($payment_info) && $payment_info["payment_seq"] == PAYMENT_SEQ_DEPOSIT ? "active" : "" ?>">
                            <input id="saldo" name="payment" type="radio" value ="<?php echo PAYMENT_SEQ_DEPOSIT; ?>" onClick="show_div(this.id)" <?php echo isset($payment_info) && $payment_info["payment_seq"] == PAYMENT_SEQ_DEPOSIT ? "checked" : ""; ?>>Deposit
                        </label>
                        <label class="btn btn-default  payment_type <?php echo isset($payment_info) && $payment_info["payment_seq"] == PAYMENT_SEQ_CREDIT_CARD ? "active" : "" ?>"">
                            <input id="kartu-kredit" name="payment" type="radio" value ="<?php echo PAYMENT_SEQ_CREDIT_CARD; ?>" onClick="show_div(this.id)" <?php echo isset($payment_info) && $payment_info["payment_seq"] == PAYMENT_SEQ_CREDIT_CARD ? "checked" : ""; ?>>Kartu Kredit
                        </label>
                        <label class="btn btn-default  payment_type <?php echo isset($payment_info) && ($payment_info["payment_seq"] == PAYMENT_SEQ_BCA_KLIKPAY || $payment_info["payment_seq"] == PAYMENT_SEQ_MANDIRI_KLIKPAY) ? "active" : "" ?>">
                            <input id="internet-banking" name="payment" type="radio" value ="<?php echo PAYMENT_SEQ_BCA_KLIKPAY; ?>" onClick="show_div(this.id)" <?php echo (isset($payment_info) && (($payment_info["payment_seq"] == PAYMENT_SEQ_BCA_KLIKPAY) || ($payment_info["payment_seq"] == PAYMENT_SEQ_MANDIRI_KLIKPAY))) ? "checked" : ""; ?>>Internet Banking
                        </label>
                        <label class="btn btn-default  payment_type <?php echo isset($payment_info) && $payment_info["payment_seq"] == PAYMENT_SEQ_MANDIRI_ECASH ? "active" : "" ?>">
                            <input id="emoney" name="payment" type="radio" value ="<?php echo PAYMENT_SEQ_MANDIRI_ECASH; ?>" onClick="show_div(this.id)" <?php echo isset($payment_info) && $payment_info["payment_seq"] == PAYMENT_SEQ_MANDIRI_ECASH ? "checked" : ""; ?>>E-money
                        </label>
                        <label class="btn btn-default  payment_type <?php echo isset($payment_info) && $payment_info["payment_seq"] == PAYMENT_SEQ_DOCU_ALFAMART ? "active" : "" ?>">
                            <input id="docu-alfamart" name="payment" type="radio" value ="<?php echo PAYMENT_SEQ_DOCU_ALFAMART; ?>" onClick="show_div(this.id)" <?php echo isset($payment_info) && $payment_info["payment_seq"] == PAYMENT_SEQ_DOCU_ALFAMART ? "checked" : ""; ?>>Alfamartku
                        </label>
                    </div>
                    <br><br>
                    <div id="div" class="saldo-div" <?php echo isset($payment_info) && $payment_info["payment_seq"] == PAYMENT_SEQ_DEPOSIT ? "" : "style=\"display:none\"" ?>>
                        <h3>Saldo Deposit <?php echo "Rp." . number_format($member_info[2][0]->deposit_amt); ?></h3>
                    </div>
                    <div id="div" class="transfer-bank-div" <?php echo!isset($payment_info) || $payment_info["payment_seq"] == PAYMENT_SEQ_BANK || $payment_info["payment_seq"] == PAYMENT_SEQ_DOCU_ATM ? "" : "style=\"display:none\"" ?>>
                        <input type="radio" name="atm_type"  value ="<?php echo PAYMENT_SEQ_BANK; ?>" checked> Pembayaran Via LLG Antar Bank
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover info">
                                <tr class="info">
                                    <th>Bank</th>
                                    <th>Nomor Rekening</th>
                                    <th>Atas Nama</th>
                                </tr>
                                <?php foreach ($data_bank as $bank) { ?>
                                    <tr>
                                        <td> <img height="30px" src="<?php echo get_image_location() . BANK_UPLOAD_IMAGE . $bank->logo_img; ?>" alt="<?php echo $bank->logo_img; ?>"></th>
                                        <td><?php echo $bank->bank_acct_no; ?> </td>
                                        <td><?php echo $bank->bank_acct_name; ?></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                        <input type="radio" name="atm_type"  value ="<?php echo PAYMENT_SEQ_DOCU_ATM; ?>" <?php echo isset($payment_info) && $payment_info["payment_seq"] == PAYMENT_SEQ_DOCU_ATM ? "checked" : "" ?>> Pembayaran Via ATM Bersama
                        </br>
                        <img src ="<?php echo get_img_url() ?>payment/logo-atm.png" alt="Docu ATM">
                    </div>
                    <div id="div" class="kartu-kredit-div" <?php echo isset($payment_info) && $payment_info["payment_seq"] == PAYMENT_SEQ_CREDIT_CARD ? "" : "style=\"display:none\"" ?>> <img class="img-responsive col-lg-4 col-md-4 col-sm-4 col-xs-4" src="<?php echo get_img_url() . "credit_card.jpg" ?>"> </div>
                    <div id="div" class="internet-banking-div" <?php echo isset($payment_info) && ($payment_info["payment_seq"] == PAYMENT_SEQ_BCA_KLIKPAY || $payment_info["payment_seq"] == PAYMENT_SEQ_MANDIRI_KLIKPAY) ? "" : "style=\"display:none\"" ?>>
                        <input type="radio" name="bank_type" value ="<?php echo PAYMENT_SEQ_MANDIRI_KLIKPAY; ?>" <?php echo!isset($payment_info) || $payment_info["payment_seq"] != PAYMENT_SEQ_BCA_KLIKPAY ? "checked" : "" ?>>
                        <img src ="<?php echo get_img_url() ?>payment_70px/mandiriclickpay_70px.png" alt="Mandiri Clikpay">
                        <input type="radio" name="bank_type" value ="<?php echo PAYMENT_SEQ_BCA_KLIKPAY; ?>" <?php echo isset($payment_info) && $payment_info["payment_seq"] == PAYMENT_SEQ_BCA_KLIKPAY ? "checked" : "" ?>>
                        <img src ="<?php echo get_img_url() ?>payment_70px/bcaklickpay.png"  alt="BCA Clikpay">
                    </div>
                    <div id="div" class="emoney-div" <?php echo isset($payment_info) && $payment_info["payment_seq"] == PAYMENT_SEQ_MANDIRI_ECASH ? "" : "style=\"display:none\"" ?>>
                        <input type="radio" name="emoney_type"  value ="<?php echo PAYMENT_SEQ_MANDIRI_ECASH; ?>" checked>
                        <img src ="<?php echo get_img_url() ?>payment_70px/mandiri e-cash_70px.png" alt="Mandiri e-cash">
                    </div>
                    <div id="div" class="docu-alfamart-div" <?php echo isset($payment_info) && $payment_info["payment_seq"] == PAYMENT_SEQ_DOCU_ALFAMART ? "" : "style=\"display:none\"" ?>>
                        <input type="radio" name="docu_type"  value ="<?php echo PAYMENT_SEQ_DOCU_ALFAMART; ?>" checked>
                        <img src ="<?php echo get_img_url() ?>payment/logo-groupalfa.png" alt="Alfamartku">
                    </div>
            </div>
            <div class="panel-footer">
                <div  class="row hidden-xs">

                    <div class="col-md-6 col-sm-6 col-lg-6">
                        <a type="button" class="btn btn-front btn-flat btn-md"  href="<?php echo base_url() ?>member" style="background-color: #EC7115;"><i class="fa fa-backward"></i>&nbsp; Kembali</a>
                    </div>

                    <div class="col-md-6 col-sm-6 col-lg-6 text-right">
                        <button  class="btn btn-front btn-flat btn-md" type="submit" id="btnSaveEdit" name="btnSaveEdit" ><i class="fa fa-save"></i>&nbsp; Simpan </button>
                    </div>

                </div>
                <div  class="row hidden-lg hidden-md hidden-sm">
                    <div class="col-xs-12">
                        <button  class="btn btn-front btn-flat btn-block" type="submit" id="btnSaveEdit" name="btnSaveEdit" ><i class="fa fa-save"></i>&nbsp; Simpan </button>
                    </div>

                    <div class="col-xs-12" style="margin-top:5px;">
                        <a type="button" class="btn btn-front btn-flat btn-block"  href="<?php echo base_url() ?>member" style="background-color: #EC7115;"><i class="fa fa-backward"></i>&nbsp; Kembali</a>
                    </div>



                </div>
            </div>                                    
            </form>
        </div>
    </div>
</div>
</div>
</section>

<!--    </div>
</div>
</div>-->
<script>
                                $('#tbl').DataTable({
                                    oLanguage: {
                                        sProcessing: "<img src='<?php echo get_image_location() . ASSET_IMG_HOME ?>loading.gif' />"
                                    },
                                    responsive: true,
                                    dom: '<"top"lB>rt<"bottom"ip><"clear">',
                                })
                                        .on('xhr.dt', function(e, settings, response, xhr) {
                                    response = JSON.stringify(response);
                                    if (isSessionExpired(response)) {
                                        response_object = json_decode(response);
                                        url = response_object.url;
                                        location.href = url;
                                    }
                                });
</script>