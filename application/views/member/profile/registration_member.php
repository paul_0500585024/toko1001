<?php
require_once VIEW_BASE_HOME;
?>
<div class="row hidden-xs " style="margin-top:25px;">&nbsp;</div>
<div class="row hidden-xs hidden-lg" style="margin-top:10px;">&nbsp;</div>
<div class='container'>
    <?php if (isset($data_err) && $data_err[ERROR] === true) { ?>
        <div class="alert alert-error-home">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $data_err[ERROR_MESSAGE][0] ?>
        </div>
    <?php } elseif (isset($data_suc) && ($data_suc[SUCCESS] === true)) { ?>
        <div class="alert alert-success-home">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo (isset($data_suc[SUCCESS_MESSAGE][0]) ? $data_suc[SUCCESS_MESSAGE][0] : SAVE_DATA_SUCCESS); ?>
        </div>           
    <?php } ?>
    <div class="panel panel-primary">
        <div class="panel-heading" style=" font-size: 20px">Daftar Member</div>
        <div class="panel-body">
            <form class="form-horizontal" id="frmMember" action="<?php echo get_base_url() . "registration"; ?>" method="post">
                <!--<section class="col-md-12 col-xs-12">-->
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="hidden-xs hidden-sm hidden-md">
                            <img src="<?php echo get_img_url(); ?>maribergabung_420X335.jpg" alt="mari bergabung" style=" width: 520px; height:427px;">
                        </div>
                        <div class="col-md-12 hidden-xs hidden-xs hidden-lg">
                            <img src="<?php echo get_img_url(); ?>maribergabung_420X335.jpg" alt="mari bergabung" style=" width: 420px; height:327px;">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <!--<div class="row">-->
                    <div class="row">
                        <div class="form-group has-feedback">
                            <div class="col-md-11 col-xs-12">
                                <input id="m_email"validate="required[]" name="member_email" type="text" class="form-control" placeholder="Email" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->email : "") ?>"/>
                                <span class="glyphicon glyphicon-envelope form-control-feedback" ></span>
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <div class="col-md-11">
                                <input id="member_name" validate="required[]" name="member_name" type="text" class="form-control" placeholder="Nama Lengkap" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->name : "") ?>" />
                                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <div class="col-md-11">
                                <input id="member_birthday" date_type="date" validate="required[]" name="member_birthday" type="text" class="form-control"  placeholder="Tanggal Lahir" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->date : "") ?>"  readonly style="background-color: #fff;"/>
                                <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <select id="gender_member" class="form-control" name="gender_member" validate="required[]">
                                    <option value="">Jenis Kelamin</option>
                                    <?php
                                    echo combostatus(json_decode(STATUS_GENDER), (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->gender : ""));
                                    ?>
                                </select>
                            </div>
                        </div>
                        <!--<div class="hidden-lg hidden-md hidden-sm" style="margin-top:12px;"></div>-->
                        <div class="col-md-offset-1 col-md-5">
                            <div class="form-group">
                                <input id="member_phone" validate="required[]"  name="member_phone" type="text" class="form-control" placeholder="No. Telepon" data-mask data-inputmask="'mask': ['999 999 999 999 999']" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->phone : "") ?>" />
                                <span class="glyphicon glyphicon-earphone form-control-feedback"></span>
                            </div>
                        </div>
                    </div>          
                    <div class="row">
                        <div class="col-md-5">                         
                            <div class="form-group ">                            
                                <input id="password1" validate="password[]" name="password1" type="password" class="form-control" placeholder="Password" />
                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                            </div> 
                        </div>
                        <!--<div class="hidden-lg hidden-md hidden-sm" style="margin-top:12px;"></div>-->
                        <div class="col-md-offset-1 col-md-5">                                
                            <div class="form-group ">  
                                <input id="password2" validate="password[]" name="password2" type="password" class="form-control" placeholder="Ulangi password" />
                                <span class="glyphicon glyphicon-edit form-control-feedback"></span>                                
                            </div>
                        </div>
                    </div>           
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group has-feedback">
                                <?php echo $image ?>
                            </div>
                        </div>
                        <div class="hidden-lg hidden-md hidden-sm" style="margin-top:12px;"></div>
                        <div class="col-md-offset-1 col-md-5">
                            <div class="form-group has-feedback">
                                <input id="captcha_member" validate="required[]" name="captcha_member" type="text" class="form-control" placeholder="Kode Captcha" maxlength="5"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="checkbox icheck">
                            <label>

                                <div class="col-md-1 col-xs-1">
                                    <div class="form-group">
                                        <input id="member_requirement"  name="member_requirement" type="checkbox" class="minimal" value="1"> 
                                    </div>
                                </div>

                                <div class="col-md-10 col-xs-10">
                                    <div class="form-group">
                                        Saya sudah membaca dan menyetujui
                                        <a href="<?php echo base_url(); ?>info/syarat-dan-ketentuan"
                                           target="_blank">Syarat dan Ketentuan</a>, serta
                                        <a href="<?php echo base_url(); ?>info/kebijakan-privasi"
                                           target="_blank">Kebijakan Privasi </a> dari Toko1001.
                                    </div>
                                </div>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-11">
                            <div class="form-group has-feedback">
                                <button type="submit" name="btnSave"  value="true" class="btn btn-google-plus btn-flat" style="width:100%;"><span class="fa fa-user"></span> Daftar &nbsp;</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    $("[data-mask]").inputmask();
    var isValidMember = $('#frmMember').validate({
        ignore: 'input[type=hidden]',
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.hasClass('select2')) {
                error.insertAfter(element.next('span'));
            } else {
                error.insertAfter(element);
            }
        }
    });

    $('form#frmMember').submit(function() {
        var get_data_member = $(this).serialize();
        if (isValidMember.valid()) {
            $('input[type="text"]').each(function() {
                if ($(this).hasClass('auto') || $(this).hasClass('auto_int') || $(this).hasClass('auto_dec'))
                {
                    var v = $(this).autoNumeric('get');
                    $(this).autoNumeric('destroy');
                    $(this).val(v);
                }
            })
        }
    })
</script>