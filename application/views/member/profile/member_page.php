<?php
require_once VIEW_BASE_HOME;
$tablesort = 'desc';
?>
<link rel="stylesheet" type="text/css" href="<?php echo get_css_url(); ?>tabdrop.css"/>
<div class="hidden-xs hidden-lg " style="margin-top:65px;">&nbsp;</div>
<div class="hidden-xs hidden-sm hidden-md" style="margin-top:35px;">&nbsp;</div>

<div class="container">
    <div class="row"><form id="profile-upload" name="profile-upload" method ="post" enctype ="multipart/form-data" action= "">
            <input id="profile-image-upload" name="profile-image-upload" class="hidden" type="file" accept="image/*" onchange ="previewimg(this)"><input type="hidden" name="profiler" value="1001" /></form>
        <div class="hidden-xs col-lg-2 col-md-3 col-sm-3">
            <div class="user-wrapper" style=" display: inline-table">
                <img src="<?php echo $img_src; ?>" class="img-profile" style=" width: 165px;height: 125px;cursor: pointer;" id="profile-image">
            </div>
        </div>
        <div class="container">
            <div class="col-xs-12 col-lg-10  col-md-9 col-sm-9">
                <div class="row col-lg-12">
                    <strong><?php echo get_display_value($data_sel[LIST_DATA][0][0]->member_name) ?></strong>
                </div>
                <div class="row hidden-xs col-lg-4 col-sm-6">
                    <h5><i class="fa fa-user"></i>&nbsp;
                        <?php if ($data_sel[LIST_DATA][0][0]->gender == "M") { ?>
                            Pria
                        <?php } else if ($data_sel[LIST_DATA][0][0]->gender == "F") { ?>
                            Wanita
                        <?php } ?>
                    </h5>
                    <h5><i class = "fa fa-phone"></i>&nbsp; <?php echo $data_sel[LIST_DATA][0][0]->mobile_phone ?></h5>
                    <h5><i class="fa fa-envelope"></i>&nbsp;
                        <?php echo ($data_sel[LIST_DATA][0][0]) ? get_display_value($data_sel[LIST_DATA][0][0]->email) : ""; ?>
                    </h5>
                    <input class="btn btn-danger btn-xs" type="button"
                           value="Simpan Gambar" onclick="uploadfile()" id="btnupload" style="display: none;">
                </div>
                <div class="row hidden-xs col-lg-4 col-sm-6">
                    <h5>Terakhir Masuk : <?php echo cdate($_SESSION[SESSION_MEMBER_LAST_LOGIN], 1) ?></h5>
                    <h5>Saldo Terakhir &nbsp;: Rp. <?php echo ($data_sel[LIST_DATA][2][0]) ? number_format($data_sel[LIST_DATA][2][0]->deposit_amt) : ""; ?></h5>
                </div>
                <div class="row hidden-sm hidden-lg hidden-md col-xs-12">
                    <h5><i class="fa fa-user"></i>&nbsp;
                        <?php if ($data_sel[LIST_DATA][0][0]->gender == "M") { ?>
                            Pria
                        <?php } else if ($data_sel[LIST_DATA][0][0]->gender == "F") { ?>
                            Wanita
                        <?php } ?>
                    </h5>
                    <h5><i class = "fa fa-phone"></i>&nbsp; <?php echo $data_sel[LIST_DATA][0][0]->mobile_phone ?></h5>
                    <h5><i class="fa fa-envelope"></i>&nbsp;
                        <?php echo ($data_sel[LIST_DATA][0][0]) ? get_display_value($data_sel[LIST_DATA][0][0]->email) : ""; ?>
                    </h5>
                    <input class="btn btn-danger btn-xs" type="button"
                           value="Simpan Gambar" onclick="uploadfile()" id="btnupload" style="display: none;">
                    <h5>Terakhir Masuk : <?php echo cdate($_SESSION[SESSION_MEMBER_LAST_LOGIN], 1) ?></h5>
                    <h5>Saldo Terakhir &nbsp;: Rp. <?php echo ($data_sel[LIST_DATA][2][0]) ? number_format($data_sel[LIST_DATA][2][0]->deposit_amt) : ""; ?></h5>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div id ="content-message">
        <?php if (isset($data_err) && $data_err[ERROR] === true) { ?>
            <div class="alert alert-error-home">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $data_err[ERROR_MESSAGE][0] ?>
            </div>
        <?php } elseif ($data_suc[SUCCESS] === true) { ?>
            <div class="alert alert-success-home">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo (isset($data_suc[SUCCESS_MESSAGE][0]) ? $data_suc[SUCCESS_MESSAGE][0] : SAVE_DATA_SUCCESS); ?>
            </div>
        <?php } else { ?>
            <div id="error-message"></div>
        <?php } ?>
    </div>
    <?php
    $count_msg = '';
    if (isset($data_sel[LIST_DATA][6][0])) {
        if ($data_sel[LIST_DATA][6][0][0]->count_msg > 0) {
            $count_msg = ' <span class="label label-danger">' . $data_sel[LIST_DATA][6][0][0]->count_msg . '</span>';
        }
    }
    ?>
    <div class="user-wrapper">
        <div class="description-inside ">
            <!-- Nav tabs -->
            <div class=" hidden-lg hidden-md ">

                <ul class="nav">
                    <li role="presentation" class="dropdown ">
                        <div class="btn-group ">

                            <button type="button" class="btn btn-flat btn-front btn-md dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span><i class="fa fa-align-justify"></i></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <button type="button" class="btn btn-flat btn-default btn-md  " ><b><span id="title-menu"></span></b></button>
                            <ul class="dropdown-menu" id="member_page_xs">
                                <li id="tab-contact-xs" class="active"><a href="#contact" aria-controls="contact" role="tab" data-toggle="tab"><i class="fa fa-user"></i>&nbsp; Info Member</a></li>
                                <li id="tab-address-xs"><a href="#address" aria-controls="address" role="tab" data-toggle="tab"><i class="fa fa-home"></i>&nbsp; Alamat</a></li>
                                <li id="tab-bank-xs"><a href="#bank" aria-controls="bank" role="tab" data-toggle="tab"><i class="fa fa-university"></i>&nbsp; Info Bank</a></li>
                                <li id="tab-deposit-xs"><a href="#deposit" aria-controls="deposit" role="tab" data-toggle="tab"><i class="fa fa-money"></i>&nbsp; Tarik Deposit</a></li>
                                <li id="tab-voucher-xs"><a href="#voucher" aria-controls="payment" role="tab" data-toggle="tab"><i class="fa fa-dollar"></i>&nbsp;&nbsp;&nbsp;  Voucher</a></li>
                                <li id="tab-order-xs"><a href="#order" aria-controls="order" role="tab" data-toggle="tab"><i class="fa fa-shopping-cart"></i>&nbsp; Order</a></li>
                                <li id="tab-review-xs"><a href="#review" aria-controls="review" role="tab" data-toggle="tab"><strong><i class="fa fa-comments-o"></i>&nbsp; Ulasan </strong> <?php echo $count_msg; ?></a></li>
                                <li id="tab-return-xs"><a href="#return" aria-controls="return" role="tab" data-toggle="tab"><i class="fa fa-cubes"></i>&nbsp; Pengembalian</a></li>
                                <li id="tab-config-xs"><a href="#configuration" aria-controls="configuration" role="tab" data-toggle="tab"><i class="fa fa-gear"></i>&nbsp; Konfigurasi</a></li>
                            </ul>
                        </div>
                    </li>

                </ul>

            </div>
            <ul class="nav nav-tabs hidden-xs hidden-sm" id="member_page">
                <li id="tab-contact" class="active"><a href="#contact" aria-controls="contact" role="tab" data-toggle="tab"><strong><i class="fa fa-user"></i>&nbsp; Info Member</strong></a></li>
                <li id="tab-address" ><a href="#address" aria-controls="address" role="tab" data-toggle="tab"><strong><i class="fa fa-home"></i>&nbsp; Alamat</strong></a></li>
                <li id="tab-bank"><a href="#bank" aria-controls="bank" role="tab" data-toggle="tab"><strong><i class="fa fa-university"></i>&nbsp; Bank</strong></a></li>
                <li id="tab-deposit"><a href="#deposit" aria-controls="deposit" role="tab" data-toggle="tab"><strong><i class="fa fa-money"></i>&nbsp; Tarik Deposit</strong></a></li>
                <li id="tab-voucher"><a href="#voucher" aria-controls="payment" role="tab" data-toggle="tab"><strong><i class="fa fa-dollar"></i>&nbsp; Voucher</strong></a></li>
                <li id="tab-order"><a href="#order" aria-controls="order" role="tab" data-toggle="tab"><strong><i class="fa fa-shopping-cart"></i>&nbsp; Order</strong></a></li>
                <li id="tab-review"><a href="#review" aria-controls="review" role="tab" data-toggle="tab"><strong><i class="fa fa-comments-o"></i>&nbsp; Ulasan </strong> <?php echo $count_msg; ?></a></li>
                <li id="tab-return"><a href="#return" aria-controls="return" role="tab" data-toggle="tab"><strong><i class="fa fa-cubes"></i>&nbsp; Pengembalian</strong></a></li>
                <li id="tab-config"><a href="#configuration" aria-controls="configuration" role="tab" data-toggle="tab"><strong><i class="fa fa-gear"></i>&nbsp; Konfigurasi</strong></a></li>
            </ul>


            <!-- Tab panes -->
            <div class="tab-content" id="myTabContent">
                <!--BASIC INFO CHANGE PROFILE-->
                <div role="tabpanel" id="contact" class="tab-pane active">
                    <br>
                    <form class ="form-horizontal text-left" id="frmFill" method ="post" action="member/update">
                        <input name ="tab" type="hidden" value="<?php echo TAB_BASIC ?>">
                        <div class="form-group">
                            <label class="col-md-2 col-sm-3 col-xs-12 col-lg-2 control-label" style="text-align:left">Nama</label>
                            <div class="col-sm-9 col-md-10 col-xs-12 col-lg-10">
                                <input type="text" class="form-control" name="member_name" value="<?php echo ($data_sel[LIST_DATA][0][0]) ? get_display_value($data_sel[LIST_DATA][0][0]->member_name) : ""; ?>" maxlength="50">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-3 col-xs-12 col-lg-2" style="text-align:left">Jenis Kelamin</label>
                            <div class="col-sm-9 col-md-10 col-xs-12 col-lg-10">
                                <div class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-default
                                           <?php echo (!isset($data_sel[LIST_DATA]) ? "active" : (($data_sel[LIST_DATA][0][0]->gender == "M") ? "active" : "")); ?>">
                                        <span class="glyphicon glyphicon-ok" ></span>
                                        <input type="radio" name="gender" <?php if ($data_sel[LIST_DATA][0][0]->gender == "M") echo "checked"; ?> value="M"> Pria

                                    </label>
                                    <label class="btn btn-default
                                           <?php echo (!isset($data_sel[LIST_DATA]) ? "active" : (($data_sel[LIST_DATA][0][0]->gender == "F") ? "active" : "")); ?>">
                                        <span class="glyphicon glyphicon-ok"></span>
                                        <input type="radio" name="gender" <?php if ($data_sel[LIST_DATA][0][0]->gender == "F") echo "checked"; ?> value="F"> Wanita
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-sm-3 col-xs-12 col-lg-2 control-label" style="text-align:left">Tanggal Lahir</label>
                            <div class="col-sm-9 col-md-10 col-xs-12 col-lg-10">
                                <input class="form-control" id="datepicker" date_type="date" name ="birthday" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0][0]) ? $data_sel[LIST_DATA][0][0]->birthday : "" ); ?>" readonly="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-sm-3 col-xs-12 col-lg-2 control-label" style="text-align:left">Handphone</label>
                            <div class="col-sm-9 col-md-10 col-xs-12 col-lg-10">
                                <input class="form-control" data-inputmask="&quot;mask&quot;: &quot;9999 9999 9999&quot;" data-mask="" name ="mobile_phone" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0][0]) ? $data_sel[LIST_DATA][0][0]->mobile_phone : "" ); ?>">
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="col-sm-offset-2 col-sm-12 ">
                                <button type="submit" class="btn btn-front btn-flat btn-md" ><i class="fa fa-save"></i>&nbsp; Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>

                <style>
                    .link-button {
                        background: none;
                        border: none;
                        color: #199CB7;
                        cursor: pointer;
                    }
                </style>
                <!--ADDRESS-->
                <div role="tabpanel" class="tab-pane fade" id="address">
                    <br>
                    <div class="row">
                        <div class='container'>
                            <button type="submit" onclick ="add_address()" class="btn btn-google-plus btn-flat" style=" background-color: #EC7115"><i class="fa fa-plus "></i>&nbsp;Tambah Alamat Baru</button>
                        </div>&nbsp;
                        <br>

                        <?php if (isset($data_sel[LIST_DATA][1])) { ?>
                            <?php foreach ($data_sel[LIST_DATA][1] as $row) { ?>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div id="<?php echo $row->address_seq ?>" class="panel panel-default" style="margin-right:10px;">
                                        <div class="panel-heading">
                                            <div class="panel-title"><strong><?php
                                                    if ($row->alias == "") {
                                                        echo "&nbsp";
                                                    } else {
                                                        echo get_display_value($row->alias);
                                                    }
                                                    ?></strong></div>
                                        </div>
                                        <div class="panel-body" style="max-height: 150px;    overflow-y: scroll;">
                                            <div class="input-group">
                                                <label class="control-label" style="text-align: left">
                                                    <?php echo get_display_value($row->pic_name) ?>
                                                </label>
                                            </div>
                                            <div class="input-group">
                                                <label class="control-label" style="text-align: left">
                                                    <?php echo get_display_value($row->address) ?>
                                                </label>
                                            </div>
                                            <div class="input-group">
                                                <label class="control-label" style="text-align: left">
                                                    <?php echo ($row->province_name) ?> -
                                                    <?php echo ($row->city_name) ?>
                                                </label>
                                            </div>
                                            <div class="input-group">
                                                <label class="control-label" style="text-align: left">
                                                    <?php echo ($row->district_name) ?> -
                                                    <?php echo get_display_value($row->zip_code) ?>
                                                </label>
                                            </div>
                                            <div class="input-group">
                                                <label class="control-label" style="text-align: left">
                                                    <?php echo ($row->phone_no) ?>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <button class="btn btn-default btn-flat btn-sm text-left"
                                                    onclick ="edit_address(<?php echo $row->address_seq ?>)"
                                                    type="submit" >Ubah Alamat</button>
                                            <button class="btn btn-default btn-flat btn-sm text-right"
                                                    onclick ="delete_address(<?php echo $row->address_seq ?>)"
                                                    type="submit" >Hapus</button>
            <!--                                                        <button class="btn btn-success btn-flat btn-sm col-xs-12"  onclick ="edit_address(<?php echo $row->address_seq ?>)"                                                              onclick ="edit_address(<?php echo $row->address_seq ?>)"
                                                    type="submit" title="Ubah Alamat"><i class="fa fa-pencil"></i>&nbsp; Ubah</button>


                                            <button class="btn btn-danger btn-flat btn-sm col-xs-12"
                                                    onclick ="delete_address(<?php echo $row->address_seq ?>)"
                                                    type="submit" title="Hapus Alamat"><i class="fa fa-trash-o"></i>&nbsp; Hapus</button>-->

                                        </div>
                                    </div></div>

                            <?php } ?>
                        <?php } else { ?>

                        <?php } ?>
                    </div>
                </div>

                <!--Info Bank-->
                <div class="tab-pane" id="bank">
                    <br>
                    <form class ="form-horizontal"  method ="post" action="member/update">
                        <input name ="tab" type="hidden" value="<?php echo TAB_PAYMENT ?>">
                        <div class="form-group ">
                            <label class="col-md-2 col-sm-3 col-xs-12 col-lg-2 control-label" style="text-align:left">Bank</label>
                            <div class="col-sm-9 col-md-10 col-xs-12 col-lg-10">
                                <input class="form-control" name ="bank_name" type="text" placeholder ="Bank" value ="<?php echo (isset($data_sel[LIST_DATA][2][0]) ? $data_sel[LIST_DATA][2][0]->bank_name : "" ); ?>" maxlength="25">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-3 col-xs-12 col-lg-2" style="text-align:left">Cabang</label>
                            <div class="col-sm-9 col-md-10 col-xs-12 col-lg-10">
                                <input class="form-control" name ="bank_branch_name" type="text" placeholder ="Cabang" value ="<?php echo (isset($data_sel[LIST_DATA][2][0]) ? $data_sel[LIST_DATA][2][0]->bank_branch_name : "" ); ?>" maxlength="50">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-sm-3 col-xs-12 col-lg-2 control-label" style="text-align:left">Nomor Rekening</label>
                            <div class="col-sm-9 col-md-10 col-xs-12 col-lg-10">
                                <input class="form-control" data-inputmask="&quot;mask&quot;: &quot;9999 9999 9999 9999&quot;" data-mask="" name ="bank_acct_no" type="text" placeholder ="No. Rekening" value ="<?php echo (isset($data_sel[LIST_DATA][2][0]) ? $data_sel[LIST_DATA][2][0]->bank_acct_no : "" ); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-sm-3 col-xs-12 col-lg-2 control-label" style="text-align:left">Rekening A/N</label>
                            <div class="col-sm-9 col-md-10 col-xs-12 col-lg-10">
                                <input class="form-control"  name ="bank_acct_name" type="text" placeholder ="Atas Nama" value ="<?php echo (isset($data_sel[LIST_DATA][2][0]) ? $data_sel[LIST_DATA][2][0]->bank_acct_name : "" ); ?>" maxlength="50">
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="col-sm-offset-2 col-sm-10 ">
                                <button type="submit" class="btn btn-front btn-flat btn-md" ><i class="fa fa-save"></i>&nbsp; Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>

                <!--DEPOSIT-->
                <div role="tabpanel" class="tab-pane fade" id="deposit">
                    <br>
                    <div class="tab-content">
                        <form class ="form-horizontal" id="frmFill" method ="post" action= "<?php echo get_base_url() ?>member/update">
                            <input name ="tab" type="hidden" value="<?php echo TAB_DEPOSIT ?>">
                            <div class="form-group ">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-2  control-label" style="text-align:left">Saldo</label>
                                                <div class="col-sm-10 ">
                                                    <input type="text" class="form-control" name="deposit" value="Rp. <?php echo number_format($data_sel[LIST_DATA][2][0]->deposit_amt) ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" style="text-align:left">Nominal Penarikan (Rp)</label>
                                                <div class="col-sm-10">
                                                    <input id="amount" class="form-control auto_int" name ="amount" type="text">
                                                </div>
                                            </div>
                                            &nbsp;
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label" style="text-align:left"><i class="fa fa-angle-double-right "></i>&nbsp;Tujuan Rekening</label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" style="text-align:left">Bank</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control" name="bank_name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][2][0]) ? $data_sel[LIST_DATA][2][0]->bank_name : "" ); ?> "readonly>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label  class="col-sm-2 control-label" style="text-align:left">Cabang</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control" name="bank_branch_name" type="text"value ="<?php echo (isset($data_sel[LIST_DATA][2][0]) ? $data_sel[LIST_DATA][2][0]->bank_branch_name : "" ); ?>"readonly>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label  class="col-sm-2 control-label" style="text-align:left">No. Rekening</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control" name="bank_acct_no" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][2][0]) ? $data_sel[LIST_DATA][2][0]->bank_acct_no : "" ); ?>"readonly>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label  class="col-sm-2 control-label" style="text-align:left">Rekening A/N</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control" name="bank_acct_name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][2][0]) ? get_display_value($data_sel[LIST_DATA][2][0]->bank_acct_name) : "" ); ?>"readonly>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <button type="submit" class="btn btn-front btn-flat btn-md" ><i class="fa fa-save"></i>&nbsp; Simpan</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="table-responsive">
                        <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                            <thead>
                                <tr class="success">
                                    <th column="trx_date"> Tanggal </th>
                                    <th column="trx_no"> No Transaksi </th>
                                    <th column="trx_type"> Tipe Transaksi </th>
                                    <th column="status"> Status </th>
                                    <th column="nilai_transaksi"> Nilai Transaksi </th>
                                    <th column="deposit_Keluar"> Deposit Keluar </th>
                                    <th column="deposit_Masuk"> Deposit Masuk </th>
                                    <th column="saldo"> Saldo </th>
                                    <th column="bank_name"> Bank </th>
                                    <th column="bank_acct_no"> Rekening </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>

                <!--VOUCHER-->
                <div role="tabpanel" class="tab-pane fade" id="voucher">
                    <br>
                    <form class ="form-horizontal">
                        <div class="table-responsive">
                            <table id="tbl_voucher" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="success">
                                        <th>Kode Voucher</th>
                                        <th>Kode Voucher Pengganti</th>
                                        <th>Aktif</th>
                                        <th>Kadaluarsa</th>
                                        <th>Nominal</th>
                                        <th>Minimum Belanja</th>
                                        <th>Terpakai di Order</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (isset($data_sel[LIST_DATA][5])) {
                                        foreach ($data_sel[LIST_DATA][5] as $row) {
                                            echo '<tr><td>' . $row->code . '</td>';
                                            echo '<td>' . $row->code_refund . '</td>';
                                            echo '<td>' . cdate($row->active_date) . '</td>';
                                            echo '<td>' . cdate($row->exp_date) . '</td>';
                                            echo '<td>' . 'Rp. ' . cnum($row->nominal) . '</td>';
                                            echo '<td>' . 'Rp. ' . cnum($row->trx_use_amt) . '</td>';
                                            echo '<td>' . '<a href="' . base_url() . 'member/payment/' . $row->trx_no . '" target= ' . BLANK_TARGET . '>' . $row->trx_no . '</a></td></tr>';
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>

                <!--ORDER-->
                <div role="tabpanel" class="tab-pane fade" id="order">
                    <br>
                    <div class="row">
                        <form class ="form-horizontal" id="frmSearch2" method="post" url="<?php echo get_base_url() ?>member">
                            <!--                                    <div class="text-left">
                                                                    <div class="col-lg-3 col-xs-12">
                                                                        <div class="input-group-btn">
                                                                            <input class="form-control" type="text" name="order_no"   placeholder="Nomor transaksi">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-3 col-xs-12">
                                                                        <div class="input-group-btn">
                                                                            <input class="form-control" type="text" name ="order_date_between" id="reservation" placeholder="Pilih tanggal transaksi" readonly="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-4 col-xs-12">
                                                                        <div class="input-group-btn">
                                                                            <select class="form-control" style="width: 100%" name="payment_status">
                                                                                <option value="">Semua</option>
                            <?php
                            echo combostatus(json_decode(STATUS_PAYMENT));
                            ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group col-lg-2 col-xs-12">
                                                                        <div class="form-group col-lg-12">
                                                                            <button type="button" id="btnSearch3" class="btn btn-front btn-flat btn-md"><i class="fa fa-search"></i>&nbsp; Cari</button>
                                                                        </div>
                                                                    </div>
                                                                </div>-->
                            <div class="container">

                                <div class="form-group col-lg-4 col-xs-12 col-md-3 col-sm-4">

                                    <input class="form-control" type="text" name="order_no" id="search_order_no"  placeholder="Nomor transaksi">

                                </div>

                                <div class="form-group col-lg-4 col-xs-12 col-md-4 col-sm-4">

                                    <input class="form-control" type="text" name ="order_date_between" id="reservation" placeholder="Pilih tanggal transaksi" readonly="">

                                </div>

                                <div class="form-group row col-lg-3 col-xs-12 col-md-3 col-sm-3" >

                                    <select class="form-control"  name="payment_status">
                                        <option value="">Semua</option>
                                        <?php
                                        echo combostatus(json_decode(STATUS_PAYMENT));
                                        ?>
                                    </select>

                                </div>
                                &nbsp;
                                <div class="  col-lg-1 col-xs-12 col-md-1 col-sm-1">

                                    <button type="button" id="btnSearch3" class="form-group btn btn-front btn-flat btn-md "><i class="fa fa-search"></i>&nbsp; Cari</button>

                                </div>

                            </div>
                            &nbsp;
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table id="tbl2" class="display table table-bordered table-striped"
                                           cellspacing="0" width="100%">
                                        <thead>
                                            <tr class="success">
                                                <th column="order_date"> Tgl. Order </th>
                                                <th column="order_no"> No. Order </th>
                                                <th column="total_order"> Total Order </th>
                                                <th column="payment_status"> Status Bayar </th>
                                                <th column="paid_date"> Tanggal Bayar </th>
                                                <th column="payment_method"> Metode Pembayaran</th>
                                                <!--<th column="detail"> Produk </th>-->
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!--CONFIG-->
                <div role="tabpanel" class="tab-pane fade" id="configuration">
                    <br>
                    <form class="form-horizontal text-left" id="frmConfig" method="post" action="<?php echo base_url() ?>member/update">
                        <input name ="tab" type="hidden" value="<?php echo TAB_CONFIG ?>">
                        <div class="form-group ">
                            <label  class="col-sm-2 control-label" style="text-align:left">Email</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" value="<?php echo ($data_sel[LIST_DATA][0][0]->email) ?>" disabled="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label  class="col-sm-2 control-label" style="text-align:left">Password Lama</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" name="old_password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-2 control-label" style="text-align:left">Password Baru</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" id="newpass" name="new_password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-2 control-label" style="text-align:left">Konfirmasi Password</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" name="confirm_password">
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-front btn-flat btn-md" ><i class="fa fa-save"></i>&nbsp; Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!--RETURN-->
                <div role="tabpanel" class="tab-pane fade" id="return">
                    <br>
                    <div class="form-group">
                        <button id="btnadd" class="btn btn-flat" style="background-color: #216275; color: #FFFFFF" >Tambah</button>
                    </div>


                    <form class="form-horizontal text-left" id="frmReturn" method="post" action="<?php echo base_url() ?>member/product_return">
                        <div id="search_order" style=" display: none">
                            <hr>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label col-lg-3 " style=" text-align: left;">No. Order</label>
                                    <div class="col-lg-9">
                                        <div class="input-group " >
                                            <input id="r_order_no" style=" text-align: left;" class="form-control" type="text" name ="r_order_no" >
                                            <span class="input-group-btn">
                                                <button id="btnSearch4" type="button"class="form-group btn btn-front btn-flat btn-md"><i class="fa fa-search"></i>&nbsp; Cari</button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <table id="tbl4" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="success">
                                        <th column="img" style="width: 20%;"> Produk </th>
                                        <th column="qty"> Qty </th>
                                        <th column="checked">Pilih</th>
                                        <th column="sell_price"> Harga Produk (Rp)</th>
                                        <th column="merchant_name"> Merchant </th>
                                    </tr>
                                </thead>
                            </table>
                            <hr>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label col-lg-3 " style=" text-align: left;">Expedisi</label>
                                    <div class="col-lg-9">
                                        <?php include get_component_url() . '/dropdown/com_drop_expedition.php' ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-3" style=" text-align: left;">No. Resi</label>
                                    <div class="col-lg-9">
                                        <input  id="r_order_no" class="form-control" type="text" name ="r_resi_no"  validate="required[]">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="contorl-label col-lg-3"></label>
                                    <div class="col-lg-9">
                                        <textarea rows="3" class="form-control" style="overflow:auto;resize:none;width: 100%;max-width: 100%"  placeholder="Komentar" name="r_comment"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-9">
                                        <button type="submit" name="btnSaveAdd" class="btn btn-front btn-flat btn-md"><i class="fa fa-save"></i>&nbsp; Simpan</button>
                                    </div>
                                </div>
                            </div>
                            <hr>
                        </div><br>
                        <input name ="tab" type="hidden" >
                        <table id="tbl3" class="display table table-bordered table-striped" cellspacing="0" width="100%" >
                            <thead>
                                <tr class="success">
                                    <th column="product_name"> Produk </th>
                                    <th column="qty"> Qty </th>
                                    <th column="created_date"> Tgl. Pengajuan</th>
                                    <th column="return_no"> No. Pengembalian </th>
                                    <th column="awb_member_no"> No. Resi Member </th>
                                    <th column="accepted"> Terima </th>
                                    <th column="return_status"> Status Pengembalian </th>
                                    <th column="shipment_status"> Status Pengiriman </th>
                                    <th column="order_no"> No. Order </th>
                                    <th column="review_member"> Komentar Member </th>
                                    <th column="review_admin"> Komentar Admin </th>
                                    <th column="review_merchant"> Komentar Merchant </th>
                                </tr>
                            </thead>
                        </table>
                    </form>
                </div>

                <!--REVIEW-->
                <div role="tabpanel" class="tab-pane fade" id="review"><br />
                    <form class="form-horizontal text-left" id="frmReview" method="post" action="member/update">
                        <input name ="tab" type="hidden" value="<?php echo TAB_REVIEW; ?>">
                        <ul class="list-group">
                            <?php
                            if (isset($data_sel[LIST_DATA][6][1])) {
                                $i = 0;
                                foreach ($data_sel[LIST_DATA][6][1] as $value) {
                                    $i++;
                                    ?><li class="list-group-item">
                                        <input type="hidden" name="orseq[]" value="<?php echo $value->order_seq; ?>" />
                                        <input type="hidden" name="pvseq[]" value="<?php echo $value->seq; ?>" />
                                        <div class="row">
                                            <div class="col-md-1">
                                                <a href ="<?php echo base_url() . url_title($value->product_name . " " . $value->seq); ?>">
                                                    <img src="<?php echo get_image_location() . PRODUCT_UPLOAD_IMAGE . $value->merchant_seq . "/" . XS_IMAGE_UPLOAD . $value->pic_1_img; ?>" class = "img-thumbnail">
                                                </a>
                                            </div>
                                            <div class="col-md-5">
                                                <a href ="<?php echo base_url() . url_title($value->product_name . " " . $value->seq); ?>"><b><?php echo $value->product_name; ?></b></a>
                                                <dl class="dl-horizontal">
                                                    <dt>No. Order :</dt>
                                                    <dd><?php echo $value->order_no; ?></dd>
                                                    <dt>Tanggal Order :</dt>
                                                    <dd><?php echo cdate($value->order_date); ?></dd>
                                                </dl>
                                            </div>
                                            <div class="col-md-4">
                                                <textarea name="review_text[]" class="form-control" cols="30" rows="2" placeholder="Ulasan Anda"></textarea>
                                            </div>
                                            <div class="col-md-2">
                                                <input type="hidden" class="rating" data-filled="fa fa-star fa-2x" data-empty="fa fa-star-o fa-2x" data-start="0" data-stop="5"/><br style="margin-bottom:10px;" />
                                                <button type="submit" class="btn btn-primary btn-sm">Kirim Ulasan</button>
                                            </div>
                                        </div>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </form>
                    <form class ="form-horizontal" id="frmSearch10" method="post" url="<?php echo get_base_url() ?>member">
                        <table id="tbl10" class="display table table-bordered table-striped" cellspacing="0" width="100%" >
                            <thead>
                                <tr class="success">
                                    <th column="product_name">Produk</th>
                                    <th column="order_date">Order</th>
                                    <th column="rate">Komentar</th>
                                </tr>
                            </thead>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">                                 //------------------------------------------ frmSearch1 --------------------------------------------------------------------------------------------
                var count_msg = '<?php echo $count_msg ?>';
                var table = '';
                var table2 = '';
                var table3 = '';
                var table4 = '';
                var table10 = '';
                var sorting = '<?php echo isset($tablesort); ?>';
                var error_html = '<div class="alert alert-error-home"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                var success_html = '<div class="alert alert-success-home"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                $(document).ready(function() {

                    $('.rating').each(function() {
                        $('<input type="hidden" name="star[]" value="0" class="star" />')
                                //.text($(this).val() || ' ')
                                .insertAfter(this);
                    });
                    $('.rating').on('change', function() {
                        $(this).next('.star').val($(this).val());
                    });

                    var url = $('#frmFill').attr('url');
                    var url2 = $('#frmSearch2').attr('url');
                    var columns_detail = [];
                    $('#tbl thead th').each(function() {
                        var sName = $(this).attr("column");
                        var bVisible = $(this).attr("visible");
                        var url = $(this).attr("url");
                        columns_detail.push({"sName": sName, "bVisible": bVisible, "mData": sName});
                    });
                    var columns_detail2 = [];
                    $('#tbl2 thead th').each(function() {
                        var sName = $(this).attr("column");
                        var bVisible = $(this).attr("visible");
                        var url2 = $(this).attr("url");
                        columns_detail2.push({"sName": sName, "bVisible": bVisible, "mData": sName});
                    });

                    var columns_detail3 = [];
                    $('#tbl3 thead th').each(function() {
                        var sName = $(this).attr("column");
                        var bVisible = $(this).attr("visible");
                        var url3 = $(this).attr("url");
                        columns_detail3.push({"sName": sName, "bVisible": bVisible, "mData": sName});
                    });

                    var columns_detail10 = [];
                    $('#tbl10 thead th').each(function() {
                        var sName = $(this).attr("column");
                        var bVisible = $(this).attr("visible");
                        var url10 = $(this).attr("url");
                        columns_detail10.push({"sName": sName, "bVisible": bVisible, "mData": sName});
                    });

                    $("ul#member_page li a[data-toggle=\"tab\"], ul#member_page_xs li a[data-toggle=\"tab\"]").on("shown.bs.tab", function(e) {
                        var tab_name = ($(this).attr('href'));
                        if (tab_name == '#contact') {
                            tabactivecontact();
                        }
                        if (tab_name == '#address') {
                            tabactiveaddress();
                        }
                        if (tab_name == '#bank') {
                            tabactivebank();
                        }
                        if (tab_name == '#deposit') {
                            tabactivedeposit();
                            if (!$.fn.DataTable.isDataTable('#tbl')) {
                                table = $('#tbl').DataTable({
                                    "oLanguage": {
                                        sProcessing: "<img src='<?php echo get_image_location() . ASSET_IMG_HOME ?>loading.gif' />"
                                    },
                                    "dom": '<"top"l>rt<"bottom"ip><"clear">',
                                    responsive: true, "processing": true,
                                    "serverSide": true,
                                    "aoColumns": columns_detail,
                                    "ajax": {
                                        "url": url,
                                        "type": 'POST',
                                        "dataType": 'JSON',
                                        "data": buildAjaxData,
                                        "error": handleAjaxError
                                    }
                                })
                                        .on('xhr.dt', function(e, settings, response, xhr) {
                                    response = JSON.stringify(response);
                                    if (isSessionExpired(response)) {
                                        response_object = json_decode(response);
                                        url = response_object.url;
                                        location.href = url;
                                    }
                                });
                            }
                        }
                        if (tab_name == '#voucher') {
                            tabactivevoucher();
                            if (!$.fn.DataTable.isDataTable('#tbl_voucher')) {
                                var table_voucher = $('#tbl_voucher').DataTable({
                                    "oLanguage": {
                                        sProcessing: "<img src='<?php echo get_image_location() . ASSET_IMG_HOME ?>loading.gif' />"
                                    },
                                    "dom": '<"top"l>rt<"bottom"ip><"clear">',
                                    responsive: true
                                })
                                        .on('xhr.dt', function(e, settings, response, xhr) {
                                    response = JSON.stringify(response);
                                    if (isSessionExpired(response)) {
                                        response_object = json_decode(response);
                                        url = response_object.url;
                                        location.href = url;
                                    }
                                });
                            }
                        }

                        if (tab_name == '#order') {
                            tabactiveorder();
                            if (!$.fn.DataTable.isDataTable('#tbl2')) {
                                table2 = $('#tbl2').DataTable({
                                    "oLanguage": {
                                        sProcessing: "<img src='<?php echo get_image_location() . ASSET_IMG_HOME ?>loading.gif' />"
                                    },
                                    "dom": '<"top"l>rt<"bottom"ip><"clear">',
                                    responsive: true,
                                    "processing": true,
                                    "serverSide": true,
                                    "aoColumns": columns_detail2,
                                    "ajax": {
                                        "url": url2,
                                        "type": 'POST',
                                        "dataType": 'JSON',
                                        "data": buildAjaxData2,
                                        "error": handleAjaxError2,
                                    }
                                })
                                        .on('xhr.dt', function(e, settings, response, xhr) {
                                    response = JSON.stringify(response);
                                    if (isSessionExpired(response)) {
                                        response_object = json_decode(response);
                                        url = response_object.url;
                                        location.href = url;
                                    }
                                });
                            }
                        }
                        if (tab_name == '#configuration') {
                            tabactiveconfig();
                        }
                        if (tab_name == '#review') {
                            tabactiveview();
                            if (!$.fn.DataTable.isDataTable('#tbl10')) {
                                table10 = $('#tbl10').DataTable({
                                    "oLanguage": {
                                        sProcessing: "<img src='<?php echo get_image_location() . ASSET_IMG_HOME ?>loading.gif' />"
                                    },
                                    "dom": '<"top"l>rt<"bottom"ip><"clear">',
                                    responsive: true,
                                    "processing": true,
                                    "serverSide": true,
                                    "aoColumns": columns_detail10,
                                    "ajax": {
                                        "type": 'POST',
                                        "dataType": 'JSON',
                                        "data": buildAjaxData10,
                                        "error": handleAjaxError10,
                                    }
                                })
                                        .on('xhr.dt', function(e, settings, response, xhr) {
                                    response = JSON.stringify(response);
                                    if (isSessionExpired(response)) {
                                        response_object = json_decode(response);
                                        url = response_object.url;
                                        location.href = url;
                                    }
                                });
                            }

                        }
                        if (tab_name == '#return') {
                            var url3 = $('#frmReturn').attr('action');
                            tabactivereturn();
                            if (!$.fn.DataTable.isDataTable('#tbl3')) {
                                table3 = $('#tbl3').DataTable({
                                    oLanguage: {
                                        sProcessing: "<img src='<?php echo get_image_location() . ASSET_IMG_HOME ?>loading.gif' />"
                                    },
                                    dom: '<"top"lB>rt<"bottom"ip><"clear">',
                                    responsive: true,
                                    processing: true,
                                    serverSide: true,
                                    aoColumns: columns_detail3,
                                    "bLengthChange": false,
                                    buttons: ['csvHtml5', 'excelHtml5', 'pdfHtml5'],
                                    ajax: {
                                        url: url3,
                                        type: 'POST',
                                        dataType: 'JSON',
                                        data: buildAjaxData3,
                                        error: handleAjaxError3
                                    }
                                })
                                        .on('xhr.dt', function(e, settings, response, xhr) {
                                    response = JSON.stringify(response);
                                    if (isSessionExpired(response)) {
                                        response_object = json_decode(response);
                                        url = response_object.url;
                                        location.href = url;
                                    }
                                });
                            }
                        }
                    });

                    function handleAjaxError(request, textStatus, error) {
                        if (textStatus === 'timeout') {
                            alert('The server took too long to send the data.');
                        }
                        else {
                            var response = error_response(request.status);
                            $('#error-message').html(error_html + " " + response + "</div>");
                            $('div#tbl_processing').css('visibility', 'hidden');
                        }
                    }

                    function handleAjaxSuccess(response) {
                        alert(response);
                    }

                    function buildAjaxData() {
                        var settings = $("#tbl").dataTable().fnSettings();
                        var search = [];
                        search = $('#frmFill').serializeArray();
                        search.push(
                                {name: "draw", value: settings.iDraw},
                        {name: "start", value: settings._iDisplayStart},
                        {name: "length", value: settings._iDisplayLength},
                        {name: "column", value: columns_detail[settings.aaSorting[0][0]].sName},
                        {name: "order", value: settings.aaSorting[0][1]},
                        {name: "btnSearch", value: "true"}
                        );
                        return search;
                    }

                    $('#tbl tbody').on('click', 'tr', function() {
                        if ($(this).hasClass('selected')) {
                            $(this).removeClass('selected');
                        }
                        else {
                            table.$('tr.selected').removeClass('selected');
                            $(this).addClass('selected');
                        }
                    });
                    //
                    //                        $('#tbl').DataTable({
                    //                        });
                    function handleAjaxError2(request, textStatus, error) {
                        if (textStatus === 'timeout') {
                            alert('The server took too long to send the data.');
                        }
                        else {
                            var response = error_response(request.status);
                            $('#error-message').html(error_html + " " + response + "</div>");
                            $('div#tbl2_processing').css('visibility', 'hidden');
                        }
                    }

                    function handleAjaxSuccess2(response, textStatus, error) {
                        return response;
                    }

                    function buildAjaxData2() {
                        var settings = $("#tbl2").dataTable().fnSettings();
                        var search2 = [];
                        if (sorting != '') {
                            sorting = "";
                            settings.aaSorting = [[0, 'desc']];
                        }

                        search2 = $('#frmSearch2').serializeArray();
                        search2.push(
                                {name: "draw", value: settings.iDraw},
                        {name: "start", value: settings._iDisplayStart},
                        {name: "length", value: settings._iDisplayLength},
                        {name: "column", value: columns_detail2[settings.aaSorting[0][0]].sName},
                        {name: "order", value: settings.aaSorting[0][1]},
                        {name: "btnSearch2", value: "true"}
                        );
                        return search2;
                    }

                    $('#tbl2 tbody').on('click', 'tr', function() {
                        if ($(this).hasClass('selected')) {
                            $(this).removeClass('selected');
                        }
                        else {
                            table.$('tr.selected').removeClass('selected');
                            $(this).addClass('selected');
                        }
                    });
                    $('#btnSearch3').on('click', function() {
                        table2.ajax.reload();
                    })

                    $('#tbl2 tbody').on('click', 'tr', function() {
                        if ($(this).hasClass('selected')) {
                            $(this).removeClass('selected');
                        }
                        else {
                            table.$('tr.selected').removeClass('selected');
                            $(this).addClass('selected');
                        }
                    });

                    function handleAjaxError3(request, textStatus, error) {
                        if (textStatus === 'timeout') {
                            alert('The server took too long to send the data.');
                        }
                        else {
                            var response = error_response(request.status);
                            $('#error-message').html(error_html + " " + response + "</div>");
                            $('div#tbl_processing').css('visibility', 'hidden');
                        }
                    }
                    function handleAjaxSuccess3(response) {
                        alert(response);
                    }

                    function buildAjaxData3() {
                        var settings = $("#tbl3").dataTable().fnSettings();
                        var search3 = [];
                        search3 = $('#frmReturn').serializeArray();
                        search3.push(
                                {name: "draw", value: settings.iDraw},
                        {name: "start", value: settings._iDisplayStart},
                        {name: "length", value: settings._iDisplayLength},
                        {name: "column", value: columns_detail3[settings.aaSorting[0][0]].sName},
                        {name: "order", value: settings.aaSorting[0][1]},
                        {name: "btnSearch", value: "true"}
                        );
                        return search3;
                    }


//////////////////
                    function handleAjaxError10(request, textStatus, error) {
                        if (textStatus === 'timeout') {
                            alert('The server took too long to send the data.');
                        }
                        else {
                            var response = error_response(request.status);
                            $('#error-message').html(error_html + " " + response + "</div>");
                            $('div#tbl10_processing').css('visibility', 'hidden');
                        }
                    }

                    function handleAjaxSuccess10(response, textStatus, error) {
                        return response;
                    }

                    function buildAjaxData10() {
                        var settings = $("#tbl10").dataTable().fnSettings();
                        var search10 = [];
                        if (sorting != '') {
                            sorting = "";
                            settings.aaSorting = [[0, 'desc']];
                        }
                        search10 = $('#frmSearch10').serializeArray();
                        search10.push(
                                {name: "draw", value: settings.iDraw},
                        {name: "start", value: settings._iDisplayStart},
                        {name: "length", value: settings._iDisplayLength},
                        {name: "column", value: columns_detail10[settings.aaSorting[0][0]].sName},
                        {name: "order", value: settings.aaSorting[0][1]},
                        {name: "btnSearch10", value: "true"}
                        );
                        return search10;
                    }

                    $('#tbl10 tbody').on('click', 'tr', function() {
                        if ($(this).hasClass('selected')) {
                            $(this).removeClass('selected');
                        }
                        else {
                            table.$('tr.selected').removeClass('selected');
                            $(this).addClass('selected');
                        }
                    });
///////////////////

                    $('#tbl3 tbody').on('click', 'tr', function() {
                        if ($(this).hasClass('selected')) {
                            $(this).removeClass('selected');
                        }
                        else {
                            table3.$('tr.selected').removeClass('selected');
                            $(this).addClass('selected');
                        }
                    });
                });

//                        $(document).ready(function() {
//                            var url = $('#frmSearch2').attr('url');
//                            var columns_detail2 = [];
//                            $('#tbl2 thead th').each(function() {
//                                var sName = $(this).attr("column");
//                                var bVisible = $(this).attr("visible");
//                                var url = $(this).attr("url");
//                                columns_detail2.push({"sName": sName, "bVisible": bVisible, "mData": sName});
//                            });
//                            $("ul#member_page li a[data-toggle=\"tab\"]").on("shown.bs.tab", function(e) {
//                                var tab_name = ($(this).attr('href'));
//                                if (tab_name == '#order') {
//                                    if (!$.fn.DataTable.isDataTable('#tbl2')) {
//                                        var table2 = $('#tbl2').DataTable({
//                                            "dom": '<"top"l>rt<"bottom"ip><"clear">',
//                                            responsive: true,
//                                            "processing": true,
//                                            "serverSide": true,
//                                            "aoColumns": columns_detail2,
//                                            "ajax": {
//                                                "url": url,
//                                                "type": 'POST',
//                                                "dataType": 'JSON',
//                                                "data": buildAjaxData2,
//                                                "error": handleAjaxError2
//                                            }
//                                        });
//                                    }
//                                }
//                            });
//                            function handleAjaxError2(xhr, textStatus, error) {
//                                if (textStatus === 'timeout') {
//                                    alert('The server took too long to send the data.');
//                                }
//                                else {
//                                    alert(xhr.responseText);
//                                }
//                            }
//                            function handleAjaxSuccess2(response) {
//                                alert(response);
//                            }
//
//                            function buildAjaxData2() {
//                                var settings = $("#tbl2").dataTable().fnSettings();
//                                var search2 = [];
//                                search2 = $('#frmSearch2').serializeArray();
//                                search2.push(
//                                        {name: "draw", value: settings.iDraw},
//                                {name: "start", value: settings._iDisplayStart}, {name: "length", value: settings._iDisplayLength},
//                                {name: "column", value: columns_detail2[settings.aaSorting[0][0]].sName},
//                                {name: "order", value: settings.aaSorting[0][1]}, {name: "btnSearch2", value: "true"}
//                                );
//                                return search2;
//                            }
//------------------------------------------ frmConfig --------------------------------------------------------------------------------------------
                $('#frmConfig').validate({
                    highlight: function(element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function(element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function(error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    },
                    rules: {
                        old_password: {
                            required: true,
                            maxlength: 20
                        },
                        new_password: {
                            required: true,
                            minlength: 8,
                            maxlength: 20
                        },
                        confirm_password: {
                            equalTo: "#newpass",
                            required: true,
                            minlength: 8,
                            maxlength: 20
                        }
                    }
                });
//-----------------------------------------------------------------------------------------------------------------------------------------------
                $('#reservation').daterangepicker({
                    format: 'DD-MMM-YYYY'});
//                    $('input[name="birthday"]').daterangepicker({
//                        singleDatePicker: true,
//                        showDropdowns: true,
//                        format: 'DD-MMM-YYYY'});

                $("[data-mask]").inputmask();
                function edit_address(address_seq) {
                    $.ajax({
                        url: "<?php echo base_url() ?>member/address/edt",
                        type: "POST",
                        data: {address_seq: address_seq},
                        success: function(response) {
                            if (isSessionExpired(response)) {
                                response_object = json_decode(response);
                                url = response_object.url;
                                location.href = url;
                            } else {
                                $('#address_modal').modal('hide');
                                $("#address_modal").remove();
                                $('.modal-backdrop').remove();
                                $(response).modal('show');
                            }
                        },
                        error: function(request, error) {
                            alert(error_response(request.status));
                        }
                    })
                }

                function add_address() {
                    $.ajax({
                        url: "<?php echo base_url() ?>member/address/edt",
                        type: "POST",
                        success: function(response) {
                            if (isSessionExpired(response)) {
                                response_object = json_decode(response);
                                url = response_object.url;
                                location.href = url;
                            } else {
                                $('#address_modal').modal('hide');
                                $("#address_modal").remove();
                                $('.modal-backdrop').remove();
                                $(response).modal('show');
                            }
                        },
                        error: function(request, error) {
                            alert(error_response(request.status));
                        }
                    })
                }

                function delete_address(address_seq) {
                    if (address_seq == undefined) {
                        alert("Harap pilih data dulu !");
                    } else {
                        if (confirm("Apakah anda yakin menghapus data yang dipilih ?")) {
                            $.ajax({
                                url: "<?php echo base_url() ?>member/address/del",
                                type: "POST",
                                data: {address_seq: address_seq},
                                success: function(response) {
                                    if (isSessionExpired(response)) {
                                        response_object = json_decode(response);
                                        url = response_object.url;
                                        location.href = url;
                                    } else {
                                        $('#' + address_seq).remove();
                                    }
                                },
                                error: function(request, error) {
                                    alert(error_response(request.status));
                                }
                            })
                        }
                    }
                }

                var isValid = $('#frmMain').validate({
                    ignore: 'input[type=hidden]',
                    highlight: function(element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function(element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function(error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else if (element.hasClass('select2')) {
                            error.insertAfter(element.next('span'));
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
                $(function() {
                    $('#profile-image').on('click', function() {
                        $('#profile-image-upload').click();
                    });
                });
                function previewimg(thisval) {
                    if (thisval.files && thisval.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $('#profile-image').attr('src', e.target.result).width('165').height('125');
                        }
                        reader.readAsDataURL(thisval.files[0]);
                    }
                    $("#btnupload").show();
                }

                function uploadfile() {
                    $("#profile-upload").submit();
                }

                var isFrmFillValid = $('#frmFill').validate({
                    highlight: function(element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function(element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function(error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    },
                    rules: {
                        member_name: {
                            required: true
                        },
                    }
                });
                $(document).on('submit', '#frmFill', function(e) {
                    if (isFrmFillValid.valid()) {
                        $('input[type="text"]').each(function() {
                            if ($(this).hasClass('auto') || $(this).hasClass('auto_int') || $(this).hasClass('auto_dec'))
                            {
                                var v = $(this).autoNumeric('get');
                                $(this).autoNumeric('destroy');
                                $(this).val(v);
                            }
                        });
                    }
                });
                $('.auto_int').autoNumeric('init', {vMin: 0, mDec: 0});
                $(document).ready(function() {
                    $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
                        localStorage.setItem('activeTab', $(e.target).attr('href'));
                    });
                    var activeTab = localStorage.getItem('activeTab');
                    if (activeTab) {
                        $('#member_page a[href="' + activeTab + '"]').tab('show');
                    }
                });
                $(document).ready(function() {
                    $("#btnadd").click(function() {
                        $("#search_order").toggle();
                    });
                });
//-----------------------
                var url4 = $('#frmReturn').attr('action');
                var columns_detail4 = [];
                $('#tbl4 thead th').each(function() {
                    var sName = $(this).attr("column");
                    var bVisible = $(this).attr("visible");
                    columns_detail4.push({"sName": sName, "bVisible": bVisible, "mData": sName});
                });
                $('#btnSearch4').on('click', function() {
                    if (!$.fn.DataTable.isDataTable('#tbl4')) {
                        table4 = $('#tbl4').DataTable({
                            "oLanguage": {
                                sProcessing: "<img src='<?php echo get_image_location() . ASSET_IMG_HOME ?>loading.gif' />"
                            },
                            "dom": '<"top"l>rt<"bottom"ip><"clear">',
                            responsive: true,
                            "processing": true,
                            "serverSide": true,
                            "aoColumns": columns_detail4,
                            "bLengthChange": false,
                            "ajax": {
                                "url": url4,
                                "type": 'POST',
                                "dataType": 'JSON',
                                "data": buildAjaxData4,
                                "error": handleAjaxError4,
                            }
                        })
                                .on('xhr.dt', function(e, settings, response, xhr) {
                            response = JSON.stringify(response);
                            if (isSessionExpired(response)) {
                                response_object = json_decode(response);
                                url = response_object.url;
                                location.href = url;
                            }
                        });
                    }
                    table4.ajax.reload();
                });
                function reset_other_checkbox(active_checkbox) {
                    if ($('#printc_' + active_checkbox).is(':checked')) {
                        $('[id^=printc]:not(#printc_' + active_checkbox + ')').attr("disabled", true);
                        $('[id^=prints]:not(#prints_' + active_checkbox + ')').attr("disabled", true);
                        $('[id^=printc]').prop('checked', false);
                        $('#printc_' + active_checkbox).prop('checked', true);
                    } else {
                        $('[id^=printc]').attr("disabled", false);
                        $('[id^=prints]').attr("disabled", false);
                        $('#printc_' + active_checkbox).prop('checked', false);
                    }

                }
                function handleAjaxError4(request, textStatus, error) {
                    if (textStatus === 'timeout') {
                        alert('The server took too long to send the data.');
                    }
                    else {
                        var response = error_response(request.status);
                        $('#error-message').html(error_html + " " + response + "</div>");
                        $('div#tbl_processing').css('visibility', 'hidden');
                    }
                }


                function buildAjaxData4() {
                    var r_order_no = $('#r_order_no').val();
                    var settings = $("#tbl4").dataTable().fnSettings();
                    var search = [];
                    search = $('#frmReturn').serializeArray();
                    search.push(
                            {name: "draw", value: settings.iDraw},
                    {name: "start", value: settings._iDisplayStart},
                    {name: "length", value: settings._iDisplayLength},
                    {name: "column", value: columns_detail4[settings.aaSorting[0][0]].sName},
                    {name: "order", value: settings.aaSorting[0][1]},
                    {name: "btnAdditional", value: "true"},
                    {name: "r_order_no", value: r_order_no}
                    )
                    return search;
                }

                $('#tb4 tbody').on('click', 'tr', function() {
                    if ($(this).hasClass('selected')) {
                        $(this).removeClass('selected');
                    }
                    else {
                        table4.$('tr.selected').removeClass('selected');
                        $(this).addClass('selected');
                    }
                });

                var isValid = $('#frmReturn').validate({
                    ignore: 'input[type=hidden]',
                    highlight: function(element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function(element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function(error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else if (element.hasClass('select2')) {
                            error.insertAfter(element.next('span'));
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
                $(document).on('submit', '#frmReturn', function(e) {
                    if (isValid.valid()) {
                        $('input[type="text"]').each(function() {
                            if ($(this).hasClass('auto') || $(this).hasClass('auto_int') || $(this).hasClass('auto_dec'))
                            {
                                var v = $(this).autoNumeric('get');
                                $(this).autoNumeric('destroy');
                                $(this).val(v);
                            }
                        });
                    }
                });
                
                function shipment(return_no) {
                    var url_action = $('#frmReturn').attr('action');
                    var data = [];
                    if (return_no == undefined) {
                        alert("Harap pilih data dulu !");
                    } else {
                        data.push(
                                {name: "btnSaveEdit", value: true},
                        {name: "return_no", value: return_no});
                        if (confirm("Apakah anda yakin terima data yang dipilih ?")) {
                            $.ajax({
                                url: url_action,
                                type: "POST",
                                data: data,
                                dataType: "json",
                                success: function(response) {
                                    if (isSessionExpired(response)) {
                                        response_object = json_decode(response);
                                        url = response_object.url_action;
                                        location.href = url_action;
                                    } else {
                                        if (response.error == true) {
                                            $('#error-message').html(error_html + response.message + "</div>")
                                        } else {
                                            $('#error-message').html(success_html + "Data telah berhasil di simpan !" + "</div>")
                                            table3.ajax.reload();
                                        }
                                        ;
                                    }
                                },
                                error: function(request, error) {
                                    $('#error-message').html(error_html + " Data Tidak Berhasil Diproses ! </div>")
                                    table.ajax.reload();
                                }

                            });
                        }
                    }
                }
                /*                $('#frmReturn').submit(function(){
                 alert('a');
                 return false;
                 });*/
</script>