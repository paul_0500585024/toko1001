<?php
require_once VIEW_BASE_ADMIN;
?>

<form id="frmSearch" url="<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
    <div class="form-group">
        <div>
            <label>Ekspedisi</label>
            <?php require_once get_component_url() . "dropdown/com_drop_expedition.php" ?>
        </div>
        <div>
            <label class ="control-label">No. Faktur</label>
            <input class="form-control" name="invoice_no" type="input">
        </div>
        <div>
            <label class ="control-label">Status</label>
            <select class="form-control select2" name="status">
                <option value="">-- Pilih --</option>
                <?php
                echo combostatus(json_decode(STATUS_RECEIPT));
                ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <?php echo get_search_button(); ?>
    </div>
</form>
