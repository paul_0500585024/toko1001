<?php
require_once VIEW_BASE_ADMIN;
?>
<link rel="stylesheet" type="text/css" href="<?php echo get_css_url() ?>daterangepicker-bs3.css" />
<script type="text/javascript" src="<?php echo get_js_url() ?>moment.min.js"></script>
<script type="text/javascript" src="<?php echo get_js_url() ?>daterangepicker.js"></script>
<form id ="frmSearch"  url= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
    <div class="form-group">
        <label>Dari Tanggal</label>
        <input class="form-control" name="tanggal1" type="input" readonly style="background-color: #fff;">
    </div>
    <div class="form-group">
        <label>Ke Tanggal</label>
        <input class="form-control" name="tanggal2" type="input" readonly style="background-color: #fff;">
    </div>
    <div class="form-group">
        <label>Status</label>
	<select class="form-control" name="status">
	    <option value="">Semua</option>
	    <?php
	    echo combostatus(json_decode(STATUS_OPC));
	    ?>
	</select>
    </div>
    <div class="form-group">
	<?php echo get_search_button(); ?>
    </div>
</form>
<script  type="text/javascript">
    $('input[name="tanggal1"]').daterangepicker({
	format: 'DD-MMM-YYYY',
	singleDatePicker: true,
	showDropdowns: true
    });
    $('input[name="tanggal2"]').daterangepicker({
	format: 'DD-MMM-YYYY',
	singleDatePicker: true,
	showDropdowns: true
    });
</script>