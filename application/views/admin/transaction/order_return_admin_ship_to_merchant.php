<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
?>

<link rel="stylesheet" type="text/css" href="<?php echo get_css_url() ?>daterangepicker-bs3.css" />
<script type="text/javascript" src="<?php echo get_js_url() ?>moment.min.js"></script>
<script type="text/javascript" src="<?php echo get_js_url() ?>daterangepicker.js"></script>

<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
    </div>
    <div class="box-body">
        <form id="frmMain" class ="form-horizontal" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
            <?php echo get_csrf_admin_token(); ?>
            <input type="hidden" name ="shipment_status" value="<?php echo ($data_head[LIST_DATA][0]->shipment_status) ?>"/>
            <div class ="col-lg-6">
                <div class ="form-group">
                    <label class="control-label col-md-4">No. Pengembalian</label>
                    <div class ="col-md-8">
                        <input class="form-control"  name ="return_no" 
                               value="<?php echo (isset($data_head[LIST_DATA][0]) ? $data_head[LIST_DATA][0]->return_no : "") ?>" readonly>
                    </div>
                </div>
                <!--                <div class ="form-group">
                                    <label class="control-label col-md-4">Tgl. Pengajuan</label>
                                    <div class ="col-md-8">
                                        <input class="form-control" name ="created_date" 
                                               value="<?php // echo (isset($data_head[LIST_DATA][0]) ? cdate($data_head[LIST_DATA][0]->created_date) : "")  ?>" readonly>
                                    </div>
                                </div>
                                <div class ="form-group">
                                    <label class="control-label col-md-4">Tgl. Pengiriman</label>
                                    <div class ="col-md-8">
                                        <input class="form-control" id="datepicker" validate ="required[]" name ="date" type="text" 
                                               value ="<?php // echo (isset($data_head[LIST_DATA][0]) ? cdate($data_head[LIST_DATA][0]->date) : "" );  ?>" readonly="">
                                    </div>
                                </div>-->
                <!--            </div>
                            <div class ="col-lg-6">-->
                <div class ="form-group">
                    <label class="control-label col-md-4">Ekspedisi *</label>
                    <div class ="col-md-8">
                        <?php require_once get_component_url() . "dropdown/com_drop_expedition.php" ?>
                    </div>
                </div>
                <div class ="form-group">
                    <label class="control-label col-md-4">No. Resi *</label>
                    <div class ="col-md-8">
                        <input class="form-control" type="text" validate ="required[]" name ="awb_admin_no" 
                               value ="<?php echo (isset($data_head[LIST_DATA][0]) ? ($data_head[LIST_DATA][0]->awb_admin_no) : "" ); ?>"/>
                    </div>
                </div>
                <div class ="form-group">
                    <label class="control-label col-md-4">Komentar</label>
                    <div class ="col-md-8">
                        <textarea class="form-control" style="overflow:auto;resize:none" type="text" validate ="required[]"
                                  name ="review_admin"><?php echo (isset($data_head[LIST_DATA][0]) ? ($data_head[LIST_DATA][0]->review_admin) : "" ); ?></textarea>
                    </div>
                </div>
                <div class ="form-group">
                    <?php
                    if ($data_head[LIST_DATA][0]->shipment_status == RECEIVED_BY_ADMIN ||
                            $data_head[LIST_DATA][0]->shipment_status == SHIP_TO_MERCHANT) {
                        ?>
                        <div class ="col-lg-6">
                            <?php echo get_save_edit_button(); ?>
                        </div>
                        <div class ="col-lg-6">
                            <a href="<?php echo base_url() ?>admin/transaction/order_return_admin" class="btn btn-google-plus" type="button" style="width: 100%">
                                <i class=" fa fa-arrow-circle-left"></i>
                                Kembali
                            </a>
                        </div>
                    <?php } else { ?>
                        <div class ="col-lg-6">
                            <a href="<?php echo base_url() ?>admin/transaction/order_return_admin" class="btn btn-google-plus" type="button" style="width: 100%">
                                <i class=" fa fa-arrow-circle-left"></i>
                                Kembali
                            </a>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </form>
    </div>
</div>

<?php
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>