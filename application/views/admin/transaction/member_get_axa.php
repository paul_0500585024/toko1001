<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
?>

<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
<?php
} else {
    $tablesort = 'desc';
    ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
    <?php require_once get_include_page_list_admin_content_header(); ?>
            <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th column="order_no"> No Order </th>
                        <th column="total_order"> Total Order </th>
                        <th column="created_date">  Tanggal Daftar </th>
                        <th column="member_name"> Nama sesuai KTP </th>
                        <th column="member_birthday"> Tanggal Lahir</th>
                        <th column="member_phone">  Nomor handphone </th>
                        <th column="member_email">  Alamat email </th>
                        <th column="gender">  Jenis kelamin </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <?php
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>