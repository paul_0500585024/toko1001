<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
?>

<link rel="stylesheet" type="text/css" href="<?php echo get_css_url() ?>daterangepicker-bs3.css" />
<script type="text/javascript" src="<?php echo get_js_url() ?>moment.min.js"></script>
<script type="text/javascript" src="<?php echo get_js_url() ?>daterangepicker.js"></script>

<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo $data_auth[FORM_ACTION_TITLE] . $data_auth[FORM_AUTH][FORM_TITLE] ?></h3>
        </div>
        <form class ="form-horizontal" id="frmMain" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
            <div class="box-body">
                <section class="col-md-6">
                    <?php echo get_csrf_admin_token(); ?>
                    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                        <input class="form-control" name="seq" type="hidden" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->seq : "" ); ?>">
                    <?php } ?>
                    <div class ="form-group">     
                        <label class ="control-label col-md-4">Ekspedisi</label>
                        <div class ="col-md-8">
                            <?php require_once get_component_url() . "dropdown/com_drop_expedition_receipt.php" ?>
                        </div>
                    </div>
                    <div class ="form-group">   
                        <label class="control-label col-md-4">No. Faktur</label>
                        <div class="col-md-8">
                            <input class="form-control" name="invoice_no" validate="required[]" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->invoice_no) : "" ); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Tanggal Faktur</label>
                        <div class="col-md-8"> 
                            <?php require_once get_component_url() . "date_picker/single_date_picker.php" ?>
                        </div>
                    </div>
                    <div class = "form-group">
                        <label class = "control-label col-md-4">Periode Pembayaran</label>
                        <div class = "col-md-8">
                            <?php require_once get_component_url() . "dropdown/com_drop_redeem_period.php" ?>
                        </div>
                    </div>
                    <div class ="form-group">
                        <?php if ($data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
                            <div class ="col-md-6"><?php echo get_back_button(); ?> </div>
                            <?php
                        } else {
                            if ($data_auth[FORM_ACTION] == ACTION_ADD) {
                                ?>
                                <div class ="col-md-6"><?php echo get_save_add_button(); ?> </div>
                            <?php } if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                                <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                            <?php } ?>
                            <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                        <?php } ?>
                    </div>
                </section>
            </div>
        </form>
    </div>
<?php } else { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <div class="box-body col-md-12">
                <?php require_once get_include_page_list_admin_content_header(); ?>
                <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th column="expedition_name"> Ekspedisi </th>
                            <th column="invoice_no"> No. Faktur </th>
                            <th column="invoice_date"> Tanggal Faktur </th>
                            <th column="status"> Status </th>
                            <th column="total_amt"> Total </th>
                            <th column="detail"> Informasi </th>
                            <th column="created_by"> <?php echo TH_CREATED_BY; ?> </th>
                            <th column="created_date"> <?php echo TH_CREATED_DATE; ?> </th>
                            <th column="modified_by"> <?php echo TH_MODIFIED_BY; ?> </th>
                            <th column="modified_date"> <?php echo TH_MODIFIED_DATE; ?> </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <?php
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>
