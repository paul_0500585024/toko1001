<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
?>

<link rel="stylesheet" type="text/css" href="<?php echo get_css_url() ?>daterangepicker-bs3.css" />
<script type="text/javascript" src="<?php echo get_js_url() ?>moment.min.js"></script>
<script type="text/javascript" src="<?php echo get_js_url() ?>daterangepicker.js"></script>

<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo ($data_auth[FORM_ACTION] == ACTION_ADD ? get_title_add($data_auth[FORM_AUTH][FORM_TITLE]) : get_title_edit($data_auth[FORM_AUTH][FORM_TITLE])); ?></h3>
        </div>
        <form class ="form-horizontal" id="frmMain" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
            <div class="box-body">
                <section class="col-md-12">
                    <?php echo get_csrf_admin_token(); ?>
                    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                        <input class="form-control" name ="seq" type="hidden" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->seq : "") ?>">
                        <input class="form-control" name ="nominal" type="hidden" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->nominal : "") ?>">
                        <input class="form-control" name ="total_payment" type="hidden" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->total_payment : "") ?>">
                        <input class="form-control" name ="total_order" type="hidden" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->total_order : "") ?>">
                        <input class="form-control" name ="conf_pay_amt_member" type="hidden" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->conf_pay_amt_member : "") ?>">
                        <input class="form-control" name ="conf_pay_date" type="hidden" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->conf_pay_date : "") ?>">
                        <input class="form-control" name ="pg_name" type="hidden" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->pg_name : "") ?>">
                        <input class="form-control" name ="bank_acct_no" type="hidden" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->bank_acct_no : "") ?>">
                        <input class="form-control" name ="bank_acct_name" type="hidden" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->bank_acct_name : "") ?>">
                        <input class="form-control" name ="logo_img" type="hidden" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->logo_img : "") ?>">
                        <input class="form-control" name ="conf_pay_note_file" type="hidden" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->conf_pay_note_file : "") ?>">
                        <input class="form-control" name ="conf_pay_bank_seq" type="hidden" value ="2">
                    <?php } ?>
                    <div class="row">
                        <div class="col-md-6">
                            <div class ="form-group">
                                <label class ="control-label col-md-4">No. Order :</label>
                                <div class ="col-md-6">
                                    <input class="form-control"  name ="order_no" value="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->order_no : "") ?>" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class ="control-label col-md-4">Tanggal Order :</label>
                            <div class ="col-md-6">
                                <input class="form-control"  name ="order_date" value="<?php echo (isset($data_sel[LIST_DATA]) ? cdate($data_sel[LIST_DATA][0]->order_date) : "") ?>" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Member :</label>
                                <div class="col-md-6">
                                    <input class="form-control" name="member_name" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->member_name : "") ?>" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label col-md-4">Status Pembayaran:</label>
                            <div class="col-md-6">
                                <input type="hidden" name ="order_payment_status" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->order_payment_status : "") ?>">
                                <input class="form-control" value="<?php echo (isset($data_sel[LIST_DATA]) ? cstdes($data_sel[LIST_DATA][0]->order_payment_status, STATUS_PAYMENT) : "") ?>" readonly>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <table style="width: 100%" class="diplay table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th> Gambar </th>
                                    <th> Merchant </th>
                                    <th> Produk </th>
                                    <th> Tipe Produk </th>
                                    <th> Harga Jual (Rp) </th>
                                    <th> Qty </th>
                                    <th> Total (Rp)</th>
                                </tr>
                                <?php
                                if (isset($data_sel[LIST_DATA][1])) {
                                    foreach ($data_sel[LIST_DATA][1] as $each) {
                                        ?>
                                        <tr valign="top">
                                            <td><a href="<?php echo get_base_url() . strtolower(url_title($each->product_name . ' ' . $each->variant_name)) . "-" . $each->product_varian_seq; ?>" target="_blank"><img src="<?php echo PRODUCT_UPLOAD_IMAGE ?><?php echo $each->merchant_seq; ?>/<?php echo $each->pic_1_img; ?>" width ="100px" height="100px" style="max-width:100px; max-height:100px"></a></td>
                                            <td><label class="control-label"><?php echo $each->merchant_name; ?></label></td>
                                            <td><label class="control-label"><?php echo $each->product_name; ?></label></td>
                                            <td><label class="control-label"><?php echo $each->variant_name; ?></label></td>
                                            <td><label class="control-label"><?php echo cnum($each->sell_price); ?></label></td>
                                            <td><label class="control-label"><?php echo cnum($each->qty); ?></label></td>
                                            <td><label class="control-label"><?php echo cnum(ceil($each->weight_kg * $each->qty) * $each->ship_price_charged + ($each->sell_price * $each->qty)); ?></label></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                <tr valign="top"><td colspan="5"></td><th>Total Pengiriman</th><th><?php echo (isset($data_sel[LIST_DATA]) ? cnum($data_sel[LIST_DATA][0]->total_ship) : "") ?></th></tr>
                                <tr valign="top"><td colspan="5"></td><th>Total</th><th><?php echo (isset($data_sel[LIST_DATA]) ? cnum($data_sel[LIST_DATA][0]->total_order) : "") ?></th></tr>
                                <?php if ($data_sel[LIST_DATA][0]->nominal != 0) { ?>
                                    <tr valign="top"><td colspan="5"></td><th>Voucher / Kupon</th><th><?php echo (isset($data_sel[LIST_DATA]) ? cnum($data_sel[LIST_DATA][0]->nominal) : "") ?></th></tr>
                                <?php } else { ?>
                                    <tr valign="top"><td colspan="5"></td><th>Voucher / Kupon</th><th><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->nominal : "") ?></th></tr>
                                <?php } ?>
                                <tr valign="top"><td colspan="5"></td><th>Total Bayar</th><th ><?php echo (isset($data_sel[LIST_DATA]) ? cnum($data_sel[LIST_DATA][0]->total_payment) : "") ?></th></tr>
                            </tbody>
                        </table>
                    </div>
                    </br>
                    <div>
                        <div class="row">
                            <table class="diplay table table-bordered table-striped">
                                <tbody>
                                    <tr>
                                        <th> Info Hasil Komfirmasi </th>
                                        <th> Info Bank </th>
                                        <th> Info Pembayaran </th>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr valign="top">
                                        <td> Jumlah Pembayaran  : <?php echo (isset($data_sel[LIST_DATA]) ? cnum($data_sel[LIST_DATA][0]->conf_pay_amt_member) : "") ?></td>
                                        <?php
                                        if (($data_sel[LIST_DATA][0]->logo_img == "")) {
                                            $image_1 = IMG_BLANK_100;
                                        } else {
                                            $image_1 = BANK_UPLOAD_IMAGE . $data_sel[LIST_DATA][0]->logo_img;
                                        }
                                        if (($data_sel[LIST_DATA][0]->conf_pay_note_file == "")) {
                                            $image_2 = IMG_BLANK_100;
                                        } else {
                                            if (isset($data_sel[LIST_DATA][0]->agent_seq)) {
                                                $image_2 = PROFILE_AGENT_IMAGE . $data_sel[LIST_DATA][0]->order_no . "/" . $data_sel[LIST_DATA][0]->conf_pay_note_file;
                                            } else {
                                                $image_2 = PROFILE_IMAGE . $data_sel[LIST_DATA][0]->order_no . "/" . $data_sel[LIST_DATA][0]->conf_pay_note_file;
                                            }
                                        }
                                        ?>
                                        <td rowspan="5"><img id="iprev" width ="40" height="40" src="<?php echo $image_1; ?>"></td>
                                        <td rowspan="5"><a href="<?php echo $image_2 ?>" target="_blank"><img width="150px" height="150px" src="<?php echo $image_2 ?>"></a></td>  
                                    </tr>
                                    <tr valign="top">
                                        <td> Tanggal Pembayaran : <?php echo (isset($data_sel[LIST_DATA]) ? cdate($data_sel[LIST_DATA][0]->conf_pay_date) : "") ?></td>
                                    </tr>
                                    <tr valign="top">
                                        <td> Pembayaran : <?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->pg_name : "") ?></td>
                                    </tr>
                                    <tr valign="top">
                                        <td> A/C :<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->bank_acct_no : "") ?></td>
                                    </tr>
                                    <tr valign="top">
                                        <td> A/N :<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->bank_acct_name : "") ?></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <?php if (isset($data_sel[LIST_DATA][0]->dp)) { ?>
                        <div class="row">
                            <div class="col-xs-6">
                                <strong>Perhitungan Angsuran</strong>
                                <div>Harga Barang
                                    <?php
                                    $total_price = $each->sell_price * $each->qty;
                                    ?>

                                    <span class="pull-right text-bold"> Rp.<?php echo number_format($total_price); ?></span></div>
                                <div>Uang Muka (DP) <span class="pull-right text-bold">Rp.<?php echo number_format($data_sel[LIST_DATA][0]->dp) ?></span></div>
                                <div><span id="min_dp" class="text-red pull-right"></span></div>
                                <div>Pokok Hutang  <span id="pokok_hutang" class="pull-right text-bold">Rp.<?php echo number_format($total_price - $data_sel[LIST_DATA][0]->dp) ?></span></div>
                                <div>Tenor <span class="pull-right text-bold"><?php echo number_format($data_sel[LIST_DATA][0]->tenor) ?> bulan</span></div>
                                <div>Bunga <span class="pull-right text-bold"><?php echo $data_sel[LIST_DATA][0]->loan_interest ?> %</span></div>
                                <div>Angsuran <span class="pull-right text-bold">Rp.<?php echo number_format($data_sel[LIST_DATA][0]->installment) ?></span></div>

                            </div>

                            <div class="col-xs-6">
                                <strong>Pembayaran Pertama</strong>
                                <div>Uang Muka (DP)  <span class="pull-right text-bold">Rp.<?php echo number_format($data_sel[LIST_DATA][0]->dp) ?></span></div>
                                <div>Angsuran I  <span class="pull-right text-bold">Rp.<?php echo number_format($data_sel[LIST_DATA][0]->installment) ?></span></div>
                                <div>Administrasi <span class="pull-right text-bold">Rp.<?php echo number_format($data_sel[LIST_DATA][0]->admin_fee) ?></span></div>
                                <div>Ongkos Kirim  <span class="pull-right text-bold">Rp.<?php echo cnum(ceil($each->weight_kg * $each->qty) * $each->ship_price_charged) ?></span></div>
                                <div>Total Pembayaran Pertama <input type="hidden" id="total_installment" name="total_installment" value="0">
                                    <span id="total_pertama" class="pull-right text-bold">Rp.<?php echo number_format($data_sel[LIST_DATA][0]->total_installment) ?></span></div>
                            </div>
                        </div>
                    <?php } ?>
                    </br>
                    <?php IF (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->order_payment_status == 'C' : "") { ?>
                        <div class="row">
                            <table class="diplay table table-bordered table-striped">
                                <tbody>
                                    <tr>
                                        <th> Status Bayar </th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-4">
                                    <select  class="form-control select2" id="payment_status" name="payment_status"  validate="required[]" onchange="leaveChange(this.value)" value="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->payment_status : "") ?>">
                                        <option value="">-- Pilih --</option>
                                        <?php
                                        echo combostatus(json_decode(STATUS_PAY));
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label class ="control-label col-md-3"></label>
                                    <div class ="col-md-9">
                                        <input  type="hidden" name="old_conf_pay_amt_admin">
                                        <input  type="text" name="conf_pay_amt_admin" class="form-control auto_int"  value="<?php isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->conf_pay_amt_member : 0 ?>" id="lenSeq" <?php echo (isset($data_sel[LIST_DATA][0]) ? (($data_sel[LIST_DATA][0]->payment_status == "X") ? "" : ' readonly') : " readonly" ); ?> >
                                    </div>
                                </div>
                                <div class ="col-md-2">
                                    <input type="text" date_type="date" name="paid_date" class="form-control" value ="<?php echo (isset($data_sel[LIST_DATA][2]) ? date("d-M-Y", strtotime($data_sel[LIST_DATA][2]->paid_date)) : "" ); ?>" readonly style="background-color: #fff;">
                                </div>
                            </div>
                        </div><br>
                    <?php } ?>
                    <div class ="form-group">
                        <?php
                        if ($data_auth[FORM_ACTION] == ACTION_VIEW) {
                            ?>
                            <div class ="col-md-6"><?php echo get_back_button(); ?> </div>
                            <?php
                        } else {
                            if ($data_auth[FORM_ACTION] == ACTION_ADD) {
                                ?>
                                <div class ="col-md-6"><?php echo get_save_add_button(); ?> </div>
                            <?php } if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                                <div class ="col-md-6"><?php echo isset($data_sel[LIST_DATA][0]) && $data_sel[LIST_DATA][0]->order_payment_status == 'C' ? get_save_edit_button() : ""; ?></div>
                            <?php } ?>
                            <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                        <?php } ?>
                    </div>
                </section>
            </div>
        </form>
    </div>
    <script  type="text/javascript">
            function previewimg(thisval) {
                if (thisval.files && thisval.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {

                        $('#iprev').attr('src', e.target.result).height(70);
                    }
                    reader.readAsDataURL(thisval.files[0]);
                }
            }
            $('input[date_type="date"]').daterangepicker({
                format: 'DD-MMM-YYYY',
                showDropdowns: true,
                singleDatePicker: true,
                startdate: "<?php echo date('Y-m-d') ?>",
                minDate: "<?php echo (isset($data_sel[LIST_DATA]) ? cdate($data_sel[LIST_DATA][0]->order_date) : "") ?>"
            });

            function leaveChange(value) {
                if (value == "P") {
                    $('#lenSeq').attr('readonly', false);
                    $('#lenSeq').val('<?php echo (isset($data_sel[LIST_DATA]) ? cnum($data_sel[LIST_DATA][0]->conf_pay_amt_member) : "") ?>');

                }
                else {
                    $('#lenSeq').attr('readonly', true);
                    $('#lenSeq').val('');

                }
            }

            function chMd(model)
            {
                $("#lenSeq").prop('readonly', model);
                if (model == true) {
                    $("#lenSeq").val("");

                }

            }
    </script>
    <?php
} else {
    $tablesort = 'desc';
    ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <?php require_once get_include_page_list_admin_content_header(); ?>
            <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th column="order_date"> Tanggal Order </th>
                        <th column="order_no"> No Order </th>
                        <th column="member_name"> Member </th>
                        <th column="payment_name"> Metode Pembayaran </th>
                        <th column="total_order"> Total Order </th>
                        <th column="voucher_nominal"> Voucher </th>
                        <th column="total_payment"> Total Bayar </th>
                        <th column="conf_pay_amt_member"> Jumlah Pembayaran </th>
                        <th column="payment_status"> Status Pembayaran </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <?php
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>