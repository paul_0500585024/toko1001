
<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
$x = isset($data_head[LIST_DATA][0]) && $data_head[LIST_DATA][0][0]->ship_price_charged != "" ? ceil(($data_head[LIST_DATA][0][0]->grand_total_weight) - ceil($data_head[LIST_DATA][0][0]->sub_weight)) * $data_head[LIST_DATA][0][0]->ship_price_charged : 0;
$y = isset($data_head[LIST_DATA][1]) ? ($data_head[LIST_DATA][1][0]->nominal) : 0;
$z = isset($data_head[LIST_DATA][0]) ? ($data_head[LIST_DATA][0][0]->sub_total) : 0;
?>
<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
    </div>
    <div class="box-body">
        <div class="row">
            <section class="col-md-8 col-lg-8">
                <?php require_once get_include_page_list_admin_content_header(); ?>        
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-md-4">Total Harga Produk (Rp)</label>
                        <div class="col-md-8"><input type="text" class="form-control" value="<?php echo number_format($z) ?>" readonly></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Biaya Pengiriman (Rp)</label>
                        <div class="col-md-8"><input type="text" class="form-control" value="<?php echo number_format($x) ?>" readonly></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Biaya Voucher (Rp)</label>
                        <div class="col-md-8"><input type="text" class="form-control" value="<?php echo number_format($y) ?>" readonly> </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Total Batal Keseluruhan (Rp)</label>
                        <div class="col-md-8"><input type="text" class="form-control" value="<?php echo number_format(($z + $x) - $y); ?>" readonly></div>
                    </div>
                </form>
            </section>
        </div>
        <br>
        <hr>
        <section class="col-md-12 col-lg-12">
            <div class="table-responsive">
                <table class="table table-bordered table-striped" cellspacing="0" width="100%" >
                    <thead>
                        <tr  class="warning"> 
                            <th >Nama Merchant</th>
                            <th >Nama Produk</th>
                            <th > Jumlah Produk </th>
                            <th > Harga Produk (Rp)</th>
                            <th > Harga Total (Rp) </th>
                            <th > Berat (KG) </th>
                            <th > Gambar Produk </th>
                            <th > Status </th>
                        </tr>
                    <tbody>
                        <?php if (isset($data_head[LIST_DATA][2])) { ?>
                            <?php foreach ($data_head[LIST_DATA][2][0] as $each): ?>
                                <tr>
                                    <td><?php echo $each->merchant_name ?></td>
                                    <td><?php echo $each->product_name ?></td>
                                    <td align="right"><?php echo number_format($each->qty) ?></td>
                                    <td align="right"><?php echo number_format($each->sell_price) ?></td>
                                    <td align="right"><?php echo number_format($each->sell_total) ?></td>
                                    <td align="center"><?php echo ($each->weight_kg) ?></td>
                                    <td align="center"><?php echo '<img style="width:50px;height:50px;" src="' . get_base_url() . PRODUCT_UPLOAD_IMAGE . $each->merchant_seq . '/' . $each->product_image . '">' ?></td>
                                    <td align="center">Dibatalkan Merchant</td>
                                </tr>
                            <?php endforeach; ?>
                        <?php } ?>
                    </tbody>
                    </thead>
                </table>
            </div>
            <a href="<?php echo get_base_url() . 'admin/master/agent/refund'; ?>" class="btn btn-google-plus">Kembali</a>
        </section>
    </div>
</div>
<?php
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>

