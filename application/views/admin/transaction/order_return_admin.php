<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
$tablesort = 'desc';
?>

<style>
    .panel-body{
        max-height: 280px;    
        overflow-y: scroll;
        height:280px;
    }

</style>

<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
    </div>
    <div class="box-body">
        <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) {
            ?>
            <section class="col-md-12 col-lg-12">
                <h3><p>Detail Pengembalian : </p></h3>
                <div class="well">
                    <div class="row">
                        <div class="col-md-6">
                            <p><span class="fa fa-tag fa-lg"></span>&nbsp; <label class="control-label"> No. Pengembalian : </label> <?php echo (isset($data_sel[LIST_DATA][0][0]) ? $data_sel[LIST_DATA][0][0]->return_no : "") ?></p>
                            <p><span class="fa fa-shopping-cart fa-lg"></span>&nbsp; <label class="control-label"> No. Order : </label> <?php echo (isset($data_sel[LIST_DATA][0][0]) ? $data_sel[LIST_DATA][0][0]->order_no : "") ?></p>
                            <p><span class="fa fa-calendar fa-lg"></span>&nbsp; <label class="control-label"> Tgl. Pengajuan : </label> <?php echo cdate(isset($data_sel[LIST_DATA][0][0]) ? $data_sel[LIST_DATA][0][0]->created_date : ""); ?> </p>
                        </div>
                        <div class="col-md-6">
                            <p>
                                <span class="fa fa-exclamation-circle fa-lg"></span>&nbsp; <label class="control-label"> Status Pengembalian  : </label>
                                <?php echo (isset($data_sel[LIST_DATA][0][0]) ? cstdes($data_sel[LIST_DATA][0][0]->return_status, STATUS_RETURN) : "") ?>
                            </p>
                            <p>
                                <span class="fa fa-truck fa-lg"></span>&nbsp; <label class="control-label"> Status Pengiriman  : </label>
                                <?php echo (isset($data_sel[LIST_DATA][0][0]) ? cstdes($data_sel[LIST_DATA][0][0]->shipment_status, STATUS_SHIPMENT) : "") ?>
                            <p>
                        </div>
                    </div>
                </div>
            </section>
            <section class="col-md-12 col-lg-12">
                <h3><p>Info Member : </p></h3>
                <div class="well">
                    <div class="row">
                        <div class="col-md-6">
                            <p><span class="fa fa-user fa-lg"></span>&nbsp;&nbsp;&nbsp;<label class="control-label"> Nama : </label> <?php echo (isset($data_sel[LIST_DATA][0][0]) ? $data_sel[LIST_DATA][0][0]->member_name : "") ?></p>
                            <p><span class="fa fa-envelope fa-lg"></span>&nbsp; <label class="control-label"> Email : </label> <?php echo (isset($data_sel[LIST_DATA][0][0]) ? $data_sel[LIST_DATA][0][0]->email_member : "") ?></p>                                    
                        </div>
                        <div class="col-md-6">                                    
                            <p><span class="fa fa-phone fa-lg"></span>&nbsp; <label class="control-label"> Telepon : </label> <?php echo (isset($data_sel[LIST_DATA][0][0]) ? $data_sel[LIST_DATA][0][0]->member_phone : "") ?></p>
                            <p><span class="fa fa-home fa-lg"></span>&nbsp; <label class="control-label"> Alamat  : </label> 
                                <?php echo (isset($data_sel[LIST_DATA][0][0]) ? $data_sel[LIST_DATA][0][0]->receiver_address : "") ?> -
                                <?php echo (isset($data_sel[LIST_DATA][0][0]) ? $data_sel[LIST_DATA][0][0]->district_name : "") ?> -
                                <?php echo (isset($data_sel[LIST_DATA][0][0]) ? $data_sel[LIST_DATA][0][0]->city_name : "") ?> -
                                <?php echo (isset($data_sel[LIST_DATA][0][0]) ? $data_sel[LIST_DATA][0][0]->province_name : "") ?> 
                            </p>
                        </div>
                    </div>
                </div>
            </section>
            <section class="col-md-12 col-lg-12">
                <h3><p>Detail Produk : </p></h3>
                <?php if (isset($data_sel[LIST_DATA][0][0]->product_seq)) { ?>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <tr class="danger">                                    
                                <th>Nama Produk</th>
                                <th>Varian</th>
                                <th>Harga Produk (Rp.)</th>
                                <th>Qty</th>
                                <th>Total (Rp.)</th>
                                <th>Gambar Produk</th>
                            </tr>
                            <?php foreach ($data_sel[LIST_DATA][1] as $each) {
                                ?>
                                <tr>
                                    <td><?php echo $each->product_name; ?></td>
                                    <td>
                                        <?php if ($each->value_seq == '1') { ?>
                                        <?php } else { ?>
                                            <?php echo $each->value; ?>
                                        <?php } ?>
                                    </td>
                                    <td><?php echo cnum($each->sell_price); ?></td>
                                    <td><?php echo $each->qty; ?></td>
                                    <td><?php echo cnum($each->sell_price * $each->qty); ?></td>
                                    <td>
                                        <img src="<?php
                                        echo get_base_url() . PRODUCT_UPLOAD_IMAGE . '/' .
                                        $each->merchant_seq . '/' .
                                        $each->pic_1_img
                                        ?>" style="width:150px;height:100px;">
                                    </td>
                                </tr>
                            <?php }
                            ?>
                        </table>
                    </div>
                <?php } ?>
            </section>
            <section class="col-md-12 col-lg-12">
                <h3><p>Proses Pengembalian Barang  : </p></h3>
                <div class="well">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="panel" style="border-color:rgb(100, 156, 80);">
                                <div class="panel-heading" style="background-color: rgba(91, 162, 65, 0.81);color:white;">                                          
                                    <h2 class="panel-title pull-left">Member</h2>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right fa-lg"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-body">
                                    <form class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-6 control-label">Tgl. Pengajuan  </label>
                                            <div class="col-sm-6">
                                                <p class="form-control-static"><?php echo cdate(isset($data_sel[LIST_DATA][0][0]) ? $data_sel[LIST_DATA][0][0]->created_date : "") ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-6 control-label">Ekspedisi  </label>
                                            <div class="col-sm-6">
                                                <p class="form-control-static"><?php echo (isset($data_sel[LIST_DATA][0][0]) ? $data_sel[LIST_DATA][0][0]->exp_to_admin : "") ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-6 control-label">No. Resi  </label>
                                            <div class="col-sm-6">
                                                <p class="form-control-static"><?php echo (isset($data_sel[LIST_DATA][0][0]) ? $data_sel[LIST_DATA][0][0]->awb_member_no : "") ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-6 control-label">Pesan Member  </label>
                                            <div class="col-sm-6">
                                                <p class="form-control-static"><?php echo (isset($data_sel[LIST_DATA][0][0]) ? $data_sel[LIST_DATA][0][0]->review_member : "") ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-6 control-label">Tgl. Terima Barang  </label>
                                            <div class="col-sm-6">
                                                <?php if ($data_sel[LIST_DATA][0][0]->member_received_date != DEFAULT_DATETIME) { ?>
                                                    <p class="form-control-static"><?php echo cdate(isset($data_sel[LIST_DATA][0][0]) ? $data_sel[LIST_DATA][0][0]->member_received_date : "") ?></p>
                                                <?php } else { ?>
                                                    <p class="form-control-static"></p>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="panel panel-warning" style="border-color: rgb(227, 165, 85)">
                                <div class="panel-heading" style="background-color: rgb(234, 148, 51);color:white;">                                            
                                    <h2 class="panel-title pull-left">Admin</h2>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right fa-lg"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-body">
                                    <form class="form-horizontal">
                                        <?php
                                        if (($data_sel[LIST_DATA][0][0]->shipment_status == RECEIVED_BY_ADMIN) ||
                                                ($data_sel[LIST_DATA][0][0]->shipment_status == SHIP_TO_MERCHANT) ||
                                                ($data_sel[LIST_DATA][0][0]->shipment_status == RECEIVED_BY_MERCHANT) ||
                                                ($data_sel[LIST_DATA][0][0]->shipment_status == SHIP_TO_MEMBER) ||
                                                ($data_sel[LIST_DATA][0][0]->shipment_status == RECEIVED_BY_MEMBER)) {
                                            ?>
                                            <div class="form-group">
                                                <label class="col-sm-6 control-label">Tgl. Terima Barang</label>
                                                <div class="col-sm-6">
                                                    <?php if ($data_sel[LIST_DATA][0][0]->admin_received_date != DEFAULT_DATETIME) { ?>
                                                        <p class="form-control-static"><?php echo cdate(isset($data_sel[LIST_DATA][0][0]) ? $data_sel[LIST_DATA][0][0]->admin_received_date : "") ?></p>
                                                    <?php } else { ?>
                                                        <p class="form-control-static"></p>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <?php if ($data_sel[LIST_DATA][0][0]->ship_to_merchant_date != DEFAULT_DATETIME) { ?>
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label">Tgl. Kirim ke Merchant</label>
                                                    <div class="col-sm-6">
                                                        <p class="form-control-static"><?php echo cdate(isset($data_sel[LIST_DATA][0][0]) ? $data_sel[LIST_DATA][0][0]->ship_to_merchant_date : "") ?></p>
                                                    </div>
                                                </div>
                                            <?php } else if ($data_sel[LIST_DATA][0][0]->ship_to_member_date != DEFAULT_DATETIME) { ?>
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label">Tgl. Kirim ke Member</label>
                                                    <div class="col-sm-6">
                                                        <p class="form-control-static"><?php echo cdate(isset($data_sel[LIST_DATA][0][0]) ? $data_sel[LIST_DATA][0][0]->ship_to_member_date : "") ?></p>
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                            <?php } ?>
                                            <div class="form-group">
                                                <label class="col-sm-6 control-label">Ekspedisi</label>
                                                <div class="col-sm-6">
                                                    <?php if ($data_sel[LIST_DATA][0][0]->exp_to_merchant != NULL) { ?>
                                                        <p class="form-control-static"><?php echo (isset($data_sel[LIST_DATA][0][0]) ? $data_sel[LIST_DATA][0][0]->exp_to_merchant : "") ?></p>
                                                    <?php } else if ($data_sel[LIST_DATA][0][0]->exp_to_member != NULL) { ?>
                                                        <p class="form-control-static"><?php echo (isset($data_sel[LIST_DATA][0][0]) ? $data_sel[LIST_DATA][0][0]->exp_to_member : "") ?></p>
                                                    <?php } else { ?>
                                                        <p class="form-control-static"></p>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-6 control-label">No. Resi</label>
                                                <div class="col-sm-6">
                                                    <p class="form-control-static"><?php echo (isset($data_sel[LIST_DATA][0][0]) ? $data_sel[LIST_DATA][0][0]->awb_admin_no : "") ?></p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-6 control-label">Pesan Admin</label>
                                                <div class="col-sm-6">
                                                    <p class="form-control-static"><?php echo (isset($data_sel[LIST_DATA][0][0]) ? $data_sel[LIST_DATA][0][0]->review_admin : "") ?></p>
                                                </div>
                                            </div>
                                        <?php } else { ?>
                                        <?php } ?>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h2 class="panel-title pull-left">Merchant</h2>
                                    <span class="pull-right"><i class="fa fa-check-circle fa-lg"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-body">
                                    <form class="form-horizontal">
                                        <?php
                                        if (($data_sel[LIST_DATA][0][0]->shipment_status == RECEIVED_BY_MERCHANT) ||
                                                (($data_sel[LIST_DATA][0][0]->shipment_status == SHIP_TO_MEMBER) &&
                                                ($data_sel[LIST_DATA][0][0]->merchant_received_date != DEFAULT_DATETIME)) ||
                                                (($data_sel[LIST_DATA][0][0]->shipment_status == RECEIVED_BY_MEMBER) &&
                                                ($data_sel[LIST_DATA][0][0]->merchant_received_date != DEFAULT_DATETIME))) {
                                            ?>
                                            <div class="form-group">
                                                <label class="col-sm-6 control-label">Tgl. Terima Barang </label>
                                                <div class="col-sm-6">
                                                    <?php if ($data_sel[LIST_DATA][0][0]->merchant_received_date != DEFAULT_DATETIME) { ?>
                                                        <p class="form-control-static"><?php echo cdate(isset($data_sel[LIST_DATA][0][0]) ? $data_sel[LIST_DATA][0][0]->merchant_received_date : "") ?></p>
                                                    <?php } else { ?>
                                                        <p class="form-control-static"></p>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <?php if ($data_sel[LIST_DATA][0][0]->return_status == REFUND_BY_MERCHANT) { ?>
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label">Tgl. Pengembalian Dana </label>
                                                    <div class="col-sm-6">
                                                        <?php if ($data_sel[LIST_DATA][0][0]->return_status == 'F') { ?>
                                                            <p class="form-control-static"><?php echo cdate(isset($data_sel[LIST_DATA][0][0]) ? $data_sel[LIST_DATA][0][0]->modified_date : "") ?></p>
                                                        <?php } else { ?>
                                                            <p class="form-control-static"></p>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            <?php } else if ($data_sel[LIST_DATA][0][0]->return_status == RETURN_ITEM_BY_MERCHANT) { ?>
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label">Tgl. Kirim ke Member</label>
                                                    <div class="col-sm-6">
                                                        <?php if ($data_sel[LIST_DATA][0][0]->ship_to_member_date != DEFAULT_DATETIME) { ?>
                                                            <p class="form-control-static"><?php echo cdate(isset($data_sel[LIST_DATA][0][0]) ? $data_sel[LIST_DATA][0][0]->ship_to_member_date : "") ?></p>
                                                        <?php } else { ?>
                                                            <p class="form-control-static"></p>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                            <?php } ?>
                                            <div class="form-group">
                                                <label class="col-sm-6 control-label">Ekspedisi</label>
                                                <div class="col-sm-6">
                                                    <?php if ($data_sel[LIST_DATA][0][0]->exp_to_member == NULL) { ?>
                                                        <p class="form-control-static"></p>
                                                    <?php } else { ?>
                                                        <p class="form-control-static"><?php echo (isset($data_sel[LIST_DATA][0][0]) ? $data_sel[LIST_DATA][0][0]->exp_to_member : "") ?></p>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-6 control-label">No. Resi  </label>
                                                <div class="col-sm-6">
                                                    <p class="form-control-static"><?php echo (isset($data_sel[LIST_DATA][0][0]) ? $data_sel[LIST_DATA][0][0]->awb_merchant_no : "") ?></p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-6 control-label">Pesan Merchant </label>
                                                <div class="col-sm-6">
                                                    <p class="form-control-static"><?php echo (isset($data_sel[LIST_DATA][0][0]) ? $data_sel[LIST_DATA][0][0]->review_merchant : "") ?></p>
                                                </div>
                                            </div>
                                        <?php } else { ?>
                                        <?php } ?>
                                    </form>
                                </div>
                            </div>
                        </div>   
                    </div>
                </div>
            </section>
            <div class="box-footer">
                <a href="<?php echo get_base_url() . 'admin/transaction/order_return_admin'; ?>" class="btn btn-google-plus">Kembali</a>
            </div>
            <?php
        } else {
            ?>
            <?php require_once get_include_page_list_admin_content_header(); ?>
            <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th column="return_date"> Tgl. Pengembalian </th>
                        <th column="return_no"> No. Pengembalian </th>
                        <th column="awb_member_no"> Resi Member </th>
                        <th column="return_status"> Status Pengembalian </th>
                        <th column="shipment_status"> Status Pengiriman </th>
                        <th column="action"> Terima / Kirim </th>
                        <th column="member_email"> Member </th>
                        <th column="order_no"> No. Order </th>
                        <th column="product_name"> Produk </th>
                        <th column="qty"> Qty </th>
                    </tr>
                </thead>
            </table>
        <?php } ?>
    </div>
</div>

<?php
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>