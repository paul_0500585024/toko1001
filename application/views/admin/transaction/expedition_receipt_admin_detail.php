<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
?>

<link rel="stylesheet" type="text/css" href="<?php echo get_css_url() ?>daterangepicker-bs3.css" />
<script type="text/javascript" src="<?php echo get_js_url() ?>moment.min.js"></script>
<script type="text/javascript" src="<?php echo get_js_url() ?>daterangepicker.js"></script>

<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo $data_auth[FORM_ACTION_TITLE] . $data_auth[FORM_AUTH][FORM_TITLE] ?></h3>
        </div>
        <form class ="form-horizontal" id="frmMain" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
            <div class="box-body">
                <section class="col-md-6">
                    <?php echo get_csrf_admin_token(); ?>
                    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                        <input class="form-control"  name ="seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->seq; ?>">
                    <?php } ?>
                    <div class ="form-group">     
                        <label class ="control-label col-md-4">No. Resi</label>
                        <div class ="col-md-8">
                            <input class="form-control" name="awb_no" validate="required[]" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->awb_no : "" ); ?>">
                        </div>
                    </div>
                    <div class ="form-group">
                        <label class="control-label col-md-4">Tanggal Resi</label>
                        <div class=" col-md-8">
                            <input class="form-control" id="datepicker" validate ="required[]" name ="awb_date" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->awb_date : "" ); ?>" readonly="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Tujuan</label>
                        <div class=" col-md-8"> 
                            <input class="form-control" name="destination" validate ="required[]" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->destination : "" ); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Harga Kirim</label>
                        <div class=" col-md-8"> 
                            <input class="form-control auto" name="ship_price" validate ="required[]" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->ship_price : "" ); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Harga Asuransi</label>
                        <div class=" col-md-8">
                            <input class="form-control auto" name="ins_price" validate ="required[]" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->ins_price : "" ); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">No. Order</label>
                        <div class=" col-md-8">
                            <?php require_once get_component_url() . "dropdown/com_drop_order_no_by_order.php" ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Merchant</label>
                        <div class=" col-md-8"> 
                            <?php require_once get_component_url() . "dropdown/com_drop_merchant_by_order.php" ?>
                        </div>
                    </div>
                    <div class ="form-group">
                        <?php if ($data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
                            <div class ="col-md-6"><?php echo get_back_button(); ?> </div>
                            <?php
                        } else {
                            if ($data_auth[FORM_ACTION] == ACTION_ADD) {
                                ?>
                                <div class ="col-md-6"><?php echo get_save_add_button(); ?> </div>
                            <?php } if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                                <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                            <?php } ?>
                            <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                        <?php } ?>
                    </div>
                </section>
            </div>
        </form>
    </div>
<?php } else { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <div class ="form-group">   
                <label class="control-label col-md-2">Ekspedisi</label>
                <div class="input-group col-md-3">
                    <input class="form-control" name="expedition_seq" validate="required[]" type="text" value ="<?php echo (isset($data_head[LIST_DATA][0]) ? $data_head[LIST_DATA][0]->expedition_name : "" ); ?>" disabled="">
                </div>
            </div>
            <div class ="form-group">   
                <label class="control-label col-md-2">No. Faktur</label>
                <div class="input-group col-md-3">
                    <input class="form-control" name="invoice_no" validate="required[]" type="text" value ="<?php echo (isset($data_head[LIST_DATA][0]) ? $data_head[LIST_DATA][0]->invoice_no : "" ); ?>" disabled="">
                </div>
            </div>
            <div class="box-body col-md-12">
                <?php require_once get_include_page_list_admin_content_header(); ?>
                <form id = "frmMain" class = "form-group" method = "post" action = "<?php echo get_base_url() . $data_auth[FORM_URL] ?>" enctype = "multipart/form-data">
                    <span class="-btn">
                        <button name ="<?php echo CONTROL_APPROVE_NAME ?>" type = "submit" class = "btn btn-primary">Check Order</button>
                    </span>
                </form>
                <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th column="awb_no"> No. Resi </th>
                            <th column="awb_date"> Tanggal Resi </th>
                            <th column="destination"> Tujuan </th>
                            <th column="ship_price"> Harga Kirim </th>
                            <th column="ins_price"> Harga Asuransi </th>
                            <th column="total_price"> Total Harga </th>
                            <th column="status"> Status </th>
                            <th column="order_no"> No. Order </th>
                            <th column="merchant_name"> Merchant </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <a href="<?php echo get_base_url() . 'admin/transaction/expedition_receipt'; ?>" class="btn btn-google-plus">Back</a>
    <?php
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>

<script type="text/javascript">
            $(document).ready(function() {
                $("#drop_order").change(function() {
                    var order_seq = $('#drop_order').val();
                        $.ajax({
                            url: "<?php echo get_base_url() . $data_auth[FORM_URL] ?>",
                                dataType: "json",
                                type: "POST",
                            data: {
                            "btnAdditional": "true",
                                "order_seq": order_seq
                            },
                            success: function(data) {
                                if(isSessionExpired(data)){
                                    response_object = json_decode(data);
                                    url = response_object.url;
                                    location.href = url;
                                }else{
                                    $('#drop_merchant option').remove();
                                    $(data).each(function(i) {
                                        $("#drop_merchant").append("<option value=\"" + data[i].merchant_seq + "\">" + data[i].merchant_name + "</option>")
                                    });
                                    $("#drop_merchant").select2();
                                }
                            },
                            error: function (request, error) {
                                alert(error_response(request.status));
                            }
                        });
                });             
            });

                $('input[name="awb_date"]').daterangepicker({
            singleDatePicker: true,
                showDropdowns: true,
                format: 'DD-MMM-YYYY'
            });
</script>