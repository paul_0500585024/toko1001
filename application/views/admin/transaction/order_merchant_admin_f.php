<?php
require_once VIEW_BASE_ADMIN;
?>
<link rel="stylesheet" type="text/css" href="<?php echo get_css_url() ?>daterangepicker-bs3.css" />
<script type="text/javascript" src="<?php echo get_js_url() ?>moment.min.js"></script>
<script type="text/javascript" src="<?php echo get_js_url() ?>daterangepicker.js"></script>
<form id="frmSearch" url="<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
    <div class="form-group">
        <label>No Order</label>
        <div>
            <input class="form-control" name="order_no" type="input">
        </div>
    </div>
    <div class="form-group">
        <label>Dari Tanggal</label>
        <div>
            <input type="text" name="tanggal1" class="form-control"  value="<?php echo date('d-M-Y',strtotime(LAST_WEEK)) ?>" readonly style="background-color: #fff;">
            </br>
            <label>Ke Tanggal</label>
            <input type="text" name="tanggal2" class="form-control" value="<?php echo date('d-M-Y') ?>" readonly style="background-color: #fff;">
        </div>
    </div>
    <div class="form-group">
        <label>Merchant</label>
        <div>
            <?php require_once get_component_url() . "dropdown/com_drop_merchant.php"; ?>
        </div>
    </div>
    <div class="form-group">
        <label>Status Order</label>
        <select class="form-control select2" name="order_status">
            <option value="">Semua</option>
            <?php
            echo combostatus(json_decode(STATUS_ORDER),"S");
            ?>
        </select>
    </div>
    <div class="form-group">
        <label>Status Pembayaran</label>
        <select class="form-control select2" name="paymnet_status">
            <option value="">Semua</option>
            <?php
            echo combostatus(json_decode(STATUS_PAYMENT),"P");
            ?>
        </select>
    </div>
    <div class="form-group">
        <label>Metode Pembayaran</label>
        <div>
            <?php require_once get_component_url() . "dropdown/com_drop_payment_gateway_method.php"; ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo get_search_button(); ?>
    </div>
</form>

<script  type="text/javascript">
    $('input[name="tanggal1"]').daterangepicker({
        format: 'DD-MMM-YYYY',
        showDropdowns: true,
        singleDatePicker: true,
        startDate: "<?php echo date('d-M-Y',strtotime(LAST_WEEK)) ?>"
    });
    $('input[name="tanggal2"]').daterangepicker({
        format: 'DD-MMM-YYYY',
        singleDatePicker: true,
        showDropdowns: true,
        startDate: "<?php echo date('d-M-Y') ?>"
    });
</script>
