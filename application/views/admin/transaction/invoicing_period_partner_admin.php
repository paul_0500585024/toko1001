<?php
//var_Dump('<pre>', $data_sel[LIST_DATA]);die();
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
?>
<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_css_url() ?>daterangepicker-bs3.css" />
    <script type="text/javascript" src="<?php echo get_js_url() ?>moment.min.js"></script>
    <script type="text/javascript" src="<?php echo get_js_url() ?>daterangepicker.js"></script>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo $data_auth[FORM_ACTION_TITLE] . $data_auth[FORM_AUTH][FORM_TITLE] ?></h3>
        </div>
        <form onsubmit ="return validate_form();" class ="form-horizontal" id="frmMain" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL]; ?>">
            <div class="box-body">
                <section class="col-md-6">
                    <?php echo get_csrf_admin_token(); ?>
                    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                        <input class="form-control"  name ="collect_seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->collect_seq; ?>">
                        <input class="form-control"  name ="partner_seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->partner_seq; ?>">
                    <?php } ?>
                    <div class="box-body">
                        <div class ="form-group">
                            <label class ="control-label col-md-3">Partner</label>
                            <div class ="col-md-9"><?php echo isset($data_sel[LIST_DATA][0]->name) ? $data_sel[LIST_DATA][0]->name : '-'; ?></div>
                        </div>
                        <div class ="form-group">
                            <label class ="control-label col-md-3">Total</label>
                            <div class ="col-md-9">Rp. <?php echo number_format(isset($data_sel[LIST_DATA][0]->total) ? $data_sel[LIST_DATA][0]->total : 0); ?></div>
                        </div>
                        <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT && $data_sel[LIST_DATA][0]->status == "D") { ?>
                            <div class ="form-group">
                                <label class ="control-label col-md-3">Tanggal Bayar</label>
                                <div class ="col-md-9">
                                    <input class="form-control" validate="required[]" date_type="date" id ="paid_date" name ="paid_date" type="text" placeholder ="Tanggal Bayar" value ="<?php echo cdate(isset($data_sel[LIST_DATA][0]->paid_date) ? $data_sel[LIST_DATA][0]->paid_date : '0000-00-00'); ?>">
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class ="form-group">
                        <?php
                        if ($data_auth[FORM_ACTION] == ACTION_VIEW) {
                            ?>
                            <div class ="col-md-6"><?php echo get_back_button(); ?> </div>
                            <?php
                        } else {
                            if ($data_auth[FORM_ACTION] == ACTION_EDIT && $data_sel[LIST_DATA][0]->status == "D") {
                                ?>
                                <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                            <?php } ?>
                            <div class ="col-md-6"><?php echo get_cancel_button(); ?></div>
                        <?php } ?>
                    </div>
                </section>
            </div>
        </form>
    </div>
    <script type="text/javascript">
        $('input[date_type="date"]').daterangepicker({
            format: 'DD-MMM-YYYY',
            singleDatePicker: true,
            showDropdowns: true
        });</script>
    <?php
} elseif ($data_auth[FORM_ACTION] == ACTION_VIEW) {
    ?>
    <div class="box-body">
        <div class="col-md-8">
            <dl class="dl-horizontal">

                <dt>Partner :</dt>
                <dd><?php echo $data_sel[LIST_DATA][0]->name; ?></dd>
                <dt>Total :</dt>
                <dd>Rp. <?php echo number_format($data_sel[LIST_DATA][0]->total); ?></dd>
                <dt>Bank :</dt>
                <dd><?php echo $data_sel[LIST_DATA][0]->bank_name; ?></dd>
                <dt>Cabang :</dt>
                <dd><?php echo $data_sel[LIST_DATA][0]->bank_branch_name; ?></dd>
                <dt>No Akun :</dt>
                <dd><?php echo $data_sel[LIST_DATA][0]->bank_acct_no; ?></dd>
                <dt>Nama Akun :</dt>
                <dd><?php echo $data_sel[LIST_DATA][0]->bank_acct_name; ?></dd>
            </dl>
        </div>
        <div class="col-md-4">
            <table class="table table-condensed">
                <tr>
                    <th>Tipe</th>
                    <th align="right">Jumlah</th>
                </tr>
                <?php
                $totalcollect = 0;
                $tcollect = 0;
                if (isset($data_sel[LIST_DATA][1])) {
                    foreach ($data_sel[LIST_DATA][1] as $each) {
                        if ($each->mutation_type == "D") {
                            $tcollect = $each->total;
                            $totalcollect = $totalcollect + $tcollect;
                            $tcollect = number_format($tcollect);
                        } else {
                            $tcollect = $each->total;
                            $totalcollect = $totalcollect - $tcollect;
                            $tcollect = "(" . number_format($tcollect) . ")";
                        }
                        ?>
                        <tr>
                            <td><?php echo status($each->type, REDEEM_TYPE); ?></td>
                            <td align="right"><?php echo ($tcollect); ?></td>
                        </tr>
                        <?php
                    }
                }
                ?>
                <tr>
                    <td>Total</td>
                    <td align="right"><?php echo $totalcollect >= 0 ? number_format($totalcollect) : "(" . number_format($totalcollect * -1) . ")"; ?></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="box">
        <!-- /.box-header -->

        <!--START PRINT BUTTON--> 
        <div class="box-header" style="background: white">
            <div class="col-xs-3 pull-right">
                <button class="btn btn-success btn-block btn-block flat" type="button" id="btnxls" name="btnxls" onclick="data_report_partner('<?php echo $data_sel[LIST_DATA][0]->collect_seq ?>')"><i class="fa fa-file-excel-o"></i> Cetak Excel</button>
            </div>
        </div>
        <!--END PRINT BUTTON--> 

        <div class="box-body">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#t_1" aria-controls="t_1" role="tab" data-toggle="tab">Detil Order</a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="t_1"><br />
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <th>No. Order</th>
                                    <th>Tanggal Order</th>
                                    <th>Nilai Order</th>
                                    <th>Pembayaran I</th>
                                    <th>Total Tagihan</th>
                                </tr>
                                <?php
                                if (isset($data_sel[LIST_DATA][2])) {
                                    //die(print_r($data_sel[LIST_DATA]));
                                    $ttlorder = 0;
                                    $ttlexp = 0;
                                    $ttlfee = 0;
                                    $ttlall = 0;
                                    $subreedem = 0;
                                    $total_instalment = 0;
                                    $total_redeem = 0;
                                    foreach ($data_sel[LIST_DATA][2] as $each) {
                                        $subreedem = ($each->total_payment - $each->total_installment);
                                        ?>
                                        <tr>
                                            <td><?php echo $each->order_no; ?> <a href="../../transaction/order_detail/<?php echo $each->order_no; ?>" target="_blank" class="pull-right"><i class="fa fa-external-link"></i></a></td>
                                            <td align="right"><?php echo cdate($each->order_date); ?></td>
                                            <td align="right"><?php echo number_format($each->total_payment); ?></td>
                                            <td align="right"><?php echo isset($each->total_installment) ? number_format($each->total_installment) : ''; ?></td>
                                            <td align="right" class="warning"><?php echo number_format($subreedem); ?></td>
                                        </tr>
                                        <?php
                                        $ttlorder = ($ttlorder + $each->total_payment);
                                        if (isset($each->total_installment))
                                            $total_instalment = ($total_instalment + $each->total_installment);
                                        $total_redeem = ($total_redeem + $subreedem);
                                    }
                                    echo '
					<tr>
					    <td align="right" colspan="2">Total</td>
	    				    <td align="right">' . number_format($ttlorder) . '</td>
                                            <td align="right">' . number_format($total_instalment) . '</td>
                                            <td align="right" class="warning">' . number_format($total_redeem) . '</td>
	    				</tr>';
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <a href="" class="btn btn-default">Back</a>
<?php } else { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <?php require_once get_include_page_list_admin_content_header(); ?>
            <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th column="name"> Partner</th>
                        <th column="total"> Total</th>
                        <th column="paid_date"> Tanggal Lunas</th>
                        <th column="status"> Status</th>
                        <th column="detail_order"> Detail Order</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <a href="<?php echo get_base_url() . 'admin/transaction/invoicing_period'; ?>" class="btn btn-google-plus">Back</a>
    <script type="text/javascript">
        function detail_order(el) {
            $(el).closest("form").submit();
        }
    </script>    

    <?php
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>
