<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
?>
<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_css_url() ?>daterangepicker-bs3.css" />
    <script type="text/javascript" src="<?php echo get_js_url() ?>moment.min.js"></script>
    <script type="text/javascript" src="<?php echo get_js_url() ?>daterangepicker.js"></script>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo $data_auth[FORM_ACTION_TITLE] . $data_auth[FORM_AUTH][FORM_TITLE] ?></h3>
        </div>
        <form onsubmit ="return validate_form();" class ="form-horizontal" id="frmMain" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL]; ?>">
            <div class="box-body">
                <section class="col-md-6">
                    <?php echo get_csrf_admin_token(); ?>
                    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                        <input class="form-control"  name ="redeem_seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->redeem_seq; ?>">
                        <input class="form-control"  name ="merchant_seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->merchant_seq; ?>">
                    <?php } ?>
                    <div class="box-body">
                        <dl class="dl-horizontal">
                            <dt>Merchant :</dt>
                            <dd><?php echo $data_sel[LIST_DATA][0]->name; ?></dd>
                            <dt>Total :</dt>
                            <dd>Rp. <?php echo number_format($data_sel[LIST_DATA][0]->total); ?></dd>
                            <dt>Bank :</dt>
                            <dd><?php echo $data_sel[LIST_DATA][0]->bank_name; ?></dd>
                            <dt>Cabang :</dt>
                            <dd><?php echo $data_sel[LIST_DATA][0]->bank_branch_name; ?></dd>
                            <dt>No Akun :</dt>
                            <dd><?php echo $data_sel[LIST_DATA][0]->bank_acct_no; ?></dd>
                            <dt>Nama Akun :</dt>
                            <dd><?php echo $data_sel[LIST_DATA][0]->bank_acct_name; ?></dd>
                        </dl>
                    </div>
                    <div class ="form-group">
                        <label class ="control-label col-md-3">Tanggal Bayar</label>
                        <div class ="col-md-9">
                            <input class="form-control" validate="required[]" date_type="date" id ="paid_date" name ="paid_date" type="text" placeholder ="Tanggal Bayar" value ="<?php echo cdate($data_sel[LIST_DATA][0]->paid_date); ?>">
                        </div>
                    </div>
                    <div class ="form-group">
                        <?php
                        if ($data_auth[FORM_ACTION] == ACTION_VIEW) {
                            ?>
                            <div class ="col-md-6"><?php echo get_back_button(); ?> </div>
                            <?php
                        } else {
                            if ($data_auth[FORM_ACTION] == ACTION_EDIT && $data_sel[LIST_DATA][0]->status == "U") {
                                ?>
                                <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                            <?php } ?>
                            <div class ="col-md-6"><?php echo get_cancel_button(); ?></div>
                        <?php } ?>
                    </div>
                </section>
            </div>
        </form>
    </div>
    <script type="text/javascript">
            $('input[date_type="date"]').daterangepicker({
                format: 'DD-MMM-YYYY',
                singleDatePicker: true,
                showDropdowns: true
            });</script>
    <?php
} elseif ($data_auth[FORM_ACTION] == ACTION_VIEW) {
//    print_r($data_sel[LIST_DATA][0]) . "<hr />";
//    print_r($data_sel[LIST_DATA][1]) . "<hr />";
//    print_r($data_sel[LIST_DATA][2]) . "<hr />";
    ?>
    <div class="box-body">
        <div class="col-md-8">
            <dl class="dl-horizontal">

                <dt>Merchant :</dt>
                <dd><?php echo $data_sel[LIST_DATA][0]->name; ?></dd>
                <dt>Total :</dt>
                <dd>Rp. <?php echo number_format($data_sel[LIST_DATA][0]->total); ?></dd>
                <dt>Bank :</dt>
                <dd><?php echo $data_sel[LIST_DATA][0]->bank_name; ?></dd>
                <dt>Cabang :</dt>
                <dd><?php echo $data_sel[LIST_DATA][0]->bank_branch_name; ?></dd>
                <dt>No Akun :</dt>
                <dd><?php echo $data_sel[LIST_DATA][0]->bank_acct_no; ?></dd>
                <dt>Nama Akun :</dt>
                <dd><?php echo $data_sel[LIST_DATA][0]->bank_acct_name; ?></dd>
            </dl>
        </div>
        <div class="col-md-4">
            <table class="table table-condensed">
                <tr>
                    <th>Tipe</th>
                    <th>Jumlah</th>
                </tr>
                <?php
                //die(print_r($data_sel[LIST_DATA]));
                foreach ($data_sel[LIST_DATA][1] as $each) {
                    ?>
                    <tr>
                        <td><?php echo status($each->type, REDEEM_TYPE); ?></td>
                        <td><?php echo number_format($each->total); ?></td>
                    </tr>
                <?php }
                ?>
            </table>
        </div>
    </div>
    <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#t_1" aria-controls="t_1" role="tab" data-toggle="tab">Detil Order</a></li>
                <li role="presentation"><a href="#t_2" aria-controls="t_2" role="tab" data-toggle="tab">Detil Ekspedisi</a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="t_1"><br />
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <th>No. Order</th>
                                    <th>Nilai Order</th>
                                    <th>Nilai Ekpedisi</th>

                                    <th>Nilai Komisi</th>
                                    <th>Total Redeem</th>
                                </tr>
                                <?php
                                if (isset($data_sel[LIST_DATA][2])) {
                                    //die(print_r($data_sel[LIST_DATA]));
                                    $ttlorder = 0;
                                    $ttlexp = 0;
                                    $ttlfee = 0;
                                    $ttlall = 0;
                                    foreach ($data_sel[LIST_DATA][2] as $each) {
//					$totalexpedition = 0;
//					if ($each->free_fee_seq > 0) {
//					    // ada promo free ongkir
//					    if ($each->real_expedition_service_seq == 0) {
//						// merchant kirim sendiri
//						$totalexpedition = $each->totalexpedition * (100 - $each->exp_fee_percent) / 100;
//					    } else {
//						// menggunakan rekanan
//						$totalexpedition = $each->totalexpedition * $each->exp_fee_percent * (-1) / 100;
//					    }
//					} else {
//					    // tidak ada promo free ongkir
//					    if ($each->real_expedition_service_seq == 0) {
//						// merchant kirim sendiri
//						$totalexpedition = $each->totalexpedition;
//					    } else {
//						// menggunakan rekanan
//						$totalexpedition = 0;
//					    }
//					}
//					$totalexpedition = $each->totalexpedition;
                                        ?>
                                        <tr>
                                            <td><a href="javascript:detail_order('<?php echo $each->order_no; ?>')"><?php echo $each->order_no; ?></a> <a href="../../transaction/order_detail/<?php echo $each->order_no; ?>" target="_blank" class="pull-right"><i class="fa fa-external-link"></i></a></td>
                                            <td align="right"><?php echo number_format($each->totalorder); ?></td>
                                            <td align="right"><?php echo number_format($each->totalexpedition * (100 - $data_sel[LIST_DATA][0]->expedition_commission) / 100); ?></td>
                                            <td align="right"><?php echo number_format($each->totalfee); ?></td>
                                            <td align="right"><?php echo number_format($each->totalorder + ($each->totalexpedition * (100 - $data_sel[LIST_DATA][0]->expedition_commission) / 100 - $each->totalfee)); ?></td>
                                        </tr>
                                        <tr id='<?php echo $each->order_no; ?>'></tr>
                                        <?php                                             
                                        $ttlorder = ($ttlorder + $each->totalorder);
                                        $ttlexp = ($ttlexp + $each->totalexpedition * (100 - $data_sel[LIST_DATA][0]->expedition_commission) / 100);
                                        $ttlfee = ($ttlfee + $each->totalfee);
                                        $ttlall = ($ttlall + $each->totalorder + $each->totalexpedition * (100 - $data_sel[LIST_DATA][0]->expedition_commission) / 100 - $each->totalfee);
                                    }
                                    echo '
					<tr>
					    <td align="right">Total</td>
	    				    <td align="right">' . number_format($ttlorder) . '</td>
	    				    <td align="right">' . number_format($ttlexp) . '</td>
	    				    <td align="right">' . number_format($ttlfee) . '</td>
	    				    <td align="right">' . number_format($ttlall) . '</td>
	    				</tr>';
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="t_2"><br />
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <th>No. Order</th>
                                    <th>Tujuan</th>
                                    <th>Nilai Ekpedisi</th>
                                </tr>
                                <?php
                                if (isset($data_sel[LIST_DATA][3])) {
                                    $totalexpedition = 0;
                                    foreach ($data_sel[LIST_DATA][3] as $each) {
                                        ?>
                                        <tr>
                                            <td><?php echo $each->order_no; ?></td>
                                            <td align="right"><?php echo $each->destination; ?></td>
                                            <td align="right"><?php echo number_format($each->totalorder); ?></td>
                                        </tr>
                                        <?php
                                        $totalexpedition = ($totalexpedition + $each->totalorder);
                                    }
                                    echo '
					<tr>
					    <td align="right" colspan=2>Total</td>
	    				    <td align="right">' . number_format($totalexpedition) . '</td>
	    				</tr>';
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <a href="" class="btn btn-default">Back</a>

    <script type="text/javascript">
        function detail_order(order_no) {
            var url = "<?php echo get_base_url() . $data_auth[FORM_URL]; ?>";
            inHTML = "";
            $("#" + order_no).html("");
            $.ajax({
                url: url,
                data: {
                    btnAdditional: "act_s_adt", key: order_no + "~<?php echo $data_sel[LIST_DATA][0]->redeem_seq; ?>~<?php echo $data_sel[LIST_DATA][0]->merchant_seq; ?>"
                },
                type: "POST", datatype: 'json',
                success: function(response) {
                    if (isSessionExpired(response)) {
                        response_object = json_decode(response);
                        url = response_object.url;
                        location.href = url;
                    } else {

                        //alert(response);
                        var dataobj = $.parseJSON(response);
                        inHTML = '<td colspan=5><table class="table table-condensed "><tr class="info"><td class="col-md-4">Produk<a href="javascript:colapse(\'' + order_no + '\')" class="pull-right btn btn-default btn-xs"><i class="fa fa-minus"></i></a></td><td class="text-center">Harga</td><td class="text-center">Jumlah</td><td class="text-center">Total</td><td class="text-center">Komisi</td><td class="text-center">Redeem</td></tr>';
                        $.each(dataobj, function() {
                            inHTML += '<tr><td class="col-md-4">' + this['product_name'] + '</td>' +
                                    '<td class="pull-right">' + this['sell_price'] + '</td>' +
                                    '<td class="text-center">' + this['qty'] + '</td>' +
                                    '<td class="text-right">' + this['totalorder'] + '</td>' +
                                    
                                    '<td class="text-right">' + this['totalfee'] + '</td>' +
                                    '<td class="text-right">' + this['totalredeem'] + '</td></tr>';
                        });
                        inHTML += "</table></td>";
                        $("#" + order_no).html(inHTML);
                    }
                },
                error: function(request, error) {
                    alert(error_response(request.status));
                }
            });
        }

        function colapse(order_no) {
            $("#" + order_no).html("");
        }
    </script>

<?php } else { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <?php require_once get_include_page_list_admin_content_header(); ?>
            <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th column="name"> Merchant</th>
                        <th column="bank_name"> Bank</th>
                        <th column="bank_branch_name"> Cabang </th>
                        <th column="bank_acct_no"> No. Akun</th>
                        <th column="bank_acct_name"> Nama Akun</th>
                        <th column="total"> Total</th>
                        <th column="status"> Status</th>
                        <th column="paid_date"> Tanggal Bayar</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <a href="<?php echo get_base_url() . 'admin/transaction/redeem_period'; ?>" class="btn btn-google-plus">Back</a>
    <?php
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>