<?php
require_once VIEW_BASE_ADMIN;
?>

<link href="<?php echo get_css_url(); ?>daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo get_js_url(); ?>daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>moment.min.js"></script>

<form id ="frmSearch"  url= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
    <div class="form-group">
        <label>No. Pengembalian</label>
        <input class="form-control" name="return_no" type="input">
    </div>
    <div class="form-group">
        <label>Tgl. Pengajuan</label>
        <?php require_once get_component_url() . "date_picker/range_date_picker.php" ?>
    </div>
    &nbsp;
    <div class="form-group">
        <label>Member</label>
        <?php require_once get_component_url() . "dropdown/com_drop_member_email.php" ?>
    </div>
    <div class="form-group">
        <label>Status</label> 
        <select id="status" class="form-control select2" name="return_status">
            <option value="">-- Semua --</option>
            <?php
            echo combostatus(json_decode(STATUS_RETURN));
            ?>
        </select>
    </div>  
    <div class="form-group">
        <?php echo get_search_button(); ?>
    </div>
</form>

<script type="text/javascript">

    $('#reservation').daterangepicker({
        format: 'DD-MMM-YYYY'
    });

</script>

