<?php
require_once VIEW_BASE_ADMIN;

require_once get_include_content_admin_top_page_navigation();
?>

<link rel="stylesheet" type="text/css" href="<?php echo get_css_url() ?>daterangepicker-bs3.css" />
<script type="text/javascript" src="<?php echo get_js_url() ?>moment.min.js"></script>
<script type="text/javascript" src="<?php echo get_js_url() ?>daterangepicker.js"></script>

<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo $data_auth[FORM_ACTION_TITLE] . $data_auth[FORM_AUTH][FORM_TITLE] ?></h3>
        </div>
        <form class ="form-horizontal" id="frmMain" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
            <div class="box-body">
                <section class="col-md-12">
                    <?php echo get_csrf_admin_token(); ?>
                    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                        <input class="form-control" name ="order_seq" type="hidden" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->order_seq : "") ?>">
                        <input class="form-control" name ="merchant_info_seq" type="hidden" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->merchant_info_seq : "" ) ?>">
                        <input class="form-control" name ="ship_note_file" type="hidden" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->ship_note_file : "" ) ?>">
                    <?php } ?>
                    <div class="row">
                        <div class="col-md-6">
                            <div class ="form-group">
                                <label class ="control-label col-md-3">No. Order :</label>
                                <div class ="col-md-6">
                                    <input class="form-control" name ="order_no" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->order_no : "") ?>" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class ="control-label col-md-3">Tanggal Order :</label>
                            <div class ="col-md-6">
                                <input class="form-control" name="order_date" value="<?php echo (isset($data_sel[LIST_DATA]) ? cdate($data_sel[LIST_DATA][0]->order_date) : "") ?>" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class ="form-group">
                                <label class ="control-label col-md-3">Member :</label>
                                <div class ="col-md-6">
                                    <input class="form-control" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->member_name : "") ?>" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class ="control-label col-md-3">Alamat Penerima :</label>
                            <div class ="col-md-6">
                                <input class="form-control" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->receiver_address : "") ?>" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <table class="diplay table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th> Gambar </th>
                                    <th> Produk </th>
                                    <th> Tipe Produk </th>
                                    <th> Status Produk </th>
                                    <th> Qty </th>
                                    <th> Harga Jual (Rp)</th>
                                    <th> Total (Rp)</th>
                                </tr>
                                <?php
                                if (isset($data_sel[LIST_DATA][1])) {
                                    foreach ($data_sel[LIST_DATA][1] as $each) {
                                        ?>
                                        <tr valign="top">
                                            <td><a href="<?php echo get_base_url() . strtolower(url_title($each->product_name . ' ' . $each->variant_name)) . "-" . $each->product_variant_seq; ?>" target="_blank"><img src="<?php echo PRODUCT_UPLOAD_IMAGE ?><?php echo $each->merchant_seq; ?>/<?php echo $each->pic_1_img; ?>" width ="100px" height="100px" style="max-width:100px; max-height:100px"></a></td>
                                            <td><label class="control-label"><?php echo $each->product_name; ?></label></td>
                                            <td><label class="control-label"><?php echo $each->variant_name; ?></label></td>
                                            <td><label class="control-label"><?php echo cstdes($each->product_status, STATUS_XR); ?></label></td>
                                            <td><label class="control-label"><?php echo $each->qty; ?></label></td>
                                            <td><label class="control-label"><?php echo cnum($each->sell_price); ?></label></td>
                                            <td><label class="control-label"><?php echo cnum(ceil($each->weight_kg * $each->qty) * $each->sell_price * $each->qty); ?></label></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    </br>
                    <div>
                        <?php
                        if (($data_sel[LIST_DATA][0]->ship_note_file == "")) {
                            $image_2 = IMG_BLANK_100;
                        } else {
                            $image_2 = RESI_UPLOAD_IMAGE . $data_sel[LIST_DATA][0]->ship_note_file;
                        }
                        ?>
                        <div class="row">
                            <table class="diplay table table-bordered table-striped">
                                <tbody>
                                    <tr>
                                        <th>Ketarangan</th>
                                    </tr>
                                    <tr valign="top">
                                        <th> Expedisi  : <?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->esp_name : "") ?></th>
                                    </tr>
                                    <tr valign="top">
                                        <th> No Resi : <?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->awb_no : "") ?></th>
                                    </tr>
                                    <tr valign="top">
                                        <td>
                                            Bukti Resi :<br>
                                            <a href="<?php echo $image_2 ?>" target="_blank"><img width="150px" height="150px" src="<?php echo $image_2 ?>"></a>  
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    </br>
                    <?php IF (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->order_status == 'S' : "") { ?>
                        <div class="row">
                            <table class="diplay table table-bordered table-striped">
                                <tbody>
                                    <tr>
                                        <th> Status Order </th>
                                        <th>Proses Kirim</th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class ="col-md-9">
                                    <input  type="radio" name="order_status" value="D" checked <?php echo (!isset($data_sel[LIST_DATA]) ? "" : (($data_sel[LIST_DATA][0]->order_status == "D")) ); ?>> <label>Terkirim Tanggal</label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label class ="control-label col-md-3"></label>
                                <div class ="col-md-9">
                                    <input readonly style="background-color: #fff;" type="text" date_type="date" name="received_date"  class="form-control" value="<?php echo date('d-M-Y') ?>">
                                </div>
                            </div>
                        </div>
                        <br>
                    <?php } ?>
                    <div class ="form-group">
                        <?php
                        if ($data_auth[FORM_ACTION] == ACTION_VIEW) {
                            ?>
                            <div class ="col-md-6"><?php echo get_back_button(); ?> </div>
                            <?php
                        } else {
                            if ($data_auth[FORM_ACTION] == ACTION_ADD) {
                                ?>
                                <div class ="col-md-6"><?php echo get_save_add_button(); ?> </div>
                            <?php } if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                                <div class ="col-md-6"><?php echo isset($data_sel[LIST_DATA][0]) && $data_sel[LIST_DATA][0]->order_status == 'S' ? get_save_edit_button() : ""; ?></div>
                            <?php } ?>
                            <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                        <?php } ?>
                    </div>
                </section>
            </div>
        </form>
    </div>
    <script  type="text/javascript">
        $('input[date_type = "date"]').daterangepicker({
            format: 'DD-MMM-YYYY',
            showDropdowns: true,
            singleDatePicker: true,
            startDate: "<?php echo date('d-M-Y') ?>",
            minDate: "<?php echo (isset($data_sel[LIST_DATA]) ? cdate($data_sel[LIST_DATA][0]->order_date) : "") ?>"
        });
    </script>
    <?php
} else {
    $tablesort = 'desc';
    ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <?php require_once get_include_page_list_admin_content_header(); ?>
            <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th column="tgl_order"> Tanggal Order </th>
                        <th column="no_order"> No Order </th>
                        <th column="merchant_name"> Merchant </th>
                        <th column="payment_name"> Metode Pembayaran  </th>
                        <th column="awb_no"> No Resi</th>
                        <th column="ship_date"> Tanggal Resi </th>
                        <th column="receiver_address"> Kota Tujuan</th>
                        <th column="order_status"> Status Order </th>
                        <th column="payment_status"> Status Pembayaran </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <?php
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>