<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
?>

<link rel="stylesheet" type="text/css" href="<?php echo get_css_url() ?>daterangepicker-bs3.css" />
<script type="text/javascript" src="<?php echo get_js_url() ?>moment.min.js"></script>
<script type="text/javascript" src="<?php echo get_js_url() ?>daterangepicker.js"></script>


<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
    </div>
    <div class="box-body">
        <?php echo get_csrf_admin_token(); ?>
        <div class='row'>
            <div class='col-md-12' style="margin-top:-20px;">
                <p><h3>Detil Order : </h3></p>
                <div class="well">
                    <div class='row'>
                        <div class='col-md-6'>

                            <p><span class='fa fa-user fa-lg'></span>&nbsp; Member : <?php echo (isset($data_sel[LIST_DATA][0]) ? ($data_sel[LIST_DATA][0][0]->member_name != null ? $data_sel[LIST_DATA][0][0]->member_name : $data_sel[LIST_DATA][0][0]->agent_name) : "Nama Member Tidak Ada"); ?> - <?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0][0]->member_email : $data_sel[LIST_DATA][0][0]->agent_email) ?></p>
                            <p><span class='fa fa-tags fa-lg'></span>&nbsp; No. Order : <?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0][0]->order_no : "No Order Tidak Ada"); ?></p>

                        </div>
                        <div class='col-md-6'>
                            <p><span class='fa fa-calendar fa-lg'></span>&nbsp; Tanggal Order : <?php echo(isset($data_sel[LIST_DATA][0]) ? date_format(date_create($data_sel[LIST_DATA][0][0]->order_date), "d-M-Y") : "Tanggal Order Tidak Ada"); ?> </p>
                            <p><span class="fa fa-exclamation-circle fa-lg"></span>&nbsp; Status Order : <font style="color: red;font-weight: bold;"> <?php
                                $data = json_decode(STATUS_PAYMENT, true);
                                echo (isset($data_sel[LIST_DATA][0]) ? $data[$data_sel[LIST_DATA][0][0]->payment_status] : "Status Order Tidak Ada");
                                ?></font></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class='row'>
            <div class='col-md-6' style="margin-top:-10px;">
                <p><h3>Informasi Penerima : </h3></p>
                <div class="well">
                    <table border='0' class="table-informasi">
                        <style> .table-informasi td{padding: 2px;}
                        </style>
                        <tr>
                            <td valign='top' style=""><span class='fa fa-user fa-lg'></td>
                            <td ><?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0][0]->receiver_name : ""); ?></td>
                        </tr>
                        <tr>
                            <td valign='top' ><span class='fa fa-home fa-lg'></td>
                            <td valign='top' ><?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0][0]->receiver_address : "") . "<br>" . (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0][0]->province_name : "") . "-" . (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0][0]->city_name : "") . "-" . (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0][0]->district_name : ""); ?></td>
                        </tr>
                        <tr>
                            <td><span class='fa fa-phone fa-lg'></td>
                            <td ><?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0][0]->receiver_phone_no : ""); ?></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class='col-md-6' style="margin-top:-10px;">
                <p><h3>Metode Pembayaran : </h3></p>
                <div class="well">
                    <span class='fa fa-bars fa-lg'></span> &nbsp; <?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0][0]->payment_name : ""); ?>
                </div>
            </div>
        </div>
        <div class='row'>
            <div class="col-md-12" style="margin-top:-20px;">
                <p><h3>Keranjang Belanja : </h3></p>   
                <!--TABLE CART FOR LG,MD,SM-->
                <table class="field-group-format group_specs table table-bordered  hidden-xs" style="margin-top:-10px;">
                    <tr class="success" id="tr-cart">
                        <th class="field-label" colspan="2" id="tr-cart">Produk</th>
                        <th class="field-label" id="tr-cart">Harga (Rp)</th>
                        <th class="field-label" id="tr-cart">Jumlah</th>
                        <!--<th class="field-label" id="tr-cart">Biaya Kirim (Rp)</th>-->
                        <th class="field-label" id="tr-cart">Total (Rp)</th>
                    </tr>
                    <?php
                    $total = 0;

                    foreach ($data_sel[LIST_DATA][1] as $merchant) {
                        $ship = 0;
                        $ship_price = 0;
                        $sub_total = 0;
                        ?>
                        <tr>
                            <th colspan="8">                                        
                        <div class='row'>
                            <div class="col-lg-5 col-md-4 col-sm-5"><b><i class="material-icons" style='font-size:20px; vertical-align:top;'>store</i>&nbsp; <a href="<?php echo base_url() . "merchant/" . $merchant->merchant_code; ?>"><?php echo $merchant->merchant_name; ?> </a></b></div> 
                            <!--<div class="col-lg-3 col-md-2 col-sm-3 text-left" ><b><i class="fa fa-truck"></i>&nbsp; <?php echo $merchant->expedition_name; ?> </b></div>--> 
                            <div class="col-lg-7 col-md-8 col-sm-7 text-right" ><i class="fa fa-pencil-square-o" title="Pesan ke <?php echo $merchant->merchant_name; ?>"></i> <?php echo $merchant->member_notes; ?></div>
                        </div>
                        </th>
                        </tr>  
                        <?php
                        foreach ($data_sel[LIST_DATA][2] as $prod) {
                            if ($merchant->merchant_seq == $prod->merchant_seq) {
                                ?>
                                <td class="field-content" style=" vertical-align: middle">
                                    <a href ="<?php echo base_url() . url_title($prod->display_name . " " . $prod->product_seq); ?>"><center><img src="<?php echo PRODUCT_UPLOAD_IMAGE . $prod->merchant_seq . "/" . S_IMAGE_UPLOAD . $prod->img; ?>" width="100px"></center></a></td>                                <td class="field-label"><strong><?php echo $prod->display_name ?></strong>
                                    <br><br>
                                    <div class="table-responsive">
                                        <table border="0" class="table table-condensed">
                                            <tr>
                                                <td width="20%">Berat</td>  
                                                <td width="5%">:</td>
                                                <td><?php echo $prod->weight_kg ?> Kg</td>
                                            </tr>
                                            <?php if ($prod->value_seq != DEFAULT_VALUE_VARIANT_SEQ) { ?>
                                                <tr>
                                                    <td>Warna</td>
                                                    <td>:</td>
                                                    <td><?php echo $prod->variant_name; ?></td>
                                                </tr>
                                            <?php } ?>
                                            <tr>
                                                <td>Status Kirim</td>
                                                <td>:</td>
                                                <td class="text-red"><?php
                                                    $data = json_decode(STATUS_ORDER, true);
                                                    echo $prod->product_status == PRODUCT_CANCEL_BY_MERCHANT_STATUS_CODE ? $data[PRODUCT_CANCEL_BY_MERCHANT_STATUS_CODE] : $data[$merchant->order_status];
                                                    ?></td>
                                            </tr>
                                            <tr>
                                                <td>No Resi</td>
                                                <td>:</td>
                                                <td class="text-red"><?php echo $prod->product_status == PRODUCT_CANCEL_BY_MERCHANT_STATUS_CODE ? "" : $merchant->awb_no; ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                                <td class="field-content" style=" text-align: center; vertical-align: middle" id="tr-cart"><?php echo number_format($prod->sell_price); ?></td>
                                <td class="field-content" style=" text-align: center; vertical-align: middle" id="tr-cart"><?php echo number_format($prod->qty); ?></td>
                                <!--<td class="field-content" style=" text-align: center; vertical-align: middle" id="tr-cart"><?php echo $prod->ship_price_charged == 0 ? "<div class='text-green'>Free</div>" : number_format(ceil($prod->weight_kg * $prod->qty) * $prod->ship_price_charged); ?></td>-->
                                <td class="field-content" style=" text-align: center; vertical-align: middle" id="tr-cart"><?php
                                    echo number_format($prod->qty * $prod->sell_price);
                                    $total = $total + $prod->qty * $prod->sell_price + ceil($prod->weight_kg * $prod->qty) * $prod->ship_price_charged;
                                    $ship = $ship + $prod->weight_kg * $prod->qty;
                                    $ship_price = $prod->ship_price_charged;
                                    $sub_total = $sub_total + $prod->qty * $prod->sell_price;
                                    ?></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        <td colspan="8">
                            <div class="col-lg-10 col-md-9 col-sm-9">
                                Biaya Pengiriman : <?php echo isset($ship_price) && $ship_price != 0 ? $merchant->expedition_name . ' (' . ceil($ship) . ' x @' . number_format($ship_price) . ') ' . RP . number_format(ceil($ship) * $ship_price) : $merchant->expedition_name ." (<b class='text-green'>Free</b>)"?>
                            </div>
                            <div class="col-lg-2 col-md-3 col-sm-3">
                                <label style="text-align: left !important"><?php echo RP . number_format((ceil($ship) * $ship_price) + $sub_total) ?></label>
                            </div>
                        </td>
                    <?php }
                    ?>
                    <tr>
                    <tr class="font22">
                        <th  colspan="6" style="text-align: left;">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <font size="4px">Kode Voucher : <?php echo $data_sel[LIST_DATA][3][0]->voucher_code; ?>  <br>Voucher Member -  <?php echo number_format($data_sel[LIST_DATA][3][0]->nominal); ?></font>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6" style="text-align: right; ">
                            <font size="4px">Total Belanja : Rp <?php echo number_format($total); ?></font>
                            <p><font size="5px">Total Bayar : <font color="red">Rp <?php echo number_format($total - $data_sel[LIST_DATA][3][0]->nominal); ?></font></font></a>
                        </div>
                    </div>
                    </th>
                    </tr>
                </table>
                <!--
                <!--                        TABLE CART FOR XS-->
                <div class="hidden-lg hidden-sm hidden-md" >
                    <div class='table-responsive'>
                        <table class="table table-bordered">
                            <tr class="success">
                                <th class="field-label"><center>Produk</center></th>
                            <th class="field-label">Keterangan</th>
                            </tr>
                            <?php foreach ($data_sel[LIST_DATA][1] as $merchant) { ?>
                                <tr>
                                    <td class="field-content" colspan="4">
                                        <div class="row">
                                            <div class="col-xs-12 text-left"><b><i class = 'material-icons' style='font-size:20px; vertical-align:top;'>store</i>&nbsp; <a href="<?php echo base_url() . "merchant/" . $merchant->merchant_code; ?>"><?php echo $merchant->merchant_name; ?> </a></b> &nbsp; </div>
                                            <div class="col-xs-12 text-left"><b><i class="fa fa-truck "></i>&nbsp; <?php echo $merchant->expedition_name; ?> </b></div>
                                            <div class="col-xs-12 text-left" ><i class="fa fa-pencil-square-o"></i> <b>Catatan  : "<?php echo $merchant->member_notes; ?>"</b></div>
                                        </div></td>
                                </tr>
                                <?php
                                foreach ($data_sel[LIST_DATA][2] as $prod) {
                                    if ($merchant->merchant_seq == $prod->merchant_seq) {
                                        ?>
                                        <tr>
                                            <td>
                                                <table border="0" class="table table-condensed">
                                                    <a href ="<?php echo base_url() . url_title($prod->display_name . " " . $prod->product_seq); ?>"><center> <img src="<?php echo get_base_url() . PRODUCT_UPLOAD_IMAGE . $prod->merchant_seq . "/" . S_IMAGE_UPLOAD . $prod->img; ?>" alt="<?php echo $prod->img; ?>" class='img' width='100%' height ="100%"></center></a>
                                                    <tr>
                                                        <td>Status Kirim</td>
                                                        <td>:</td>
                                                        <td class="text-red"><?php
                                                            $data = json_decode(STATUS_ORDER, true);
                                                            echo $data[$merchant->order_status];
                                                            ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>No Resi</td>
                                                        <td>:</td>
                                                        <td class="text-red"><?php echo $merchant->awb_no ?></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>
                                                <strong><?php echo $prod->display_name ?></strong>
                                                <br><br>
                                                <table border="0" class="table table-condensed">
                                                    <tr>
                                                        <td>Berat</td>
                                                        <td>:</td>
                                                        <td><?php echo $prod->weight_kg ?> Kg</td>
                                                    </tr>
                                                    <?php if ($prod->value_seq != DEFAULT_VALUE_VARIANT_SEQ) { ?>
                                                        <tr>
                                                            <td>Warna</td>
                                                            <td>:</td>
                                                            <td><?php echo $prod->variant_name; ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                    <tr>
                                                        <td>Qty</td>
                                                        <td>:</td>
                                                        <td><?php echo number_format($prod->qty); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Harga</td>
                                                        <td>:</td>
                                                        <td>Rp. <?php echo number_format($prod->sell_price); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Biaya Kirim</td>
                                                        <td>:</td>
                                                        <td><?php echo $prod->ship_price_charged == 0 ? "<div class='text-green'>Free</div>" : number_format(ceil($prod->weight_kg * $prod->qty) * $prod->ship_price_charged); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Total</td>
                                                        <td>:</td>
                                                        <td>Rp <?php echo number_format($prod->qty * $prod->sell_price); ?></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                <td colspan="8">
                                    <div class="col-lg-10 col-md-9 col-sm-9">
                                        Biaya Pengiriman : <?php echo isset($ship_price) && $ship_price != 0 ? $merchant->expedition_name . ' (' . ceil($ship) . ' x @' . number_format($ship_price) . ') ' . RP . number_format(ceil($ship) * $ship_price) : $merchant->expedition_name ." (<b class='text-green'>Free</b>)"?>
                                    </div>
                                    <div class="col-lg-2 col-md-3 col-sm-3">
                                        <label style="text-align: left !important"><?php echo RP . number_format((ceil($ship) * $ship_price) + $sub_total) ?></label>
                                    </div>
                                </td>
                            <?php }
                            ?>
                            <tr>
                                <th class='field-content' colspan='2' ><div class='text-right'><font size="3px"  >Total Belanja : <?php echo cnum($total); ?></font></div></th>
                            </tr>
                            <tr><th colspan="2">
                            <div class="row">
                                <div class="col-xs-12  text-right">
                                    <font size="3px">Voucher : Rp <?php echo cnum($data_sel[LIST_DATA][3][0]->nominal); ?></font>
                                </div>
                            </div>
                            </th></tr>
                            <tr>
                                <th class="field-content" colspan="2">
                            <div class="row">
                                <div class="col-xs-12" style="text-align: right; ">
                                    <font size="4px">Total Bayar : <font color="red">Rp <?php echo cnum($total - $data_sel[LIST_DATA][3][0]->nominal); ?></font></font></a>
                                </div>
                            </div>

                            </th>
                            </tr>
                        </table>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </div>

        <?php if (isset($customer_table) && $table_simulation): ?>
            <div class="col-md-12 order-striped">
                <div class="m-bottom-20px">    
                    <?php echo $customer_table; ?>
                </div>
            </div>
            <br>
            <div class="col-md-12 order-striped">
                <div class="m-bottom-20px">    
                    <?php echo $table_simulation; ?>
                </div>
            </div>
        <?php endif; ?>

    </div>    
</div>
<a href="<?php echo get_base_url() . 'admin/transaction/order'; ?>" class="btn btn-google-plus">Back</a>
<?php
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>