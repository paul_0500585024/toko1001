<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
?>
<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_css_url() ?>daterangepicker-bs3.css" />
    <script type="text/javascript" src="<?php echo get_js_url() ?>moment.min.js"></script>
    <script type="text/javascript" src="<?php echo get_js_url() ?>daterangepicker.js"></script>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo $data_auth[FORM_ACTION_TITLE] . $data_auth[FORM_AUTH][FORM_TITLE] ?></h3>
        </div>
        <form class ="form-horizontal" id="frmMain" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
            <div class="box-body">
                <section class="col-md-6">
                    <?php echo get_csrf_admin_token(); ?>
                    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                        <input class="form-control"  name ="seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->seq; ?>">
                    <?php } ?>
                    <div class ="form-group">
                        <label class ="control-label col-md-3">Dari Tanggal</label>
                        <div class ="col-md-9">
                            <input class="form-control" readonly id="from_date" name ="from_date" type="text" placeholder ="Tanggal Awal" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? date("d-M-Y", strtotime($data_sel[LIST_DATA][0]->from_date)) : date("d-M-Y", strtotime("2016-01-01")) ); ?>" >
                        </div>
                    </div>
                    <div class ="form-group">
                        <label class ="control-label col-md-3">Ke Tanggal</label>
                        <div class ="col-md-9">
                            <input class="form-control" validate="required[]" date_type="date" id ="to_date" name ="to_date" type="text" placeholder ="Tanggal Akhir" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? date("d-M-Y", strtotime($data_sel[LIST_DATA][0]->to_date)) : "" ); ?>" readonly style="background-color: #fff;">
                        </div>
                    </div>
                    <div class ="form-group">
                        <?php
                        if ($data_auth[FORM_ACTION] == ACTION_VIEW) {
                            ?>
                            <div class ="col-md-6"><?php echo get_back_button(); ?> </div>
                            <?php
                        } else {
                            if ($data_auth[FORM_ACTION] == ACTION_ADD) {
                                ?>
                                <div class ="col-md-6"><?php echo get_save_add_button(); ?> </div>
                            <?php } if ($data_auth[FORM_ACTION] == ACTION_EDIT && $data_sel[LIST_DATA][0]->status == "O") { ?>
                                <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                            <?php } ?>
                            <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                        <?php } ?>
                    </div>
                </section>
            </div>
        </form>
    </div>
    <script type="text/javascript">
            $('input[date_type="date"]').daterangepicker({
                format: 'DD-MMM-YYYY',
                singleDatePicker: true,
                showDropdowns: true,
                maxDate: '<?php echo date("d-M-Y"); ?>'
    <?php echo (isset($data_sel[LIST_DATA][0]) ? ',minDate: "' . date("d-M-Y", strtotime($data_sel[LIST_DATA][0]->from_date)) . '"' : ',minDate: "' . date("d-M-Y", strtotime("2016-01-01")) . '"' ); ?>
            });
    </script>
<?php } else { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <?php require_once get_include_page_list_admin_content_header(); ?>
            <table id="tbl" width="100%" class="display table table-bordered table-striped" url="<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
                <thead>
                    <tr>
                        <th column="from_date"> Dari Tanggal</th>
                        <th column="to_date"> Ke Tanggal</th>
                        <th column="status"> Status </th>
                        <th column="total"> Total </th>
                        <th column="action"> Pembayaran</th>
                        <th column="created_by"> <?php echo TH_CREATED_BY; ?> </th>
                        <th column="created_date"> <?php echo TH_CREATED_DATE; ?> </th>
                        <th column="modified_by"> <?php echo TH_MODIFIED_BY; ?> </th>
                        <th column="modified_date"> <?php echo TH_MODIFIED_DATE; ?> </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <?php
}
require_once get_include_page_list_admin_content_footer();
if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) {

} else {
    ?>
    <script type="text/javascript">
        <!--
        var url = "<?php echo get_base_url() . $data_auth[FORM_URL]; ?>";
        function proses_redeem(seq, id) {
            if (confirm('Proses periode redem ini?')) {
                $.ajax({
                    url: url,
                    data: {btnAdditional: "act_s_adt", idh: seq, tipe: "proses_redeem"
                    },
                    type: "POST",
                    success: function(response) {
//                        alert(response);
                        if (isSessionExpired(response)) {
                            response_object = json_decode(response);
                            url = response_object.url;
                            location.href = url;
                        } else {
                            response = response.replace(/(?:\r\n|\r|\n)/g, '').trim();
                            if (response.trim() == 'OK') {
                                table.ajax.reload();
                            } else {
                                alert(response);
                            }
                        }
                    },
                    error: function(request, error) {
                        alert(error_response(request.status));
                    }
                });
            }

        }
        //-->
    </script>
    <?php
}
require_once get_include_content_admin_bottom_page_navigation();
?>