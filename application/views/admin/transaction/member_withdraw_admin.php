<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
?>
<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
    </div>
    <form class ="form-horizontal" id="frmSearch" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
        <div class="box-body">
            <?php require_once get_include_page_list_admin_content_header(); ?>
            <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th column="member_email"> Member </th>
                        <th column="deposit_amt"> Saldo </th>
                        <th column="trx_no"> Nomor Pengajuan </th>
                        <th column="trx_date"> Tanggal Pengajuan </th>
                        <th column="deposit_trx_amt"> Tarik Dana </th>
                        <th column="non_deposit_trx_amt"> Kartu Kredit </th>
                        <th column="bank_name"> Bank </th>
                        <th column="bank_acct_no"> Nomor Rekening </th>
                        <th column="bank_acct_name"> A/N </th>
                        <th column="status"> Status </th>
                        <th column="refund_date"> Tanggal Pengembalian </th>
                    </tr>
                </thead>
            </table>
        </div>
    </form>
</div>
<?php
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>