<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
?>

<!--<section class="content">-->
<div class="row">
    <div class="col-lg-2 col-xs-6">
        <div class="small-box" style="background-color: #f4701e; color: #fff">
            <div class="inner">
                <h3><?php echo $order_need_confirmation ?></h3>
                <p>New Order</p>
            </div>
            <div class="icon">
                <i class="ion ion-ios-cart-outline"></i>
            </div>
            <a href="<?php echo get_base_url() ?>admin/transaction/order" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-lg-2 col-xs-6">
        <div class="small-box" style="background-color: #ca248e; color: #fff">
            <div class="inner">
                <h3><?php echo $withdraw_need_confirmation ?></h3>
                <p>Pending Withdraw</p>
            </div>
            <div class="icon">
                <i class="ion ion-ios-pricetags-outline"></i>
            </div>
            <a href="<?php echo get_base_url() ?>admin/transaction/member_withdraw_admin" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-lg-2 col-xs-6">
        <div class="small-box" style="background-color: #5d3380; color: #fff">
            <div class="inner">
                <h3><?php echo $new_merchant ?></h3>
                <p>New Merchant</p>
            </div>
            <div class="icon">
                <i class="ion ion-ios-personadd-outline"></i>
            </div>
            <a href="<?php echo get_base_url() ?>admin/master/merchant" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-lg-2 col-xs-6">
        <div class="small-box" style="background-color: #CC0000; color: #fff">
            <div class="inner">
                <h3><?php echo "334,780" ?></h3>
                <p>Active User (Maret 2017)</p>
            </div>
            <div class="icon">
                <i class="ion ion-ios-box-outline"></i>
            </div>
            <a href="<?php echo get_base_url() ?>admin/master/product_new" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
<!--    <div class="col-lg-2 col-xs-6">
        <div class="small-box" style="background-color: #8A8A8A; color: #fff">
            <div class="inner">
                <h3><?php echo $pending_refund ?></h3>
                <p>Pending Refund</p>
            </div>
            <div class="icon">
                <i class="ion ion-ios-pricetags-outline"></i>
            </div>
            <a href="<?php echo get_base_url() ?>admin/transaction/member/refund" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>-->
    <div class="col-lg-2 col-xs-6">
        <div class="small-box" style="background-color: #8A8A8A; color: #fff">
            <div class="inner">
                <h3><?php echo "302,508" ?></h3>
                <p>Active User (April 2017)</p>
            </div>
            <div class="icon">
                <i class="ion ion-ios-pricetags-outline"></i>
            </div>
            <a href="<?php echo get_base_url() ?>admin/transaction/member/refund" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>

    <div class="col-lg-2 col-xs-6">
        <div class="small-box" style="background-color: green; color: #fff">
            <div class="inner">
                <h3><?php echo  "239,501"?></h3>
                <p>Active User (May 2017)</p>
            </div>
            <div class="icon">
                <i class="ion ion-ios-world-outline"></i>
            </div>
            <a href="<?php echo get_base_url() ?>admin/transaction/order_merchant" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="chart">
                    <div id="charts2"></div>
                    <table id="datatable2" style='display: none'>
                        <thead>
                            <tr>
                                <th></th>
                                <th>Session User</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($active_user as $data) { ?>
                                <tr>
                                    <th><?php echo $data->join_month ?></th>
                                    <td><?php echo isset($data->active_user) ? $data->active_user : 0 ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!--</section>-->

<script type="text/javascript">
    $(function() {
        $('#charts').highcharts({
            data: {
                table: 'datatable'
            },
            chart: {
                type: 'column'
            },
            title: {
                text: 'Perkembangan Lingkup Kerja Toko 1001'
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Bergabung'
                }}
            //            tooltip: {
            //                formatter: function() {
            //                    return '<b>' + this.series.name + '</b><br/>' +
            //                            this.point.y + ' ' + this.point.name.toLowerCase();
            //                }
//            }
        });

        $('#charts2').highcharts({
            data: {
                table: 'datatable2'
            },
            chart: {
                type: 'column'
            },
            title: {
                text: 'Active User Toko1001'
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Bergabung'
                }}
            //            tooltip: {
            //                formatter: function() {
            //                    return '<b>' + this.series.name + '</b><br/>' +
            //                            this.point.y + ' ' + this.point.name.toLowerCase();
            //                }
//            }
        });
    });

</script>

<?php
require_once get_include_content_admin_bottom_page_navigation();
?>