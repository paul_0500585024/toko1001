<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
?>

<!--<section class="content">-->
<div class="row">
    <div class="col-lg-2 col-xs-6">
        <div class="small-box" style="background-color: #f4701e; color: #fff">
            <div class="inner">
                <h3><?php echo $order_need_confirmation ?></h3>
                <p>New Order</p>
            </div>
            <div class="icon">
                <i class="ion ion-ios-cart-outline"></i>
            </div>
            <a href="<?php echo get_base_url() ?>admin/transaction/order" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-lg-2 col-xs-6">
        <div class="small-box" style="background-color: #ca248e; color: #fff">
            <div class="inner">
                <h3><?php echo $withdraw_need_confirmation ?></h3>
                <p>Pending Withdraw</p>
            </div>
            <div class="icon">
                <i class="ion ion-ios-pricetags-outline"></i>
            </div>
            <a href="<?php echo get_base_url() ?>admin/transaction/member_withdraw_admin" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-lg-2 col-xs-6">
        <div class="small-box" style="background-color: #5d3380; color: #fff">
            <div class="inner">
                <h3><?php echo $new_merchant ?></h3>
                <p>New Merchant</p>
            </div>
            <div class="icon">
                <i class="ion ion-ios-personadd-outline"></i>
            </div>
            <a href="<?php echo get_base_url() ?>admin/master/merchant" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-lg-2 col-xs-6">
        <div class="small-box" style="background-color: #CC0000; color: #fff">
            <div class="inner">
                <h3><?php echo $new_products ?></h3>
                <p>New Product</p>
            </div>
            <div class="icon">
                <i class="ion ion-ios-box-outline"></i>
            </div>
            <a href="<?php echo get_base_url() ?>admin/master/product_new" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-lg-2 col-xs-6">
        <div class="small-box" style="background-color: #8A8A8A; color: #fff">
            <div class="inner">
                <h3><?php echo $pending_refund ?></h3>
                <p>Pending Refund</p>
            </div>
            <div class="icon">
                <i class="ion ion-ios-pricetags-outline"></i>
            </div>
            <a href="<?php echo get_base_url() ?>admin/transaction/member/refund" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-lg-2 col-xs-6">
        <div class="small-box" style="background-color: green; color: #fff">
            <div class="inner">
                <h3><?php echo $pending_delivery ?></h3>
                <p>Pending Delivery</p>
            </div>
            <div class="icon">
                <i class="ion ion-ios-world-outline"></i>
            </div>
            <a href="<?php echo get_base_url() ?>admin/transaction/order_merchant" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="chart">
                    <div id="charts"></div>
                    <table id="datatable" style='display: none'>
                        <thead>
                            <tr>
                                <th></th>
                                <th>Merchant</th>
                                <th>Member</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>January</th>
                                <td><?php echo $merchant_january ?></td>
                                <td><?php echo $member_january ?></td>
                            </tr>
                            <tr>
                                <th>February</th>
                                <td><?php echo $merchant_february ?></td>
                                <td><?php echo $member_february ?></td>
                            </tr>
                            <tr>
                                <th>March</th>
                                <td><?php echo $merchant_march ?></td>
                                <td><?php echo $member_march ?></td>
                            </tr>
                            <tr>
                                <th>April</th>
                                <td><?php echo $merchant_april ?></td>
                                <td><?php echo $member_april ?></td>
                            </tr>
                            <tr>
                                <th>May</th>
                                <td><?php echo $merchant_may ?></td>
                                <td><?php echo $member_may ?></td>
                            </tr>
                            <tr>
                                <th>June</th>
                                <td><?php echo $merchant_june ?></td>
                                <td><?php echo $member_june ?></td>
                            </tr>
                            <tr>
                                <th>July</th>
                                <td><?php echo $merchant_july ?></td>
                                <td><?php echo $member_july ?></td>
                            </tr>
                            <tr>
                                <th>August</th>
                                <td><?php echo $merchant_august ?></td>
                                <td><?php echo $member_august ?></td>
                            </tr>
                            <tr>
                                <th>September</th>
                                <td><?php echo $merchant_september ?></td>
                                <td><?php echo $member_september ?></td>
                            </tr>
                            <tr>
                                <th>October</th>
                                <td><?php echo $merchant_october ?></td>
                                <td><?php echo $member_october ?></td>
                            </tr>
                            <tr>
                                <th>November</th>
                                <td><?php echo $merchant_november ?></td>
                                <td><?php echo $member_november ?></td>
                            </tr>
                            <tr>
                                <th>December</th>
                                <td><?php echo $merchant_december ?></td>
                                <td><?php echo $member_december ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!--</section>-->

<script type="text/javascript">
    $(function() {
        $('#charts').highcharts({
            data: {
                table: 'datatable'
            },
            chart: {
                type: 'column'
            },
            title: {
                text: 'Perkembangan Lingkup Kerja Toko 1001'
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Bergabung'
                }}
            //            tooltip: {
            //                formatter: function() {
            //                    return '<b>' + this.series.name + '</b><br/>' +
            //                            this.point.y + ' ' + this.point.name.toLowerCase();
            //                }
//            }
        });
    });

</script>

<?php
require_once get_include_content_admin_bottom_page_navigation();
?>