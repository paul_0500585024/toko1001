<?php
require_once VIEW_BASE_ADMIN;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">

<html>
    <head>
        <title>Toko1001</title>
        <link rel="icon" href="<?php echo get_img_url(); ?>home/icon/pavicon.png"/>
        <link href="<?php echo get_css_url(); ?>bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_css_url(); ?>admin/toko1001_admin.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_css_url(); ?>font-awesome.min.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="<?php echo get_js_url(); ?>jQuery-2.1.4.min.js" ></script>
        <script type="text/javascript" src="<?php echo get_js_url(); ?>jquery.validate.js" ></script>
    </head>
    <body class="login-page">   
        <div class="login-box">
            <div class="login-logo">
                <a href="<?php echo base_url() ?>"><b>ADMIN</b> Toko1001 </a>
            </div>
            <div class="login-box-body">
                <?php if ($data_err[ERROR] === true) { ?>
                    <p class="login-box-msg text-red"><?php echo $data_err[ERROR_MESSAGE][0] ?> </p> 
                <?php } else { ?>   
                <?php } ?>
                <form id="frmlogin" method = "post" action="<?php echo get_base_url() ?>admin/login/sign_in<?php echo isset($data_auth[FORM_AUTH][FORM_URL]) ? $data_auth[FORM_AUTH][FORM_URL] : ""; ?>">

                    <div class="form-group has-feedback">
                        <input id="txtUserName" name="user_id" type="text" class="form-control"
                               placeholder="ID User" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA]->user_id : "") ?>"/> <span
                               class="glyphicon glyphicon-user form-control-feedback" ></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input id="txtPassword" name="password" type="password"
                               class="form-control" placeholder="Password"  value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA]->password : "") ?>"/> <span
                               class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <button name="submit" type="submit" class="btn btn-google-plus" style="background-color: #4E4E4E; color: #fff"><i class="fa fa-sign-in"></i>&nbsp; Masuk</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <script>
            $('#frmlogin').validate({
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                rules: {
                    user_id: {
                        required: true
                    },
                    password: {
                        required: true,
                        minlength: 8,
                        maxlength: 20
                    }
                }
            });
            $.validator.messages.required = "Data wajib diisi !";
            $.validator.messages.maxlength = "Harap diisi tidak lebih dari {0} karakter !";
            $.validator.messages.minlength = "Harap diisi dengan minimal {0} karakter !";
        </script>
    </body>
</html>