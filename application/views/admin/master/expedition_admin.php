<?php
require_once VIEW_BASE_ADMIN;

require_once get_include_content_admin_top_page_navigation();
?>

<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo $data_auth[FORM_ACTION_TITLE] . $data_auth[FORM_AUTH][FORM_TITLE] ?></h3>
        </div>
        <form class ="form-horizontal" id="frmMain" onsubmit ="return validate_form();" method ="post" enctype ="multipart/form-data" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
            <div class="box-body">
                <div class="col-md-6">
                    <?php echo get_csrf_admin_token(); ?>
                    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                        <input class="form-control"  name ="seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->seq; ?>">
                        <input class="form-control"  name ="old_img" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->old_img; ?>">
                    <?php } ?>
                    <div class ="form-group">
                        <label class ="control-label col-md-3">Kode Ekspedisi *</label>
                        <div class ="col-md-9">
                            <input class="form-control" name ="code" maxlength="25" validate ="required[]" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->code) : "" ); ?>" >
                        </div>
                    </div>
                    <div class ="form-group">
                        <div class ="col-md-offset-3 col-md-9">
                            <label> <input type="checkbox" name ="active" <?php echo (!isset($data_sel[LIST_DATA]) ? "checked" : (($data_sel[LIST_DATA][0]->active == "1" OR $data_sel[LIST_DATA][0]->active == "on") ? "checked" : "")); ?> /> Aktif</label>
                        </div>
                    </div>
                    <div class ="form-group">
                        <label class ="control-label col-md-3">Nama Ekspedisi *</label>
                        <div class ="col-md-9">
                            <input class="form-control" name ="name" maxlength="50" validate ="required[]" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->name) : "" ); ?>">
                        </div>
                    </div>
                    <div class ="form-group">
                        <label class ="control-label col-md-3">Cara Buat Resi</label>
                        <div class ="col-md-9">
                            <input type="radio" name="awb_method" value="A" <?php echo (!isset($data_sel[LIST_DATA]) ? "" : (($data_sel[LIST_DATA][0]->awb_method == 'A') ? "checked" : "")); ?>> API &nbsp;
                            <input type="radio" name="awb_method" value="L" <?php echo (!isset($data_sel[LIST_DATA]) ? "checked" : (($data_sel[LIST_DATA][0]->awb_method == "L" OR $data_sel[LIST_DATA][0]->awb_method == "L") ? "checked" : "")); ?>> Lokal
                        </div>
                    </div>
                    <div class ="form-group">
                        <label class ="control-label col-md-3">Premi Asuransi (%) *</label>
                        <div class ="col-md-9">
                            <input class="form-control auto" validate ="required[]" data-d-group="3" data-a-pad="false" data-v-max="100" data-m-dec="2" name ="ins_rate_percent" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->ins_rate_percent : 0 ); ?>">
                        </div>
                    </div>
                    <div class ="form-group">
                        <label class ="control-label col-md-3">Keterangan</label>
                        <div class ="col-md-9">
                            <input class="form-control" name="notes" type="input" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->notes) : "" ); ?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <?php
                    if (isset($data_sel[LIST_DATA])) {
                        if ($data_sel[LIST_DATA][0]->logo_img == '') {
                            $gambar = get_base_url() . IMG_BLANK_100;
                        } else {
                            $gambar = get_base_url() . EXPEDITION_UPLOAD_IMAGE . $data_sel[LIST_DATA][0]->logo_img;
                        }
                    } else {
                        $gambar = get_base_url() . IMG_BLANK_100;
                    }
                    ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">Gambar Logo (h:70 - 200px, w:70 - 200px)</div>
                        <div class="panel-body">
                            <input type="file" accept="<?php echo IMAGE_TYPE_UPLOAD; ?>" id="nlogo_img" name="nlogo_img" class="btn btn-default" onchange ="previewimg(this)" <?php echo ($data_auth[FORM_ACTION] == ACTION_ADD ? "validate=\"required[]\"" : ""); ?>/>
                            <img id="iprev" src="<?php echo $gambar; ?>" height="70" />
                        </div>
                    </div>
                    <div class ="form-group">
                        <?php
                        if ($data_auth[FORM_ACTION] == ACTION_VIEW) {
                            ?>
                            <div class ="col-md-6"><?php echo get_back_button(); ?> </div>
                            <?php
                        } else {
                            if ($data_auth[FORM_ACTION] == ACTION_ADD) {
                                ?>
                                <div class ="col-md-6"><?php echo get_save_add_button(); ?> </div>
                            <?php } if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                                <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                            <?php } ?>
                            <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <script type="text/javascript">
            function previewimg(thisval) {
                if (thisval.files && thisval.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#iprev').attr('src', e.target.result).height(70);
                    }
                    reader.readAsDataURL(thisval.files[0]);
                }
            }
    </script>

<?php } else { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <?php require_once get_include_page_list_admin_content_header(); ?>
            <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th column="code">Kode Ekspedisi</th>
                        <th column="name">Nama Ekspedisi</th>
                        <th column="active">Aktif</th>
                        <th column="detail">Paket</th>
                        <th column="awb">Resi</th>
                        <th column="created_by"><?php echo TH_CREATED_BY; ?></th>
                        <th column="created_date"><?php echo TH_CREATED_DATE; ?></th>
                        <th column="modified_by"><?php echo TH_MODIFIED_BY; ?></th>
                        <th column="modified_date"><?php echo TH_MODIFIED_DATE; ?></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <?php
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>