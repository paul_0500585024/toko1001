<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();

?>

<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo ($data_auth[FORM_ACTION] == ACTION_ADD ? get_title_add($data_auth[FORM_AUTH][FORM_TITLE]) : get_title_edit($data_auth[FORM_AUTH][FORM_TITLE])); ?></h3>
    </div>
    <form id="frmMain" class="form-horizontal" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
        <div class="box-body">
            <section class="col-md-12">
                <?php echo get_csrf_admin_token(); ?>
                <input name="old_promo_credit_period_from" type="hidden" value="<?php echo $data_sel[LIST_DATA][0]->promo_credit_period_from; ?>">
                <input name="old_promo_credit_period_to" type="hidden" value="<?php echo $data_sel[LIST_DATA][0]->promo_credit_period_to; ?>">
                <input name="old_promo_credit_name" type="hidden" value="<?php echo $data_sel[LIST_DATA][0]->promo_credit_name; ?>">
                <div class="form-group">
                    <div class="col-md-12">
                        <div class ="form-group">
                            <label class ="control-label col-md-2">Periode Cicilan</label>
                            <div class ="col-md-6">
                                <input class="form-control" date_type="date" id ="a0" name ="a0" type="text"  value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? date("d-M-Y", strtotime($data_sel[LIST_DATA][0]->promo_credit_period_from)) . ' - ' . date("d-M-Y", strtotime($data_sel[LIST_DATA][0]->promo_credit_period_to)) : "" ); ?>" readonly>
                            </div>
                        </div>
                        <div class ="form-group">
                            <label class ="control-label col-md-2">Nama Promo Cicilan </label>
                            <div class ="col-md-6">
                                <input class="form-control" id="notes" readonly name ="promo_credit_name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->promo_credit_name) : "" ); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <hr>
                    </div>
                    <div class='col-md-6' style="margin-top:-10px;">
                        <p><h4>Credit Bank : </h4></p>
                        <div class="well">
                            <table border='0' class="table-informasi">
                                <style> .table-informasi td{padding: 2px;}
                                </style>
                                <tr>
                                    <td>
                                        <?php if (isset($data_sel[LIST_DATA][1])) {foreach ($data_sel[LIST_DATA][1] as $each) { ?>
                                            <input name='bank[]' type='checkbox' value="<?php echo $each->seq ?>" <?php echo isset($each->promo_seq) ? 'checked' : '' ?>> <?php echo $each->bank_month ?></br>                                              
                                        <?php }} ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class = "box-footer">
                    <div class = "row">
                        <div class = "col-md-6"><?php echo get_save_add_button(); ?> </div>
                        <div class ="col-md-6"><?php echo get_cancel_link(get_base_url() . "admin/master/promo_credit"); ?></div>
                    </div>
                </div>
            </section>
        </div><!-- /.box-footer -->
    </form>
</div>
<?php
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>