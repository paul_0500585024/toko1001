<?php
require_once VIEW_BASE_ADMIN;
?>

<form id ="frmSearch"  url= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
    <div class="form-group">
        <label>Nama Partner</label>
        <?php require_once get_component_url() . "dropdown/com_drop_partner_dp_get_partner_name.php" ?>
    </div>
    <div class="form-group">
        <label>Category</label>
        <?php require_once get_component_url() . "dropdown/com_drop_partner_dp_category.php" ?>
    </div>
    <div class="form-group">
        <label>Tenor</label>
        <?php require_once get_component_url() . "dropdown/com_drop_partner_dp_tenor.php" ?>
    </div>
    <div class="form-group">
        <input name ="active" type="checkbox" checked> Aktif </input>
    </div>
    <div class="form-group">
        <?php echo get_search_button(); ?>
    </div>
</form>
 <script text="javascript">
            $("#drop_category").change(function() {
                if ($(this).val() != ""){
                    $.ajax({
                        url: "<?php echo get_base_url() . $data_auth[FORM_URL] ?>",
                        dataType: "json",
                        data: {
                            btnAdditional: true,
                            product_category_seq: $(this).val()
                        },
                        type: "POST",
                        success: function(data) {
                            if(isSessionExpired(data)){
                                response_object = json_decode(data);
                                url = response_object.url;
                                location.href = url;
                            }else{
                                $('#drop_tenor option').remove();
                                $("#drop_tenor").append("<option value=\"" + "\">-- Pilih --</option>");
                                $(data).each(function(i) {
                                    $("#drop_tenor").append("<option value=\"" + data[i].seq + "\">" + data[i].tenor_loan + "</option>");
                                });
                                $("#drop_tenor").val(<?php echo isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA]->partner_loan_seq : ""; ?>);
                                $('#drop_tenor').select2();
                            }
                        },
                        error: function (request, error) {
                            $('#error').html(JSON.stringify(request));
                        }
                    });
                }
            }).change();
    </script>