<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
?>

<?php if ($data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="box box-default">
        <div class="box-header with-border">
    	<h3 class="box-title"><?php echo $data_auth[FORM_AUTH][FORM_TITLE] ?></h3>
        </div>
        <form class="form-horizontal" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>" id="frmMain" onsubmit ="return validate_form();">
    	<div class="box-body">
		<?php echo get_csrf_admin_token(); ?>
    	    <input name ="order_seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->order_seq; ?>">
    	    <input name ="product_variant_seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->product_variant_seq; ?>">
    	    <div class="col-md-6">
    		<div class ="form-group">
    		    <label class ="control-label col-md-3">Nama Produk</label>
    		    <div class ="col-md-9">
    			<input class="form-control" type="text" value ="<?php echo get_display_value($data_sel[LIST_DATA][0]->product_name) ?>" readonly="" name="product_name">
    		    </div>
    		</div>
    	    </div>
    	    <div class="col-md-6">
    		<div class ="form-group">
    		    <label class ="control-label col-md-3">Rate</label>
    		    <div class ="col-md-2">
    			<input class="form-control" type="text" value ="<?php echo get_display_value($data_sel[LIST_DATA][0]->rate) ?>" readonly="" name="rate">
    		    </div>
    		</div>
    	    </div>
    	    <div class="col-md-6">
    		<div class ="form-group">
    		    <label class ="control-label col-md-3">Review</label>
    		    <div class ="col-md-9">
    			<textarea class="form-control" rows="2" style="overflow:auto;resize:none" readonly="" name="review"><?php echo get_display_value($data_sel[LIST_DATA][0]->review) ?></textarea>
    		    </div>
    		</div>
    	    </div>
    	    <div class="col-md-6">
    		<div class ="form-group">
    		    <label class ="control-label col-md-3">Review Admin</label>
    		    <div class ="col-md-9">
    			<textarea class="form-control" rows="2" style="overflow:auto;resize:none" name="review_admin"><?php echo get_display_value($data_sel[LIST_DATA][0]->review_admin) ?></textarea>
    		    </div>
    		</div>
    	    </div>

    	    <div class ="form-group">
		    <?php
		    if ($data_auth[FORM_ACTION] == ACTION_EDIT) {
			if ($data_sel[LIST_DATA][0]->status == NEW_STATUS_CODE) {
			    ?>
	    		<div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
	    		<div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
			    <?php
			} else {
			    echo '<div class ="col-md-6">' . get_back_button() . '</div>';
			}
		    }
		    if ($data_auth[FORM_ACTION] == ACTION_VIEW) {
			?>
			<div class ="col-md-6"><?php echo get_back_button(); ?></div>
		    <?php } ?>
    	    </div>
    	</div>
        </form>
    </div>
<?php } else { ?>
    <div class="box box-default">
        <div class="box-header with-border">
    	<h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
	    <?php require_once get_include_page_list_admin_content_header(); ?>
    	<table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
    	    <thead>
    		<tr>
    		    <th column="product_name">Produk</th>
    		    <th column="order_no">No. Order</th>
    		    <th column="rate">Rate</th>
    		    <th column="review">Isi ulasan</th>
    		    <th column="review_admin">Revisi ulasan</th>
    		    <th column="status">Status</th>
    		    <th column="created_by"> <?php echo TH_CREATED_BY; ?> </th>
    		    <th column="created_date"> <?php echo TH_CREATED_DATE; ?> </th>
    		    <th column="modified_by"> <?php echo TH_MODIFIED_BY; ?> </th>
    		    <th column="modified_date"> <?php echo TH_MODIFIED_DATE; ?> </th>
    		</tr>
    	    </thead>
    	</table>
        </div>
    </div>

    <?php
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>

