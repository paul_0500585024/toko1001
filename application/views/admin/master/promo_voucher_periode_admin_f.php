<?php
require_once VIEW_BASE_ADMIN;
?>
<link rel="stylesheet" type="text/css" href="<?php echo get_css_url() ?>daterangepicker-bs3.css" />
<script type="text/javascript" src="<?php echo get_js_url() ?>moment.min.js"></script>
<script type="text/javascript" src="<?php echo get_js_url() ?>daterangepicker.js"></script>
<form id ="frmSearch"  url= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
    <div class="form-group">
        <label>Periode Mulai</label>
        <div>

            <input type="text" name="date_from" class="form-control" value="<?php echo date('d-M-Y') ?>"  readonly style="background-color: #fff;">
            </br>
            <label>Periode Akhir</label>
            <input type="text" name="date_to" class="form-control"  value="<?php echo date('d-M-Y', strtotime(NEXT_WEEK)) ?>" readonly style="background-color: #fff;">
        </div>
    </div>
    <div class="form-group">
        <label>Nama Voucher</label>
        <input class="form-control" name="v_name" type="input">
    </div>
    <div class="form-group">
        <label>Kode Voucher</label>
        <input class="form-control" name="v_code" type="input">
    </div>
    <div class="form-group">
        <label>Transaksi</label>
        <div>
            <?php include dirname(__FILE__) . '/../component/dropdown/com_drop_promo_voucher_node.php' ?>
        </div>
    </div>
    <div class="form-group">
        <label>Status</label>
        <select class="form-control select2" name="status">
            <option value="">Semua</option>
            <?php
            echo combostatus(json_decode(STATUS_MEMBER));
            ?>
        </select>
    </div>
    <div class="form-group">
        <input name ="active" type="checkbox" checked> Aktif
    </div>
    <div class="form-group">
        <?php echo get_search_button(); ?>
    </div>
</form>

<script  type="text/javascript">
    $('input[name="date_from"]').daterangepicker({
        format: 'DD-MMM-YYYY',
        showDropdowns: true,
        singleDatePicker: true,
        startDate: "<?php echo date('d-M-Y') ?>"
    });
    $('input[name="date_to"]').daterangepicker({
        format: 'DD-MMM-YYYY',
        singleDatePicker: true,
        showDropdowns: true,
        startDate: "<?php echo date('d-M-Y', strtotime(NEXT_WEEK)) ?>"
    });
</script>