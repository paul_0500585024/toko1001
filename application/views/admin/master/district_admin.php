<?php
require_once VIEW_BASE_ADMIN;

require_once get_include_content_admin_top_page_navigation();
?>

<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo $data_auth[FORM_ACTION_TITLE] . $data_auth[FORM_AUTH][FORM_TITLE] ?></h3>
        </div>
        <form class ="form-horizontal" id='frmMain' onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
            <div class="box-body">
                <section class="col-md-6">
                    <?php echo get_csrf_admin_token(); ?>
                    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                        <input class="form-control"  name ="seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->seq; ?>">
                    <?php } ?>
                    <div class ="form-group">     
                        <label class ="control-label col-md-3">Propinsi *</label>
                        <div class ="col-md-9">
                            <?php require_once get_component_url() . "dropdown/com_drop_province.php" ?>
                        </div>
                    </div>
                    <div class ="form-group">     
                        <label class ="control-label col-md-3">Kota / Kabupaten *</label>
                        <div class ="col-md-9" >
                            <?php require_once get_component_url() . "dropdown/com_drop_city.php" ?>
                        </div>
                    </div>
                    <div class ="form-group">     
                        <label class ="control-label col-md-3">Nama Kecamatan *</label>   
                        <div class ="col-md-9">
                            <input class="form-control"  validate ="required[]" name ="district_name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->name) : "" ); ?>">
                        </div>
                    </div>
                    <div class ="form-group">
                        <div class ="col-md-offset-3 col-md-9">
                            <label> <input type="checkbox" name ="active" <?php echo (!isset($data_sel[LIST_DATA]) ? "checked" : (($data_sel[LIST_DATA][0]->active == "1" OR $data_sel[LIST_DATA][0]->active == "on") ? "checked" : "")); ?> /> Aktif</label>
                        </div>
                    </div>
                    <div class ="form-group">
                        <?php
                        if ($data_auth[FORM_ACTION] == ACTION_VIEW) {
                            ?>
                            <div class ="col-md-6"><?php echo get_back_button(); ?> </div>
                            <?php
                        } else {
                            if ($data_auth[FORM_ACTION] == ACTION_ADD) {
                                ?>
                                <div class ="col-md-6"><?php echo get_save_add_button(); ?> </div>
                            <?php } if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                                <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                            <?php } ?>
                            <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                        <?php } ?>
                    </div>
                </section>
            </div>
        </form>
    </div>
    <script text="javascript">
            $("#drop_province").change(function() {
                if ($(this).val() != ""){
                    $.ajax({
                        url: "<?php echo get_base_url() . $data_auth[FORM_URL] ?>",
                        dataType: "json",
                        data: {
                            btnAdditional: true,
                            province_seq: $(this).val()
                        },
                        type: "POST",
                        success: function(data) {
                            if(isSessionExpired(data)){
                                response_object = json_decode(data);
                                url = response_object.url;
                                location.href = url;
                            }else{
                                $('#drop_city option').remove();
                                $("#drop_city").append("<option value=\"" + "\">-- Pilih --</option>");
                                $(data).each(function(i) {
                                    $("#drop_city").append("<option value=\"" + data[i].seq + "\">" + data[i].name + "</option>");
                                });
                                $("#drop_city").val(<?php echo isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA]->city_seq : ""; ?>);
                                $('#drop_city').select2();
                            }
                        },
                        error: function (request, error) {
                            alert(error_response(request.status));
                        }
                    });
                }
            }).change();
    </script>
<?php } else { ?>    
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <?php require_once get_include_page_list_admin_content_header(); ?>
            <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>                                          
                        <th column="province_name">Propinsi</th>
                        <th column="city_name">Kota / Kabupaten</th>
                        <th column="district_name">Nama Kecamatan</th>
                        <th column="active"> Aktif </th>
                        <th column="created_by"> <?php echo TH_CREATED_BY; ?> </th>
                        <th column="created_date"> <?php echo TH_CREATED_DATE; ?> </th>
                        <th column="modified_by"> <?php echo TH_MODIFIED_BY; ?> </th>
                        <th column="modified_date"> <?php echo TH_MODIFIED_DATE; ?> </th>
                    </tr>
                </thead>
            </table>    
        </div>
    </div>
    <?php
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>
