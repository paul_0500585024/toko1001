<?php
require_once VIEW_BASE_ADMIN;
?>

<form id="frmSearch" url="<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
    <div class="form-group">
        <label>Agen Email</label>
        <?php require_once get_component_url() . "dropdown/com_drop_agent.php" ?>
    </div>
    <div class="form-group">
        <label>Status</label>
        <select id="status" class="form-control select2" name="status">
            <option value="">Semua</option>
            <?php
            echo combostatus(json_decode(STATUS_WITHDRAW_NO_T), "");
            ?>
        </select>
    </div>
    <div class="form-group">
        <?php echo get_search_button(); ?>
    </div>
</form>
