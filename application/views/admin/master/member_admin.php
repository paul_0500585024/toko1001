<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
?>

<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo $data_auth[FORM_ACTION_TITLE] . $data_auth[FORM_AUTH][FORM_TITLE] ?></h3>
        </div>
        <form class="form-horizontal" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
            <div class="box-body">
                <section class="col-md-6">
                    <?php echo get_csrf_admin_token(); ?>
                    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                        <input class="form-control"  name ="seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->seq; ?>">
                    <?php } ?>
                    <div>
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#t_1" aria-controls="t_1" role="tab" data-toggle="tab">Info Login</a></li>
                            <li role="presentation"><a href="#t_2" aria-controls="t_2" role="tab" data-toggle="tab">Info Profile</a></li>
                            <li role="presentation"><a href="#t_3" aria-controls="t_3" role="tab" data-toggle="tab">Info Bank</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="t_1"><br />
                                <!--                                <div class="row">-->
                                <div class ="form-group">
                                    <label class ="control-label col-md-3">Email</label>
                                    <div class ="col-md-9">
                                        <input class="form-control" id="email" name="email" type="email" placeholder ="Input Email" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->email : "") ?>" required onblur="cEmail();" disabled>
                                    </div>
                                </div>
                            </div><!-- end t1 -->
                            <div role="tabpanel" class="tab-pane" id="t_2"><br />
                                <div class ="form-group">
                                    <label class ="control-label col-md-3">Name</label>
                                    <div class ="col-md-9">
                                        <input class="form-control" name="name" type="text" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->name : "") ?>"disabled>
                                    </div>
                                </div>
                                <div class ="form-group">
                                    <label class ="control-label col-md-3">Tanggal Lahir</label>
                                    <div class ="col-md-9">
                                        <input class="form-control" name="birthday" type="text" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->birthday : "") ?>"disabled>
                                    </div>
                                </div>
                                <div class ="form-group">
                                    <label class ="control-label col-md-3">Jenis Kelamin</label>
                                    <div class="col-md-3">
                                        <input type="radio" name="gender" value="M" checked onclick="chMd()" <?php echo (!isset($data_sel[LIST_DATA]) ? "" : (($data_sel[LIST_DATA][0]->gender == 'M') ? "checked" : "")); ?> disabled> <label>Laki - Laki</label>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="radio" name="gender" value="F" onclick="chMd()" <?php echo (!isset($data_sel[LIST_DATA]) ? "" : (($data_sel[LIST_DATA][0]->gender == 'F') ? "checked" : "")); ?> disabled> <label>Perempuan</label>
                                    </div>
                                </div>
                                <div class ="form-group">
                                    <label class ="control-label col-md-3">No Telepon</label>
                                    <div class ="col-md-9">
                                        <input class="form-control" name="phone_no" type="text" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->mobile_phone : "") ?>"disabled>
                                    </div>
                                </div>
                            </div><!-- end t2 -->
                            <div role="tabpanel" class="tab-pane" id="t_3"><br />
                                <div class ="form-group">
                                    <label class ="control-label col-md-3">Nama Bank</label>
                                    <div class ="col-md-9">
                                        <input class="form-control" id="email" name="bank_name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->bank_name : "") ?>"disabled>
                                    </div>
                                </div>
                                <div class ="form-group">
                                    <label class ="control-label col-md-3">Atas Nama</label>
                                    <div class ="col-md-9">
                                        <input class="form-control" id="email" name="bank_acct_name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->bank_acct_name : "") ?>"disabled>
                                    </div>
                                </div>
                                <div class ="form-group">
                                    <label class ="control-label col-md-3">No Rekening</label>
                                    <div class ="col-md-9">
                                        <input class="form-control" id="email" name="bank_acct_no" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->bank_acct_no : "") ?>"disabled>
                                    </div>
                                </div>
                                <div class ="form-group">
                                    <label class ="control-label col-md-3">Cabang</label>
                                    <div class ="col-md-9">
                                        <input class="form-control" id="email" name="bank_branch_name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->bank_branch_name : "") ?>" disabled>
                                    </div>
                                </div>
                            </div><!-- end t3 -->
                        </div>
                    </div>
                    </br>
                    <div class ="form-group">
                        <div class ="col-md-6"><?php echo get_back_button(); ?> 
                        </div>
                </section>
            </div>
        </form>
    </div>

<?php } else { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <div >
                <form id="frmAuth"  method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>" >
                    <div class = "box-tools pull-left">
                        <button class = "btn btn-flat bg-orange" id ="refresh" data-toggle = "tooltip" title = "refresh" type='button'><i class = "fa fa-refresh"></i></button>
                        <button data-toggle="control-sidebar" class = "btn btn-flat bg-navy" title="Filter" type='button'><i class="fa fa-search"></i></button>
                        <?php if (!$data_auth[FORM_AUTH][FORM_AUTH_ADD] === false) { ?>
                            <button name ="<?php echo CONTROL_ADD_NAME ?>" class = "btn btn-flat bg-green" type ="submit" id ="add"  onclick="button = this.id" value ="test" data-toggle = "tooltip" title = "Tambah"><i class = "fa fa-plus"> </i></button>
                        <?php } ?>
                        <?php if (!$data_auth[FORM_AUTH][FORM_AUTH_EDIT] === false) { ?>
                            <button  name ="<?php echo CONTROL_EDIT_NAME ?>" class = "btn btn-flat bg-blue" type ="submit" id ="edit" onclick="button = this.id"  value ="test" data-toggle = "tooltip" title = "Ubah" ><i class = "fa fa-pencil"></i></button>
                        <?php } ?>
                        <?php if (!$data_auth[FORM_AUTH][FORM_AUTH_VIEW] === false) { ?>
                            <button  name ="<?php echo CONTROL_VIEW_NAME ?>" class = "btn btn-flat bg-yellow" type ="submit" id ="view" onclick="button = this.id"  value ="test" data-toggle = "tooltip" title = "Lihat" ><i class = "fa fa-eye"></i></button>
                            <button  data-toggle = "tooltip" title = "Setuju Semua" class = "btn btn-flat bg-blue"  id="all_selected" type="button"><i class = "fa fa-check"></i></button>
                        <?php } ?>
                        <?php if (!$data_auth[FORM_AUTH][FORM_AUTH_DELETE] === false) { ?>
                            <button class = "btn btn-flat bg-red" id ="delete" data-toggle = "tooltip" title = "Hapus" type='button'><i class = "fa fa-trash-o"></i></button>
                        <?php } if (!$data_auth[FORM_AUTH][FORM_AUTH_APPROVE] === false) { ?>
                            <button class = "btn btn-flat bg-blue" id ="approve" data-toggle = "tooltip" title = "Setuju" type='button'><i class = "fa fa-check"></i></button>
                            <button class = "btn btn-flat bg-orange" id ="reject" data-toggle = "tooltip" title = "Tolak" type='button'><i class = "fa fa-thumbs-o-down"></i></button>
                        <?php } if (!$data_auth[FORM_AUTH][FORM_AUTH_PRINT] === false) { ?>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class = "fa fa-download"></i> <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><button class = "btn btn-flat btn-default" id="e_csv" style="width:100%" type='button'>CSV</button></li>
                                    <li><button class = "btn btn-flat btn-default" id="e_xls" style="width:100%" type='button'>Excel</button></li>
                                    <li><button class = "btn btn-flat btn-default" id="e_pdf" style="width:100%" type='button'>PDF</button></li>
                                </ul>
                            </div>
                        <?php } ?>
                    </div>
                </form>
                <br>
                <br>
                <div>
                    <form id = "frmAdd" method = "post" action = "<?php echo get_base_url() . $data_auth[FORM_URL] ?>" enctype = "multipart/form-data">
                        <div class="input-group col-md-5">
                            <span class="input-group-btn">
                                <span class="btn btn-primary btn-file">
                                    Pilih File
                                    <input class='form-control' id="file_upload" name='userfile' type="file" onchange="set_upload(this.value)"/>
                                </span>
                            </span>
                            <input class="form-control" readonly="" id="upload_url" type="text">
                            <span class="input-group-btn">
                                <button name ="<?php echo CONTROL_ADDITIONAL_NAME ?>" type = "submit" class = "btn btn-primary"><i class='fa fa-upload'></i> Unggah</button>
                            </span>
                        </div>
                    </form>
                </div>
                </br>
            </div>
            <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th column="email"> Email</th>
                        <th column="name"> Nama Member</th>
                        <th column="status"> Status </th>
                        <th column="gender"> Gender </th>
                        <th column="mobile_phone"> No Tlp </th>
                        <th column="checked" align="center" class="no-sort"> <input id="checkAll" type="checkbox"></th>
                        <th column="created_by"> <?php echo TH_CREATED_BY; ?> </th>
                        <th column="created_date"> <?php echo TH_CREATED_DATE; ?> </th>
                        <th column="modified_by"> <?php echo TH_MODIFIED_BY; ?> </th>
                        <th column="modified_date"> <?php echo TH_MODIFIED_DATE; ?> </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <script text ="javascript">
                                            $('#checkAll').change(function() {
                                                $("input:checkbox").prop('checked', $(this).prop("checked"));
                                            });

                                            var success_html = '<div class="alert alert-success-admin" style="margin-bottom: 0!important;><h4><i class="fa fa-info"></i></h4><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                                            var error_html = '<div class="alert alert-error-admin" style="margin-bottom: 0!important;> <h4><i class="fa fa-info"></i></h4><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';


                                            $("#all_selected").on('click', function() {
                                                var url = '<?php echo base_url() . "admin/master/member/save_approve" ?>';
                                                var checkbox_value = [];
                                                $("input:checkbox[name='printc[]']").each(function() {
                                                    var ischecked = $(this).is(":checked");
                                                    if (ischecked) {
                                                        checkbox_value.push($(this).val());
                                                    }
                                                });
                                                if (confirm("Apakah anda yakin menyetujui semua data yang dipilih ?")) {
                                                    $.ajax({
                                                        url: url,
                                                        type: "POST",
                                                        dataType: 'JSON',
                                                        data: {key: checkbox_value},
                                                        success: function(response) {
                                                            if (isSessionExpired(response)) {
                                                                response_object = json_decode(response);
                                                                url = response_object.url;
                                                                location.href = url;
                                                            } else {
                                                                if (checkbox_value == "") {
                                                                    $('#content-message').html(error_html + '<?php echo ERROR_VALIDATION_MUST_FILL ?>' + "</div>")
                                                                } else {
                                                                    if (response.error == true)
                                                                    {
                                                                        $('#content-message').html(error_html + response.message + "</div>")
                                                                    }
                                                                    else {
                                                                        $('#content-message').html(success_html + " Data telah berhasil disetujui ! </div>")
                                                                    }
                                                                }

                                                            }
                                                        }
                                                    });
                                                }
                                                table.ajax.reload();
                                            });


                                            function set_upload(url_upload) {
                                                $('#upload_url').val(url_upload);
                                            }
    </script>
    <?php
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>
