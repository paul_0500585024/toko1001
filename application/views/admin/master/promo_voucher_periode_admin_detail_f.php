<?php
require_once VIEW_BASE_ADMIN;
?>
<head>
    <link href="<?php echo get_css_url() ?>ionicons.min.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<?php echo get_css_url(); ?>ion.rangeSlider.css" rel="stylesheet">
    <link type="text/css" href="<?php echo get_css_url(); ?>ion.rangeSlider.skinNice.css" rel="stylesheet">
    <script type="text/javascript" src="<?php echo get_js_url(); ?>ion.rangeSlider.min.js" type="text/javascript"></script>
</head>
<form id ="frmSearch"  url= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
    <div class="form-group">
        <label class="control-label">Nominal</label>
        <input id="range_2" type="text" name="range_2" value="0;1000000" data-type="double" data-step="500" data-from="0" data-to="250000" data-hasgrid="true">
    </div>
    <div class="form-group">
        <?php echo get_search_button(); ?>
    </div>
    <script>
        $(function() {
            $("#range_2").ionRangeSlider();
        }); 
    </script>
</form>
