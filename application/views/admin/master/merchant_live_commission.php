<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
?>
<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo ($data_auth[FORM_ACTION] == ACTION_ADD ? get_title_add($data_auth[FORM_AUTH][FORM_TITLE]) : get_title_edit($data_auth[FORM_AUTH][FORM_TITLE])); ?></h3>
    </div>
    <form class="form-horizontal" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
	<?php echo get_csrf_admin_token(); ?>
        <input name ="seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->seq; ?>">
        <div class="box-body">
            <div class="row">
                <section class="col-md-12">
                    <div class="col-md-4">
                        <div class ="form-group">
                            <label class ="control-label col-md-3">Merchant</label>
                            <div class ="col-md-9">
                                <input class="form-control" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->name : ""); ?>" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class ="form-group">
                            <label class ="control-label col-md-3">Email</label>
                            <div class ="col-md-9">
                                <input class="form-control" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->email : ""); ?>" readonly>
                            </div>
                        </div>
                    </div>
		    <div class="col-md-4">
                        <div class ="form-group">
                            <label class ="control-label col-md-5">Riwayat Perubahan</label>
			    <div class="input-group input-group-sm">
				<select class="form-control" name="log_date" id="log_date">
				    <option value=''>-- Pilih --</option>
				    <?php
				    $dt_m = '';
				    if (isset($list_data)) {
					if (isset($key))
					    $dt_m = $key;
					foreach ($list_data as $each) {
					    ?>
					    <option value="<?php echo $each->modified_date; ?>"<?php if ($dt_m == $each->modified_date) echo " selected"; ?>><?php echo cdate($each->modified_date, 1); ?></option>
					    <?php
					}
				    }
				    ?>
				</select>
				<span class="input-group-btn">
				    <button type="button" class="btn btn-default btn-flat" onclick="viewlog()">Lihat</button>
				</span>
			    </div>
                        </div>
                    </div>
                </section>
            </div>
	    <hr />
            <div class="row" id="data_list">
                <div class="col-md-3"><label class ="control-label col-md-12">Kategori Level 1</label></div>
                <div class="col-md-3"><label class ="control-label col-md-12">Kategori Level 2</label></div>
                <div class="col-md-3"><label class ="control-label col-md-12">Master Komisi (%)</label></div>
                <div class="col-md-3"><label class ="control-label col-md-12">Komisi Merchant (%)</label></div>
            </div>
	    <?php
	    if ($dt_m == '') {
		if (isset($tree_menu)) {
		    if ($tree_menu != "") {
			$header = "";
			$i = 0;
			$ret = '';
			$show = "";
			foreach ($tree_menu as $value) {
			    if ($header != $value->hname) {
				if ($i > 1)
				    $ret.= '</div></div></div>';
				if ($show != "")
				    $ret = str_replace('<div  id="sm' . $show . '" style="display:none;">', '<div  id="sm' . $show . '">', $ret);
				;
				$show = "";
				$ret.= '<div class="row" style="border-top:1px solid #f0f0f0;padding:5px;"><div class="col-md-3">'
					. (($value->parent_seq != NULL) ? '<input type="hidden" name="hseq[]" value="' . $value->hseq . '" /><i class="fa fa-check-square-o"></i> ' : '<input type="checkbox" ' . (($value->mcatlvl1 != NULL) ? ' checked' : '') . ' name="hseq[]" value="' . $value->hseq . '" onclick="showhide(\'' . $value->hseq . '\')">') . ' ' . $value->hname . '</div>'
					. '<div  id="sm' . $value->hseq . '" style="display:none;">
			<div class="col-md-9">';
				$header = $value->hname;
			    }
			    if ($value->mcatlvl1 !== NULL)
				$show = $value->hseq;
			    $ret.= '<div class="row"' . (fmod($i, 2) == 0 ? '' : ' style="background-color:#eee"') . '">
				    <div class="col-md-4"><input type="checkbox" ' . (($value->mcatlvl2 !== NULL) ? ' checked' : '') . ' name="lvlcat[]" value="' . $value->seq . '~' . $i . '~' . $value->hseq . '" onclick="sht(this.checked,\'lvlcat_' . $value->seq . '\')"> ' . $value->name . '</div>
				    <div class="col-md-2" align="right">' . $value->trx_fee_percent . '</div>
				    <div class="col-md-2 col-md-offset-2"><input type="text" ' . (($value->mcatlvl2 !== NULL) ? '' : 'style="display : none ;"') . ' class="form-control auto_dec" data-d-group="3" data-v-max="100" name="mfee[]" id="lvlcat_' . $value->seq . '" value="' . $value->mfee . '"></div>
				</div>';

			    $i++;
			}
			$ret = str_replace('<div  id="sm' . $show . '" style="display:none;">', '<div  id="sm' . $show . '">', $ret);
			$ret.= '</div></div></div>';
			echo $ret;
		    }
		}
	    }else {

		if (isset($tree_menu)) {
		    if ($tree_menu != "") {
			$header = "";
			$i = 0;
			$ret = '';
			$show = "";
			foreach ($tree_menu as $value) {
			    if ($header != $value->hname) {
				if ($i > 1)
				    $ret.= '</div></div></div>';
				if ($show != "")
				    $ret = str_replace('<div  id="sm' . $show . '" style="display:none;">', '<div  id="sm' . $show . '">', $ret);
				;
				$show = "";
				$ret.= '<div class="row" style="border-top:1px solid #f0f0f0;padding:5px;"><div class="col-md-3">'
					. (($value->mcatlvl1 != NULL) ? ' <i class="fa fa-check-square-o"></i> ' . $value->hname : '<i class="fa fa-square-o"></i> ' . $value->hname) . '</div>'
					. '<div  id="sm' . $value->hseq . '" style="display:none;"><div class="col-md-9">';
				$header = $value->hname;
			    }
			    if ($value->mcatlvl1 !== NULL)
				$show = $value->hseq;
			    $ret.= '<div class="row"' . (fmod($i, 2) == 0 ? '' : ' style="background-color:#eee"') . '">
			<div class="col-md-4">' . (($value->mcatlvl2 !== NULL) ? '<i class="fa fa-check-square-o"></i> ' : '<i class="fa fa-square-o"></i> ') . $value->name . '</div>
			<div class="col-md-2" align="right">' . $value->trx_fee_percent . '</div>
			<div class="col-md-4" align="right">' . (($value->mcatlvl2 !== NULL) ? $value->mfee : '') . '</div>
		</div>';

			    $i++;
			}
			$ret = str_replace('<div  id="sm' . $show . '" style="display:none;">', '<div  id="sm' . $show . '">', $ret);
			$ret.= '</div></div></div>';
			echo $ret;
		    }
		}
	    }
	    ?>
	</div>
	<div class="box-footer no-border">
	    <div class="box-footer">
		<?php if ($dt_m == '') { ?>
    		<div class ="col-md-6"><?php echo (($data_sel[LIST_DATA][0]->status != 'R') ? get_save_edit_button() : ''); ?> </div>
    		<div class ="col-md-6"><a href="<?php echo get_base_url(); ?>admin/master/merchant_live" class="btn btn-google-plus" style="width: 100%;">Batal</a></div>
		    <?php
		} else {
		    echo '<div class ="col-md-6"></div><div class ="col-md-6"><a href="' . current_url() . '" class="btn btn-google-plus" style="width: 100%;">Lihat Data Sekarang</a></div>';
		}
		?>
	    </div>
	</div>
    </form>
</div>
<script type="text/javascript">
    <!--
    function showhide(idmenu) {
	$("#sm" + idmenu).toggle("slow");
    }
    function sht(nilai, idmenu) {
	if (nilai) {
	    $("#" + idmenu).show("slow");
	} else {
	    $("#" + idmenu).hide("slow");
	}
    }
    function viewlog() {
	$log_date = $("#log_date").val();
	window.location.href = '?log_date=' + $log_date
    }
    //-->
</script>
<?php
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>