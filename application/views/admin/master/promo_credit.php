<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
$tablesort = 'desc';
?>

<link rel="stylesheet" type="text/css" href="<?php echo get_css_url() ?>daterangepicker-bs4.css" />
<script type="text/javascript" src="<?php echo get_js_url() ?>moment.min.js"></script>
<script type="text/javascript" src="<?php echo get_js_url() ?>daterangepicker.js"></script>

<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h4 class="box-title"><?php echo $data_auth[FORM_ACTION_TITLE] . $data_auth[FORM_AUTH][FORM_TITLE] ?></h4>
        </div>
        <form class ="form-horizontal" id="frmMain" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
            <div class="box-body">
                <section class="col-md-6">
                    <?php echo get_csrf_admin_token(); ?>
                    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                        <input class="form-control" name ="seq" type="hidden" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->seq : ""); ?>">
                    <?php } ?>
                    <div class ="form-group">   
                        <label class="control-label col-md-4">Periode Cicilan </label>
                        <div class ="col-md-8">
                            <input class="form-control" date_type="date" id ="date_from" validate ="required[]" name ="date_from" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? date("d-M-Y", strtotime($data_sel[LIST_DATA][0]->promo_credit_period_from)) . ' - ' . date("d-M-Y", strtotime($data_sel[LIST_DATA][0]->promo_credit_period_to)) : "" ); ?>" readonly>
                        </div>
                    </div>
                    <div class ="form-group">     
                        <label class ="control-label col-md-4">Nama Promo Cicilan</label>
                        <div class ="col-md-8">
                            <input class="form-control" name="promo_credit_name" validate="required[]" type="text" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->promo_credit_name : "") ?>">
                        </div>
                    </div>
                    <div class ="form-group">     
                        <label class ="control-label col-md-4">Minimal Nominal</label>
                        <div class ="col-md-8">
                            <input class="form-control auto_int" name="minimum_nominal" validate="required[]" type="text" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->minimum_nominal : "") ?>">
                        </div>
                    </div>
                    <div class ="form-group">     
                        <label class ="control-label col-md-4">Maximal Nominal</label>
                        <div class ="col-md-8">
                            <input class="form-control auto_int" name="maximum_nominal" validate="required[]" type="text" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->maximum_nominal : "") ?>">
                        </div>
                    </div>
                    <div class ="form-group">
                        <?php
                        if ($data_auth[FORM_ACTION] == ACTION_VIEW) {
                            ?>
                            <div class ="col-md-6"><?php echo get_back_button(); ?> </div>
                            <?php
                        } else {
                            if ($data_auth[FORM_ACTION] == ACTION_ADD) {
                                ?>
                                <div class ="col-md-6"><?php echo get_save_add_button(); ?> </div>
                            <?php } if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                                <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                            <?php } ?>
                            <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                        <?php } ?>
                    </div>
                </section>
            </div>
        </form>
    </div>
    <script type="text/javascript">
            $('input[date_type="date"]').daterangepicker({
                format: 'DD-MMM-YYYY',
                showDropdowns: true
            });
    </script>
<?php } else { ?>    
    <div class="box box-default">
        <div class="box-header with-border">
            <h4 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h4>
        </div>
        <div class="box-body">
            <?php require_once get_include_page_list_admin_content_header(); ?>
            <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>       
                        <th column="promo_credit_period_from"> Periode Awal </th>
                        <th column="promo_credit_period_to"> Periode Akhir </th>
                        <th column="promo_credit_name">Nama Promo Cicilan</th>
                        <th column="minimum_nominal">Minimum Nominal</th>
                        <th column="maximum_nominal">Maximum Nominal</th>
                        <th column="status"> Status </th>
                        <th column="detail"> Detil </th>
                        <th column="credit"> Cicilan </th>
                        <th column="created_by"> <?php echo TH_CREATED_BY; ?> </th>
                        <th column="created_date"> <?php echo TH_CREATED_DATE; ?> </th>
                        <th column="modified_by"> <?php echo TH_MODIFIED_BY; ?> </th>
                        <th column="modified_date"> <?php echo TH_MODIFIED_DATE; ?> </th>
                    </tr>
                </thead>
            </table>  
        </div>
    </div>
    <?php
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>