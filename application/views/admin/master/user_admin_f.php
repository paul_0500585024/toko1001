<?php
require_once VIEW_BASE_ADMIN;
?>

<form id ="frmSearch"  url= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
    <div class="form-group">
        <label>Golongan User</label>
        <?php include dirname(__FILE__) . '/../component/dropdown/com_drop_user_group.php' ?>
    </div>
    <div class="form-group">
        <label>ID User</label>
        <input class="form-control" name="user_id" type="input">
    </div>
    <div class="form-group">
        <label>Nama User</label>
        <input class="form-control" name="user_name" type="input">
    </div>
    <div class="form-group">
        <input name ="active" type="checkbox" checked> Aktif</input>
    </div>
    <div class="form-group">
        <?php echo get_search_button(); ?>
    </div>
</form>