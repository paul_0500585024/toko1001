<?php
require_once VIEW_BASE_ADMIN;

require_once get_include_content_admin_top_page_navigation();
?>
<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_add($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <form class ="form-horizontal"id ="frmMain" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>" enctype="multipart/form-data">
            <div class="box-body">
                <section class ="col-md-12">
                    <?php echo get_csrf_admin_token(); ?>
                    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                        <input class="form-control"  name ="seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->seq; ?>">
                        <input class="form-control"  name ="old_category_seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->category_seq; ?>">
                        <input class="form-control"  name ="old_img_1" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->old_img_1; ?>">
                        <input class="form-control"  name ="old_img_2" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->old_img_2; ?>">
                        <input class="form-control"  name ="old_img_3" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->old_img_3; ?>">
                        <input class="form-control"  name ="old_img_4" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->old_img_4; ?>">
                        <input class="form-control"  name ="old_img_5" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->old_img_5; ?>">
                        <input class="form-control"  name ="old_img_6" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->old_img_6; ?>">
                        <input class="form-control"  name ="old_img_7" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->old_img_7; ?>">
                        <input class="form-control"  name ="old_img_banner" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->old_img_banner; ?>">
                    <?php } ?>
                    <div class="col-md-12">  
                        <div class="form-group">
                            <label class ="control-label col-md-3">Kategori *</label>
                            <div class ="col-md-9">
                                <?php require_once get_component_url() . "dropdown/com_drop_product_category_disable.php" ?>
                            </div>
                        </div>                        
                    </div>
                    <div class="col-md-4">  
                        <div class="panel panel-primary" style="height:420px">
                            <div class="panel-heading">Gambar Kategori 1(h:393px, w:477px)</div>
                            <div class="panel-body">
                                <div class ="col-md-12">    
                                    <div class ="form-group">  
                                        <input type="file" accept="<?php echo IMAGE_TYPE_UPLOAD; ?>" name="img1" class="btn btn-default" onchange ="previewimg(this, 'iprev1')" value="<?php echo isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->img_1 : ""; ?>" />
                                        <img id="iprev1" src="<?php echo (!isset($data_sel[LIST_DATA])) || $data_sel[LIST_DATA][0]->img_1 == '' ?  IMG_BLANK_100 : ADV_TMP_UPLOAD_IMAGE . $data_sel[LIST_DATA][0]->img_1; ?>" height="200" style="max-width:400px"/>                                     
                                    </div>  
                                    <div class="form-group">                                    
                                        <input class="form-control"  name ="img1_url" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->img_1_url) : "" ); ?>" placeholder ="URL Link ...">                                            
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">  
                        <div class="panel panel-default" style="height:420px">
                            <div class="panel-heading">Gambar Kategori 2(h:196px, w:237px)</div>
                            <div class="panel-body">
                                <div class ="col-md-12">   
                                    <div class ="form-group"> 
                                        <input type="file" accept="<?php echo IMAGE_TYPE_UPLOAD; ?>" name="img2" class="btn btn-default" onchange ="previewimg(this, 'iprev2')" value="<?php echo isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->img_2 : ""; ?>" />
                                        <img id="iprev2" src="<?php echo (!isset($data_sel[LIST_DATA])) || $data_sel[LIST_DATA][0]->img_2 == '' ? IMG_BLANK_100 : ADV_TMP_UPLOAD_IMAGE . $data_sel[LIST_DATA][0]->img_2; ?>" height="200" style="max-width:400px" />
                                    </div>
                                    <div class="form-group">                                    
                                        <input class="form-control col-md-10" name ="img2_url" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->img_2_url) : "" ); ?>" placeholder ="URL Link ...">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-default" style="height:420px">
                            <div class="panel-heading">Gambar Kategori 3(h:196px, w:237px)</div>
                            <div class="panel-body">
                                <div class ="col-md-12">  
                                    <div class ="form-group">   
                                        <input type="file" accept="<?php echo IMAGE_TYPE_UPLOAD; ?>" name="img3" class="btn btn-default" onchange ="previewimg(this, 'iprev3')" value="<?php echo isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->img_3 : ""; ?>" />
                                        <img id="iprev3" src="<?php echo (!isset($data_sel[LIST_DATA])) || $data_sel[LIST_DATA][0]->img_3 == '' ? IMG_BLANK_100 :  ADV_TMP_UPLOAD_IMAGE . $data_sel[LIST_DATA][0]->img_3; ?>" height="200" style="max-width:400px" />                                  
                                    </div>
                                    <div class="form-group">                                    
                                        <input class="form-control col-md-10" name ="img3_url" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->img_3_url) : "" ); ?>" placeholder ="URL Link ...">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-default" style="height:420px">
                            <div class="panel-heading">Gambar Kategori 4(h:196px, w:237px)</div>
                            <div class="panel-body">
                                <div class ="col-md-12">  
                                    <div class ="form-group">                          
                                        <input type="file" accept="<?php echo IMAGE_TYPE_UPLOAD; ?>" name="img4" class="btn btn-default" onchange ="previewimg(this, 'iprev4')" value="<?php echo isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->img_4 : ""; ?>" />
                                        <img id="iprev4" src="<?php echo (!isset($data_sel[LIST_DATA])) || $data_sel[LIST_DATA][0]->img_4 == '' ?  IMG_BLANK_100 :  ADV_TMP_UPLOAD_IMAGE . $data_sel[LIST_DATA][0]->img_4; ?>" height="200" style="max-width:400px" />
                                    </div>
                                    <div class="form-group">                                    
                                        <input class="form-control col-md-10" name ="img4_url" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->img_4_url) : "" ); ?>" placeholder ="URL Link ...">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-default" style="height:420px">
                            <div class="panel-heading">Gambar Kategori 5(h:196px, w:237px)</div>
                            <div class="panel-body">
                                <div class ="col-md-12">  
                                    <div class ="form-group"> 
                                        <input type="file" accept="<?php echo IMAGE_TYPE_UPLOAD; ?>" name="img5" class="btn btn-default" onchange ="previewimg(this, 'iprev5')" value="<?php echo isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->img_5 : ""; ?>" />
                                        <img id="iprev5" src="<?php echo (!isset($data_sel[LIST_DATA])) || $data_sel[LIST_DATA][0]->img_5 == '' ? IMG_BLANK_100 : ADV_TMP_UPLOAD_IMAGE . $data_sel[LIST_DATA][0]->img_5; ?>" height="200" style="max-width:400px" />
                                    </div>
                                    <div class="form-group">                                    
                                        <input class="form-control col-md-10" name ="img5_url" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->img_5_url) : "" ); ?>" placeholder ="URL Link ...">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-default" style="height:420px">
                            <div class="panel-heading">Gambar Kategori 6(h:196px, w:237px)</div>
                            <div class="panel-body">
                                <div class ="col-md-12">  
                                    <div class ="form-group"> 
                                        <input type="file" accept="<?php echo IMAGE_TYPE_UPLOAD; ?>" name="img6" class="btn btn-default" onchange ="previewimg(this, 'iprev6')" value="<?php echo isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->img_6 : ""; ?>" />
                                        <img id="iprev6" src="<?php echo (!isset($data_sel[LIST_DATA])) || $data_sel[LIST_DATA][0]->img_6 == '' ? IMG_BLANK_100 :  ADV_TMP_UPLOAD_IMAGE . $data_sel[LIST_DATA][0]->img_6; ?>" height="200" style="max-width:400px"/>
                                    </div>
                                    <div class="form-group">                                    
                                        <input class="form-control col-md-10" name ="img6_url" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->img_6_url) : "" ); ?>" placeholder ="URL Link ...">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-default" style="height:420px">
                            <div class="panel-heading">Gambar Kategori 7(h:196px, w:237px)</div>
                            <div class="panel-body">
                                <div class ="col-md-12">  
                                    <div class ="form-group"> 
                                        <input type="file" accept="<?php echo IMAGE_TYPE_UPLOAD; ?>" name="img7" class="btn btn-default" onchange ="previewimg(this, 'iprev7')" value="<?php echo isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->img_7 : ""; ?>" />
                                        <img id="iprev7" src="<?php echo (!isset($data_sel[LIST_DATA])) || $data_sel[LIST_DATA][0]->img_7 == '' ? IMG_BLANK_100 :  ADV_TMP_UPLOAD_IMAGE . $data_sel[LIST_DATA][0]->img_7; ?>" height="200" style="max-width:400px"/>
                                    </div>
                                    <div class="form-group">                                    
                                        <input class="form-control col-md-10" name ="img7_url" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->img_7_url) : "" ); ?>" placeholder ="URL Link ...">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="panel panel-warning" style="height:420px">
                            <div class="panel-heading">Gambar Banner(h:1140px, w:284px)</div>                             
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class ="form-group">       
                                        <input type="file" accept="<?php echo IMAGE_TYPE_UPLOAD; ?>" name="banner" class="btn btn-default" onchange ="previewimg(this, 'iprev8')" value="<?php echo isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->img_banner : ""; ?>" />
                                        <img id="iprev8" src="<?php echo (!isset($data_sel[LIST_DATA])) || $data_sel[LIST_DATA][0]->img_banner == '' ?  IMG_BLANK_100 : ADV_TMP_UPLOAD_IMAGE . $data_sel[LIST_DATA][0]->img_banner; ?>" height="200" style="max-width:400px"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6" nopadding>
                                        <input class="form-control" name ="banner_url" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->img_banner_url) : "" ); ?>" placeholder ="URL Link ...">
                                    </div>
                                </div>    
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class ="form-group">
                            <?php
                            if ($data_auth[FORM_ACTION] == ACTION_VIEW) {
                                ?>
                                <div class ="col-md-6"><?php echo get_back_button(); ?> </div>
                                <?php
                            } else {
                                if ($data_auth[FORM_ACTION] == ACTION_ADD) {
                                    ?>
                                    <div class ="col-md-6"><?php echo get_save_add_button(); ?> </div>
                                <?php } if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                                    <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                                <?php } ?>
                                <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                            <?php } ?>
                        </div>
                    </div>
                </section>
            </div>
        </form>
    </div>
    <script type="text/javascript">
            function previewimg(thisval, idprev) {
                if (thisval.files && thisval.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#' + idprev + '').attr('src', e.target.result).height(200);
                    };
                    reader.readAsDataURL(thisval.files[0]);
                }
            }
    </script>
<?php } else {
    ?>    
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <?php require_once get_include_page_list_admin_content_header(); ?>
            <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>                                         
                        <th column="category_name"> Category Name </th>
                        <th column="seq"> Pengajuan #</th>
                        <th column="status"> Status </th>
                        <th column="active"> Aktif </th>
                        <th column="auth_by"> Disetujui / Ditolak Oleh </th>
                        <th column="auth_date"> Tgl Disetujui / Ditolak </th>
                        <th column="created_by"> <?php echo TH_CREATED_BY; ?> </th>
                        <th column="created_date"> <?php echo TH_CREATED_DATE; ?> </th>
                        <th column="modified_by"> <?php echo TH_MODIFIED_BY; ?> </th>
                        <th column="modified_date"> <?php echo TH_MODIFIED_DATE; ?> </th>
                    </tr>
                </thead>
            </table>    
        </div>
    </div>
    <?php
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>