<?php
require_once VIEW_BASE_ADMIN;
?>

<form id ="frmSearch"  url= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
    <div class="form-group">
        <label>Merchant</label>
        <?php require_once get_component_url() . "dropdown/com_drop_merchant.php" ?>
    </div>
    <div class ="form-group">     
        <label>No. Resi</label>
        <input class="form-control" name ="awb_no" type="text"/>
    </div>
    <div class ="form-group">     
        <label>No. Pesanan</label>
        <input class="form-control" name ="trx_no" type="text"/>
    </div>
    <div class="form-group">
        <input name ="active" type="checkbox" checked/>
        <label> Aktif</label>
    </div>
    <div class="form-group">
        <?php echo get_search_button(); ?>
    </div>
</form>