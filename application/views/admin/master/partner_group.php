<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
?>


<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>

    <div class="box box-default">     
        <div class="box-header">
            <h3 class="box-title"><?php echo get_title_edit($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <form class ="form-horizontal" id="frmMain" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
                <?php echo get_csrf_admin_token(); ?>
                <input type="hidden" name="partner_group_seq" value="<?php echo isset($data_sel[LIST_DATA][0]->partner_group_seq) ? get_display_value($data_sel[LIST_DATA][0]->partner_group_seq):''; ?>"/>
                <section class="col-md-8">
                    <div class ="form-group">     
                        <label class ="control-label col-md-3">Nama Grup</label>
                        <div class ="col-md-8">
                            <input class="form-control" validate="required[]" name ="partner_group_name" type="text" value ="<?php echo isset($data_sel[LIST_DATA][0]->partner_group_name) ? get_display_value($data_sel[LIST_DATA][0]->partner_group_name):''; ?>"/>
                        </div>
                    </div>
                    </br>
                    <div class ="form-group">
                        <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                            <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                            <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                        <?php } ?>
                        <?php if ($data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
                            <div class ="col-md-6"><?php echo get_back_button(); ?></div> 
                        <?php } ?>
                        <?php if ($data_auth[FORM_ACTION] == ACTION_ADD) { ?>
                            <div class ="col-md-6"><?php echo get_save_add_button(); ?></div> 
                            <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                        <?php } ?>
                    </div>
                </section>
            </form>
        </div>
    </div>
<?php } else { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <?php require_once get_include_page_list_admin_content_header(); ?>
            <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th column="partner_group_name"> Nama Grup</th>
                        <th column="detail">Detil</th>
                        <th column="created_by"> <?php echo TH_CREATED_BY; ?> </th>
                        <th column="created_date"> <?php echo TH_CREATED_DATE; ?> </th>
                        <th column="modified_by"> <?php echo TH_MODIFIED_BY; ?> </th>
                        <th column="modified_date"> <?php echo TH_MODIFIED_DATE; ?> </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <?php
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>


