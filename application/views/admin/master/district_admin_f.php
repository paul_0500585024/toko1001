<?php
require_once VIEW_BASE_ADMIN;
?>

<form id ="frmSearch"  url= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
    <div class="form-group">
        <label>Propinsi</label>
        <?php require_once get_component_url() . "dropdown/com_drop_province.php" ?>
    </div>
    <div class="form-group">
        <label>Kota / Kabupaten</label>
        <?php require_once get_component_url() . "dropdown/com_drop_city.php" ?>
    </div>
    <div class="form-group">
        <label>Nama Kecamatan</label>
        <input class="form-control" name="district_name" type="input">
    </div>
    <div class="form-group">
        <input name ="active" type="checkbox" checked > Aktif</input>
    </div>
    <div class="form-group">
        <?php echo get_search_button(); ?>
    </div>
</form>
<script text="javascript">
    $("#drop_province").change(function() {
        var city_val = $("#drop_city").val();
        if ($(this).val() != "")
        {
            $.ajax({
                url: "<?php echo get_base_url() . $data_auth[FORM_URL] ?>",
                dataType: "json",
                data: {
                    btnAdditional: true,
                    province_seq: $(this).val()
                },
                type: "POST",
                success: function(data) {
                    if(isSessionExpired(data)){
                        response_object = json_decode(data);
                        url = response_object.url;
                        location.href = url;
                    }else{
                        $('#drop_city option').remove();
                        $("#drop_city").append("<option value=\"" + "\">-- Pilih --</option>")
                        $(data).each(function(i) {
                            $("#drop_city").append("<option value=\"" + data[i].seq + "\">" + data[i].name + "</option>")
                        });
                        $("#drop_city").val(city_val);
                        $('#drop_city').select2();
                    }
                },
                error: function (request, error) {
                    alert(error_response(request.status));
                }
            });
        }
        else
        {
            $('#drop_city option').remove();
            $('#drop_city').select2()
        }
    }).change();
</script>