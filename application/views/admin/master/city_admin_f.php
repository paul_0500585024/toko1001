<?php
require_once VIEW_BASE_ADMIN;
?>
<form id="frmSearch" url="<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
    <div class="form-group">
        <label>Propinsi</label>
        <div>
            <?php include dirname(__FILE__) . '/../component/dropdown/com_drop_province.php' ?>
        </div>
    </div>
    <div class="form-group">
        <label>Nama Kota / Kabupaten</label>
        <input class="form-control" name="city_name" type="input">
    </div>
    <div class="form-group">
        <input name ="active" type="checkbox" checked> Aktif </input>
    </div>
    <div class="form-group">
        <?php echo get_search_button(); ?>
    </div>
</form>