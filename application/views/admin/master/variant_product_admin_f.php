<?php
require_once VIEW_BASE_ADMIN;
?>
<form id="frmSearch" url="<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
    <div class="form-group">
        <label>Kategori</label>
        <div>
            <?php require_once get_component_url() . "dropdown/com_drop_product_category.php"; ?>
        </div>
    </div>
    <div class="form-group">
        <label>Nama Varian</label>
        <input class="form-control" name="variant_name" type="input">
    </div>
    <div class="form-group">
        <label>Tampilan Varian</label>
        <input class="form-control" name="display_name" type="input">
    </div>
    <div class="form-group">
        <input name ="active" type="checkbox" checked> Aktif</input>
    </div>
    <div class="form-group">
        <?php echo get_search_button(); ?>
    </div>
</form>