<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
?>


    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <?php require_once get_include_page_list_admin_content_header(); ?>          
            <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th column="pg_name"> Batal Bayar Via</th>
                        <th column="created_date"> Tgl Batal</th>                        
                        <th column="agent_name"> Member </th>
                        <th column="trx_no"> No. Transaksi </th>                        
                        <th column="trx_date"> Tgl Order </th>
                        <th column="refund_date"> Tgl Refund </th>
                        <th column="trx_type"> Tipe </th>
                        <th column="deposit_trx_amt"> Nilai Batal (Rp) </th>
                        <th column="status"> Status </th>
                        <th column="detail"> Detail Produk</th>
                        
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <?php

require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
 ?>