<?php
require_once VIEW_BASE_ADMIN;
?>
<form id ="frmSearch"  url= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
    <div class="form-group">
        <label>Nama Merchant</label>
        <?php include dirname(__FILE__) . '/../component/dropdown/com_drop_merchant.php' ?>
    </div>

    <div class="form-group">
        <label>Nama Product</label>
        <input class="form-control" name="name" type="input">
    </div>
    <div class="form-group">
        <label>Status</label>
        <select class="form-control select2" name="status">
            <option value="">Semua</option>
            <?php
            echo combostatus(json_decode(STATUS_ANR),"N");
            ?>
        </select>
    </div>
    <div class="form-group">
        <?php echo get_search_button(); ?>
    </div>
</form>
