<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
$tablesort = 'desc';
?>
<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_css_url() ?>daterangepicker-bs3.css" />
    <script type="text/javascript" src="<?php echo get_js_url() ?>moment.min.js"></script>
    <script type="text/javascript" src="<?php echo get_js_url() ?>daterangepicker.js"></script>
    <script type="text/javascript" src="<?php echo get_js_url() ?>ckeditor/ckeditor.js"></script>
    <div class="box box-default">
        <div class="box-header with-border">
    	<h3 class="box-title"><?php echo $data_auth[FORM_ACTION_TITLE] . $data_auth[FORM_AUTH][FORM_TITLE] ?></h3>
        </div>
        <form class ="form-horizontal" id="frmMain" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
    	<div class="box-body">
    	    <section class="col-md-12">
		    <?php echo get_csrf_admin_token(); ?>
		    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
			<input class="form-control"  name ="seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->seq; ?>">
		    <?php } ?>
    		<div class ="form-group">
    		    <label class ="control-label col-md-3">Nama Promo</label>
    		    <div class ="col-md-9">
    			<input class="form-control" id="promo_name" name ="promo_name" type="text" placeholder ="Nama Promo" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->promo_name) : ''); ?>" >
    		    </div>
    		</div>
    		<div class ="form-group">
    		    <label class ="control-label col-md-3">Periode Awal</label>
    		    <div class ="col-md-2">
    			<input readonly class="form-control" id="promo_img_from_date" date_type="date" style="background-color: #fff;" name ="promo_img_from_date" type="text" placeholder ="Periode Awal" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? date("d-M-Y", strtotime($data_sel[LIST_DATA][0]->promo_img_from_date)) : '' ); ?>" >
    		    </div>
    		</div>
    		<div class ="form-group">
    		    <label class ="control-label col-md-3">Periode Akhir</label>
    		    <div class ="col-md-2">
    			<input readonly class="form-control" id ="promo_img_to_date" date_type="date" style="background-color: #fff;" name ="promo_img_to_date" type="text" placeholder ="Periode Akhir" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? date("d-M-Y", strtotime($data_sel[LIST_DATA][0]->promo_img_to_date)) : "" ); ?>">
    		    </div>
    		</div>
    		<div class ="form-group">
    		    <div class ="col-md-12"><b>Isi Promo</b><br />
    			<textarea class="form-control" rows="3" id="promo_img_text" name="promo_img_text"><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->promo_img_text : "") ?></textarea>
    		    </div>
    		</div>
    		<div class ="form-group">
			<?php
			if ($data_auth[FORM_ACTION] == ACTION_VIEW) {
			    ?>
			    <div class ="col-md-6"><?php echo get_back_button(); ?> </div>
			    <?php
			} else {
			    if ($data_auth[FORM_ACTION] == ACTION_ADD) {
				?>
	    		    <div class ="col-md-6"><?php echo get_save_add_button(); ?> </div>
			    <?php } else { ?>
	    		    <input name ="status" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->status; ?>">
	    		    <div class ="col-md-6"><?php IF ($data_sel[LIST_DATA][0]->status == 'N') echo get_save_edit_button(); ?> </div>
			    <?php } ?>
			    <div class ="col-md-6"><?php echo get_cancel_button(); ?></div>
			<?php } ?>
    		</div>
    	    </section>
    	</div>
        </form>
    </div>
    <script type="text/javascript">
        $('input[date_type="date"]').daterangepicker({
    	format: 'DD-MMM-YYYY',
    	singleDatePicker: true,
    	showDropdowns: true
        });
        CKEDITOR.replace('promo_img_text');
    </script>
<?php } else { ?>
    <div class="box box-default">
        <div class="box-header with-border">
    	<h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
	    <?php require_once get_include_page_list_admin_content_header(); ?>
    	<table id="tbl" width="100%" class="display table table-bordered table-striped" url="<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
    	    <thead>
    		<tr>
    		    <th column="promo_img_from_date"> Periode</th>
    		    <th column="promo_name"> Promo</th>
    		    <th column="status"> Status </th>
    		    <th column="created_by"> <?php echo TH_CREATED_BY; ?> </th>
    		    <th column="created_date"> <?php echo TH_CREATED_DATE; ?> </th>
    		    <th column="modified_by"> <?php echo TH_MODIFIED_BY; ?> </th>
    		    <th column="modified_date"> <?php echo TH_MODIFIED_DATE; ?> </th>
    		</tr>
    	    </thead>
    	</table>
        </div>
    </div>

    <?php
}
require_once get_include_page_list_admin_content_footer();
if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) {

} else {
    ?>
    <script type="text/javascript">
        <!--
        var url = "<?php echo get_base_url() . $data_auth[FORM_URL]; ?>";
        function proses_redeem(seq, id) {
    	if (confirm('Proses periode redem ini?')) {
    	    $.ajax({
    		url: url,
    		data: {btnAdditional: "act_s_adt", idh: seq, tipe: "proses_redeem"
    		},
    		type: "POST",
    		success: function (response) {
    		    response = response.replace(/(?:\r\n|\r|\n)/g, '').trim();
    		    if (response.trim() == 'OK') {
    			table.ajax.reload();
    		    } else {
    			alert(response);
    		    }
    		},
    		error: function (request, error) {
    		    alert(error_response(request.status));
    		}
    	    });
    	}

        }
        //-->
    </script>
    <?php
}
require_once get_include_content_admin_bottom_page_navigation();
?>