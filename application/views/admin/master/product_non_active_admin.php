<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
?>
<?php if ($data_auth[FORM_ACTION] == ACTION_VIEW) { ?>

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo $data_auth[FORM_ACTION_TITLE] . $data_auth[FORM_AUTH][FORM_TITLE] ?></h3>
        </div>
        <form class="form-horizontal" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>" enctype="multipart/form-data" name="frmMain" id="frmMain">
            <?php echo get_csrf_admin_token(); ?>
            <div class="box-body">
                <input type="hidden" value="<?php echo $data_sel[LIST_DATA][0]->product_variant_seq; ?>" name="p_variant_seq">
                <section class="col-md-6 col-lg-6">
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-md-4">Nama Merchant </label>
                            <div class="col-md-8"><input type="text" class="form-control" value="<?php echo $data_sel[LIST_DATA][0]->merchant_name; ?>" readonly></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Nama Produk </label>
                            <div class="col-md-8"><input type="text" class="form-control" value="<?php echo $data_sel[LIST_DATA][0]->product_name; ?>" readonly></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Varian Produk</label>
                            <div class="col-md-8"><input type="text" class="form-control" value="<?php echo $data_sel[LIST_DATA][0]->variant_name; ?>" readonly></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Status Produk</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" value="<?php
                                if ($data_sel[LIST_DATA][0]->status_product == "L") {
                                    echo "Tayang";
                                } elseif ($data_sel[LIST_DATA][0]->status_product == "C") {
                                    echo "Perubahan";
                                };
                                ?>" readonly> </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Stok Produk</label>
                            <div class="col-md-8"><input type="text" class="form-control" value="<?php echo $data_sel[LIST_DATA][0]->stock_product; ?>" readonly></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Produk Aktif</label>
                            <div class="col-md-8">                                
                                <label class="checkbox-inline">
                                    <input name="active_btn" type="checkbox" data-toggle="toggle" <?php
                                    if ($data_sel[LIST_DATA][0]->product_active == '1') {
                                        echo 'checked';
                                    }
                                    ?> >
                                </label>
                            </div>
                        </div>
                </section>
            </div>

            <div class="box-footer">                
                <div class="col-md-4"><?php echo get_back_button(); ?> </div>
            </div>
        </form>

    </div>



<?php } else {
    ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <?php require_once get_include_page_list_admin_content_header(); ?>
            <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th column="merchant_name"> Nama Merchant</th>                    
                        <th column="product_name"> Nama Produk</th>                    
                        <th column="variant_name"> Varian </th>                    
                        <th column="status_product"> Status </th>    		    
                        <th column="product_active"> Aktif </th>    		    
                        <th column="product_stock"> Stok </th>    		    
                        <th column="tgl_buat"> <?php echo TH_CREATED_DATE; ?> </th>
                        <th column="tgl_ubah"> <?php echo TH_MODIFIED_DATE; ?> </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <?php
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>