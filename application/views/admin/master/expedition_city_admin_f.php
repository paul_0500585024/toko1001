<?php
require_once VIEW_BASE_ADMIN;
?>

<form id ="frmSearch"  url= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
    <div class="form-group">
        <label>Ekspedisi</label>
        <?php require_once get_component_url() . "dropdown/com_drop_expedition.php" ?>
    </div>
    <div class="form-group">
        <label>Propinsi</label>
        <?php require_once get_component_url() . "dropdown/com_drop_province.php"; ?>
    </div>
    <div class="form-group">
        <label>Kota / Kabupaten</label>
        <?php require_once get_component_url() . "dropdown/com_drop_city.php"; ?>
    </div>
    <div class="form-group">
        <label>Kecamatan</label>
        <?php require_once get_component_url() . "dropdown/com_drop_district.php"; ?>
    </div>
    <div class="form-group">
        <label>Kecamatan Ekspedisi</label>
        <input class="form-control" name="exp_district_code" type="input">
    </div>
    <div class="form-group">
        <input name ="active" type="checkbox" checked> Aktif</input>
    </div>
    <div class="form-group">
        <?php echo get_search_button(); ?>
    </div>
</form>

<script text="javascript">

    $("#drop_province").change(function() {
        var province_seq = $('#drop_province').val();
        $.ajax({
            url: "<?php echo get_base_url() . $data_auth[FORM_URL] ?>",
            type: "POST",
            dataType: "json",
            data: {
                "type": "<?php echo TASK_PROVINCE_CHANGE ?>",
                "province_seq": province_seq
            },
            success: function(data) {
                if(isSessionExpired(data)){
                    response_object = json_decode(data);
                    url = response_object.url;
                    location.href = url;
                }else{
                    $('#drop_city option').remove();
                    $("#drop_city").append("<option value=\"" + "\">-- Pilih --</option>");
                    $('#drop_district option').remove();
                    $("#drop_district").append("<option value=\"" + "\">-- Pilih --</option>");
                    $('#drop_city').select2();
                    $('#drop_district').select2();
                    $(data).each(function(i) {
                        $("#drop_city").append("<option value=\"" + data[i].seq + "\">" + data[i].name + "</option>");
                    });
                }
            },
            error: function (request, error) {
                alert(error_response(request.status));
            }
        });
    });

    $("#drop_city").change(function() {
        var city_seq = $('#drop_city').val();
        $.ajax({
            url: "<?php echo get_base_url() . $data_auth[FORM_URL] ?>",
            type: "POST",
            dataType: "json",
            data: {
                "type": "<?php echo TASK_CITY_CHANGE ?>",
                "city_seq": city_seq
            },
            success: function(data) {
                if(isSessionExpired(data)){
                    response_object = json_decode(data);
                    url = response_object.url;
                    location.href = url;
                }else{
                    $('#drop_district option').remove();
                    $("#drop_district").append("<option value=\"" + "\">-- Pilih --</option>");
                    $('#drop_district').select2();
                    $(data).each(function(i) {
                        $("#drop_district").append("<option value=\"" + data[i].seq + "\">" + data[i].name + "</option>");
                    });
                }
            },
            error: function (request, error) {
                alert(error_response(request.status));
            }                    
        });
    });

</script>