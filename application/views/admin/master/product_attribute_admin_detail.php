<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
?>

<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo $data_auth[FORM_ACTION_TITLE] . $data_auth[FORM_AUTH][FORM_TITLE] ?></h3>
        </div>
        <form class ="form-horizontal" id="frmMain" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
            <div class="box-body">
                <section class="col-md-6">
                    <?php echo get_csrf_admin_token(); ?>
                    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                        <input name ="seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->seq; ?>">
                    <?php } ?>
                    <div class ="form-group">
                        <label class ="control-label col-md-3">Nilai Atribut *</label>
                        <div class ="col-md-9">
                            <input class="form-control" validate ="required[]" name ="a_value" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->value) : "") ?>">
                        </div>
                    </div>

                    <div class ="form-group">
                        <label class ="control-label col-md-3">Urutan *</label>
                        <div class ="col-md-9">
                            <input class="form-control auto_int" data-a-pad="false" validate ="required[]" name ="a_order" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->order) : 0) ?>">
                        </div>
                    </div>
                    <div class ="form-group">
                        <div class ="col-md-offset-3 col-md-9">
                            <label> <input type="checkbox" name ="active" <?php echo (!isset($data_sel[LIST_DATA]) ? "checked" : (($data_sel[LIST_DATA][0]->active == "1" OR $data_sel[LIST_DATA][0]->active == "on") ? "checked" : "")); ?> /> Aktif</label>
                        </div>
                    </div>
                    <div class ="form-group">
                        <?php
                        if ($data_auth[FORM_ACTION] == ACTION_VIEW) {
                            ?>
                            <div class ="col-md-6"><?php echo get_back_button(); ?> </div>
                            <?php
                        } else {
                            if ($data_auth[FORM_ACTION] == ACTION_ADD) {
                                ?>
                                <div class ="col-md-6"><?php echo get_save_add_button(); ?> </div>
                            <?php } if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                                <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                            <?php } ?>
                            <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                        <?php } ?>
                    </div>
                </section>
            </div>
        </form>
    </div>

    <?php
} else {
    if (isset($data_sel[LIST_DATA])) {
        ?>
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
            </div>
            <div class="box-body">
                <form class ="form-horizontal col-md-12">
                    <div class ="form-group">
                        <label class ="control-label col-md-2">Nama Atribut</label>
                        <div class ="col-md-3">
                            <input class="form-control" value="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->name) : ""); ?>" readonly>
                        </div>
                    </div>
                    <div class ="form-group">
                        <label class ="control-label col-md-2">Tampilan Atribut</label>
                        <div class ="col-md-3">
                            <input class ="form-control" value="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->display_name) : ""); ?>" readonly>
                        </div>
                    </div>
                    <div class ="form-group">
                        <div class ="col-md-offset-2 col-md-3">
                            <label> <input type="checkbox" name ="active" <?php echo (isset($data_sel[LIST_DATA]) ? ($data_sel[LIST_DATA][0]->active == "1" ? "checked" : "") : ''); ?> disabled/> Aktif</label>
                        </div>
                    </div>
                </form>
                <div class="box-tools pull-left">
                    <?php require_once get_include_page_list_admin_content_header(); ?>
                </div>
                <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th column="value">Nilai Atribut</th>
                            <th column="order_search">Urutan</th>
                            <th column="active"> Aktif </th>
                            <th column="created_by"> <?php echo TH_CREATED_BY; ?> </th>
                            <th column="created_date"> <?php echo TH_CREATED_DATE; ?> </th>
                            <th column="modified_by"> <?php echo TH_MODIFIED_BY; ?> </th>
                            <th column="modified_date"> <?php echo TH_MODIFIED_DATE; ?> </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>

        <?php
    }
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>