<?php
require_once VIEW_BASE_ADMIN;

require_once get_include_content_admin_top_page_navigation();
?>

<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo ($data_auth[FORM_ACTION] == ACTION_ADD ? get_title_add($data_auth[FORM_AUTH][FORM_TITLE]) : get_title_edit($data_auth[FORM_AUTH][FORM_TITLE])); ?></h3>
        </div>
        <form id="frmMain" class="form-horizontal" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
            <div class="box-body">
                <section class="col-md-6">
                    <?php echo get_csrf_admin_token(); ?>
                    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                        <input name ="seq" type="hidden" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->coupon_seq : ""); ?>"></input>
                    <?php } ?>

                    <div class ="form-group">
                        <?php if ($data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
                            <div class ="col-md-6"><?php echo get_back_button(); ?> </div>
                            <?php
                        } else {
                            if ($data_auth[FORM_ACTION] == ACTION_ADD) {
                                ?>
                                <div class ="col-md-6"><?php echo get_save_add_button(); ?> </div>
                            <?php } if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                                <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                            <?php } ?>
                            <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                        <?php } ?>
                    </div>
                </section>
            </div><!-- /.box-footer -->
        </form>
    </div>
<?php } else { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <form class ="form-horizontal col-md-12">
                <div class ="form-group">
                    <label class ="control-label col-md-2">Periode</label>
                    <div class ="col-md-3">
                        <input class="form-control" date_type="date" id ="date_from" name ="date_from" type="text" value ="<?php echo (isset($data_head[LIST_DATA][0]) ? date("d-M-Y", strtotime($data_head[LIST_DATA][0]->coupon_period_from)) . ' - ' . date("d-M-Y", strtotime($data_head[LIST_DATA][0]->coupon_period_to)) : "" ); ?>" readonly>
                    </div>
                </div>
                <div class ="form-group">
                    <label class ="control-label col-md-2">Kode Kupon</label>
                    <div class ="col-md-3">
                        <input class="form-control" value="<?php echo (isset($data_head[LIST_DATA]) ? $data_head[LIST_DATA][0]->coupon_code : ""); ?>" readonly>
                    </div>
                </div>
                <div class ="form-group">
                    <label class ="control-label col-md-2">Batas Pemakaian</label>
                    <div class ="col-md-3">
                        <input class="form-control" value="<?php echo (isset($data_head[LIST_DATA]) ? $data_head[LIST_DATA][0]->coupon_limit : ""); ?>" readonly>
                    </div>
                </div>
            </form>
            <div class="box-tools pull-left">
                <?php require_once get_include_page_list_admin_content_header(); ?>
            </div>
            <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th column="seq"> No </th>
                        <th column="trx_no"> No Transaksi </th>
                        <th column="created_by"> <?php echo TH_CREATED_BY; ?> </th>
                        <th column="created_date"> <?php echo TH_CREATED_DATE; ?> </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <a href="<?php echo get_base_url() . 'admin/master/coupon'; ?>" class="btn btn-google-plus">Back</a>
    <?php
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>