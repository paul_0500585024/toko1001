<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
?>

<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo $data_auth[FORM_AUTH][FORM_TITLE] ?></h3>
        </div>
        <div class="box-body">
            <form class="form-horizontal" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>" id="frmMain" onsubmit ="return validate_form();" enctype ="multipart/form-data">
                <section class="col-md-12">
                    <?php echo get_csrf_admin_token(); ?>
                    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                        <input name ="seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->seq; ?>">
                        <input name="oldname" type="hidden" value="<?php echo $data_sel[LIST_DATA][0]->profile_img ?>">
                        <input name="key" type="hidden" value="<?php echo $data_sel[LIST_DATA][0]->seq ?>">
                    <?php } ?>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#t_1" aria-controls="t_1" role="tab" data-toggle="tab">Info Login</a></li>
                        <li role="presentation"><a href="#t_2" aria-controls="t_2" role="tab" data-toggle="tab">Info Partner</a></li>
                        <li role="presentation"><a href="#t_3" aria-controls="t_6" role="tab" data-toggle="tab">Info Bank</a></li>
                    </ul>
                    <!--Tab Info Login (1)-->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="t_1"><br />
                            <section class="col-md-12">
                                <div class="col-md-6">
                                    <div class ="form-group">
                                        <label class ="control-label col-md-3">Email</label>
                                        <div class ="col-md-9">
                                            <input class="form-control" name="email" type="email" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->email) : "") ?>" <?php echo (isset($data_sel[LIST_DATA][0]) ? 'readonly=""' : '') ?> required>
                                        </div>
                                    </div>
                                </div>
                                <?php if ($data_auth[FORM_ACTION] == ACTION_ADD) { ?>
                                    <div class="col-md-6">
                                        <div class ="form-group">
                                            <label class ="control-label col-md-3">Password</label>
                                            <div class ="col-md-9">
                                                <input class="form-control" name="password" type="password" value ="" required>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </section>
                        </div>
                        <!--Tab Info Partner (2)-->
                        <div role="tabpanel" class="tab-pane" id="t_2"><br />
                            <section class="col-md-12">
                                <div class="col-md-6">
                                    <div class ="form-group">
                                        <label class ="control-label col-md-3">Nama</label>
                                        <div class ="col-md-9">
                                            <input class="form-control" name="name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->name) : "") ?>" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class ="form-group">
                                        <label class ="control-label col-md-3">No. Telp</label>
                                        <div class ="col-md-9">
                                            <input class="form-control" data-inputmask="&quot;mask&quot;: &quot;9999 9999 9999&quot;" data-mask="" validate="required[]" aria-required="true" aria-invalid="false" name="mobile_phone" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->mobile_phone) : "") ?>" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class ="form-group">
                                        <label class ="control-label col-md-3">Biaya Admin</label>
                                        <div class ="col-md-9">
                                            <input class="form-control auto_int" name="admin_fee" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->admin_fee) : "0") ?>" required>
                                        </div>
                                    </div>
                                </div>
                                <?php if ($data_auth[FORM_ACTION] == ACTION_ADD) { ?>
                                    <div class="col-md-6">
                                        <div class ="form-group">
                                            <label class ="control-label col-md-3">Status</label>
                                            <div class ="col-md-9">
                                                <input class="form-control" name="status" type="text" value ="<?php echo get_display_value(get_display_status('N', STATUS_MEMBER)) ?>" readonly="" required>
                                            </div>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <div class="col-md-6">
                                        <div class ="form-group">
                                            <label class ="control-label col-md-3">Status</label>
                                            <div class ="col-md-9">
                                                <input class="form-control" name="status" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->status) : "") ?>" <?php echo (isset($data_sel[LIST_DATA][0]) ? 'readonly=""' : '') ?> required>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>

                                <div class="col-md-6">
                                    <div class ="form-group">
                                        <label class ="control-label col-md-3">Foto Profil</label>
                                        <div class ="form-group">
                                            <div class="row">
                                                <div class="col-xs-4 col-xs-offset-2">
                                                    <div class="slim height-img width-img" data-label="Drop your image here" >
                                                        <?php if ($data_err[ERROR] == true && isset($data_err[ERROR_MESSAGE])) { ?>
                                                            <input type="file" accept="image/gif,image/jpeg,image/png"
                                                                   name="image"><br> <img
                                                                   width="<?php echo unserialize(IMAGE_DIMENSION_LOGO)["max_width"] ?>px"
                                                                   src="<?php echo json_decode($this->input->post('image'), true)['output']['image'] ?>"
                                                                   alt="Gambar Profil">

                                                        <?php } else { ?>
                                                            <input type="file" accept="image/gif,image/jpeg,image/png"
                                                                   name="image"><br> <img
                                                                   width="<?php echo unserialize(IMAGE_DIMENSION_LOGO)["max_width"] ?>px"
                                                                   <?php echo isset($data_sel[LIST_DATA][0]->profile_img) ? ( $data_sel[LIST_DATA][0]->profile_img == '' ? '' : 'src="' . base_url() . PROFILE_IMAGE_PARTNER . '/'.$data_sel[LIST_DATA][0]->seq.'/'.$data_sel[LIST_DATA][0]->profile_img . '"') : '' ?>
                                                                   alt="Gambar Profil">
                                                               <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6">
                                                    <p class="info-text">
                                                        <br><b>Photo Partner</b> Gambar yang di upload harus
                                                        dalam bentuk JPEG, JPG, PNG dengan resolusi min 150x150
                                                        sampai dengan 200x200 harus berbentuk persegi
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </section>
                        </div>
                        
                        <!--Tab Info Bank (3)-->
                        <div role="tabpanel" class="tab-pane" id="t_3"><br />
                            <div class="col-md-6">
                                <div class ="form-group">
                                    <label class ="control-label col-md-3">Nama Bank</label>
                                    <div class ="col-md-9">
                                        <input class="form-control" name="bank_name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->bank_name) : "") ?>" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class ="form-group">
                                    <label class ="control-label col-md-3">Cabang</label>
                                    <div class ="col-md-9">
                                        <input class="form-control" name="bank_branch_name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->bank_branch_name) : "") ?>" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class ="form-group">
                                    <label class ="control-label col-md-3">No. Rekening</label>
                                    <div class ="col-md-9">
                                        <input class="form-control" name="bank_acct_no" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->bank_acct_no) : "") ?>" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class ="form-group">
                                    <label class ="control-label col-md-3">Rekening A/N</label>
                                    <div class ="col-md-9">
                                        <input class="form-control" name="bank_acct_name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->bank_acct_name) : "") ?>" required="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class ="form-group">
                            <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                                <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                                <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                            <?php } ?>
                            <?php if ($data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
                                <div class ="col-md-6"><?php echo get_back_button(); ?></div> 
                            <?php } ?>
                            <?php if ($data_auth[FORM_ACTION] == ACTION_ADD) { ?>
                                <div class ="col-md-6"><?php echo get_save_add_button(); ?></div> 
                                <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                            <?php } ?>
                        </div>
                    </div>
                </section>
            </form>
        </div>
    </div>
    <script>function previewimg(thisval) {
                if (thisval.files && thisval.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {

                        $('#iprev').attr('src', e.target.result).height(70);
                    }
                    reader.readAsDataURL(thisval.files[0]);
                }
            }
            $(document).ready(function() {
                $("#grup").select2({
                    tags: true, placeholder: " Select/New", allowClear: true, maximumSelectionLength: 1
                })
            })

            function previewimg(thisval) {
                if (thisval.files && thisval.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {

                        $('#iprev').attr('src', e.target.result).height(70);
                    }
                    reader.readAsDataURL(thisval.files[0]);
                }
            }
            $('.slim').slim({
                ratio: '1:1',
                minSize: {
                    width: <?php echo unserialize(IMAGE_DIMENSION_LOGO)["min_width"] ?>,
                    height: <?php echo unserialize(IMAGE_DIMENSION_LOGO)["min_height"] ?>,
                },
                size: {
                    width: <?php echo unserialize(IMAGE_DIMENSION_LOGO)["max_width"] ?>,
                    height: <?php echo unserialize(IMAGE_DIMENSION_LOGO)["max_height"] ?>,
                },
                maxFileSize: "2",
                jpegCompression: "60",
                label: 'Upload Foto Agen Anda',
                onRemove: function(slim, data) {
                    $("#oldfile").val("");
                }

            });
    </script>
<?php } else { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <?php require_once get_include_page_list_admin_content_header(); ?>
            <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th column="name">Nama Partner</th>
                        <th column="email">Email</th>
                        <th column="admin_fee">Biaya Admin</th>
                        <th column="created_by"> <?php echo TH_CREATED_BY; ?> </th>
                        <th column="created_date"> <?php echo TH_CREATED_DATE; ?> </th>
                        <th column="modified_by"> <?php echo TH_MODIFIED_BY; ?> </th>
                        <th column="modified_date"> <?php echo TH_MODIFIED_DATE; ?> </th>
                        <th column="generate_password"> <?php echo "Generate Password"; ?> </th>
                    </tr>
                </thead>
            </table>    
        </div>
    </div>

    <?php
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>

