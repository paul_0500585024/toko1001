<?php
require_once VIEW_BASE_ADMIN;

require_once get_include_content_admin_top_page_navigation();
//var_dump($data_sel);
//die();
?>

<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo ($data_auth[FORM_ACTION] == ACTION_ADD ? get_title_add($data_auth[FORM_AUTH][FORM_TITLE]) : get_title_edit($data_auth[FORM_AUTH][FORM_TITLE])); ?></h3>
    </div>
    <form id="frmMain" name="frmMain" class="form-horizontal" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
        <div class="box-body">
            <section class="col-md-12">
                <?php echo get_csrf_admin_token(); ?>
                <input name ="coupon_period_from" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->coupon_period_from; ?>">
                <input name ="coupon_period_to" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->coupon_period_to; ?>">
                <input name ="coupon_code" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->coupon_code; ?>">       
                <input name ="coupon_limit" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->coupon_limit; ?>">       
                <input name ="nominal" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->nominal; ?>">       
                <div class="col-md-6">
                    <div class ="form-group">
                        <label class ="control-label col-md-4">Periode</label>
                        <div class ="col-md-8">
                            <input class="form-control" date_type="date" id ="date_from" name ="date_from" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? date("d-M-Y", strtotime($data_sel[LIST_DATA][0]->coupon_period_from)) . ' - ' . date("d-M-Y", strtotime($data_sel[LIST_DATA][0]->coupon_period_to)) : "" ); ?>" readonly>
                        </div>
                    </div>
                    <div class ="form-group">
                        <label class ="control-label col-md-4">Kode Kupon</label>
                        <div class ="col-md-8">
                            <input class="form-control" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->coupon_code : ""); ?>" readonly>
                        </div>
                    </div>
                </div>
                <div class ="col-md-6">
                    <div class ="form-group">
                        <label class ="control-label col-md-4">Batas Pemakaian</label>
                        <div class ="col-md-8">
                            <input class="form-control" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->coupon_limit : ""); ?> " readonly>
                        </div>
                    </div>
                    <div class ="form-group">
                        <label class ="control-label col-md-4">Nominal</label>
                        <div class ="col-md-8">
                            <input class="form-control" value="<?php echo (isset($data_sel[LIST_DATA]) ? number_format($data_sel[LIST_DATA][0]->nominal) : ""); ?>" readonly>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <hr> 
                    <div class ="form-group">
                        <label class ="control-label col-md-2"><i class="fa fa-cog"></i>&nbsp; Kupon Produk </label>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div id="tree_menu" style="overflow-y:auto;height:525px;border: 1px solid black; width: 100%;">
                                    <?php require_once get_component_url() . "treeview/tree_view_category.php"; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Pilih Produk</label>
                            <div id="product_list">
                                <select size="10" class="form-control" multiple="multiple" name="change_product" id="change_product"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="button" id="add" name="add" value="Tambah" class="btn btn-flat btn-primary"/>
                        </div>
                        <div class="form-group">
                            <label>Produk Pilihan</label>
                            <div id="idkota_o">
                                <select size="10" class="form-control" multiple="multiple" name="select_product[]" id="select_product">
                                    <?php if (isset($data_sel[LIST_DATA][1][0]->coupon_seq)) { ?>
                                        <?php
                                        foreach ($data_sel[LIST_DATA][1] as $each) {
                                            echo '<option value="' . $each->product_seq . '">' . $each->product_name . '</option>';
                                        }
                                        ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class = "form-group">
                            <input type = "button" id = "remove" value = "Hapus" class = "btn btn-flat btn-danger" />
                        </div>
                    </div>
                </div>
                <div class = "box-footer">
                    <div class = "row">
                        <div class = "col-md-6"><?php echo get_save_add_button(); ?> </div>
                        <div class ="col-md-6"><?php echo get_cancel_link(get_base_url() . "admin/master/coupon"); ?></div>
                    </div>
                </div>
            </section>
        </div>
    </form>
</div>
<script type="text/javascript">
        var is_selection_product_list = false;
        var error_html = '<div class="alert alert-error-admin" style="margin-bottom: 0!important;> <h4><i class="fa fa-info"></i></h4><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
        function pilihmenu(values) {
            $.ajax({
                url: "<?php echo current_url() ?>",
                data: "data=" + values + "&btnAdditional=true",
                type: "POST",
                success: function(response) {
                    list_obj = JSON.parse(response);
                    selection = '<select size="10" class="form-control" multiple="multiple" name="change_product" id="change_product">';
                    $.each(list_obj.list_product, function(product_variant_seq, product_variant_name) {
                        selection += '<option value="' + product_variant_seq + '">' + product_variant_name + '</option>';
                        is_selection_product_list = true;
                    });
                    selection += '</select>';
                    $('#product_list').html(selection);
                },
                error: function(request, error) {
                    alert(error_response(request.status));
                }
            });

        }
        function is_selected(selectoptionfield) {
            retval = false;
            selectoptionfield.each(function() {
                if ($(this).is(':selected')) {
                    retval = true;
                }
            });
            return retval;
        }

        function get_object_selected(selectoption) {
            list = [];
            $.each(selectoption, function() {
                value = $(this).val();
                text_display = $(this).text();
                list.push({val: value, text: text_display});
            });
            return list;
        }



        $('#add').click(function() {
            if (is_selection_product_list) {
<?php if ($data_sel[LIST_DATA][0]->status == NEW_STATUS_CODE) { ?>
                    if (is_selected($('#change_product option'))) {
                        var change_product = [];
                        var select_product = [];
                        select_product = get_object_selected($("#select_product option"));
                        change_product = get_object_selected($("#change_product option:selected"));

                        var selected_change_product = '';

                        var selected_select_product_value = [];
                        $.each(select_product, function(key, each_result) {
                            value = each_result.val;
                            text = each_result.text;
                            selected_select_product_value.push(value);
                        });

                        $.each(change_product, function(key, each_result) {
                            value = each_result.val;
                            text = each_result.text;
                            if ($.inArray(value, selected_select_product_value) == -1) {
                                selected_change_product += '<option value="' + value + '">' + text + '</option>';
                            }
                        });
                        $('#select_product').append(selected_change_product);
                    } else {
                        alert('Pilih Produk !');
                    }
<?php } else { ?>
                    $('#content-message').html(error_html + '<?php echo ERROR_SAVE_ADD ?>' + "</div>");
                    $('#add').attr('disabled', true);
<?php } ?>
            } else {
                $('#content-message').html(error_html + "Pilih kategory Produk </div>");
            }

        });

        $('#remove').click(function() {
            if (is_selected($('#select_product option'))) {
                var select_product = [];
                select_product = get_object_selected($("#select_product option:selected"));
                $("#select_product option:selected").remove();
            } else {
                $('#content-message').html(error_html + "Pilih produk yang akan dihapus </div>");
            }
        });
        $("form[name=frmMain]").submit(function() {
            $("#select_product option").prop("selected", "selected");
            if (!is_selected($('#select_product option'))) {
                $('#content-message').html(error_html + "Pilih Produk </div>");
                return false;
            }
        });
</script>
<?php
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>