<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
?>

<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_css_url() ?>daterangepicker-bs3.css" />
    <script type="text/javascript" src="<?php echo get_js_url() ?>moment.min.js"></script>
    <script type="text/javascript" src="<?php echo get_js_url() ?>daterangepicker.js"></script>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo $data_auth[FORM_ACTION_TITLE] . $data_auth[FORM_AUTH][FORM_TITLE] ?></h3>
        </div>
        <form class="form-horizontal" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>" enctype ="multipart/form-data">
            <div class="box-body">
                <section class="col-md-6">
                    <?php echo get_csrf_admin_token(); ?>
                    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                        <input class="form-control" name ="seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->seq; ?>">
                    <?php } ?>
                    <div>
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#t_1" aria-controls="t_1" role="tab" data-toggle="tab">Info Login</a></li>
                            <li role="presentation"><a href="#t_2" aria-controls="t_2" role="tab" data-toggle="tab">Info Agen</a></li>
                            <li role="presentation"><a href="#t_3" aria-controls="t_3" role="tab" data-toggle="tab">Foto Agen</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="t_1"><br />
                                <div class ="form-group">
                                    <label class ="control-label col-md-3">Email</label>
                                    <div class ="col-md-9">
                                        <input class="form-control" id="email" name="email" type="email" placeholder ="Input Email" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->email : "") ?>" disabled>
                                    </div>
                                </div>
                                <?php if ($data_auth[FORM_ACTION] == ACTION_ADD || $data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                                    <div class ="form-group">
                                        <label class ="control-label col-md-3">Password</label>
                                        <div class ="col-md-9">
                                            <input class="form-control" id="password" name="password" type="password" placeholder ="Input password" value ="">
                                        </div>
                                    </div>
                                    <input class="form-control" name ="profile_img" type="hidden" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->profile_img : ""); ?>">

                                <?php } ?>
                            </div><!-- end t1 -->
                            <div role="tabpanel" class="tab-pane" id="t_2"><br />
                                <div class ="form-group">
                                    <label class ="control-label col-md-3">Nama</label>
                                    <div class ="col-md-9">
                                        <input class="form-control" name="name" type="text" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->name : "") ?>"disabled>
                                    </div>
                                </div>
                                <div class ="form-group">
                                    <label class ="control-label col-md-3">Tanggal Lahir</label>
                                    <div class ="col-md-9">
                                        <input class="form-control" name="birthday" id="birthday" type="text" value="<?php echo (isset($data_sel[LIST_DATA]) ? cdate($data_sel[LIST_DATA][0]->birthday) : "") ?>"disabled>
                                    </div>
                                </div>
                                <div class ="form-group">
                                    <label class ="control-label col-md-3">Jenis Kelamin</label>
                                    <div class="col-md-9"><?php echo display_gender(isset($data_sel[LIST_DATA][0]->gender)?$data_sel[LIST_DATA][0]->gender:'','gender');?></div>
                                </div>
                                <div class ="form-group">
                                    <label class ="control-label col-md-3">No Telepon</label>
                                    <div class ="col-md-9">
                                        <input class="form-control" name="mobile_phone" type="text" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->mobile_phone : "") ?>"disabled>
                                    </div>
                                </div>
                            </div><!-- end t2 -->
                            <div role="tabpanel" class="tab-pane" id="t_3"><br />
                                <?php
                                $gambar = "";
                                if (isset($data_sel[LIST_DATA])) {
                                    if ($data_sel[LIST_DATA][0]->profile_img != '') {
                                        $gambar = get_img_url() . ROUTE_AGENT . $data_sel[LIST_DATA][0]->profile_img;
                                    }
                                }
                                ?>
                                <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT || $data_auth[FORM_ACTION] == ACTION_ADD) { ?>
                                    <br /><input type="file" id="profile-image-upload" name="profile-image-upload" class="btn btn-default" onchange ="previewimg(this)" />
                                <?php } ?>
                                <img id="iprev" src="<?php echo $gambar; ?>" height="100" />
                            </div><!-- end t3 -->
                        </div>
                    </div>
                    </br>
                    <div class ="form-group">
                        <?php if ($data_auth[FORM_ACTION] == ACTION_VIEW) {
                            ?>
                            <div class ="col-md-6"><?php echo get_back_button(); ?> </div>
                            <?php
                        } else {
                            if ($data_auth[FORM_ACTION] == ACTION_ADD) {
                                ?>
                                <div class ="col-md-6"><?php echo get_save_add_button(); ?> </div>
                                <?php
                            }
                            if ($data_auth[FORM_ACTION] == ACTION_EDIT) {
                                ?>
                                <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                            <?php } ?>
                            <div class ="col-md-6"><?php echo get_back_button(); ?> </div>
                        <?php } ?>
                </section>
            </div>
        </form>
    </div>
    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT || $data_auth[FORM_ACTION] == ACTION_ADD) { ?>
        <script type="text/javascript">
                                            <!--
                                                $('.form-control').prop("disabled", false);
                                            $('.radio').prop("disabled", false);
                                            $('input[name="birthday"]').daterangepicker({
                                                format: 'DD-MMM-YYYY',
                                                showDropdowns: true,
                                                singleDatePicker: true
                                            });
                                            function previewimg(thisval) {
                                                if (thisval.files && thisval.files[0]) {
                                                    var reader = new FileReader();
                                                    reader.onload = function(e) {

                                                        $('#iprev').attr('src', e.target.result).height(70);
                                                    }
                                                    reader.readAsDataURL(thisval.files[0]);
                                                }
                                            }

        <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) echo "$('#email').prop('disabled', true);"; ?>
                                            //-->
        </script>
        <?php
    }
} else {
    ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <?php require_once get_include_page_list_admin_content_header(); ?>
            <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th column="email"> Email</th>
                        <th column="name"> Nama Agen</th>
                        <th column="status"> Status </th>
                        <th column="gender"> Gender </th>
                        <th column="mobile_phone"> No Tlp </th>
                        <th column="created_by"> <?php echo TH_CREATED_BY; ?> </th>
                        <th column="created_date"> <?php echo TH_CREATED_DATE; ?> </th>
                        <th column="modified_by"> <?php echo TH_MODIFIED_BY; ?> </th>
                        <th column="modified_date"> <?php echo TH_MODIFIED_DATE; ?> </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <?php
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>