<?php
require_once VIEW_BASE_ADMIN;
?>

<form id ="frmSearch"  url= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
    <div class="form-group">
        <label>Batal Bayar Via</label>
        <?php require_once get_component_url() . "dropdown/com_drop_payment_method.php" ?>
    </div>
    <div class="form-group">
        <label>Agent Email</label>
        <?php require_once get_component_url() . "dropdown/com_drop_member_email.php" ?>
    </div>
    
    <div class="form-group">
        <label>Status</label> 
        <p>            
        <input type="radio" name="status" id="optionsRadios2" value="N">&nbsp;Baru &nbsp;
        <input type="radio" name="status" id="optionsRadios2" value="A">&nbsp;Setuju &nbsp;
        <input type="radio" name="status" id="optionsRadios2" value="R">&nbsp;Tolak &nbsp;
        <input type="radio" name="status" id="optionsRadios2" value="">&nbsp;Semua &nbsp;
        
    </div>  
    <div class="form-group">
	<?php echo get_search_button(); ?>
    </div>
</form>