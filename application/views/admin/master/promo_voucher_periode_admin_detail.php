<?php
require_once VIEW_BASE_ADMIN;

require_once get_include_content_admin_top_page_navigation();
?>

<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo ($data_auth[FORM_ACTION] == ACTION_ADD ? get_title_add($data_auth[FORM_AUTH][FORM_TITLE]) : get_title_edit($data_auth[FORM_AUTH][FORM_TITLE])); ?></h3>
        </div>
        <form id="frmMain" class="form-horizontal" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
            <div class="box-body">
                <section class="col-md-6">
                    <?php echo get_csrf_admin_token(); ?>
                    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                        <input name ="seq" type="hidden" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->voucher_seq : ""); ?>"></input>
                        <input name ="exp_days" type="hidden" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->exp_days : ""); ?>"></input>
                    <?php } ?>
                    <div class ="form-group">
                        <label class ="control-label col-md-3">Code </label>
                        <div class ="col-md-9">
                            <input class="form-control" name="code" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->code : "") ?>" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class ="control-label col-md-3">Member </label>
                        <div class ="col-md-9">
                            <?php require_once get_component_url() . 'dropdown/com_drop_member.php' ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class ="control-label col-md-3">Nominal </label>
                        <div class ="col-md-9">
                            <input class="form-control" name="nominal" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? number_format($data_sel[LIST_DATA][0]->nominal) : "") ?>" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class ="control-label col-md-3">Nilai Belanja </label>
                        <div class ="col-md-9">
                            <input class="form-control" name="trx_use_amt" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? number_format($data_sel[LIST_DATA][0]->trx_use_amt) : "") ?>" readonly>
                        </div>
                    </div>
                    <div class ="form-group">
                        <?php if ($data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
                            <div class ="col-md-6"><?php echo get_back_button(); ?> </div>
                            <?php
                        } else {
                            if ($data_auth[FORM_ACTION] == ACTION_ADD) {
                                ?>
                                <div class ="col-md-6"><?php echo get_save_add_button(); ?> </div>
                            <?php } if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                                <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                            <?php } ?>
                            <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                        <?php } ?>
                    </div>
                </section>
            </div><!-- /.box-footer -->
        </form>
    </div>
<?php } else { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">

            <form class ="form-horizontal col-md-12">
                <div class ="form-group">
                    <label class ="control-label col-md-2">Periode</label>
                    <div class ="col-md-3">
                        <input class="form-control" date_type="date" id ="date_from" name ="date_from" type="text" value ="<?php echo (isset($data_head[LIST_DATA][0]) ? date("d-M-Y", strtotime($data_head[LIST_DATA][0]->date_from)) . ' - ' . date("d-M-Y", strtotime($data_head[LIST_DATA][0]->date_to)) : "" ); ?>" readonly>
                    </div>
                </div>
                <div class ="form-group">
                    <label class ="control-label col-md-2">Nama Voucher</label>
                    <div class ="col-md-3">
                        <input class="form-control" value="<?php echo (isset($data_head[LIST_DATA]) ? $data_head[LIST_DATA][0]->name : ""); ?>" readonly>
                    </div>
                </div>
                <div class ="form-group">
                    <label class ="control-label col-md-2">Kode Voucher</label>
                    <div class ="col-md-3">
                        <input class="form-control" value="<?php echo (isset($data_head[LIST_DATA]) ? $data_head[LIST_DATA][0]->code : ""); ?>" readonly>
                    </div>
                </div>
                <div class ="form-group">
                    <label class ="control-label col-md-2">Transaksi</label>
                    <div class ="col-md-3">
                        <input class="form-control" value="<?php echo (isset($data_head[LIST_DATA]) ? $data_head[LIST_DATA][0]->node_name : ""); ?>" readonly>
                    </div>
                </div>
                <div class ="form-group">
                    <div class ="col-md-offset-2 col-md-3">
                        <label><input type="checkbox" name ="active" <?php echo (isset($data_head[LIST_DATA]) ? ($data_head[LIST_DATA][0]->active == "1" ? "checked" : "") : ''); ?> disabled/> Aktif</label>
                    </div>
                </div>
            </form>
            <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class = "fa fa-download"></i> <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><button class = "btn btn-flat btn-default" id="e_csv" style="width:100%" type='button'>CSV</button></li>
                    <li><button class = "btn btn-flat btn-default" id="e_xls" style="width:100%" type='button'>Excel</button></li>
                    <li><button class = "btn btn-flat btn-default" id="e_pdf" style="width:100%" type='button'>PDF</button></li>
                </ul>
            </div>
            <br>
            <br>
            <div class="box-tools pull-left">
                <?php if ($data_head[LIST_DATA][0]->type == 'M' && $data_head[LIST_DATA][0]->node_cd == NODE_GIFT_MEMBER) { ?>
                    <?php require_once get_include_page_list_admin_content_header(); ?>
                    <?php
                    if (!$data_auth[FORM_AUTH][FORM_AUTH_EDIT] === false) {
                        ?>
                        <section class ="col-md-12">
                            <form id = "frmAdd" class="form-horizontal" method = "post" action = "<?php echo get_base_url() . $data_auth[FORM_URL] ?>" enctype = "multipart/form-data">
                                <?php echo get_csrf_admin_token(); ?>
                                <div class ="form-group">
                                    <div class="input-group col-md-5">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                Pilih File
                                                <input class='form-control' id="file_upload" name='userfile' type="file" onchange="set_upload(this.value)" accept=".csv">
                                            </span>
                                        </span>
                                        <input class="form-control" readonly="" id="upload_url" type="text">
                                        <span class="input-group-btn">
                                            <button name ="<?php echo CONTROL_ADDITIONAL_NAME ?>" type = "submit" class = "btn btn-primary"><i class='fa fa-upload'></i> Unggah</button>
                                        </span>
                                    </div>
                                </div>
                            </form>
                        </section>
                    <?php } ?>
                    </br></br>
                <?php } ?>
            </div>
            <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th column="code"> Kode Voucher </th>
                        <th column="member_name"> Member </th>
                        <th column="nominal"> Nominal </th>
                        <th column="trx_use_amt"> Nilai Belanja </th>
                        <th column="active_date"> Tanggal Aktif </th>
                        <th column="exp_date"> Tanggal kadaluarsa </th>
                        <th column="trx_no"> No Order </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <a href="<?php echo get_base_url() . 'admin/master/promo_voucher_periode'; ?>" class="btn btn-google-plus">Back</a>
    <script text ="javascript">

            function set_upload(url_upload) {
                $('#upload_url').val(url_upload);
            }

    </script>
    <?php
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>