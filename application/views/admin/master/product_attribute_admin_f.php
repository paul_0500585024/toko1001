<?php
require_once VIEW_BASE_ADMIN;
?>

<form id ="frmSearch"  url= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
    <div class="form-group">
        <label>Kategori</label>
        <?php include dirname(__FILE__) . '/../component/dropdown/com_drop_product_category.php' ?>
    </div>
    <div class="form-group">
        <label>Nama Atribut</label>
        <input class="form-control" name="name" type="text">
    </div>
    <div class="form-group">
        <label>Tampilan Atribut</label>
        <input class="form-control" name="display_name" type="text">
    </div>
    <div class="form-group">
        <input name ="filter" type="checkbox" checked > Tampil di Pencarian</input>
    </div>
    <div class="form-group">
        <input name ="active" type="checkbox" checked > Aktif</input>
    </div>
    <div class="form-group">
        <?php echo get_search_button(); ?>
    </div>
</form>
