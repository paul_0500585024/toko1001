<?php
require_once VIEW_BASE_ADMIN;
?>

<form id ="frmSearch"  url= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
    <div class="form-group">
        <label>Tipe Setting</label>
        <?php require_once get_component_url() . "/dropdown/com_drop_setting.php" ?>
    </div>
    <div class="form-group">
        <label>Nama Setting</label>
        <input class="form-control" name="name" type="input">
    </div>
    <div class="form-group">
        <label>Nilai Data</label>
        <input class="form-control" name="value" type="input">
    </div>
    <div class="form-group">
        <input name ="active" type="checkbox" checked > Aktif </input>
    </div>
    <div class="form-group">
        <?php echo get_search_button(); ?>
    </div>
</form>