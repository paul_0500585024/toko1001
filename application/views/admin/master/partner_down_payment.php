<?php
require_once VIEW_BASE_ADMIN;

require_once get_include_content_admin_top_page_navigation();
?>
<div id="error"></div>
<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo $data_auth[FORM_ACTION_TITLE] . $data_auth[FORM_AUTH][FORM_TITLE] ?></h3>
        </div>
        <form class ="form-horizontal" id="frmMain" onsubmit ="return validate_form();" method ="post" enctype ="multipart/form-data" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
            <div class="box-body">
                <div class="col-md-7">
                    <?php echo get_csrf_admin_token(); ?>
                    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                        <input class="form-control"  name ="seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->seq; ?>">
                    <?php } ?>
                    <div class ="form-group">
                        <label class ="control-label col-md-3">Category Loan</label>
                        <div class ="col-md-9">
                            <?php include dirname(__FILE__) . '/../component/dropdown/com_drop_partner_dp_category.php' ?>
                        </div>
                    </div>
                    <div class ="form-group">
                        <label class ="control-label col-md-3">Tenor (Bulan)</label>
                        <div class ="col-md-9">
                            <?php include dirname(__FILE__) . '/../component/dropdown/com_drop_partner_dp_tenor.php' ?>
                        </div>
                    </div>
                    <div class ="form-group">
                        <div class ="col-md-offset-3 col-md-9">
                            <label> <input type="checkbox" name ="active" <?php echo (!isset($data_sel[LIST_DATA]) ? "checked" : (($data_sel[LIST_DATA][0]->active == "1" OR $data_sel[LIST_DATA][0]->active == "on") ? "checked" : "")); ?> /> Aktif</label>
                        </div>
                    </div>
                    <div class ="form-group">
                        <label class ="control-label col-md-3">Minimal Loan</label>
                        <div class ="col-md-9">
                            <input class="form-control auto_int" validate ="required[]" data-inputmask="'mask': ['99999 99999 99999 99999']" data-mask name ="minimal_loan" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->minimal_loan : "" ); ?>">
                        </div>
                    </div>
                    <div class ="form-group">
                        <label class ="control-label col-md-3">Maximal Loan</label>
                        <div class ="col-md-9">
                            <input class="form-control auto_int" validate ="required[]" data-inputmask="'mask': ['99999 99999 99999 99999']" data-mask name ="maximal_loan" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->maximal_loan : "" ); ?>">
                        </div>
                    </div>
                    <div class ="form-group">
                        <label class ="control-label col-md-3">Minimal DP (%)</label>
                        <div class ="col-md-9">
                            <input class="form-control auto_dec" data-d-group="3" data-v-max="100" validate ="required[]" data-inputmask="'mask': ['99999 99999 99999 99999']" data-mask name ="min_dp_percent" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->min_dp_percent : "" ); ?>" onkeypress="return hanyaAngka(event, false)">
                        </div>
                    </div>
                </div>    
                <div class ="form-group">
                    <?php
                    if ($data_auth[FORM_ACTION] == ACTION_VIEW) {
                        ?>
                        <div class ="col-md-6"><?php echo get_back_button(); ?> </div>
                        <?php
                    } else {
                        if ($data_auth[FORM_ACTION] == ACTION_ADD) {
                            ?>
                            <div class ="col-md-6"><?php echo get_save_add_button(); ?> </div>
                        <?php } if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                            <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                        <?php } ?>
                        <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                    <?php } ?>
                </div>
            </div>
            </form>
    </div>
    
         <script text="javascript">
            $("#drop_category").change(function() {
                if ($(this).val() != ""){
                    $.ajax({
                        url: "<?php echo get_base_url() . $data_auth[FORM_URL] ?>",
                        dataType: "json",
                        data: {
                            btnAdditional: true,
                            product_category_seq: $(this).val()
                        },
                        type: "POST",
                        success: function(data) {
                            if(isSessionExpired(data)){
                                response_object = json_decode(data);
                                url = response_object.url;
                                location.href = url;
                            }else{
                                $('#drop_tenor option').remove();
                                $("#drop_tenor").append("<option value=\"" + "\">-- Pilih --</option>");
                                $(data).each(function(i) {
                                    $("#drop_tenor").append("<option value=\"" + data[i].seq + "\">" + data[i].tenor_loan + "</option>");
                                });
                                $("#drop_tenor").val(<?php echo isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA]->partner_loan_seq : ""; ?>);
                                $('#drop_tenor').select2();
                            }
                        },
                        error: function (request, error) {
                            $('#error').html(JSON.stringify(request));
                        }
                    });
                }
            }).change();
    </script>
<?php } else { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <?php require_once get_include_page_list_admin_content_header(); ?>
            <table id="tbl" class="diplay table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th column="partner_name">Partner</th>
                        <th column="category_loan">Category</th>
                        <th column="tenor_loan">Tenor (Bulan)</th>
                        <th column="minimal_loan">Min Loan</th>
                        <th column="maximal_loan">Max Loan</th>
                        <th column="min_dp_percent">DP (%)</th>
                        <th column="active"> Aktif </th>
                        <th column="created_by"><?php echo TH_CREATED_BY; ?></th>
                        <th column="created_date"><?php echo TH_CREATED_DATE; ?></th>
                        <th column="modified_by"><?php echo TH_MODIFIED_BY; ?></th>
                        <th column="modified_date"><?php echo TH_MODIFIED_DATE; ?></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <?php
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>
