<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
if (!isset($data_sel[LIST_DATA]))
    die('ERROR');
?>
<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo ($data_auth[FORM_ACTION] == ACTION_ADD ? get_title_add($data_auth[FORM_AUTH][FORM_TITLE]) : get_title_edit($data_auth[FORM_AUTH][FORM_TITLE])); ?></h3>
    </div>
    <form class="form-horizontal" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
	<?php echo get_csrf_admin_token(); ?>
        <input name ="seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->seq; ?>">
        <input name ="date_from" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->date_from; ?>">
        <input name ="date_to" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->date_to; ?>">
        <input name ="active" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->active; ?>">
        <input name ="all_origin_city" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->all_origin_city; ?>">
        <input name ="all_destination_city" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->all_destination_city; ?>">
        <input name ="status" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->status; ?>">
        <div class="box-body">
	    <div class ="form-group">
		<label class ="control-label col-md-2">Periode</label>
		<div class ="col-md-8">
		    <input class="form-control" date_type="date" id ="a0" name ="a0" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? date("d-M-Y", strtotime($data_sel[LIST_DATA][0]->date_from)) . ' - ' . date("d-M-Y", strtotime($data_sel[LIST_DATA][0]->date_to)) : "" ); ?>" readonly>
		</div>
	    </div>
	    <div class ="form-group">
		<label class ="control-label col-md-2">Keterangan</label>
		<div class ="col-md-8">
		    <input class="form-control" id="notes" readonly name ="notes" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->notes) : "" ); ?>">
		</div>
	    </div>
            <div class ="form-group">
                <div class ="col-md-offset-3 col-md-9">
                    <label> <input type="checkbox" name ="a1" <?php echo ($data_sel[LIST_DATA][0]->active == "1" ? "checked" : ""); ?> disabled/> Aktif</label>
                    <label> <input type="checkbox" name ="a2" <?php echo ($data_sel[LIST_DATA][0]->all_origin_city == "1" ? "checked" : ""); ?> disabled/> Semua Kota Asal Merchant</label>
                    <label> <input type="checkbox" name ="a3" <?php echo ($data_sel[LIST_DATA][0]->all_destination_city == "1" ? "checked" : ""); ?> disabled/> Semua Kota Tujuan Member</label>
                </div>
            </div>
        </div>
	<?php
	if (isset($tree_menu)) {
	    $origin = "";
	    $dest = "";
	    if ($tree_menu != "") {
		foreach ($tree_menu as $value) {
		    foreach ($value as $each) {
			if ($each->type == "O") {
			    $origin.='<option value="' . $each->city_seq . '" selected>' . $each->city_name . '</option>';
			} else {
			    $dest.='<option value="' . $each->city_seq . '" selected>' . $each->city_name . '</option>';
			}
		    }
		}
	    }
	}
	?>
        <div class="box-footer no-border">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <!-- tools box -->
                            <div class="pull-right box-tools">
                                <button class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 5px;"><i class="fa fa-minus"></i></button>
                            </div><!-- /. tools -->
                            <i class="fa fa-map-marker"></i>
                            <h3 class="box-title">Kota Asal Merchant</h3>
                        </div>
                        <div class="box-body" id="data_origin">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Pilih Propinsi</label>
                                        <select size="10" class="form-control" name="propinsi_o" id="propinsi_o">
					    <?php
					    if (isset($province_name)) {
						foreach ($province_name as $each) {
						    ?>
						    <option value="<?php echo $each->seq; ?>"><?php echo $each->name; ?></option>
						    <?php
						}
					    }
					    ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Pilih Kota</label>
                                        <div id="idkota_o">
                                            <select size="10" class="form-control" multiple="multiple" name="kota_o" id="kota_o">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <input type="button" id="add_o" name="add_o" value="Tambah" />
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Pilhan Kota</label>
                                        <select size="10" class="form-control" multiple="multiple" name="city_seq_o[]" id="city_seq_o">
					    <?php echo $origin; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <input type="button" id="remove_o" value="Hapus" />
                                </div>
                            </div>
                        </div><!-- /.box-body-->
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <!-- tools box -->
                            <div class="pull-right box-tools">
                                <button class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 5px;"><i class="fa fa-minus"></i></button>
                            </div><!-- /. tools -->
                            <i class="fa fa-map-marker"></i>
                            <h3 class="box-title">
                                Kota Tujuan Member
                            </h3>
                        </div>
                        <div class="box-body" id="data_dest">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Pilih Propinsi</label>
                                        <select size="10" class="form-control" name="propinsi_d" id="propinsi_d">
					    <?php
					    if (isset($province_name)) {
						foreach ($province_name as $each) {
						    ?>
						    <option value="<?php echo $each->seq; ?>"><?php echo $each->name; ?></option>
						    <?php
						}
					    }
					    ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Pilih Kota</label>
                                        <div id="idkota_d">
                                            <select size="10" class="form-control" multiple="multiple" name="kota_d" id="kota_d">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <input type="button" id="add_d" name="add_d" value="Tambah" />
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Pilhan Kota</label>
                                        <select size="10" class="form-control" multiple="multiple" name="city_seq_d[]" id="city_seq_d">
					    <?php echo $dest; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <input type="button" id="remove_d" value="Hapus" />
                                </div>
                            </div>
                        </div><!-- /.box-body-->
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class ="col-md-6"><?php echo (($data_sel[LIST_DATA][0]->status == 'N') ? get_save_add_button() : ''); ?> </div>
                <div class ="col-md-6"><?php echo get_cancel_link(get_base_url() . "admin/master/promo_free_fee"); ?></div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
<!--
    $(function () {
<?php echo ($data_sel[LIST_DATA][0]->all_origin_city == "1" ? "$('#data_origin').html('Semua Kota Asal Merchant');" : ""); ?>
<?php echo ($data_sel[LIST_DATA][0]->all_destination_city == "1" ? "$('#data_dest').html('Semua Kota Tujuan Member');" : ""); ?>
	var url = "<?php echo get_base_url() . $data_auth[FORM_URL]; ?>";
	$('#propinsi_o').change(function () {
	    inHTML = "";
	    $('#kota_o').empty();
	    $.ajax({
		url: url,
		data: {
		    btnAdditional: "act_s_adt", province_seq: $('#propinsi_o').val()
		},
		type: "POST",
		success: function (response) {
                    if(isSessionExpired(response)){
                        response_object = json_decode(response);
                        url = response_object.url;
                        location.href = url;
                    }else{
                        $.each($.parseJSON(response), function () {
                            inHTML += '<option value="' + this.seq + '">' + this.name + '</option>';
                        });
                        $("#kota_o").append(inHTML);
                    }
		},
                error: function (request, error) {
                    alert(error_response(request.status));
                }
	    });
	});

	$("#add_o").click(function () {

	    inHTML = "";
	    $("#kota_o > option:selected").each(function () {
		inHTML += '<option value="' + $(this).val() + '" selected>' + $(this).text() + '</option>';
	    });
	    $("#city_seq_o").append(inHTML);
	});

	$("#remove_o").click(function () {
	    $("#city_seq_o > option:selected").each(function () {
		$(this).remove();
	    });
	    $('#city_seq_o option').prop('selected', true);
	});

	var url = "<?php echo get_base_url() . $data_auth[FORM_URL]; ?>";
	$('#propinsi_d').change(function () {
	    inHTML = "";
	    $('#kota_d').empty();
	    $.ajax({
		url: url,
		data: {
		    btnAdditional: "act_s_adt", province_seq: $('#propinsi_d').val()
		},
		type: "POST",
		success: function (response) {
                    if(isSessionExpired(response)){
                        response_object = json_decode(response);
                        url = response_object.url;
                        location.href = url;
                    }else{
                        $.each($.parseJSON(response), function () {
                            inHTML += '<option value="' + this.seq + '">' + this.name + '</option>';
                        });
                        $("#kota_d").append(inHTML);
                    }
		},
                error: function (request, error) {
                    alert(error_response(request.status));
                }
	    });
	});

	$("#add_d").click(function () {
	    inHTML = "";
	    $("#kota_d > option:selected").each(function () {
		inHTML += '<option value="' + $(this).val() + '" selected>' + $(this).text() + '</option>';
	    });
	    $("#city_seq_d").append(inHTML);
	});

	$("#remove_d").click(function () {
	    $("#city_seq_d > option:selected").each(function () {
		$(this).remove();
	    });
	    $('#city_seq_d option').prop('selected', true);
	});
    });
//-->
</script>
<?php
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>