<?php
require_once VIEW_BASE_ADMIN;

require_once get_include_content_admin_top_page_navigation();
?>
<style>
.select2-dropdown .select2-search__field:focus,.select2-search--inline .select2-search__field:focus{
    outline:none;
    border:none !important;
}

</style>
<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo $data_auth[FORM_ACTION_TITLE] . $data_auth[FORM_AUTH][FORM_TITLE] ?></h3>
        </div>
        <form class ="form-horizontal" id="frmMain" onsubmit ="return validate_form();" method ="post" enctype ="multipart/form-data" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
            <div class="box-body">
                <div class="col-md-7">
                    <?php echo get_csrf_admin_token(); ?>
                    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                        <input class="form-control"  name ="seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->seq; ?>">
                    <?php } ?>
                    <div class ="form-group">
                        <label class ="control-label col-md-3">Nama Partner</label>
                        <div class ="col-md-9">
                            <?php include dirname(__FILE__) . '/../component/dropdown/com_drop_partner_name.php' ?>
                        </div>
                    </div>
                    <div class ="form-group">
                        <div class ="col-md-offset-3 col-md-9">
                            <label> <input type="checkbox" name ="active" <?php echo (!isset($data_sel[LIST_DATA]) ? "checked" : (($data_sel[LIST_DATA][0]->active == "1" OR $data_sel[LIST_DATA][0]->active == "on") ? "checked" : "")); ?> /> Aktif</label>
                        </div>
                    </div>
                    <div class ="form-group">
                        <label class ="control-label col-md-3">Nama Category</label>
                        <div class ="col-md-9">
                            <?php include dirname(__FILE__) . '/../component/dropdown/com_drop_category_name.php' ?>
                        </div>
                    </div>
                    <div class ="form-group">
                        <label class ="control-label col-md-3">Tenor (Bulan)</label>
                        <div class ="col-md-9">
                            
                            <?php 
                            $multiple = '';
                            if ($data_auth[FORM_ACTION] == ACTION_ADD){
                              $multiple = 'multiple="multiple"';
                            }
                            $existing_tenor = array();
                            $exist_tenor = array();
                            foreach($exist_tenor as $each_exist_tenor){
                                $existing_tenor[]= $each_exist_tenor->tenor;
                            }
                            ?>
                            <select name="tenor[]" class="form-control select2" <?php echo $multiple?>>
                                <?php for($i=1;$i<=255;$i++):?>
                                <?php if(!in_array($i, $existing_tenor)){?>
                                <?php $selected = ''; ?>
                                    <?php if(isset($data_sel[LIST_DATA][0]))?>
                                            <?php if($data_sel[LIST_DATA][0]->tenor == $i)$selected = 'selected'?>
                                                <option value="<?php echo $i?>" <?php echo $selected?>><?php echo $i?> bulan</option>
                                <?php }?>
                                <?php endfor;?>
                            </select>
                            
<?php /*                            <input class="form-control auto_int" data-d-group="3" data-v-max="255" validate ="required[]" data-inputmask="'mask': ['99999 99999 99999 99999']" data-mask name ="tenor" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->tenor : "" ); ?>" onkeypress="return hanyaAngka(event, false)"> */?>
                        </div>
                    </div>
                    <div class ="form-group">
                        <label class ="control-label col-md-3">Loan Interest (%)</label>
                        <div class ="col-md-9">
                            <input class="form-control auto_dec" data-d-group="3" data-v-max="100" validate="required[]" name ="loan_interest" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->loan_interest) : "" ); ?>">
                        </div>
                    </div>
                </div>    
                <div class ="form-group">
                    <?php
                    if ($data_auth[FORM_ACTION] == ACTION_VIEW) {
                        ?>
                        <div class ="col-md-6"><?php echo get_back_button(); ?> </div>
                        <?php
                    } else {
                        if ($data_auth[FORM_ACTION] == ACTION_ADD) {
                            ?>
                            <div class ="col-md-6"><?php echo get_save_add_button(); ?> </div>
                        <?php } if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                            <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                        <?php } ?>
                        <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                    <?php } ?>
                </div>
            </div>
    </div>
    </form>
<?php } else { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <?php require_once get_include_page_list_admin_content_header(); ?>
            <table id="tbl" class="diplay table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th column="partner_name"> Nama Partner</th>
                        <th column="category_name">Nama Category</th>
                        <th column="tenor">Tenor (Bulan)</th>
                        <th column="loan_interest">Loan Interets (%)</th>
                        <th column="active"> Aktif </th>
                        <th column="created_by"><?php echo TH_CREATED_BY; ?></th>
                        <th column="created_date"><?php echo TH_CREATED_DATE; ?></th>
                        <th column="modified_by"><?php echo TH_MODIFIED_BY; ?></th>
                        <th column="modified_date"><?php echo TH_MODIFIED_DATE; ?></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <?php
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>
