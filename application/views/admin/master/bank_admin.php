<?php
require_once VIEW_BASE_ADMIN;

require_once get_include_content_admin_top_page_navigation();
?>

<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo $data_auth[FORM_ACTION_TITLE] . $data_auth[FORM_AUTH][FORM_TITLE] ?></h3>
        </div>
        <form class ="form-horizontal" id="frmMain" onsubmit ="return validate_form();" method ="post" enctype ="multipart/form-data" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
            <div class="box-body">
                <div class="col-md-7">
                    <?php echo get_csrf_admin_token(); ?>
                    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                        <input class="form-control"  name ="seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->seq; ?>">
                        <input class="form-control" name="oldname" type="hidden" value="<?php echo $data_sel[LIST_DATA][0]->logo_img ?>">
                    <?php } ?>
                    <div class ="form-group">
                        <label class ="control-label col-md-3">Nama bank</label>
                        <div class ="col-md-9">
                            <input class="form-control" validate="required[]" name ="bank_name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->bank_name) : "" ); ?>" >
                        </div>
                    </div>
                    <div class ="form-group">
                        <div class ="col-md-offset-3 col-md-9">
                            <label> <input type="checkbox" name ="active" <?php echo (!isset($data_sel[LIST_DATA]) ? "checked" : (($data_sel[LIST_DATA][0]->active == "1" OR $data_sel[LIST_DATA][0]->active == "on") ? "checked" : "")); ?> /> Aktif</label>
                        </div>
                    </div>
                    <div class ="form-group">
                        <label class ="control-label col-md-3">Cabang bank</label>
                        <div class ="col-md-9">
                            <input class="form-control" validate="required[]" name ="bank_branch_name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->bank_branch_name) : "" ); ?>">
                        </div>
                    </div>
                    <div class ="form-group">
                        <label class ="control-label col-md-3">No rekening</label>
                        <div class ="col-md-9">
                            <input class="form-control" validate ="required[]" data-inputmask="'mask': ['99999 99999 99999 99999']" data-mask name ="bank_acct_no" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->bank_acct_no : "" ); ?>">
                        </div>
                    </div>
                    <div class ="form-group">
                        <label class ="control-label col-md-3">Nama rekening</label>
                        <div class ="col-md-9">
                            <input class="form-control" validate="required[]" name ="bank_acct_name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->bank_acct_name) : "" ); ?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <?php
                    if (isset($data_sel[LIST_DATA])) {
                        $gambar = get_base_url() . BANK_UPLOAD_IMAGE . $data_sel[LIST_DATA][0]->logo_img;
                    } else {
                        $gambar = get_base_url() . IMG_BLANK_100;
                    }
                    ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">Gambar Logo (h:70 - 200px, w:70 - 200px)</div>
                        <div class="panel-body">
                            <input type="file" id="nlogo_img" name="nlogo_img" class="btn btn-default" onchange ="previewimg(this)" />
                            <img id="iprev" src="<?php echo $gambar; ?>" height="70" />
                        </div>
                    </div>
                    <div class ="form-group">
                        <?php
                        if ($data_auth[FORM_ACTION] == ACTION_VIEW) {
                            ?>
                            <div class ="col-md-6"><?php echo get_back_button(); ?> </div>
                            <?php
                        } else {
                            if ($data_auth[FORM_ACTION] == ACTION_ADD) {
                                ?>
                                <div class ="col-md-6"><?php echo get_save_add_button(); ?> </div>
                            <?php } if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                                <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                            <?php } ?>
                            <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <script src="<?php echo get_js_url() ?>input-mask/jquery.inputmask.js"></script>
    <script type="text/javascript">
            function previewimg(thisval) {
                if (thisval.files && thisval.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {

                        $('#iprev').attr('src', e.target.result).height(70);
                    }
                    reader.readAsDataURL(thisval.files[0]);
                }
            }
            $("[data-mask]").inputmask();
    </script>

<?php } else { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <?php require_once get_include_page_list_admin_content_header(); ?>
            <table id="tbl" class="diplay table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th column="bank_name"> Nama Bank </th>
                        <th column="bank_branch_name"> Cabang Bank </th>
                        <th column="bank_acct_no"> No Rekening</th>
                        <th column="bank_acct_name"> Nama Rekening </th>
                        <th column="active"> Aktif </th>
                        <th column="created_by"><?php echo TH_CREATED_BY; ?></th>
                        <th column="created_date"><?php echo TH_CREATED_DATE; ?></th>
                        <th column="modified_by"><?php echo TH_MODIFIED_BY; ?></th>
                        <th column="modified_date"><?php echo TH_MODIFIED_DATE; ?></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <?php
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>