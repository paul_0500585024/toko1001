<?php
require_once VIEW_BASE_ADMIN;

require_once get_include_content_admin_top_page_navigation();
?>

<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_add($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <form class ="form-horizontal" id ="frmMain" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>" enctype="multipart/form-data">
            <div class="box-body">
                <section class ="col-md-8">
                    <?php echo get_csrf_admin_token(); ?>
                    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                        <input class="form-control"  name ="seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->seq; ?>">
                        <input class="form-control"  name ="old_img" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->img; ?>">
                    <?php } ?>
                    <div class="panel panel-default" style="height:500px">
                        <div class="panel-heading">Gambar Slide Show(h:477px, w:696px)</div>
                        <div class="panel-body">
                            <div class ="col-md-12">                                    
                                <div class ="form-group">  
                                    <input type="file" accept="<?php echo IMAGE_TYPE_UPLOAD; ?>" name="img" class="btn btn-default" onchange ="previewimg(this, 'iprev1')"  <?php echo ($data_auth[FORM_ACTION] == ACTION_ADD ? "validate=\"required[]\"" : ""); ?>/>
                                    <img id="iprev1" src="<?php echo (!isset($data_sel[LIST_DATA])) || $data_sel[LIST_DATA][0]->img == '' ?  IMG_BLANK_100 :  SLIDE_UPLOAD_IMAGE . $data_sel[LIST_DATA][0]->img; ?>" height="200" style="max-width:400px"/>                                     
                                </div>
                                <div class="row">   
                                    <div class="form-group">                                    
                                        <label class ="control-label col-md-1">Link </label>
                                        <div class="col-md-8">
                                            <input class="form-control" validate="required[]" name ="img_url" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->img_url) : "" ); ?>" placeholder ="URL Link ...">                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">   
                                        <label class ="control-label col-md-1">Urutan </label>
                                        <div class="col-md-4">
                                            <input class="form-control auto_int" validate ="required[]" name ="order" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->order : 0 ); ?>" placeholder ="Urutan">
                                        </div>
                                        <div class ="col-md-3">
                                            <label><input type="checkbox" name ="active" <?php echo (!isset($data_sel[LIST_DATA]) ? "checked" : (($data_sel[LIST_DATA][0]->active == "1" OR $data_sel[LIST_DATA][0]->active == "on") ? "checked" : "")); ?> /> Aktif</label>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                        </div>
                    </div>
                    <div class ="form-group">
                        <?php
                        if ($data_auth[FORM_ACTION] == ACTION_VIEW) {
                            ?>
                            <div class ="col-md-6"><?php echo get_back_button(); ?> </div>
                            <?php
                        } else {
                            if ($data_auth[FORM_ACTION] == ACTION_ADD) {
                                ?>
                                <div class ="col-md-6"><?php echo get_save_add_button(); ?> </div>
                            <?php } if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                                <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                            <?php } ?>
                            <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                        <?php } ?>
                    </div>
                </section>
            </div>
        </form>
    </div>
    <script type="text/javascript">
            function previewimg(thisval, idprev) {
                if (thisval.files && thisval.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#' + idprev + '').attr('src', e.target.result).height(200);
                    };
                    reader.readAsDataURL(thisval.files[0]);
                }
            }
    </script>
<?php } else { ?>  
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <?php require_once get_include_page_list_admin_content_header(); ?>
            <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>                                          
                        <th column="seq">Pengajuan #</th>
                        <th column="order">Urutan </th>
                        <th column="active"> Aktif </th>
                        <th column="status"> Status </th>
                        <th column="created_by"> <?php echo TH_CREATED_BY; ?> </th>
                        <th column="created_date"> <?php echo TH_CREATED_DATE; ?> </th>
                        <th column="modified_by"> <?php echo TH_MODIFIED_BY; ?> </th>
                        <th column="modified_date"> <?php echo TH_MODIFIED_DATE; ?> </th>             
                    </tr>
                </thead>
            </table>    
        </div>
    </div>
    <?php
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>