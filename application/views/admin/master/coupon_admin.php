<?php
require_once VIEW_BASE_ADMIN;

require_once get_include_content_admin_top_page_navigation();

$tablesort = 'desc';
?>

<link rel="stylesheet" type="text/css" href="<?php echo get_css_url() ?>daterangepicker-bs3.css" />
<script type="text/javascript" src="<?php echo get_js_url() ?>moment.min.js"></script>
<script type="text/javascript" src="<?php echo get_js_url() ?>daterangepicker.js"></script>

<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo $data_auth[FORM_ACTION_TITLE] . $data_auth[FORM_AUTH][FORM_TITLE] ?></h3>
        </div>
        <form class ="form-horizontal" id="frmMain" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
            <div class="box-body">
                <section class="col-md-6">
                    <?php echo get_csrf_admin_token(); ?>
                    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                        <input class="form-control" name ="seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->seq; ?>">
                    <?php } ?>
                    <div class ="form-group">     
                        <label class ="control-label col-md-3">Kode Kupon </label>
                        <div class ="col-md-9">
                            <input class="form-control" name="coupon_code" validate="required[]" type="text" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->coupon_code : "") ?>">
                        </div>
                    </div>
                    <div class ="form-group">     
                        <label class ="control-label col-md-3">Nama Kupon </label>
                        <div class ="col-md-9">
                            <input class="form-control" name="coupon_name" validate="required[]" type="text" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->coupon_name : "") ?>">
                        </div>
                    </div>
                    <div class ="form-group">   
                        <label class="control-label col-md-3">Periode Kupon </label>
                        <div class ="col-md-9">
                            <input class="form-control" date_type="date" id ="date_from" validate ="required[]" name ="date_from" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? date("d-M-Y", strtotime($data_sel[LIST_DATA][0]->coupon_period_from)) . ' - ' . date("d-M-Y", strtotime($data_sel[LIST_DATA][0]->coupon_period_to)) : "" ); ?>" readonly>
                        </div>
                    </div>
                    <div class ="form-group">   
                        <label class="control-label col-md-3">Batas Pemakaian (Hari)</label>
                        <div class="col-md-9">
                            <input class="form-control auto_int" name="coupon_limit" validate="required[]" type="text" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->coupon_limit : "") ?>">
                        </div>
                    </div>
                    <div class ="form-group">   
                        <label class="control-label col-md-3">Nominal</label>
                        <div class="col-md-9">
                            <input class="form-control auto_int" name="nominal" validate="required[]" type="text" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->nominal : "") ?>">
                        </div>
                    </div>
                    <div class ="form-group">
                        <?php
                        if ($data_auth[FORM_ACTION] == ACTION_VIEW) {
                            ?>
                            <div class ="col-md-6"><?php echo get_back_button(); ?> </div>
                            <?php
                        } else {
                            if ($data_auth[FORM_ACTION] == ACTION_ADD) {
                                ?>
                                <div class ="col-md-6"><?php echo get_save_add_button(); ?> </div>
                            <?php } if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                                <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                            <?php } ?>
                            <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                        <?php } ?>
                    </div>
                </section>
            </div>
        </form>
    </div>
    <script type="text/javascript">
            $('input[date_type="date"]').daterangepicker({
                format: 'DD-MMM-YYYY',
                showDropdowns: true
            });
    </script>
<?php } else { ?>    
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <?php require_once get_include_page_list_admin_content_header(); ?>
            <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>       
                        <th column="coupon_period_from"> Periode Awal </th>
                        <th column="coupon_period_to"> Periode Akhir </th>
                        <th column="coupon_code">Kode Kupon</th>
                        <th column="coupon_name">Nama Kupon</th>
                        <th column="coupon_limit">Batas Pemakaian (Hari)</th>
                        <th column="nominal">Nominal</th>
                        <th column="status"> Status </th>
                        <th column="detail"> Detil </th>
                        <th column="transaction">Transaksi</th>
                        <th column="created_by"> <?php echo TH_CREATED_BY; ?> </th>
                        <th column="created_date"> <?php echo TH_CREATED_DATE; ?> </th>
                        <th column="modified_by"> <?php echo TH_MODIFIED_BY; ?> </th>
                        <th column="modified_date"> <?php echo TH_MODIFIED_DATE; ?> </th>
                    </tr>
                </thead>
            </table>  
        </div>
    </div>
    <?php
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>