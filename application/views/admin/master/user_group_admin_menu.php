<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
?>

<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo $data_auth[FORM_ACTION_TITLE] . $data_auth[FORM_AUTH][FORM_TITLE] ?></h3>
        </div>
        <form class ="form-horizontal" id="frmMain" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
            <div class="box-body">
                <div class="col-md-6" id="tree_menu" style="overflow-y:auto;height:435px;border: 1px solid black;">
                    <?php require_once get_component_url() . "treeview/tree_view_menu.php" ?>
                </div>
                <section class="col-md-6">
                    <?php echo get_csrf_admin_token(); ?>
                    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                        <input name ="old_menu_cd"  type="hidden"  value ="<?php echo $data_sel[LIST_DATA][0]->menu_cd; ?>"></input>
                    <?php } ?>
                    <input name ="menu_cd" id ="menu_cd" type="hidden" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->menu_cd : "" ); ?>"></input>
                    <input name ="seq" type="hidden" value ="<?php echo (isset($data_sel[LIST_DATA][0]->seq) ? $data_sel[LIST_DATA][0]->seq : $data_head[LIST_DATA][0]->seq); ?>"></input>
                    <div class ="form-group">
                        <label class ="control-label col-md-3">Modul</label>
                        <div class ="col-md-9" >
                            <?php require_once get_component_url() . "dropdown/com_drop_menu_module.php"; ?>
                        </div>
                    </div>
                    <div class ="form-group">
                        <label class ="control-label col-md-3">User Group</label>
                        <div class ="col-md-9">
                            <input class="form-control" name ="name" type="input"  value ="<?php echo (isset($data_sel[LIST_DATA][0]->name) ? get_display_value($data_sel[LIST_DATA][0]->name) : get_display_value($data_head[LIST_DATA][0]->name)); ?>" readonly></input>
                        </div>
                    </div>
                    <div class ="form-group">
                        <label class ="control-label col-md-3">Menu</label>
                        <div class ="col-md-9">
                            <input class="form-control" id ="menu_name" name ="menu_name" type="input" value ="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->menu_name) : "") ?>" readonly></input>
                        </div>
                    </div>                        
                    <div class ="form-group">
                        <div class ="col-md-offset-3 col-md-9">
                            <label> <input type="checkbox" id="chk_view" name ="can_view" <?php echo (!isset($data_sel[LIST_DATA]) ? "checked" : (($data_sel[LIST_DATA][0]->can_view == "1" OR $data_sel[LIST_DATA][0]->can_view == "on") ? "checked" : "")); ?> /> Lihat</label>
                        </div>
                    </div>
                    <div class ="form-group">
                        <div class ="col-md-offset-3 col-md-9">
                            <label> <input type="checkbox" id="chk_add" name ="can_add" <?php echo (!isset($data_sel[LIST_DATA]) ? "checked" : (($data_sel[LIST_DATA][0]->can_add == "1" OR $data_sel[LIST_DATA][0]->can_add == "on") ? "checked" : "")); ?> /> Tambah</label>
                        </div>
                    </div>
                    <div class ="form-group">
                        <div class ="col-md-offset-3 col-md-9">
                            <label> <input type="checkbox" id="chk_edit" name ="can_edit" <?php echo (!isset($data_sel[LIST_DATA]) ? "checked" : (($data_sel[LIST_DATA][0]->can_edit == "1" OR $data_sel[LIST_DATA][0]->can_edit == "on") ? "checked" : "")); ?> /> Ubah</label>
                        </div>
                    </div>
                    <div class ="form-group">
                        <div class ="col-md-offset-3 col-md-9">
                            <label> <input type="checkbox" id="chk_delete" name ="can_delete" <?php echo (!isset($data_sel[LIST_DATA]) ? "checked" : (($data_sel[LIST_DATA][0]->can_delete == "1" OR $data_sel[LIST_DATA][0]->can_delete == "on") ? "checked" : "")); ?> /> Hapus</label>
                        </div>
                    </div>
                    <div class ="form-group">
                        <div class ="col-md-offset-3 col-md-9">
                            <label> <input type="checkbox" id="chk_print" name ="can_print" <?php echo (!isset($data_sel[LIST_DATA]) ? "checked" : (($data_sel[LIST_DATA][0]->can_print == "1" OR $data_sel[LIST_DATA][0]->can_print == "on") ? "checked" : "")); ?> /> Cetak</label>
                        </div>
                    </div>
                    <div class ="form-group">
                        <div class ="col-md-offset-3 col-md-9">
                            <label> <input type="checkbox" id="chk_auth" name ="can_auth" <?php echo (!isset($data_sel[LIST_DATA]) ? "checked" : (($data_sel[LIST_DATA][0]->can_auth == "1" OR $data_sel[LIST_DATA][0]->can_auth == "on") ? "checked" : "")); ?> /> Akses Khusus</label>
                        </div>
                    </div>                        
                    <div class ="form-group">
                        <?php
                        if ($data_auth[FORM_ACTION] != ACTION_VIEW) {
                            if ($data_auth[FORM_ACTION] == ACTION_ADD) {
                                ?>
                                <div class ="col-md-6"><?php echo get_save_add_button(); ?> </div>
                            <?php } if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                                <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                            <?php } ?>
                            <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                        <?php } ?>
                    </div>
                </section>
            </div>
        </form>
    </div>
    <script type="text/javascript">
            function pilihmenu(values) {
                var res = values.split("~");
                $("#menu_cd").val(res[0]);
                $("#menu_name").val(res[1]);
    <?php if (($data_auth[FORM_ACTION] == ACTION_EDIT) || ($data_auth[FORM_ACTION] == ACTION_VIEW)) { ?>
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "<?php echo get_base_url() . $data_auth[FORM_URL] ?>",
                        data: {
                            btnAdditional: true,
                            task: "<?php echo TASK_MENU_CHANGE; ?>",
                            module_seq: $('#drop_module').val(),
                            menu_cd: res[0]
                        },
                        success: function(data) {
                            if(isSessionExpired(data)){
                                response_object = json_decode(data);
                                url = response_object.url;
                                location.href = url;
                            }else{
                                $('#chk_view').prop('checked', (data === null || data[0].can_view == '0') ? false : true);
                                $('#chk_add').prop('checked', (data === null || data[0].can_add == '0') ? false : true);
                                $('#chk_edit').prop('checked', (data === null || data[0].can_edit == '0') ? false : true);
                                $('#chk_delete').prop('checked', (data === null || data[0].can_delete == '0') ? false : true);
                                $('#chk_print').prop('checked', (data === null || data[0].can_print == '0') ? false : true);
                                $('#chk_auth').prop('checked', (data === null || data[0].can_auth == '0') ? false : true);
                            }
                        },
                        error: function(request, error) {
                            alert(error_response(request.status));
                        }
                    });
    <?php } ?>
            }
            ;
            $("#drop_module").change(function() {
                $.ajax({
                    url: "<?php echo get_base_url() . $data_auth[FORM_URL] ?>",
                    data: {
                        btnAdditional: true,
                        module_seq: $(this).val()
                    },
                    type: "POST",
                    success: function(data) {
                        if(isSessionExpired(data)){
                            response_object = json_decode(data);
                            url = response_object.url;
                            location.href = url;
                        }else{
                            $('#tree_menu').html(data);
                        }
                    },
                    error: function(request, error) {
                        alert(error_response(request.status));
                    }
                });
            }).change();
    </script>
<?php } else { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">

            <form class ="form-horizontal col-md-12">
                <section class="col-md-8">
                    <div class ="form-group">
                        <label class ="control-label col-md-3">Nama Golongan User</label>
                        <div class ="col-md-9">
                            <input class="form-control" value="<?php echo (isset($data_head[LIST_DATA]) ? $data_head[LIST_DATA][0]->name : ""); ?>" readonly>
                        </div>
                    </div>
                    <div class ="form-group">
                        <div class ="col-md-offset-3 col-md-9">
                            <label> <input type="checkbox" name ="active" <?php echo ($data_head[LIST_DATA][0]->active == "1" ? "checked" : ""); ?> disabled/> Aktif</label>
                        </div>
                    </div>
                </section>     
            </form>
            <div class="box-tools pull-left">
                <?php require_once get_include_page_list_admin_content_header(); ?>
            </div>
            <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">                
                <thead>
                    <tr>
                        <th column="menu_cd">Kode Menu</th>
                        <th column="name">Nama Menu</th>
                        <th column="can_view">Lihat</th>
                        <th column="can_add">Tambah</th>
                        <th column="can_edit">Ubah</th>
                        <th column="can_delete">Hapus</th>
                        <th column="can_print">Cetak</th>
                        <th column="can_auth">Akses Khusus</th>
                        <th column="active">Aktif</th>                        
                        <th column="created_by"> <?php echo TH_CREATED_BY; ?> </th>
                        <th column="created_date"> <?php echo TH_CREATED_DATE; ?> </th>
                        <th column="modified_by"> <?php echo TH_MODIFIED_BY; ?> </th>
                        <th column="modified_date"> <?php echo TH_MODIFIED_DATE; ?> </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <?php
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>