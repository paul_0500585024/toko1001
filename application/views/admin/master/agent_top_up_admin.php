<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
$tablesort = 'desc';
$order_column = '3';
?>

<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo $data_auth[FORM_ACTION_TITLE] . $data_auth[FORM_AUTH][FORM_TITLE] ?></h3>
        </div>
        <form class="form-horizontal" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
            <div class="box-body">
                <div class="col-md-6">
                    <?php echo get_csrf_admin_token(); ?>
                    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                        <input class="form-control" name ="seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->seq; ?>">
                        <input class="form-control" name ="agent_seq_old" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->agent_seq; ?>">
                    <?php } ?>
                    <div class ="form-group">
                        <label class ="control-label col-md-3">Nomor Top Up</label>
                        <div class ="col-md-9">
                            <input class="form-control" name="trx_no" type="text" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->trx_no : "Auto") ?>" readonly>
                        </div>
                    </div>
                    <div class ="form-group">
                        <label class ="control-label col-md-3">Email Agen</label>
                        <div class ="col-md-9">
                            <?php require_once get_component_url() . "dropdown/com_drop_agent.php" ?>
                        </div>
                    </div>
                    <div class ="form-group">
                        <label class ="control-label col-md-3">Nama Agen</label>
                        <div class ="col-md-9">
                            <input class="form-control" id="agent_name" name="agent_name" type="text" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->agent_name : "") ?>" disabled>
                        </div>
                    </div>
                    <div class ="form-group">
                        <label class ="control-label col-md-3">Tanggal Pengajuan</label>
                        <div class ="col-md-9">
                            <input class="form-control" name="trx_date" id="trx_date" type="text" value="<?php echo (isset($data_sel[LIST_DATA]) ? cdate($data_sel[LIST_DATA][0]->trx_date) : date("d-M-Y")) ?>" readonly>
                        </div>
                    </div>

                    <div class ="form-group">
                        <label class ="control-label col-md-3">Besaran Top Up</label>
                        <div class ="col-md-9">
                            <input class="form-control" id="deposit_trx_amt" name="deposit_trx_amt" type="text" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->deposit_trx_amt : "0") ?>" disabled>
                        </div>
                    </div>

                    </br>
                    <div>
                        <div class="row">
                            <table class="diplay table table-bordered table-striped">
                                <tbody>
                                    <tr>
                                        <th> Info Hasil Komfirmasi </th>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr valign="top">
                                        <td> Jumlah Pembayaran  : <?php echo (isset($data_sel[LIST_DATA]) ? cnum($data_sel[LIST_DATA][0]->deposit_trx_amt) : "") ?></td>
                                    </tr>
                                    <tr valign="top">
                                        <td> Tanggal Pembayaran : <?php echo (isset($data_sel[LIST_DATA]) ? cdate($data_sel[LIST_DATA][0]->conf_topup_date_agent) : "") ?></td>
                                    </tr>
                                    <tr valign="top">
                                        <td> Topup Via : <?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->pg_name : "") ?></td>
                                    </tr>
                                    <tr valign="top">
                                        <?php
                                        if (($data_sel[LIST_DATA][0]->logo_img == "")) {
                                            $image_1 = get_base_url() . IMG_BLANK_100;
                                        } else {
                                            $image_1 = get_base_url() . BANK_UPLOAD_IMAGE . $data_sel[LIST_DATA][0]->logo_img;
                                        }
                                        if (($data_sel[LIST_DATA][0]->conf_topup_file_agent == "")) {
                                            $image_2 = get_base_url() . IMG_BLANK_100;
                                        } else {
                                            $image_2 = get_base_url() . AGENT_UPLOAD_IMAGE_TOPUP_DEPOSIT . $data_sel[LIST_DATA][0]->trx_no . "/" . $data_sel[LIST_DATA][0]->conf_topup_file_agent;
                                        }
                                        ?>
                                        <td>
                                            <img id="iprev" src="<?php echo $image_1; ?>">
                                            <br>A/C :<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->bank_acct_no : "") ?> <br>
                                            A/N :<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->bank_acct_name : "") ?>
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <td>
                                            bukti Topup :<br>
                                            <a href="<?php echo $image_2 ?>" target="_blank"><img width="150px" height="150px" src="<?php echo $image_2 ?>"></a>  
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    </br>

                    <div class ="form-group">
                        <?php if ($data_auth[FORM_ACTION] == ACTION_VIEW) {
                            ?>
                            <div class ="col-md-6"><?php echo get_back_button(); ?> </div>
                            <?php
                        } else {
                            if ($data_auth[FORM_ACTION] == ACTION_ADD) {
                                ?>
                                <div class ="col-md-6"><?php echo get_save_add_button(); ?> </div>
                                <?php
                            }
                            if ($data_auth[FORM_ACTION] == ACTION_EDIT) {
                                ?>
                                <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                            <?php } ?>
                            <div class ="col-md-6"><?php echo get_back_button(); ?> </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <script type="text/javascript">
        <!--
    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT || $data_auth[FORM_ACTION] == ACTION_ADD) { ?>
            $('#deposit_trx_amt').prop("disabled", false);
    <?php
    }
    if ($data_auth[FORM_ACTION] == ACTION_EDIT || $data_auth[FORM_ACTION] == ACTION_VIEW)
        echo "$('#agent_seq').prop('disabled', true);";
    ?>
        //-->
    </script>
    <?php
} else {
    ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
    <?php require_once get_include_page_list_admin_content_header(); ?>
            <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th column="agent_email"> Email Agen </th>
                        <th column="deposit_amt"> Saldo </th>
                        <th column="trx_no"> Nomor Pengajuan </th>
                        <th column="trx_date"> Tanggal Pengajuan </th>
                        <th column="deposit_trx_amt"> Tarik Dana </th>
                        <th column="status"> Status </th>
                        <th column="refund_date"> Tanggal Top Up </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <?php
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>