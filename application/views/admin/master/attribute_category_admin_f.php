<?php
require_once VIEW_BASE_ADMIN;
?>

<form id ="frmSearch"  url= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
    <div class="form-group">
        <label>Nama Kategori</label>
        <?php require_once get_component_url() . "/dropdown/com_drop_product_category.php" ?>
    </div>
    <div class="form-group">
        <label>Nama Atribut</label>
        <?php require_once get_component_url() . "/dropdown/com_drop_attribute.php" ?>  
    </div>
    <div class="form-group">
        <?php echo get_search_button(); ?>
    </div>
</form>