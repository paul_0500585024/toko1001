<?php
require_once VIEW_BASE_ADMIN;
?>

<form id ="frmSearch"  url= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
    <div class="form-group">
        <label>Status</label>
        <select class="form-control select2" name="status" id="status">
            <option value="">Semua</option>
	    <?php
	    echo combostatus(json_decode(STATUS_NRAV), "N");
	    ?>
        </select>
    </div>
    <div class="form-group">
	<?php echo get_search_button(); ?>
    </div>
</form>