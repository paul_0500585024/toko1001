<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
?>

<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo $data_auth[FORM_AUTH][FORM_TITLE] ?></h3>
    </div>
    <div class="box-body">
        <form class="form-horizontal" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>" id="frmMain" onsubmit ="return validate_form();">
            <section class="col-md-12">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#t_1" aria-controls="t_1" role="tab" data-toggle="tab">Info Login</a></li>
                    <li role="presentation"><a href="#t_2" aria-controls="t_2" role="tab" data-toggle="tab">Info Alamat</a></li>
                    <li role="presentation"><a href="#t_3" aria-controls="t_6" role="tab" data-toggle="tab">Info Bank</a></li>
                    <li role="presentation"><a href="#t_4" aria-controls="t_3" role="tab" data-toggle="tab">Info Ekspedisi</a></li>
                    <li role="presentation"><a href="#t_5" aria-controls="t_4" role="tab" data-toggle="tab">Info Retur</a></li>
                    <li role="presentation"><a href="#t_6" aria-controls="t_5" role="tab" data-toggle="tab">Biaya Merchant</a></li>
                </ul>
                <!--Tab Info Login (1)-->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="t_1"><br />
                        <section class="col-md-12">
                            <div class="col-md-6">
                                <div class ="form-group">
                                    <label class ="control-label col-md-3">Email</label>
                                    <div class ="col-md-9">
                                        <input class="form-control" type="text" value ="<?php echo get_display_value($data_sel[LIST_DATA][0][0]->email) ?>" readonly="">
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!--Tab Info Alamat (2)-->
                    <div role="tabpanel" class="tab-pane" id="t_2"><br />
                        <section class="col-md-12">
                            <div class="col-md-6">
                                <div class ="form-group">
                                    <label class ="control-label col-md-3">Nama</label>
                                    <div class ="col-md-9">
                                        <input class="form-control" type="text" value ="<?php echo get_display_value($data_sel[LIST_DATA][0][0]->name) ?>" readonly="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class ="form-group">
                                    <label class ="control-label col-md-2">Alamat</label>
                                    <div class ="col-md-6">
                                        <textarea class="form-control" rows="2" style="overflow:auto;resize:none" readonly=""><?php echo get_display_value($data_sel[LIST_DATA][0][0]->address) ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class ="control-label col-md-3">Propinsi</label>
                                    <div class ="col-md-9">
                                        <input class="form-control" type="text" value ="<?php echo get_display_value($data_sel[LIST_DATA][0][0]->province_name) ?>" readonly="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class ="form-group">
                                    <label class ="control-label col-md-3">Kota</label>
                                    <div class ="col-md-9">
                                        <input class="form-control" type="text" value ="<?php echo get_display_value($data_sel[LIST_DATA][0][0]->city_name) ?>" readonly="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class ="form-group">
                                    <label class ="control-label col-md-3">Kecamatan</label>
                                    <div class ="col-md-9">
                                        <input class="form-control" type="text" value ="<?php echo get_display_value($data_sel[LIST_DATA][0][0]->district_name) ?>" readonly="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class ="form-group">
                                    <label class ="control-label col-md-3">Kode Pos</label>
                                    <div class ="col-md-9">
                                        <input class="form-control" type="text" value ="<?php echo get_display_value($data_sel[LIST_DATA][0][0]->zip_code) ?>" readonly="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class ="form-group">
                                    <label class ="control-label col-md-3">No. Telp</label>
                                    <div class ="col-md-9">
                                        <input class="form-control" type="text" value ="<?php echo get_display_value($data_sel[LIST_DATA][0][0]->phone_no) ?>" readonly="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class ="form-group">
                                    <label class ="control-label col-md-3">No. Fax</label>
                                    <div class ="col-md-9">
                                        <input class="form-control" type="text" value ="<?php echo get_display_value($data_sel[LIST_DATA][0][0]->fax_no) ?>" readonly="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class ="form-group">
                                    <label class ="control-label col-md-3">Nama PIC</label>
                                    <div class ="col-md-9">
                                        <input class="form-control" type="text" value ="<?php echo get_display_value($data_sel[LIST_DATA][0][0]->pic1_name) ?>" readonly="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class ="form-group">
                                    <label class ="control-label col-md-3">Telp PIC</label>
                                    <div class ="col-md-9">
                                        <input class="form-control" type="text" value ="<?php echo get_display_value($data_sel[LIST_DATA][0][0]->pic1_phone_no) ?>" readonly="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class ="form-group">
                                    <label class ="control-label col-md-3">Nama Finance</label>
                                    <div class ="col-md-9">
                                        <input class="form-control" type="text" value ="<?php echo get_display_value($data_sel[LIST_DATA][0][0]->pic2_name) ?>" readonly="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class ="form-group">
                                    <label class ="control-label col-md-3">Telp Finance</label>
                                    <div class ="col-md-9">
                                        <input class="form-control" type="text" value ="<?php echo get_display_value($data_sel[LIST_DATA][0][0]->pic2_phone_no) ?>" readonly="">
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!--Tab Info Bank (3)-->
                    <div role="tabpanel" class="tab-pane" id="t_3"><br />
                        <div class="col-md-6">
                            <div class ="form-group">
                                <label class ="control-label col-md-3">Nama Bank</label>
                                <div class ="col-md-9">
                                    <input class="form-control" type="text" value ="<?php echo get_display_value($data_sel[LIST_DATA][0][0]->bank_name) ?>" readonly="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class ="form-group">
                                <label class ="control-label col-md-3">Cabang</label>
                                <div class ="col-md-9">
                                    <input class="form-control" type="text" value ="<?php echo get_display_value($data_sel[LIST_DATA][0][0]->bank_branch_name) ?>" readonly="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class ="form-group">
                                <label class ="control-label col-md-3">No. Rekening</label>
                                <div class ="col-md-9">
                                    <input class="form-control" type="text" value ="<?php echo get_display_value($data_sel[LIST_DATA][0][0]->bank_acct_no) ?>" readonly="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class ="form-group">
                                <label class ="control-label col-md-3">Rekening A/N</label>
                                <div class ="col-md-9">
                                    <input class="form-control" type="text" value ="<?php echo get_display_value($data_sel[LIST_DATA][0][0]->bank_acct_name) ?>" readonly="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Tab Info Ekspedisi (4)-->
                    <div role="tabpanel" class="tab-pane" id="t_4"><br />
                        <div class="col-md-6">
                            <div class ="form-group">
                                <label class ="control-label col-md-3">Ekspedisi</label>
                                <div class ="col-md-9">
                                    <input class="form-control" type="text" value ="<?php echo get_display_value($data_sel[LIST_DATA][0][0]->exp_name) ?>" readonly="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class ="form-group">
                                <label class ="control-label col-md-2">Alamat Pengambilan</label>
                                <div class ="col-md-6">
                                    <textarea class="form-control" row="2" style="overflow:auto;resize:none" type="text" readonly=""><?php echo get_display_value($data_sel[LIST_DATA][0][0]->pickup_addr) ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class ="form-group">
                                <label class ="control-label col-md-3">Propinsi</label>
                                <div class ="col-md-9">
                                    <input class="form-control" type="text" value ="<?php echo get_display_value($data_sel[LIST_DATA][0][0]->pickup_province_name) ?>" readonly="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class ="form-group">
                                <label class ="control-label col-md-3">Kota</label>
                                <div class ="col-md-9">
                                    <input class="form-control" type="text" value ="<?php echo get_display_value($data_sel[LIST_DATA][0][0]->pickup_city_name) ?>" readonly="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class ="form-group">
                                <label class ="control-label col-md-3">Kecamatan</label>
                                <div class ="col-md-9">
                                    <input class="form-control" type="text" value ="<?php echo get_display_value($data_sel[LIST_DATA][0][0]->pickup_district_name) ?>" readonly="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class ="form-group">
                                <label class ="control-label col-md-3">Kode Pos</label>
                                <div class ="col-md-9">
                                    <input class="form-control" type="text" value ="<?php echo get_display_value($data_sel[LIST_DATA][0][0]->pickup_zip_code) ?>" readonly="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Tab Info Retur (5)-->
                    <div role="tabpanel" class="tab-pane" id="t_5"><br />
                        <div class="col-md-9">
                            <div class ="form-group">
                                <label class ="control-label col-md-2">Alamat Pengembalian</label>
                                <div class ="col-md-6">
                                    <textarea class="form-control" row="2" style="overflow:auto;resize:none" type="text" readonly=""><?php echo get_display_value($data_sel[LIST_DATA][0][0]->return_addr) ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class ="form-group">
                                <label class ="control-label col-md-3">Propinsi</label>
                                <div class ="col-md-9">
                                    <input class="form-control" type="text" value ="<?php echo get_display_value($data_sel[LIST_DATA][0][0]->return_province_name) ?>" readonly="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class ="form-group">
                                <label class ="control-label col-md-3">Kota</label>
                                <div class ="col-md-9">
                                    <input class="form-control" type="text" value ="<?php echo get_display_value($data_sel[LIST_DATA][0][0]->return_city_name) ?>" readonly="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class ="form-group">
                                <label class ="control-label col-md-3">Kecamatan</label>
                                <div class ="col-md-9">
                                    <input class="form-control" type="text" value ="<?php echo get_display_value($data_sel[LIST_DATA][0][0]->return_district_name) ?>" readonly="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class ="form-group">
                                <label class ="control-label col-md-3">Kode Pos</label>
                                <div class ="col-md-9">
                                    <input class="form-control" type="text" value ="<?php echo get_display_value($data_sel[LIST_DATA][0][0]->return_zip_code) ?>" readonly="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Tab Biaya Merchant (6)-->
                    <div role="tabpanel" class="tab-pane" id="t_6"><br />
                        <section class="col-md-12">
                            <?php echo get_csrf_admin_token(); ?>
                            <div class="form-group">
                                <label class ="col-md-4">Biaya Ekspedisi Tanggungan Merchant (%) *</label>
                                <div class ="col-md-8">
                                    <input class="form-control auto_int" validate ="required[]" data-v-max="100" id="exp_fee_percent" name="exp_fee_percent" data-a-pad="false" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0][0]->exp_fee_percent : "100") ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class ="col-md-4">Biaya Asuransi Tanggungan Merchant (%) *</label>
                                <div class ="col-md-8">
                                    <input class="form-control auto_int" validate ="required[]" data-v-max="100" id="ins_fee_percent" name="ins_fee_percent" data-a-pad="false" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0][0]->ins_fee_percent : "100") ?>">
                                </div>
                            </div>
                            <div class ="form-group">
                                <label class ="col-md-4">Keterangan</label>
                                <div class ="col-md-8">
                                    <input class="form-control" id="notes" name="notes" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0][0]->notes) : "") ?>">
                                </div>
                            </div>
                            <div class ="form-group">
                                <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                                <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                            </div>
                        </section>
                    </div>
<!--                    &nbsp;
                    <div class ="form-group">
                        <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                        <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                    </div>-->
                </div>
            </section>
        </form>
    </div>
</div>

<?php
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>