<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
?>
<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo ($data_auth[FORM_ACTION] == ACTION_ADD ? get_title_add($data_auth[FORM_AUTH][FORM_TITLE]) : get_title_edit($data_auth[FORM_AUTH][FORM_TITLE])); ?></h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <form id ="frmMain" method="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>" class="form-horizontal" onsubmit ="return validate_form();">
                        <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                            <input name ="seq" type="hidden" value ="<?php if (isset($data_sel[LIST_DATA][0])) echo $data_sel[LIST_DATA][0]->seq; ?>">
                        <?php } ?>
                        <input id="level" name ="level" type="hidden" value ="<?php
                        if (isset($data_sel[LIST_DATA][0])) {
                            echo $data_sel[LIST_DATA][0]->level;
                        } else {
                            echo "1";
                        }
                        ?>">
                        <input id="parent_seq" name="parent_seq" type="hidden" value ="<?php
                        if (isset($data_sel[LIST_DATA][0])) {
                            echo $data_sel[LIST_DATA][0]->parent_seq;
                        } else {
                            echo "0";
                        }
                        ?>">
                               <?php echo get_csrf_admin_token(); ?>
                        <div class="col-md-6" style="overflow-y:auto;height:500px;" id="treviewdiv">
                            <?php require_once get_component_url() . "treeview/tree_view_category.php"; ?>
                        </div>
                        <div class="col-md-6">
                            <div class ="form-group">
                                <label class ="control-label col-md-4">Induk Kategori <label style="color: red">*</label></label>
                                <div class ="col-md-8">
                                    <input class="form-control" name ="parent_name" id="parent_name" type="text" readonly value="<?php
                                    if (isset($data_sel[LIST_DATA][0])) {
                                        echo (($data_sel[LIST_DATA][0]->parent_seq == 0) ? "ROOT" : $data_sel[LIST_DATA][0]->parent_name);
                                    } else {
                                        echo "ROOT";
                                    }
                                    ?>">
                                </div>
                            </div>
                            <div class ="form-group">
                                <label class ="control-label col-md-4">Nama Kategori <label style="color: red">*</label></label>
                                <div class ="col-md-8">
                                    <input class="form-control" name ="name" maxlength="100" type="text" validate ="required[]" value ="<?php if (isset($data_sel[LIST_DATA][0])) echo get_display_value($data_sel[LIST_DATA][0]->name); ?>">
                                </div>
                            </div>
                            <div class ="form-group">
                                <label class ="control-label col-md-4">Urutan <label style="color: red">*</label></label>
                                <div class ="col-md-8">
                                    <input class="form-control auto_int" maxlength="10" name ="order" type="text" validate ="required[]" value ="<?php
                                    if (isset($data_sel[LIST_DATA][0])) {
                                        echo $data_sel[LIST_DATA][0]->order;
                                    } else {
                                        echo "0";
                                    }
                                    ?>">
                                </div>
                            </div>
                            <div class ="form-group">
                                <div class ="col-md-offset-4 col-md-9">
                                    <label> <input type="checkbox" name ="active" <?php echo (!isset($data_sel[LIST_DATA]) ? "checked" : (($data_sel[LIST_DATA][0]->active == "1" OR $data_sel[LIST_DATA][0]->active == "on") ? "checked" : "")); ?> /> Aktif</label>
                                </div>
                            </div>
                            <div id="levelduap" style="display:none;">
                                <div class ="form-group">
                                    <div class ="col-md-offset-3 col-md-9">
                                        <label> <input type="checkbox" name ="include_ins"<?php if (isset($data_sel[LIST_DATA][0])) if ($data_sel[LIST_DATA][0]->include_ins == "1" or $data_sel[LIST_DATA][0]->include_ins == "on") echo " checked";  ?> /> Asuransi</label>
                                    </div>
                                </div>
                                <div class ="form-group">
                                    <label class ="control-label col-md-3">Biaya Ekspedisi</label>
                                    <div class ="col-md-9">
                                        <input type="radio" name="exp_method" value="N"<?php
                                        if (isset($data_sel[LIST_DATA][0])) {
                                            if ($data_sel[LIST_DATA][0]->exp_method == "N")
                                                echo " checked";
                                        }else {
                                            echo " checked";
                                        }
                                        ?>> None
                                        <input type="radio" name="exp_method" value="W"<?php if (isset($data_sel[LIST_DATA][0])) if ($data_sel[LIST_DATA][0]->exp_method == "W") echo " checked";  ?>> Berat (kg)
                                        <input type="radio" name="exp_method" value="V"<?php if (isset($data_sel[LIST_DATA][0])) if ($data_sel[LIST_DATA][0]->exp_method == "V") echo " checked";  ?>> Volumetric (P x L x T)
                                    </div>
                                </div>

                                <div class ="form-group">
                                    <label class ="control-label col-md-3">Default Komisi (%)</label>
                                    <div class="col-sm-9">
                                        <input class="form-control auto" maxlength="5" validate ="required[]" name ="trx_fee_percent" type="text" value ="<?php
                                        if (isset($data_sel[LIST_DATA][0])) {
                                            echo $data_sel[LIST_DATA][0]->trx_fee_percent;
                                        } else {
                                            echo "0";
                                        }
                                        ?>">
                                    </div>
                                </div>
                                <div class ="form-group">
                                    <label class ="control-label col-md-3">Varian</label>
                                    <div class ="col-md-9">
                                        <div id="data">
                                            <?php include dirname(__FILE__) . '/../component/dropdown/com_drop_varian.php' ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!--                                                            <div class="box-footer">-->
                            <div class="row">
                                <?php
                                if ($data_auth[FORM_ACTION] == ACTION_VIEW) {
                                    ?>
                                    <div class ="col-md-6"><?php echo get_back_button(); ?> </div>
                                    <?php
                                } else {
                                    if ($data_auth[FORM_ACTION] == ACTION_ADD) {
                                        ?>
                                        <div class ="col-md-6"><?php echo get_save_add_button(); ?> </div>
                                    <?php } if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                                        <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                                    <?php } ?>
                                    <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                                <?php } ?>
                                <!--                                                        </div> /.box-footer -->
                            </div>
                        </div>
                    </form>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
    <div id="tes">
    </div>
    <script type="text/javascript">
    <?php
    if ($data_auth[FORM_ACTION] == ACTION_VIEW) {
        ?>
                            $('#treviewdiv a').click(function() {
                                return false
                            });
    <?php } ?>
                        function pilihmenu(values) {
                            var res = values.split("~");
                            $("#parent_seq").val(res[0]);
                            $("#level").val((parseInt(res[1]) + (1)));
                            $("#parent_name").val(res[2]);
                            if (res[1] == 1 && res[0] > 0) {
                                $("#levelduap").show("slow");
                                cvariant(res[3]);
                            } else {
                                $("#levelduap").hide("slow");
                            }
                        }

                        function cvariant(idseq, tipe) {
                            if (tipe === "undefined")
                                tipe = 0;
                            var url = "<?php echo site_url('admin/master/product_category'); ?>";
                            $.ajax({
                                url: url,
                                data: {
                                    btnAdditional: "true",
                                    stipe: tipe,
                                    variant_seq: idseq
                                },
                                type: "POST",
                                success: function(response) {
                                    if (isSessionExpired(response)) {
                                        response_object = json_decode(response);
                                        url = response_object.url;
                                        location.href = url;
                                    } else {
                                        $('#data').html(response);
                                    }
                                },
                                error: function(request, error) {
                                    alert(error_response(request.status));
                                }
                            });
                        }
                        $('.select2').select2();
    <?php
    if ($data_auth[FORM_ACTION] == ACTION_EDIT) {
//echo "cvariant('".$data_sel[LIST_DATA][0]->variant_seq."','1');";
    }
    ?>
    <?php
    if (isset($data_sel[LIST_DATA][0])) {
        if ($data_sel[LIST_DATA][0]->level != 2) {
            echo ' $(\'#levelduap\').hide();';
        } else {
            echo ' $(\'#levelduap\').show();';
        }
    } else {
        echo ' $(\'#levelduap\').hide();';
    }
    ?>
                        //-->
    </script>
<?php } else {
    ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <?php require_once get_include_page_list_admin_content_header(); ?>
            <?php //exit(); ?>
            <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th column="parent_name">Induk Kategori</th>
                        <th column="level">Level</th>
                        <th column="name">Nama Kategori</th>
                        <th column="trx_fee_percent">Komisi (%)</th>
                        <th column="active">Aktif</th>
                        <th column="created_by"> <?php echo TH_CREATED_BY; ?> </th>
                        <th column="created_date"> <?php echo TH_CREATED_DATE; ?> </th>
                        <th column="modified_by"> <?php echo TH_MODIFIED_BY; ?> </th>
                        <th column="modified_date"> <?php echo TH_MODIFIED_DATE; ?> </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <?php
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>
