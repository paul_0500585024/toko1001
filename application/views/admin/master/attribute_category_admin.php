<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
?>

<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo $data_auth[FORM_ACTION_TITLE] . $data_auth[FORM_AUTH][FORM_TITLE] ?></h3>
        </div>
        <form class ="form-horizontal" id='frmMain' onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
            <div class="box-body">
                <?php echo get_csrf_admin_token(); ?>
                <input class="form-control" name ="parent_seq" id="parent_seq" type="hidden" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->key : "" ); ?>">
                <div class="col-md-6" style="overflow-y:auto;height:405px;">
                    <?php require_once get_component_url() . "treeview/tree_view_category.php" ?>
                </div>
                <div class="col-md-6">
                    <div class ="form-group">
                        <label class ="control-label col-md-2" style="text-align: center">Kategori</label>
                        <div class ="col-md-10">
                            <input class="form-control" validate="required[]" name ="namaparent" id="namaparent" type="input" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->name : "" ); ?>"  readonly>
                        </div>
                    </div>
                </div>
                <div class="col-md-6" style="overflow-y:auto;height:300px;" id="tbl_detail">
                    <?php require_once get_component_url() . "checklist/check_attribute.php" ?>
                </div>
                &nbsp;
                <div class ="form-group">
                    <?php if ($data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
                        <div class ="col-md-3"><?php echo get_back_button(); ?> </div>
                        <?php
                    } else {
                        if ($data_auth[FORM_ACTION] == ACTION_ADD) {
                            ?>
                            <div class ="col-md-3"><?php echo get_save_add_button(); ?> </div>
                        <?php } ?>
                        <div class ="col-md-3"><?php echo get_cancel_button(); ?> </div>
                    <?php } ?>
                </div>
            </div>
        </form>
    </div>
<?php } else { ?>    
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <?php require_once get_include_page_list_admin_content_header(); ?>
            <table id="tbl" class="diplay table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th column="category_name"> Kategori </th>
                        <th column="name"> Atribut </th>
                        <th column="created_by"><?php echo TH_CREATED_BY; ?></th>
                        <th column="created_date"><?php echo TH_CREATED_DATE; ?></th>
                        <th column="modified_by"><?php echo TH_MODIFIED_BY; ?></th>
                        <th column="modified_date"><?php echo TH_MODIFIED_DATE; ?></th>
                    </tr>
                </thead>
            </table>    
        </div>
    </div>

    <?php
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>

<script type = "text/javascript">
            function pilihmenu(values) {
                var res = values.split("~");
                var url = $('#frmAdd').attr('action');
                if (res[2] == "ROOT") {
                    res[2] = "";
                    res[3] = 0
                }
                ;
                $("#parent_seq").val(res[0]);
                $("#level").val((parseInt(res[1]) + parseInt(1)));
                $("#namaparent").val(res[2]);
                $.ajax({
                    type: "post",
                    url: url,
                    data: {"btnAdditional": true, "seq": res[0], "parent_seq": res[3]},
                    success: function(response)
                    {
                        if(isSessionExpired(response)){
                            response_object = json_decode(response);
                            url = response_object.url;
                            location.href = url;
                        }else{
                            $('#tbl_detail').html(response);
                        }
                    },
                    error: function (request, error) {
                        alert(error_response(request.status));
                    }
                })
            }
    </script>

