<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
?>
<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo $data_auth[FORM_AUTH][FORM_TITLE] ?></h3>
    </div>
    <div class="box-body">
        <br>
        <div class="col-md-9">
            <div class ="form-group">
                <label class ="control-label col-md-2">Nama Group</label>
                <div class ="col-md-6">
                    <input type="text" name="" class="form-control" value="<?php echo $data_sel[LIST_DATA][0]->partner_group_name ?>" disabled/>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <form id="frmMain" method="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
        <?php echo get_csrf_admin_token(); ?>
        <input type="hidden" name="seq" value="<?php echo $data_sel[LIST_DATA][0]->partner_group_seq ?>"/>
        <div class="box-body">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border">Produk Category</legend>

                <div class="row">
                    <div class="col-md-3"><label class ="control-label col-md-12">Kategori Level 1</label></div>
                    <div class="col-md-3"><label class ="control-label col-md-12">Kategori Level 2</label></div>
                    <div class="col-md-3"><label class ="control-label col-md-12">Master Komisi (%)</label></div>
                    <div class="col-md-3"><label class ="control-label col-md-12">Komisi Merchant (%)</label></div>
                </div>

                <div class="col-xs-12">
                    <?php
                    if (isset($tree_menu)) {
                        if ($tree_menu != "") {
                            $header = "";
                            $i = 0;
                            $ret = '';
                            $show = "";
                            foreach ($tree_menu as $value) {
                                if ($header != $value->hname) {
                                    if ($i > 1)
                                        $ret.= '</div></div></div>';
                                    if ($show != "")
                                        $ret = str_replace('<div  id="sm' . $show . '" style="display:none;">', '<div  id="sm' . $show . '">', $ret);
                                    ;
                                    $show = "";
                                    $ret.= '<div class="row" style="border-top:1px solid #f0f0f0;padding:5px;"><div class="col-md-3">'
                                            . '<input type="checkbox" ' . (($value->mcatlvl1 != NULL) ? ' checked' : '') . ' name="hseq[]" value="' . $value->hseq . '" onclick="showhide(\'' . $value->hseq . '\')"> ' . $value->hname . '</div>'
                                            . '<div  id="sm' . $value->hseq . '" style="display:none;">
			<div class="col-md-9">';
                                    $header = $value->hname;
                                }
                                if ($value->mcatlvl1 !== NULL)
                                    $show = $value->hseq;
                                $ret.= '<div class="row"' . (fmod($i, 2) == 0 ? '' : ' style="background-color:#eee"') . '">
				    <div class="col-md-4"><input type="checkbox" ' . (($value->mcatlvl2 !== NULL) ? ' checked' : '') . ' name="lvlcat[]" value="' . $value->seq . '~' . $i . '~' . $value->hseq . '" onclick="sht(this.checked,\'lvlcat_' . $value->seq . '\')"> ' . $value->name . '</div>
				    <div class="col-md-4" align="center">' . $value->trx_fee_percent . '</div>
				    <div class="col-md-2" align="center"><input type="text" ' . (($value->mcatlvl2 !== NULL) ? '' : 'style="display : none ;"') . ' class="auto_dec" data-d-group="3" data-v-max="100" name="mfee[]" id="lvlcat_' . $value->seq . '" value="' . $value->mfee . '"></div>
				</div>';

                                $i++;
                            }
                            $ret = str_replace('<div  id="sm' . $show . '" style="display:none;">', '<div  id="sm' . $show . '">', $ret);
                            $ret.= '</div></div></div>';
                            echo $ret;
                        }
                    }
                    ?>
                </div>
            </fieldset>
            <div class ="col-md-6"><?php echo get_save_edit_button() ?> </div>
            <div class ="col-md-6"><?php echo get_cancel_link(get_base_url() . "admin/master/partner_group"); ?></div>
        </div>
    </form>
</div>
<script type="text/javascript">
    function showhide(idmenu) {
        $("#sm" + idmenu).toggle("slow");
    }
    function sht(nilai, idmenu) {
        if (nilai) {
            $("#" + idmenu).show("slow");
        } else {
            $("#" + idmenu).hide("slow");
        }
    }
</script>
<?php
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>