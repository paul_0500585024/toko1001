<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
?>

<?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
    <div class="box box-default">     
        <div class="box-header">
            <h3 class="box-title"><?php echo get_title_edit($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <form class ="form-horizontal" id="frmMain" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
                <?php echo get_csrf_admin_token(); ?>
                <section class="col-md-8">
                    <div class ="form-group">     
                        <label class ="control-label col-md-4">Ekspedisi</label>
                        <div class ="col-md-8">
                            <input class="form-control" name ="exp_seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->exp_seq; ?>"/>
                            <input class="form-control" name ="exp_name" type="input" value ="<?php echo get_display_value($data_sel[LIST_DATA][0]->exp_name); ?>" readonly=""/>
                        </div>
                    </div>
                    <div class ="form-group">     
                        <label class ="control-label col-md-4">Kecamatan</label>
                        <div class ="col-md-8">
                            <input class="form-control" name ="district_seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->district_seq; ?>"/>
                            <input class="form-control" name ="district_name" type="input" value ="<?php echo get_display_value($data_sel[LIST_DATA][0]->district_name); ?>" readonly=""/>
                        </div>
                    </div>
                    <div class ="form-group">     
                        <label class ="control-label col-md-4">Kecamatan Ekspedisi *</label>
                        <div class ="col-md-8">
                            <input class="form-control" validate="required[]" name ="exp_district_code" type="text" value ="<?php echo get_display_value($data_sel[LIST_DATA][0]->exp_district_code); ?>"/>
                        </div>
                    </div>
                    </br>
                    <div class ="form-group">
                        <?php
                        if ($data_auth[FORM_ACTION] != ACTION_VIEW) {
                            if ($data_auth[FORM_ACTION] == ACTION_ADD) {
                                ?>
                                <div class ="col-md-6"><?php echo get_save_add_button(); ?> </div>
                            <?php } if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                                <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                            <?php } ?>
                            <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                        <?php } ?>
                    </div>
                </section>
            </form>
        </div>
    </div>
<?php } else { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <?php
            require_once get_include_page_list_admin_content_header();
            if (!$data_auth[FORM_AUTH][FORM_AUTH_EDIT] === false) {
                ?>
                <section class ="col-md-12">
                    <form id = "frmAdd" class="form-horizontal" method = "post" action = "<?php echo get_base_url() . $data_auth[FORM_URL] ?>" enctype = "multipart/form-data">
                        <?php echo get_csrf_admin_token(); ?>
                        <div class ="form-group">
                            <div class="input-group col-md-5">
                                <span class="input-group-btn">
                                    <span class="btn btn-primary btn-file">
                                        Pilih File
                                        <input class='form-control' id="file_upload" name='userfile' type="file" onchange="set_upload(this.value)" accept=".csv">
                                    </span>
                                </span>
                                <input class="form-control" readonly="" id="upload_url" type="text">
                                <span class="input-group-btn">
                                    <button name ="<?php echo CONTROL_ADDITIONAL_NAME ?>" type = "submit" class = "btn btn-primary"><i class='fa fa-upload'></i> Unggah</button>
                                </span>
                            </div>
                        </div>
                    </form>
                </section>
            <?php } ?>
            </br></br>
            <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th column="exp_name">Ekspedisi</th>
                        <th column="province_name">Propinsi</th>
                        <th column="city_name">Kota / Kabupaten</th>
                        <th column="district_name">Kecamatan</th>
                        <th column="exp_district_code">Kecamatan Ekspedisi</th>
                        <th column="created_by"><?php echo TH_CREATED_BY ?></th>
                        <th column="created_date"><?php echo TH_CREATED_DATE ?></th>
                        <th column="modified_by"><?php echo TH_MODIFIED_BY ?></th>
                        <th column="modified_date"><?php echo TH_MODIFIED_DATE ?></th>
                    </tr>
                </thead>
            </table>    
        </div>
    </div>

    <script text ="javascript">

                function set_upload(url_upload) {
                    $('#upload_url').val(url_upload);
                }

    </script>

    <?php
}

require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>

