<?php
require_once VIEW_BASE_ADMIN;

require_once get_include_content_admin_top_page_navigation();
?>

<link rel="stylesheet" type="text/css" href="<?php echo get_css_url() ?>daterangepicker-bs3.css" />
<script type="text/javascript" src="<?php echo get_js_url() ?>moment.min.js"></script>
<script type="text/javascript" src="<?php echo get_js_url() ?>daterangepicker.js"></script>

<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo ($data_auth[FORM_ACTION] == ACTION_ADD ? get_title_add($data_auth[FORM_AUTH][FORM_TITLE]) : get_title_edit($data_auth[FORM_AUTH][FORM_TITLE])); ?></h3>
        </div>
        <form class ="form-horizontal" id="frmMain" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
            <div class="box-body">
                <section class="col-md-12">
                    <?php echo get_csrf_admin_token(); ?>
                    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                        <input class="form-control"  name ="seq" type="hidden" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->seq : "" ); ?>">
                    <?php } ?>
                    <div class="col-md-6">
                        <div class ="form-group">
                            <label class ="control-label col-md-4">Periode *</label>
                            <div class ="col-md-7">
                                <input class="form-control" date_type="date" id ="date_from" validate ="required[]" name ="date_from" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? date("d-M-Y", strtotime($data_sel[LIST_DATA][0]->date_from)) . ' - ' . date("d-M-Y", strtotime($data_sel[LIST_DATA][0]->date_to)) : "" ); ?>" readonly>
                            </div>
                        </div>
                        <div class ="form-group">
                            <div class ="col-md-offset-4 col-md-8">
                                <label> <input type="checkbox" name ="active" <?php echo (!isset($data_sel[LIST_DATA]) ? "checked" : (($data_sel[LIST_DATA][0]->active = "1" OR $data_sel[LIST_DATA][0]->active = "on") ? "checked" : "")); ?> /> Aktif</label>
                            </div>
                        </div>
                        <div class ="form-group">
                            <label class ="control-label col-md-4">Nama Voucher *</label>
                            <div class ="col-md-7">
                                <input class="form-control" validate="required[]" name ="v_name" type="text"  value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->name) : "" ); ?>" >
                            </div>
                        </div>
                        <div class ="form-group">
                            <label class ="control-label col-md-4">Kode Voucher *</label>
                            <div class ="col-md-7">
                                <input minlength="3" maxlength="8" class="form-control" validate="required[]" name ="v_code" type="text"  value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->code) : "" ); ?>" >
                            </div>
                        </div>
                        <div class ="form-group">
                            <label class ="control-label col-md-4">Transaksi *</label>
                            <div class ="col-md-7">
                                <?php require_once get_component_url() . 'dropdown/com_drop_promo_voucher_node.php' ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Cara Buat Voucher *</label>
                            <div class="col-md-3">
                                <input  type="radio" name="t_type" value="A"  onclick="chMd(true)" <?php echo (!isset($data_sel[LIST_DATA]) ? "" : (($data_sel[LIST_DATA][0]->type == 'A') ? "checked" : "")); ?>> <label>Otomatis</label>
                            </div>
                            <div class="col-md-3">
                                <input type="radio" name="t_type" value="M" onclick="chMd(false)" <?php echo (!isset($data_sel[LIST_DATA]) ? "" : (($data_sel[LIST_DATA][0]->type == 'M') ? "checked" : "")); ?>> <label>Manual</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-7 col-md-4">
                                <input  validate="required[]" class="form-control auto_int"  data-a-pad="false" type="text" id="lenSeq" data-v-min="1" name="voucher_count" <?php echo (isset($data_sel[LIST_DATA][0]) ? (($data_sel[LIST_DATA][0]->type == "A") ? "readonly" : '') : "readonly" ); ?>  value="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->voucher_count : "0" ); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Nominal Voucher</label>
                            <div class ="col-md-7">
                                <input class="form-control auto_int" validate ="required[]" id="nominal"  name="nominal" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->nominal : "1" ); ?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Kadaluarsa (Hari) *</label>
                            <div class ="col-md-7">
                                <input  class="form-control auto_int" validate="required[]" data-a-pad="false" name="exp_days" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->exp_days : "" ); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Mininum Belanja</label>
                            <div class ="col-md-7">
                                <input class="form-control auto_int" validate ="required[]" type="text" name="trx_get_amt" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->trx_get_amt : "0" ); ?>" >
                                Untuk mendapatkan Voucher
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Minimum Belanja</label>
                            <div class ="col-md-7">
                                <input class="form-control auto_int" validate ="required[]" type="text" name="trx_use_amt" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->trx_use_amt : "0" ); ?>"> Untuk memakai Voucher
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Keterangan</label>
                            <div class="col-md-7">
                                <textarea class="form-control" style="overflow:auto;resize:none" name="notes" rows="3"><?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->notes) : ""); ?> </textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class ="form-group">
                                <?php
                                if ($data_auth[FORM_ACTION] == ACTION_VIEW) {
                                    ?>
                                    <div class ="col-md-6"><?php echo get_back_button(); ?> </div>
                                    <?php
                                } else {
                                    if ($data_auth[FORM_ACTION] == ACTION_ADD) {
                                        ?>
                                        <div class ="col-md-6"><?php echo get_save_add_button(); ?> </div>
                                    <?php } if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                                        <input class="form-control"  name ="status" type="hidden" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->status : "" ); ?>">
                                        <div class ="col-md-6"><?php echo (($data_sel[LIST_DATA][0]->status == 'N') ? get_save_edit_button() : ''); ?></div>
                                    <?php } ?>
                                    <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </form>
    </div>
    <script type="text/javascript">
            <!--
         $('input[date_type="date"]').daterangepicker({
                format: 'DD-MMM-YYYY',
                showDropdowns: true
            });
            function chMd(model)
            {
                $("#lenSeq").prop('readonly', model);
            }
    </script>
    <script type="text/javascript">
        $('input').keypress(function() {
            var nominal = document.getElementById("nominal").value;
            if ($("#nominal").val().length >= 0) {
                if (nominal == "0") {
                }
            }
        });
    </script>

<?php } else {
    ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <?php require_once get_include_page_list_admin_content_header(); ?>
            <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th column="date_from"> Periode Mulai </th>
                        <th column="date_to"> Periode Akhir</th>
                        <th column="name"> Nama Voucher</th>
                        <th column="code"> Kode Voucher</th>
                        <th column="node_name"> Transaksi</th>
                        <th column="type">Cara Buat Voucher</th>
                        <th column="voucher_count"> Jumlah Voucher</th>
                        <th column="nominal">Nominal Voucher</th>
                        <th column="exp_days"> Kadaluarsa</th>
                        <th column="trx_get_amt"> Nilai Belanja</th>
                        <th column="trx_use_amt"> Nilai Belanja</th>
                        <th column="notes"> Keterangan</th>
                        <th column="status"> Status</th>
                        <th column="action">Detail</th>
                        <th column="active"> Aktif </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <?php
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>
