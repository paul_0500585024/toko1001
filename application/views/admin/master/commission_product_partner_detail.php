<?php
$runingNumber = '';

if (isset($data_head[LIST_DATA][1]) && $data_head[LIST_DATA][1] != '')
    foreach ($data_head[LIST_DATA][1] as $key) {
        $runingNumber[] = explode('$#', $key->product_seq)[0];
    }

require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
?>
<div class="box box-default">     
    <div class="box-header">
        <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
    </div>
    <div class="box-body">
        <section class="col-md-8">
            <div class ="form-group m-bottom-10px">     
                <label class ="control-label col-md-3">Periode From</label>
                <div class ="col-md-8">
                    <input class="form-control" name ="period_from" type="text" value ="<?php echo( isset($data_head[LIST_DATA][0][0]) ? $data_head[LIST_DATA][0][0]->period_from : '' ) ?>" disabled>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class ="form-group">     
                <label class ="control-label col-md-3">Period To</label>
                <div class ="col-md-8">
                    <input class="form-control" type="text" value ="<?php echo( isset($data_head[LIST_DATA][0][0]) ? $data_head[LIST_DATA][0][0]->period_to : '' ) ?>" disabled>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class ="form-group" style="margin-bottom: 10px">     
                <label class ="control-label col-md-3">Notes</label>
                <div class ="col-md-8">
                    <input class="form-control"  name ="notes" type="text" value ="<?php echo( isset($data_head[LIST_DATA][0][0]) ? $data_head[LIST_DATA][0][0]->notes : '' ) ?>" disabled/>
                </div>
                <div class="clearfix"></div>
            </div>
        </section>
    </div>
    <hr>
    <form id="frmMain" method="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
        <?php echo get_csrf_admin_token(); ?>
        <input type="hidden" name="partner_product_seq" value="<?php echo $data_head[LIST_DATA][0][0]->seq ?>">
        <div class="box-body">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border">Produk</legend>
                <button type="button" class="btn btn-success" id="add_product_partner_commission"><i class="fa fa-plus"></i></button>
                <div class="control-group" style="margin-top: 15px">
                    <table class="table table-striped">
                        <thead>
                            <tr style="background: #008d4c;color:#fff">
                                <th>Produk</th>
                                <th>Grup Partner</th>
                                <th>Komisi Partner</th>
                                <th>Komisi Agent</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody id="tabelContent">
                            <?php if (isset($data_head[LIST_DATA][1])) { ?>
                                <?php
                                foreach ($data_head[LIST_DATA][1] as $product) {
                                    $productAttribute = explode('$#', $product->product_seq);
                                    $prodSeq = is_array($productAttribute) && isset($productAttribute[0]) ? $productAttribute[0] : $productAttribute[0];
                                    $prodName = is_array($productAttribute) && isset($productAttribute[1]) ? $productAttribute[1] : $productAttribute[0];
                                    ?>
                                    <tr id="ls_product_<?php echo $prodSeq ?>">
                                        <td>
                                            <select id="prod_name_<?php echo $prodSeq ?>" class="slt" name="product_seq[]" style="min-width:400px;max-width:400px;height:34px" onmouseover="auto_complete('<?php echo $prodSeq ?>')" />
                                <option value="<?php echo $product->product_seq ?>" selected><?php echo $product->name ?></option>
                                </select>
                                </td>
                                <td>
                                    <select style="min-width:200px;height:34px" name="partner_group_seq[]" class="select2">
                                        <?php
                                        if (isset($data_dropdown_group_partner) && $data_dropdown_group_partner != '') {
                                            foreach ($data_dropdown_group_partner as $group) {
                                                $a = explode('$#', $product->partner_group_seq);
                                                $b = explode('$#', $group->partner_group_seq);
                                                echo '<option value="' . $group->partner_group_seq . '"' . ($a[0] == $b[0] ? 'selected' : '') . '>' . $group->partner_group_name . '</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input name="nominal_commission_partner[]" id="nom_" type="text" class="form-control auto_int text-right" placeholder="Komisi Agent" value="<?php echo $product->nominal_commission_partner ?>"/>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input name="nominal_commission_agent[]" id="nom_" type="text" class="form-control auto_int text-right" placeholder="Komisi Agent" value="<?php echo $product->nominal_commission_agent ?>"/>
                                        </div>
                                    </div>
                                </td>
        <!--                                <td style="text-align:right">
                                    <input class="form-control auto_dec text-right" type="text" name="commission_fee_partner_percent[]" data-v-max="100" value="<?php echo $product->commission_fee_partner_percent ?>"/>
                                </td>
                                <td style="text-align:right">
                                    <input class="form-control auto_dec text-right" type="text" name="commission_fee_agent_percent[]" data-v-max="100" value="<?php echo $product->commission_fee_agent_percent ?> "/>
                                </td>-->
                                <td>
                                    <button type="button" onclick="delete_product('<?php echo (int) $prodSeq ?>')" class="btn btn-danger btn-flat"><i class="fa fa-times"></i></button>
                                </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </fieldset>
            <div class="col-xs-6">
                <a href="<?php echo base_url('admin/master/commission_product_partner') ?>" class="btn btn-block btn-flat btn-google-plus">Kembali</a>
            </div>
            <div class="col-xs-6">
                <?php echo get_save_add_button() ?>
            </div>
        </div>
    </form>
</div>

<script>
    var runingNumber = (<?php echo isset($runingNumber) && is_array($runingNumber) ? max($runingNumber) : 0 ?> + 1);
    $("#add_product_partner_commission").on('click', function () {
        var appent = '<tr id="ls_product_' + runingNumber + '">'
                + '<td>'
                + '<select name="product_seq[]" style="min-width:400px;max-width:400px;height:34px" class="select-ajax" id="prod_name_' + runingNumber + '" onfocus="auto_complete(\'' + runingNumber + '\')" autofocus> '
                + '<option value="" selected="selected">-- Pilih Produk --</option>'
                + '</select>'
                + '</td>'

                + '<td>'
                + '<select style="min-width:200px;height:34px" name="partner_group_seq[]" id="group_partner_' + runingNumber + '" onfocus="group_partner(\'' + runingNumber + '\')">'
                + '<option value=""> -- Pilih Group Partner -- </option>'
                + '<?php
                if (isset($data_dropdown_group_partner) && $data_dropdown_group_partner != '') {
                    foreach ($data_dropdown_group_partner as $group) {
                        echo '<option value="' . $group->partner_group_seq . '">' . $group->partner_group_name . '</option>';
                    }
                }
                ?>'
                + '</select>'
                + '</td>'

                + '<td>'
                + '<div class="form-group">'
                + '<div class="input-group">'
                + '<div class="input-group-addon">Rp</div>'
                + '<input name="nominal_commission_partner[]" id="partner_' + runingNumber + '" type="text" class="form-control auto_int text-right" placeholder="Komisi Partner" onfocus="nominal_partner(\'' + runingNumber + '\')">'
                + '</div>'
                + '</div>'
                + '</td>'

                + '<td>'
                + '<div class="form-group">'
                + '<div class="input-group">'
                + '<div class="input-group-addon">Rp</div>'
                + '<input name="nominal_commission_agent[]" id="agent_' + runingNumber + '" type="text" class="form-control auto_int text-right" placeholder="Komisi Agent" onfocus="nominal_agent(\'' + runingNumber + '\')">'
                + '</div>'
                + '</div>'
                + '</td>'
                + '<td>'
                + '<button type="button" onclick="delete_product(\'' + runingNumber + '\')" class="btn btn-danger btn-flat"><i class="fa fa-times"></i>'
                + '</td>'
                + '</tr>';
        $('#tabelContent').append(appent);
        $('#prod_name_' + runingNumber).focus();
        $('#group_partner_' + runingNumber).focus();
        runingNumber = runingNumber + 1;
    });
    function delete_product(id) {
        $("#ls_product_" + id).remove();
    }

    function group_partner(id) {
        $('#group_partner_' + id).select2();
    }

    function nominal_partner(id) {
        $("#partner_" + id).autoNumeric('init', {vMin: 0, mDec: 0});
    }
    
    function nominal_agent(id) {
        $("#agent_" + id).autoNumeric('init', {vMin: 0, mDec: 0});
    }

    function partner_commission_percent(id) {
        $("#partner_commission_percent_" + id).autoNumeric('init', {vMin: 0, mDec: 2});
    }

    function agent_commission_percent(id) {
        $("#agent_commission_percent_" + id).autoNumeric('init', {vMin: 0, mDec: 2});
    }

    function auto_complete(id) {
        $("#prod_name_" + id).select2({
            minimumInputLength: 2,
            placeholder: "Please select a country",
            ajax: {
                url: "<?php echo get_base_url() . $data_auth[FORM_URL] ?>",
                dataType: 'json',
                delay: 250,
                type: "POST",
                data: function (params) {
                    return {
                        type: "data_dropdown_product",
                        product_name: params.term,
                        btnAdditional: "act_s_adt",
                        act: "<?php echo ACTION_ADDITIONAL ?>"
                    };
                },
                results: function (data, page) {
                    return {results: data};
                }
            }
        });
    }



    function getDropDown(value, contentBoxID) {
        if (value.length > 4) {
            $("#contentBox_" + contentBoxID).empty();
            $("#contentBox_" + contentBoxID).show();
            $.ajax({url: "<?php echo get_base_url() . $data_auth[FORM_URL] ?>",
                dataType: "JSON",
                type: "POST",
                data: {type: "data_dropdown_product", product_name: value, btnAdditional: "act_s_adt", act: "<?php echo ACTION_ADDITIONAL ?>"},
                success: function (data) {
                    if (data.total_data > 0) {
                        $("#contentBox_" + contentBoxID).empty();
                        $("#contentBox_" + contentBoxID).append(data.data);
                    } else {
                        $("#contentBox_" + contentBoxID).empty();
                        $("#contentBox_" + contentBoxID).append('no data finded');
                    }
                },
                error: function (xhr) {
                    alert(xhr);
                }
            });
        }
    }
</script>
<?php
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>

