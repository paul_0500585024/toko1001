<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
?>
<link rel="stylesheet" type="text/css" href="<?php echo get_css_url() ?>daterangepicker-bs3.css" />
<script type="text/javascript" src="<?php echo get_js_url() ?>moment.min.js"></script>
<script type="text/javascript" src="<?php echo get_js_url() ?>daterangepicker.js"></script>
<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="box box-default">     
        <div class="box-header">
            <h3 class="box-title"><?php echo get_title_edit($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <form class ="form-horizontal" id="frmMain" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
                <?php echo get_csrf_admin_token(); ?>
                <input type="hidden" name="seq" value="<?php echo isset($data_sel[LIST_DATA][0]->seq) ? get_display_value($data_sel[LIST_DATA][0]->seq) : ''; ?>"/>
                <section class="col-md-8">
                    <div class ="form-group">     
                        <label class ="control-label col-md-3">Periode From</label>
                        <div class ="col-md-8">
                            <input class="form-control" date_type="date" id ="period_to" validate ="required[]" name ="period_from" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? date("d-M-Y", strtotime($data_sel[LIST_DATA][0]->period_from)) : "" ); ?>" readonly>
                        </div>
                    </div>
                    <div class ="form-group">     
                        <label class ="control-label col-md-3">Period To</label>
                        <div class ="col-md-8">
                            <input class="form-control" date_type="date" id ="period_from" validate ="required[]" name ="period_to" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? date("d-M-Y", strtotime($data_sel[LIST_DATA][0]->period_to)) : "" ); ?>" readonly>
                        </div>
                    </div>
                    <div class ="form-group">     
                        <label class ="control-label col-md-3">Notes</label>
                        <div class ="col-md-8">
                            <input class="form-control" validate="required[]" name ="notes" type="text" value ="<?php echo isset($data_sel[LIST_DATA][0]->notes) ? get_display_value($data_sel[LIST_DATA][0]->notes) : ''; ?>"/>
                        </div>
                    </div>
                    </br>
                    <div class ="form-group">
                        <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                            <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                            <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                        <?php } ?>
                        <?php if ($data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
                            <div class ="col-md-6"><?php echo get_back_button(); ?></div> 
                        <?php } ?>
                        <?php if ($data_auth[FORM_ACTION] == ACTION_ADD) { ?>
                            <div class ="col-md-6"><?php echo get_save_add_button(); ?></div> 
                            <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                        <?php } ?>
                    </div>
                </section>
            </form>
        </div>
    </div>


    <script>
        $('input[date_type="date"]').daterangepicker({
            format: 'DD-MMM-YYYY',
            showDropdowns: true,
            singleDatePicker: true
        });
    </script>


<?php } else { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <?php require_once get_include_page_list_admin_content_header(); ?>
            <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th column="period_from">Periode Awal</th>
                        <th column="period_to">period Akhir</th>
                        <th column="detail">Detail</th>
                        <th column="notes">Notes</th>
                        <th column="created_by">Dibuat Oleh</th>
                        <th column="created_date">Dibuat Tanggal</th>
                        <th column="modified_by">Diubah Oleh</th>
                        <th column="modified_date">Diubah Tanggal</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <?php
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>