<?php
require_once VIEW_BASE_ADMIN;
?>

<form id ="frmSearch"  url= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
    <div class="form-group">
        <label>Modul</label>
        <?php include dirname(__FILE__) . '/../component/dropdown/com_drop_menu_module.php' ?>
    </div>
    <div class="form-group">
        <label>Kode Menu</label>
        <input class="form-control" name="menu_cd" type="input">
    </div>
    <div class="form-group">
        <label>Nama Menu</label>
        <input class="form-control" name="menu_name" type="input">
    </div>
    <div class="form-group">
        <input name ="active" type="checkbox" checked > Aktif</input>
    </div>
    <div class="form-group">
        <?php echo get_search_button(); ?>
    </div>
</form>