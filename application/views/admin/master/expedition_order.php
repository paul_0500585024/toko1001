<?php
require_once VIEW_BASE_ADMIN;

require_once get_include_content_admin_top_page_navigation();
?>

<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo $data_auth[FORM_ACTION_TITLE] . $data_auth[FORM_AUTH][FORM_TITLE] ?></h3>
        </div>
        <form class ="form-horizontal" id="frmMain" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
            <div class="box-body">
                <section class="col-md-12">
                    <?php echo get_csrf_admin_token(); ?>
                    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                        <input class="form-control" name ="order_seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->order_seq; ?>">
                        <input class="form-control" name ="merchant_info_seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->merchant_info_seq; ?>">
                    <?php } ?>
                    <div class="row">
                        <div class="col-md-6">
                            <div class ="form-group">
                                <label class ="control-label col-md-3">No. Order :</label>
                                <div class ="col-md-6">
                                    <input class="form-control" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->order_no : "") ?>" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class ="control-label col-md-3">Tanggal Order :</label>
                            <div class ="col-md-6">
                                <input class="form-control" value="<?php echo (isset($data_sel[LIST_DATA]) ? cdate($data_sel[LIST_DATA][0]->order_date) : "") ?>" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class ="form-group">
                                <label class ="control-label col-md-3">Member :</label>
                                <div class ="col-md-6">
                                    <input class="form-control" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->member_name : "") ?>" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class ="control-label col-md-3">Alamat Penerima :</label>
                            <div class ="col-md-6">
                                <input class="form-control" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->receiver_address : "") ?>" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <table class="diplay table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th> # </th>
                                    <th> Produk </th>
                                    <th> Tipe Produk </th>
                                    <th> Qty </th>
                                    <th> Biaya Pengiriman </th>
                                    <th> Harga Jual </th>
                                    <th> Total </th>
                                </tr>
                                <?php
                                if (isset($data_sel[LIST_DATA][1])) {
                                    foreach ($data_sel[LIST_DATA][1] as $each) {
                                        ?>
                                        <tr valign="top">
                                            <td><img src="<?php echo get_base_url() . PRODUCT_UPLOAD_IMAGE ?><?php echo $each->merchant_seq; ?>/<?php echo $each->pic_1_img; ?>" width="100px" height="65px"></td>
                                            <td><label class="control-label"><?php echo $each->product_name; ?></label></td>
                                            <td><label class="control-label"><?php echo $each->variant_name; ?></label></td>
                                            <td><label class="control-label"><?php echo $each->qty; ?></label></td>
                                            <td><label class="control-label"><?php echo cnum($each->ship_price_charged); ?></label></td>
                                            <td><label class="control-label"><?php echo cnum($each->sell_price); ?></label></td>
                                            <td><label class="control-label"><?php echo cnum(ceil($each->weight_kg * $each->qty) * $each->ship_price_charged + $each->sell_price * $each->qty); ?></label></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    </br>
                    <?php IF ($data_sel[LIST_DATA][0]->order_status == 'N') { ?>
                        <div class="row">
                            <table class="display table table-bordered table-striped" cellspacing="0" width="100%">
                                <thead class="bg-blue">
                                    <tr>
                                        <td style="width: 200px">Expedisi Lama</td>
                                        <td style="width: 200px"><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->es_name : "") ?></td>
                                        <td style="width: 200px"><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->exs_name : "") ?></td>
                                    </tr>                                    
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Expedisi Baru</th>
                                        <td valign="top"><?php require_once get_component_url() . "dropdown/com_drop_expedition.php"; ?></td>
                                        <td valign="top"><?php require_once get_component_url() . "dropdown/com_drop_expedition_service.php"; ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    <?php } ?>

                    <div class ="form-group">
                        <?php if ($data_auth[FORM_ACTION] == ACTION_ADD) { ?>
                            <div class ="col-md-6"><?php echo get_save_add_button(); ?> </div>
                        <?php } if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                            <div class ="col-md-6"><?php IF ($data_sel[LIST_DATA][0]->order_status == 'N') echo get_save_edit_button(); ?></div>
                        <?php } ?>
                        <div class ="col-md-6"><?php echo get_cancel_button(); ?></div>
                    </div>
                </section>
            </div>
        </form>
    </div>

    <script text="javascript">
            $("#expedition_seq").change(function() {
                var city_val = $("#drop_exp_svc").val();
                if ($(this).val() != "")
                {
                    $.ajax({
                        url: "<?php echo get_base_url() . $data_auth[FORM_URL] ?>",
                        dataType: "json",
                        data: {
                            btnAdditional: true,
                            expedition_seq: $(this).val()
                        },
                        type: "POST",
                        success: function(data) {
                            if(isSessionExpired(data)){
                                response_object = json_decode(data);
                                url = response_object.url;
                                location.href = url;
                            }else{
                                $('#drop_exp_svc option').remove();
                                $(data).each(function(i) {
                                    $("#drop_exp_svc").append("<option value=\"" + data[i].value + "\">" + data[i].display_name + "</option>")
                                });
                                $("#drop_exp_svc").val(city_val);
                                $('#drop_exp_svc').select2();
                            }
                        },
                        error: function(request, error) {
                            alert(error_response(request.status));
                        }
                    });
                }
                else
                {
                    $('#drop_exp_svc option').remove();
                    $('#drop_exp_svc').select2()
                }
            }).change();
    </script>
    <!--</div-->
<?php } else {
    $tablesort = 'desc'; ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
    <?php require_once get_include_page_list_admin_content_header(); ?>
            <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th column="tgl_order"> Tanggal Order </th>
                        <th column="no_order"> No Order </th>
                        <th column="merchant_name"> Merchant </th>
                        <th column="expedition_name"> Ekspedisi Lama</th>
                        <th column="expedition_name_real">  Ekspedisi Baru </th>
                        <th column="order_status">  Status Order </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <?php
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>