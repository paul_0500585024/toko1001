<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
?>

<?php { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <div class="form-horizontal">
                <div class ="form-group">
                    <label class ="control-label col-md-2">Kode Ekspedisi</label>
                    <div class ="col-md-3">
                        <input type="text" name="code" class="form-control" value="<?php echo (isset($data_head[LIST_DATA]) ? $data_head[LIST_DATA][0]->code : ""); ?>" disabled/>
                    </div>
                </div>
                <div class ="form-group">
                    <label class ="control-label col-md-2">Nama Ekspedisi</label>
                    <div class ="col-md-3">
                        <input type="text" name="name" class="form-control" value="<?php echo (isset($data_head[LIST_DATA]) ? $data_head[LIST_DATA][0]->name : ""); ?>" disabled/>
                    </div>
                </div>
                <div class ="form-group">
                    <div class ="col-md-offset-2 col-md-9">
                        <input type="checkbox" name ="active" <?php echo (!isset($data_sel[LIST_DATA]) ? "checked" : (($data_sel[LIST_DATA][0]->active == "1" OR $data_sel[LIST_DATA][0]->active == "on") ? "checked" : "")); ?> disabled/>
                        <label> Aktif</label>
                    </div>
                </div>  
            </div>
            <form id = "frmAdd" method = "post" action = "<?php echo get_base_url() . $data_auth[FORM_URL] ?>" enctype = "multipart/form-data">
                <?php echo get_csrf_admin_token(); ?>
                <div class="input-group col-md-5">
                    <span class="input-group-btn">
                        <span class="btn btn-primary btn-file">
                            Pilih File
                            <input class='form-control' id="file_upload" name='userfile' type="file" onchange="set_upload(this.value)"/>
                        </span>
                    </span>
                    <input class="form-control" readonly="" id="upload_url" type="text">
                    <span class="input-group-btn">
                        <button name ="<?php echo CONTROL_ADDITIONAL_NAME ?>" type = "submit" class = "btn btn-primary"><i class='fa fa-upload'></i> Unggah</button>
                    </span>
                </div>
            </form>
        </div>
        <div class="box-body">
            <?php
            require_once get_include_page_list_admin_content_header();
            ?>
            <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th column="awb_no"> No. Resi </th>
                        <th column="trx_no"> No. Order </th>
                        <th column="ship_date"> Tgl Resi </th>
                        <th column="name"> Merchant </th>
                        <th column="created_by"><?php echo TH_CREATED_BY ?></th>
                        <th column="created_date"><?php echo TH_CREATED_DATE ?></th>
                        <th column="modified_by"><?php echo TH_MODIFIED_BY ?></th>
                        <th column="modified_date"><?php echo TH_MODIFIED_DATE ?></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <script text ="javascript">

                                function set_upload(url_upload) {
                                    $('#upload_url').val(url_upload);
                                }

    </script>
    <?php
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>