<?php
require_once VIEW_BASE_ADMIN;

require_once get_include_content_admin_top_page_navigation();
?>

<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo $data_auth[FORM_ACTION_TITLE] . $data_auth[FORM_AUTH][FORM_TITLE] ?></h3>
        </div>
        <form class ="form-horizontal" id="frmMain" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
            <div class="box-body">
                <div class="col-md-6" id="tree_menu" style="overflow-y:auto;height:525px;border: 1px solid black;">
                    <?php require_once get_component_url() . "treeview/tree_view_menu.php" ?>
                </div>
                <section class="col-md-6">
                    <?php echo get_csrf_admin_token(); ?>
                    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                        <input class="form-control" name ="old_menu_cd" type="hidden" value ="<?php echo (isset($data_sel[LIST_DATA][0]->old_menu_cd) ? $data_sel[LIST_DATA][0]->old_menu_cd : $data_sel[LIST_DATA][0]->menu_cd) ?>" >
                        <input class="form-control" name ="old_module_seq" type="hidden" value ="<?php echo (isset($data_sel[LIST_DATA][0]->old_module_seq) ? $data_sel[LIST_DATA][0]->old_module_seq : $data_sel[LIST_DATA][0]->module_seq) ?>" >
                    <?php } ?>
                    <input class="form-control" id='parent_menu_cd' name ="parent_menu_cd" type="hidden" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->parent_menu_cd : "" ); ?>">
                    <div class ="form-group">
                        <label class ="control-label col-md-3 pull-right">Modul*</label>
                        <div class ="col-md-9">
                            <?php require_once get_component_url() . "dropdown/com_drop_menu_module.php"; ?>
                        </div>
                    </div>
                    <div class ="form-group">     
                        <label class ="control-label col-md-3">Parent Menu*</label>
                        <div class ="col-md-9">                           
                            <input class="form-control" id ='parent_menu_name' validate ='required[]' name ="parent_menu_name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->parent_menu_name : "" ); ?>" readonly>
                        </div>
                    </div>  
                    <div class ="form-group">     
                        <label class ="control-label col-md-3">Kode Menu*</label>
                        <div class ="col-md-9">
                            <input class="form-control" name ="menu_cd" validate ='required[]' type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->menu_cd : "" ); ?>">
                        </div>
                    </div>
                    <div class ="form-group">     
                        <label class ="control-label col-md-3">Nama Menu*</label>
                        <div class ="col-md-9">
                            <input class="form-control" name ="menu_name" validate ='required[]' type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->name : "" ); ?>">
                        </div>
                    </div>
                    <div class ="form-group">     
                        <label class ="control-label col-md-3">Tampilan Menu*</label>
                        <div class ="col-md-9">
                            <input class="form-control" name ="title_name" validate ='required[]' type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->title_name : "" ); ?>">
                        </div>
                    </div>         
                    <div class ="form-group">     
                        <label class ="control-label col-md-3">URL</label>
                        <div class ="col-md-9">
                            <input class="form-control" name ="url" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->url : "" ); ?>">
                        </div>
                    </div> 
                    <div class ="form-group">     
                        <label class ="control-label col-md-3">Icon</label>
                        <div class ="col-md-9">
                            <input class="form-control" name ="icon" type="input" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->icon : "" ); ?>">
                        </div>
                    </div> 
                    <div class ="form-group">
                        <label class ="control-label col-md-3">Urutan</label>
                        <div class ="col-md-9">
                            <input class="form-control auto" validate ="required[]" data-d-group="3" name ="order" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->order : 0 ); ?>">
                        </div>
                    </div>
                    <div class ="form-group">
                        <div class ="col-md-offset-3 col-md-9">
                            <label> <input type="checkbox" name ="detail" <?php echo (!isset($data_sel[LIST_DATA]) ? "checked" : (($data_sel[LIST_DATA][0]->detail == "1" OR $data_sel[LIST_DATA][0]->detail == "on") ? "checked" : "")); ?> /> Detail</label>
                        </div>
                    </div>
                    <div class ="form-group">
                        <div class ="col-md-offset-3 col-md-9">
                            <label> <input type="checkbox" name ="active" <?php echo (!isset($data_sel[LIST_DATA]) ? "checked" : (($data_sel[LIST_DATA][0]->active == "1" OR $data_sel[LIST_DATA][0]->active == "on") ? "checked" : "")); ?> /> Aktif</label>
                        </div>
                    </div>
                    <div class ="form-group">
                        <?php
                        if ($data_auth[FORM_ACTION] != ACTION_VIEW) {
                            if ($data_auth[FORM_ACTION] == ACTION_ADD) {
                                ?>
                                <div class ="col-md-6"><?php echo get_save_add_button(); ?> </div>
                            <?php } if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                                <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                            <?php } ?>
                            <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                        <?php } ?>
                    </div>
                </section>
            </div>
        </form>
    </div>

    <script type="text/javascript">
            function pilihmenu(values) {
                var res = values.split("~");
                $("#parent_menu_cd").val(res[0]);
                $("#parent_menu_name").val(res[1]);
            }
            
            $("#drop_module").change(function() {
                $.ajax({
                    url: "<?php echo get_base_url() . $data_auth[FORM_URL] ?>",
                    data: {
                        btnAdditional: true,
                        module_seq: $(this).val()
                    },
                    type: "POST",
                    success: function(data) {
                        if(isSessionExpired(data)){
                            response_object = json_decode(data);
                            url = response_object.url;
                            location.href = url;
                        }else{
                            $('#tree_menu').html(data);
                        }
                    },
                    error: function (request, error) {
                        alert(error_response(request.status));
                    }
                });
            }).change();
    </script>
<?php } else { ?>    
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
        </div>
        <div class="box-body">
            <?php require_once get_include_page_list_admin_content_header(); ?>
            <table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>                                          
                        <th column="module_seq">Modul</th>
                        <th column="menu_cd">Kode Menu</th>
                        <th column="name">Nama Menu</th>
                        <th column="title_name">Tampilan Menu</th>
                        <th column="url">URL</th>
                        <th column="icon">Icon</th>
                        <th column="order">Urutan</th>
                        <th column="active">Aktif</th>
                        <th column="detail">Detil</th>
                        <th column="created_by">Dibuat Oleh</th>
                        <th column="created_date">Tanggal Buat</th>
                    </tr>
                </thead>
            </table>    
        </div>
    </div>
    <?php
}
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>