<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
?>
<?php echo get_csrf_admin_token();?>
<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Data Pengajuan</h3>
    </div>
    <form class ="form-horizontal" id="frmMain" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
        <div class="box-body">
            <div class="col-md-12">
                <table class="table table-bordered table-striped"cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Data Lama</th>
                            <th>Data Baru</th>
                        </tr>
                    </thead>
                    <tbody>                  
                        <tr>
                            <?php if ($data_sel[LIST_DATA][0]->address != $data_sel[LIST_DATA][1]->address) { ?>
                                <td>Alamat</td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->address : "") ?></td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][1]->address : "") ?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <?php if ($data_sel[LIST_DATA][0]->province_name != $data_sel[LIST_DATA][1]->province_name) { ?>
                                <td>Propinsi</td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->province_name : "") ?></td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][1]->province_name : "") ?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <?php if ($data_sel[LIST_DATA][0]->city_name != $data_sel[LIST_DATA][1]->city_name) { ?>
                                <td>Kota</td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->city_name : "") ?></td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][1]->city_name : "") ?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <?php if ($data_sel[LIST_DATA][0]->district_name != $data_sel[LIST_DATA][1]->district_name) { ?>
                                <td>Kabupaten</td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->district_name : "") ?></td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][1]->district_name : "") ?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <?php if ($data_sel[LIST_DATA][0]->zip_code != $data_sel[LIST_DATA][1]->zip_code) { ?>
                                <td>Kode Pos</td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->zip_code : "") ?></td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][1]->zip_code : "") ?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <?php if ($data_sel[LIST_DATA][0]->phone_no != $data_sel[LIST_DATA][1]->phone_no) { ?>
                                <td>No. Telp</td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->phone_no : "") ?></td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][1]->phone_no : "") ?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <?php if ($data_sel[LIST_DATA][0]->fax_no != $data_sel[LIST_DATA][1]->fax_no) { ?>
                                <td>No Fax</td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->fax_no : "") ?></td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][1]->fax_no : "") ?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <?php if ($data_sel[LIST_DATA][0]->pic1_name != $data_sel[LIST_DATA][1]->pic1_name) { ?>
                                <td>Nama PIC</td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->pic1_name : "") ?></td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][1]->pic1_name : "") ?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <?php if ($data_sel[LIST_DATA][0]->pic1_phone_no != $data_sel[LIST_DATA][1]->pic1_phone_no) { ?>
                                <td>Telp PIC</td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->pic1_phone_no : "") ?></td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][1]->pic1_phone_no : "") ?></td>
                            <?php } ?>  
                        </tr>
                        <tr>
                            <?php if ($data_sel[LIST_DATA][0]->pic2_name != $data_sel[LIST_DATA][1]->pic2_name) { ?>
                                <td>Nama Finance</td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->pic2_name : "") ?></td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][1]->pic2_name : "") ?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <?php if ($data_sel[LIST_DATA][0]->pic2_phone_no != $data_sel[LIST_DATA][1]->pic2_phone_no) { ?>
                                <td>Telp Finance</td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->pic2_phone_no : "") ?></td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][1]->pic2_phone_no : "") ?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <?php if ($data_sel[LIST_DATA][0]->pickup_addr != $data_sel[LIST_DATA][1]->pickup_addr) { ?>
                                <td>Alamat Pengambilan</td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->pickup_addr : "") ?></td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][1]->pickup_addr : "") ?></td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <?php if ($data_sel[LIST_DATA][0]->pickup_province_name != $data_sel[LIST_DATA][1]->pickup_province_name) { ?>
                                <td>Propinsi Pengambilan</td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->pickup_province_name : "") ?></td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][1]->pickup_province_name : "") ?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <?php if ($data_sel[LIST_DATA][0]->pickup_city_name != $data_sel[LIST_DATA][1]->pickup_city_name) { ?>
                                <td>Kota Pengambilan</td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->pickup_city_name : "") ?></td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][1]->pickup_city_name : "") ?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <?php if ($data_sel[LIST_DATA][0]->pickup_district_name != $data_sel[LIST_DATA][1]->pickup_district_name) { ?>
                                <td>Kecamatan Pengambilan</td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->pickup_district_name : "") ?></td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][1]->pickup_district_name : "") ?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <?php if ($data_sel[LIST_DATA][0]->pickup_zip_code != $data_sel[LIST_DATA][1]->pickup_zip_code) { ?>
                                <td>Kode Pos Pengambilan</td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->pickup_zip_code : "") ?></td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][1]->pickup_zip_code : "") ?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <?php if ($data_sel[LIST_DATA][0]->return_addr != $data_sel[LIST_DATA][1]->return_addr) { ?>
                                <td>Alamat Pengambilan</td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->return_addr : "") ?></td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][1]->return_addr : "") ?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <?php if ($data_sel[LIST_DATA][0]->return_province_name != $data_sel[LIST_DATA][1]->return_province_name) { ?>
                                <td>Propinsi Pengambilan</td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->return_province_name : "") ?></td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][1]->return_province_name : "") ?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <?php if ($data_sel[LIST_DATA][0]->return_city_name != $data_sel[LIST_DATA][1]->return_city_name) { ?>
                                <td>Kota Pengambilan</td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->return_city_name : "") ?></td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][1]->return_city_name : "") ?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <?php if ($data_sel[LIST_DATA][0]->return_district_name != $data_sel[LIST_DATA][1]->return_district_name) { ?>
                                <td>Kecamatan Pengambilan *</td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->return_district_name : "") ?></td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][1]->return_district_name : "") ?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <?php if ($data_sel[LIST_DATA][0]->return_zip_code != $data_sel[LIST_DATA][1]->return_zip_code) { ?>
                                <td>Kode Pos Pengambilan</td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->return_zip_code : "") ?></td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][1]->return_zip_code : "") ?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <?php if ($data_sel[LIST_DATA][0]->bank_name != $data_sel[LIST_DATA][1]->bank_name) { ?>
                                <td>Bank</th>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->bank_name : "") ?></td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][1]->bank_name : "") ?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <?php if ($data_sel[LIST_DATA][0]->bank_branch_name != $data_sel[LIST_DATA][1]->bank_branch_name) { ?>
                                <td>Cabang</td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->bank_branch_name : "") ?></td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][1]->bank_branch_name : "") ?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <?php if ($data_sel[LIST_DATA][0]->bank_acct_no != $data_sel[LIST_DATA][1]->bank_acct_no) { ?>
                                <td>No Rekening</td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->bank_acct_no : "") ?></td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][1]->bank_acct_no : "") ?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <?php if ($data_sel[LIST_DATA][0]->bank_acct_name != $data_sel[LIST_DATA][1]->bank_acct_name) { ?>
                                <td>Nama Rekening</td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->bank_acct_name : "") ?></td>
                                <td><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][1]->bank_acct_name : "") ?></td>
                            <?php } ?>
                        </tr>
                    </tbody>
                </table>
                <div class="form-group">
                    <div class="col-md-6"><a href="<?php echo get_base_url() . 'admin/master/merchant_log'; ?>" class="btn btn-google-plus"><i class=" fa fa-arrow-circle-left"></i>&nbsp; kembali</a></div>
                </div>
            </div>
        </div>
    </form>
</div>

<?php
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>