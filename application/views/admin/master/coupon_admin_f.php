<?php
require_once VIEW_BASE_ADMIN;
?>
<link rel="stylesheet" type="text/css" href="<?php echo get_css_url() ?>daterangepicker-bs3.css" />
<script type="text/javascript" src="<?php echo get_js_url() ?>moment.min.js"></script>
<script type="text/javascript" src="<?php echo get_js_url() ?>daterangepicker.js"></script>
<form id ="frmSearch"  url= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
    <div class="form-group">
        <label>Periode Mulai</label>
        <div>
            <input type="text" name="coupon_period_from" class="form-control" value="<?php echo date('d-M-Y', strtotime(LAST_WEEK)) ?>"  readonly style="background-color: #fff;">
            </br>
            <label>Periode Akhir</label>
            <input type="text" name="coupon_period_to" class="form-control"  value="<?php echo date('d-M-Y', strtotime(NEXT_WEEK)) ?>" readonly style="background-color: #fff;">
        </div>
    </div>
    <div class="form-group">
        <label>Kode Kupon</label>
        <input class="form-control" name="coupon_code" type="input">
    </div>
    <div class="form-group">
        <label>Status</label>
        <select class="form-control select2" name="status">
            <option value="">Semua</option>
            <?php
            echo combostatus(json_decode(STATUS_ANR));
            ?>
        </select>
    </div>
    <div class="form-group">
        <?php echo get_search_button(); ?>
    </div>
</form>

<script  type="text/javascript">
    $('input[name="coupon_period_from"]').daterangepicker({
        format: 'DD-MMM-YYYY',
        showDropdowns: true,
        singleDatePicker: true,
        startDate: "<?php echo date('d-M-Y', strtotime(LAST_WEEK)) ?>"
    });
    $('input[name="coupon_period_to"]').daterangepicker({
        format: 'DD-MMM-YYYY',
        singleDatePicker: true,
        showDropdowns: true,
        startDate: "<?php echo date('d-M-Y', strtotime(NEXT_WEEK)) ?>"
    });
</script>