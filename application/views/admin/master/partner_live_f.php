<?php
require_once VIEW_BASE_ADMIN;
?>

<form id ="frmSearch"  url= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
    <div class="form-group">
        <label>Email</label>
        <input class="form-control" name ="email" type="text"/>
    </div>
    <div class="form-group">
        <label>Nama Partner</label>
        <?php require_once get_component_url() . "dropdown/com_drop_partner.php" ?>
    </div>
    <div class="form-group">
        <?php echo get_search_button(); ?>
    </div>
</form>