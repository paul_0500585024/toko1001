<?php
require_once VIEW_BASE_ADMIN;
require_once get_include_content_admin_top_page_navigation();
if (!isset($data_sel[LIST_DATA]))
    die('ERROR');
?>
<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo ($data_auth[FORM_ACTION] == ACTION_ADD ? get_title_add($data_auth[FORM_AUTH][FORM_TITLE]) : get_title_edit($data_auth[FORM_AUTH][FORM_TITLE])); ?></h3>
    </div>
    <form class="form-horizontal" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
	<?php echo get_csrf_admin_token(); ?>
        <input name ="seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->seq; ?>">
        <input name ="date_from" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->date_from; ?>">
        <input name ="date_to" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->date_to; ?>">
        <input name ="active" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->active; ?>">
        <input name ="all_origin_city" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->all_origin_city; ?>">
        <input name ="all_destination_city" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->all_destination_city; ?>">
        <input name ="status" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->status; ?>">
        <div class="box-body">
            <div class ="form-group">
		<label class ="control-label col-md-2">Periode</label>
		<div class ="col-md-8">
		    <input class="form-control" date_type="date" id ="a0" name ="a0" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? date("d-M-Y", strtotime($data_sel[LIST_DATA][0]->date_from)) . ' - ' . date("d-M-Y", strtotime($data_sel[LIST_DATA][0]->date_to)) : "" ); ?>" readonly>
		</div>
	    </div>
	    <div class ="form-group">
		<label class ="control-label col-md-2">Keterangan</label>
		<div class ="col-md-8">
		    <input class="form-control" id="notes" readonly name ="notes" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->notes) : "" ); ?>">
		</div>
	    </div>
            <div class ="form-group">
                <div class ="col-md-offset-3 col-md-9">
                    <label> <input type="checkbox" name ="a1" <?php echo ($data_sel[LIST_DATA][0]->active == "1" ? "checked" : ""); ?> disabled/> Aktif</label>
                    <label> <input type="checkbox" name ="a2" <?php echo ($data_sel[LIST_DATA][0]->all_origin_city == "1" ? "checked" : ""); ?> disabled/> Semua Kota Asal Merchant</label>
                    <label> <input type="checkbox" name ="a3" <?php echo ($data_sel[LIST_DATA][0]->all_destination_city == "1" ? "checked" : ""); ?> disabled/> Semua Kota Tujuan Member</label>
                </div>
            </div>
        </div>
	<?php
	$merchantcombo = '';
	if (isset($tree_menu)) {
	    if ($tree_menu != "") {
		foreach ($tree_menu as $value) {
		    $merchantcombo.='<option value="' . $value->seq . '" ' . (($value->merchant_seq == $value->seq) ? 'selected' : '') . '>' . $value->merchant_name . '</option>';
		}
	    }
	}
	?>
        <div class="box-footer no-border">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <i class="fa fa-users"></i>
                            <h3 class="box-title">Merchant</h3>
                        </div>
                        <div class="box-body" id="data_origin">
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label>Pilih Merchant</label>
                                        <select size="20" class="form-control" name="merchant_seq[]" id="merchant_seq" multiple="" style=" font-size: 15px;">
					    <option value="">-- Pilih --</option>
					    <?php
					    echo $merchantcombo;
					    ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.box-body-->
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class ="col-md-6"><?php echo (($data_sel[LIST_DATA][0]->status == 'N') ? get_save_add_button() : ''); ?> </div>
                <div class ="col-md-6"><?php echo get_cancel_link(get_base_url() . "admin/master/promo_free_fee"); ?></div>
            </div>
        </div>
    </form>
</div>
<?php
require_once get_include_page_list_admin_content_footer();
require_once get_include_content_admin_bottom_page_navigation();
?>