<?php
require_once VIEW_BASE_ADMIN;
?>

<header class="main-header">
    <a href="<?php echo base_url() ?>admin/main_page" class="logo" style="padding: 0px; background-color:#232323;">
        <span class="logo-mini"><img src="<?php echo get_img_url(); ?>black_50x50.jpg"></span>
        <span class="logo-lg"><img src="<?php echo get_img_url(); ?>balack_230x50.jpg"></span>
    </a>
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="width: 270px; text-align: center; " id="user-options">
                        <span>Halo, <?php echo ($data_prof[USER_NAME]) ?> &nbsp;&nbsp;
                        <i class='fa fa-caret-down '></i></span>
                    </a>
                    <ul class="dropdown-menu" style="width: 270px">
                        <li class="user-body">
                            <div class="form-group">
                                <i class="fa fa-pencil"></i>
                                <a href="<?php echo get_base_url(); ?>admin/profile/change_password">Ubah Password</a>
                            </div>
                            <div class="form-group">
                                <i class="fa fa-sign-out"></i>
                                <a href="<?php echo get_base_url() ?>logout/admin/login">Keluar</a>
                            </div>
                            <div class="form-group">
                                <i class="fa fa-clock-o"></i>
                                <a>Login Terakhir : <?php echo $data_prof[LAST_LOGIN] ?></a>
                            </div>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" data-toggle="control-sidebar" class="control-search"><i class="fa fa-search"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>