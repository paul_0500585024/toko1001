<?php
require_once VIEW_BASE_ADMIN;
?>

<aside class="main-sidebar">
    <section class="sidebar">
        <!--        <div class="user-panel">
                    <div class="pull-left image">
                        <img src="<?php // echo get_img_url()  ?>avatar5.png" class="img-circle" alt="User Image" />
                    </div>
                    <div class="pull-left info">
                        <p><?php // echo $data_prof[USER_NAME]  ?></p>
                        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                    </div>
                </div>-->
        <ul class="sidebar-menu">
            <!--            <li class="treeview">
                            <a href="#"><i class='fa fa-folder-o'></i> <span>Master</span> <i class="fa fa-angle-left pull-right"></i></a>-->
            <?php echo $data_left /* ?> 
              <!--                <ul class="treeview-menu">
              <li>
              <a href="#"><i class='fa fa-gears'></i> <span>General Setting</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
              <li><a href="<?php echo get_base_url(); ?>admin/master/province"><i class='fa fa-globe'></i><span>Propinsi</span></a></li>
              </ul>
              <ul class="treeview-menu">
              <li><a href="<?php echo get_base_url(); ?>admin/master/city"><i class='fa fa-globe'></i><span>Kota</span></a></li>
              </ul>
              <ul class="treeview-menu">
              <li><a href="<?php echo get_base_url(); ?>admin/master/district"><i class='fa fa-globe'></i><span>Kecamatan</span></a></li>
              </ul>
              <ul class="treeview-menu">
              <li><a href="<?php echo get_base_url(); ?>admin/master/setting_type"><i class='fa fa-globe'></i><span>Tipe Setting</span></a></li>
              </ul>
              <ul class="treeview-menu">
              <li><a href="<?php echo get_base_url(); ?>admin/master/setting"><i class='fa fa-globe'></i><span>Setting</span></a></li>
              </ul>
              <ul class="treeview-menu">
              <li><a href="<?php echo get_base_url(); ?>admin/master/bank"><i class='fa fa-globe'></i><span>Rekening Bank</span></a></li>
              </ul>
              <ul class="treeview-menu">
              <li><a href="<?php echo get_base_url(); ?>admin/master/attribute_category"><i class='fa fa-globe'></i><span>Kategori Atribut</span></a></li>
              </ul>
              <ul class="treeview-menu">
              <li><a href="<?php echo get_base_url(); ?>admin/master/expedition"><i class='fa fa-globe'></i><span>Ekspedisi</span></a></li>
              </ul>
              <ul class="treeview-menu">
              <li><a href="<?php echo get_base_url(); ?>admin/master/promo_free_fee"><i class='fa fa-globe'></i><span>Gratis Ongkos Kirim</span></a></li>
              </ul>
              <ul class="treeview-menu">
              <li><a href="<?php echo get_base_url(); ?>admin/master/promo_voucher_periode"><i class='fa fa-globe'></i><span>Jenis Voucher</span></a></li>
              </ul>
              <ul class="treeview-menu">
              <li><a href="<?php echo get_base_url(); ?>admin/master/variant_product"><i class='fa fa-globe'></i><span>Varian Produk</span></a></li>
              </ul>
              <ul class="treeview-menu">
              <li><a href="<?php echo get_base_url(); ?>admin/master/product_attribute"><i class='fa fa-globe'></i><span>Atribut Produk</span></a></li>
              </ul>
              <ul class="treeview-menu">
              <li><a href="<?php echo get_base_url(); ?>admin/master/product_category"><i class='fa fa-globe'></i><span>Kategori Produk</span></a></li>
              </ul>
              <ul class="treeview-menu">
              <li><a href="<?php echo get_base_url(); ?>admin/master/image"><i class='fa fa-globe'></i><span>Gambar Kategori</span></a></li>
              </ul>
              <ul class="treeview-menu">
              <li><a href="<?php echo get_base_url(); ?>admin/master/menu"><i class='fa fa-globe'></i><span>menu</span></a></li>
              </ul>
              <ul class="treeview-menu">
              <li><a href="<?php echo get_base_url(); ?>admin/master/user_admin"><i class='fa fa-globe'></i><span>Pengguna</span></a></li>
              </ul>
              <ul class="treeview-menu">
              <li><a href="<?php echo get_base_url(); ?>admin/master/user_group"><i class='fa fa-globe'></i><span>Kelompok Pengguna</span></a></li>
              </ul>
              <ul class="treeview-menu">
              <li><a href="<?php echo get_base_url(); ?>admin/master/payment_gateway"><i class='fa fa-globe'></i><span>Payment</span></a></li>
              </ul>
              <ul class="treeview-menu">
              <li><a href="<?php echo get_base_url(); ?>admin/master/image_front"><i class='fa fa-globe'></i><span>Gambar Hal Utama</span></a></li>
              </ul>
              <ul class="treeview-menu">
              <li><a href="<?php echo get_base_url(); ?>admin/master/expedition_city"><i class='fa fa-globe'></i><span>Kota Ekspedisi</span></a></li>
              </ul>
              </li>
              <li>
              <a href="<?php echo get_base_url(); ?>admin/master/merchant"><i class='fa fa-users'></i> <span>Merchant</span></a>
              </li>
              <li>
              <a href="<?php echo get_base_url(); ?>admin/master/merchant_log"><i class='fa fa-users'></i> <span>Perubahan Data Merchant</span></a>
              </li>
              <li>
              <a href="<?php echo get_base_url(); ?>admin/master/product_new"><i class='fa fa-cubes'></i> <span>Pengajuan Produk</span></a>
             * -->
              <?php */ ?>
            <!--                    </li>
                            </ul>
                        </li>-->
        </ul>
    </section>
</aside>