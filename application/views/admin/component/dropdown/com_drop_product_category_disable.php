<select class="form-control select2" style='width : 100%' name="category_seq" validate ="required[]" type="input" <?php echo $data_auth[FORM_ACTION] == ACTION_EDIT ? "disabled" : "" ?>>
    <option value="">-- Pilih --</option>
    <?php foreach ($category_name as $each) { ?>
        <option value="<?php echo $each->seq; ?>" <?php echo (isset($data_sel[LIST_DATA]) && $each->seq == $data_sel[LIST_DATA][0]->category_seq) ? "selected" : "" ?>><?php echo $each->name; ?></option>
    <?php } ?>
</select>