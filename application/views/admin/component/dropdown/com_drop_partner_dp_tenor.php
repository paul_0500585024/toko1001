<select id="drop_tenor" class="form-control select2" style='width : 100%' name="partner_loan_seq">
    <option value="">-- Pilih --</option>
    <?php
    if (isset($tenor_loan)) {
        foreach ($tenor_loan as $each) {
            ?>
            <option value="<?php echo $each->seq; ?>" <?php echo (isset($data_sel[LIST_DATA]) && $each->seq == $data_sel[LIST_DATA][0]->partner_loan_seq) ? "selected" : "" ?>><?php echo get_display_value($each->tenor_loan); ?></option>
        <?php
        }
    }
    ?>
</select>


