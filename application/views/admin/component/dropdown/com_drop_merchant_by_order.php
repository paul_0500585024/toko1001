<select class="form-control select2" id ="drop_merchant" style='width : 100%' name="merchant_seq" type="input">
    <option value="">-- Pilih --</option>
    <?php
    if (isset($merchant_name)) {
        foreach ($merchant_name as $each) {
            ?>
            <option value="<?php echo $each->merchant_seq; ?>" <?php echo (isset($data_sel[LIST_DATA]) && $each->merchant_seq == $data_sel[LIST_DATA][0]->merchant_seq) ? "selected" : "" ?>><?php echo $each->merchant_name; ?></option>
            <?php
        }
    }
    ?>
</select>

