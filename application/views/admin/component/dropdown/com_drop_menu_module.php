
<select class="form-control select2" id ="drop_module" validate ="required[]" style='width : 100%' name="module_seq" type="input" >
    <option value="">-- Pilih --</option>
    <?php foreach ($module_name as $each) { ?>
        <option value="<?php echo $each->seq; ?>" <?php echo (isset($data_sel[LIST_DATA][0]->module_seq) && $each->seq == $data_sel[LIST_DATA][0]->module_seq) ? "selected" : (!isset($data_sel[LIST_DATA]) && $each->seq == DEFAULT_MODULE_SEQ ? "selected" : "") ?>><?php echo $each->name; ?></option>
    <?php } ?>
</select>