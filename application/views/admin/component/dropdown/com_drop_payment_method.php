<select id="drop_payment_method" validate="required[]" class="form-control select2" style='width : 100%' name="pg_method">
    <option value="">-- Pilih --</option>
    <?php if (isset($payment_name)) {
        foreach ($payment_name as $each) {
            ?>
            <option value="<?php echo $each->seq; ?>"><?php echo $each->name; ?></option>
        <?php }
    }
    ?>
</select>
