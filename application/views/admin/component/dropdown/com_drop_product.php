<select class="form-control select2" validate ="required[]" style='width : 100%' name="product_seq" type="input" >
    <option value="">-- Pilih --</option>
    <?php foreach ($product as $each) { ?>
        <option value="<?php echo $each->seq; ?>" <?php echo (isset($data_sel[LIST_DATA]) && $each->seq == $data_sel[LIST_DATA][0]->product_seq) ? "selected" : "" ?>><?php echo $each->product; ?></option>
    <?php } ?>
</select>
