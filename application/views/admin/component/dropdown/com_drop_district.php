<select id="drop_district" validate="required[]" class="form-control select2" style='width : 100%' name="district_seq">
    <option value="">-- Pilih --</option>
    <?php
    if (isset($district_name)) {
        foreach ($district_name as $each) {
            ?>
            <option value="<?php echo $each->seq; ?>" <?php echo (isset($data_sel[LIST_DATA]) && $each->seq == $data_sel[LIST_DATA][0]->district_seq) ? "selected" : "" ?>><?php echo $each->name; ?></option>
            <?php
        }
    }
    ?>
</select>