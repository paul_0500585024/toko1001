<select id="expedition_seq" class="form-control select2" style='width : 100%' name="expedition_seq" required="">
    <option value="">-- Pilih --</option>
    <?php
    if (isset($exp_name)) {
	foreach ($exp_name as $each) {
	    ?>
	    <option value="<?php echo $each->seq; ?>" <?php echo (isset($data_sel[LIST_DATA]) && $each->seq == $data_sel[LIST_DATA][0]->expedition_seq) ? "selected" : "" ?>><?php echo $each->name; ?></option>
	    <?php
	}
    }
    ?>
</select>