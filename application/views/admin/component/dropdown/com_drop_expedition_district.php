<select id="drop_exp_dist" class="form-control select2" style='width : 100%' name="exp_district_code">
    <option value="">-- Pilih --</option>
    <?php
    if (isset($exp_district_code)) {
	foreach ($exp_district_code as $each) {
	    ?>
	    <option value="<?php echo $each->seq; ?>" <?php echo (isset($data_sel[LIST_DATA]) && $each->seq == $data_sel[LIST_DATA][0]->exp_district_code) ? "selected" : "" ?>><?php echo $each->exp_district_code; ?></option>
	    <?php
	}
    }
    ?>
</select>