<?php
require_once VIEW_BASE_PARTNER;
require_once get_include_content_partner_top_page_navigation();
echo $breadcrumb;
?>
<br>
<div class="col-xs-12">
    <div class="box box-default no-border">
        <div class="box-header no-border">
            <h3 class="box-title">
                <?php if (isset($data_list[LIST_DATA][0])) { ?>
                    Daftar Redeem Partner Periode <?php echo cdate($data_list[LIST_DATA][0]->from_date) ?> - <?php echo cdate($data_list[LIST_DATA][0]->to_date) ?>
                <?php } else { ?>
                    Daftar List Redeem Partner Periode
                <?php } ?>
            </h3>
        </div>
        <div class="box-body" style="display: block;">
            <br>
            <div class="col-xs-3 pull-right">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-6 pull-right">
                                <button class="btn btn-success btn-block btn-block flat" type="button" id="btnxls" name="btnxls" onclick="data_report()"><i class="fa fa-file-excel-o"></i> Excel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <div class="table-responsive">
                <table class="table-order table no-margin">
                    <thead>
                        <tr>
                            <th class="tb-paid sorting text-center" column="order_no" order="">Order No.</th>
                            <th class="tb-paid sorting text-center" column="name" order="">Nama Agen</th>
                            <th class="tb-paid sorting text-center" column="product_name" order="">Nama Produk</th>
                            <th class="tb-paid sorting text-right pr-20px" column="qty" order="">Qty</th>
                            <th class="tb-paid sorting text-right pr-20px" column="sell_price" order="">Harga(Rp)</th>
                            <th class="tb-paid sorting text-right pr-20px" column="total_ship_charged" order="">Ekspedisi(Rp)</th>
                            <th class="tb-paid sorting text-right pr-20px" column="total_payment" order="">Total (Rp)</th>
                            <th class="tb-paid sorting text-right pr-20px" column="commission_fee_percent" order="">Komisi (%)</th>
                            <th class="tb-paid sorting text-right pr-20px" column="total_commission" order="">Komisi (Rp)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (isset($data_list[LIST_DATA])) { ?>
                            <?php foreach ($data_list[LIST_DATA] as $each) { ?>
                                <tr>
                                    <td class="text-center"><?php echo $each->order_no ?></td>
                                    <td class="text-center"><?php echo $each->name ?></a></td>
                                    <td class="text-center"><?php echo $each->product_name ?></td>
                                    <td class="text-right"><?php echo $each->qty ?></td>
                                    <td class="text-right"><?php echo cnum($each->sell_price) ?></td>
                                    <td class="text-right"><?php echo cnum($each->total_ship_charged) ?></td>
                                    <td class="text-right"><?php echo cnum($each->total_payment) ?></td>
                                    <td class="text-right"><?php echo $each->commission_fee_percent ?></td>
                                    <td class="text-right"><?php echo cnum($each->total_commission) ?></td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="col-xs-12 no-pad">
    <div class="box box-default no-border">
        <div class="col-xs-12">
            <div class="box-pagging">
                <center><?php echo $this->pagination->create_links(); ?></center>
            </div>
        </div>
    </div>
</div>

<?php
require_once get_include_page_list_partner_content_footer();
?>
<script>
    function data_report() {
        var data = GET;
        data.act = '<?php echo ACTION_ADDITIONAL ?>';
        data.type = 'print_excel';
        $.ajax({
            url: '<?php echo base_url() . $data_auth[FORM_URL] ?>',
            type: 'GET',
            data: data,
            success: function (response) {
                try {
                    response = jQuery.parseJSON(response);
                    if (response != null) {
                        create_report(response);
                    } else {
                        alert("Tidak ada data.");
                    }
                } catch (e) {
                    alert("Tidak ada data.");
                }
                return false;
            },
            error: function (res) {

            }
        });
        return false;
    }
</script>
<?php
require_once get_include_content_partner_bottom_page_navigation();
?>
