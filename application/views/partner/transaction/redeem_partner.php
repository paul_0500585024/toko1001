<?php
require_once VIEW_BASE_PARTNER;
require_once get_include_content_partner_top_page_navigation();
echo $breadcrumb;
?>

<style>
    .daterangepicker{
        width:250px;
    }
    .table-condensed thead tr:nth-child(2),
    .table-condensed tbody {
        display: none
    }
    .btn-date-back{
        position: absolute;
        top: -1px;
        height: 30px;
        width: 30px;
        left: -36px;
        z-index: 999
    }
    .parent-button-in{position: relative;display: inline-block}
    .w-25-perc{width: 25%}
    .top-min-3px{margin-top: -3px}
    .top-min-10px{margin-top: -10px}
</style>

<div class="p-10px">
    <div class="box box-default no-border no-margin">
        <div class="box-body">  
            <div class="col-xs-3 pull-right">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-6 pull-right">
                                <button class="btn btn-success btn-block btn-block flat" type="button" id="btnxls" name="btnxls" onclick="data_report()"><i class="fa fa-file-excel-o"></i> Excel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <form id ="frmSearchExcl" method ="get" url= "<?php echo get_base_url() . $data_auth[FORM_URL]; ?>" class="relative">
                <input date_type="date" readonly class="form-control-cst w-25-perc" id="order_date" name="paid_date" type="text" placeholder="Tanggal Bayar" 
                       value="<?php echo $this->input->get('paid_date'); ?>"/>
                <div class="parent-button-in">
                    <button class="close-icon btn-date-back" id="btn-search" type="reset">X</button>
                    <button class="btn btn-danger top-min-3px" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="p-10px top-min-10px">
    <div class="box box-default no-border no-margin">
        <div class="box-body">  
            <div class="table-responsive">
                <br><br>
                <table class="table no-margin table-order">
                    <thead>
                        <tr>
                            <th class="tb-paid sorting text-center" column="to_date" order="">Periode</th>
                            <th class="tb-paid sorting text-center" column="paid_date" order="">Tanggal Bayar</th>
                            <th class="tb-paid sorting text-right pr-20px" column="total_commission" order="">Total Komisi (<?php echo RP ?>)</th>
                            <th class="tb-paid sorting text-right pr-20px" column="total_order" order="">Total Order (<?php echo RP ?>)</th>
                            <th class="tb-paid text-center">Rincian</th>
                            <th class="tb-paid text-center">Komisi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (isset($data_list[LIST_DATA])) { ?>
                            <?php foreach ($data_list[LIST_DATA] as $each) { ?>
                                <tr>
                                    <td class="text-center"><?php echo cdate($each->from_date) ?> - <?php echo cdate($each->to_date) ?></td>
                                    <td class="text-center"><?php echo cdate($each->paid_date) ?></td>
                                    <td class="text-right"><?php echo cnum($each->total_commission) ?></td>
                                    <td class="text-right"><?php echo cnum($each->total_order) ?></td>
                                    <td class="text-center"><a href="<?php echo base_url() . 'partner/redeem_detail_partner?key=' . $each->seq ?>">Rincian Redeem</a></td>
                                    <td class="text-center"><a href="<?php echo base_url() . 'partner/redeem_detail_partner?key=' . $each->seq . '&type=' . 'redem' ?>">Komisi</a></td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                    </tbody>
                </table>
                <div class="col-xs-12 no-pad">
                    <div class="box-pagging">
                        <center><?php echo $this->pagination->create_links(); ?></center>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>

        $("#btn-search").on("click", function () {
            var curr_url = window.location.href;
            var prev_url = curr_url.split("?");
            window.location = prev_url[0];
        })

        $('input[date_type="date"]').daterangepicker({
            format: 'MMM-YYYY',
            singleDatePicker: true,
            showDropdowns: true,
            showWeekNumbers: false
        }).on('hide.daterangepicker', function (ev, picker) {
            $('.table-condensed tbody tr:nth-child(2) td').click();
        });

        function data_report() {
            var data = $('#frmSearchExcl').serializeArray();
            data.push({name: 'type', value: 'print_data_excel'}, {name: '<?php echo CONTROL_GET_TYPE ?>', value: '<?php echo ACTION_ADDITIONAL ?>'});
            $.ajax({
                url: "<?php echo base_url() . $data_auth[FORM_URL]; ?>",
                data: data,
                type: "get",
                success: function (response) {
                    if (isSessionExpired(response)) {
                    } else {
                        try {
                            response = jQuery.parseJSON(response);
                            if (response != null) {
                                create_report(response);
                            } else {
                                alert("Tidak ada data.");
                            }
                        } catch (e) {
                            alert("Tidak ada data.");
                        }
                        return false;
                    }
                },
                error: function (request, error) {
                    alert(error_response(request.status));
                }
            });
        }
    </script>


    <?php
    require_once get_include_page_list_partner_content_footer();
    require_once get_include_content_partner_bottom_page_navigation();
    ?>