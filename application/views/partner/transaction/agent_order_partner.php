<?php
require_once VIEW_BASE_PARTNER;
require_once get_include_content_partner_top_page_navigation();
echo $breadcrumb;

function get_qty_status($arraystatus, $val) {
    if (isset($arraystatus[0])) {
        foreach ($arraystatus[0] as $key => $obj) {
            if ($obj->payment_status == $val) {
                return $obj->qty_status;
                break;
            }
        }return 0;
    } else {
        return 0;
    }
}

function timeline_status($status = '', $status_date = '') {
    $ret = '';
    if ($status_date != '' && $status_date != '0000-00-00 00:00:00' && $status_date != '0000-00-00' && $status_date != null) {
        $ret = '<tr><td>' . date("d M y", strtotime($status_date)) . '</td><td>' . get_display_status($status, STATUS_HISTORY) . '</td></tr>';
    }
    return $ret;
}
?>
<style>
    div {
        font-size:12px;
    }
</style>
<?php
if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT or $data_auth[FORM_ACTION] == ACTION_VIEW) {
    $statusorder = $data_sel[LIST_DATA][0][0]->order_status;
    ?>
    <div class="p-10px">
        <div class="box box-default no-border no-margin">
            <form class="form-horizontal" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>" name="form1" id="form1">
                <div class="box-body">
                    <section class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12">
                                <?php echo get_csrf_partner_token(); ?>
                                <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                                    <input class="form-control"  name ="seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0][0]->seq; ?>">
                                <?php } ?>

                                <div class="row">
                                    <div class="col-xs-6">
                                        <div><strong>Detail Order</strong></strong>
                                            <div class="margin-left-15px">
                                                <div>Nomor Order : <?php echo $data_sel[LIST_DATA][0][0]->order_no; ?></div>
                                                <div>Tanggal Order : <?php echo date("d-M-Y", strtotime($data_sel[LIST_DATA][0][0]->order_date)); ?></div>
                                                <div>Status Order : <?php echo status($data_sel[LIST_DATA][0][0]->payment_status, STATUS_PAYMENT); ?></div>
                                                <div>Metode Pembayaran : <?php echo $data_sel[LIST_DATA][0][0]->payment_method; ?></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-6">
                                        <div><strong>Penerima</strong></div>
                                        <div class="margin-left-15px">
                                            <strong>
                                                <div><?php echo $data_sel[LIST_DATA][0][0]->receiver_name; ?></div>
                                            </strong>
                                            <div><?php echo $data_sel[LIST_DATA][0][0]->receiver_address; ?></div>
                                            <div><?php echo $data_sel[LIST_DATA][2][0]->d_name; ?>, <?php echo $data_sel[LIST_DATA][2][0]->c_name; ?></div>
                                            <div><?php echo $data_sel[LIST_DATA][2][0]->p_name; ?>, Kode Pos <?php echo $data_sel[LIST_DATA][0][0]->receiver_zip_code; ?></div>
                                            <div><?php echo $data_sel[LIST_DATA][0][0]->receiver_phone_no; ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr style="border: 1px dashed #f4f4f4;">

                        <strong>Produk yang dibeli</strong>
                        <div class="margin-left-15px">
                            <?php
                            $total = 0;
                            $order_history = '';
                            $iterate = 0;
                            if (isset($data_ord[1])) {
                                foreach ($data_ord[1] as $merchant) {
                                    $iterate++;
                                    ?>
                                    <div class="row reset-margin merchant-header">
                                        <div class="col-xs-3">
                                            <a href = "<?php echo base_url() . "merchant/" . $merchant->merchant_code; ?>" ><h6><?php echo $merchant->merchant_name; ?></h6></a>
                                        </div>
                                        <div class="col-xs-7"><input  type = "text"  class = "form-control input-sm" value="<?php echo $merchant->member_notes; ?>" readonly="" maxlength="100"></div>
                                        <div class="col-xs-2">
                                            <div class="text-align-right">
                                                <div><?php echo "<b>(" . $merchant->total_product . ")</b> Produk"; ?></div>
                                                <div>
                                                    <div>Shipping :  <?php echo $merchant->expedition_name; ?></div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <?php
                                    $total_commision_agent = 0;
                                    foreach ($data_ord[2] as $prod) {
                                        if ($prod->product_status == PRODUCT_READY_STATUS_CODE)
                                            $total_commision_agent = ($total_commision_agent + ($prod->qty * $prod->commission_fee_percent * $prod->sell_price / 100));
                                        if ($merchant->merchant_seq == $prod->merchant_seq) {
                                            ?>
                                            <div class="row reset-margin un-style">
                                                <div class="col-xs-2">
                                                    <a href ="<?php echo base_url() . url_title($prod->display_name . " " . $prod->product_seq); ?>"><center><img src="<?php echo PRODUCT_UPLOAD_IMAGE . $prod->merchant_seq . "/" . S_IMAGE_UPLOAD . $prod->img; ?>" alt="<?php echo $prod->img; ?>" width = "100px"></center></a>
                                                </div>
                                                <div class="col-xs-6">
                                                    <div class="row">
                                                        <div class="col-xs-12 cart-font-color-blue no-pad"><?php echo $prod->display_name ?></div>
                                                    </div>
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-xs-12 no-pad">
                                                            <div class="col-xs-6 no-pad">
                                                                <div class="col-xs-3 pad-auto">
                                                                    Warna
                                                                </div>
                                                                <div class="col-xs-1 pad-auto">
                                                                    &nbsp;:&nbsp;
                                                                </div>
                                                                <div class="col-xs-6 no-pad">
                                                                    <?php echo $prod->variant_name; ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-6 no-pad">
                                                                <div class="col-xs-3 pad-auto">
                                                                    No Resi
                                                                </div>
                                                                <div class="col-xs-1 pad-auto">
                                                                    &nbsp;:&nbsp;
                                                                </div>
                                                                <div class="col-xs-7 no-pad">
                                                                    <?php echo $prod->product_status == PRODUCT_CANCEL_BY_MERCHANT_STATUS_CODE ? "" : $merchant->awb_no; ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-xs-12 no-pad">
                                                            <div class="col-xs-6 no-pad">
                                                                <div class="col-xs-3 pad-auto">
                                                                    Berat
                                                                </div>
                                                                <div class="col-xs-1 pad-auto">
                                                                    &nbsp;:&nbsp;
                                                                </div>
                                                                <div class="col-xs-6 no-pad"><?php echo $prod->weight_kg ?> Kg</div>
                                                            </div>
                                                            <div class="col-xs-6 no-pad">
                                                                <div class="col-xs-3 pad-auto">
                                                                    Status Pengiriman
                                                                </div>
                                                                <div class="col-xs-2 pad-auto">&nbsp;:&nbsp;</div>
                                                                <div class="col-xs-7 no-pad">
                                                                    <?php
                                                                    $data = json_decode(STATUS_ORDER, true);
                                                                    $data_prod = json_decode(STATUS_XR, true);
                                                                    echo $prod->product_status == PRODUCT_CANCEL_BY_MERCHANT_STATUS_CODE ? $data_prod[$prod->product_status] : $data[$merchant->order_status];
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 pad-auto">
                                                            Biaya pengiriman : <b><?php echo $prod->ship_price_charged == 0 ? "<span class='text-green'>Free</span>" : "Rp. " . number_format(ceil($prod->weight_kg * $prod->qty) * $prod->ship_price_charged); ?></b>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 cart-font-color-gray">
                                                    <div class="row"><div class="col-xs-12">&nbsp;</div></div>
                                                    <div class="row"><div class="col-xs-12">&nbsp;</div></div>

                                                    <div class="row">
                                                        <div class="col-xs-4 no-pad text-align-right">
                                                            Rp. <?php echo number_format($prod->sell_price); ?>
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <center><input class="input-style" readonly value="<?php echo number_format($prod->qty); ?>"/></center>
                                                        </div>
                                                        <div class="col-xs-6 no-pad">
                                                            Rp.  <?php
                                                            echo number_format($prod->qty * $prod->sell_price + ceil($prod->weight_kg * $prod->qty) * $prod->ship_price_charged);
                                                            $total = $total + $prod->qty * $prod->sell_price + ceil($prod->weight_kg * $prod->qty) * $prod->ship_price_charged;
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    if (isset($_COOKIE[VIEW_MODE]) AND $_COOKIE[VIEW_MODE] != '' and isset($_SESSION[SESSION_AGENT_CSRF_TOKEN])) {
                                                        if ($prod->product_status == PRODUCT_READY_STATUS_CODE) {
                                                            if (isset($prod->commission_fee_percent)) {
                                                                ?>
                                                                <div class="row"><div class="col-xs-12">&nbsp;</div></div>
                                                                <div class="row">
                                                                    <div class="col-xs-2">Komisi:</div>
                                                                    <div class="col-xs-4 no-pad text-align-right">Rp. <?php echo number_format($prod->commission_fee_percent * $prod->sell_price / 100); ?></div>
                                                                    <div class="col-xs-6 no-pad">Rp. <?php echo number_format($prod->qty * $prod->commission_fee_percent * $prod->sell_price / 100); ?></div>
                                                                </div>
                                                                <?php
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    }
                                    $auth_date_adira = '';
                                    $order_history.=timeline_status(ORDER_NEW_ORDER_STATUS_CODE, $data_ord[0][0]->order_date);
                                    if ($data_ord[0][0]->pg_method_seq == PAYMENT_SEQ_ADIRA) {
                                        if ($data_ord[0][0]->status_order == REJECT_STATUS_CODE) {
                                            $auth_date_adira = '<tr><td>' . date("d M y", strtotime($data_ord[0][0]->auth_date)) . '</td><td>' . cstdes("R", STATUS_LOAN) . '</td></tr>';
                                        } elseif ($data_ord[0][0]->status_order != NEW_STATUS_CODE) {
                                            $auth_date_adira = '<tr><td>' . date("d M y", strtotime($data_ord[0][0]->auth_date)) . '</td><td>' . cstdes("A", STATUS_LOAN) . '</td></tr>';
                                        }
                                    }
                                    $order_history.=$auth_date_adira;
                                    if ($data_ord[0][0]->payment_status == PAYMENT_CONFIRM_STATUS_CODE) {
                                        $order_history.=timeline_status($data_ord[0][0]->payment_status, $data_ord[0][0]->conf_pay_date);
                                    }
                                    if ($data_ord[0][0]->payment_status == PAYMENT_PAID_STATUS_CODE) {
                                        $order_history.=timeline_status(PAYMENT_CONFIRM_STATUS_CODE, $data_ord[0][0]->conf_pay_date);
                                        $order_history.=timeline_status(PAYMENT_PAID_STATUS_CODE, $data_ord[0][0]->paid_date);
                                        if ($merchant->order_status != ORDER_CANCEL_BY_MERCHANT_STATUS_CODE && $merchant->order_status != ORDER_CANCEL_BY_SYSTEM_STATUS_CODE) {
                                            $order_history.=timeline_status(ORDER_READY_TO_SHIP_STATUS_CODE, $merchant->print_date);
                                            $order_history.=timeline_status(ORDER_SHIPPING_STATUS_CODE, $merchant->ship_date);
                                            $order_history.=timeline_status(ORDER_DELIVERED_STATUS_CODE, $merchant->received_date);
                                            if ($merchant->order_status == ORDER_DELIVERED_STATUS_CODE) {
                                                if (strtotime("+" . FINISH_DATE . " days", strtotime($merchant->received_date)) < strtotime(date("Y-m-d H:i:s"))) {
                                                    $order_history.=timeline_status("FINISH", date("Y-m-d", strtotime("+" . FINISH_DATE . " days", strtotime($merchant->received_date))));
                                                }
                                            }
                                        } else {
                                            $order_history.=timeline_status("XM", $merchant->modified_date);
                                        }
                                    } else {
                                        switch ($data_ord[0][0]->payment_status) {
                                            case ORDER_CANCEL_BY_MERCHANT_STATUS_CODE:
                                                $order_history.=timeline_status($data_ord[0][0]->payment_status, $data_ord[0][0]->paid_date);
                                                break;
                                            case ORDER_FAIL_STATUS_CODE:
                                                $order_history.=timeline_status($data_ord[0][0]->payment_status, $data_ord[0][0]->paid_date);
                                                break;
                                            case "W":
                                                // $order_history.=timeline_status($data_ord[0][0]->payment_status, $data_ord[0][0]->paid_date);
                                                break;
                                        }
                                    }
                                    ?>
                                    <div class="row reset-margin un-style"><a href="javascript:trackers(<?php echo $iterate;
                                    ?>)">Riwayat Status</a><br />
                                        <div class="col-xs-1"></div>
                                        <div class="col-xs-8">
                                            <table id="timeline<?php echo $iterate; ?>" style="display:none;background-color: #fff !important" class="table">
                                                <thead><tr><th>Tanggal</th><th>Status</th></tr></thead>
                                                <?php echo $order_history; ?>
                                            </table>
                                        </div>
                                    </div>
                                    <?php
                                    $order_history = '';
                                }
                            }
                            ?>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">     
                            </div>
                            <div class="col-xs-6">
                                <div class="row">
                                    <div class="col-xs-8">
                                        <div style="text-align: right">Total Belanja : </div>
                                    </div>
                                    <div class="col-xs-4 no-pad">
                                        <b>Rp. <?php echo number_format($total); ?></b>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                            </div>
                            <div class="col-xs-6">
                                <div class="row">
                                    <div class="col-xs-8">
                                        <div style="text-align: right">Total Pembayaran : </div>
                                    </div>
                                    <div class="col-xs-4 no-pad">
                                        <b class="font-color-red">Rp. <?php echo number_format($data_ord[0][0]->total_payment); ?></b>
                                    </div>
                                </div>

                                <?php
                                if (isset($_COOKIE[VIEW_MODE]) AND $_COOKIE[VIEW_MODE] != '' and isset($_SESSION[SESSION_AGENT_CSRF_TOKEN])) {
                                    ?>
                                    <div class="row"><br/>
                                        <div class="col-xs-8 commission-price">
                                            <div style="text-align: right">Total Komisi : </div>
                                        </div>
                                        <div class="col-xs-4 no-pad">
                                            <b class="commission-price">Rp. <?php echo number_format($total_commision_agent); ?></b>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>

                        <?php if ($data_sel[LIST_DATA][0][0]->payment_seq == PAYMENT_SEQ_ADIRA || $data_sel[LIST_DATA][0][0]->payment_seq == PAYMENT_SEQ_ADIRA_DEPOSIT) { ?>
                            <hr style="border: 1px dashed #f4f4f4;">
                            <div class="row">
                                <div class="col-xs-12">
                                    <?php
                                    if (isset($data_sel[LIST_DATA][1])) {
                                        foreach ($data_sel[LIST_DATA][1] as $each) {
                                            ?>
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <strong>Perhitungan Angsuran</strong>
                                                    <div>Harga Barang
                                                        <?php
                                                        $total_price = $each->sell_price * $each->qty;
                                                        ?>

                                                        <span class="pull-right text-bold"> Rp.<?php echo number_format($total_price); ?></span></div>
                                                    <div>Uang Muka (DP) <span class="pull-right text-bold">Rp.<?php echo number_format($data_sel[LIST_DATA][0][0]->dp) ?></span></div>
                                                    <div><span id="min_dp" class="text-red pull-right"></span></div>
                                                    <div>Pokok Hutang  <span id="pokok_hutang" class="pull-right text-bold">Rp.<?php echo number_format($total_price - $data_sel[LIST_DATA][0][0]->dp) ?></span></div>
                                                    <div>Tenor <span class="pull-right text-bold"><?php echo number_format($data_sel[LIST_DATA][0][0]->tenor) ?> bulan</span></div>
                                                    <div>Bunga <span class="pull-right text-bold"><?php echo $data_sel[LIST_DATA][0][0]->loan_interest ?> %</span></div>
                                                    <div>Angsuran <span class="pull-right text-bold">Rp.<?php echo number_format($data_sel[LIST_DATA][0][0]->installment) ?></span></div>

                                                </div>

                                                <div class="col-xs-6">
                                                    <strong>Pembayaran Pertama</strong>
                                                    <div>Uang Muka (DP)  <span class="pull-right text-bold">Rp.<?php echo number_format($data_sel[LIST_DATA][0][0]->dp) ?></span></div>
                                                    <div>Angsuran I  <span class="pull-right text-bold">Rp.<?php echo number_format($data_sel[LIST_DATA][0][0]->installment) ?></span></div>
                                                    <div>Administrasi <span class="pull-right text-bold">Rp.<?php echo number_format($data_sel[LIST_DATA][0][0]->admin_fee) ?></span></div>
                                                    <div>Ongkos Kirim  <span class="pull-right text-bold">Rp.<?php echo number_format($data_sel[LIST_DATA][0][0]->total_ship_charged) ?></span></div>
                                                    <div>Total Pembayaran Pertama <input type="hidden" id="total_installment" name="total_installment" value="0">
                                                        <span id="total_pertama" class="pull-right text-bold">Rp.<?php echo number_format($data_sel[LIST_DATA][0][0]->total_installment) ?></span></div>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr style="border: 1px dashed #f4f4f4;">
                            <strong>Data Customer</strong>
                            <div class="row">
                                <div class="col-xs-12">       

                                    <div class="col-xs-6 simulation-field odd no-pad">
                                        <div class="col-xs-5">
                                            Nama Customer
                                        </div>
                                        <div class="col-xs-7 text-right">
                                            <?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][5][0]->pic_name) : "" ); ?>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 simulation-field odd no-pad">
                                        <div class="col-xs-5">
                                            No Indentitas
                                        </div>
                                        <div class="col-xs-7 text-right">
                                            <?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][5][0]->identity_no) : "" ); ?>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 simulation-field no-pad">
                                        <div class="col-xs-5">
                                            Email
                                        </div>
                                        <div class="col-xs-7 text-right">
                                            <?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][5][0]->email_address) : "" ); ?>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 simulation-field no-pad">
                                        <div class="col-xs-5">  
                                            Tanggal Lahir
                                        </div>
                                        <div class="col-xs-7 text-right">
                                            <?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][5][0]->birthday) : "" ); ?>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 simulation-field no-pad odd">
                                        <div class="col-xs-5">
                                            No Telephone 
                                        </div>
                                        <div class="col-xs-7 text-right">
                                            <?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][5][0]->phone_no) : "" ); ?>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 simulation-field no-pad odd">
                                        <div class="col-xs-5">
                                            Alamat Sesuai KTP
                                        </div>
                                        <div class="col-xs-7 text-right">
                                            <?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][5][0]->identity_address) : "" ); ?>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 simulation-field no-pad">
                                        <div class="col-xs-5">
                                            Kelurahan
                                        </div>
                                        <div class="col-xs-7 text-right">
                                            <?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][5][0]->sub_district) : "" ); ?>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 simulation-field no-pad">
                                        <div class="col-xs-5">
                                            Alamat Tinggal
                                        </div>
                                        <div class="col-xs-7 text-right">
                                            <?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][5][0]->address) : "" ); ?>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 simulation-field no-pad">
                                        <div class="col-xs-5">
                                            Kode Pos
                                        </div>
                                        <div class="col-xs-7 text-right">
                                            <?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][5][0]->zip_code) : "" ); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class ="row">
                                <label class ="control-label col-md-1"><strong>No. PO</strong></label>
                                <div class ="col-md-5">
                                    <input class="form-control"  data-d-group="3" data-v-max="100" name ="po_no" id ="po_no" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0][0]) ? get_display_value($data_sel[LIST_DATA][0][0]->po_no) : "" ); ?>">
                                </div>
                            </div>
                        <?php } ?>


                        <hr style="border: 1px dashed #f4f4f4;">&nbsp;&nbsp;&nbsp;
                        <?php if ($data_sel[LIST_DATA][0][0]->status_order == 'N') { ?>

                            <button class="btn btn-success" type="button" id="btnapprove" name="btnxls" onclick="action_status('A')">Approve</button>
                            <button class="btn btn-danger" type="button" id="btnreject" name="btnxls" onclick="action_status('R')">Reject</button>
                        <?php } ?>

                </div>
            </form>
            <div class ="form-group">
                <?php if ($data_auth[FORM_ACTION] == ACTION_VIEW) { ?>

                <?php } else { ?>
                    <div class ="col-xs-12">
                        <?php if ($statusorder == "N") { ?>
                            <input type="hidden" name="<?php echo CONTROL_SAVE_EDIT_NAME ?>" value="<?php echo ACTION_SAVE_UPDATE ?>">
                            <button type="button" onclick="cekdata()" class="btn btn-success btn-partner-width pull-right">Simpan </button>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div><!-- /.box-footer -->
            </section>
        </div>

    </div>
    </div>
    <div class="modal modal-danger" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            &times;
                        </span>
                    </button>
                    <strong class="modal-title">
                        Konfirmasi Pembatalan Order
                    </strong>
                </div>
                <div class="modal-body">
                    <p>Semua produk yang diorder tidak tersedia.
                        <br />
                        Apakah anda akan membatalkan order ini ?
                    </p>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" id="batal-order">Ya, batalkan order ini !</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <script type="text/javascript">
                            function trackers(id) {
                                if ($("#timeline" + id).is(':visible')) {
                                    $("#timeline" + id).hide('slow');
                                } else {
                                    $("#timeline" + id).show('slow');
                                }

                            }
                            function action_status(status) {
                                po_no = $('#po_no').val();
                                if (status == 'A') {
                                    if ( po_no == '') {
                                        alert('No. PO harus diisi');
                                        return;
                                    }
                                }
                                   
                                
                                $.ajax({
                                    url: "<?php echo base_url() ?>partner/agent_order",
                                    type: "GET",
                                    data: {
                                        "act": "<?php echo ACTION_ADDITIONAL ?>",
                                        seq: "<?php echo $data_sel[LIST_DATA][0][0]->seq; ?>",
                                        type: 'update_status',
                                        status: status,
                                        po_no: po_no,
                                        order_no: "<?php echo $data_sel[LIST_DATA][0][0]->order_no; ?>"},
                                    success: function(response) {
                                        
                                        if (status == 'A') {
                                            alert('Data customer diterima');
                                        }
                                        if (status == 'R') {
                                            alert('Data customer ditolak');
                                        }
                                        location.reload();
                                    },
                                    error: function(request, error) {
                                        alert(error_response(request.status));
                                    }
                                });
                            }
                            function backlist() {
                                window.location = '<?php echo get_base_url() . $data_auth[FORM_URL]; ?>';
                            }
                            function cekdata() {
                                status = "";
                                $(".prdsts").each(function() {
                                    if ($(this).val() == "R") {
                                        status += "ada";
                                    }
                                });
                                if (status == "") {
                                    batalorder();
                                    return false;
                                } else {
                                    $("form#form1").submit();
                                }
                            }
                            ;
                            function batalorder() {
                                $('#myModal').modal();
                            }
                            $('#batal-order').on(
                                    'click',
                                    function(evt)
                                    {
                                        $("#order_status").val("X");
                                        $("form#form1").submit();
                                        return true;
                                    }
                            );
    </script>
    <?php
} elseif ($data_auth[FORM_ACTION] == ACTION_ADDITIONAL) {
    switch ($data_sel[LIST_DATA][0]->tipe) {
        case "bukti_kirim":
//	    die(print_r($data_sel[LIST_DATA]));
            ?>
            <!--            <div class="col-xs-12">
                            <div class="pull-right">
                                <a class="btn btn-warning btn-flat btn-block" href="<?php echo base_url() . $data_auth[FORM_URL] ?>">Kembali</a>
                            </div>
                        </div>-->
            <div class="clearfix">

                <div class="row no-margin">
                    <div class="col-xs-12">
                        <div class="box box-default no-border m-top10">
                            <form class="form-horizontal" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] . '?' . CONTROL_GET_TYPE . '=' . ACTION_ADDITIONAL . '&tipe=save_resi' ?>" enctype="multipart/form-data" name="frmMain" id="frmMain" onsubmit ="return validate_form();">
                                <div class="box-body">
                                    <section class="col-xs-12">
                                        <?php echo get_csrf_partner_token(); ?>
                                        <input class="form-control"  name ="seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->seq; ?>">
                                        <input type="hidden" name="btnAdditional" value="act_s_adt">
                                        <input type="hidden" name="tipe" value="save_resi">
                                        <div class="row">

                                            <div class="col-xs-6">
                                                <div class ="form-group">
                                                    <label class ="control-label col-xs-12">Expedisi *</label>
                                                    <div class ="col-xs-12">
                                                        <select class="form-control" name="ship_by_exp_seq" id="ship_by_exp_seq" onchange="cekship(this.value)">
                                                            <?php
                                                            if (isset($exp_name)) {
                                                                foreach ($exp_name as $each) {
                                                                    ?>
                                                                    <option value="<?php echo $each->seq; ?>"><?php echo $each->name; ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                        <div id="partner_ship">
                                                            <input class="form-control no-show" id="ship_by" name="ship_by" type="text" placeholder ="Input Nama Expedisi" value ="<?php echo $data_list[LIST_RECORD][0]->ship_by; ?>" maxlength="50">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class ="form-group">
                                                    <label class ="control-label col-xs-12">No Resi *</label>
                                                    <div class ="col-xs-12">
                                                        <input class="form-control" id="awb_no" name="awb_no" type="text" placeholder ="Input No Resi" value ="<?php echo $data_list[LIST_RECORD][0]->awb_no; ?>" required validate="required[]" maxlength="100">
                                                    </div>
                                                </div>
                                                <div class ="form-group">
                                                    <label class ="control-label col-xs-12">Tanggal Resi *</label>
                                                    <div class ="col-xs-4">
                                                        <input date_type="date" readonly class="form-control" id="ship_date" name="ship_date" type="text" placeholder ="Input Tanggal Resi" value ="<?php echo (($data_list[LIST_RECORD][0]->ship_date != '0000-00-00') ? date("d-M-Y", strtotime($data_list[LIST_RECORD][0]->ship_date)) : date("d-M-Y") ); ?>" required validate="required[]" style="background-color:#ffffff;">
                                                    </div>
                                                </div>
                                                <div class ="form-group">
                                                    <label class ="control-label col-xs-12">Bukti Kirim *</label>
                                                    <div class ="col-xs-12 center">
                                                        <?php
                                                        echo '
                                                        <div class="slim" data-max-file-size="2" style="height:300px;width:300px;">
                                                        <input test type="file" accept="' . IMAGE_TYPE_UPLOAD . '"  name="ifile" ><br>
                                                        <img ' . (($data_list[LIST_RECORD][0]->ship_note_file != '') ? 'src="' . base_url() . RESI_UPLOAD_IMAGE . $data_list[LIST_RECORD][0]->ship_note_file . '"' : '') . '" alt="Bukti Kirim">
                                                        </div> ';
                                                        ?>
                                                        <input type="hidden" id="ofile" name="ofile" value="<?php echo $data_list[LIST_RECORD][0]->ship_note_file; ?>">
                                                    </div>
                                                </div>


                                            </div>

                                            <div class="col-xs-6">
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        Nomor Order : <span class="label label-primary"><strong><?php echo $data_list[LIST_RECORD][0]->order_no; ?></strong></span><br />
                                                        Tanggal Order : <?php echo date("d-M-Y", strtotime($data_list[LIST_RECORD][0]->order_date)); ?>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        Status Order : <span class="label label-info"><strong><?php echo status($data_list[LIST_RECORD][0]->order_status, STATUS_ORDER); ?></strong></span>
                                                        <br>
                                                        <p>Catatan untuk ekspedisi</p>
                                                    </div>
                                                </div>
                                                <?php echo $data_list[LIST_RECORD][0]->ship_notes; ?>
                                                Penerima :<br>
                                                <strong><?php echo $data_list[LIST_RECORD][0]->receiver_name; ?></strong><br />
                                                <?php echo $data_list[LIST_RECORD][0]->receiver_address; ?><br />
                                                <?php echo $data_sel[LIST_DATA][1][0]->d_name; ?>, <?php echo $data_sel[LIST_DATA][1][0]->c_name; ?><br />
                                                <?php echo $data_sel[LIST_DATA][1][0]->p_name; ?>, Kode Pos <?php echo $data_list[LIST_RECORD][0]->receiver_zip_code; ?><br />
                                                Nomor Telp. : <?php echo $data_list[LIST_RECORD][0]->receiver_phone_no; ?><br />
                                                Pesan :<br>
                                                <?php echo $data_list[LIST_RECORD][0]->member_notes; ?>
                                            </div>



                                        </div>
                                        <!-- /.box-footer -->

                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <script type="text/javascript">
                                    <!--
                                            $('.slim').slim({
                                        maxFileSize: "2",
                                        onRemove: function(slim, data) {
                                            $("#ofile").val("");
                                        }
                                    })
                                    function cekship(expedisi) {
                                        if (expedisi == 0) {
                                            $("#partner_ship").show();
                                        } else {
                                            $("#partner_ship").hide();
                                        }
                                    }

                                    $('input[date_type="date"]').daterangepicker({
                                        format: 'DD-MMM-YYYY',
                                        singleDatePicker: true,
                                        showDropdowns: true
                                    });
                                    $("#ship_by_exp_seq").val('<?php echo $data_list[LIST_RECORD][0]->ship_by_exp_seq; ?>');
                                    cekship('<?php echo $data_list[LIST_RECORD][0]->ship_by_exp_seq; ?>');
                                    //-->
                </script>
                <?php
                break;
        }
    } else {
        $totals = 0;
        ?>
        <link rel="stylesheet" type="text/css" href="<?php echo get_css_url() ?>daterangepicker-bs3.css" />
        <div class="top-30">
            <div class="box-body">
                <!--SUB HEADER-->
                <div id="header">
                    <div id="sub-navbar" class="box-body sub-header">
                        <div class="panel-body no-pad">
                            <form id ="frmSearchExcl" method ="get" url= "<?php echo get_base_url() . $data_auth[FORM_URL]; ?>">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <select id="agent_name" class="form-control select2" style='width : 20%' name="agent_seq">
                                                <option value="">-- Nama Agen --</option>
                                                <?php
                                                if (isset($data_sel[LIST_DATA][1])) {
                                                    foreach ($data_sel[LIST_DATA][1] as $each) {
                                                        ?>
                                                        <option value="<?php echo $each->agent_seq; ?>" <?php echo ($each->agent_seq == $this->input->get('agent_seq')) ? "selected" : "" ?>><?php echo $each->name; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                            <input class="form-control-cst" style="width: 20%" name="order_no_f" type="input" placeholder="Nomor Order" value="<?php if (isset($_GET["order_no_f"])) echo $_GET["order_no_f"]; ?>">
                                            <input date_type="date" readonly class="form-control-cst" style="width: 25%" id="order_date_s" name="order_date_s" type="text" placeholder="Tanggal awal order" value="<?php
                                            if (isset($_GET["order_date_s"])) {
                                                if (($_GET["order_date_s"] != ''))
                                                    echo date("d-M-Y", strtotime($_GET["order_date_s"]));
                                            }
                                            ?>">

                                            <input date_type="date" readonly class="form-control-cst" style="width: 25%" id="order_date_e" name="order_date_e" type="text" placeholder="Tanggal akhir order" value="<?php
                                            if (isset($_GET["order_date_e"])) {
                                                if (($_GET["order_date_e"] != ''))
                                                    echo date("d-M-Y", strtotime($_GET["order_date_e"]));
                                            }
                                            ?>">
                                            <button class="btn btn-danger" style="margin-top: -4px" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                    <input type="hidden" id="start1" name="start" value="<?php
                                    if (isset($_GET["start"])) {
                                        echo $_GET["start"];
                                    } else {
                                        echo '1';
                                    }
                                    ?>">
                                    <input type="hidden" id="length1" name="length" value="<?php
                                    if (isset($_GET["length1"])) {
                                        echo $_GET["length1"];
                                    } else {
                                        echo '10';
                                    }
                                    ?>">
                                </div>
                            </form>

                            <div class="col-xs-9 no-pad">
                                <ul class="nav nav-pills no-padding">
                                    <!--<li><a class="text-block">Status Order :</a></li>-->
                                    <li class="blue-link"><a class="btn btn-flat <?php echo $this->input->get('payment_status') == '' ? 'btn-active' : '' ?>" href="<?php echo base_url($data_auth[FORM_URL]) . '?payment_status=' . $filter_sort . $filter ?>">Semua (<span id="status_all"></span>)</a></li>
                                    <?php
                                    $qty_status = 0;
                                    $total_qty_status_other = 0;
                                    $qty_status_wait_confirm_status_code = 0;
                                    $qty_status_confirm_status_code = 0;
                                    $qty_status_wait_confirm_third_party = 0;
                                    $qty_status_failed = 0;
                                    $qty_status_cancel = 0;
                                    foreach (json_decode(STATUS_PAYMENT) as $key => $stat) {
                                        if ($key == PAYMENT_UNPAID_STATUS_CODE || $key == PAYMENT_PAID_STATUS_CODE || $key == PAYMENT_WAIT_CONFIRM_ADIRA_APPROVAL) {
                                            if (isset($data_sel[LIST_DATA])) {
                                                $qty_status = get_qty_status($data_sel[LIST_DATA], $key);
                                                $totals = ($totals + $qty_status);
                                            } else {
                                                $qty_status = 0;
                                            }
                                            ?>
                                            <li class="blue-link">
                                                <a class="btn btn-flat <?php echo $this->input->get('payment_status') == $key ? 'btn-active' : '' ?>"
                                                   href="<?php echo base_url($data_auth[FORM_URL]) . '?payment_status=' . $key . $filter_sort . $filter ?>"
                                                   >
                                                    <?php echo $stat; ?> (<?php echo $qty_status ?>)
                                                </a>
                                            </li>
                                            <?php
                                        } elseif ($key == PAYMENT_CANCEL_BY_ADMIN_STATUS_CODE) {
                                            $qty_status = get_qty_status($data_sel[LIST_DATA], $key);
                                            $qty_status_cancel = $qty_status_cancel + $qty_status;
                                            $total_qty_status_other = ($total_qty_status_other + $qty_status);
                                        } elseif ($key == PAYMENT_FAILED_STATUS_CODE) {
                                            $qty_status = get_qty_status($data_sel[LIST_DATA], $key);
                                            $qty_status_failed = $qty_status_failed + $qty_status;
                                            $total_qty_status_other = ($total_qty_status_other + $qty_status);
                                        } elseif ($key == PAYMENT_WAIT_CONFIRM_STATUS_CODE) {
                                            $qty_status = get_qty_status($data_sel[LIST_DATA], $key);
                                            $qty_status_wait_confirm_status_code = $qty_status_wait_confirm_status_code + $qty_status;
                                            $total_qty_status_other = ($total_qty_status_other + $qty_status);
                                        } elseif ($key == PAYMENT_CONFIRM_STATUS_CODE) {
                                            $qty_status = get_qty_status($data_sel[LIST_DATA], $key);
                                            $qty_status_confirm_status_code = $qty_status_confirm_status_code + $qty_status;
                                            $total_qty_status_other = ($total_qty_status_other + $qty_status);
                                        } elseif ($key == PAYMENT_WAIT_CONFIRM_THIRD_PARTY) {
                                            $qty_status = get_qty_status($data_sel[LIST_DATA], $key);
                                            $qty_status_wait_confirm_third_party = $qty_status_wait_confirm_third_party + $qty_status;
                                            $total_qty_status_other = ($total_qty_status_other + $qty_status);
                                        }
                                    }
                                    ?>
                                    <li class="blue-link">
                                        <?php
                                        if ($this->input->get('payment_status') == PAYMENT_WAIT_CONFIRM_STATUS_CODE || $this->input->get('payment_status') == PAYMENT_CONFIRM_STATUS_CODE || $this->input->get('payment_status') == PAYMENT_WAIT_CONFIRM_THIRD_PARTY || $this->input->get('payment_status') == PAYMENT_CANCEL_BY_ADMIN_STATUS_CODE || $this->input->get('payment_status') == PAYMENT_FAILED_STATUS_CODE) {
                                            $c_active = 'btn-active';
                                        } else {
                                            $c_active = '';
                                        }
                                        ?>
                                        <a class="btn btn-flat <?php echo $c_active; ?>" href="#" id="waiting_payment" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <?php
                                            switch ($this->input->get('payment_status')) {
                                                case PAYMENT_WAIT_CONFIRM_STATUS_CODE:
                                                    echo cstdes(PAYMENT_WAIT_CONFIRM_STATUS_CODE, STATUS_PAYMENT) . "(" . $qty_status_wait_confirm_status_code . ")";
                                                    break;
                                                case PAYMENT_CONFIRM_STATUS_CODE:
                                                    echo cstdes(PAYMENT_CONFIRM_STATUS_CODE, STATUS_PAYMENT) . "(" . $qty_status_confirm_status_code . ")";
                                                    break;
                                                case PAYMENT_WAIT_CONFIRM_THIRD_PARTY:
                                                    echo cstdes(PAYMENT_WAIT_CONFIRM_THIRD_PARTY, STATUS_PAYMENT) . "(" . $qty_status_wait_confirm_third_party . ")";
                                                    break;
                                                case PAYMENT_FAILED_STATUS_CODE:
                                                    echo cstdes(PAYMENT_FAILED_STATUS_CODE, STATUS_PAYMENT) . "(" . $qty_status_failed . ")";
                                                    break;
                                                case PAYMENT_CANCEL_BY_ADMIN_STATUS_CODE:
                                                    echo cstdes(PAYMENT_CANCEL_BY_ADMIN_STATUS_CODE, STATUS_PAYMENT) . "(" . $qty_status_cancel . ")";
                                                    break;
                                                default:
                                                    echo "Lainnya (" . $total_qty_status_other . ")";
                                            }
                                            ?>
                                            <i class="fa fa-chevron-down"></i>
                                        </a>
                                        <ul class="dropdown-menu" aria-labelledby="waiting_payment">
                                            <?php
                                            $qty_status = 0;
                                            foreach (json_decode(STATUS_PAYMENT) as $key => $stat) {
                                                if ($key == PAYMENT_WAIT_CONFIRM_STATUS_CODE || $key == PAYMENT_CONFIRM_STATUS_CODE || $key == PAYMENT_WAIT_CONFIRM_THIRD_PARTY || $key == PAYMENT_CANCEL_BY_ADMIN_STATUS_CODE || $key == PAYMENT_FAILED_STATUS_CODE) {
                                                    if (isset($data_sel[LIST_DATA])) {
                                                        $qty_status = get_qty_status($data_sel[LIST_DATA], $key);
                                                        $totals = ($totals + $qty_status);
                                                    } else {
                                                        $qty_status = 0;
                                                    }
                                                    ?>
                                                    <li class="blue-link col-xs-12">
                                                        <a class="btn btn-flat"
                                                           href="<?php echo base_url($data_auth[FORM_URL]) . '?payment_status=' . $key . $filter_sort . $filter ?>" >
                                                            <?php echo $stat; ?> (<?php echo $qty_status ?>)
                                                        </a>
                                                    </li>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-xs-3">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="row">
                                            <?php echo get_order_filter("order_date", "order_no", base_url($data_auth[FORM_URL]) . $filter_payment_status . $filter, $this->input->get("sort"), $this->input->get("column_sort")) ?>
                                            <?php
                                            if (isset($_GET['payment_status'])) {
                                                if ($_GET['payment_status'] == PAYMENT_WAIT_CONFIRM_ADIRA_APPROVAL) {
                                                    ?><div class="col-xs-6 no-pad"><button class="btn btn-success btn-block btn-block flat" type="button" id="btnxls" name="btnxls" onclick="data_report()"><i class="fa fa-file-excel-o"></i> Excel</button></div><?php
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <form method="post" action="" target="_blank" id="frmprint" name="frmprint">
                        <input type="hidden" name="btnAdditional" value="act_s_adt">
                        <input type="hidden" id="tipe" name="tipe" value="docprint"><input type="hidden" id="orderid" name="orderid" value="">
                        <br style="margin-bottom:30px;" />
                        <div class="col-xs-12 no-pad" id="tabledata1">
                            <?php
                            if (isset($data_list[LIST_DATA])) {
                                $status_edit = '';
                                $dataresi = '';
                                foreach ($data_list[LIST_DATA] as $orders) {
                                    ?>
                                    <div class="box no-border">
                                        <div style="padding-right: 10px;padding-top: 10px;">
                                        </div>
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-xs-3">Tanggal Order : <?php echo c_date($orders->order_date) ?></div>
                                                <div class="col-xs-4">Nomor Order : <strong><?php echo $orders->order_no ?></strong></div>
                                                <div class="col-xs-3">Status : <?php echo cstdes($orders->payment_status, STATUS_PAYMENT); ?></div>
                                                <div class = "col-xs-1">
                                                    <?php
                                                    echo "<a href='?" . CONTROL_GET_TYPE . "=" . ACTION_VIEW . "&key=" . $orders->seq . "' class='btn btn-xs btn-flat btn-border-view' title='Lihat'><i class = 'fa fa-eye'></i> Lihat</a>";
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-3">Nama Agen: <?php echo $orders->agent_name ?></div>
                                                <div class="col-xs-4">Nilai Order : <?php echo RP . cnum($orders->total_order) ?></div>
                                                <div class="col-xs-4">Biaya Pengiriman : <?php echo RP . cnum($orders->total_ship_charged) ?></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-3">Nama Penerima: <?php echo $orders->name ?></div>
                                                <div class="col-xs-4">Telpon Penerima: <?php echo $orders->receiver_phone_no ?></div>
                                                <div class="col-xs-4">Komisi Agen: <?php echo RP . cnum($orders->commission) ?></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-7">Alamat Penerima : <?php echo ucfirst($orders->receiver_address) ?>, <?php echo $orders->district ?>, <?php echo $orders->city ?>, <?php echo $orders->province ?>, <?php echo $orders->receiver_zip_code ?></div>
                                                <div class="col-xs-4">Komisi Partner : <?php echo RP . cnum($orders->commission_partner) ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            } else {
                                ?>
                                <div class="col-xs-12 no-padding">
                                    <div class="box box-no-border-top">
                                        <div class="box-body min-height300">
                                            <center class="no-found-src" >Tidak ada data order agent yang di tampilkan. </center>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="col-xs-12 no-pad">
                                <div class="box-pagging">
                                    <center>
                                        <?php echo $this->pagination->create_links(); ?>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
                <div class="collapse" id="detail"></div>
            </div>
        </div>
        <div class="modal modal-danger" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong class="modal-title">Konfirmasi Pembatalan Order</strong>
                    </div>
                    <div class="modal-body">
                        <p>Semua produk yang diorder tidak tersedia.<br />
                            Apakah anda akan membatalkan order ini ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" id="batal-order">Ya, batalkan order ini !</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <script type="text/javascript">
                            $("#status_all").html("<?php echo $totals; ?>");
                            $('input[date_type="date"]').daterangepicker({
                                format: 'DD-MMM-YYYY',
                                singleDatePicker: true,
                                showDropdowns: true
                            });
                            function data_report() {
                                window.open("<?php echo base_url() ?>partner/agent_order?act=<?php echo ACTION_ADDITIONAL ?>&type=unpaid_xls");
                            }

        </script>
        <?php
    }
    require_once get_include_page_list_partner_content_footer();
    require_once get_include_content_partner_bottom_page_navigation();
    ?>