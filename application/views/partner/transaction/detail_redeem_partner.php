<?php
require_once VIEW_BASE_PARTNER;
require_once get_include_content_partner_top_page_navigation();
echo $breadcrumb;
?>
<br>
<div class="col-xs-12">
    <div class="box box-default no-border">
        <div class="box-header no-border">
            <h3 class="box-title">
                <?php if (isset($data_list[LIST_DATA][0])) { ?>
                    Daftar Redeem Partner Periode <?php echo date('j F Y', strtotime($data_list[LIST_DATA][0]->from_date)) ?> - <?php echo date('j F Y', strtotime($data_list[LIST_DATA][0]->to_date)) ?> 
                <?php } else { ?>
                    Daftar Redeem Partner Periode
                <?php } ?>
            </h3>
        </div>
        <!-- /.box-header -->
        <form id="frmMain" method="POST" action="<?php echo base_url() . $data_auth[FORM_URL] . '?' . $_SERVER["QUERY_STRING"]; ?>">
            <?php echo get_csrf_partner_token(); ?>
            <input type="hidden" name="redeem_seq" value="<?php echo $this->input->get('key'); ?>">
            <div class="box-body" style="display: block;">
                <br>
                <div class="col-xs-3 pull-right">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-xs-6 pull-right">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <br>
                <style>
                    .can_change{
                        cursor: pointer !important
                    }
                </style>
                <div class="table-responsive">
                    <table class="table-order table no-margin">
                        <thead>
                            <tr>
                                <th style="max-width: 30px!important;vertical-align: middle;" class="text-center">Pilih</th>
                                <th style="min-width: 120px!important;vertical-align: middle;" class="sorting text-center" column="name" order="">Nama Agent</th>
                                <th style="min-width: 80px!important;vertical-align: middle;" class="sorting text-right" column="total" order="">Total (Rp)</th>
                                <th style="width: 200px;vertical-align: middle" class="sorting text-center pr-20px" column="paid_date" order="">Tanggal Bayar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (isset($data_list[LIST_DATA])) { ?>
                                <?php foreach ($data_list[LIST_DATA] as $each) { ?>
                                    <tr id="<?php echo $each->agent_seq ?>">
                                        <td class="text-center">
                                            <input <?php echo isset($_SESSION[SESSION_PAID_CHECKED]) && in_array($each->agent_seq, $_SESSION[SESSION_PAID_CHECKED]) ? 'checked' : '' ?> 
                                                id='<?php echo 'test_' . $each->agent_seq ?>' onclick="set_date(<?php echo $each->agent_seq ?>)" 
                                                type="checkbox" name="agent_seq[]" value="<?php echo $each->agent_seq; ?>" 
                                                <?php echo isset($each->paid_date) && $each->paid_date != DEFAULT_DATE ? 'disabled' : ''; ?> 
                                                <?php echo isset($each->paid_date) && $each->paid_date != DEFAULT_DATE ? 'checked' : ''; ?> >
                                        </td>
                                        <td class="text-center"><?php echo $each->name ?></td>
                                        <td class="text-right"><?php echo number_format($each->total); ?></a></td>
                                        <td class="text-center" style="width: 200px">


                                            <?php if ($each->paid_date == DEFAULT_DATE) { ?>
                                                <input date_type="date"  <?php echo isset($_SESSION[SESSION_PAID_CHECKED]) && in_array($each->agent_seq, $_SESSION[SESSION_PAID_CHECKED]) ? '' : 'disabled="true"' ?> 
                                                       class="form-control <?php echo isset($each->paid_date) && $each->paid_date != DEFAULT_DATE ? '' : 'can_change'; ?>  
                                                       <?php echo (isset($_SESSION[SESSION_PAID_CHECKED]) && in_array($each->agent_seq, $_SESSION[SESSION_PAID_CHECKED]) ? '' : 'hide' ) ?>"
                                                       name="paid_date[]" id="paid_date_<?php echo $each->agent_seq ?>"
                                                       value="<?php echo isset($_SESSION[SESSION_PAID_SELECTED][$each->agent_seq]) ? $_SESSION[SESSION_PAID_SELECTED][$each->agent_seq] : '' ?>"
                                                        placeholder="Tanggal bayar" type="text" />
                                                       <?php
                                                   } else {
                                                       echo date('d-M-Y', strtotime($each->paid_date));
                                                   }
                                                   ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            <?php } ?>
                        </tbody>
                    </table>
                    <div class="col-xs-12 no-pad">
                        <div class="box-pagging">
                            <center><?php echo $this->pagination->create_links(); ?></center>
                        </div>
                    </div>
                </div>
                <hr>
                <div style="width: 100px;" class="pull-right"><?php echo get_save_edit_button(); ?></div>
            </div>
        </form>
    </div>
</div>
<?php
require_once get_include_page_list_partner_content_footer();
?>
<script>

    function set_date(id) {
        $('#paid_date_' + id).toggleClass('hide');
        var stat = $('#test_' + id).prop('checked');
        if (stat) {
            $('#paid_date_' + id).prop('disabled', false);
        } else {
            $('#paid_date_' + id).prop('disabled', true);
        }


    }


    $(document).ready(function () {
        $('input[name="paid_date[]"]').daterangepicker({
            format: 'DD-MMM-YYYY',
            showDropdowns: true,
            singleDatePicker: true
        });
    })

    function data_report() {
        var data = GET;
        data.act = '<?php echo ACTION_ADDITIONAL ?>';
        data.type = 'print_excel';
        $.ajax({
            url: '<?php echo base_url() . $data_auth[FORM_URL] ?>',
            type: 'GET',
            data: data,
            success: function (response) {
                try {
                    response = jQuery.parseJSON(response);
                    if (response != null) {
                        create_report(response);
                    } else {
                        alert("Tidak ada data.");
                    }
                } catch (e) {
                    alert("Tidak ada data.");
                }
                return false;
            },
            error: function (res) {

            }
        });
        return false;
    }
</script>
<?php
require_once get_include_content_partner_bottom_page_navigation();
?>
