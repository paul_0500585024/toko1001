<?php
require_once VIEW_BASE_PARTNER;
require_once get_include_content_partner_top_page_navigation();
echo $breadcrumb;
?>
<style>
    body {
        background-color: #f1f1f1;
        font-family: Helvetica,Arial,Verdana;

    }
    h1 {
        color: green;
        text-align: center;
    }
    .redfamily {
        color: red;	
    }
    .search-box,.close-icon,.search-wrapper {
        position: relative;
        padding: 4px;
    }
    .search-wrapper {
        width: 500px;
        margin: auto;
        margin-top: 50px;
    }
    .search-box {
        width: 80%;
        border: 1px solid #ccc;
        outline: 0;
        border-radius: 15px;
    }
    .search-box:focus {
        box-shadow: 0 0 15px 5px #b0e0ee;
        border: 2px solid #bebede;
    }
    .close-icon {
        border:1px solid transparent;
        background-color: transparent;
        display: inline-block;
        vertical-align: middle;
        outline: 0;
        cursor: pointer;
    }
    .close-icon:after {
        content: "X";
        display: block;
        width: 15px;
        height: 15px;
        position: absolute;
        z-index:1;
        right: 35px;
        bottom: 0;
        margin: auto;
        padding: 2px;
        border-radius: 50%;
        text-align: center;
        color: #708090;
        font-weight: normal;
        font-size: 12px;
        cursor: pointer;
    }
    .search-box:not(:valid) ~ .close-icon {
        display: none;
    }
</style>
<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
    <div class="top-30">
        <div class="box-body">
            <div id="header">
                <div class="box no-border">
                    <form onsubmit ="return validate_form();" class ="form-horizontal" id="frmMain" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL]; ?>">
                        <div class="box-body">
                            <section class="col-md-6">
                                <?php echo get_csrf_admin_token(); ?>
                                <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                                    <input class="form-control"  name ="collect_seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->collect_seq; ?>">
                                    <input class="form-control"  name ="partner_seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->partner_seq; ?>">
                                <?php } ?>
                                <div class="box-body">
                                    <dl class="dl-horizontal">
                                        <dt>Partner :</dt>
                                        <dd><?php echo $data_sel[LIST_DATA][0]->name; ?></dd>
                                        <dt>Total :</dt>
                                        <dd>Rp. <?php echo number_format($data_sel[LIST_DATA][0]->total); ?></dd>
                                        <?php
                                        if (isset($data_sel[LIST_DATA][2])) {
                                            foreach ($data_sel[LIST_DATA][2] as $each) {
                                                ?>
                                                <br>
                                                <dt>Bank :</dt>                                
                                                <dd><?php echo $each->bank_name; ?></dd>
                                                <dt>Cabang :</dt>
                                                <dd><?php echo $each->bank_branch_name; ?></dd>
                                                <dt>No Akun :</dt>
                                                <dd><?php echo $each->bank_acct_no; ?></dd>
                                                <dt>Nama Akun :</dt>
                                                <dd><?php echo $each->bank_acct_name; ?></dd>
                                            <?php
                                            }
                                        }
                                        ?>
                                    </dl>
                                </div>
                                <div class ="form-group">
                                    <label class ="control-label col-md-3">Tanggal Bayar</label>
                                    <div class ="col-md-9">
                                        <input class="form-control" validate="required[]" date_type="date" id ="paid_date" name ="paid_date" type="text" placeholder ="Tanggal Bayar" value ="<?php echo cdate($data_sel[LIST_DATA][0]->paid_date); ?>">
                                    </div>
                                </div>
                                <div class ="form-group">
                                    <?php if ($data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
                                        <div class ="col-md-6"><?php echo get_back_button(); ?></div>
                                    <?php } else { ?>
                                        <div class ="col-md-6"><?php echo get_cancel_button(); ?></div>
                                        <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT && $data_sel[LIST_DATA][0]->status == "P") { ?>
                                            <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                                        <?php } ?>
    <?php } ?>
                                </div>
                            </section>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $('input[date_type="date"]').daterangepicker({
            format: 'DD-MMM-YYYY',
            singleDatePicker: true,
            showDropdowns: true
        });</script>

<?php } elseif ($data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="col-xs-12">
        <!-- /.box-header -->
        <div class="box box-default no-border">
            <div class="box-header">
                <h3 class="box-title">Daftar Detail Order</h3>
            </div>
            <div class="box-body">
                <div class="col-xs-3 pull-right">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-xs-6 pull-right">
                                    <button class="btn btn-success btn-block btn-block flat" type="button" id="btnxls" name="btnxls" onclick="data_report('<?php echo $data_sel[LIST_DATA][3]->seq ?>')"><i class="fa fa-file-excel-o"></i> Excel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#t_1" aria-controls="t_1" role="tab" data-toggle="tab">Detil Order</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="t_1">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-bordered">
                                    <tr>
                                        <th>No. Order</th>
                                        <th>Tanggal Order</th>
                                        <th>Nilai Order</th>
                                        <th>Pembayaran I</th>
                                        <th>Total Tagihan</th>
                                    </tr>
                                    <?php
                                    if (isset($data_sel[LIST_DATA][1])) {
                                        //die(print_r($data_sel[LIST_DATA]));
                                        $ttlorder = 0;
                                        $ttlexp = 0;
                                        $ttlfee = 0;
                                        $ttlall = 0;
                                        $subreedem = 0;
                                        $total_instalment = 0;
                                        $total_redeem = 0;
                                        foreach ($data_sel[LIST_DATA][1] as $each) {
                                            $subreedem = ($each->total_payment - $each->total_installment);
                                            ?>
                                            <tr>
                                                <td><?php echo $each->order_no; ?><a href="<?php echo base_url() ?>partner/agent_order?<?php echo CONTROL_GET_TYPE . '=' . ACTION_VIEW . '&key=' . $each->order_seq ?>" target="_blank" class="pull-right"><i class="fa fa-external-link"></i></a></td>
                                                <td align="right"><?php echo cdate($each->order_date); ?></td>
                                                <td align="right"><?php echo number_format($each->total_payment); ?></td>
                                                <td align="right"><?php echo isset($each->total_installment) ? number_format($each->total_installment) : ''; ?></td>
                                                <td align="right" class="warning"><?php echo number_format($subreedem); ?></td>
                                            </tr>
                                            <?php
                                            $ttlorder = ($ttlorder + $each->total_payment);
                                            if (isset($each->total_installment))
                                                $total_instalment = ($total_instalment + $each->total_installment);
                                            $total_redeem = ($total_redeem + $subreedem);
                                        }
                                        echo '
					<tr>
					    <td align="right" colspan="2">Total</td>
	    				    <td align="right">' . number_format($ttlorder) . '</td>
                                            <td align="right">' . number_format($total_instalment) . '</td>
                                            <td align="right" class="warning">' . number_format($total_redeem) . '</td>
	    				</tr>';
                                    }
                                    ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="<?php echo base_url() . $data_auth[FORM_URL] ?>" class="btn btn-warning">Kembali</a>

            </div>
            <!-- /.box-body -->
        </div>
    </div>
    <!-- /.box -->
    <script>
        function data_report(key) {
            var data = $('#frmSearchExcl').serializeArray();
            data.push({name: 'type', value: 'print_data_excel'}, {name: '<?php echo CONTROL_GET_TYPE ?>', value: '<?php echo ACTION_ADDITIONAL ?>'}, {name: 'key', value: key});
            $.ajax({
                url: "<?php echo base_url() . $data_auth[FORM_URL]; ?>",
                data: data,
                type: "get",
                success: function (response) {
                    if (isSessionExpired(response)) {
                    } else {
                        try {
                            response = jQuery.parseJSON(response);
                            if (response != null) {
                                create_report(response);
                            } else {
                                alert("Tidak ada data.");
                            }
                        } catch (e) {
                            alert("Tidak ada data.");
                        }
                        return false;
                    }
                },
                error: function (request, error) {
                    alert(error_response(request.status));
                }
            });
        }

        function create_report(jsondata) {
            var rpt = new CSVExport(jsondata);
        }
    </script>
<?php }else { ?>
    <div class="top-30">
        <!-- /.box-header -->
        <div class="box-body">
            <div id="header">
                <div id="sub-navbar" class="box-body sub-header">
                    <div class="panel-body no-pad">
                        <form id ="frmSearchExcl" method ="get" url= "<?php echo get_base_url() . $data_auth[FORM_URL]; ?>">
                            <input date_type="date" readonly class="form-control-cst" style="width: 25%" id="order_date_s" name="order_date_s" type="text" placeholder="Tanggal awal periode" 
                                   value="<?php echo $data_head[LIST_DATA][0]->from_date == '0000-00-00' ? '' : $data_head[LIST_DATA][0]->from_date ?>">

                            <input date_type="date" readonly class="form-control-cst" style="width: 25%" id="order_date_e" name="order_date_e" type="text" placeholder="Tanggal akhir periode" 
                                   value="<?php echo $data_head[LIST_DATA][0]->to_date == '0000-00-00' ? '' : $data_head[LIST_DATA][0]->to_date ?>">

                            <button class="btn btn-danger" style="margin-top: -4px" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </form>
                    </div>
                </div>
                <br style="margin-bottom:30px;">
                <div class="box no-border">
                    <div style="padding-right: 10px;padding-top: 10px;"></div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin table-order">
                                <thead>
                                    <tr>
                                        <th style="min-width: 100px!important;vertical-align: middle;" class="sorting text-center" column="from_date" order="">Tanggal Awal</th>
                                        <th style="min-width: 100px!important;vertical-align: middle;" class="sorting text-center" column="to_date" order="">Tanggal Akhir</th>
                                        <th style="min-width: 100px!important;vertical-align: middle;" class="sorting text-center" column="status" order="">Status</th>
                                        <th style="min-width: 100px!important;vertical-align: middle;" class="sorting text-center" column="paid_date" order="">Tanggal Bayar</th>
                                        <th style="min-width: 100px!important;vertical-align: middle;" class="sorting text-right pr-20px" column="total" order="">Total Bayar (<?php echo RP ?>)</th>
                                        <th style="min-width: 100px!important;vertical-align: middle;" class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (isset($data_list[LIST_DATA])) { ?>
        <?php foreach ($data_list[LIST_DATA] as $each) { ?>
                                            <tr>
                                                <td class="text-center"><?php echo cdate($each->from_date) ?></td>
                                                <td class="text-center"><?php echo cdate($each->to_date) ?></td>
                                                <td class="text-center"><?php echo status($each->status, STATUS_LOAN) ?></td>
                                                <td class="text-center"><?php echo cdate($each->paid_date) ?></td>
                                                <td class="text-right"><a href="<?php echo base_url() . $data_auth[FORM_URL] . '?' . CONTROL_GET_TYPE . '=' . ACTION_VIEW . '&key=' . $each->collect_seq ?>"><?php echo cnum($each->total) ?></a></td>
                                                <td class="text-center">
                                                    <?php if ($each->status == 'P') { ?>
                                                        <a href="<?php echo base_url() . $data_auth[FORM_URL] . '?' . CONTROL_GET_TYPE . '=' . ACTION_EDIT . '&key=' . $each->collect_seq ?>">Bayar</a>
                                                    <?php
                                                    } elseif ($each->status == 'D') {
                                                        echo "Menunggu Verifikasi Admin";
                                                    } elseif ($each->status == 'F') {
                                                        echo "Lunas";
                                                    } else {
                                                        echo status($each->status, STATUS_LOAN);
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
        <?php } ?>
    <?php } ?>
                                </tbody>
                            </table>
                            <div class="col-xs-12 no-pad">
                                <div class="box-pagging">
                                    <center><?php echo $this->pagination->create_links(); ?></center>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- /.table-responsive -->
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <script>
        $("#btn-search").on("click", function () {
            var curr_url = window.location.href;
            var prev_url = curr_url.split("?");
            window.location = prev_url[0];
        })


        $('input[date_type="date"]').daterangepicker({
            format: 'DD-MMM-YYYY',
            singleDatePicker: true,
            showDropdowns: true
        });
    </script>
    <?php
}
require_once get_include_page_list_partner_content_footer();
require_once get_include_content_partner_bottom_page_navigation();
?>