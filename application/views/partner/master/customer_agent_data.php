<?php
require_once VIEW_BASE_PARTNER;
require_once get_include_content_partner_top_page_navigation();
echo $breadcrumb;
?>
<style>
    .select2-dropdown .select2-search__field:focus,.select2-search--inline .select2-search__field:focus{
        outline:none;
        border:none !important;
    }

</style>
<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="box-body">
        <div class="box box-default">
            <div>
                <h3 class="box-title"></h3>
            </div>

            <form class ="form-horizontal" id="frmMain" onsubmit ="return validate_form();" method ="post" enctype ="multipart/form-data" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
                <div class="box-body">
                    <div class="col-md-7">
                        <?php echo get_csrf_partner_token(); ?>
                        <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                            <input class="form-control"  name ="seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->seq; ?>">
                        <?php } ?>

                        <div class ="form-group">
                            <label class ="control-label col-md-3">No. KTP</label>
                            <div class ="col-md-9">
                                <input class="form-control" data-d-group="3" data-v-max="100" validate="required[]" name ="identity_no" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->identity_no) : "" ); ?>">
                            </div>
                        </div>
                        <div class ="form-group">
                            <label class ="control-label col-md-3">Alamat KTP</label>
                            <div class ="col-md-9">
                                <input class="form-control" data-d-group="3" data-v-max="100" validate="required[]" name ="identity_address" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->identity_address) : "" ); ?>">
                            </div>
                        </div>
                        <div class ="form-group">
                            <label class ="control-label col-md-3">No. Telp</label>
                            <div class ="col-md-9">
                                <input class="form-control" data-d-group="3" data-v-max="100" validate="required[]" name ="phone_no" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->phone_no) : "" ); ?>">
                            </div>
                        </div>
                        <div class ="form-group">
                            <label class ="control-label col-md-3">Email</label>
                            <div class ="col-md-9">
                                <input class="form-control" data-d-group="3" data-v-max="100" validate="required[]" name ="email_address" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->email_address) : "" ); ?>">
                            </div>
                        </div>
                        <div class ="form-group">
                            <label class ="control-label col-md-3">Kelurahan</label>
                            <div class ="col-md-9">
                                <input class="form-control" data-d-group="3" data-v-max="100" validate="required[]" name ="sub_district" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->sub_district) : "" ); ?>">
                            </div>
                        </div>
                        <div class ="form-group">
                            <label class ="control-label col-md-3">Alamat Tinggal</label>
                            <div class ="col-md-9">
                                <input class="form-control" data-d-group="3" data-v-max="100" validate="required[]" name ="address" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->address) : "" ); ?>">
                            </div>
                        </div>
                        <div class ="form-group">
                            <label class ="control-label col-md-3">Tanggal Lahir</label>
                            <div class ="col-md-9">
                                <input class="form-control" data-d-group="3" data-v-max="100" validate="required[]" name ="birthday" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->birthday) : "" ); ?>">
                            </div>
                        </div>
                        <div class ="form-group">
                            <label class ="control-label col-md-3">Kode Pos</label>
                            <div class ="col-md-9">
                                <input class="form-control" data-d-group="3" data-v-max="100" validate="required[]" name ="zip_code" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->zip_code) : "" ); ?>" >
                            </div>
                        </div>

                        <div class ="form-group">
                            <?php
                            if ($data_auth[FORM_ACTION] == ACTION_VIEW) {
                                ?>
                                <div class ="col-md-6"><?php echo get_back_button(); ?> </div>
                                <?php
                            } else {
                                if ($data_auth[FORM_ACTION] == ACTION_ADD) {
                                    ?>
                                    <div class ="col-md-6"><?php echo get_save_add_button(); ?> </div>
                                <?php } if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                                    <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                                <?php } ?>
                                <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                            <?php } ?>
                        </div>
                    </div> 
                </div>
        </div>
    </div>

    </form>
<?php } else { ?>
    <div class="p-10px">
        <div class="box box-body no-border">
            <form id="frmSearch" method="get" action="<?php echo get_base_url() . $data_auth[FORM_URL]; ?>">
                <div class="row">
                    <div class="col-xs-8">
                        <div class="form-group">
                            <input class="form-control-cst" style="width: 40%" name="identity_no"
                                   type="input" placeholder="No. KTP"
                                   value="<?php if (isset($_GET["identity_no"])) echo $_GET["identity_no"]; ?>">
                            <input class="form-control-cst" style="width: 40%" name="email_address"
                                   type="input" placeholder="Email"
                                   value="<?php if (isset($_GET["email_address"])) echo $_GET["email_address"]; ?>">
                            <button class="btn btn-danger" type="submit" style="margin-top: -4px">
                                <i class="fa fa-search" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
            <?php require_once get_include_page_list_partner_content_header(); ?>
        </div>
    </div>

    <div class="p-10px">
        <div class="box box-default">
            <div>
                <h3 class="box-title"></h3>
            </div>
                    <div class="box-body">
                        <table id="tbl" class="diplay table table-bordered table-striped" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th column="identity_no">No. KTP</th>
                                    <th column="identity_address">Alamat KTP</th>
                                    <th column="phone_no">No. Telp</th>
                                    <th column="email_address">Email</th>
                                    <th column="sub_district">Kelurahan</th>
                                    <th column="address">Alamat Tinggal</th>
                                    <th column="birthday">Tanggal Lahir</th>
                                    <th column="created_date"><?php echo CREATED_DATE; ?></th>

                                </tr>
                            </thead>
                        </table>
                    </div>

                   
        </div>
    </div>   

    <?php
}
require_once get_include_page_list_partner_content_footer();
require_once get_include_content_partner_bottom_page_navigation();
?> 
