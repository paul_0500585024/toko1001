<?php
//var_dump($data_sel[LIST_DATA][0][0]);die();
require_once VIEW_BASE_PARTNER;
require_once get_include_content_partner_top_page_navigation();

echo $breadcrumb;

function get_qty_status($arraystatus, $val) {
    foreach ($arraystatus as $key => $obj) {
        if ($obj->agent_status == $val) {
            return $obj->qty_status;
            break;
        }
    }return 0;
}
?>
<style>
    .switch:before {
        background: #00a65a;
    }
</style>
<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
    <div class="modal fade col-md-12" id="address_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-header col-md-offset-2 col-md-10"
                 style="background-color: #F2F2F2; padding: 2px">
                <button type="button" class="close" data-dismiss="modal"
                        style="margin-top: 5px; margin-right: 15px">&times;</button>
                <h3 class="modal-title" id="address_title"
                    style="margin: 10px; text-align: center; font-weight: 600"></h3>
            </div>
            <div class="modal-content col-md-offset-2 col-md-10">
                <form class="form-horizontal" id='frmAddress' method="POST"
                      action="#">
                    <input type="hidden" name="act" id="act" />
                    <div class="modal-body">
                        <div class="row">
                            <input name="address_seq" type="hidden" id="address_seq" /> <input
                                name="agent_seq" type="hidden" id="agent_seq" /> <input
                                name="uniqId" type="hidden" id="uniqId" />
                            <div class="form-group">
                                <label class="control-label col-md-4">Alias *</label>
                                <div class="col-md-8">
                                    <input class="form-control" name="alias" id="alias" type="text"
                                           maxlength="25" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Penerima *</label>
                                <div class="col-md-8">
                                    <input class="form-control" name="pic_name" id="pic_name"
                                           type="text" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Alamat *</label>
                                <div class="col-md-8">
                                    <input class="form-control" name="address" id="address"
                                           type="text" validate="required[]" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">No. Telp</label>
                                <div class="col-md-8">
                                    <input class="form-control" name="phone_no" id="phone_no"
                                           type="text"
                                           data-inputmask="&quot;mask&quot;: &quot;9999 9999 9999 999&quot;"
                                           data-mask="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Propinsi *</label>
                                <div class="col-md-8" id="select_province"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Kota / Kabupaten *</label>
                                <div class="col-md-8" id="select_city"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Kecamatan *</label>
                                <div class="col-md-8" id="select_district"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Kode Pos</label>
                                <div class="col-md-8">
                                    <input class="form-control" name="zip_code" id="zip_code"
                                           type="text" />
                                </div>
                            </div>
                            &nbsp;
                            <div class="form-group">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <a type="button" class="btn btn-warning"
                                       data-dismiss="modal"
                                       style="background-color: #EC7115; width: 100%"> <i
                                            class="fa fa-arrow-left"></i>&nbsp;Kembali
                                    </a>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <button type="submit" name="btnSave"
                                            class="btn btn-success" style="width: 100%">
                                        <i class="fa fa-save"></i>&nbsp;Simpan
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <link rel="stylesheet" type="text/css"
          href="<?php echo get_css_url() ?>daterangepicker-bs3.css" />
    <script type="text/javascript"
    src="<?php echo get_js_url() ?>moment.min.js"></script>
    <script type="text/javascript"
    src="<?php echo get_js_url() ?>daterangepicker.js"></script>
    <div class="p-10px">
        <div class="box no-border">
            <form class="form-horizontal" method="post" id="frmMain"
                  action="<?php echo get_base_url() . $data_auth[FORM_URL] ?>"
                  enctype="multipart/form-data">
                <div class="box-body">
                    <section class="col-md-12">                       
                        <?php echo get_csrf_partner_token(); ?>
                        <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                            <input class="form-control" name="seq"
                                   type="hidden"
                                   value="<?php echo $data_sel[LIST_DATA][0][0]->seq; ?>">
                               <?php } ?>
                        <div>
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#t_1"
                                                                          aria-controls="t_1" role="tab" data-toggle="tab">Info Login</a></li>
                                <li role="presentation"><a href="#t_2" aria-controls="t_2"
                                                           role="tab" data-toggle="tab">Daftar Bank</a></li>
                                <li role="presentation"><a href="#t_3" aria-controls="t_3"
                                                           role="tab" data-toggle="tab">Alamat Agen</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="t_1">
                                    <br />
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Email</label>
                                            <div class="col-md-9">
                                                <input class="form-control" id="info_login_email"
                                                       name="info_login_email" type="text"
                                                       placeholder="Input Email" validate="email[]"
                                                       value="<?php echo $data_auth[FORM_ACTION] == ACTION_EDIT ? $data_sel[LIST_DATA][0][0]->email : "" ?>"
                                                       <?php echo $data_auth[FORM_ACTION] == ACTION_EDIT ? "disabled" : "" ?>>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Password</label>
                                            <div class="col-md-9">
                                                <input class="form-control" id="password"
                                                       name="info_login_password" type="password"
                                                       placeholder="Input password" value="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Status</label>
                                            <div class="col-md-9">
                                                <div class="switch-style">
                                                    <label class="switch-lbl"> 
                                                        <?php if ($data_err[ERROR] == true && isset($data_err[ERROR_MESSAGE])) { ?>
                                                            <input type="checkbox"
                                                                   class="no-show" id="account_status"
                                                                   name="info_login_account_status"
                                                                   <?php echo $this->input->post('info_login_account_status') == 'on' ? 'checked' : ''; ?>>
                                                            <div
                                                                class="switch <?php echo $this->input->post('info_login_account_status') == 'on' ? 'switchOn' : ''; ?>">
                                                                <span
                                                                    class="<?php echo $this->input->post('info_login_account_status') == 'on' ? 'account-status-label-on' : 'account-status-label-off'; ?>"
                                                                    id='label-account-status'></span>
                                                            </div>
                                                        <?php } else { ?>
                                                            <input type="checkbox"
                                                                   class="no-show" id="account_status"
                                                                   name="info_login_account_status"
                                                                   <?php echo $data_auth[FORM_ACTION] == ACTION_EDIT ? ($data_sel[LIST_DATA][0][0]->status != 'A' ? '' : 'checked') : 'checked'; ?>>
                                                            <div
                                                                class="switch <?php echo $data_auth[FORM_ACTION] == ACTION_EDIT ? ($data_sel[LIST_DATA][0][0]->status != 'A' ? '' : 'switchOn') : 'switchOn' ?> ">
                                                                <span
                                                                    class="<?php echo $data_auth[FORM_ACTION] == ACTION_EDIT ? ($data_sel[LIST_DATA][0][0]->status != 'A' ? 'account-status-label-off' : 'account-status-label-on') : 'account-status-label-on' ?>"
                                                                    id='label-account-status'></span>
                                                            </div>
                                                        <?php } ?>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Nama</label>
                                            <div class="col-md-9">
                                                <input class="form-control" id="info_agent_name"
                                                       name="info_agent_name" type="text" placeholder="Nama Agen"
                                                       value="<?php echo $data_auth[FORM_ACTION] == ACTION_EDIT ? $data_sel[LIST_DATA][0][0]->name : "" ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Tanggal Lahir</label>
                                            <div class="col-md-9">
                                                <input date_type="date" class="form-control"
                                                       name="info_agent_birthday" id="birthday"
                                                       placeholder="Tanggal Lahir" type="text" readonly
                                                       value="<?php echo $data_auth[FORM_ACTION] == ACTION_EDIT ? cdate($data_sel[LIST_DATA][0][0]->birthday) : "" ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Jenis Kelamin</label>
                                            <div class="col-md-9">
                                                <?php if ($data_err[ERROR] == true && isset($data_err[ERROR_MESSAGE])) { ?>
                                                    <?php echo display_gender($this->input->post('info_agent_gender'), 'info_agent_gender') ?>
                                                <?php } else { ?>
                                                    <?php echo display_gender($data_auth[FORM_ACTION] == ACTION_EDIT ? $data_sel[LIST_DATA][0][0]->gender : '', 'info_agent_gender') ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">No Telepon</label>
                                            <div class="col-md-9">
                                                <input class="form-control" id="info_agent_mobile_phone" name="info_agent_mobile_phone"
                                                       type="text" placeholder="Nomor Telpon Agen"
                                                       data-inputmask="&quot;mask&quot;: &quot;9999 9999 9999 999&quot;"
                                                       data-mask=""
                                                       value="<?php echo $data_auth[FORM_ACTION] == ACTION_EDIT ? $data_sel[LIST_DATA][0][0]->mobile_phone : '' ?>">
                                            </div>
                                        </div>                                                                
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Komisi Agent</label>
                                            <div class="col-md-9">
                                                <input class="form-control auto_dec" id="commision_pro_rate" name="commision_pro_rate"
                                                       type="text" placeholder="Komisi Agent" validate="required[]"
                                                       value="<?php echo ($data_auth[FORM_ACTION] == ACTION_EDIT ? (isset($data_sel[LIST_DATA]) && isset($data_sel[LIST_DATA][0][0]->commission_pro_rate) ? $data_sel[LIST_DATA][0][0]->commission_pro_rate : '' ) : '') ?>">
                                                       <?php // echo (isset($data_sel[LIST_DATA]) && $data_sel[LIST_DATA][0][0]->commission_pro_rate != '' ? $data_sel[LIST_DATA][0][0]->commission_pro_rate : '') ?>
                                            </div>
                                        </div>                                                                

                                    </div>
                                    <div class="col-md-12">
                                        <?php
                                        $gambar = "";
                                        if (isset($data_sel[LIST_DATA][0])) {
                                            if ($data_sel[LIST_DATA][0][0]->profile_img != '') {
                                                $gambar = get_img_url() . ROUTE_AGENT . $data_sel[LIST_DATA][0][0]->profile_img;
                                            }
                                        }
                                        ?>
                                        <div class="row">
                                            <div class="col-xs-2" style="min-width: 200px;">
                                                <div class="slim height-img full-img" data-label="Drop your avatar here" >
                                                    <?php if ($data_err[ERROR] == true && isset($data_err[ERROR_MESSAGE])) { ?>
                                                        <input type="file" accept="image/gif,image/jpeg,image/png"
                                                               name="agent_photo_profil_img"><br> <img
                                                               width="<?php echo unserialize(IMAGE_DIMENSION_LOGO)["max_width"] ?>px"
                                                               src="<?php echo json_decode($this->input->post('agent_photo_profil_img'), true)['output']['image'] ?>"
                                                               alt="Gambar Profil">                                                    
                                                           <?php } else { ?>
                                                        <input type="file" accept="image/gif,image/jpeg,image/png"
                                                               name="agent_photo_profil_img"><br> <img
                                                               width="<?php echo unserialize(IMAGE_DIMENSION_LOGO)["max_width"] ?>px"
                                                               <?php echo isset($data_sel[LIST_DATA][0][0]->profile_img) ? ( $data_sel[LIST_DATA][0][0]->profile_img == '' ? '' : 'src="' . AGENT_UPLOAD_IMAGE . $data_sel[LIST_DATA][0][0]->profile_img . '"') : '' ?>
                                                               alt="Gambar Profil">
                                                           <?php } ?>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <p class="info-text">
                                                    <br> <br> <b>Photo Profile</b> Gambar yang di upload harus
                                                    dalam bentuk JPEG, JPG, PNG dengan resolusi min 150x150
                                                    sampai dengan 200x200 harus berbentuk persegi
                                                </p>
                                            </div>
                                        </div>
                                    </div>    
                                </div>                                                                
                                <div role="tabpanel" class="tab-pane" id="t_2">
                                    <br />
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Bank</label>
                                            <div class="col-md-9">
                                                <input class="form-control" id="info_bank_name"
                                                       name="info_bank_name" type="text"
                                                       placeholder="Input Nama Bank" validate="bank[]"
                                                       value="<?php echo $data_auth[FORM_ACTION] == ACTION_EDIT ? $data_sel[LIST_DATA][0][0]->bank_name : "" ?>"
                                                       <?php echo $data_auth[FORM_ACTION] == ACTION_EDIT ? "" : "" ?>>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Cabang</label>
                                            <div class="col-md-9">
                                                <input class="form-control" id="info_bank_branch"
                                                       name="info_bank_branch" type="text"
                                                       placeholder="Input Cabang Bank" validate="bank_branch[]"
                                                       value="<?php echo $data_auth[FORM_ACTION] == ACTION_EDIT ? $data_sel[LIST_DATA][0][0]->bank_branch_name : "" ?>"
                                                       <?php echo $data_auth[FORM_ACTION] == ACTION_EDIT ? "" : "" ?>>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Nomor Rekening</label>
                                            <div class="col-md-9">
                                                <input class="form-control" id="info_bank_acct_no"
                                                       name="info_bank_acct_no" type="text"
                                                       placeholder="Input No. Rekening Bank " validate="bank_acct_no[]"
                                                       value="<?php echo $data_auth[FORM_ACTION] == ACTION_EDIT ? $data_sel[LIST_DATA][0][0]->bank_acct_no : "" ?>"
                                                       <?php echo $data_auth[FORM_ACTION] == ACTION_EDIT ? "" : "" ?>>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Rekening A/N</label>
                                            <div class="col-md-9">
                                                <input class="form-control" id="info_bank_acct_name"
                                                       name="info_bank_acct_name" type="text"
                                                       placeholder="Input Nama Rekening Bank" validate="bank[]"
                                                       value="<?php echo $data_auth[FORM_ACTION] == ACTION_EDIT ? $data_sel[LIST_DATA][0][0]->bank_acct_name : "" ?>"
                                                       <?php echo $data_auth[FORM_ACTION] == ACTION_EDIT ? "" : "" ?>>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end t2 -->
                                <div role="tabpanel" class="tab-pane" id="t_3">
                                    <br />
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="box box-default">
                                                <div class="box-header with-border">
                                                    <div class="box-tools" style="left:0px !important;">
                                                        <a href="#"
                                                           onclick="add_address('<?php echo $data_auth[FORM_ACTION] == ACTION_EDIT ? $data_sel[LIST_DATA][0][0]->seq : '' ?>')"
                                                           class="color-toko1001btnbtn-flatbtn-confirm"> <i
                                                                class="fa fa-plus"></i> Tambah Alamat Baru
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="box-body" id='agent_address'>
                                                    <?php
                                                    if ($data_auth[FORM_ACTION] == ACTION_EDIT && isset($data_sel[LIST_DATA][1])) {
                                                        $i = 0;
                                                        foreach ($data_sel[LIST_DATA][1] as $row) {
                                                            $i ++;
                                                            ?>
                                                            <div
                                                                class="col-xs-6" id="<?php echo $row->address_seq; ?>">
                                                                <div class="box box-custom box-solid">
                                                                    <div class="box-header with-border">
                                                                        <h3 class="box-title address-tittle">
                                                                            <?php echo get_display_value($row->alias); ?>
                                                                        </h3>
                                                                        <div class="box-tools pull-right">
                                                                            <button type="button"
                                                                                    onclick="edit_address('<?php echo $row->address_seq; ?>', '<?php echo $row->address_seq; ?>',
                                                                                                                '<?php echo $data_auth[FORM_ACTION] == ACTION_EDIT ? $data_sel[LIST_DATA][0][0]->seq : '' ?>',
                                                                                                                '<?php echo $row->alias; ?>',
                                                                                                                '<?php echo $row->pic_name; ?>',
                                                                                                                '<?php echo $row->address; ?>',
                                                                                                                '<?php echo $row->phone_no; ?>',
                                                                                                                '<?php echo $row->province_seq; ?>',
                                                                                                                '<?php echo $row->province_name; ?>',
                                                                                                                '<?php echo $row->city_seq; ?>',
                                                                                                                '<?php echo $row->city_name; ?>',
                                                                                                                '<?php echo $row->district_seq; ?>',
                                                                                                                '<?php echo $row->district_name; ?>',
                                                                                                                '<?php echo $row->zip_code; ?>')"
                                                                                    class="btn btn-box-tool btn-font-color-black">
                                                                                <i class="fa fa-edit">&nbsp;Edit</i>
                                                                            </button>
                                                                            <button type="button"
                                                                                    onclick="delete_address('<?php echo $row->address_seq; ?>', '<?php echo $row->address_seq; ?>', '<?php echo isset($data_sel[LIST_DATA][0][0]->seq) ? $data_sel[LIST_DATA][0][0]->seq : '' ?>')"
                                                                                    class="btn btn-box-tool btn-font-color-black">
                                                                                <i class="fa fa-trash">&nbsp;Delete</i>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                    <div class="box-body box-body-custom">
                                                                        <div class="row">
                                                                            <div class="col-xs-4">
                                                                                <i class="fa fa-user"></i>&nbsp;Penerima
                                                                            </div>
                                                                            <div class="col-xs-1">:</div>
                                                                            <div class="col-xs-7"><?php echo ucfirst(get_display_value($row->pic_name)); ?></div>

                                                                            <div class="col-xs-4">
                                                                                <i class="fa fa-map-marker"></i>&nbsp;Alamat
                                                                            </div>
                                                                            <div class="col-xs-1">:</div>
                                                                            <div class="col-xs-7"><?php echo ucfirst(get_display_value($row->address)); ?></div>

                                                                            <div class="col-xs-4">
                                                                                <i class="fa fa-thumb-tack"></i>&nbsp;Kota
                                                                            </div>
                                                                            <div class="col-xs-1">:</div>
                                                                            <div class="col-xs-7"><?php echo ucfirst(get_display_value($row->province_name) . " - " . ($row->city_name)); ?></div>

                                                                            <div class="col-xs-4">
                                                                                <i class="fa fa-bookmark"></i>&nbsp;Kode Pos
                                                                            </div>
                                                                            <div class="col-xs-1">:</div>
                                                                            <div class="col-xs-7"><?php echo (isset($row->zip_code) && $row->zip_code != "") ? get_display_value($row->zip_code) : " - "; ?></div>

                                                                            <div class="col-xs-4">
                                                                                <i class="fa fa-phone"></i>&nbsp;Nomor Telp
                                                                            </div>
                                                                            <div class="col-xs-1">:</div>
                                                                            <div class="col-xs-7"><?php echo ucfirst(str_replace("_", "", get_display_value($row->phone_no))); ?></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <input type="hidden" name="address_agent_uniqId[]"
                                                                       id="uniqId_<?php echo $row->address_seq; ?>"
                                                                       value="<?php echo $row->address_seq; ?>" /> <input
                                                                       type="hidden" name="address_agent_agent_seq[]"
                                                                       id="agent_seq_<?php echo $row->address_seq; ?>"
                                                                       value="<?php echo $data_auth[FORM_ACTION] == ACTION_EDIT ? $data_sel[LIST_DATA][0][0]->seq : '' ?>" />
                                                                <input type="hidden" name="address_agent_address_seq[]"
                                                                       id="address_seq_<?php echo $row->address_seq; ?>"
                                                                       value="<?php echo $row->address_seq; ?>" /> <input
                                                                       type="hidden" name="address_agent_alias[]"
                                                                       id="alias_<?php echo $row->address_seq; ?>"
                                                                       value="<?php echo $row->alias ?>" /> <input type="hidden"
                                                                       name="address_agent_pic_name[]"
                                                                       id="pic_name_<?php echo $row->address_seq; ?>"
                                                                       value="<?php echo $row->pic_name ?>" /> <input
                                                                       type="hidden" name="address_agent_address[]"
                                                                       id="address_<?php echo $row->address_seq; ?>"
                                                                       value="<?php echo $row->address ?>" /> <input type="hidden"
                                                                       name="address_agent_phone_no[]"
                                                                       id="phone_no_<?php echo $row->address_seq; ?>"
                                                                       value="<?php echo $row->phone_no ?>" /> <input
                                                                       type="hidden" name="address_agent_province_seq[]"
                                                                       id="province_seq_<?php echo $row->address_seq; ?>"
                                                                       value="<?php echo $row->province_seq ?>" /> <input
                                                                       type="hidden" name="address_agent_province_name[]"
                                                                       id="province_name_<?php echo $row->address_seq; ?>"
                                                                       value="<?php echo $row->province_name ?>" /> <input
                                                                       type="hidden" name="address_agent_city_seq[]"
                                                                       id="city_seq_<?php echo $row->address_seq; ?>"
                                                                       value="<?php echo $row->city_seq ?>" /> <input
                                                                       type="hidden" name="address_agent_city_name[]"
                                                                       id="city_name_<?php echo $row->address_seq; ?>"
                                                                       value="<?php echo $row->city_name ?>" /> <input
                                                                       type="hidden" name="address_agent_district_seq[]"
                                                                       id="district_seq_<?php echo $row->address_seq; ?>"
                                                                       value="<?php echo $row->district_seq ?>" /> <input
                                                                       type="hidden" name="address_agent_district_name[]"
                                                                       id="district_name_<?php echo $row->address_seq; ?>"
                                                                       value="<?php echo $row->district_name ?>" /> <input
                                                                       type="hidden" name="address_agent_zip_code[]"
                                                                       id="zip_code_<?php echo $row->address_seq; ?>"
                                                                       value="<?php echo $row->zip_code ?>" />
                                                            </div>												
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--t_3-->

                                <!--t_4-->
                                <!-- end t5 -->
                            </div>
                        </div>
                        <div class="col-md-12">
                            </br>
                            <div class="form-group">
                                <?php if ($data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
                                    <div class="col-md-3"><?php echo get_back_button(); ?> </div>
                                <?php } elseif ($data_auth[FORM_ACTION] == ACTION_ADD) { ?>
                                    <div class="col-md-3"><?php echo get_save_add_button(); ?> </div>
                                <?php } elseif ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                                    <div class="col-md-3"><?php echo get_save_edit_button(); ?> </div>
                                <?php } else { ?>
                                    <div class="col-md-3"><?php echo get_back_button(); ?> </div>
                                <?php } ?>				
                            </div>
                        </div>

                    </section>
                </div>
            </form>
        </div>
    </div>
    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT || $data_auth[FORM_ACTION] == ACTION_ADD) { ?>
        <script type="text/javascript">
        <?php if ($data_err[ERROR] == true && isset($data_err[ERROR_MESSAGE])) { ?>
            <?php if ($data_auth[FORM_ACTION] == ACTION_ADD) { ?>
                    $('#info_login_email').val('<?php echo $this->input->post('info_login_email') ?>');
            <?php } else { ?>
                    $('#info_login_email').val('<?php echo isset($data_sel[LIST_DATA][0][0]->email) ? $data_sel[LIST_DATA][0][0]->email : '' ?>');
            <?php } ?>
                $('#commision_pro_rate').val('<?php echo $this->input->post('commision_pro_rate') ?>');
                $('#password').val('<?php echo $this->input->post('info_login_password') ?>');
                $('#info_agent_name').val('<?php echo $this->input->post('info_agent_name') ?>');
                $('#birthday').val('<?php echo $this->input->post('info_agent_birthday') ?>');
                $('#info_agent_mobile_phone').val('<?php echo $this->input->post('info_agent_mobile_phone') ?>');

                $('#info_bank_name').val('<?php echo $this->input->post('info_bank_name') ?>');
                $('#info_bank_branch').val('<?php echo $this->input->post('info_bank_branch') ?>');
                $('#info_bank_acct_no').val('<?php echo $this->input->post('info_bank_acct_no') ?>');
                $('#info_bank_acct_name').val('<?php echo $this->input->post('info_bank_acct_name') ?>');

                address_agent_uniqId = '<?php echo json_encode($this->input->post('address_agent_uniqId')) ?>';
                each_address_agent_uniqId = JSON.parse(address_agent_uniqId);
                address_agent_agent_seq = '<?php echo json_encode($this->input->post('address_agent_agent_seq')) ?>';
                each_address_agent_agent_seq = JSON.parse(address_agent_agent_seq);
                address_agent_address_seq = '<?php echo json_encode($this->input->post('address_agent_address_seq')) ?>';
                each_address_agent_address_seq = JSON.parse(address_agent_address_seq);
                address_agent_alias = '<?php echo json_encode($this->input->post('address_agent_alias')) ?>';
                each_address_agent_alias = JSON.parse(address_agent_alias);
                address_agent_pic_name = '<?php echo json_encode($this->input->post('address_agent_pic_name')) ?>';
                each_address_agent_pic_name = JSON.parse(address_agent_pic_name);
                address_agent_address = '<?php echo json_encode($this->input->post('address_agent_address')) ?>';
                each_address_agent_address = JSON.parse(address_agent_address);
                address_agent_phone_no = '<?php echo json_encode($this->input->post('address_agent_phone_no')) ?>';
                each_address_agent_phone_no = JSON.parse(address_agent_phone_no);
                address_agent_province_seq = '<?php echo json_encode($this->input->post('address_agent_province_seq')) ?>';
                each_address_agent_province_seq = JSON.parse(address_agent_province_seq);
                address_agent_province_name = '<?php echo json_encode($this->input->post('address_agent_province_name')) ?>';
                each_address_agent_province_name = JSON.parse(address_agent_province_name);
                address_agent_city_seq = '<?php echo json_encode($this->input->post('address_agent_city_seq')) ?>';
                each_address_agent_city_seq = JSON.parse(address_agent_city_seq);
                address_agent_city_name = '<?php echo json_encode($this->input->post('address_agent_city_name')) ?>';
                each_address_agent_city_name = JSON.parse(address_agent_city_name);
                address_agent_district_seq = '<?php echo json_encode($this->input->post('address_agent_district_seq')) ?>';
                each_address_agent_district_seq = JSON.parse(address_agent_district_seq);
                address_agent_district_name = '<?php echo json_encode($this->input->post('address_agent_district_name')) ?>';
                each_address_agent_district_name = JSON.parse(address_agent_district_name);
                address_agent_zip_code = '<?php echo json_encode($this->input->post('address_agent_zip_code')) ?>';
                each_address_agent_zip_code = JSON.parse(address_agent_zip_code);
                if (each_address_agent_uniqId !== null) {
                    $.each(each_address_agent_uniqId, function (index, value) {
                        act = '<?php echo ACTION_ADD ?>';
                        alias = each_address_agent_alias[index];
                        pic_name = each_address_agent_pic_name[index];
                        address = each_address_agent_address[index];
                        phone_no = each_address_agent_phone_no[index];
                        province_seq = each_address_agent_province_seq[index];
                        province_name = each_address_agent_province_name[index];
                        city_seq = each_address_agent_city_seq[index];
                        city_name = each_address_agent_city_name[index];
                        district_seq = each_address_agent_district_seq[index];
                        district_name = each_address_agent_district_name[index];
                        zip_code = each_address_agent_zip_code[index];
                        address_seq = each_address_agent_address_seq[index];
                        agent_seq = each_address_agent_agent_seq[index];
                        uniqId = each_address_agent_uniqId[index];
                        delete_address_no_confirm(uniqId, address_seq, agent_seq);
                        update_data_address(uniqId, address_seq, agent_seq, alias, pic_name, address, phone_no, province_seq, province_name, city_seq, city_name, district_seq, district_name, zip_code, act);
                    });
                }

                agent_commission_seq = '<?php echo json_encode($this->input->post("agent_commission_seq")); ?>';
                agent_commission_lvl_cat = '<?php echo json_encode($this->input->post("agent_commission_lvl_cat")); ?>';
                agent_commission_fee = '<?php echo json_encode($this->input->post("agent_commission_fee")); ?>';
                each_agent_commission_seq = JSON.parse(agent_commission_seq);
                $('input[name="agent_commission_seq[]"]').each(function () {
                    if ($.inArray($(this).val(), each_agent_commission_seq) >= 0) {
                        $(this).attr('checked', true);
                        show($(this).val());
                    }
                });
                each_agent_commission_lvl_cat = JSON.parse(agent_commission_lvl_cat);
                each_agent_commission_fee = JSON.parse(agent_commission_fee);
                $('input[name="agent_commission_lvl_cat[]"]').each(function (index, value) {
                    if ($.inArray($(this).val(), each_agent_commission_lvl_cat) >= 0) {
                        $(this).attr('checked', true);
                        sht(true, 'lvlcat_' + $(this).val().split('~')[0]);
                        $('#lvlcat_' + $(this).val().split('~')[0]).val(each_agent_commission_fee[index]);
                    }
                });
            <?php if ($data_err[ERROR_MESSAGE][0] == 'ERROR : Email ' . ERROR_VALIDATION_MUST_FILL || $data_err[ERROR_MESSAGE][0] == 'ERROR : Password ' . ERROR_VALIDATION_MUST_FILL) { ?>
                    $('ul.nav-tabs li').removeClass('active');
                    $('ul.nav-tabs li a[href="#t_1"]').parent().addClass('active');
                    $('div.tab-content div.tab-pane').removeClass('active');
                    $('#t_1').addClass('active');
            <?php } ?>
            <?php
            if ($data_err[ERROR_MESSAGE][0] == 'ERROR : Nama ' . ERROR_VALIDATION_MUST_FILL || $data_err[ERROR_MESSAGE][0] == 'ERROR : Tanggal Lahir ' . ERROR_VALIDATION_MUST_FILL || $data_err[ERROR_MESSAGE][0] == 'ERROR : Jenis Kelamin ' . ERROR_VALIDATION_MUST_FILL || $data_err[ERROR_MESSAGE][0] == 'ERROR : No Telepon ' . ERROR_VALIDATION_MUST_FILL
            ) {
                ?>
                    $('ul.nav-tabs li').removeClass('active');
                    $('ul.nav-tabs li a[href="#t_1"]').parent().addClass('active');
                    $('div.tab-content div.tab-pane').removeClass('active');
                    $('#t_1').addClass('active');
            <?php } ?>
        <?php } ?>
            function showhide(idmenu) {
                $("#sm" + idmenu).toggle("slow");
            }
            function show(idmenu) {
                $("#sm" + idmenu).show();
            }

            $(document).ready(function () {
                $('.switch').click(function () {
                    var kondisi = $('#account_status').prop('checked');
                    if (kondisi === true) {
                        $('#label-account-status').removeClass('account-status-label-on');
                        $('#label-account-status').addClass('account-status-label-off');
                    } else {
                        $('#label-account-status').removeClass('account-status-label-off');
                        $('#label-account-status').addClass('account-status-label-on');
                    }
                    $(this).toggleClass('switchOn');
                });
                $('#info_login_email').keyup(function () {
                    $('#agent_commission_email').val($(this).val());
                });
                $('#info_agent_name').keyup(function () {
                    $('#agent_commission_name').val($(this).val());
                });
            });
            $('input[name="info_agent_birthday"]').daterangepicker({
                format: 'DD-MMM-YYYY',
                showDropdowns: true,
                singleDatePicker: true
            });
            function previewimg(thisval) {
                if (thisval.files && thisval.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {

                        $('#iprev').attr('src', e.target.result).height(70);
                    }
                    reader.readAsDataURL(thisval.files[0]);
                }
            }
            $('.slim').slim({
                ratio: '1:1',
                minSize: {
                    width: <?php echo unserialize(IMAGE_DIMENSION_LOGO)["min_width"] ?>,
                    height: <?php echo unserialize(IMAGE_DIMENSION_LOGO)["min_height"] ?>,
                },
                size: {
                    width: <?php echo unserialize(IMAGE_DIMENSION_LOGO)["max_width"] ?>,
                    height: <?php echo unserialize(IMAGE_DIMENSION_LOGO)["max_height"] ?>,
                },
                maxFileSize: "2",
                jpegCompression: "60",
                label: 'Upload Foto Agen Anda',
                onRemove: function (slim, data) {
                    $("#oldfile").val("");
                }

            });
            function add_address(agent_seq) {
                $.ajax({
                    url: "<?php echo base_url() ?>partner/agent_address",
                    type: "GET",
                    data: {
                        act: "<?php echo ACTION_ADD ?>",
                        agent_seq: agent_seq,
                    },
                    success: function (response) {
                        if (isSessionExpired(response)) {
                            response_object = json_decode(response);
                            url = response_object.url;
                            location.href = url;
                        } else {
                            result = JSON.parse(response);
                            province = '<select id="drop_province" validate="required[]" class="form-control" name="province_seq" onchange="change_province()">';
                            province += '<option value="">-- Pilih --</option>';
                            $(result.province_name).each(function (i) {
                                province += '<option value="' + result.province_name[i].seq + '">' + result.province_name[i].name + '</option>';
                            });
                            province += '</select>';
                            city = '<select id="drop_city" validate="required[]" class="form-control" name="city_seq" onchange="change_city()">';
                            city += '<option value="">-- Pilih --</option>';
                            $(result.city_name).each(function (i) {
                                city += '<option value="' + result.city_name[i].seq + '">' + result.city_name[i].name + '</option>';
                            });
                            city += '</select>';
                            district = '<select id="drop_district" validate="required[]" class="form-control" name="district_seq">';
                            district += '<option value="">-- Pilih --</option>';
                            $(result.district_name).each(function (i) {
                                district += '<option value="' + result.district_name[i].seq + '">' + result.district_name[i].name + '</option>';
                            });
                            district += '</select>';

                            $('#address_title').html('TAMBAH ALAMAT');
                            $('#act').val('<?php echo ACTION_ADD ?>');
                            $('#uniqId').val(create_uniqId());
                            $('#address_seq').val('');
                            $('#agent_seq').val('<?php echo $data_auth[FORM_ACTION] == ACTION_EDIT ? $data_sel[LIST_DATA][0][0]->seq : '' ?>');
                            $('#alias').val('');
                            $('#pic_name').val('');
                            $('#address').val('');
                            $('#phone_no').val('');
                            $('#select_province').html(province);
                            $('#select_city').html(city);
                            $('#select_district').html(district);
                            $('#zip_code').val('');
                            $('#address_modal').modal('show');
                        }
                    },
                    error: function (request, error) {
                        alert('error');
                        //alert(error_response(request.status));
                    }
                });
            }
            function edit_address(uniqId, address_seq, agent_seq, alias, pic_name, address, phone_no, province_seq, province_name, city_seq, city_name, district_seq, district_name, zip_code) {
                $.ajax({
                    url: "<?php echo get_base_url() . "partner/agent_address" ?>",
                    type: "GET",
                    data: {
                        "act": "<?php echo ACTION_ADDITIONAL ?>",
                        "type": "task_province_city_district_change",
                        "province_seq": province_seq,
                        "city_seq": city_seq,
                        "district_seq": district_seq,
                    },
                    success: function (data) {
                        if (isSessionExpired(data)) {
                            response_object = json_decode(data);
                            url = response_object.url;
                            location.href = url;
                        } else {
                            result = JSON.parse(data);
                            province = '<select id="drop_province" validate="required[]" class="form-control" name="province_seq" onchange="change_province()">';
                            province += '<option value="">-- Pilih --</option>';
                            $(result.province_name).each(function (i) {
                                province += '<option value="' + result.province_name[i].seq + '"' + (province_seq == result.province_name[i].seq ? ' selected' : '') + '>' + result.province_name[i].name + '</option>';
                            });
                            province += '</select>';
                            city = '<select id="drop_city" validate="required[]" class="form-control" name="city_seq" onchange="change_city()">';
                            city += '<option value="">-- Pilih --</option>';
                            $(result.city_name).each(function (i) {
                                city += '<option value="' + result.city_name[i].seq + '"' + (city_seq == result.city_name[i].seq ? ' selected' : '') + '>' + result.city_name[i].name + '</option>';
                            });
                            city += '</select>';
                            district = '<select id="drop_district" validate="required[]" class="form-control" name="district_seq" onchange="change_district()">';
                            district += '<option value="">-- Pilih --</option>';
                            $(result.district_name).each(function (i) {
                                district += '<option value="' + result.district_name[i].seq + '"' + (district_seq == result.district_name[i].seq ? ' selected' : '') + '>' + result.district_name[i].name + '</option>';
                            });
                            district += '</select>';
                            $('#address_title').html('UBAH ALAMAT');
                            $('#act').val('<?php echo ACTION_EDIT ?>');
                            $('#uniqId').val(uniqId);
                            $('#address_modal').modal('show');
                            $('#address_seq').val(address_seq);
                            $('#agent_seq').val(agent_seq);
                            $('#alias').val(alias);
                            $('#pic_name').val(pic_name);
                            $('#address').val(address);
                            $('#phone_no').val(phone_no);
                            $('#select_province').html(province);
                            $('#select_city').html(city);
                            $('#select_district').html(district);
                            $('#zip_code').val(zip_code);
                            $('#address_modal').modal('show');
                            return false;
                        }
                    },
                    error: function (request, error) {
                        alert(error_response(request.status));
                    }
                });
            }
            function delete_address_no_confirm(uniqId, address_seq, agent_seq) {
                if (address_seq == '') {
                    seq = uniqId;
                } else {
                    seq = address_seq;
                }
                $('#' + seq).slideUp(1000, function () {
                    $(this).remove();
                });
            }

            function delete_address(uniqId, address_seq, agent_seq) {
                if (confirm("Apakah anda yakin menghapus data yang dipilih ?")) {
                    if (address_seq == '') {
                        seq = uniqId;
                    } else {
                        seq = address_seq;
                    }
                    $('#' + seq).slideUp(1000, function () {
                        $(this).remove();
                    });
                }
            }

            function change_province() {
                var province_seq = $('#drop_province').val();
                $.ajax({
                    url: "<?php echo get_base_url() . "partner/agent_address" ?>",
                    type: "GET",
                    dataType: "json",
                    data: {
                        "act": "<?php echo ACTION_ADDITIONAL ?>",
                        "type": "<?php echo TASK_PROVINCE_CHANGE ?>",
                        "province_seq": province_seq
                    },
                    success: function (data) {
                        if (isSessionExpired(data)) {
                            response_object = json_decode(data);
                            url = response_object.url;
                            location.href = url;
                        } else {
                            $('#drop_city option').remove();
                            $("#drop_city").append("<option value=\"" + "\">-- Pilih --</option>");
                            $('#drop_district option').remove();
                            $(data).each(function (i) {
                                $("#drop_city").append("<option value=\"" + data[i].seq + "\">" + data[i].name + "</option>")
                            });
                        }
                    },
                    error: function (request, error) {
                        alert(error_response(request.status));
                    }
                });
            }

            function change_city() {
                var city_seq = $('#drop_city').val();
                $.ajax({
                    url: "<?php echo get_base_url() . "partner/agent_address" ?>",
                    type: "GET",
                    dataType: "json",
                    data: {
                        "act": "<?php echo ACTION_ADDITIONAL ?>",
                        "type": "<?php echo TASK_CITY_CHANGE ?>",
                        "city_seq": city_seq
                    },
                    success: function (data) {
                        if (isSessionExpired(data)) {
                            response_object = json_decode(data);
                            url = response_object.url;
                            location.href = url;
                        } else {
                            $('#drop_district option').remove();
                            $("#drop_district").append("<option value=\"" + "\">-- Pilih --</option>");
                            $(data).each(function (i) {
                                $("#drop_district").append("<option value=\"" + data[i].seq + "\">" + data[i].name + "</option>")
                            });
                        }
                    },
                    error: function (request, error) {
                        alert(error_response(request.status));
                    }
                });
            }


            $("[data-mask]").inputmask();
            $('#frmAddress').validate({
                highlight: function (element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                rules: {
                    alias: {
                        required: true,
                        maxlength: 10
                    },
                    pic_name: {
                        required: true,
                    },
                    address: {
                        required: true,
                    },
                    province_seq: {
                        required: true,
                    },
                    city_seq: {
                        required: true,
                    },
                    district_seq: {
                        required: true,
                    }
                },
                submitHandler: function () {
                    create_address_list();
                    return false;

                }
            });
            function create_address_list() {
                agent_seq = '<?php echo $data_auth[FORM_ACTION] == ACTION_EDIT ? $data_sel[LIST_DATA][0][0]->seq : '' ?>';
                alias = $('#alias').val();
                pic_name = $('#pic_name').val();
                address = $('#address').val();
                phone_no = $('#phone_no').val();
                province_seq = $('#drop_province').val();
                province_name = $('#drop_province option:selected').text();
                city_seq = $('#drop_city').val();
                city_name = $('#drop_city option:selected').text();
                district_seq = $('#drop_district').val();
                district_name = $('#drop_district option:selected').text();
                zip_code = $('#zip_code').val();
                address_seq = $('#address_seq').val();
                uniqId = $('#uniqId').val();
                act = $('#act').val();
                $('#address_modal').modal('hide');
                update_data_address(uniqId, address_seq, agent_seq, alias, pic_name, address, phone_no, province_seq, province_name, city_seq, city_name, district_seq, district_name, zip_code, act);
            }
            function ucfirst(string) {
                return string.charAt(0).toUpperCase() + string.slice(1);
            }
            function create_uniqId() {
                return Math.round(new Date().getTime() + (Math.random() * 100));
            }

            function update_data_address(uniqId, address_seq, agent_seq, alias, pic_name, address, phone_no, province_seq, province_name, city_seq, city_name, district_seq, district_name, zip_code, act) {
                if (act == '<?php echo ACTION_EDIT ?>') {
                    if (address_seq == '') {
                        seq = uniqId;
                    } else {
                        seq = address_seq;
                    }
                } else {
                    seq = uniqId;
                }
                data_address = '<div class="box box-custom box-solid">' +
                        '<div class="box-header with-border">' +
                        '<h3 class="box-title address-tittle">' + alias + '</h3>' +
                        '<div class="box-tools pull-right">' +
                        '<button type="button"' +
                        'onclick="edit_address(\'' + uniqId + '\',\'' + address_seq + '\',\'' + agent_seq + '\',\'' + alias + '\',\'' + pic_name + '\',\'' + address + '\',\'' + phone_no + '\',\'' + province_seq + '\',\'' + province_name + '\',\'' + city_seq + '\',\'' + city_name + '\',\'' + district_seq + '\',\'' + district_name + '\',\'' + zip_code + '\')"' +
                        'class="btn btn-box-tool btn-font-color-black">' +
                        '<i class="fa fa-edit">&nbsp;Edit</i>' +
                        '</button>' +
                        '<button type="button"' +
                        'onclick="delete_address(\'' + uniqId + '\',\'' + address_seq + '\',\'' + agent_seq + '\')"' +
                        'class="btn btn-box-tool btn-font-color-black">' +
                        '<i class="fa fa-trash">&nbsp;Delete</i>' +
                        '</button>' +
                        '</div>' +
                        '</div>' +
                        '<div class="box-body box-body-custom">' +
                        '<div class="row">' +
                        '<div class="col-xs-4">' +
                        '<i class="fa fa-user"></i>&nbsp;Penerima' +
                        '</div>' +
                        '<div class="col-xs-1">:</div>' +
                        '<div class="col-xs-7">' + ucfirst(pic_name) + '</div>' +
                        '<div class="col-xs-4">' +
                        '<i class="fa fa-map-marker"></i>&nbsp;Alamat' +
                        '</div>' +
                        '<div class="col-xs-1">:</div>' +
                        '<div class="col-xs-7">' + ucfirst(address) + '</div>' +
                        '<div class="col-xs-4">' +
                        '<i class="fa fa-thumb-tack"></i>&nbsp;Kota' +
                        '</div>' +
                        '<div class="col-xs-1">:</div>' +
                        '<div class="col-xs-7">' + ucfirst(province_name + ' - ' + city_name + '-' + district_name) + '</div>' +
                        '<div class="col-xs-4">' +
                        '<i class="fa fa-bookmark"></i>&nbsp;Kode Pos' +
                        '</div>' +
                        '<div class="col-xs-1">:</div>' +
                        '<div class="col-xs-7">' + zip_code + '</div>' +
                        '<div class="col-xs-4">' +
                        '<i class="fa fa-phone"></i>&nbsp;Nomor Telp' +
                        '</div>' +
                        '<div class="col-xs-1">:</div>' +
                        '<div class="col-xs-7">' + ucfirst(phone_no.replace('_', '')) + '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '<input type="hidden" name="address_agent_uniqId[]" id="uniqId_' + seq + '" value="' + uniqId + '" />' +
                        '<input type="hidden" name="address_agent_agent_seq[]" id="agent_seq_' + seq + '" value="<?php echo $data_auth[FORM_ACTION] == ACTION_EDIT ? $data_sel[LIST_DATA][0][0]->seq : '' ?>" />' +
                        '<input type="hidden" name="address_agent_address_seq[]" id="address_seq_' + seq + '" value="' + address_seq + '" />' +
                        '<input type="hidden" name="address_agent_alias[]" id="alias_' + seq + '" value="' + alias + '" />' +
                        '<input type="hidden" name="address_agent_pic_name[]" id="pic_name_' + seq + '" value="' + pic_name + '" />' +
                        '<input type="hidden" name="address_agent_address[]" id="address_' + seq + '" value="' + address + '" />' +
                        '<input type="hidden" name="address_agent_phone_no[]" id="phone_no_' + seq + '" value="' + phone_no + '" />' +
                        '<input type="hidden" name="address_agent_province_seq[]" id="province_seq_' + seq + '" value="' + province_seq + '" />' +
                        '<input type="hidden" name="address_agent_province_name[]" id="province_name_' + seq + '" value="' + province_name + '" />' +
                        '<input type="hidden" name="address_agent_city_seq[]" id="city_seq_' + seq + '" value="' + city_seq + '" />' +
                        '<input type="hidden" name="address_agent_city_name[]" id="city_name_' + seq + '" value="' + city_name + '" />' +
                        '<input type="hidden" name="address_agent_district_seq[]" id="district_seq_' + seq + '" value="' + district_seq + '" />' +
                        '<input type="hidden" name="address_agent_district_name[]" id="district_name_' + seq + '" value="' + district_name + '" />' +
                        '<input type="hidden" name="address_agent_zip_code[]" id="zip_code_' + seq + '" value="' + zip_code + '" />';
                if (act == '<?php echo ACTION_EDIT ?>') {
                    $('#' + seq).html(data_address);
                } else {
                    $('#agent_address').append('<div class="col-xs-6" id="' + seq + '">' + data_address + '</div>');
                }
            }

            function sht(nilai, idmenu) {
                if (nilai) {
                    $("#" + idmenu).show("slow");
                } else {
                    $("#" + idmenu).hide("slow");
                }
            }
        <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) echo "$('#email').prop('disabled', true);"; ?>

        </script>
        <?php
    }
} else {
    $totals = 0;
    ?>
    <div class="p-10px">
        <div class="box box-body no-border">
            <form id="frmSearchExcl" method="get"
                  url="<?php echo get_base_url() . $data_auth[FORM_URL]; ?>">
                <div class="row">
                    <div class="col-xs-8">
                        <div class="form-group">
                            <input class="form-control-cst" style="width: 40%" name="email"
                                   type="input" placeholder="Email Agent"
                                   value="<?php if (isset($_GET["email"])) echo $_GET["email"]; ?>">
                            <input class="form-control-cst" style="width: 40%" name="name"
                                   type="input" placeholder="Nama Agent"
                                   value="<?php if (isset($_GET["name"])) echo $_GET["name"]; ?>">
                            <input type="hidden" name="status" value="<?php echo $this->input->get('status') ?>" />
                            <input type="hidden" name="sort" value="<?php echo $this->input->get('sort') ?>" />
                            <input type="hidden" name="column_sort" value="<?php echo $this->input->get('column_sort') ?>" />
                            <button class="btn btn-danger" type="submit" style="margin-top: -4px">
                                <i class="fa fa-search" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
            <div class="col-xs-3 pull-right">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-6">
                                <button class="btn btn-success btn-block btn-block flat" type="button" id="btnxls" name="btnxls" onclick="data_report()"><i class="fa fa-file-excel-o"></i> Excel</button>
                            </div>
                            <?php echo get_order_filter("modified_date", "name", base_url($data_auth[FORM_URL]) . $filter_agent_data . '&status=' . $this->input->get('status'), $this->input->get("sort"), $this->input->get("column_sort")) ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-6 pull-right">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <ul class="nav nav-pills no-padding">
                                <li class="blue-link"><a class="btn btn-flat <?php echo $this->input->get('status') == '' ? 'btn-active' : '' ?>" href="<?php echo base_url($data_auth[FORM_URL]) . $filter_agent_data . '&status=' . $filter_sort ?>">Semua (<span id="status_all"></span>)</a></li>
                                <?php
                                $qty_status = 0;
                                foreach (json_decode(STATUS_AGENT) as $key => $stat) {
                                    if (isset($data_sel[LIST_DATA][0])) {
                                        $qty_status = get_qty_status($data_sel[LIST_DATA][0], $key);
                                        $totals = ($totals + $qty_status);
                                    } else {
                                        $qty_status = 0;
                                    }
                                    ?>
                                    <li class="blue-link">
                                        <a class="btn btn-flat <?php echo $this->input->get('status') == $key ? 'btn-active' : '' ?>" 
                                           href="<?php echo base_url($data_auth[FORM_URL]) . $filter_agent_data . '&status=' . $key . $filter_sort ?>">
                                            <?php echo $stat; ?> (<?php echo $qty_status ?>)
                                        </a>
                                    </li>
                                    <?php
                                }
                                ?>
                            </ul>        				
                        </div>
                    </div>
                </div>        
            </div>
            <?php require_once get_include_page_list_agent_new_content_header(); ?>
        </div>
    </div>



    <div class="p-10px">
        <div class="box box-body no-border">
            <div class="col-xs-5">
                <form enctype="multipart/form-data" method="post" action="<?php echo get_base_url() . $data_auth[FORM_URL] . '?type=agent_excel' ?>">
                    <div class="input-group">
                        <div class="input-group-btn">                    
                            <a href='<?php echo base_url(EXCEL_AGENT_DOWNLOAD_TEMPLATE) ?>' class="btn btn-warning btn-flat    ">Download Template excel agent</a>
                        </div>
                        <input type="file" class="form-control" id='upload_excel_action' name="excel-Agent" value=""/>
                        <input type="hidden" name="type" value="save_excel"/>
                        <div class="input-group-btn">                    
                            <?php echo get_additional() ?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $('upload_excel_action').change(function (e) {
            console.log(e);
        })
    </script>



    <div class="p-10px">
        <div class="row">
            <?php if (isset($data_list[LIST_DATA])) { ?>
                <?php foreach ($data_list[LIST_DATA] as $each_data) { ?>
                    <!--============================================================-->
                    <div class="col-xs-4">

                        <div class="box box-success with-border well" style="position: relative;margin-bottom: 10px;height: 300px;">
                            <div class="col-xs-12 no-padding" 
                                 style="
                                 width: 100%;
                                 white-space: nowrap;
                                 overflow: hidden;
                                 text-overflow: ellipsis
                                 ">
                                <a class="each-data-agent" href="<?php echo base_url() . $data_auth[FORM_URL] . '?' . CONTROL_GET_TYPE . '=' . ACTION_EDIT . '&key=' . $each_data->agent_seq ?>" >
                                    <strong><?php echo ucwords($each_data->name) ?></strong>
                                </a>

                                <button onclick='generate_password_agent("<?php echo $each_data->agent_seq; ?>")' class='generate-<?php echo $each_data->agent_seq; ?> btn btn-warning btn-flat btn-xs pull-right'>Reset Password</button>



                            </div>
                            <hr>

                            <div class="row">
                                <div class="col-sm-4 pos-relative">
                                    <?php if ($each_data->status != APPROVE_STATUS_CODE) { ?>
                                        <img src="<?php echo get_img_url() . 'not_active.png'; ?>" style="position: absolute;
                                             top: -15px;
                                             left: 0px;
                                             opacity: 0.7;" class="img-circle m-t-xs img-responsive"/>
                                         <?php } ?>
                                    <div class="text-center">
                                        <img src="<?php echo get_image_exist($each_data->profile_img, $each_data->gender, '\img\agent') ?>" class="img-circle m-t-xs img-responsive"/>
                                    </div>
                                </div>

                                <div class="col-xs-8">
                                    <div class="clearfix"></div>

                                    <div class="dt-agent">
                                        <div class="dt-agent-field">
                                            <i class="fa fa-user"></i> &nbsp;<?php echo $each_data->name ?>
                                        </div>
                                    </div>

                                    <div class="dt-agent">
                                        <div class="dt-agent-field">
                                            <i class="fa fa-envelope-o"></i> &nbsp;<?php echo $each_data->email ?>
                                        </div>
                                    </div>

                                    <div class="dt-agent">
                                        <div class="dt-agent-field">
                                            <i class="fa fa-venus-mars"></i> &nbsp;<?php echo (isset($each_data->gender) && $each_data->gender == "M" ? "Laki - Laki" : " Perempuan") ?>
                                        </div>
                                    </div>

                                    <div class="dt-agent">
                                        <div class="dt-agent-field">
                                            <i class="fa fa-money"></i> &nbsp;<?php echo (isset($each_data->bank_name) && $each_data->bank_name !== "" ? $each_data->bank_name : " - ") ?>
                                        </div>
                                    </div>

                                    <?php ?>

                                    <div class="dt-agent">
                                        <div class="dt-agent-field">
                                            <i class="fa fa-calendar"></i> &nbsp;<?php echo (isset($each_data->birthday) && $each_data->birthday !== "" ? date_format(date_create($each_data->birthday), "j, F Y") : " - ") ?>
                                        </div>
                                    </div>




                                </div>
                            </div>
                        </div>

                    </div>
                    <!--============================================================-->



                <?php } ?>
            <?php } ?>
        </div>
    </div>

    <div class="box box-default no-border">
        <div class="col-xs-10">
            <center>
                <?php echo $this->pagination->create_links(); ?>
            </center>
        </div>
        <div class="clearfix"></div>
    </div>
    <script>
        function generate_password_agent(agent_id) {
            if (confirm("Apakah anda yakin mereset password ?")) {
                var success_html = '<div class="alert alert-success-admin" style="margin-bottom: 0!important;><h4><i class="fa fa-info"></i></h4><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                var error_html = '<div class="alert alert-error-admin" style="margin-bottom: 0!important;> <h4><i class="fa fa-info"></i></h4><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                $(".generate-" + agent_id).html("<span class='fa fa-spinner fa-spin'></span>");
                $(".generate-" + agent_id).addClass('disabled');
                $.ajax({
                    url: "<?php echo get_base_url() . $data_auth[FORM_URL] ?>",
                    dataType: "JSON",
                    type: "POST",
                    data: {agent_seq: agent_id, btnAdditional: "act_s_adt", act: "<?php echo ACTION_ADDITIONAL ?>"},
                    success: function (data) {
                        $(".generate-" + agent_id).text("Buat Password baru");
                        $(".generate-" + agent_id).removeClass('disabled');
                        if (data.error == true)
                        {
                            $('#content-message').html(error_html + data.message + "</div>")
                        } else {
                            $('#content-message').html(success_html + "<div id='msg-shw' class='shw-success'>" + "Data telah berhasil disimpan ! " + "</div>")
                        }
                    },
                    error: function (xhr) {
                        alert(xhr.responseText);
                    }
                })
            }
        }

        $(document).ready(function () {
            $('.address-scroll').slimScroll({
                color: '#000',
                size: '3px',
                height: '80',
                alwaysVisible: false,
            });
        });

        $("#status_all").html("<?php echo $totals; ?>");
        function data_report() {
            var data = $('#frmSearchExcl').serializeArray();
            data.push({name: 'type', value: 'print_data_excel'}, {name: '<?php echo CONTROL_GET_TYPE ?>', value: '<?php echo ACTION_ADDITIONAL ?>'});
            $.ajax({
                url: "<?php echo get_base_url(); ?>partner/agent_data",
                data: data,
                type: "get",
                success: function (response) {
                    if (isSessionExpired(response)) {
                    } else {
                        try {
                            response = jQuery.parseJSON(response);
                            if (response != null) {
                                create_report(response);
                            } else {
                                alert("Tidak ada data.");
                            }
                        } catch (e) {
                            alert("Tidak ada data.");
                        }
                        return false;
                    }
                },
                error: function (request, error) {
                    alert(error_response(request.status));
                }
            });
        }

    </script>
    <?php
}
require_once get_include_page_list_partner_content_footer();
require_once get_include_content_partner_bottom_page_navigation();
?>