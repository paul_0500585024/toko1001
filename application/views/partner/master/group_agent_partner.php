<?php
require_once VIEW_BASE_PARTNER;
require_once get_include_content_partner_top_page_navigation();
echo $breadcrumb;
?>
<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="p-10px">    
        <div class="box box-default">
            <hr style="margin: 5px">
            <div class="box-body">
                <div class="col-xs-6">
                    <form class="form-horizontal" method="POST" action="">
                        <?php echo get_csrf_partner_token(); ?>
                        <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                            <input class="form-control"  name ="agent_group_seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->agent_group_seq; ?>">
                        <?php }
                        ?>
                        <div class="form-group"> 
                            <label for="group_name" class="col-sm-3 control-label">Group Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="agent_group_name" name="agent_group_name" value="<?php echo isset($data_sel[LIST_DATA][0]) && $data_sel[LIST_DATA][0]->agent_group_name != '' ? $data_sel[LIST_DATA][0]->agent_group_name : '' ?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="group_name" class="col-sm-3 control-label">Komisi Agent</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control auto_dec text-right" data-v-max="100" id="commission_fee_percent" name="commission_fee_percent" value="<?php echo (isset($data_sel[LIST_DATA][0]) && $data_sel[LIST_DATA][0]->commission_fee_percent ? $data_sel[LIST_DATA][0]->commission_fee_percent : '' ) ?>" />
                            </div>
                        </div>
                        <div class ="form-group">
                            <?php
                            if ($data_auth[FORM_ACTION] == ACTION_VIEW) {
                                ?>
                                <div class ="col-md-6"><?php echo get_back_button(); ?> </div>
                                <?php
                            } else {
                                if ($data_auth[FORM_ACTION] == ACTION_ADD) {
                                    ?>
                                    <div class ="col-md-6"><?php echo get_save_add_button(); ?> </div>
                                <?php } if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                                    <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                                <?php } ?>
                                <div class ="col-md-6"><?php echo get_cancel_button(); ?> </div>
                            <?php } ?>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } else { ?>
    <!-- Header start here -->
    <div class="p-10px">
        <div class="box box-body no-border">
            <form id="frmSearch" method="GET" action="<?php echo get_base_url() . $data_auth[FORM_URL]; ?>">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label class ="control-label"></label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="agent_group_name" value="<?php echo (isset($_GET['agent_group_name']) ? $_GET['agent_group_name'] : '') ?>"/>
                                <span class="input-group-addon" style="padding: 0px;border: none">
                                    <button class="btn btn-danger btn-flat" type="submit">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <?php require_once get_include_page_list_partner_content_header(); ?>
        </div>
    </div>
    <!-- Header Ending here -->
    <!-- Start content here -->
    <div class="p-10px">
        <div class="box box-default">
            <div class="box-body">
                <table id="tbl" class="diplay table table-bordered table-striped" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th column="agent_group_seq">Nomor Urut Grup</th>
                            <th column="agent_group_name">nama Grup</th>
                            <th column="commission_fee_percent">Komisi Agent (%)</th>
                            <th column="created_by"><?php echo TH_CREATED_BY; ?></th>
                            <th column="created_date"><?php echo TH_CREATED_DATE; ?></th>
                            <th column="modified_by"><?php echo TH_MODIFIED_BY; ?></th>
                            <th column="modified_date"><?php echo TH_MODIFIED_DATE; ?></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>  
    <!-- Content Ending here -->
    <?php
}
require_once get_include_page_list_partner_content_footer();
require_once get_include_content_partner_bottom_page_navigation();
?>