<?php
require_once VIEW_BASE_PARTNER;
?>
<header class="header">
    <nav class="navbar navbar-fixed-top navbar-custom-partner">
        <div class="navbar-custom-menu">
            <div class="pull-left">
                <a href="<?php echo get_base_url(); ?>partner/main_page">
                    <img class="logo logo-partner" src="<?php echo get_image_location() ?>assets/img/partner_logo_panel.png" />
                </a>
            </div>
            <!--sisi kanan-->
            <ul class="nav navbar-nav pull-right">
                <li class="dropdown notifications-menu removeMenuActive margin-top-4">                    
                    <ul class="dropdown-menu toko1001-notification" id="clarified_notif">
                        <?php /* <?php if ($n_notification >= 5): ?>
                          <li class="footer"><a href="<?php echo $n_notification ?>">View all</a></li>
                          <?php endif; ?> */ ?>
                    </ul>
                </li>
                <li class="dropdown user user-menu margin-top-4">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?php echo (isset($data_prof[USER_IMAGE]) ? $data_prof[USER_IMAGE] : ''); ?>" 
                          class="user-image" alt="<?php echo (isset($data_prof[USER_NAME]) ? $data_prof[USER_NAME] : ''); ?>">
                        <span class="c-616161"> Halo, <?php echo (isset($data_prof[USER_NAME]) ? $data_prof[USER_NAME] : '' ); ?>
                        </span>&nbsp;&nbsp;<span class="fa fa-caret-down" aria-hidden="true"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?php echo (isset($data_prof[USER_IMAGE]) ? $data_prof[USER_IMAGE] : ''); ?>" 
                                 class="img-circle" alt="<?php echo (isset($data_prof[USER_NAME]) ? $data_prof[USER_NAME] : ''); ?>">
                            <p style="color: #000;">
                        <center><?php echo (isset($data_prof[USER_EMAIL]) ? $data_prof[USER_EMAIL] : ''); ?></center>
                        <small><?php echo (isset($data_prof[LAST_LOGIN]) ? $data_prof[LAST_LOGIN] : ''); ?></small>
                        </p>
                </li>
                <li class="user-body">
                    <div class="row">
                        <div class="col-xs-6 text-center">
                            <a href="<?php echo get_base_url(); ?>partner/profile" class="btn btn-default btn-flat btn-block">Profil</a>
                        </div>
                        <div class="col-xs-6 text-center">
                            <a href="<?php echo get_base_url(); ?>partner/change_picture" class="btn btn-default btn-flat btn-block">Ubah Gambar</a>
                        </div>
                    </div>
                </li>
                <li class="user-footer no-padding">
                    <a href="<?php echo get_base_url(); ?>partner/change_password" class="c-616161 pad-15">Ubah Password</a>
                </li>
                <hr class="no-margin">
                <li class="user-footer no-padding">
                    <a href="<?php echo get_base_url(); ?>partner/logout" class="pad-15">Sign out</a>
                </li>

            </ul>
            </li>
            </ul>
        </div>
    </nav>
</header>

