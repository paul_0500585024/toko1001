<div id="show_error"></div>
<script type="text/javascript" src="<?php echo get_js_url(); ?>json2csv.js"></script>
<script type="text/javascript">
    var button;
    var table;
    var exp = '';
    var sorting = '<?php echo isset($tablesort); ?>';
    var order_column = '<?php echo isset($order_column)?$order_column:'0'; ?>'; 
    var table = '';
    var GET = {};
    if(document.location.toString().indexOf('?') !== -1) {
        var query = document.location
                       .toString()
                       .replace(/^.*?\?/, '')
                       .replace(/#.*$/, '')
                       .split('&');

        for(var i=0, l=query.length; i<l; i++) {
           var aux = decodeURIComponent(query[i]).split('=');
           GET[aux[0]] = aux[1];
        }
    }    
    $(document).ready(function() {
        var url = $('#frmAuth').attr('action');
        var success_html = '';
        var error_html = '';
        var columns_detail = [];

        $('#tbl thead th').each(function() {
            var sName = $(this).attr("column");
            var bVisible = $(this).attr("visible");
            columns_detail.push({"sName": sName, "bVisible": bVisible, "mData": sName});
        });

        table = $('#tbl').DataTable({
            oLanguage: {
                sProcessing: "<img src='<?php echo base_url() . ASSET_IMG_HOME ?>loading.gif' />"
            },
            dom: '<"top"lB>rt<"bottom"ip><"clear">',
            responsive: true,
            processing: true,
            serverSide: true,
            aoColumns: columns_detail,
            buttons: ['csvHtml5', 'excelHtml5', 'pdfHtml5'],
            ajax: {
                url: url,
                type: 'POST',
//                dataType: 'JSON',
                data: buildAjaxData,
                error: handleAjaxError
            }
        })
                .on('xhr.dt', function(e, settings, response, xhr) {
            response = JSON.stringify(response);
            if (isSessionExpired(response)) {
                response_object = json_decode(response);
                url = response_object.url;
                location.href = url;
            }
        });

        function handleAjaxError(request, textStatus, error) {
            if (textStatus === 'timeout') {
                alert('The server took too long to send the data.');
            } else {
                $('#show_error').html(JSON.stringify(request));
                var response = error_response(request.status);
                var session = response.search("Sign in to start your session")
                if (session > 0) {
                    alert("Session habis, silahkan login ulang");
                    window.location.href = "<?php echo get_base_url() ?>admin/login";
                    return false;
                }
                $('#content-message').html(error_html + " " + response + "</div>");
                $('div#tbl_processing').css('visibility', 'hidden');
            }
        }

        function handleAjaxSuccess(response) {
            alert(response);
        }

        function buildAjaxData() {
            var settings = $("#tbl").dataTable().fnSettings();
            var search = [];
            var displaylength = settings._iDisplayLength;
            if (exp != '') {
                displaylength = exp;
            }
            if (sorting != '') {
                sorting = "";
                settings.aaSorting = [[order_column, 'desc']];
            }
            if (order_column != '') {
                column_order = columns_detail[order_column].sName;
            }else{
                column_order = columns_detail[settings.aaSorting[0][0]].sName;
            }
            search = $('#frmSearch').serializeArray();
            search.push(
                    {name: "draw", value: settings.iDraw},
            {name: "start", value: settings._iDisplayStart},
            {name: "length", value: displaylength},
            {name: "column", value: column_order},
            {name: "order", value: settings.aaSorting[0][1]},
            {name: "btnAdditional", value: "true"},
            {name: "export", value: exp}
            );
            exp = '';
            $('.dt-buttons').css("visibility", "hidden");
            return search;
        }

        $("#e_csv").click(function() {
            exp = $("#tbl").dataTable().fnSettings().fnRecordsTotal();
            table.ajax.reload();
            setTimeout(function() {
                table.button(0).trigger()
            }, 1000);
            setTimeout(function() {
                table.ajax.reload();
            }, 1000);

        });
        $("#e_xls").click(function() {
            exp = $("#tbl").dataTable().fnSettings().fnRecordsTotal();
            table.ajax.reload();
            setTimeout(function() {
                table.button(1).trigger()
            }, 1000);
            setTimeout(function() {
                table.ajax.reload();
            }, 1000);

        });
        $("#e_pdf").click(function() {
            exp = $("#tbl").dataTable().fnSettings().fnRecordsTotal();
            table.ajax.reload();
            setTimeout(function() {
                table.button(2).trigger()
            }, 1000);
            setTimeout(function() {
                table.ajax.reload();
            }, 1000);

        });
        $('#tbl tbody').on('click', 'tr', function() {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });

        $("#delete").click(function() {
            var key = $(".selected").attr('id');
            if (key == undefined) {
                alert("Harap pilih data dulu !");
            } else {
                if (confirm("Apakah anda yakin menghapus data yang dipilih ?")) {
                    $.ajax({
                        url: url,
                        type: "POST",
                        dataType: "json",
                        data: {key: key, btnSaveDel: "true"},
                        success: function(response) {
                            if (isSessionExpired(response)) {
                                response_object = json_decode(response);
                                url = response_object.url;
                                location.href = url;
                            } else {
                                if (response.error == true) {
                                    +
                                            $('#content-message').html(error_html + "<div id='msg-shw' class='shw-error'>" + response.message + "</div>")
                                } else {
                                    table.row('.selected').remove().draw(false);
                                    $('#content-message').html(success_html + "<div id='msg-shw' class='shw-success'>" + "Data telah berhasil dihapus ! " + "</div>")
                                }
                                table.ajax.reload();
                            }
                        },
                        error: function(request, error) {
                            $('#content-message').html(error_html + " " + error_response(request.status) + "</div>");
                        }
                    });
                }
            }
        });

        $("#approve").click(function() {
            var key = $(".selected").attr('id');
            if (key == undefined) {
                alert("Harap pilih data dulu !");
            } else {
                if (confirm("Apakah anda yakin menyetujui data yang dipilih ?")) {
                    $.ajax({
                        url: url,
                        type: "POST",
                        dataType: "json",
                        data: {key: key, btnApprove: "true"},
                        success: function(response) {
                            if (isSessionExpired(response)) {
                                response_object = json_decode(response);
                                url = response_object.url;
                                location.href = url;
                            } else {
                                if (response.error == true)
                                {
                                    $('#content-message').html(error_html + response.message + "</div>")
                                } else {
                                    $('#content-message').html(success_html + " Data telah berhasil disetujui ! </div>")
                                }
                                table.ajax.reload();
                            }
                        },
                        error: function(request, error) {
                            alert(error_response(request.status));
                            $('#content-message').html(error_html + "" + error_response(request.status) + "</div>");
                        }
                    });
                }
            }
        });

        $("#reject").click(function() {
            var key = $(".selected").attr('id');
            if (key == undefined) {
                alert("Harap pilih data dulu !");
            } else {
                if (confirm("Apakah anda yakin menolak data yang dipilih ?")) {
                    $.ajax({
                        url: url,
                        type: "POST",
                        dataType: "json",
                        data: {key: key, btnReject: "true"},
                        success: function(response) {
                            if (isSessionExpired(response)) {
                                response_object = json_decode(response);
                                url = response_object.url;
                                location.href = url;
                            } else {
                                if (response.error == true) {
                                    $('#content-message').html(error_html + response.message + "</div>")
                                } else {
                                    $('#content-message').html(success_html + " Data Telah Berhasil Ditolak ! </div>")
                                    table.ajax.reload();
                                }
                                ;
                            }
                        },
                        error: function(request, error) {
                            $('#content-message').html(error_html + " Data Tidak Berhasil Diproses ! </div>")
                            table.ajax.reload();
                        }
                    });
                }
            }
        });

        $("#refresh").click(function() {
            table.ajax.reload();
        });

        $(document).on('click', '#search', function(e) {
            table.ajax.reload();
        });

        var isValid = $('#frmMain').validate({
            ignore: 'input[type=hidden]',
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else if (element.hasClass('select2')) {
                    error.insertAfter(element.next('span'));
                } else {
                    error.insertAfter(element);
                }
            }
        });
        $.validator.messages.required = "Data wajib diisi !";
        $.validator.messages.maxlength = "Harap diisi tidak lebih dari {0} karakter !";
        $.validator.messages.minlength = "Harap diisi dengan minimal {0} karakter !";
        $.validator.messages.email = "Harap diisi dengan alamat email dengan benar !";
        $.validator.messages.date = "Harap diisi dengan tanggal !";
        $.validator.messages.dateiso = "Harap diisi dengan format tanggal yang benar !";
        $.validator.messages.number = "Harap diisi dengan angka !";

        $(document).on('submit', '#frmMain', function(e) {
            if (isValid.valid()) {
                $('input[type="text"]').each(function() {
                    if ($(this).hasClass('auto') || $(this).hasClass('auto_int') || $(this).hasClass('auto_dec'))
                    {
                        var v = $(this).autoNumeric('get');
                        $(this).autoNumeric('destroy');
                        $(this).val(v);
                    }
                });
            }
        });

        $('input[type="text"], input[type="file"], input[type="password"], input[type="radio"],textarea').each(function() {
            var validate = $(this).attr('validate');
            switch (validate) {
                case  "required[]" :
                    $(this).rules('add', {
                        required: true});
                    break;
                case  "email[]" :
                    $(this).rules('add', {
                        required: true,
                        email: true});
                    break;
                case  "password[]" :
                    $(this).rules('add', {
                        required: true,
                        minlength: 8,
                        maxlength: 20});
                    break;
                case  "email-not-required[]" :
                    $(this).rules('add', {
                        email: true});
                    break;
                case  "date[]" :
                    $(this).rules('add', {
                        required: true,
                        date: true});
                    break;
                case  "date-not-required[]" :
                    $(this).rules('add', {
                        date: true
                    });
                    break;
                case  "num[]" :
                    $(this).rules('add', {
                        required: true,
                        number: true
                    });
                    break;
                case  "num-not-required[]" :
                    $(this).rules('add', {
                        number: true
                    });
                    break;
            }
            ;
        });

        $('select').each(function() {
            if ($(this).attr('validate') == "required[]")
            {
                $(this).rules('add', {
                    required: true
                });
            }
        });
        $('.select2').on('change', function() {
            $(this).valid();
        });
        if(GET['order'] === 'ASC'){
            $('table.table-order thead .sorting[column="'+GET['column']+'"], table.table-order thead .sorting_desc[column="'+GET['column']+'"]')
                    .removeClass('sorting')
                    .removeClass('sorting_desc')
                    .addClass('sorting_asc')
                    .attr("order","ASC");
        }
        if(GET['order'] === 'DESC'){
            $('table.table-order thead .sorting[column="'+GET['column']+'"], table.table-order thead .sorting_asc[column="'+GET['column']+'"]')
                    .removeClass('sorting')
                    .removeClass('sorting_asc')
                    .addClass('sorting_desc')
                    .attr("order","DESC");

        }
       $('table.table-order thead .sorting, table.table-order thead .sorting_asc, table.table-order thead .sorting_desc').click(function(){
            if($(this).attr('order') === ''){
                $.extend( GET, {'order':'ASC'} );
            }else if($(this).attr('order') === 'ASC'){
                $.extend( GET, {'order':'DESC'} );
            }else if($(this).attr('order') === 'DESC'){
                $.extend( GET, {'order':'ASC'} );
            }
            if($(this).attr('column') !== ''){
                $.extend( GET, {'column':$(this).attr('column')} );
            }
            window.location = location.protocol + '//' + location.host + location.pathname+'?'+jQuery.param(GET);
       });        
    });

    $('.select2').select2();
    $('.auto').autoNumeric('init', {vMin: 0});
    $('.auto_int').autoNumeric('init', {vMin: 0, mDec: 0});
    $('.auto_dec').autoNumeric('init', {vMin: 0, mDec: 2});

    function validate_form()
    {
        if (button == 'cancel')
        {
            if (confirm("Apakah anda yakin batal ?")) {
                $("#frmMain").validate().cancelSubmit = true;
                return true;
            } else {
                $("#frmMain").validate().cancelSubmit = true;
                button = "";
                return false;
            }
        } else if (button == 'back')
        {
            $("#frmMain").validate().cancelSubmit = true;
            return true;
        } else {
            return true;
        }


    }

    function get_id()
    {
        if (button == 'add')
        {
            return true;
        } else
        {
            var key = $(".selected").attr('id');
            if (key === undefined) {
                alert("Harap pilih data dulu !");
                return false;
            } else {
                $("<input>").attr("type", "hidden").attr('name', "key").attr('value', key).appendTo('#frmAuth');
                return true;
            }
        }
    }

    function generate_password(merchant_id) {
        if (confirm("Apakah anda yakin mereset password ?")) {
            var success_html = '<div class="alert alert-success-admin" style="margin-bottom: 0!important;><h4><i class="fa fa-info"></i></h4><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
            var error_html = '<div class="alert alert-error-admin" style="margin-bottom: 0!important;> <h4><i class="fa fa-info"></i></h4><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
            $(".generate-" + merchant_id).html("<span class='fa fa-spinner fa-spin'></span>");
            $(".generate-" + merchant_id).addClass('disabled');
            $.ajax({
                url: "<?php echo get_base_url() . $data_auth[FORM_URL] ?>",
                type: "JSON",
                type: "POST",
                        data: {merchant_seq: merchant_id, btnAdditional: "act_s_adt", act: "<?php echo ACTION_ADDITIONAL ?>"},
                success: function(data) {
                    $(".generate-" + merchant_id).text("Buat Password baru");
                    $(".generate-" + merchant_id).removeClass('disabled');
                    if (data.error == true)
                    {
                        $('#content-message').html(error_html + data.message + "</div>")
                    } else {
                        $('#content-message').html(success_html + " Data telah berhasil disetujui ! </div>")
                    }
                },
                error: function(xhr) {
                    alert(xhr.responseText);
                }
            })
        }
    }

    function create_report(jsondata) {
        var rpt = new CSVExport(jsondata);
    }

</script>
