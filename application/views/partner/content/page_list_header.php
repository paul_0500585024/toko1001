<form id="frmAuth" onsubmit ="return get_id()" method ="GET" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>" >
   <div class = "box-tools pull-left">
       &nbsp;
	<?php if (!$data_auth[FORM_AUTH][FORM_AUTH_ADD] === false) { ?>
    	<button name ="act" class = "btn btn-flat bg-green" type ="submit" id ="add"  onclick="button = this.id" value ="<?php echo ACTION_ADD?>" data-toggle = "tooltip" title = "Tambah"><i class = "fa fa-plus"> </i></button>
	<?php } ?>
	<?php if (!$data_auth[FORM_AUTH][FORM_AUTH_EDIT] === false) { ?>
		<?php if($data_auth[FORM_AUTH][FORM_CD] != 'PMST00002'){?>
    		<button  name ="act" class = "btn btn-flat bg-blue" type ="submit" id ="edit" onclick="button = this.id"  value ="<?php echo ACTION_EDIT?>" data-toggle = "tooltip" title = "Ubah" ><i class = "fa fa-pencil"></i></button>
    	<?php }?>
	<?php } ?>
	<?php if (!$data_auth[FORM_AUTH][FORM_AUTH_VIEW] === false) { ?>
    	<button  name ="act" class = "btn btn-flat bg-yellow" type ="submit" id ="view" onclick="button = this.id"  value ="<?php echo ACTION_VIEW?>" data-toggle = "tooltip" title = "Lihat" ><i class = "fa fa-eye"></i></button>
	<?php } ?>
	<?php if (!$data_auth[FORM_AUTH][FORM_AUTH_DELETE] === false) { ?>
    	<button class = "btn btn-flat bg-red" id ="delete" data-toggle = "tooltip" title = "Hapus" type='button'><i class = "fa fa-trash-o"></i></button>
	<?php } if (!$data_auth[FORM_AUTH][FORM_AUTH_APPROVE] === false) { ?>
    	<button class = "btn btn-flat bg-blue" id ="approve" data-toggle = "tooltip" title = "Setuju" type='button'><i class = "fa fa-check"></i></button>
    	<button class = "btn btn-flat bg-orange" id ="reject" data-toggle = "tooltip" title = "Tolak" type='button'><i class = "fa fa-thumbs-o-down"></i></button>
	<?php } if (!$data_auth[FORM_AUTH][FORM_AUTH_PRINT] === false) { ?>
    	<div class="btn-group">
    	    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    		<i class = "fa fa-download"></i> <span class="caret"></span>
    	    </button>
    	    <ul class="dropdown-menu">
    		<li><button class = "btn btn-flat btn-default" id="e_csv" style="width:100%" type='button'>CSV</button></li>
    		<li><button class = "btn btn-flat btn-default" id="e_xls" style="width:100%" type='button'>Excel</button></li>
    		<li><button class = "btn btn-flat btn-default" id="e_pdf" style="width:100%" type='button'>PDF</button></li>
    	    </ul>
    	</div>
	<?php } ?>
    </div>
</form>
<br/><br/>

