<?php
require_once VIEW_BASE_PARTNER;
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once get_include_content_partner_header(); ?>
        <meta charset="UTF-8">
        <title><?php echo isset($data_auth[FORM_AUTH][FORM_TITLE]) ? $data_auth[FORM_AUTH][FORM_TITLE] : DEFAULT_SLOGAN ?></title>
    </head>
    <body class="skin-blue sidebar-mini">
        <?php require_once get_include_content_partner_top_navigation(); ?>
        <div class="wrapper">
            <?php require_once get_include_content_partner_left_navigation(); ?>
            <div class="content-wrapper">
                <div id ="content-message">
                    <?php
                    if (isset($data_err) && $data_err[ERROR] === true) {
                        ?>
                        <div id='msg-shw' class='shw-error'>
                            <span><?php echo $data_err[ERROR_MESSAGE][0] ?></span>
                        </div>
                    <?php } elseif (isset($data_suc) && $data_suc[SUCCESS] === true) { ?>
                        <div id='msg-shw' class='shw-success'>
                            </i>&nbsp;&nbsp;<span><?php echo (isset($data_suc[SUCCESS_MESSAGE][0]) ? $data_suc[SUCCESS_MESSAGE][0] : SAVE_DATA_SUCCESS); ?></span>
                        </div>
                        <?php
                    }
                    ?>


                    <?php
                    //for message redeem
                    if (isset($_SESSION['succes_paid']) && $_SESSION['succes_paid'] != '') {
                        ?>
                        <div id='msg-shw' class='shw-success'>
                            </i>&nbsp;&nbsp;<span><?php echo $_SESSION['succes_paid']; ?></span>
                        </div>
<?php } ?>



                    <?php
                    //for message redeem error
                    if (isset($_SESSION['error_paid']) && $_SESSION['error_paid'] != '') {
                        ?>
                        <div id='msg-shw' class='shw-error'>
                            </i>&nbsp;&nbsp;<span><?php echo $_SESSION['error_paid']; ?></span>
                        </div>
<?php } ?>


                </div>
                    <?php
                

