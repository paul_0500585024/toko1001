<select id="drop_category" class="form-control select2" style='width : 100%' name="product_category_seq">
    <option value="">-- Pilih --</option>
    <?php
    if (isset($category_loan)) {
        foreach ($category_loan as $each) {
            ?>
            <option value="<?php echo $each->product_category_seq; ?>" <?php echo (isset($data_sel[LIST_DATA]) && $each->product_category_seq == $data_sel[LIST_DATA][0]->product_category_seq) ? "selected" : "" ?>><?php echo get_display_value($each->category_loan); ?></option>
        <?php
        }
    }
    ?>
</select>


