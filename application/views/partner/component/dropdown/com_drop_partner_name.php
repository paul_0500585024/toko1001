<select id="drop_partner" class="form-control select2" style='width : 100%' name="partner_name">
    <option value="">-- Pilih --</option>
    <?php
    if (isset($partner_name)) {
        foreach ($partner_name as $each) {
            ?>
            <option value="<?php echo $each->seq; ?>" <?php echo (isset($data_sel[LIST_DATA]) && $each->seq == $data_sel[LIST_DATA][0]->partner_seq) ? "selected" : "" ?>><?php echo get_display_value($each->partner_name); ?></option>
        <?php
        }
    }
    ?>
</select>


