<select id="drop_category" class="form-control select2" style='width : 100%' name="product_category_seq">
    <option value="">-- Pilih --</option>
    <?php
    if (isset($category_name)) {
        foreach ($category_name as $each) {
            ?>
            <option value="<?php echo $each->seq; ?>" <?php echo (isset($data_sel[LIST_DATA]) && $each->seq == $data_sel[LIST_DATA][0]->product_category_seq) ? "selected" : "" ?>><?php echo get_display_value($each->category_name); ?></option>
        <?php
        }
    }
    ?>
</select>


