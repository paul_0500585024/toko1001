<?php
require_once VIEW_BASE_PARTNER;
require_once get_include_content_partner_top_page_navigation();
?>
<br>
<div class="col-xs-6">
    <div class="box box-info">
        <div class="box-header">
            <h3 class="box-title">10 Pesanan Terakhir</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body" style="display: block;">
            <div class="table-responsive">
                <table class="table no-margin">
                    <thead>
                        <tr>
                            <th>Order No</th>
                            <th>Agen</th>
                            <th>Barang</th>
                            <th>Status</th>
                            <th>Tanggal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(isset($data_sel[LIST_DATA])){?>
                            <?php foreach ($data_sel[LIST_DATA] as $each) { ?>
                                <tr>
                                    <td><a href="<?php echo base_url().'partner/agent_order?'.CONTROL_GET_TYPE.'='.ACTION_VIEW.'&key='.$each->seq?>"><?php echo $each->order_no ?></a></td>
                                    <td><?php echo $each->agent_name ?></a></td>
                                    <td><?php echo $each->product_name ?></td>
                                    <td><span class="label label-success"><?php echo cstdes($each->order_status, STATUS_ORDER) ?></span></td>
                                    <td><?php echo cdate($each->created_date) ?></td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.box-body -->
    </div>
</div>
<?php
require_once get_include_content_partner_bottom_page_navigation();
?>