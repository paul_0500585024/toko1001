<?php
require_once VIEW_BASE_PARTNER;
require_once get_include_content_partner_top_page_navigation();
echo $breadcrumb;
?>
<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
    <?php require_once get_component_url() . '/dropdown/function_drop_down.php'; ?>
    <div class="row reset-margin">
        <div class="col-xs-12">    
            <div class="box box-no-border-top p-10px no-border-radius m-top5">
                <br>
                <form class ="" id="frmMain" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
                    <div class="box-body">
                        <section class="col-xs-12">
                            <?php echo get_csrf_partner_token(); ?>
                            <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                                <input class="form-control" name="old_status" type="hidden" value="<?php echo(isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->status : "") ?>">
                                <input class="form-control" name="old_created_date" type="hidden" value="<?php echo(isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->created_date : "") ?>">
                                <input class="form-control" name="old_modified_date" type="hidden" value="<?php echo(isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->modified_date : "") ?>">
                            <?php } ?>                            
                            <!--==START SECTION 2==-->
                            <div class="row">
                                <div class="box bg-custom-border no-border-radius">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <h5 class="box-title title-form">INFORMASI</h5>
                                            </div>
                                        </div>
                                        <div class="row">

                                            <div class="col-xs-12">
                                                <div class ="form-group col-xs-6">
                                                    <label>Nama <span class="require">*</span></label>
                                                    <input class="form-control" maxlength="50" validate ="required[]" id="pic1_name" name="pic1_name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->name) : "") ?>" readonly >
                                                </div>
                                                <div class ="form-group col-xs-6">
                                                    <label>Telp </label>
                                                    <input class="form-control" data-inputmask="&quot;mask&quot;: &quot;9999 9999 9999&quot;" data-mask="" validate ="required[]" id="phone_no" name="phone_no" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->phone_no) : "") ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            </div>
                            <br>
                            <br>                            
                            
                            <div class="row">
                                <div class="box bg-custom-border no-border-radius">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <h5 class="box-title title-form">Informasi Bank</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 no-pad">
                                                <div class="form-group col-xs-3 no-margin">
                                                    <label class ="control-label col-xs-12">Nama Bank <span class="require">*</span></label>
                                                    <div class="col-xs-12">
                                                        <input class="form-control" validate ="required[]" name="bank_name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->bank_name : "") ?>"maxlength="25">
                                                    </div>
                                                </div>
                                                <div class="form-group col-xs-3 no-margin">
                                                    <label class ="col-xs-12">Cabang <span class="require">*</span></label>
                                                    <div class="col-xs-12">
                                                        <input class="form-control" validate ="required[]" name="bank_branch_name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->bank_branch_name : "") ?>" maxlength="50">
                                                    </div>
                                                </div>
                                                <div class="form-group col-xs-3 no-margin">
                                                    <label class ="col-xs-12">No.Rekening <span class="require">*</span></label>
                                                    <div class="col-xs-12">
                                                        <input class="form-control" validate ="required[]" name="bank_acct_no" type="text"  value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->bank_acct_no : "") ?>"  maxlength="50">
                                                    </div>
                                                </div>
                                                <div class="form-group col-xs-3 no-margin">
                                                    <label class ="col-xs-12">Rekening A/N <span class="require">*</span></label>
                                                    <div class="col-xs-12">
                                                        <input class="form-control" validate ="required[]" name="bank_acct_name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->bank_acct_name : "") ?>"  maxlength="50">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class ="form-group col-xs-12 no-pad">
                        <div class ="col-xs-3 col-xs-offset-9 no-pad"><?php echo get_save_edit_button(); ?> </div>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
    </div>    
<?php 
}
require_once get_include_page_list_partner_content_footer();
require_once get_include_content_partner_bottom_page_navigation();
?>