<?php
require_once VIEW_BASE_PARTNER;
require_once get_include_content_partner_top_page_navigation();
echo $breadcrumb;
?>

<div class="row reset-margin">
    <div class="col-xs-12">
        <div class="box box-no-border-top p-10px no-border-radius m-top5">
            <div class="box-body">
                <form method="post" action="<?php echo get_base_url() . $data_auth[FORM_URL] ?>" enctype ="multipart/form-data">
                    <?php echo get_csrf_partner_token(); ?>

                    <div class="row">
                        <div class="col-xs-6">
                            <b>Nama Partner</b>
                            <input id="description-active" name="name" class="form-control pic-profile" value="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->name : "" ); ?>" />
                        </div>
                        <div class="col-xs-4">
                            <p class="info-text">
                                <br><br>
                                <b>Nama Partner</b> Mendefinisikan Nama Perusahaan Anda.
                            </p> 
                        </div>
                    </div>
                    <hr>

                    <div class="row">
                        <div class="col-xs-2">
                            <b>Gambar Profil</b>
                            <div class = "slim height-img">
                                <input type = "file" accept = "image/gif,image/jpeg,image/png" name="profil_img" ><br>
                                <img  <?php echo ($data_sel[LIST_DATA][0]->profile_img != ''?'src="' .PARTNER_LOGO.$data_sel[LIST_DATA][0]->seq.'/'.$data_sel[LIST_DATA][0]->profile_img . '"' : '')?> alt="Gambar Profil">
                            </div>
                            <input type = "hidden" value = "<?php echo PARTNER_LOGO.$data_sel[LIST_DATA][0]->seq ?>" name="oldfile" id="oldfile">
                        </div>
                        <div class="col-xs-6">
                            <p class="info-text">
                                <br><br>
                                <b>Photo Profile</b> upload sebuah logo atau gambar untuk toko anda, gambar yang di upload harus dalam bentuk JPEG, JPG, PNG dengan resolusi min 150x150 sampai dengan 200x200 harus berbentuk persegi  
                            </p>                        
                        </div>
                    </div>
                    <hr>
                    <div class="col-xs-2 pull-right">
                        <button type="submit" name="<?php echo CONTROL_SAVE_EDIT_NAME; ?>" class="btn btn-success btn-flat btn-block pull-right">simpan perubahan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>

//>> js to make a navbar fixed

    $(document).on('scroll', function() {
        var posi = $('body').scrollTop();
        if (posi > 50) {
            $('#sub-header').css({'position': 'fixed', 'top': '50px', 'z-index': '999'});
        } else {
            $('#sub-header').css('position', 'static');
        }
    });

    $('#iprev1').click(function() {
        $('#banner_efct').click();
    });
    $('#iprev2').click(function() {
        $('#profil_efct').click();
    });
    $(document).ready(function() {
        $('.upload-hover').mouseenter(function() {
            var type = $(this).attr('img-type');
            $('.' + type + '-upload-area').css('bottom', '0');
        });
        $('.upload-hover').mouseleave(function() {
            var type = $(this).attr('img-type');
            $('.' + type + '-upload-area').css('bottom', '-50px');
        });

        $('.slim').slim({
            ratio: '1:1',
            minSize: {
                width: <?php echo unserialize(IMAGE_DIMENSION_LOGO)["min_width"]?>,
                height: <?php echo unserialize(IMAGE_DIMENSION_LOGO)["min_height"]?>,
            },
            size: {
                width: <?php echo unserialize(IMAGE_DIMENSION_LOGO)["max_width"]?>,
                height: <?php echo unserialize(IMAGE_DIMENSION_LOGO)["max_height"]?>,
            },
            maxFileSize: "2",
            jpegCompression: "60" ,            
            onRemove: function(slim, data) {
                $("#oldfile").val("");
            }

        })

    });
</script>

<?php
require_once get_include_page_list_partner_content_footer();
require_once get_include_content_partner_bottom_page_navigation();
?>
