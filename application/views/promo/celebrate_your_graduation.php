<?php
require_once VIEW_BASE_HOME;
die('a');
?>

<?php
$data = array(
    
    '2' => array(
        'seq' => '1701',
        'pic_name' => 'pegatron_defender.jpg',
        'url' => 'pegatron-defender-jump-start-powerbank-13600-mah-jumper-aki-1701',
    ),
    '3' => array(
        'seq' => '1560',
        'pic_name' => 'mediatech_bluetooth.jpg',
        'url' => 'mediatech-bluetooth-mango-stereo-earset-earphone-hitam-1560',
    ),
    '4' => array(
        'seq' => '1993',
        'pic_name' => 'alfalink_smart.jpg',
        'url' => 'alfalink-smart-car-mount-holder-ach-100-1993',
    ),
    '5' => array(
        'seq' => '2056',
        'pic_name' => 'canon_eosm10.jpg',
        'url' => 'canon-eos-m10-white-with-ef-m15-45mm-gratis-boneka-rillakkuma-edisi-spesial-2056',
    ),
    
    '6' => array(
        'seq' => '1566',
        'pic_name' => 'pritech_professional.jpg',
        'url' => 'pritech-professional-hair-dryer-rc1703-pink-1566',
    ),
    '7' => array(
        'seq' => '1850',
        'pic_name' => 'bima_rak_sepatu.jpg',
        'url' => 'bima-rak-sepatu-3-susun-aluminium-1850',
    ),
   
    '8' => array(
        'seq' => '1747',
        'pic_name' => 'gyesok_useoyo.jpg',
        'url' => 'gyesok-useoyo-sarange-lovely-peach-gloss-2-1747',
    ),
    '9' => array(
        'seq' => '1748',
        'pic_name' => 'nolrawoyo.jpg',
        'url' => 'nolrawoyo-sarange-graceful-glossy-pink-3-1748',
    ),
    '10' => array(
        'seq' => '1749',
        'pic_name' => 'tok_tokhae.jpg',
        'url' => 'tok-tokhae-sarange-pinky-gloss-4-1749',
    ),
    '11' => array(
        'seq' => '1750',
        'pic_name' => 'banjjak.jpg',
        'url' => 'banjjak-sarange-cutie-orange-gloss-5-1750',
    ),
    '12' => array(
        'seq' => '1751',
        'pic_name' => 'meositsseoyo.jpg',
        'url' => 'meositsseoyo-sarange-jubilee-dare-red-gloss-6-1751',
    ),
    '13' => array(
        'seq' => '1752',
        'pic_name' => 'halsuituh.jpg',
        'url' => 'halsuituh-sarange-peach-lips-1-1752',
    ),
    '4' => array(
        'seq' => '1753',
        'pic_name' => 'bogosip_eoyo.jpg',
        'url' => 'bogosip-eoyo-sarange-cherry-lips-3-1753',
    ),
    '12' => array(
        'seq' => '1754',
        'pic_name' => 'nikkeoya.jpg',
        'url' => 'nikkeoya-sarange-violet-lips-4-1754',
    ),
    '7' => array(
        'seq' => '1755',
        'pic_name' => 'dalcomhae.jpg',
        'url' => 'dalcomhae-sarange-coral-lips-2-1755',
    ),
    '6' => array(
        'seq' => '1756',
        'pic_name' => 'choigoya.jpg',
        'url' => 'choigoya-sarange-real-red-lips-6-1756',
    ),
  
    '16' => array(
        'seq' => '1572',
        'pic_name' => 'pritech_electric.jpg',
        'url' => 'pritech-electric-shaver-dual-cutter-styes-rsm1310-merah-1572',
    ),
    '17' => array(
        'seq' => '1196',
        'pic_name' => 'romoss_car.jpg',
        'url' => 'romoss-car-charger-ranger-17-putih-1196',
    ),
    
    
    '20' => array(
        'seq' => '1898',
        'pic_name' => 'zola_4_output.jpg',
        'url' => 'zola-4-output-charger-zthunder-4-1898',
    ),
    '21' => array(
        'seq' => '1894',
        'pic_name' => 'zola_corona.jpg',
        'url' => 'zola-corona-powerbank-10000mah-1894',
    ),
        )
?>


<?php if (strtotime('now') < strtotime("2016-10-03")) { ?>
    <div class="hidden-xs hidden-lg " style="margin-top:40px;">&nbsp;</div>
    <div class="hidden-xs hidden-sm hidden-md" style="margin-top:20px;">&nbsp;</div>
    <div class="container">
        <img class="img-responsive" src="<?php echo get_image_location() . PROMO_IMAGE ?>promo_iese_2016_super_great_sale.jpg" alt="Promo IESE 2016 SUPER GREAT SALE">                 
    </div>

<?php } else { ?>
    <style>
        .no-margin{
            margin:0px;
            padding:0px !important;            
        }

        .bit-margin-top{
            margin-top:2em;
        }

        .relative{
            position:relative;
            overflow:hidden;
        }
        .relative img{
            width:100%;
            transition:all .512s ease-in-out;
        }

        .relative img:hover{
            transform:scale(1.1, 1.1);
        }

    </style>

    <div class="container">
        <div class="col-xs-12 no-pad no-margin mb-10px">
            <div class="row no-pad no-margin">
                <div class="col-xs-12 no-pad no-margin">
                    <img class="img-responsive" src="<?php echo get_image_location() . CAMPAIGN_IESE_SUPER_GREAT_SALE_IMAGE ?>iese_banner_celebrate.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row reset-margin">
            <?php foreach ($data as $each_data): ?>
                <div class="mt-0px" >
                    <div class="col-xs-15 no-pad">
                        <div data-img ="1" class="productDetails panel height_fix_product prodDiv">
                            <!--<div class="discount clearfix">-->
                                <!--<span class="disc-box">0</span>-->
                            <!--</div>-->
                            <div class="panel-body p-03em no-border-radius">
                                <a href="<?php echo base_url() . $each_data['url'] ?>">            
                                    <div class="">
                                        <div class="box-img mb-10px load_img">
                                            <img id="img-terbaru-<?php echo $each_data['seq'] ?>" class="b-lazy full-display-img mr-0px"
                                                 data-src="<?php echo get_image_location() . CAMPAIGN_IESE_SUPER_GREAT_SALE_IMAGE . $each_data['pic_name'] ?>"
                                                 src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" />
                                        </div>
                                        <!--<div class="title-produk-box line-clamp">-->
                                            <!--<p class="title-produk"><center class="c-black">Produk</center>-->
                                        <!--</div>-->
                                        <!--<p class="no-old-price">&nbsp;</p>-->
                                        <!-- <p class="old-price">123</p> -->
                                        <!--<p class="final-price">94500</p>-->
                                    </div>
                                </a>

                                <!-- <div class="bCredit"> -->
                                <!-- <div class="credit-promo-sprite"></div> -->
                                <!-- <center>12 bulan<br></center> -->
                                <!-- <div class="creditName" style="text-align:center!important">Promo</div> -->
                                <!-- </div> -->
                                <div class="bCredit vhide">
                                    <img src="<?php echo get_base_url() . 'assets/img/cicilan_2.png'; ?>" />
                                    <br>
                                    <div class="creditName">Promo</div>';
                                </div>

                                <div class="row">

                                    <div id="title-1" class="col-xs-12">
                                            <!-- <button data-toggle="tooltip" title="stok tidak tersedia" class="btn btn-block btn-flat btn-disable" disabled><i class="fa fa-shopping-cart"></i>&nbsp;Tambah Ke Keranjang</button>' -->
                                        <?php if (!isset($_SESSION[SESSION_MEMBER_CSRF_TOKEN])): ?>
                                            <button onClick ="login_register()" class="btn btn-block btn-flat btn-cart"><i class="fa fa-shopping-cart"></i>Tambah Ke Keranjang</button>
                                        <?php else: ?>
                                                <!-- <button data-toggle="tooltip" title="stok tidak tersedia" class="btn btn-block btn-flat btn-disable" disabled><i class="fa fa-shopping-cart"></i>&nbsp;Tambah Ke Keranjang</button>' -->
                                            <button onClick ="add_product(<?php echo $each_data['seq'] ?>)" class="btn btn-flat btn-block btn-cart"><i class="fa fa-shopping-cart"></i>&nbsp;Tambah Ke Keranjang</button>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

    <script>
                                                $(document).ready(function() {
                                                    if (navigator.userAgent.indexOf("Firefox") != -1) {
                                                        var heights = $(".col-lg-5").map(function() {
                                                            return $(this).height();
                                                        }).get(),
                                                                maxHeight = Math.max.apply(null, heights);
                                                        $(".eq-height").height(Math.round(maxHeight));
                                                    }
                                                });
                                                $('.productDetails').mouseenter(function() {
                                                    var title = 'title-' + $(this).attr('data-img');
                                                    $('#' + title).removeClass('no-show');
                                                });
                                                $('.productDetails').mouseleave(function() {
                                                    var title = 'title-' + $(this).attr('data-img');
                                                });
    </script>
<?php } ?>
<?php $this->load->view(LIVEVIEW . 'home/template/segment_main/js_add_product.php'); ?>

<!--END TAMBAH BARU-->

