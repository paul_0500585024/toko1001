<?php
require_once VIEW_BASE_HOME;
?>


<?php if (strtotime('now') < strtotime("2016-04-27")) { ?>
    <div class="hidden-xs hidden-lg " style="margin-top:40px;">&nbsp;</div>
    <div class="hidden-xs hidden-sm hidden-md" style="margin-top:20px;">&nbsp;</div>
    <div class="container">
        <img class="img-responsive" src="<?php echo get_image_location() . PROMO_IMAGE ?>promo_iese_2016_super_great_sale.jpg" alt="Promo IESE 2016 SUPER GREAT SALE">                 
    </div>

<?php } else { ?>
    <style>
        .no-margin{
            margin:0px;
            padding:0px !important;            
        }

        .bit-margin-top{
            margin-top:2em;
        }

        .relative{
            position:relative;
            overflow:hidden;
        }
        .relative img{
            width:100%;
            transition:all .512s ease-in-out;
        }

        .relative img:hover{
                transform:scale(1.1, 1.1);
            }

    </style>
    <div class="hidden-xs hidden-lg " style="margin-top:70px;">&nbsp;</div>
    <div class="hidden-xs hidden-sm hidden-md" style="margin-top:20px;">&nbsp;</div>
    <div class="container">
        <div class="row well" style="margin:0px;padding: .5em;border-radius:0px;background:transparent;">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 no-margin" style="position:relative; overflow: hidden;">
                <img class="img-responsive" src="<?php echo get_image_location() . PROMO_IMAGE ?>iese_banner1.png" alt="">
            </div>
        </div>
    </div>
    <div class="container bit-margin-top" >
        <div class="row well" style="margin:0px;padding: .5em;border-radius:0px;background:transparent;">
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 no-margin relative">
                <a href="https://toko1001.id/robot-power-bank-6600-mah-rt6800-1131"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-1_01.jpg" alt=""></a>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-6 no-margin relative">
                <a href="https://toko1001.id/zte-blade-s6-silver-604"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-1_02.jpg" alt=""></a>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-6 no-margin relative">
                <a href="https://toko1001.id/zte-nubia-z5s-mini-403a-16gb-lte-black-1151"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-1_03.jpg" alt=""></a>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-6 no-margin relative">
                <a href="https://toko1001.id/mimo-earphone-mi-46-1418"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-1_04.jpg" alt=""></a>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-6 no-margin relative">
                <a href="https://toko1001.id/zte-blade-l2-4gb-black-1428"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-1_05.jpg" alt=""></a>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 no-margin relative">
                <a href="https://toko1001.id/zte-blade-a5-8gb-lte-black-1426 "><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-1_06.jpg" alt=""></a>
            </div>

        </div>
    </div>

    <div class="container bit-margin-top">
        <div class="row well" style="margin:0px;padding: .5em;border-radius:0px;background:transparent;">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-margin relative">
                <a href="https://toko1001.id/power-bank-evercoss-p88-8800-mah-silver-518"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-2_01.jpg" alt=""></a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 no-margin relative">
                <a href="https://toko1001.id/evercoss-elevate-y3-582"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-2_02.jpg" alt=""></a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 no-margin relative">
                <a href="https://toko1001.id/power-bank-evercoss-p88-8800-mah-silver-518"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-2_03.jpg" alt=""></a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 no-margin relative">
                <a href="https://toko1001.id/yunteng-tongsis-monopod-cable-yt-1188-1242"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-2_04.jpg" alt=""></a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 no-margin relative">
                <a href="https://toko1001.id/evercoss-winner-t3-583"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-2_05.jpg" alt=""></a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-margin relative">
                <a href="https://toko1001.id/evercoss-android-one-x-a65-black-270"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-2_06.jpg" alt=""></a>
            </div>
        </div>
    </div>

    <div class="container bit-margin-top">
        <div class="row well" style="margin:0px;padding: .5em;border-radius:0px;background:transparent;">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-margin relative">
                <a href="https://toko1001.id/sphero-star-wars-bb-8-app-enabled-droid-robot-1408"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-3_01.jpg" alt=""></a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 no-margin relative">
                <a href="https://toko1001.id/mediatech-big-camera-bag-mcb-01-1345"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-3_02.jpg" alt=""></a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 no-margin relative">
                <a href="https://toko1001.id/mediatech-mouse-pad-gaming-gp-01-epicenter-1257"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-3_03.jpg" alt=""></a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 no-margin relative">
                <a href="https://toko1001.id/mediatech-mnb-09-laptop-bag-pink-hitam-1260"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-3_04.jpg" alt=""></a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 no-margin relative">
                <a href="https://toko1001.id/nohoo-bag-bee-green-nh001-704"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-3_05.jpg" alt=""></a>
            </div>
        </div>
    </div>

    <div class="container bit-margin-top">
        <div class="row well" style="margin:0px;padding: .5em;border-radius:0px;background:transparent;">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 no-margin relative">
                <a href="https://toko1001.id/surya-sft-l2410-kipas-dan-lampu-led-emergency-1421"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-4_01.jpg" alt=""></a>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 no-margin relative">
                <a href="https://toko1001.id/aoki-fitting-lampu-emergency-double-aki-kering-ak-320-1422"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-4_02.jpg" alt=""></a>
            </div>
            <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12 no-margin relative">
                <a href="https://toko1001.id/ecovacs-vacuum-robot-deebot-d45-1379"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-4_03.jpg" alt=""></a>
            </div>
            <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12 no-margin relative">
                <a href="https://toko1001.id/rinnai-kompor-gas-satu-tungku-ri-511-a-stainless-steel-1227"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-4_04.jpg" alt=""></a>
            </div>
        </div>
    </div>

    <div class="container bit-margin-top">
        <div class="row well" style="margin:0px;padding: .5em;border-radius:0px;background:transparent;">
            <div class="col-lg-6 no-margin">
                <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12 no-margin relative">
                    <a href="https://toko1001.id/modern-gergaji-mesin-triplek-m-2200b-1424"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-5_01.jpg" alt=""></a>
                </div>
                <div class="col-lg-12 col-md-6 col-sm-6 col-xs-12 no-margin relative">
                    <a href="https://toko1001.id/aoki-perangkap-nyamuk-elektrik-ak-8829-1423"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-5_03.jpg" alt=""></a>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 no-margin relative">
                <a href="https://toko1001.id/modern-bor-set-m-2130-1425"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-5_02.jpg" alt=""></a>
            </div>
        </div>
    </div>
    <div class="container bit-margin-top">
        <div class="row well" style="margin:0px;padding: .5em;border-radius:0px;background:transparent;">
            <div class="col-lg-3 no-margin relative">
                <a href="https://toko1001.id/nano-spray-handy-mist-1275"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-6_01.jpg" alt=""></a>
            </div>
            <div class="col-lg-4 no-margin relative eq-height">
                <a href="https://toko1001.id/pritech-professional-roll-for-pedicure-pink-scholl-1255"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-6_02.jpg" alt=""></a>
            </div>
            <div class="col-lg-5 no-margin relative eq-height">
                <a href="https://toko1001.id/pritech-professional-hairdressing-tools-tb805-pink-1281"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-6_03.jpg" alt=""></a>
            </div>
            <div class="col-lg-5 no-margin relative eq-height">
                <a href="https://toko1001.id/infrared-thermometer-8-in-1-for-adult-dewasa-baby-anak-1277"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-6_04.jpg" alt=""></a>
            </div>
            <div class="col-lg-4 no-margin relative eq-height">
                <a href="https://toko1001.id/universal-beauty-care-massager-5-in-1-1276"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-6_05.jpg" alt="" ></a> 
            </div>

        </div>
    </div>

    <div class="container bit-margin-top">
        <div class="row well" style="margin:0px; padding:.5em;border-radius:0px;background:transparent;">
            <div class="col-lg-4 col-md-12 col-xs-12 no-margin relative">
                <a href="https://toko1001.id/briliant-vensio-drink-set-motif-irisiana-gm00101-997"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-7_01_01.jpg" alt=""></a>
            </div>
            <div class="col-lg-4 col-md-6 col-xs-12 no-margin relative">
                <a href="#"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-7_01_02.jpg" alt=""></a>

            </div>
            <div class="col-lg-4 col-md-6 col-xs-12 no-margin relative">
                <a href="#"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-7_01_03.jpg" alt=""></a>
            </div>
        </div>

    </div>


    <div class="container bit-margin-top">
        <div class="row well" style="margin:0px; padding:.5em;border-radius:0px;background:transparent;">
            <div class="col-lg-4 col-md-12 col-xs-12 no-margin relative">
                <a href="https://toko1001.id/mediatech-keyboard-gaming-k-010-1246"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-8_01.jpg" alt=""></a>
            </div>
            <div class="col-lg-4 col-md-6 col-xs-12 no-margin relative">
                <a href="https://toko1001.id/mediatech-mouse-gaming-krobelus-z1-1245"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-8_02.jpg" alt=""></a>
            </div>
            <div class="col-lg-4 col-md-6 col-xs-12 no-margin relative">
                <a href="https://toko1001.id/xuncow-universal-travel-bag-organizer-1419"><img src="<?php echo get_image_location() . PROMO_IMAGE ?>layout-untuk-promo-web--EISE-01-8_03.jpg" alt=""></a>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            if (navigator.userAgent.indexOf("Firefox") != -1) {
                var heights = $(".col-lg-5").map(function() {
                    return $(this).height();
                }).get(),
                        maxHeight = Math.max.apply(null, heights);
                $(".eq-height").height(Math.round(maxHeight));
            }
        });
    </script>
<?php } ?>


<!--END TAMBAH BARU-->

