<?php
require_once VIEW_BASE_HOME;
parse_str($this->input->server('QUERY_STRING'), $query_string);
$get_attribute_name_display = array();
$filter_message = array();
?>
<div class="hidden-xs hidden-lg " style="margin-top:60px;">&nbsp;</div>
<!--    for lg md-->
<div class="hidden-xs hidden-sm hidden-md" style="margin-top:25px;">&nbsp;</div>
<?php if ($product_category_seq != '0' AND $level == 1): ?>
    <div class="container">
        <div class="item active">
            <?php if (isset($category_img[0])): ?>
                <?php if (is_get_file_exists(ADV_UPLOAD_IMAGE . $category_img[0]->banner_image)): ?>
                    <a href="<?php echo $category_img[0]->banner_image_url; ?>">
                        <img data-src="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $category_img[0]->banner_image) ?>" 
                             data-src-retina="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $category_img[0]->banner_image) ?>" 
                             src="<?php echo base_url() . ASSET_IMG_HOME . 'blank.png' ?>" alt="no image"
                             class="img-responsive center-block full-img2 load_img">
                    </a>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>

<div class="container">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!--                                 Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Filter</a>
            </div>
            <!--                                 Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav  navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Harga <span class="caret"></span></a>

                        <div class="dropdown-menu" >
                            <div class="price-slider">
                                <form class="form" id="formLogin" method="get" action="<?php echo current_url_with_query_string(array(PRICE_RANGE,START_OFFSET)) ?>">
                                    <input type="text" value="" class="slider form-control"
                                           id="sliderprice" name="" data-slider-min="0"
                                           data-slider-max="10000000" data-slider-step="1000"
                                           data-slider-value="[0,10000000]"
                                           data-slider-orientation="horizontal" style="display: block;"
                                           data-slider-selection="before" data-slider-tooltip="show"
                                           data-slider-id="aqua">
                                    <div class="col-lg-12 col-xs-12  col-md-12 col-sm-12">
                                        <div class="input-group">
                                            <input type="text" id="pricerange" name="pr" value="0,10000000" class="form-control">                                    
                                            <span class="input-group-btn">
                                                <button type="submit" id="btnLogin" class="btn btn-front btn-flat" ><i class="fa fa-search"></i></button>                                    

                                            </span>
                                        </div>
                                    </div>
<?php /*                                    <?php foreach ($query_string as $name => $each_query_string): ?>
                                        <?php if ($name != PRICE_RANGE): ?>
                                            <input type="hidden" name="<?php echo $name ?>" value="<?php echo $each_query_string ?>" />
                                        <?php endif; ?>
                                    <?php endforeach; ?>                                    */?>
                                </form>
                                <br>
                            </div>
                        </div>

                    </li>
                    <?php if ($product_category_seq != '0'): ?>
                        <?php foreach ($filter['name'] as $each_filter_name): ?>
                            <?php $attribute_name_display = display_filter_name($each_filter_name->attribute_name, $each_filter_name->attribute_display_name); ?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $attribute_name_display ?> <span class="caret"></span></a>
                                <?php if (count($filter['detail'][$each_filter_name->attribute_seq]) > 0): ?>
                                    <ul class="dropdown-menu textleftright">
                                        <?php $counter = 0; ?>
                                        <?php /*for label semua*/?>
                                        <?php if(count($filter['detail'][$each_filter_name->attribute_seq]) > 0):?>
                                            <?php
                                            $attribute_category_attribute_seq = $each_filter_name->attribute_seq;
                                            $selected_filter = '';
                                            $attribute_value_seq = '0';
                                            $attribute_value_value = 'Semua';
                                            if (get_selected_category($attribute_category_attribute_seq, $attribute_value_seq, $this->input->get(PARAMETER_CATEGORY_ATTRIBUTE, TRUE))) {
                                                //                                        $selected_filter = "class='active'";
                                                $selected_filter = true;
                                                //to get filter value; 0 is for semua
                                                if ($attribute_value_seq != 0) {
                                                    $get_attribute_name_display[$attribute_name_display][] = $attribute_value_value;
                                                }
                                            };
                                            ?>
                                            <li>
                                                <a href="<?php echo add_current_url_with_query_string_special($attribute_category_attribute_seq . '-' . $attribute_value_seq) ?>"><?php echo $attribute_value_value; ?> 
                                                    <?php if ($selected_filter): ?>
                                                        <span class="glyphicon glyphicon-ok check-mark kbd"></span>    
                                                    <?php endif; ?>
                                                </a>
                                            </li>
                                            <?php $counter++; ?>
                                        <?php endif;?>
                                        <?php /*end of label semua*/?>
                                        <?php foreach ($filter['detail'][$each_filter_name->attribute_seq] as $each_filter_detail): ?>
                                            <?php
                                            $selected_filter = '';
                                            $attribute_category_attribute_seq = $each_filter_detail->attribute_category_attribute_seq;
                                            if ($counter == 0) {
                                                $attribute_value_seq = '0';
                                                $attribute_value_value = 'Semua';
                                            } else {
                                                $attribute_value_seq = $each_filter_detail->attribute_value_seq;
                                                $attribute_value_value = $each_filter_detail->attribute_value_value;
                                            }
                                            if (get_selected_category($attribute_category_attribute_seq, $attribute_value_seq, $this->input->get(PARAMETER_CATEGORY_ATTRIBUTE, TRUE))) {
                                                //                                        $selected_filter = "class='active'";
                                                $selected_filter = true;
                                                //to get filter value; 0 is for semua
                                                if ($attribute_value_seq != 0) {
                                                    $get_attribute_name_display[$attribute_name_display][] = $attribute_value_value;
                                                }
                                            };
                                            ?>
                                            <li>
                                                <a href="<?php echo add_current_url_with_query_string_special($attribute_category_attribute_seq . '-' . $attribute_value_seq) ?>"><?php echo $attribute_value_value; ?> 
                                                    <?php if ($selected_filter): ?>
                                                        <span class="glyphicon glyphicon-ok check-mark kbd"></span>    
                                                    <?php endif; ?>
                                                </a>
                                            </li>
                                            <?php $counter++; ?>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                            </li>         
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Urutkan <span class="caret"></span></a>
                        <ul class="dropdown-menu textleftright">
                            <li>
                                <a href="<?php echo current_url_with_query_string(array(ORDER_CATEGORY,START_OFFSET), array(ORDER_CATEGORY => NEW_TO_OLD_PRODUCT)) ?>">Produk terbaru ke terlama
                                    <?php if ($order_category == NEW_TO_OLD_PRODUCT): ?>
                                        <span class="glyphicon glyphicon-ok check-mark kbd"></span>
                                    <?php endif; ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo current_url_with_query_string(array(ORDER_CATEGORY,START_OFFSET), array(ORDER_CATEGORY => OLD_TO_NEW_PRODUCT)) ?>">Produk terlama ke terbaru
                                    <?php if ($order_category == OLD_TO_NEW_PRODUCT): ?>
                                        <span class="glyphicon glyphicon-ok check-mark kbd"></span>
                                    <?php endif; ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo current_url_with_query_string(array(ORDER_CATEGORY,START_OFFSET), array(ORDER_CATEGORY => PRICE_EXPENSIVE_TO_CHEAP)) ?>">Harga mahal ke murah
                                    <?php if ($order_category == PRICE_EXPENSIVE_TO_CHEAP): ?>
                                        <span class="glyphicon glyphicon-ok check-mark kbd"></span>
                                    <?php endif; ?>                                    
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo current_url_with_query_string(array(ORDER_CATEGORY,START_OFFSET), array(ORDER_CATEGORY => PRICE_CHEAP_TO_EXPENSIVE)) ?>">Harga murah ke mahal
                                    <?php if ($order_category == PRICE_CHEAP_TO_EXPENSIVE): ?>
                                        <span class="glyphicon glyphicon-ok check-mark kbd"></span>
                                    <?php endif; ?>                                    
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <?php if ($this->input->get(PRICE_RANGE, TRUE) != false): ?>
        <span>
            <?php
            $price_range = $this->input->get(PRICE_RANGE, TRUE);
            $price_range_exploder = explode(',', $price_range);
            $start_price = isset($price_range_exploder[0]) ? $price_range_exploder[0] : '';
            $to_price = isset($price_range_exploder[1]) ? $price_range_exploder[1] : '';
            $filter_message[] = "Harga : " . RP . cnum($start_price) . " - " . RP . cnum($to_price);
            ?>
        </span>
    <?php endif; ?>

    <?php if ($level != ''): ?>

        <?php
        $number_of_category = count($category_seq_self_parent_with_name);
        $counter_category = 0;
        ?>
        <?php foreach ($category_seq_self_parent_with_name as $product_category_seq => $each_seq_self_parent_category): ?>
            <strong><a href="<?php echo base_url() . url_title(strtolower($each_seq_self_parent_category->name)) . "-" . CATEGORY . ($each_seq_self_parent_category->seq) ?>"><?php echo $each_seq_self_parent_category->name; ?></a></strong>
            <?php if ($counter_category < $number_of_category - 1): ?>
                &nbsp;<i class="fa fa-caret-right"></i>&nbsp;
            <?php endif; ?>
            <?php $counter_category++; ?>
        <?php endforeach; ?>

    <?php endif; ?>
    <br>

    <?php
    $counter_filter = 0;
    $end_filter_index = count($get_attribute_name_display);
    ?>
    <?php foreach ($get_attribute_name_display as $attribute_name_display => $each_get_attribute_name_display): ?>
        <?php foreach ($each_get_attribute_name_display as $key => $attribute_value_value): ?>
            <?php $filter_message[] = $attribute_name_display . " : " . $attribute_value_value ?>
            <?php $counter_filter++; ?>
        <?php endforeach; ?>
    <?php endforeach; ?>

    <?php if ($this->input->get(ORDER_CATEGORY, TRUE) != false): ?>
        <?php $filter_message[] = "Urutkan : " . $ordering_category; ?>
    <?php endif; ?>        



    <?php
    $counter = 0;
    $end_index_filter_message = count($filter_message);
    ?>
    <?php foreach ($filter_message as $each_filter_message): ?>
        <span><strong><?php echo $each_filter_message; ?></strong></span>
        <?php if ($counter < $end_index_filter_message - 1): ?>
            &nbsp;&#8226;&nbsp;
        <?php endif; ?>
        <?php $counter++; ?>
    <?php endforeach; ?>
</div>
<br>

<?php
if (count($product) > 0) {
    echo display_product($product);
} elseif ($this->input->get(SEARCH, TRUE) != FALSE) {
    ?>  
    <div class="container">
        <p>Kami tidak menemukan hasil pencarian untuk  &quot;<b><?php echo htmlspecialchars($this->input->get(SEARCH, TRUE)); ?></b>&quot;</p>
        <p>Tolong periksa pengejaan kata, gunakan kata-kata yang lebih umum &amp; coba lagi!</p>
    </div>
<?php
} elseif (count($product) == 0) {
    echo "<div style='margin-bottom:72px;' class='container'>Tidak ada produk dengan kategori ini.</div>";
}
?>
<div class="container"><?php echo $this->pagination->create_links(); ?></div>



