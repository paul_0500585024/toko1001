<?php
require_once VIEW_BASE_HOME;
?>
<div class="container">
    <div class="row" style="margin-top:80px;">
        <div class="container">
            <ul class='nav nav-wizard hidden-xs'>
                <li id="li-info" <?php echo $data_step == TASK_FIRST_STEP ? "class=\"active\"" : "" ?> ><a href="<?php echo get_base_url() . "member/checkout?step=1" ?>" >1. Informasi Penerima</a></li>
                <li id="li-metode" <?php echo $data_step == TASK_SECOND_STEP ? "class=\"active\"" : "" ?> ><a href="<?php echo get_base_url() . "member/checkout?step=2" ?>" >2. Metode Pembayaran</a></li>
                <li id="li-detil" <?php echo $data_step == TASK_FINAL_STEP ? "class=\"active\"" : "" ?> ><a href="<?php echo get_base_url() . "member/checkout?step=3" ?>" >3. Ringkasan  Belanja</a></li>
            </ul>
            <ul class="nav nav-tabs hidden-lg hidden-sm hidden-md" >                
                <div class="direct-chat-msg">                      
                    <a <?php echo $data_step == TASK_FIRST_STEP ? "class=\"active btn btn-cart direct-chat-img\"" : "class=\"btn btn-cart direct-chat-img\"" ?> href="<?php echo get_base_url() . "member/checkout?step=1" ?>"><i class="fa fa-user"></i></a>                      
                    <div <?php echo $data_step == TASK_FIRST_STEP ? "class=\"active step-info\"" : "class=\"step-info\"" ?>> 1. Informasi Member </div><!-- /.step-info -->
                </div>
                <div class="direct-chat-msg">                      
                    <a <?php echo $data_step == TASK_SECOND_STEP ? "class=\"active btn btn-cart direct-chat-img\"" : "class=\"btn btn-cart direct-chat-img\"" ?> href="<?php echo get_base_url() . "member/checkout?step=2" ?>"><i class="fa fa-credit-card"></i></a>                      
                    <div <?php echo $data_step == TASK_SECOND_STEP ? "class=\"active step-info\"" : "class=\"step-info\"" ?>> 2. Metode Pembayaran </div><!-- /.step-info -->
                </div>
                <div class="direct-chat-msg">                      
                    <a <?php echo $data_step == TASK_FINAL_STEP ? "class=\"active btn btn-cart direct-chat-img\"" : "class=\"btn btn-cart direct-chat-img\"" ?> href="<?php echo get_base_url() . "member/checkout?step=3" ?>"><i class="fa fa-list-alt"></i></a>                      
                    <div <?php echo $data_step == TASK_FINAL_STEP ? "class=\"active step-info\"" : "class=\"step-info\"" ?>> 3. Ringkasan Belanja </div><!-- /.step-info -->
                </div>
            </ul>
            <br>
            <?php if (isset($data_err) && $data_err[ERROR] === true) { ?>
                <div class="alert alert-error-home">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $data_err[ERROR_MESSAGE][0] ?>
                </div>
            <?php } ?> 
            <div id="myTabContent" class="tab-content">
                <?php if ($data_step == TASK_FIRST_STEP) { ?>
                    <!--                    INFORMASI PENERIMA-->
                    <div class="tab-pane fade  active in" id="informasi-penerima">
                        <br>
                        <form id="frmMain" method="post" url="<?php echo get_base_url() . $data_auth[FORM_URL] . "?step=2" ?>" onSubmit="next_step()">
                            <input type="hidden" name ="type" value="<?php echo TASK_FIRST_STEP ?>">
                            <input id="province_name" type="hidden" name ="province_name">
                            <input type="hidden" name ="step" value="<?php echo TASK_SECOND_STEP ?>">
                            <input id="city_name" type="hidden" name ="city_name" >
                            <input id="district_name" type="hidden" name ="district_name">
                            <div class="panel panel-primary">
                                <div class="panel-heading big_title">Informasi Penerima</div>
                                <div class='panel-body'>
                                    <div class='col-md-4'>
                                        <div class='form-group'>
                                            <label>Jenis Alamat Penerima</label>
                                            <?php require_once get_component_url() . "dropdown/com_drop_member_address.php"; ?>
                                        </div>
                                        <div class='row'>
                                            <div class="col-lg-12">
                                                <div class='form-group'>
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <input type="checkbox" aria-label="..." name="chk_alias" <?php echo (!isset($data_sel[LIST_DATA][0]->chk_alias) ? "" : (($data_sel[LIST_DATA][0]->chk_alias == "1" OR $data_sel[LIST_DATA][0]->chk_alias == "on") ? "checked" : "")); ?> > 
                                                        </span>
                                                        <input type="text" class="form-control" placeholder="Simpan dengan alias" name="alias" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->alias) : "" ); ?>" maxlength="25">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class='form-group'>
                                            <label>Nama Penerima*</label>
                                            <input id="receiver_name"  validate="required[]" type='text' name="receiver_name" class='form-control' value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->receiver_name) : "" ); ?>" maxlength="25">
                                        </div>
                                        <div class='form-group'>
                                            <label>No. Telp*</label>
                                            <input id="phone_no" validate ="required[]" name ="phone_no" type='text' class='form-control' value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->phone_no) : "" ); ?>" data-inputmask="&quot;mask&quot;: &quot;9999 9999 9999 9999&quot;" data-mask="" maxlength="18">
                                        </div>
                                    </div>
                                    <div class='col-md-8'>
                                        <div class='form-group'>
                                            <label>Alamat*</label>
                                            <textarea id="address" validate="required[]" name ='address' class='form-control' style="resize:none" rows="3" ><?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->address) : "" ); ?></textarea>
                                        </div>
                                        <div class='row'>
                                            <div class="col-lg-4">
                                                <div class='form-group'>
                                                    <label>Propinsi*</label>
                                                    <?php require_once get_component_url() . "dropdown/com_drop_province.php"; ?>
                                                </div>

                                            </div>
                                            <div class="col-lg-4">
                                                <div class='form-group'>
                                                    <label>Kabupaten*</label>
                                                    <?php require_once get_component_url() . "dropdown/com_drop_city.php"; ?>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class='form-group'>
                                                    <label>Kecamatan*</label>
                                                    <?php require_once get_component_url() . "dropdown/com_drop_district.php"; ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class='form-group'>
                                            <label>Kode Pos</label>
                                            <input id="postal_code" name='postal_code' type='text' class='form-control' value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->postal_code) : "" ); ?>" maxlength="10">
                                        </div>

                                    </div>
                                    <!-- /.box -->
                                </div>

                                <div class="panel-footer">
                                    <div class="row hidden-xs">
                                        <div class="col-md-6 col-lg-6 col-sm-6 " style="text-align: left;">
                                            <a href='<?php echo base_url() ?>' class='btn btn-front btn-flat '><i class='fa fa-shopping-cart'></i>&nbsp; Lanjutkan Belanja</a>
                                        </div>
                                        <div class="col-md-6 col-lg-6 col-sm-6 " style="text-align: right; ">
                                            <button type="submit" onclick="next_step()" class='btn btn-front btn-flat' style='background-color: #0992a6'>Lanjutkan Metode Pembayaran <i class='fa fa-arrow-circle-o-right'></i>&nbsp; </button>
                                            <!--                                            <button type="submit">test</button>-->
                                        </div>
                                    </div>
                                    <div class="row hidden-lg hidden-md hidden-sm">
                                        <div class="col-xs-12 " ><a href='<?php echo base_url() ?>' class='btn btn-front btn-flat btn-block'><i class='fa fa-shopping-cart'></i>&nbsp; Lanjutkan Belanja</a></div>
                                        <br><br><br>
                                        <div class="col-xs-12 " >
                                            <button type="submit" onclick="next_step()" class='btn btn-front btn-block btn-flat' style='background-color: #0992a6'>Lanjutkan Metode Pembayaran <i class='fa fa-arrow-circle-o-right'></i>&nbsp; </button>
                                        </div>
                                    </div></div>
                            </div>
                        </form>
                    </div>
                    <script text="javascript">
                            function change_province() {
                                var province_seq = $('#drop_province').val();
                                $.ajax({
                                    url: "<?php echo get_base_url() . "member/checkout" ?>",
                                    type: "POST",
                                    dataType: "json",
                                    data: {
                                        "type": "<?php echo TASK_PROVINCE_CHANGE ?>",
                                        "province_seq": province_seq
                                    },
                                    success: function(data) {
                                        if (isSessionExpired(data)) {
                                            response_object = json_decode(data);
                                            url = response_object.url;
                                            location.href = url;
                                        } else {
                                            $('#drop_city option').remove();
                                            $('#drop_district option').remove();
                                            $("#drop_city").append("<option value=\"" + "\">-- Pilih --</option>");
                                            $(data).each(function(i) {
                                                $("#drop_city").append("<option value=\"" + data[i].seq + "\">" + data[i].name + "</option>")
                                            });
                                            $('.select2').select2();
                                        }
                                    },
                                    error: function(request, error) {
                                        alert(error_response(request.status));
                                    }
                                });
                            }

                            function change_city() {
                                var city_seq = $('#drop_city').val();
                                $.ajax({
                                    url: "<?php echo get_base_url() . "member/checkout" ?>",
                                    type: "POST",
                                    dataType: "json",
                                    data: {
                                        "type": "<?php echo TASK_CITY_CHANGE ?>",
                                        "city_seq": city_seq
                                    },
                                    success: function(data) {
                                        if (isSessionExpired(data)) {
                                            response_object = json_decode(data);
                                            url = response_object.url;
                                            location.href = url;
                                        } else {
                                            $('#drop_district option').remove();
                                            $("#drop_district").append("<option value=\"" + "\">-- Pilih --</option>");
                                            $(data).each(function(i) {
                                                $("#drop_district").append("<option value=\"" + data[i].seq + "\">" + data[i].name + "</option>")
                                            });
                                            $('.select2').select2();
                                        }
                                    },
                                    error: function(request, error) {
                                        alert(error_response(request.status));
                                    }
                                });
                            }

                            function change_member_address() {
                                var member_address_seq = $('#drop_member_address').val();
                                $.ajax({
                                    url: "<?php echo get_base_url() . "member/checkout" ?>",
                                    type: "POST",
                                    dataType: "json",
                                    data: {
                                        "type": "<?php echo TASK_MEMBER_ADDRESS_CHANGE; ?>",
                                        "member_address_seq": member_address_seq
                                    },
                                    success: function(data) {
                                        if (isSessionExpired(data)) {
                                            response_object = json_decode(data);
                                            url = response_object.url;
                                            location.href = url;
                                        } else {
                                            $(data[0]).each(function(i) {
                                                $('#drop_city option').remove();
                                                $('#drop_district option').remove();
                                                $(data[1]).each(function(ix) {
                                                    $("#drop_city").append("<option value=\"" + data[1][ix].seq + "\">" + data[1][ix].name + "</option>")
                                                });
                                                $(data[2]).each(function(ix) {
                                                    $("#drop_district").append("<option value=\"" + data[2][ix].seq + "\">" + data[2][ix].name + "</option>")
                                                });
                                                $('#receiver_name').val(data[0][i].pic_name);
                                                $('#address').val(data[0][i].address);
                                                $('#postal_code').val(data[0][i].zip_code);
                                                $('#phone_no').val(data[0][i].phone_no);
                                                $('#drop_province').val(data[0][i].province_seq);
                                                $('#drop_city').val(data[0][i].city_seq);
                                                $('#drop_district').val(data[0][i].district_seq);
                                                $('.select2').select2();
                                            });
                                        }
                                    },
                                    error: function(request, error) {
                                        alert(error_response(request.status));
                                    }
                                });
                            }

                            function next_step() {
                                $('#province_name').val($('#drop_province option:selected').text());
                                $('#city_name').val($('#drop_city option:selected').text());
                                $('#district_name').val($('#drop_district option:selected').text());
                            }

                    </script>
                <?php } elseif ($data_step == TASK_SECOND_STEP) {
                    ?>
                    <!--                    METODE PEMBAYARAN-->

                    <div class="tab-pane fade active in" id="metode-pembayaran">
                        <br>

                        <form id="frmMain" method="post" url="<?php echo get_base_url() . $data_auth[FORM_URL] . "?step=3" ?>" >
                            <div class="panel panel-primary">
                                <div class="panel-heading big_title">Metode Pembayaran</div>
                                <div class="panel-body">
                                    <input type="hidden" name ="type" value="<?php echo TASK_SECOND_STEP; ?>">
                                    <input type="hidden" name ="step" value="<?php echo TASK_FINAL_STEP; ?>">
                                    <label>Pilih metode pembayaran : </label><br>
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-default payment_type <?php echo!isset($payment_info) || $payment_info["payment_seq"] == PAYMENT_SEQ_BANK || $payment_info["payment_seq"] == PAYMENT_SEQ_DOCU_ATM ? "active" : "" ?>" >
                                            <input id="transfer-bank" name="payment" value ="<?php echo PAYMENT_SEQ_BANK; ?>" type="radio" onClick="show_div(this.id)" <?php echo (!isset($payment_info) || $payment_info["payment_seq"] == PAYMENT_SEQ_BANK) || $payment_info["payment_seq"] == PAYMENT_SEQ_DOCU_ATM ? "checked" : ""; ?> >Transfer Bank
                                        </label>
                                        <label class="btn btn-default   payment_type <?php echo isset($payment_info) && $payment_info["payment_seq"] == PAYMENT_SEQ_DEPOSIT ? "active" : "" ?>">
                                            <input id="saldo" name="payment" type="radio" value ="<?php echo PAYMENT_SEQ_DEPOSIT; ?>" onClick="show_div(this.id)" <?php echo isset($payment_info) && $payment_info["payment_seq"] == PAYMENT_SEQ_DEPOSIT ? "checked" : ""; ?>>Deposit
                                        </label>
                                        <label class="btn btn-default  payment_type <?php echo isset($payment_info) && $payment_info["payment_seq"] == PAYMENT_SEQ_CREDIT_CARD ? "active" : "" ?>"">
                                            <input id="kartu-kredit" name="payment" type="radio" value ="<?php echo PAYMENT_SEQ_CREDIT_CARD; ?>" onClick="show_div(this.id)" <?php echo isset($payment_info) && $payment_info["payment_seq"] == PAYMENT_SEQ_CREDIT_CARD ? "checked" : ""; ?>>Kartu Kredit
                                        </label>
                                        <label class="btn btn-default  payment_type <?php echo isset($payment_info) && ($payment_info["payment_seq"] == PAYMENT_SEQ_BCA_KLIKPAY || $payment_info["payment_seq"] == PAYMENT_SEQ_MANDIRI_KLIKPAY) ? "active" : "" ?>">
                                            <input id="internet-banking" name="payment" type="radio" value ="<?php echo PAYMENT_SEQ_BCA_KLIKPAY; ?>" onClick="show_div(this.id)" <?php echo (isset($payment_info) && (($payment_info["payment_seq"] == PAYMENT_SEQ_BCA_KLIKPAY) || ($payment_info["payment_seq"] == PAYMENT_SEQ_MANDIRI_KLIKPAY))) ? "checked" : ""; ?>>Internet Banking
                                        </label>
                                        <label class="btn btn-default  payment_type <?php echo isset($payment_info) && $payment_info["payment_seq"] == PAYMENT_SEQ_MANDIRI_ECASH ? "active" : "" ?>">
                                            <input id="emoney" name="payment" type="radio" value ="<?php echo PAYMENT_SEQ_MANDIRI_ECASH; ?>" onClick="show_div(this.id)" <?php echo isset($payment_info) && $payment_info["payment_seq"] == PAYMENT_SEQ_MANDIRI_ECASH ? "checked" : ""; ?>>E-money
                                        </label>
                                    </div>
                                    <br><br>

                                    <div id="div" class="saldo-div" <?php echo isset($payment_info) && $payment_info["payment_seq"] == PAYMENT_SEQ_DEPOSIT ? "" : "style=\"display:none\"" ?>>
                                        <h3>Saldo Deposit <?php echo "Rp." . number_format($member_info[2][0]->deposit_amt); ?></h3>
                                    </div>
                                    <div id="div" class="transfer-bank-div" <?php echo!isset($payment_info) || $payment_info["payment_seq"] == PAYMENT_SEQ_BANK || $payment_info["payment_seq"] == PAYMENT_SEQ_DOCU_ATM ? "" : "style=\"display:none\"" ?>>
                                        <input type="radio" name="atm_type"  value ="<?php echo PAYMENT_SEQ_BANK; ?>" checked> Pembayaran Setoran Tunai atau Via LLG Antar Bank
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped table-hover info">
                                                <tr class="info">
                                                    <th>Bank</th>
                                                    <th>Nomor Rekening</th>
                                                    <th>Atas Nama</th>
                                                </tr>
                                                <?php foreach ($data_bank as $bank) { ?>
                                                    <tr>
                                                        <td> <img height="30px" src="<?php echo get_image_location() . BANK_UPLOAD_IMAGE . $bank->logo_img; ?>" alt="<?php echo $bank->logo_img; ?>"></th>
                                                        <td><?php echo $bank->bank_acct_no; ?> </td>
                                                        <td><?php echo $bank->bank_acct_name; ?></td>
                                                    </tr>
                                                <?php } ?>
                                            </table>
                                        </div>
                                        <input type="radio" name="atm_type"  value ="<?php echo PAYMENT_SEQ_DOCU_ATM; ?>" <?php echo isset($payment_info) && $payment_info["payment_seq"] == PAYMENT_SEQ_DOCU_ATM ? "checked" : "" ?>> Pembayaran Via ATM Bersama
                                        </br>
                                        <img src ="<?php echo get_img_url() ?>payment/logo-atm.png" alt="Docu ATM">
                                    </div>
                                    <div id="div" class="kartu-kredit-div" <?php echo isset($payment_info) && $payment_info["payment_seq"] == PAYMENT_SEQ_CREDIT_CARD ? "" : "style=\"display:none\"" ?>> <img class="img-responsive col-lg-4 col-md-4 col-sm-4 col-xs-4" src="<?php echo get_img_url() . "credit_card.jpg" ?>"> </div>
                                    <div id="div" class="internet-banking-div" <?php echo isset($payment_info) && ($payment_info["payment_seq"] == PAYMENT_SEQ_BCA_KLIKPAY || $payment_info["payment_seq"] == PAYMENT_SEQ_MANDIRI_KLIKPAY) ? "" : "style=\"display:none\"" ?>>
                                        <input type="radio" name="bank_type" value ="<?php echo PAYMENT_SEQ_MANDIRI_KLIKPAY; ?>" <?php echo!isset($payment_info) || $payment_info["payment_seq"] != PAYMENT_SEQ_BCA_KLIKPAY ? "checked" : "" ?>>
                                        <img src ="<?php echo get_img_url() ?>payment_70px/mandiriclickpay_70px.png" alt="Mandiri Clikpay">
                                    </div>
                                    <div id="div" class="emoney-div" <?php echo isset($payment_info) && $payment_info["payment_seq"] == PAYMENT_SEQ_MANDIRI_ECASH ? "" : "style=\"display:none\"" ?>>
                                        <input type="radio" name="emoney_type"  value ="<?php echo PAYMENT_SEQ_MANDIRI_ECASH; ?>" checked>
                                        <img src ="<?php echo get_img_url() ?>payment_70px/mandiri e-cash_70px.png" alt="Mandiri e-cash">
                                    </div>									
                                </div>
                                <div class="panel-footer">
                                    <div class="row hidden-xs">
                                        <div class="col-md-6 col-lg-6 col-sm-6 " style="text-align: left;" >
                                            <a href='<?php echo base_url(); ?>' class='btn btn-front btn-flat '><i class='fa fa-shopping-cart'></i>&nbsp; Lanjutkan Belanja</a>
                                        </div>
                                        <div class="col-md-6 col-lg-6 col-sm-6 " style="text-align: right; ">
                                            <button type="submit" class='btn btn-front btn-flat' style='background-color: #0992a6'>Lanjutkan Ringkasan Belanja <i class='fa fa-arrow-circle-o-right'></i>&nbsp; </button>
                                        </div>
                                    </div>
                                    <div class="row hidden-lg hidden-md hidden-sm">
                                        <div class="col-xs-12"  >
                                            <a href='<?php echo base_url(); ?>' class='btn btn-front btn-flat btn-block'><i class='fa fa-shopping-cart'></i>&nbsp; Lanjutkan Belanja</a>
                                        </div>
                                        <br><br><br>
                                        <div class="col-xs-12" >
                                            <button type="submit" class='btn btn-front btn-flat btn-block' style='background-color: #0992a6'>Lanjutkan Ringkasan Belanja <i class='fa fa-arrow-circle-o-right'></i>&nbsp; </button>
                                        </div>
                                    </div>                                    
                                </div>
                            </div>
                        </form>

                    </div>
                <?php } else if ($data_step == TASK_FINAL_STEP) {
                    ?>
                    <!--DETAIL TRANSAKSI-->
                    <div class = "tab-pane active in" id = "detil-transaksi">
                        <form method="post" action ="<?php echo get_base_url() ?>member/checkout?step=3" >
                            <?php echo get_csrf_member_token() ?>
                            <input type="hidden" name="type" value="<?php echo TASK_FINAL_STEP ?>">
                            <br>
                            <div class = "panel panel-primary">
                                <div class = "panel-heading big_title">Ringkasan Belanja</div>
                                <div class = 'panel-body'>
                                    <div class = 'row'>
                                        <div class = 'col-md-6 col-sm-12 col-xs-12' style = "margin-top:-20px;">
                                            <p><h3>Informasi Penerima : </h3></p>
                                            <div class = "well">
                                                <table border = '0' class = "table-informasi">
                                                    <style>
                                                        .table-informasi td{
                                                            padding: 2px;
                                                        }
                                                    </style>
                                                    <tr>
                                                        <td valign = 'top' style = ""><span class = 'fa fa-user fa-lg'></td>
                                                        <td><?php echo $data_sel[LIST_DATA][0]["receiver_name"]; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign = 'top' ><span class = 'fa fa-home fa-lg'></td>
                                                        <td valign = 'top' ><?php echo $data_sel[LIST_DATA][0]["address"] . "</br>" . $data_sel[LIST_DATA][0]["province_name"] . "-" . $data_sel[LIST_DATA][0]["city_name"] . "-" . $data_sel[LIST_DATA][0]["district_name"] . "</br> Kode Pos :" . $data_sel[LIST_DATA][0]["postal_code"]; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td><span class = 'fa fa-phone fa-lg'></td>
                                                        <td><?php echo $data_sel[LIST_DATA][0]["phone_no"]; ?></td>
                                                    </tr>
                                                </table>

                                            </div>
                                        </div>
                                        <div class = 'col-md-6 col-sm-12 col-xs-12' style = "margin-top:-20px;">

                                            <p><h3>Metode Pembayaran : </h3></p>
                                            <div class = "well">
                                                <span class = 'fa fa-bars fa-lg'></span> &nbsp;
                                                <?php echo $payment_info["payment_name"]; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class = 'row'>
                                        <div class = "col-lg-12 col-md-12 col-sm-12" style = "margin-top:-25px;">
                                            <p><h3>Keranjang Belanja : </h3></p>

                                            <table class = "field-group-format group_specs table table-bordered  hidden-xs">
                                                <tr class = "success" id = "tr-cart">
                                                    <th class = "field-label" colspan = "2" id = "tr-cart">Produk</th>
                                                    <th class = "field-label" id = "tr-cart">Harga (Rp)</th>
                                                    <th class = "field-label" id = "tr-cart">Qty</th>
                                                    <th class = "field-label" id = "tr-cart">Biaya Kirim (Rp)</th>
                                                    <th class = "field-label" id = "tr-cart">Total (Rp)</th>
                                                </tr>
                                                <?php
                                                $order;
                                                $total = 0;
                                                if (isset($data_prod) && sizeof($data_prod) > 0) {
                                                    foreach ($data_prod as $key => $prod) {
                                                        $order[$prod[$key]["merchant_seq"]][] = $prod[$key];
                                                    }
                                                    foreach ($order as $key => $prod) {
                                                        ?>
                                                        <tr>
                                                            <td colspan = "8" valign = "bottom">
                                                                <div class = 'row'>
                                                                    <div class = "col-lg-3 col-md-3 col-sm-3"><b><i class = "material-icons" style="font-size:20px; vertical-align:top;">store</i>&nbsp;
                                                                            <a href = "<?php echo base_url() . "merchant/" . $prod[0]["merchant_code"] ?>" ><?php echo $prod[0]["merchant_name"]; ?></a></b></div>
                                                                    <div class = "col-lg-2 col-md-2 col-sm-2" ><b><i class = "fa fa-truck"></i>&nbsp;
                                                                            <?php echo $prod[0]["exp_name"]; ?> </b></div>
                                                                    <div class = "col-lg-7 col-md-7 col-sm-7 text-right" ><input id="msg_<?php echo $prod[0]["merchant_seq"]; ?>" onblur="set_value(this.id)" type = "text" name="member_message[<?php echo $prod[0]["merchant_seq"]; ?>]"  class = "form-control input-sm" value="<?php echo $prod[0]["member_message"]; ?>" placeholder = "Pesan untuk merchant <?php echo $prod[0]["merchant_name"]; ?>" maxlength="100"></div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        if (isset($prod)) {
                                                            foreach ($prod as $prod_item) {
                                                                ?>
                                                                <tr id = "tr-cart">
                                                                    <td class = "field-content">
                                                                        <a href ="<?php echo base_url() . url_title($prod_item["product_name"] . " " . $prod_item["product_seq"]); ?>"><center><img src = "<?php echo get_image_location() . PRODUCT_UPLOAD_IMAGE . $prod_item["merchant_seq"] . "/" . S_IMAGE_UPLOAD . $prod_item["img"]; ?>" alt="<?php echo $prod_item["img"] ?>" width = "100px"></center></a>
                                                                    </td>
                                                                    <td class = "field-label" align = "left"><strong><?php echo $prod_item["product_name"]; ?></strong><br>
                                                                        <br>
                                                                        <table border = "0" class = "table table-condensed">
                                                                            <tr>
                                                                                <td width = "20%">Berat</td>
                                                                                <td width = "5%">:</td>
                                                                                <td><?php echo $prod_item["product_weight"]; ?> Kg</td>
                                                                            </tr>
                                                                            <?php if ($prod_item["variant_value"] != DEFAULT_VALUE_VARIANT_SEQ) { ?>
                                                                                <tr>
                                                                                    <td>Warna</td>
                                                                                    <td>:</td>
                                                                                    <td><?php echo $prod_item["variant_name"]; ?></td>
                                                                                </tr>
                                                                            <?php } ?>
                                                                        </table>
                                                                    </td>
                                                                    <td class = "field-content"><?php echo number_format($prod_item["sell_price"]); ?></td>
                                                                    <td class = "field-content"><?php echo number_format($prod_item["qty"]); ?></td>
                                                                    <td class = "field-content"><?php echo $prod_item["exp_fee"] == 0 ? "<div class='text-green'>Free</div>" : number_format($prod_item["exp_fee"] * ceil($prod_item["product_weight"] * $prod_item["qty"])); ?></td>
                                                                    <td class = "field-content"><?php
                                                                        echo number_format($prod_item["qty"] * $prod_item["sell_price"] + $prod_item["exp_fee"] * ceil($prod_item["product_weight"] * $prod_item["qty"]));
                                                                        $total = $total + $prod_item["qty"] * $prod_item["sell_price"] + $prod_item["exp_fee"] * ceil($prod_item["product_weight"] * $prod_item["qty"]);
                                                                        ?></td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                    }
                                                }
                                                ?>

                                                <tr class = "font22">
                                                    <th colspan = "6" style = "text-align: left;">
                                                <div class = "row">
                                                    <div class = "col-lg-6 col-md-6 col-sm-6 ">
                                                        <font size = "4px">Pilih Voucher : <?php require get_component_url() . "dropdown/com_drop_promo_voucher.php"; ?></font>
                                                    </div>
                                                    <div class = "col-lg-6 col-md-6 col-sm-6" style = "text-align: right; ">
                                                        <font size = "4px">Total Belanja : Rp <?php echo number_format($total); ?>
                                                        </font><p> Total Bayar : <font color = "red">Rp <span id="total" nominal="<?php echo $total; ?>"><?php echo number_format($total); ?></span></font></p>
                                                    </div>
                                                    <?php if ($coupon == true) { ?>
                                                        <div class ="col-lg-6 col-md-6 col-sm-6">
                                                            <font size = "4px"> Kode Kupon / Voucher: </font>
                                                            <div class="input-group input-group-sm">
                                                                <input id="coupon_code" class ="input-sm form-control" type="text" placeholder="Masukan Kode Kupon / Voucher" onblur="set_coupon(id)">
                                                                <span class="input-group-btn">
                                                                    <button id="apply" type="button" class="btn btn-success btn-flat" onClick="get_coupon()">Terapkan</button>
                                                                </span>                                                             
                                                            </div>
                                                            <label id="coupon_field" class="help-block" style="font-size:12px"></label>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                                </th>
                                                </tr>
                                            </table>

                                            <!--TABLE DETAIL FOR XS-->
                                            <div class="table-responsive">
                                                <div class = "hidden-lg hidden-sm hidden-md ">
                                                    <table class = "table table-bordered">
                                                        <tr class = "success">
                                                            <th class = "field-label">Produk</th>
                                                            <th class = "field-label">Keterangan</th>
                                                        </tr>
                                                        <?php
                                                        if (isset($data_prod) && sizeof($data_prod) > 0) {
                                                            foreach ($order as $key => $prod) {
                                                                ?>
                                                                <tr>
                                                                    <td class = "field-content" colspan = "4">
                                                                        <div class = "row">
                                                                            <div class = "col-xs-4 text-left"><b><i class = "material-icons" style="font-size:20px; vertical-align:top;">store</i>&nbsp;&nbsp;
                                                                                    <a href = "<?php echo base_url() . "merchant/" . $prod[0]["merchant_code"] ?>"><?php echo $prod[0]["merchant_name"]; ?></a></b> &nbsp;
                                                                            </div>
                                                                            <div class = "col-xs-2 text-left"><b><i class = "fa fa-truck "></i>&nbsp;
                                                                                    <?php echo $prod[0]["exp_name"]; ?></b></div>
                                                                            <div class = "col-xs-6 text-right" ><input id="msg_<?php echo $prod[0]["merchant_seq"]; ?>_xs" onblur="set_value(this.id)" type= "text" class = "input-sm form-control" placeholder = "Pesan untuk merchant "<?php echo $prod[0]["merchant_name"]; ?> maxlength="100"></div>
                                                                        </div>

                                                                </tr>
                                                                <?php foreach ($prod as $prod_item) { ?>
                                                                    <tr>
                                                                        <td><a href ="<?php echo base_url() . url_title($prod_item["product_name"] . " " . $prod_item["product_seq"]); ?>"><center><img src =  "<?php echo get_image_location() . PRODUCT_UPLOAD_IMAGE . $prod_item["merchant_seq"] . "/" . S_IMAGE_UPLOAD . $prod_item["img"]; ?>" alt="<?php echo $prod_item["img"] ?>" class = 'img' width = '100px' ></center></a>
                                                                        </td>
                                                                        <td>
                                                                            <strong><?php echo $prod_item["product_name"]; ?></strong>
                                                                            <br><br>
                                                                            <div class = "row container">
                                                                                <table border = "0" class = "table table-condensed">
                                                                                    <tr>
                                                                                        <td>Berat</td>
                                                                                        <td>:</td>
                                                                                        <td><?php echo $prod_item["product_weight"]; ?> Kg</td>
                                                                                    </tr>
                                                                                    <?php if ($prod_item["variant_value"] != DEFAULT_VALUE_VARIANT_SEQ) { ?>
                                                                                        <tr>
                                                                                            <td>Warna</td>
                                                                                            <td>:</td>  
                                                                                            <td><?php echo $prod_item["variant_name"]; ?></td>
                                                                                        </tr>
                                                                                    <?php } ?>
                                                                                    <tr>
                                                                                        <td>Qty</td>
                                                                                        <td>:</td>
                                                                                        <td><?php echo number_format($prod_item["qty"]); ?></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Harga</td>
                                                                                        <td>:</td>
                                                                                        <td>Rp <?php echo number_format($prod_item["sell_price"]); ?></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Biaya Kirim</td>
                                                                                        <td>:</td>
                                                                                        <td><?php echo $prod_item["exp_fee"] == 0 ? "<div class='text-green'>Free</div>" : "Rp. " . number_format($prod_item["exp_fee"] * ceil($prod_item["product_weight"] * $prod_item["qty"])); ?></td>                                                                                    
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Total</td>
                                                                                        <td>:</td>
                                                                                        <td>Rp <?php echo number_format($prod_item["qty"] * $prod_item["sell_price"] + $prod_item["exp_fee"] * ceil($prod_item["product_weight"] * $prod_item["qty"])); ?></td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                        <tr>
                                                            <th class = 'field-content' colspan = '2' ><div class = 'text-right'><font size = "3px" >Total Belanja : Rp <?php echo number_format($total); ?></font></div></th>
                                                        </tr>
                                                        <tr>
                                                            <th colspan="2" >
                                                        <div class = "row">
                                                            <div class = "col-xs-12  text-right">
                                                                <font size = "3px">Pilih Voucher : <?php require get_component_url() . "dropdown/com_drop_promo_voucher_xs.php"; ?></font>
                                                            </div>
                                                        </div>
                                                        </br>
                                                        <?php if ($coupon == true) { ?>
                                                            <div class = "row">
                                                                <div class = "col-xs-12  text-right">
                                                                    <font size = "3px">Kode Kupon / Voucher :  </font>
                                                                    <div class="input-group input-group-sm">
                                                                        <input id="coupon_code_xs" class ="input-xs form-control col-xs-12" type="text" placeholder="Masukan Kode Kupon / Voucher" onblur="set_coupon(id)">
                                                                        <span class="input-group-btn">
                                                                            <button id="apply_xs" type="button" class="btn btn-success btn-flat" onClick="get_coupon()">Terapkan</button>
                                                                        </span>                                                                    
                                                                    </div>     
                                                                    <label id="coupon_field_xs" class="help-block" style="font-size:12px"></label>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                        </th>
                                                        </tr>
                                                        <tr>
                                                            <th class = "field-content" colspan = "2">
                                                        <div class = "row">
                                                            <div class = "col-xs-12" style = "text-align: right; ">
                                                                <font size = "4px">Total Bayar : <font color = "red">Rp <span id="total_xs" nominal="<?php echo $total; ?>"><?php echo number_format($total); ?></span></font></font>
                                                            </div>
                                                        </div>
                                                        </th>
                                                        </tr>
                                                    </table>

                                                    <!--/.box-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "panel-footer">
                                    <div class = "row">
                                        <div class = "col-md-12 col-lg-12 col-sm-12 col-xs-12" style = "text-align: right; ">
                                            <button type="submit" class ='btn btn-front btn-flat btn-lg' style = 'background-color: #0992a6'>Bayar Sekarang <i class = 'fa fa-check-circle-o'></i>&nbsp;</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <script text="javascript">
                            function voucher_change(id) {
                                var voucher = $('#' + id).val();
                                if (id.substr(id.length - 2) == 'xs') {
                                    $('#' + id.substr(0, id.length - 3)).val(voucher);
                                } else {
                                    $('#' + id + '_xs').val(voucher);
                                }
                                $('select').select2();
                                var total = $('#total').attr('nominal');
                                var voucher = $('#drop_voucher option:selected').attr('nominal');
                                if (total - voucher > 0) {
                                    $('#total').text(numberWithCommas(total - voucher));
                                    $('#total_xs').text(numberWithCommas(total - voucher));
                                } else {
                                    $('#total').text(0);
                                    $('#total_xs').text(0);
                                }
                            }
                            function numberWithCommas(x) {
                                return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            }

                            function set_value(id) {
                                var text = $('#' + id).val();
                                if (id.substr(id.length - 2) == 'xs') {
                                    $('#' + id.substr(0, id.length - 3)).val(text);
                                } else {
                                    $('#' + id + '_xs').val(text);
                                }
                            }
    <?php if ($coupon == true) { ?>
                                function get_coupon() {
                                    var coupon_code = $('#coupon_code').val();
                                    $.ajax({
                                        url: "<?php echo base_url() . "member/checkout" ?>",
                                        type: "POST",
                                        dataType: "json",
                                        data: {type: "<?php echo TASK_GET_COUPON ?>",
                                            coupon_code: coupon_code,
                                            total_order: $('#total').attr('nominal'),
                                            product_variant_seq: "<?php echo $product_variant_string; ?>"},
                                       success: function(data) {
                                            if (isSessionExpired(data)) {
                                                response_object = json_decode(data);
                                                url = response_object.url;
                                                location.href = url;
                                            } else {
                                                if (data.error == "true") {
                                                    $('#coupon_field').removeClass('has-success');
                                                    $('#coupon_field').removeClass('has-error');
                                                    $('#coupon_field').addClass('has-error');
                                                    $('#coupon_field').addClass('text-red');
                                                    $('#coupon_field').text(data.message);

                                                    $('#coupon_field_xs').removeClass('has-success');
                                                    $('#coupon_field_xs').removeClass('has-error');
                                                    $('#coupon_field_xs').addClass('has-error');
                                                    $('#coupon_field_xs').addClass('text-red');
                                                    $('#coupon_field_xs').text(data.message);
                                                } else {
                                                    var total = $('#total').attr('nominal');
                                                    $('#coupon_field').removeClass('has-success');
                                                    $('#coupon_field').removeClass('has-error');
                                                    $('#coupon_field').addClass('has-success');
                                                    $('#coupon_code').attr('readonly', true);
                                                    $('#coupon_field_xs').removeClass('has-success');
                                                    $('#coupon_field_xs').removeClass('has-error');
                                                    $('#coupon_field_xs').addClass('has-success');
                                                    $('#coupon_code_xs').attr('readonly', true);
                                                    $('#apply').removeClass('btn-success');
                                                    $('#apply').addClass('btn-danger');
                                                    $("#apply").attr("onclick", "clear_coupon()");
                                                    $('#apply').text("Hapus");
                                                    $('#apply_xs').removeClass('btn-success');
                                                    $('#apply_xs').addClass('btn-danger');
                                                    $("#apply_xs").attr("onclick", "clear_coupon()");
                                                    $('#apply_xs').text("Hapus");
                                                    $('#drop_voucher_xs').val('0');
                                                    $('#drop_voucher').val('0');
                                                    $('#drop_voucher_xs').prop('disabled', true);
                                                    $('#drop_voucher').prop('disabled', true);
                                                    $('select').select2();
                                                    $('#coupon_field_xs').addClass('text-green');
                                                    $('#coupon_field_xs').text('Anda mendapatkan potongan sebesar Rp ' + numberWithCommas(data[0].nominal));
                                                    $('#coupon_field').addClass('text-green');
                                                    $('#coupon_field').text('Anda mendapatkan potongan sebesar Rp ' + numberWithCommas(data[0].nominal));
                                                    if (total - data[0].nominal > 0) {
                                                        $('#total').text(numberWithCommas(total - data[0].nominal));
                                                        $('#total_xs').text(numberWithCommas(total - data[0].nominal));
                                                    } else {
                                                        $('#total').text(0);
                                                        $('#total_xs').text(0);
                                                    }
                                                }
                                            }
                                        }
                                    })

                                }

                                function set_coupon(id) {
                                    var coupon = $('#' + id).val();
                                    if (id.substr(id.length - 2) == 'xs') {
                                        $('#' + id.substr(0, id.length - 3)).val(coupon);
                                    } else {
                                        $('#' + id + '_xs').val(coupon);
                                    }
                                }

                                function clear_coupon() {
                                    $.ajax({
                                        url: "<?php echo base_url() . "member/checkout" ?>",
                                        type: "POST",
                                        dataType: "json",
                                        data: {type: "<?php echo TASK_CLEAR_COUPON ?>"},
                                        success: function() {
                                            var total = $('#total').attr('nominal');

                                            $('#coupon_code').val('');
                                            $('#coupon_code').attr('readonly', false);
                                            $('#coupon_code').removeClass('text-green');

                                            $('#apply').removeClass('btn-danger');
                                            $('#apply').addClass('btn-success');
                                            $('#apply').text("Terapkan");
                                            $("#apply").attr("onclick", "get_coupon()");

                                            $('#coupon_code_xs').val('');
                                            $('#coupon_code_xs').attr('readonly', false);
                                            $('#coupon_code_xs').removeClass('text-green');

                                            $('#apply_xs').removeClass('btn-danger');
                                            $('#apply_xs').addClass('btn-success');
                                            $('#apply_xs').text("Terapkan");
                                            $("#apply_xs").attr("onclick", "get_coupon()");

                                            $('#drop_voucher_xs').prop('disabled', false);
                                            $('#drop_voucher').prop('disabled', false);

                                            $('#coupon_field_xs').text('');
                                            $('#coupon_field').text('');

                                            $('#total').text(numberWithCommas(total));
                                            $('#total_xs').text(numberWithCommas(total));
                                        }
                                    });
                                }
    <?php } ?>
                    </script>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<script>
    $("[data-mask]").inputmask();
</script>
