<?php
require_once VIEW_BASE_HOME;
?>

<div class="row hidden-xs " style="margin-top:55px;">&nbsp;</div>
<div class="row hidden-xs hidden-lg" style="margin-top:10px;">&nbsp;</div>
<div class="container">    
    <div class="row">
        <div class="container">
            <?php if (isset($data_err) && $data_err[ERROR] === true) { ?>
                <div class="alert alert-error-home">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $data_err[ERROR_MESSAGE][0] ?>
                </div>
            <?php } ?>
            <div class="panel panel-primary">
                <div class="panel-heading big_title">No. Order : <?php echo $data_ord[0][0]->order_no ?></div>
                <div class='panel-body'>
                    <div class='row'>
                        <div class='col-md-12' style="margin-top:-20px;">
                            <p><h3>Detil Order : </h3></p>
                            <div class="well">
                                <div class='row'>
                                    <div class='col-md-6'>
                                        <p><span class='fa fa-tags fa-lg'></span>&nbsp; No. Order : <?php echo $data_ord[0][0]->order_no; ?></p>
                                        <p><span class='fa fa-calendar fa-lg'></span>&nbsp; Tanggal Order : <?php echo date_format(date_create($data_ord[0][0]->order_date), "d-M-Y"); ?> </p>
                                    </div>
                                    <div class='col-md-6'>
                                        <span class="fa fa-exclamation-circle fa-lg"></span>&nbsp; Status Order : <font style="color: red;font-weight: bold;"> <?php
                                        $data = json_decode(STATUS_PAYMENT, true);
                                        echo $data[$data_ord[0][0]->payment_status];
                                        ?></font>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class='col-md-6' style="margin-top:-10px;">
                            <p><h3>Informasi Penerima : </h3></p>
                            <div class="well">
                                <table border='0' class="table-informasi">
                                    <style> .table-informasi td{padding: 2px;}
                                    </style>
                                    <tr>
                                        <td valign='top' style=""><span class='fa fa-user fa-lg'></td>
                                        <td ><?php echo $data_ord[0][0]->receiver_name; ?></td>
                                    </tr>
                                    <tr>
                                        <td valign='top' ><span class='fa fa-home fa-lg'></td>
                                        <td valign='top' ><?php echo $data_ord[0][0]->receiver_address . "<br>" . $data_ord[0][0]->province_name . "-" . $data_ord[0][0]->city_name . "-" . $data_ord[0][0]->district_name; ?></td>
                                    </tr>
                                    <tr>
                                        <td><span class='fa fa-phone fa-lg'></td>
                                        <td ><?php echo $data_ord[0][0]->receiver_phone_no; ?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!--                    </div>-->
                        <!--                    <div class='row'>-->
                        <div class='col-md-6' style="margin-top:-10px;">
                            <p><h3>Metode Pembayaran : </h3></p>
                            <div class="well">
                                <span class='fa fa-bars fa-lg'></span> &nbsp; <?php echo $data_ord[0][0]->payment_name; ?>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="col-md-12" style="margin-top:-20px;">
                            <p><h3>Keranjang Belanja : </h3></p>   
                            <!--                        TABLE CART FOR LG,MD,SM-->

                            <table class="field-group-format group_specs table table-bordered  hidden-xs" style="margin-top:-10px;">
                                <tr class="success" id="tr-cart">
                                    <th class="field-label" colspan="2" id="tr-cart">Produk</th>
                                    <th class="field-label" id="tr-cart">Harga (Rp)</th>
                                    <th class="field-label" id="tr-cart">Jumlah</th>
                                    <th class="field-label" id="tr-cart">Biaya Kirim (Rp)</th>
                                    <th class="field-label" id="tr-cart">Total (Rp)</th>
                                </tr>
                                <?php
                                $total = 0;
                                foreach ($data_ord[1] as $merchant) {
                                    ?>
                                    <tr>
                                        <th colspan="8">                                        
                                    <div class='row'>
                                        <div class="col-lg-4 col-md-3 col-sm-4"><b><i class = 'material-icons' style='font-size:20px; vertical-align:top;'>store</i>&nbsp; <a href="<?php echo base_url() . "merchant/" . $merchant->merchant_code; ?>"><?php echo $merchant->merchant_name; ?> </a></b></div> 
                                        <div class="col-lg-3 col-md-2 col-sm-3 text-left" ><b><i class="fa fa-truck"></i>&nbsp; <?php echo $merchant->expedition_name; ?> </b></div> 
                                        <div class="col-lg-5 col-md-7 col-sm-5 text-right" ><i class="fa fa-pencil-square-o" title="Pesan untuk merchant <?php echo $merchant->merchant_name; ?>"></i> <?php echo $merchant->member_notes; ?></div>
                                    </div>
                                    </th>
                                    </tr>  
                                    <?php
                                    foreach ($data_ord[2] as $prod) {
                                        if ($merchant->merchant_seq == $prod->merchant_seq) {
                                            ?>
                                            <td class="field-content">
                                                <a href ="<?php echo base_url() . url_title($prod->display_name . " " . $prod->product_seq); ?>"><center><img src="<?php echo get_base_url() . PRODUCT_UPLOAD_IMAGE . $prod->merchant_seq . "/" . S_IMAGE_UPLOAD . $prod->img; ?>" width="100px"></center></a></td>
                                            <td class="field-label"><strong><?php echo $prod->display_name ?></strong>
                                                <br><br>
                                                <table border="0" class="table table-condensed">
                                                    <tr>
                                                        <td width="20%">Berat</td>  
                                                        <td width="5%">:</td>
                                                        <td><?php echo $prod->weight_kg ?> Kg</td>
                                                    </tr>
                                                    <?php if ($prod->value_seq != DEFAULT_VALUE_VARIANT_SEQ) { ?>
                                                        <tr>
                                                            <td>Warna</td>
                                                            <td>:</td>
                                                            <td><?php echo $prod->variant_name; ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                    <tr>
                                                        <td>Status Kirim</td>
                                                        <td>:</td>
                                                        <td class="text-red"><?php
                                                            $data = json_decode(STATUS_ORDER, true);
                                                            $data_prod = json_decode(STATUS_XR, true);
                                                            echo $prod->product_status == PRODUCT_CANCEL_BY_MERCHANT_STATUS_CODE ? $data_prod[$prod->product_status] : $data[$merchant->order_status];
                                                            ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>No Resi</td>
                                                        <td>:</td>
                                                        <td class="text-red"><?php echo $prod->product_status == PRODUCT_CANCEL_BY_MERCHANT_STATUS_CODE ? "" : $merchant->awb_no; ?></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td class="field-content" id="tr-cart"><?php echo number_format($prod->sell_price); ?></td>
                                            <td class="field-content" id="tr-cart"><?php echo number_format($prod->qty); ?></td>
                                            <td class="field-content" id="tr-cart"><?php echo $prod->ship_price_charged == 0 ? "<div class='text-green'>Free</div>" : number_format(ceil($prod->weight_kg * $prod->qty) * $prod->ship_price_charged); ?></td>
                                            <td class="field-content" id="tr-cart"><?php
                                                echo number_format($prod->qty * $prod->sell_price + ceil($prod->weight_kg * $prod->qty) * $prod->ship_price_charged);
                                                $total = $total + $prod->qty * $prod->sell_price + ceil($prod->weight_kg * $prod->qty) * $prod->ship_price_charged;
                                                ?></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                                <tr>
                                <tr class="font22">
                                    <th  colspan="6" style="text-align: left;">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <font size="4px">Kode Voucher :<?php echo $data_ord[0][0]->voucher_code; ?> <br>Voucher Member / Kupon - Rp. <?php echo number_format($data_ord[0][0]->nominal); ?></font>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6" style="text-align: right; ">
                                        <font size="4px">Total Belanja : Rp <?php echo number_format($total); ?></font>
                                        <p><font size="4px">Total Bayar : </font><font color="red">Rp <?php echo number_format($data_ord[0][0]->total_payment); ?></font></a>
                                    </div>
                                </div>
                                </th>
                                </tr>
                            </table>
                            <!--
                            <!--                        TABLE CART FOR XS-->
                            <div class="hidden-lg hidden-sm hidden-md" >
                                <div class='table-responsive'>
                                    <table class="table table-bordered">
                                        <tr class="success">
                                            <th class="field-label"><center>Produk</center></th>
                                        <th class="field-label">Keterangan</th>
                                        </tr>
                                        <?php foreach ($data_ord[1] as $merchant) { ?>
                                            <tr>
                                                <td class="field-content" colspan="4">
                                                    <div class="row">
                                                        <div class="col-xs-12 text-left"><b><i class = 'material-icons' style='font-size:20px; vertical-align:top;'>store</i>&nbsp; <a href="<?php echo base_url() . "merchant/" . $merchant->merchant_code; ?>"><?php echo $merchant->merchant_name; ?> </a></b> &nbsp; </div>
                                                        <div class="col-xs-12 text-left"><b><i class="fa fa-truck "></i>&nbsp; <?php echo $merchant->expedition_name; ?> </b></div>
                                                        <div class="col-xs-12 text-left" ><i class="fa fa-pencil-square-o"></i> <b>Catatan  : "<?php echo $merchant->member_notes; ?>"</b></div>
                                                    </div></td>
                                            </tr>
                                            <?php
                                            foreach ($data_ord[2] as $prod) {
                                                if ($merchant->merchant_seq == $prod->merchant_seq) {
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <table border="0" class="table table-condensed">
                                                                <a href ="<?php echo base_url() . url_title($prod->display_name . " " . $prod->product_seq); ?>"><center> <img src="<?php echo get_base_url() . PRODUCT_UPLOAD_IMAGE . $prod->merchant_seq . "/" . S_IMAGE_UPLOAD . $prod->img; ?>" alt="<?php echo $prod->img; ?>" class='img' width='100%' height ="100%"></center></a>
                                                                <tr>
                                                                    <td>Status Kirim</td>
                                                                    <td>:</td>
                                                                    <td class="text-red"><?php
                                                                        $data = json_decode(STATUS_ORDER, true);
                                                                        $data_prod = json_decode(STATUS_XR, true);
                                                                        echo $prod->product_status == PRODUCT_CANCEL_BY_MERCHANT_STATUS_CODE ? $data_prod[$prod->product_status] : $data[$merchant->order_status];
                                                                        ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>No Resi</td>
                                                                    <td>:</td>
                                                                    <td class="text-red"><?php echo $prod->product_status == PRODUCT_CANCEL_BY_MERCHANT_STATUS_CODE ? "" : $merchant->awb_no ?></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td>
                                                            <strong><?php echo $prod->display_name ?></strong>
                                                            <br><br>
                                                            <table border="0" class="table table-condensed">
                                                                <tr>
                                                                    <td>Berat</td>
                                                                    <td>:</td>
                                                                    <td><?php echo $prod->weight_kg ?> Kg</td>
                                                                </tr>
                                                                <?php if ($prod->value_seq != DEFAULT_VALUE_VARIANT_SEQ) { ?>
                                                                    <tr>
                                                                        <td>Warna</td>
                                                                        <td>:</td>
                                                                        <td><?php echo $prod->variant_name; ?></td>
                                                                    </tr>
                                                                <?php } ?>
                                                                <tr>
                                                                    <td>Qty</td>
                                                                    <td>:</td>
                                                                    <td><?php echo number_format($prod->qty); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Harga</td>
                                                                    <td>:</td>
                                                                    <td>Rp. <?php echo number_format($prod->sell_price); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Biaya Kirim</td>
                                                                    <td>:</td>
                                                                    <td><?php echo $prod->ship_price_charged == 0 ? "<div class='text-green'>Free</div>" : number_format(ceil($prod->weight_kg * $prod->qty) * $prod->ship_price_charged); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Total</td>
                                                                    <td>:</td>
                                                                    <td>Rp <?php echo number_format($prod->qty * $prod->sell_price); ?></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                        }
                                        ?>
                                        <tr>
                                            <th class='field-content' colspan='2' ><div class='text-right'><font size="3px"  >Total Belanja : <?php echo number_format($total); ?></font></div></th>
                                        </tr>
                                        <tr><th colspan="2">
                                        <div class="row">
                                            <div class="col-xs-12  text-right">
                                                <font size="3px">Kode Voucher : Voucher Member / Kupon -  Rp <?php echo number_format($data_ord[0][0]->nominal); ?></font>
                                            </div>
                                        </div>
                                        </th></tr>
                                        <tr>
                                            <th class="field-content" colspan="2">
                                        <div class="row">
                                            <div class="col-xs-12" style="text-align: right; ">

                                                <font size="4px">Total Bayar : <font color="red">Rp <?php echo number_format($data_ord[0][0]->total_payment); ?></font></font></a>
                                            </div>
                                        </div>
                                        </th>
                                        </tr>
                                    </table>
                                </div>
                                <!-- /.box -->
                            </div>
                        </div>
                    </div>
                    <?php
                    if ($data_ord[0][0]->payment_code == PAYMENT_TYPE_BANK && $data_ord[0][0]->payment_status == PAYMENT_WAIT_CONFIRM_STATUS_CODE) {
                        ?>
                        <form id ="frmMain" action ="<?php echo get_base_url() . "member/payment/" . $data_ord[0][0]->order_no; ?>" method="post" enctype ="multipart/form-data">
                            <?php echo get_csrf_member_token(); ?>
                            <div class='row'>
                                <div class='col-md-12' style="margin-top:-20px;">
                                    <p><h3>Konfirmasi Pembayaran  </h3></p>
                                    <div class="well">
                                        <div class="row">
                                            <div class='col-md-6 col-xs-12 col-sm-12 col-lg-6'>
                                                <div class="form-group">
                                                    <label>Jenis Setoran</label>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="payment_type" value="<?php echo PAYMENT_TYPE_CASH; ?>" <?php echo isset($data_sel[LIST_DATA][0]->payment_type) && $data_sel[LIST_DATA][0]->payment_type == PAYMENT_TYPE_CASH ? "checked" : ""; ?>>
                                                            Setoran Tunai
                                                        </label>
                                                        &nbsp;
                                                        <label>
                                                            <input type="radio" name="payment_type" value="<?php echo PAYMENT_TYPE_TRANSFER; ?>" <?php echo isset($data_sel[LIST_DATA][0]->payment_type) && $data_sel[LIST_DATA][0]->payment_type == PAYMENT_TYPE_TRANSFER || !isset($data_sel[LIST_DATA][0]) ? "checked" : ""; ?>>
                                                            Transfer Bank
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class='form-group'>

                                                    <label>Tanggal Setoran</label>
                                                    <div class="input-group input-append date" id="dateRangePicker">
        <!--                                                    <input type="text" class="form-control" name="date" id="dateRangePicker" value='1994-07-08' >
                                                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>-->
                                                        <input class="form-control" id="datepicker" validate ="required[]" name ="payment_date" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->payment_date : date('d-M-Y') ); ?>" readonly>
                                                        <script type="text/javascript">
                                                            $('input[name="payment_date"]').daterangepicker({
                                                                singleDatePicker: true,
                                                                showDropdowns: true,
                                                                format: 'DD-MMM-YYYY',
                                                                maxDate: "<?php echo date('d-M-Y') ?>"
                                                            });</script>
                                                    </div>
                                                </div>
                                                <div class='form-group'>
                                                    <label>Jumlah Setoran</label>
                                                    <input type="text" name ="payment_amt" class='form-control auto_int' value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->payment_amt : 0 ); ?>" validate ="required[]">
                                                </div>
                                            </div>
                                            <div class='col-md-6 col-xs-12 col-sm-12 col-lg-6'>
                                                <div class='form-group'>
                                                    <label>Bukti Setoran</label>
                                                    <input type="file" name="nlogo_img" id="nlogo_img">
                                                </div>
                                                <div class="form-group">
                                                    <label>Setor ke Bank</label>
                                                    <?php foreach ($data_bank as $bank) { ?>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="bank" value="<?php echo $bank->seq; ?>" <?php echo isset($data_sel[LIST_DATA][0]) && $bank->seq == $data_sel[LIST_DATA][0]->confirm_pay_bank_seq ? "checked" : ""; ?>>
                                                                <img height="30px" src="<?php echo get_base_url() . BANK_UPLOAD_IMAGE . $bank->logo_img; ?>" alt="<?php echo $bank->logo_img; ?>"> 
                                                                <br>
                                                                A/C : <?php echo $bank->bank_acct_no; ?> <br>
                                                                A/N : <?php echo $bank->bank_acct_name; ?>
                                                            </label>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div class="panel-footer">
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" style="text-align: right; ">
                                        <button type="submit" name="<?php echo CONTROL_SAVE_EDIT_NAME; ?>" class='btn btn-front btn-flat btn-lg' style='background-color: #0992a6'>Proses <i class='fa fa-check-circle-o'></i>&nbsp; </a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                <?php } else if ($data_ord[0][0]->payment_code == PAYMENT_TYPE_CREDIT_CARD && $data_ord[0][0]->payment_status == PAYMENT_UNPAID_STATUS_CODE) { ?>
                </div>

                <body onLoad="oke()">
                    <div id="paymentcc">
                        <table align="center" style="margin-top: 200px;">
                            <tr>
                                <td style="text-align: center;">
                                    <img src="<?php echo get_img_url() ?>loading-bar.gif" alt="loading-bar">
                                    <div id="subtitle">
                                        We are processing your transaction, You will be redirected to our payment gateway system
                                        <div style="color: red; font-weight: bold;">DO NOT CLOSE THIS WINDOW</div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <form action="<?php echo PGS_URL; ?>" method="POST" name="theForm" id="theForm">
                        <!-- saat production diganti jadi https://www.payment2go.com:443/payment/PaymentWindow.jsp-->
                        <input type="hidden" name="GEN_HASH" value="Yes" />
                        <input type="hidden" name="LANG" value="en" />
                        <input type="hidden" name="MERCHANTID" value="<?php echo PGS_MERCHANT_ID; ?>" />
                        <input type="hidden" name="MERCHANT_TRANID" value="<?php echo $data_ord[0][0]->order_trans_no; ?>" />
                        <input type="hidden" name="PAYMENT_METHOD" value="<?php echo PAYMENT_METHOD; ?>" />
                        <input type="hidden" name="PYMT_IND" value="" /> 
                        <input type="hidden" name="PYMT_CRITERIA" value="" />
                        <input type="hidden" name="CURRENCYCODE" value="IDR" /> 
                        <input type="hidden" name="AMOUNT" value="<?php echo $data_ord[0][0]->total_payment . ".00" ?>" />
                        <input type="hidden" name="DESCRIPTION" value="" /> <!-- disesuaikan dengan database merchant -->
                        <input type="hidden" name="CUSTNAME" value="<?php echo $data_ord[0][0]->member_name; ?>" />
                        <input type="hidden" name="CUSTEMAIL" value="<?php echo $data_ord[0][0]->email; ?>" />
                        <input type="hidden" name="PHONE_NO" value="" />
                        <input type="hidden" name="RETURN_URL" value="<?php echo base_url() . "member/payment/proccess/" . $data_ord[0][0]->order_no; ?>" />
                        <input type="hidden" name="handshake_url" value=""/>
                        <input type="hidden" name="handshake_param" value="" /> 
                        <input type="hidden" name="TXN_PASSWORD" value="<?php echo PGS_TXN_PASSWORD ?>" />
                        <input type="hidden" name="SIGNATURE" value="<?php echo $data_sign; ?>" />
                        <input type="hidden" name="BILLING_ADDRESS" value="" />
                        <input type="hidden" name="BILLING_ADDRESS_CITY" value="" />
                        <input type="hidden" name="BILLING_ADDRESS_REGION" value="" />
                        <input type="hidden" name="BILLING_ADDRESS_STATE" value="" />
                        <input type="hidden" name="BILLING_ADDRESS_POSCODE" value="" />
                        <input type="hidden" name="BILLING_ADDRESS_COUNTRY_CODE" value="ID" />
                        <input type="hidden" name="RECEIVER_NAME_FOR_SHIPPING" value="" />
                        <input type="hidden" name="SHIPPING_ADDRESS" value="" />
                        <input type="hidden" name="SHIPPING_ADDRESS_CITY" value="" />
                        <input type="hidden" name="SHIPPING_ADDRESS_REGION" value="" />
                        <input type="hidden" name="SHIPPING_ADDRESS_STATE" value="" />
                        <input type="hidden" name="SHIPPING_ADDRESS_POSCODE" value="" />
                        <input type="hidden" name="SHIPPING_ADDRESS_COUNTRY_CODE" value="" />
                        <input type="hidden" name="SHIPPINGCOST" value="" />
                        <input type="hidden" name="DOMICILE_ADDRESS" value="" />
                        <input type="hidden" name="DOMICILE_ADDRESS_CITY" value="" />
                        <input type="hidden" name="DOMICILE_ADDRESS_REGION" value="" /> 
                        <input type="hidden" name="DOMICILE_ADDRESS_STATE" value="" /> 
                        <input type="hidden" name="DOMICILE_ADDRESS_POSCODE" value="" />
                        <input type="hidden" name="DOMICILE_ADDRESS_COUNTRY_CODE" value="" />
                        <input type="hidden" name="DOMICILE_PHONE_NO" value="" />
                        <input type="hidden" name="MREF1" value="" />
                        <input type="hidden" name="MREF2" value="" />
                        <input type="hidden" name="MREF3" value="" />
                        <input type="hidden" name="MREF4" value="" />
                        <input type="hidden" name="MREF5" value="" />
                        <input type="hidden" name="MREF6" value="" />
                        <input type="hidden" name="MREF7" value="" />
                        <input type="hidden" name="MREF8" value="" />
                        <input type="hidden" name="MREF9" value="" />
                        <input type="hidden" name="MREF10" value="" />
                        <input type="hidden" name="MPARAM1" value="" />
                        <input type="hidden" name="MPARAM2" value="" />
                        <input type="hidden" name="CUSTOMER_REF" value="" />
                        <input type="hidden" name="FRISK1" value="" />
                        <input type="hidden" name="FRISK2" value="" />

                    </form>
                </body>
                <script language="JavaScript">
                                                            function oke() {
                                                                var payment_info = [];
                                                                payment_info = $('#theForm').serializeArray();
                                                                payment_info.push({name: "type", value: "<?php echo TASK_UPDATE_CC_LOG ?>"});
                                                                form = document.theForm;
                                                                $.ajax({
                                                                    url: "<?php echo base_url() . "member/payment/" . $data_ord[0][0]->order_no ?>",
                                                                    type: "post",
                                                                    data: payment_info,
                                                                    success: function(data) {
                                                                        if (isSessionExpired(data)) {
                                                                            response_object = json_decode(data);
                                                                            url = response_object.url;
                                                                            location.href = url;
                                                                        } else {
                                                                            document.forms['theForm'].submit();
                                                                        }
                                                                    },
                                                                    error: function(request, error) {
                                                                        alert(error_response(request.status));
                                                                    }
                                                                });
                                                            }
                </script>
            <?php } else if (($data_ord[0][0]->payment_code == PAYMENT_TYPE_DOCU_ATM || $data_ord[0][0]->payment_code == PAYMENT_TYPE_DOCU_ALFAMART) && $data_ord[0][0]->payment_status == PAYMENT_UNPAID_STATUS_CODE) { ?>
                <body onLoad="load_form()">
                    <div id="payment_docu">
                        <table align="center" style="margin-top: 200px;">
                            <tr>
                                <td style="text-align: center;">
                                    <img src="<?php echo get_img_url() ?>loading-bar.gif" alt="loading-bar">
                                    <div id="subtitle">
                                        We are processing your transaction, You will be redirected to our payment gateway system
                                        <div style="color: red; font-weight: bold;">DO NOT CLOSE THIS WINDOW</div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <form method="POST" action="<?php echo URL_PAYMENT_REQUEST ?>" accept-charset="UTF-8" id="theForm">
                        <input type="hidden" name="MALLID" value="<?php echo MALLID; ?>">
                        <input type="hidden" name="CHAINMERCHANT" value="<?php echo CHAIN_MERCHANT ?>">
                        <input type="hidden" name="AMOUNT" value="<?php echo $data_ord[0][0]->total_payment . ".00" ?>">
                        <input type="hidden" name="PURCHASEAMOUNT" value="<?php echo $data_ord[0][0]->total_payment . ".00" ?>">
                        <input type="hidden" name="TRANSIDMERCHANT" value="<?php echo $data_ord[0][0]->order_trans_no; ?>">
                        <input type="hidden" name="WORDS" value="<?php echo $data_sign; ?>">
                        <input type="hidden" name="REQUESTDATETIME" value="<?php echo date('YmdHis'); ?>">
                        <input type="hidden" name="CURRENCY" value="<?php echo DOCU_CURRENCY; ?>">
                        <input type="hidden" name="PURCHASECURRENCY" value="<?php echo DOCU_CURRENCY ?>">
                        <input type="hidden" name="SESSIONID" value="<?php echo session_id(); ?>">
                        <input type="hidden" name="NAME" value="<?php echo $data_ord[0][0]->member_name ?>">
                        <input type="hidden" name="EMAIL" type="text" value="<?php echo $data_ord[0][0]->email ?>">
                        <input type="hidden" name="BASKET" type="text" value="<?php echo "Total," . $data_ord[0][0]->total_payment . ".00,1," . $data_ord[0][0]->total_payment . ".00;" ?>">
                        <input type="hidden" name="PAYMENTCHANNEL" value="<?php echo $data_ord[0][0]->payment_code == PAYMENT_TYPE_DOCU_ATM ? ATM_PERMATA_VA_LITE : ALFAMART; ?>">
                    </form>
                    <script language="JavaScript">
                            function load_form() {
                                var payment_info = [];
                                payment_info = $('#theForm').serializeArray();
                                payment_info.push({name: "type", value: "<?php echo TASK_UPDATE_CC_LOG ?>"});
                                form = document.theForm;
                                $.ajax({
                                    url: "<?php echo base_url() . "member/payment/" . $data_ord[0][0]->order_no ?>",
                                    type: "post",
                                    data: payment_info,
                                    success: function(data) {
                                        if (isSessionExpired(data)) {
                                            response_object = json_decode(data);
                                            url = response_object.url;
                                            location.href = url;
                                        } else {
                                            document.forms['theForm'].submit();
                                        }
                                    },
                                    error: function(request, error) {
                                        alert(error_response(request.status));
                                    }
                                });
                                                            }
                    </script>
                <?php } else { ?>
            </div>
        <?php } ?>      
    </div>                 
</div>
</div>
</div>  

