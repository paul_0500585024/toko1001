<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('home/template/segment_main/html_head'); ?>
    </head>
    <body>  
        <!-- Fixed navbar -->
        <?php $this->load->view('home/template/segment_main_cart/menu'); ?>

        <!-- Lantai promo -->
        <?php echo $_content_;?>
        <?php $this->load->view('home/template/segment_main_cart/body_footer'); ?>
        <?php $this->load->view('home/template/segment_main/html_script'); ?>
    </body>
</html>