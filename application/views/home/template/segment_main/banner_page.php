<script type="text/javascript">
    (function () {
        var po = document.createElement('script');
        po.type = 'text/javascript';
        po.async = true;
        po.src = 'https://apis.google.com/js/client.js?onload=onLoadCallback';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(po, s);
    })();

    (function (d) {
        var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement('script');
        js.id = id;
        js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";
        ref.parentNode.insertBefore(js, ref);
    }(document));
</script>

<div class="modal fade" id="login_register" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">x</button>
                        <ul class="nav nav-tabs sidebar-tabs" id="sidebar" role="tablist">
                            <li id="tablogin" role="presentation"><a href="#login" role="tab" data-toggle="tab">Login</a></li>
                            <li id="tabregister" role="presentation"><a href="#daftar" role="tab" data-toggle="tab">Daftar</a></li>
                        </ul>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane deskripsi" id="login">
                            <div class="login-logo">
                                <b>Login</b>&nbsp;Member
                            </div>
                            <div class='col-xs-12 col-lg-6 col-md-6 col-sm-6'>
                                <?php if (!isset($_SESSION[SESSION_MEMBER_CSRF_TOKEN])) { ?>
                                    <form id="frmMain" method = "post" action= "<?php echo get_base_url() ?>member/login/sign_in">
                                        <div class="form-group has-feedback">
                                            <input name="type" type="hidden" value="standard_login"/>
                                            <input type="text" name="email" class="form-control" placeholder="Email" validate="email[]" />
                                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                        </div>
                                        <div class="form-group has-feedback">
                                            <input type="password" name="password" class="form-control" placeholder="Password"/>
                                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-lg-7">
                                                <button type="submit" class="btn  btn-front btn-block btn-flat"><i class="fa fa-sign-in"></i>&nbsp;Login</button>
                                            </div>
                                            <div class="col-xs-12 col-lg-5 text-right" >
                                                <a href="<?php echo base_url() ?>member/forgot_password" style='font-size:11px;margin-top:5px;'>Lupa Password </a><br><br>
                                            </div>
                                        </div>
                                        <input type="hidden" name="current_url" value="<?php echo current_url_with_query_string() ?>" />
                                    </form>
                                <?php } ?>
                            </div>
                            <div class='col-xs-12 col-lg-6 col-md-6 col-sm-6'>
                                <div class="form-group has-feedback">
                                    <button id="facebook" class="btn btn-flat btn-front btn-block btn-facebook" 
                                            onclick="fb_login();" type="submit">
                                        <i class="fa fa-facebook"></i>&nbsp; Masuk dengan Facebook
                                    </button>
                                </div>
                                <div class="form-group has-feedback">
                                    <button id="facebook" class="btn btn-flat btn-front btn-block btn-facebook" 
                                            style="background-color: #D73D32" onclick="gp_login();" type="submit">
                                        <i class="fa fa-google-plus""></i>&nbsp; Masuk dengan Google+
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane deskripsi" id="daftar">
                            <div class="register-logo">
                                <b>Daftar</b>&nbsp;Member
                            </div>
                            <p class="col-lg-12" id="content-message"></p>
                            <div class='col-lg-6 col-sm-12'>
                                <form id="frmSignupMember">
                                    <div class="form-group has-feedback">
                                        <input validate="email[]" id="member_email" name="member_email" type="text" class="form-control" placeholder="Email" />
                                        <span id="email_span_member" class="glyphicon glyphicon-envelope form-control-feedback" ></span>

                                    </div>
                                    <div class="form-group has-feedback">
                                        <input id="member_name" validate="required[]" name="member_name" type="text" class="form-control" placeholder="Nama Lengkap" />
                                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <input id="member_birthday" date_type="date" validate="required[]" name="member_birthday" type="text" class="form-control"  placeholder="Tanggal Lahir" readonly style="background-color: #fff;"/>
                                        <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-sm-6">
                                            <div class="form-group">
                                                <select id="gender_member" class="form-control" name="gender_member" validate="required[]">
                                                    <option value="" selected>Jenis Kelamin</option>
                                                    <option value="M">Pria</option>
                                                    <option value="F">Wanita</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-sm-6">
                                            <div class="form-group has-feedback">
                                                <input id="member_phone" validate="required[]"  name="member_phone" type="text" class="form-control" placeholder="No. Telepon" data-mask data-inputmask="'mask': ['999 999 999 999 999']"/>
                                                <span class="glyphicon glyphicon-earphone form-control-feedback"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-sm-6">
                                            <div class="form-group has-feedback">
                                                <input id="password1" validate="password[]" name="password1" type="password" class="form-control" placeholder="Password" />
                                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-sm-6">
                                            <div class="form-group has-feedback">
                                                <input id="password2" validate="password[]" name="password2" type="password" class="form-control" placeholder="Ulangi password" />
                                                <span class="glyphicon glyphicon-edit form-control-feedback"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-sm-6"  id="captcha_img_member" >   
                                        </div>
                                        <div class="col-lg-6 col-sm-6">
                                            <div class="form-group">
                                                <input id="captcha_member" validate="required[]" name="captcha_member" type="text" class="form-control" placeholder="Kode Captcha" maxlength="5"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class='col-lg-12 col-sm-12'>
                                            <div class="checkbox icheck">
                                                <label> <div class="col-lg-1 col-xs-1 col-sm-1 col-md-1">
                                                        <input id="member_requirement"  name="member_requirement" type="checkbox" class="minimal" value="1"> </div>
                                                    <div class="col-lg-11 col-xs-10 col-sm-11 col-md-11">
                                                        Saya sudah membaca dan menyetujui
                                                        <a href="<?php echo base_url(); ?>info/syarat-dan-ketentuan"
                                                           target="_blank">Syarat dan Ketentuan</a>, serta
                                                        <a href="<?php echo base_url(); ?>info/kebijakan-privasi"
                                                           target="_blank">Kebijakan Privasi </a> dari Toko1001.
                                                    </div>
                                                </label>
                                            </div>
                                            <button id="btnmember" type="submit" class="btn btn-front btn-block btn-flat"><span class="fa fa-user"></span> Daftar &nbsp;</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class='col-lg-6 col-sm-12'>
                                <form id="frmSignupMember">
                                    <div class="form-group has-feedback">
                                        <div class="row hidden-sm hidden-xs hidden-md">
                                            <img src="<?php echo get_img_url(); ?>maribergabung_420X335.jpg" alt="mari bergabung" style=" width: 420px; height:335px;">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--MODAL FOR CART-->
<div class="modal fade" id="add_to_cart" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <div class="modal-title"><b>KERANJANG BELANJA</b></div>
            </div>
            <!-- TABLE CART FOR LG,SM,MD-->
            <div class="modal-body table-responsive hidden-xs"  >
                <div style="height:350px; table-layout:fixed; overflow-y: auto;">
                    <table id ="table_product" class="table table-bordered">
                        <tr class="success" id="tr-cart">
                            <th class="field-label" colspan="2" id="tr-cart">Produk</th>
                            <th class="field-label" id="tr-cart">Harga (Rp)</th>
                            <th class="field-label" id="tr-cart">Qty</th>
                            <th class="field-label" id="tr-cart">Total (Rp)</th>
                        </tr>
                        <?php if (isset($_SESSION[SESSION_PRODUCT_INFO]) && sizeof($_SESSION[SESSION_PRODUCT_INFO]) > 0) { ?>
                            <?php
                            $total = 0;
                            foreach ($_SESSION[SESSION_PRODUCT_INFO] as $key => $img) {
                                ?>
                                <tr id="<?php echo "row_" . $img[$key]["product_seq"] ?>">
                                    <td class="field-content"><a href ="<?php echo base_url() . url_title($img[$key]["product_name"] . " " . $img[$key]["product_seq"]); ?>"><center><img src= "<?php echo get_file_exists(PRODUCT_UPLOAD_IMAGE . $img[$key]["merchant_seq"] . "/" . S_IMAGE_UPLOAD . $img[$key]["img"]); ?>" alt="<?php echo $img[$key]["img"] ?>" width="100px" ></center></a>
                                    </td>
                                    <td class="field-label" align="left"><strong><?php echo $img[$key]["product_name"] ?></strong><br>
                                        <br>
                                        <table border="0" class="table table-condensed">
                                            <tr>
                                                <td width="20%">Berat</td>
                                                <td width="5%">:</td>
                                                <td><?php echo $img[$key]["product_weight"]; ?> Kg</td>
                                            </tr>
                                            <tr>
                                                <td >Warna</td>
                                                <td>:</td>
                                                <td><?php echo $img[$key]["variant_name"]; ?></td>
                                            </tr>
                                        </table>
                                        <div class="row">
                                            <div class="col-lg-10 col-md-10 col-sm-10 text-left">
                                                <i class = "material-icons" style="font-size:20px; vertical-align:top;">store</i>&nbsp;<a href="<?php echo base_url() . "merchant/" . $img[$key]["merchant_code"]; ?>">&nbsp;<?php echo $img[$key]["merchant_name"]; ?></a> &nbsp;
                                                <i class="fa fa-truck"></i>&nbsp;<?php echo $img[$key]["exp_name"]; ?>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-2 text-left"><div class="row">
                                                    <a href="#"  class='btn btn-danger btn-sm btn-flat' onClick ="<?php echo "delete_product(" . $img[$key]["product_seq"] . ")" ?>"><i class='fa fa-trash'></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="cart" id="sell_price_<?php echo $img[$key]["product_seq"]; ?>" sell_price ="<?php echo $img[$key]["sell_price"]; ?>"><center><?php echo number_format($img[$key]["sell_price"]); ?></center></td>
                                <td class="cart"><center>
                                    <select name="product_qty"  id ="product_qty_<?php echo $img[$key]["product_seq"] ?>" onchange ="dropdown_qty(<?php echo $img[$key]["product_seq"]; ?>, this.id)">
                                        <?php for ($i = 1; $i <= $img[$key]["max_buy"]; $i++) { ?>
                                            <option value ="<?php echo $i ?>" <?php echo $i == $img[$key]["qty"] ? "selected" : "" ?>> <?php echo $i ?></option>
                                        <?php } ?>
                                    </select></center>
                                </td>
                                <td class="cart" id="total_price_<?php echo $img[$key]["product_seq"]; ?>" total_price=<?php echo $img[$key]["sell_price"] * $img[$key]["qty"]; ?>><center><?php echo number_format($img[$key]["sell_price"] * $img[$key]["qty"]); ?></center></td>
                                </tr>
                                <?php
                                $total = $total + $img[$key]["sell_price"] * $img[$key]["qty"];
                            }
                            ?>
                        </table>
                    </div>
                    <div id="product_summary">
                        <div class="row">
                            <div class="col-lg-12 col-md-12  text-right" ><div class="hidden-lg hidden-md"><br></div><h3><strong>Total Belanja : <font color="red">Rp <span id="total_order"><?php echo number_format($total); ?></span></font></strong></h3>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6"><button class="btn btn-front btn-flat" data-dismiss="modal" aria-label="Close">&nbsp;<span class="fa fa-shopping-cart"> </span>&nbsp;Lanjutkan Belanja</button></div>
                            <div class="col-lg-6 col-md-6 col-sm-6 text-right"><a href="<?php echo get_base_url() ?>member/checkout?step=1" class="btn btn-front btn-flat" style="background-color: #0992a6;">Lanjutkan Pembayaran &nbsp;<span class="fa fa-arrow-circle-o-right"> </span>&nbsp;</a></div>
                        </div>
                    </div>
                <?php } else { ?>
                    </table>
                    <center><h3 id ="no_product" class="text-red"> Tidak ada barang dalam keranjang belanja </h3></center>
                </div>
                <div id="product_summary">
                </div>
            <?php } ?>
        </div>

        <!--TABLE CART FOR XS-->
        <div class="modal-body hidden-lg hidden-sm hidden-md">
            <div style="height:450px; overflow: auto; ">
                <table id ="table_product_xs" class="table table-bordered">
                    <tr class="success">
                        <th class="field-label">Produk</th>
                        <th class="field-label">Keterangan</th>
                    </tr>
                    <?php if (isset($_SESSION[SESSION_PRODUCT_INFO]) && sizeof($_SESSION[SESSION_PRODUCT_INFO]) > 0) { ?>
                        <?php
                        foreach ($_SESSION[SESSION_PRODUCT_INFO] as $key => $img) {
                            ?>
                            <tr id="<?php echo "row_" . $img[$key]["product_seq"]; ?>_xs">
                                <td>
                                    <a href ="<?php echo base_url() . url_title($img[$key]["product_name"] . " " . $img[$key]["product_seq"]); ?>"><center><img src= "<?php echo get_file_exists(PRODUCT_UPLOAD_IMAGE . $img[$key]["merchant_seq"] . "/" . S_IMAGE_UPLOAD . $img[$key]["img"]); ?>" alt="<?php echo $img[$key]["img"] ?>" width="100px"></center></a>
                                </td>
                                <td><strong><?php echo $img[$key]["product_name"] ?></strong>
                                    <br><br>
                                    <table border="0" class="table table-condensed">
                                        <tr>
                                            <th>Berat</th>
                                            <td>:</td>
                                            <td><?php echo $img[$key]["product_weight"]; ?> Kg</td>
                                        </tr>
                                        <tr>
                                            <th>Warna</th>
                                            <td>:</td>
                                            <td><?php echo $img[$key]["variant_name"]; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Qty</th>
                                            <td>:</td>
                                            <td>
                                                <select name="product_qty"  id ="product_qty_<?php echo $img[$key]["product_seq"] ?>_xs" onchange ="dropdown_qty(<?php echo $img[$key]["product_seq"]; ?>, this.id)">
                                                    <?php for ($i = 1; $i <= $img[$key]["max_buy"]; $i++) { ?>
                                                        <option value ="<?php echo $i ?>" <?php echo $i == $img[$key]["qty"] ? "selected" : "" ?>> <?php echo $i ?></option>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Harga</th>
                                            <td>:</td>
                                            <td id="sell_price_<?php echo $img[$key]["product_seq"]; ?>" sell_price ="<?php echo $img[$key]["sell_price"]; ?>">Rp. <?php echo number_format($img[$key]["sell_price"]); ?></td>
                                        </tr>
                                        <tr>
                                            <th>Total</th>
                                            <td>:</td>
                                            <td id="total_price_<?php echo $img[$key]["product_seq"]; ?>_xs">Rp. <?php echo number_format($img[$key]["sell_price"] * $img[$key]["qty"]); ?></td>
                                        </tr>
                                    </table>
                                    <div class="row">
                                        <div class="col-xs-9"><div class="row">
                                                <div class="col-xs-12"><i class = "material-icons" style="font-size:20px; vertical-align:top;">store</i>&nbsp;<a href="<?php echo base_url() . "merchant/" . $img[$key]["merchant_code"]; ?>">&nbsp;<?php echo $img[$key]["merchant_name"]; ?></a> &nbsp;</div>
                                                <div class="col-xs-12"><i class="fa fa-truck"></i>&nbsp;<?php echo $img[$key]["exp_name"]; ?></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-3 ">
                                            <div class="row">
                                                <a href="#" onClick ="<?php echo "delete_product(" . $img[$key]["product_seq"] . ")" ?>" class='btn btn-danger btn-sm btn-flat'><i class='fa fa-trash'></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
                <div id="product_summary_xs">
                    <div class="row">
                        <div class="col-lg-12"><div class="well"><h3 style="text-align: center;"><b>Total Belanja  <br><font color="red">Rp <span id="total_order_xs"><?php echo number_format($total); ?></span></font></b></h3></div></div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12"><button class="btn btn-front btn-flat btn-block" data-dismiss="modal" aria-label="Close">&nbsp;<span class="fa fa-shopping-cart"> </span>&nbsp;Lanjutkan Belanja</button></div> <br>
                        <div class="col-lg-12"><a href="<?php echo get_base_url() ?>member/checkout?step=1" class="btn btn-front btn-flat btn-block" style="background-color: #0992a6;">Lanjutkan Pembayaran &nbsp;<span class="fa fa-arrow-circle-o-right"> </span>&nbsp;</a></div>
                    </div>
                </div>
            <?php } else { ?>
                </table>
                <center><h3 id ="no_product_xs" class="text-red"> Tidak ada barang dalam keranjang belanja </h3></center>
            </div>
            <div id="product_summary_xs">
            </div>
        <?php } ?>
    </div>
</div>
</div>
</div>
<!--MODAL FOR REGISTER MERCHANT-->
<div class="modal fade" id="modal_register_merchant" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">x</button><br>
            </div>
            <div class="modal-body">
                <p class="col-lg-12" id="content-message_merchant"></p>
                <div class="login-logo">
                    <b>Daftar</b>&nbsp;Merchant
                </div>
                <div class="row">
                    <div class='col-xs-12 col-lg-6 col-md-12 col-sm-12'>
                        <form id="frmSignUpMerchant">
                            <div class="form-group has-feedback">
                                <input validate="email[]"  type="text" id="email_merchant" class="form-control" placeholder="Email" name="email_merchant" />
                                <div id="usr_verify" class="verify"></div>
                                <span id="email_span" class="glyphicon glyphicon-envelope form-control-feedback"></span>
                            </div>
                            <div class="form-group has-feedback">
                                <input id="name_merchant" validate="required[]"type="text" class="form-control" placeholder="Nama Merchant" name="merchant_name" />
                                <span class="material-icons form-control-feedback" style="margin-top:5px;">store</span>    
                            </div>
                            <div class="form-group has-feedback">
                                <input id="phone_merchant" validate="required[]" type="text" class="form-control" placeholder="No. Telepon" name="phone_merchant" data-mask data-inputmask="'mask': ['999 999 999 999 999']"/>
                                <span class="glyphicon glyphicon-phone form-control-feedback"></span>
                            </div>
                            <div class="form-group">
                                <textarea id="merchant_address" validate="required[]"  class="form-control" name="merchant_address" placeholder="Alamat Merchant" rows="3" style="overflow:auto;resize:none"></textarea>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-sm-6 col-md-6">
                                    <div class="form-group" id="captcha_img_merchant">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6 col-md-6 ">
                                    <div class="form-group">
                                        <input validate="required[]"  id ="code_captcha_merchant" type="text" name="captcha_merchant" class="form-control" placeholder="Kode Captcha" />
                                        <div id="captcha_verify" class="verify"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-lg-12">
                                    <button id="btnMSignUp" type="submit" name="submit" class="btn  btn-front btn-block btn-flat" ><i class="fa fa-user"></i> Daftar </button>
                                </div>
                            </div>

                        </form>
                    </div>
                    <div class='hidden-sm hidden-xs hidden-md col-xs-12 col-lg-6 col-md-6 col-sm-6 '>
                        <img src="<?php echo get_img_url(); ?>MERCHANT_420X349.jpg" alt="merchant" class="hidden-xs hidden-sm" style="margin-bottom: 5px;">
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script text="javascirpt">

                                                    $("[data-mask]").inputmask();
                                                    function delete_product(product_seq) {
                                                        $.ajax({
                                                            url: "<?php echo base_url("member/main_page"); ?>",
                                                            type: "post",
                                                            data: {
                                                                "type": "<?php echo TASK_TYPE_DELETE ?>",
                                                                "product_seq": product_seq
                                                            },
                                                            success: function (data) {
                                                                if (isSessionExpired(data)) {
                                                                    response_object = json_decode(data);
                                                                    url = response_object.url;
                                                                    location.href = url;
                                                                } else {
                                                                    $('#row_' + product_seq).remove();
                                                                    $('#row_' + product_seq + "_xs").remove();
                                                                    calculate_total();
                                                                }
                                                            },
                                                            error: function (request, error) {
                                                                alert(error_response(request.status));
                                                            }
                                                        });
                                                    }

                                                    function calculate_total() {
                                                        var grand_total = 0;
                                                        var qty_product = 0;
                                                        var content_no_product = "Tidak ada barang dalam keranjang belanja";
                                                        $('td').each(function () {
                                                            var total = $(this).attr("total_price");
                                                            if (total != undefined) {
                                                                grand_total += parseInt(total);
                                                                qty_product++;
                                                            }
                                                        });

                                                        if (qty_product == 0) {
                                                            $('#no_product_xs').text(content_no_product);
                                                            $('#no_product').text(content_no_product);
                                                            $('#product_summary').empty();
                                                            $('#product_summary_xs').empty();
                                                            qty_product = "";
                                                        }

                                                        $('#total_order').text(format(grand_total));
                                                        $('#total_order_xs').text(format(grand_total));
                                                        $('#cart_qty_md').text(qty_product);
                                                        $('#cart_qty_xs').text(qty_product);
                                                        $('#cart_qty_sm').text(qty_product);

                                                    }

                                                    function format(num) {
                                                        var n = num.toString(), p = n.indexOf('.');
                                                        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
                                                            return p < 0 || i < p ? ($0 + ',') : $0;
                                                        });
                                                    }

                                                    function dropdown_qty(product_seq, id) {
                                                        var sell_price = $('#sell_price_' + product_seq).attr("sell_price");
                                                        var qty = $('#' + id).val();

                                                        if (id.substr(id.length - 2) == 'xs') {
                                                            $('#' + id.substr(0, id.length - 3)).val(qty);
                                                        } else {
                                                            $('#' + id + '_xs').val(qty);
                                                        }

                                                        var total_price = parseInt(sell_price) * parseInt(qty);
                                                        $.ajax({
                                                            url: "<?php echo base_url("member/main_page"); ?>",
                                                            type: "POST",
                                                            data: {
                                                                "type": "<?php echo TASK_TYPE_ADD_QTY ?>",
                                                                "product_seq": product_seq,
                                                                "qty_product": qty
                                                            },
                                                            success: function (response) {
                                                                if (isSessionExpired(response)) {
                                                                    response_object = json_decode(response);
                                                                    url = response_object.url;
                                                                    location.href = url;
                                                                } else {
                                                                    $('#total_price_' + product_seq).attr("total_price", total_price);
                                                                    $('#total_price_' + product_seq).text(format(total_price));
                                                                    $('#total_price_' + product_seq + '_xs').text("Rp. " + format(total_price));
                                                                    calculate_total();
                                                                }
                                                            },
                                                            error: function (request, error) {
                                                                alert(error_response(request.status));
                                                            }
                                                        });
                                                    }

//                                        Google Login
                                                    function gp_login()
                                                    {
                                                        var myParams = {
                                                            'clientid': '1005160495089-fn8j24mmkdt1aemgcto51dv55sohq0he.apps.googleusercontent.com',
                                                            'cookiepolicy': 'single_host_origin',
                                                            'callback': 'loginCallback',
                                                            'approvalprompt': 'force',
                                                            'scope': 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email'
                                                        };
                                                        gapi.auth.signIn(myParams);
                                                    }

                                                    function loginCallback(result)
                                                    {
                                                        if (result['status']['signed_in'])
                                                        {
                                                            var request = gapi.client.plus.people.get(
                                                                    {
                                                                        'userId': 'me'
                                                                    });
                                                            request.execute(function (resp)
                                                            {
                                                                var email = '';
                                                                if (resp['emails'])
                                                                {
                                                                    for (i = 0; i < resp['emails'].length; i++)
                                                                    {
                                                                        if (resp['emails'][i]['type'] == 'account')
                                                                        {
                                                                            email = resp['emails'][i]['value'];
                                                                        }
                                                                    }
                                                                }

                                                                var base_url = '<?php echo base_url(); ?>'
                                                                $.ajax({
                                                                    url: "<?php echo base_url() ?>member/login_fb",
                                                                    type: "post",
                                                                    data: {
                                                                        "name": resp['displayName'],
                                                                        "email": email,
                                                                        "gender": resp['gender'],
                                                                        "type": "google_login"
                                                                    },
                                                                    success: function (data) {
                                                                        window.location = "<?php echo base_url(); ?>";
                                                                    },
                                                                    error: function (request, error) {
                                                                        alert(error_response(request.status));
                                                                    }
                                                                });
                                                            });

                                                        }

                                                    }
                                                    function onLoadCallback()
                                                    {
                                                        gapi.client.load('plus', 'v1', function () {});
                                                    }

//                                        Facebook Login
                                                    window.fbAsyncInit = function () {
                                                        FB.init({
                                                            appId: '672193332925149',
                                                            status: true,
                                                            cookie: true,
                                                            xfbml: true
                                                        });

                                                        FB.Event.subscribe('auth.authResponseChange', function (response)
                                                        {
                                                            if (response.status === 'connected')
                                                            {
                                                                document.getElementById("message").innerHTML += "<br>Connected to Facebook";
                                                            } else if (response.status === 'not_authorized')
                                                            {
                                                                document.getElementById("message").innerHTML += "<br>Failed to Connect";
                                                            } else
                                                            {
                                                                document.getElementById("message").innerHTML += "<br>Logged Out";
                                                            }
                                                        });

                                                    };

                                                    function fb_login() {
                                                        FB.login(function (response) {
                                                            if (response.authResponse)
                                                            {
                                                                _i();
                                                            } else
                                                            {
                                                                console.log('Batal masuk Facebook, tidak ada otoritas !');
                                                            }
                                                        }, {scope: 'email'});

                                                    }

                                                    function _i() {
                                                        FB.api('/me?fields=name,email,gender,birthday', function (data) {

                                                            var base_url = '<?php echo base_url(); ?>'

                                                            $.ajax({
                                                                url: "<?php echo base_url() ?>member/login_fb",
                                                                type: "post",
                                                                data: {
                                                                    "name": data.name,
                                                                    "email": data.email,
                                                                    "gender": data.gender,
                                                                    "birthday": data.birthday,
                                                                    "type": "facebook_login"
                                                                },
                                                                success: function (data) {
                                                                    window.location = "<?php echo base_url(); ?>";
                                                                },
                                                                error: function (request, error) {
                                                                    alert(error_response(request.status));
                                                                }
                                                            });
                                                        })
                                                    }
</script>
