<?php
$product_category_seq = isset($product_category_seq) ? $product_category_seq : '';
parse_str($this->input->server('QUERY_STRING'), $query_string);
$tree_category = isset($tree_category) ? $tree_category : array();
$url_form_search = isset($is_category) ? current_url() : (base_url() . ALL_CATEGORY);
$signout_param = http_build_query(array(CONTINUE_URL => current_url_with_query_string()));
?>

<script type="text/javascript">
    (function(d) {
        var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement('script');
        js.id = id;
        js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";
        ref.parentNode.insertBefore(js, ref);
    }(document));
</script>

<nav class="navbar navbar-inverse navbar-fixed-top top-menu" role="navigation" id="topnavbar" style="margin-bottom:100px;">
    <div style='background-color:#042e3a'>
        <div class="container">
            <div class=" col-sm-6 col-md-6 col-lg-6 banner_left hidden-xs" style='margin-top:10px; margin-bottom: 10px;'>
                <img src="<?php echo get_img_url(); ?>mobil.png" alt="mobil" height="16px">&nbsp;&nbsp;Pengiriman
                Gratis ke Area Jabodetabek&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo get_img_url(); ?>tlp.png" alt="telepon" height="16px">&nbsp;&nbsp; +6221-6514224/5
            </div>
            <div class="col-xs-12 banner_left_xs hidden-sm hidden-lg hidden-md " style='margin-top:3px; margin-bottom: 3px;'>
                <img src="<?php echo get_img_url(); ?>mobil.png" alt="mobil" height="16px">&nbsp;&nbsp;Pengiriman
                Gratis ke Area Jabodetabek&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo get_img_url(); ?>tlp.png" alt="telepon" height="16px">+6221-6514224/5
            </div>
            <div class="hidden-xs col-sm-6 col-md-6 col-lg-6 banner_right">
            </div>
        </div>
    </div>
    <div class="container">
        <div class="navbar-header">
            <button type="button" id="showLeft"
                    class="navbar-toggle btn-category-top pull-left"
                    data-toggle="collapse" data-target="#category-top">
                <span class="sr-only">Toggle navigation</span> <span
                    class="icon-bar"></span> <span class="icon-bar"></span> <span
                    class="icon-bar"></span>
            </button>
            <div class='hidden-sm hidden-md hidden-lg'>
                <?php if (isset($_SESSION[SESSION_MEMBER_CSRF_TOKEN])) { ?>
                    <button type="button" id="user_profile"
                            class="navbar-toggle btn-category-top"
                            data-toggle="collapse" data-target="#category-top">
                        <span class="fa fa-user white"></span>
                    </button>
                    <?php /*
                      <button class="navbar-toggle" type="text" data-toggle="dropdown">
                      <span class="fa fa-user white"></span></button>
                      <ul class="dropdown-menu pull-right">
                      <li><a href="<?php echo get_base_url() ?>member"><span class="fa fa-user"></span>Profile</a></li>
                      <li><a href="<?php echo get_base_url() ?>member/login/sign_out"><span class="fa fa-sign-out"></span>Sign Out</a></li>
                      </ul>
                      ` */ ?>
                <?php } else { ?>
                    <a href="<?php echo base_url() . "member/login" ?>">
                        <button type="button" class="navbar-toggle"  data-id="1">
                            <span class="fa fa-user white"></span>
                        </button>
                    </a>
                <?php } ?>

                <button type="button" class="bag_box_xs box_cart_xs navbar-toggle" data-toggle="modal"  data-target="#add_to_cart"><span id ="cart_qty_xs" class="fa fa-shopping-cart white"><?php echo isset($_SESSION[SESSION_PRODUCT_INFO]) && sizeof($_SESSION[SESSION_PRODUCT_INFO]) != 0 ? sizeof($_SESSION[SESSION_PRODUCT_INFO]) : ""; ?></span>&nbsp;<font color="white"></font></button>

            </div>
            <a class="navbar-brand logo hidden-xs" href="<?php echo base_url() ?>"> <img
                    class="" src="<?php echo get_img_url(); ?>522X120_LOGO_PUTIH.png" alt="LOGO PUTIH">

            </a>
            <a class="navbar-brand logo hidden-lg hidden-md hidden-sm" href="<?php echo base_url() ?>"> <img
                    class="" src="<?php echo get_img_url(); ?>icon/LOGOPOLOSBARU.png" alt="LOGO BARU" style="width:110px; height: 30px; margin-top:-5px;">

            </a>
        </div>

        <!--        Form pencarian untuk tampilan lg & md-->
        <div class="hidden-xs hidden-sm hidden-md">
            <div class="navbar-collapse collapse  ">
                <div class="col-lg-7">
                    <form class="navbar-form navbar-left search_bar input-group"  role="search" action="<?php echo $url_form_search ?>" method="get">
                        <input type="text" class="form-control form-group" placeholder="Cari produk, kategori, atau merk" id="dropdown-kategori" name="<?php echo SEARCH ?>" style="width:100%;">
                        <div class="input-group-btn" >
                            <select id="maxOption2_lg" class="selectpicker form-group" data-max-options="1" data-style="btn-inverse" >
                                <?php echo get_category_option($tree_category, 0, $product_category_seq); ?>
                            </select>
                            <button type="submit" class="btn btn-info btn-flat" style="background-color: #0992a6; border-color:#0992a6;"><i class="glyphicon glyphicon-search"></i></button>
                        </div>
                    </form>

                    <div class="hidden-xs hidden-md hidden-lg">
                        <button type="button" class="navbar-toggle"  data-toggle="modal" data-target="#login_register"
                                id="loginregister" data-id="1">
                            <span class="glyphicon glyphicon-user white"></span>
                        </button>
                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                                data-target="#cart-top">
                            <a href="#" data-toggle="modal" data-target="#add_to_cart"><span class="glyphicon glyphicon-shopping-cart white"></span></a>
                        </button>
                    </div>
                </div>
                <div class="col-md-1 col-lg-1" >
                    <div class="bag_box box_cart" >
                        <a href="#" data-toggle="modal" data-target="#add_to_cart">
                            <img
                                src="<?php echo get_img_url(); ?>cart.png" class="invert" height="38px" width="40px"><span
                                id ="cart_qty_md" class="bag_number"><?php echo isset($_SESSION[SESSION_PRODUCT_INFO]) && sizeof($_SESSION[SESSION_PRODUCT_INFO]) != 0 ? sizeof($_SESSION[SESSION_PRODUCT_INFO]) : ""; ?></span></a>
                    </div>
                </div>
                <div class="col-md-2  col-lg-2 " style="margin-left: -45px; margin-top: 10px; " align="right">
                    <center>
                        <?php // if ($member_session != Null) { ?>
                        <?php if (isset($_SESSION[SESSION_MEMBER_CSRF_TOKEN])) { ?>
                            <div class="container col-md-2">
                                <div class="dropdown">
                                    <button class="btn btn-front" style="width:167px;" type="text" data-toggle="dropdown">
                                        <span class="pull-right"><i class='fa fa-caret-down'></i></span>
                                        <span class='pull-right' style="margin-right:10px; width: 120px; overflow: hidden; text-overflow: ellipsis;">Halo, <?php echo ucwords($_SESSION[SESSION_MEMBER_UNAME]); ?></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo get_base_url() ?>member"><span class="fa fa-user"></span>Profile</a></li>
                                        <li><a href="<?php echo get_base_url() ?>member/wishlist"><span class="fa fa-heart"></span>Wishlist</a></li>
                                        <li><a href="<?php echo get_base_url() ?>member/login/sign_out?<?php echo $signout_param ?>" id="Logout"><span class="fa fa-sign-out"></span>Log Out</a></li>
                                    </ul>
                                </div>
                            </div>
                        <?php } else { ?>
                            <a
                                href="#" data-toggle="modal" data-target="#login_register"
                                id="loginregister" class="btn btn-flat btn-front" data-id="0" style="width:47%;"><i class="fa fa-user"></i>
                                Daftar</a>
                            <a
                                href="#" data-toggle="modal" class="btn btn-flat btn-front"
                                id="loginregister" data-target="#login_register" data-id="1" style="width:47%;" ><i class="fa fa-sign-in"></i>
                                Login</a>
                        <?php } ?>
                    </center>
                </div>


                <nav id="primary_nav_wrap">
                    <?php $this->load->view('home/template/segment_main/main_menu'); ?>
                </nav>

            </div>
        </div>

        <!--        Form pencarian untuk tampilan sm -->
        <?php /*        <div class="hidden-lg  hidden-xs" style="margin-top:25px; margin-right: -50px; margin-left:-30px"> */ ?>
        <div class="hidden-lg  hidden-xs" style="margin-top:25px; margin-right: -50px;">
            <div class="navbar-collapse collapse  ">
                <div class="col-sm-8 col-md-8" >
                    <form action="<?php echo $url_form_search ?>" method="get">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Cari produk, kategori, atau merk" name="<?php echo SEARCH ?>">

                            <div class="input-group-btn" style="width:138px;" >
                                <select id="maxOption2_md" class="selectpicker show-menu-arrow input-group-btn form-control" data-max-options="1" data-style="btn-inverse" >
                                    <?php echo get_category_option($tree_category, 0, $product_category_seq); ?>
                                </select>
                            </div>
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-info   btn-flat " style="background-color: #0992a6; border-color:#0992a6; margin-right:20px; height:34px;"><i class="glyphicon glyphicon-search"></i></button>
                            </span>
                            <span class="input-group-btn">
                                <div style='margin-left:-8px;' >
                                    <button type="button" class="btn btn-flat btn-front bag_box_sm" data-toggle="modal"
                                            data-target="#add_to_cart">
                                        <a href="#" data-toggle="modal"><center><span id ="cart_qty_sm" class="fa fa-shopping-cart white"><?php echo isset($_SESSION[SESSION_PRODUCT_INFO]) && sizeof($_SESSION[SESSION_PRODUCT_INFO]) != 0 ? sizeof($_SESSION[SESSION_PRODUCT_INFO]) : ""; ?></span></center></a>
                                    </button>
                                    <?php if (isset($_SESSION[SESSION_MEMBER_CSRF_TOKEN])) { ?>
                                        <button class="btn btn-flat btn-front" type="text" data-toggle="dropdown" style='margin-left:2px'>
                                            <span class="fa fa-user white"></span></button>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a href="<?php echo get_base_url() ?>member"><span class="fa fa-user"></span>Profile</a></li>
                                            <li><a href="<?php echo get_base_url() ?>member/wishlist"><span class="fa fa-heart"></span>Wishlist</a></li>
                                            <li><a href="<?php echo get_base_url() ?>member/login/sign_out?<?php echo $signout_param ?>" id="Logout"><span class="fa fa-sign-out"></span>Log Out</a></li>
                                        </ul>
                                    <?php } else { ?>
                                        <button type="button" class="btn btn-flat btn-front"  data-toggle="modal" data-target="#login_register"
                                                id="loginregister" data-id="1" style='margin-left:2px'>
                                            <center><span class="fa fa-user white"></span></center>
                                        </button>
                                    <?php } ?>
                                </div>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
            <nav id="primary_nav_wrap">
                <?php $this->load->view('home/template/segment_main/main_menu'); ?>
            </nav>
        </div>
    </div>
</nav>

<div class="container">
    <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left hidden-md hidden-lg hidden-sm"
         id="cbp-spmenu-s1">
             <?php if (isset($tree_category)): ?>
            <h3>
                <span id="closeMenu" class="glyphicon glyphicon-tasks white"></span>&nbsp;&nbsp;&nbsp;Kategori
            </h3>
            <div style="overflow: auto; position: absolute; top:0;right:0;bottom:0;left:0px; margin-top:60px">
                <div id="MainMenu">
                    <div class="list-group"><br>
                        <?php echo get_menu_3level_xs($tree_category, 0); ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </nav>
</div>

<div class="container">
    <nav
        class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right hidden-md hidden-lg "
        id="cbp-spmenu-login">
        <h3>
            <span id="closeLogin" class="glyphicon glyphicon-tasks white"></span>&nbsp;&nbsp;&nbsp;Login
        </h3>
        <div style="overflow: auto; position: absolute; top:0;right:0;bottom:0;left:0px; margin-top:60px">
            <div id="MainMenuLogin">
                <div class="list-group"><br>
                    <a href="<?php echo get_base_url() ?>member" class="list-group-item list-group-item-success ">
                        <span class="fa fa-user"></span>&nbsp;&nbsp;&nbsp;Profile
                    </a>
                    <a href="<?php echo get_base_url() ?>member/login/sign_out?<?php echo $signout_param ?>" class="list-group-item list-group-item-success">
                        <span class="fa fa-sign-out"></span>&nbsp;&nbsp;&nbsp;Sign Out
                    </a>
                </div>
            </div>
        </div>

    </nav>
</div>

<div class="container" style="margin-top:80px;">
    <div class="hidden-md hidden-sm hidden-lg">
        <center>
            <br>
            <form action="<?php echo $url_form_search ?>" method="get">
                <div class="input-group">


                    <input type="text" class="form-control" placeholder="Cari produk, kategori, atau merk" name="<?php echo SEARCH ?>">
                    <?php /* remove category menu when display on xs
                     *                     <span class="input-group-btn">
                      <select id="maxOption2_general" class="selectpicker show-menu-arrow input-group-btn form-control" data-max-options="1" data-style="btn-inverse" >
                      <?php echo get_category_option($tree_category, 0,$product_category_seq); ?>
                      </select>
                      </span> */ ?>
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-info btn-flat " style="background-color: #0992a6; border-color:#0992a6;"><i class="glyphicon glyphicon-search"></i></button>
                    </span>

                </div>
            </form>
            <br>
        </center>

    </div>
</div>

<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId: '672193332925149',
            status: true,
            cookie: true,
            xfbml: true
        });
    };

    $('#Logout').click(function(e) {
<?php if (isset($_SESSION[SESSION_TYPE_LOGIN])) { ?>
    <?php if ($_SESSION[SESSION_TYPE_LOGIN] == STANDARD_LOGIN) { ?>
                //Do Nothing
    <?php } else if ($_SESSION[SESSION_TYPE_LOGIN] == GOOGLE_PLUS_LOGIN) { ?>
                e.preventDefault();
                var href = $(this).attr('href');
                (function() {
                    var po = document.createElement('script');
                    po.type = 'text/javascript';
                    po.async = true;
                    po.src = 'http://accounts.google.com/logout';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(po, s);
                })();
                setTimeout(function() {
                    window.location = href;
                }, 1000);
    <?php } else if ($_SESSION[SESSION_TYPE_LOGIN] == FACEBOOK_LOGIN) { ?>
                FB.logout(function() {
                    document.location.reload();
                });
                setTimeout(function() {
                    window.location = href;
                }, 1000);
    <?php } ?>
            //Do Nothing
<?php } else { ?>
            //Do Nothing
<?php } ?>
    });

</script>