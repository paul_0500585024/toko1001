<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta property="fb:admins" content="100008738526458" />
<link rel="canonical" href="<?php echo current_url_with_query_string();?>" />
<meta name="description" content="<?php echo (isset($description) ? htmlentities($description): htmlentities(DEFAULT_DESCRIPTION) )?>">
<meta name="keyword" content="<?php echo (isset($keyword) ? htmlentities($keyword): htmlentities(DEFAULT_KEYWORD) )?>">
<?php if(isset($is_detail)):?>
    <meta name="robots" content="index, nofollow">
<?php else:?>
    <meta name="robots" content="index, follow" />
<?php endif;?>
<?php echo $_add_header_; ?>
<title><?php echo (isset($title) ? $title : DEFAULT_TITLE) ?></title>
<?php require_once VIEW_BASE_HOME; ?>


<link rel="stylesheet" type="text/css" href="<?php echo get_css_url(); ?>bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_css_url(); ?>font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_css_url(); ?>font-toko1001.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_css_url(); ?>ubuntu_fonts.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_css_url(); ?>bootstrap-nav-wizard.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_css_url(); ?>AdminLTE.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_css_url(); ?>dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_css_url(); ?>daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_css_url(); ?>ionicons.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_css_url(); ?>bootstrap-select.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_css_url(); ?>photoGalleryStyle.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_css_url(); ?>magnify.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_css_url(); ?>style.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_css_url(); ?>jquery.dataTables.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_css_url(); ?>select2.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_css_url(); ?>slider.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_css_url(); ?>component.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_css_url(); ?>iCheck/all.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_css_url(); ?>rating.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_css_url(); ?>responsive.dataTables.min.css"/>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"   rel="stylesheet">
<link href="<?php echo get_css_url() ?>bxslider/jquery.bxslider.css" rel="stylesheet" />
<link rel="icon" href="<?php echo get_img_url(); ?>icon/pavicon.png"/>

<script type="text/javascript" src="<?php echo get_js_url(); ?>jQuery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>dataTables.responsive.min.js"></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>select2.full.min.js"></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>autoNumeric-min.js"></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>moment.min.js"></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>iCheck/icheck.min.js"></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>jquery.elevateZoom-2.2.3.min.js"></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>jquery.validate.js"></script>
<script src="<?php echo get_js_url() ?>input-mask/jquery.inputmask.js"></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>error_response.js"></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>rating.js"></script>

<script src="<?php echo get_js_url() ?>bxslider/jquery.bxslider.min.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63276627-1', 'auto');
  ga('send', 'pageview');

</script>