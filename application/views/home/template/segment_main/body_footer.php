<br>
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <img src="<?php echo get_img_url(); ?>522x120_biru.png" alt="LOGO BIRU" height="40px">
                <p align="justify">Website toko1001.id masih dalam tahap pengembangan. 
                   Kami menerima semua masukan untuk menjadi lebih baik dalam melayani Anda.
                   Atas perhatiannya kami ucapkan terima kasih.</p>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="footer_menu">Layanan Pelanggan</div>

                <ul class="footer_menu_list">
                    <li><a href="<?php echo base_url(); ?>info/panduan-belanja">Panduan Belanja</a></li>
                    <li><a href="<?php echo base_url(); ?>info/pengembalian-barang">Pengembalian Barang</a></li>
                    <li><a href="<?php echo base_url(); ?>info/pengembalian-dana">Pengembalian Dana</a></li>
                    <br>
                </ul>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="footer_menu">Customer Care</div>
                <ul class="footer_menu_list">
                    <li>Senin-Jumat, 08.30-17.00 WIB</li>
                    <li>Sabtu, 08.30-14.00 WIB</li>

                    <li><span class="fa fa-phone"> </span> <span class="footer_text_small">+6221-6514224</span></li>
                    <li><span class="fa fa-phone"> </span> <span class="footer_text_small">+6221-6514225</span></li>
                    <li><a class="sendemail" href="mailto:cc@toko1001.id"><span class="glyphicon glyphicon-envelope"></span></a>&nbsp;<a class="sendemail" href="mailto:cc@toko1001.id"><span class="footer_text_small">cc@toko1001.id</span></a></li>
                </ul>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="footer_menu">Jadi Merchant Kami</div>                
                <ul class="footer_menu_list" >

                    <li><p style='margin-top:0px;'>Tanpa biaya iuran tahunan ataupun biaya tambahan
                            pemeliharaan,hanya fee transaksi yang kompetitif (komisi).
                    </li> 
                    <li>

<!--                        <div class="button_register">
-->                            <a href="" class="btn btn-front btn-flat "data-toggle="modal" data-target="#modal_register_merchant""
                               id="register_merchant" data-id="1" style="color:white"> Daftar Sekarang</a><!--
                        </div>-->


                    </li> 
                </ul>

            </div>
        </div>
        <div class="row container">&nbsp;</div>
       <div class="row">
            <div class="col-md-6 col-sm-3 col-xs-12" style='margin-bottom:15px;'>
                <span class="menu_title">Metode Pembayaran</span> <br />
                <img height="30px" src="<?php echo get_img_url(); ?>payment_70px/VISA_70px.png" alt="VISA">
                <img height="30px" src="<?php echo get_img_url(); ?>payment_70px/MASTERCARD_70px.png" alt="MASTERCARD">
                <img height="30px" src="<?php echo get_img_url(); ?>payment_70px/JCB_70px.png" alt="JCB">
                <img height="30px" src="<?php echo get_img_url(); ?>payment_70px/BCA_70px.png" alt="BCA">
                <img height="30px" src="<?php echo get_img_url(); ?>payment_70px/MANDIRI_70px.png" alt="MANDIRI">
                <img height="30px" src="<?php echo get_img_url(); ?>payment_70px/mandiriclickpay_70px.png" alt="MANDIRI CLICKPAY">
            </div>

            <div class="col-md-2 col-sm-3 col-xs-12" style='margin-bottom:15px;'>
                <span class="menu_title">Keamanan Belanja</span> <br />
                <img height="35px" src="<?php echo get_img_url(); ?>payment_70px/COMODOSECURE_70px.png" alt="COMODOSECURE">
                <img height="35px" src="<?php echo get_img_url(); ?>payment_70px/verifiedbyvisa_70px.png" alt="VERIFIEDBYVISA">
                <img height="35px" src="<?php echo get_img_url(); ?>payment_70px/mastercardsecurecode_70px.png" alt="MASTERCHARDSECURE">
                <img height="35px" src="<?php echo get_img_url(); ?>payment_70px/jcb_secure_70px.png" alt="JCB">

            </div>

            <div class="col-md-2 col-sm-3 col-xs-12" style='margin-bottom:15px;'>
                <span class="menu_title">Jasa Pengiriman</span> <br />
                <img height="35px" src="<?php echo get_img_url(); ?>payment_70px/jne_70px.png" alt="JNE">
                <?php /*<img height="35px" src="<?php echo get_img_url(); ?>payment_70px/pandu_70px.png" alt="PANDU">*/?>
                <img height="35px" src="<?php echo get_img_url(); ?>payment_70px/Rajakirim_70px.png" alt="Rajakirim">

            </div>




            <div class="col-md-2 col-sm-3 col-xs-12" style='margin-bottom:15px;'>
                <span class="menu_title" style="padding-bottom:15px;">Ikuti Kami</span>
                <div style="margin-top:5px;"></div>
                    <a
                    href="https://instagram.com/toko1001.id"><img
                        src="<?php echo get_img_url(); ?>INSTAGRAM.png" al="instagram"></a>&nbsp;&nbsp;&nbsp;<a
                    href="https://www.facebook.com/toko1001.id"><img
                        src="<?php echo get_img_url(); ?>FACEBOOK.png" alt="facebook"></a>&nbsp;&nbsp;&nbsp;<a
                    href="https://twitter.com/toko1001_id"><img
                        src="<?php echo get_img_url(); ?>TWITTER.png" alt="twitter"></a>
            </div>
        </div>


        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <p class="small_txt" align='justify'>TOKO1001 merupakan salah satu pusat belanja
                    "online", yang menawarkan pengalaman belanja online dengan konsep
                    aman, cepat dan nyaman. Kami menyediakan beragam produk, mulai dari
                    fashion, handphone & tablet, kesehatan & kecantikan, hobby & jam
                    tangan, kamera, computer, elektronik & audio, peralatan rumah
                    tangga, mainan anak-anak dan bayi, tas & koper, olahraga & music,
                    otomotif, makanan & minuman, buku & alat tulis.</p>
                <p class="small_txt" align='justify'>Belanja online di TOKO1001 sangat mudah!!!
                    Tetap terhubung dan dapatkan penawaran terbaru dan transaksi setiap
                    harinya. Kami menawarkan beragam pilihan produk-produk berkualitas,
                    yang dapat ditemukan melalui ujung jari Anda. Dengan navigasi
                    pencarian yang sederhana namun efektif, daftar koleksi, dan harga
                    yang menarik, Kami memberikan kemudahan untuk Anda menemukan produk
                    sesuai kebutuhan belanja Anda.</p>
                <p class="small_txt" align='justify'>Jadikanlah momen belanja online Anda menjadi
                    kegiatan yang menyenangkan dengan TOKO1001. Terima kasih atas
                    kunjungan Anda dan nikmati sensasi berbelanja online di TOKO1001,
                    dimana Anda dapat mencari yang terbaik hanya dengan beberapa klik
                    saja. Selamat berbelanja online!!!</p>
            </div>
        </div>
    </div>

    <div class="bottom_page">
        <div class="container text-center">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <a href="<?php echo base_url()?>info/kebijakan-privasi">Kebijakan
                    Privasi</a>&nbsp;&nbsp;&nbsp;<a
                    href="<?php echo base_url()?>info/syarat-dan-ketentuan">Syarat &
                    Ketentuan</a>
                <p>Copyright &copy;2015-2016 PT. Deltamas Mandiri Sejahtera</p>
            </div>
        </div>
    </div>
</footer>