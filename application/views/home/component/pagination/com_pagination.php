<ul class ="pagination">
    <?php for ($i = 1; $i <= $data_list[LIST_RECORD][0]->totalPage; $i++) { ?>
        <li <?php echo $i == $data_list[LIST_RECORD][0]->pCurrPage ? "class='active'" : "" ?> ><a href="<?php echo base_url($data_auth[FORM_URL]) . "?page=" . $i . "&filter=" ?>"><?php echo $i ?></a></li>
    <?php }; ?>
</ul>   