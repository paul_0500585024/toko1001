<?php
require_once VIEW_BASE_HOME;
?>
<?php
if (isset($popupbanner)) {
    if ($popupbanner == 0) {
        ?>
        <link rel="stylesheet" type="text/css" href="<?php echo get_css_url(); ?>animate.css"/>
        <style>
            .modal-backdrop {
                background-color: transparent;
            }

            .modal-dialogs {
                position: fixed;margin: 0;width: 100%;height: 100%;padding: 0;background-color:rgba(0, 0, 0, 0.5);
            }
            .modal-contents {
                position: absolute;top: 0;right: 0;bottom: 0;left: 0;border: 2px solid #000;
            }
            .vertical-center {
                min-height: 100%;  /* Fallback for browsers do NOT support vh unit */
                min-height: 100vh; /* These two lines are counted as one :-)       */
                display: flex;
                align-items: center;
            }
        </style>
        <!-- modal -->
        <div id="fsModal" class="modal">
            <div class="modal-dialogs">
                <div class="modal-contents">
                    <div class="container container-table animated zoomInUp" id="formid">
                        <div class="row vertical-center">
                            <div class="text-center col-md-6 col-md-offset-3">
                                <form id="frmRegister" 
                                      style="background-image: url(<?php echo get_img_url() . 'welcome_screen.jpg' ?>);
                                      width: 117%;"
                                      method="post" action= "<?php echo get_base_url() ?>">
                                    <input type="hidden" id="newm" name="newm" value="new_member"/>
                                    <div class="container">
                                        <div class="form-group text-left">
                                            <label style="font-family: 'Source Sans Pro',sans-serif; margin-top: 15px;
                                                   font-size: 157%; 
                                                   font-weight: 800;
                                                   font-style: oblique;
                                                   color: #0B2C39;">
                                                DAPATKAN VOUCHER BELANJA
                                            </label>
                                            </br>
                                            <label style="font-family: 'Source Sans Pro',sans-serif; margin-top: -20px; 
                                                   font-size: 470%; 
                                                   font-weight: 800;
                                                   font-style: oblique;
                                                   color: #0B2C39;">
                                                Rp50.000,-
                                            </label>
                                        </div>
                                        <h4><p class="pull-left" style="font-family: 'Source Sans Pro',sans-serif; 
                                               color: #0B2C39;
                                               margin-top: -20px;
                                               margin-left: 5px;">
                                                Segera daftarkan email anda di sini
                                            </p></h4>
                                    </div>
                                    <div class="container">
                                        <p id="divmessage"></p>
                                        <div class="form-group col-lg-12">
                                            <input type="email" class="form-control"
                                                   style="width: 30%; 
                                                   background-color: rgba(255, 255, 255, 0.7);
                                                   margin-left: -12px;"
                                                   id="qemail" name="qemail" placeholder="Masukkan alamat email anda" 
                                                   required=""/>
                                        </div>
                                        <div class="form-group pull-left">
                                            <div class="row" style="margin-left : 5px;">
                                                <input type="radio" id="jk" name="jk" value="M" required=""/> Pria &nbsp;
                                                <input type="radio" id="jk" name="jk" value="F" required=""/> Wanita
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container">
                                        <div class="form-group pull-left">
                                            <label class="text-left" for="snk" 
                                                   style="font-family: 'Source Sans Pro',sans-serif;
                                                   margin-left: 5px;
                                                   color: #0B2C39;
                                                   font-size: small;">
                                                <input type="checkbox" id="snk" name="snk" value="1" required=""/>
                                                Setuju dengan
                                                <a style="color: #EE3029;" href="<?php echo get_base_url(); ?>info/syarat-dan-ketentuan"
                                                   target="_blank">Syarat dan Ketentuan</a>, serta
                                                <a style="color: #EE3029;" href="<?php echo get_base_url(); ?>info/kebijakan-privasi"
                                                   target="_blank">Kebijakan </a>
                                                </br>&nbsp;&nbsp;&nbsp;
                                                dari Toko1001.
                                            </label>
                                        </div>
                                    </div>
                                    <div class="container">
                                        <div class="form-group col-lg-12">
                                            <button class="btn btn-info pull-left"
                                                    style="background-color: #0B2C39;
                                                    border: none;
                                                    margin-left: -10px; 
                                                    width: 120px"> Daftar 
                                            </button>
                                            <a class="text-left col-lg-3" 
                                               style="margin-left: 10px; 
                                               margin-top: 5px;
                                               font-family: 'Source Sans Pro',sans-serif; 
                                               color: rgb(11, 125, 189);" 
                                               href="#" data-dismiss="modal">
                                                Lanjutkan ke Toko1001 &nbsp;<i class="fa fa-arrow-right"></i>
                                            </a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            <!--
        setTimeout(function () {
                $('#fsModal').modal()
            }, 1000);
            var success_msg = '<p class="text-left" style="margin-left: 5px; color: green; width: 500px; margin-bottom: 0!important;><h4><i class="fa fa-info"></i></h4>';
            var error_msg = '<p class="text-left" style="margin-left: 5px; color: red; width: 500px; margin-bottom: 0!important;> <h4><i class="fa fa-info"></i></h4>';
            $(function () {
                $("#frmRegister").on("submit", function (event) {
                    event.preventDefault();

                    $.ajax({
                        url: "<?php echo get_base_url(); ?>",
                        type: "post",
                        data: $(this).serialize(),
                        success: function (data) {
                            if (data.trim() == 'OK') {
                                $('#divmessage').html(success_msg + "Terima kasih telah mendaftar sebagai Member di Toko1001. </br>Harap cek email anda.</p>");
                                $("#divmessage").fadeTo(4000, 1500).slideUp(1000, function () {
                                    $('#fsModal').modal('toggle');
                                });
                                return false;
                            } else {
                                $('#divmessage').html(error_msg + data + "</p>");
                                $("#divmessage").fadeTo(4000, 1500).slideUp(1000, function () {
                                });
                                return false;
                            }
                        }
                    });
                });
            });

            var isValidSignUp = $('#frmSignUpMerchant').validate({
                ignore: 'input[type=hidden]',
                highlight: function (element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else if (element.hasClass('select2')) {
                        error.insertAfter(element.next('span'));
                    } else {
                        error.insertAfter(element);
                    }
                }
            });

        </script>
        <?php
    }
}
?>
<div class="container" >
    <!--    for sm-->
    <div class="row hidden-xs hidden-lg " style="margin-top:60px;">&nbsp;</div>
    <!--    for lg md-->
    <div class="row hidden-xs hidden-sm hidden-md" style="margin-top:25px;">&nbsp;</div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <?php if (isset($banner[0])): ?>
            <div class="row">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <?php foreach ($banner[0] as $key => $each_banner): ?>
                            <?php if ($key == 0): ?>
                                <div class="item active">
                                    <a href="<?php echo base_url() . $each_banner->image_url; ?>">
                                        <img src="<?php echo get_file_exists(SLIDE_UPLOAD_IMAGE . $each_banner->image); ?>" alt="<?php echo $each_banner->image; ?>" id="img-prev-slider"
                                             class="img-responsive center-block">
                                    </a>
                                </div>
                            <?php else: ?>
                                <div class="item">
                                    <a href="<?php echo base_url() . $each_banner->image_url; ?>">
                                        <img src="<?php echo get_file_exists(SLIDE_UPLOAD_IMAGE . $each_banner->image); ?>" alt="<?php echo $each_banner->image; ?>"
                                             class="img-responsive center-block" id="img-prev-slider">
                                    </a>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                    <a class="left carousel-control" href="#myCarousel" role="button"
                       data-slide="prev"> <span
                            class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a> <a class="right carousel-control" href="#myCarousel"
                            role="button" data-slide="next"> <span
                            class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        <?php endif; ?>
        <div class="row ">
            <div class="hover01">
                <?php if (isset($banner[1][0])): ?>
                    <a href="<?php echo $banner[1][0]->banner_image_url; ?>">
                        <img data-src="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $banner[1][0]->banner_image, 'no_image600x160.png'); ?>"
                             data-src-retina="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $banner[1][0]->banner_image, 'no_image600x160.png'); ?>"
                             src="<?php echo get_image_location() . ASSET_IMG_HOME . 'blank.png' ?>" alt="blank"
                             class="img-responsive  load_img">
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div class="col-md-4 col-sm-4 col-xs-8">
        <div class="row">
            <div class="hover01">
                <?php if (isset($banner[1][0])): ?>
                    <a href='<?php echo $banner[1][0]->advertise1_image_url; ?>'>
                        <img data-src="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $banner[1][0]->advertise1_image); ?>"
                             data-src-retina="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $banner[1][0]->advertise1_image); ?>"
                             src="<?php echo get_image_location() . ASSET_IMG_HOME . 'blank.png' ?>" alt="blank"
                             class="img-responsive center-block  load_img" id="adv1" style="min-width:100%">
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="col-md-2 col-sm-2 col-xs-4">
        <div class="row">
            <div class="hover01">
                <?php if (isset($banner[1][0])): ?>
                    <a href="<?php echo $banner[1][0]->advertise2_image_url; ?>">
                        <img data-src="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $banner[1][0]->advertise2_image); ?>"
                             data-src-retina="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $banner[1][0]->advertise2_image); ?>"
                             src="<?php echo get_image_location() . ASSET_IMG_HOME . 'blank.png' ?>" alt="blank"
                             class="img-responsive center-block  load_img" id="adv2" style="min-width:100%">
                    </a>
                <?php endif; ?>
            </div>
            <div class="hover01">
                <?php if (isset($banner[1][0])): ?>
                    <a href="<?php echo $banner[1][0]->advertise3_image_url; ?>">
                        <img data-src="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $banner[1][0]->advertise3_image); ?>"
                             data-src-retina="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $banner[1][0]->advertise3_image); ?>"
                             src="<?php echo get_image_location() . ASSET_IMG_HOME . 'blank.png' ?>" alt="blank"
                             class="img-responsive center-block  load_img" id="adv3" style="min-width:100%">
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<div class="container" ><br>
    <div class="row">
        <div class="container">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 floornewproduct_category">
                <div class="col-lg-6 col-sm-6 col-xs-7 nopadding">
                    <span class="floornewproduct_number">
<!--                            <img id="newproductlogo" src="<?php echo base_url() . ASSET_IMG_HOME ?>icon/peroduk_terbaru_50x50.png" alt="" height="40px" style="margin-left:1px;margin-right:0px;" /></span>-->
                        <img id="newproductlogo" src="<?php echo get_img_url(); ?>product_newest.png" alt="" height="40px" style="margin-left:1px;margin-right:0px;" /></span>
                    <h1 class="floor_title" style="color:white; margin-top:17px;margin-bottom:13px;" id="h1newtitle">PRODUK TERBARU</h1>
                </div>
                <div class="col-lg-6 col-sm-6 col-xs-5 text-right">
                    <h1 class="floornew_title"><a href="<?php echo base_url(); ?>produk-terbaru" style="color:white;">Lihat Semua&nbsp;<i class="fa fa-angle-double-right"></i></a></h1>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="container">
            <div class="floornewproduct">
            </div>
        </div>
    </div>
</div>

<?php echo display_product_home($new_products); ?>

<?php $counter = 1; ?>
<?php if (isset($tree_category[0])): ?>
    <?php foreach ($tree_category[0] as $each_tree_category): ?>
        <?php if ($each_tree_category->seq == "1" || $each_tree_category->seq == "7" || $each_tree_category->seq == "3" || $each_tree_category->seq == "9") { ?>
            <?php if ($counter <= FLOOR_MAX): ?>
                <?php $each_tree_category_seq = $each_tree_category->seq; ?>
                <?php
                $color_icon = "black"; //filled with black or white
                ?>
                <div class="container">
                    <br>
                    <div class="row">
                        <div class="container" >
                            <a href="<?php echo base_url() . url_title(strtolower($each_tree_category->name)) . "-" . CATEGORY . ($each_tree_category->seq); ?>">
                                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 floor<?php echo url_title(strtolower($each_tree_category->name)); ?>_category" id="floordiv">
                                    <span class="floorimg floor<?php echo url_title(strtolower($each_tree_category->name)); ?>_number"><i class="<?php echo $tree_category['icon'][$color_icon][url_title(strtolower($each_tree_category->name))] ?> text-navy" ></i></span>
                                    <h1 class="floor_title"><?php echo $each_tree_category->name; ?></h1>
                                </div>
                            </a>

                        </div>
                    </div>
                    <div class="row">
                        <div class="container">
                            <div class="floor<?php echo url_title(strtolower($each_tree_category->name)); ?>">
                                <div class="col-md-2 col-sm-4 hidden-xs">
                                    <div class="row">
                                        <div class="floor<?php echo url_title(strtolower($each_tree_category->name)); ?>_menu_title" >Rekomendasi</div>
                                        <ul class="floor<?php echo url_title(strtolower($each_tree_category->name)); ?>_menu_list">
                                            <li><a href="<?php echo base_url() . url_title(strtolower($each_tree_category->name)) . "-" . CATEGORY . ($each_tree_category->seq) . '?' . ORDER_CATEGORY . '=' . NEW_TO_OLD_PRODUCT ?>">Produk Baru</a></li>
                                            <li><a href="#">Produk Terlaris</a></li>

                                            <?php if (isset($tree_category[$each_tree_category_seq])): ?>
                                                <?php $counter_menu_level2 = 1; ?>
                                                <?php foreach ($tree_category[$each_tree_category_seq] as $each_tree_category_level2): ?>
                                                    <?php if ($counter_menu_level2 <= LEFT_MENU_LEVEL2_MAX): ?>
                                                        <li><a href="<?php echo base_url() . url_title(strtolower($each_tree_category_level2->name)) . "-" . CATEGORY . ($each_tree_category_level2->seq) ?>"><?php echo $each_tree_category_level2->name ?></a></li>
                                                        <?php $counter_menu_level2++; ?>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>
                                <?php if (isset($floor)): ?>
                                    <?php foreach ($floor as $each_floor): ?>
                                        <?php if ($each_floor->category_seq == $each_tree_category_seq): ?>
                                            <div class="col-md-4 col-sm-8 ">
                                                <div class="row">
                                                    <div class="hover02">
                                                        <a href="<?php echo $each_floor->adv_1_img_url ?>">
                                                            <img data-src="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $each_floor->adv_1_img) ?>"
                                                                 data-src-retina="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $each_floor->adv_1_img) ?>"
                                                                 src="<?php echo base_url() . ASSET_IMG_HOME . 'blank.png' ?>" alt="blank"
                                                                 class="img-responsive center-block full-img-floor load_img">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-sm-4 col-xs-6">
                                                <div class="row">
                                                    <div class="hover02">
                                                        <a href="<?php echo $each_floor->adv_2_img_url ?>">
                                                            <img data-src="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $each_floor->adv_2_img) ?>"
                                                                 data-src-retina="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $each_floor->adv_2_img) ?>"
                                                                 src="<?php echo base_url() . ASSET_IMG_HOME . 'blank.png' ?>" alt="blank"
                                                                 class="img-responsive center-block full-img-floor load_img">
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="hover02">
                                                        <a href="<?php echo $each_floor->adv_7_img_url ?>">
                                                            <img data-src="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $each_floor->adv_7_img) ?>"
                                                                 data-src-retina="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $each_floor->adv_7_img) ?>"
                                                                 src="<?php echo base_url() . ASSET_IMG_HOME . 'blank.png' ?>" alt="blank"
                                                                 class="img-responsive center-block full-img-floor load_img">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-sm-4 col-xs-6">
                                                <div class="row">
                                                    <div class="hover02">
                                                        <a href="<?php echo $each_floor->adv_3_img_url ?>">
                                                            <img data-src="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $each_floor->adv_3_img) ?>"
                                                                 data-src-retina="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $each_floor->adv_3_img) ?>"
                                                                 src="<?php echo base_url() . ASSET_IMG_HOME . 'blank.png' ?>" alt="blank"
                                                                 class="img-responsive center-block full-img-floor load_img">
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="hover02">
                                                        <a href="<?php echo $each_floor->adv_6_img_url ?>">
                                                            <img data-src="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $each_floor->adv_6_img) ?>"
                                                                 data-src-retina="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $each_floor->adv_6_img) ?>"
                                                                 src="<?php echo base_url() . ASSET_IMG_HOME . 'blank.png' ?>" alt="blank"
                                                                 class="img-responsive center-block full-img-floor load_img">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-sm-4 col-xs-6 ">
                                                <div class="row">
                                                    <div class="hover02">
                                                        <a href="<?php echo $each_floor->adv_4_img_url ?>">
                                                            <img data-src="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $each_floor->adv_4_img) ?>"
                                                                 data-src-retina="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $each_floor->adv_4_img) ?>"
                                                                 src="<?php echo base_url() . ASSET_IMG_HOME . 'blank.png' ?>" alt="blank"
                                                                 class="img-responsive center-block full-img-floor load_img">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div   class="col-md-2 col-sm-4 col-xs-6  ">
                                                <div class="row">
                                                    <div class="hover02">
                                                        <a href="<?php echo $each_floor->adv_5_img_url ?>">
                                                            <img data-src="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $each_floor->adv_5_img) ?>"
                                                                 data-src-retina="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $each_floor->adv_5_img) ?>"
                                                                 src="<?php echo base_url() . ASSET_IMG_HOME . 'blank.png' ?>" alt="blank"
                                                                 class="img-responsive center-block full-img-floor load_img">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $counter++; ?>
            <?php endif; ?>
        <?php } ?>
    <?php endforeach; ?>
<?php endif; ?>