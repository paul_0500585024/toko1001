<?php
require_once VIEW_BASE_HOME;
?>

<div class="hidden-xs hidden-lg " style="margin-top:55px;">&nbsp;</div>
<!--    for lg md-->
<div class="hidden-xs hidden-sm hidden-md" style="margin-top:25px;">&nbsp;</div>

<div class="container">
    <?php
    switch ($informasi) {
	case "kebijakan-privasi":
	    ?>
	    <div class="content-header" style="text-align:justify">
		<h2 class="page-header">Kebijakan Privasi</h2>
		<p><strong><em>Kami menjaga privasi pelanggan kami dengan serius dan kami hanya akan mengumpulkan, merekam, menahan, menyimpan, mengungkapkan, dan menggunakan informasi pribadi Anda seperti yang diuraikan di bawah.</em></strong></p>
		<p>Adanya Kebijakan Privasi ini adalah komitmen nyata dari TOKO1001 untuk menghargai dan melindungi setiap informasi pribadi Pengguna situs www.toko1001.id (situs TOKO1001).</p>
		<p>Perlindungan data adalah hal yang menyangkut kepercayaan dan privasi Anda sangatlah penting bagi Kami. Oleh karena itu, Kami hanya akan menggunakan nama Anda dan informasi lain yang berhubungan dengan Anda sesuai dengan kebijakan privasi ini. Kami hanya akan mengumpulkan informasi yang penting bagi Kami dan Kami hanya akan mengumpulkan informasi yang relevan dengan transaksi antara Kami dengan Anda.</p>
		<p><br /> Kami hanya akan menyimpan informasi privasi Anda selama dibutuhkan diwajibkan oleh hukum atau selama informasi tersebut berhubungan dengan tujuan-tujuan yang ada saat informasi dikumpulkan.</p>
		<p><br /> Anda dapat mengunjungi <em>website / platform </em>kami (sebagaimana yang dijelaskan pada <em>Syarat &amp; Ketentuan</em>) dan melihat-lihat tanpa harus meninggalkan informasi pribadi. Saat Anda mengunjungi <em>website / platform</em> ini, identitas Anda akan tetap terjaga dan Kami tidak akan bisa mengidentifikasi Anda kecuali Anda <em>login</em> menggunakan <em>username (email)</em> dan <em>password</em> Anda.</p>
		<p><br /> Kebijakan privasi Kami mengikuti kebijakan perundangan-undangan yang berlaku. Bila Anda memiliki komentar, keluhan, dan masukan, kami dengan senang hati menerimanya melalui alamat <em>email </em>kami di cc@toko1001.id</strong></p>
		<p>&nbsp;</p>
		<p><strong>Pengumpulan Informasi Pribadi</strong></p>
		<p>TOKO1001 tidak menjual, menyebarkan atau memperdagangkan informasi pribadi milik pelanggan kepada pihak lain.</p>
		<p>Informasi pribadi yang dikumpulkan secara online hanya diperlihatkan dalam perusahaan Kami dan hanya dipergunakan TOKO1001 dalam rangka pemberian layanan kepada Anda secara internal.</p>
		<p>Saat Anda membuat akun TOKO1001, informasi pribadi yang mungkin akan kami kumpulkan termasuk namun tidak terbatas pada:</p>
		<ul>
		    <li>Nama</li>
		    <li>Alamat Pengiriman</li>
		    <li>Alamat Email</li>
		    <li>Nomor Telepon</li>
		    <li>Nomor Telepon Genggam</li>
		    <li>Tanggal Lahir</li>
		    <li>Jenis Kelamin</li>
		</ul>
		<p>Anda harus menyerahkan kepada Kami dan/atau, agen resmi Kami dan/atau dalam <em>website</em>/<em>Platform</em>, informasi yang akurat, lengkap dan tidak menyesatkan. Anda harus tetap memperbarui dan menginformasikannya kepada Kami apabila terdapat perubahan (informasi lebih lanjut di bawah). Kami berhak meminta dokumentasi untuk melakukan verifikasi informasi yang Anda berikan.</p>
		<p>Kami hanya akan dapat mengumpulkan informasi pribadi Anda jika Anda secara sukarela menyerahkan informasi kepada kami. Jika Anda memilih untuk tidak mengirimkan informasi pribadi Anda kepada kami atau kemudian menarik persetujuan menggunakan informasi pribadi Anda, maka hal itu dapat menyebabkan kami tidak dapat menyediakan layanan kami kepada Anda. Anda dapat mengakses dan memperbarui informasi pribadi yang Anda berikan kepada kami setiap saat seperti yang dijelaskan di bawah.</p>
		<p>Jika Anda memberikan informasi pribadi dari pihak ketiga manapun kepada Kami, maka Kami menganggap bahwa Anda telah memperoleh izin yang diperlukan dari pihak ketiga terkait untuk berbagi dan mentransfer informasi pribadinya kepada Kami.</p>
		<p>Jika Anda mendaftar menggunakan akun media sosial Anda atau menghubungkan akun TOKO1001 Anda ke akun media sosial Anda atau menggunakan fitur media sosial TOKO1001 lainnya, Kami dapat mengakses informasi tentang Anda yang terdapat dalam <em>media social</em> Anda sesuai dengan kebijakan penyedia <em>social media</em> bersangkutan, dan Kami akan mengelola data pribadi Anda yang telah Kami kumpulkan sesuai dengan kebijakan privasi TOKO1001.</p>
		<p>&nbsp;</p>
		<p><strong>Penggunaan dan Pengungkapan Informasi Pribadi</strong></p>
		<p>Informasi pribadi yang Kami dapatkan dari Anda akan digunakan untuk:</p>
		<ul>
		    <li>Untuk memfasilitasi penggunaan      Layanan (sebagaimana didefinisikan dalam <em>Syarat</em> &amp; <em>Ketentuan</em>)      dan / atau akses ke <em>Website</em> / <em>Platform</em></li>
		    <li>Mengantarkan pengiriman      produk-produk yang telah Anda beli dari TOKO1001, kami dapat menyampaikan      informasi pribadi Anda kepada pihak ketiga dalam rangka pengiriman produk      kepada Anda (misalnya untuk mitra pengiriman atau supplier kami)</li>
		    <li>Memperbarui informasi tentang      pengiriman produk-produk dan untuk pelayanan konsumen </li>
		    <li>Memberikan kepada Anda      informasi yang berhubungan dengan produk-produk</li>
		    <li>Menjalankan proses pemesanan      Anda dan untuk memberikan Anda pelayanan dan informasi yang ditawarkan      oleh <em>website</em> / <em>platform</em> kami dan yang Anda      harapkan </li>
		    <li>Untuk membandingkan informasi,      dan memverifikasinya dengan pihak ketiga dalam rangka memastikan      keakuratan informasi.</li>
		    <li>Lebih dari itu, Kami akan      menggunakan informasi yang Anda berikan untuk urusan administrasi akun Anda      dengan kami; untuk verifikasi dan mengelola transaksi keuangan yang      berhubungan dengan pembayaran yang Anda buat secara online; mengaudit data      yang diunduh dari <em>website</em> Kami;      memperbaiki susunan dan/atau isi dari halaman-halaman <em>website</em> Kami dan menyesuaikannya dengan kebutuhan pengguna <em>website</em>; mengidentifikasi      pengunjung <em>website</em> Kami;      melakukan riset mengenai data demografis pengguna <em>website</em> Kami; mengirimkan Anda informasi yang Kami nilai akan      berguna untuk Anda yang Anda minta dari Kami, termasuk informasi tentang      produk-produk dan pelayanan-pelayanan Kami, asalkan Anda telah      mengindikasikan bahwa Anda tidak keberatan dihubungi mengenai hal-hal ini. </li>
		    <li>Ketika Anda mendaftar      menggunakan akun TOKO1001 atau memberikan kepada Kami informasi pribadi      Anda melalui <em>Website</em> / <em>Platform</em>,      Kami juga akan menggunakan informasi pribadi Anda untuk mengirimkan      pemasaran dan / atau materi promosi tentang produk dan layanan Kami atau      penjual pihak ketiga dari waktu ke waktu. Anda dapat berhenti dari      berlangganan dan menerima informasi pemasaran setiap saat dengan      menggunakan fungsi <em>unsubscribe</em> dalam materi pemasaran elektronik.      Kami dapat menggunakan informasi kontak Anda untuk mengirim <em>newsletter</em> dari kami dan dari perusahaan terkait dengan kami; dan</li>
		    <li>Dalam keadaan tertentu,      TOKO1001 dimungkinkan perlu untuk mengungkapkan informasi pribadi, seperti      ketika ada alasan kuat yang dapat dipercaya bahwa pengungkapan tersebut      diperlukan untuk mencegah ancaman terhadap nyawa atau kesehatan, untuk      tujuan penegakan hukum, atau untuk permintaan pemenuhan persyaratan hukum      dan peraturan terkait.</li>
		</ul>
		<p>TOKO1001 dapat menggunakan informasi pribadi Anda dengan pihak ketiga dan afiliasi Kami untuk tujuan tersebut di atas, khususnya, menyelesaikan transaksi dengan Anda, (seperti misalnya contohnya kepada mitra pengiriman atau pemasok), mengelola akun Anda dan hubungan kami dengan Anda, dalam rangka pemasaran dan pemenuhan persyaratan hukum atau peraturan dan permintaan yang dianggap perlu oleh TOKO1001. Dalam hal ini, Kami berusaha untuk memastikan bahwa pihak ketiga dan afiliasi Kami menjaga informasi pribadi Anda aman dari akses yang tidak sah, pengumpulan, penggunaan, pengungkapan, atau risiko sejenis dan menyimpan informasi pribadi Anda selama informasi pribadi Anda dibutuhkan untuk tujuan yang disebutkan di atas.</p>
		<p>Dalam mengungkapkan atau pentransferan informasi pribadi Anda kepada pihak ketiga dan afiliasi kami yang berlokasi di luar negeri, TOKO1001 mengambil langkah-langkah untuk memastikan bahwa yurisdiksi setempat telah mempunyai standar perlindungan informasi pribadi.</p>
		<p>TOKO1001 tidak terlibat dalam bisnis memperjualbelikan informasi pribadi pelanggan kepada pihak ketiga.</p>
		<p>&nbsp;</p>
		<p><strong>Penarikan Persetujuan</strong></p>
		<p>Anda dapat komunikasikan keberatan Anda atas penggunaan terus-menerus dan/atau pengungkapan informasi pribadi Anda untuk tujuan dan dengan cara tersebut di atas setiap saat dengan menghubungi kami di alamat <em>e-mail</em> kami di bawah.</p>
		<p>Perlu diketahui bahwa jika Anda menyatakan keberatan Anda untuk menggunakan dan / atau pengungkapan informasi pribadi Anda atas tujuan dan dengan cara yang Kami nyatakan di atas, tergantung pada sifat dari keberatan Anda, Kami mungkin saja tidak dapat menyediakan produk atau layanan Kami kepada Anda atau melakukan kesepakatan apapun yang Kami miliki dengan Anda. Dalam hal ini hak dan upaya hukum Kami telah dinyatakan secara tertulis.</p>
		<p>&nbsp;</p>
		<p><strong>Memperbarui Informasi Pribadi Anda</strong></p>
		<p>Anda dapat memperbarui Informasi pribadi Anda kapan saja dengan mengakses akun dalam <em>website</em> / <em>Platform</em> TOKO1001. Jika Anda tidak memiliki akun dengan Kami, maka Anda dapat menghubungi Kami pada alamat email di bawah.</p>
		<p>Kami mengambil langkah-langkah untuk berbagi pembaruan informasi pribadi Anda dengan pihak ketiga dan afiliasi Kami jika informasi pribadi Anda masih diperlukan untuk tujuan tersebut di atas.</p>
		<p>&nbsp;</p>
		<p><strong>Mengakses Informasi Pribadi Anda</strong></p>
		<p>Apabila Anda ingin melihat informasi pribadi Anda, yang Kami miliki atau menanyakan tentang informasi pribadi Anda yang telah atau mungkin akan digunakan atau diungkapkan oleh TOKO1001 dalam satu tahun terakhir, silahkan hubungi kami di alamat <em>e-mail</em> kami di bawah. Kami berhak untuk membebankan biaya administrasi yang wajar atas keperluan ini.</p>
		<p>Jika Anda memiliki akun TOKO1001, Anda dapat mengakses rincian pesanan Anda dengan cara masuk ke akun Anda (<em>login</em>) di <em>Website</em> / <em>Platform</em>. Di sini Anda dapat melihat rincian pesanan Anda yang telah selesai, rincian yang masih terbuka, rincian yang segera akan dikirim, dan pengelolaan alamat lengkap Anda, rincian bank, dan setiap <em>newsletter</em> yang mungkin telah berlangganan dengan Anda. Anda mengusahakan <em>username</em>, <em>password</em> dan rincian permintaan terjaga kerahasiaannya dan tidak memberikannya kepada pihak ketiga yang tidak berwenang. Kami tidak memiliki kewajiban menanggung penyalahgunaan <em>username</em>, <em>password</em>&nbsp; TOKO1001 atau rincian pesanan, kecuali sebagaimana yang tercantum dalam <em>Syarat &amp; Ketentuan</em>.</p>
		<p>&nbsp;</p>
		<p><strong>Keamanan Informasi Pribadi Anda</strong></p>
		<p>TOKO1001 memastikan bahwa informasi yang dikumpulkan akan disimpan dengan aman. Kami menyimpan informasi pribadi Anda dengan cara:</p>
		<ul>
		    <li>Membatasi akses ke dalam      informasi pribadi Anda</li>
		    <li>Mengurus dan mengelola      produk-produk teknologi kami untuk mencegah akses komputer yang tidak      memiliki izin </li>
		    <li>Secara aman menghancurkan      informasi pribadi Anda saat kami tidak lagi membutuhkannya untuk tujuan      catatan retensi, keperluan hukum atau bisnis</li>
		</ul>
		<p>TOKO1001 menggunakan teknologi 128 - bit SSL (secure sockets layer) encyption saat memproses rincial finansial Anda. 128-bit SSL encryption bisa dikira mungkin akan membutuhkan waktu sesedikitnya satu triliun tahun untuk rusak, dan ini adalah standar industri.</p>
		<p>Password Anda adalah kunci untuk masuk akun Anda. Silakan gunakan nomor unik, huruf dan karakter khusus, dan jangan berbagi <em>password</em> TOKO1001 Anda kepada siapa pun. Jika Anda berbagi <em>password</em> Anda dengan orang lain, Anda akan bertanggung jawab untuk semua tindakan dan konsekuensi yang diambil atas nama akun Anda. Jika Anda kehilangan kontrol <em>password</em> Anda, Anda mungkin kehilangan kontrol atas informasi pribadi Anda dan informasi lainnya yang disampaikan kepada TOKO1001. Anda juga bisa dikenakan tindakan yang mengikat secara hukum yang diambil atas nama Anda. Oleh karena itu, jika <em>password</em> Anda telah diganggu untuk alasan apapun atau jika Anda memiliki alasan yang dapat dipercaya bahwa <em>password</em> Anda telah diganggu, Anda harus segera menghubungi Kami dan mengganti <em>password</em> Anda. Anda diingatkan untuk selalu <em>log off</em> dari akun Anda dan menutup <em>browser</em> ketika selesai menggunakan komputer bersama.</p>
		<p>&nbsp;</p>
		<p><strong>Penyingkapan Informasi Pribadi</strong></p>
		<p><br /> Kami tidak akan membagikan informasi Anda dengan organisasi lain selain organisasi yang berkaitan dengan kami dan pihak ketiga yang bersangkutan dengan pengiriman barang-barang yang telah Anda beli dari <em>website</em> TOKO1001. Dalam situasi yang dikecualikan, TOKO1001 mungkin akan membutuhkan Anda untuk menyingkapkan informasi pribadi Anda, termasuk saat dimana adanya sesuai perintah pengadilan atau undang-undang yang berlaku bukti bahwa penyingkapan informasi dapat mencegah ancaman hidup atau kesehatan, atau untuk kepentingan hukum. TOKO1001 memiliki komitmen untuk mematuhi the Privacy Act and the National Privacy Principles kebijakan perundangundangan yang berlaku. <strong>&nbsp;</strong></p>
		<p><strong>Bila Anda yakin bahwa privasi Anda telah dilanggar oleh TOKO1001, harap hubungi Kami di </strong><strong>cc@toko1001.id</strong><strong> dan Kami akan menyelesaikan masalah tersebut. </strong></p>
		<p><strong>&nbsp;</strong></p>
		<p><strong>Dibawah Umur</strong></p>
		<p>TOKO1001 &nbsp;tidak menjual produk untuk pembelian yang dilakukan oleh anak-anak. Jika Anda berusia di bawah 17 tahun, Anda harus menggunakan <em>website</em> Kami dengan keterlibatan dan pendampingan orang tua atau wali.</p>
		<p>&nbsp;</p>
		<p><strong>Pengumpulan Data Komputer</strong></p>
		<p>TOKO1001 atau penyedia layanan resmi Kami mungkin menggunakan <em>cookies</em>, <em>web beacons</em>, dan teknologi serupa lainnya untuk menyimpan informasi dalam rangka memberi Anda pengalaman yang lebih baik, lebih cepat, lebih aman dan personal ketika Anda menggunakan Layanan dan / atau mengakses <em>Website</em> / <em>Platform</em>.</p>
		<p>Ketika Anda mengunjungi TOKO1001, <em>server</em> perusahaan kami akan secara otomatis menyimpan informasi bahwa <em>browser</em> Anda mengunjungi sebuah <em>website</em>. Data ini mungkin termasuk:</p>
		<ul>
		    <li>Alamat IP komputer Anda</li>
		    <li>Tipe <em>browser</em></li>
		    <li>Halaman Web yang Anda kunjungi      sebelum Anda datang ke <em>website</em> /      <em>platform</em> Kami</li>
		    <li>Halaman-halaman dalam <em>Website</em> / <em>Platform</em> TOKO1001 yang      Anda kunjungi</li>
		    <li>Waktu yang dihabiskan pada      halaman tersebut, barang dan informasi yang dicari pada <em>Website</em> / <em>Platform</em>, waktu      akses dan tanggal, dan statistik lainnya.</li>
		</ul>
		<p>Informasi tersebut diatas dikumpulkan akan digunakan untuk analisa dan evaluasi guna membantu Kami memperbaiki <em>Website / Platform </em>Kami, dan meningkatkan pelayanan-pelayanan beserta produk-produk yang Kami sediakan. Data-data ini tidak akan digunakan berhubungan dengan informasi pribadi lainnya.</p>
		<p><em>Cookies</em> adalah file teks kecil (biasanya terdiri dari huruf dan angka) yang ditempatkan dalam memori <em>browser</em> atau perangkat ketika Anda mengunjungi <em>website</em> atau melihat pesan. <em>Cookies</em> memungkinkan kita untuk mengenali perangkat atau <em>browser</em> tertentu dan membantu kita dalam personalisasi konten yang sesuai dengan minat Anda lebih cepat, dan untuk membuat Layanan dan Landasan kita lebih nyaman dan berguna bagi Anda.</p>
		<p>Sebagai bagian di atas, TOKO1001 dapat menggunakan fitur Google Analytics berdasarkan pada Tampilan Iklan, termasuk (walaupun tidak terbatas) pada hal berikut: Remarketing, Google Display Network Impression Reporting, integrasi DoubleClick Campaign Manager, dan Demografi Google Analytics serta Bunga Pelaporan. Dengan menggunakan Google Ads Setting <a href="https://www.google.com/settings/ads"><strong>(https://www.google.com/settings/ads)</strong></a> , Anda dapat memilih Display Advertising dari Google Analytics dan menyesuaikan iklan Jaringan Display Google.</p>
		<p>TOKO1001 juga menggunakan Remarketing dengan Google Analytics untuk mengiklankan secara online; vendor pihak ketiga, termasuk Google, dapat menampilkan iklan online TOKO1001 di Internet. TOKO1001 dan vendor pihak ketiga, termasuk Google, menggunakan cookies pihak pertama (seperti cookie Google Analytics) dan cookies pihak ketiga (seperti cookies DoubleClick ) bersama-sama untuk menginformasikan, mengoptimalkan, dan melayani iklan berdasarkan kunjungan terakhir pengunjung situs TOKO1001, serta melaporkan bagaimana hasil dari tayangan iklan, kegunaan lain dari layanan iklan, dan interaksi dengan tayangan iklan tersebut dan layanan iklan yang terkait dengan kunjungan ke TOKO1001.</p>
		<p>&nbsp;</p>
		<p><strong>No Spam, Spyware, or Virus</strong></p>
		<p><em>Spam</em>, <em>Spyware</em>, atau <em>virus</em> tidak diperbolehkan dalam <em>Website </em>/ <em>Platform</em>. Harap mengatur dan menjaga preferensi komunikasi Anda sehingga kami dapat mengirimkan komunikasi seperti yang Anda inginkan. Anda tidak memiliki ijin atau tidak diizinkan untuk menambahkan pengguna lain (bahkan pengguna yang telah membeli <em>item</em> dari Anda) ke milis Anda (<em>email</em> atau surat fisik) tanpa persetujuan mereka. Anda tidak boleh mengirim pesan yang mengandung <em>spam</em>, <em>spyware</em> atau <em>virus</em> melalui <em>Platform</em>. Jika Anda ingin melaporkan pesan yang mencurigakan, silahkan hubungi kami di alamat email kami di bawah.</p>
		<p><strong>&nbsp;</strong></p>
		<p><strong>Perubahan dalam Kebijakan Privasi</strong></p>
		<p>TOKO1001 dapat secara berkala meninjau kecukupan Kebijakan Privasi ini. Kami berhak untuk memodifikasi dan mengubah kebijakan privasi setiap saat. Setiap perubahan kebijakan ini akan dipublikasikan pada <em>Website /</em> <em>Platform</em>.</p>
		<p>&nbsp;</p>
		<p><strong>Hak TOKO1001</strong></p>
		<p>ANDA MEMAHAMI DAN MENYETUJUI BAHWA TOKO1001 MEMILIKI HAK UNTUK MENGUNGKAPKAN INFORMASI PRIBADI ANDA PADA SETIAP HUKUM, PERATURAN, PEMERINTAHAN, PAJAK, PENEGAKAN HUKUM ATAU PEMERINTAH ATAU PEMILIK HAK TERKAIT, JIKA TOKO1001 MEMILIKI ALASAN WAJAR YANG DAPAT DIPERCAYA BAHWA PENGUNGKAPAN INFORMASI PRIBADI ANDA DIPERLUKAN UNTUK KEWAJIBAN APAPUN, SEBAGAI PERSYARATAN ATAU PENGATURAN, BAIK SUKARELA ATAU WAJIB, SEBAGAI AKIBAT DARI PESANAN, PEMERIKSAAN DAN / ATAU PERMINTAAN PIHAK TERKAIT. SEJAUH DIIZINKAN OLEH HUKUM YANG BERLAKU, DALAM HAL INI ANDA SETUJU UNTUK TIDAK MELAKUKAN TUNTUTAN APAPUN TERHADAP TOKO1001 UNTUK PENGUNGKAPAN INFORMASI PRIBADI ANDA.</p>
		<p>&nbsp;</p>
		<p><strong>Menghubungi TOKO1001</strong></p>
		<p>Jika Anda ingin menarik persetujuan Anda dalam penggunaan informasi pribadi, meminta akses dan / atau koreksi dari informasi pribadi Anda, memiliki pertanyaan, komentar atau keluhan, atau memerlukan bantuan mengenai hal-hal teknis atau terkait dengan cookies, jangan ragu untuk hubungi kami (dan <em>Data Protection Officer</em> kami) di <em>cc@toko1001.id</em></p>
	    </div>

	    <?php
	    break;
	case "syarat-dan-ketentuan":
	    ?>
	    <div class="content-header" style="text-align:justify;">
		<h2 class="page-header">Syarat & Ketentuan</h2>
		<p>Selamat datang di <strong>www.toko1001.id</strong></p>
		<p>Halaman ini menjelaskan tentang persyaratan dan kondisi penggunaan dimana Anda dapat menggunakan <em>Website / </em><em>Platfor</em><em>m TOKO1001 </em>&nbsp;(akses <em>website</em>/<em>Platform </em>dan penggunaan layanan). Dengan menggunakan <em>Website / Platform</em>, menunjukkan bahwa anda setuju untuk menerima persyaratan tersebut dan setuju untuk mematuhinya. Apabila Anda tidak menyetujui salah satu, sebagian, atau seluruh isi Syarat dan Ketentuan Penggunaan ini, maka Anda jangan/berhenti mengakses dan/atau menggunakan <em>Website / &nbsp;Platform</em> atau Layanan ini.</p>
		<p>Dengan mendaftar dan/atau menggunakan situs www.toko1001.id, maka Pengguna/Pelanggan dianggap telah membaca, mengerti, memahami dan menyetujui semua isi dalam Syarat dan Ketentuan Penggunaan. Syarat &amp; ketentuan ini merupakan bentuk kesepakatan yang dituangkan dalam sebuah perjanjian yang sah antara Pengguna/Pelanggan dengan TOKO1001.</p>
		<p><br /> Akses atas <em>password</em> dan penggunaan password dilindungi dan/atau area tertentu yang diterlindungi pada Platform dan/atau penggunaan Layanan dibatasi hanya untuk Pelanggan yang memiliki akun saja. Anda tidak diperbolehkan memperoleh dan/atau berusaha memperoleh akses tidak sah ke area Platform dan/atau Layanan ini, atau ke area informasi lain yang dilindungi, dengan cara apapun yang tanpa ijin penggunaan khusus oleh Kami. Pelanggaran terhadap ketentuan ini merupakan pelanggaran yang didasarkan pada hukum Indonesia dan/atau undang-undang serta peraturan yang berlaku.</p>
		<p><strong>TOKO1001.id</strong> dioperasikan oleh PT. Deltamas Mandiri Sejahtera . Kami berhak untuk mengubah ketentuan ini tanpa ada pemberitahuan terlebih dahulu, dengan mengubahnya secara online. Anda bertanggung jawab untuk selalu memeriksa ketentuan yang ada disini secara berkala untuk mendapatkan pemberitahuan yang tepat waktu. Setelah adanya perubahan, apabila Anda tetap menggunakan situs secara berkala berarti Anda setuju terhadap ketentuan yang diubah. <br /> <br /> Untuk menggunakan <em>Website / Platform</em>, anda harus berusia minimal 17 tahun dan memiliki kartu debit atau kredit yang sah dan dikeluarkan oleh bank yang berkerja sama dengan kami. <strong>Apabila Anda berusia di bawah 17 tahun:</strong> Anda harus memperoleh persetujuan dari orang tua atau wali Anda, penerimaan atau persetujuan orang tua/wali terhadap Persyaratan Penggunaan ini beserta persetujuan mereka untuk mengambil tanggung jawab untuk: (i) tindakan Anda; (ii) biaya yang terkait dengan penggunaan setiap Layanan atau pembelian Produk; dan (iii) penerimaan dan kepatuhan Anda sesuai dengan Syarat &amp; Ketentuan Penggunaan ini.</p>
		<p><strong>Jika Anda tidak memiliki izin dari orang tua atau wali, Anda harus berhenti menggunakan/mengakses platform dan berhenti menggunakan Layanan ini.</strong></p>
		<p><strong>&nbsp;</strong></p>
		<p><strong>1. Definisi &amp; Interpretasi</strong></p>
		<p>Kecuali jika didefinisikan lain, definisi dan ketentuan pada Lampiran 1 akan berlaku untuk Syarat Penggunaan ini.</p>
		<p>&nbsp;</p>
		<p><strong>2. Penggunaan umum Layanan dan/atau akses atas Platform </strong></p>
		<p><strong>&nbsp;</strong></p>
		<p>2.1 <strong>Pedoman penggunaan Platform dan/atau Layanan</strong>: Anda setuju untuk mematuhi setiap dan semua pedoman, pemberitahuan, aturan operasi dan kebijakan dan instruksi yang berkaitan dengan penggunaan Layanan dan/atau akses ke Platform, serta setiap perubahan-nya, yang dikeluarkan oleh Kami, dari waktu ke waktu. Kami berhak untuk merevisi pedoman, pemberitahuan, aturan operasi, kebijakan dan instruksi sewaktu-waktu dan Anda dianggap mengetahui dan tunduk oleh setiap perubahan tersebut di atas setelah ada-nya pemberitahuan atau publikasi atas perubahan tersebut di Platform atau pemberitahuan melalui media lain.</p>
		<p><br /> 2.2 <strong>Kegiatan terlarang: Anda setuju dan TIDAK akan melakukan</strong> :</p>
		<p>(a) Berpura-pura sebagai orang lain/entitas, atau memberikan keterangan yang salah, atau mengaku sebagai orang lain atau kelompok tertentu;</p>
		<p>(b) menggunakan Platform atau Layanan untuk tujuan yang melanggar hukum (illegal);<br /> (c) berusaha untuk mendapatkan akses tidak sah atau mengganggu atau mengacaukan sistem komputer atau jaringan yang terhubung dengan Platform atau Layanan;<br /> (d) mengumumkan (<em>posting</em>), mempromosikan atau mengirimkan Materi Terlarang apapun melalui Platform atau Layanan;</p>
		<p>(e) mengganggu pemanfaatan dan pemakaian dari Platform atau Layanan;<br /> (f) menggunakan atau unggah atau meng-<em>upload</em> perangkat lunak atau material yang mengandung / dicurigai mengandung virus, komponen merusak, kode berbahaya atau komponen berbahaya dengan cara apapun yang dapat merusak data atau mengakibatkan kerusakan Platform atau mengganggu pengoperasian komputer Pelanggan lain atau perangkat <em>mobile</em> atau Platform atau Layanan; dan</p>
		<p>(g) menggunakan Platform atau Layanan diluar dari aturan / kebijakan penggunaan setiap jaringan komputer yang terhubung, setiap standar Internet yang berlaku dan hukum yang berlaku lainnya.<br /> <br /></p>
		<p>2.3 <strong>Ketersediaan Platform dan Layanan</strong>: Kami dapat meningkatkan, memodifikasi, menghentikan sementara, menghentikan penyediaan, menghapus, baik secara keseluruhan atau sebagian dari Platform atau Layanan, tanpa memberikan alasan &amp; pemberitahuan sebelumnya, dan tidak bertanggung jawab jika peningkatan, modifikasi, suspensi atau penghapusan tersebut mencegah Anda mengakses Platform atau bagian dari Layanan.</p>
		<p><br /> 2.4 <strong>Berhak, namun tidak berkewajiban, untuk memantau konten</strong>: Kami berhak, tetapi tidak wajib untuk:<br /> (a) memantau, menyaring atau mengontrol setiap kegiatan, isi atau materi pada Platform dan / atau melalui Layanan. Atas kebijakan kami sendiri, kami dapat menyelidiki setiap pelanggaran terhadap syarat dan ketentuan yang tercantum di sini dan dapat mengambil tindakan apapun yang dianggap sesuai atau tepat;</p>
		<p>(b) mencegah atau membatasi akses Pelanggan untuk Platform dan/atau Jasa;<br /> (c) melaporkan kegiatan yang dicuragai sebagai pelanggaran terhadap hukum yang berlaku, undang-undang atau peraturan kepada pihak yang berwenang serta bekerja sama dengan pihak berwenang tersebut; dan/atau</p>
		<p>(d) meminta informasi dan data dari Anda sehubungan dengan penggunaan Layanan dan/atau akses Platform setiap saat, dan sebagai pelaksanaan hak Kami jika Anda menolak untuk memberikan/mengungkapkan informasi/data tersebut, atau jika Anda memberikan informasi tidak akurat, menyesatkan, penipuan data dan/atau informasi atau jika kami memiliki alasan yang cukup mencurigai Anda telah menyediakan informasi tidak akurat, menyesatkan atau penipuan data dan/atau informasi.</p>
		<p><br /> 2.<strong>5 Kebijakan Privasi</strong>: Penggunaan Anda atas Layanan dan/atau akses Anda ke Platform juga tunduk pada Kebijakan Privasi sebagaimana tercantum di http://www.toko1001.id/syarat-dan-ketentuan.html<br /> <br /></p>
		<p>2.6 <strong>Syarat &amp; Ketentuan Penjualan</strong>: Pembelian Produk apapun akan tunduk pada Syarat &amp; Ketentuan Penjualan sebagaimana tercantum di http://www.toko1001.id/syarat-dan-ketentuan.html Jika Anda menggunakan Voucher, maka Syarat &amp; Ketentuan Voucher sebagaimana dimaksud pada http://www.toko1001.id/Bantuan-FAQ.html<br /> <br /></p>
		<p>2.7 <strong>Ketentuan lain</strong>: selain Syarat Penggunaan ini, penggunaan aspek-aspek tertentu dari Materi dan Layanan, lebih komprehensif atau versi terbaru dari Materi yang ditawarkan oleh Kami atau sub-kontraktor yang ditunjuk, dapat dikenakan syarat dan ketentuan tambahan, yang akan berlaku sepenuhnya.</p>
		<p><strong>&nbsp;</strong></p>
		<p><strong>3. Penggunaan Layanan </strong></p>
		<p><strong>&nbsp;</strong></p>
		<p>3.1 <strong>Penerapan Klausul ini</strong>: Selain dari semua syarat dan ketentuan di Syarat Penggunaan ini, ketentuan dalam Pasal ini 3 merupakan ketentuan &ndash; ketentuan tambahan yang spesifik berlaku atas penggunaan Anda atas Layanan.</p>
		<p><br /> 3.2 <strong>Pembatasan</strong>: Penggunaan Layanan terbatas pada Pelanggan yang sah yang berada pada usia dewasa secara hukum dan yang memiliki kapasitas hukum untuk masuk/membuat kesepakatan/perjanjian dan mengikatkan diri berdasarkan hukum yang berlaku. Pelanggan yang melanggar syarat dan ketentuan yang tercantum di sini serta Pelanggan yang ditangguhkan secara permanen atau sementara dari penggunaan setiap Layanan tidak diperbolehkan menggunakan Layanan meskipun mereka memenuhi persyaratan Klausul 3.2 ini.</p>
		<p><br /> 3.3 <strong>Persyaratan penggunaan umum</strong>:</p>
		<p><br /> Dengan ini Anda setuju:</p>
		<p>(a) untuk selalu mengakses dan/atau menggunakan Layanan hanya untuk tujuan yang tidak melanggar hukum, dan dengan cara yang sah dan selanjutnya setuju untuk melakukan kegiatan yang berkaitan dengan Layanan dengan itikad baik; dan</p>
		<p>(b) memastikan bahwa setiap informasi atau data yang Anda berikan/umumkan/posting atau yang dimunculkan di Platform sehubungan dengan Layanan adalah akurat dan setuju untuk bertanggung jawab atas informasi dan data tersebut.</p>
		<p><br /> 3.4 <strong>Deskripsi Produk</strong>: Kami selalu berusaha untuk memberikan deskripsi yang akurat tentang Produk, namun tidak bertanggungjawab atas jaminan bahwa deskripsi tersebut akurat, terkini atau bebas dari kesalahan.</p>
		<p><br /> 3.5 <strong>Harga Produk</strong>: Semua Harga terdaftar tunduk pada pajak, kecuali dinyatakan lain. Kami berhak untuk mengubah Daftar Harga setiap saat tanpa memberikan alasan apapun atau pemberitahuan sebelumnya.</p>
		<p><br /> 3.6 Vendor Pihak Ketiga: Dengan ini Anda mengetahui bahwa atas keberadaan pihak &ndash; pihak lain selain TOKO1001 (Vendor Pihak Ketiga) yang mendaftar dan menjual produk di Platform. Produk yang dijual di Platform oleh TOKO1001 atau Vendor Pihak Ketiga dapat dicantumkan pada daftar Produk pada laman web. Untuk menghindari keraguan, setiap transaksi / kesepakatan atas penjualan produk Vendor Pihak Ketiga kepada Pelanggan, adalah kesepakatan atau perjanjian yang dibuat langsung oleh Pelanggan dan Vendor Pihak Ketiga, yang mengikat hanya antara Pelanggan dan Vendor Pihak Ketiga.</p>
		<p><strong>&nbsp;</strong></p>
		<p><strong>4. Pelanggan dengan akun TOKO1001</strong></p>
		<p>&nbsp;</p>
		<p>4.1 <strong><em>Username (email)/Password</em></strong>: Layanan &ndash; layanan tertentu yang tersedia di Platform memerlukan pembuatan akun atau memerlukan Anda untuk memberikan Data Pribadi Anda. Jika Anda ingin membuat akun TOKO1001, <em>Username (email)</em> dan <em>Password</em> dapat berupa: (i) ditentukan dan diberikan oleh Kami kepada Anda; atau (ii) Anda tentukan/berikan dan diterima oleh Kami berdasarkan kebijakan mutlak Kami sehubungan dengan penggunaan Layanan dan/atau akses ke Platform yang relevan. Kami sewaktu-waktu dengan kebijakan kami, meminta Anda memperbaharui Data Pribadi Anda atau dapat membatalkan <em>Username (email)</em> dan/atau <em>Password</em> tanpa pemberitahuan terlebih dahulu dan tidak bertanggung jawab atas Kerugian (apabila ada) yang diderita oleh Anda, atau disebabkan oleh Anda, atau yang timbul dari pembatalan, atau sehubungan dengan pembatalan, atau dari dasar/alasan permintaan pembatalan atau pembatalan tersebut. Anda dengan ini setuju untuk merubah/memperbaharui <em>Password</em> Anda secara berkala dan untuk menjaga <em>Username (email)</em> dan <em>Password</em> secara rahasia dan bertanggung jawab atas keamanan akun Anda dan bertanggung jawab atas pengungkapan atau penggunaan (apakah penggunaan tersebut diijinkan atau tidak) dari <em>Username (email)</em> dan/atau <em>password</em> Anda. Anda harus segera hubungi Kami jika Anda mengetahui atau memiliki alasan untuk mencurigai bahwa kerahasiaan <em>Username (email)</em> dan/atau <em>Password</em> Anda terganggu atau jika terjadi penggunaan yang tidak sah dari <em>Username (email)</em> dan/atau <em>Password</em> atau jika Data Pribadi Anda butuh diperbaharui.</p>
		<p><br /> 4.2 <strong>Pengakuan penggunaan/akses</strong>: Anda setuju dan mengetahui bahwa setiap penggunaan Layanan dan/atau akses ke Platform dan informasi, data atau komunikasi merujuk <em>Username (email)</em> dan <em>Password</em> anda akan dianggap, sebagai:</p>
		<p>(a) akses terhadap Platform dan/atau penggunaan Layanan oleh Anda; atau<br /> (b) <em>Posting</em>, pengumuman/pengungkapan, pemberian: informasi, data atau komunikasi, yang sah dikeluarkan oleh Anda.</p>
		<p>Dengan ini Anda setuju untuk tunduk atas akses Platform dan/atau penggunaan Layanan (apakah akses atau penggunaan tersebut dengan ijin Anda atau tidak) dan Anda setuju bahwa kami berhak (tetapi tidak berkewajiban) untuk berpegangan dan menganggap bahwa penggunaan atas nama <em>Username (email) </em>&nbsp;dan/atau <em>Password</em> Anda adalah dilakukan oleh Anda sendiri (sepanjang tidak ada laporan pemakaian tidak sah sebelumnya atas akun Anda yang telah terbukti) dan berhak menyimpulkan bahwa pengunaan/kegiatan tersebut dilakukan atau dikirimkan oleh Anda dan meminta pertanggungjawaban Anda. Anda selanjutnya setuju dan mengakui bahwa Anda terikat dan bertanggungjawa sepenuhnya mengganti atau atas kerugian yang timbul disebabkan setiap penggunaan setiap Layanan dan/atau akses ke Platform atas nama <em>Username (email)</em> dan <em>Password</em> Anda.</p>
		<p><strong>&nbsp;</strong></p>
		<p><strong>5. Kekayaan Intelektual </strong></p>
		<p><strong>&nbsp;</strong></p>
		<p>5.1 <strong>Kepemilikan</strong>: Kekayaan Intelektual (HAKI) dalam atau atas Platform serta atas setiap Materi, dimiliki, atau berlisensi atau dikuasai oleh kami, atau pemberi lisensi kami atau penyedia layanan kami. Kami berhak untuk menjaga Kekayaan Intelektual sepenuhnya demi hukum.</p>
		<p><br /> 5.2 <strong>Penggunaan Terbatas</strong>: Tidak ada bagian-bagian dari Platform atau Material yang dapat direproduksi, direkayasa, <em>decompiled</em>, dibongkar, dipisahkan, diubah, didistribusikan, <em>republished</em>, ditampilkan, disiarkan, ditautkan (<em>hyperlinked</em>), direfleksikan (<em>mirrored</em>), disusun (<em>framed</em>), ditransfer atau dikirim dengan cara apapun atau disimpan/dipasang dalam suatu sistem pencarian informasi atau dipasang pada suatu server, sistem atau peralatan, tanpa izin tertulis sebelumnya dari kami atau dari pemilik hak cipta yang bersangkutan. Sesuai dengan Pasal 5.3, izin hanya akan diberikan kepada Anda untuk men-<em>download</em>, mencetak atau menggunakan Material untuk penggunaan pribadi dan non-komersial, dengan ketentuan Anda tidak mengubah Material, Kami atau pemilik Hak Cipta yang bersangkutan adalah pemegang semua/tiap Hak Cipta dan Hak Cipta kepemilikan lainnya yang terkandung pada Material.</p>
		<p><br /> 5.3 <strong>Merek Dagang (<em>Trademarks</em>):</strong> <em>Trademarks</em> yang terdaftar dan yang tidak terdaftar atas nama Kami atau atas nama pihak ketiga.</p>
		<p>Tidak ada pada Platform dan Syarat Penggunaan ini yang bisa ditafsirkan sebagai pemberian, secara tersirat atau tidak, atau di tafsirkan sebagai lisensi atau hak untuk menggunakan (termasuk atas <em>meta tag </em>atau "<em>hot</em>" link ke website lain) setiap <em>Trademarks</em> yang ditampilkan pada Layanan, tanpa izin tertulis dari Kami atau pemilik Trade lain yang berlaku.</p>
		<p>&nbsp;</p>
		<p><strong>6. Keterbatasan kami atas tanggung jawab dan kewajiban</strong></p>
		<p><strong>&nbsp;</strong></p>
		<p>6.1 <strong>Tidak ada pernyataan atau jaminan</strong>:</p>
		<p>Layanan, Platform dan Material yang tersedia adalah berbasis "sebagaimana adanya" dan "sebagaimana tersedia". Semua data dan/atau informasi yang terkandung dalam Platform, Layanan atau Material yang disediakan ditujukan sebagai informasi saja. Tidak ada pernyataan atau jaminan apapun, yang tersirat, tersurat maupun diatur, termasuk jaminan non-pelanggaran (<em>non-infringement</em>) atas pihak ketiga yang meliputi: hak, kepemilikan, kelayakan jual (<em>merchantability</em>), kualitas yang memuaskan atau kesesuaian untuk tujuan tertentu, sehubungan dengan Platform, Layanan atau Material. Tanpa mengesampingkan ketentuan lain yang berlaku, kami tidak menjamin: <br /> <br /></p>
		<p>(a) akurasi, ketepatan waktu, kecukupan, nilai komersial atau kelengkapan dari semua data dan/atau informasi yang terkandung dalam Platform, Layanan atau Material;<br /> (b) bahwa Platform, Layanan atau bahwa setiap Material selalu tersedia tanpa gangguan, aman atau bebas dari kesalahan atau kelalaian, atau setiap cacat yang ditemukan akan langsung diperbaiki;<br /> (c) bahwa Platform, Layanan atau Material selalu bebas dari virus komputer atau kode berbahaya lainnya, merugikan, merusak, agen, program atau macro; dan</p>
		<p>(d) keamanan atas informasi apapun yang dikirim oleh Anda atau untuk Anda melalui Platform atau Layanan, dan dengan ini Anda menerima risiko bahwa informasi apapun yang dikirim atau diterima melalui Layanan atau Platform dapat diakses oleh pihak ketiga yang tidak sah dan / atau diungkapkan oleh kami atau petugas, karyawan atau agen kepada pihak ketiga yang mengaku Anda atau mengaku bertindak di bawah otoritas Anda. Transmisi melalui Internet dan surat elektronik dapat dikenakan gangguan, transmisi pemadaman, tertunda transmisi karena lalu lintas internet atau transmisi data yang tidak benar karena sifat publik dari internet.</p>
		<p><br /> 6.2 <strong>Pengecualian pertanggungjawaban</strong>: TOKO1001 tidak bertanggung jawab kepada Anda untuk Kerugian apapun atau apapun penyebabnya (dalam bentuk apapun) yang timbul secara langsung atau tidak langsung yang terkait atas:</p>
		<p><br /> (a) akses, penggunaan dan/atau ketidakmampuan untuk menggunakan Platform atau Layanan;<br /> (b) ketergantungan Anda (mengandalkan) pada data atau informasi yang tersedia melalui Platform dan/atau melalui Layanan. Anda tidak seharusnya bertindak hanya mengandalkan data atau informasi tanpa terlebih dahulu secara independen/mandiri memverifikasi isinya;<br /> (c) sistem, server atau koneksi yang gagal, kesalahan, kelalaian, gangguan, keterlambatan dalam transmisi, virus komputer atau kode berbahaya, merugikan, merusak lainnya, <em>agent program</em> atau <em>macros</em>; dan<br /> (d) setiap penggunaan atau akses ke website lain atau halaman web yang ter-<em>link </em>dengan Platform atau terdapat <em>link</em> &ndash;nya di Platform, walaupun jika kami atau petugas kami atau agen atau karyawan kami telah diberitahukan, atau mungkin telah diantisipasi, atas kemungkinan yang sama.<br /> 6.3 Risiko Anda sendiri: Setiap risiko kesalahpahaman, kesalahan, kerusakan, biaya atau Kerugian yang diakibatkan dari penggunaan Platform, adalah sepenuhnya risiko Anda sendiri dan Lazada tidak bertanggung jawab untuk itu.</p>
		<p><strong>&nbsp;</strong></p>
		<p><strong>7. <em>Hyperlink</em>, peringatan dan iklan/<em>advertising</em> </strong></p>
		<p><strong>&nbsp;</strong></p>
		<p>7.1<strong> <em>Hyperlinks</em></strong>:</p>
		<p>Untuk kemudahan Anda, Kami dapat mencantumkan <em>hyperlink</em> ke situs-situs lain atau konten di Internet yang dimiliki atau dioperasikan oleh pihak ketiga. Situs yang terhubung atau konten tidak di bawah kendali Kami dan Kami tidak bertanggung jawab atas kesalahan, kelalaian, penundaan, pencemaran nama baik, fitnah, kebohongan, kecabulan, pornografi, tidak senonoh, ketidaktepatan atau materi yang lain yang terkandung dalam isi, atau konsekuensi mengakses, setiap situs web terkait. Setiap <em>hyperlink</em> ke situs-situs lain atau konten bukanlah merupakan bentuk dukungan atau verifikasi situs atau konten tersebut dan Anda setuju bahwa akses Anda ke atau penggunaan situs yang terhubung atau konten sepenuhnya menjadi resiko Anda sendiri.</p>
		<p><br /> 7.2 <strong>Iklan</strong>: Kami dapat melampirkan <em>banners</em>, <em>java applet</em> dan/atau bahan lain seperti untuk Platform untuk tujuan <em>advertising</em> atau iklan produk dan/atau layanan Kami atau iklan/<em>advertising</em>produk/layanan Vendor Pihak Ketiga Kami. Untuk menghindari keraguan, Anda tidak mempunyai hak untuk mendapatkan pembayaran, biaya dan / atau komisi sehubungan dengan iklan tersebut atau materi promosi lainnya.</p>
		<p><strong>&nbsp;</strong></p>
		<p><strong>&nbsp;</strong></p>
		<p><strong>8. Submisi dan Informasi dari Anda </strong></p>
		<p><strong>&nbsp;</strong></p>
		<p>8.1 <strong>Submisi oleh Anda</strong>: Anda memberikan Kami &nbsp;ijin/lisensi non-eksklusif untuk menggunakan materi atau informasi yang Anda kirimkan ke Platform dan/atau berikan kepada kami, termasuk tetapi tidak terbatas pada, pertanyaan, ulasan, komentar, dan saran (secara kolektif disebut "Submisi"). Ketika Anda me-<em>posting</em> Submisi, Anda juga memberikan kami hak untuk menggunakan nama yang Anda kirimkan atau Username sehubungan dengan ulasan tersebut, komentar, atau konten lainnya. Anda tidak akan menggunakan alamat e-mail palsu, berpura-pura menjadi orang lain selain diri sendiri atau menyesatkan kita atau pihak ketiga mengenai asal <em>Submissions</em> apapun. Kita mungkin, tapi tidak diwajibkan untuk, mempublikasikan, menghapus atau mengedit Submisi Anda.<br /> <br /></p>
		<p>8.2 <strong>Izin untuk menerima <em>e-mail</em></strong>: Anda menyetujui dan mengesahkan penggunaan oleh Kami dari setiap informasi yang diberikan oleh Anda (termasuk Data Pribadi) untuk keperluan pengiriman informasi dan promosi e-mail kepada Anda. Persetujuan Anda atas ketentuan Klausul ini 8.2 merupakan persetujuan Anda yang bertujuan untuk perlindungan dari <em>spam</em> (baik di Indonesia atau di tempat lain). Anda dapat memilih untuk tidak menerima e-mail promosi dengan mengklik <em>hyperlink</em> yang tersedia di promosi e-mail untuk menyatakan bahwa Anda akan berhenti berlangganan promosi <em>e-mail</em>.</p>
		<p><br /> 8.3 <strong>Kebijakan Privasi</strong>: Dengan ini Anda menyatakan bahwa Anda telah membaca dan menyetujui Kebijakan Privasi sebagaimana dijabarkan pada <span style="text-decoration: underline;"><a href="http://www.toko1001.id/syarat-dan-ketentuan.html">http://www.toko1001.id/syarat-dan-ketentuan.html</a></span> , dan menyetujui atas pengumpulan, penggunaan dan pengungkapan Data Pribadi Anda untuk tujuan sebagai ditetapkan dalam Kebijakan Privasi tersebut.<em>&nbsp;</em></p>
		<p><strong>&nbsp;</strong></p>
		<p><strong>&nbsp;</strong></p>
		<p><strong>9. Pengakhiran </strong></p>
		<p><strong>&nbsp;</strong></p>
		<p>9.1 <strong>Pengakhiran oleh Kami</strong>: Berdasarkan kebijakan Kami, segera setelah memberikan pemberitahuan, kami dapat menghentikan penggunaan Platform dan/atau Layanan dan/atau menonaktifkan <em>Username</em> dan <em>Password</em> Anda. Kami dapat menghentikan akses Anda ke Platform dan/atau Layanan (atau bagian daripadanya) dengan alasan tertentu, termasuk jika terjadi pelanggaran terhadap Syarat &amp; Ketentuan Penggunaan ini atau di mana jika kami percaya bahwa Anda telah melanggar atau bertindak tidak sesuai dengan syarat atau ketentuan yang berlaku di sini, atau jika dalam pendapat kami atau pendapat otoritas regulasi, layanan tersebut yang berkaitan dengan Platform tidak untuk dapat diberikan/dilaksanakan.</p>
		<p><br /> 9.2 <strong>Pengakhiran oleh Anda</strong>: Anda dapat mengakhiri Syarat &amp; Ketentuan Penggunaan ini dengan memberikan pemberitahuan secara tertulis dalam waktu 7 (tujuh) hari kerja kepada Kami.<br /> <br /></p>
		<p>9.3 <strong>Untuk tujuan Penghentian ini</strong>, Anda dan TOKO1001 sepakat untuk mengesampingkan ketentuan yang diatur dalam Pasal 1266 KUHPerdata Indonesia sehingga pengakhiran akan dilakukan tanpa memerlukan persetujuan atau keputusan pengadilan atau lembaga lain dalam wilayah Republik Indonesia.</p>
		<p><strong>&nbsp;</strong></p>
		<p><strong>&nbsp;</strong></p>
		<p><strong>10. Pemberitahuan </strong></p>
		<p><strong>&nbsp;</strong></p>
		<p>10.1 <strong>Pemberitahuan dari Kami</strong>: Semua pemberitahuan atau komunikasi lainnya yang diberikan kepada Anda:</p>
		<p>(a) dikomunikasikan melalui media cetak atau elektronik yang Kami kirimkan dalam suatu tanggal publikasi atau siaran yang telah kita pilih; atau</p>
		<p>(b) dikirim melalui pos atau disampaikan pada alamat terakhir Anda, dan akan dianggap telah diterima oleh Anda pada hari pengiriman tersebut atau pada hari diterimanya.<br /> <br /></p>
		<p>10.2 <strong>Pemberitahuan dari Anda</strong>: Anda hanya dapat memberikan pemberitahuan kepada Kami secara tertulis dikirim ke alamat atau alamat e-mail kami ditunjuk, dan Kami akan menganggap Anda telah menerima pemberitahuan tersebut hanya pada saat tanggal penerimaan. Kami berusaha untuk menanggapi secara cepat terhadap setiap pemberitahuan dari Anda, namun Kami tidak dapat menjamin bahwa kami akan selalu merespon dengan kecepatan yang konsisten setiap saat.</p>
		<p><br /> 10.3 <strong>Media lain</strong>: Kendati diatur dalam Klausul 10.1 dan 10.2 diatas, Kami mungkin sewaktu-waktu dapat memilih media atau cara yang lain dalam memberikan pemberitahuan (termasuk namun tidak terbatas pada <em>e-mail</em> atau bentuk lain dari komunikasi elektronik) yang waktu atau saat dianggap telah diterima pada saat pemberitahuan tersebut diberikan.</p>
		<p><strong>&nbsp;</strong></p>
		<p><strong>11. Umum </strong></p>
		<p><strong>&nbsp;</strong></p>
		<p>11.1 <strong>Hak kumulatif dan remedi</strong>: Kecuali ditentukan lain dalam Syarat &amp; Ketentuan Penggunaan ini, klausa - klausa Syarat &amp; Ketentuan Penggunaan ini dan hak dan remedi Kami berdasarkan Syarat &amp; Ketentuan Penggunaan ini, adalah kumulatif dan tanpa prasangka serta hak atau remedi Kami mungkin kami miliki berdasarkan hukum atau keadilan, dan tidak ada ketentuan yang akan menghalangi atau mencegah hak Kami dan upaya hukum atau remedi Kami berdasarkan hukum yang berlaku.</p>
		<p>&nbsp;</p>
		<p>11.2 <strong>No <em>Waiver</em></strong>: Kegagalan Kami untuk menegakkan Syarat &amp; Ketentuan Penggunaan tidak dapat diartikan sebagai diabaikannya dari ketentuan - ketentuan ini, dan kegagalan tersebut tidak akan mempengaruhi hak Kami selanjutnya untuk menegakkan Syarat &amp; Ketentuan Penggunaan ini. Kami akan tetap berhak menggunakan hak dan remedi/upaya hukum Kami dalam setiap kondisi lain di mana Anda melanggar Syarat Penggunaan ini.</p>
		<p><br /> 11.3 <strong>Severability:</strong> Jika suatu saat ada ketentuan pada Syarat &amp; Ketentuan Penggunaan ini yang menjadi ilegal, tidak sah atau tidak dapat diterapkan dalam hal apapun, maka legalitas, validitas dan keberlakuan dari ketentuan lainnya dalam Syarat &amp; Ketentuan Penggunaan ini tidak akan terpengaruh atau berkurang karenanya, dan akan terus berlaku.</p>
		<p><br /> 11.4 <strong>Hak Pihak Ketiga</strong>: Seseorang atau pihak yang bukan merupakan pihak yang tunduk pada Syarat &amp; Ketentuan Penggunaan ini, tidak berhak berdasarkan hukum apapun dan yurisdiksi manapun untuk menjalankan ketentuan dari Syarat &amp; Ketentuan Penggunaan ini. Untuk menghindari keraguan, tidak ada dalam Klausa ini yang akan mempengaruhi hak-hak dari setiap pengalihan yang diizinkan atas Syarat &amp; Ketentuan Penggunaan ini.</p>
		<p><br /> 11.5 <strong>Hukum yang Berlaku</strong>: Penggunaan Platform dan/atau Layanan dan Syarat &amp; Ketentuan Penggunaan ini akan diatur oleh dan ditafsirkan sesuai dengan hukum Republik Indonesia dan pada yurisdiksi eksklusif pengadilan Jakarta Utara.</p>
		<p><br /> 11.6 <strong>Bantuan Hukum</strong>: Kami dapat mencari atau menggunakan bantuan hukum dengan segera berdasarkan itikad baik untuk menangani pelanggaran atau tidak dipatuhinya Syarat &amp; Ketentuan Pengunaan ini adalah sedemikian rupa sehingga perintah penahanan sementara atau langsung ganti-rugi lainnya adalah satu-satunya yang sesuai atau memadai.</p>
		<p><br /> 11.7 <strong>Perubahan</strong>: Kami dengan pemberitahuan melalui Platform atau dengan metode lain seperti kami inginkan (yang mungkin termasuk pemberitahuan melalui <em>e-mail</em>), dapat merubah syarat &amp; ketentuan pada Syarat &amp; Ketentuan Penggunaan yang berlaku pada tanggal yang kami tentukan melalui cara di atas. Jika Anda menggunakan Platform atau Layanan setelah tanggal tersebut, Anda dianggap telah menerima perubahan tersebut. Jika Anda mau menerima atau penolakan perubahan tersebut, Anda harus berhenti akses atau menggunakan Platform dan Layanan dan mengakhiri Syarat &amp; Ketentuan Penggunaan ini. Hak kami untuk merubah Syarat &amp; Ketentuan Penggunaan di tersebut di atas dapat dilaksanakan tanpa persetujuan dari setiap orang atau badan yang bukan merupakan pihak berdasarkan Syarat &amp; Ketentuan Penggunaan ini.</p>
		<p><br /> 11.8 <strong>Koreksi kesalahan</strong>: Setiap kesalahan ketik, administrasi atau kesalahan lainnya atau kelalaian dalam penerimaan, faktur atau dokumen lainnya yang terjadi pada sisi Kami akan menjadi bahan perbaikan kami yang dilakukan oleh kami.</p>
		<p><br /> 11.9 <strong>Mata uang</strong>: Uang yang berkaitan dengan Syarat &amp; Ketentuan Penggunaan ini adalah dalam mata uang Rupiah Indonesia (IDR).</p>
		<p><br /> 11.10 <strong>Seluruh kesepakatan</strong>: Syarat &amp; Ketentuan Penggunaan harus merupakan keseluruhan perjanjian antara Anda dan kami berkaitan dengan materi pokok dalam perjanjian dan menggantikan dan menggantikan secara penuh semua pemahaman, komunikasi dan perjanjian sebelumnya sehubungan dengan materi pokok dalam perjanjian.</p>
		<p><br /> 11.11 <strong>Mengikat dan konklusif</strong>: Anda mengakui dan menyetujui bahwa setiap catatan (termasuk catatan dari setiap percakapan telepon terkait dengan Layanan, jika ada) dikelola oleh kami atau penyedia layanan kami yang berhubungan dengan atau yang berhubungan dengan Platform dan Layanan bersifat mengikat dan konklusif pada Anda untuk semua tujuan apapun dan menjadi bukti dari setiap informasi dan / atau data yang dikirimkan antara kami dan Anda. Anda setuju bahwa semua catatan tersebut diterima sebagai bukti dan bahwa Anda tidak akan menantang atau membantah diterimanya, kehandalan, akurasi atau keaslian catatan tersebut dengan alasan karena catatan tersebut dalam bentuk elektronik atau output dari sistem komputer.</p>
		<p><br /> 11.12 <strong>Sub-kontraktor dan delegasi</strong>: Kami berhak untuk mendelegasikan atau sub-kontrak kinerja dari setiap fungsi kami sehubungan dengan Platform dan / atau Jasa dan berhak untuk menggunakan penyedia layanan, subkontraktor dan / atau agen pada seperti hal sebagaimana kami anggap pantas.</p>
		<p><br /> 11.13 <strong>Pengalihan</strong>: Anda tidak dapat mengalihkan hak dan kewajiban Anda atas Syarat &amp; Ketentuan Penggunaan tanpa izin tertulis sebelumnya dari kami.</p>
		<p><br /> 11.14 <strong><em>Force Majeure</em></strong>: Kami dilepaskan dari perrtanggungjawaban atas wanprestasi, kesalahan, gangguan atau keterlambatan dalam pelaksanaan kewajiban atau untuk setiap ketidakakuratan, atau ketidaksesuaian di Platform dan / atau isi Layanan, jika hal tersebut disebabkan, secara keseluruhan atau sebagian, langsung atau tidak langsung, oleh suatu peristiwa atau kegagalan yang disebabkan <em>Force Majeur</em> atau Keadaan Memaksa.</p>
		<p><strong>&nbsp;</strong></p>
		<p><strong>Lampiran 1 </strong></p>
		<p><br /> <strong>Definisi dan Interpretasi </strong></p>
		<p>&nbsp;</p>
		<p>1. <strong>Definisi.</strong> Kecuali ditentukan lain, istilah berikut akan memiliki arti sebagai berikut dalam Syarat &amp; Ketentuan Penggunaan ini:</p>
		<p><br /> 1.1 "<strong>Pelanggan</strong>" memiliki arti yang sama seperti istilah dalam Syarat &amp; Ketentuan Penjualan.</p>
		<p><br /> 1.2 "<strong>Kekayaan Intelektual</strong>" berarti semua hak cipta, paten, inovasi utilitas, merek dagang dan merek layanan, indikasi geografis, nama domain, hak desain tata letak, desain terdaftar, hak desain, hak database, nama dagang atau bisnis, hak melindungi rahasia dagang dan rahasia informasi, hak melindungi <em>goodwill</em> dan reputasi, dan semua hak kepemilikan yang sama atau terkait lainnya dan semua aplikasi yang sama, baik saat ada atau dibuat di masa depan, di mana saja di dunia, apakah terdaftar atau tidak, dan semua manfaat, hak istimewa, hak untuk menuntut, memulihkan kerusakan dan mendapatkan bantuan atau solusi lainnya untuk setiap masa lalu, pelanggaran saat ini atau masa depan, penyalahgunaan atau pelanggaran hak-hak tersebut di atas.</p>
		<p><br /> 1.3 "<strong>TOKO1001 Indemnitees</strong>" berarti TOKO1001, afiliasinya dan semua yang masing-masing petugas, karyawan, direktur, agen, kontraktor dan penerima.</p>
		<p>&nbsp;</p>
		<p>1.4 "<strong>TOKO1001</strong>", "<strong>Kita</strong>", dan "<strong>Kami</strong>" merujuk kepada PT. Deltamas Mandiri Sejahtera, Sebuah perusahaan yang didirikan berdasarkan hukum Republik Indonesia dan beralamat terdaftar di Jl.&nbsp; Indokarya 1 Blok D Kav. No. 1, Sunter, Papanggo, Jakarta Utara 14340.</p>
		<p><br /> 1.5 "<strong>Daftar Harga</strong>" berarti harga produk yang tercantum untuk dijual kepada Pelanggan, sebagaimana tercantum pada Platform.</p>
		<p>&nbsp;</p>
		<p>1.6 "<strong>Kerugian</strong>" berarti semua penalti, kerugian, jumlah penyelesaian, biaya (termasuk biaya hukum dan biaya pengacara), biaya, pengeluaran, aksi, proses, klaim, tuntutan dan kewajiban lainnya, yang dapat di duga atau tidak.</p>
		<p>&nbsp;</p>
		<p>1.7 "<strong>Material</strong>" berarti, secara kolektif, semua halaman web pada Platform, termasuk informasi, gambar, link, suara, gambar, video, software, aplikasi dan bahan lain yang ditampilkan atau tersedia pada Platform dan fungsi atau layanan yang disediakan di Platform.</p>
		<p>&nbsp;</p>
		<p>1.8 "<strong>Pesanan</strong>" berarti pesanan Anda untuk produk melalui Platform sesuai dengan Syarat &amp; Kondisi Penjualan.</p>
		<p><br /> 1.9 "<strong>Password</strong>" mengacu pada password yang valid milik Pelanggan yang memiliki akun TOKO1001 yang dapat digunakan dengan Username untuk mengakses Platform dan / atau Layanan terkait.</p>
		<p><br /> 1.10 "<strong>Data Pribadi</strong>" berarti data, apakah benar atau tidak, yang dapat digunakan untuk mengidentifikasi, menghubungi atau mencari Anda. Data Pribadi bisa termasuk nama, alamat e-mail, alamat penagihan, alamat pengiriman, nomor telepon dan informasi kartu kredit. "Data Pribadi" akan dianggap termasuk setiap data yang Anda berikan kepada kami ketika menempatkan Order.</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p><br /> 1.11 "<strong>Platform</strong>" berarti: (a) web dan/atau versi <em>mobile</em> dari web yang dioperasikan dan/atau dimiliki oleh TOKO1001 yang saat ini terletak di <em>URL</em> berikut: www.toko1001.id; dan (b) aplikasi <em>mobile</em> yang dibuat dari waktu ke waktu oleh TOKO1001 termasuk iOS dan Android.</p>
		<p>&nbsp;</p>
		<p>1.12 "<strong>Kebijakan Privasi</strong>" berarti kebijakan privasi yang ditetapkan di <span style="text-decoration: underline;"><a href="http://www.toko1001.id/syarat-dan-ketentuan.html"><span>http://www.toko1001.id/kebijakan-privasi</span><span>.html</span></a></span>.</p>
		<p><br /> 1.13 "<strong>Produk</strong>" berarti produk (termasuk angsuran produk atau bagian dari pada itu) yang tersedia untuk dijual ke pelanggan di Platform.</p>
		<p>&nbsp;</p>
		<p>1.14 "<strong>Materi Terlarang</strong>" berarti setiap informasi, grafis, foto, data dan / atau materi lainnya yang:<br /> (a) mengandung virus komputer atau kode invasif atau merusak lainnya, program atau makro;<br /> (b) melanggar Hak Kekayaan Intelektual kami atau pihak ketiga atau hak kepemilikan lainnya;<br /> (c) memfitnah, memfitnah atau mengancam;</p>
		<p>(d) cabul, pornografi, tidak senonoh, palsu, penipuan, pencurian, berbahaya atau ilegal berdasarkan hukum yang berlaku; dan / atau</p>
		<p>(e) atau isi yang ofensif atau tidak pantas, berdasarkan pendapat kami.</p>
		<p><br /> 1.15 "<strong>Layanan</strong>" berarti layanan, informasi dan fungsi yang disediakan oleh kami di Platform.</p>
		<p><br /> 1.16 "<strong>Submission</strong>" seperti yang didefinisikan dalam Klausul 8.1 Syarat &amp; Ketentuan Penggunaan.</p>
		<p><br /> 1.17 "<strong>Syarat &amp; Kondisi Penjualan</strong>" berarti syarat dan ketentuan yang mengatur pembelian Pelanggan atas Produk dan ditetapkan di <span style="text-decoration: underline;"><a href="http://www.toko1001.id/syarat-dan-ketentuan.html"><span>http://www.toko1001.id/syarat-dan-ketentuan.html</span></a></span>.</p>
		<p><br /> 1.18 "<strong>Syarat &amp; Ketentuan Penggunaan</strong>" berarti semua ketentuan, Klausul 1-11 dan setiap Lampiran dari Syarat dan Ketentuan Penggunaan ini.</p>
		<p>&nbsp;</p>
		<p>1.19 "<strong>Trademark</strong>" berarti merek dagang, merek layanan, nama dagang dan logo yang digunakan dan ditampilkan pada Platform.</p>
		<p>&nbsp;</p>
		<p>1.20 "<strong>Username</strong>" mengacu pada nama identifikasi login unik atau kode yang mengidentifikasi Pelanggan yang memiliki akun di TOKO1001.</p>
		<p>&nbsp;</p>
		<p>1.21 "<strong>Voucher</strong>" berarti voucher yang dapat digunakan oleh Pelanggan atas pembayaran pembelian pada Platform, berdasarkan dengan syarat dan ketentuan di Platform atau di <span style="text-decoration: underline;"><a href="http://www.toko1001.id/Bantuan-FAQ.html"><span>http://www.toko1001.id/Bantuan-FAQ.html</span></a></span></p>
		<p><br /> 1.22 "<strong>Anda</strong>" merujuk pada Pelanggan, Pembeli dan/atau individu berusia di atas 17 tahun atau yang di bawah 17 tahun namun dengan perwakilan dan/atau pengawasan orang tua atau wali yang sah.<br /> <br /></p>
		<p>2. <strong>Interpretasi:</strong> Setiap acuan pada Syarat &amp; Ketentuan Penggunaan untuk setiap ketentuan harus ditafsirkan sebagai acuan bahwa ketentuan tersebut sebagaimana telah diubah, diberlakukan kembali atau diperpanjang pada waktu yang relevan. Dalam Perjanjian, setiap kali kata "termasuk", " digunakan&rdquo;, akan dianggap sebagai "tanpa batasan". Kecuali ditentukan lain, semua referensi untuk hari adalah hari kalender, dan "bulan" atau "bulanan" mengacu bulan kalender. Setiap Judul tidak di jadikan interpretasi atas Syarat &amp; Ketentuan Penggunaan ini, jika terjadi konflik atau inkonsistensi antara dua atau lebih ketentuan dalam Syarat &amp; Ketentuan Penggunaan ini, apakah ketentuan tersebut tercantum dalam dokumen yang sama atau berbeda, konflik atau inkonsistensi tersebut maka TOKO1001 akan menentukan ketentuan yang berlaku.</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p><strong>Syarat dan Ketentuan Penjualan</strong></p>
		<p><strong>&nbsp;</strong></p>
		<p><strong>1. Definisi dan Interpretasi </strong></p>
		<p><strong>&nbsp;</strong></p>
		<p>Kecuali jika didefinisikan lain, definisi dalam Lampiran 1 akan berlaku untuk Syarat &amp; Ketentuan Penjualan ini.</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p><strong>2. Pembelian Produk </strong></p>
		<p><strong>&nbsp;</strong></p>
		<p>2.1 <strong>Kepatuhan Anda</strong>: Anda setuju untuk mematuhi setiap dan semua pedoman, pemberitahuan, aturan operasional dan kebijakan dan instruksi yang berkaitan dengan pembelian produk melalui Platform, serta setiap amandemen tersebut, yang dikeluarkan oleh TOKO1001 (baik sebagai bagian dari penggunaan Platform atau dalam kaitannya dengan pembelian produk, atas nama penjual), dari waktu ke waktu. TOKO1001 berhak untuk merevisi pedoman, pemberitahuan, aturan operasional ini dan kebijakan serta instruksinya sewaktu-waktu, Anda dianggap mengetahui dan terikat oleh setiap perubahan tersebut di atas setelah dipublikasi melalui Platform.</p>
		<p><br /> 2.2 <strong>Deskripsi Produk</strong>: Penjual berupaya untuk memberikan gambaran yang akurat dari Produk, TOKO1001 tidak menjamin bahwa deskripsi tersebut akurat, terkini atau bebas dari kesalahan. Dalam hal Produk yang Anda terima adalah berbeda dari produk seperti yang dijelaskan pada Platform dan yang Anda pesan, maka Pasal 6 dari Syarat &amp; Ketentuan Penjualan berlaku.<br /> <br /></p>
		<p>2.3 <strong>Penjual</strong>: Produk yang dijual oleh "Penjual". Penjual merujuk ke pihak lain selain &nbsp;(pihak tersebut sebagaimana dimaksud dalam Syarat &amp; Ketentuan Penjualan sebagai "<strong>Vendor Pihak Ketiga</strong>"). TOKO1001 mungkin dapat menjadi "Penjual" untuk Produk tertentu. Keterangan atas Produk yang dijual oleh Vendor Pihak Ketiga dan Produk yang dijual oleh TOKO1001, dinyatakan pada halaman web Produk pada Platform. Produk yang dijual kepada Anda oleh Penjual akan diatur oleh Kontrak Pelanggan (rincian lebih lanjut di bawah ini dalam Klausul 2.6) dimana: <br /> (a) untuk Produk yang dijual oleh Vendor Pihak Ketiga, adalah merupakan kesepakatan atau perjanjian yang mengikat hanya antara Vendor Pihak Ketiga dengan Anda secara langsung; dan <br /> (b) untuk Produk yang dijual oleh TOKO1001, adalah merupakan kesepakatan atau perjanjian yang mengikat antara TOKO1001 dan Anda.</p>
		<p><br /> 2.4 <strong>Pemesanan Anda</strong>: Anda dapat memesan Order Anda dengan mengisi formulir Order pada Platform dan mengklik pada tombol "Konfirmasi Pesanan" Anda harus bertanggung jawab untuk memastikan keakuratan Pesanan Anda.</p>
		<p><br /> 2.5 <strong>Pesanan tidak dapat ditarik kembali</strong>: Semua Pesanan tidak dapat ditarik kembali setelah transmisi melalui Platform dan Penjual berhak (namun tidak berkewajiban) untuk memproses Pesanan tersebut tanpa konfirmasi lebih lanjut dan tanpa pemberitahuan lebih lanjut kepada Anda. Namun demikian, dalam keadaan tertentu sebagaimana tercantum dalam Pasal 8, Anda dapat meminta untuk membatalkan atau mengubah Pesanan, Penjual akan berusaha untuk memberikan upaya yang wajar secara komersial. Namun, terlepas dari hal tersebut, Penjual tidak diwajibkan untuk memenuhi permintaan pembatalan Anda atau mengubah Pesanan apapun.</p>
		<p>&nbsp;</p>
		<p><br /> 2.6 <strong>Hak Penjual sehubungan Pesanan</strong>: Semua Pesanan berdasarkan penerimaan Penjual atas kebijakannya sendiri dan masing-masing Pesanan diterima oleh Penjual (Pesanan yang diterima selanjutnya disebut sebagai "Kontrak Pelanggan") merupakan kontrak yang terpisah. Dengan ini Anda sepakat bahwa, jika Anda tidak menerima pemberitahuan bahwa Penjual menerima pesanan Anda, maka Penjual bukan merupakan pihak yang tunduk pada perjanjian atau janji yang mengikat secara hukum atas penjualan atau transaksi lainnya dengan Produk, dan karena itu Penjual tidak bertanggung jawab untuk setiap Kerugian yang mungkin terjadi sebagai hasilnya. Untuk menghindari keraguan, Penjual berhak untuk menolak proses atau menerima Pesanan yang diterima dari atau melalui Platform berdasarkan kebijaksanaan Penjual.</p>
		<p><br /> 2.7 <strong>Pemutusan oleh Penjual dalam hal kesalahan harga</strong>: Penjual berhak untuk mengakhiri Kontrak Pelanggan, dalam hal terjadi kesalahan pencantuman harga Produk pada Platform, di mana TOKO1001 akan membantu Penjual, untuk memberitahu Anda tentang pembatalan tersebut. Penjual memiliki hak untuk mengakhiri Kontrak Pelanggan terlepas Produk telah dikirim atau sedang dalam pengiriman dan apakah terjadi pembayaran. Atas pembatalan dari Penjual ini, maka setelah ada pembatalan Pemesanan atau pengakhiran Kontrak Pelanggan namun Pelanggan telah membayar, maka uang pembayaran Pesanan yang dibatalkan tersebut akan dikembalikan kepada Pelanggan.</p>
		<p><br /> 2.8 <strong>Garansi Produk</strong>: garansi sehubungan dengan Produk ("Produk Garansi") yang dijual dengan Kontrak Pelanggan harus seperti yang dinyatakan oleh Penjual melalui Platform, dalam "Spesifikasi" pada "Tipe Garansi" dan "Masa Garansi" atas Produk yang bersangkutan, dimana terdapat syarat dan ketentuan di dalamnya. Garansi dan persyaratannya, perbaikan dalam garansi atau persyaratannya, atau ketentuan lain yang tercantum dalam Garansi Produk, adalah berlaku kecuali secara tegas dilarang oleh hukum yang berlaku.</p>
		<p><br /> 2.9 <strong>Pengakuan Pelanggan</strong>: Anda mengakui dan menjamin bahwa Anda tidak mengandalkan dan berpegangan hanya pada setiap syarat, ketentuan, garansi, usaha, bujukan atau representasi yang dibuat oleh atau atas nama Penjual yang belum dinyatakan secara tegas dalam Kontrak Pelanggan atau atas setiap deskripsi atau ilustrasi atau spesifikasi yang tercantum dalam dokumen apapun termasuk katalog atau bahan publikasi di Platform atau oleh Penjual. Anda juga mengetahui dan sepakat bahwa pengecualian garansi, pengecualian kewajiban dan pengecualian remedi pada Syarat &amp; Ketentuan Penjualan dan Pelanggan Kontrak, akan mengalokasikan risiko antara pihak-pihak dan ijin Penjual untuk menyediakan produk pada biaya yang lebih rendah atau harga dari penjual lain bisa dan Anda setujui bahwa pengecualian tersebut adalah tanggung jawab yang wajar.</p>
		<p><br /> 2.10 <strong>Tidak ada pernyataan atau jaminan: Tanpa mengurangi Klausul 2.9:</strong></p>
		<p>2.10.1 Tidak ada Ketentuan yang dibuat atau yang akan tersirat juga tidak ada jaminan yang diberikan atau diimplikasikan sebagai jangka waktu pemakaian atau keausan Produk atau bahwa akan cocok untuk tujuan tertentu atau menggunakan dalam kondisi tertentu, walaupun untuk tujuan atau kondisi tersebut dapat diketahui atau diberitahukan kepada Penjual; <br /> 2.10.2 Penjual mengikatkan diri hanya untuk memberikan produk sesuai dengan gambaran umum di mana mereka dijual, apakah ada atau tidak ada keterangan khusus atau keterangan tertentu harus telah diberikan atau dinyatakan secara tersirat oleh hukum. Setiap keterangan khusus atau tertentu yang harus diambil hanya sebagai ekspresi pendapat Penjual. TOKO1001 maupun penjual tidak memberikan jaminan mutlak untuk kualitas, keadaan, kondisi atau kelayakan Produk; <br /> 2.10.3 TOKO1001 tidak akan bertanggungjawab untuk tindakan yang diambil oleh Pelanggan atau pihak ketiga dan konsekuensinya atas: perbaikan yang tidak tepat, perubahan dari Produk tanpa persetujuan sebelumnya dari TOKO1001, penambahan dan penyisipan komponen atau <em>spare part </em>/ sukucadang.<br /> 2.10.4 Penjual tidak akan bertanggungjawab atas setiap cacat yang timbul dari ketidaksesuaian pemakaian atau penggunaan yang salah, instalasi atau pemasangan yang salah yang dilakukan Pelanggan atau pihak ketiga, keausan wajar, kerusakan yang disengaja, kelalaian, pemakaian yang tidak wajar, penanganan atau pemakaian yang salah, perawatan yang salah, beban yang berlebihan, materi operasi dan materi pengganti yang tidak cocok, pondasi tidak cocok, kimia atau pengaruh elektronik atau listrik elektro-teknis, kegagalan atau kelalaian atau kesalahan Pelanggan atau pihak ketiga untuk mengikuti instruksi TOKO1001 atau deskripsi atau manual Produk (baik secara lisan atau tertulis), penyalahgunaan atau perubahan atau perbaikan Produk tanpa persetujuan TOKO1001 atau <em>service centre</em> resmi Produk;</p>
		<p>2.10.5 Penjual tidak bertanggung jawab atas Kerugian yang diderita oleh pihak ketiga secara langsung atau tidak langsung disebabkan oleh perbaikan atau pekerjaan perbaikan yang dilakukan tanpa persetujuan tertulis dari TOKO1001 terlebih dahulu. Pelanggan membebaskan Penjual dan TOKO1001 terhadap semua Kerugian yang timbul;</p>
		<p>2.10.6 Penjual tidak bertanggungjawab atas garansi atau jaminan atas (atau jaminan lainnya, garansi) jika total harga untuk Produk belum dibayarkan sepenuhnya dana pada jatuh tempo pembayaran; dan <br /> 2.10.7 Penjual tidak bertanggungjawab atas cacat pada Produk yang timbul setelah berakhirnya masa berlaku Garansi Produk.</p>
		<p><br /> 2.11 <strong>Kekayaan Intelektual</strong>:</p>
		<p>2.11.1 Kecuali persetujuan tertulis dari TOKO1001, Pelanggan tidak akan menghapus atau mengubah <em>trademark</em>, merek dagang, logo, hak cipta, nomor seri, label, tag atau tanda identitas lainnya, simbol atau ulasan ditempelkan dalam Produk.</p>
		<p>2.11.2 Apabila aplikasi perangkat lunak, driver atau program komputer lain dan/atau semua rincian desain lainnya, buku pegangan atau manual teknis, gambar atau data lain (secara kolektif disebut sebagai "Materi Produk") disediakan oleh Penjual kepada Pelanggan atas /berkaitan dengan Pesanan, penggunaan dan penyimpanan Materi Produk tunduk pada syarat dan ketentuan dari lisensi atau penggunaan (seperti lisensi <em>end customer</em>, pembatasan atau persyaratan penggunaan) sebagaimana ditentukan oleh penjual atau pemberi lisensinya dan yang harus digunakan sesuai syarat dan ketentuan.</p>
		<p>2.11.3 Pelanggan setuju dan mengakui bahwa Materi Produk harus tetap menjadi milik penjual atau pemberi lisensinya. Konsumen juga setuju bahwa setiap dan semua Kekayaan Intelektual yang terkandung di dalamnya atau yang berkaitan dengan Materi Produk akan tetap menjadi milik tunggal dan eksklusif dari Penjual atau pemberi lisensinya. Kecuali secara tertulis dicantumkan dalam Pesanan atau persetujuan tertulis dari TOKO1001, Pelanggan menyanggupi untuk mengembalikan materi Produk dan/atau salinannya berdasarkan permintaan TOKO1001.</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p><strong>3. Pengiriman Produk </strong></p>
		<p><strong>&nbsp;</strong></p>
		<p>3.1 <strong>Alamat</strong>: Pengiriman Produk harus dikirim ke alamat yang Anda tentukan dalam Pesanan Anda, kecuali disepakati lain.</p>
		<p>&nbsp;</p>
		<p>3.2 <strong>Biaya pengiriman &amp; pengepakan</strong>: Biaya pengiriman dan pengepakan sebagaimana tercantum dalam Pesanan.</p>
		<p>&nbsp;</p>
		<p>3.3 <strong>Pelacakan</strong>: Anda dapat melacak status pengiriman pada halaman "<em>Order Tracking</em>" pada Platform.</p>
		<p><br /> 3.4 <strong>Jangka waktu pengiriman</strong>: Anda mengakui bahwa pengiriman produk tergantung pada ketersediaan dari produk. Penjual akan melakukan segala upaya yang wajar untuk memberikan Produk kepada Anda dalam jangka waktu pengiriman yang tertera pada halaman terkait yang digunakan oleh Produk terdaftar, namun dengan ini Anda mengakui bahwa ada kemungkinan sebuah Produk menjadi tidak tersedia pada saat <em>Platform</em> di update atau terjadi update atas Pesanan atau Produk. Jangka waktu pengiriman yang diberikan merupakan perkiraan dan penundaan dapat saja terjadi. Jika pengiriman Produk Anda tertunda karena ketidaksediaan Produk, Penjual memberitahu Anda melalui <em>e-mail</em> dan Produk Anda akan dikirim secepatnya ketika telah tersedia pada Penjual. Jangka waktu pengiriman bukan merupakan hakikat, dan Penjual tidak bertanggung jawab atas keterlambatan pengiriman.</p>
		<p>&nbsp;</p>
		<p>3.5 <strong>Penerimaan</strong>: Dalam hal Anda tidak menerima Produk dengan jangka waktu pengiriman yang diproyeksikan dan disediakan pada Platform, maka Anda perlu memberitahukan TOKO1001 atas hal tersebut paling lambat 3 (tiga) hari segera dari tanggal pengiriman, agar Penjual dapat dengan itikad baid dan usaha terbaik mencari dan memberikan Produk. Jika TOKO1001 tidak menerima pemberitahuan Anda dalam waktu 3 (tiga) hari tersebut, maka TOKO1001 akan anggap Anda telah menerima Produk.</p>
		<p>&nbsp;</p>
		<p>3.6 <strong>Voucher dari TOKO1001</strong>: Jika ada keterlambatan dalam pengiriman produk, TOKO1001 dapat saja dengan kebijakannya sendiri menawarkan Voucher kepada Pelanggan. Dengan diterimanya Voucher tersebut oleh Pelanggan, maka dengan penerimaan tersebut tanda Pelanggan setuju untuk tidak memberikan tuntutan lebih lanjut terhadap Penjual dan TOKO1001.</p>
		<p>&nbsp;</p>
		<p>3.7 <strong>Kegagalan Pelanggan atas menerima pengiriman Produk</strong>: Jika Pelanggan gagal menerima pengiriman Produk dikarenakan kesalahan dan/atau kelalaian-nya sendiri (selain karena alasan sebab apapun di luar kendali yang wajar Pelanggan atau dengan alasan kesalahan Penjual) maka tanpa mengurangi hak Penjual untuk melakukan pengiriman ulang atau tindakan perbaikan lainnya, Penjual dapat saja memutuskan untuk mengakhiri atau membatalkan Pesanan atau Kontrak Pelanggan. Jika dalam hal ini, Pelanggan telah membayarkan pembayaran atas Produk maka Lazada akan mengembalikan pembayaran (refund) atas Pesanan tersebut kepada Pelanggan.</p>
		<p><strong>&nbsp;</strong></p>
		<p><strong>&nbsp;</strong></p>
		<p><strong>4. Harga Produk </strong></p>
		<p><strong>&nbsp;</strong></p>
		<p>4.1 <strong>Daftar Harga</strong>: Harga yang harus dibayar oleh Pelanggan harus merupakan Harga yang didaftarkan pada saat Pesanan ditempatkan oleh Pelanggan dan ditransmisikan kepada Penjual (melalui <em>Platform</em>).</p>
		<p>&nbsp;</p>
		<p>4.2 <strong>Pajak</strong>: Semua Harga yang didaftarkan tunduk pada pajak, kecuali dinyatakan lain. Penjual berhak untuk mengubah Daftar Harga sewaktu-waktu tanpa memberikan alasan apapun atau pemberitahuan sebelumnya kepada Pelanggan.</p>
		<p><strong>&nbsp;</strong></p>
		<p><strong>&nbsp;</strong></p>
		<p><strong>5. Payment </strong></p>
		<p><strong>&nbsp;</strong></p>
		<p>5.1 <strong>Umum</strong>: Anda dapat membayar Produk menggunakan salah satu metode pembayaran yang ditentukan oleh TOKO1001. Bila Anda menempatkan Pesanan, pembayaran yang sebenarnya hanya akan dikenakan pada saat penerimaan Pesanan Penjual dan pembentukan Kontrak Pelanggan. Semua pembayaran harus dilakukan kepada TOKO1001, baik penerimaan pembayaran dalam dirinya sendiri atau sebagai agen penjual (di mana Penjual adalah Vendor Pihak Ketiga). Anda mengakui bahwa TOKO1001 berhak untuk mengumpulkan pembayaran dari Anda atas nama Vendor Pihak Ketiga.</p>
		<p>&nbsp;</p>
		<p>5.2 <strong>Istilah tambahan</strong>: Metode pembayaran dapat dikenakan persyaratan tambahan seperti yang ditentukan oleh TOKO1001 dari waktu ke waktu.</p>
		<p>&nbsp;</p>
		<p>5.3 <strong>Metode pembayaran</strong>: Anda setuju bahwa Anda tunduk pada perjanjian pengguna yang berlaku atas metode pembayaran. TOKO1001 berhak sewaktu-waktu untuk mengubah atau menghentikan, untuk sementara atau seterusnya atas suatu metode pembayaran tanpa pemberitahuan kepada Anda.</p>
		<p><br /> 5.4 <strong>Pembayaran dengan Voucher</strong>: Jika Anda menggunakan Voucher, Syarat &amp; Ketentuan Voucher sebagaimana dimaksud pada <em>Platform</em> akan berlaku atau sebagaimana tercantum pada <span style="text-decoration: underline;"><a href="http://www.toko1001.id/Bantuan-FAQ.html"><span>http://www.toko1001.id/b</span><span>antuan-faq.html</span></a></span></p>
		<p><br /> 5.5 <strong>Faktur</strong>: Penjual dapat menagih Anda pada tanggal jatuh tempo pembayaran berdasarkan Kontrak Pelanggan.</p>
		<p>&nbsp;</p>
		<p>5.6 <strong>Kegagalan untuk membayar</strong>: Jika Pelanggan gagal melakukan pembayaran sesuai dengan syarat dan ketentuan dari metode pembayaran yang dipilih atau pembayaran dibatalkan untuk alasan apapun, maka tanpa mengurangi hak atau upaya hukum yang tersedia untuk penjual lainnya, Penjual berhak untuk membatalkan Kontrak Pelanggan atau menangguhkan pengiriman Produk hingga pembayaran dilakukan secara penuh.</p>
		<p>&nbsp;</p>
		<p>5.7 <strong>Pengembalian Dana Pembayaran</strong>:</p>
		<p>(a) Semua pengembalian dana akan dilakukan melalui mekanisme pembayaran asal kepada orang yang membuat pembayaran asal, kecuali untuk <em>Cash on Delivery</em>, di mana pengembalian dana akan dilakukan melalui transfer bank ke rekening bank individu Pelanggan dengan syarat rincian rekening bank yang diberikan kepada kami telah lengkap dan akurat.</p>
		<p>(b) Kami tidak memberikan jaminan apapun dalam akurasu waktu masuknya pengembalian dana ke rekening Anda. Proses pembayaran mungkin memerlukan waktu dan tunduk pada masing-masing bank dan/atau waktu dari proses internal penyedia system pembayaran.</p>
		<p>(c) Semua biaya yang berkaitan dengan proses pengembalian yang dikenakan oleh bank dan/atau penyedia system pembayaran akan ditanggung oleh kami.</p>
		<p>(d) Semua pengembalian dana berdasarkan pengembalian Produk yang telah valid/sesuai. <br /> (e) Kami berhak untuk melakukan perubahan mekanisme refund atau pengembalian dana tanpa kewajiban dengan pemberitahuan sebelumnya.</p>
		<p><strong>&nbsp;</strong></p>
		<p><strong>&nbsp;</strong></p>
		<p><strong>6. Pengembalian / Perbaikan / Penggantian</strong></p>
		<p><strong>&nbsp;</strong></p>
		<p>6.1 <strong>Kebijakan Pengembalian</strong>: Semua pengembalian harus dilakukan sesuai dengan petunjuk yang ditetapkan dalam Kebijakan Pengembalian di <span style="text-decoration: underline;"><a href="http://www.toko1001.id/pengembalian-barang.html"><span>http://www.toko1001.id/pengembalian-barang.html</span></a></span> . Penjual tidak berkewajiban untuk menyetujui pengembalian kecuali semua instruksi tersebut diikuti. Jika Penjual setuju untuk pengembalian, Penjual akan memberikan penggantian produk ke alamat Anda.</p>
		<p>&nbsp;</p>
		<p>6.2 <strong>Pengembalian yang diijinkan</strong>: Sesuai dengan Pasal 6.1, dalam jangka waktu 14 hari terhitung sejak tanggal pengiriman produk, Anda dapat mengembalikan Produk jika:</p>
		<p>6.2.1 menerima produk yang secara fundamental berbeda dari Produk ditetapkan dalam Kontrak Pelanggan; atau</p>
		<p>6.2.2 menerima Produk yang rusak atau salah.</p>
		<p>&nbsp;</p>
		<p>6.3 <strong>Penggantian Produk</strong>: Ketika TOKO1001 (atau Penjual) telah setuju memberikan penggantian Produk atau telah setuju untuk memberikan pengembalian dana kepada Pelanggan, Produk dan seluruh komponen atau lengkap dengan kemasannya dan seluruh isi-nya, wajib dikembalikan atau dikirim kembali ke Penjual secepatnya dengan biaya pengiriman ditanggung oleh Pelanggan. <br /> <br /></p>
		<p>6.4 <strong>Resiko kerusakan atau kehilangan</strong>: Resiko kerusakan atau hilangnya produk berada pada Pelanggan saat Produk telah dikirim/telah diterima berdasarkan tanda terima, walaupun Pelanggan lalai atau gagal dalam menerima pengiriman Produk yang telah terkirim atau telah sampai di alamat Pelanggan berdasarkan tanda terima.</p>
		<p><strong>&nbsp;</strong></p>
		<p><strong>&nbsp;</strong></p>
		<p><strong>7. Pertanyaan dan Keluhan </strong></p>
		<p><strong>&nbsp;</strong></p>
		<p>Jika Anda memiliki pertanyaan atau keluhan, silahkan hubungi TOKO1001 melalui <a href="http://www.toko1001.id/hubungi-kami.html"><span style="text-decoration: underline;"><span>http://www.toko1001.id/hubungi-kami</span><span>.html</span></a> </span>atau menggunakan pilih/klik "Hubungi Kami" pada <span style="text-decoration: underline;"><span>http://www.toko1001.id/b</span><a href="http://www.toko1001.id/faq/"><span>bantuan-faq.html</span></a></span> , atau dengan menghubungi Customer Service kami, TOKO1001 akan bekerja sama dengan Penjual untuk menjawab pertanyaan dan keluhan Anda.</p>
		<p><strong>&nbsp;</strong></p>
		<p><strong>&nbsp;</strong></p>
		<p><strong>8. Pemutusan </strong></p>
		<p><strong>&nbsp;</strong></p>
		<p>8.1 <strong>Pembatalan oleh Anda</strong>: Anda dapat membatalkan Kontrak Pelanggan sebelum Penjual mengirimkan Produk berdasarkan Kontrak Pelanggan tersebut dengan mengisi formulir pada http://www.toko1001.id atau menghubungi Customer Service kami. &nbsp;Jika Produk telah dikirim, Anda tidak dapat membatalkan Kontrak Pelanggan namun hanya dapat mengembalikan Produk sesuai dengan Pasal 6.</p>
		<p>&nbsp;</p>
		<p>8.2 <strong>Pembatalan oleh Penjual</strong>: Tanpa mengesampingkan hak lain yang diatur dalam Syarat &amp; Ketentuan Penjualan ini, Penjual atau TOKO1001 yang bertindak atas nama Penjual, dapat menghentikan Produk dalam perjalanan, menangguhkan pengiriman dan/atau mengakhiri Kontrak Pelanggan dengan efek langsung melalui pemberitahuan tertulis kepada Pelanggan atau sewaktu-waktu setelah terjadinya salah satu dari peristiwa berikut:</p>
		<p>8.2.1 Produk tidak tersedia dengan alasan tertentu / apapun;</p>
		<p>8.2.2 Pelanggan melanggar kewajiban berdasarkan Kontrak Pelanggan, Syarat &amp; Ketentuan Penggunaan, Syarat &amp; Ketentuan Penjualan, atau ketentuan lain yang berlaku di <em>Platform</em>;</p>
		<p>8.2.3 Pelanggan ditetapkan pailit oleh pengadilan atau badan yang berwenang;</p>
		<p>8.2.4 Kecurigaan atas adanya <em>fraud</em> atau kecurangan dalam bentuk apapun berdasarkan kebijakan atau investigasi TOKO1001 ataupun Penjual;</p>
		<p>&nbsp;</p>
		<p>8.3 <strong>Para Pihak sepakat untuk mengesampingkan Pasal 1266 KUHPerdata Indonesia</strong> sehingga pemutusan Kontrak Pelanggan baik yang dibuat oleh Anda atau Penjual akan dilakukan tanpa memerlukan persetujuan atau keputusan pengadilan atau lembaga lain di dalam wilayah Republik Indonesia.</p>
		<p><strong>&nbsp;</strong></p>
		<p><strong>&nbsp;</strong></p>
		<p><strong>9. Resiko dan Kepemilikan Barang </strong></p>
		<p><strong>&nbsp;</strong></p>
		<p>9.1 Resiko kerusakan atau kehilangan barang akan ditanggung oleh Pembeli pada saat pengiriman atau jika Pembeli / Pelanggan lalai atau gagal dalam menerima pengiriman Produk yang telah terkirim atau telah sampai di alamat Pembeli / Pelanggan.</p>
		<p><br /> 9.2 Kepemilikan Produk tidak akan berpindah ke Pembeli/Pelanggan sampai pembayaran atas Produk diterima TOKO1001 secara penuh atas harga Produk.</p>
		<p><br /> 9.3 Sebelum kewajiban pelunasan Produk dipenuhi, Pembeli atau Pelanggan hanya bertindak sebagai agen fidusia TOKO1001 (atau <em>Bailee</em>) bukan pemilik atas Produk.</p>
		<p><br /> 9.4 Pembeli setuju dengan TOKO1001 bahwa Pembeli harus segera memberitahukan TOKO1001 materi apapun dari waktu ke waktu yang mempengaruhi nama TOKO1001 atas Barang dan Pembeli harus memberikan informasi kepada TOKO1001 yang berkaitan dengan barang yang mungkin diperlukan TOKO1001 dari waktu ke waktu.</p>
		<p><br /> 9.5 Pada kondisi yang dimaksud Klausa 9.2 dan 9.3, TOKO1001 berhak untuk meminta pembeli untuk menyerahkan barang ke TOKO1001 dan apabila terjadi ketidakpatuhan, TOKO1001 memiliki hak untuk mengambil tindakan hukum terhadap pembeli untuk mendapatkan kembali Produk atau Barang tersebut, dan juga memiliki hak untuk meminta pertanggungjawabab atas seluruh kerugian dan biaya lainnya termasuk namun tidak terbatas atas biaya upaya hukum.</p>
		<p><br /> 9.6 Pembeli tidak berhak untuk mentransfer, mengalihkan, melepaskan, menjaminkan atau dengan apapun membebankan Barang atau Produk yang masih belum terlunasi, yang merupakan milik TOKO1001, jika pembeli melanggarnya maka sehingga semua hutang atau sisa pelunasan atau pembayaran atas Barang atau Produk menjadi jatuh tempo dan dapat ditagih.</p>
		<p><br /> 9.7 Jika ketentuan dalam Klausa 9 tidak efektif sesuai dengan hukum negara di mana Barang berada, maka Pembeli harus mengambil semua langkah yang diperlukan untuk memberikan efek yang sama dengan yang berlaku pada Klausa 9 ini.</p>
		<p><br /> 9.8 Dengan ini Pembeli atau Pelanggan membebaskan TOKO1001, semua petugas, karyawan, direktur, agen, kontraktor, atas semua kerusakan, kerugian, biaya, beban dan biaya hukum yang dikeluarkan oleh Pembeli sehubungan dengan pernyataan dan penegakan hak TOKO1001 di bawah Syarat &amp; Ketentuan Penjualan ini.</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p><strong>10. PEMBATASAN TANGGUNG JAWAB </strong></p>
		<p><strong>&nbsp;</strong></p>
		<p>10.1 <strong>REMEDI</strong>: REMEDI YANG DITETAPKAN DALAM PASAL 6 ADALAH REMEDI ATAS KETIDAKKESESUAIAN ATAU CACAT PADA PRODUK YANG BERLAKU SATU-SATUNYA DAN EKSKLUSIF. <br /> <br /></p>
		<p>10.2 <strong>TANGGUNG JAWAB MAKSIMAL</strong>: KEWAJIBAN MAKSIMUM PENJUAL KEPADA ANDA ATAU PIHAK LAIN UNTUK SEMUA KERUGIAN, YANG TIMBUL DARI ATAU BERHUBUNGAN DENGAN PENJUALAN PRODUK DALAM SETIAP KONTRAK PELANGGAN, TIDAK AKAN MELEBIHI JUMLAH YANG ANDA BAYAR DALAM KONTRAK PELANGGAN TERSEBUT.</p>
		<p>&nbsp;</p>
		<p>10.<strong>3 PENGECUALIAN TANGGUNG JAWAB</strong>: TOKO1001 TIDAK BERTANGGUNG JAWAB KEPADA ANDA UNTUK SETIAP KERUGIAN APAPUN ATAU APAPUN PENYEBABNYA (APAPUN BENTUKNYA) YANG TIMBUL LANGSUNG MAUPUN TIDAK LANGSUNG BERHUBUNGAN DENGAN: (I) PIUTANG PENGGUNA LAIN DALAM PLATFORM YANG BERKAITAN DENGAN PEMBELIAN PRODUK APAPUN; (II) PENJUALAN PRODUK OLEH PENJUAL VENDOR KETIGA UNTUK ANDA, ATAU PENGGUNAAN PRODUK, ATAU PENJUALAN KEMBALI PRODUK OLEH ANDA; DAN (III) SETIAP CACAT TIMBUL DARI KEAUSAN, KERUSAKAN YANG DISENGAJA, PENYALAHGUNAAN, KELALAIAN, KECELAKAAN, PENYIMPANAN ABNORMAL DAN ATAU KONDISI PENGGUNAAN, PERUBAHAN ATAU MODIFIKASI PRODUK ATAU KEGAGALAN MEMATUHI INSTRUKSI PENJUAL (ATAU INTRUKSI DI DESKIRIPSI PRODUK ATAU DI MANUAL) TERHADAP PENGGUNAAN PRODUK (BAIK LISAN MAUPUN TERTULIS)</p>
		<p><strong>&nbsp;</strong></p>
		<p><strong>&nbsp;</strong></p>
		<p><strong>11. Umum </strong></p>
		<p><strong>&nbsp;</strong></p>
		<p>11.1 <strong>Referensi untuk "TOKO1001":</strong> Istilah "TOKO1001" dalam Syarat dan Ketentuan Penjualan ini berlaku baik untuk tindakan TOKO1001 atas nama sendiri sebagai Penjual (hanya jika TOKO1001 bertindak sebagai Penjual) dan/atau sebagai operator <em>Platform</em>, atau sebagai <em>customer service</em> bagi Vendor Pihak Ketiga yang bertindak sebagai Penjual.</p>
		<p><br /> 11.2 <strong>Hak untuk subkontrak</strong>: Vendor Pihak Ketiga berhak untuk mendelegasikan dan/atau melakukan subkontrak hak atau kewajiban berdasarkan Syarat &amp; Ketentuan Penjualan kepada TOKO1001 atau penyedia layanan TOKO1001 yang ditunjuk.</p>
		<p><br /> 11.3 <strong>Hak kumulatif dan remedi</strong>: Kecuali ditentukan lain dalam Syarat &amp; Ketentuan Penjualan ini, klausa - klausa Syarat &amp; Ketentuan Penjualan ini dan hak dan remedi kami berdasarkan Syarat &amp; Ketentuan Penjualan ini, adalah kumulatif dan tanpa prasangka serta hak atau remedi kami mungkin kami miliki berdasarkan hukum atau keadilan, dan tidak ada ketentuan yang akan menghalangi atau mencegah hak kami dan upaya hukum atau remedi kami berdasarkan hukum yang berlaku.</p>
		<p><br /> 11.4 <strong><em>No Waiver</em>:</strong> Kegagalan Penjual untuk melaksanakan Syarat &amp; Ketentuan Penjualan ini tidak dapat diartikan sebagai diabaikannya dari ketentuan - ketentuan ini, dan kegagalan tersebut tidak akan mempengaruhi hak kami selanjutnya untuk menegakkan Syarat &amp; Ketentuan Penjualan ini. Penjual akan tetap berhak menggunakan hak dan remedy/upaya hukum dalam setiap kondisi lain di mana Anda melanggar Syarat Penggunaan ini.</p>
		<p><br /> 11.5 <strong><em>Severability</em></strong>: Jika suatu saat ada ketentuan pada Syarat &amp; Ketentuan Penjualan ini yang menjadi ilegal, tidak sah atau tidak dapat diterapkan dalam hal apapun, maka legalitas, validitas dan keberlakuan dari ketentuan lainnya dalam Syarat &amp; Ketentuan Penjualan ini tidak akan terpengaruh atau berkurang karenanya, dan akan terus berlaku.</p>
		<p><br /> 11.6 <strong>Hak Pihak Ketiga</strong>: Seseorang atau pihak yang bukan merupakan pihak yang tunduk pada Syarat &amp; Ketentuan Penjualan ini, tidak berhak berdasarkan hukum apapun dan yurisdiksi manapun untuk menjalankan ketentuan dari Syarat &amp; Ketentuan Penjualan ini. Untuk menghindari keraguan, tidak ada dalam Klausa ini yang akan mempengaruhi hak-hak dari setiap pengalihan yang diizinkan atas Syarat &amp; Ketentuan Penjualan ini.</p>
		<p><br /> 11.7 <strong>Hukum yang Berlaku</strong>: Syarat &amp; Ketentuan Penjualan akan diatur dan ditafsirkan sesuai dengan hukum Indonesia dan Anda dengan ini tunduk pada yurisdiksi eksklusif Pengadilan Negeri Jakarta Selatan.</p>
		<p><br /> 11.8 <strong>Kecuali sebagaimana diatur dalam Klausul 11.7</strong>, sengketa, perselisihan atau gugatan yang timbul dari atau berhubungan dengan kontrak ini, atau pelanggaran, penghentian atau cacat daripadanya harus diselesaikan melalui arbitrase sesuai dengan Aturan Arbitrase Dewan Nasional Indonesia Arbitrase (Badan Arbitrase Nasional Indonesia - "BANI"). Sidang arbitrase harus terdiri dari arbiter tunggal, yang ditunjuk oleh Ketua BANI. Tempat arbitrase adalah Jakarta. Setiap keputusan oleh pengadilan arbitrase bersifat final dan mengikat para pihak.</p>
		<p><br /> 11.9 <strong>Bantuan Hukum</strong>: Kami dapat mencari atau menggunakan upaya / bantuan hukum dengan segera berdasarkan itikad baik untuk menangani pelanggaran atau tidak dipatuhinya Syarat &amp; Ketentuan Penjualan ini.</p>
		<p><br /> 11.10 <strong>Perubahan</strong>: Kami dengan pemberitahuan melalui <em>Platform</em> atau dengan metode lain seperti kami inginkan (yang mungkin termasuk pemberitahuan melalui <em>e-mail</em>), dapat merubah syarat &amp; ketentuan pada Syarat &amp; Ketentuan Penjualan dan berlaku pada tanggal yang kami tentukan melalui cara di atas. Jika Anda menggunakan <em>Platform</em> atau Layanan setelah tanggal tersebut, Anda dianggap telah setuju atas perubahan tersebut. Jika Anda menolak perubahan tersebut, Anda harus berhenti akses atau menggunakan <em>Platform</em> dan Layanan dan mengakhiri Syarat &amp; Ketentuan Penjualan ini. Hak kami untuk merubah Syarat &amp; Ketentuan Penjualan ini di dapat dilaksanakan tanpa persetujuan dari setiap orang atau badan yang bukan merupakan pihak berdasarkan Syarat &amp; Ketentuan Penjualan ini.</p>
		<p><br /> 11.11 <strong>Koreksi kesalahan</strong>: Setiap kesalahan ketik, administrasi atau kesalahan lainnya atau kelalaian dalam penerimaan, faktur atau dokumen lainnya yang terjadi pada sisi kami akan menjadi bahan perbaikan kami yang dilakukan oleh kami</p>
		<p><br /> 11.12 <strong>Mata uang</strong>: Uang yang berkaitan dengan Syarat &amp; Ketentuan Penjualan ini adalah dalam mata uang Rupiah Indonesia.</p>
		<p><br /> 11.13 <strong>Seluruh kesepakatan</strong>: Syarat &amp; Ketentuan Penjualan harus merupakan keseluruhan perjanjian antara Anda dan kami berkaitan dengan materi pokok dalam perjanjian dan menggantikan dan menggantikan secara penuh semua pemahaman, komunikasi dan perjanjian sebelumnya sehubungan dengan materi pokok dalam perjanjian.</p>
		<p><br /> 11.14 <strong>Mengikat dan konklusif</strong>: Anda mengakui dan menyetujui bahwa setiap catatan (termasuk catatan dari setiap percakapan telepon terkait dengan Layanan, jika ada) dikelola oleh kami atau penyedia layanan kami yang berhubungan dengan atau yang berhubungan dengan <em>Platform</em> dan Layanan bersifat mengikat dan konklusif pada Anda untuk semua tujuan apapun dan menjadi bukti dari setiap informasi dan / atau data yang dikirimkan antara kami dan Anda. Anda setuju bahwa semua catatan tersebut diterima sebagai bukti dan bahwa Anda tidak akan menantang atau membantah diterimanya, kehandalan, akurasi atau keaslian catatan tersebut dengan alasan karena catatan tersebut dalam bentuk elektronik atau output dari sistem computer.</p>
		<p><br /> 11.15 <strong>Sub-kontraktor dan delegasi</strong>: Kami berhak untuk mendelegasikan atau sub-kontrak kinerja dari setiap fungsi kami sehubungan dengan <em>Platform</em> dan / atau Jasa dan berhak untuk menggunakan penyedia layanan, subkontraktor dan / atau agen pada seperti hal sebagaimana kami anggap pantas.</p>
		<p><br /> 11.16 <strong>Pengalihan:</strong> Anda tidak dapat mengalihkan hak dan kewajiban Anda atas Syarat &amp; Ketentuan Penjualan tanpa izin tertulis sebelumnya dari kami.</p>
		<p><br /> 11.17 <strong><em>Force Majeure</em>:</strong> Kami dilepaskan dari pertanggungjawaban atas wanprestasi, kesalahan, gangguan atau keterlambatan dalam pelaksanaan kewajiban atau untuk setiap ketidakakuratan, atau ketidaksesuaian di <em>Platform</em> dan / atau isi Layanan, jika hal tersebut disebabkan, secara keseluruhan atau sebagian, langsung atau tidak langsung, oleh suatu peristiwa atau kegagalan yang disebabkan <em>Force Majeur</em> atau Keadaan Memaksa.</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p><strong>Lampiran 1 </strong></p>
		<p><br /> <strong>Definisi dan Interpretasi</strong></p>
		<p>&nbsp;</p>
		<p>1<strong>. Definisi:</strong> Kecuali konteksnya menentukan lain, ungkapan berikut akan memiliki arti sebagai berikut dalam Ketentuan Penggunaan ini:</p>
		<p><br /> 1.1 "<strong>Hari Kerja</strong>" berarti hari (tidak termasuk hari Sabtu dan Minggu) dimana bank umum terbuka untuk bisnis di Indonesia</p>
		<p>&nbsp;</p>
		<p>1.2 "<strong>Pelanggan</strong>" berarti pembeli dan/atau pengguna yang sah dari Platform dan/atau Layanan.</p>
		<p><br /> 1.3 "<strong>Kontrak Pelanggan</strong>" harus seperti yang didefinisikan dalam Klausul 2.6.</p>
		<p>&nbsp;</p>
		<p>1.4 "<strong>Kekayaan Intelektual</strong>" berarti semua hak cipta, paten, inovasi utilitas, merek dagang dan merek layanan, indikasi geografis, nama domain, hak desain tata letak, desain terdaftar, hak desain, hak database, nama dagang atau bisnis, hak melindungi rahasia dagang dan rahasia informasi, hak melindungi goodwill dan reputasi, dan semua hak kepemilikan yang sama atau terkait lainnya dan semua aplikasi yang sama, baik saat ada atau dibuat di masa depan, di mana saja di dunia, apakah terdaftar atau tidak, dan semua manfaat, hak istimewa, hak untuk menuntut, memulihkan kerusakan dan mendapatkan bantuan atau solusi lainnya untuk setiap masa lalu, pelanggaran saat ini atau masa depan, penyalahgunaan atau pelanggaran hak-hak tersebut di atas.</p>
		<p>&nbsp;</p>
		<p>1.5 "<strong>TOKO1001 Indemnitees</strong>" berarti TOKO1001, afiliasinya dan semua yang masing-masing petugas, karyawan, direktur, agen, kontraktor dan penerima.</p>
		<p>&nbsp;</p>
		<p>1.6 "<strong>TOKO1001</strong>", "<strong>kita</strong>", dan "<strong>kami</strong>" merujuk kepada PT. Deltamas Mandiri Sejahtera., Sebuah perusahaan yang didirikan berdasarkan hukum Republik Indonesia dan beralamat terdaftar di Jl. Indokarya Barat I Blok D No. 1, Jakarta Utara 14330.</p>
		<p>&nbsp;</p>
		<p>1.7 "<strong>Daftar Harga</strong>" berarti harga produk yang tercantum untuk dijual kepada pelanggan, sebagaimana tercantum pada <em>Platform</em>.</p>
		<p>&nbsp;</p>
		<p>1.8 "<strong>Kerugian</strong>" berarti semua penalti, kerugian, jumlah penyelesaian, biaya (termasuk biaya hukum dan biaya pengacara), biaya, pengeluaran, aksi, proses, klaim, tuntutan dan kewajiban lainnya, yang dapat di duga atau tidak.</p>
		<p>&nbsp;</p>
		<p>1.9 "<strong>Material</strong>" atau materi berarti, secara kolektif, semua halaman web pada Platform, termasuk informasi, kontenn, gambar, link, suara, gambar, video, software, aplikasi dan bahan lain yang ditampilkan atau tersedia pada Platform dan fungsi atau layanan yang disediakan di <em>Platform</em>.</p>
		<p>&nbsp;</p>
		<p>1.10 "<strong>Pesanan</strong>" atau "<strong>Order</strong>" berarti pesanan Anda untuk produk melalui Platform sesuai dengan Syarat &amp; Kondisi Penjualan.</p>
		<p>&nbsp;</p>
		<p>1.11 "<strong>Password</strong>" mengacu pada <em>password</em> yang valid milik Pelanggan yang memiliki akun TOKO1001 yang dapat digunakan dengan <em>Username</em> untuk mengakses Platform dan / atau Layanan terkait.</p>
		<p><br /> 1.12 "<strong>Data Pribadi</strong>" berarti data, apakah benar atau tidak, yang dapat digunakan untuk mengidentifikasi, menghubungi atau mencari Anda. Data Pribadi bisa termasuk nama, <em>alamat e-mail</em>, alamat penagihan, alamat pengiriman, nomor telepon dan informasi kartu kredit. "Data Pribadi" akan dianggap termasuk setiap data yang Anda berikan kepada kami ketika menempatkan Order.</p>
		<p><br /> 1.13 "<strong>Platform</strong>" berarti:</p>
		<p>(a) web dan/atau versi <em>mobile</em> dari web yang dioperasikan dan/atau dimiliki oleh TOKO1001 yang saat ini terletak di <em>URL</em> berikut: www.toko1001.id; dan</p>
		<p>(b) aplikasi <em>mobile</em> yang dibuat dari waktu ke waktu oleh TOKO1001, termasuk iOS dan Android.</p>
		<p><br /> 1.14 "<strong>Garansi Produk</strong>" berarti produk (termasuk angsuran produk atau bagiannya) tersedia untuk dijual ke pelanggan di Platform.</p>
		<p>&nbsp;</p>
		<p>1.15 "<strong>Kebijakan Pengembalian</strong>" berarti kebijakan pengembalian ditetapkan di&nbsp;<span>http://www.toko1001.id/pengembalian-barang.html</span></p>
		<p><br /> 1.16 "<strong>Vendor Pihak Ketiga</strong>" berarti penjual atau merchant yang menggunakan Platform dan/atau Layanan untuk menjual Produk kepada Pelanggan, yang terpisah dari TOKO1001.</p>
		<p><br /> 1.17 "<strong>Penjual</strong>" berarti penjual produk seperti yang dijelaskan dalam Klausul 2.3, termasuk Vendor Pihak Ketiga.</p>
		<p>1.18 "<strong>Layanan</strong>" berarti layanan, informasi dan fungsi yang disediakan oleh penjual di Platform.</p>
		<p><br /> 1.19 "<strong>Syarat &amp; Ketentuan Penjualan</strong>" berarti Klausul 1-11 dan setiap Jadwal untuk syarat dan ketentuan ini.</p>
		<p>&nbsp;</p>
		<p>1.20 "<strong>Syarat dan Ketentuan Penggunaan</strong>" berarti syarat dan ketentuan yang mengatur penggunaan Pelanggan Platform dan / atau Layanan dan ditetapkan di <span style="text-decoration: underline;"><span>http://www.toko1001.id/syarat-dan-ketentuan.html</span><a href="http://www.toko1001.id/terms-of-use/#tou"></a></span></p>
		<p><br /> 1.21 "<strong><em>Trademark</em></strong>" berarti merek dagang, merek layanan, nama dagang dan logo yang digunakan dan ditampilkan pada Platform.</p>
		<p>&nbsp;</p>
		<p>1.22 "<strong><em>Username</em></strong>" mengacu pada nama identifikasi login unik atau kode yang mengidentifikasi Pelanggan yang memiliki akun di TOKO1001.</p>
		<p>&nbsp;</p>
		<p>1.23 "<strong>Voucher</strong>" berarti voucher yang dapat digunakan oleh Pelanggan atas pembayaran pembelian pada Platform, berdasarkan dengan syarat dan ketentuan di Platform atau di <span style="text-decoration: underline;">http://www.toko1001.id/Bantuan-FAQ.html</span></p>
		<p><br /> 1.24 "<strong>Anda</strong>" merujuk pada Pelanggan, Pembeli, dan/atau individu berusia di atas 17 atau yang di bawah 17 namun dengan perwakilan dan/atau pengawasan orang tua atau wali yang sah.<br /> <br /></p>
		<p>&nbsp;</p>
		<p>2<strong>. Interpretasi</strong>: Setiap acuan pada Syarat &amp; Ketentuan Penjualan untuk setiap ketentuan harus ditafsirkan sebagai acuan bahwa ketentuan tersebut sebagaimana telah diubah, diberlakukan kembali atau diperpanjang pada waktu yang relevan. Dalam Perjanjian, setiap kali kata "termasuk", " digunakan, akan dianggap sebagai "tanpa batasan". Kecuali ditentukan lain, semua referensi untuk hari adalah hari kalender, dan "bulan" atau "bulanan" mengacu bulan kalender. Setiap Judul tidak di jadikan interpretasi atas Syarat &amp; Ketentuan Penjualan ini, jika terjadi konflik atau inkonsistensi antara dua atau lebih ketentuan dalam Syarat &amp; Ketentuan Penjualan ini, apakah ketentuan tersebut tercantum dalam dokumen yang sama atau berbeda, konflik atau inkonsistensi tersebut maka TOKO1001 akan menentukan ketentuan yang berlaku.</p>
	    </div>
	    <?php
	    break;
	case "panduan-belanja":
	    ?>
	    <div class="media"><h2> Panduan belanja</h2>
		<div class="media-left">
		    <a href="#">
			<img class="media-object img-rounded" src="<?php echo base_url() . ASSET_IMG_HOME; ?>panduan-belanja-1.png" alt="Panduan belanja">
		    </a>
		</div>
		<div class="media-body">
		    <h2 class="media-heading"><span class="label bg-green">1</span></h2>
		    <p style="margin-top:20px;">Pilih Kategori Produk atau kolom Pencarian Barang dan pilih Produk yang diinginkan dengan cukup klik pada gambar maka otomatis,
			produk akan ditambahkan ke troli, kemudian klik Beli dan Selesai Belanja Atau, jika Anda masih ingin membeli produk lainnya,
			maka cukup klik tombol Tambahkan ke Keranjang dan Lanjutkan Belanja dan ulangi langkah yang sama.
			Setelah menemukan barang yang diinginkan, klik gambar produk tersebut untuk melihat informasi lengkap mengenai produk dan toko
			yang kamu pilih, antara lain:<br /><br />
			Diskripsi : berisi semua informasi penting yang berhubungan dengan produk yang sedang kamu lihat.<br />
			Spesifikasi : berisi tentang beberapa spesifikasi barang yang anda pilih, dari berat, ukuran, warna, hingga tahun pembuatan dll.<br />
			Rating : Berisi tentang berapa rating pembelian, dari kepuasan anda membeli peroduk dari kami dan anda akan dapat mengisi berapa rating
			yang adan berikan untuk sebuah peroduk tersebut.</p>
		</div>
	    </div>
	    <div class="media">
		<div class="media-body">
		    <h2 class="media-heading"><span class="label bg-blue">2</span></h2>
		    <p style="margin-top:20px;">
			Login Member atau Daftar dan lanjutkan. Bagi Anda yang belum menjadi member atau yang belum memiliki Akun,
			klik tombol "Daftar Disini" dan Anda akan diminta untuk memasukan alamat email, nama lengkap, tanggal lahir,
			jenis kelamin, No Telepon / HP, dan password. Bagi Anda yang sudah memiliki Akun cukup mengisi data email dan
			password. Setelah data terisi lengkap, silahkan klik tombol " Login ".<br />
			Memasukan alamat email, nama lengkap, tanggal lahir, jenis kelamin, No Telepon / HP, dan password.
			Bagi Anda yang sudah memiliki Akun cukup mengisi data email dan password. Setelah data terisi lengkap
			silahkan klik tombol " Login ".
		    </p>
		</div>
		<div class="media-right">
		    <a href="#">
			<img class="media-object img-rounded" src="<?php echo base_url() . ASSET_IMG_HOME; ?>panduan-belanja-2.png" alt="Panduan belanja">
		    </a>
		</div>
	    </div>
	    <div class="media">
		<div class="media-left">
		    <a href="#">
			<img class="media-object img-rounded" src="<?php echo base_url() . ASSET_IMG_HOME; ?>panduan-belanja-3.png" alt="Panduan belanja">
		    </a>
		</div>
		<div class="media-body">
		    <h2 class="media-heading"><span class="label bg-yellow">3</span> </h2>
		    <p style="margin-top:20px;">
			Klik Checkout dan lanjutkan. Setelah akun selesai dimasukan/didaftarkan maka Anda akan masuk ke laman Checkout
			Summary, di mana Anda diminta untuk mengisi jumlah produk yang dipesan pada kolom Quantity yang diinginkan
			dan mengisi pilihan Shipping Method, selanjutnya akan muncul Total nominal belanja, sekaligus tertera biaya ongkos
			kirimnya. Untuk melanjutkan prosesnya, silahkan untuk klik tombol "Checkout"<br /><br />

			Bank Transfer<br />
			Pembayaran dilakukan melalui bank-bank / Internet Banking melalui Bank<br /><br />

			Deposit<br />
			Dan untuk melanjutkan prosesnya, silahkan untuk klik tombol Lanjut Selanjutnya Anda akan diminta untuk mengisi Konfirmasi
			Pembayaran apabila Anda telah melakukan transfer pembayaran atas pesanan Anda dan klik " Proses ", selesai.
		    </p>
		</div>
	    </div>

	    <?php
	    break;
	case "tentang-kami":
	    ?>
	    <h2>Tentang Kami</h2>
	    <p>
		TOKO1001 merupakan salah satu pusat belanja 'online', yang menawarkan pengalaman belanja online dengan konsep aman,
		cepat dan nyaman. Kami menyediakan beragam produk, mulai dari fashion, handphone & tablet, kesehatan & kecantikan,
		hobby & jam tangan, kamera, computer, elektronik & audio, peralatan rumah tangga, mainan anak-anak dan bayi, tas & koper,
		olahraga & music, otomotif, makanan & minuman, buku & alat tulis.<br /><br />

		Kepuasan pelanggan merupakan sasaran TOKO1001. Menyediakan beragam pilihan produk berkualitas, berikut pilihan metode
		pembayaran bagi kepuasan belanja para pelanggan, termasuk layanan jaminan "uang kembali", layanan pelanggan dan garansi komitmen.
		Kami memberikan kemudahan saat Anda berbelanja dengan menu navigasi online sederhana namun efektif.Belanja online di TOKO1001
		sangat mudah!!! Tetap terhubung dan dapatkan penawaran terbaru dan transaksi setiap harinya. Kami menawarkan beragam pilihan
		produk-produk berkualitas, yang dapat ditemukan melalui ujung jari Anda. Dengan navigasi pencarian yang sederhana namun efektif,
		daftar koleksi, dan harga yang menarik, Kami memberikan kemudahan untuk Anda menemukan produk sesuai kebutuhan belanja Anda.<br /><br />

		Jadikanlah momen belanja online Anda menjadi kegiatan yang menyenangkan dengan TOKO1001. Terima kasih atas kunjungan
		Anda dan nikmati sensasi berbelanja online di TOKO1001, dimana Anda dapat mencari yang terbaik hanya dengan beberapa klik saja.
		Selamat berbelanja online!!!<br /><br />
		<img class="img-responsive img-rounded" style="float:left;margin-right: 150px" src="<?php echo base_url() . ASSET_IMG_HOME; ?>tentang-kami-1.png" alt="Tentang kami">
		KAMI SELALU SIAP MENJAWAB PERTANYAAN ANDA!<br />
		Anda bisa menghubungi kami melalui:<br />
		Customer care<br />
		Telepon : 021-6514224/5<br />
		Email : cc@toko1001.id<br />
		Jadwal operasional Customer Care :<br />
		Senin-Jumat, 08.30-17.00 WIB<br />
		Sabtu, 08.30-14.00 WIB<br />


		<img class="img-responsive img-rounded" style="float:right;" src="<?php echo base_url() . ASSET_IMG_HOME; ?>tentang-kami-2.png" alt="Tentang kami"><br />
		PARTNERSHIP<br />
		SELAMAT BERGABUNG DENGAN TOKO1001 Merchant TOKO1001.<br />
		Dapat langsung mengirimkan proposal pengajuan penawaran produk yang Anda miliki
		melalui email ke merchant@toko1001.id atau langsung daar sebagai
		Merchant di http://www.toko1001.id/ dengan informasi sebagai berikut :<br />
		Produk dan informasi terkait produk (asal, identitas kalau ada) Kategori/jenis produk, misal: Fashion,
		Handphone & Tablet, Kesehatan & Kecantikan dll. Foto contoh produk Informasi lainnya tentang produk,
		misal: deskripsi, spesifikasi, bahan, warna, dll. Sesegera mungkin Kami merespon dan menindaklanjuti
		proposal pengajuan penawaran produk Anda.<br /><br />

		CAREER<br />
		TOKO1001 adalah destinasi mall online cepat, aman dan nyaman. Kami adalah situs ecommerce
		market place dengan kualitas produk terbaik, pilihan, terbaru dan terlaris dengan harga yang spesial
		yang sedang berkembang menjadi yang terlengkap dan tercepat. Didirikan pada awal tahun 2015,
		kami terdaar di Indonesia, melengkapi situs ecommerce yang ada. Jika Anda pribadi yang memiliki passion,
		innovative and responsive, we welcome you on TOKO1001! Tertarik untuk bergabung di TOKO1001?<br />
		Kirimkan CV ke career@toko1001.id

	    </p>
	    <?php
	    break;
	case "pengembalian-barang":
	    ?>
	    <h2>Kebijakan / Prosedur Pengembalian Produk</h2>
	    <p>
		Kebijakan pengembalian Kami, memungkinkan member untuk mengembalikan produk yang telah
		dibeli, termasuk pilihan untuk pengembaliandana seluruhnya atau penggantian produk.<br />
		Adapun hal-hal terkait syarat dan ketentuan pengembalian produk dapat dijelaskan sebagai berikut :
		Anda mungkin ingin mengembalikan pesanan karena beberapa alasan berikut ini:</p>

	    <table class="table table-bordered">
		<tbody>
		    <tr>
			<td colspan="2" align="center">
			    <p><strong>Alasan</strong></p>
			</td>
			<td align="center">
			    <p><strong>Uraian</strong></p>
			</td>
		    </tr>
		    <tr>
			<td rowspan="2" style="text-align:center;width:115px;">
			    <p><strong>Alasan pribadi<br />(Subyektif)</strong></p>
			</td>
			<td width="174" valign="top">
			    <p>Tidaksesuai harapan</p>
			</td>
			<td width="301">
			    <p>Produk tidak sesuai dengan harapan pelanggan</p>
			</td>
		    </tr>
		    <tr>
			<td width="174" valign="top">
			    <p>Berubah pikiran</p>
			</td>
			<td width="301" valign="top">
			    <p>Pelanggan tidak membutuhkan produknya lagi</p>
			</td>
		    </tr>
		    <tr>
			<td rowspan="6" style="text-align:center;width:115px;">
			    <p><strong><em>Alasan Produk<br />(Obyektif)</em></strong></p>
			</td>
			<td width="174" valign="top">
			    <p>Rusak</p>
			</td>
			<td width="301" valign="top">
			    <p>Produk rusak saat pengiriman</p>
			</td>
		    </tr>
		    <tr>
			<td width="174" valign="top">
			    <p>Cacat</p>
			</td>
			<td width="301" valign="top">
			    <p>Produk tidak berfungsi seperti yang dijelaskan dalam spesifikasinya dari pabrik</p>
			</td>
		    </tr>
		    <tr>
			<td width="174" valign="top">
			    <p>Salah / item yang salah</p>
			</td>
			<td width="301" valign="top">
			    <p>Bukan produk yang dipesan oleh pelanggan (misal salah ukuran atau salah warna)</p>
			</td>
		    </tr>
		    <tr>
			<td width="174" valign="top">
			    <p>Produk / bagian hilang</p>
			</td>
			<td width="301" valign="top">
			    <p>Hilang item / bagian seperti yang ditunjukkan dalam kemasan</p>
			</td>
		    </tr>
		    <tr>
			<td width="174" valign="top">
			    <p>Tidak sesuai ukuran</p>
			</td>
			<td width="301" valign="top">
			    <p>Pelanggan menerima ukuran sesuai pesanan, namun tidak muat</p>
			</td>
		    </tr>
		    <tr>
			<td width="174" valign="top">
			    <p>Situs Gangguan</p>
			</td>
			<td width="301" valign="top">
			    <p>Produk tidak sesuai dengan spesifikasi, deskripsi dan gambar di website (masalah ini   disebabkan kesalahan website / informasi yang salah)</p>
			</td>
		    </tr>
		</tbody>
	    </table>
	    <p>Kecuali dinyatakan lain dalam deskripsi produk, persyaratan umum dan ketentuan &nbsp;untuk pengembalian produk adalah sebagai berikut:</p>
	    <ol>
		<li>Masih dalam periode pengembalian produk yang diperbolehkan selama <strong>14</strong> <strong>(empat belas) hari</strong> dari tanggal penerimaan</li>
		<li>Anda memiliki bukti pembelian (nomor pesanan/order, faktur pembelian, catatan pengiriman, pernyataan bank)</li>
		<li>Kondisi produk nya sama seperti saat diterima, termasuk kondisi kemasan/dus nya.</li>
		<li>Produk harus belum dipergunakan dan/atau belum ada program di install dan/ atau tidak ada data yang dimasukkan di dalamnya</li>
		<li><strong>Label harga dan barcode (sticker dengan garis hitam putih) masih tertera atau tertempel</strong> di produk nya.</li>
		<li>Barang akan dianggap sudah digunakan jika,</li>
		<ol>
		    <li>Barang tersebut tidak berada dalam kondisi baru</li>
		    <li>Tidak dikembalikan dalam kemasan asli bersama semua aksesoris (termasuk buku panduan, kartu garansi, sertifikat keaslian atau hadiah gratis/bundling jika ada)</li>
		    <li>Jika segel telah terbuka atau kemasan produk rusak</li>
		</ol>
		<li>Stiker pengembalian barang tidak ditempelkan dikemasan/box produk tapi pada box pengiriman</li>
		<li>Jika Anda menggunakan voucher pada saat pembelian, nilai voucher tidak dapat dikembalikan.</li>
		<li>Item yang dikembalikan dianggap <strong>rusak</strong></li>

		<ol>
		    <li>Produk rusak saat pengiriman</li>
		    <li>Produk tidak berfungsi seperti yang   dijelaskan dalam spesifikasi dari pabrik</li>
		    <li>Bukan produk yang dipesan oleh   pelanggan (misal: salah ukuran atau salah warna)</li>
		    <li>Item / bagian item hilang seperti   yang ditunjukkan dalam kemasan</li>
		    <li>Pelanggan menerima ukuran sesuai   pesanan, namun tidak muat</li>
		    <li>Produk tidak sesuai dengan   spesifikasi, deskripsi dan gambar di website (masalah ini disebabkan   kesalahan website / informasi yang salah)</li>
		</ol>
	    </ol>
	    <p><strong>Syarat Pengembalian Produk</strong></p>
	    <table class="table table-bordered">
		<tbody>
		    <tr>
			<td align="center">
			    <p><strong>Produk</strong></p>
			</td>
			<td colspan="2" align="center">
			    <p><strong>Alasan Pengembalian </strong></p>
			</td>
			<td align="center">
			    <p><strong>Pengembalian Diterima/Tidak </strong></p>
			</td>
		    </tr>
		    <tr>
			<td rowspan="4">
			    <p>Semua *kecuali untuk dibawah tabel berikut</p>
			</td>
			<td>
			    <p><br /> Berubah Pikiran</p>
			</td>
			<td>
			    <p>Pelanggan tidak membutuhkan produknya lagi</p>
			</td>
			<td>
			    <p>Pengembalian Diterima</p>
			</td>
		    </tr>
		    <tr>
			<td>
			    <p>Cacat</p>
			</td>
			<td>
			    <p>Mengarah pada definisi item yang cacat</p>
			</td>
			<td>
			    <p>Pengembalian Diterima</p>
			</td>
		    </tr>
		    <tr>
			<td rowspan="2">
			    <p>Tidak Cacat</p>
			</td>
			<td>
			    <p>Digunakan</p>
			</td>
			<td>
			    <p>Tidak dapat diterima</p>
			</td>
		    </tr>
		    <tr>
			<td>
			    <p>Tidak digunakan</p>
			</td>
			<td>
			    <p>Pengembalian diterima</p>
			</td>
		    </tr>
		</tbody>
	    </table>
	    <p>Kecuali:</p>
	    <table class="table table-bordered">
		<tbody>
		    <tr>
			<td align="center">
			    <p><strong>Produk </strong></p>
			</td>
			<td colspan="2" align="center">
			    <p><strong>Alasan Pengembalian</strong></p>
			</td>
			<td align="center">
			    <p><strong>Pengembalian Diterima/Tidak </strong></p>
			</td>
		    </tr>
		    <tr>
			<td rowspan="4">
			    <ul>
				<li>Air Conditioners (AC)</li>
				<li>Pengering</li>
				<li>Wine Cellar</li>
				<li>Gantungan</li>
				<li>Mesin Pencuci</li>
				<li>Lemari Es</li>
				<li>Pemanas Air</li>
				<li>TV</li>
				<li>Home Theatres</li>
			    </ul>
			</td>
			<td>
			    <p> Berubah Pikiran</p>
			</td>
			<td>
			    <p>Pelanggan tidak membutuhkan produknya lagi</p>
			</td>
			<td>
			    <p>Tidak dapat diterima</p>
			</td>
		    </tr>
		    <tr>
			<td>
			    <p>Cacat</p>
			</td>
			<td>
			    <p>Mengarah pada definisi item yang cacat</p>
			</td>
			<td>
			    <p>Pengembalian Diterima</p>
			</td>
		    </tr>
		    <tr>
			<td rowspan="2">
			    <p><br /> Tidak Cacat</p>
			</td>
			<td>
			    <p>Digunakan</p>
			</td>
			<td>
			    <p>Tidak dapat diterima</p>
			</td>
		    </tr>
		    <tr>
			<td>
			    <p>Tidak digunakan</p>
			</td>
			<td>
			    <p>Pengembalian diterima</p>
			</td>
		    </tr>
		</tbody>
	    </table>
	    <table class="table table-bordered">
		<tbody>
		    <tr>
			<td align="center">
			    <p><strong>Produk </strong></p>
			</td>
			<td align="center" colspan="2">
			    <p><strong>Alasan Pengembalian</strong></p>
			</td>
			<td align="center">
			    <p><strong>Pengembalian Diterima/Tidak </strong></p>
			</td>
		    </tr>
		    <tr>
			<td rowspan="4">
			    <ul>
				<li>Alat Kecantikan:
				    <ul>
					<li>Pengering Rambut</li>
					<li>Pengeriting Rambut</li>
					<li>Alat Cukur</li>
					<li>Pengharum Ruangan</li>
					<li>Make up</li>
					<li>Perawatan Kulit</li>
					<li>Perawatan Rambut</li>
				    </ul>
				</li>
				<li>Aksesoris Laptop/Komputer:
				    <ul>
					<li>Printer</li>
					<li>Printer Multifungsi</li>
					<li>Scanner</li>
					<li>Proyektor</li>
					<li>Komputer</li>
					<li>Laptop</li>
					<li>Ponsel</li>
					<li>Tablet</li>
				    </ul>
				</li>
			    </ul>
			</td>
			<td>
			    <p><br /> Berubah Pikiran</p>
			</td>
			<td>
			    <p>Pelanggan tidak membutuhkan produknya lagi</p>
			</td>
			<td>
			    <p>Pengembalian Diterima</p>
			</td>
		    </tr>
		    <tr>
			<td>
			    <p>Cacat</p>
			</td>
			<td>
			    <p>Mengarah pada definisi item yang cacat</p>
			</td>
			<td>
			    <p>Pengembalian Diterima</p>
			</td>
		    </tr>
		    <tr>
			<td rowspan="2">
			    <p><br />Tidak Cacat</p>
			</td>
			<td>
			    <p>Digunakan</p>
			</td>
			<td>
			    <p>Tidak dapat diterima</p>
			</td>
		    </tr>
		    <tr>
			<td>
			    <p>Tidak digunakan</p>
			</td>
			<td>
			    <p>Tidak dapat diterima</p>
			</td>
		    </tr>
		</tbody>
	    </table>
	    <p>&nbsp;</p>
	    <table class="table table-bordered">
		<tbody>
		    <tr>
			<td width="71">
			    <p><strong>Fashion </strong></p>
			</td>
			<td>
			    <p>TOKO1001 akan melakukan pengecekan produk yang dikembalikan untuk memastikan barang sesuai dengan klaim yang diajukan. Harap diperhatikan, jika mencoba produk sepatu, harap mencoba pada permukaan yang bersih.</p>
			</td>
		    </tr>
		</tbody>
	    </table>
	    <p>&nbsp;</p>
	    <table class="table table-bordered">
		<tbody>
		    <tr>
			<td width="83">
			    <p><strong>Jam Tangan </strong></p>
			</td>
			<td>
			    <p>TOKO1001 akan   melakukan pengecekan produk yang dikembalikan untuk memastikan barang sesuai   dengan klaim yang diajukan.</p>
			</td>
		    </tr>
		</tbody>
	    </table>
	    <p>&nbsp;</p>
	    <p>Dalam kasus Anda telah membuka segel, silakan lihat bantuan ke pusat layanan yang relevan ditunjukkan pada kartu garansi.</p>
	    <p>Kami tidak akan menerima pengembalian untuk salah satu produk berikut:</p>
	    <table class="table table-bordered">
		<tbody>
		    <tr>
			<td width="220" align="center">
			    <p><strong>Kategori</strong></p>
			</td>
			<td width="600" align="center">
			    <p><strong>Produk Yang Tidak Bisa Dikembalikan</strong></p>
			</td>
		    </tr>
		    <tr>
			<td>
			    <p>Handphone &amp; Tablet <br /> Peralatan Elektronok Rumah Tangga <br /> Komputer dan Laptop <br /> Kamera</p>
			</td>
			<td>
			    <p>Ipod</p>
			</td>
		    </tr>
		    <tr>
			<td>
			    <p>Mainan, Anak &amp; Bayi</p>
			</td>
			<td>
			    <p>Susu, makanan bayi, makanan dan minuman untuk ibu &amp; bayi</p>
			</td>
		    </tr>
		    <tr>
			<td>
			    <p>Media, Games &amp; Musik</p>
			</td>
			<td>
			    <p>Buku, majalah, surat kabar dan majalah <br /> Audio atau video rekaman (yaitu CD atau DVD) atau perangkat lunak komputer (yaitu permainan komputer) yang telah dibuka <br /> eBooks, eMagazines, voucher elektronik</p>
			</td>
		    </tr>
		    <tr>
			<td>
			    <p>Kesehatan dan Kecantikan</p>
			</td>
			<td>
			    <p>Kosmetik &amp; Parfum <br /> Produk suplemen <br /> Produk kesenangan pribadi</p>
			</td>
		    </tr>
		    <tr>
			<td>
			    <p>Produk lainnya</p>
			</td>
			<td>
			    <p>Hadiah gratis <br /> Makanan dan Minuman <br /> Bunga dan produk dengan penggunaan dalam jangka waktu terbatas</p>
			</td>
		    </tr>
		</tbody>
	    </table>
	    <p>&nbsp;</p>
	    <p><strong>Cara Pengembalian Produk</strong><strong></strong></p>
	    <p>TOKO1001 berupaya untuk menghadirkan produk dan layanan yang terbaik untuk kami. Jika Anda merasa tidak puas dengan produk yang Anda terima, Anda bisa mengembalikan atau menukar barang yang Anda beli. <strong><span style="text-decoration: underline;">Langkah pengembalian produk</span></strong> :</p>
	    <ul>
		<li>Silahkan isi formulir pengembalian online TOKO1001 di <a href="http://www.toko1001.id/"><strong>www.toko1001.id</strong></a>.</li>
	    </ul>
	    <p style="margin-left: 50px;">Dengan cara &nbsp;mudah sebagai berikut:</p>
	    <ol style="margin-left: 30px;">
		<li>Masuk ke akun Anda</li>
		<li>Pilih "Pesanan Saya"</li>
		<li>Pilih pesanan yang akan dikembalikan dan klik pada "Pengembalian"</li>
		<li>Isi formulir pengembalian online dan isi kolom komentar</li>
		<li>Tempelkan stiker pengembalian diluar kotak pengiriman (bukan di kemasan produk)</li>
		<li>Kirim ke <em>warehouse </em>TOKO1001</li>
	    </ol>
	    <ul>
		<li>Siapkan produk yang akan dikembalikan, pastikan untuk return slip sudah di isi dan disertakan dalam paket. Khusus untuk sepatu, jangan menempel apapun di dus/kemasan asli sepatu nya, lapisi dus/kemasan nya dengan kertas lain agar tidak merusak dus /kemasan sepatu yang asli nya. Tempel stiker pengembalian di luar paket yang sudah di lapisi tersebut.</li>
	    </ul>
	    <ul>
		<li>Kirim balik produknya ke Warehouse TOKO1001, tergantung lokasi Anda : <br /> <strong>Jakarta</strong> : Anda bisa pilih pick up (pilih pick up saat isi online form) penjemputan barang oleh kurir kami, atau kirimkan kembali ke Warehouse TOKO1001 menggunakan jasa pengiriman pilihan Anda. <br /> <strong>Luar Jakarta</strong> : silahkan kirimkan kembali ke Warehouse TOKO1001 menggunakan jasa pengiriman pilihan Anda. </li>
	    </ul>
	    <p><strong>Alamat Warehouse TOKO1001</strong></p>
	    <p><strong>Tim Pengembalian Produk</strong></p>
	    <p><strong><br /> PT. DELTAMAS MANDIRI SEJAHTERA<br /> Jl. Indokarya Barat I Blok D No. 1<br /> Papanggo, Sunter<br /> Jakarta Utara 14330, Indonesia </strong></p>
	    <p><strong><span style="text-decoration: underline;">CATATAN</span>:</strong> Formulir pengembalian online hanya bisa diakses apabila produk telah diterima.<br /> Jika Anda sudah menerima pesanan namun masih menemukan pesan "Produk belum terkirim", klik pada link <a href="http://www.toko1001.id/">www.toko1001.id</a> untuk menginformasikan bahwa barang telah diterima.</p>
	    <ul>
		<li>Tunggu konfirmasi lebih lanjut dari TOKO1001, Anda akan menerima email dari kami jika barang nya sudah diterima dan sudah di proses di Warehouse kami, estimasi penerimaan produk tergantung jenis pengiriman balik yang digunakan. </li>
	    </ul>
	    <p>&nbsp;</p>
	    <p><strong>Biaya Pengembalian Produk</strong><strong></strong></p>
	    <p><strong>Gratis Pengembalian Produk ke TOKO1001 !</strong></p>
	    <p>Ongkos kirim untuk pengembalian produk harus dibayarkan oleh Anda dahulu. <br /> Kemudian setelah kami menerima produknya, kami akan melakukan proses evaluasi dan akan mengembalikan biaya pengiriman jika pengembalian Anda dinyatakan valid.</p>
	    <p>Kami akan mengganti biaya pengiriman balik Anda (jika menggunakan jasa pengiriman) maksimal Rp 30.000.</p>
	    <p>Caranya cukup foto Resi pengiriman yang Anda gunakan dan kirimkan dalam bentuk lampiran/attachment melalui email ke cc@toko1001.id, cantumkan juga :</p>
	    <ul>
		<li>Nomor Order</li>
		<li>Biaya pengiriman</li>
		<li>Detail Bank untuk Refund (nama bank &amp; cabang, nomor rekening dan nama pemilik rekening)</li>
	    </ul>
	    <p>Proses Pergantian/Pengembalian Dana hanya dapat dilakukan jika kami telah selesai melakukan pengecekan produk Anda. <br /> Proses pengecekan ini membutuhkan waktu hingga <strong>5 (lima) hari kerja</strong>.</p>
	    <p>CATATAN :</p>
	    <p>Produk Anda tidak dapat dikembalikan setelah melewati 14 hari produk diterima; produk tersebut hanya bisa di service.</p>
	    <p>Kami sarankan Anda menghubungi dan mengirim produk langsung ke pusat garansi untuk bantuan lebih lanjut. Anda dapat menemukan semua informasi kontak dari service centre produk Anda di kartu garansi yang terletak di dalam paket.</p>

	    <?php
	    break;
	case "pengembalian-dana":
	    ?>
	    <h2>Pengembalian Dana</h2>
	    <p>
		Definisi Pengembalian Dana (refund)<br />
		Pengembalian Dana dilakukan atas pembatalan produk setelah pembayaran pesanan, kelebihan pembayaran, tidak terkirimnya pesanan dan pengembalian produk.<br />

		Beberapa hal yang bisa mengakibatkan proses Pengembalian Dana (refund) terjadi, seperti:<br />

	    <ul>
		<li>Kelebihan pembayaran</li>
		<li>Penjual tidak bisa menyanggupi order karena kehabisan stock, perubahan ongkos kirim, maupun penyebab lainnya</li>
		<li>Penjual sudah menyanggupi pengiriman order, tetapi setelah batas waktu yang ditentukan ternyata tidak pernah mengirimkan barang</li>
	    </ul>

	    <h4>Metode Pengembalian Dana (refund)</h4>
	    <p>Refund akan dilakukan berdasarkan metode pembayaran yang dilakukan oleh Pembeli pada saat melakukan proses pemesanan.</p>
	    <p>Apabila Pembatalan dilakukan oleh Pembeli, maka pengembalian dana dapat langsung dikembalikan ke bank rekening Pembeli setelah memasukkan nomor rekening yang benar melalui menu Pengajuan Pembatalan di <strong>toko1001.id</strong></p>
	    <p>Proses <em>refund</em> ke rekening Pembeli dilakukan dalam jangka waktu maksimum 5 (lima) hari kerja.</p>
	    <p>Proses <em>refund</em> hanya dapat dilakukan jika kami telah selesai melakukan pengecekan produk Anda. <br /> Proses pengecekan ini membutuhkan waktu hingga <strong>5 </strong><strong>(lima) </strong><strong>hari kerja</strong></p>
	    <p>Dalam hal adanya ketidaksepakatan atau perbedaan pendapat antara Penjual dan Pembeli mengenai hal ini, maka adalah Ketentuan TOKO1001 yang berlaku</p>
	    <p>Apabila pembatalan dilakukan oleh Penjual atau terbatalkan otomatis oleh sistem, maka pengembalian dana akan dikembalikan sementara kedalam bentuk kredit POIN yang dapat dicairkan langsung ke nomor rekening Pembeli melalui menu Saldo Pengembalian / Deposit di <strong>toko1001.id</strong></p>

	</p>

	<?php
	break;
    case "bantuan":
	?>
	<h1>Bantuan / F A Q</h1>
	<?php
	break;
    default :
	redirect(base_url() . "error_404");
}
?>
</div>