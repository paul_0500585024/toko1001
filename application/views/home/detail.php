<?php
$total_product_review = isset($product_review['total_product_review']) ? $product_review['total_product_review'] : '';
$product_review = isset($product_review['product_review']) ? $product_review['product_review'] : '';
require_once VIEW_BASE_HOME;
$number_of_pic = 6;
?>
<!--    for sm-->
<div class="hidden-xs hidden-lg " style="margin-top:65px;">&nbsp;</div>
<!--    for lg md-->
<div class="hidden-xs hidden-sm hidden-md" style="margin-top:40px;">&nbsp;</div>
<div class="container hidden-xs">
    <!-- //Tampilkan Title Produk utk lg,sm,md-->
    <div class="col-lg-4 col-md-5 col-sm-6" style="margin-right:0px;">
        <div class="pagi-container" >
            <!-- Following DIV plays an Important Role for Hiding the Bottom Transition of Images overlapping the Pagination Thumbnail images. -->
            <?php for ($i = 1; $i <= $number_of_pic; $i++): ?>
                <?php if ($detail_description->{"image" . $i} != ""): ?>
                    <input type="radio" name="input_thumb" id='input_detail_big_thumb<?php echo $i ?>'>
                    <label for='id_detail_big_thumb<?php echo $i ?>'>
                        <img
                            src='<?php echo get_file_exists(PRODUCT_UPLOAD_IMAGE . "/" . $detail_description->merchant_seq . "/" . XL_IMAGE_UPLOAD . "/" . $detail_description->{"image" . $i}); ?>' alt="<?php echo $detail_description->{"image" . $i} ?>"
                            height="42px " id='id_detail_big_thumb<?php echo $i ?>'>
                    </label>
                    <img
                        data-src='<?php echo get_file_exists(PRODUCT_UPLOAD_IMAGE . "/" . $detail_description->merchant_seq . "/" . XL_IMAGE_UPLOAD . "/" . $detail_description->{"image" . $i}); ?>' alt="<?php echo $detail_description->{"image" . $i} ?>"
                        data-src-retina='<?php echo get_file_exists(PRODUCT_UPLOAD_IMAGE . "/" . $detail_description->merchant_seq . "/" . XL_IMAGE_UPLOAD . "/" . $detail_description->{"image" . $i}); ?>'
                        src="<?php echo get_image_location() . ASSET_IMG_HOME . 'blank.png' ?>"
                        alt="Image" class="DefaultBigStyle1 load_img" id="id_data_small_thumb<?php echo $i ?>"
                        style="opacity:1;">
                        <?php /*                    <img
                          data-src='<?php echo get_file_exists(PRODUCT_UPLOAD_IMAGE . "/" . $detail_description->merchant_seq . "/" . XL_IMAGE_UPLOAD . "/" . $detail_description->{"image" . $i}); ?>' alt="<?php echo $detail_description->{"image" . $i} ?>"
                          data-src-retina='<?php echo get_file_exists(PRODUCT_UPLOAD_IMAGE . "/" . $detail_description->merchant_seq . "/" . XL_IMAGE_UPLOAD . "/" . $detail_description->{"image" . $i}); ?>'
                          src="<?php echo get_file_exists(PRODUCT_UPLOAD_IMAGE . "/" . $detail_description->merchant_seq . "/" . XL_IMAGE_UPLOAD . "/" . $detail_description->{"image" . $i}); ?>"
                          alt="Image" class="DefaultBigStyle1 load_img" id="id_data_small_thumb<?php echo $i ?>"
                          style="opacity:1;"> */ ?>
                    <?php endif; ?>
                <?php endfor; ?>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 ">
        <div class="detail">
            <div class="col-lg-12 col-md-12 col-sm-12 hidden-xs">
                <div>
                    <?php
                    $number_of_category = count($seq_self_parent_category);
                    $counter_category = 0;
                    ?>
                    <?php foreach ($seq_self_parent_category as $product_category_seq => $each_seq_self_parent_category): ?>
                        <strong><a href="<?php echo base_url() . url_title(strtolower($each_seq_self_parent_category)) . "-" . CATEGORY . ($product_category_seq) ?>"><?php echo $each_seq_self_parent_category; ?></a></strong>
                        <?php if ($counter_category < $number_of_category - 1): ?>
                            &nbsp;<i class="fa fa-caret-right"></i>&nbsp;
                        <?php endif; ?>
                        <?php $counter_category++; ?>
                    <?php endforeach; ?>

                </div>
                <h1><?php echo $detail_description->name . get_variant_value($detail_description->variant_seq, $detail_description->variant_value, "-"); ?></h1>
                <div class="hidden-xs hidden-lg " style="margin-bottom:20px;"><strong>Dijual Oleh </strong> <a href="<?php echo base_url() . 'merchant/' . $detail_description->code; ?>"><?php echo $detail_description->merchant_name ?></a></div><p>
                    <?php if ($detail_description->product_price != $detail_description->sell_price): ?>
                        <!--<hr>-->
                        <span class="old_price"><?php echo RP, cnum($detail_description->product_price); ?></span>
                        <span class="detail_diskon">> <?php echo get_percentage_discount($detail_description->product_price, $detail_description->sell_price); ?> OFF</span>
                    <?php endif; ?>
                    <br>
                    <span class="new_price"><?php echo RP, cnum($detail_description->sell_price); ?></span>

            </div>

            <div class="col-lg-12 col-md-12 col-sm-12">
                <br>
                <div class="row">
                    <?php if ($detail_description->stock == '' OR $detail_description->stock <= 0) : ?>
                        <div class="col-lg-6 col-md-12 col-sm-12 "><b>Stock Tidak tersedia</b></div>
                    <?php else: ?>
                        <div class="col-lg-6 col-md-12 col-sm-12 " style="margin-bottom:10px;">
                            <?php if (!isset($_SESSION[SESSION_MEMBER_CSRF_TOKEN])) { ?>
                                <a onClick ="login_register()" class="btn btn-block btn-front btn-flat"><i class="fa fa-arrow-circle-o-left"></i>&nbsp; Beli dan Selesai Belanja</a>
                            <?php } else { ?>
                                <button onClick ="<?php echo "add_product(" . $detail_description->product_variant_seq . ", true)" ?>" class="btn btn-block btn-front btn-flat"><i class="fa fa-arrow-circle-o-left"></i>&nbsp; Beli dan Selesai Belanja</button>
                            <?php } ?>
                        </div>

                        <div class="col-lg-6 col-md-12 col-sm-12 ">
                            <button type="button" onClick ="<?php echo "add_product(" . $detail_description->product_variant_seq . ")" ?>" class="btn btn-block btn-flat btn-success"><i class="fa fa-shopping-cart"></i>&nbsp;Tambahkan ke Keranjang</button>
                        </div>
                    <?php endif; ?>
                </div>
            </div>

            <?php if (isset($product_related)): ?>
                <div class="hidden-sm col-lg-12 col-md-12 col-xs-12" style="margin-top:10px;" >
                    <strong> <strong>Pilihan :</strong> </strong><br>
                    <div style="margin-top:10px;">
                        <?php foreach ($product_related as $each_product_related): ?>

                            <a href="<?php echo base_url() . strtolower(url_title($each_product_related->name . ' ' . $each_product_related->variant_value)) . '-' . $each_product_related->product_variant_seq ?>">
                                <img class="img-terbaru load_img"
                                     data-src="<?php echo get_file_exists(PRODUCT_UPLOAD_IMAGE . $each_product_related->merchant_seq . '/' . XL_IMAGE_UPLOAD . "/" . $each_product_related->image) ?>"
                                     data-src-retina="<?php echo get_file_exists(PRODUCT_UPLOAD_IMAGE . $each_product_related->merchant_seq . "/" . XL_IMAGE_UPLOAD . "/" . $each_product_related->image) ?>"
                                     src="<?php echo get_image_location() . ASSET_IMG_HOME . 'blank.png' ?>" alt="blank" height="42px">
                            </a>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>
            &nbsp;
            <!--FOR MD-->
            <div class="hidden-sm" >
                &nbsp;&nbsp;&nbsp;&nbsp;<strong><strong>Bagikan produk ini kepada teman dan keluarga anda ! </strong></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </div>
            <div class="hidden-sm col-lg-10 col-md-10 col-xs-10" style="margin-top:10px;">
                <div class="st_facebook_custom col-lg-2 col-md-2 col-sm-2 col-xs-2" displayText='Facebook'></div>
                <div class="st_twitter_custom col-lg-2 col-md-2 col-sm-2 col-xs-2" displayText='Tweet'></div>
                <div class="st_pinterest_custom col-lg-2 col-md-2 col-sm-2 col-xs-2" displayText='Pinterest'></div>
                <div class="st_googleplus_custom col-lg-2 col-md-2 col-sm-2 col-xs-2" displayText='Google +'></div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><?php echo $wishlist; ?></div>
            </div>
            <div class="col-md-6 text-right hidden-sm" style="margin-top:5px;">
                <?php if ($delivery_city != ''): ?>
                    <button id="pengirimangratis" type="button" class="btn btn-front btn-flat hidden-lg"  style="background-color: #0992A6"
                            data-toggle="popover" title="Pengiriman Gratis"  data-trigger='click'
                            data-content="<?php echo $delivery_city; ?>" data-placement="right" data-html="true">

                        <i class="fa fa-truck"></i>&nbsp;Pengiriman Gratis
                    </button>
                <?php endif; ?>
            </div>
            <div class="hidden-lg hidden-xs hidden-md ">
                <?php if (isset($product_related)): ?>
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12" >
                        <strong>Pilihan :</strong> <br>
                        <div style="margin-top:10px;">
                            <?php foreach ($product_related as $each_product_related): ?>
                                <a href="<?php echo base_url() . strtolower(url_title($each_product_related->name . ' ' . $each_product_related->variant_value)) . '-' . $each_product_related->product_variant_seq ?>">
                                    <img class="img-terbaru load_img"
                                         data-src="<?php echo get_file_exists(PRODUCT_UPLOAD_IMAGE . $each_product_related->merchant_seq . '/' . XL_IMAGE_UPLOAD . "/" . $each_product_related->image) ?>"
                                         data-src-retina="<?php echo get_file_exists(PRODUCT_UPLOAD_IMAGE . $each_product_related->merchant_seq . "/" . XL_IMAGE_UPLOAD . "/" . $each_product_related->image) ?>"
                                         src="<?php echo get_image_location() . ASSET_IMG_HOME . 'blank.png' ?>" alt="blank" height="42px">
                                </a>
                            <?php endforeach; ?>
                        </div>
                    </div>
                <?php endif; ?>
                &nbsp;
                <div class="col-sm-12"><strong>Bagikan produk ini kepada teman dan keluarga anda ! </strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </div>

                <div class="col-lg-8 col-md-8 col-xs-8 col-sm-6" style="margin-top:10px;">
                    <div class="st_facebook_custom col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding" displayText='Facebook'></div>
                    <div class="st_twitter_custom col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding" displayText='Tweet'></div>
                    <div class="st_pinterest_custom col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding" displayText='Pinterest'></div>
                    <div class="st_googleplus_custom col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding" displayText='Google +'></div>
                </div>
                <div class="col-sm-6 " style="margin-top:10px;">

                    <?php if ($delivery_city != ''): ?>
                        <button type="button" id="pengirimangratis" class="btn btn-front btn-flat btn-sm"  style="background-color: #0992A6"
                                data-toggle="popover" title="Pengiriman Gratis"  data-trigger='click'
                                data-content="<?php echo $delivery_city; ?>" data-placement="right" data-html="true">
                            <i class="fa fa-truck"></i>&nbsp;Pengiriman Gratis
                        </button>
                    <?php endif; ?>
                </div>



            </div>
        </div>
    </div>

    <div class="col-lg-2 col-md-1 hidden-sm ">
        <div class="detail">
            <div class="info">
                <p class="hidden-md">
                    <strong >Dijual Oleh : </strong><br /> <a href="<?php echo base_url() . 'merchant/' . $detail_description->code; ?>"><?php echo $detail_description->merchant_name ?></a>
                </p>
                <p>

                    <?php if ($delivery_city != ''): ?>
                        <button type="button" class="btn btn-front btn-flat  btn-sm hidden-md"  style="background-color: #0992A6"
                                data-toggle="popover" title="Pengiriman Gratis"  data-trigger='click'
                                data-content="<?php echo $delivery_city; ?>" data-placement="bottom" data-html="true">
                            <i class="fa fa-truck"></i>&nbsp;Pengiriman Gratis
                        </button>
                    <?php endif; ?>

                    <?php if ($delivery_city != ''): ?>
                        <!--                        <button id="pengirimangratis" type="button" class="btn btn-front btn-flat hidden-lg"  style="background-color: #0992A6"
                                                        data-toggle="popover" title="Pengiriman Gratis"  data-trigger='click'
                                                        data-content="<?php echo $delivery_city; ?>" data-placement="left" data-html="true">

                                                    <i class="fa fa-truck"></i>&nbsp;Pengiriman Gratis
                                                </button>-->
                    <?php endif; ?>
                </p>
            </div>
        </div>
    </div>

</div>


<!-- For xs -->
<div class="container hidden-lg hidden-md hidden-sm">
    <div class="detail"><center>
            <div style="z-index:999">
                <?php
                $number_of_category = count($seq_self_parent_category);
                $counter_category = 0;
                ?>
                <?php foreach ($seq_self_parent_category as $product_category_seq => $each_seq_self_parent_category): ?>
                    <strong><a  href="<?php echo base_url() . url_title(strtolower($each_seq_self_parent_category)) . "-" . CATEGORY . ($product_category_seq) ?>"><?php echo $each_seq_self_parent_category; ?></a></strong>
                    <?php if ($counter_category < $number_of_category - 1): ?>
                        &nbsp;<i class="fa fa-caret-right"></i>&nbsp;
                    <?php endif; ?>
                    <?php $counter_category++; ?>
                <?php endforeach; ?>
            </div>
            <h1><?php echo $detail_description->name . get_variant_value($detail_description->variant_seq, $detail_description->variant_value, "-"); ?></h1>
        </center>
    </div>


    <div class="pagi-container" style="padding-top:0px;" >
        <br>
        <!-- Following DIV plays an Important Role for Hiding the Bottom Transition of Images overlapping the Pagination Thumbnail images. -->
        <center><div id="id_display_xs_detail_big_thumb" style="text-align: center; " ></div></center>

        <?php for ($i = 1; $i <= $number_of_pic; $i++): ?>
            <?php if ($detail_description->{"image" . $i} != ""): ?>
                <input type="radio" name="input_thumb" id='id_detail_big_thumb<?php echo $i ?>' >
                <label for="id_detail_big_thumb<?php echo $i ?>">
                    <img
                        src='<?php echo get_file_exists(PRODUCT_UPLOAD_IMAGE . "/" . $detail_description->merchant_seq . "/" . XL_IMAGE_UPLOAD . "/" . $detail_description->{"image" . $i}); ?>' alt="<?php echo $detail_description->{"image" . $i} ?>"
                        height="42px " id='id_detail_big_thumb<?php echo $i ?>'>
                </label>
                <img
                    src="<?php echo get_file_exists(PRODUCT_UPLOAD_IMAGE . "/" . $detail_description->merchant_seq . "/" . XL_IMAGE_UPLOAD . "/" . $detail_description->{"image" . $i}); ?>" alt="<?php echo $detail_description->{"image" . $i} ?>"
                    alt="Image" class="DefaultBigStyle1" id="id_xs_data_small_thumb<?php echo $i ?>" style="z-index:-1">
                <?php endif; ?>
            <?php endfor; ?>
    </div>
</center>




<div class="col-xs-12">
    <div class="detail">

        <div class="row">
            <center>
                <?php if ($detail_description->product_price != $detail_description->sell_price): ?>
                    <div class=" col-xs-12">
                        <span class="old_price"><?php echo RP, cnum($detail_description->product_price); ?></span> <span
                            class="detail_diskon">> <?php echo get_percentage_discount($detail_description->product_price, $detail_description->sell_price); ?> OFF</span>
                    </div>
                <?php endif; ?>
                <div class=" col-xs-12">
                    <span class="new_price"><?php echo RP, cnum($detail_description->sell_price); ?></span>
                </div>
                <br>

            </center>
        </div>
        <hr>
        <div clas="row">
            <?php if ($detail_description->stock == '' OR $detail_description->stock <= 0) : ?>
                <div class="row" style="text-align:center"><b>Stock Tidak tersedia</b></div>
            <?php else: ?>
                <div style="margin-bottom:10px;">
                    <?php if (!isset($_SESSION[SESSION_MEMBER_CSRF_TOKEN])) { ?>
                        <a onClick ="login_register()" class="btn btn-block btn-front btn-flat"><i class="fa fa-arrow-circle-o-left"></i>&nbsp; Beli dan Selesai Belanja</a>
                    <?php } else { ?>
                        <button onClick ="<?php echo "add_product(" . $detail_description->product_variant_seq . ", true)" ?>" class="btn btn-block btn-front btn-flat"><i class="fa fa-arrow-circle-o-left"></i>&nbsp; Beli dan Selesai Belanja</a>
                        <?php } ?>
                </div>
                <div >
                    <button type="button" onClick ="<?php echo "add_product(" . $detail_description->product_variant_seq . ")" ?>" class="btn btn-block btn-flat btn-success"><i class="fa fa-shopping-cart"></i>&nbsp;Tambahkan ke Keranjang</button>
                </div>
            <?php endif; ?>
        </div>
        <?php if (isset($product_related)): ?>
            <div class="row">
                <div  style="margin-top:20px;">
                    <strong>Pilihan :</strong> <br>
                    <div  style="margin-top:10px;">
                        <?php foreach ($product_related as $each_product_related): ?>
                            <a href="<?php echo base_url() . strtolower(url_title($each_product_related->name . ' ' . $each_product_related->variant_value)) . '-' . $each_product_related->product_variant_seq ?>">
                                <img class="img-terbaru load_img"
                                     data-src="<?php echo get_file_exists(PRODUCT_UPLOAD_IMAGE . $each_product_related->merchant_seq . '/' . XL_IMAGE_UPLOAD . "/" . $each_product_related->image) ?>"
                                     data-src-retina="<?php echo get_file_exists(PRODUCT_UPLOAD_IMAGE . $each_product_related->merchant_seq . "/" . XL_IMAGE_UPLOAD . "/" . $each_product_related->image) ?>"
                                     src="<?php echo get_image_location() . ASSET_IMG_HOME . 'blank.png' ?>" alt="blank" height="42px">
                            </a>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <div class="row info ">
            <p>
                <strong>Bagikan produk ini kepada teman dan keluarga anda ! </strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <br>
            </p>

            <div class="col-lg-4 col-md-4 col-xs-12 nopadding ">
                <div class="st_facebook_custom col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding" displayText='Facebook'></div>
                <div class="st_twitter_custom col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding" displayText='Tweet'></div>
                <div class="st_pinterest_custom col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding" displayText='Pinterest'></div>
                <div class="st_googleplus_custom col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding" displayText='Google +'></div>
            </div>
        </div>

    </div>
</div>

&nbsp;
<div class="info" >
    <p>
        <strong>Dijual Oleh:</strong><br /> <a href="<?php echo base_url() . 'merchant/' . $detail_description->code; ?>"><?php echo $detail_description->merchant_name ?></a>
    </p>
    <?php if ($delivery_city != ''): ?>
        <p>
            <button id="pengirimangratis" type="button" class="btn btn-front btn-flat btn-sm"  style="background-color: #0992A6"
                    data-toggle="popover" title="Pengiriman Gratis"  data-trigger='click'
                    data-content="<?php echo $delivery_city; ?>" data-placement="bottom" data-html="true">
                <i class="fa fa-truck"></i>&nbsp;Pengiriman Gratis
            </button>
        </p>
    <?php endif; ?>
</div>



</div>

<!-- Content -->
<br>
<div class="container">

    <ul class="nav nav-tabs sidebar-tabs" id="sidebar" role="tablist">
        <li role="presentation" class="active"><a href="#deskripsi"  role="tab" data-toggle="tab">Deskripsi</a></li>
        <li role="presentation"><a href="#spesifikasi" role="tab" data-toggle="tab" >Spesifikasi</a></li>
        <li role="presentation"><a href="#ulasan" role="tab" data-toggle="tab" >Ulasan</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="deskripsi"><br/><?php echo $detail_description->description; ?></div>
        <div class="tab-pane deskripsi" id="spesifikasi">
            <table
                class="field-group-format group_specs table table-striped table-bordered table-striped">
                <tbody>
                    <?php if (isset($detail_specification)): ?>
                        <?php foreach ($detail_specification as $each_detail_specification): ?>
                            <?php if (isset($each_detail_specification->name) OR isset($each_detail_specification->value)): ?>
                                <tr>
                                    <?php if (isset($each_detail_specification->name)): ?>
                                        <th class="field-label"><?php echo $each_detail_specification->name ?></th>
                                    <?php endif; ?>
                                    <?php if (isset($each_detail_specification->value)): ?>
                                        <td class="field-content">
                                            <div
                                                class="commerce-product-field commerce-product-field-field-package-content field-field-package-content product-field-package-content">
                                                <div
                                                    class="field field-name-field-package-content field-type-text-long field-label-hidden">
                                                    <div class="field-items">
                                                        <div class="field-item even"><?php echo $each_detail_specification->value ?></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <?php if (isset($detail_description->specification)): ?>
                        <tr style="border: 1px solid #ddd;">
                            <td colspan="2"><?php echo $detail_description->specification ?></td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
        <div class="tab-pane deskripsi" id="ulasan">
            <?php if ($total_product_review == '0'): ?>
                <p>Belum ada ulasan</p>
            <?php else: ?>
                <nav class="navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse-sort-review" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand">Ulasan Pelanggan (Total <?php echo $total_product_review ?>)</a>
                        </div>
                        <div class="collapse navbar-collapse" id="collapse-sort-review">
                            <ul class="nav navbar-nav navbar-right">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Atur berdasarkan<span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="javascript:create_ulasan('0','3','','1')">Tanggal Dibuat</a></li>
                                        <li><a href="javascript:create_ulasan('0','3','','2')">Ulasan Terbaik</a></li>
                                        <li><a href="javascript:create_ulasan('0','3','','3')">Rating Terburuk</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>                        
                    </div>
                </nav>

                <div class="well" id="display_product_review">                 
                    <?php /*                    <?php foreach($product_review as $each_product_review):?>
                      <div class="row">
                      <div class="col-md-12">
                      <?php echo create_star($each_product_review->rate);?><?php echo $each_product_review->name?>
                      <span class="pull-right"><?php echo $each_product_review->days_old?> hari lalu</span>
                      <p><?php echo $each_product_review->review_admin?></p>
                      </div>
                      </div>
                      <hr>
                      <?php endforeach;?>
                     */ ?>
                </div>            
            <?php endif; ?>
        </div>
    </div>
    <hr>
</div>

<div class="container">    
    <div class="row">
        <?php if (isset($product_recent)) { ?>
            <div class="col-lg-6 col-md-5 col-sm-6 col-xs-10">

                <h4><strong>Produk yang telah dilihat :</strong> </h4>                                            
                <div class="pull-left">
                    <ul class="bxslider">
                        <?php echo display_product_slide($product_recent) ?>
                    </ul>
                </div>
            </div>
        <?php } ?>
        <?php if (isset($product_buy_other)) { ?>
            <div class="col-lg-6 col-md-5 col-sm-6 col-xs-10 ">            
                <h4><strong> Produk lain yang dibeli bersama ini  : </strong> </h4>                                            
                <div class="pull-left">
                    <ul class="bxslider">
                        <?php echo display_product_slide($product_buy_other) ?>
                    </ul>
                </div>
            </div>
        <?php } ?>
    </div></div>
<script type="text/javascript">
                                

                                    $('.bxslider').bxSlider({
                                        minSlides: 3,
                                        maxSlides: 3,
                                        slideWidth: 170,
                                        slideMargin: 0
                                    });
                                    $(".bx-pager bx-default-pager").hide();
                                
</script>
<input type="hidden" id="active_img_zoom_container_number" value="" />
<script>var product_variant_seq = '<?php echo $product_variant_seq ?>';</script>
<script text="javascript">
    function add_product(product_seq, redirect) {
<?php if (!isset($_SESSION[SESSION_MEMBER_CSRF_TOKEN])) { ?>
            $("#login_register").modal('show');
            $("#daftar").removeClass('active');
            $("#tabregister").removeClass('active');
            $("#tablogin").addClass('active');
            $("#login").addClass('active');

<?php } else { ?>
            $.ajax({
                url: "<?php echo base_url("member/main_page"); ?>",
                type: "POST",
                dataType: "json",
                data: {
                    "txtMemberSID": "<?php echo $_SESSION[SESSION_MEMBER_CSRF_TOKEN]; ?>",
                    "type": "<?php echo TASK_TYPE_ADD ?>",
                    "product_seq": product_seq
                },
                success: function(data) {
                    if (isSessionExpired(data)) {
                        response_object = json_decode(data);
                        url = response_object.url;
                        location.href = url;
                    } else {
                        if (redirect != undefined) {
                            location.href = "<?php echo base_url("member/checkout") . "?step=1" ?>";
                        }
                        if (data[product_seq].count > 0) {
                            $('#cart_qty_md').text(data[product_seq].count);
                            $('#cart_qty_xs').text(data[product_seq].count);
                            var number_active_img_zoom = '';
                            var itemImg = '';
                            if (findBootstrapEnvironment() == 'xs') {
                                itemImg = $("#id_display_xs_detail_big_thumb");
                                flyToElement($(itemImg), $('.bag_box_xs'));
                            } else if (findBootstrapEnvironment() == 'md') {
                                number_active_img_zoom = $('#active_img_zoom_container_number').val();
                                itemImg = $("[id^='id_data_small_thumb" + number_active_img_zoom + "']");
                                flyToElement($(itemImg), $('.bag_box_sm'));
                            } else if (findBootstrapEnvironment() == 'lg') {
                                number_active_img_zoom = $('#active_img_zoom_container_number').val();
                                itemImg = $("[id^='id_data_small_thumb" + number_active_img_zoom + "']");
                                flyToElement($(itemImg), $('.bag_box'));
                            }
                            ;
                            var content =
                                    "<tr id='row_" + product_seq + "' class='product'>" +
                                    "<td class='cart'>" +
                                    "<a href='" + "<?php echo base_url(); ?>" + data[product_seq].product_name.replace(/\s+/g, '-').toLowerCase() + "-" + product_seq + "'>" +
                                    "<center><img src='" + "<?php echo get_image_location() . PRODUCT_UPLOAD_IMAGE; ?>" + data[product_seq].merchant_seq + "/" + "<?php echo S_IMAGE_UPLOAD; ?>" + data[product_seq].img + "'" +
                                    "alt='" + data[product_seq].img + "'" +
                                    "width=100px'></a><center></td><td class='cart' align='left'><strong>" + data[product_seq].product_name + "</strong><br><br>" +
                                    "<table border='0' class='table table-condensed'>" +
                                    "<tr>" +
                                    "<td width = '20%' > Berat </td>" +
                                    "<td width = '5%' > : </td>" +
                                    "<td>" + format(data[product_seq].product_weight) + " kg</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                    "<td> Warna </td>" +
                                    "<td > : </td>" +
                                    "<td >" + data[product_seq].variant_name + "</td>" +
                                    "</tr>" +
                                    "</table>" +
                                    "<div class='row'>" +
                                    "<div class='col-lg-10 col-md-10 col-sm-10 text-left'>" +
                                    "<i class = 'material-icons' style='font-size:20px; vertical-align:top;'>store</i><a href='" + "<?php echo base_url() . "merchant/" ?>" + data[product_seq].merchant_code + "'>&nbsp;" + data[product_seq].merchant_name + "</a> &nbsp;" +
                                    "<i class='fa fa-truck'></i>&nbsp;" + data[product_seq].exp_name +
                                    "</div>" +
                                    "<div class='col-lg-2 col-md-2 col-sm-2 text-left'><div class='row'>" +
                                    "<a href = '#' class = 'btn btn-danger btn-sm btn-flat' onClick =delete_product(" + product_seq + ")> <i class = 'fa fa-trash' > </i></a>" +
                                    "</div>" +
                                    "</div>" +
                                    "</div>" +
                                    "</td>" +
                                    "<td class='cart' id='sell_price_" + product_seq + "' sell_price ='" + data[product_seq].sell_price + "'><center>" + format(data[product_seq].sell_price) + "</center></td>" +
                                    "<td class='cart'>" +
                                    "<center><select id='qty' name='product_qty'  id ='product_qty_" + product_seq + "' onchange = 'dropdown_qty(" + product_seq + ", this.id)'>";
                            for (i = 1; i <= data[product_seq].max_buy; ++i) {
                                content = content + "<option>" + i + "</option>";
                            }
                            content = content + "</selected></center><td class='cart' id ='total_price_" + product_seq + "' total_price=" + data[product_seq].sell_price + "><center>" + format(data[product_seq].sell_price) + "</center></td></tr>";

                            var content_xs =
                                    "<tr id='row_" + product_seq + "_xs' >" +
                                    "<td class='cart'>" +
                                    "<a href='" + "<?php echo base_url(); ?>" + data[product_seq].product_name.replace(/\s+/g, '-').toLowerCase() + "-" + product_seq + "'>" +
                                    "<center><img src='" + "<?php echo get_image_location() . PRODUCT_UPLOAD_IMAGE; ?>" + data[product_seq].merchant_seq + "/" + "<?php echo S_IMAGE_UPLOAD; ?>" + data[product_seq].img + "'" +
                                    "alt='" + data[product_seq].img + "'" +
                                    "width=100px'></a></center></td><td class='cart'><strong>" + data[product_seq].product_name + "</strong><br><br>" +
                                    "<table border='0' class='table table-condensed'>" +
                                    "<tr>" +
                                    "<td> Berat </td>" +
                                    "<td> : </td>" +
                                    "<td>" + format(data[product_seq].product_weight) + " kg</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                    "<td> Warna </td>" +
                                    "<td > : </td>" +
                                    "<td >" + data[product_seq].variant_name + "</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                    "<td> Qty </td>" +
                                    "<td > : </td>" +
                                    "<td ><select name='product_qty'  id ='product_qty_" + product_seq + "_xs' onchange = 'dropdown_qty(" + product_seq + ", this.id)'>";

                            for (i = 1; i <= data[product_seq].max_buy; ++i) {
                                content_xs = content_xs + "<option>" + i + "</option>";
                            }
                            ;

                            content_xs = content_xs +
                                    "</select></td>" +
                                    "<tr>" +
                                    "<td> Harga </td>" +
                                    "<td> : </td>" +
                                    "<td>Rp. " + format(data[product_seq].sell_price) + "</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                    "<td> Total </td>" +
                                    "<td> : </td>" +
                                    "<td id='total_price_" + product_seq + "_xs'>Rp. " + format(data[product_seq].sell_price) + "</td>" +
                                    "</tr>" +
                                    "</table>" +
                                    "<div class='row'>" +
                                    "<div class='col-xs-9'><div class='row'>" +
                                    "<div class='col-xs-12'><i class = 'material-icons' style='font-size:20px; vertical-align:top;'>store</i><a href='" + "<?php echo base_url() . "merchant/" ?>" + data[product_seq].merchant_code + "'>&nbsp;" + data[product_seq].merchant_name + "</a> &nbsp;</div>" +
                                    "<div class='col-xs-12'><i class='fa fa-truck'></i>&nbsp;" + data[product_seq].exp_name + "</div>" +
                                    "</div></div>" +
                                    "<div class='col-xs-3'><div class='row'>" +
                                    "<a href = '#' class = 'btn btn-danger btn-sm btn-flat' onClick =delete_product(" + product_seq + ")> <i class = 'fa fa-trash' > </i></a>" +
                                    "</div>" +
                                    "</div>" +
                                    "</div>" +
                                    "</td>" +
                                    "</tr>";

                            $('#table_product').append(content);
                            $('#table_product_xs').append(content_xs);
                            calculate_total();
                            if (data[product_seq].count == '1') {
                                $('#no_product_xs').text('');
                                $('#no_product').text('');

                                var content_summary = "<div class='row'>" +
                                        "<div class='col-lg-12 col-md-12  text-right' ><div class='hidden-lg hidden-md'><br></div><h3><strong>Total Belanja : <font color='red'>Rp <span id='total_order'>" + format(data[product_seq].sell_price) + "</span></font></strong></h3>" +
                                        "</div>" +
                                        "</div>" +
                                        "<br>" +
                                        "<div class='row'>" +
                                        "<div class='col-lg-6 col-md-6 col-sm-6'><button class='btn btn-front btn-flat' data-dismiss='modal' aria-label='Close'>&nbsp;<span class='fa fa-shopping-cart'> </span>&nbsp;Lanjutkan Belanja</button></div>" +
                                        "<div class='col-lg-6 col-md-6 col-sm-6 text-right'><a href='" + "<?php echo base_url() . "member/checkout?step=1" ?>" + "' class='btn btn-front btn-flat' style='background-color: #0992a6;'>Lanjutkan Pembayaran &nbsp;<span class='fa fa-arrow-circle-o-right'> </span>&nbsp;</a></div>" +
                                        "</div>" +
                                        "</div>";

                                var content_summary_xs = "<div class='row'>" +
                                        "<div class='col-lg-12'><div class='well'><h3 style='text-align: center;'><b>Total Belanja  <br><font color='red'>Rp <span id='total_order_xs'>" + format(data[product_seq].sell_price) + "</span></font></b></h3></div></div>" +
                                        "</div>" +
                                        "<div class='row'>" +
                                        "<div class='col-lg-12'><button class='btn btn-front btn-flat btn-block' data-dismiss='modal' aria-label='Close'>&nbsp;<span class='fa fa-shopping-cart'> </span>&nbsp;Lanjutkan Belanja</button></div> <br>" +
                                        "<div class='col-lg-12'><a href='" + "<?php echo base_url() . "member/checkout?step=1" ?>" + "' class='btn btn-front btn-flat btn-block' style='background-color: #0992a6;'>Lanjutkan Pembayaran &nbsp;<span class='fa fa-arrow-circle-o-right'> </span>&nbsp;</a></div>" +
                                        "</div>" +
                                        "</div>";

                                $('#product_summary').append(content_summary);
                                $('#product_summary_xs').append(content_summary_xs);

                            }

                        }
                    }
                },
                error: function(request, error) {
                    alert(error_response(request.status));
                }
            });
<?php } ?>
    }
    function login_register() {
        $("#login_register").modal('show');
        $("#tablogin").addClass('active');
        $("#login").addClass('active');
    }
</script>
<style>
</style>    