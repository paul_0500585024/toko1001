<?php
require_once VIEW_BASE_HOME;
?>

<div class="hidden-xs hidden-lg " style="margin-top:55px;">&nbsp;</div>
<!--    for lg md-->
<div class="hidden-xs hidden-sm hidden-md" style="margin-top:25px;">&nbsp;</div>

<div class="container">
    <!--            TEMA 1-->
    <?php
    $logo = get_base_url() . IMG_NO_LOGO;
    $banner = get_base_url() . IMG_NO_BANNER;
    if (isset($data_sel[LIST_DATA])) {
	$folder = get_base_url() . "assets/img/merchant/logo/" . $data_sel[LIST_DATA][0]->seq . "/";
	if ($data_sel[LIST_DATA][0]->logo_img != "")
	    $logo = $folder . $data_sel[LIST_DATA][0]->logo_img;
	if ($data_sel[LIST_DATA][0]->banner_img != "")
	    $banner = $folder . $data_sel[LIST_DATA][0]->banner_img;
    }
    ?>

    <div class="fb-profile">
	<img align="left" class="fb-image-lg img-responsive hidden-xs" src="<?php echo $banner; ?>" alt="Banner Image" >
	<img align="left" class="fb-image-profile img-responsive hidden-xs thumbnail" src="<?php echo $logo; ?>" alt="Logo Image"/>
	<div class="fb-profile-text">
            
	    <p><h3 id="merchant_name"><?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->name) : ""); ?></h3></p>
	    <div>
		<hr id="garis-merchant">
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
		    <div class="row">
			<i class="fa fa-calendar" title="Tanggal Bergabung"></i>&nbsp;&nbsp; <?php echo (isset($data_sel[LIST_DATA]) ? cdate($data_sel[LIST_DATA][0]->created_date) : ""); ?>&nbsp; &nbsp; <br>
			<i class="fa fa-paper-plane" title="Dikirim dari"></i>&nbsp;&nbsp; <?php echo (isset($data_sel[LIST_DATA][1][0]) ? $data_sel[LIST_DATA][1][0]->province_name . ' - ' . $data_sel[LIST_DATA][1][0]->city_name . ' - ' . $data_sel[LIST_DATA][1][0]->name : ''); ?>
			<?php
			$city = '';
//			print_r($data_sel[LIST_DATA][2]);
			if (isset($data_sel[LIST_DATA][2])) {
			    foreach ($data_sel[LIST_DATA][2] as $each) {
				$city .= "<li>" . $each->name . "</li>";
				if ($each->name == "SEMUA KOTA") {
				    break;
				};
			    }
			    ?>
    			<button id="pengirimangratis" type="button" data-container="body"  class="btn btn-front btn-flat btn-sm pull-right" style="background-color: #0992A6;"
    				data-toggle="popover" title="Pengiriman Gratis"  data-trigger='click'
    				data-content=
    				"<ul>
				    <?php echo $city; ?>
    				</ul>
    				" data-placement="left" data-html="true">
    			    <i class="fa fa-truck"></i>&nbsp;Pengiriman Gratis
    			</button>

    			<script>
    			    $("#pengirimangratis").on("click", function () {
    				$(this).popover();
    			    });

    			</script>
			<?php } ?>
<!--                                        <span class="pull-right" style="margin-right:-170px;margin-top:-7px;">
Bagikan :
<a href="#" class="btn btn-facebook btn-flat "><i class="fa fa-facebook"></i></a>
<a href="#" class="btn btn-twitter btn-flat "><i class="fa fa-twitter"></i></a>
<a href="#" class="btn btn-google-plus btn-flat "><i class="fa fa-google-plus"></i></a>
</span>-->
		    </div>
		</div>
	    </div>

	</div>
    </div>
</div>
<br>

<div class="container">

    <ul class="nav nav-tabs" role="tablist">
	<li role="presentation" class="active"><a href="#produk" aria-controls="produk" role="tab" data-toggle="tab"><i class="fa fa-briefcase fa-lg"></i>&nbsp;Produk</a></li>
	<li role="presentation"><a href="#informasi" aria-controls="informasi" role="tab" data-toggle="tab"><i class="fa fa-info-circle fa-lg"></i>&nbsp;Informasi</a></li>
    </ul>
</div>
<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="produk">
	<br>
	<div style="margin-top:0px;" class="hidden-xs">
	    <?php echo display_product($product); ?>
	</div>
	<!--                    TABLE PRODUK FOR X-->
	<div class='hidden-lg hidden-md hidden-sm'>
	    <?php echo display_product($product); ?>
	</div>

	<div class="container"><?php echo $this->pagination->create_links(); ?></div>
    </div>

    <div role="tabpanel" class="tab-pane" id="informasi">

	<div class="container"><br>
	    <div class="well">
		<?php echo (isset($data_sel[LIST_DATA]) ? ($data_sel[LIST_DATA][0]->welcome_notes) : ""); ?>
	    </div>
	</div>
    </div>
</div>


