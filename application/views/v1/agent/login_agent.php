<?php
require_once VIEW_BASE_AGENT;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">
<html>
    <head>
        <title>Toko1001</title>
        <link rel="icon" href="<?php echo get_img_url(); ?>home/icon/pavicon.png"/>
        <link href="<?php echo get_css_url(); ?>bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_css_url(); ?>agent/toko1001_agent.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_css_url(); ?>font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="<?php echo get_css_url() . "/home/" . LIVEVIEW; ?>style.css"/>
        <script type="text/javascript" src="<?php echo get_js_url(); ?>jQuery-2.1.4.min.js" ></script>
        <script type="text/javascript" src="<?php echo get_js_url(); ?>jquery.validate.js" ></script>
        <style>
            .m-login{
                background: white;
                width: 350px;
                height: 365px;
                margin-top: 100px!important;
                position: relative;
                border-radius: 10px;
            }
            .cst-close{
                width: 30px;
                height: 30px;
                background: #663399;
                position: absolute;
                right: -40px;
                top: -40px;
                border-radius: 50%;
                text-align: center;
                font-size: 30px;
                padding: 0px;
                line-height: 15px;
                color: #f5f5f5;
                transition: all .5s;
            }

            .cst-close:hover{
                color: #663399!important;
                background: #f5f5f5;
            }


            .mdl-content{
                margin: 1em;
                padding: 1em;
            }
            .header{
                font-size: 1.5em;
                font-weight: bold;
                text-align: center;
            }
            .cst-transparent-btn{
                border: none!important;
                border-radius: 0px!important;
                box-shadow: none!important;
                background: transparent!important;
            }
            .cst-transparent-btn:active{
                border: none!important;
                outline: none;
            }
            .cst-transparent-btn:hover{
                border: none!important;
                outline: none;
            }
            .cst-transparent-btn:focus{
                border: none!important;
                outline: none;
            }
            .soc{
                padding: .1em;
                padding: .2em;
                color: white;
                width: 25px;
                height: 25px;
                line-height: 20px;
                border-radius: 2px !important
            }
            .no-border{
                border: none!important;
            }
            .add-top-margin{
                margin-top: 1em!important;
            }

            .mdl-lbl-global{
                background: #ccc!important;
            }
        </style>
    </head>
    <body class="" style="background-image: url('<?php echo get_img_url() ?>/agentMasterLoginBg.jpg');background-size:  100%;background-repeat: no-repeat;">
        <div class="container">
            <div class="login-box pull-right" style="border: 1px solid #ddd">
                <div class="login-box-body" style="border-radius: 16px">
                    <div class="form-group text-center">
                        <a href="<?php echo base_url() ?>"><img src='<?php echo get_img_url() ?>logo_biru.png'></img></a>
                    </div>
                    <div id ="err_msg">
                        <?php if ($data_err[ERROR] === true) { ?>
                            <p  class="login-box-msg text-red"><?php echo $data_err[ERROR_MESSAGE][0] ?> </p>
                        <?php } else { ?>
                        <?php } ?>
                    </div>
                    <form id="frmlogin" method = "post" action= "<?php echo get_base_url() ?>agent/login/sign_in<?php echo isset($data_auth[FORM_AUTH][FORM_URL]) ? $data_auth[FORM_AUTH][FORM_URL] : ""; ?>">
                        <div class="form-group has-feedback">
                            <input name="type" type="hidden" value="standard_login"/>
                            <input id="txtUserName" name="email"
                                   type="text" class="form-control" placeholder="Email"
                                   style="background-color: rgba(250, 255, 189, 0.8); color: #000"
                                   value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA]->user_id : "") ?>"/>
                            <span class="glyphicon glyphicon-envelope form-control-feedback" ></span>
                        </div>
                        <div class="form-group has-feedback">
                            <div class="form-group">
                                <input id="txtPassword" name="password"
                                       type="password" class="form-control" placeholder="Password"
                                       style="background-color: rgba(250, 255, 189, 0.8); color: #000"
                                       value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA]->password : "") ?>"/>
                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                            </div>
                            <div class="row add-top-margin">

                                <div class="col-xs-12 no-margin">
                                    <button type="submit" class="btn btn-info btn-block bg-blue">Login</button>
                                </div><br /><br /><br />
                            </div>
                            <div>
                                <div class="col-xs-7">
                                </div>
                                <div class="col-xs-5" style="text-align: right;">
                                    <a href="<?php echo base_url() ?>agent/forgot_password">Lupa Password </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $.validator.messages.required = "Data wajib diisi !";
            $.validator.messages.maxlength = "Harap diisi tidak lebih dari {0} karakter !";
            $.validator.messages.minlength = "Harap diisi dengan minimal {0} karakter !";
            $.validator.messages.email = "Harap diisi dengan alamat email dengan benar !";
        </script>
    </body>
</html>