<?php
require_once VIEW_BASE_HOME;
?>
<!--<div class="row margin-top-5">&nbsp;</div>-->

<div class="container margin-top-5">
    <?php if (isset($data_err) && $data_err[ERROR] === true) { ?>
        <div class="alert alert-error-home">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $data_err[ERROR_MESSAGE][0] ?>
        </div>
    <?php } elseif (isset($data_suc) && ($data_suc[SUCCESS] === true)) { ?>
        <div class="alert alert-succes-cst">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo (isset($data_suc[SUCCESS_MESSAGE][0]) ? $data_suc[SUCCESS_MESSAGE][0] : SAVE_DATA_SUCCESS); ?>
        </div>
    <?php } ?>
    <div class="col-xs-12 box-header-cart">

        <h5><b>Lupa Password Agent</b></h5>
        <div class="row margin-top-50"></div>
        <center><div id="success"></div></center>
        <div class="col-xs-6">
            <form class="form-horizontal" id="frmForgot" action="<?php echo get_base_url() . "agent/forgot_password"; ?>" method="post">
                <div class="form-group">
                    <label class="col-xs-2">Email</label>
                    <div class="col-xs-9">
                        <input type="text" class="form-control" id="email" placeholder="Email" name="email" validate="email[]">
                    </div>
                    <div class="col-xs-2">
                        <div id="usr_verify" class="verify"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-xs-2 ">Captcha</label>
                    <div class="col-xs-9">
                        <?php echo $image; ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-xs-2"></label>
                    <div class="col-xs-9">
                        <input type="text" class="form-control" name="security_code" placeholder="Masukkan captcha" validate="required[]">
                    </div>
                    <div class="col-xs-2">
                        <div id="captcha_verify" class="verify"></div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-offset-2">
                        <div class="col-xs-9">
                            <button type="submit" name="btnSave" value="true" class ='btn btn-success btn-flat  button-cart width-100'> Lupa Password</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--</div>-->
</div>
<!--</div>-->
<script>
    var isValidAgent = $('#frmForgot').validate({
        ignore: 'input[type=hidden]',
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.hasClass('select2')) {
                error.insertAfter(element.next('span'));
            } else {
                error.insertAfter(element);
            }
        }
    });

    $('form#frmForgot').submit(function() {
        var get_data_agent = $(this).serialize();
        if (isValidAgent.valid()) {
            $('input[type="text"]').each(function() {
                if ($(this).hasClass('auto') || $(this).hasClass('auto_int') || $(this).hasClass('auto_dec'))
                {
                    var v = $(this).autoNumeric('get');
                    $(this).autoNumeric('destroy');
                    $(this).val(v);
                }
            })
        }
    })
</script>

