<form id="frmAuth" onsubmit ="return get_id()" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>" >
    <div class = "box-tools pull-left">
        <button class = "btn btn-flat bg-orange" id ="refresh" data-toggle = "tooltip" title = "Cari" type='button'><i class = "fa fa-refresh"></i></button>
        <?php if (!$data_auth[FORM_AUTH][FORM_AUTH_ADD] === false) { ?>
            <button name ="<?php echo CONTROL_ADD_NAME ?>" class = "btn btn-flat bg-green" type ="submit" id ="add"  onclick="button = this.id" value ="test" data-toggle = "tooltip" title = "Tambah"><i class = "fa fa-plus"> </i></button>
        <?php } ?>
        <?php if (!$data_auth[FORM_AUTH][FORM_AUTH_EDIT] === false) { ?>
            <button  name ="<?php echo CONTROL_EDIT_NAME ?>" class = "btn btn-flat bg-blue" type ="submit" id ="edit" onclick="button = this.id"  value ="test" data-toggle = "tooltip" title = "Ubah" ><i class = "fa fa-pencil"></i></button>
        <?php } ?>
        <?php if (!$data_auth[FORM_AUTH][FORM_AUTH_VIEW] === false) { ?>
            <button  name ="<?php echo CONTROL_VIEW_NAME ?>" class = "btn btn-flat bg-yellow" type ="submit" id ="view" onclick="button = this.id"  value ="test" data-toggle = "tooltip" title = "Lihat" ><i class = "fa fa-search"></i></button>
        <?php } ?>
        <?php if (!$data_auth[FORM_AUTH][FORM_AUTH_DELETE] === false) { ?>
            <button class = "btn btn-flat bg-red" id ="delete" data-toggle = "tooltip" title = "Hapus" type='button'><i class = "fa fa-trash-o"></i></button>
        <?php } if (!$data_auth[FORM_AUTH][FORM_AUTH_APPROVE] === false) { ?>
            <button class = "btn btn-flat bg-blue" id ="approve" data-toggle = "tooltip" title = "Approve" type='button'><i class = "fa fa-check"></i></button>
            <button class = "btn btn-flat bg-orange" id ="reject" data-toggle = "tooltip" title = "Reject" type='button'><i class = "fa fa-thumbs-o-down"></i></button>
            <?php } ?>            
    </div>
</form>
<br/><br/>

