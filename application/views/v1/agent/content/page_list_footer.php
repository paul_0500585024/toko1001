<script type="text/javascript">
    var button;
    $(document).ready(function() {
        var url = $('#frmSearch').attr('url');
        var success_html = '<div class="callout callout-info" style="margin-bottom: 0!important;><h4><i class="fa fa-info"></i> Note:</h4>';
        var error_html = '<div class="callout callout-danger" style="margin-bottom: 0!important;> <h4><i class="fa fa-info"></i> Note:</h4>';
        var columns_detail = [];

        $('#tbl thead th').each(function() {
            var sName = $(this).attr("column");
            var bVisible = $(this).attr("visible");
            var url = $(this).attr("url");
            columns_detail.push({"sName": sName, "bVisible": bVisible, "mData": sName});

        });

        var table = $('#tbl').DataTable({
            "oLanguage": {
                    sProcessing: "<img src='<?php echo base_url().ASSET_IMG_HOME?>loading.gif' />"
            },
            "processing": true,
            "serverSide": true,
            "aoColumns": columns_detail,
            "ajax": {
                "url": url,
                "type": 'POST',
                "dataType": 'JSON',
                "data": buildAjaxData,
                "error": handleAjaxError
            }
        })
        .on('xhr.dt', function ( e, settings, response, xhr ){
            response = JSON.stringify(response);
            if(isSessionExpired(response)){
                response_object = json_decode(response);
                url = response_object.url;
                location.href = url;
            }
        });

        function handleAjaxError(request, textStatus, error) {
            if (textStatus === 'timeout') {
                alert('The server took too long to send the data.');
            }
            else {
		var response = error_response(request.status);
                $('#content-message').html(error_html + " " + response + "</div>");
                $('div#tbl_processing').css('visibility','hidden');
            }
        }

        function handleAjaxSuccess(response) {
            alert(response);
        }

        function buildAjaxData() {
            var settings = $("#tbl").dataTable().fnSettings();
            var search = [];
            search = $('#frmSearch').serializeArray();
            search.push(
            {name: "draw", value: settings.iDraw},
            {name: "start", value: settings._iDisplayStart},
            {name: "length", value: settings._iDisplayLength},
            {name: "column", value: columns_detail[settings.aaSorting[0][0]].sName},
            {name: "order", value: settings.aaSorting[0][1]},
            {name: "btnSearch", value: "true"}
            );
            return search;
        }

        $('#tbl tbody').on('click', 'tr', function() {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });

        $("#delete").click(function() {
            var key = $(".selected").attr('id');
            if (key == undefined) {
                alert("Select the item first !");
            } else {
                if (confirm("Are you sure want to delete this item?")) {
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: {key: key, btnSaveDel: "true"},
                        success: function(response) {
                            if(isSessionExpired(response)){
                                response_object = json_decode(response);
                                url = response_object.url;
                                location.href = url;
                            }else{                
                                table.row('.selected').remove().draw(false);
                                $('#content-message').html(success_html + " Data Berhasil di hapus ! </div>").hide().fadeIn(1000, function() {
                                    $('#content-message').append("");
                                }).delay(2000).fadeOut("fast");
                            }
                        },
                        error: function(request, error) {
                            $('#content-message').html(error_html + " " + error_response(request.status) + "</div>");
                        }
                    });
                }
            }
        });


        $("#approve").click(function() {
            var key = $(".selected").attr('id');
            if (key == undefined) {
                alert("Select the item first !");
            } else {
                if (confirm("Are you sure want to approve this item?")) {
                    $.ajax({
                        url: url,
                        type: "POST",
                        dataType: "json",
                        data: {key: key, btnApprove: "true"},
                        success: function(response) {
                            if(isSessionExpired(response)){
                                response_object = json_decode(response);
                                url = response_object.url;
                                location.href = url;
                            }else{                
                                if (response.error == true)
                                {
                                    $('#content-message').html(error_html + response.message + "</div>").hide().fadeIn(1000, function() {
                                        $('#content-message').append("");
                                    }).delay(2000).fadeOut("fast");
                                    table.ajax.reload();
                                }
                                else {
                                    $('#content-message').html(success_html + " Data Berhasil di approve ! </div>").hide().fadeIn(1000, function() {
                                        $('#content-message').append("");
                                    }).delay(2000).fadeOut("fast");
                                    table.ajax.reload();
                                }
                            }
                        },
                        error: function(request, error) {
                            $('#content-message').html(error_html + " " + error_response(request.status) + "</div>");
                        }
                    });
                }
            }
        });


        $("#reject").click(function() {
            var key = $(".selected").attr('id');
            if (key == undefined) {
                alert("Select the item first !");
            } else {
                if (confirm("Are you sure want to reject this item?")) {
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: {key: key, btnReject: "true"},
                        success: function(response) {
                            if(isSessionExpired(response)){
                                response_object = json_decode(response);
                                url = response_object.url;
                                location.href = url;
                            }else{                
                                $('#content-message').html(success_html + " Data Berhasil di reject ! </div>").hide().fadeIn(1000, function() {
                                    $('#content-message').append("");
                                }).delay(2000).fadeOut("fast");
                                table.ajax.reload();
                            }
                        },
                        error: function(request, error) {
                            $('#content-message').html(error_html + " " + request.ResponseText + "</div>");
                        }
                    });
                }
            }
        });

        $("#refresh").click(function() {
            table.ajax.reload();
        });

        $(document).on('click', '#search', function(e) {
            table.ajax.reload();
        });


        var isValid = $('#frmMain').validate({
            ignore: 'input[type=hidden]',
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else if (element.hasClass('select2')) {
                    error.insertAfter(element.next('span'));
                } else {
                    error.insertAfter(element);
                }
            }
        });

        $(document).on('submit', '#frmMain', function(e) {
            if (isValid.valid()) {
                $('input[type="text"]').each(function() {
                    if ($(this).hasClass('auto'))
                    {
                        var v = $(this).autoNumeric('get');
                        $(this).autoNumeric('destroy');
                        $(this).val(v);
                    }
                });
            }
        });

        $('input[type="text"], input[type="file"], input[type="password"] ').each(function() {
            var validate = $(this).attr('validate');
            switch (validate) {
                case  "required[]" :
                    $(this).rules('add', {
                        required: true});
                    break;
                case  "email[]" :
                    $(this).rules('add', {
                        required: true,
                        email: true});
                    break;
                case  "password[]" :
                    $(this).rules('add', {
                        required: true,
                        minlength: 8,
                        maxlength: 20});
                    break;
                case  "email-not-required[]" :
                    $(this).rules('add', {
                        email: true});
                    break;
            }
            ;
        });

        $('select').each(function() {
            if ($(this).attr('validate') == "required[]")
            {
                $(this).rules('add', {
                    required: true
                });
            }
        });

        $('.select2').on('change', function() {
            $(this).valid();
        });
    });

    $('.select2').select2();
    $('.auto').autoNumeric('init');

    function validate_form()
    {
        if (button == 'cancel')
        {
            if (confirm("Are you sure want to cancel?")) {
                $("#frmMain").validate().cancelSubmit = true;
                return true;
            }
            else {
                $("#frmMain").validate().cancelSubmit = true;
                button = "";
                return false;
            }
        }
        else
        {
            return true;
        }

    }

    function get_id()
    {
        if (button == 'add')
        {
            return true;
        }
        else
        {
            var key = $(".selected").attr('id');
            if (key === undefined) {
                alert("Select the item first !");
                return false;
            } else {
                $("<input>").attr("type", "hidden").attr('name', "key").attr('value', key).appendTo('#frmAuth');
                return true;
            }
        }
    }

</script>  
