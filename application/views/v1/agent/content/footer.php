<?php
require_once VIEW_BASE_AGENT;
?>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        Beta Version
    </div>
    <strong>Copyright &copy; 2015-2016 <a href="<?php echo base_url() ?>">TOKO1001</a>.</strong> All rights reserved.
</footer>
