<select id="drop_voucher_xs" validate="required[]" class="form-control select2" style='width : 100%' onChange="voucher_change(this.id);">
    <option nominal="0">--Tanpa Voucher--</option>
    <?php
    if (isset($voucher_name)) {
        foreach ($voucher_name as $each) {
            ?>
            <option value="<?php echo $each->seq; ?>" nominal="<?php echo $each->nominal; ?>"><?php echo $each->code . " - Rp. " . number_format($each->nominal); ?></option>
            <?php
        }
    }
    ?>
</select>
