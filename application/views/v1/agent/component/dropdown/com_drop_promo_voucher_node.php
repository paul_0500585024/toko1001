<script text="javascript">
    $('.select2').select2();
</script>

<select class="form-control select2" validate="required[]" style='width : 100%' name="node_cd" type="input">
    <option value="">-- Pilih --</option>
    <?php foreach ($node_name as $each) { ?>
        <option value="<?php echo $each->code; ?>" <?php echo (isset($data_sel[LIST_DATA]) && $each->code == $data_sel[LIST_DATA][0]->node_cd) ? "selected" : "" ?>><?php echo $each->name; ?></option>
    <?php } ?>
</select>