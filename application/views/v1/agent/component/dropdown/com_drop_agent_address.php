<select id ="drop_agent_address" class="form-control select2" style="width : 100%" name="agent_address_seq" onchange ="change_agent_address()">
    <option value=""></option>
    <?php
    if (isset($agent_info)) {
        foreach ($agent_info as $each) {
            ?>
            <option value="<?php echo $each->seq; ?>" <?php echo (isset($data_sel[LIST_DATA]) && $each->seq == $data_sel[LIST_DATA][0]->agent_address) ? "selected" : "" ?>><?php echo $each->alias; ?></option>
    <?php }
}; ?>
</select>