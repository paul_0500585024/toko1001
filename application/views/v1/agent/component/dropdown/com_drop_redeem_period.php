<select class="form-control select2" id ="drop_city" validate ="required[]" style='width : 100%' name="period_seq" type="input">
    <option value="">-- Pilih --</option>
    <?php
    if (isset($redeem_period)) {
        foreach ($redeem_period as $each) {
            ?>
            <option value="<?php echo $each->period_seq; ?>" <?php echo (isset($data_sel[LIST_DATA]) && $each->period_seq == $data_sel[LIST_DATA][0]->period_seq) ? "selected" : "" ?>><?php echo $each->period_date; ?></option>
            <?php
        }
    }
    ?>
</select>

