<?php

function drop_down($id, $content = "", $value = "", $additional = "") {
    $return = '';
    $return = '<select class="form-control select2" style="width : 100%" name="' . $id . '" id="' . $id . '" ' . $additional . '>'
	    . '<option value="">-- Pilih --</option>';
    if ($content != '') {
	foreach ($content as $each) {
	    $return.='<option value="' . $each->seq . '"' . (($each->seq == $value) ? " selected" : "") . '>' . $each->name . '</option>';
	}
    }
    $return.='</select>';
    return $return;
}
