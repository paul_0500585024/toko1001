<select id="drop_provider_service_nominal" class="form-control select2" style='width : 100%' validate="required[]" name="provider_service_nominal_seq" >
    <option value="">-- Pilih --</option>
    <?php
    if (isset($provider_service_nominal_name)) {
        foreach ($provider_service_nominal__name as $each) {
            ?>
            <option value="<?php echo $each->seq; ?>" <?php echo isset($provider) && $provider->provider_service_nominal_seq === $each->seq ? "selected" : "" ?>><?php echo $each->name; ?></option>
            <?php
        }
    }
    ?>
</select>
