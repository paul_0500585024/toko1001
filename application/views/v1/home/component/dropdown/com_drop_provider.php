<select id="drop_provider" class="form-control select2" style='width : 100%' validate="required[]" name="provider_seq" validate="required[]" >
    <option value="">-- Pilih --</option>
    <?php
    if (isset($provider_name)) {
        foreach ($provider_name as $each) {
            ?>
            <option value="<?php echo $each->seq; ?>" <?php echo isset($provider) && $provider->provider_seq === $each->seq ? "selected" : "" ?>><?php echo $each->name; ?></option>
            <?php
        }
    }
    ?>
</select>
