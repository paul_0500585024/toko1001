<?php
require_once VIEW_BASE_HOME;
?>

<html>
    <head>
        <title>title</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
        <style>
            *{font-size: 12px}
            .well{
                border-radius: 0px
            }
            .font-color-red{
                color: red
            }
            .box-header-cart{
                border-top: 2px solid #CCC;
                background: #FCFCFC;
            }
            .customer-field{
                height: 70px;
                padding: 0px 15px; 
            }
            .lh34{
                line-height: 34px
            }
            .odd{
                background: #f5f5f5;
            }
            .simulation-field{
                height: 45px;
                padding: 5px;
                line-height: 35px
            }
        </style>
    </head>
    <body>

        <div class="container">
            <div class="well">
                <h3>Detail Order</h3>
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <td>Nomor Order</td>
                            <td>:</td>
                            <td><?php echo $data_sel[LIST_DATA][0][0]->order_no ?></td>
                        </tr>


                        <tr>
                            <td>Tanggal</td>
                            <td>:</td>
                            <td><?php echo date_format(date_create($data_sel[LIST_DATA][0][0]->order_date), "d-M-Y"); ?></td>
                        </tr>

                        <tr>
                            <td>Status</td>
                            <td>:</td>
                            <td>
                                <?php
                                $data = json_decode(STATUS_PAYMENT, true);
                                echo $data[$data_sel[LIST_DATA][0][0]->payment_status]
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Pembayaran menggunakan</td>
                            <td>:</td>
                            <td> <?php echo $data_sel[LIST_DATA][0][0]->payment_name ?></td>
                        </tr>

                    </tbody>
                </table>
            </div>



            <div class="well">
                <h3>Penerima</h3>
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <td>Nama</td>
                            <td>:</td>
                            <td><?php echo $data_sel[LIST_DATA][0][0]->receiver_name; ?></td>
                        </tr>


                        <tr>
                            <td>Alamat</td>
                            <td>:</td>
                            <td><?php echo $data_sel[LIST_DATA][0][0]->receiver_address . "-" . $data_sel[LIST_DATA][0][0]->city_name . " / " . $data_sel[LIST_DATA][0][0]->district_name ?></td>
                        </tr>

                        <tr>
                            <td>provinsi</td>
                            <td>:</td>
                            <td><div><?php echo $data_sel[LIST_DATA][0][0]->province_name; ?></div></td>
                        </tr>

                        <tr>
                            <td>Kode Pos</td>
                            <td>:</td>
                            <td><?php echo $data_sel[LIST_DATA][0][0]->receiver_zip_code; ?></td>
                        </tr>

                    </tbody>
                </table>
            </div>


            <?php
            if ($data_sel[LIST_DATA][0][0]->pg_method_seq == PAYMENT_SEQ_ADIRA || $data_sel[LIST_DATA][0][0]->pg_method_seq == PAYMENT_SEQ_ADIRA_DEPOSIT) {
                echo $table_simulation . '<br><br>';
                $active_deposit = "";
                $active_bank = "";


                echo $customer_table . '<br><br>';
            }
            ?>

            <div class="col-xs-12 box-body-cart well">
                <h4>Produk yang dibeli</h4>
                <?php
                $total = 0;
                $order_history = '';
                $iterate = 0;
                foreach ($data_sel[LIST_DATA][1] as $merchant) {
                    $iterate++;
                    ?>
                    <div class="row reset-margin merchant-header">
                        <div class="col-xs-4">
                            <div class="col-xs-12">
                                <div class="row">
                                    <h5><b><?php echo $merchant->merchant_name; ?></b></h5>
                                </div>

                            </div>
                        </div>
                        <div class="col-xs-6">
                            Pessan member : <?php echo $merchant->member_notes; ?><hr>
                        </div>
                        <div class="col-xs-2">
                            <div class="text-align-right">
                                <div><?php echo "<b>(" . $merchant->total_product . ")</b> Produk"; ?></div>
                                <div>
                                    <div>Shipping :  <?php echo $merchant->expedition_name; ?></div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <?php
                    $total_commision_agent = 0;
                    foreach ($data_sel[LIST_DATA][2] as $prod) {
                        if ($prod->product_status == PRODUCT_READY_STATUS_CODE && is_agent())
                            $total_commision_agent = ($total_commision_agent + ($prod->qty * $prod->commission_fee_percent * $prod->sell_price / 100));
                        if ($merchant->merchant_seq == $prod->merchant_seq) {
                            ?>
                            <div class="row reset-margin un-style">
                                <div class="col-xs-2">
                                    <center><img src="<?php echo PRODUCT_UPLOAD_IMAGE . $prod->merchant_seq . "/" . S_IMAGE_UPLOAD . $prod->img; ?>" alt="<?php echo $prod->img; ?>" width = "100px"></center>
                                </div>
                                <div class="col-xs-6">
                                    <div class="row">
                                        <div class="col-xs-12 cart-font-color-blue no-pad"><?php echo $prod->display_name ?></div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-xs-12 no-pad">
                                            <div class="col-xs-6 no-pad">
                                                <div class="col-xs-3 pad-auto">
                                                    Warna
                                                </div>
                                                <div class="col-xs-1 pad-auto">
                                                    &nbsp;:&nbsp;
                                                </div>
                                                <div class="col-xs-6 no-pad">
                                                    <?php echo $prod->variant_name; ?>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 no-pad">
                                                <div class="col-xs-3 pad-auto">
                                                    No Resi
                                                </div>
                                                <div class="col-xs-1 pad-auto">
                                                    &nbsp;:&nbsp;
                                                </div>
                                                <div class="col-xs-7 no-pad">
                                                    <?php echo $prod->product_status == PRODUCT_CANCEL_BY_MERCHANT_STATUS_CODE ? "" : $merchant->awb_no; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12 no-pad">
                                            <div class="col-xs-6 no-pad">
                                                <div class="col-xs-3 pad-auto">
                                                    Berat
                                                </div>
                                                <div class="col-xs-1 pad-auto">
                                                    &nbsp;:&nbsp;
                                                </div>
                                                <div class="col-xs-6 no-pad"><?php echo $prod->weight_kg ?> Kg</div>
                                            </div>
                                            <div class="col-xs-6 no-pad">
                                                <div class="col-xs-3 pad-auto">
                                                    Status Pengiriman
                                                </div>
                                                <div class="col-xs-2 pad-auto">&nbsp;:&nbsp;</div>
                                                <div class="col-xs-7 no-pad">
                                                    <?php
                                                    $data = json_decode(STATUS_ORDER, true);
                                                    $data_prod = json_decode(STATUS_XR, true);
                                                    echo $prod->product_status == PRODUCT_CANCEL_BY_MERCHANT_STATUS_CODE ? $data_prod[$prod->product_status] : $data[$merchant->order_status];
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 pad-auto">
                                            Biaya pengiriman : <b><?php echo $prod->ship_price_charged == 0 ? "<span class='text-green'>Free</span>" : "Rp. " . number_format(ceil($prod->weight_kg * $prod->qty) * $prod->ship_price_charged); ?></b>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4 cart-font-color-gray">
                                    <div class="row"><div class="col-xs-12">&nbsp;</div></div>
                                    <div class="row"><div class="col-xs-12">&nbsp;</div></div>

                                    <div class="row">
                                        <div class="col-xs-4 no-pad text-align-right">
                                            Rp. <?php echo number_format($prod->sell_price); ?>
                                        </div>
                                        <div class="col-xs-2">
                                            <center><?php echo number_format($prod->qty); ?></center>
                                        </div>
                                        <div class="col-xs-6 no-pad">
                                            Rp.  <?php
                                            echo number_format($prod->qty * $prod->sell_price + ceil($prod->weight_kg * $prod->qty) * $prod->ship_price_charged);
                                            $total = $total + $prod->qty * $prod->sell_price + ceil($prod->weight_kg * $prod->qty) * $prod->ship_price_charged;
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                if (isset($_COOKIE[VIEW_MODE]) AND $_COOKIE[VIEW_MODE] != '' and isset($_SESSION[SESSION_AGENT_CSRF_TOKEN])) {
                                    if ($prod->product_status == PRODUCT_READY_STATUS_CODE) {
                                        if (isset($prod->commission_fee_percent)) {
                                            ?>
                                            <div class="row"><div class="col-xs-12">&nbsp;</div></div>
                                            <div class="row">
                                                <div class="col-xs-1">Komisi:</div>
                                                <div class="col-xs-3 no-pad text-align-right">Rp. <?php echo number_format($prod->commission_fee_percent * $prod->sell_price / 100); ?></div>
                                                <div class="col-xs-2"></div>
                                                <div class="col-xs-6 no-pad">Rp. <?php echo number_format($prod->qty * $prod->commission_fee_percent * $prod->sell_price / 100); ?></div>
                                            </div>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </div>
                            <hr>
                            <?php
                        }
                    }
                    ?>
                    <?php
                    $order_history = '';
                }
                ?>
            </div>
        </div>

        <div class="container">
            <div class="well">
                <div class="col-xs-6">
                    Voucher Member / Kupon - Rp. <?php echo number_format($data_sel[LIST_DATA][0][0]->nominal); ?>
                </div>
                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-8">
                            <div style="text-align: right">Total Belanja : </div>
                        </div>
                        <div class="col-xs-4 no-pad">
                            <b>Rp. <?php echo number_format($total); ?></b>
                        </div>
                    </div>
                </div>

                <div class="col-xs-6">
                    Kode Voucher :<?php echo $data_sel[LIST_DATA][0][0]->voucher_code; ?>
                </div>
                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-8">
                            <div style="text-align: right">Total Pembayaran : </div>
                        </div>
                        <div class="col-xs-4 no-pad">
                            <b class="font-color-red">Rp. <?php echo number_format($data_sel[LIST_DATA][0][0]->total_payment); ?></b>
                        </div>
                    </div>
                    <?php if (isset($data_sel[LIST_DATA][0][0]->promo_credit_seq) && $data_sel[LIST_DATA][0][0]->promo_credit_seq > 0) { ?>
                        <div class="row">
                            <div class="col-xs-8">
                                <div style="text-align: right">Bank : </div>
                            </div>
                            <div class="col-xs-4 no-pad">
                                <b><?php echo $data_credit_info[0]->bank_name; ?></b>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-8">
                                <div style="text-align: right">Periode cicilan : </div>
                            </div>
                            <div class="col-xs-4 no-pad">
                                <b><?php echo number_format($data_credit_info[0]->credit_month); ?> bulan</b>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-8">
                                <div style="text-align: right">Pembayaran bulanan : </div>
                            </div>
                            <div class="col-xs-4 no-pad">
                                <b>Rp. <?php echo number_format($data_sel[LIST_DATA][0][0]->total_payment / $data_credit_info[0]->credit_month); ?></b>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                if (isset($_COOKIE[VIEW_MODE]) AND $_COOKIE[VIEW_MODE] != '' and isset($_SESSION[SESSION_AGENT_CSRF_TOKEN])) {
                    ?>
                    <div class="row"><br/>
                        <div class="col-xs-8 commission-price">
                            <div style="text-align: right">Total Komisi : </div>
                        </div>
                        <div class="col-xs-4 no-pad">
                            <b class="commission-price">Rp. <?php echo number_format($total_commision_agent); ?></b>
                        </div>
                    </div>
                <?php } ?>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>


        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <script>
            $(document).ready(function () {
                var tempTitle = document.title;
                document.title = "Toko1001-<?php echo $data_sel[LIST_DATA][0][0]->order_no ?>";
                window.print();
                document.title = tempTitle;
            });
        </script>

    </body>
</html>
