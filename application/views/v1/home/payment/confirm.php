<?php
require_once VIEW_BASE_HOME;

function timeline_status($status = '', $status_date = '') {
    $ret = '';
    if ($status_date != '' && $status_date != '0000-00-00 00:00:00' && $status_date != '0000-00-00' && $status_date != null) {
        $ret = '<tr><td>' . cdate($status_date) . '</td><td>' . get_display_status($status, STATUS_HISTORY) . '</td></tr>';
    }
    return $ret;
}
?>
<style>
    .payment-height{
        height: 80px;
        text-align: center;
        line-height: 50px;
        border-radius: 4px;
        background-color: #00c0ef !important;
        color: #fff;
    }z  
    .b-radius-5{
        border-radius: 5px;
    }
</style>

<div id="payment_success" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content b-radius-5">
            <div class="modal-body payment-height">
                <p>Terima kasih Anda Telah melakukan pembayaran</p>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <br>
    <div class="row">
        <div class="container">
            <?php
            if (isset($data_err) && $data_err[ERROR] === true) {
                ?>
                <div class="alert alert-error-home">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $data_err[ERROR_MESSAGE][0] ?>
                </div>
            <?php } ?>
            <?php if (isset($data_suc[SUCCESS_MESSAGE][0]) && $data_err[ERROR] === false) { ?>
                <div class="alert alert-succes-cst">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo (isset($data_suc[SUCCESS_MESSAGE][0]) ? $data_suc[SUCCESS_MESSAGE][0] : SAVE_DATA_SUCCESS); ?>
                </div>
                <br><br>
            <?php } ?>

        </div>
    </div>
    <?php if (strlen($data_ord[0][0]->order_no) > 15) { ?>
        <div class="row reset-margin">
            <div class="col-xs-12 box-header-cart">
                <div><h4>Detail Order</h4></div>
                <div class="margin-left-15px">
                    <div>No. Order : <?php echo $data_ord[0][0]->order_no ?></div>
                    <div>Tanggal : <?php echo date_format(date_create($data_ord[0][0]->order_date), "d-M-Y"); ?></div>
                    <div>Status : <?php
                        $data = json_decode(STATUS_PAYMENT, true);
                        $note = "";
                        echo $data[$data_ord[0][0]->payment_status] . $note;
                        ?>
                    </div>
                    <div><br>Pembayaran menggunakan : <?php echo $data_ord[0][0]->payment_name ?></div>
                </div>
            </div>
        </div>
        <br>
        <div class="row reset-margin">
            <div class="col-xs-12 box-header-cart">
                <div><h4>Penerima</h4></div>
                <div class="margin-left-15px">
                    <div><?php echo $data_ord[0][0]->receiver_name; ?></div>
                    <div><?php echo $data_ord[0][0]->receiver_address . "-" . $data_ord[0][0]->city_name . " / " . $data_ord[0][0]->district_name . "<br>" . $data_ord[0][0]->province_name ?></div>
                    <div><?php echo $data_ord[0][0]->receiver_zip_code; ?></div>
                    <div><?php echo $data_ord[0][0]->receiver_phone_no; ?></div>
                </div>
            </div>
        </div>
        <br>
        <div class="row reset-margin">
            <div class="col-xs-12 box-header-cart">
                <div class="row">
                    <div class="col-xs-12 box-body-cart">
                        <h4>Produk yang dibeli</h4>
                        <?php
                        $total = 0;
                        $order_history = '';
                        $iterate = 0;
                        $totalPriceExp = 0;
                        $shippingTotal = 0;
                        $expPriceKilos = 0;
                        $weightMerchantExp = 0;




                        foreach ($data_ord[1] as $merchant) {
                            (float) $totalProduct_kg = 0;
                            $iterate++;
                            ?>
                            <div class="row reset-margin merchant-header">
                                <div class="col-xs-4">
                                    <div class="col-xs-12">
                                        <div class="row">
                                            <a href = "<?php echo base_url() . "merchant/" . $merchant->merchant_code; ?>" ><h5><b><?php echo $merchant->merchant_name; ?></b></h5></a>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-xs-6"><input  type = "text"  class = "form-control input-sm" value="<?php echo $merchant->member_notes; ?>" readonly="" maxlength="100"></div>
                                <div class="col-xs-2">
                                    <div class="text-align-right">
                                        <div><?php echo "<b>(" . $merchant->total_product . ")</b> Produk"; ?></div>
                                        <div>
                                            <div>Shipping :  <?php echo $merchant->expedition_name; ?></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <?php
                            $total_commision_agent = 0;
                            foreach ($data_ord[2] as $prod) {
                                $expPricekilos = $prod->ship_price_charged;
                                if ($prod->product_status == PRODUCT_READY_STATUS_CODE && is_agent())
                                    $total_commision_agent = ($total_commision_agent + ($prod->qty * $prod->commission_fee_percent * $prod->sell_price / 100));
                                if ($merchant->merchant_seq == $prod->merchant_seq) {
                                    ?>
                                    <div class="row reset-margin un-style">
                                        <div class="col-xs-2">
                                            <a href ="<?php echo base_url() . url_title($prod->display_name . " " . $prod->product_seq); ?>"><center><img src="<?php echo PRODUCT_UPLOAD_IMAGE . $prod->merchant_seq . "/" . S_IMAGE_UPLOAD . $prod->img; ?>" alt="<?php echo $prod->img; ?>" width = "100px"></center></a>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="row">
                                                <div class="col-xs-12 cart-font-color-blue no-pad"><?php echo $prod->display_name ?></div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-xs-12 no-pad">

                                                    <?php if (isset($prod->value_seq) && $prod->value_seq != DEFAULT_VALUE_VARIANT_SEQ) { ?>
                                                        <div class="col-xs-6 no-pad">
                                                            <div class="col-xs-3 pad-auto">
                                                                Warna
                                                            </div>
                                                            <div class="col-xs-1 pad-auto">
                                                                &nbsp;:&nbsp;
                                                            </div>
                                                            <div class="col-xs-6 no-pad">
                                                                <?php echo $prod->variant_name; ?>
                                                            </div>
                                                        </div>
                                                    <?php } ?>

                                                    <div class="col-xs-6 no-pad">
                                                        <div class="col-xs-3 pad-auto">
                                                            No Resi
                                                        </div>
                                                        <div class="col-xs-1 pad-auto">
                                                            &nbsp;:&nbsp;
                                                        </div>
                                                        <div class="col-xs-7 no-pad">
                                                            <?php echo $prod->product_status == PRODUCT_CANCEL_BY_MERCHANT_STATUS_CODE ? "" : $merchant->awb_no; ?>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 no-pad">
                                                    <div class="col-xs-6 no-pad">
                                                        <div class="col-xs-3 pad-auto">
                                                            Berat
                                                        </div>
                                                        <div class="col-xs-1 pad-auto">
                                                            &nbsp;:&nbsp;
                                                        </div>
                                                        <div class="col-xs-6 no-pad"><?php echo $prod->weight_kg ?> Kg</div>
                                                    </div>
                                                    <div class="col-xs-6 no-pad">
                                                        <div class="col-xs-3 pad-auto">
                                                            Status Pengiriman
                                                        </div>
                                                        <div class="col-xs-2 pad-auto">&nbsp;:&nbsp;</div>
                                                        <div class="col-xs-7 no-pad">
                                                            <?php
                                                            $data = json_decode(STATUS_ORDER, true);
                                                            $data_prod = json_decode(STATUS_XR, true);
                                                            echo $prod->product_status == PRODUCT_CANCEL_BY_MERCHANT_STATUS_CODE ? $data_prod[$prod->product_status] : $data[$merchant->order_status];
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-4 cart-font-color-gray">
                                            <div class="row"><div class="col-xs-12">&nbsp;</div></div>
                                            <div class="row"><div class="col-xs-12">&nbsp;</div></div>

                                            <div class="row">
                                                <div class="col-xs-4 no-pad text-align-right">
                                                    Rp. <?php echo number_format($prod->sell_price); ?>
                                                </div>
                                                <div class="col-xs-2">
                                                    <center><input class="input-style" readonly value="<?php echo number_format($prod->qty); ?>"/></center>
                                                </div>
                                                <div class="col-xs-6 no-pad">
                                                    Rp.  <?php
                                                    echo number_format($prod->qty * $prod->sell_price);
                                                    $total = $total + $prod->qty * $prod->sell_price;
                                                    ?>
                                                </div>
                                            </div>
                                            <?php
                                            if (isset($_COOKIE[VIEW_MODE]) AND $_COOKIE[VIEW_MODE] != '' and isset($_SESSION[SESSION_AGENT_CSRF_TOKEN])) {
                                                if ($prod->product_status == PRODUCT_READY_STATUS_CODE) {
                                                    if (isset($prod->commission_fee_percent)) {
                                                        ?>
                                                        <div class="row"><div class="col-xs-12">&nbsp;</div></div>
                                                        <div class="row">
                                                            <div class="col-xs-1">Komisi:</div>
                                                            <div class="col-xs-3 no-pad text-align-right">Rp. <?php echo number_format($prod->commission_fee_percent * $prod->sell_price / 100); ?></div>
                                                            <div class="col-xs-2"></div>
                                                            <div class="col-xs-6 no-pad">Rp. <?php echo number_format($prod->qty * $prod->commission_fee_percent * $prod->sell_price / 100); ?></div>
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <?php
                                    (int) $totalProduct_kg = $totalProduct_kg + ($prod->weight_kg * $prod->qty);
                                }
                            }

                            $order_history.=timeline_status(ORDER_NEW_ORDER_STATUS_CODE, $data_ord[0][0]->order_date);
                            $auth_date_adira = '';
                            if ($data_ord[0][0]->pg_method_seq == PAYMENT_SEQ_ADIRA) {
                                if ($data_ord[0][0]->status_order == REJECT_STATUS_CODE) {
                                    $auth_date_adira = '<tr><td>' . cdate($data_ord[0][0]->auth_date) . '</td><td>' . cstdes("R", STATUS_LOAN) . '</td></tr>';
                                } elseif ($data_ord[0][0]->status_order != NEW_STATUS_CODE) {
                                    $auth_date_adira = '<tr><td>' . (strtotime($data_ord[0][0]->auth_date) != null ? cdate($data_ord[0][0]->auth_date) : "") . '</td><td>' . cstdes("A", STATUS_LOAN) . '</td></tr>';
                                }
                            }
                            $order_history.=$auth_date_adira;
                            if ($data_ord[0][0]->payment_status == PAYMENT_CONFIRM_STATUS_CODE) {
                                $order_history.=timeline_status($data_ord[0][0]->payment_status, $data_ord[0][0]->conf_pay_date);
                            }
                            if ($data_ord[0][0]->payment_status == PAYMENT_PAID_STATUS_CODE) {
                                $order_history.=timeline_status(PAYMENT_CONFIRM_STATUS_CODE, $data_ord[0][0]->conf_pay_date);
                                $order_history.=timeline_status(PAYMENT_PAID_STATUS_CODE, $data_ord[0][0]->paid_date);
                                if ($merchant->order_status != ORDER_CANCEL_BY_MERCHANT_STATUS_CODE && $merchant->order_status != ORDER_CANCEL_BY_SYSTEM_STATUS_CODE) {
                                    $order_history.=timeline_status(ORDER_READY_TO_SHIP_STATUS_CODE, $merchant->print_date);
                                    $order_history.=timeline_status(ORDER_SHIPPING_STATUS_CODE, $merchant->ship_date);
                                    $order_history.=timeline_status(ORDER_DELIVERED_STATUS_CODE, $merchant->received_date);
                                    if ($merchant->order_status == ORDER_DELIVERED_STATUS_CODE) {
                                        if (strtotime("+" . FINISH_DATE . " days", strtotime($merchant->received_date)) < strtotime(date("Y-m-d H:i:s"))) {
                                            $order_history.=timeline_status("FINISH", date("Y-m-d", strtotime("+" . FINISH_DATE . " days", strtotime($merchant->received_date))));
                                        }
                                    }
                                } else {
                                    $order_history.=timeline_status("XM", $merchant->modified_date);
                                }
                            } else {
                                switch ($data_ord[0][0]->payment_status) {
                                    case ORDER_CANCEL_BY_MERCHANT_STATUS_CODE:
                                        $order_history.=timeline_status($data_ord[0][0]->payment_status, $data_ord[0][0]->paid_date);
                                        break;
                                    case ORDER_FAIL_STATUS_CODE:
                                        $order_history.=timeline_status($data_ord[0][0]->payment_status, $data_ord[0][0]->paid_date);
                                        break;
                                    case "W":
                                        break;
                                }
                            }
                            ?>
                            <div class="row reset-margin un-style"><a href="javascript:trackers(<?php echo $iterate;
                            ?>)">Riwayat Status</a><br />
                                <div class="col-xs-1"></div>
                                <div class="col-xs-8">
                                    <table id="timeline<?php echo $iterate; ?>" style="display:none;background-color: #fff !important" class="table">
                                        <thead><tr><th>Tanggal</th><th>Status</th></tr></thead>
                                        <?php echo $order_history; ?>
                                    </table>
                                </div>
                            </div>
                            <?php
                            $order_history = '';
                            ?>
                            <?php //echo (ceil($totalProduct_kg) * $data_ord[2][0]->ship_price_charged ) ?>
                            <div class="well expedisi-well">
                                <?php if ($data_ord[2][0]->ship_price_charged !== '0') { ?>
                                    <p class="text-bold text-color-cart">Biaya Pengiriman :<span class="text-bold text-color-cart">
                                            <span style="color: #007dc6"><?php echo $merchant->expedition_name ?></span>
                                            (<span class="expedisi-label-kg"> <?php echo ceil($totalProduct_kg) . ' X ' . number_format($data_ord[2][0]->ship_price_charged) ?>  </span>)
                                            <?php echo RP . ' ' . number_format(ceil($totalProduct_kg) * $data_ord[2][0]->ship_price_charged) ?>
                                        </span>
                                    </p>
                                <?php } else { ?>
                                    <span class="pull-left"><p class="text-bold text-color-cart">Biaya Pengiriman : <span style="color: #007dc6"><?php echo $merchant->expedition_name; ?></span>&nbsp;<span class="text-green">Free</span></p></span>
                                <?php } ?>
                                <span class="pull-right"><p class="pull-right"><span class="text-bold" style="font-size:1.1em"><?php echo RP . number_format($merchant->total_merchant + $merchant->total_ship_charged) ?></span></p></span>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        Voucher Member / Kupon - Rp. <?php echo number_format($data_ord[0][0]->nominal); ?>
                    </div>
                    <div class="col-xs-6">
                        <div class="row">
                            <div class="col-xs-8">
                                <div class="pull-right" style="padding-right: 19px;font-size: 1.3em">Total Belanja : </div>
                            </div>
                            <div class="col-xs-4">
                                <b class="pull-right" style="padding-right: 19px;font-size: 1.3em">Rp. <?php echo number_format($data_ord[0][0]->total_order); ?></b>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        Kode Voucher :<?php echo $data_ord[0][0]->voucher_code; ?>
                    </div>
                    <div class="col-xs-6">
                        <div class="row">
                            <div class="col-xs-8">
                                <div class="pull-right" style="padding-right: 19px;font-size: 1.3em">Total Pembayaran : </div>
                            </div>
                            <div class="col-xs-4">
                                <b class="pull-right font-color-red" style="padding-right: 19px;font-size: 1.3em">Rp. <?php echo number_format($data_ord[0][0]->total_payment); ?></b>
                            </div>
                        </div>
                        <?php if (isset($data_ord[0][0]->promo_credit_seq) && $data_ord[0][0]->promo_credit_seq > 0) { ?>
                            <div class="row">
                                <div class="col-xs-8">
                                    <div style="text-align: right">Bank : </div>
                                </div>
                                <div class="col-xs-4 no-pad">
                                    <b><?php echo $data_credit_info[0]->bank_name; ?></b>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-8">
                                    <div style="text-align: right">Periode cicilan : </div>
                                </div>
                                <div class="col-xs-4 no-pad">
                                    <b><?php echo number_format($data_credit_info[0]->credit_month); ?> bulan</b>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-8">
                                    <div style="text-align: right">Pembayaran bulanan : </div>
                                </div>
                                <div class="col-xs-4 no-pad">
                                    <b>Rp. <?php echo number_format($data_ord[0][0]->total_payment / $data_credit_info[0]->credit_month); ?></b>
                                </div>
                            </div>
                            <?php
                        }
                        if (isset($_COOKIE[VIEW_MODE]) AND $_COOKIE[VIEW_MODE] != '' and isset($_SESSION[SESSION_AGENT_CSRF_TOKEN])) {
                            ?>
                            <div class="row"><br/>
                                <div class="col-xs-8 commission-price">
                                    <div style="text-align: right">Total Komisi : </div>
                                </div>
                                <div class="col-xs-4 no-pad">
                                    <b class="commission-price">Rp. <?php echo number_format($total_commision_agent); ?></b>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <?php if ($data_ord[0][0]->payment_code == PAYMENT_TYPE_BANK && $data_ord[0][0]->payment_status == PAYMENT_WAIT_CONFIRM_STATUS_CODE) { ?>
            <form id ="frmMain" action ="<?php echo get_base_url() . "member/payment/" . $data_ord[0][0]->order_no; ?>" method="post" enctype ="multipart/form-data">
                <div class="row reset-margin">
                    <div class="col-xs-12  box-header-cart">
                        <div><h4>Konfirmasi Pembayaran</h4></div>
                        <div class="col-xs-6">
                            <div class="margin-left-15px">
                                <div class="margin-buttom-10px">Metode Pembayaran : <?php echo $data_ord[0][0]->payment_name; ?></div>
                                <div class="form-group clean-margin margin-buttom-10px">
                                    <label>Jenis Setoran</label>
                                    <div class="radio clean-margin margin-left-15px">
                                        <label>
                                            <input type="radio" name="payment_type" value="<?php echo PAYMENT_TYPE_CASH; ?>" <?php echo isset($data_sel[LIST_DATA][0]->payment_type) && $data_sel[LIST_DATA][0]->payment_type == PAYMENT_TYPE_CASH ? "checked" : ""; ?>>
                                            Setoran Tunai
                                        </label>
                                        &nbsp;
                                        <label>
                                            <input type="radio" name="payment_type" value="<?php echo PAYMENT_TYPE_TRANSFER; ?>" <?php echo isset($data_sel[LIST_DATA][0]->payment_type) && $data_sel[LIST_DATA][0]->payment_type == PAYMENT_TYPE_TRANSFER || !isset($data_sel[LIST_DATA][0]) ? "checked" : ""; ?>>
                                            Transfer Bank
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class='form-group margin-buttom-10px'>
                                        <div class="col-xs-3">
                                            <label>Tanggal Setoran</label>
                                        </div>
                                        <div class="col-xs-8">
                                            <div class="input-group input-append date full-img" id="dateRangePicker">
                                                <input class="form-control" id="datepicker" date_type="date" validate ="required[]" name ="payment_date" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->payment_date : date('d-M-Y') ); ?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row pad-buttom-10">
                                    <div class='form-group'>
                                        <div class="col-xs-3">
                                            <label>Jumlah Setoran</label>
                                        </div>
                                        <div class="col-xs-8">
                                            <div>
                                                <input type="text" name ="payment_amt" class='form-control auto_int' value ="<?php echo $total_instalment; ?>" validate ="required[]">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class='form-group'>
                                    <label>Bukti Setoran<div class="text-color-939598">(unggah foto bukti setoran anda)</div></label>
                                    <div class="margin-left-15px"><input type="file" name="nlogo_img" id="nlogo_img"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class=" margin-buttom-10px">&nbsp;</div>
                            <div class="form-group">
                                <label>Setoran ke Bank<div class="text-color-939598">(Pilih bank tempat anda menyetor)</div></label>
                                <?php foreach ($data_bank as $bank) { ?>
                                    <div class="radio margin-left-15px">
                                        <label>
                                            <input type="radio" name="bank" value="<?php echo $bank->seq; ?>" <?php echo isset($data_sel[LIST_DATA][0]) && $bank->seq == $data_sel[LIST_DATA][0]->confirm_pay_bank_seq ? "checked" : ""; ?>>
                                            <img height="30px" src="<?php echo BANK_UPLOAD_IMAGE . $bank->logo_img; ?>" alt="<?php echo $bank->logo_img; ?>">
                                            <?php echo $bank->bank_acct_no; ?>
                                            <div class="margin-left-94px"><?php echo $bank->bank_acct_name; ?></div>
                                        </label>
                                    </div>
                                <?php } ?>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12 col-xs-12 col-sm-12 text-align-right ">
                                <button type="submit" name="<?php echo CONTROL_SAVE_EDIT_NAME; ?>" class='btn btn-flat btn-confirm'>PROSES ORDER</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        <?php } else if (($data_ord[0][0]->payment_code == PAYMENT_TYPE_CREDIT_CARD || $data_ord[0][0]->payment_code == PAYMENT_TYPE_CREDIT) && $data_ord[0][0]->payment_status == PAYMENT_UNPAID_STATUS_CODE) { ?>
            <body onLoad="oke()">
                <div id="paymentcc">
                    <table align="center" class="margin-top-200">
                        <tr>
                            <td style="text-align: center;">
                                <img src="<?php echo get_img_url() ?>loading-bar.gif" alt="loading-bar">
                                <div id="subtitle">
                                    We are processing your transaction, You will be redirected to our payment gateway system
                                    <div style="color: red; font-weight: bold;">DO NOT CLOSE THIS WINDOW</div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>

                <form action="<?php echo PGS_URL; ?>" method="POST" name="theForm" id="theForm">
                    <input type="hidden" name="GEN_HASH" value="Yes" />
                    <input type="hidden" name="LANG" value="en" />
                    <?php
                    $merchant_id_val = PGS_MERCHANT_ID;
                    $merchant_pass_val = PGS_TXN_PASSWORD;
                    $payment_ind = '';
                    $payment_criteria = '';
                    if ($data_ord[0][0]->promo_credit_seq > 0) {
                        $merchant_id_val = $data_credit_mid;
                        $merchant_pass_val = $data_credit_pas;
                        $payment_ind = '';
                        $payment_criteria = '';
                    }
                    ?>
                    <input type="hidden" name="MERCHANTID" value="<?php echo $merchant_id_val; ?>" />
                    <input type="hidden" name="MERCHANT_TRANID" value="<?php echo $data_ord[0][0]->order_trans_no; ?>" />
                    <input type="hidden" name="PAYMENT_METHOD" value="<?php echo PAYMENT_METHOD; ?>" />
                    <input type="hidden" name="PYMT_IND" value="<?php echo $payment_ind ?>" />
                    <input type="hidden" name="PYMT_CRITERIA" value="<?php echo $payment_criteria ?>" />
                    <input type="hidden" name="CURRENCYCODE" value="IDR" />
                    <input type="hidden" name="AMOUNT" value="<?php echo $data_ord[0][0]->total_payment . ".00" ?>" />
                    <input type="hidden" name="DESCRIPTION" value="" />
                    <input type="hidden" name="CUSTNAME" value="<?php echo $data_ord[0][0]->member_name; ?>" />
                    <input type="hidden" name="CUSTEMAIL" value="<?php echo $data_ord[0][0]->email; ?>" />
                    <input type="hidden" name="PHONE_NO" value="" />
                    <input type="hidden" name="RETURN_URL" value="<?php echo base_url() . "member/payment/proccess/" . $data_ord[0][0]->order_no; ?>" />
                    <input type="hidden" name="handshake_url" value=""/>
                    <input type="hidden" name="handshake_param" value="" />
                    <input type="hidden" name="TXN_PASSWORD" value="<?php echo $merchant_pass_val; ?>" />
                    <input type="hidden" name="SIGNATURE" value="<?php echo $data_sign; ?>" />
                    <input type="hidden" name="BILLING_ADDRESS" value="" />
                    <input type="hidden" name="BILLING_ADDRESS_CITY" value="" />
                    <input type="hidden" name="BILLING_ADDRESS_REGION" value="" />
                    <input type="hidden" name="BILLING_ADDRESS_STATE" value="" />
                    <input type="hidden" name="BILLING_ADDRESS_POSCODE" value="" />
                    <input type="hidden" name="BILLING_ADDRESS_COUNTRY_CODE" value="ID" />
                    <input type="hidden" name="RECEIVER_NAME_FOR_SHIPPING" value="" />
                    <input type="hidden" name="SHIPPING_ADDRESS" value="" />
                    <input type="hidden" name="SHIPPING_ADDRESS_CITY" value="" />
                    <input type="hidden" name="SHIPPING_ADDRESS_REGION" value="" />
                    <input type="hidden" name="SHIPPING_ADDRESS_STATE" value="" />
                    <input type="hidden" name="SHIPPING_ADDRESS_POSCODE" value="" />
                    <input type="hidden" name="SHIPPING_ADDRESS_COUNTRY_CODE" value="" />
                    <input type="hidden" name="SHIPPINGCOST" value="" />
                    <input type="hidden" name="DOMICILE_ADDRESS" value="" />
                    <input type="hidden" name="DOMICILE_ADDRESS_CITY" value="" />
                    <input type="hidden" name="DOMICILE_ADDRESS_REGION" value="" />
                    <input type="hidden" name="DOMICILE_ADDRESS_STATE" value="" />
                    <input type="hidden" name="DOMICILE_ADDRESS_POSCODE" value="" />
                    <input type="hidden" name="DOMICILE_ADDRESS_COUNTRY_CODE" value="" />
                    <input type="hidden" name="DOMICILE_PHONE_NO" value="" />
                    <input type="hidden" name="MREF1" value="" />
                    <input type="hidden" name="MREF2" value="" />
                    <input type="hidden" name="MREF3" value="" />
                    <input type="hidden" name="MREF4" value="" />
                    <input type="hidden" name="MREF5" value="" />
                    <input type="hidden" name="MREF6" value="" />
                    <input type="hidden" name="MREF7" value="" />
                    <input type="hidden" name="MREF8" value="" />
                    <input type="hidden" name="MREF9" value="" />
                    <input type="hidden" name="MREF10" value="" />
                    <input type="hidden" name="MPARAM1" value="" />
                    <input type="hidden" name="MPARAM2" value="" />
                    <input type="hidden" name="CUSTOMER_REF" value="" />
                    <input type="hidden" name="FRISK1" value="" />
                    <input type="hidden" name="FRISK2" value="" />

                </form>
            </body>
            <script language="JavaScript">
                function oke() {

                    var payment_info = [];
                    payment_info = $('#theForm').serializeArray();
                    payment_info.push({name: "type", value: "<?php echo TASK_UPDATE_CC_LOG ?>"});
                    form = document.theForm;
                    $.ajax({
                        url: "<?php echo base_url() . "member/payment/" . $data_ord[0][0]->order_no ?>",
                        type: "post",
                        data: payment_info,
                        success: function (data) {
                            if (isSessionExpired(data)) {
                                response_object = json_decode(data);
                                url = response_object.url;
                                location.href = url;
                            } else {
                                document.forms['theForm'].submit();
                            }
                        },
                        error: function (request, error) {
                            alert(error_response(request.status));
                        }
                    });

                }
            </script>
        <?php } else if (($data_ord[0][0]->payment_code == PAYMENT_TYPE_DOCU_ATM || $data_ord[0][0]->payment_code == PAYMENT_TYPE_DOCU_ALFAMART) && $data_ord[0][0]->payment_status == PAYMENT_UNPAID_STATUS_CODE) { ?>
            <body onLoad="load_form()">
                <div id="payment_docu">
                    <table align="center" class="margin-top-200">
                        <tr>
                            <td style="text-align: center;">
                                <img src="<?php echo get_img_url() ?>loading-bar.gif" alt="loading-bar">
                                <div id="subtitle">
                                    We are processing your transaction, You will be redirected to our payment gateway system
                                    <div style="color: red; font-weight: bold;">DO NOT CLOSE THIS WINDOW</div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <form method="POST" action="<?php echo URL_PAYMENT_REQUEST ?>" accept-charset="UTF-8" id="theForm">
                    <input type="hidden" name="MALLID" value="<?php echo MALLID; ?>">
                    <input type="hidden" name="CHAINMERCHANT" value="<?php echo CHAIN_MERCHANT ?>">
                    <input type="hidden" name="AMOUNT" value="<?php echo $data_ord[0][0]->total_payment . ".00" ?>">
                    <input type="hidden" name="PURCHASEAMOUNT" value="<?php echo $data_ord[0][0]->total_payment . ".00" ?>">
                    <input type="hidden" name="TRANSIDMERCHANT" value="<?php echo $data_ord[0][0]->order_trans_no; ?>">
                    <input type="hidden" name="WORDS" value="<?php echo $data_sign; ?>">
                    <input type="hidden" name="REQUESTDATETIME" value="<?php echo date('YmdHis'); ?>">
                    <input type="hidden" name="CURRENCY" value="<?php echo DOCU_CURRENCY; ?>">
                    <input type="hidden" name="PURCHASECURRENCY" value="<?php echo DOCU_CURRENCY ?>">
                    <input type="hidden" name="SESSIONID" value="<?php echo session_id(); ?>">
                    <input type="hidden" name="NAME" value="<?php echo $data_ord[0][0]->member_name ?>">
                    <input type="hidden" name="EMAIL" type="text" value="<?php echo $data_ord[0][0]->email ?>">
                    <input type="hidden" name="BASKET" type="text" value="<?php echo "Total," . $data_ord[0][0]->total_payment . ".00,1," . $data_ord[0][0]->total_payment . ".00;" ?>">
                    <input type="hidden" name="PAYMENTCHANNEL" value="<?php echo $data_ord[0][0]->payment_code == PAYMENT_TYPE_DOCU_ATM ? ATM_PERMATA_VA_LITE : ALFAMART; ?>">
                </form>
                <script language="JavaScript">
                    function load_form() {
                        var payment_info = [];
                        payment_info = $('#theForm').serializeArray();
                        payment_info.push({name: "type", value: "<?php echo TASK_UPDATE_CC_LOG ?>"});
                        form = document.theForm;
                        $.ajax({
                            url: "<?php echo base_url() . "member/payment/" . $data_ord[0][0]->order_no ?>",
                            type: "post",
                            data: payment_info,
                            success: function (data) {
                                if (isSessionExpired(data)) {
                                    response_object = json_decode(data);
                                    url = response_object.url;
                                    location.href = url;
                                } else {
                                    document.forms['theForm'].submit();
                                }
                            },
                            error: function (request, error) {
                                alert(error_response(request.status));
                            }
                        });
                    }
                </script>
            <?php } else if ($data_ord[0][0]->payment_code == PAYMENT_TYPE_QR_CODE && $data_ord[0][0]->payment_status == PAYMENT_WAIT_CONFIRM_STATUS_CODE) { ?>
                <?php if (isset($data_sel[LIST_DATA][0]->signature)) { ?>
                    <div id="qr_code" class="row reset-margin">
                        <div class="col-xs-12 box-header-cart">
                            <div class="row reset-margin">
                                <div class="col-xs-4 col-xs-offset-1">
                                    <img src="<?php echo URL_DIMO_API . 'api/invoiceQRCode?invoiceId=' . $data_sel[LIST_DATA][0]->signature ?>" />
                                </div>
                                <div class="col-xs-7 mt-20px">
                                    <strong>Cara Pembayaran</strong>
                                    <ol>
                                        <li>Scan QR Code Melalui menu Bayar Pakai QR / Pay By QR di aplikasi smartphone Anda</li>
                                        <li>Pembayaran akan terkonfirmasi di layar smartphone Anda
                                            <br />
                                            <?php foreach (json_decode($data_sel[LIST_DATA][0]->display_issuer_list) as $each_issuer): ?>
                                                <?php if ($each_issuer->icon != ''): ?>
                                                    <img src="<?php echo $each_issuer->icon ?>" width="40px"/>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                            <br />
                                            Scan QR Code menggunakan menu Bayar Pakai QR di aplikasi mobile banking atau e-money diatas
                                        </li>
                                    </ol>
                                    <div style="margin-left:40px">
                                        Tekanlah Tombol Refresh setelah anda melakukan Pembayaran
                                        <button type="button" class="btn btn-flat btn-confirm" onclick="location.reload()">Refresh</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <script>
                    $(document).ready(function () {
                        $('.payment-height').css('margin-top', ($(window).height() / 4) - 40 + 'px');
        <?php if ($data_ord[0][0]->payment_status != 'P') { ?>
                            $('html, body').animate({
                                scrollTop: ($("#qr_code").offset().top) - 200
                            }, 2000);
        <?php } ?>
                        function success_payment() {
                            $.ajax({
                                url: "<?php echo base_url() . "member/payment/" . $data_ord[0][0]->order_no ?>",
                                data: {
                                    btnAdditional: "act_s_adt", order_no: "<?php echo $data_ord[0][0]->order_no ?>", signature: "<?php echo isset($data_sel[LIST_DATA][0]->signature) ? $data_sel[LIST_DATA][0]->signature : '' ?>", payment_type: "<?php echo PAYMENT_TYPE_QR_CODE ?>"
                                },
                                type: "POST",
                                success: function (result) {
                                    data_result = JSON.parse(result);
                                    if (data_result.status === '1') {
                                        clearInterval(check_success);
                                        $('#payment_success').modal('show');
                                        setTimeout(function () {
                                            $('#payment_success').modal('hide');
                                        }, 2000);
                                        $('#qr_code').remove();
                                        if (data_result.get_axa === '1') {
                                            window.location.href = "<?php echo base_url() . "member/payment/" . $data_ord[0][0]->order_no ?>";
                                        }
                                    }
                                },
                                error: function (request, error) {
                                }
                            });
                        }
                    });
                </script>
            <?php } else if ($data_ord[0][0]->payment_code == PAYMENT_TYPE_BCA_KLIKPAY && $data_ord[0][0]->payment_status == PAYMENT_UNPAID_STATUS_CODE) { ?>
                <script language="JavaScript">
                    function oke()
                    {
                        document.forms["theForm"].submit();
                    }
                </script>
                <script>
                    $(document).ready(function () {
                        oke();
                    });
                </script>
                <form
                    action="<?php echo URL_BCA_REDIRECT ?>"
                    method="POST" name="theForm">
                    <!-- Transaction Detail -->
                    <input type="hidden" name="transactionNo" value="<?php echo $data_ord[0][0]->order_trans_no; ?>" />
                    <input type="hidden" name="transactionDate" value="<?php echo $data_bca['transactionDate']; ?>" />
                    <input type="hidden" name="totalAmount" value="<?php echo $data_bca['totalAmount'] ?>" />
                    <input type="hidden" name="descp" value="<?php echo $data_bca['descp'] ?>" />
                    <input type="hidden" name="miscFee" value="<?php echo $data_bca['miscFee'] ?>" />
                    <input type="hidden" name="klikPayCode" value="<?php echo BCA_KLIKPAYCODE ?>" />
                    <input type="hidden" name="signature" value="<?php echo $data_bca[0]->signature ?>" />
                    <input type="hidden" name="clearkey" value="<?php echo BCA_CLEARKEY ?>" />
                    <input type="hidden" name="payType" value="<?php echo BCA_PAYTYPE ?>" />
                    <input type="hidden" name="currency" value="<?php echo BCA_CURRENCY ?>" />
                    <input type="hidden" name="callback" value="<?php echo BCA_CALLBACK . $data_ord[0][0]->order_trans_no ?>" />
                </form>
            <?php } else if ($data_ord[0][0]->pg_method_seq == PAYMENT_SEQ_ADIRA || $data_ord[0][0]->pg_method_seq == PAYMENT_SEQ_ADIRA_DEPOSIT) { ?>
                <?php
                echo $table_simulation;
                $active_deposit = "";
                $active_bank = "";
                echo $customer_table;



                if (isset($data_ord[0][0]->status_order)) {
                    if ($data_ord[0][0]->payment_status == PAYMENT_WAIT_CONFIRM_STATUS_CODE) {
                        if ($data_ord[0][0]->pg_method_seq == PAYMENT_SEQ_ADIRA) {
                            $active_bank = " active";
                        } else {
                            $active_deposit = " active";
                        }
                        ?>


                        <div class="row reset-margin" >
                            <div class="col-xs-12  box-header-cart">
                                <div id="scroll_payment">
                                </div>  
                                <div><h4>Konfirmasi Pembayaran</h4></div>
                                <div class="margin-buttom-10px">Metode Pembayaran : </div>
                                <style>
                                </style>
                                <div id="tab" class="btn-group" data-toggle="buttons-radio">
                                    <a href="#bank" class="btn btn-default<?php echo $active_bank; ?>" data-id="bank" data-toggle="tab">Transfer Bank</a>
                                    <a href="#deposit" class="btn btn-default<?php echo $active_deposit; ?>" data-id="deposit" data-toggle="tab">Deposit</a>
                                </div>
                                <div class="tab-content">
                                    <div id="bank" class="tab-pane<?php echo $active_bank; ?>">
                                        <form id ="frmMain" action ="<?php echo get_base_url() . "member/payment/" . $data_ord[0][0]->order_no; ?>" method="post" enctype ="multipart/form-data">
                                            <input type="hidden" id="payment_type" name="payment_type" value="<?php echo $data_ord[0][0]->pg_method_seq; ?>">
                                            <div class="row reset-margin">
                                                <div class="col-xs-12">
                                                    <div class="col-xs-6">
                                                        <div class="margin-left-15px">
                                                            <br/>
                                                            <div class="form-group clean-margin margin-buttom-10px">
                                                                <label>Jenis Setoran</label>
                                                                <div class="radio clean-margin margin-left-15px">
                                                                    <label>
                                                                        <input type="radio" name="payment_type" value="<?php echo PAYMENT_TYPE_CASH; ?>" <?php echo isset($data_sel[LIST_DATA][0]->payment_type) && $data_sel[LIST_DATA][0]->payment_type == PAYMENT_TYPE_CASH ? "checked" : ""; ?>>
                                                                        Setoran Tunai
                                                                    </label>
                                                                    &nbsp;
                                                                    <label>
                                                                        <input type="radio" name="payment_type" value="<?php echo PAYMENT_TYPE_TRANSFER; ?>" <?php echo isset($data_sel[LIST_DATA][0]->payment_type) && $data_sel[LIST_DATA][0]->payment_type == PAYMENT_TYPE_TRANSFER || !isset($data_sel[LIST_DATA][0]) ? "checked" : ""; ?>>
                                                                        Transfer Bank
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class='form-group margin-buttom-10px'>
                                                                    <div class="col-xs-3">
                                                                        <label>Tanggal Setoran</label>
                                                                    </div>
                                                                    <div class="col-xs-8">
                                                                        <div class="input-group input-append date full-img" id="dateRangePicker">
                                                                            <input class="form-control" id="datepicker" date_type="date" validate ="required[]" name ="payment_date" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->payment_date : date('d-M-Y') ); ?>" readonly>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row pad-buttom-10">
                                                                <div class='form-group'>
                                                                    <div class="col-xs-3">
                                                                        <label>Jumlah Setoran</label>
                                                                    </div>
                                                                    <div class="col-xs-8">
                                                                        <div > 
                                                                            <input type="text" name ="payment_amt" class='form-control auto_int' value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->payment_amt : $total_instalment ); ?>" ></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class='form-group'>
                                                                <label>Bukti Setoran<div class="text-color-939598">(unggah foto bukti setoran anda)</div></label>
                                                                <div class="margin-left-15px"><input type="file" name="nlogo_img" id="nlogo_img"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <div class=" margin-buttom-10px">&nbsp;</div>
                                                        <div class="form-group">
                                                            <label>Setoran ke Bank<div class="text-color-939598">(Pilih bank tempat anda menyetor)</div></label>
                                                            <?php foreach ($data_bank as $bank) { ?>
                                                                <div class="radio margin-left-15px">
                                                                    <label>
                                                                        <input type="radio" class="bank" name="bank" value="<?php echo $bank->seq; ?>" <?php echo isset($data_sel[LIST_DATA][0]) && $bank->seq == $data_sel[LIST_DATA][0]->confirm_pay_bank_seq ? "checked" : ""; ?>>
                                                                        <img height="30px" src="<?php echo BANK_UPLOAD_IMAGE . $bank->logo_img; ?>" alt="<?php echo $bank->logo_img; ?>">
                                                                        <?php echo $bank->bank_acct_no; ?>
                                                                        <div class="margin-left-94px"><?php echo $bank->bank_acct_name; ?></div>
                                                                    </label>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 col-xs-12 col-sm-12 text-align-right ">
                                                    <button type="submit" name="<?php echo CONTROL_SAVE_EDIT_NAME; ?>" class='btn btn-flat btn-confirm'>PROSES ORDER</button>
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                    <div id="deposit" class="tab-pane<?php echo $active_deposit; ?>">
                                        <br />
                                        <form id ="frmMain" action ="<?php echo get_base_url() . "member/payment/" . $data_ord[0][0]->order_no; ?>" method="post" enctype ="multipart/form-data">
                                            <input type="hidden" id="payment_type1" name="payment_type" value="<?php echo $data_ord[0][0]->pg_method_seq; ?>">
                                            <label><input type="radio" name="payment_type_a" value="<?php echo PAYMENT_TYPE_DEPOSIT; ?>"  checked>
                                                Saldo deposit : Rp. <?php echo number_format($member_info[2][0]->deposit_amt); ?></label>
                                            <div class="row">
                                                <div class="col-md-12 col-xs-12 col-sm-12 text-align-right ">
                                                    <button type="submit" name="" class='btn btn-flat btn-confirm'>PROSES ORDER</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <?php if ($data_ord[0][0]->payment_status == PAYMENT_WAIT_CONFIRM_STATUS_CODE && is_agent()) { ?>
                            <script>
                                $(function () {
                                    var payment_scroll = $('#scroll_payment');
                                    var box_body_ff = $('html');
                                    var box_body_gc = $('body');
                                    box_body_ff.animate({scrollTop: $(payment_scroll).offset().top - 73}, 1000);
                                    box_body_gc.animate({scrollTop: $(payment_scroll).offset().top - 73}, 1000);
                                });
                            </script>
                        <?php } ?>


                        <?php
                    }
                }
            } else {
                ?>
            <?php } ?><br>
            <script>
                function submit_form() {
                    $('#frmMain').submit();
                }
                function trackers(id) {
                    if ($("#timeline" + id).is(':visible')) {
                        $("#timeline" + id).hide('slow');
                    } else {
                        $("#timeline" + id).show('slow');
                    }

                }
                function onColor(id, type) {
                    $(".a, .b").each(function () {
                        $(this).css('display', 'none');
                    })

                    $("input[type='radio']").each(function () {
                        $(this).prop("checked", false);
                    })

                    $('#' + id).find('.a').css('display', 'block');
                    $('#' + id).children('.b').css('display', 'block');
                    $('#' + id).find("input[type='radio']").prop("checked", true);
                    $("#payment").val(id);

                }
            </script>
        <?php } else { ?>
            <div class="row reset-margin">
                <div class="col-xs-12 box-header-cart" style="font-size: 14px">
                    <div><h4>Detail Order</h4></div>
                    <div class="margin-left-15px">
                        <div>No. Order : <?php echo $data_ord[0][0]->order_no ?></div>
                        <div>Tanggal : <?php echo date_format(date_create($data_ord[0][0]->order_date), "d-M-Y"); ?></div>
                        <div>Status Pembayaran: <?php
                            $data = json_decode(STATUS_PAYMENT, true);
                            echo $data[$data_ord[0][0]->payment_status];
                            ?>
                        </div>
                        <div>Status Pesanan: <?php
                            $data = json_decode(STATUS_ORDER_VOUCHER, true);
                            echo $data[$data_ord[0][0]->order_status];
                            ?>
                        </div>
                        <div>No HP: <?php echo $data_ord[0][0]->phone_number; ?></div>
                        <div>Provider: <?php echo $data_ord[0][0]->provider_name; ?></div>
                        <div>Service: <?php echo $data_ord[0][0]->provider_service_name; ?></div>
                        <div>Nominal: Rp. <?php echo number_format($data_ord[0][0]->nominal); ?></div>
                        <div>Total Bayar: Rp. <?php echo number_format($data_ord[0][0]->total_payment); ?></div>
                        <div>Metode Pembayaran: <?php echo $data_ord[0][0]->pg_method_name; ?></div>
                    </div>
                </div>
            </div>
        <?php } ?>
</div>