<?php
require_once VIEW_BASE_HOME;
unset($popupbanner);
?>
<?php
if (isset($popupbanner)) {
    if ($popupbanner == 0) {
        ?>
        <link rel="stylesheet" type="text/css" href="<?php echo get_css_url(); ?>animate.css"/>
        <!-- modal -->
        <div id="fsModal" class="modal">
            <div class="modal-dialogs">
                <div class="modal-contents">
                    <div class="container container-table animated zoomInUp" id="formid">
                        <div class="row vertical-center">
                            <div class="text-center col-xs-6 col-xs-offset-3">
                                <form id="frmRegister"
                                      style="background-image: url(<?php echo get_img_url() . 'welcome_screen.jpg' ?>);
                                      width: 117%;"
                                      method="post" action= "<?php echo get_base_url() ?>">
                                    <input type="hidden" id="newm" name="newm" value="new_member"/>
                                    <div class="container">
                                        <div class="form-group text-left">
                                            <label style="font-family: 'Source Sans Pro',sans-serif; margin-top: 15px;
                                                   font-size: 157%;
                                                   font-weight: 800;
                                                   font-style: oblique;
                                                   color: #0B2C39;">
                                                DAPATKAN VOUCHER BELANJA
                                            </label>
                                            </br>
                                            <label style="font-family: 'Source Sans Pro',sans-serif; margin-top: -20px;
                                                   font-size: 470%;
                                                   font-weight: 800;
                                                   font-style: oblique;
                                                   color: #0B2C39;">
                                                Rp50.000,-
                                            </label>
                                        </div>
                                        <h4><p class="pull-left" style="font-family: 'Source Sans Pro',sans-serif;
                                               color: #0B2C39;
                                               margin-top: -20px;
                                               margin-left: 5px;">
                                                Segera daftarkan email anda di sini
                                            </p></h4>
                                    </div>
                                    <div class="container">
                                        <p id="divmessage"></p>
                                        <div class="form-group col-xs-12">
                                            <input type="email" class="form-control"
                                                   style="width: 30%;
                                                   background-color: rgba(255, 255, 255, 0.7);
                                                   margin-left: -12px;"
                                                   id="qemail" name="qemail" placeholder="Masukkan alamat email anda"
                                                   required=""/>
                                        </div>
                                        <div class="form-group pull-left">
                                            <div class="row" style="margin-left : 5px;">
                                                <input type="radio" id="jk" name="jk" value="M" required=""/> Pria &nbsp;
                                                <input type="radio" id="jk" name="jk" value="F" required=""/> Wanita
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container">
                                        <div class="form-group pull-left">
                                            <label class="text-left" for="snk"
                                                   style="font-family: 'Source Sans Pro',sans-serif;
                                                   margin-left: 5px;
                                                   color: #0B2C39;
                                                   font-size: small;">
                                                <input type="checkbox" id="snk" name="snk" value="1" required=""/>
                                                Setuju dengan
                                                <a style="color: #EE3029;" href="<?php echo get_base_url(); ?>info/syarat-dan-ketentuan"
                                                   target="_blank">Syarat dan Ketentuan</a>, serta
                                                <a style="color: #EE3029;" href="<?php echo get_base_url(); ?>info/kebijakan-privasi"
                                                   target="_blank">Kebijakan </a>
                                                </br>&nbsp;&nbsp;&nbsp;
                                                dari Toko1001
                                            </label>
                                        </div>
                                    </div>
                                    <div class="container">
                                        <div class="form-group col-xs-12">
                                            <button class="btn btn-info pull-left"
                                                    style="background-color: #0B2C39;
                                                    border: none;
                                                    margin-left: -10px;
                                                    width: 120px"> Daftar
                                            </button>
                                            <a class="text-left col-xs-3"
                                               style="margin-left: 10px;
                                               margin-top: 5px;
                                               font-family: 'Source Sans Pro',sans-serif;
                                               color: rgb(11, 125, 189);"
                                               href="#" data-dismiss="modal">
                                                Lanjutkan ke Toko1001 &nbsp;<i class="fa fa-arrow-right"></i>
                                            </a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            setTimeout(function () {
                $('#fsModal').modal()
            }, 1000);
            var success_msg = '<p class="text-left" style="margin-left: 5px; color: green; width: 500px; margin-bottom: 0!important;><h4><i class="fa fa-info"></i></h4>';
            var error_msg = '<p class="text-left" style="margin-left: 505px; color: red; width: 500px; margin-bottom: 0!important;> <h4><i class="fa fa-info"></i></h4>';
            $(function () {
                $("#frmRegister").on("submit", function (event) {
                    event.preventDefault();

                    $.ajax({
                        url: "<?php echo get_base_url(); ?>",
                        type: "post",
                        data: $(this).serialize(),
                        success: function (data) {
                            if (data.trim() == 'OK') {
                                $('#divmessage').html(success_msg + "Terima kasih telah mendaftar sebagai Member di Toko1001. </br>Harap cek email anda.</p>");
                                $("#divmessage").fadeTo(4000, 1500).slideUp(1000, function () {
                                    $('#fsModal').modal('toggle');
                                });
                                return false;
                            } else {
                                $('#divmessage').html(error_msg + data + "</p>");
                                $("#divmessage").fadeTo(4000, 1500).slideUp(1000, function () {
                                });
                                return false;
                            }
                        }
                    });
                });
            });

            var isValidSignUp = $('#frmSignUpMerchant').validate({
                ignore: 'input[type=hidden]',
                highlight: function (element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else if (element.hasClass('select2')) {
                        error.insertAfter(element.next('span'));
                    } else {
                        error.insertAfter(element);
                    }
                }
            });

        </script>
        <?php
    }
}
?>

<div class="container">
    <div class="col-xs-12 no-pad no-margin">
        <?php if (isset($banner[0])): ?>
            <div class="row no-pad no-margin">
                <div class='col-xs-12 no-pad no-margin'>
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <?php foreach ($banner[0] as $key => $each_banner): ?>
                                <?php if ($key == 0): ?>
                                    <div class="item active width-100">
                                        <a href="<?php echo base_url() . $each_banner->image_url; ?>">
                                            <img src="<?php echo get_file_exists(SLIDE_UPLOAD_IMAGE . '/' . $each_banner->image); ?>" alt="<?php echo $each_banner->image; ?>" id="img-prev-slider"
                                                 class="img-responsive center-block">
                                        </a>
                                    </div>
                                <?php else: ?>
                                    <div class="item">
                                        <a href="<?php echo base_url() . $each_banner->image_url; ?>">
                                            <img src="<?php echo get_file_exists(SLIDE_UPLOAD_IMAGE . '/' . $each_banner->image); ?>" alt="<?php echo $each_banner->image; ?>"
                                                 class="img-responsive center-block" id="img-prev-slider">
                                        </a>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <!--                            <div class="item active width-100">
                                                            <img src="<?php base_url(); ?>assets/img/1.jpg" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="item width-100">
                                                            <img src="<?php base_url(); ?>assets/img/2.jpg" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="item width-100">
                                                            <img src="<?php base_url(); ?>assets/img/4.jpg" alt="">
                                                            </a>
                                                        </div>-->
                        </div>
                        <a class="left carousel-control" href="#myCarousel" role="button"
                           data-slide="prev"> <span
                                class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a> <a class="right carousel-control" href="#myCarousel"
                                role="button" data-slide="next"> <span
                                class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>

<div id="nav-anchor"></div>

<div class="container" id="mfloor_0">
    <div class="col-xs-12 bb-2px mb-10px">
        <div class="im-floor">
            <div class="header-floor">
                <h1 class="color-primary text-head size-h3">PRODUK TERBARU</h1>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row reset-margin">
        <?php echo display_product_home($new_products); ?>
    </div>
    <div class="row reset-margin see-more">
        <div class="pull-right">
            <div style="padding:.1em;"><a href="<?php base_url(); ?> produk-terbaru">Lihat Selengkapnya</a></div>
        </div>
    </div>
</div>
<br>
<!--
--------------------------------------------------------------------------------
 PROMO PAGES HERE 
--------------------------------------------------------------------------------
-->
<!--<div class="container">
    <div class="row reset-margin promo">
        promo should be here ... !!
    </div>
</div>-->
<!-- 
--------------------------------------------------------------------------------
END PROMO PAGE
--------------------------------------------------------------------------------
-->

<?php $counter = 1; ?>
<?php if (isset($tree_category[0])): ?>
    <?php foreach ($tree_category[0] as $each_tree_category): ?>
        <?php if ($each_tree_category->seq == "1" || $each_tree_category->seq == "7" || $each_tree_category->seq == "3" || $each_tree_category->seq == "8" || $each_tree_category->seq == "4" || $each_tree_category->seq == "9") { ?>
            <?php if ($counter <= FLOOR_MAX): ?>
                <?php $each_tree_category_seq = $each_tree_category->seq; ?>
                <?php
                $color_icon = "black"; //filled with black or white
                ?>
                <?php if (isset($floor)): ?>
                    <?php foreach ($floor as $each_floor): ?>
                        <?php if ($each_floor->category_seq == $each_tree_category_seq): ?>
                            <div class="container" id="mfloor_<?php echo $each_floor->category_seq; ?>">
                                <div class="col-xs-12 bb-2px mb-10px">
                                    <div class="im-floor">
                                        <div class="header-floor">
                                            <h3 class="text-head color-primary">
                                                <?php echo $each_tree_category->name; ?>
                                                <!--</a>-->
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="container">
                                <div class="floor<?php echo url_title(strtolower($each_tree_category->name)); ?>">
                                    <div class='row reset-margin'>
                                        <!--golongan 1-->
                                        <div class='set-statis-img'>
                                            <div class='row reset-margin'>
                                                <div class="col-xs-12 no-pad">
                                                    <div class="hvr-wobble-vertical load_img" style="position: relative;height: 468px !important;width: 500px">
                                                        <a href="<?php echo $each_floor->adv_1_img_url ?>">
                                                            <img data-src="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $each_floor->adv_1_img) ?>" data-src-retina="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $each_floor->adv_1_img) ?>" src="<?php echo base_url() . ASSET_IMG_HOME . 'blank.png' ?>" alt="<?php echo url_title(strtolower($each_tree_category->name)); ?>" class="b-lazy" style="position: absolute;top: -20px;left: -20px;">
                                                            <!--<img src="<?php echo base_url() ?>assets/img/iphonetesting.png" >-->
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!--golongan 2-->
                                        <div class="flor-img-box">
                                            <div class="row reset-margin">
                                                <div class="set-statis-img-mini no-pad">
                                                    <div class="hvr-bob load_img">
                                                        <div class="img-h load_img">
                                                            <a href="<?php echo $each_floor->adv_2_img_url ?>">
                                                                <img data-src="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $each_floor->adv_2_img) ?>" data-src-retina="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $each_floor->adv_2_img) ?>" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="<?php echo url_title(strtolower($each_tree_category->name)); ?>" class="center-block set-statis-img-mini b-lazy">
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="set-statis-img-mini no-pad">
                                                    <div class="hvr-bob">
                                                        <div class="img-h load_img">
                                                            <a href="<?php echo $each_floor->adv_7_img_url ?>">
                                                                <img data-src="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $each_floor->adv_7_img) ?>" data-src-retina="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $each_floor->adv_7_img) ?>" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="<?php echo url_title(strtolower($each_tree_category->name)); ?>" class="set-statis-img-mini center-block b-lazy">
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <!--golongan 3-->
                                        <div class="flor-img-box">
                                            <div class="row reset-margin">
                                                <div class="set-statis-img-mini no-pad">
                                                    <div class="hvr-bob load_img">
                                                        <a href="<?php echo $each_floor->adv_3_img_url ?>">
                                                            <img data-src="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $each_floor->adv_3_img) ?>" data-src-retina="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $each_floor->adv_3_img) ?>" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="<?php echo url_title(strtolower($each_tree_category->name)); ?>" class="set-statis-img-mini center-block b-lazy">
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="set-statis-img-mini no-pad">
                                                    <div class="hvr-bob load_img">
                                                        <a href="<?php echo $each_floor->adv_6_img_url ?>">
                                                            <img data-src="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $each_floor->adv_6_img) ?>" data-src-retina="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $each_floor->adv_6_img) ?>" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="<?php echo url_title(strtolower($each_tree_category->name)); ?>" class="set-statis-img-mini center-block b-lazy">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!--golongan 4-->
                                        <div class="flor-img-box">
                                            <div class="row reset-margin">
                                                <div class="set-statis-img-mini no-pad">
                                                    <div class="hvr-bob load_img">
                                                        <a href="<?php echo $each_floor->adv_4_img_url ?>">
                                                            <img data-src="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $each_floor->adv_4_img) ?>" data-src-retina="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $each_floor->adv_4_img) ?>" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="<?php echo url_title(strtolower($each_tree_category->name)); ?>" class="set-statis-img-mini center-block b-lazy">
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="set-statis-img-mini no-pad">
                                                    <div class="hvr-bob load_img">
                                                        <a href="<?php echo $each_floor->adv_5_img_url ?>">
                                                            <img data-src="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $each_floor->adv_5_img) ?>" data-src-retina="<?php echo get_file_exists(ADV_UPLOAD_IMAGE . $each_floor->adv_5_img) ?>" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="<?php echo url_title(strtolower($each_tree_category->name)); ?>" class="set-statis-img-mini center-block b-lazy">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="row reset-margin see-more">
                                        <div class="pull-right"><a class="floor-link" href="<?php echo base_url() . url_title(strtolower($each_tree_category->name)) . "-" . CATEGORY . ($each_tree_category->seq); ?>">Lihat selengkapnya&nbsp;</a></div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
                <?php $counter++; ?>
            <?php endif; ?>
        <?php } ?>
    <?php endforeach; ?>
<?php endif; ?>
<navmenu class="hidden-md" style="display:none;">
    <ul>
        <li><a href="#homes" data-toggle="tooltip" data-placement="right" data-original-title="Home"><i class="fa fa-home"></i></a></li>
        <li><a href="#mfloor_0" data-toggle="tooltip" data-placement="right" data-original-title="Promo"><i class="fa fa-tags"></i></a></li>
        <li><a href="#mfloor_1" data-toggle="tooltip" data-placement="right" data-original-title="Handphone & Tablet"><i class="icon icon-icon icon-handphone-tablet"></i></a></li>
        <li><a href="#mfloor_7" data-toggle="tooltip" data-placement="right" data-original-title="Komputer"><i class="fa fa-desktop"></i></a></li>
        <li><a href="#mfloor_8" data-toggle="tooltip" data-placement="right" data-original-title="Elektronik & Audio"><i class="fa fa-television"></i></a></li>
        <li><a href="#mfloor_3" data-toggle="tooltip" data-placement="right" data-original-title="Fashion"><i class="icon icon-icon icon-fashion"></i></a></li>
        <li><a href="#mfloor_4" data-toggle="tooltip" data-placement="right" data-original-title="Kesehatan dan Kecantikan"><i class="icon icon-icon icon-kesehatan-kecantikan" aria-hidden='true'></i></a></li>
        <li><a href="#mfloor_9" data-toggle="tooltip" data-placement="right" data-original-title="Peralatan Rumah Tangga"><i class="icon icon-icon icon-peralatan-rumahtangga"></i></a></li>
    </ul>
</navmenu>
<?php $this->load->view(LIVEVIEW . 'home/template/segment_main/js_add_product.php'); ?>
<script>
    $('.productDetails').mouseenter(function () {
        var title = 'title-' + $(this).attr('data-img');
        $('#' + title).removeClass('no-show');
    });
    $('.productDetails').mouseleave(function () {
        var title = 'title-' + $(this).attr('data-img');
    });
    $.scrollTo = $.fn.scrollTo = function (x, y, options) {
        if (!(this instanceof $))
            return $.fn.scrollTo.apply($('html, body'), arguments);
        if (y == '#homes') {
            coord_y = -80;
        } else {
            coord_y = -45;
        }
        options = $.extend({}, {
            gap: {
                x: 0,
                y: coord_y
            },
            animation: {
                easing: 'swing',
                duration: 600,
                complete: $.noop,
                step: $.noop
            }
        }, options);

        return this.each(function () {
            var elem = $(this);
            elem.stop().animate({
                scrollLeft: !isNaN(Number(x)) ? x : $(y).offset().left + options.gap.x,
                scrollTop: !isNaN(Number(y)) ? y : $(y).offset().top + options.gap.y
            }, options.animation);
        });
    };
    $(document).ready(function () {
        $(window).scroll(function () {
            var window_top = $(window).scrollTop() + 200;
            var div_top = $('#nav-anchor').offset().top;
            if (window_top > div_top) {
                $('navmenu').addClass('stick');
                $('navmenu').slideDown();
            } else {
                $('navmenu').removeClass('stick');
                $('navmenu').hide();
            }
        });
        $("navmenu a").click(function (evn) {
            evn.preventDefault();
            $('html,body').scrollTo(this.hash, this.hash);
        });
        var aChildren = $("navmenu li").children();
        var aArray = [];
        for (var i = 0; i < aChildren.length; i++) {
            var aChild = aChildren[i];
            var ahref = $(aChild).attr('href');
            aArray.push(ahref);
        }
        $(window).scroll(function () {
            var windowPos = $(window).scrollTop();
            var windowHeight = $(window).height();
            var docHeight = $(document).height();

            for (var i = 0; i < aArray.length; i++) {
                var theID = aArray[i];
                var divPos = $(theID).offset().top;
                var divHeight = $(theID).height();
                if ((windowPos + 60) >= divPos && windowPos < (divPos + divHeight)) {
                    $("a[href='" + theID + "']").addClass("nav-active");
                } else {
                    $("a[href='" + theID + "']").removeClass("nav-active");
                }
            }
            if (windowPos + windowHeight == docHeight) {
                if (!$("navmenu li:last-child a").hasClass("nav-active")) {
                    var navActiveCurrent = $(".nav-active").attr("href");
                    $("a[href='" + navActiveCurrent + "']").removeClass("nav-active");
                    $("navmenu li:last-child a").addClass("nav-active");
                }
            }
        });
    });
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

</script>