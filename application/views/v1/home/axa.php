<?php
require_once VIEW_BASE_HOME;
?>
<br>
<div class="container">
    <?php if (isset($data_err) && $data_err[ERROR] === true) { ?>
        <div class="alert alert-error-home">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $data_err[ERROR_MESSAGE][0] ?>
        </div>
    <?php } ?>
    <div class="row reset-margin">
        <div class="col-xs-12 background_axa">
            <div class="box box-solid mt-20px">
                <div class="box-body">
                    <img src="<?php echo base_url() ?>assets/img/logo-axa.png" class="pull-left" />
                </div>
            </div>
            <h4>AXA Care protection<small>&nbsp;Asuransi Kecelakaan Diri Gratis dari AXA Life</small></h4>
            <div class="box box-solid">
                <div class="box-body">
                    <p>
                        <strong>Definisi</strong>
                        <br><br>
                        Axa Care Protection adalah program perlindungan dari <a href="https://axa.co.id/">AXA Life Indonesia</a>.
                        Program ini merupakan asuransi kecelakaan diri yang memberikan manfaat uang pertanggungan jika meninggal dunia akibat kecelakaan.
                        <br><br>
                        <strong>Syarat &amp; Ketentuan:</strong>

                    <ul>
                        <li>Usia Kepesertaan: 18 (delapan belas) tahun sampai dengan 64 (enam puluh empat) tahun</li>
                        <li>Premi: Rp: 0 (nol rupiah)</li>
                        <li>Masa Perlindungan: 3 (tiga) bulan sejak tanggal registrasi</li>
                    </ul>

                    <strong>Manfaat Asuransi AXA Care Protection</strong>
                    <br><br>
                    Santunan meninggal dunia akibat kecelakaan sebesar Rp. 25.000.000,-(dua puluh lima juta rupiah).
                    </p>
                    <br>

                    <div class="col-xs-8 col-xs-offset-2 form_box_axa">
                        <form class="axa" id="frmAxa" method="post" action="<?php echo base_url() . $data_auth[FORM_URL] ?>">
                            <div class="form-group has-feedback">                            
                                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                <input type="text" id="member_name" name="member_name" class="form-control line-input" maxlength="50" placeholder="Nama sesuai KTP" validate="required[]" 
                                       value="<?php echo $data_sel[LIST_DATA][0][0]->member_name ?>" />
                            </div>
                            <div class="form-group has-feedback ">
                                <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                                <input type="text" id="member_birthday" name="member_birthday" class="form-control line-input" style="background-color: #fff;" placeholder="Tanggal Lahir" date_type="date" validate="required[]" readonly 
                                       value="<?php echo $data_sel[LIST_DATA][0][0]->birthday ?>"/>
                            </div>
                            <div class = "form-group has-feedback">
                                <span class = "glyphicon glyphicon-earphone form-control-feedback"></span>
                                <input type="text" id="member_phone" name="member_phone" class="form-control line-input" placeholder = "Nomor handphone" data-mask data-inputmask = "'mask': ['999 999 999 999 999']" validate="required[]" 
                                       value="<?php echo $data_sel[LIST_DATA][0][0]->mobile_phone ?>"/>
                            </div>
                            <div class="form-group has-feedback ">
                                <span class="glyphicon glyphicon-envelope form-control-feedback" ></span>
                                <input type="text" id="member_email" name="member_email" class="form-control line-input" placeholder="Alamat email" maxlength="150" validate="email[]" 
                                       value="<?php echo $data_sel[LIST_DATA][0][0]->email ?>"/>
                            </div>
                            <div class="row reset-margin">
                                <input type="radio" name="gender_member" value="M" 
                                       <?php if ($data_sel[LIST_DATA][0][0]->gender == "M") echo "checked" ?>>&nbsp;Pria&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="gender_member" value="F" 
                                       <?php if ($data_sel[LIST_DATA][0][0]->gender == "F") echo "checked" ?>>&nbsp;Wanita
                            </div>
                            <div class = "row add-top-margin">
                                <div class = "col-xs-12">
                                    <label>
                                        <input type="checkbox" id="member_requirement" name="member_requirement" value = "1">
                                        &nbsp;Saya setuju mendapatkan Asuransi Kecelakaan Diri Gratis dan mendapatkan informasi mengenai produk dari&nbsp;<a href="https://axa.co.id/">AXA Life Indonesia</a>
                                    </label>
                                </div>
                            </div>
                            <br>
                            <?php echo get_csrf_member_token() ?>
                            <button type="submit" name ="<?php echo CONTROL_SAVE_ADD_NAME ?>" id="btnmember" class="btn btn-green btn-block btn-flat">DAFTAR</button>
                            <button type="submit" name ="<?php echo CONTROL_SAVE_DELETE_NAME ?>" id="btnnonmember" class="btn btn-confirm btn-block btn-flat">LEWATI</button>
                        </form>
                    </div>
                </div>
            </div>
            <p>*<a href="https://axa.co.id/">AXA Life Indonesia</a> akan mengirimkan Sertifikat Polis ke alamat email anda yang aktif, pastikan semua data anda valid agar bisa menerima Sertifikat Polis dan melakukan klaim.</p>
        </div>
    </div>
</div>
<script>
    $("#btnnonmember").click(function() {
        if (confirm("Apakah anda yakin tidak ingin mendaftarkan diri sebagai member AXA?")) {
            form.submit();
        } else {
            $("#btnnonmember").blur();
            return false;
        }
    });
</script>

