<!DOCTYPE>
<html lang="en">
    <head>
        <?php $this->load->view(LIVEVIEW . 'home/template/segment_main/html_head'); ?>
    </head>
    <body>
        <!-- Fixed navbar -->
        <?php $this->load->view(LIVEVIEW . 'home/template/segment_main_cart/menu'); ?>
        <!-- Lantai promo -->
        <div class="container-for-ipad">
            <?php echo $_content_; ?>
        </div>
        <?php $this->load->view(LIVEVIEW . 'home/template/segment_main_cart/body_footer'); ?>
        <?php $this->load->view(LIVEVIEW . 'home/template/segment_main/html_script'); ?>
    </body>
</html>