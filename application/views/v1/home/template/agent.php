<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view(LIVEVIEW . 'home/template/segment_agent/html_head'); ?>
    </head>
    <body>
        <span id="homes"></span>
        <!-- Fixed navbar -->
        <?php $this->load->view(LIVEVIEW . 'home/template/segment_agent/banner_page'); ?>
        <?php $this->load->view(LIVEVIEW . 'home/template/segment_agent/menu'); ?>

        <!-- Lantai promo -->
        <div class="container-for-ipad">
            <?php echo $_content_; ?>
        </div>

        <?php $this->load->view(LIVEVIEW . 'home/template/segment_agent/body_footer'); ?>
        <?php $this->load->view(LIVEVIEW . 'home/template/segment_agent/html_script'); ?>

    </body>
</html>