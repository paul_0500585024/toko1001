<script text="javascript">
    function add_product(product_seq, redirect) {
<?php if (!isset($_SESSION[SESSION_MEMBER_CSRF_TOKEN]) && !isset($_SESSION[SESSION_AGENT_CSRF_TOKEN])) { ?>
            $("#login_register").modal('show');
            $("#daftar").removeClass('active');
            $("#tabregister").removeClass('active');
            $("#tablogin").addClass('active');
            $("#login").addClass('active');
<?php } else { ?>
            var credit = "";
            if ($('#promo_credit') !== null) {
                credit = $('#promo_credit').val();
            }
            $.ajax({                
                async: true, //set to cross origin problem 
                url: "<?php echo base_url("member/main_page"); ?>",
                type: "POST",
                dataType: "json",
                data: {
                    "txtMemberSID": "<?php echo (isset($_SESSION[SESSION_MEMBER_CSRF_TOKEN]) ? $_SESSION[SESSION_MEMBER_CSRF_TOKEN] : $_SESSION[SESSION_AGENT_CSRF_TOKEN]); ?>",
                    "type": "<?php echo TASK_TYPE_ADD; ?>",
                    "product_seq": product_seq,
                    "credit": credit
                },
                success: function(data) {
                    if (isSessionExpired(data)) {
                        response_object = json_decode(data);
                        url = response_object.url;
                        location.href = url;
                    } else {
                        if (redirect != undefined) {
                            location.href = "<?php echo base_url("member/checkout") . "?step=1" ?>";
                        }
                        if (data[product_seq].count > 0) {
                            //$('#cart_qty_xs').text(data[product_seq].count);
                            var number_active_img_zoom = '';
                            var itemImg = '';
                            if (imgdtl != '') {
                                var number_active_img_zoom = '';
                                number_active_img_zoom = $('#active_img_zoom_container_number').val();
                                itemImg = $("[id^='id_data_small_thumb" + number_active_img_zoom + "']");
                            } else {
                                itemImg = $("#img-terbaru-" + product_seq);
                            }
                            $('#img-terbaru-' + product_seq).removeClass('full-display-img');
                            flyToElement($(itemImg), $('.bag_box'));
                            $('#img-terbaru-' + product_seq).addClass('full-display-img');
                            imageFile = "<?php echo  PRODUCT_UPLOAD_IMAGE; ?>" + data[product_seq].merchant_seq + "/<?php echo S_IMAGE_UPLOAD; ?>" + data[product_seq].img;
//                            if (!imageExists(imageFile))
//                                imageFile = '<?php echo get_image_location() . ASSET_IMG_HOME . "/no_image.png"; ?>';
                            if (data[product_seq].variant_name == "All Product") {
                                variant_name = "";
                            } else {
                                variant_name = "Warna : " + data[product_seq].variant_name + "<br />";
                            }
                            var content = "<table class='table no-border no-margin' id='row_" + product_seq + "'>" +
                                    "<tr><td width='90' rowspan='2' style='border-top: 1px solid lightgray;'><a href ='<?php echo base_url(); ?>" + data[product_seq].product_name.replace(/\s+/g, '-').toLowerCase() + "-" + product_seq + "'><img class='pure-img' src= '" + imageFile + "' alt='" + data[product_seq].img + "'></a></td>" +
                                    "<td colspan='4' style='border-top: 1px solid lightgray;'><b><a href='<?php echo base_url(); ?>" + data[product_seq].product_name.replace(/\s+/g, '-').toLowerCase() + "-" + product_seq + "' class='font-merchant-black'>" + data[product_seq].product_name + "</a></b><div class='pull-right'><button class='btn-transparent' href='#' onClick ='delete_product(" + product_seq + ")'><i class='fa fa-remove text-red'></i></button></div></td></tr>" +
                                    "<tr><td>Berat : " + format(data[product_seq].product_weight) + " Kg.<br />" + variant_name +
                                    "<div class='relative'><div class='store-merchant'></div> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href='<?php echo base_url() . "merchant/"; ?>" + data[product_seq].merchant_code + "'>" + data[product_seq].merchant_name + "</a><br /></div>" +
                                    "<i class='fa fa-truck'></i>&nbsp;" + data[product_seq].exp_name + "</p></td>" +
                                    "<td width='150' style='text-align:right;padding-right: 20px'><div id='sell_price_" + product_seq + "' sell_price ='" + data[product_seq].sell_price + "'>Rp. " + format(data[product_seq].sell_price) + "</div></td>" +
                                    "<td width='80'><select name='product_qty'  id ='product_qty_" + product_seq + "' onchange = 'dropdown_qty(" + product_seq + ", this.id)'>";
                            for (i = 1; i <= data[product_seq].max_buy; ++i) {
                                content = content + "<option>" + i + "</option>";
                            }
                            content = content + " </select></td>" +
                                    "<td width='150' style='text-align:right;padding-right: 20px'><div id='total_price_" + product_seq + "' total_price='" + data[product_seq].sell_price + "'>Rp." + format(data[product_seq].sell_price) +
                                    "</tr></table>";

                            $('#table_product').append(content);
                            calculate_total();
                        }
                    }
                },
                error: function(request, error) {
                    alert(error_response(request.status));
                }
            });
<?php } ?>
    }
    function login_register() {
        $("#frm_login").modal('show');
    }
</script>
