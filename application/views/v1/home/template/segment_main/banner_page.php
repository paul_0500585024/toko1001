<script type="text/javascript">
    (function () {
        var po = document.createElement('script');
        po.type = 'text/javascript';
        po.async = true;
        po.src = 'https://apis.google.com/js/client.js?onload=onLoadCallback';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(po, s);
    })();
    (function (d) {
        var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement('script');
        js.id = id;
        js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";
        ref.parentNode.insertBefore(js, ref);
    }(document));
    function showhidefrm() {
        $("#frm_login").modal('hide');
        $("#frm_register").modal('show');
    }
</script>
<div class="modal fade" id="frm_login" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="dialog">
        <div class="row">
            <div class="col-xs-12">
                <div class="m-login">
                    <div class="header">
                        LOGIN
                    </div>
                    <div class="cst-close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i></div>
                    <div class="row" style="margin-top:1em;">
                        <div class='col-xs-12'>
                            <?php if (!isset($_SESSION[SESSION_MEMBER_CSRF_TOKEN])) { ?>
                                <form id="frmMain" class="inline" method = "post" action= "<?php echo get_base_url() ?>member/login/sign_in">
                                    <input name="type" type="hidden" value="standard_login"/>
                                    <div class="form-group has-feedback">
                                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                        <input type="text" name="email" class="form-control line-input" placeholder="Email" validate="email[]" maxlength="150" />
                                    </div>
                                    <div class="form-group has-feedback">
                                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                        <input validate="required[]" type="password" name="password" class="form-control line-input" placeholder="Password" maxlength="50" />
                                    </div>
                                    <div class="col-xs-6 mr-5px btn-login">
                                        <div class="row reset-margin pb-6px">&nbsp;</div>
                                        <div class="row reset-margin">
                                            <div class="row reset-margin">
                                                <button type="submit" class="btn btn-block btn-green btn-flat">Login</button>
                                            </div>
                                            <div class="row reset-margin">
                                                <a href="<?php echo base_url() ?>member/forgot_password">Lupa Password </a>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="current_url" value="<?php echo current_url_with_query_string() ?>" />
                                </form>
                                <div class="col-xs-6 pl-6">
                                    <div class="row reset-margin">
                                        <button class="btn btn-flat btn-front btn-block btn-facebook mb-5px"  id="facebook" onclick="fb_login();"><i class="fa fa-facebook"></i>&nbsp;Login Dengan Facebook</button>
                                    </div>
                                    <div class="row reset-margin">
                                        <div class="row reset-margin">
                                            <button class="btn btn-flat btn-front btn-block btn-facebook back-color-gplus2"  id="facebook" onclick="gp_login();" ><i class="fa fa-google-plus back-color-gplus1"></i>&nbsp;Login Dengan Google +</button>
                                        </div>

                                    </div>
                                </div>
                                <div class="row add-top-margin">
                                    <div class="col-xs-12" style="text-align: center;">
                                        Belum punya akun ? <a id="loginregister" data-id="0" style="cursor: pointer " onclick="showhidefrm()">Daftar</a>
                                    </div>
                                </div>

                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="frm_register" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="dialog">
        <div class="row">
            <div class="col-xs-12">
                <div class="mdl-register">
                    <div class="cst-close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i></div>
                    <div class="mdl-content">
                        <div class="header">
                            Daftar Member
                        </div>
                        <p class="col-xs-12" id="content-message"></p>
                        <div class="row add-top-margin">
                            <div class="col-xs-12">
                                <form id="frmSignupMember" class="" action="<?php echo get_base_url(); ?>member/sign_up/register" method="post">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group has-feedback">
                                                <span class="glyphicon glyphicon-envelope form-control-feedback" ></span>
                                                <input type="text" name="member_email" id="member_email" class="form-control line-input" placeholder="Email" validate="email[]"  maxlength="150" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="form-group has-feedback">
                                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                                <input id="password1" validate="password[]" name="password1" type="password" class="form-control line-input" placeholder="Password" maxlength="50" />
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="form-group has-feedback">
                                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                                <input id="password2" validate="password[]" name="password2" type="password" class="form-control line-input" placeholder="Ulangi password" maxlength="50" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <!--<div class="col-xs-12">-->
                                        <div class="col-xs-6">
                                            <div class="form-group has-feedback">
                                                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                                <input id="member_name" validate="required[]" name="member_name" type="text" class="form-control line-input" placeholder="Nama Lengkap" maxlength="50" />
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="form-group has-feedback">
                                                <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                                                <input id="member_birthday" date_type="date" validate="required[]" name="member_birthday" type="text" class="form-control line-input"  placeholder="Tanggal Lahir" readonly style="background-color: #fff;" />
                                            </div>
                                        </div>
                                        <!--</div>-->
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="row reset-margin">
                                                <input type="radio" name="gender_member" value="M" checked >&nbsp;Pria&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <input type="radio" name="gender_member" value="F" >&nbsp;Wanita
                                            </div>
                                        </div>
                                        <div class = "col-xs-6">
                                            <div class = "form-group has-feedback">
                                                <span class = "glyphicon glyphicon-earphone form-control-feedback"></span>
                                                <input id = "member_phone" validate = "required[]" name = "member_phone" type = "text" class = "form-control line-input" placeholder = "No. Telepon" data-mask data-inputmask = "'mask': ['999 999 999 999 999']"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class = "row">
                                        <div class = "col-xs-6">
                                            <div id = "captcha_img_member" ></div>
                                        </div>
                                        <div class = "col-xs-6">
                                            <div class = "form-group"><input id = "captcha_member" validate = "required[]" name = "captcha_member" type = "text" class = "form-control line-input" placeholder = "Kode Captcha" maxlength = "5"/></div>
                                        </div>
                                    </div>
                                    <div class = "row add-top-margin">
                                        <div class = "col-xs-12">
                                            <label>
                                                <input id = "member_requirement" name = "member_requirement" type = "checkbox" value = "1">
                                                &nbsp;Saya sudah membaca dan menyetujui
                                                <a href = "<?php echo base_url(); ?>info/syarat-dan-ketentuan"
                                                   target = "_blank">Syarat dan Ketentuan</a>, serta
                                                <a href = "<?php echo base_url(); ?>info/kebijakan-privasi"
                                                   target = "_blank">Kebijakan Privasi </a>
                                            </label>
                                        </div>
                                    </div>
                                    <br>
                                    <div class = "row">
                                        <div class = "col-xs-8 col-xs-offset-2">
                                            <button id = "btnmember" type = "submit" class = "btn btn-green btn-block btn-flat">DAFTAR</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>;
</div>
<!--MODAL FOR CART-->
<div class="modal fade" id="add_to_cart" tabindex="-1" role="dialog">
    <div class="container">
        <div class="row">
            <div class="mdl-cart">
                <div class="cst-close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i></div>
                <?php
                $styles = '';
                $total = 0;
                if (isset($_SESSION[SESSION_PRODUCT_INFO]) && sizeof($_SESSION[SESSION_PRODUCT_INFO]) > 0) {
                    $styles = 'style="display:none;"';
                    ?>
                    <div class="col-xs-8 table_fill">
                        <div class="box box-default">
                            <div class="box-header mt-10px">
                                <h3 class="box-title-cart">Keranjang Belanja
                                    <span class="pull-right"><b id="cart_qty_md"><?php echo isset($_SESSION[SESSION_PRODUCT_INFO]) && sizeof($_SESSION[SESSION_PRODUCT_INFO]) != 0 ? sizeof($_SESSION[SESSION_PRODUCT_INFO]) : "0"; ?></b> produk</span></h3>
                            </div>
                            <!-- /.box-header -->
                            <div id ="box-cart-product" class="box-body border-top">
                                <div id="table_product" class="mdl-cart-fix-height">
                                    <?php
                                    foreach ($_SESSION[SESSION_PRODUCT_INFO] as $key => $img) {
                                        ?>
                                        <table class="table no-border no-margin" id="row_<?php echo $img[$key]["product_seq"] ?>">
                                            <tr>
                                                <td width="90" rowspan="2" class="border-cart-bag">
                                                    <a href ="<?php echo base_url() . url_title($img[$key]["product_name"] . " " . $img[$key]["product_seq"]); ?>"><img class="pure-img" src= "<?php echo get_file_exists(PRODUCT_UPLOAD_IMAGE . $img[$key]["merchant_seq"] . "/" . S_IMAGE_UPLOAD . $img[$key]["img"]); ?>" alt="<?php echo $img[$key]["img"] ?>"></a>
                                                </td>
                                                <td colspan="4" class="border-cart-bag" >
                                                    <b><a href ="<?php echo base_url() . url_title($img[$key]["product_name"] . " " . $img[$key]["product_seq"]); ?>" class="font-merchant-black"><?php echo $img[$key]["product_name"] ?></a></b>
                                                    <div class="pull-right">
                                                        <button class="btn-transparent" href="#" onClick ="<?php echo "delete_product(" . $img[$key]["product_seq"] . ")" ?>"><i class='fa fa-remove text-red'></i></button>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p class="small"> Berat : <?php echo $img[$key]["product_weight"]; ?> Kg. </p>
                                                    <?php echo ($img[$key]["variant_name"] == "All Product" ? "" : "Warna : " . $img[$key]["variant_name"] ); ?>
                                                    <div class="relative">
                                                        <div class="store-merchant"></div>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <a href="<?php echo base_url() . "merchant/" . $img[$key]["merchant_code"]; ?>">&nbsp;<?php echo $img[$key]["merchant_name"]; ?></a>
                                                    </div>
                                                    <i class="fa fa-truck"></i>&nbsp;<?php echo $img[$key]["exp_name"]; ?>

                                                </td>

                                                <td width="150" class="cart-sellprice">
                                                    <div id="sell_price_<?php echo $img[$key]["product_seq"]; ?>" sell_price ="<?php echo $img[$key]["sell_price"]; ?>">
                                                        Rp. <?php echo number_format($img[$key]["sell_price"]); ?>
                                                    </div>
                                                </td>
                                                <td width="80">
                                                    <select name="product_qty"  id ="product_qty_<?php echo $img[$key]["product_seq"] ?>" onchange ="dropdown_qty(<?php echo $img[$key]["product_seq"]; ?>, this.id)">
                                                        <?php
                                                        for ($i = 1; $i <= $img[$key]["max_buy"]; $i++) {
                                                            ?>
                                                            <option value ="<?php echo $i ?>" <?php echo $i == $img[$key]["qty"] ? "selected" : "" ?>> <?php echo $i ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </td>
                                                <td width="150" class="cart-sellprice">
                                                    <div id="total_price_<?php echo $img[$key]["product_seq"]; ?>" total_price="<?php echo $img[$key]["sell_price"] * $img[$key]["qty"]; ?>">
                                                        Rp. <?php echo number_format($img[$key]["sell_price"] * $img[$key]["qty"]); ?>
                                                    </div>

                                                </td>
                                            </tr>
                                        </table>
                                        <?php
                                        $total = $total + $img[$key]["sell_price"] * $img[$key]["qty"];
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4 table_fill">
                        <div class="box box-default">
                            <div class="box-header with-border mt-10px">
                                <h3 class="box-title-cart">Ringkasan</h3>
                            </div>
                            <div class="box-body">
                                Total Belanja<br />
                                <div class="total_order">Rp. </div><price id="total_order"> <?php echo number_format($total); ?></price>
                                <div class="pt-10px"><a href="<?php echo get_base_url() ?>member/checkout?step=1" class="btn btn-green"><i class="fa fa-shopping-cart"></i> Proses Pembelian</a>
                                    <a href="#" class="btn btn-default" data-dismiss = "modal">Lanjutkan Belanja</a></div>
                            </div>
                        </div>
                        <div class="box box-default">
                            <div class="box-body">
                                <p>Total belanja belum termasuk biaya pengiriman. Biaya pengiriman akan diestimasikan saat anda menentukan alamat pengiriman.</p>
                                <p>Proses pembelian anda untuk mengisi detail pembelian.</p><hr>
                                <p class="text-navy">GRATIS biaya pengiriman khusus wilayah JABODETABEK</p>
                            </div>
                        </div>
                    </div>
                <?php } else {
                    ?>
                    <div class="col-xs-8 table_fill no-display">
                        <div class="box box-default">
                            <div class="box-header with-border">
                                <h3 class="box-title-cart">Keranjang Belanja
                                    <span class="pull-right"><b id="cart_qty_md"><?php echo isset($_SESSION[SESSION_PRODUCT_INFO]) && sizeof($_SESSION[SESSION_PRODUCT_INFO]) != 0 ? sizeof($_SESSION[SESSION_PRODUCT_INFO]) : "0"; ?></b> produk</span></h3>
                            </div>
                            <div class="box-body">
                                <div id="table_product" class="mdl-cart-fix-height"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4 table_fill no-display">
                        <div class="box box-default">
                            <div class="box-header with-border">
                                <h3 class="box-title-cart">Ringkasan</h3>
                            </div>
                            <div class="box-body">
                                Total Belanja<br />
                                <span class="total_order">Rp.</span><price id="total_order"> 0</price>
                                <div class="pt-10px"><a href="<?php echo get_base_url() ?>member/checkout?step=1" class="btn btn-green"><i class="fa fa-shopping-cart"></i> Proses Pembelian</a>
                                    <a href="#" class="btn btn-default" data-dismiss = "modal">Lanjutkan Belanja</a></div>
                            </div>
                        </div>
                        <div class="box box-default">
                            <div class="box-body">
                                <p>Total belanja belum termasuk biaya pengiriman. Biaya pengiriman akan diestimasikan saat anda menentukan alamat pengiriman.</p>
                                <p>Proses pembelian anda untuk mengisi detail pembelian.</p><hr>
                                <p class="text-navy">GRATIS biaya pengiriman khusus wilayah JABODETABEK</p>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <!--JIKA KANTONG BELANJA KOSONG-->
                <div class="col-xs-12" id="table_empty" <?php echo $styles; ?>>
                    <div class="well cart-empty">
                        <div class="row">
                            <div class="col-xs-5 col-xs-offset-3">
                                <div class="empty-content" id ="no_product">
                                    <p class='min-text'>Kantong belanja anda kosong</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--END KANTONG BELANJA KOSONG-->
                <div class="clearfix"></div>
            </div>

        </div>
    </div>
</div>
<!--MODAL FOR REGISTER MERCHANT-->
<div class = "modal fade" id = "modal_register_merchant" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel">
    <div class = "modal-dialog modal-md" role = "dialog">
        <div class = "mdl-merchant-register">

            <div class = "mdl-content">
                <div class = "header">
                    Daftar Merchant
                </div>
                <div class = "cst-close" data-dismiss = "modal" aria-label = "Close"><i class = "fa fa-times"></i></div>
                <p class = "col-xs-12" id = "content-message_merchant"></p>
                <div class = "row add-top-margin">
                    <div class = "col-xs-12">
                        <form id = "frmSignUpMerchant" method = "post" action = "<?php echo get_base_url() ?>merchant/sign_up/register_merchant">
                            <div class = "row">
                                <div class = "col-xs-12">
                                    <div class = "form-group has-feedback">
                                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                        <input validate = "email[]" type = "text" id = "email_merchant" class = "form-control line-input" placeholder = "Email" name = "email_merchant" />
                                    </div>
                                </div>
                            </div>
                            <div class = "row">
                                <div class = "col-xs-12">
                                    <div class = "form-group has-feedback">
                                        <span class="glyphicon glyphicon-home form-control-feedback"></span>
                                        <input id = "name_merchant" validate = "required[]" type = "text" class = "form-control line-input" placeholder = "Nama Merchant" name = "merchant_name" />
                                    </div>
                                </div>
                            </div>
                            <div class = "row">
                                <div class = "col-xs-12">
                                    <div class = "form-group has-feedback">
                                        <span class="glyphicon glyphicon-phone form-control-feedback"></span>
                                        <input id = "phone_merchant" validate = "required[]" type = "text" class = "form-control line-input" placeholder = "No. Telepon" name = "phone_merchant" data-mask data-inputmask = "'mask': ['999 999 999 999 999']"/>
                                    </div>
                                </div>
                            </div>
                            <div class = "row">
                                <div class = "col-xs-12">
                                    <div class = "form-group">
                                        <textarea id = "merchant_address" validate = "required[]" class = "form-control line-input" name = "merchant_address" placeholder = "Alamat Merchant" rows = "3" style = "overflow:auto;resize:none"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class = "row">
                                <div class = "col-xs-6">
                                    <div id = "captcha_img_merchant" ></div>
                                </div>
                                <div class = "col-xs-6">
                                    <input id = "code_captcha_merchant" validate = "required[]" name = "captcha_merchant" type = "text" class = "form-control line-input" placeholder = "Kode Captcha" maxlength = "5"/>
                                </div>
                            </div>

                            <div class = "row">
                                <div class = "col-xs-8 col-xs-offset-2"><br/>
                                    <button id = "btnMSignUp" type = "submit" name = "submit" class = "btn btn-block btn-green" ><i class = "fa fa-user"></i> Daftar </button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script text = "javascirpt">
    function delete_product(product_seq) {
        $.ajax({
            url: "<?php echo base_url("member/main_page"); ?>",
            type: "post",
            data: {
                "type": "<?php echo TASK_TYPE_DELETE ?>",
                "product_seq": product_seq
            },
            success: function (data) {
                if (isSessionExpired(data)) {
                    response_object = json_decode(data);
                    url = response_object.url;
                    location.href = url;
                } else {
                    $('#row_' + product_seq).remove();
                    calculate_total();
                }
            },
            error: function (request, error) {
                alert(error_response(request.status));
            }
        });
    }

    function calculate_total() {
        var grand_total = 0;
        var qty_product = 0;
        $('div').each(function () {
            var total = $(this).attr("total_price");
            if (total != undefined) {
                grand_total += parseInt(total);
                qty_product++;
            }
        });
        if (qty_product == 0) {
            $('.table_fill').hide();
            $('#table_empty').show();
            qty_product = "0";
        } else {
            $('#table_empty').hide();
            $('.table_fill').show();
        }

        $('#total_order').text(format(grand_total));
        $('#cart_qty_md').text(qty_product);
        $('#cart_qty_xs').text(qty_product);
    }

    function format(num) {
        var n = num.toString(), p = n.indexOf('.');
        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
            return p < 0 || i < p ? ($0 + ',') : $0;
        });
    }
    function dropdown_qty(product_seq, id) {
        var sell_price = $('#sell_price_' + product_seq).attr("sell_price");
        var qty = $('#' + id).val();
        if (id.substr(id.length - 2) == 'xs') {
            $('#' + id.substr(0, id.length - 3)).val(qty);
        } else {
            $('#' + id + '_xs').val(qty);
        }

        var total_price = parseInt(sell_price) * parseInt(qty);
        $.ajax({
            url: "<?php echo base_url("member/main_page"); ?>",
            type: "POST",
            data: {
                "type": "<?php echo TASK_TYPE_ADD_QTY ?>",
                "product_seq": product_seq,
                "qty_product": qty
            },
            success: function (response) {
                if (isSessionExpired(response)) {
                    response_object = json_decode(response);
                    url = response_object.url;
                    location.href = url;
                } else {
                    $('#total_price_' + product_seq).attr("total_price", total_price);
                    $('#total_price_' + product_seq).text("Rp. " + format(total_price));
                    calculate_total();
                }
            },
            error: function (request, error) {
                alert(error_response(request.status));
            }
        });
    }

    function gp_login()
    {
        var myParams = {
            'clientid': "<?php echo GOOGLE_API ?>",
            'cookiepolicy': 'single_host_origin',
            'callback': 'loginCallback',
            'approvalprompt': 'force',
            'scope': 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email'
        };
        gapi.auth.signIn(myParams);
    }

    function loginCallback(result)
    {
        if (result['status']['signed_in'])
        {
            gapi.client.load('plus', 'v1', function () {

                var request = gapi.client.plus.people.get(
                        {
                            'userId': 'me'
                        });
                request.execute(function (resp)
                {
                    var email = '';
                    if (resp['emails'])
                    {
                        for (i = 0;
                                i < resp['emails'].length;
                                i++)
                        {
                            if (resp['emails'][i]['type'] == 'account')
                            {
                                email = resp['emails'][i]['value'];
                            }
                        }
                    }

                    var base_url = '<?php echo base_url(); ?>'
                    $.ajax({
                        url: "<?php echo base_url() ?>member/login_fb",
                        type: "post",
                        data: {
                            "name": resp['displayName'],
                            "email": email,
                            "gender": resp['gender'],
                            "type": "google_login"
                        },
                        success: function (data) {
                            window.location = "<?php echo base_url(); ?>";
                        },
                        error: function (request, error) {
                            alert(error_response(request.status));
                        }
                    });
                });
            })
        }


    }
    function onLoadCallback()
    {
        gapi.client.load('plus', 'v1', function () {
        });
    }

    window.fbAsyncInit = function () {
        FB.init({
            appId: "<?php echo FACEBOOK_API ?>",
//            appId: "421079861618141",
            status: true,
            cookie: true,
            xfbml: true
        });
        FB.Event.subscribe('auth.authResponseChange', function (response)
        {
            if ($('#message').length > 0) {
                if (response.status === 'connected')
                {
                    document.getElementById("message").innerHTML += "<br>Connected to Facebook";
                } else if (response.status === 'not_authorized')
                {
                    document.getElementById("message").innerHTML += "<br>Failed to Connect";
                } else
                {
                    document.getElementById("message").innerHTML += "<br>Logged Out";
                }
            }
        });
    };
    function fb_login() {
        FB.login(function (response) {
            if (response.authResponse)
            {
                _i();
            } else
            {
                console.log('Batal masuk Facebook, tidak ada otoritas !');
            }
        }, {scope: 'email'});
    }

    function _i() {
        FB.api('/me?fields=name,email,gender,birthday', function (data) {

            var base_url = '<?php echo base_url(); ?>'
            $.ajax({
                url: "<?php echo base_url() ?>member/login_fb",
                type: "post",
                dataType: "json",
                data: {
                    "name": data.name,
                    "email": data.email,
                    "gender": data.gender,
                    "birthday": data.birthday,
                    "type": "facebook_login"
                },
                success: function (data) {
                    if (data.error == true) {
                        alert(data.message);
                        return false;
                    } else {
                        window.location = "<?php echo base_url(); ?>";
                    }
                },
                error: function (request, error) {
                    alert(error_response(request.status));
                }
            });
        })
    }
    function imageExists(image_url) {
        var http = new XMLHttpRequest();
        http.open('HEAD', image_url, false);
        http.send();
        return http.status != 404;
    }
</script>
