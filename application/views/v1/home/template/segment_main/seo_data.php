<title><?php echo (isset($title) ? $title : DEFAULT_TITLE); ?></title>
<link rel="author" href="https://plus.google.com/104986352936735532918"/>
<link rel="alternate" media="only screen and (maxwidth: 640px)" href="https://m.toko1001.id/" />
<meta name="description" content="<?php echo (isset($description) ? htmlentities($description) : htmlentities(DEFAULT_DESCRIPTION) ); ?>">
<meta name="keyword" content="<?php echo (isset($keyword) ? htmlentities($keyword) : htmlentities(DEFAULT_KEYWORD) ); ?>">
<meta name="geo.placename" content="Indonesia"/>
<meta name="geo.country" content="id"/>
<meta name="geo.region" content="ID-TMG"/>
<meta name="copyright" content="Toko1001.id"/>
<meta name="language" content="id"/>
<meta name="distribution" content="global"/>
<meta name="audience" content="all"/>
<link rel="alternate" type="application/rss+xml" title="<?php echo (isset($title) ? $title : DEFAULT_TITLE); ?>" href="https://toko1001.id/feed/rss" />
<?php if (isset($is_detail)): ?>
    <meta name="robots" content="index, nofollow">
<?php else: ?>
    <meta name="robots" content="index, follow" />
<?php endif; ?>
<?php //echo $_add_header_; ?>
<?php
//  Canonical

if (isset($this->pagination) && $_add_header_ == "") {

    function getProtectedValue($obj, $name) {
        $array = (array) $obj;
        $prefix = chr(0) . '*' . chr(0);
        return $array[$prefix . $name];
    }

    $canonicaldata = $this->pagination;
    $total_rows = getProtectedValue($canonicaldata, 'total_rows');
    echo '<link rel="canonical" href="' . current_url_with_query_string() . '" />' . chr(10);
    if ($canonicaldata->cur_page > 0) {
        if ($canonicaldata->cur_page == 1) {
            if ($total_rows > $canonicaldata->per_page)
                echo '<link rel="next" href="' . current_url() . '?mulai=' . ($canonicaldata->cur_page * $canonicaldata->per_page) . '" />' . chr(10);
        } else {
            if ($canonicaldata->cur_page == 2) {
                echo '<link rel="prev" href="' . current_url() . '" />' . chr(10);
            } else {
                echo '<link rel="prev" href="' . current_url() . '?mulai=' . ceil($total_rows / ($canonicaldata->cur_page - 1)) . '" />' . chr(10);
            }
            if (ceil($total_rows / $canonicaldata->per_page) != $canonicaldata->cur_page) {
                if (($canonicaldata->cur_page * $canonicaldata->per_page) > 0)
                    echo '<link rel="next" href="' . current_url() . '?mulai=' . ($canonicaldata->cur_page * $canonicaldata->per_page) . '" />' . chr(10);
            }
        }
    }
} else {
    echo '<link rel="canonical" href="' . current_url_with_query_string() . '" />' . chr(10);
}
//  Facebook Open Graph
// <meta content='xxx' property='fb:app_id'/>
?>
<meta property='fb:admins' content='100008738526458' />
<meta property='og:title' expr:content='<?php echo (isset($title) ? $title : DEFAULT_TITLE); ?>' />
<meta property='og:description' content='<?php echo (isset($description) ? htmlentities($description) : htmlentities(DEFAULT_DESCRIPTION) ); ?>' />
<meta property='og:url' expr:content='<?php echo current_url_with_query_string(); ?>' />
<meta property='og:type' content='<?php echo (isset($description) ? "product" : "website" ); ?>' />
<meta property='og:locale' content='id_ID' />
<?php
if (isset($detail_description))
    $seo_image = base_url() . PRODUCT_UPLOAD_IMAGE . $detail_description->merchant_seq . "/" . XL_IMAGE_UPLOAD . $detail_description->{"image1"};
echo (isset($seo_image) ? "<meta property='og:image' content='" . $seo_image . "' />" : "");
?>
<?php //  Twitter Card data  ?>
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@toko1001_id">
<meta name="twitter:title" content="<?php echo (isset($title) ? $title : DEFAULT_TITLE); ?>">
<meta name="twitter:description" content="<?php echo (isset($description) ? htmlentities($description) : htmlentities(DEFAULT_DESCRIPTION) ); ?>">
<meta name="twitter:creator" content="@toko1001_id">
<?php echo (isset($seo_image) ? "<meta name='twitter:image' content='$seo_image'>" : ""); ?>
<?php //   Schema.org markup for Google+                  ?>
<meta itemprop="name" content="<?php echo (isset($title) ? $title : DEFAULT_TITLE); ?>">
<meta itemprop="description" content="<?php echo (isset($description) ? htmlentities($description) : htmlentities(DEFAULT_DESCRIPTION) ); ?>">
<?php // echo (isset($seo_image) ? "<meta itemprop='image' content='$seo_image'>" : "");           ?>
<link rel="icon" href="<?php echo get_img_url(); ?>icon/pavicon.png"/>
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?php echo get_img_url(); ?>icons/mstile-144x144.png">
<meta name="theme-color" content="#ffffff">
<?php
//  echo '<meta name="msapplication-config" content="' . get_img_url() . 'icons/browserconfig.xml" />';
//  Search Box
?>
<script type="application/ld+json">
    {
    "@context": "http://www.schema.org","@type": "LocalBusiness","name": "<?php echo DEFAULT_NAME; ?>","url": "https://toko1001.id",  "sameAs" : ["https://www.facebook.com/toko1001.id","https://twitter.com/toko1001_id","https://instagram.com/toko1001.id"],"logo": "https://toko1001.id/assets/img/home/522X120_LOGO_PUTIH.png","image": "https://toko1001.id/assets/img/home/522X120_LOGO_PUTIH.png","description": "<?php echo str_replace('"', '', DEFAULT_DESCRIPTION); ?>","address": {"@type": "PostalAddress","streetAddress": "Jl. Indokarya Barat I Blok D No. 1 Papanggo, Sunter Jakarta Utara","addressLocality": "Sunter","addressRegion": "DKI Jakarta","postalCode": "14330","addressCountry": "Indonesia"},"geo": {"@type": "GeoCoordinates","latitude": "-6.131609","longitude": "106.872022"},"hasMap": "https://goo.gl/maps/WxGkZT9QRE12","contactPoint": {"@type": "ContactPoint","telephone": "+6221-6514224","contactType" : "customer service"}
    }
</script>
<script type="application/ld+json">
    {
    "@context": "http://schema.org","@type": "WebSite","url": "https://toko1001.id/","potentialAction": {"@type": "SearchAction","target": "https://toko1001.id/kategori?e={e}","query-input": "required name=e"}
    }
</script>
