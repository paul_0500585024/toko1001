<div id="background-category-menu">
</div>

<ul class="hover_menu" id='test'>
    <?php if (isset($tree_category)): ?>
        <li id="mega-menu" class="trapScroll" style="z-index:998;">
            <a href="#">
                <div id="drop-menu" class="class-kategori pull-left">
                    <i class="fa fa-bars"></i>&nbsp; <b>KATEGORI</b>
                </div>
            </a>
            <?php echo get_menu_3level($tree_category, 0); ?>       
        </li>		
        <?php echo get_discount(); ?>
        <li><a style="background-color: #26a69a; color: white !important" href= "<?php echo base_url('berkah_ramadhan') ?>">1001 Berkah</a></li>			
        <?php echo get_menu_level1_top_n($tree_category, 0); ?>        
        <li><a href= "<?php echo base_url("pulsa") ?>">Pulsa</a></li>
        
    <?php endif; ?>
</ul>
<script>

    $('#mega-menu').mouseenter(function () {
        $('#background-category-menu').fadeIn(10);
    });

    $('#mega-menu').mouseleave(function () {
        $('#background-category-menu').hide(10);
    })

    $('.subMegaMenu').bind('mousewheel DOMMouseScroll', function (e) {
        var scrollTo = null;

        if (e.type == 'mousewheel') {
            scrollTo = (e.originalEvent.wheelDelta * -1);
        }
        else if (e.type == 'DOMMouseScroll') {
            scrollTo = e.originalEvent.detail;
        }

        if (scrollTo) {
            e.preventDefault();
            $(this).scrollTop(scrollTo + $(this).scrollTop());
        }
    });


    $(document).scroll(function () {
        var position = $(window).scrollTop();
        if (position > 30) {
            $('#mega-menu').addClass("mega-menu");
            $('#mega-menu').addClass('mt-15px');
            $('#drop-menu').addClass("drop-menu");
            $('#brand').addClass("no-show");
            $('#drop-menu').html('<div class=""><i class="fa fa-bars link"></i>&nbsp;&nbsp;&nbsp;&nbsp;<div class="brand-mini-logo"></div>');
        } else {
            $('#mega-menu').removeClass('mega-menu');
            $('#mega-menu').removeClass('mt-15px');
            $('#drop-menu').removeClass('drop-menu');
            $('#brand').addClass("no-show");
            $('#brand').removeClass("no-show");
            $('#brand').css('display', 'none')
            $('#brand').slideDown('fast');
            $('#drop-menu').html('<i class="fa fa-bars"></i>&nbsp;KATEGORI');
        }
    })
    $('.menu li').mouseenter(function () {
        var mnActive = $(this).attr('class');
        $('#' + mnActive + '-active').addClass('mn-active');
        $('.' + mnActive).siblings().css('opacity', '.5');
    })
    $('.menu li').mouseleave(function () {
        var mnActive = $(this).attr('class');
        $('#' + mnActive + '-active').removeClass('mn-active');
        $('.' + mnActive).siblings().css('opacity', '1');
    })
</script>