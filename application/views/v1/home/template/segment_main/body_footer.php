<div class="clearfix"></div>
<br><br>
<footer class="footer">
    <div class="container">
        <div class="col-xs-4 no-pad">
            <div class="col-xs-12 no-pad"><b>Layanan Pelanggan</b></div>
            <div class="col-xs-6 no-pad m-top10">
                <a class="wsh" href="<?php echo base_url(); ?>info/panduan-belanja">Panduan Belanja</a><br>
                <a class="wsh" href="<?php echo base_url(); ?>info/pengembalian-barang">Pengembalian Barang</a><br>
                <a class="wsh" href="<?php echo base_url(); ?>info/pengembalian-dana">Pengembalian Dana</a><br>
            </div>
            <div class="col-xs-6 no-pad m-top10">
                <a class="wsh" href="<?php echo base_url(); ?>info/kebijakan-privasi">Kebijakan Privasi</a><br>
                <a class="wsh" href="<?php echo base_url(); ?>info/syarat-dan-ketentuan">Syarat Dan Ketentuan</a><br>
                <a class="wsh" href="<?php echo base_url(); ?>info/syarat-dan-ketentuan-penggunaan-voucher">Penggunaan Voucher</a><br>
            </div>
        </div>

        <div class="col-xs-3 no-pad">
            <div class="col-xs-12 no-pad">
                <b>Customer Care</b>
            </div>
           <div class="col-xs-12 no-pad m-top10 add-line-height30">
                <i class="fa fa-calendar"></i>&nbspSenin - Jumat <i class="fa fa-clock-o"></i>&nbsp;09.00-17.00 WIB<br>
                <i class="fa fa-calendar"></i>&nbspSabtu <i class="fa fa-clock-o"></i>&nbsp;09.00-14.00 WIB<br>
                <i class="fa fa-phone"></i>&nbsp;+6221 651 4224&nbsp;&nbsp;<i class="fa fa-phone"></i>&nbsp;+6221 651 4225<br>
                <i class="fa fa-envelope"></i>&nbsp;cc@toko1001.id
            </div>
        </div>
        <div class="col-xs-2 no-pad">
            <div class="col-xs-12 no-pad m-top40">
                <div id="tk_1001_idEA_trustmark" class="tk-1001-idEA-trustmark-overide-main">
                    <script type="text/javascript" src="https://www.idea.or.id/assets/js/trustmark_badge.js"></script><link rel="stylesheet" href="https://www.idea.or.id/assets/css/trustmark_badge.css" /><input type="hidden" id="hid_trustmark_code" value="oz5e97xTfYsnWlIDQ4ObhwiF"/><input type="hidden" id="hid_base_url" value="https://www.idea.or.id/" /><input type="hidden" id="hid_alt" value="no"/><div id="idEA_trustmark"><div id="idEA_trustmark_wrapper"><img id="idEA_trustmark_image" width="105px" /><div id="idEA_trustmark_verified">Verifikasi Hingga</div><div id="idEA_trustmark_until"></div></div></div><!-- Trustmark idEA End -->
                </div>
            </div>
        </div>
        <div class="col-xs-3 footer-merchant-1_v2">
            <div class="col-xs-12 no-pad text-center"><h4 class="footer-merchant-h4">JADI MERCHANT KAMI</h4></div>
            <div class="col-xs-12 no-pad text-justify">Tanpa biaya iuran tahunan ataupun biaya tambahan pemeliharaan, hanya fee transaksi yang kompetitif (komisi).</p></div>
            <div class="col-xs-12 no-pad">
                <a href="#" class="btn btn-green btn-flat btn-block" data-toggle="modal" data-target="#modal_register_merchant" id="register_merchant" data-id="1">Daftar Merchant</a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="footer-body-line-2">
            <div class="col-xs-5 no-pad">
                <div class="col-xs-12 no-pad">
                    <b>Metode Pembayaran</b>
                </div>
                <div class="col-xs-12 no-pad m-top10">
                    <span class="tk_icon visa pos1"></span>
                    <span class="tk_icon master-card pos2"></span>
                    <span class="tk_icon jcb pos3"></span>
                    <span class="tk_icon bca pos4"></span>
                    <span class="tk_icon mandiri pos5"></span>
                    <span class="tk_icon mandiri_ck pos6"></span>
                    <span class="tk_icon qr pos7"></span>
                </div>
            </div>
            <div class="col-xs-3 no-pad">
                <div class="col-xs-12 no-pad">
                    <b>Keamanan Belanja</b>
                </div>
                <div class="col-xs-12 no-pad m-top10">
                    <span class="tk_icon comodo pos1"></span>
                    <span class="tk_icon v_visa pos2"></span>
                    <span class="tk_icon mc_sourcecode pos3"></span>
                    <span class="tk_icon j-secure pos4"></span>
                </div>
            </div>
            <div class="col-xs-2 no-pad">
                <div class="col-xs-12 no-pad">
                    <b>Jasa Pengiriman</b>
                </div>
                <div class="col-xs-12 no-pad m-top10">
                    <span class="tk_icon raja_kirim pos1"></span>
                    <span class="tk_icon jne pos2"></span>
                </div>
            </div>
            <div class="col-xs-2 no-pad">
                <div class="col-xs-12 no-pad">
                    <b>Ikuti Kami</b>
                </div>
                <div class="col-xs-12 m-top15">
                    <a href="https://instagram.com/toko1001.id">
                        <div class="img-icon f-instagram"></div>
                    </a>&nbsp; &nbsp; &nbsp;
                    <a href="https://www.facebook.com/toko1001.id">
                        <div class="img-icon f-facebook"></div>
                    </a>&nbsp; &nbsp; &nbsp;
                    <a href="https://twitter.com/toko1001_id">
                        <div class="img-icon f-twitter"></div>
                    </a>
                </div>
            </div>

            <style>
                #idEA_trustmark_wrapper{
                    border: solid 1px #ddd;
                    border-radius: 0px !important;
                    box-shadow: none !important;
                    background-color: #F2F2F2;
                    padding: 5px
                }
            </style>

        </div>
    </div>
    <div class="container">
        <p class="text-justify">
            TOKO1001 merupakan salah satu pusat belanja "online", yang menawarkan pengalaman belanja online dengan konsep aman, cepat dan nyaman. Kami menyediakan beragam produk, mulai dari fashion, handphone & tablet, kesehatan & kecantikan, hobby & jam tangan, kamera, computer, elektronik & audio, peralatan rumah tangga, mainan anak-anak dan bayi, tas & koper, olahraga & music, otomotif, makanan & minuman, buku & alat tulis.
            Belanja online di TOKO1001 sangat mudah!!! Tetap terhubung dan dapatkan penawaran terbaru dan transaksi setiap harinya. Kami menawarkan beragam pilihan produk-produk berkualitas, yang dapat ditemukan melalui ujung jari Anda. Dengan navigasi pencarian yang sederhana namun efektif, daftar koleksi, dan harga yang menarik, Kami memberikan kemudahan untuk Anda menemukan produk sesuai kebutuhan belanja Anda.
            Jadikanlah momen belanja online Anda menjadi kegiatan yang menyenangkan dengan TOKO1001. Terima kasih atas kunjungan Anda dan nikmati sensasi berbelanja online di TOKO1001, dimana Anda dapat mencari yang terbaik hanya dengan beberapa klik saja. Selamat berbelanja online!!!
        </p>
    </div>
    <div class="well well-footer reset-margin no-border-radius">
        <div class="container ">
            <span>&copy; 2015 - <?php echo date('Y'); ?>&nbsp;PT. Deltamas Mandiri Sejahtera</span>
        </div>
    </div>
</footer>