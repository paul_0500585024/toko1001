<style>
    .cst-breadcrumbs{
        list-style: none;
        padding-left:350px 
    }

    .cst-breadcrumbs li{
        float: left;
        background: #D6D6D6;
        padding: 5px;
        font-size: 1em;
        border: 1px solid #fff;
        margin: 5px;
    }
    .text-color-white{
        color: #000 !important
    }
    .active {
        background: #EFEFEF  !important;
    }
    a:hover{
        background: none !important;
        background-color: transparent !important;
    }
</style>

<nav class="navbar navbar-inverse navbar-fixed-top top-menu" role="navigation" id="topnavbar">
    <div class="container">
        <div class='col-xs-2 pos-relative'>
            <div class="navbar-header">
                <a class="navbar-brand" href="<?php echo base_url() ?>"><img src="<?php echo get_img_url(); ?>522X120_LOGO_PUTIH.png" alt="toko1001.id" ></a>
            </div>
        </div>
        <div class="col-xs-10">
            <ul class='nav cst-breadcrumbs'>
                <li id="li-info" <?php echo $data_step == TASK_FIRST_STEP ? "class=\"active\"" : "" ?> ><a class="text-color-white" href="<?php echo get_base_url() . "member/checkout?step=1" ?>" >1. Informasi Penerima</a></li>
                <li id="li-metode" <?php echo $data_step == TASK_SECOND_STEP ? "class=\"active\"" : "" ?> ><a class="text-color-white" href="<?php echo get_base_url() . "member/checkout?step=2" ?>" >2. Metode Pembayaran</a></li>
                <li id="li-detil" <?php echo $data_step == TASK_FINAL_STEP ? "class=\"active\"" : "" ?> ><a class="text-color-white" href="<?php echo get_base_url() . "member/checkout?step=3" ?>" >3. Ringkasan  Belanja</a></li>
            </ul>
        </div>
    </div>
</nav>
