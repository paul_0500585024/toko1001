<title>Agen <?php echo (isset($title) ? $title : DEFAULT_TITLE); ?></title>
<link rel="alternate" media="only screen and (maxwidth: 640px)" href="https://m.toko1001.id/agent" />
<meta name="description" content="<?php echo (isset($description) ? htmlentities($description) : htmlentities(DEFAULT_DESCRIPTION) ); ?>">
<meta name="keyword" content="<?php echo (isset($keyword) ? htmlentities($keyword) : htmlentities(DEFAULT_KEYWORD) ); ?>">
<link rel="icon" href="<?php echo get_img_url(); ?>home/icon/pavicon.png"/>