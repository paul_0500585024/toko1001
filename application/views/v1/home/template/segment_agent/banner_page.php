<script type="text/javascript">
    function showhidefrm() {
        $("#frm_login").modal('hide');
        $("#frm_register").modal('show');
    }
</script>
<!--MODAL FOR CART-->
<div class="modal fade" id="add_to_cart" tabindex="-1" role="dialog">
    <div class="container">
        <div class="row">
            <div class="mdl-cart">
                <div class="cst-close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i></div>
                <?php
                $styles = '';
                $total = 0;
                if (isset($_SESSION[SESSION_PRODUCT_INFO]) && sizeof($_SESSION[SESSION_PRODUCT_INFO]) > 0) {
                    $styles = 'style="display:none;"';
                    ?>
                    <div class="col-xs-8 table_fill">
                        <div class="box box-default">
                            <div class="box-header mt-10px">
                                <h3 class="box-title-cart">Keranjang Belanja
                                    <span class="pull-right"><b id="cart_qty_md"><?php echo isset($_SESSION[SESSION_PRODUCT_INFO]) && sizeof($_SESSION[SESSION_PRODUCT_INFO]) != 0 ? sizeof($_SESSION[SESSION_PRODUCT_INFO]) : "0"; ?></b> produk</span></h3>
                            </div>
                            <!-- /.box-header -->
                            <div id ="box-cart-product" class="box-body border-top">
                                <div id="table_product" class="mdl-cart-fix-height">
                                    <?php
                                    foreach ($_SESSION[SESSION_PRODUCT_INFO] as $key => $img) {
                                        ?>
                                        <table class="table no-border no-margin" id="row_<?php echo $img[$key]["product_seq"] ?>">
                                            <tr>
                                                <td width="90" rowspan="2" class="border-cart-bag">
                                                    <a href ="<?php echo base_url() . url_title($img[$key]["product_name"] . " " . $img[$key]["product_seq"]); ?>"><img class="pure-img" src= "<?php echo get_file_exists(PRODUCT_UPLOAD_IMAGE . $img[$key]["merchant_seq"] . "/" . S_IMAGE_UPLOAD . $img[$key]["img"]); ?>" alt="<?php echo $img[$key]["img"] ?>"></a>
                                                </td>
                                                <td colspan="4" class="border-cart-bag" >
                                                    <b><a href ="<?php echo base_url() . url_title($img[$key]["product_name"] . " " . $img[$key]["product_seq"]); ?>" class="font-merchant-black"><?php echo $img[$key]["product_name"] ?></a></b>
                                                    <div class="pull-right">
                                                        <button class="btn-transparent" href="#" onClick ="<?php echo "delete_product(" . $img[$key]["product_seq"] . ")" ?>"><i class='fa fa-remove text-red'></i></button>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p class="small"> Berat : <?php echo $img[$key]["product_weight"]; ?> Kg. </p>
                                                    <?php echo ($img[$key]["variant_name"] == "All Product" ? "" : "Warna : " . $img[$key]["variant_name"] ); ?>
                                                    <div class="relative">
                                                        <div class="store-merchant"></div>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <a href="<?php echo base_url() . "merchant/" . $img[$key]["merchant_code"]; ?>">&nbsp;<?php echo $img[$key]["merchant_name"] . "namamerchant"; ?></a>
                                                    </div>
                                                    <i class="fa fa-truck"></i>&nbsp;<?php echo $img[$key]["exp_name"]; ?>

                                                </td>

                                                <td width="150" class="cart-sellprice">
                                                    <div id="sell_price_<?php echo $img[$key]["product_seq"]; ?>" sell_price ="<?php echo $img[$key]["sell_price"]; ?>">
                                                        Rp. <?php echo number_format($img[$key]["sell_price"]); ?>
                                                    </div>
                                                </td>
                                                <td width="80">
                                                    <select name="product_qty"  id ="product_qty_<?php echo $img[$key]["product_seq"] ?>" onchange ="dropdown_qty(<?php echo $img[$key]["product_seq"]; ?>, this.id)">
                                                        <?php
                                                        for ($i = 1; $i <= $img[$key]["max_buy"]; $i++) {
                                                            ?>
                                                            <option value ="<?php echo $i ?>" <?php echo $i == $img[$key]["qty"] ? "selected" : "" ?>> <?php echo $i ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </td>
                                                <td width="150" class="cart-sellprice">
                                                    <div id="total_price_<?php echo $img[$key]["product_seq"]; ?>" total_price="<?php echo $img[$key]["sell_price"] * $img[$key]["qty"]; ?>">
                                                        Rp. <?php echo number_format($img[$key]["sell_price"] * $img[$key]["qty"]); ?>
                                                    </div>

                                                </td>
                                            </tr>
                                        </table>
                                        <?php
                                        $total = $total + $img[$key]["sell_price"] * $img[$key]["qty"];
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4 table_fill">
                        <div class="box box-default">
                            <div class="box-header with-border mt-10px">
                                <h3 class="box-title-cart">Ringkasan</h3>
                            </div>
                            <div class="box-body">
                                Total Belanja<br />
                                <div class="total_order">Rp. </div><price id="total_order"> <?php echo number_format($total); ?></price>
                                <div class="pt-10px"><a href="<?php echo get_base_url() ?>member/checkout?step=1" class="btn btn-green"><i class="fa fa-shopping-cart"></i> Proses Pembelian</a>
                                    <a href="#" class="btn btn-default" data-dismiss = "modal">Lanjutkan Belanja</a></div>
                            </div>
                        </div>
                        <div class="box box-default">
                            <div class="box-body">
                                <p>Total belanja belum termasuk biaya pengiriman. Biaya pengiriman akan diestimasikan saat anda menentukan alamat pengiriman.</p>
                                <p>Proses pembelian anda untuk mengisi detail pembelian.</p><hr>
                                <p class="text-navy">GRATIS biaya pengiriman khusus wilayah JABODETABEK</p>
                            </div>
                        </div>
                    </div>
                <?php } else {
                    ?>
                    <div class="col-xs-8 table_fill no-display">
                        <div class="box box-default">
                            <div class="box-header with-border">
                                <h3 class="box-title-cart">Keranjang Belanja
                                    <span class="pull-right"><b id="cart_qty_md"><?php echo isset($_SESSION[SESSION_PRODUCT_INFO]) && sizeof($_SESSION[SESSION_PRODUCT_INFO]) != 0 ? sizeof($_SESSION[SESSION_PRODUCT_INFO]) : "0"; ?></b> produk</span></h3>
                            </div>
                            <div class="box-body">
                                <div id="table_product" class="mdl-cart-fix-height"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4 table_fill no-display">
                        <div class="box box-default">
                            <div class="box-header with-border">
                                <h3 class="box-title-cart">Ringkasan</h3>
                            </div>
                            <div class="box-body">
                                Total Belanja<br />
                                <span class="total_order">Rp.</span><price id="total_order"> 0</price>
                                <div class="pt-10px"><a href="<?php echo get_base_url(); ?>checkout?step=1" class="btn btn-green"><i class="fa fa-shopping-cart"></i> Proses Pembelian</a>
                                    <a href="#" class="btn btn-default" data-dismiss = "modal">Lanjutkan Belanja</a></div>
                            </div>
                        </div>
                        <div class="box box-default">
                            <div class="box-body">
                                <p>Total belanja belum termasuk biaya pengiriman. Biaya pengiriman akan diestimasikan saat anda menentukan alamat pengiriman.</p>
                                <p>Proses pembelian anda untuk mengisi detail pembelian.</p><hr>
                                <p class="text-navy">GRATIS biaya pengiriman khusus wilayah JABODETABEK</p>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <!--JIKA KANTONG BELANJA KOSONG-->
                <div class="col-xs-12" id="table_empty" <?php echo $styles; ?>>
                    <div class="well cart-empty">
                        <div class="row">
                            <div class="col-xs-5 col-xs-offset-3">
                                <div class="empty-content" id ="no_product">
                                    <p class='min-text'>Kantong belanja anda kosong</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--END KANTONG BELANJA KOSONG-->
                <div class="clearfix"></div>
            </div>

        </div>
    </div>
</div>
<script text = "javascirpt">
    function delete_product(product_seq) {
        $.ajax({
            url: "<?php echo base_url("member/main_page"); ?>",
            type: "post",
            data: {
                "type": "<?php echo TASK_TYPE_DELETE ?>",
                "product_seq": product_seq
            },
            success: function(data) {
                if (isSessionExpired(data)) {
                    response_object = json_decode(data);
                    url = response_object.url;
                    location.href = url;
                } else {
                    $('#row_' + product_seq).remove();
                    calculate_total();
                }
            },
            error: function(request, error) {
                alert(error_response(request.status));
            }
        });
    }

    function calculate_total() {
        var grand_total = 0;
        var qty_product = 0;
        $('div').each(function() {
            var total = $(this).attr("total_price");
            if (total != undefined) {
                grand_total += parseInt(total);
                qty_product++;
            }
        });
        if (qty_product == 0) {
            $('.table_fill').hide();
            $('#table_empty').show();
            qty_product = "0";
        } else {
            $('#table_empty').hide();
            $('.table_fill').show();
        }

        $('#total_order').text(format(grand_total));
        $('#cart_qty_md').text(qty_product);
        $('#cart_qty_xs').text(qty_product);
    }

    function format(num) {
        var n = num.toString(), p = n.indexOf('.');
        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function($0, i) {
            return p < 0 || i < p ? ($0 + ',') : $0;
        });
    }
    function dropdown_qty(product_seq, id) {
        var sell_price = $('#sell_price_' + product_seq).attr("sell_price");
        var qty = $('#' + id).val();
        if (id.substr(id.length - 2) == 'xs') {
            $('#' + id.substr(0, id.length - 3)).val(qty);
        } else {
            $('#' + id + '_xs').val(qty);
        }

        var total_price = parseInt(sell_price) * parseInt(qty);
        $.ajax({
            url: "<?php echo base_url("member/main_page"); ?>",
            type: "POST",
            data: {
                "type": "<?php echo TASK_TYPE_ADD_QTY ?>",
                "product_seq": product_seq,
                "qty_product": qty
            },
            success: function(response) {
                if (isSessionExpired(response)) {
                    response_object = json_decode(response);
                    url = response_object.url;
                    location.href = url;
                } else {
                    $('#total_price_' + product_seq).attr("total_price", total_price);
                    $('#total_price_' + product_seq).text("Rp. " + format(total_price));
                    calculate_total();
                }
            },
            error: function(request, error) {
                alert(error_response(request.status));
            }
        });
    }
    function imageExists(image_url) {
        var http = new XMLHttpRequest();
        http.open('HEAD', image_url, false);
        http.send();
        return http.status != 404;
    }
</script>
