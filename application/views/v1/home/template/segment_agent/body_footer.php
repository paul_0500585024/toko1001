<div class="clearfix"></div>
<br><br>
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-2 col-xs-offset-5 mb-10px">
                <center>
                    <a href="https://instagram.com/toko1001.id">
                        <div class="img-icon f-instagram"></div>
                    </a>&nbsp; &nbsp; &nbsp;
                    <a href="https://www.facebook.com/toko1001.id">
                        <div class="img-icon f-facebook"></div>
                    </a>&nbsp; &nbsp; &nbsp;
                    <a href="https://twitter.com/toko1001_id">
                        <div class="img-icon f-twitter"></div>
                    </a>
                </center>
            </div>
        </div><br>
        <div class="row">
            <div class="col-xs-2 text-left">
                <b>LAYANAN PELANGGAN</b><br>
                <a class="wsh" href="<?php echo base_url(); ?>info/panduan-belanja">Panduan Belanja</a><br>
                <a class="wsh" href="<?php echo base_url(); ?>info/pengembalian-barang">Pengembalian Barang</a><br>
                <a class="wsh" href="<?php echo base_url(); ?>info/pengembalian-dana">Pengembaliain Dana</a><br>
                <a class="wsh" href="<?php echo base_url(); ?>info/kebijakan-privasi">Kebijakan Privasi</a><br>
                <a class="wsh" href="<?php echo base_url(); ?>info/syarat-dan-ketentuan">Syarat Dan Ketentuan</a><br>
                <a class="wsh" href="<?php echo base_url(); ?>info/syarat-dan-ketentuan-penggunaan-voucher">Penggunaan Voucher</a><br>
            </div>
            <div class="col-xs-2 text-left">
                <b>CUSTOMER CARE</b><br>
                Senin-Jumat<br>
                08.30-17.00 WIB<br>
                Sabtu<br> 08.30-14.00 WIB<br>
                +6221-6514224<br>
                +6221-6514225<br>
                cc@toko1001.id<br>
            </div>
            <div class="col-xs-8 no-pad">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-6 height35">
                                <b>KEAMANAN BELANJA</b> <br />
                                <div class="security-shoping-sprite"></div>
                            </div>
                            <div class="col-xs-6 height35">
                                <b>JASA PENGIRIMAN</b><br>
                                <div class="img-icon jasa-pengiriman"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 mt-10px height35">
                        <b>METODE PEMBAYARAN</b><br>
                        <div class="img-icon metode-pembayaran"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 text-justify">
                <p>TOKO1001 merupakan salah satu pusat belanja "online", yang menawarkan pengalaman belanja online dengan konsep aman, cepat dan nyaman. Kami menyediakan beragam produk, mulai dari fashion, handphone & tablet, kesehatan & kecantikan, hobby & jam tangan, kamera, computer, elektronik & audio, peralatan rumah tangga, mainan anak-anak dan bayi, tas & koper, olahraga & music, otomotif, makanan & minuman, buku & alat tulis.
                    Belanja online di TOKO1001 sangat mudah!!! Tetap terhubung dan dapatkan penawaran terbaru dan transaksi setiap harinya. Kami menawarkan beragam pilihan produk-produk berkualitas, yang dapat ditemukan melalui ujung jari Anda. Dengan navigasi pencarian yang sederhana namun efektif, daftar koleksi, dan harga yang menarik, Kami memberikan kemudahan untuk Anda menemukan produk sesuai kebutuhan belanja Anda.
                    Jadikanlah momen belanja online Anda menjadi kegiatan yang menyenangkan dengan TOKO1001. Terima kasih atas kunjungan Anda dan nikmati sensasi berbelanja online di TOKO1001, dimana Anda dapat mencari yang terbaik hanya dengan beberapa klik saja. Selamat berbelanja online!!!</p>
            </div>
        </div>
    </div>
    <div class="well well-footer reset-margin no-border-radius">
        <div class="container ">
            Copyright &copy; 2015 - <?php echo date('Y'); ?>&nbsp;PT. Deltamas Mandiri Sejahtera
        </div>
    </div>
</footer>
