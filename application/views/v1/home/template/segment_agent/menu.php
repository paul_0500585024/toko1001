<?php
$product_category_seq = isset($product_category_seq) ? $product_category_seq : '';
parse_str($this->input->server('QUERY_STRING'), $query_string);
$tree_category = isset($tree_category) ? $tree_category : array();
$url_form_search = isset($is_category) ? current_url() : (base_url() . ALL_CATEGORY);
$signout_param = http_build_query(array(CONTINUE_URL => current_url_with_query_string()));
?>
<nav class="navbar navbar-inverse navbar-fixed-top bg-primary-toko1001 no-border-radius" role="navigation" id="topnavbar" style="z-index:999;height: 60px;">
    <div class="container">
        <div class="row">
            <div class='col-xs-2 pos-relative'>
                <?php if (is_agent()) { ?>
                    <a id="brand" class="navbar-brand" href="<?php echo base_url() . '?' . 'view_mode' . '=' . AGENT; ?>" >
                        <img src="<?php echo get_img_url(); ?>home/522X120_LOGO_PUTIH.png" alt="toko1001.id">
                    </a>
                <?php } else { ?>
                    <a id="brand" class="navbar-brand" href="<?php echo base_url(); ?>">
                        <img src="<?php echo get_img_url(); ?>home/522X120_LOGO_PUTIH.png" alt="toko1001.id">
                    </a>
                <?php } ?>


            </div>

            <div class="col-xs-8">
                <form class="navbar-form navbar-left search_bar input-group" role="search" action="<?php echo $url_form_search ?>" method="get">
                    <input type="text" class="form-control form-group" placeholder="Cari produk, kategori, atau merk" id="dropdown-kategori" name="<?php echo SEARCH ?>" style="width:100%;border:0px;border-top-left-radius: 15px!important;border-bottom-left-radius: 15px!important;heigh:34px!important;">
                    <div class="input-group-btn" style="height:34pximportant;">
                        <select id="maxOption2_lg" class="selectpicker form-group" data-max-options="1" data-style="btn-inverse" style="heigh:34px!important;">
                            <?php echo get_category_option($tree_category, 0, $product_category_seq); ?>
                        </select>
                        <button type="submit" class="btn btn-info btncst-search"><i class="fa fa-search" style="font-size:1.2em;"></i></button>
                    </div>
                </form>
            </div>

            <div class="col-xs-2" style="padding-right: 15px;">
                <div class="row">
                    <div class="col-xs-4 no-pad no-margin" style="padding-left:15px!important;">
                        <div class="bag_box box_cart setPosrelative">
                            <a href="#" data-toggle="modal" data-target="#add_to_cart">
                                <!--<img src="<?php echo get_img_url(); ?>cart.png" class="invert" height="35px">-->
                                <i class="fa fa-shopping-cart invert nav-cart"></i>
                                <span id="cart_qty_xs" class="numberCircle">
                                    <?php echo isset($_SESSION[SESSION_PRODUCT_INFO]) && sizeof($_SESSION[SESSION_PRODUCT_INFO]) != 0 ? sizeof($_SESSION[SESSION_PRODUCT_INFO]) : "0"; ?>
                                </span>
                            </a>
                        </div>
                    </div>

                    <div class="col-xs-8 no-margin">
                        <center>
                            <?php if (is_agent()) { ?>
                                <div class="dropdown" style="margin-top:5px;">
                                    <button class="btn btn-login-register cst-dd" type="text" data-toggle="dropdown">
                                        <div class="caption-name">Halo,</div><br>
                                        <div class="addColorWhite pull-right">
                                            <div class="relative"> <div class="user-name cst-username"><?php echo ucwords($_SESSION[SESSION_AGENT_UNAME]); ?>&nbsp;&nbsp;</div><span class="fa fa-user pull-right cst-user-icon"></span></div>
                                    </button>
                                    <ul class="dropdown-menu cst-dropdown pull-right cst-pos" role='menu'>
                                        <li><a href="<?php echo get_base_url() . ROUTE_AGENT; ?>"><span class="fa fa-user"></span>Profile</a></li>
                                        <li><a href="<?php echo get_base_url() . ROUTE_AGENT; ?>login/sign_out?<?php echo $signout_param ?>" id="Logout"><span class="fa fa-sign-out"></span>Log Out</a></li>
                                    </ul>
                                </div>
                            <?php } else { ?>
                                <div class="row">
                                    <div class="col-xs-12 l-attribute">
                                        <div class="pull-right">
                                            <div class="pull-right attr-account caption-name">Halo,</div><br>
                                            <strong>
                                                <a href="#" data-toggle="modal" id="loginregister" data-target="#frm_login" data-id="1" class="acount">YOUR ACCOUNT&nbsp;<i class="fa fa-chevron-down"></i></a>
                                            </strong>
                                        </div>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</nav>
<div class="container no-pad">
    <div id="primary_nav_wrap">
        <?php $this->load->view(LIVEVIEW . 'home/template/segment_agent/main_menu'); ?>
    </div>
</div>
<script>
    var imgdtl = '';
</script>