<?php

$toRealDate = function($data) {
    $_a = date_create($data);
    $_a = date_format($_a, 'Y-m-d');
    return $_a;
};
require_once VIEW_BASE_HOME;
?>
<?php
$logo = IMG_NO_LOGO;
$banner = IMG_NO_BANNER;
if (isset($data_sel[LIST_DATA])) {
    $folder = CDN_IMAGE . "assets/img/merchant/logo/" . $data_sel[LIST_DATA][0]->seq . "/";
    if ($data_sel[LIST_DATA][0]->logo_img != "")
        $logo = $folder . $data_sel[LIST_DATA][0]->logo_img;
    if ($data_sel[LIST_DATA][0]->banner_img != "")
        $banner = $folder . $data_sel[LIST_DATA][0]->banner_img;
}
?>

<div class="container">
    <div class="row reset-margin">
        <img class="full-img" src="<?php echo $banner; ?>" alt="Banner Image" >
    </div>
    <div class="clearfix"></div>
    <div class="row reset-margin">
        <div class="col-xs-12  no-pad">
            <div class="col-xs-2 pad-15 primary-block h-260px">
                <div class="row reset-margin">
                    <center><img align="left" class="img-profile-v1 img-responsive thumbnail" src="<?php echo $logo; ?>" alt="Logo Image"/></center>
                </div>
                <div class="row reset-margin">
                    <div class="row reset-margin profile-margin-small-bottom">
                        <i class="fa fa-map-marker" title="Dikirim dari"></i>&nbsp;&nbsp; <?php echo (isset($data_sel[LIST_DATA][1][0]) ? $data_sel[LIST_DATA][1][0]->province_name . ' - ' . $data_sel[LIST_DATA][1][0]->city_name . ' - ' . $data_sel[LIST_DATA][1][0]->name : ''); ?>
                    </div>
                    <div class="row reset-margin profile-margin-small-bottom">
                        <i class="fa fa-calendar" title="Tanggal Bergabung"></i>&nbsp;&nbsp; Sejak <?php echo (isset($data_sel[LIST_DATA]) ? cdate($data_sel[LIST_DATA][0]->created_date) : ""); ?>&nbsp; &nbsp; <br>
                    </div>
                    <div class="row reset-margin profile-margin-small-bottom">
                        <i class="fa fa-cart-plus"></i>&nbsp;&nbsp; <?php echo count($product); ?> Produk
                    </div>
                    <div class="row reset-margin">
                        <?php
                        $city = '';
//			print_r($data_sel[LIST_DATA][2]);
                        if (isset($data_sel[LIST_DATA][2])) {
                            foreach ($data_sel[LIST_DATA][2] as $each) {
                                $city .= "<li>" . $each->name . "</li>";
                                if ($each->name == "SEMUA KOTA") {
                                    break;
                                };
                            }
                            ?>
                            <button id="pengirimangratis" type="button" data-container="body"  class="btn btn-front btn-sm btn-flat btn-delivery" 
                                    data-toggle="popover" title="Pengiriman Gratis"  data-trigger='click'
                                    data-content=
                                    "<ul>
                                    <?php echo $city; ?>
                                    </ul>
                                    " data-placement="bottom" data-html="true">
                                <i class="fa fa-truck"></i>&nbsp;Pengiriman Gratis
                            </button>

                            <script>
                                $("#pengirimangratis").on("click", function () {
                                    $(this).popover();
                                });
                            </script>
                        <?php } ?>
                    </div>

                </div>
                <?php
                if ($toRealDate($data_sel[LIST_DATA][0]->from_date) !== $toRealDate(DEFAULT_DATETIME)) {
                    if ($toRealDate($data_sel[LIST_DATA][0]->from_date) <= $toRealDate(date('Y-m-d h:i:s')) && $toRealDate(date('Y-m-d h:i:s')) < $toRealDate($data_sel[LIST_DATA][0]->to_date)) {
                        ?>
                        <div class='m-close'>
                            <br><br><br><br><br>
                            Buka kembali Tanggal <br><?php echo $dateConvert(date_format(date_create($data_sel[LIST_DATA][0]->to_date), 'j-M-Y')) ?>
                        </div>
                        <br/>
                        <?php
                    }
                }
                ?>
            </div>
            <div class="col-xs-10 no-pad pad-15">
                <div class="row reset-margin">
                    <div class="text-justify">
                    </div>
                    <h3><b><?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->name) : ""); ?></b></h3>
                    <?php echo (isset($data_sel[LIST_DATA]) ? ("<h5>" . $data_sel[LIST_DATA][0]->welcome_notes) . "</h5>" : ""); ?>

                </div>
                <hr id="garis-merchant">
                <div class="row reset-margin">
                    <?php echo display_product($product); ?>
                </div>
            </div>


            <div class="col-xs-10 no-pad pad-15 col-xs-offset-2">
                <div class="row reset-margin">
                    <div class="col-xs-12">
                        <center>
                            <?php echo $this->pagination->create_links(); ?>
                        </center>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>

<script>
    $('.productDetails').mouseenter(function () {
        var title = 'title-' + $(this).attr('data-img');
        $('#' + title).removeClass('no-show');
    });

    $('.productDetails').mouseleave(function () {
        var title = 'title-' + $(this).attr('data-img');
        $('#' + title).addClass('no-show');
    });

</script>
<?php $this->load->view(LIVEVIEW . 'home/template/segment_main/js_add_product.php'); ?>