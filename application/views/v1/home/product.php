<?php
require_once VIEW_BASE_HOME;
?>
<?php
$link = base_url() . "produk-terbaru";
$pages = $this->uri->segment(1);
?>
<div class="container">
    <div class="cst-breadcrumb">
        <div class="cst-breadcrumb-inside">
            <a href="<?php echo base_url(); ?>"><i class="fa fa-home"style="font-size:1.5em;"></i></a>
        </div>
        <div class="cst-breadcrumb-inside">
            <img src="<?php echo get_image_location() . 'assets/img/breadcrumb.png' ?>">
        </div>
        <a class="wsh" href='<?php echo $link ?>'><?php echo $breadcrumb_pages ?></a>
    </div>
    <?php
    echo display_product_home($product);
    ?>
</div>
<div class="container aad-display-block">
    <div class="row">
        <div class="col-xs-12">
            <center class="span12 centered-text"><?php echo $this->pagination->create_links(); ?></center>
        </div>
    </div>
</div>
<?php $this->load->view(LIVEVIEW . 'home/template/segment_main/js_add_product.php'); ?>
