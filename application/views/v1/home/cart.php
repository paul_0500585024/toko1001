<?php
require_once VIEW_BASE_HOME;
?>
<div class="container">
    <div class="row">
        <div class="container">
            <br>
            <?php if (isset($data_err) && $data_err[ERROR] === true) { ?>
                <div class="alert alert-error-home">
                    <button type="button" class="close" data-dismiss="alert" 
                            aria-hidden="true">×</button>
                            <?php echo $data_err[ERROR_MESSAGE][0] ?>
                </div>
            <?php } ?>

            <div class="row no-margin">
                <?php if ($data_step == TASK_FIRST_STEP) { ?>
                    <div class="container">
                        <div class="row reset-margin">
                            <?php if (is_agent()) { ?>
                                <div class="note note-info">
                                    Isian Informasi dibawah ini merupakan informasi pembeli
                                    <span id="note_close" class="btn no-margin fa fa-times pull-right"></span>
                                </div>
                            <?php } ?>



                            <div class="col-xs-12 box-header-cart">
                                <h4>Informasi Penerima</h4><br>
                                <form id="frmMain" method="post" url="<?php echo get_base_url() . $data_auth[FORM_URL] . "?step=2" ?>" onSubmit="next_step()">
                                    <input type="hidden" name ="type" value="<?php echo TASK_FIRST_STEP ?>">
                                    <input id="province_name" type="hidden" name ="province_name">
                                    <input type="hidden" name ="step" value="<?php echo TASK_SECOND_STEP ?>">
                                    <input id="city_name" type="hidden" name ="city_name" >
                                    <input id="district_name" type="hidden" name ="district_name">
                                    <div class='col-xs-4'>
                                        <div class='form-group'>
                                            <label>Jenis Alamat Penerima</label>
                                            <?php require_once get_component_url() . "dropdown/com_drop_member_address.php"; ?>
                                        </div>
                                        <div class='row'>
                                            <div class="col-xs-12">
                                                <div class='form-group'>
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <input type="checkbox" aria-label="..." name="chk_alias" <?php echo (!isset($data_sel[LIST_DATA][0]->chk_alias) ? "" : (($data_sel[LIST_DATA][0]->chk_alias == "1" OR $data_sel[LIST_DATA][0]->chk_alias == "on") ? "checked" : "")); ?> >
                                                        </span>
                                                        <input type="text" class="form-control" placeholder="Simpan dengan alias" name="alias" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->alias) : "" ); ?>" maxlength="25">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class='form-group'>
                                            <label>Nama Penerima*</label>
                                            <input id="receiver_name"  validate="required[]" type='text' name="receiver_name" class='form-control' value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->receiver_name) : "" ); ?>" maxlength="25">
                                        </div>
                                        <div class='form-group'>
                                            <label>No. Telp*</label>
                                            <input id="phone_no" validate ="required[]" name ="phone_no" type='text' class='form-control' value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->phone_no) : "" ); ?>" data-inputmask="&quot;mask&quot;: &quot;9999 9999 9999 9999&quot;" data-mask="" maxlength="18">
                                        </div>
                                    </div>
                                    <div class='col-xs-8'>
                                        <div class='form-group'>
                                            <label>Alamat*</label><textarea id="address" validate="required[]" name ='address' class='form-control resize-none' rows="3" ><?php echo(isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->address) : ""); ?></textarea></div>
                                        <div class='row'>
                                            <div class="col-xs-4">
                                                <div class='form-group'>
                                                    <label>Propinsi*</label>
                                                    <?php require_once get_component_url() . "dropdown/com_drop_province.php"; ?>
                                                </div>

                                            </div>
                                            <div class="col-xs-4">
                                                <div class='form-group'>
                                                    <label>Kabupaten*</label>
                                                    <?php require_once get_component_url() . "dropdown/com_drop_city.php"; ?>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class='form-group'>
                                                    <label>Kecamatan*</label>
                                                    <?php require_once get_component_url() . "dropdown/com_drop_district.php"; ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class='form-group'>
                                            <label>Kode Pos</label>
                                            <input id="postal_code" name='postal_code' type='text' class='form-control' value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? get_display_value($data_sel[LIST_DATA][0]->postal_code) : "" ); ?>" maxlength="10">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 no-pad">
                                        <div class="col-xs-9">
                                            &nbsp;
                                        </div>
                                        <div class="col-xs-3 no-pad">
                                            <div class="pull-right">
                                                <button type="submit" class='btn btn-flat btn-confirm'  >Gunakan Alamat</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <script text="javascript">


                            $("#note_close").on("click", function () {
                                $(this).parent().slideUp(500);
                            })

                            function change_province() {
                                var province_seq = $('#drop_province').val();
                                $.ajax({
                                    url: "<?php echo get_base_url() . "member/checkout" ?>",
                                    type: "POST",
                                    dataType: "json",
                                    data: {
                                        "type": "<?php echo TASK_PROVINCE_CHANGE ?>",
                                        "province_seq": province_seq
                                    },
                                    success: function (data) {
                                        if (isSessionExpired(data)) {
                                            response_object = json_decode(data);
                                            url = response_object.url;
                                            location.href = url;
                                        } else {
                                            $('#drop_city option').remove();
                                            $('#drop_district option').remove();
                                            $("#drop_city").append("<option value=\"" + "\">-- Pilih --</option>");
                                            $(data).each(function (i) {
                                                $("#drop_city").append("<option value=\"" + data[i].seq + "\">" + data[i].name + "</option>")
                                            });
                                            $('.select2').select2();
                                        }
                                    },
                                    error: function (request, error) {
                                        alert(error_response(request.status));
                                    }
                                });
                            }

                            function change_city() {
                                var city_seq = $('#drop_city').val();
                                $.ajax({
                                    url: "<?php echo get_base_url() . "member/checkout" ?>",
                                    type: "POST",
                                    dataType: "json",
                                    data: {
                                        "type": "<?php echo TASK_CITY_CHANGE ?>",
                                        "city_seq": city_seq
                                    },
                                    success: function (data) {
                                        if (isSessionExpired(data)) {
                                            response_object = json_decode(data);
                                            url = response_object.url;
                                            location.href = url;
                                        } else {
                                            $('#drop_district option').remove();
                                            $("#drop_district").append("<option value=\"" + "\">-- Pilih --</option>");
                                            $(data).each(function (i) {
                                                $("#drop_district").append("<option value=\"" + data[i].seq + "\">" + data[i].name + "</option>")
                                            });
                                            $('.select2').select2();
                                        }
                                    },
                                    error: function (request, error) {
                                        alert(error_response(request.status));
                                    }
                                });
                            }

                            function change_member_address() {
                                var member_address_seq = $('#drop_member_address').val();
                                $.ajax({
                                    url: "<?php echo get_base_url() . "member/checkout" ?>",
                                    type: "POST",
                                    dataType: "json",
                                    data: {
                                        "type": "<?php echo TASK_MEMBER_ADDRESS_CHANGE; ?>",
                                        "member_address_seq": member_address_seq
                                    },
                                    success: function (data) {
                                        if (isSessionExpired(data)) {
                                            response_object = json_decode(data);
                                            url = response_object.url;
                                            location.href = url;
                                        } else {
                                            $(data[0]).each(function (i) {
                                                $('#drop_city option').remove();
                                                $('#drop_district option').remove();
                                                $(data[1]).each(function (ix) {
                                                    $("#drop_city").append("<option value=\"" + data[1][ix].seq + "\">" + data[1][ix].name + "</option>")
                                                });
                                                $(data[2]).each(function (ix) {
                                                    $("#drop_district").append("<option value=\"" + data[2][ix].seq + "\">" + data[2][ix].name + "</option>")
                                                });
                                                $('#receiver_name').val(data[0][i].pic_name);
                                                $('#address').val(data[0][i].address);
                                                $('#postal_code').val(data[0][i].zip_code);
                                                $('#phone_no').val(data[0][i].phone_no);
                                                $('#drop_province').val(data[0][i].province_seq);
                                                $('#drop_city').val(data[0][i].city_seq);
                                                $('#drop_district').val(data[0][i].district_seq);
                                                $('.select2').select2();
                                            });
                                        }
                                    },
                                    error: function (request, error) {
                                        alert(error_response(request.status));
                                    }
                                });
                            }

                            function next_step() {
                                $('#province_name').val($('#drop_province option:selected').text());
                                $('#city_name').val($('#drop_city option:selected').text());
                                $('#district_name').val($('#drop_district option:selected').text());
                            }

                        </script>
                    <?php } elseif ($data_step == TASK_SECOND_STEP) { ?>

                        <form method="post" url="<?php echo get_base_url(); ?>member/checkout?step=3">
                            <input type="hidden" name ="type" value="<?php echo TASK_SECOND_STEP; ?>">
                            <input type="hidden" name ="step" value="<?php echo TASK_FINAL_STEP; ?>">
                            <input type="hidden" id="payment_head" name="payment_head" value="<?php echo (isset($_SESSION[SESSION_PAYMENT_INFO][PAYMENT_HEAD]) ? $_SESSION[SESSION_PAYMENT_INFO][PAYMENT_HEAD] : '' ) ?>"/>
                            <input type="hidden"  id="payment" name="payment" value="<?php echo (isset($_SESSION[SESSION_PAYMENT_INFO]['payment_seq']) ? $_SESSION[SESSION_PAYMENT_INFO]['payment_seq'] : '' ) ?>"/>
                            <div class="payment-head box-header-cart">Pilih Jenis Pembayaran</div>
                            <div class="row no-margin pos-relative">
                                <div id="pg_loading" class='hidden'>
                                    <center><i class="fa fa-spinner fa-spin fa-3x fa-fw margin-bottom"></i></center>
                                </div>
                                <?php
                                foreach ($pg_menu as $payment_data) {
                                    if ($payment_data->seq == PAYMENT_SEQ_ADIRA_HEAD) {
                                        if (adiraCheck($_SESSION[SESSION_PRODUCT_INFO])) {
                                            if (is_agent()) {
                                                ?>
                                                <div class = "col-xs-1 no-pad">
                                                    <div id = "pg_<?php echo $payment_data->seq ?>" class = "thumbnail pg_payment
                                                         <?php echo (isset($_SESSION[SESSION_PAYMENT_INFO][PAYMENT_HEAD]) && $payment_data->seq == $_SESSION[SESSION_PAYMENT_INFO][PAYMENT_HEAD]) ? 'active' : '' ?> ">
                                                        <center>
                                                            <img class = "img-payment-method" src = "<?php echo get_img_url() . 'payment/' . $payment_data->logo_img ?>"/>
                                                            <?php echo $payment_data->name
                                                            ?>
                                                        </center>
                                                    </div>
                                                </div><?php
                                            }
//                                            hide adira seq from member
                                        } else {
                                            
                                        }
                                    } else {
                                        if ($payment_data->seq == PAYMENT_CREDIT_HEAD) {
                                            if (is_agent()) {
//                                                hide cicilan from agent
                                            } else {
                                                ?>
                                                <div class="col-xs-1 no-pad">
                                                    <div id="pg_<?php echo $payment_data->seq ?>" class="thumbnail pg_payment
                                                         <?php echo (isset($_SESSION[SESSION_PAYMENT_INFO][PAYMENT_HEAD]) && $payment_data->seq == $_SESSION[SESSION_PAYMENT_INFO][PAYMENT_HEAD]) ? 'active' : '' ?> ">
                                                        <center>
                                                            <img class="img-payment-method" src="<?php echo get_img_url() . 'payment/' . $payment_data->logo_img ?>"/>
                                                            <?php echo $payment_data->name ?>
                                                        </center>
                                                    </div>
                                                </div> 
                                                <?php
                                            }
                                        } else {
                                            ?>
                                            <div class="col-xs-1 no-pad">
                                                <div id="pg_<?php echo $payment_data->seq ?>" class="thumbnail pg_payment
                                                     <?php echo (isset($_SESSION[SESSION_PAYMENT_INFO][PAYMENT_HEAD]) && $payment_data->seq == $_SESSION[SESSION_PAYMENT_INFO][PAYMENT_HEAD]) ? 'active' : '' ?> ">
                                                    <center>
                                                        <img class="img-payment-method" src="<?php echo get_img_url() . 'payment/' . $payment_data->logo_img ?>"/>
                                                        <?php echo $payment_data->name ?>
                                                    </center>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </div>
                            <br>
                            <div class="payment-head hidden box-header-cart">Pilih Metode Pembayaran</div>
                            <div id='pg_method_list'>
                            </div>
                            <br>
                            <div id="pg_btn_to_final_step" class="col-xs-3 col-xs-offset-9 no-pad hidden">
                                <div class="pull-right">
                                    <button type="submit" class='btn btn-flat btn-confirm'>Pilih dan lanjutkan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <script>


                        (function ($) {
    <?php if (isset($_SESSION[SESSION_PAYMENT_INFO][PAYMENT_HEAD])) { ?>
                                get_payment_method(<?php echo $_SESSION[SESSION_PAYMENT_INFO][PAYMENT_HEAD] ?>);
    <?php } ?>
                            $.fn.btnPayment = function () {
                                return this.each(function () {
                                    $(this).on("click", function () {
                                        $(this).addClass('active');
                                        $(this).parent().siblings().children().removeClass("active");
                                        var id = $(this).attr("id").split('_');
                                        $("#payment_head").val('');
                                        $("#payment_head").val(id[1]);
                                        $("#payment").val('');
                                        get_payment_method(id[1]);
                                    })
                                })
                            }
                            function get_payment_method(pg_seq) {
                                var payment_type_seq = pg_seq;
                                $.ajax({
                                    url: "<?php echo get_base_url() . "member/checkout" ?>",
                                    type: "POST",
                                    dataType: "json",
                                    data: {
                                        "type": "get_payment_method",
                                        "pg_seq": payment_type_seq
                                    },
                                    success: function (data) {
                                        if (isSessionExpired(data)) {
                                            response_object = json_decode(data);
                                            url = response_object.url;
                                            location.href = url;
                                        } else {
                                            var build = "";
                                            $("#pg_method_list").empty();
                                            $(".payment-head").addClass("hidden");
                                            $(".payment-head").removeClass("hidden");
                                            $(data).each(function (i) {

                                                if (data[i].pg_seq == '2' || data[i].pg_seq == '7') {
                                                    var deposit_amt = '<div class="saldo-amt-box">Saldo Deposit : <div class="">Rp.&nbsp;<?php echo number_format($member_info[2][0]->deposit_amt) ?></div></div>';
                                                }

                                                build += '<div id="' + data[i].seq + '" onclick="pickUpPayment(this.id)" class="col-xs-3 border-cart no-pad m-top10">' +
                                                        '<div id="chk_' + data[i].seq + '" class="payment-method-ceklist hidden"><i class="fa fa-check" aria-hidden="true"></i></div>' +
                                                        '<center><b>' + data[i].name + '</b></center>';
                                                if (data[i].seq == 2 || data[i].seq == 10) {
                                                    build += deposit_amt;
                                                }

                                                build += '<img class="img-responsive" src="<?php echo get_img_url() . "payment_70px/"; ?>' + data[i].logo_img + '"/>' +
                                                        '</div>';
                                            });
                                            $("#pg_method_list").append(build);

    <?php if (isset($_SESSION['pg_info']['payment_seq'])) { ?>
                                                $('#chk_' + '<?php echo $_SESSION[SESSION_PAYMENT_INFO]['payment_seq']; ?>').removeClass('hidden');
    <?php } ?>

                                        }
                                    },
                                    complete: function () {
                                        $('#pg_btn_to_final_step').removeClass("hidden");
                                    },
                                    error: function (request, error) {
                                        alert(error_response(request.status));
                                    }
                                });
                            }
                        }(jQuery));

                        function pickUpPayment(id) {
                            $("#payment").val("");
                            $("#payment").val(id);
                            $(".border-cart").children(".payment-method-ceklist").addClass("hidden");
                            $("#" + id).find(".payment-method-ceklist").removeClass("hidden");
                        }
                        $(".pg_payment").btnPayment();

                    </script>
                </div>

            <?php } else if ($data_step == TASK_FINAL_STEP) { ?>
                <div class="container">
                    <div class="col-xs-12 bg-white">
                        <form id="frmMain" method="post" action ="<?php echo get_base_url(); ?>member/checkout?step=3">
                            <?php echo get_csrf_member_token() ?>
                            <input type="hidden" name="type" value="<?php echo TASK_FINAL_STEP ?>">
                            <div class="row">
                                <div class="col-xs-12 box-header-cart">
                                    <h4>Penerima</h4>
                                    <div class="medium-margin-left">
                                        <div><?php echo $_SESSION[SESSION_PERSONAL_INFO]["receiver_name"]; ?></div>
                                        <div><?php echo $_SESSION[SESSION_PERSONAL_INFO]["address"] . "&nbsp;" . $_SESSION[SESSION_PERSONAL_INFO]["city_name"] . " / " . $_SESSION[SESSION_PERSONAL_INFO]["district_name"] . "</br>" . $_SESSION[SESSION_PERSONAL_INFO]["province_name"] . "</br>" . $_SESSION[SESSION_PERSONAL_INFO]["postal_code"]; ?></div>
                                        <div><?php echo $_SESSION[SESSION_PERSONAL_INFO]["phone_no"]; ?></div>
                                        <div><br>Pembayaran menggunakan : <?php echo $payment_info["payment_name"]; ?></div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col-xs-12 box-header-cart">
                                    <h4>Produk yang dibeli</h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 box-body-cart">
                                    <?php
                                    $order;
                                    $show_kredit_pop = "";
                                    $prd_credit = '';
                                    $total = 0;
                                    if (isset($data_prod) && sizeof($data_prod) > 0) {
                                        foreach ($data_prod as $key => $prod) {
                                            $order[$prod[$key]["merchant_seq"]][] = $prod[$key];
                                        }
                                        $totAllWieght = 0;
                                        foreach ($order as $key => $prod) {
                                            (float) $weightMerchant = 0;
                                            ?>
                                            <div class="row reset-margin merchant-header">
                                                <div class="col-xs-4">
                                                    <div class="col-xs-12">
                                                        <div class="row">
                                                            <a href = "<?php echo base_url() . "merchant/" . $prod[0]["merchant_code"] ?>" ><h5><b><?php echo $prod[0]["merchant_name"]; ?></b></h5></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6"><input id="msg_<?php echo $prod[0]["merchant_seq"]; ?>" onblur="set_value(this.id)" type = "text" name="member_message[<?php echo $prod[0]["merchant_seq"]; ?>]"  class = "form-control input-sm" value="<?php echo $prod[0]["member_message"]; ?>" placeholder = "Pesan untuk merchant <?php echo $prod[0]["merchant_name"]; ?>" maxlength="100"></div>
                                                <div class="col-xs-2">
                                                    <div class="text-align-right">
                                                        <div><?php echo "<b>(" . count($prod) . ")</b> Produk"; ?></div>
                                                        <div>
                                                            <div>Shipping : <?php echo $prod[0]["exp_name"]; ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            $product_seq = '';
                                            $product_price_total = '0';
                                            $shipping_total = '0';
                                            $admin_fee = '0';
                                            if (isset($prod)) {
                                                foreach ($prod as $prod_item) {
                                                    $product_seq = $prod_item["product_seq"];
                                                    $weightMerchant = $weightMerchant + ( $prod_item["product_weight"] * $prod_item["qty"]);
                                                    ?>
                                                    <div class="row reset-margin un-style">
                                                        <div class="col-xs-2">
                                                            <a href ="<?php echo base_url() . url_title($prod_item["product_name"] . " " . $prod_item["product_seq"]); ?>">
                                                                <center><img src = "<?php echo PRODUCT_UPLOAD_IMAGE . $prod_item["merchant_seq"] . "/" . S_IMAGE_UPLOAD . $prod_item["img"]; ?>" alt="<?php echo $prod_item["img"] ?>" class="img" width="100"></center></a>
                                                        </div>
                                                        <div class="col-xs-6">
                                                            <div class="row">
                                                                <div class="col-xs-12 cart-font-color-blue no-pad text-bold"><?php echo $prod_item["product_name"]; ?></div>
                                                            </div>
                                                            <?php if (isset($prod_item["variant_value"]) && $prod_item["variant_value"] != '1') { ?>
                                                                <div class="row">
                                                                    <div class="col-xs-3 pad-auto">Warna</div>
                                                                    <div class="col-xs-1 pad-auto">&nbsp;:&nbsp;</div>
                                                                    <div class="col-xs-6 no-pad"><?php echo $prod_item["variant_name"]; ?></div>
                                                                </div>
                                                            <?php } ?>
                                                            <div class="row">
                                                                <div class="col-xs-3 pad-auto">Berat</div>
                                                                <div class="col-xs-1 pad-auto">&nbsp;:&nbsp;</div>
                                                                <div class="col-xs-6 no-pad"><?php echo $prod_item["product_weight"]; ?> Kg</div>
                                                            </div>
                                                            <!--                                                            <div class="row">
                                                                                                                            <div class="col-xs-3 pad-auto">Biaya pengiriman</div>
                                                                                                                            <div class="col-xs-1 pad-auto">&nbsp;:&nbsp;</div>
                                                                                                                            <div class="col-xs-6 no-pad">
                                                                                                                                <b><?php echo $prod_item["exp_fee"] == 0 ? "<div class='text-green'>Free</div>" : "Rp. " . number_format($prod_item["exp_fee"] * ceil($prod_item["product_weight"] * $prod_item["qty"])); ?></b>
                                                                                                                            </div>
                                                                                                                        </div>-->
                                                        </div>
                                                        <div class="col-xs-4 cart-font-color-gray">
                                                            <?php if ($payment_info["payment_seq"] == PAYMENT_SEQ_CREDIT && $prod_item["credit_seq"] > 0) { ?>
                                                                <div class="row">
                                                                    <div class="col-xs-12">
                                                                        <a href="javascript:show_cr(<?php echo $prod_item["product_seq"]; ?>)" class="cicils">Bayar dengan cicilan</a>
                                                                        <input type="hidden" id="crseq_<?php echo $prod_item["product_seq"]; ?>" name="crseq_<?php echo $prod_item["product_seq"]; ?>" value="<?php echo $prod_item["credit_seq"]; ?>" />
                                                                        <?php $prd_credit = $prod_item["product_seq"]; ?>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-xs-12">&nbsp;</div>
                                                                    </div>
                                                                </div>
                                                            <?php } else { ?>
                                                                <div class="row">
                                                                    <div class="col-xs-12">&nbsp;</div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xs-12">&nbsp;</div>
                                                                </div>
                                                            <?php } ?>
                                                            <div class="row rowdefault" id="rowdefault_<?php echo $prod_item["product_seq"]; ?>">
                                                                <div class="col-xs-4 no-pad text-align-right">
                                                                    Rp. <?php echo number_format($prod_item["sell_price"]); ?>
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <center><input class="input-style" readonly value="<?php echo number_format($prod_item["qty"]); ?>"/></center>
                                                                </div>
                                                                <div class="col-xs-5 no-pad">
                                                                    Rp.  <?php
                                                                    echo number_format($prod_item["qty"] * $prod_item["sell_price"]);
                                                                    $total = $total + ($prod_item["qty"] * $prod_item["sell_price"]);
                                                                    $product_price_total = $product_price_total + ($prod_item["qty"] * $prod_item["sell_price"]);
                                                                    $shipping_total = $shipping_total + ($prod_item["exp_fee"] * ceil($prod_item["product_weight"] * $prod_item["qty"]));
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                                <div class="well" style="line-height: 0px;background: #fff;border-top: none">
                                                    <span class="pull-left"><p class="text-bold text-color-cart">Biaya Pengiriman : <span style="color: #007dc6"><?php echo $prod[0]["exp_name"]; ?></span> <?php if ($prod_item["exp_fee"] !== 0) { ?> (<span class="expedisi-label-kg"><?php echo ceil($weightMerchant) ?> X <?php echo number_format($prod_item["exp_fee"]) ?></span> ) <?php } echo $prod_item["exp_fee"] == 0 ? "<span class='text-green'>Free</span>" : "Rp. " . number_format($prod_item["exp_fee"] * ceil($weightMerchant)) ?></p></span>
                                                        <span class="pull-right"><p><span class="text-bold" style="font-size:1em"><?php echo RP . number_format($product_price_total + $prod_item["exp_fee"] * ceil($weightMerchant)) ?></span></p></span>
                                                    </div>
                                                    <?php
                                                    $totAllWieght = $totAllWieght + ($prod_item["exp_fee"] * ceil($weightMerchant));
                                                }
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <?php if (is_agent() === false) { ?>
                                            <div class="row">
                                                <div class="col-xs-12"><br>
                                                    Pilih Voucher : <?php require get_component_url() . "dropdown/com_drop_promo_voucher.php"; ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <div class="row"><br>


                                            <?php if ($payment_info["payment_seq"] !== PAYMENT_SEQ_ADIRA) { ?>
                                                <div class="col-xs-12">
                                                    <?php if ($coupon == true) { ?>
                                                        Kode Kupon / Voucher:
                                                        <div class="input-group input-group-sm">
                                                            <input id="coupon_code" class ="input-sm form-control" placeholder="Masukan Kode Kupon / Voucher" onblur="set_coupon(id)">
                                                            <span class="input-group-btn">
                                                                <button id="apply" type="button" class="btn btn-flat btn-height-1 btn-green" onClick="get_coupon()">Terapkan</button>
                                                            </span>
                                                        </div>
                                                        <label id="coupon_field" class="help-block font-small-2"><br /></label>
                                                    <?php } ?>
                                                </div>
                                            <?php } ?>


                                        </div>

                                    </div>
                                    <div class="col-xs-6 font-small-1">
                                        <br>
                                        <div class="row">
                                            <div class="col-xs-8">
                                                <div class="text-right">Total Belanja : </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <b class="pull-right" style="padding-right: 19px;font-size: 1.3em">Rp. <?php echo number_format($total + $totAllWieght); ?></b>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8">
                                                <div class="text-right">Total Pembayaran: </div>
                                            </div>
                                            <div class="col-xs-4 cart-font-color-brown">
                                                <b class="font-color-red pull-right" style="padding-right: 19px;font-size: 1.3em">Rp. <span id="total" nominal="<?php echo $total + $totAllWieght; ?>"><?php echo number_format($total + $totAllWieght); ?></span></b>
                                            </div>
                                        </div>

                                        <?php
                                        if ($payment_info["payment_seq"] == PAYMENT_SEQ_CREDIT && $prod_item["credit_seq"] > 0 && sizeof($data_prod) == 1) {
                                            $show_kredit_pop = "show";
                                            ?>

                                            <div class="row rowcredit" id="rowcredit" total_credit="<?php echo $total; ?>">
                                                <div class="col-xs-9">
                                                    <div class="text-right"><span class="cicilspop">Cicilan :</span> </div>
                                                </div>
                                                <div class="col-xs-3 cart-font-color-brown no-pad">
                                                    <select class="form-control" id="promo_credit_bank_seq" name="promo_credit_seq">
                                                        <option value="0">-- Cicilan --</option>
                                                        <?php
                                                        $bank = '';
                                                        if (isset($credit[0])) {
                                                            foreach ($credit[0] as $each) {
                                                                if ($bank != $each->bank_name) {
                                                                    if ($bank != '')
                                                                        echo '</optgroup>';
                                                                    echo "<optgroup label='" . $each->bank_name . "'>";
                                                                }
                                                                $bank = $each->bank_name;
                                                                echo '<option value="' . $each->credit_bank_seq . '"' . ($prod_item["promo_credit_bank_seq"] == $each->credit_bank_seq ? ' selected' : '') . '>' . $each->credit_month . ' Bulan -- ' . $each->bank_name . '</option>';
                                                            }
                                                            if ($bank != '')
                                                                echo '</optgroup>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <br>
                                <?php
                                if ($payment_info["payment_seq"] == PAYMENT_SEQ_ADIRA || $payment_info["payment_seq"] == PAYMENT_SEQ_ADIRA_DEPOSIT) {
                                    if (isset($info_product_loan)) {
                                        if ($info_product_loan != '') {
                                            ?>
                                            <!-- SIMULASI AREA-->
                                            <div class="row">
                                                <div class="col-xs-12 box-header-cart">
                                                    <h4>Simulasi</h4>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-6 simulation-sub-header">Simulasi cicilan</div>
                                                <div class="col-xs-6 simulation-sub-header">Angsuran Pertama</div>
                                            </div>

                                            <div class="row">
                                                <div class="clearfix"></div>
                                                <div class="col-xs-6">
                                                    <div class="row">
                                                        <div class="col-xs-12 simulation-field no-margin">                                                        
                                                            <div class="col-xs-5">
                                                                Harga Barang
                                                            </div>
                                                            <div class="col-xs-1">Rp.</div>
                                                            <div class="col-xs-6">
                                                                <input type="hidden" id="product_price_total" name="product_price_total" value="<?php echo $product_price_total; ?>">
                                                                <span class="pull-right text-bold"><?php echo number_format($product_price_total); ?></span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>

                                                        <div class="col-xs-12 simulation-field odd">                                                        
                                                            <div class="col-xs-5">
                                                                Uang Muka (DP)
                                                            </div>
                                                            <div class="col-xs-1 lh34">Rp.</div>
                                                            <div class="col-xs-6">
                                                                <input type="hidden" id="dp" name="dp" 
                                                                       value="<?php echo (isset($_SESSION[SESSION_DATA_SIMULATION]->dps) && $_SESSION[SESSION_DATA_SIMULATION]->dps != '' ? $_SESSION[SESSION_DATA_SIMULATION]->dps : '0' ) ?>" 
                                                                       class="auto_int text-align-right form-control">        
                                                                <span id = "nilai_dp" class = "pull-right"><?php echo(isset($_SESSION[SESSION_DATA_SIMULATION]->dp) && $_SESSION[SESSION_DATA_SIMULATION]->dp ? $_SESSION[SESSION_DATA_SIMULATION]->dp : '')
                                            ?> </span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <span id="min_dp" class="text-red pull-right"></span> <!-- BELUM TAU NIH BUAT APA KALO DI APPEND PASTI BERANTAKAN -->
                                                        <div class="col-xs-12 simulation-field">                                                        
                                                            <div class="col-xs-5">
                                                                Pokok Hutang
                                                            </div>
                                                            <div class="col-xs-1 lh34">Rp.</div>
                                                            <div class="col-xs-6">
                                                                <span id="pokok_hutang" class="pull-right text-bold">
                                                                    <?php echo (isset($_SESSION[SESSION_DATA_SIMULATION]->ph) && $_SESSION[SESSION_DATA_SIMULATION]->ph != '' ? $_SESSION[SESSION_DATA_SIMULATION]->ph : '' ) ?>                                                                
                                                                </span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>


                                                        <div class="col-xs-12 simulation-field odd">                                                        
                                                            <div class="col-xs-5">
                                                                Tenor
                                                            </div>
                                                            <div class="col-xs-1 lh34"></div>
                                                            <div class="col-xs-6">
                                                                <select class="form-control" id="tenor" name="tenor" onchange="check_interest()" style="text-align: right;">
                                                                    <?php
                                                                    if (isset($info_product_loan)) {
                                                                        foreach ($info_product_loan[0] as $data_loan) {
                                                                            $admin_fee = $data_loan->admin_fee;
                                                                            echo '<option value="' . $data_loan->seq . '" data-bunga="' . $data_loan->loan_interest . '" data-tenor="' . $data_loan->tenor . '"' . (isset($_SESSION[SESSION_DATA_SIMULATION]) && $data_loan->tenor == $_SESSION[SESSION_DATA_SIMULATION]->tenor_value ? "selected" : "") . '>' . $data_loan->tenor . '&nbsp;Bulan&nbsp;</option>';
                                                                        };
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <!-- ANGSURAN -->
                                                        <div class="col-xs-12 simulation-field odd">                                                        
                                                            <div class="col-xs-5">
                                                                Angsuran
                                                            </div>
                                                            <div class="col-xs-1 lh34"></div>
                                                            <div class="col-xs-6 text-right">
                                                                <input type="hidden" id="installment" name="installment" value="0">
                                                                <input type="hidden" id="tenor_value" name="tenor_value" 
                                                                       value="<?php echo(isset($_SESSION[SESSION_DATA_SIMULATION]->tenor_value) && $_SESSION[SESSION_DATA_SIMULATION]->tenor_value != '' ? $_SESSION[SESSION_DATA_SIMULATION]->tenor_value : ''); ?>">
                                                                <span id="angsuran" class="pull-right text-bold">
                                                                    <?php
                                                                    echo(isset($_SESSION[SESSION_DATA_SIMULATION]->angsuran) && $_SESSION[SESSION_DATA_SIMULATION]->angsuran != '' ? $_SESSION[SESSION_DATA_SIMULATION]->angsuran : '')
                                                                    ?>
                                                                </span>
                                                            </div>
                                                            <div class = "clearfix"></div>
                                                        </div>
                                                        <!--END ANGSURAN -->
                                                    </div>
                                                </div>
                                                <div class = "col-xs-6 box-body-cart">
                                                    <div class = "row">
                                                        <div class = "col-xs-12 simulation-field">
                                                            <div class = "col-xs-5">
                                                                Uang Muka (DP)
                                                            </div>
                                                            <div class = "col-xs-1 lh34">Rp.</div>
                                                            <div class = "col-xs-6 text-right">
                                                                <span id = "nilai_dp_2" class = "pull-right"><?php echo(isset($_SESSION[SESSION_DATA_SIMULATION]->dp) && $_SESSION[SESSION_DATA_SIMULATION]->dp ? $_SESSION[SESSION_DATA_SIMULATION]->dp : '')
                                                                    ?> </span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>


                                                        <div class="col-xs-12 simulation-field odd">                                                        
                                                            <div class="col-xs-5">
                                                                Angsuran I
                                                            </div>
                                                            <div class="col-xs-1 lh34">Rp.</div>
                                                            <div class="col-xs-6 text-right">
                                                                <span id="angsuran_pertama" class="pull-right">
                                                                    <?php echo (isset($_SESSION[SESSION_DATA_SIMULATION]->angsuran) && $_SESSION[SESSION_DATA_SIMULATION]->angsuran != '' ? $_SESSION['data_simulation']->angsuran : '' ) ?>
                                                                </span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>


                                                        <div class="col-xs-12 simulation-field">                                                        
                                                            <div class="col-xs-5">
                                                                Administrasi
                                                            </div>
                                                            <div class="col-xs-1 lh34">Rp.</div>
                                                            <div class="col-xs-6 text-right">
                                                                <input type="hidden" id="admin_fee" name="admin_fee" value="<?php echo $admin_fee; ?>">
                                                                <span class="pull-right"><?php echo number_format($admin_fee); ?></span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>


                                                        <div class="col-xs-12 simulation-field odd">                                                        
                                                            <div class="col-xs-5">
                                                                Ongkos Kirim
                                                            </div>
                                                            <div class="col-xs-1 lh34">Rp.</div>
                                                            <div class="col-xs-6 text-right">
                                                                <span class="pull-right"><?php echo number_format($shipping_total); ?></span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>

                                                        <div class="col-xs-12 simulation-field">                                                        
                                                            <div class="col-xs-5">
                                                                Total Pembayaran Pertama
                                                            </div>
                                                            <div class="col-xs-1 lh34">Rp.</div>
                                                            <div class="col-xs-6 text-right">
                                                                <input type="hidden" id="total_installment" name="total_installment" value="0">
                                                                <span id="total_pertama" class="pull-right text-bold">
                                                                    <?php echo (isset($_SESSION[SESSION_DATA_SIMULATION]->total_pay_1) && $_SESSION[SESSION_DATA_SIMULATION]->total_pay_1 != '' ? $_SESSION[SESSION_DATA_SIMULATION]->total_pay_1 : '' ) ?>
                                                                </span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 box-body-cart" style="padding: 10px">
                                                    <div class="col-xs-12">
                                                        <button type="button" onclick="calculate()" class="btn btn-primary pull-right btn-flat">Hitung Simulasi Pembayaran</button>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <!-- END SIMULASI-->
                                            <br>

                                            <style>
                                                /*Must inline style for overide*/
                                                .select2-selection{
                                                    height: 34px !important;
                                                    line-height: 34px!important;
                                                    border-radius: 0px!important;
                                                    margin-top: -3px!important  ;
                                                }
                                            </style>


                                            <!-- START CUSTOMER-->
                                            <div class="row">
                                                <div class="col-xs-12 box-header-cart">
                                                    <h4>Form Pengajuan Kredit </h4>
                                                    <!--<button onclick="add_new_customer()" class="btn btn-success btn-flat" style="position: absolute;right: 15px;top: 15px;line-height: 9px;z-index: 99;">-->
                                                        <!--<i class="fa fa-plus"></i>&nbsp;Tambah Customer Baru-->
                                                    <!--</button>-->
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 box-body-cart" style="padding: 5px">
                                                    <div class="col-xs-6">
                                                        <!--<div class="col-xs-12 lh34 no-pad" >Ketikkan Nama Atau No Id Customer</div>-->
                                                        <div class="col-xs-12 lh34 no-pad">
                            <!--                                                        <select id="customer_seq" name="customer_seq" style='width : 100%'>
                                                            <?php // echo isset($_SESSION[SESSION_CUSTOMER_DATA]) ? "<option name='' value='" . $_SESSION[SESSION_CUSTOMER_DATA]->customer_seq . "' selected>" . $_SESSION[SESSION_CUSTOMER_DATA]->pic_name . " / " . $_SESSION[SESSION_CUSTOMER_DATA]->identity_no . "</option>" : ""               ?>
                                                            </select>-->
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <br>
                                                <input id="province_name" type="hidden" name ="province_name">
                                                <input id="city_name" type="hidden" name ="city_name" >
                                                <input id="district_name" type="hidden" name ="district_name">

                                                <div class="col-xs-6 box-body-cart no-pad">


                                                    <div class="customer-field">
                                                        <div class='form-group col-xs-12 no-pad'>
                                                            <label>No Identitas</label>
                                                            <input id="identity_no" type='text' 
                                                            <?php echo (isset($_SESSION[SESSION_CUSTOMER_DATA]->customer_seq) && $_SESSION[SESSION_CUSTOMER_DATA]->customer_seq != '' ? 'readonly' : '') ?> 
                                                                   name="identity_no" class='form-control frmcust' 
                                                                   value ="<?php echo (isset($_SESSION[SESSION_CUSTOMER_DATA]->identity_no) ? $_SESSION[SESSION_CUSTOMER_DATA]->identity_no : '') ?>" maxlength="50">
                                                        </div>
                                                    </div>


                                                    <div class="customer-field">
                                                        <div class='form-group col-xs-12 no-pad'>
                                                            <label>Nama Customer</label>
                                                            <input id="pic_name" type='text' 
                                                            <?php echo (isset($_SESSION[SESSION_CUSTOMER_DATA]->customer_seq) && $_SESSION[SESSION_CUSTOMER_DATA]->customer_seq != '' ? 'readonly' : '') ?> 
                                                                   name="pic_name" class='form-control frmcust' 
                                                                   value ="<?php echo (isset($_SESSION[SESSION_PERSONAL_INFO]) ? $_SESSION[SESSION_PERSONAL_INFO]['receiver_name'] : '') ?>" maxlength="50" disabled>
                                                        </div>
                                                    </div>


                                                    <div class="customer-field">
                                                        <div class='form-group col-xs-12 no-pad'>
                                                            <label>Tanggal Lahir</label>
                                                            <input type='text' id="datepicker" date_type="date" 
                                                            <?php echo (isset($_SESSION[SESSION_CUSTOMER_DATA]->customer_seq) && $_SESSION[SESSION_CUSTOMER_DATA]->customer_seq != '' ? 'readonly' : '') ?> 
                                                                   name="birthday" class='form-control frmcust' 
                                                                   value ="<?php echo (isset($_SESSION[SESSION_CUSTOMER_DATA]->birthday) ? cdate($_SESSION[SESSION_CUSTOMER_DATA]->birthday) : '') ?>">
                                                        </div>
                                                    </div>


                                                    <div class="customer-field">
                                                        <div class='form-group col-xs-12 no-pad'>
                                                            <label>Alamat Email</label>
                                                            <input id="pic_email" type='text' 
                                                            <?php echo (isset($_SESSION[SESSION_CUSTOMER_DATA]->customer_seq) && $_SESSION[SESSION_CUSTOMER_DATA]->customer_seq != '' ? 'readonly' : '') ?> 
                                                                   name="pic_email" class='form-control frmcust' validate="email[]"
                                                                   value ="<?php echo (isset($_SESSION[SESSION_CUSTOMER_DATA]->email_address) ? $_SESSION[SESSION_CUSTOMER_DATA]->email_address : '') ?>" maxlength="50">
                                                        </div>
                                                    </div>


                                                    <div class="customer-field">
                                                        <div class='form-group col-xs-12 no-pad'>
                                                            <label>No. Telp</label>
                                                            <input id="phone_no" name ="phone_no" 
                                                                   type='text' class='form-control frmcust' 
                                                                   value ="<?php echo (isset($_SESSION[SESSION_PERSONAL_INFO]) ? $_SESSION[SESSION_PERSONAL_INFO]['phone_no'] : '') ?>" maxlength="18" disabled>
                                                        </div>
                                                    </div>


                                                    <div class="customer-field">
                                                        <div class='form-group col-xs-12 no-pad'>
                                                            <div class="col-xs-5 no-pad"><label>Alamat Sesuai KTP</label></div>
                                                            <textarea id="identity_address" name ='identity_address' <?php echo (isset($_SESSION[SESSION_CUSTOMER_DATA]->customer_seq) && $_SESSION[SESSION_CUSTOMER_DATA]->customer_seq != '' ? 'readonly' : '') ?> class='form-control frmcust resize-none' rows="3" ><?php echo (isset($_SESSION[SESSION_CUSTOMER_DATA]->identity_address) ? trim($_SESSION[SESSION_CUSTOMER_DATA]->identity_address) : '') ?></textarea>
                                                            <div class="col-xs-7 col-xs-offset-5 no-pad"><label style="font-weight: normal" class="pull-right">samakan dengan alamat Sekarang &nbsp;&nbsp;
                                                                    <span class="pull-right"><input style="padding-top: 10px" type="checkbox" id="condAddress" name="" onclick="customerAddress()"></span></label></div>
                                                        </div>
                                                    </div>
                                                    <style>
                                                        disable
                                                    </style>

                                                    <script>
                                                        function customerAddress() {

                                                            var stat = $('#condAddress').prop('checked');
                                                            console.log(stat);
                                                            var address2 = $(address).val();
                                                            if (stat) {
                                                                $('#identity_address').val(address2).attr('readonly', true);

                                                            } else {
                                                                $('#identity_address').val('').removeAttr('readonly', false);
                                                            }

                                                        }
                                                    </script>


                                                </div>
                                                <!-- RIGHT SIDE-->
                                                <div class="col-xs-6 box-body-cart">
                                                    <div class="customer-field">
                                                        <div class='form-group col-xs-12 no-pad'>
                                                            <label>Alamat Tinggal Sekarang</label>
                                                            <textarea id="address" name ='address' 
                                                                      class='form-control frmcust resize-none' rows="3" disabled><?php echo (isset($_SESSION[SESSION_PERSONAL_INFO]) ? $_SESSION[SESSION_PERSONAL_INFO]['address'] : '') ?></textarea>
                                                        </div>
                                                    </div>



                                                    <div class="customer-field">
                                                        <div class='form-group col-xs-12 no-pad'>
                                                            <label>Propinsi</label>
                                                            <input id="phone_no" name ="phone_no" 
                                                                   type='text' class='form-control frmcust' 
                                                                   value ="<?php echo (isset($_SESSION[SESSION_PERSONAL_INFO]) ? $_SESSION[SESSION_PERSONAL_INFO]['province_name'] : '') ?>" maxlength="18" disabled>
                                                        </div>
                                                    </div>


                                                    <div class="customer-field">
                                                        <div class='form-group col-xs-12 no-pad'>
                                                            <label>Kabupaten</label>
                                                            <input id="phone_no" name ="phone_no" 
                                                                   type='text' class='form-control frmcust' 
                                                                   value ="<?php echo (isset($_SESSION[SESSION_PERSONAL_INFO]) ? $_SESSION[SESSION_PERSONAL_INFO]["city_name"] : '') ?>" maxlength="18" disabled>
                                                        </div>
                                                    </div>


                                                    <div class="customer-field">
                                                        <div class='form-group col-xs-12 no-pad'>
                                                            <label>Kecamatan</label>
                                                            <input id="phone_no" name ="phone_no" 
                                                                   type='text' class='form-control frmcust' 
                                                                   value ="<?php echo (isset($_SESSION[SESSION_PERSONAL_INFO]["district_name"]) ? $_SESSION[SESSION_PERSONAL_INFO]["district_name"] : '') ?>" maxlength="18" disabled>
                                                        </div>
                                                    </div>


                                                    <div class="customer-field">
                                                        <div class='form-group col-xs-12 no-pad'>
                                                            <label>Kelurahan</label>
                                                            <input id="sub_district" name ="sub_district" 
                                                            <?php echo (isset($_SESSION[SESSION_CUSTOMER_DATA]->customer_seq) && $_SESSION[SESSION_CUSTOMER_DATA]->customer_seq != '' ? 'readonly' : '') ?> 
                                                                   type='text' class='form-control frmcust' 
                                                                   value ="<?php echo (isset($_SESSION[SESSION_CUSTOMER_DATA]->sub_district) ? $_SESSION[SESSION_CUSTOMER_DATA]->sub_district : '') ?>" maxlength="18">
                                                        </div>
                                                    </div>



                                                    <div class="customer-field">
                                                        <div class='form-group col-xs-12 no-pad'>
                                                            <label>Kode Pos</label>
                                                            <input id="zip_code" name='zip_code' 
                                                                   type='text' class='form-control frmcust' 
                                                                   value =" <?php echo (isset($_SESSION[SESSION_PERSONAL_INFO]) && $_SESSION[SESSION_PERSONAL_INFO]["postal_code"] != "" ? $_SESSION[SESSION_PERSONAL_INFO]["postal_code"] : (isset($_SESSION[SESSION_CUSTOMER_DATA]->zip_code) ? $_SESSION[SESSION_CUSTOMER_DATA]->zip_code : "")); ?>" maxlength="10" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <script text="javascript">
                                                    $(document).ready(function () {
                                                        $("#customer_seq").select2({
                                                            ajax: {
                                                                url: "<?php echo get_base_url() . "member/checkout" ?>",
                                                                dataType: 'json',
                                                                delay: 250,
                                                                method: 'POST',
                                                                data: function (params) {
                                                                    return {
                                                                        q: params.term,
                                                                        type: "<?php echo TASK_CUSTOMER_SEARCH; ?>"
                                                                    };
                                                                },
                                                                processResults: function (data) {
                                                                    return {
                                                                        results: $.map(data.items, function (items) {
                                                                            return {
                                                                                text: items.pic_name,
                                                                                id: items.seq
                                                                            }
                                                                        })
                                                                    };
                                                                },
                                                            },
                                                            minimumInputLength: 1
                                                        });

                                                    });
                                                    function add_new_customer() {
                                                        $(".frmcust").prop("readonly", false);
                                                        $("#drop_province").val("").trigger("change");
                                                        $("#customer_seq").val("").trigger("change");
                                                    }

                                                    function change_province() {
                                                        var province_seq = $('#drop_province').val();
                                                        $.ajax({
                                                            url: "<?php echo get_base_url() . "member/checkout" ?>",
                                                            type: "POST",
                                                            dataType: "json",
                                                            data: {
                                                                "type": "<?php echo TASK_PROVINCE_CHANGE ?>",
                                                                "province_seq": province_seq
                                                            },
                                                            success: function (data) {
                                                                if (isSessionExpired(data)) {
                                                                    response_object = json_decode(data);
                                                                    url = response_object.url;
                                                                    location.href = url;
                                                                } else {
                                                                    $('#drop_city option').remove();
                                                                    $('#drop_district option').remove();
                                                                    $("#drop_city").append("<option value=\"" + "\">-- Pilih --</option>");
                                                                    $(data).each(function (i) {
                                                                        $("#drop_city").append("<option value=\"" + data[i].seq + "\">" + data[i].name + "</option>")
                                                                    });
                                                                }
                                                            },
                                                            error: function (request, error) {
                                                                alert(error_response(request.status));
                                                            }
                                                        });
                                                    }

                                                    function change_city() {
                                                        var city_seq = $('#drop_city').val();
                                                        $.ajax({
                                                            url: "<?php echo get_base_url() . "member/checkout" ?>",
                                                            type: "POST",
                                                            dataType: "json",
                                                            data: {
                                                                "type": "<?php echo TASK_CITY_CHANGE ?>",
                                                                "city_seq": city_seq
                                                            },
                                                            success: function (data) {
                                                                if (isSessionExpired(data)) {
                                                                    response_object = json_decode(data);
                                                                    url = response_object.url;
                                                                    location.href = url;
                                                                } else {
                                                                    $('#drop_district option').remove();
                                                                    $("#drop_district").append("<option value=\"" + "\">-- Pilih --</option>");
                                                                    $(data).each(function (i) {
                                                                        $("#drop_district").append("<option value=\"" + data[i].seq + "\">" + data[i].name + "</option>")
                                                                    });
                                                                }
                                                            },
                                                            error: function (request, error) {
                                                                alert(error_response(request.status));
                                                            }
                                                        });
                                                    }

                                                    $('#customer_seq').on("select2:select", function (e) {
                                                        change_customer();
                                                    });

                                                    function change_customer() {
                                                        var customer_seq = $('#customer_seq').select2("val");
                                                        $(".frmcust").prop("readonly", true);
                                                        $.ajax({
                                                            url: "<?php echo get_base_url() . "member/checkout" ?>",
                                                            type: "POST",
                                                            dataType: "json",
                                                            data: {
                                                                "type": "<?php echo TASK_CUSTOMER_DATA; ?>",
                                                                "customer_seq": customer_seq
                                                            },
                                                            success: function (data) {
                                                                if (isSessionExpired(data)) {
                                                                    response_object = json_decode(data);
                                                                    url = response_object.url;
                                                                    location.href = url;
                                                                } else {
                                                                    $(data[0]).each(function (i) {

                                                                        $('#drop_city option').remove();
                                                                        $('#drop_district option').remove();
                                                                        $('#pic_name').val(data[0][i].pic_name);
                                                                        $('#birthday').val(data[0][i].birthday);
                                                                        $('#pic_email').val(data[0][i].email_address);
                                                                        $('#sub_district').val(data[0][i].sub_district);
                                                                        $('#identity_address').val(data[0][i].identity_address);
                                                                        $('#address').val(data[0][i].address);
                                                                        $('#zip_code').val(data[0][i].zip_code);
                                                                        $('#phone_no').val(data[0][i].phone_no);
                                                                        $('#identity_no').val(data[0][i].identity_no);
                                                                        $("#drop_province").val(data[0][i].province_seq).trigger("change");
                                                                        setTimeout(function () {
                                                                            $("#drop_city").val(data[0][i].city_seq).trigger("change");
                                                                        }, 750);

                                                                        setTimeout(function () {
                                                                            $("#drop_district").val(data[0][i].district_seq).trigger("change");
                                                                        }, 1500);
                                                                    });
                                                                }
                                                            },
                                                            error: function (request, error) {
                                                                alert(error_response(request.status));
                                                            }
                                                        });
                                                    }
                                                </script>
                                            </div>

                                            <?php
                                        }
                                    }
                                }
                                ?>
                                <div class="row">
                                    <div class="col-xs-12 no-pad">
                                        <div class="pull-right"><br>
                                            <?php
                                            if ($payment_info["payment_seq"] == PAYMENT_SEQ_ADIRA || $payment_info["payment_seq"] == PAYMENT_SEQ_ADIRA_DEPOSIT) {
                                                if (isset($info_product_loan)) {
                                                    if ($info_product_loan != '') {
                                                        ?>
                                                        <div class="col-xs-3 col-xs-9">
                                                            <button type="submit" class ='btn btn-confirm btn-flat'>Ajukan Kredit</button>
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            <?php } else { ?>
                                                <button type="submit" class ='btn btn-confirm'>Proses Pembayaran</button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal fade" tabindex="-1" role="dialog" id="myModal">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Pembayaran dengan cicilan</h4>
                            </div>
                            <div class="modal-body">
                                <p><?php echo DEFAULT_CREDIT_DESCRIPTION; ?></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                                <button type="button" class="btn btn-primary" onclick="submit_cr();">Ya</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </div>
        </div>
        <script text="javascript">
            var product_seq = '<?php if (sizeof($data_prod) == 1) echo $product_seq; ?>';
        <?php if ($payment_info["payment_seq"] == PAYMENT_SEQ_ADIRA || $payment_info["payment_seq"] == PAYMENT_SEQ_ADIRA_DEPOSIT) { ?>
                $(document).ready(function () {
                    $("#customer_seq").select2({
                        ajax: {
                            url: "<?php echo get_base_url() . "member/checkout" ?>",
                            dataType: 'json',
                            delay: 250,
                            method: 'POST',
                            data: function (params) {
                                return {
                                    q: params.term,
                                    type: "<?php echo TASK_CUSTOMER_SEARCH; ?>"
                                };
                            },
                            processResults: function (data) {
                                return {
                                    results: $.map(data.items, function (items) {
                                        return {
                                            text: items.pic_name + " / " + items.identity_no,
                                            id: items.seq
                                        }
                                    })
                                };
                            },
                        },
                        minimumInputLength: 1
                    });
                    check_interest();
                    calculate();
                });

                function check_interest() {
                    var interest = $("#tenor option:selected").attr("data-bunga");
                    var tenor_value = $("#tenor option:selected").attr("data-tenor");
                    $("#tenor_value").val(tenor_value);
                    $("#persen_bunga").html(interest);
                    $("#loan_interest").val(interest);
                }

                function calculate() {
                    var tenor = $("#tenor").val();
                    if (tenor == 0) {
                        alert('Silahkan pilih tenor terlebih dahulu.');
                        return;
                    }
                    var harga_barang = <?php echo $product_price_total; ?>;
                    var dp = $("#dp").autoNumeric('get');

                    if (dp > harga_barang) {
                        alert("Uang Muka Tidak Boleh Lebih besar Dari Harga Barang.");
                        return;
                    }

                    $.ajax({
                        url: "<?php echo get_base_url() . "member/checkout" ?>",
                        type: "POST",
                        dataType: "json",
                        data: {
                            "type": "<?php echo TASK_LOAN_SIMULATION; ?>",
                            "product_seq": product_seq,
                            "tenor": tenor,
                            "total": harga_barang,
                            "expedisi": "<?php echo $shipping_total; ?>",
                            "dp": dp
                        },
                        success: function (data) {
                            if (isSessionExpired(data)) {
                                response_object = json_decode(data);
                                url = response_object.url;
                                location.href = url;
                            } else {
                                if (data.error != true) {
                                    $("#nilai_dp").html(data.min_dp_value_num);
                                    $("#nilai_dp_2").html(data.min_dp_value_num);
                                    $("#angsuran_pertama").html(data.angsuran);
                                    $("#total_pertama").html(data.total_pay_1);
                                    $("#angsuran").html(data.angsuran);
                                    $("#pokok_hutang").html(data.ph);
                                    //$("#min_dp").html(data.min_dp_error);
                                    $("#dp").attr("value", data.min_dp_value);
                                    $("#installment").val(data.angsurans);
                                    $("#total_installment").val(data.total_pay_1s);

                                } else {
                                    alert(data.message);
                                }

                            }
                        },
                        error: function (request, error) {
                            alert(error_response(request.status));
                        }
                    });
                }
        <?php } ?>
            function submit_cr() {
        <?php if ($payment_info["payment_seq"] == PAYMENT_SEQ_CREDIT && sizeof($data_prod) > 1) { ?>
                    $.ajax({
                        url: "<?php echo base_url() . "member/checkout"; ?>",
                        type: "POST",
                        data: {type: "<?php echo TASK_GET_ONE_CREDIT; ?>",
                            product_seq: product_seq},
                        success: function (data) {
                            window.location.href = "<?php echo base_url() . "member/checkout?step=3"; ?>";
                        },
                        error: function (request, error) {
                            alert(error_response(request.status));
                        }
                    })
        <?php } ?>
            }
            function clear_cr() {
                var myElem = document.getElementById('monthly_pay');
                if (myElem !== null) {
                    show_cr(product_seq);
                }
            }
            function close_cr() {
                $("#rowcredit").html('');
            }
            function show_cr(productvar) {
                product_seq = productvar;
                $htmls = '';
        <?php if ($payment_info["payment_seq"] == PAYMENT_SEQ_CREDIT && sizeof($data_prod) > 1) { ?>
                    $('#myModal').modal('show');

        <?php } ?>
            }
        <?php if ($payment_info["payment_seq"] == PAYMENT_SEQ_CREDIT && sizeof($data_prod) > 1) { ?>
                $(".cicils").popover(
                        {placement: "left", trigger: "hover",
                            title: "Informasi Cicilan",
                            content: "Pembelian dengan cicilan hanya diperbolehkan 1 produk per order"
                        });
                $(".cicils").popover("show");
            <?php
        }
        if ($show_kredit_pop == "show") {
            ?>
                $(".cicilspop").popover(
                        {placement: "bottom", trigger: "hover",
                            title: "Informasi Cicilan",
                            content: "Pilih periode cicilan"
                        });
                $(".cicilspop").popover("show");
        <?php }
        ?>
            function change_total() {
                period = $('#promo_credit_seq option:selected').attr('nominal');
                var total_cr = $('#rowcredit').attr('total_credit');
                if (period > 0) {
                    $('#monthly_pay').text('Rp ' + numberWithCommas(Math.round(total_cr / period)));
                } else {
                    $('#monthly_pay').text(numberWithCommas(total_cr));
                }
            }
            function voucher_change(id) {
                var voucher = $('#' + id).val();
                //    	    $('select').select2();
                var total = $('#total').attr('nominal');
                var voucher = $('#drop_voucher option:selected').attr('nominal');
                clear_cr();
                if (total - voucher > 0) {
                    $('#rowcredit').attr('total_credit', (total - voucher));
                    $('#total').text(numberWithCommas(total - voucher));
                } else {
                    $('#rowcredit').attr('total_credit', 0);
                    $('#total').text(0);
                }
            }
            function numberWithCommas(x) {
                return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }

            function set_value(id) {
                var text = $('#' + id).val();
            }
        <?php if ($coupon == true) { ?>
                function get_coupon() {
                    var coupon_code = $('#coupon_code').val();
                    $.ajax({
                        url: "<?php echo base_url() . "member/checkout" ?>",
                        type: "POST",
                        dataType: "json",
                        data: {type: "<?php echo TASK_GET_COUPON ?>",
                            coupon_code: coupon_code,
                            total_order: $('#total').attr('nominal'),
                            product_variant_seq: "<?php echo $product_variant_string; ?>"},
                        success: function (data) {
                            if (isSessionExpired(data)) {
                                response_object = json_decode(data);
                                url = response_object.url;
                                location.href = url;
                            } else {
                                if (data.error == "true") {
                                    $('#coupon_field').addClass('text-red');
                                    $('#coupon_field').text('Kode yang anda masukan tidak valid silahkan masukan kembali');
                                } else {
                                    var total = $('#total').attr('nominal');
                                    $('#coupon_field').removeClass('has-success');
                                    $('#coupon_field').removeClass('has-error');
                                    $('#coupon_field').addClass('has-success');
                                    $('#coupon_code').attr('readonly', true);
                                    $('#apply').removeClass('btn-success');
                                    $('#apply').addClass('btn-danger');
                                    $("#apply").attr("onclick", "clear_coupon()");
                                    $('#apply').text("Hapus");
                                    $('#drop_voucher').val('0');
                                    $('#drop_voucher').prop('disabled', true);
                                    $('select').select2();
                                    $('#coupon_field').addClass('text-green');
                                    $('#coupon_field').text('Anda mendapatkan potongan sebesar Rp ' + numberWithCommas(data[0].nominal));
                                    clear_cr();
                                    if (total - data[0].nominal > 0) {
                                        $('#total').text(numberWithCommas(total - data[0].nominal));
                                        $('#rowcredit').attr('total_credit', (total - data[0].nominal));
                                    } else {
                                        $('#total').text(0);
                                        $('#rowcredit').attr('total_credit', 0);
                                    }
                                }
                            }
                        }
                    });
                }

                function set_coupon(id) {
                    var coupon = $('#' + id).val();
                }

                function clear_coupon() {
                    $.ajax({
                        url: "<?php echo base_url() . "member/checkout" ?>",
                        type: "POST",
                        dataType: "json",
                        data: {type: "<?php echo TASK_CLEAR_COUPON ?>"},
                        success: function () {
                            var total = $('#total').attr('nominal');
                            $('#coupon_code').val('');
                            $('#coupon_code').attr('readonly', false);
                            $('#coupon_code').removeClass('text-green');
                            $('#apply').removeClass('btn-danger');
                            $('#apply').addClass('btn-success');
                            $('#apply').text("Terapkan");
                            $("#apply").attr("onclick", "get_coupon()");
                            $('#drop_voucher').prop('disabled', false);
                            $('#coupon_field').text('');
                            $('#total').text(numberWithCommas(total));
                            $('#rowcredit').attr('total_credit', total);
                            clear_cr();
                        }
                    });
                }

                $(function () {
                    $("#popbadge1").popover('show');
                    // Now it has
                    shownOnce = true;
                });
        <?php } ?>
        </script>
    <?php } ?>
