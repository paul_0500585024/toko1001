<?php
require_once VIEW_BASE_HOME;
$getSearchHarga = $this->input->get('pr');
$getMinPrice = 0;
$getMaxPrice = 99999999;
if ($getSearchHarga !== NULL) :
    $searchPrice = explode(',', $getSearchHarga);
    $getMinPrice = isset($searchPrice[0]) ? $searchPrice[0] : 0;
    $getMaxPrice = isset($searchPrice[1]) ? $searchPrice[1] : 99999999;
endif;
$order_category = $this->input->get('g');
?>
<div class="container">
    <div class="cst-breadcrumb">
        <div class="cst-breadcrumb-inside">
            <a href="<?php echo base_url(); ?>"><i class="fa fa-home"style="font-size:1.5em;"></i></a>
        </div>
        <div class="cst-breadcrumb-inside">
            <img src="<?php echo get_image_location() . 'assets/img/breadcrumb.png' ?>">
        </div>
        <a class="wsh" href='<?php echo $link ?>'><?php echo $breadcrumb_pages ?></a>
    </div>
    <div class="col-xs-12 no-pad no-margin mb-10px">
        <div class="row no-pad no-margin">
            <div class="col-xs-12 no-pad no-margin">
                <img class="full-img" src="<?php echo get_image_location() . 'assets/img/home/'; ?>credit_bca_0.jpg" alt="">
            </div>
        </div>
    </div>
    <div class="col-xs-2 no-pad">
        <form class="form" id="formLogin" method="get" action="<?php echo current_url_with_query_string(array(PRICE_RANGE, START_OFFSET)) ?>">
            <input type="text" value="" class="slider form-control"
                   id="harga" name="" data-slider-min="0"
                   data-slider-max="99999999" data-slider-step="10000"
                   data-slider-value="[<?php echo $getMinPrice ?>,<?php echo $getMaxPrice ?>]"
                   data-slider-orientation="horizontal" style="display: block;"
                   data-slider-selection="before" data-slider-tooltip="show"
                   data-slider-id="grey">
            <div id="slideprice"></div>
            <div class="col-xs-6 p-3px">
                <input type="text" id="minrange"  value="<?php echo (isset($getSearchHarga) ? number_format($searchPrice[0]) : number_format("0") ); ?>" class="form-control numeric" maxlength="9" style="padding: 9px !important">
            </div>
            <div class="col-xs-6 p-3px">
                <input type="text" id="maxrange"  value="<?php echo (isset($getSearchHarga) ? number_format($searchPrice[1]) : number_format("99999999") ); ?>" class="form-control numeric" maxlength="9" style="padding: 9px !important">
            </div>
            <input type="hidden" id="pricerange" name="pr" value="0,99999999" class="form-control">
            <div class="mt-10px">
                <button type="submit" id="btnLogin" class="btn btn-block btn-flat btn-green" ><i class="fa fa-search"></i></button>
            </div>
        </form>
    </div>
    <div class="col-xs-10">
        <div class="row">
            <div class="col-xs-12">
                <div class="col-xs-10">
                </div>
                <div class="col-xs-2 no-pad mb-10px">
                    <button type="button" class="btn dropdown-toggle pull-right no-border-radius btn-normal width-210" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                        Urutkan
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu btn-normal pull-right cst-zindex width-210">
                        <li>
                            <?php if ($order_category == NEW_TO_OLD_PRODUCT): ?>
                                <a href="<?php echo current_url_with_query_string(array(ORDER_CATEGORY, START_OFFSET), array(ORDER_CATEGORY => NEW_TO_OLD_PRODUCT)) ?>" class="cst-option-act">Produk terbaru ke terlama</a>
                            <?php else: ?>
                                <a href="<?php echo current_url_with_query_string(array(ORDER_CATEGORY, START_OFFSET), array(ORDER_CATEGORY => NEW_TO_OLD_PRODUCT)) ?>" class="cst-option">Produk terbaru ke terlama</a>
                            <?php endif; ?>
                        </li>
                        <li>
                            <?php if ($order_category == OLD_TO_NEW_PRODUCT): ?>
                                <a href="<?php echo current_url_with_query_string(array(ORDER_CATEGORY, START_OFFSET), array(ORDER_CATEGORY => OLD_TO_NEW_PRODUCT)) ?>" class="cst-option-act">Produk terlama ke terbaru</a>
                            <?php else: ?>
                                <a href="<?php echo current_url_with_query_string(array(ORDER_CATEGORY, START_OFFSET), array(ORDER_CATEGORY => OLD_TO_NEW_PRODUCT)) ?>" class="cst-option">Produk terlama ke terbaru</a>
                            <?php endif; ?>
                        </li>
                        <li>
                            <?php if ($order_category == PRICE_EXPENSIVE_TO_CHEAP): ?>
                                <a href="<?php echo current_url_with_query_string(array(ORDER_CATEGORY, START_OFFSET), array(ORDER_CATEGORY => PRICE_EXPENSIVE_TO_CHEAP)) ?>" class="cst-option-act">Harga mahal ke murah</a>
                            <?php else: ?>
                                <a href="<?php echo current_url_with_query_string(array(ORDER_CATEGORY, START_OFFSET), array(ORDER_CATEGORY => PRICE_EXPENSIVE_TO_CHEAP)) ?>" class="cst-option">Harga mahal ke murah</a>
                            <?php endif; ?>
                        </li>
                        <li>
                            <?php if ($order_category == PRICE_CHEAP_TO_EXPENSIVE): ?>
                                <a href="<?php echo current_url_with_query_string(array(ORDER_CATEGORY, START_OFFSET), array(ORDER_CATEGORY => PRICE_CHEAP_TO_EXPENSIVE)) ?>" class="cst-option-act">Harga murah ke mahal</a>
                            <?php else: ?>
                                <a href="<?php echo current_url_with_query_string(array(ORDER_CATEGORY, START_OFFSET), array(ORDER_CATEGORY => PRICE_CHEAP_TO_EXPENSIVE)) ?>" class="cst-option">Harga murah ke mahal</a>
                            <?php endif; ?>
                        </li>
                    </ul>
                </div>
            </div>
            <?php
            if (count($data_sel[LIST_DATA]['product']) > 0) {
                echo display_product($data_sel[LIST_DATA]['product']);
            } else {
                echo '<div class="well credit-empty-produk"><center>Maaf Produk Yang Anda Cari Tidak Di Temukan</center></div>';
            }
            ?>
            <div class="col-xs-12">
                <center>
                    <?php echo $this->pagination->create_links(); ?>
                </center>
            </div>
        </div>
    </div>
</div>

<script>
    (function ($) {
        $.fn.extend({
            formatInput: function (settings) {
                var $elem = $(this);
                settings = $.extend({
                    errback: null
                }, settings);
                $elem.bind("keyup.filter_input", $.fn.formatEvent);
            },
            formatEvent: function (e) {
                var valueDecimal = initial_value.split(".");
                var formatedValue = $.fn.insertSpaces(valueDecimal[0]);
                var finalValue;
                if (valueDecimal.length > 1) {
                    finalValue = formatedValue + "." + valueDecimal[1];
                } else {
                    finalValue = formatedValue;
                }
                elem.val(finalValue);
                elem.trigger('change');
            },
            insertSpaces: function (number) {
                return number.replace(" ", "").replace(/(\d)(?=(?:\d{3})+$)/g, "$1 ");
            }
        });
    })(jQuery);

    $(document).ready(function () {
        $('.numeric').bind('keyup filter_input', function () {
            $('#minrange').extends();
        });
    });

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    $('#harga').on('slide slideStart slideStop', function () {
        var range = $('#harga').val();
        var subrange = range.split(',');
        $('#minrange').val(numberWithCommas(subrange[0]));
        $('#maxrange').val(numberWithCommas(subrange[1]));
    });

    $('#harga').on('slideStop', function () {
        var price1 = $('#minrange').val().replace(/\D/g, '');
        var price2 = $('#maxrange').val().replace(/\D/g, '');
        $('#pricerange').val(price1 + "," + price2);
    })

    $('#minrange, #maxrange').on('change blur keyup', function () {
        var price1 = $('#minrange').val();
        var price2 = $('#maxrange').val();
        $('#pricerange').val(price1 + "," + price2);
    })

    $('.productDetails').mouseenter(function () {
        var title = 'title-' + $(this).attr('data-img');
        $('#' + title).removeClass('no-show');
    });

    $('.productDetails').mouseleave(function () {
        var title = 'title-' + $(this).attr('data-img');
        $('#' + title).addClass('no-show');
    });
</script>


<?php $this->load->view(LIVEVIEW . 'home/template/segment_main/js_add_product.php'); ?>