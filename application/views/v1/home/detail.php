<?php

$dateConvert = function($date) {
    $data = [
        'Jan' => 'January',
        'Feb' => 'Febuari',
        'Mar' => 'Maret',
        'Apr' => 'April',
        'May' => 'Mei',
        'Jun' => 'juni',
        'Jul' => 'Juli',
        'Aug' => 'Agustus',
        'Sep' => 'September',
        'Oct' => 'Oktober',
        'Nov' => 'November',
        'Dec' => 'Desember'
    ];
    $particle = explode('-', $date);
    return '<span>' . $particle[0] . ', ' . $data[$particle[1]] . ' ' . $particle[2] . '</span>';
};


$total_product_review = isset($product_review['total_product_review']) ? $product_review['total_product_review'] : '';
$product_review = isset($product_review['product_review']) ? $product_review['product_review'] : '';
require_once VIEW_BASE_HOME;
$number_of_pic = 6;

function trim_text($input, $length, $ellipses = true, $strip_html = true) {
    if ($strip_html)
        $input = strip_tags($input);
    if (strlen($input) <= $length)
        return $input;
    $last_space = strrpos(substr($input, 0, $length), ' ');
    $trimmed_text = substr($input, 0, $last_space);
    if ($ellipses)
        $trimmed_text .= ' ... ';
    return $trimmed_text;
}
?>


<div class="container">
    <div class="row">
        <div class="col-xs-12" itemscope itemtype="http://schema.org/BreadcrumbList">
            <?php
            $number_of_category = count($seq_self_parent_category);
            $counter_category = 0;
            ?>
            <div class="cst-breadcrumb">
                <div class="cst-breadcrumb-inside">
                    <a href="<?php get_base_url() ?>"><i class="fa fa-home font-sz-1"></i></a>
                </div>
                <div class="cst-breadcrumb-inside">
                    <img src="<?php echo get_image_location() . 'assets/img/breadcrumb.png' ?>">
                </div>
                <?php foreach ($seq_self_parent_category as $product_category_seq => $each_seq_self_parent_category): ?>
                    <div class="cst-breadcrumb-inside" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">

                        <a itemprop="item" href="<?php echo base_url() . url_title(strtolower($each_seq_self_parent_category)) . "-" . CATEGORY . ($product_category_seq) ?>">
                            <span itemprop="name"><?php echo $each_seq_self_parent_category; ?></span>
                        </a>
                    </div>
                    <?php if ($counter_category < $number_of_category - 1): ?>
                        <div class="cst-breadcrumb-inside">
                            <img src="<?php echo get_image_location() . 'assets/img/breadcrumb.png' ?>">
                        </div>
                    <?php endif; ?>
                    <?php $counter_category++; ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-9">
            <h1><?php
                $productname = $detail_description->name . get_variant_value($detail_description->variant_seq, $detail_description->variant_value, "-");
                echo $productname;
                ?>
            </h1>
            <div class="row">
                <div class="col-xs-6">
                    <div class="pagi-container" >
                        <?php
                        for ($i = 1; $i <= $number_of_pic; $i++):
                            ?>
                            <?php if ($detail_description->{"image" . $i} != ""): ?>
                                <input type="radio" name="input_thumb" id='input_detail_big_thumb<?php echo $i; ?>'>
                                <label for='id_detail_big_thumb<?php echo $i; ?>'>
                                    <img
                                        src='<?php
                                        $productimage = PRODUCT_UPLOAD_IMAGE . "/" . $detail_description->merchant_seq . "/" . XL_IMAGE_UPLOAD . "/" . $detail_description->{"image" . $i};
                                        echo get_file_exists($productimage);
                                        ?>' alt="<?php echo $detail_description->{"image" . $i} ?>" height="50px " id='id_detail_big_thumb<?php echo $i; ?>'>
                                </label>
                                <img
                                    data-src='<?php echo get_file_exists(PRODUCT_UPLOAD_IMAGE . "/" . $detail_description->merchant_seq . "/" . XL_IMAGE_UPLOAD . "/" . $detail_description->{"image" . $i}); ?>' alt="<?php echo $detail_description->{"image" . $i}; ?>"
                                    src='<?php echo get_file_exists(PRODUCT_UPLOAD_IMAGE . "/" . $detail_description->merchant_seq . "/" . XL_IMAGE_UPLOAD . "/" . $detail_description->{"image" . $i}); ?>' class="DefaultBigStyle1" style="opacity:0!important;" id="id_data_small_thumb<?php echo $i; ?>" style="opacity:1;">
                                <?php endif; ?>
                            <?php endfor; ?>
                    </div>
                </div>
                <div class="col-xs-6 border">
                    <div class="detail">
                        <div class='col-xs-12 no-pad'>
                            <?php if ($detail_description->product_price != $detail_description->sell_price): ?>
                                <div class="col-xs-12 no-pad">
                                <?php endif; ?>
                                <span class="new_price">
                                    <?php echo RP, cnum($detail_description->sell_price); ?>
                                </span>
                                <?php if ($detail_description->product_price != $detail_description->sell_price): ?>
                                    <span class="detail_diskon">
                                        Hemat <?php echo get_percentage_discount($detail_description->product_price, $detail_description->sell_price); ?>
                                    </span><br/>
                                </div>
                                <div class="col-xs-12 no-pad">
                                    <span class="old_price">
                                        <?php echo RP, cnum($detail_description->product_price); ?>
                                    </span>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="col-xs-12 no-pad">
                            <?php if (is_agent() && is_agent_view()) { ?>
                                Komisi Agent :&nbsp;
                                <span class="komisi-price"><?php echo ($detail_description->commission_fee === NULL ? '-' : RP . number_format(floor($detail_description->commission_fee))); ?>
                                </span>
                            <?php } ?>
                        </div>
                        <br/>
                        <div class="col-xs-12 no-pad">
                            <p class="detail_title">Dijual oleh : <a href="<?php echo base_url() . 'merchant/' . $detail_description->code; ?>"><?php echo $detail_description->merchant_name ?></a></p>
                        </div>
                        <div class="col-xs-12">
                            <ul>
                                <?php if ($detail_description->guarante != '') { ?>
                                    <li>Garansi :  <?php echo isset($detail_description->guarante) ? $detail_description->guarante : '-'; ?> </li>
                                <?php } ?>
                                <?php if ($detail_description->weight != '') { ?>
                                    <li>Berat : <?php echo isset($detail_description->weight) ? $detail_description->weight . '&nbsp;Kg' : '-' ?> </li>
                                <?php } ?>
                                <?php if ($detail_description->dimension_lenght != '' AND $detail_description->dimension_lenght != 0 AND $detail_description->dimension_width != '' AND $detail_description->dimension_width != 0 AND $detail_description->dimension_height != '' AND $detail_description->dimension_height != 0) { ?>
                                    <li>Dimensi (PxLxT) :<?php
                                        echo isset($detail_description->dimension_lenght) ? round($detail_description->dimension_lenght) . ' cm x ' : "-";
                                        echo isset($detail_description->dimension_width) ? round($detail_description->dimension_width) . ' cm x ' : "-";
                                        echo isset($detail_description->dimension_height) ? round($detail_description->dimension_height) . ' cm' : "-";
                                        ?>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>

                        <div class="pt-10px col-xs-12 mb-5px no-pad">
                            <?php if (isset($product_related)): ?>
                                <strong class="pr-20px">Pilihan :</strong><br>
                                <?php if (count($product_related) > 5): ?>
                                    <div>
                                        <div id="slider-next" class="pull-right mt-20px"></div>
                                        <div id="slider-prev" class="pull-left mt-20px"></div>
                                    </div>
                                    <ul id="bxslider_horizontal_related" class="bxslider">
                                        <?php echo display_product_slide_image($product_related) ?>
                                    </ul>
                                <?php else: ?>
                                    <?php foreach ($product_related as $each_product_related): ?>
                                        <a href="<?php echo base_url() . strtolower(url_title($each_product_related->name . ' ' . $each_product_related->variant_value)) . '-' . $each_product_related->product_variant_seq ?>">
                                            <img class="img-terbaru v-align-top"
                                                 src="<?php echo get_file_exists(PRODUCT_UPLOAD_IMAGE . $each_product_related->merchant_seq . '/' . XL_IMAGE_UPLOAD . "/" . $each_product_related->image) ?>"
                                                 alt="blank" height="50px">
                                        </a>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>

                        <?php if (!is_agent()) { ?>
                            <?php if ($detail_description->store_status == OPEN_STORE) { ?>
                                <?php if ($detail_description->status_credit === "A" || $detail_description->pc_status === "A"): ?>
                                    <img src="<?php echo get_image_location() . CREDIT_IMG_FULL_PATH ?>" class="credit-image">
                                <?php endif; ?>
                                <?php
                            }
                        }
                        ?>

                        <?php /*
                          <div class="pt-10px">
                          <strong class="pr-20px">Pilihan </strong>

                          <?php foreach ($product_related as $each_product_related): ?>

                          <a href="<?php echo base_url() . strtolower(url_title($each_product_related->name . ' ' . $each_product_related->variant_value)) . '-' . $each_product_related->product_variant_seq ?>">
                          <img class="img-terbaru v-align-top"
                          src="<?php echo get_file_exists(PRODUCT_UPLOAD_IMAGE . $each_product_related->merchant_seq . '/' . XL_IMAGE_UPLOAD . "/" . $each_product_related->image) ?>"
                          alt="blank" height="50px">
                          </a>
                          <?php endforeach; ?>

                          </div>
                         * 
                         */ ?>
                        <div class="add-top-margin"></div>

                        <?php if (!isset($_SESSION[SESSION_MEMBER_CSRF_TOKEN]) && !isset($_SESSION[SESSION_AGENT_CSRF_TOKEN])) { ?>
                            <?php if ($detail_description->stock == '' OR $detail_description->stock <= 0) { ?>
                                <div class="row">
                                    <div class="col-xs-12"><div class="no_stock"><p class="text-bold">Stock Tidak tersedia</p></div>
                                        <button disabled class="btn btn-block btn-front btn-default"> Beli</button>
                                        <br />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <button disabled class="btn btn-block btn-front btn-default"> Tambah ke Keranjang</button>
                                    </div>
                                    <div class="col-xs-6">
                                        <button disabled class="btn btn-block btn-front btn-default"> Tambah ke Wishlist</button>
                                    </div>
                                </div>
                                <?php
                            } else {
                                ?>
                                <?php if ($detail_description->store_status !== OPEN_STORE) { ?>
                                    <div class="row">
                                        <div class="col-xs-12 m-bottom-20px">
                                            <div class="col-xs-12">
                                                <p class="text-bold text-orange text-center c-store">Maaf, toko sedang tutup. <br>buka kembali tanggal <?php echo $dateConvert(cdate($detail_description->to_date)) ?></p>
                                            </div>
                                            <div class="col-xs-12">
                                                <button onClick ="login_register()" class="btn btn-block btn-front btn-default"> Tambah ke Wishlist</button>
                                            </div>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <div class="row">
                                        <div class="col-xs-12 m-bottom-20px">
                                            <button onClick ="login_register()" class="btn btn-block btn-green">Beli</button>
                                        </div>
                                        <div class="col-xs-6">
                                            <button type="button" onClick ="login_register()"  class="btn btn-block btn-default btn-front">Tambah ke Keranjang</button>
                                        </div>
                                        <div class="col-xs-6">
                                            <button onclick="login_register()" class="btn btn-block btn-default">Wishlist</button>
                                            <?php // echo ($wishlist == ' <a href="?q=aw" class="btn btn-info btn-sm"><i class="fa fa-plus-circle"></i> Tambah ke Wishlist</a>' ? '<a href="?q=aw" class="btn btn-block btn-front btn-default"> Tambah ke Wishlist</a>' : '<a href="?q=dw" class="btn btn-block btn-danger"> Hapus dari Wishlist</a>');    ?>
                                        </div>
                                    </div>
                                <?php } ?>


                                <?php
                            }
                        } else {
                            if ($detail_description->stock == '' OR $detail_description->stock <= 0) {
                                if (isset($_SESSION[SESSION_AGENT_CSRF_TOKEN])) {
                                    ?>
                                    <div class="row">
                                        <div class="col-xs-12"><p class="text-bold">Stock Tidak tersedia</p></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <button disabled  class="btn btn-block btn-front btn-default"> Beli</button>
                                            <br />
                                        </div>
                                        <div class="col-xs-6">
                                            <button disabled  class="btn btn-block btn-front btn-default"> Tambah ke Keranjang</button><br />
                                        </div>
                                    </div>
                                    <?php
                                } else {
                                    ?>
                                    <div class="row">
                                        <div class="col-xs-12"><p class="text-bold">Stock Tidak tersedia</p>
                                            <button disabled  class="btn btn-block btn-front btn-default"> Beli</button>
                                            <br />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <button disabled  class="btn btn-block btn-front btn-default"> Tambah ke Keranjang</button>
                                        </div>
                                        <div class="col-xs-6">
                                            <?php echo ($wishlist == ' <a href="?q=aw" class="btn btn-info btn-sm"><i class="fa fa-plus-circle"></i> Tambah ke Wishlist</a>' ? '<a href="?q=aw" class="btn btn-block btn-front btn-default"> Tambah ke Wishlist</a>' : '<a href="?q=dw" class="btn btn-block btn-danger"> Hapus dari Wishlist</a>'); ?>
                                        </div>
                                    </div>
                                    <?php
                                }
                            } else {
                                if (isset($_SESSION[SESSION_AGENT_CSRF_TOKEN])) {
                                    ?>

                                    <?php if ($detail_description->store_status !== OPEN_STORE) { ?>
                                        <div class="row">
                                            <div class="col-xs-12 m-bottom-20px">
                                                <div class="col-xs-12">
                                                    <p class="text-bold text-orange text-center c-store">Maaf, toko sedang tutup. <br>buka kembali tanggal <?php echo $dateConvert(cdate($detail_description->to_date)) ?></p>
                                                </div>
                                                <div class="col-xs-12">
                                                    <?php echo ($wishlist == ' <a href="?q=aw" class="btn btn-info btn-sm"><i class="fa fa-plus-circle"></i> Tambah ke Wishlist</a>' ? '<a href="?q=aw" class="btn btn-block btn-front btn-default"> Tambah ke Wishlist</a>' : '<a href="?q=dw" class="btn btn-block btn-danger"> Hapus dari Wishlist</a>'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } else { ?>
                                        <div class="row">
                                            <div class="col-xs-12 m-bottom-20px">
                                                <button onClick ="<?php echo "add_product(" . $detail_description->product_variant_seq . ", true)" ?>" class="btn btn-block btn-green">Beli</button>
                                            </div>
                                            <div class="col-xs-6">
                                                <button type="button" onClick ="<?php echo "add_product(" . $detail_description->product_variant_seq . ")" ?>" class="btn btn-block btn-default btn-front">Tambah ke Keranjang</button>
                                            </div>
                                            <div class="col-xs-6">
                                                <?php echo ($wishlist == ' <a href="?q=aw" class="btn btn-info btn-sm"><i class="fa fa-plus-circle"></i> Tambah ke Wishlist</a>' ? '<a href="?q=aw" class="btn btn-block btn-front btn-default"> Tambah ke Wishlist</a>' : '<a href="?q=dw" class="btn btn-block btn-danger"> Hapus dari Wishlist</a>'); ?>
                                            </div>
                                        </div>
                                    <?php } ?>


                                    <?php
                                } else {
                                    ?>
                                    <?php if ($detail_description->store_status !== OPEN_STORE) { ?>
                                        <div class="row">
                                            <div class="col-xs-12 m-bottom-20px">
                                                <div class="col-xs-12">
                                                    <p class="text-bold text-orange text-center c-store">Maaf, toko sedang tutup. <br>buka kembali tanggal <?php echo $dateConvert(cdate($detail_description->to_date)) ?></p>
                                                </div>
                                                <div class="col-xs-12">
                                                    <?php echo ($wishlist == ' <a href="?q=aw" class="btn btn-info btn-sm"><i class="fa fa-plus-circle"></i> Tambah ke Wishlist</a>' ? '<a href="?q=aw" class="btn btn-block btn-front btn-default"> Tambah ke Wishlist</a>' : '<a href="?q=dw" class="btn btn-block btn-danger"> Hapus dari Wishlist</a>'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } else { ?>
                                        <div class="row">
                                            <div class="col-xs-12 m-bottom-20px">
                                                <button onClick ="<?php echo "add_product(" . $detail_description->product_variant_seq . ", true)" ?>" class="btn btn-block btn-green">Beli</button>
                                            </div>
                                            <div class="col-xs-6">
                                                <button type="button" onClick ="<?php echo "add_product(" . $detail_description->product_variant_seq . ")" ?>" class="btn btn-block btn-default btn-front">Tambah ke Keranjang</button>
                                            </div>
                                            <div class="col-xs-6">
                                                <?php echo ($wishlist == ' <a href="?q=aw" class="btn btn-info btn-sm"><i class="fa fa-plus-circle"></i> Tambah ke Wishlist</a>' ? '<a href="?q=aw" class="btn btn-block btn-front btn-default"> Tambah ke Wishlist</a>' : '<a href="?q=dw" class="btn btn-block btn-danger"> Hapus dari Wishlist</a>'); ?>
                                            </div>
                                        </div>

                                    <?php } ?>
                                    <?php
                                }
                            }
                        }
                        ?>
                        <div>
                            <h3>Overview</h3>
                            <?php
                            if (strlen($detail_description->description) > 380) {
                                echo trim_text($detail_description->description, 380) . '<a href="#" onclick="scrollToID(\'overview\')">Selengkapnya</a>';
                            } else {
                                echo $detail_description->description;
                            };
                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <?php if (isset($product_recent)) { ?>
                    <div class="col-xs-12">
                        <div class=" detail_title detail-title-padding">Produk yang telah dilihat :</div>
                        <div class="pull-left">
                            <ul id="bxslider_horizontal" class="bxslider">
                                <?php echo display_product_slide($product_recent) ?>
                            </ul>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="row">
                <div class="col-xs-12" id="overview">
                    <h3>Deskripsi Produk</h3>
                    <?php echo $detail_description->description; ?>
                </div>
            </div>
            <div class="row" id="spek_produk">
                <div class="col-xs-12">
                    <h3>Spesifikasi Produk</h3>
                    <table
                        class="field-group-format group_specs table table-striped table-bordered table-striped">
                        <tbody>
                            <?php
                            $adaspek = '';
                            if (isset($detail_specification)):
                                ?>
                                <?php foreach ($detail_specification as $each_detail_specification): ?>
                                    <?php if (isset($each_detail_specification->name) OR isset($each_detail_specification->value)): ?>
                                        <tr>
                                            <?php
                                            if (isset($each_detail_specification->name)):
                                                if ($each_detail_specification->name != '')
                                                    $adaspek = 'ok';
                                                ?>
                                                <th class="field-label"><?php echo $each_detail_specification->name ?></th>
                                            <?php endif; ?>
                                            <?php
                                            if (isset($each_detail_specification->value)):
                                                if ($each_detail_specification->value != '')
                                                    $adaspek = 'ok';
                                                ?>
                                                <td class="field-content">
                                                    <div class="commerce-product-field commerce-product-field-field-package-content field-field-package-content product-field-package-content">
                                                        <div class="field field-name-field-package-content field-type-text-long field-label-hidden">
                                                            <div class="field-items">
                                                                <div class="field-item even"><?php echo $each_detail_specification->value ?></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            <?php endif; ?>
                                        </tr>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            <?php if (isset($detail_description->specification)): ?>
                                <?php
                                if ($detail_description->specification != '') {
                                    $adaspek = 'ok';
                                    ?>
                                    <tr style="border: 1px solid #ddd;">
                                        <td colspan="2"><?php echo $detail_description->specification ?></td>
                                    </tr>
                                    <?php
                                }
                            endif;
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row pad-15">
                <div class="col-xs-12 box-header-cart">
                    <h3>Ulasan Produk<span class="f-size17px">&nbsp;(&nbsp;<?php echo isset($total_product_review) ? $total_product_review : '' ?>&nbsp;)</span></h3>
                    <?php if ($total_product_review == '0'): ?>
                        <p>Belum ada ulasan</p>
                    <?php else:
                        ?>
                        <?php foreach ($product_review as $each_product_review): ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <?php echo $each_product_review->name ?>&nbsp;&nbsp;<?php echo create_star($each_product_review->rate); ?>
                                    <?php $_a = date_create($each_product_review->created_date); ?>
                                    <span class="pull-right"><?php echo $dateConvert(date_format($_a, "j-M-Y")); ?></span>
                                    <p><?php echo $each_product_review->review_admin ?></p>
                                </div>
                            </div>
                            <hr>
                        <?php endforeach; ?>

                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="col-xs-3">
            <div class="row">				 
                <div class=" detail_title detail-title-padding">Bagikan Produk</div>
                <div class="st_facebook_custom col-xs-3" displayText='Facebook'></div>
                <div class="st_twitter_custom col-xs-3" displayText='Tweet'></div>
                <div class="st_pinterest_custom col-xs-3" displayText='Pinterest'></div>
                <div class="st_googleplus_custom col-xs-3" displayText='Google +'></div>
            </div>
            <div><br/><p class="text-navy">GRATIS biaya pengiriman khusus wilayah JABODETABEK</p></div>


            <?php
            if (isset($info_product_loan)) {
                if ($info_product_loan != '') {
                    ?>
                    <!-- SIMULASI AREA-->
                    <div class="row">
                        <div class="col-xs-12 box-header-cart">
                            <h4>Simulasi Kredit</h4>
                            <img class="pull-right adira-image-mini" src="<?php echo get_img_url() ?>adira-mini-detail-image.jpg"/>
                        </div>
                    </div>
                    <!--                    <div class="row">
                                            <div class="col-xs-12" style="border-left: 3px solid #ddd">Simulasi cicilan</div>                        
                                        </div>-->
                    <style>
                        .lh34{
                            line-height: 34px
                        }
                        .odd{
                            background: #f5f5f5;
                        }
                        .simulation-field{
                            height: 45px;
                            padding: 5px;
                            line-height: 35px
                        }
                        select.text-right{
                            text-align: right;
                        }
                    </style>
                    <div class="row">
                        <div class="clearfix"></div>
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-xs-12 simulation-field no-margin">                                                        
                                    <div class="col-xs-6">
                                        Harga Barang
                                    </div>
                                    <div class="col-xs-2 lh34">Rp.</div>
                                    <div class="col-xs-4">
                                        <input type="hidden" id="product_price_total" name="product_price_total" value="<?php echo $detail_description->sell_price; ?>">
                                        <span class="pull-right text-bold"><?php echo number_format($detail_description->sell_price); ?></span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="col-xs-12 simulation-field odd">                                                        
                                    <div class="col-xs-6">
                                        Uang Muka
                                    </div>
                                    <div class="col-xs-2 lh34">Rp.</div>
                                    <div class="col-xs-4">
                                        <span id="nilai_dp" class="pull-right"></span>
                                        <input type="hidden" id="dp" name="dp" value="0" class="auto_int text-align-right form-control">        
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <span id="min_dp" class="text-red pull-right"></span> <!-- BELUM TAU NIH BUAT APA KALO DI APPEND PASTI BERANTAKAN -->

                                <div class="col-xs-12 simulation-field">                                                        
                                    <div class="col-xs-6">
                                        Pokok Hutang
                                    </div>
                                    <div class="col-xs-2 lh34">Rp.</div>
                                    <div class="col-xs-4">
                                        <span id="pokok_hutang" class="pull-right text-bold"></span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>


                                <div class="col-xs-12 simulation-field odd">                                                        
                                    <div class="col-xs-4">
                                        Tenor
                                    </div>
                                    <div class="col-xs-2 lh34"></div>
                                    <div class="col-xs-6">
                                        <select class="form-control" id="tenor" name="tenor" onchange="check_interest()">
                                            <?php
                                            if (isset($info_product_loan)) {
                                                foreach ($info_product_loan[0] as $data_loan) {
                                                    $admin_fee = $data_loan->admin_fee;
                                                    echo '<option value="' . $data_loan->seq . '" data-bunga="' . $data_loan->loan_interest . '" data-tenor="' . $data_loan->tenor . '" data-min-dp ="' . $data_loan->min_dp_value . '">' . $data_loan->tenor . '&nbsp;Bulan&nbsp;</option>';
                                                };
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <!-- BUNGA -->
                                <!--                                <div class="col-xs-12 simulation-field">                                                        
                                                                    <div class="col-xs-4">
                                                                        Bunga
                                                                    </div>
                                                                    <div class="col-xs-2 lh34"></div>
                                                                    <div class="col-xs-6 text-right">
                                                                        <span id="persen_bunga"></span>0%<input type="hidden" id="loan_interest" name="loan_interest" value="0">
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div>-->
                                <!-- END BUNGA-->
                                <!-- ANGSURAN -->
                                <div class="col-xs-12 simulation-field odd">                                                        
                                    <div class="col-xs-6">
                                        Angsuran per bln
                                    </div>
                                    <div class="col-xs-2 lh34"></div>
                                    <div class="col-xs-4 text-right">
                                        <input type="hidden" id="installment" name="installment" value="0">
                                        <input type="hidden" id="tenor_value" name="tenor_value" value="0">
                                        <span id="angsuran" class="pull-right text-bold"></span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <!-- END ANGSURAN -->
                            </div>
                        </div>
                    </div>
                    <!--                    <div class="row">
                                            <div class="col-xs-12" style="border-left: 3px solid #ddd">Angsuran Pertama</div>
                                        </div>-->
                    <div class="row">
                        <div class="col-xs-12 box-body-cart">
                            <div class="row">
                                <div class="col-xs-12 simulation-field">                                                        
                                    <div class="col-xs-6">
                                        Total Pembayaran I
                                    </div>
                                    <div class="col-xs-2 lh34">Rp.</div>
                                    <div class="col-xs-4 text-right">
                                        <input type="hidden" id="total_installment" name="total_installment" value="0">
                                        <span id="total_pertama" class="pull-right text-bold"></span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 box-body-cart" style="padding: 10px">
                            <div class="row">
                                <div class="col-xs-12">
                                    <button type="button" onclick="calculate()" class="btn btn-primary pull-right btn-flat">Hitung Simulasi Pembayaran</button>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <!-- END SIMULASI-->
                    <?php
                }
            } else {
                ?>

                <div class="row">
                    <?php if (isset($credit[0])) { ?>
                    <div class="col-xs-12">
                        <h4 class="text-center">SIMULASI CICILAN</h4>
                        <ul class="list-group">
                            <?php
                            $bank = '';
                            
                                foreach ($credit[0] as $each) {
                                    if ($bank != $each->bank_name) {
                                        echo '<li class="list-group-item list-group-item-info text-bold">' . $each->bank_name . '</li>';
                                    }
                                    $bank = $each->bank_name;
                                    ?>
                                    <li class="list-group-item"> &nbsp; <strong>Rp. <?php echo cnum($each->sell_price / $each->credit_month) . "</strong> " . "x" . " " . $each->credit_month . " Bulan"; ?></li>
                                    <?php
                                }
                            ?>
                        </ul>
                        <p><em>* Harga cicilan belum termasuk ongkos kirim, biaya admin dan asuransi</em></p>
                    </div>
                    <?php } ?>
                </div>
            <?php }
            ?>
            <?php if (isset($product_buy_other)) { ?>
                <div class="row">
                    <div class=" detail_title detail-title-padding"> Rekomendasi </div>
                    <div class=" text-center mb-5px"><a href="javascript:moveprevs()" class=" text-center"><i class="fa fa-chevron-up"></i></a></div>
                    <ul id="bxslider_vertical" class="bxslider">
                        <?php //echo display_product_slide_vertical($product_buy_other)     ?>
                    </ul>
                    <div class=" text-center margin-top-min-57"><a href="javascript:movenexts()"><i class="fa fa-chevron-down"></i></a></div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>

<script type="text/javascript">
<?php echo ($adaspek == '' ? '$("#spek_produk").hide();' : ''); ?>
    $(document).ready(function () {
        $('#promo_credit').select2();
<?php if (isset($info_product_loan)) { ?>
            check_interest();
            calculate();
<?php } ?>
    });

    var slider2;

    $('#bxslider_horizontal_related').bxSlider({
        nextSelector: '#slider-next',
        prevSelector: '#slider-prev',
        nextText: '<i class="fa fa-chevron-right"></i>',
        prevText: '<i class="fa fa-chevron-left"></i>',
        minSlides: 1,
        maxSlides: 6,
        slideWidth: 50,
        slideMargin: 0, randomStart: false
    });

    $('#bxslider_horizontal').bxSlider({
        minSlides: 4,
        maxSlides: 4,
        slideWidth: 220,
        slideMargin: 0, randomStart: false
    });
    slider2 = $('#bxslider_vertical').bxSlider({
        minSlides: 3,
        mode: 'vertical',
        maxSlides: 3,
        slideWidth: 245,
        slideMargin: 10,
        controls: false
    });

    function moveprevs() {
        slider2.goToPrevSlide();
    }
    function movenexts() {
        slider2.goToNextSlide();
    }
    $(".bx-pager bx-default-pager").hide();
    imgdtl = 'detail_product';
    function scrollToID(id) {
        $('html, body').animate({
            scrollTop: (($("#" + id).offset().top) - 60)
        }, 1000);
    }
    var product_variant_seq = '<?php echo $product_variant_seq ?>';

<?php if (isset($info_product_loan)) { ?>
        function check_interest() {
            var interest = $("#tenor option:selected").attr("data-bunga");
            var tenor_value = $("#tenor option:selected").attr("data-tenor");
            var dp_value = $("#tenor option:selected").attr("data-min-dp");
            $("#tenor_value").val(tenor_value);
            $("#persen_bunga").html(interest);
            $("#loan_interest").val(interest);
            $("#dp").val(dp_value);
        }

        function calculate() {

            var tenor = $("#tenor").val();
            if (tenor == 0) {
                alert('Silahkan pilih tenor terlebih dahulu.');
                return;
            }
            var harga_barang = <?php echo $detail_description->sell_price; ?>;
            var dp = $("#dp").autoNumeric('get');
            if (dp > harga_barang) {
                alert("Uang Muka tidak boleh lebih besar dari Harga Product");
                return;
            }
            $.ajax({
                url: "<?php echo get_base_url() . "member/checkout" ?>",
                type: "POST",
                dataType: "json",
                data: {
                    "type": "<?php echo TASK_LOAN_SIMULATION; ?>",
                    "product_variant_seq": "<?php echo $detail_description->product_variant_seq ?>",
                    "tenor": tenor,
                    "total": harga_barang,
                    "expedisi": "0",
                    "dp": dp,
                    "sim_type": "true"
                },
                success: function (data) {
                    if (isSessionExpired(data)) {
                        response_object = json_decode(data);
                        url = response_object.url;
                        location.href = url;
                    } else {
                        if (data != "error") {
                            $("#nilai_dp").html(data.min_dp_value_num);
                            $("#angsuran_pertama").html(data.angsuran);
                            $("#total_pertama").html(data.total_pay_1);
                            $("#angsuran").html(data.angsuran);
                            $("#pokok_hutang").html(data.ph);
                            //$("#min_dp").html(data.min_dp_error);
                            $("#installment").val(data.angsurans);
                            $("#total_installment").val(data.total_pay_1s);

                        } else {
                            alert("Error");
                        }

                    }
                },
                error: function (request, error) {
                    alert(error_response(request.status));
                }
            });
        }
<?php } ?>
</script>
<input type="hidden" id="active_img_zoom_container_number" value="" />
<?php $this->load->view(LIVEVIEW . 'home/template/segment_main/js_add_product.php'); ?>
</div>
