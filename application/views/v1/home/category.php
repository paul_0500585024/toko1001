<?php

$getSearchHarga = $this->input->get('pr');
$getMinPrice = 0;
$getMaxPrice = 99999999;
if ($getSearchHarga !== NULL) :
    $searchPrice = explode(',', $getSearchHarga);
    $getMinPrice = isset($searchPrice[0]) ? $searchPrice[0] : 0;
    $getMaxPrice = isset($searchPrice[1]) ? $searchPrice[1] : 99999999;
endif;
?>

<?php
require_once VIEW_BASE_HOME;
parse_str($this->input->server('QUERY_STRING'), $query_string);
$get_attribute_name_display = array();
$filter_message = array();
$active_url = $this->uri->segment(1);
?>
<?php
$title_product = $this->input->get('e');
?>

<div class="container"> <!--global container-->
    <!--BREADCRUMB-->
    <div class="cst-breadcrumb">
        <?php if ($level != ''): ?>
            <?php
            $number_of_category = count($category_seq_self_parent_with_name);
            $counter_category = 0;
            ?>
            <div class="cst-breadcrumb-inside">
                <a href="<?php echo base_url(); ?>"><i class="fa fa-home font-sz-1"></i></a>
            </div>
            <div class="cst-breadcrumb-inside">
                <img src="<?php echo get_image_location() . 'assets/img/breadcrumb.png' ?>">
            </div>
            <?php foreach ($category_seq_self_parent_with_name as $product_category_seq => $each_seq_self_parent_category): ?>
                <div class="cst-breadcrumb-inside">
                    <a href="<?php echo base_url() . url_title(strtolower($each_seq_self_parent_category->name)) . "-" . CATEGORY . ($each_seq_self_parent_category->seq) ?>"><?php echo $each_seq_self_parent_category->name; ?></a>
                </div>
                <?php if ($counter_category < $number_of_category - 1): ?>
                    <div class="cst-breadcrumb-inside">
                        <img src="<?php echo get_image_location() . 'assets/img/breadcrumb.png' ?>">
                    </div>
                <?php endif; ?>
                <?php $counter_category++; ?>
            <?php endforeach; ?>
        <?php endif; ?>
        <!--harcode breadcumb-->
        <?php
        if (isset($breadcrumb_pages)):
            $link = base_url() . "produk/promo";
            ?>
            <div class="cst-breadcrumb-inside">
                <a href="<?php echo base_url(); ?>"><i class="fa fa-home font-sz-1"></i></a>
            </div>
            <div class="cst-breadcrumb-inside">
                <img src="<?php echo get_image_location() . 'assets/img/breadcrumb.png' ?>">
            </div>
            <a class="wsh" href="<?php echo $link ?>"><?php echo $breadcrumb_pages ?></a>
            <?php
        endif;
        ?>

    </div>
    <!--END-BREADCRUM-->

    <div class="row">
        <div class="col-xs-2 pl-15px pr-5px">
            <div id="filter" class="filter-cst">
                <div class='cat-body'>
                    <form class="form" id="formLogin" method="get" action="<?php echo current_url_with_query_string(array(PRICE_RANGE, START_OFFSET)) ?>">
                        <input type="text" value="" class="slider form-control"
                               id="harga" name="" data-slider-min="0"
                               data-slider-max="99999999" data-slider-step="10000"
                               data-slider-value="[<?php echo $getMinPrice ?>,<?php echo $getMaxPrice ?>]"
                               data-slider-orientation="horizontal" style="display: block;"
                               data-slider-selection="before" data-slider-tooltip="show"
                               data-slider-id="grey">
                        <div id="slideprice"></div>
                        <div class="col-xs-6 p-3px">
                            <input type="text" id="minrange"  value="<?php echo (isset($getSearchHarga) ? number_format($searchPrice[0]) : number_format("0") ); ?>" class="form-control numeric" maxlength="9" style="padding: 9px !important">
                        </div>
                        <div class="col-xs-6 p-3px">
                            <input type="text" id="maxrange"  value="<?php echo (isset($getSearchHarga) ? number_format($searchPrice[1]) : number_format("99999999") ); ?>" class="form-control numeric" maxlength="9" style="padding: 9px !important">
                        </div>
                        <input type="hidden" id="pricerange" name="pr" value="0,99999999" class="form-control">
                        <div class="mt-10px">
                            <button type="submit" id="btnLogin" class="btn btn-block btn-flat btn-green" ><i class="fa fa-search"></i></button>
                        </div>
                    </form>
                    <br>
                    <?php if ($product_category_seq != '0'): ?>
                        <?php foreach ($filter['name'] as $each_filter_name): ?>
                            <?php $attribute_name_display = display_filter_name($each_filter_name->attribute_name, $each_filter_name->attribute_display_name); ?>
                            <div class="filter">
                                <?php echo $attribute_name_display ?>
                            </div>
                            <div id="custom-scroll" class="cst-scrool">
                                <?php if (count($filter['detail'][$each_filter_name->attribute_seq]) > 0): ?>
                                    <ul class="pl-10px">
                                        <?php $counter = 0; ?>
                                        <?php /* for label semua */ ?>
                                        <?php if (count($filter['detail'][$each_filter_name->attribute_seq]) > 0): ?>
                                            <?php
                                            $attribute_category_attribute_seq = $each_filter_name->attribute_seq;
                                            $selected_filter = '';
                                            $attribute_value_seq = '0';
                                            $attribute_value_value = 'Semua';
                                            if (get_selected_category($attribute_category_attribute_seq, $attribute_value_seq, $this->input->get(PARAMETER_CATEGORY_ATTRIBUTE, TRUE))) {
                                                //                                        $selected_filter = "class='active'";
                                                $selected_filter = true;
                                                //to get filter value; 0 is for semua
                                                if ($attribute_value_seq != 0) {
                                                    $get_attribute_name_display[$attribute_name_display][] = $attribute_value_value;
                                                }
                                            };
                                            ?>

                                            <?php $counter++; ?>
                                        <?php endif; ?>
                                        <?php /* end of label semua */ ?>
                                        <?php foreach ($filter['detail'][$each_filter_name->attribute_seq] as $each_filter_detail): ?>
                                            <?php
                                            $selected_filter = '';
                                            $attribute_category_attribute_seq = $each_filter_detail->attribute_category_attribute_seq;
                                            if ($counter == 0) {
                                                $attribute_value_seq = '0';
                                                $attribute_value_value = 'Semua';
                                            } else {
                                                $attribute_value_seq = $each_filter_detail->attribute_value_seq;
                                                $attribute_value_value = $each_filter_detail->attribute_value_value;
                                            }
                                            if (get_selected_category($attribute_category_attribute_seq, $attribute_value_seq, $this->input->get(PARAMETER_CATEGORY_ATTRIBUTE, TRUE))) {
                                                //                                        $selected_filter = "class='active'";
                                                $selected_filter = true;
                                                //to get filter value; 0 is for semua
                                                if ($attribute_value_seq != 0) {
                                                    $get_attribute_name_display[$attribute_name_display][] = $attribute_value_value;
                                                }
                                            };
                                            ?>
                                            <?php if ($selected_filter): ?>
                                                <span class="fa fa-check-square-o"></span>&nbsp;<a href="<?php echo add_current_url_with_query_string_special($attribute_category_attribute_seq . '-' . $attribute_value_seq) ?>"><?php echo $attribute_value_value; ?></a>
                                            <?php else: ?>
                                                <span class="fa fa-square-o"></span>&nbsp;<a href="<?php echo add_current_url_with_query_string_special($attribute_category_attribute_seq . '-' . $attribute_value_seq) ?>"><?php echo $attribute_value_value; ?></a>
                                            <?php endif; ?>
                                            <br>
                                            <?php $counter++; ?>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                            </div>
                        <?php endforeach; ?>
                        <?php if (count($filter['name']) != 0): ?> 
                            <div class="row">
                                <div class="col-xs-12">
                                    <a href="<?php echo base_url() . $active_url; ?>">
                                        <button class="btn btn-block no-border-radius btn-green ">Reset filter</button>
                                    </a>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php
        if ($title_product == ""):
            if (isset($each_seq_self_parent_category->name)):
                $title_product = $each_seq_self_parent_category->name;
            endif;
        endif;
        ?>
        <div class="col-xs-10 bl-1px">
            <div class='well header-cat-product p-10px'>
                <div class='row no-pad no-margin'>
                    <div class='col-xs-6 no-pad no-margin'>
                        <?php if (isset($title_product)): ?>
                            <div class="title_preoduct"><?php echo htmlspecialchars($title_product); ?></div>
                        <?php endif; ?>

                        <bold><i>  <?php echo "Terdapat" . " " . $row . " " . "produk" . " " . htmlspecialchars($title_product); ?></i></bold>
                    </div>
                    <div class='col-xs-6 no-pad no-margin'>
                        <button type="button" class="btn dropdown-toggle pull-right no-border-radius btn-normal width-210" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                            Urutkan
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu btn-normal pull-right cst-zindex width-210">
                            <li>
                                <?php if ($order_category == NEW_TO_OLD_PRODUCT): ?>
                                    <a href="<?php echo current_url_with_query_string(array(ORDER_CATEGORY, START_OFFSET), array(ORDER_CATEGORY => NEW_TO_OLD_PRODUCT)) ?>" class="cst-option-act">Produk terbaru ke terlama</a>
                                <?php else: ?>
                                    <a href="<?php echo current_url_with_query_string(array(ORDER_CATEGORY, START_OFFSET), array(ORDER_CATEGORY => NEW_TO_OLD_PRODUCT)) ?>" class="cst-option">Produk terbaru ke terlama</a>
                                <?php endif; ?>
                            </li>
                            <li>
                                <?php if ($order_category == OLD_TO_NEW_PRODUCT): ?>
                                    <a href="<?php echo current_url_with_query_string(array(ORDER_CATEGORY, START_OFFSET), array(ORDER_CATEGORY => OLD_TO_NEW_PRODUCT)) ?>" class="cst-option-act">Produk terlama ke terbaru</a>
                                <?php else: ?>
                                    <a href="<?php echo current_url_with_query_string(array(ORDER_CATEGORY, START_OFFSET), array(ORDER_CATEGORY => OLD_TO_NEW_PRODUCT)) ?>" class="cst-option">Produk terlama ke terbaru</a>
                                <?php endif; ?>
                            </li>
                            <li>
                                <?php if ($order_category == PRICE_EXPENSIVE_TO_CHEAP): ?>
                                    <a href="<?php echo current_url_with_query_string(array(ORDER_CATEGORY, START_OFFSET), array(ORDER_CATEGORY => PRICE_EXPENSIVE_TO_CHEAP)) ?>" class="cst-option-act">Harga mahal ke murah</a>
                                <?php else: ?>
                                    <a href="<?php echo current_url_with_query_string(array(ORDER_CATEGORY, START_OFFSET), array(ORDER_CATEGORY => PRICE_EXPENSIVE_TO_CHEAP)) ?>" class="cst-option">Harga mahal ke murah</a>
                                <?php endif; ?>
                            </li>
                            <li>
                                <?php if ($order_category == PRICE_CHEAP_TO_EXPENSIVE): ?>
                                    <a href="<?php echo current_url_with_query_string(array(ORDER_CATEGORY, START_OFFSET), array(ORDER_CATEGORY => PRICE_CHEAP_TO_EXPENSIVE)) ?>" class="cst-option-act">Harga murah ke mahal</a>
                                <?php else: ?>  
                                    <a href="<?php echo current_url_with_query_string(array(ORDER_CATEGORY, START_OFFSET), array(ORDER_CATEGORY => PRICE_CHEAP_TO_EXPENSIVE)) ?>" class="cst-option">Harga murah ke mahal</a>
                                <?php endif; ?>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
            <!--FILTER MESSAGE-->
            <?php if ($this->input->get(PRICE_RANGE, TRUE) != false): ?>
                <span>
                    <?php
                    $price_range = $this->input->get(PRICE_RANGE, TRUE);
                    $price_range_exploder = explode(',', $price_range);
                    $start_price = isset($price_range_exploder[0]) ? $price_range_exploder[0] : '';
                    $to_price = isset($price_range_exploder[1]) ? $price_range_exploder[1] : '';
                    $filter_message[] = "Harga : " . RP . cnum($start_price) . " - " . RP . cnum($to_price);
                    ?>
                </span>
            <?php endif; ?>



            <?php
            $counter_filter = 0;
            $end_filter_index = count($get_attribute_name_display);
            ?>
            <?php foreach ($get_attribute_name_display as $attribute_name_display => $each_get_attribute_name_display): ?>
                <?php foreach ($each_get_attribute_name_display as $key => $attribute_value_value): ?>
                    <?php $filter_message[] = $attribute_name_display . " : " . $attribute_value_value ?>
                    <?php $counter_filter++; ?>
                <?php endforeach; ?>
            <?php endforeach; ?>

            <?php if ($this->input->get(ORDER_CATEGORY, TRUE) != false): ?>
                <?php $filter_message[] = "Diurutkan : " . $ordering_category; ?>
            <?php endif; ?>

            <!--showing filter messsage ...-->
            <?php if (count($filter_message) != '' OR count($filter_message) != 0): ?>
                <div class='no-border-radius'>
                    <?php
                    $counter = 0;
                    $end_index_filter_message = count($filter_message);
                    ?>
                    <?php foreach ($filter_message as $each_filter_message): ?>
                        <span><?php echo $each_filter_message; ?></span>
                        <?php if ($counter < $end_index_filter_message - 1): ?>
                            &nbsp;&#8226;&nbsp;
                        <?php endif; ?>
                        <?php $counter++; ?>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
            <!--END FILTER MESSAGE-->

            <?php
            if (count($product) > 0) {
                echo display_product($product);
            } elseif ($this->input->get(SEARCH, TRUE) != FALSE) {
                ?>
                <p>Kami tidak menemukan hasil pencarian untuk  &quot;<b><?php echo htmlspecialchars($this->input->get(SEARCH, TRUE)); ?></b>&quot;</p>
                <p>Tolong periksa pengejaan kata, gunakan kata-kata yang lebih umum &amp; coba lagi!</p>
                <?php
            } elseif (count($product) == 0) {
                echo "Tidak ada produk dengan kategori ini.";
            }
            ?><br>
        </div>
        <div class="container aad-display-block">
            <div class="row">
                <div class="col-xs-10 col-xs-offset-2">
                    <center>
                        <?php echo $this->pagination->create_links(); ?>
                    </center>
                </div>
            </div>
        </div>
    </div> <!--end container awal-->
</div>
<?php $this->load->view(LIVEVIEW . 'home/template/segment_main/js_add_product.php'); ?>


<script>
    (function ($) {
        $.fn.extend({
            formatInput: function (settings) {
                var $elem = $(this);
                settings = $.extend({
                    errback: null
                }, settings);
                $elem.bind("keyup.filter_input", $.fn.formatEvent);
            },
            formatEvent: function (e) {
                //split with the "." because of the decimal value
                var valueDecimal = initial_value.split(".");
                //formats the part without the decimal part
                var formatedValue = $.fn.insertSpaces(valueDecimal[0]);
                var finalValue;
                //checks if it has decimal values or not
                if (valueDecimal.length > 1) {
                    finalValue = formatedValue + "." + valueDecimal[1];
                } else {
                    finalValue = formatedValue;
                }
                elem.val(finalValue);
                //I had to add this line because I'm working with knockout, and it wouldn't update the value on IE
                elem.trigger('change');
            },
            insertSpaces: function (number) {
                return number.replace(" ", "").replace(/(\d)(?=(?:\d{3})+$)/g, "$1 ");
            }
        });
    })(jQuery);


    $(document).ready(function () {
        $('.numeric').bind('keyup filter_input', function () {
//            $('#minrange').exten  ds();
        });
    });

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    $('#harga').on('slide slideStart slideStop', function () {
        var range = $('#harga').val();
        var subrange = range.split(',');
        $('#minrange').val(numberWithCommas(subrange[0]));
        $('#maxrange').val(numberWithCommas(subrange[1]));
    });

    $('#harga').on('slideStop', function () {
        var price1 = $('#minrange').val().replace(/\D/g, '');
        var price2 = $('#maxrange').val().replace(/\D/g, '');
        $('#pricerange').val(price1 + "," + price2);
    })

    $('#minrange, #maxrange').on("keypress keyup blur", function (event) {
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    $('#minrange, #maxrange').on('change blur', function (e) {
        var a = $(this).val();
        $(this).val('');
        $(this).val(numberWithCommas(a));

        var price1 = $('#minrange').val().replace(/,/g, '');
        var price2 = $('#maxrange').val().replace(/,/g, '');
        ;
        $('#pricerange').val(price1 + "," + price2);
    })

    $('.productDetails').mouseenter(function () {
        var title = 'title-' + $(this).attr('data-img');
        $('#' + title).removeClass('no-show');
    });

    $('.productDetails').mouseleave(function () {
        var title = 'title-' + $(this).attr('data-img');
        $('#' + title).addClass('no-show');
    });
</script>
