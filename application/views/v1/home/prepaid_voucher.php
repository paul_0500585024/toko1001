<?php
require_once VIEW_BASE_HOME;
?>
<?php if (!isset($_GET["step"]) || $_GET["step"] === null) { ?>
    <div class="container">
        <div class="row">
            <div class="container">
                <?php if (isset($data_err) && $data_err[ERROR] === true) { ?>
                    <div class="alert alert-error-home alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $data_err[ERROR_MESSAGE][0] ?>
                    </div>
                <?php } ?>
                <div class="box">
                    <div class="box-body-cart">
                        <br>
                        <form id="frmProvider" class="form-horizontal" method='post' action='<?php echo get_base_url() . "pulsa" ?>'>

                            <!--tempat input field-->
                            <input id="providerSeqId" type="hidden" name="provider_seq" validate="required[]" value="">
                            <input id="provider_service" name="provider_service_seq" type="hidden" value="">
                            <input id="provider_service_nominal" type="hidden" name="provider_service_nominal_seq" value="">
                            <!--end input field-->

                            <div class="row no-margin">
                                <div class="col-xs-6">
                                    <div class="well no-border-radius prepaid-box">
                                        <div class="col-xs-12">
                                            <label>Masukkan Nomor HP :</label>
                                            <div class="input-group phone-number-group">
                                                <input id='phone_number' name="phone_number" class="form-control line-input" type="text" data-mask data-inputmask = "'mask': ['999 999 999 999 999']" value ="<?php echo isset($provider) ? $provider->phone_number : '' ?>" onkeyup="change_phonenumber()">
                                                <span class="input-group-addon" id="provider"></span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 m-top10">
                                            <div id="service">
                                            </div>
                                        </div>
                                        <div class="col-xs-12 m-top10">
                                            <div id="nominal">
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div id="info" class="col-xs-12 m-top10"></div> 
                                        <div class="clearfix"></div>
                                        <div class="col-xs-12 text-right" id="prepaid-voucher-price" style="margin-top: 20px !important;">
                                        </div>
                                        <div class="clearfix"></div>

                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="well no-border-radius prepaid-box">
                                        <div class="col-xs-12">
                                            <center><h4 class="bold-text">Metode Pembayaran</h4> </center>
                                            <center>
                                                <div class="" data-toggle="buttons">
                                                    <label class="btn btn-default h80 no-border-radius <?php echo isset($_SESSION[SESSION_MEMBER_CSRF_TOKEN]) || isset($_SESSION[SESSION_AGENT_CSRF_TOKEN]) ? 'col-xs-4' : 'col-xs-6'; ?>  <?php echo isset($provider->pg_method_seq) && $provider->pg_method_seq == PAYMENT_SEQ_MANDIRI_KLIKPAY ? "active" : "" ?>">
                                                        <label> Mandiri Klikpay </label>
                                                        <br><input type="radio" name="payment_method" value="<?php echo PAYMENT_SEQ_MANDIRI_KLIKPAY ?>" <?php echo isset($provider->pg_method_seq) && $provider->pg_method_seq == PAYMENT_SEQ_MANDIRI_KLIKPAY ? "checked" : "" ?>>   <img height="30px" src ="<?php echo get_img_url() ?>payment_70px/mandiriclickpay_70px.png" alt="Mandiri Clikpay">
                                                    </label>
    <!--                                                    <label class="btn btn-default h80 no-border-radius col-xs-4 <?php echo isset($provider->pg_method_seq) && $provider->pg_method_seq == PAYMENT_SEQ_BCA_KLIKPAY ? "active" : "" ?>">
                                                        <input type="radio" name="payment_method" value="<?php echo PAYMENT_SEQ_BCA_KLIKPAY ?>" <?php echo isset($provider->pg_method_seq) && $provider->pg_method_seq == PAYMENT_SEQ_BCA_KLIKPAY ? "checked" : "" ?>>   <img height="30px" src ="<?php echo get_img_url() ?>payment_70px/bca_click_pay.jpg"  alt="BCA Clikpay">
                                                        <br><label> BCA KlikPay </label>
                                                    </label>-->
                                                    <?php if (isset($_SESSION[SESSION_MEMBER_CSRF_TOKEN]) || isset($_SESSION[SESSION_AGENT_CSRF_TOKEN])) { ?>
                                                        <label class="btn btn-default h69 no-border-radius col-xs-4 <?php echo isset($provider->pg_method_seq) && $provider->pg_method_seq == PAYMENT_SEQ_DEPOSIT ? "active" : "" ?>">
                                                            <label> Deposit member  </label>
                                                            <input  type="radio" name="payment_method" value="<?php echo PAYMENT_SEQ_DEPOSIT ?>" <?php echo isset($provider->pg_method_seq) && $provider->pg_method_seq == PAYMENT_SEQ_DEPOSIT ? "checked" : "" ?>>   
                                                            <br>
                                                            <?php if (isset($_SESSION[SESSION_MEMBER_CSRF_TOKEN])) { ?>
                                                                <font size="2">Saldo Anda <?php echo isset($member_info[2][0]) ? "Rp." . number_format($member_info[2][0]->deposit_amt) : "0"; ?></font>
                                                            <?php } elseif (isset($_SESSION[SESSION_AGENT_CSRF_TOKEN])) { ?>
                                                                <font size="2">Saldo Anda <?php echo isset($agent_info[2][0]) ? "Rp." . number_format($agent_info[2][0]->deposit_amt) : "0"; ?></font>
                                                            <?php } ?>
                                                        </label>
                                                    <?php } ?>
                                                    <label class="btn btn-default h80 no-border-radius <?php echo isset($_SESSION[SESSION_MEMBER_CSRF_TOKEN]) || isset($_SESSION[SESSION_AGENT_CSRF_TOKEN]) ? 'col-xs-4' : 'col-xs-6'; ?> <?php echo isset($provider->pg_method_seq) && $provider->pg_method_seq == PAYMENT_SEQ_MANDIRI_ECASH ? "active" : "" ?>">
                                                        <label> Mandiri Ecash </label>
                                                        <br><input type="radio" name="payment_method" value="<?php echo PAYMENT_SEQ_MANDIRI_ECASH ?>" <?php echo isset($provider->pg_method_seq) && $provider->pg_method_seq == PAYMENT_SEQ_MANDIRI_ECASH ? "checked" : "" ?>>   <img height="30px" src ="<?php echo get_img_url() ?>payment_70px/mandiri e-cash_70px.png" alt="Mandiri e-cash">
                                                    </label>
                                                </div>
                                            </center>
                                        </div>
                                        <div class="col-xs-6 col-xs-offset-6">
                                            <center class="margin-top-50">
                                                <?php if (get_csrf_member_token() !== "" || get_csrf_agent_token() !== "") { ?>
                                                    <button type="submit" name="type" value="<?php echo TASK_PROVIDER_SAVE ?>" class ='btn btn-success btn-flat  button-cart'>Beli Sekarang</button>
                                                <?php } else { ?>
                                                    <a href="#" data-toggle="modal" onclick="submit_provider()" data-target="#frm_login" data-id="1" class="btn btn-success btn-flat  button-cart">Beli Sekarangg</a>
                                                <?php } ?>
                                            </center>
                                        </div>
                                    </div>
                                </div>  
                            </div>
                        </form>
                        <br>
                        <div class="col-xs-12">
                            <div class="col-xs-4">
                                <div class="col-xs-4 col-xs-offset-4" style="padding: 10px">
                                    <img src="<?php echo get_img_url() ?>prepaid-voucher-steep-1.png" class="img-responsive">
                                </div>
                                <div class="col-xs-12 m-top10">
                                    <p style="text-align: center">Masukkan No Handphone anda, pilih paket data dan Nominal yang akan dibeli</p>
                                </div>
                            </div>

                            <div class="col-xs-4">
                                <div class="col-xs-4 col-xs-offset-4" style="padding: 10px">
                                    <img src="<?php echo get_img_url() ?>prepaid-voucher-steep-2.png" class="img-responsive">
                                </div>
                                <div class="col-xs-12 m-top10">
                                    <p style="text-align: center">Pilih metode pembayaran yang tersedia</p>
                                </div>
                            </div>

                            <div class="col-xs-4">
                                <div class="col-xs-4 col-xs-offset-4" style="padding: 10px">
                                    <img src="<?php echo get_img_url() ?>prepaid-voucher-steep-3.png" class="img-responsive">
                                </div>
                                <div class="col-xs-12 m-top10">
                                    <p style="text-align: center">pulsa akan terisi secara otomatis setelah pembayaran sudah terverifikasi</p>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>


            </div>
        </div>
    </div>


    <?php if (isset($provider)) { ?>
        <script>
            $(document).ready(function () {
                change_phonenumber(true);
            })
        </script>
    <?php } ?>



    <script>




        var currProvider = '';
        var old_phone_number = '<?php isset($provider) ? $provider->phone_number : '' ?>';
        function change_phonenumber(manual) {
            var phone_number_code = $('#phone_number').val();
            var numberLenght = $('#phone_number').val().length;

            if (numberLenght >= 5) {
                if (phone_number_code.substring(0, 5) !== old_phone_number.substring(0, 5)) {
                    $('#provider_service').val('');
                    $('#provider_service_nominal').val('');
                    $("#nominal").empty();
                    $('#info').html('');
                    old_phone_number = phone_number_code;
                    $.ajax({
                        url: "<?php echo get_base_url() . "pulsa" ?>",
                        type: "POST",
                        dataType: "json",
                        data: {
                            "type": "<?php echo TASK_PHONE_NUMBER_CHANGE ?>",
                            "phone_number_code": phone_number_code
                        },
                        success: function (data) {
                            if (isSessionExpired(data)) {
                                response_object = json_decode(data);
                                url = response_object.url;
                                location.href = url;
                            } else {
                                if (data) {
                                    $('#providerSeqId').val('');
                                    $('#provider_service').val('');
                                    $('#provider_service_nominal').val('');
                                    $('#providerSeqId').val(data[0].provider_seq);
                                    $('#provider').empty();
                                    $('#provider').append('<img src="<?php echo get_img_url() ?>' + data[0].logo + '.png" style="height:30px;">');
                                    if (manual === true) {
                                        get_service(data[0].provider_seq, manual);
                                    } else {
                                        get_service(data[0].provider_seq);
                                    }

                                    var urlData = [];
                                    urlData['simpati'] = 'info/detail-paket-data-telkomsel';
                                    urlData['three'] = 'info/detail-paket-data-three';
                                    urlData['indosat'] = 'info/detail-paket-data-indosat';
                                    if (urlData[data[0].name.toLowerCase()] != undefined) {

                                        var link = 'Informasi lebih lengkap tentang <a href="<?php echo get_base_url() ?>info/detail-paket-data-' + ((data[0].name).toLowerCase()) + '">paket Data ' + data[0].name + '</a>';
                                        $('#info').append(link);
                                    }
                                } else {
                                    $('#providerSeqId').val("");
                                    $('#provider_service').val('');
                                    $('#provider_service_nominal').val('');

                                    $("#service").empty();
                                    $("#provider").empty();
                                    $("#nominal").empty();
                                    $("#provider").append('<i style="color:red !important;position:relative !important" class="fa fa-exclamation" aria-hidden="true"><div class="prepaid-warning" style="">periksa kembali nomor anda</div></i>');
                                }
                            }
                        }
                    });
                }
            } else {
                $('#providerSeqId').val('');
                $('#provider_service').val('');
                $('#provider_service_nominal').val('');
                $('#provider').empty();
                $('#service').empty();
                $('#nominal').empty();
                $("#prepaid-voucher-price").empty();
            }
        }


        function get_service(provider_seq, manual) {
            $('#provider_service').val('');
            $('#provider_service_nominal').val('');
            $("#prepaid-voucher-price").empty();

            $.ajax({
                url: "<?php echo get_base_url() . "pulsa" ?>",
                type: "POST",
                dataType: "json",
                data: {
                    "type": "<?php echo TASK_PROVIDER_CHANGE ?>",
                    "provider_seq": provider_seq
                },
                success: function (data) {
                    if (isSessionExpired(data)) {
                        response_object = json_decode(data);
                        url = response_object.url;
                        location.href = url;
                    } else {
                        var htmlService = '';
                        htmlService += '<label>Service  </label><div class="row no-margin">';
                        $(data).each(function (i) {
                            htmlService += '<button type="button" class="btn btn-service-type btn-flat col-xs-3';
                            htmlService += '" id="service' + data[i].seq + '" onclick="getNominal(' + data[i].seq + ')">' + data[i].name + '</button>';
                        });
                        htmlService += '</div>';
                        $("#service").empty();
                        $("#service").append(htmlService);
                        if (manual == true) {
                            getNominal(
    <?php
    if (isset($provider->provider_service_seq)) {
        if ($provider->provider_service_seq != '') {
            echo $provider->provider_service_seq . ',true';
        } else {
            echo 'null,true';
        }
    } else {
        echo 'null' . ',' . 'true';
    }
    ?>);
                        }
                    }
                }
            });
        }


        function getNominal(provider_service_seq, manual) {
            if (provider_service_seq != null) {
                $('#provider_service_nominal').val('');
                $("#prepaid-voucher-price").empty();
                $.ajax({
                    url: "<?php echo get_base_url() . "pulsa" ?>",
                    type: "POST",
                    dataType: "json",
                    data: {
                        "type": "<?php echo TASK_PROVIDER_SERVICE_CHANGE ?>",
                        "provider_service_seq": provider_service_seq
                    },
                    success: function (data) {
                        if (isSessionExpired(data)) {
                            response_object = json_decode(data);
                            url = response_object.url;
                            location.href = url;
                        } else {
                            var htmlnominal = '';
                            htmlnominal += '<label>Pilih Nominal</label>';
                            htmlnominal += '<div class="row">';
                            $(data).each(function (i) {
                                htmlnominal += '<div class="col-xs-4"><button type="button" class="btn btn-nominal-pulsa btn-block btn-flat ';
                                htmlnominal += '" id="nominal' + data[i].seq + '" onclick="setNominal(' + data[i].seq + ',' + data[i].sell_price + ')" harga="' + data[i].sell_price + '">' + " " + toRp(data[i].name, true) + '</button></div>';
                            });
                            htmlnominal += '<div><div class="clearfix"></div>';
                            $("#provider_service").val("");
                            $("#provider_service").val(provider_service_seq);
                            $("#nominal").empty();
                            $("#nominal").delay(200).append(htmlnominal);
                            $("#service").children().children("button").removeClass("service-type-active");
                            $('#service' + provider_service_seq).addClass('service-type-active').delay(500);
                            if (manual === true) {
                                setNominal(
    <?php
    if (isset($provider->provider_service_nominal_seq)) {
        if ($provider->provider_service_nominal_seq != '') {
            echo $provider->provider_service_nominal_seq;
        } else {
            echo 'null';
        }
    } else {
        echo 'true';
    }
    ?>
                                ,
    <?php
    if (isset($provider->sell_price)) {
        if ($provider->sell_price != '') {
            echo $provider->sell_price;
        } else {
            echo 'null';
        }
    } else {
        echo 'null';
    }
    ?>);
                            }
                        }
                    }
                });
            }
        }

        function setNominal(seqNominal, price) {
            if (seqNominal != null) {
                var totalHarga = "Total Pembelian adalah <span class='' style='font-size:1.5em;color:red'>" + "Rp." + " " + toRp(price, true) + '</span>';
                $("#provider_service_nominal").val("");
                $("#provider_service_nominal").val(seqNominal);
                $("#prepaid-voucher-price").empty();
                $("#prepaid-voucher-price").append(totalHarga);
                $("#nominal").children().children().children("button").removeClass("nominal-active");
                $("#nominal" + seqNominal).addClass("nominal-active");
            }
        }


        function toRp(nStr, rupiah) {
            nStr += '';
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + '.' + '$2');
            }
            return x1 + x2;
        }


        function submit_provider() {
            var provider_data = $('#frmProvider').serializeArray();
    <?php if (!isset($_SESSION[SESSION_MEMBER_CSRF_TOKEN])) { ?>
                provider_data.push({"name": "type", value: "<?php echo TASK_PROVIDER_SAVE ?>"});
                $.ajax({
                    url: "<?php echo get_base_url() . "pulsa" ?>",
                    type: "POST",
                    data: provider_data})
    <?php } ?>
        }

    </script>





<?php } else if ($_GET["step"] === "2") { ?>
    <form method="post" action="<?php echo get_base_url() . "pulsa" ?>">
        <div class="container prepaid-box-final" style="background-image: url('<?php echo get_img_url() . "bg-pulsa.jpg" ?>');">
            <div class="col-xs-8">
                <h4>INFORMASI PENGISIAN PULSA</h4>
            </div>
            <div class="col-xs-4">
                <img src="<?php echo get_img_url() . $provider->logo ?>.png" class="prepaid-img-provider">
            </div>

            <div class="col-xs-12 prepaid-box-number">
                <div class="text-warning">*pastikan nomor anda sudah benar</div>
                <div class="prepaid-bigphonenumber"><?php echo $provider->phone_number ?> </div>
            </div>
            <div class="col-xs-12 f-size17px">
                Service : <?php echo $provider->provider_service_name ?><br>
                Nominal : <?php echo number_format($provider->nominal) ?><br>
                Metode Pembayaran : <?php echo $provider->pg_method_name ?>
            </div>
            <div class="final-price-prepaid">
                <h4>TOTAL BAYAR&nbsp;  <span class="final-price fsize20px">Rp.&nbsp;<?php echo number_format($provider->sell_price) ?></span></h4>
            </div>
        </div>
        <br>
        <div class="container no-pad max-width-600px">
            <div class="col-xs-6 col-xs-offset-6 no-pad">
                <button type="submit" name="type" value="<?php echo TASK_PROVIDER_PROCESS ?>" class ='btn btn-success btn-flat btn-pulsa btn-flat pull-right'>Proses Sekarang</button>
            </div>
        </div>
    </form>

<?php } ?>
