<?php
require_once VIEW_BASE_MERCHANT;
require_once get_include_content_merchant_top_page_navigation();
echo $breadcrumb;

function get_qty_status($arraystatus, $val) {
    foreach ($arraystatus as $key => $obj) {
        if ($obj->order_status == $val) {
            return $obj->qty_status;
            break;
        }
    }return 0;
}
?>
<?php
if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT or $data_auth[FORM_ACTION] == ACTION_VIEW) {
    $statusorder = $data_sel[LIST_DATA][0][0]->order_status;
    ?>
    <div class="p-10px">
        <div class="box box-default no-border no-margin">
            <form class="form-horizontal" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>" name="form1" id="form1">
                <div class="box-body">
                    <section class="col-xs-12">
                        <?php echo get_csrf_merchant_token(); ?>
                        <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                            <input class="form-control"  name ="seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0][0]->seq; ?>">
                        <?php } ?>
                        <div class="row">
                            <div class="col-xs-4">
                                Nomor Order :
                                <span class="label label-primary">
                                    <strong>
                                        <?php echo $data_sel[LIST_DATA][0][0]->order_no; ?>
                                    </strong>
                                </span>
                                <br />
                                Tanggal Order :
                                <?php echo date("d-M-Y", strtotime($data_sel[LIST_DATA][0][0]->order_date)); ?>
                                <br />
                                Status Order :
                                <span class="label label-info">
                                    <strong>
                                        <?php echo status($data_sel[LIST_DATA][0][0]->order_status, STATUS_ORDER); ?>
                                    </strong>
                                    <input type="hidden" name="order_status" id="order_status" value="<?php echo $data_sel[LIST_DATA][0][0]->order_status; ?>">
                                </span>
                            </div>
                            <div class="col-xs-5">
                                Penerima :
                                <br />
                                <strong>
                                    <?php echo $data_sel[LIST_DATA][0][0]->receiver_name; ?>
                                </strong>
                                <br />
                                <?php echo $data_sel[LIST_DATA][0][0]->receiver_address; ?>
                                <br />
                                <?php echo $data_sel[LIST_DATA][2][0]->d_name; ?>, <?php echo $data_sel[LIST_DATA][2][0]->c_name; ?>
                                <br />
                                <?php echo $data_sel[LIST_DATA][2][0]->p_name; ?>, Kode Pos <?php echo $data_sel[LIST_DATA][0][0]->receiver_zip_code; ?>
                                <br />
                                Nomor Telp. :
                                <?php echo $data_sel[LIST_DATA][0][0]->receiver_phone_no; ?>
                                <br />
                            </div>
                            <div class="col-xs-3">
                                Pesan Member:
                                <br />
                                <?php echo $data_sel[LIST_DATA][0][0]->member_notes; ?>
                            </div>
                        </div>
                        <hr style="border: 1px dashed #f4f4f4;">
                        <div class="row">
                            <div class="col-xs-12">
                                <h2>List Product</h2>
                                <table class="table table-bordered">
                                    <thead class="table-head">
                                        <tr>
                                            <th>Picture</th>
                                            <th>Nama Produk</th>
                                            <th class="text-right">Qty</th>
                                            <th class="text-right">Harga</th>
                                            <th>Status Produk</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (isset($data_sel[LIST_DATA][1])) {
                                            foreach ($data_sel[LIST_DATA][1] as $each) {
                                                ?>

                                                <tr>
                                                    <td align="center" width="110">
                                                        <?php echo (($each->pic_1_img == "") ? $each->merchant_sku : "<img src='" . PRODUCT_UPLOAD_IMAGE . "/" . $each->merchant_seq . "/" . $each->pic_1_img . "' width = '100px'  height='100px' alt='" . $each->merchant_sku . "'><br />" . $each->merchant_sku); ?>
                                                    </td>
                                                    <td valign="top">
                                                        <?php echo $each->name; ?><input type="hidden" name="product_variant_seq[]" id="product_variant_seq" value="<?php echo $each->product_variant_seq; ?>">
                                                    </td>
                                                    <td valign="top" align="right">
                                                        <?php echo number_format($each->qty); ?>
                                                    </td>
                                                    <td valign="top" align="right">
                                                        <?php echo number_format($each->sell_price); ?>
                                                    </td>
                                                    <?php if ($statusorder == "N") { ?>
                                                        <td>
                                                            <select name="product_status[]" id="product_status" class="prdsts">
                                                                <option value="R"<?php echo(($each->product_status == "R") ? " selected" : ""); ?>>Terima</option>
                                                                <option value="X"<?php echo(($each->product_status == "X") ? " selected" : ""); ?>>Tolak</option>
                                                            </select>
                                                        </td>
                                                    <?php } else { ?>
                                                        <td>
                                                            <?php echo(($each->product_status == "R") ? " Terima" : "Tolak"); ?>
                                                        </td>
                                                    <?php } ?>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                <div class ="form-group">
                                    <label class ="control-label col-xs-3">
                                        Pesan untuk ekspedisi
                                    </label>
                                    <div class ="col-xs-8">
                                        <?php if ($statusorder == "N") { ?>
                                            <input type="text" class="form-control" id="ship_notes" name="ship_notes" value="<?php echo $data_sel[LIST_DATA][0][0]->ship_notes; ?>" maxlength="45">
                                        <?php } else { ?>
                                            <input type="text" class="form-control" id="ship_notes" name="ship_notes" value="<?php echo $data_sel[LIST_DATA][0][0]->ship_notes; ?>" disabled>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        if ($data_auth[FORM_ACTION] == ACTION_VIEW) {

                            if ($statusorder == "S" || $statusorder == "D" || $statusorder == "R") {
                                if ($data_sel[LIST_DATA][0][0]->real_expedition_service_seq == 0) {
                                    ?>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class ="form-group">
                                                <label class ="control-label col-xs-3">Expedisi *</label>
                                                <div class ="col-xs-8">
                                                    <select class="form-control" name="ship_by_exp_seq" id="ship_by_exp_seq" disabled>
                                                        <option value=''></option>
                                                        <?php
                                                        if (isset($exp_name)) {
                                                            foreach ($exp_name as $each) {
                                                                ?>
                                                                <option value="<?php echo $each->seq; ?>" <?php echo(($data_sel[LIST_DATA][0][0]->ship_by_exp_seq == $each->seq) ? ' selected' : '') ?>><?php echo $each->name; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                    <div id="merchant_ship" style="<?php echo (($data_sel[LIST_DATA][0][0]->ship_by != '')? : 'display:none') ?>;">
                                                        <input class="form-control" id="ship_by" name="ship_by" type="text" placeholder ="Input Nama Expedisi" value ="<?php echo $data_sel[LIST_DATA][0][0]->ship_by; ?>" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class ="form-group">
                                                <label class ="control-label col-xs-3">
                                                    No Resi
                                                    <span class="require">*</span>
                                                </label>
                                                <div class ="col-xs-8">
                                                    <input class="form-control" id="awb_no" name="awb_no" type="text" placeholder ="Input No Resi" value ="<?php echo $data_sel[LIST_DATA][0][0]->awb_no; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class ="form-group">
                                                <label class ="control-label col-xs-3">
                                                    Tanggal Resi <span class="requie">*</span>
                                                </label>
                                                <div class ="col-xs-3">
                                                    <input date_type="date" readonly class="form-control" id="ship_date" name="ship_date" type="text" placeholder ="Input Tanggal Resi" value ="<?php echo (($data_sel[LIST_DATA][0][0]->ship_date != '0000-00-00') ? date("d-M-Y", strtotime($data_sel[LIST_DATA][0][0]->ship_date)) : '' ); ?>" style="background-color :#fff;">
                                                </div>
                                            </div>
                                            <div class ="form-group">
                                                <label class ="control-label col-xs-3">Bukti Kirim <span class="require">*</span></label>
                                                <div class ="col-xs-8">
                                                    <?php echo (($data_sel[LIST_DATA][0][0]->ship_note_file != '') ? '<a href="' . RESI_UPLOAD_IMAGE . $data_sel[LIST_DATA][0][0]->ship_note_file . '" target="_blank"><img src="' .  RESI_UPLOAD_IMAGE . $data_sel[LIST_DATA][0][0]->ship_note_file . ' " alt="Bukti Kirim" height="200" border="0"></a>' : 'Tidak Ada Bukti Kirim') ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                        }
                        ?>
                        <hr/>
                        <div class ="form-group">
                            <?php if ($data_auth[FORM_ACTION] == ACTION_VIEW) { ?>

                            <?php } else { ?>
                                <div class ="col-xs-12">
                                    <?php if ($statusorder == "N") { ?>
                                        <input type="hidden" name="<?php echo CONTROL_SAVE_EDIT_NAME ?>" value="<?php echo ACTION_SAVE_UPDATE ?>">
                                        <button type="button" onclick="cekdata()" class="btn btn-success btn-merchant-width pull-right">Proses Order </button>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div><!-- /.box-footer -->
                    </section>
                </div>
            </form>
        </div>
    </div>
    <div class="modal modal-danger" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        Konfirmasi Penolakan Order
                    </h4>
                </div>
                <div class="modal-body">
                    <p>Semua produk yang diorder tidak tersedia.
                        <br />
                        Apakah anda akan menolak order ini ?
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" id="batal-order">Ya, tolak order ini !</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <script type="text/javascript">
        function backlist() {
            window.location = '<?php echo get_base_url() . $data_auth[FORM_URL]; ?>';
        }
        function cekdata() {
            status = "";
            $(".prdsts").each(function () {
                if ($(this).val() == "R") {
                    status += "ada";
                }
            });
            if (status == "") {
                batalorder();
                return false;
            } else {
                var r = confirm("Proses order ini ?");
                if (r == true) {
                    $("form#form1").submit();
                }

            }
        }
        ;
        function batalorder() {
            $('#myModal').modal();
        }
        $('#batal-order').on(
                'click',
                function (evt)
                {
                    $("#order_status").val("X");
                    $("form#form1").submit();
                    return true;
                }
        );
    </script>
    <?php
} elseif ($data_auth[FORM_ACTION] == ACTION_ADDITIONAL) {
    switch ($data_sel[LIST_DATA][0]->tipe) {
        case "bukti_kirim":
            ?>
            <link href="<?php echo get_css_url(); ?>daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
            <div class="clearfix">
                <div class="row no-margin">
                    <div class="col-xs-12">
                        <div class="box box-default no-border m-top10">
                            <form class="form-horizontal" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] . '?' . CONTROL_GET_TYPE . '=' . ACTION_ADDITIONAL . '&tipe=save_resi' ?>" enctype="multipart/form-data" name="frmMain" id="frmMain" onsubmit ="return validate_form();">
                                <div class="box-body">
                                    <section class="col-xs-12">
                                        <?php echo get_csrf_merchant_token(); ?>
                                        <input class="form-control"  name ="seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->seq; ?>">
                                        <input type="hidden" name="btnAdditional" value="act_s_adt">
                                        <input type="hidden" name="tipe" value="save_resi">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class ="form-group">
                                                    <label class ="control-label col-xs-12">Expedisi *</label>
                                                    <div class ="col-xs-12">
                                                        <select class="form-control" name="ship_by_exp_seq" id="ship_by_exp_seq" onchange="cekship(this.value)" validate="required[]">
                                                            <?php
                                                            if (isset($exp_name)) {
                                                                foreach ($exp_name as $each) {
                                                                    ?>
                                                                    <option value="<?php echo $each->seq; ?>"><?php echo $each->name; ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                        <div id="merchant_ship">
                                                            <input class="form-control no-show" id="ship_by" name="ship_by" type="text" placeholder ="Input Nama Expedisi" value ="<?php echo $data_list[LIST_RECORD][0]->ship_by; ?>" maxlength="50">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class ="form-group">
                                                    <label class ="control-label col-xs-12">No Resi *</label>
                                                    <div class ="col-xs-12">
                                                        <input class="form-control" id="awb_no" name="awb_no" type="text" placeholder ="Input No Resi" value ="<?php echo $data_list[LIST_RECORD][0]->awb_no; ?>" required validate="required[]" maxlength="100">
                                                    </div>
                                                </div>
                                                <div class ="form-group">
                                                    <label class ="control-label col-xs-12">Tanggal Resi *</label>
                                                    <div class ="col-xs-4">
                                                        <input date_type="date" readonly class="form-control" id="ship_date" name="ship_date" type="text" placeholder ="Input Tanggal Resi" value ="<?php echo (($data_list[LIST_RECORD][0]->ship_date != '0000-00-00') ? date("d-M-Y", strtotime($data_list[LIST_RECORD][0]->ship_date)) : date("d-M-Y") ); ?>" required validate="required[]" style="background-color:#ffffff;">
                                                    </div>
                                                </div>
                                                <div class ="form-group">
                                                    <label class ="control-label col-xs-12">Bukti Kirim *</label>
                                                    <div class ="col-xs-12 center">
                                                        <?php
                                                        echo '
                                                        <div class="slim" data-max-file-size="2" style="height:300px;width:300px;">
                                                        <input test type="file" accept="' . IMAGE_TYPE_UPLOAD . '"  name="ifile" ><br>
                                                        <img ' . (($data_list[LIST_RECORD][0]->ship_note_file != '') ? 'src="' . RESI_UPLOAD_IMAGE . $data_list[LIST_RECORD][0]->ship_note_file . '"' : '') . '" alt="Bukti Kirim">
                                                        </div> ';
                                                        ?>
                                                        <input type="hidden" id="ofile" name="ofile" value="<?php echo $data_list[LIST_RECORD][0]->ship_note_file; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        Nomor Order : <span class="label label-primary"><strong><?php echo $data_list[LIST_RECORD][0]->order_no; ?></strong></span><br />
                                                        Tanggal Order : <?php echo date("d-M-Y", strtotime($data_list[LIST_RECORD][0]->order_date)); ?>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        Status Order : <span class="label label-info"><strong><?php echo status($data_list[LIST_RECORD][0]->order_status, STATUS_ORDER); ?></strong></span>
                                                        <br>
                                                        <p>Catatan untuk ekspedisi</p>
                                                    </div>
                                                </div>
                                                <?php echo $data_list[LIST_RECORD][0]->ship_notes; ?>
                                                Penerima :<br>
                                                <strong><?php echo $data_list[LIST_RECORD][0]->receiver_name; ?></strong><br />
                                                <?php echo $data_list[LIST_RECORD][0]->receiver_address; ?><br />
                                                <?php echo $data_sel[LIST_DATA][1][0]->d_name; ?>, <?php echo $data_sel[LIST_DATA][1][0]->c_name; ?><br />
                                                <?php echo $data_sel[LIST_DATA][1][0]->p_name; ?>, Kode Pos <?php echo $data_list[LIST_RECORD][0]->receiver_zip_code; ?><br />
                                                Nomor Telp. : <?php echo $data_list[LIST_RECORD][0]->receiver_phone_no; ?><br />
                                                Pesan :<br>
                                                <?php echo $data_list[LIST_RECORD][0]->member_notes; ?>
                                            </div>
                                            <div class ="form-group">
                                                <?php if ($data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
                                                    <div class ="col-xs-6 pull-right"><?php echo get_back_button(); ?></div>
                                                <?php } else {
                                                    ?>
                                                    <div class ="col-xs-12 pull-right">
                                                        <hr>
                                                        <?php
                                                        if ($data_list[LIST_RECORD][0]->order_status == READY_STATUS_CODE || $data_list[LIST_RECORD][0]->order_status == PROCESS_STATUS_CODE) {
                                                            echo '<input type="submit" value="Simpan" id="btnsaveresi" class="pull-right btn btn-google-plus btn-success btn-merchant-width">';
                                                        }
                                                        if ($data_list[LIST_RECORD][0]->order_status == 'N')
                                                            echo '<div class="btn btn-danger" style="width:100%;">Data harus di Cetak dahulu</div>';
                                                        ?>
                                                    </div>

                                                <?php } ?>
                                            </div>
                                        </div>
                                        <!-- /.box-footer -->
                                    </section>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                <!--
                                        $('.slim').slim({
                    maxFileSize: "2",
                    onRemove: function (slim, data) {
                        $("#ofile").val("");
                    }
                })
                function cekship(expedisi) {
                    if (expedisi == 0) {
                        $("#merchant_ship").show();
                    } else {
                        $("#merchant_ship").hide();
                    }
                }
				
			  $('input[date_type="date"]').daterangepicker({
                    "locale": {
                        "format": "DD-MMM-YYYY"
                    },
                    singleDatePicker: true,
                    showDropdowns: true
                });

                $("#ship_by_exp_seq").val('<?php echo $data_list[LIST_RECORD][0]->ship_by_exp_seq; ?>');
                cekship('<?php echo $data_list[LIST_RECORD][0]->ship_by_exp_seq; ?>');
                //-->
            </script>
            <?php
            break;
    }
} else {
    $totals = 0;
    ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_css_url() ?>daterangepicker-bs3.css" />
    <div class="top-30">
        <div class="box-body">
            <!--SUB HEADER-->
            <div id="header">
                <div id="sub-navbar" class="box-body sub-header">
                    <div class="panel-body no-pad">
                        <form id ="frmSearchExcl" method ="get" url= "<?php echo get_base_url() . $data_auth[FORM_URL]; ?>">
                            <div class="row">
                                <div class="col-xs-8">
                                    <div class="form-group">
                                        <input class="form-control-cst" style="width: 40%" name="order_no_f" type="input" placeholder="Nomor Order" value="<?php if (isset($_GET["order_no_f"])) echo $_GET["order_no_f"]; ?>">
                                        <input date_type="date" readonly class="form-control-cst" style="width: 25%" id="order_date_s" name="order_date_s" type="text" placeholder="Tanggal awal order" value="<?php
                                        if (isset($_GET["order_date_s"])) {
                                            if (($_GET["order_date_s"] != ''))
                                                echo date("d-M-Y", strtotime($_GET["order_date_s"]));
                                        }
                                        ?>">

                                        <input date_type="date" readonly class="form-control-cst" style="width: 25%" id="order_date_e" name="order_date_e" type="text" placeholder="Tanggal akhir order" value="<?php
                                        if (isset($_GET["order_date_e"])) {
                                            if (($_GET["order_date_e"] != ''))
                                                echo date("d-M-Y", strtotime($_GET["order_date_e"]));
                                        }
                                        ?>">
                                        <button class="btn btn-danger" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                                <input type="hidden" id="start1" name="start" value="<?php
                                if (isset($_GET["start"])) {
                                    echo $_GET["start"];
                                } else {
                                    echo '1';
                                }
                                ?>">
                                <input type="hidden" id="length1" name="length" value="<?php
                                if (isset($_GET["length1"])) {
                                    echo $_GET["length1"];
                                } else {
                                    echo '10';
                                }
                                $codeActive = $this->input->get('status');
                                ?>">
                                <input type="hidden" name="status" id="status" value="<?php if (isset($_GET["status"])) echo $_GET["status"]; ?>">
                            </div>
                        </form>

                        <div class="col-xs-9 no-pad">
                            <ul class="nav nav-pills no-padding">
                                <!--<li><a class="text-block">Status Order :</a></li>-->
                                <li class="blue-link"><a class="btn btn-flat <?php echo ( (!isset($codeActive)) || $codeActive === '' ? 'btn-active' : '' ) ?>" href="<?php echo base_url($data_auth[FORM_URL]) . '?status=' . $filter_sort . $filter ?>">Semua (<span id="status_all"></span>)</a></li>
                                <?php
                                $qty_status = 0;
                                foreach (json_decode(STATUS_ORDER) as $key => $stat) {
                                    if ($key != "P") {
                                        if (isset($data_sel[LIST_DATA][2])) {
                                            $qty_status = get_qty_status($data_sel[LIST_DATA][2], $key);
                                            $totals = ($totals + $qty_status);
                                        } else {
                                            $qty_status = 0;
                                        }
                                        ?>
                                        <li class="blue-link"><a class="btn btn-flat <?php echo ($key === $codeActive ? 'btn-active' : '' ); ?>" href="<?php echo base_url($data_auth[FORM_URL]) . '?status=' . $key . $filter_sort . $filter ?>" <?php echo ($codeActive == $key ? 'class="btn-active"' : '' ) ?> ><?php echo $stat; ?> (<?php echo $qty_status ?>)</a></li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="col-xs-3">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <button class="btn btn-success btn-block btn-block flat" type="button" id="btnxls" name="btnxls" onclick="data_report()"><i class="fa fa-file-excel-o"></i> Excel</button>
                                        </div>
                                        <?php echo get_order_filter("order_date", "order_no", base_url($data_auth[FORM_URL]) . $filter_status . $filter, $this->input->get("sort"), $this->input->get("column_sort")) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <form method="post" action="" target="_blank" id="frmprint" name="frmprint">
                    <input type="hidden" name="btnAdditional" value="act_s_adt">
                    <input type="hidden" id="tipe" name="tipe" value="docprint"><input type="hidden" id="orderid" name="orderid" value="">
                    <div class="col-xs-4 pull-left no-pad" style="margin-bottom: 5px; margin-top: 5px;">
                        <div class="input-group input-group-xs">
                            <span class="input-group-addon">Pilih format</span>
                            <select class="form-control" id="format_type" name="format_type">
                                <option value=""></option>
                                <option value="A1">A4 (Faktur +Resi)</option>
                                <option value="A2">A5 (Faktur +Resi)</option>
                                <option value="A3">A5 (Faktur)</option>
                                <!-- <option value="A4">Stiker Resi</option>-->
                            </select>
                            <span class="input-group-btn">
                                <button class="btn btn-success btn-flat" type="button" id="btnprint" name="btnprint" onclick="docprint()"><i class="fa fa-fw fa-print"></i> Cetak</button>
                            </span>
                        </div>
                    </div>

                    <br style="margin-bottom:30px;" />
                    <div class="col-xs-12 no-pad" id="tabledata1">
                        <?php
                        if (isset($data_sel[LIST_DATA][1])) {
                            $status_edit = '';
                            $dataresi = '';
                            foreach ($data_sel[LIST_DATA][1] as $orders) {
                                ?>
                                <div class="box no-border">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-xs-4">Tanggal Order : <?php echo c_date($orders->order_date) ?></div>
                                            <div class="col-xs-4">Nomor Order : <strong><?php echo $orders->order_no ?></strong></div>
                                            <div class="col-xs-3">Status : <?php echo cstdes($orders->order_status, STATUS_ORDER); ?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4">Nama : <?php echo $orders->receiver_name; ?></div>
                                            <div class="col-xs-7">Pesan Member : <?php echo $orders->member_notes ?></div>
                                            <div class="col-xs-1 checkbox">
                                                <?php
                                                $dataresi = '';
                                                $status_edit = '';
                                                $chekbox = '';
                                                if ($orders->real_expedition_service_seq == "0") {
                                                    if ($orders->order_status == READY_STATUS_CODE || $orders->order_status == PROCESS_STATUS_CODE) {
                                                        if ($orders->awb_no == "") {
                                                            $dataresi = ' <a class="btn btn-warning btn-xs btn-flat btn-border-stskirim" href="?' . CONTROL_GET_TYPE . '=' . ACTION_ADDITIONAL . '&tipe=bukti_kirim&idh=' . $orders->seq . '">Upload Bukti kirim</a>';
                                                        } else {
                                                            $dataresi = ' <a class="btn btn-warning btn-xs btn-flat btn-border-stskirim" href="?' . CONTROL_GET_TYPE . '=' . ACTION_ADDITIONAL . '&tipe=bukti_kirim&idh=' . $orders->seq . '">Revisi Bukti Kirim</a>';
                                                        }
                                                    }
                                                }
                                                if ($orders->order_status == NEW_STATUS_CODE) {
                                                    $status_edit = '';
                                                } else {
                                                    $status_edit = 'no';
                                                }
                                                if (in_array($orders->order_status, array("P", "N", "X", "Y"))) {
                                                    $chekbox = "";
                                                    $dataresi = "";
                                                } else {
                                                    $chekbox = '
                                                    <div class="input-group"><span class="input-group-addon">Cetak</span>
                                                            <span class="input-group-addon">
                                                              <input type="checkbox" id="printc' . $orders->seq . '" name="printc[]" value="' . $orders->seq . '" class="prntorder">
                                                            </span>
                                                    </div>';
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4">Telpon : <?php echo $orders->receiver_phone_no; ?></div>
                                            <div class="col-xs-4">Nilai Order : <?php echo cnum($orders->total_merchant); ?></div>
                                            <div class = "col-xs-4">No AWB : <?php echo $orders->awb_no; ?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8">Alamat : <?php echo $orders->receiver_address . ', ' . $orders->districtname . ', ' . $orders->cityname . ', ' . $orders->provincename . ', ' . $orders->receiver_zip_code; ?></div>
                                            <div class = "col-xs-4 ">
                                                <div class="row">
                                                    <div class = "col-xs-5"><?php echo $dataresi; ?></div>
                                                    <div class = "col-xs-3">
                                                        <?php
                                                        if ($status_edit == '') {
                                                            echo "<a href='?" . CONTROL_GET_TYPE . "=" . ACTION_EDIT . "&key=" . $orders->seq . "' class='btn btn-xs btn-flat btn-border-edit' title='Edit'><i class = 'fa fa-pencil'></i> Edit</a>";
                                                        } else {
                                                            echo "<a href='?" . CONTROL_GET_TYPE . "=" . ACTION_VIEW . "&key=" . $orders->seq . "' class='btn btn-xs btn-flat btn-border-view' title='Lihat'><i class = 'fa fa-eye'></i> Lihat</a>";
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class = "col-xs-4"><?php echo $chekbox; ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        } else {
                            ?>
                            <div class="col-xs-12 no-padding">
                                <div class="box box-no-border-top">
                                    <div class="box-body min-height300">
                                        <center class="no-found-src" >Tidak ada data order yang di tampilkan. </center>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="col-xs-12 no-pad">
                            <div class="box-pagging">
                                <center>
                                    <?php echo $this->pagination->create_links(); ?>
                                </center>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
            <div class="collapse" id="detail"></div>
        </div>
    </div>
    <div class="modal modal-danger" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Konfirmasi Penolakan Order</h4>
                </div>
                <div class="modal-body">
                    <p>Semua produk yang diorder tidak tersedia.<br />
                        Apakah anda akan menolak order ini ?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" id="batal-order">Ya, tolak order ini !</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <script type="text/javascript">

        $(document).ready(function () {
                $('#order_date_s').val('<?php echo isset($_GET['order_date_s']) ? $_GET['order_date_s'] : '' ?>');
                $('#order_date_e').val('<?php echo isset($_GET['order_date_e']) ? $_GET['order_date_e'] : '' ?>');
            });
            $('input[date_type="date"]').daterangepicker({
                "locale": {
                    "format": "DD-MMM-YYYY"
                },
                singleDatePicker: true,
                showDropdowns: true
            });

        $(document).scroll(function () {
            var a = $('body').scrollTop();
            if (a > 100) {
                $('#print_sticker').addClass('sticker-fix');
            } else {
                $('#print_sticker').removeClass('sticker-fix');
            }
        });

        $("#status_all").html("<?php echo $totals; ?>");

        function docprint() {

            status = "";
            $("input:checkbox[class=prntorder]").each(function () {
                if ($(this).is(":checked"))
                    status += "ada";
            });
            if ($("#format_type").val() == '')
                status = "";
            if (status == "") {
                alert("Pilih minimal satu order untuk dicetak !!!");
                return false;
            } else {
                $("form#frmprint").submit();
            }
        }

        

        function data_report() {
            var data = $('#frmSearchExcl').serializeArray();
            data.push({name: 'type', value: 'order'});
            $.ajax({
                url: "<?php echo get_base_url(); ?>merchant/report",
                data: data,
                type: "get",
                success: function (response) {
                    if (isSessionExpired(response)) {
                    } else {
                        try {
                            response = jQuery.parseJSON(response);
                            if (response != null) {
                                create_report(response);
                            } else {
                                alert("Tidak ada data.");
                            }
                        } catch (e) {
                            alert("Tidak ada data.");
                        }
                        return false;
                    }
                },
                error: function (request, error) {
                    alert(error_response(request.status));
                }
            });
        }
    </script>
    <?php
}
require_once get_include_page_list_merchant_content_footer();
require_once get_include_content_merchant_bottom_page_navigation();
?>