<?php
require_once VIEW_BASE_MERCHANT;
require_once get_include_content_merchant_top_page_navigation();
echo $breadcrumb;
?>
<?php if ($data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <?php if (isset($data_sel[LIST_DATA][0])) { ?>
        <div class="p-10px">
            <div class="box no-border no-margin">
                <div class="box-body">
                    <div class="col-xs-8">
                        <dl class="dl-horizontal"><br />
                            <dt>Merchant :</dt>
                            <dd><?php echo $data_sel[LIST_DATA][0]->name; ?></dd>
                            <dt>Total :</dt>
                            <dd>Rp. <?php echo number_format($data_sel[LIST_DATA][0]->total); ?></dd>
                            <dt>Bank :</dt>
                            <dd><?php echo $data_sel[LIST_DATA][0]->bank_name; ?></dd>
                            <dt>Cabang :</dt>
                            <dd><?php echo $data_sel[LIST_DATA][0]->bank_branch_name; ?></dd>
                            <dt>No Akun :</dt>
                            <dd><?php echo $data_sel[LIST_DATA][0]->bank_acct_no; ?></dd>
                            <dt>Nama Akun :</dt>
                            <dd><?php echo $data_sel[LIST_DATA][0]->bank_acct_name; ?></dd>
                            <dt>Tanggal Pembayaran:</dt>
                            <dd><?php echo c_date($data_sel[LIST_DATA][0]->paid_date); ?></dd>
                        </dl>
                    </div>
                    <div class="col-xs-4">
                        <table class="table table-condensed">
                            <tr>
                                <th>Tipe</th>
                                <th>Jumlah</th>
                            </tr>
                            <?php
                            foreach ($data_sel[LIST_DATA][1] as $each) {
                                ?>
                                <tr>
                                    <td>
                                        <?php echo status($each->type, REDEEM_TYPE); ?>
                                    </td>
                                    <td>
                                        <?php echo number_format($each->total); ?>
                                    </td>
                                </tr>
                            <?php }
                            ?>
                            <tr>
                                <td colspan='2'>
                                    <br />
                                    <div class="pull-right"><span style="color:green">*</span><span class="info-text text-green">Cetak Dalam Bentuk microsoft excel</span></div>
                                    <button class="btn btn-warning btn-merchant-width btn-flat pull-right" type="button" id="btnxls" name="btnxls" onclick="data_report()">
                                        <i class="fa fa-file-excel-o"></i>
                                        Excel
                                    </button>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <div class="p-10px">
            <div class="box no-border">
                <!-- /.box-header -->
                <div class="box-body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#t_1" aria-controls="t_1" role="tab" data-toggle="tab">Detil Order</a></li>
                        <li role="presentation"><a href="#t_2" aria-controls="t_2" role="tab" data-toggle="tab">Detil Ekspedisi</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="t_1"><br />
                            <div class="row">
                                <div class="col-xs-12">
                                    <table class="table table-striped table-bordered">
                                        <thead class="table-head">
                                            <tr>
                                                <th class="text-right">No. Order</th>
                                                <th class="text-right">Nilai Order</th>
                                                <th class="text-right">Nilai Ekpedisi</th>
                                                <th class="text-right">Nilai Komisi</th>
                                                <th class="text-right">Total Redeem</th>
                                            </tr>
                                        </thead>
                                        <?php
                                        if (isset($data_sel[LIST_DATA][2])) {
                                            $ttlorder = 0;
                                            $ttlexp = 0;
                                            $ttlfee = 0;
                                            $ttlall = 0;
                                            foreach ($data_sel[LIST_DATA][2] as $each) {
                                                ?>
                                                <tr>
                                                    <td><a href="javascript:detail_order('<?php echo $each->order_no; ?>')"><?php echo $each->order_no; ?></a></td>
                                                    <td align="right"><?php echo number_format($each->totalorder); ?></td>
                                                    <td align="right"><?php echo number_format($each->totalexpedition * (100 - $data_sel[LIST_DATA][0]->expedition_commission) / 100); ?></td>
                                                    <td align="right"><?php echo number_format($each->totalfee); ?></td>
                                                    <td align="right"><?php echo number_format($each->totalorder + $each->totalexpedition * (100 - $data_sel[LIST_DATA][0]->expedition_commission) / 100 - $each->totalfee); ?></td>
                                                </tr>
                                                <tr id='<?php echo $each->order_no; ?>'></tr>
                                                <?php
                                                $ttlorder = ($ttlorder + $each->totalorder);
                                                $ttlexp = ($ttlexp + $each->totalexpedition * (100 - $data_sel[LIST_DATA][0]->expedition_commission) / 100);
                                                $ttlfee = ($ttlfee + $each->totalfee);
                                                $ttlall = ($ttlall + $each->totalorder + $each->totalexpedition * (100 - $data_sel[LIST_DATA][0]->expedition_commission) / 100 - $each->totalfee);
                                            }
                                            echo '
					<tr>
					    <td align="right">Total</td>
	    				    <td align="right">' . number_format($ttlorder) . '</td>
	    				    <td align="right">' . number_format($ttlexp) . '</td>
	    				    <td align="right">' . number_format($ttlfee) . '</td>
	    				    <td align="right">' . number_format($ttlall) . '</td>
	    				</tr>';
                                        }
                                        ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="t_2"><br />
                            <div class="row">
                                <div class="col-xs-12">
                                    <table class="table table-striped table-bordered">
                                        <thead class="table-head">
                                            <tr>
                                                <th>No. Order</th>
                                                <th>Tujuan</th>
                                                <th>Nilai Ekpedisi</th>
                                            </tr>
                                        </thead>
                                        <?php
                                        if (isset($data_sel[LIST_DATA][3])) {
                                            $totalexpedition = 0;
                                            foreach ($data_sel[LIST_DATA][3] as $each) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $each->order_no; ?></td>
                                                    <td align="right"><?php echo $each->destination; ?></td>
                                                    <td align="right"><?php echo number_format($each->totalorder); ?></td>
                                                </tr>
                                                <?php
                                                $totalexpedition = ($totalexpedition + $each->totalorder);
                                            }
                                            echo '
					<tr>
					    <td align="right" colspan=2>Total</td>
	    				    <td align="right">' . number_format($totalexpedition) . '</td>
	    				</tr>';
                                        }
                                        ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>


        <script type="text/javascript">
        <!--

            function data_report() {
                $.ajax({
                    url: "<?php echo get_base_url(); ?>merchant/report",
                    data: {type: 'redeem', key: '<?php echo $data_sel[LIST_DATA][0]->redeem_seq; ?>'},
                    type: "get",
                    success: function (response) {
                        if (isSessionExpired(response)) {
                        } else {
                            try {
                                response = jQuery.parseJSON(response);
                                if (response != null) {
                                    create_report(response);
                                } else {
                                    alert("Tidak ada data.");
                                }
                            } catch (e) {
                                alert("Tidak ada data.");
                            }
                            return false;
                        }
                    },
                    error: function (request, error) {
                        alert(error_response(request.status));
                    }
                });
            }
            function detail_order(order_no) {
                var url = "<?php echo get_base_url() . $data_auth[FORM_URL]; ?>";
                inHTML = "";
                $("#" + order_no).html("");
                $.ajax({
                    url: url,
                    data: {
        <?php echo CONTROL_GET_TYPE; ?>: "<?php echo ACTION_ADDITIONAL; ?>", key: order_no + "~<?php echo $data_sel[LIST_DATA][0]->redeem_seq; ?>"
                    },
                    type: "get", datatype: 'json',
                    success: function (response) {
                        if (isSessionExpired(response)) {
                            response_object = json_decode(response);
                            url = response_object.url;
                            location.href = url;
                        } else {

                            //alert(response);
                            var dataobj = $.parseJSON(response);
                            inHTML = '<td colspan=5><table class="table table-condensed "><tr class="info"><td class="col-md-4">Produk <a href="javascript:colapse(\'' + order_no + '\')" class="pull-right btn btn-default btn-xs"><i class="fa fa-minus"></i></a></td><td class="text-right">Harga</td><td class="text-right">Jumlah</td><td class="text-right">Total</td><td class="text-right">Komisi</td><td class="text-right">Redeem</td></tr>';
                            $.each(dataobj, function () {
                                inHTML += '<tr><td class="col-md-4">' + this['product_name'] + '</td>' +
                                        '<td class="pull-right">' + this['sell_price'] + '</td>' +
                                        '<td class="text-right">' + this['qty'] + '</td>' +
                                        '<td class="text-right">' + this['totalorder'] + '</td>' +
                                        '<td class="text-right">' + this['totalfee'] + '</td>' +
                                        '<td class="text-right">' + this['totalredeem'] + '</td></tr>';
                            });
                            inHTML += "</table></td>";
                            $("#" + order_no).html(inHTML);
                        }
                    },
                    error: function (request, error) {
                        alert(error_response(request.status));
                    }
                });
            }

            function colapse(order_no) {
                $("#" + order_no).html("");
            }

            //-->
        </script>
        <?php
    } else {
        echo '  <div class="col-xs-12 no-padding">
                  <div class="box box-no-border-top">
                   <div class="box-body min-height300">Data redeem tidak ditemukan.</div>
                  </div>
                </div>';
    }
    ?>
<?php } else { ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_css_url() ?>daterangepicker-bs3.css" />
    <div class="box-body">
        <div class="header">
            <div class="row no-margin">
                <div id="sub-navbar" class="box-body sub-header">
                    <div class="row">
                        <div class="col-xs-6">
                            <form action ="<?php echo base_url($data_auth[FORM_URL]) ?>" method="get">
                                <div class="form-group no-margin no-pad">
                                    <input id="order_date_s" class="form-control-cst" name="tanggal1" type="text" date_type="date" readonly style="background-color: #fff;width: 40%;" placeholder="Tanggal awal redeem" value="<?php if (isset($_GET["tanggal1"])) echo $_GET["tanggal1"]; ?>">
                                    <input id="order_date_e" class="form-control-cst" name="tanggal2" type="text" date_type="date" readonly style="background-color: #fff;width: 40%;" placeholder="Tanggal akhir redeem" value="<?php if (isset($_GET["tanggal2"])) echo $_GET["tanggal2"]; ?>">
                                    <button class="btn btn-danger" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </div>
                            </form>
                        </div>

                        <div class="col-xs-3"></div>
                        <div class="col-xs-3">
                            <div class="row">
                                <div class="col-xs-6"></div>
                                <?php echo get_order_filter("created_date", "", base_url($data_auth[FORM_URL]) . $filter_status . $filter, $this->input->get("sort"), $this->input->get("column_sort")) ?>
                            </div>
                        </div>
                        <!--                    </div>-->
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <br>

        <?php if (isset($data_sel[LIST_DATA][1])) { ?>
            <?php foreach ($data_sel[LIST_DATA][1] as $merchant) {
                ?>

                <div class="box box-no-border-top no-padding">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-4">Tanggal Pembayaran : <?php echo c_date($merchant->paid_date); ?></div>
                            <div class="col-xs-4">Jumlah Pebayaran : Rp. <?php echo cnum($merchant->total); ?></div>
                            <div class="col-xs-2"><?php echo ($merchant->notif_status == "U" ? "Belum Dilihat" : ""); ?></div>
                            <div class="col-xs-2 pull-right"><a class="btn btn-border-stskirim btn-flat btn-xs" href="<?php echo base_url($data_auth[FORM_URL]) . "?" . CONTROL_GET_TYPE . "=" . ACTION_VIEW . "&key=" . $merchant->redeem_seq; ?>">Rincian</a></div>
                        </div>
                    </div>
                </div>

                <?php
            }
        } else {
            ?>
            <div class="col-xs-12 no-padding">
                <div class="box box-no-border-top">
                    <div class="box-body min-height300">
                        <center class="no-found-src" >Tidak ada data redeem yang di tampilkan. </center>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="col-xs-12 no-pad">
            <div class="box-pagging">
                <center>
                    <?php echo $this->pagination->create_links(); ?>
                </center>
            </div>
        </div>
    </div>
    </div>
    <script  type="text/javascript">
            $(document).ready(function () {
                $('#order_date_s').val('<?php echo isset($_GET['tanggal1']) ? $_GET['tanggal1'] : '' ?>');
                $('#order_date_e').val('<?php echo isset($_GET['tanggal2']) ? $_GET['tanggal2'] : '' ?>');
            });
            $('input[date_type="date"]').daterangepicker({
                "locale": {
                    "format": "DD-MMM-YYYY"
                },
                singleDatePicker: true,
                showDropdowns: true
            });
    </script>
    <?php
}
require_once get_include_page_list_merchant_content_footer();
require_once get_include_content_merchant_bottom_page_navigation();
?>