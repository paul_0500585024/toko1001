<?php
require_once VIEW_BASE_MERCHANT;
require_once get_include_content_merchant_top_page_navigation();
echo $breadcrumb;
?>
<link rel="stylesheet" type="text/css" href="<?php echo get_css_url() ?>daterangepicker-bs3.css" />
<script type="text/javascript" src="<?php echo get_js_url() ?>moment.min.js"></script>
<script type="text/javascript" src="<?php echo get_js_url() ?>daterangepicker.js"></script>
<div class="box box-default">
    <div class="box-body">

        <section class="col-xs-12">
            <form id="frmSearch" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
                <?php echo get_csrf_merchant_token(); ?>
                <div class="col-xs-6">
                    <input type="hidden" name="return_no" class="form-control" value="<?php echo $data_sel[LIST_DATA][0]->return_no; ?>">
                    <div class="form-group">
                        <label>No. Pengembalian</label>
                        <input type="text" class="form-control" value="<?php echo $data_sel[LIST_DATA][0]->return_no; ?>" readonly>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="form-group">
                        <label>Tgl. Pengajuan</label>
                        <input type="text" class="form-control" value="<?php echo c_date($data_sel[LIST_DATA][0]->created_date); ?>" readonly>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="form-group">
                        <label>Tgl. Terima</label>
                        <input validate ="required[]" class="form-control" name="tgl_terima" type="input" value="<?php
                        if (!empty($data_sel[LIST_DATA][0]->merchant_received_date)) {
                            if ($data_sel[LIST_DATA][0]->merchant_received_date == "0000-00-00 00:00:00") {
                                echo c_date(date('Y-m-d'));
                            } else {
                                echo c_date($data_sel[LIST_DATA][0]->merchant_received_date);
                            }
                        } else {
                            echo c_date(date('Y-m-d'));
                        }
                        ?>">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-6">
                        <?php
                        if ($data_sel[LIST_DATA][0]->shipment_status == SHIP_TO_MERCHANT) {
                            echo get_save_edit_button();
                        }
                        ?>
                    </div>
                    <div class="col-xs-6">
                        <a href="<?php echo get_base_url(); ?>merchant/transaction/return_merchant/" class="btn btn-google-plus" style="width: 100%;"><i class="fa fa-angle-double-left"></i>&nbsp;Kembali</a>
                    </div>
                </div>
            </form>
        </section>
    </div>
</div>
<script  type="text/javascript">
                $('input[name="tgl_terima"]').daterangepicker({
                    format: 'DD-MMM-YYYY',
                    singleDatePicker: true,
                    showDropdowns: true,
                    startDate: "<?php echo date('d-M-Y') ?>"
                });
</script>
<?php
require_once get_include_page_list_merchant_content_footer();
require_once get_include_content_merchant_bottom_page_navigation();
?>