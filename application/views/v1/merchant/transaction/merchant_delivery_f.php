<?php
require_once VIEW_BASE_MERCHANT;
?>
<link rel="stylesheet" type="text/css" href="<?php echo get_css_url() ?>daterangepicker-bs3.css" />
<script type="text/javascript" src="<?php echo get_js_url() ?>moment.min.js"></script>
<script type="text/javascript" src="<?php echo get_js_url() ?>daterangepicker.js"></script>
<form id ="frmSearch"  url= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
    <div class="form-group">
        <label>Nomor Order</label>
        <input class="form-control" name="order_no_f" type="input">
    </div>
    <div class="form-group">
        <label>Tanggal Order</label>
        <input class="form-control" name="order_date_s" type="input">
    </div>
    <div class="form-group">
        <label>S/D</label>
        <input class="form-control" name="order_date_e" type="input">
    </div>
    <div class="form-group">
        <label>Status Order</label>
	<select class="form-control select2" name="status_order_f">
	    <option value="">Semua</option>
	    <?php
	    echo combostatus(json_decode(STATUS_ORDER));
	    ?>
	</select>
    </div>
    <div class="form-group">
	<?php echo get_search_button(); ?>
    </div>
</form>
<script  type="text/javascript">
    $('input[name="order_date_s"]').daterangepicker({
	format: 'DD-MMM-YYYY',
	singleDatePicker: true,
	showDropdowns: true
    });
    $('input[name="order_date_e"]').daterangepicker({
	format: 'DD-MMM-YYYY',
	singleDatePicker: true,
	showDropdowns: true
    });
</script>