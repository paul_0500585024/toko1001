<?php
require_once VIEW_BASE_MERCHANT;
require_once get_include_content_merchant_top_page_navigation();
?>
<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
    <div class="box box-default">
        <div class="box-header with-border">
    	<h3 class="box-title"><?php echo $data_auth[FORM_ACTION_TITLE] . $data_auth[FORM_AUTH][FORM_TITLE] ?></h3>
        </div>
        <form class="form-horizontal" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>" enctype="multipart/form-data" name="product_new" id="product_new">
    	<div class="box-body">
    	    <section class="col-md-12">
		    <?php echo get_csrf_merchant_token(); ?>
		    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
			<input class="form-control"  name ="seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->seq; ?>">
		    <?php } ?>

		<?php } else {
		    ?>
    		<div class="box box-default">
    		    <div class="box-header with-border">
    			<h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
    		    </div>
    		    <div class="box-body">
			    <?php require_once get_include_page_list_merchant_content_header(); ?>
    			<table id="tbl" class="display table table-bordered table-striped" cellspacing="0" width="100%">
    			    <thead>
    				<tr>
    				    <th column="order_date"> Tanggal Order </th>
    				    <th column="order_no"> No. Order </th>
    				    <th column="receiver_name"> Penerima </th>
    				    <th column="receiver_address"> Alamat Penerima </th>
    				    <th column="order_status"> Status Order </th>
    				    <th column="paid_date"> Tanggal Pembayaran </th>
    				    <th column="produk"> Produk </th>
    				</tr>
    			    </thead>
    			</table>
    		    </div>
    		</div>
		    <?php
		    $tablesort = 'desc';
		}
		require_once get_include_page_list_merchant_content_footer();
		require_once get_include_content_merchant_bottom_page_navigation();
		?>