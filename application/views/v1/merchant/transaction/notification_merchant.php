<?php
require_once VIEW_BASE_MERCHANT;
require_once get_include_content_merchant_top_page_navigation();
echo $breadcrumb;
?>
<style>
    .read_text{opacity: 0.8;}
    a{cursor:pointer}
</style>
<link rel="stylesheet" type="text/css" href="<?php echo get_css_url() ?>daterangepicker-bs3.css" />
<script type="text/javascript" src="<?php echo get_js_url() ?>moment.min.js"></script>
<script type="text/javascript" src="<?php echo get_js_url() ?>daterangepicker.js"></script>
<?php if($data_auth[FORM_ACTION] == ACTION_VIEW) {?>
<?php if (isset($data_sel[LIST_DATA])): ?>
    <?php foreach ($data_sel[LIST_DATA][0] as $each_data_sel): ?>
        <div class="p-10px" id="header">
            <div class="box box-default no-border">        
                <div class="col-xs-12 no-pad">
                    <div class="box box-default no-border">
                        <div class="body-header">
                            <div style="padding-right: 10px;padding-top: 10px;">
                                <div class="pull-right">
                                    <?php echo date('j F, Y', strtotime($each_data_sel->created_date)); ?>
                                </div>
                            </div>
                        </div>                        
                        <div class="box-body">
                            <div class="col-xs-12">
                                <strong><?php echo $each_data_sel->subject; ?></strong><br><br>
                                <?php echo $each_data_sel->content; ?>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    <?php endforeach; ?>
    <div id="detail"></div>
    <div class="modal modal-danger" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            &times;
                        </span>
                    </button>
                    <h4 class="modal-title">
                        Konfirmasi Pembatalan Order
                    </h4>
                </div>
                <div class="modal-body">
                    <p>Semua produk yang diorder tidak tersedia.
                        <br />
                        Apakah anda akan membatalkan order ini ?
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" id="batal-order">Ya, batalkan order ini !</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php endif; ?>
<script>
    $(document).ready(function(){
       $("a[id^='order_seq']").click(function(){
          order_seq = $(this).attr('id').replace('order_seq_','');
          window.location=('<?php echo base_url()?>merchant/transaction/merchant_delivery?<?php echo CONTROL_GET_TYPE?>=<?php echo ACTION_EDIT?>&<?php echo KEY?>='+order_seq);
       });
    });
    $('#batal-order').on(
            'click',
            function(evt)
            {
                $("#order_status").val("X");
                save_edit();
                $('#myModal').modal('hide');
            }
    );
    
    function cekdata() {
        status = "";
        $(".prdsts").each(function() {
            if ($(this).val() == "R") {
                status += "ada";
            }
        });
        if (status == "") {
            batalorder();
        } else {
            save_edit();
        }
    }
    function batalorder() {
        $('#myModal').modal();
    }
    function save_edit() {
        var seq2 = $("#seq_2").val();
        var urls = $("#form2");
        $.ajax({
            url: '<?php echo base_url() ?>merchant/transaction/merchant_delivery', data: urls.serialize(), type: 'POST', datatype: 'json',
            success: function(data) {
                alert("Data telah berhasil disimpan");
                location.reload();
            }
        });
    }
    function cancel_edit() {
        $("#detail").hide("slow");
        $("#detail").html("");
        $("#header").show("slow");
    }

</script>
<?php }else{?>
<?php if (isset($data_list[LIST_DATA])): ?>
    <?php foreach ($data_list[LIST_DATA] as $each_list_data): ?>
        <div class="p-10px">
            <div class="box box-default no-border">        
                <div class="col-xs-12 no-pad <?php if ($each_list_data->status == 'R') echo " read_text" ?>">
                    <div class="box box-default no-border">
                        <a onclick='read_notif("<?php echo $each_list_data->seq ?>")'>
                            <div class="box-body">
                                <div class="col-xs-12 <?php if ($each_list_data->status == 'R') echo " read_text" ?>">
                                    <strong><?php echo $each_list_data->subject; ?></strong><br>
                                    <?php echo date('j F, Y', strtotime($each_list_data->created_date)); ?>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    <?php endforeach; ?>
<?php endif; ?>
<div class="box box-default no-border">
    <div class="col-xs-10 col-xs-offset-2">
        <center>
            <?php echo $this->pagination->create_links(); ?>
        </center>
    </div>
    <div class="clearfix"></div>
</div>
<?php } ?>
<?php
require_once get_include_page_list_merchant_content_footer();
require_once get_include_content_merchant_bottom_page_navigation();
?>