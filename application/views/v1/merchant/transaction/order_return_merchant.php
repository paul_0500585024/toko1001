<?php
require_once VIEW_BASE_MERCHANT;
require_once get_include_content_merchant_top_page_navigation();
echo $breadcrumb;

function get_qty_status($arraystatus, $val) {
    foreach ($arraystatus as $key => $obj) {
        if ($obj->shipment_status == $val) {
            return $obj->qty_status;
            break;
        }
    }return 0;    
}

?>
<?php if ($data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) {
    ?>

    <style>
        .sub-box{
            background: none !important;
            border: none;   
            border-top: 1px solid #ddd;
            border-radius: 0px;
        }
        .min-250px-height{
            min-height: 250px !important
        }
        .h-200px{
            min-height: 200px !important;
        }


    </style>

    <form class ="form-horizontal" id="frmSearch" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
        <div class="p-10px">
            <div class="row">
                <div class="col-xs-12">
                    <?php
                    echo get_csrf_merchant_token();
                    ?> 
                    <div class="box box-no-border-top">
                        <br>

                        <div class="col-xs-6">

                            <div class="panel panel-default">
                                <div class="panel-heading"><b>Detil Pengembalian</b></div>
                                <div class="panel-body h-200px">
                                    <p><span class="fa fa-tags fa-lg"></span>&nbsp; <label class="control-label">No. Return : </label> <?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->return_no : "") ?></p>
                                    <p><span class="fa fa-shopping-cart fa-lg"></span>&nbsp; <label class="control-label"> No. Order : </label> <?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->order_no : "") ?></p>
                                    <p><span class="fa fa-calendar fa-lg"></span>&nbsp; <label class="control-label"> Tgl. Pengajuan Pengembalian  : </label> <?php echo c_date(isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->created_date : ""); ?> </p>
                                    <p><span class="fa fa-exclamation-circle fa-lg"></span>&nbsp; <label class="control-label"> Status Pengembalian  : </label>
                                        <?php
                                        if (isset($data_sel[LIST_DATA][0]->return_status)) {
                                            if ($data_sel[LIST_DATA][0]->return_status == REFUND_BY_MERCHANT) {
                                                echo "Pengembalian Dana";
                                            } elseif ($data_sel[LIST_DATA][0]->return_status == RETURN_ITEM_BY_MERCHANT) {
                                                echo "Pengembalian Barang";
                                            } elseif ($data_sel[LIST_DATA][0]->return_status == REJECT_BY_MERCHANT) {
                                                echo "Ditolak";
                                            } elseif ($data_sel[LIST_DATA][0]->return_status == NEW_STATUS_CODE) {
                                                echo "Pengajuan";
                                            }
                                        } else {
                                            echo "";
                                        }
                                        ?>
                                    </p>
                                    <p><span class="fa fa-truck fa-lg"></span>&nbsp; <label class="control-label"> Status Pengiriman  : </label>
                                        <?php
                                        if (isset($data_sel[LIST_DATA][0]->shipment_status)) {
                                            if ($data_sel[LIST_DATA][0]->shipment_status == RECEIVED_BY_MEMBER) {
                                                echo "Diterima Member";
                                            } elseif ($data_sel[LIST_DATA][0]->shipment_status == SHIP_TO_MERCHANT) {
                                                echo "Dikirim ke Merchant";
                                            } elseif ($data_sel[LIST_DATA][0]->shipment_status == RECEIVED_BY_MERCHANT) {
                                                echo "Diterima Merchant";
                                            } elseif ($data_sel[LIST_DATA][0]->shipment_status == SHIP_TO_MEMBER) {
                                                echo "Dikirim ke Member";
                                            }
                                        } else {
                                            echo "";
                                        }
                                        ?>
                                    </p>
                                </div>
                            </div>

                        </div><!--end col-xs-6-->

                        <div class="col-xs-6">
                            <div class="panel panel-default">
                                <div class="panel-heading"><b>Info Member</b></div>
                                <div class="panel-body h-200px">
                                    <p><span class="fa fa-user fa-lg"></span>&nbsp;&nbsp;&nbsp;<label class="control-label"> Nama : </label> <?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->member_name : "") ?></p>
                                    <p><span class="fa fa-envelope fa-lg"></span>&nbsp; <label class="control-label"> Email : </label> <?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->email_member : "") ?></p>                                    
                                    <p><span class="fa fa-phone fa-lg"></span>&nbsp; <label class="control-label"> Telepon : </label> <?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->member_name : "") ?></p>
                                    <p><span class="fa fa-home fa-lg"></span>&nbsp; <label class="control-label"> Alamat  : </label> 
                                        <?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->receiver_address : "") ?> -
                                        <?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->district_name : "") ?> -
                                        <?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->city_name : "") ?> -
                                        <?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->province_name : "") ?> 
                                    </p>
                                </div>
                            </div>
                        </div><!--end col-xs-6-->
                        <div class="clearfix"></div>
                    </div>
                    <div class="box box-no-border-top">
                        <div class="header" style="padding-top: 10px;padding-left: 10px;">
                            <b>Detail Produk</b>
                        </div>
                        <br>
                        <div class="col-xs-12">
                            <div class="well sub-box">
                                <?php if (isset($data_sel[LIST_DATA][0]->product_seq)) { ?>
                                    <div class="table-responsive">
                                        <table class="table table-bordered  ">
                                            <thead class="table-head">
                                                <tr>                                    
                                                    <th>Nama Produk</th>
                                                    <th>Variant Produk</th>
                                                    <th>Harga Produk (Rp.)</th>
                                                    <th>Qty</th>
                                                    <th>Total (Rp.)</th>
                                                    <th>Gambar Produk</th>
                                                </tr>
                                            </thead>
                                            <?php foreach ($data_sel[LIST_DATA][1] as $each) {
                                                ?>
                                                <tr>
                                                    <td><?= $each->product_name; ?></td>
                                                    <td><?= $each->value; ?></td>
                                                    <td><?= cnum($each->sell_price); ?></td>
                                                    <td><?= $each->qty; ?></td>
                                                    <td><?= cnum($each->sell_price * $each->qty); ?></td>
                                                    <td><img src="<?php echo get_base_url() . PRODUCT_UPLOAD_IMAGE . '/' . $each->merchant_seq . '/' . $each->pic_1_img ?>" style="width:50px;height:50px;"></td>
                                                </tr>
                                            <?php }
                                            ?>
                                        </table>
                                    <?php } ?>  
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="box box-no-border-top">
                        <div class="header" style="padding-top: 10px;padding-left: 10px;">
                            <b>Proses Pengembalian Barang</b>
                        </div>
                        <br>
                        <div class="col-xs-12">
                            <div class="col-xs-12">
                            </div>
                            <div class="well sub-box">
                                <br>
                                <div class="col-xs-4">
                                    <div class="panel panel-default" style="height: 250px;">
                                        <div class="panel-heading">Member</div>
                                        <div class="panel-body">
                                            <div class="" style="font-size: .9em;">Tgl. Pengajuan </div>
                                            <b><?php echo c_date(isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->created_date : "") ?></b>
                                            <div class="" style="font-size: .9em;">Ekspedisi</div>
                                            <b><?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->exp_to_admin : "") ?></b>
                                            <div class="" style="font-size: .9em;">No. Resi</div>
                                            <b><?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->awb_member_no : "") ?></b>
                                            <div class="" style="font-size: .9em;">Pesan Member</div>
                                            <b><?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->review_member : "") ?></b>
                                            <div class="" style="font-size: .9em;">Tanggal Terima Barang</div>
                                            <b><?php
                                                if ($data_sel[LIST_DATA][0]->member_received_date == DEFAULT_DATETIME) {
                                                    echo "-";
                                                } else {
                                                    echo c_date($data_sel[LIST_DATA][0]->member_received_date);
                                                }
                                                ?>  </b>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <!--<div class="well sub-box">-->
                                    <div class="panel panel-default" style="height: 250px;">
                                        <div class="panel-heading">Admin</div>
                                        <div class="panel-body">
                                            <div class="" style="font-size: .9em;">Tgl. Terima</div>
                                            <b><?php echo c_date(isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->admin_received_date : "") ?></b>
                                            <div class="" style="font-size: .9em;">Tgl. Kirim ke Merchant</div>
                                            <b><?php echo c_date(isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->ship_to_merchant_date : "") ?></b>
                                            <div class="" style="font-size: .9em;">Expedisi</div>
                                            <b><?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->exp_to_merchant : "") ?></b>
                                            <div class="" style="font-size: .9em;">No. Resi</div>
                                            <b><?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->awb_admin_no : "") ?></b>
                                            <div class="" style="font-size: .9em;">Pesan Admin</div>
                                            <b><?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->review_admin : "") ?></b>
                                        </div>
                                    </div>
                                    <!--</div>-->
                                </div>
                                <div class="col-xs-4">
                                    <div class="panel panel-default" style="height: 250px;">
                                        <div class="panel-heading">Merchant</div>
                                        <div class="panel-body">
                                            <div class="" style="font-size: .9em;">Tgl. Terima </div>
                                            <b><?php
                                                if ($data_sel[LIST_DATA][0]->merchant_received_date == DEFAULT_DATETIME) {
                                                    echo " ";
                                                } else {
                                                    echo c_date($data_sel[LIST_DATA][0]->merchant_received_date);
                                                }
                                                ?>
                                            </b>
                                            <div class="" style="font-size: .9em;">Ekspedisi</div>
                                            <b><?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->exp_to_admin : "") ?></b>
                                            <div class="" style="font-size: .9em;">No. Resi</div>
                                            <b><?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->awb_member_no : "") ?></b>
                                            <div class="" style="font-size: .9em;">Pesan Member</div>
                                            <b><?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->review_member : "") ?></b>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                            </div>
                        </div>
                        <div class="clearfix"></div>    


                    </div> 

                </div> 
            </div>
        </div>
    </form>
    <?php
} else {
    $codeActive = $this->input->get('status');
    $totals = 0;
    ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_css_url() ?>daterangepicker-bs3.css" />
    <div class="box-body">
        <div class="header">
            <div class="row no-margin">
                <div class="box-body sub-header" id="sub-navbar">
                    <form id="frmAuth" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL]; ?>" ><input type="hidden" id="key" name="key" /></form>
                    <form id ="frmSearch1" url= "<?php echo get_base_url() . $data_auth[FORM_URL]; ?>" class="form-inline">
                        <div class="row">

                            <div class="col-xs-12">
                                <div class="form-group">
                                    <input class="form-control-cst" placeholder="Nomor Pengembalian" name="no_return" type="input" value="<?php if (isset($_GET["no_return"])) echo $_GET["no_return"]; ?>">
                                    <input id="order_date_s" readonly date_type="date" class="form-control-cst" name="return_date_from" type="text" placeholder="Tanggal awal pengembalian" value="<?php if (isset($_GET["return_date_from"])) echo rawurldecode($_GET["return_date_from"]); ?>">
                                    <input id="order_date_e" readonly date_type="date" class="form-control-cst" name="return_date_to" type="text" placeholder="Tanggal akhir pengembalian" value="<?php if (isset($_GET["return_date_to"])) echo rawurldecode($_GET["return_date_to"]); ?>">

                                    <?php require_once get_component_url() . "dropdown/com_drop_member_email_return.php" ?>

                                    <button class="btn btn-danger" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-9">
                                <ul class="nav nav-pills no-padding">
                                    <li class="blue-link"><a class="btn btn-flat <?php echo (!isset($codeActive) || $codeActive === '' ? 'btn-active' : '' ) ?>" href="<?php echo base_url($data_auth[FORM_URL]) . '?status=' . $filter_sort . $filter ?>">Semua&nbsp;(<span id="status_all"></span>)</a></li>
                                    <?php
                                    $qty_status = 0;
                                    foreach (json_decode(STATUS_SHIPMENT_BY_MERCHANT) as $key => $stat) {
                                        if (isset($data_sel[LIST_DATA][2])) {
                                            $qty_status = get_qty_status($data_sel[LIST_DATA][2], $key);
                                            $totals = ($totals + $qty_status);
                                        } else {
                                            $qty_status = 0;
                                        }
                                        ?>
                                        <li class="blue-link"><a class="btn btn-flat <?php echo ($key === $codeActive ? 'btn-active' : '' ); ?>" href="<?php echo base_url($data_auth[FORM_URL]) . '?status=' . $key . $filter_sort . $filter ?>" <?php echo ($codeActive == $key ? 'class="btn-active"' : '' ) ?> ><?php echo $stat; ?> <span>(<?php echo $qty_status ?>)</span></a></li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                            <div class="col-xs-3">
                                <div class="row">
                                    <div class="col-xs-6"></div>
                                    <?php echo get_order_filter("created_date", "return_no", base_url($data_auth[FORM_URL]) . $filter_status . $filter, $this->input->get("sort"), $this->input->get("column_sort")) ?>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" id="start1" name="start" value="<?php
                        if (isset($_GET["start"])) {
                            echo $_GET["start"];
                        } else {
                            echo '1';
                        }
                        ?>">
                        </div>
                        </div>



                        <div class="row no-margin">
                            <div class="m-top10" id="tabledata1">
                                <?php
                                if (isset($data_sel[LIST_DATA][1])) {
                                    foreach ($data_sel[LIST_DATA][1] as $orders) {
                                        ?>
                                        <div class="box no-border">
                                            <div class="box-body">
                                                <div class="row">
                                                    <div class="col-xs-3">No. Pengembalian : <strong><?php echo $orders->return_no ?></strong></div>
                                                    <div class="col-xs-3">Tgl. Pengajuan : <?php echo c_date($orders->created_date) ?></div>
                                                    <div class="col-xs-3">Status Pengembalian : <?php echo cstdes($orders->return_status, STATUS_RETURN); ?></div>
                                                    <div class="col-xs-3">Status Pengiriman : <?php echo cstdes($orders->shipment_status, STATUS_SHIPMENT_BY_MERCHANT); ?></div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-3">No. Order : <?php echo $orders->order_no ?></div>
                                                    <div class="col-xs-4">Nama Produk : <?php echo $orders->product_name ?></div>
                                                    <div class="col-xs-2">Qty : <?php echo $orders->qty ?></div>
                                                    <div class = "col-xs-3">
                                                        <?php
                                                        if ($orders->return_status == NEW_STATUS_CODE AND $orders->shipment_status == SHIP_TO_MERCHANT) {
                                                            $action = "<a class='btn btn-xs btn-success btn-flat btn-flat' href='" . base_url() . "merchant/transaction/return_merchant/received/" . $orders->return_no . "?" . CONTROL_GET_TYPE . "=" . CONTROL_EDIT_NAME . "'>Terima</a>";
                                                        } elseif ($orders->return_status == NEW_STATUS_CODE AND $orders->shipment_status == RECEIVED_BY_MERCHANT) {
                                                            $action = "<a  class='btn btn-xs btn-flat btn-primary btn-flat' href='" . base_url() . "merchant/transaction/return_merchant/change_status/$orders->return_no'><center>Ubah Status</center></a>";
                                                        } elseif (($orders->return_status == RETURN_ITEM_BY_MERCHANT OR $orders->return_status == REJECT_STATUS_CODE) AND $orders->shipment_status == RECEIVED_BY_MERCHANT) {
                                                            $action = "<a  class='btn btn-xs btn-flat btn-warning btn-flat' href='" . base_url() . "merchant/transaction/return_merchant/ship_to_member/$orders->return_no'><center>Kirim ke Member</center></a>";
                                                        } elseif (($orders->return_status == RETURN_ITEM_BY_MERCHANT OR $orders->return_status == REJECT_STATUS_CODE) AND $orders->shipment_status == SHIP_TO_MEMBER) {
                                                            $action = "<a  class='btn btn-xs btn-flat btn-warning btn-flat btn-border-stskirim' href='" . base_url() . "merchant/transaction/return_merchant/ship_to_member/$orders->return_no'><center>Sedang dikirim ... </center></a>";
                                                        } elseif ($orders->return_status == REFUND_BY_MERCHANT AND $orders->shipment_status == RECEIVED_BY_MERCHANT) {
                                                            $action = "";
                                                        } elseif (($orders->return_status == RETURN_ITEM_BY_MERCHANT OR $orders->return_status == REJECT_STATUS_CODE) AND $orders->shipment_status == RECEIVED_BY_MEMBER) {
                                                            $action = "";
                                                        } else {
                                                            $action = "";
                                                        }
                                                        echo $action;
                                                        echo " <a href='?" . CONTROL_GET_TYPE . "=" . ACTION_EDIT . "&key=" . $orders->return_seq . "' class='btn btn-xs btn-flat btn-border-edit' title='Lihat'><i class = 'fa fa-eye'></i> Lihat</a>";
                                                        ?>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <div class="col-xs-12 no-padding">
                                        <div class="box box-no-border-top">
                                            <div class="box-body min-height300">
                                                <center class="no-found-src" >Tidak ada data pengembalian yang di tampilkan. </center>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="col-xs-12 no-pad">
                                    <div class="box-pagging">
                                        <center>
                                            <?php echo $this->pagination->create_links(); ?>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('#order_date_s').val('<?php echo isset($_GET['return_date_from']) ? $_GET['return_date_from'] : '' ?>');
                    $('#order_date_e').val('<?php echo isset($_GET['return_date_to']) ? $_GET['return_date_to'] : '' ?>');
                });
                $('input[date_type="date"]').daterangepicker({
                    "locale": {
                        "format": "DD-MMM-YYYY"
                    },
                    singleDatePicker: true,
                    showDropdowns: true
                });
                $("#status_all").html("<?php echo $totals; ?>");
            </script>
            <?php
        }
        ;
        require_once get_include_page_list_merchant_content_footer();
        require_once get_include_content_merchant_bottom_page_navigation();
        ?>
