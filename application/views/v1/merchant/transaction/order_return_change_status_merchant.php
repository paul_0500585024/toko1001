<?php
require_once VIEW_BASE_MERCHANT;
require_once get_include_content_merchant_top_page_navigation();
echo $breadcrumb;
?>
<link rel="stylesheet" type="text/css" href="<?php echo get_css_url() ?>daterangepicker-bs3.css" />
<div class="box box-default">
    <div class="box-body">
        <form id="frmSearch" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
            <div class="row">
                <section class="col-md-12 col-lg-12">
                    <?php echo get_csrf_merchant_token(); ?>
                    <input type="hidden" name="return_no" class="form-control" value="<?php echo $data_sel[LIST_DATA][0]->return_no; ?>">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>No. Return</label>
                            <input type="text" class="form-control" value="<?php echo $data_sel[LIST_DATA][0]->return_no; ?>" readonly>
                        </div>
                        <div class="form-group">
                            <label>Tgl. Pengajuan</label>
                            <input type="text" class="form-control" value="<?php echo c_date($data_sel[LIST_DATA][0]->created_date); ?>" readonly>
                        </div>

                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label>Status Pengembalian</label>
                            <select class="form-control select2" name="return_status">
                                <?php
                                $arraystatus = (json_decode(STATUS_RETURN_BY_MERCHANT, true));
                                unset($arraystatus['R']);
                                echo combostatus($arraystatus);
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Komentar</label>
                            <textarea class="form-control" name="merchant_comment" style="height: 125px;  max-height: 125px; resize: none;"><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->review_merchant : ""); ?></textarea>
                        </div>
                    </div>
                </section>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-md-6" >
                            <?php
                            if ($data_sel[LIST_DATA][0]->shipment_status == RECEIVED_BY_MERCHANT) {
                                echo get_save_edit_button();
                            }
                            ?>
                        </div>
                        <div class="col-md-6">
                            <a href="<?php echo get_base_url(); ?>merchant/transaction/return_merchant/" class="btn btn-google-plus" style="width: 100%;">&nbsp;Kembali</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>
<script  type="text/javascript">
            $('input[name="tgl_kirim"]').daterangepicker({
                format: 'DD-MMM-YYYY',
                singleDatePicker: true,
                showDropdowns: true,
                startDate: "<?php echo date('d-M-Y') ?>"
            });

            $("#save_edit").click(function() {
                var action = $('.select2 :selected').text();
                return confirm('Anda yakin untuk mengubah status menjadi "' + action + '" ?');
            })
</script>
<?php
require_once get_include_page_list_merchant_content_footer();
require_once get_include_content_merchant_bottom_page_navigation();
?>