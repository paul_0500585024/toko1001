<?php
require_once VIEW_BASE_MERCHANT;
require_once get_include_content_merchant_top_page_navigation();
echo $breadcrumb;
?>
<link rel="stylesheet" type="text/css" href="<?php echo get_css_url() ?>daterangepicker-bs3.css" />
<div class="p-10px">
    <div class="box no-border">
        <div class="box-body">
            <section class="col-xs-12">
                <form onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>" name="frmMain" id="frmMain">
                    <div class="row">
                        <?php echo get_csrf_merchant_token(); ?>
                        <input type="hidden" name="return_no" class="form-control" value="<?php echo $data_head[LIST_DATA][0]->return_no; ?>">
                        <div class="col-xs-5">
                            <div class="form-group">
                                <label>No. Pengembalian</label>
                                <input type="text" class="form-control" value="<?php echo $data_head[LIST_DATA][0]->return_no; ?>" readonly>
                            </div>
                            <div class="form-group">
                                <label>Tgl. Pengajuan</label>
                                <input type="text" class="form-control" value="<?php echo c_date($data_head[LIST_DATA][0]->created_date); ?>" readonly>
                            </div>
                            <div class="form-group">
                                <label>Tgl. Terima</label>
                                <input type="text" class="form-control" value="<?php echo c_date($data_head[LIST_DATA][0]->merchant_received_date); ?>" readonly>
                            </div>
                            <div class="form-group">
                                <label>Status Pengembalian</label>
                                <select class="form-control select2" name="return_status" disabled>
                                    <?php
                                    echo combostatus(json_decode(STATUS_RETURN_BY_MERCHANT), $data_head[LIST_DATA][0]->return_status);
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-7">
                            <div class="form-group">
                                <label>Tgl. Kirim</label>
                                <input validate ="required[]" class="form-control" name="tgl_kirim"  type="input" value="<?php
                                if (isset($data_sel[LIST_DATA][0])) {
                                    echo c_date($data_sel[LIST_DATA][0]->tgl_kirim);
                                } else {
                                    if (!empty($data_head[LIST_DATA][0]->ship_to_member_date)) {
                                        if ($data_head[LIST_DATA][0]->ship_to_member_date == "0000-00-00 00:00:00") {
                                            echo c_date(date('Y-m-d'));
                                        } else {
                                            echo c_date($data_head[LIST_DATA][0]->ship_to_member_date);
                                        }
                                    } else {
                                        echo c_date(date('Y-m-d'));
                                    }
                                }
                                ?>">
                            </div>
                            <div class="form-group">
                                <label>Ekspedisi</label>
                                <?php require_once get_component_url() . "dropdown/com_drop_expedition_return.php" ?>
                            </div>
                            <div class="form-group">
                                <label>No. Resi</label>
                                <input type="text" class="form-control" name="no_resi" validate ="required[]" value="<?php
                                if (isset($data_sel[LIST_DATA][0])) {
                                    echo $data_sel[LIST_DATA][0]->no_awb_merchant;
                                } elseif (isset($data_head[LIST_DATA][0])) {
                                    echo $data_head[LIST_DATA][0]->awb_merchant_no;
                                };
                                ?>">
                            </div>
                            <div class="form-group">
                                <label>Komentar</label>
                                <textarea class="form-control"name="merchant_comment" readonly style="height: 150px;  max-height: 150px; resize: none;"><?php echo (isset($data_head[LIST_DATA]) ? $data_head[LIST_DATA][0]->review_merchant : ""); ?></textarea>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <hr>
                            <div class="form-group">
                                <!--<div class="col-xs-6">-->
                                    <?php
                                    if ($data_head[LIST_DATA][0]->shipment_status == RECEIVED_BY_MEMBER) {
                                        
                                    } else {
//                                        echo get_save_edit_button();
                                        ?>
                                        <button type='submit' name='btnSaveEdit' class='btn btn-success btn-merchant-width pull-right'>Simpan</button>
                                        <?php
                                    }
                                    ?>

                                <!--</div>-->
                            </div>
                        </div>
                    </div>
                </form>
            </section>
        </div>
    </div>
</div>
<script  type="text/javascript">
    $('input[name="tgl_kirim"]').daterangepicker({
        format: 'DD-MMM-YYYY',
        singleDatePicker: true,
        showDropdowns: true,
        startDate: "<?php echo date('d-M-Y') ?>"
    });
</script>
<?php
require_once get_include_page_list_merchant_content_footer();
require_once get_include_content_merchant_bottom_page_navigation();
?>