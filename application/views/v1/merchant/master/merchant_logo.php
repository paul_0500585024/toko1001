<?php

require_once VIEW_BASE_MERCHANT;
require_once get_include_content_merchant_top_page_navigation();
?>

<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
    <script type="text/javascript" src="<?php echo get_js_url(); ?>ckeditor/ckeditor.js"></script>
    <?php require_once get_component_url() . '/dropdown/function_drop_down.php'; ?>
    <form class ="form-horizontal" id="frmMain" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>" enctype ="multipart/form-data">

        <div class="box box-default no-radius add-box-height">
            <div class="box-body no-pad">
                <div class="row">
                    <div class="col-xs-12">
                        <?php echo get_csrf_merchant_token(); ?>
                        <input type="hidden"  name="old_logo" value="<?php echo (isset($data_sel[LIST_DATA][0]) || $data_sel[LIST_DATA][0]->old_logo_img == "" ? $data_sel[LIST_DATA][0]->logo_img : $data_sel[LIST_DATA][0]->old_logo_img); ?>">
                        <input type="hidden"  name="old_banner" value="<?php echo (isset($data_sel[LIST_DATA][0]) || $data_sel[LIST_DATA][0]->old_banner_img == "" ? $data_sel[LIST_DATA][0]->banner_img : $data_sel[LIST_DATA][0]->old_banner_img); ?>">
                        <div class="baner_merchant">
                            <input id="banner_efct" style="display: none;" type="file" accept="<?php echo IMAGE_TYPE_UPLOAD; ?>" name="banner_img" class="btn btn-default" onchange ="previewimg(this, 'iprev1')" value="<?php echo isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->banner_img : ""; ?>" <?php echo ($data_auth[FORM_ACTION] == ACTION_ADD ? "validate=\"required[]\"" : ""); ?>/>
                            <img id="iprev1" class="merchant_full_img" src="<?php echo (!isset($data_sel[LIST_DATA])) || $data_sel[LIST_DATA][0]->banner_img == '' ? get_base_url() . IMG_BLANK_100 : get_base_url() . MERCHANT_LOGO . $data_sel[LIST_DATA][0]->seq . "/" . $data_sel[LIST_DATA][0]->banner_img; ?>"/>
                            <!--button untuk upload image baru-->
                            <!--                            <div class="set_banner">
                                                            <button id="iprev1Btn" class="btn btn-success btn-hidden">Simpan Perubahan</button>
                                                        </div>-->
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <div class="merchant_img_profile">
                            <input id="profil_efct" style="display: none;" type="file" accept="<?php echo IMAGE_TYPE_UPLOAD; ?>" name="logo_img" class="btn btn-default" onchange ="previewimg(this, 'iprev2')" value="<?php echo isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->logo_img : ""; ?>" <?php echo ($data_auth[FORM_ACTION] == ACTION_ADD ? "validate=\"required[]\"" : ""); ?>/>
                            <img id="iprev2" class="img_upload_profile_img" src="<?php echo (!isset($data_sel[LIST_DATA])) || $data_sel[LIST_DATA][0]->logo_img == '' ? get_base_url() . IMG_BLANK_100 : get_base_url() . MERCHANT_LOGO . $data_sel[LIST_DATA][0]->seq . "/" . $data_sel[LIST_DATA][0]->logo_img; ?>"/>
                            <!--                            <div class="set_banner_profil">
                                                            <button id="iprev2Btn" class="btn btn-success btn-mini btn-default-position btn-hidden">Simpan Perubahan</button>
                                                        </div>-->
                        </div>
                    </div>
                    <div class="col-xs-9">
                        <div class="merchant_info">
                            <label>Store Description</label>
                            <textarea class="merchantTxt" id="editor1" name="description"><?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->description : "" ); ?></textarea>
                        </div>
                        <button id="rsAll" type="button" name="rstMerchant" class="saveAllMerchant pull-right btn-save btn-danger btn-hidden">Batal</button>
                        <button id="svAll" type="submit" name="<?php echo CONTROL_SAVE_EDIT_NAME; ?>" class="saveAllMerchant pull-right btn-save btn-success btn-hidden">simpan perubahan</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-default">
            <div class="box box-body">
                <div class="row">

                    <!--####################################################################################-->
                    <div>
                        <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                            <input class="form-control" name="old_status" type="hidden" value="<?php echo(isset($data_head[LIST_DATA][0]) ? $data_head[LIST_DATA][0]->status : "") ?>">
                            <input class="form-control" name="old_created_date" type="hidden" value="<?php echo(isset($data_head[LIST_DATA][0]) ? $data_head[LIST_DATA][0]->created_date : "") ?>">
                            <input class="form-control" name="old_modified_date" type="hidden" value="<?php echo(isset($data_head[LIST_DATA][0]) ? $data_head[LIST_DATA][0]->modified_date : "") ?>">
                        <?php } ?>

                        <div class="col-xs-3">
                            <!-- Nav tabs -->
                            <div class="nav-tabs-custom no-shadow" style="padding:0px;">
                                <ul class="nav" role="tablist">
                                    <li role="presentation" class="active"><a href="#t_5" aria-controls="t_5" role="tab" data-toggle="tab">Info Pengajuan</a></li>
                                    <li role="presentation"><a href="#t_1" class="no-msg" aria-controls="t_1" role="tab" data-toggle="tab">Info Alamat</a></li>
                                    <li role="presentation"><a href="#t_2" class="no-msg" aria-controls="t_2" role="tab" data-toggle="tab">Info Ekspedisi</a></li>
                                    <li role="presentation"><a href="#t_3" class="no-msg" aria-controls="t_3" role="tab" data-toggle="tab">Info Retur</a></li>
                                    <li role="presentation"><a href="#t_4" class="no-msg" aria-controls="t_4" role="tab" data-toggle="tab">Info Bank</a></li>
                                    <li role="presentation"><a href="#t_6" id="msg" aria-controls="t_6" role="tab" data-toggle="tab">Pesan</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- Tab panes -->
                        <div class="col-xs-9">
                            <div class="merchantTabs no-pad" style="border: 1px solid #ededed;margin-bottom: 1em;">
                                <div class="tab-content" style="margin:1em;">
                                    <div role="tabpanel" class="tab-pane  active" id="t_5"> <br>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class ="form-group">
                                                    <label class ="control-label col-xs-3">Status</label>
                                                    <div class ="col-xs-9">
                                                        <input class="form-control" name="s_status" type="text" value="<?php echo status($data_head[LIST_DATA][0]->status, (STATUS_ANRL)); ?>" readonly >
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class ="form-group">
                                                    <label class ="control-label col-xs-3">Tanggal Pengajuan</label>
                                                    <div class ="col-xs-9">
                                                        <input class="form-control" name="s_tgl" type="text" value="<?php echo c_date($data_head[LIST_DATA][0]->created_date, 1) ?>" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class ="form-group">
                                                    <label class ="control-label col-xs-3">Tanggal Verifikasi</label>
                                                    <div class ="col-xs-9">
                                                        <input class="form-control" name="s_tgl_k" value="<?php echo c_date($data_head[LIST_DATA][0]->modified_date, 1) ?>" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="t_1"><br />
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class ="form-group">
                                                    <label class ="control-label col-xs-3">Alamat *</label>
                                                    <div class ="col-xs-9">
                                                        <textarea onblur="cekpr('1')" class="form-control" validate ="required[]" id="address" name="address" style="overflow:auto;resize:none" rows="7"><?php echo (isset($data_head[LIST_DATA]) ? get_display_value($data_head[LIST_DATA][0]->address) : "") ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label class ="control-label col-xs-3">Propinsi *</label>
                                                    <div class ="col-xs-9">
                                                        <?php echo drop_down('propinsi', $province_name, '', 'validate ="required[]" onchange="cData(this.value, \'city\', \'city\');"'); ?>
                                                    </div>
                                                </div>
                                                <div class ="form-group">
                                                    <label class ="control-label col-xs-3">Kota *</label>
                                                    <div class ="col-xs-9">
                                                        <?php echo drop_down('city', '', '', 'validate ="required[]" onchange="cData(this.value, \'district_seq\', \'district\');"'); ?>
                                                    </div>
                                                </div>
                                                <div class ="form-group">
                                                    <label class ="control-label col-xs-3">Kecamatan *</label>
                                                    <div class ="col-xs-9">
                                                        <?php echo drop_down('district_seq', '', '', 'validate ="required[]"  onchange="cekpr(\'2\')"'); ?>
                                                    </div>
                                                </div>
                                                <div class ="form-group">
                                                    <label class ="control-label col-xs-3">Kode Pos</label>
                                                    <div class ="col-xs-9">
                                                        <input onblur="cekpr('3')" class="form-control" maxlength="10" id="zip_code" name="zip_code" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_head[LIST_DATA][0]->zip_code) : "") ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class ="form-group">
                                                    <label class ="control-label col-xs-3">No. Telp *</label>
                                                    <div class ="col-xs-9">
                                                        <input class="form-control" data-inputmask="&quot;mask&quot;: &quot;9999 9999 9999&quot;" data-mask="" validate ="required[]" id="phone_no" name="phone_no" type="text" value ="<?php echo (isset($data_head[LIST_DATA]) ? get_display_value($data_head[LIST_DATA][0]->phone_no) : "") ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class ="form-group">
                                                    <label class ="control-label col-xs-3">No. Fax</label>
                                                    <div class ="col-xs-9">
                                                        <input class="form-control" maxlength="50" id="fax_no" name="fax_no" type="text" value ="<?php echo (isset($data_head[LIST_DATA]) ? get_display_value($data_head[LIST_DATA][0]->fax_no) : "") ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class ="form-group">
                                                    <label class ="control-label col-xs-3">Nama PIC *</label>
                                                    <div class ="col-xs-9">
                                                        <input class="form-control" maxlength="50" validate ="required[]" id="pic1_name" name="pic1_name" type="text" value ="<?php echo (isset($data_head[LIST_DATA]) ? get_display_value($data_head[LIST_DATA][0]->pic1_name) : "") ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class ="form-group">
                                                    <label class ="control-label col-xs-3">Telp PIC</label>
                                                    <div class ="col-xs-9">
                                                        <input class="form-control" maxlength="50" id="pic1_phone_no" name="pic1_phone_no" type="text" value ="<?php echo (isset($data_head[LIST_DATA]) ? $data_head[LIST_DATA][0]->pic1_phone_no : "") ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class ="form-group">
                                                    <label class ="control-label col-xs-3">Nama Finance</label>
                                                    <div class ="col-xs-9">
                                                        <input class="form-control" maxlength="50" id="pic2_name" name="pic2_name" type="text" value ="<?php echo (isset($data_head[LIST_DATA]) ? get_display_value($data_head[LIST_DATA][0]->pic2_name) : "") ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">

                                                <div class ="form-group">
                                                    <label class ="control-label col-xs-3">Telp Finance</label>
                                                    <div class ="col-xs-9">
                                                        <input class="form-control" maxlength="50" id="pic2_phone_no" name="pic2_phone_no" type="text" value ="<?php echo (isset($data_head[LIST_DATA]) ? get_display_value($data_head[LIST_DATA][0]->pic2_phone_no) : "") ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- end t1 -->
                                    <div role="tabpanel" class="tab-pane" id="t_2"><br />
                                        <div class="row">
                                            <div class="col-xs-8">
                                                <div class ="form-group">
                                                    <label class ="control-label col-xs-3">Alamat Pengambilan *</label>
                                                    <div class ="col-xs-9">
                                                        <textarea class="form-control" style="overflow:auto;resize:none" rows="3" id="pickup_addr" name="pickup_addr"><?php echo (isset($data_head[LIST_DATA]) ? get_display_value($data_head[LIST_DATA][0]->pickup_addr) : "") ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <label> <input type="checkbox" id ="pickup_addr_eq" name ="pickup_addr_eq" <?php echo (isset($data_head[LIST_DATA]) ? (($data_head[LIST_DATA][0]->pickup_addr_eq == "1" OR $data_head[LIST_DATA][0]->pickup_addr_eq == "on") ? "checked" : "") : "checked") ?> onclick="aSama(this.checked, 'pickup')" /> Sama dengan alamat</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8">
                                                <div class="form-group">
                                                    <label class ="control-label col-xs-3">Propinsi</label>
                                                    <div class ="col-xs-9">
                                                        <?php echo drop_down('pickup_propinsi', $province_name, '', 'onchange="cData(this.value, \'city\', \'city\',\'pickup_\');"'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8">
                                                <div class ="form-group">
                                                    <label class ="control-label col-xs-3">Kota</label>
                                                    <div class ="col-xs-9">

                                                        <?php echo drop_down('pickup_city', '', '', 'onchange="cData(this.value, \'district_seq\', \'district\',\'pickup_\');"'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8">
                                                <div class ="form-group">
                                                    <label class ="control-label col-xs-3">Kecamatan *</label>
                                                    <div class ="col-xs-9">
                                                        <?php echo drop_down('pickup_district_seq'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8">
                                                <div class ="form-group">
                                                    <label class ="control-label col-xs-3">Kode Pos Pengambilan</label>
                                                    <div class ="col-xs-9">
                                                        <input maxlength="10" class="form-control" id="pickup_zip_code" name="pickup_zip_code" type="text" value ="<?php echo (isset($data_head[LIST_DATA]) ? get_display_value($data_head[LIST_DATA][0]->pickup_zip_code) : "") ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- end t2 -->
                                    <div role="tabpanel" class="tab-pane" id="t_3"><br />
                                        <div class="row">
                                            <div class="col-xs-8">
                                                <div class ="form-group">
                                                    <label class ="control-label col-xs-3">Alamat Pengembalian *</label>
                                                    <div class ="col-xs-9">
                                                        <textarea class="form-control" style="overflow:auto;resize:none" rows="3" id="return_addr" name="return_addr"><?php echo (isset($data_head[LIST_DATA]) ? get_display_value($data_head[LIST_DATA][0]->return_addr) : "") ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <label> <input type="checkbox" id ="return_addr_eq" name ="return_addr_eq" <?php echo (isset($data_head[LIST_DATA]) ? (($data_head[LIST_DATA][0]->return_addr_eq == "1" OR $data_head[LIST_DATA][0]->return_addr_eq == "on") ? "checked" : "") : "checked") ?>  onclick="aSama(this.checked, 'return')"  /> Sama dengan alamat</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8">
                                                <div class="form-group">
                                                    <label class ="control-label col-xs-3">Propinsi</label>
                                                    <div class ="col-xs-9">
                                                        <?php echo drop_down('return_propinsi', $province_name, '', 'onchange="cData(this.value, \'city\', \'city\',\'return_\');"'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8">
                                                <div class ="form-group">
                                                    <label class ="control-label col-xs-3">Kota</label>
                                                    <div class ="col-xs-9">
                                                        <?php echo drop_down('return_city', '', '', 'onchange="cData(this.value, \'district_seq\', \'district\',\'return_\');"'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8">
                                                <div class ="form-group">
                                                    <label class ="control-label col-xs-3">Kecamatan *</label>
                                                    <div class ="col-xs-9">
                                                        <?php echo drop_down('return_district_seq'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8">
                                                <div class ="form-group">
                                                    <label class ="control-label col-xs-3">Kode Pos Pengembalian</label>
                                                    <div class ="col-xs-9">
                                                        <input class="form-control" maxlength="10" id="return_zip_code" name="return_zip_code" type="text" value ="<?php echo (isset($data_head[LIST_DATA]) ? get_display_value($data_head[LIST_DATA][0]->return_zip_code) : "") ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- end t3 -->
                                    <div role="tabpanel" class="tab-pane" id="t_4"><br />
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <label class ="control-label col-xs-3">Nama Bank *</label>
                                                    <div class="col-xs-9">
                                                        <input class="form-control" validate ="required[]" name="bank_name" type="text" value ="<?php echo (isset($data_head[LIST_DATA][0]) ? $data_head[LIST_DATA][0]->bank_name : "") ?>"maxlength="25">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class ="col-xs-3">Cabang *</label>
                                                    <div class="col-xs-9">
                                                        <input class="form-control" validate ="required[]" name="bank_branch_name" type="text" value ="<?php echo (isset($data_head[LIST_DATA][0]) ? $data_head[LIST_DATA][0]->bank_branch_name : "") ?>" maxlength="50">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class ="col-xs-3">No.Rekening *</label>
                                                    <div class="col-xs-9">
                                                        <input class="form-control" validate ="required[]" name="bank_acct_no" type="text"  value ="<?php echo (isset($data_head[LIST_DATA][0]) ? $data_head[LIST_DATA][0]->bank_acct_no : "") ?>"  maxlength="50">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class ="col-xs-3">Rekening A/N *</label>
                                                    <div class="col-xs-9">
                                                        <input class="form-control" validate ="required[]" name="bank_acct_name" type="text" value ="<?php echo (isset($data_head[LIST_DATA][0]) ? $data_head[LIST_DATA][0]->bank_acct_name : "") ?>"  maxlength="50">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--INBOX-->
                                    <div role="tabpanel" class="tab-pane" id="t_6">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="box-header with-border">
                                                    <b>Info Pesan</b>
                                                </div>
                                                <div class="box-body">
                                                    <div class="form-group pull-right">
                                                        <button id="btnaddinbox" class="btn btn-toko1001" onclick="newmsg(0)" type="button"><i class="fa fa-plus"></i> Tulis Pesan Baru</button>
                                                    </div>

                                                    <div class="col-xs-12" id="tabledata11"></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div class ="col-xs-offset-9 col-xs-3"><?php echo get_save_edit_button(); ?> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <form class ="form-horizontal" id="frmSearch11" method="post" url="<?php echo get_base_url() . $data_auth[FORM_URL]; ?>">
        <input type="hidden" id="start11" name="start" value="1">
        <input type="hidden" id="length11" name="length" value="15">
        <input type="hidden" name="order" value="desc">
        <input type="hidden" name="column" value="created_date">
        <input type="hidden" name="btnSearch11" value="true">
        <input type="hidden" name="btnAdditional" value="act_s_adt">
        <input type="hidden" name="tipe" value="tblmsg">
    </form>
    <form class ="form-horizontal" id="frmInbox" name="frmInbox" method="post" url="<?php echo get_base_url() . $data_auth[FORM_URL]; ?>">
        <input type="hidden" id="tipe" name="tipe" value="msgsave">
        <input type="hidden" name="btnAdditional" value="act_s_adt">
        <input type="hidden" id="prev_message_seq" name="prev_message_seq" value="0">
        <!-- Modal -->
        <div class="modal fade" id="InboxModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Pesan Baru</h4>
                    </div>
                    <div class="modal-body">
                        <textarea cols="80" rows="10" id="message_text" name="message_text" style="resize: none;" class="form-control"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <button type="button" class="btn btn-primary" onclick="savemsg()">Kirim Pesan</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <?php
}
?>
<script type="text/javascript">

        $(document).ready(function() {
            $('#msg').click(function() {
                $('#save_edit').hide();
            });
        });
        $(document).ready(function() {
            $('.no-msg').click(function() {
                $('#save_edit').show();
            });
        });


        function newmsg(idh) {
            $('#message_text').val('');
            $('#prev_message_seq').val(idh);
            $('#InboxModal').modal('show');
        }

        function savemsg() {
            if ($('#message_text').val() == '') {
                alert('Pesan <?php echo ERROR_VALIDATION_MUST_FILL; ?>');
                return false;
            }
            $('#InboxModal').modal('hide');
            var urlmsgs = $("#frmInbox");
            $.ajax({
                url: urlmsgs.attr('url'), data: urlmsgs.serialize(), type: 'POST', datatype: 'json',
                success: function(data) {
                    listinbox(11);
                }
            });
        }

        function listinbox(idf) {
            var urls = $("#frmSearch" + idf);
            var htmls = '';
            var pagings = '';
            $.ajax({
                url: urls.attr('url'), data: urls.serialize(), type: 'POST', datatype: 'json',
                success: function(JSONString) {
                    var dataobj = $.parseJSON('[' + JSONString + ']');
                    $.each(dataobj, function() {
                        var realdata = this['aaData'];
                        htmls = '<ul class="list-group">';
                        icount = 0;
                        $.each(realdata, function() {
                            icount++;
                            if (icount % 2 == true) {
                                clasodd = " list-group-item-info";
                            } else {
                                clasodd = "";
                            }
                            if (this['status'] == 'U' && this['member_seq'] == "<?php echo $_SESSION[SESSION_MERCHANT_SEQ]; ?>") {
                                content = "<strong>" + this['content'] + "</strong>";
                                facoment = "fa-comment";
                            } else {
                                content = this['content'];
                                facoment = "fa-comment-o";
                            }
                            htmls += '<li id="list_' + this['headers'] + '" class="list-group-item' + clasodd + '">';
                            htmls += '<div class="row"><a href="javascript:detail(' + this['headers'] + ')"><div class="col-xs-3">' + '<i class="fa ' + facoment + '"></i> ' + this['user_name'] + '</div>' + '<div class="col-xs-7">' + content + '</div>' + '<div class="col-xs-2">' + this['created_date'] + '</div></a></div>';
                            htmls += '</li>';
                        });
                        var recorddata = this['iTotalRecords'];
                        htmls += '</ul>';
                        pagings = paging(recorddata, idf, "listinbox");
                    });
                    $('#tabledata' + idf).html(htmls + pagings);
                },
                error: function(request, error) {
                    alert("Error");
                }
            });
        }
        listinbox(11);
        function detail(idh) {
            $('#list_' + idh).html('');
            htmls = '';
            thelastreplay = '';
            $.ajax({
                url: "<?php echo get_base_url() . $data_auth[FORM_URL]; ?>", data: {'tipe': 'onemsg', 'btnAdditional': 'act_s_adt', 'seq': idh}, type: 'POST', datatype: 'json',
                success: function(JSONString) {
                    var dataobj = $.parseJSON('[' + JSONString + ']');
                    $.each(dataobj, function() {
                        var realdata = this['aaData'];
                        $.each(realdata, function() {
                            if (this['member_seq'] == "<?php echo $_SESSION[SESSION_MERCHANT_SEQ]; ?>") {
                                htmls += '<div class="direct-chat-msg">' + '<div class="direct-chat-info clearfix">' + '<span class="direct-chat-name pull-left">' + this['user_name'] + '</span>' + '<span class="direct-chat-timestamp pull-right">' + this['created_date'] + '</span>' + '</div>' + '<img class="direct-chat-img" src="' + '<?php echo (!isset($data_sel[LIST_DATA])) || $data_sel[LIST_DATA][0]->logo_img == '' ? get_base_url() . IMG_BLANK_100 : get_base_url() . MERCHANT_LOGO . $data_sel[LIST_DATA][0]->seq . "/" . $data_sel[LIST_DATA][0]->logo_img; ?>' + '">' + '<div class="direct-chat-text" style="color: rgb(255, 255, 255);background: rgb(60, 141, 188);border-color: rgb(60, 141, 188);">' + this['content'] + '</div>' + '</div>';
                            } else {
                                htmls += '<div class="direct-chat-msg right">' + '<div class="direct-chat-info clearfix">' + '<span class="direct-chat-name pull-right">' + this['user_name'] + '</span>' + '<span class="direct-chat-timestamp pull-left">' + this['created_date'] + '</span>' + '</div>' + '<img class="direct-chat-img" src="' + '<?php echo get_image_location() . "assets/img/admin_default_img.jpg" ?>' + '">' + '<div class="direct-chat-text">' + this['content'] + '</div>' + '</div>';
                            }
                            thelastreplay = this['member_seq'];
                        });
                    });
                    if (thelastreplay != "<?php echo $_SESSION[SESSION_MERCHANT_SEQ]; ?>") {
                        htmls += '<br /><button type="button" onclick="newmsg(' + idh + ')" class="pull-right"><i class=" fa fa-reply"> Balas Pesan</i></button><br />';
                    }
                    $('#list_' + idh).html(htmls);
                }
            });
        }

        function previewimg(thisval, idprev) {
            if (thisval.files && thisval.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#' + idprev + '').attr('src', e.target.result);
                    $('#svAll').removeClass('btn-hidden');
                    $('#rsAll').removeClass('btn-hidden');
                };
                reader.readAsDataURL(thisval.files[0]);
            }
        }

        $('#editor1').on('change keyup keypres focus blur', function() {
            $('#svAll').removeClass('btn-hidden');
            $('#rsAll').removeClass('btn-hidden');
        });
        $('#rsAll').click(function() {
            $(this).addClass('btn-hidden');
            $('#svAll').addClass('btn-hidden');
            $('#iprev1').attr('src', '<?php echo (!isset($data_sel[LIST_DATA])) || $data_sel[LIST_DATA][0]->banner_img == '' ? get_base_url() . IMG_BLANK_100 : get_base_url() . MERCHANT_LOGO . $data_sel[LIST_DATA][0]->seq . "/" . $data_sel[LIST_DATA][0]->banner_img; ?>');
            $('#iprev2').attr('src', '<?php echo (!isset($data_sel[LIST_DATA])) || $data_sel[LIST_DATA][0]->logo_img == '' ? get_base_url() . IMG_BLANK_100 : get_base_url() . MERCHANT_LOGO . $data_sel[LIST_DATA][0]->seq . "/" . $data_sel[LIST_DATA][0]->logo_img; ?>');
        });
        $('#iprev1').click(function() {
            $('#banner_efct').click();
        });
        $('#iprev2').click(function() {
            $('#profil_efct').click();
        });</script>

<script type="text/javascript">
    $("[data-mask]").inputmask();
    function cekpr($iddata) {
        var eqadr_p = '';
        var eqadr_r = '';
        if ($("#pickup_addr_eq").is(':checked'))
            eqadr_p = '1';
        if ($("#return_addr_eq").is(':checked'))
            eqadr_r = '1';
        if ($iddata == '1' && eqadr_p == '1')
            $('#pickup_addr').val($('#address').val());
        if ($iddata == '1' && eqadr_r == '1')
            $('#return_addr').val($('#address').val());
        if ($iddata == '2' && eqadr_p == '1') {
            if ($('#district_seq').val() != '')
                cekdata($('#district_seq').val(), 'pickup_');
        }
        if ($iddata == '2' && eqadr_r == '1') {
            if ($('#district_seq').val() != '')
                setTimeout(function() {
                    cekdata($('#district_seq').val(), 'return_');
                }, 2000)
        }
        if ($iddata == '3' && eqadr_p == '1')
            $('#pickup_zip_code').val($('#zip_code').val());
        if ($iddata == '3' && eqadr_r == '1')
            $('#return_zip_code').val($('#zip_code').val());
    }

    function aSama(nilai, tab)
    {
        if (nilai == true || nilai == "1") {
            $("#" + tab + "_addr").prop('disabled', true);
            $("#" + tab + "_district_seq").prop('disabled', true);
            $("#" + tab + "_zip_code").prop('disabled', true);
            $("#" + tab + "_propinsi").prop('disabled', true);
            $("#" + tab + "_city").prop('disabled', true);
            $("#" + tab + "_district_seq").prop('required', false);
            $("#" + tab + "_addr").prop('required', false);
        } else {
            $("#" + tab + "_addr").prop('disabled', false);
            $("#" + tab + "_district_seq").prop('disabled', false);
            $("#" + tab + "_zip_code").prop('disabled', false);
            $("#" + tab + "_propinsi").prop('disabled', false);
            $("#" + tab + "_city").prop('disabled', false);
            $("#" + tab + "_district_seq").prop('required', true);
            $("#" + tab + "_addr").prop('required', true);
        }

    }
    var url = "<?php echo get_base_url() . $data_auth[FORM_URL]; ?>";
    function cData(idcombo, iddtl, jenis, grup, nilai) {
        grup = grup || "";
        nilai = nilai || "";
        var list = $("#" + grup + iddtl);
        if (jenis == 'city')
            $("#" + grup + "district_seq").empty();
        inHTML = "";
        list.empty();
        if (idcombo != '') {
            $.ajax({
                url: url,
                data: {
                    btnAdditional: "act_s_adt", idh: idcombo, tipe: jenis
                },
                type: "POST",
                success: function(response) {
                    if (isSessionExpired(response)) {
                        response_object = json_decode(response);
                        url = response_object.url;
                        location.href = url;
                    } else {
                        if (nilai != '') {
                            inHTML += '<option value="">-- Pilih --</option>';
                        } else {
                            inHTML += '<option value="" selected>-- Pilih --</option>';
                        }
                        $.each($.parseJSON(response), function() {
                            if (nilai != '') {
                                selected = "";
                                if (nilai == this.seq)
                                    selected = " selected";
                                inHTML += '<option value="' + this.seq + '"' + selected + '>' + this.name + '</option>';
                            } else {
                                inHTML += '<option value="' + this.seq + '">' + this.name + '</option>';
                            }
                        });
                        list.append(inHTML);
                        if (nilai == '')
                            list.select2("val", "");
                    }
                },
                error: function(request, error) {
                    alert(error_response(request.status));
                }
            });
        }
    }
<?php if ($data_auth[FORM_ACTION] == ACTION_ADD) { ?>
        function cEmail() {
            var idemail = $("#email").val();
            if (idemail == '') {
                return false;
            } else {
                $("#emailerror").html("");
            }
            $.ajax({
                url: url,
                data: {
                    btnAdditional: "act_s_adt", idh: idemail, tipe: "email"
                },
                type: "POST",
                success: function(data) {
                    if (isSessionExpired(data)) {
                        response_object = json_decode(data);
                        url = response_object.url;
                        location.href = url;
                    } else {
                        if (data.trim() != "OK") {
                            $("#emailerror").html("Email sudah terdaftar, harap memakai email yang lain !");
                            $("#email").focus();
                        } else {
                            $("#emailerror").html("");
                        }
                    }
                },
                error: function(request, error) {
                    alert(error_response(request.status));
                }
            });
        }
        function cekdata(dtdistrict, grup) {
            grup = grup || "";
            var idcity = grup + "city";
            var iddistrict = grup + "district_seq";
            if (dtdistrict != '') {
                $.ajax({
                    url: url,
                    data: {
                        btnAdditional: "act_s_adt", idh: dtdistrict, tipe: 'valpcd'
                    },
                    type: "POST",
                    success: function(response) {
                        if (isSessionExpired(response)) {
                            response_object = json_decode(response);
                            url = response_object.url;
                            location.href = url;
                        } else {
                            $.each($.parseJSON(response), function() {
                                idp = this.idp;
                                idc = this.idc;
                                idd = this.idd;
                            });
                            $("#" + grup + "propinsi").select2("val", idp);
                            setTimeout(function() {
                                filldata(idp, idcity, 'city', idc);
                            }, 500);
                            setTimeout(function() {
                                filldata(idc, iddistrict, 'district', idd);
                            }, 800);
                        }
                    },
                    error: function(request, error) {
                        alert(error_response(request.status));
                    }
                });
            }
        }

        function filldata(idcombo, iddtl, jenis, nilai) {
            nilai = nilai || "";
            var list = $("#" + iddtl);
            inHTML = "";
            list.empty();
            if (idcombo != '') {
                $.ajax({
                    url: url,
                    data: {
                        btnAdditional: "act_s_adt", idh: idcombo, tipe: jenis
                    },
                    type: "POST",
                    success: function(response) {
                        if (isSessionExpired(response)) {
                            response_object = json_decode(response);
                            url = response_object.url;
                            location.href = url;
                        } else {
                            inHTML += "<option value=''>-- Pilih --</option>";
                            $.each($.parseJSON(response), function() {
                                if (nilai != '') {
                                    selected = "";
                                    if (nilai == this.seq)
                                        selected = " selected";
                                    inHTML += '<option value="' + this.seq + '"' + selected + '>' + this.name + '</option>';
                                } else {
                                    inHTML += '<option value="' + this.seq + '">' + this.name + '</option>';
                                }
                            });
                            list.append(inHTML);
                            list.select2("val", nilai);
                        }
                    },
                    error: function(request, error) {
                        alert(error_response(request.status));
                    }
                });
            }
        }
    <?php if (isset($data_sel[LIST_DATA])) { ?>

            setTimeout(function() {
                aSama(<?php echo (($data_sel[LIST_DATA][0]->pickup_addr_eq == "1" || $data_sel[LIST_DATA][0]->pickup_addr_eq == "on") ? "'1'" : "''") ?>, 'pickup');
            }, 500);
            setTimeout(function() {
                aSama(<?php echo (($data_sel[LIST_DATA][0]->return_addr_eq == "1" || $data_sel[LIST_DATA][0]->return_addr_eq == "on") ? "'1'" : "''") ?>, 'return');
            }, 1000);
        <?php
        echo "cekdata('" . $data_sel[LIST_DATA][0]->district_seq . "');";

//if ($data_sel[LIST_DATA][0]->pickup_addr_eq == "1" || $data_sel[LIST_DATA][0]->pickup_addr_eq == "on") {
        echo "setTimeout(function(){cekdata('" . $data_head[LIST_DATA][0]->pickup_district_seq . "','pickup_');}, 2000);";
//}
//if ($data_sel[LIST_DATA][0]->return_addr_eq == "1" || $data_sel[LIST_DATA][0]->return_addr_eq == "on") {
        echo "setTimeout(function(){cekdata('" . $data_head[LIST_DATA][0]->return_district_seq . "','return_');}, 3000);";
//}
        echo "cekpr('1');cekpr('3');";
    } else {
        ?>

            $("#propinsi").val("");
            cData("", "city", "city");
            setTimeout(function() {
                aSama('1', 'pickup');
            }, 500);
            setTimeout(function() {
                aSama('1', 'return');
            }, 1000);
        <?php
    }
} else {
    ?>
        $("#email").prop('required', false);
        $("#email").prop('disabled', true);
        setTimeout(function() {
            aSama(<?php echo (($data_head[LIST_DATA][0]->pickup_addr_eq == "1" || $data_head[LIST_DATA][0]->pickup_addr_eq == "on") ? "'1'" : "''") ?>, 'pickup');
        }, 500);
        setTimeout(function() {
            aSama(<?php echo (($data_head[LIST_DATA][0]->return_addr_eq == "1" || $data_head[LIST_DATA][0]->return_addr_eq == "on") ? "'1'" : "''") ?>, 'return');
        }, 1000);
        function filldata(idcombo, iddtl, jenis, nilai) {
            nilai = nilai || "";
            var list = $("#" + iddtl);
            inHTML = "";
            list.empty();
            if (idcombo != '') {
                $.ajax({
                    url: url,
                    data: {
                        btnAdditional: "act_s_adt", idh: idcombo, tipe: jenis
                    },
                    type: "POST",
                    success: function(response) {
                        if (isSessionExpired(response)) {
                            response_object = json_decode(response);
                            url = response_object.url;
                            location.href = url;
                        } else {
                            inHTML += "<option value=''>-- Pilih --</option>";
                            $.each($.parseJSON(response), function() {
                                if (nilai != '') {
                                    selected = "";
                                    if (nilai == this.seq)
                                        selected = " selected";
                                    inHTML += '<option value="' + this.seq + '"' + selected + '>' + this.name + '</option>';
                                } else {
                                    inHTML += '<option value="' + this.seq + '">' + this.name + '</option>';
                                }
                            });
                            list.append(inHTML);
                            list.select2("val", nilai);
                        }
                    },
                    error: function(request, error) {
                        alert(error_response(request.status));
                    }
                });
            }
        }
        function cekdata(dtdistrict, grup) {
            grup = grup || "";
            var idcity = grup + "city";
            var iddistrict = grup + "district_seq";
            if (dtdistrict != '') {
                $.ajax({
                    url: url,
                    data: {
                        btnAdditional: "act_s_adt", idh: dtdistrict, tipe: 'valpcd'
                    }, type: "POST",
                    success: function(response) {
                        if (isSessionExpired(response)) {
                            response_object = json_decode(response);
                            url = response_object.url;
                            location.href = url;
                        } else {
                            $.each($.parseJSON(response), function() {
                                idp = this.idp;
                                idc = this.idc;
                                idd = this.idd;
                            });
                            $("#" + grup + "propinsi").select2("val", idp);
                            setTimeout(function() {
                                filldata(idp, idcity, 'city', idc);
                            }, 500);
                            setTimeout(function() {
                                filldata(idc, iddistrict, 'district', idd);
                            }, 800);
                        }
                        ;
                    },
                    error: function(request, error) {
                        alert(error_response(request.status));
                    }
                });
            }
        }
    <?php
    if (isset($data_sel[LIST_DATA])) {
        echo "cekdata('" . $data_head[LIST_DATA][0]->district_seq . "');";
//	    if ($data_sel[LIST_DATA][0]->pickup_addr_eq == "1" || $data_sel[LIST_DATA][0]->pickup_addr_eq == "on") {
        echo "setTimeout(function(){cekdata('" . $data_head[LIST_DATA][0]->pickup_district_seq . "','pickup_');}, 2000);";
//	    }
//	    if ($data_sel[LIST_DATA][0]->return_addr_eq == "1" || $data_sel[LIST_DATA][0]->return_addr_eq == "on") {
        echo "setTimeout(function(){cekdata('" . $data_head[LIST_DATA][0]->return_district_seq . "','return_');}, 3000);";
//	    }
        echo "cekpr('1');cekpr('3');";
    }
}
?>
</script>


<?php
require_once get_include_page_list_merchant_content_footer();
require_once get_include_content_merchant_bottom_page_navigation();
?>