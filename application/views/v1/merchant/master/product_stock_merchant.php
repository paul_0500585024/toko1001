<?php
require_once VIEW_BASE_MERCHANT;
require_once get_include_content_merchant_top_page_navigation();
?>

<?php if ($data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="box box-default">
        <div class="box-header with-border">
    	<h3 class="box-title"><?php echo $data_auth[FORM_ACTION_TITLE] . $data_auth[FORM_AUTH][FORM_TITLE] ?></h3>
        </div>
        <form class ="form-horizontal" id="frmMain" onsubmit ="return validate_form();" method ="post"
    	  action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
    	<div class="box-body">
    	    <section class="col-xs-12">
		    <?php echo get_csrf_merchant_token(); ?>
		    <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
			<div class ="form-group pull-left col-xs-12">
			    <label class ="control-label col-xs-1">Produk</label>
			    <div class ="col-xs-5">
				<input class="form-control" name ="product_variant_seq" type="hidden"
				       value ="<?php echo $data_sel[LIST_DATA][0][0]->product_variant_seq; ?>">
				<input class="form-control" name ="product_name" type="text"
				       value ="<?php echo $data_sel[LIST_DATA][0][0]->product_name; ?>" readonly>
			    </div>
			</div>
			<div class ="form-group pull-left col-xs-12">
			    <label class ="control-label col-xs-1">Varian</label>
			    <div class ="col-xs-5">
				<input class="form-control" name ="variant_value_seq" type="hidden"
				       value ="<?php echo $data_sel[LIST_DATA][0][0]->variant_value_seq; ?>">
				<input class="form-control" name ="value" type="text"
				       value ="<?php echo $data_sel[LIST_DATA][0][0]->value; ?>" readonly>
			    </div>
			</div>
			<br><br><br><br><br><br>
			<table id="tbl" class="diplay table table-bordered table-striped" cellpadding="0" cellspacing="0" border="0">
			    <tr>
				<th class="col-xs-1"><label class="control-label">Varian</label></th>
				<th class="col-xs-1"><label class="control-label">Merchant SKU</label></th>
				<th class="col-xs-1"><label class="control-label">Stok Sisa</label></th>
				<th class="col-xs-1"><label class="control-label"># Belum Bayar</label></th>
				<th class="col-xs-1"><label class="control-label"># Belum Kirim</label></th>
				<th class="col-xs-1"><label class="control-label">Stok</label></th>
			    </tr>
			    <?php
			    foreach ($data_sel[LIST_DATA][1] as $data) {
				?>
	    		    <tr>
	    		    <input type ="hidden" name ="value_seq[]" value ="<?php echo $data->value_seq; ?>">
	    		    <td class="col-xs-1"><input class="form-control" name ="size[]" type="text"
	    						value ="<?php echo $data->size; ?>" readonly></td>
	    		    <td class="col-xs-1"><input class="form-control" name ="merchant_sku[]" type="text"
	    						value ="<?php echo $data->merchant_sku; ?>"></td>
	    		    <td class="col-xs-1"><input class="form-control auto_int" name ="stock[]" type="text"
	    						value ="<?php echo $data->stock; ?>" readonly></td>
	    		    <td class="col-xs-1"><input class="form-control" name ="pending_payment[]" type="text"
	    						value ="<?php echo $data->pending_payment; ?>" readonly></td>
	    		    <td class="col-xs-1"><input class="form-control" name ="pending_delivery[]" type="text"
	    						value ="<?php echo $data->pending_delivery; ?>" readonly></td>
	    		    <td class="col-xs-1"><input class="form-control auto_int" name ="stock_add[]" type="text"
	    						value ="<?php echo $data->stock; ?>" required=""></td>
	    		    </tr>
				<?php
			    }
			}
			?>
    		</table>
    		&nbsp;
    		<div class ="form-group">
			<?php if ($data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
			    <div class ="col-xs-6"><?php echo get_back_button(); ?> </div>
			    <?php
			} else {
			    if ($data_auth[FORM_ACTION] == ACTION_ADD) {
				?>
	    		    <div class ="col-xs-6"><?php echo get_save_add_button(); ?> </div>
			    <?php } if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
	    		    <div class ="col-xs-6"><?php echo get_save_edit_button(); ?> </div>
			    <?php } ?>
			    <div class ="col-xs-6"><?php echo get_cancel_button(); ?> </div>
			<?php } ?>
    		</div>
    	    </section>
    	</div>
        </form>
    </div>

    <script type="text/javascript">
        var isFrmFillValid = $('#frmMain').validate({
    	highlight: function (element) {
    	    $(element).closest('.form-group').addClass('has-error');
    	},
    	unhighlight: function (element) {
    	    $(element).closest('.form-group').removeClass('has-error');
    	},
    	errorElement: 'span',
    	errorClass: 'help-block',
    	errorPlacement: function (error, element) {
    	    if (element.parent('.input-group').length) {
    		error.insertAfter(element.parent());
    	    } else {
    		error.insertAfter(element);
    	    }
    	},
    	rules: {
    	    stock_add: {
    		required: true
    	    },
    	}
        });

        $(document).on('submit', '#frmMain', function (e) {
    	if (isFrmFillValid.valid()) {
    	    $('input[type="text"]').each(function () {
    		if ($(this).hasClass('auto') || $(this).hasClass('auto_int') || $(this).hasClass('auto_dec'))
    		{
    		    var v = $(this).autoNumeric('get');
    		    $(this).autoNumeric('destroy');
    		    $(this).val(v);
    		}
    	    });
    	}
        });

        $('.auto_int').autoNumeric('init', {vMin: 0, mDec: 0});
    </script>

<?php } else { ?>
    <style>
        .btn.active {
    	display: none;
        }
    </style>
    <div class="box box-default">
        <div class="box-header with-border">
    	<h3 class="box-title"><?php echo get_title_list($data_auth[FORM_AUTH][FORM_TITLE]); ?></h3>
    	<div class="box-tools">
    	    <a href="<?php echo get_base_url() . "merchant/master/product_merchant" ?>" class="btn btn-default" style="margin-left:5px;"><i class="fa fa-arrow-left"></i> Kembali</a>
    	</div>
        </div>
        <div class="box-body">

    	<div id="filter-panel" class="collapse filter-panel">
    	    <div class="panel panel-default">
    		<div class="panel-body">
    		    <form id="frmAuth" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL]; ?>" ><input type="hidden" id="key" name="key" /></form>
    		    <form id ="frmSearch1" url= "<?php echo get_base_url() . $data_auth[FORM_URL]; ?>" class="form-inline" onsubmit="return false;">
    			<input type="hidden" id="start1" name="start" value="<?php
			    if (isset($_GET["start"])) {
				echo $_GET["start"];
			    } else {
				echo '1';
			    }
			    ?>">
    			<input type="hidden" id="length1" name="length" value="<?php
			    if (isset($_GET["length1"])) {
				echo $_GET["length1"];
			    } else {
				echo '20';
			    }
			    ?>">
    			<input type="hidden" name="order" value="asc">
    			<input type="hidden" name="column" value="value">
    			<input type="hidden" name="btnSearch" value="true">
    			<div class="form-group">
				<?php echo get_search_button(); ?>
    			</div>
    		    </form>
    		</div>
    	    </div>
    	</div>
    	<div class="col-xs-12" id="tabledata1"></div>
        </div>
    </div>
    <script type="text/javascript">
    <!--

        function listtable(idf) {
    	var urls = $("#frmSearch" + idf);
    	var htmls = '';
    	var pagings = '';
    	$.ajax({
    	    url: urls.attr('url'), data: urls.serialize(), type: 'POST', datatype: 'json',
    	    success: function (JSONString) {
    		var dataobj = $.parseJSON('[' + JSONString + ']');
    		$.each(dataobj, function () {
    		    var realdata = this['aaData'];
    		    $.each(realdata, function () {
    			htmls += '<div class="row" style="border:solid 1px gray;padding:5px;">' +
    				'<div class="row">' +
    				'<div class="col-xs-9">Nama Produk : <strong>' + this['product_name'] + '</strong></div>' +
    				'<div class="col-xs-3 text-right">Status : ' + this['active'] + '</div>' +
    				'</div>' +
    				'<div class="row">' +
    				'<div class="col-xs-4">Varian : ' + this['value'] + '</div>' +
    				'<div class="col-xs-8">SKU : ' + this['merchant_sku'] + '</div>' +
    				'</div>' +
    				'<div class="row">' +
    				'<div class="col-xs-4">Belum Bayar : ' + this['pending_payment'] + '</div>' +
    				'<div class="col-xs-4">Belum Kirim : ' + this['pending_delivery'] + '</div>' +
    				'<div class="col-xs-2">Stok : ' + this['stock'] + '</div>' +
    				'<div class="col-xs-2"><div class="pull-right">' + this['navigasi'] + '</div></div>' +
    				'</div>' +
    				'</div><div style="height:5px;">&nbsp;</div>';

    		    });
    		    var recorddata = this['iTotalRecords'];
    		    pagings = paging(recorddata, idf, "listtable");

    		});
    		$('#tabledata' + idf).html(htmls + pagings);
    	    }
    	});
        }
        listtable(1);
        function changest(idv, st) {
    	$.ajax({
    	    url: "<?php echo get_base_url() . "merchant/master/product_non_active_merchant" ?>",
    	    type: "POST",
    	    data: {p_variant_seq: idv, active_btn: st, btnSaveEdit: "save_update", txtMerchantnSID: "<?php echo $_SESSION[SESSION_MERCHANT_CSRF_TOKEN]; ?>"},
    	    dataType: "html",
    	    success: function (html) {
    		listtable(1);
    	    }
    	});
        }
        //-->
    </script>
    <?php
}
require_once get_include_page_list_merchant_content_footer();
require_once get_include_content_merchant_bottom_page_navigation();
?>
