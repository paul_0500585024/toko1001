<?php
$DRPdateFormat = function($params) {
    date_default_timezone_set("Asia/Jakarta");
    if ($params != '') {
        if ($params === 'today') {
            $result = date('d-M-Y');
        } else if ($params === 'tommorow') {
            $result = strtotime('+1 day');
            $result = date('d-M-Y', $result);
        }
    } else {
        $result = DEFAULT_DATETIME;
    }
    return $result;
};
$storeCloseFrom = isset($data_sel[LIST_DATA][0]->from_date) && $data_sel[LIST_DATA][0]->from_date > DEFAULT_DATETIME ? date_create($data_sel[LIST_DATA][0]->from_date) : date_create($DRPdateFormat('today'));
$storeCloseTo = isset($data_sel[LIST_DATA][0]->to_date) && $data_sel[LIST_DATA][0]->to_date > DEFAULT_DATETIME ? date_create($data_sel[LIST_DATA][0]->to_date) : date_create($DRPdateFormat('tommorow'));
$storeCloseFrom = date_format($storeCloseFrom, "d-M-Y");
$storeCloseTo = date_format($storeCloseTo, "d-M-Y");
require_once VIEW_BASE_MERCHANT;
require_once get_include_content_merchant_top_page_navigation();
echo $breadcrumb;
?>
<style>
    .switch:before{
        background: #00a65a;
    }
</style>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-preview-merchant-page" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="fa fa-times" aria-hidden="true"></span></button>
                <h4 class="modal-title" id="myModalLabel">Preview Tampilan Toko Anda</h4>
            </div>
            <div class="modal-body min-height500">
                <div class="row">
                    <img id="pv-banner" class="full-img height200" src="">
                </div>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-xs-2 cst-xs-2">
                        <center>
                            <p class="info-text">
                                <br>
                                Deskripsi toko anda
                            </p>
                        </center>
                        <div class="thumbnail cst-thumbnail">
                            <img id="pv-pic" src="" class="full-img">
                        </div>
                    </div>
                    <div class="col-xs-10">
                        <b>Nama Toko</b><br>
                        <div class="info-text" id="pv-description"></div>
                        <hr class="dashed-line">
                        <div class="row">
                            <?php
                            for ($i = 1; $i < 5; $i++) {
                                ?>
                                <div class = "col-xs-3">
                                    <div class = "thumbnail height200 bg-gray" style = "text-align: center">
                                        <div class = "box-product_sample"><span class="font-size-9em">Sample</span><br> product</div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row reset-margin">
    <div class="col-xs-12">
        <div class="box box-no-border-top p-10px no-border-radius m-top5">
            <div class="box-body">
                <form method="post" action="<?php echo get_base_url() . $data_auth[FORM_URL] ?>" enctype ="multipart/form-data">
                    <?php echo get_csrf_merchant_token(); ?>
                    <input class="" type="hidden"  name="old_logo" value="<?php echo (isset($data_sel[LIST_DATA][0]) || $data_sel[LIST_DATA][0]->old_logo_img == "" ? $data_sel[LIST_DATA][0]->logo_img : $data_sel[LIST_DATA][0]->old_logo_img); ?>">
                    <input class="" type="hidden"  name="old_banner" value="<?php echo (isset($data_sel[LIST_DATA][0]) || $data_sel[LIST_DATA][0]->old_banner_img == "" ? $data_sel[LIST_DATA][0]->banner_img : $data_sel[LIST_DATA][0]->old_banner_img); ?>">


                    <div class="row">
                        <div class="col-xs-12 m-bottom-10px">
                            <b>Tutup Toko</b>
                            <p class="info-text"><span class="red">*</span>Fitur tutup toko digunakan ketika anda ingin menutup toko anda, dengan mengaktifkan fitur ini semua produk yang anda jual hanya akan bisa dilihat namun tidak bisa di beli sampai toko anda kembali buka</p>
                        </div>
                        <div class="col-xs-6 h50px mt-10px">
                            <div class="switch-style">
                                <label class="switch-lbl">
                                    <input type="checkbox" class="no-show" id="storeStatus" name="co-status" 
                                           <?php echo isset($data_sel[LIST_DATA][0]->from_date) && $data_sel[LIST_DATA][0]->from_date != DEFAULT_DATETIME ? 'checked' : ''; ?>> 
                                    <div class="switch  <?php echo isset($data_sel[LIST_DATA][0]->from_date) && $data_sel[LIST_DATA][0]->from_date != DEFAULT_DATETIME ? 'switchOn' : ''; ?> ">
                                        <?php if (isset($data_sel[LIST_DATA][0]->from_date) && $data_sel[LIST_DATA][0]->from_date != DEFAULT_DATETIME) { ?>
                                            <span class="store-label-on" id='label-store-status'></span>
                                        <?php } else { ?>
                                            <span class="store-label-off" id='label-store-status'></span>
                                        <?php } ?>
                                    </div>
                                </label>
                            </div>
                            <img id="storeCloseImage" class="<?php echo $data_sel[LIST_DATA][0]->from_date > DEFAULT_DATETIME && $data_sel[LIST_DATA][0]->to_date > DEFAULT_DATETIME ? '' : 'no-show' ?>" src="<?php echo get_img_url() ?>/home/storeCloseMerchantOrange.png">
                        </div>
                        <div id="storeDateBox" style="margin-top: 10px;" class="col-xs-6 h50px <?php echo (isset($data_sel[LIST_DATA][0]->from_date) && $data_sel[LIST_DATA][0]->from_date > DEFAULT_DATETIME) ? '' : 'no-show' ?>">
                            <?php if (isset($data_sel[LIST_DATA][0]->from_date) && $data_sel[LIST_DATA][0]->from_date > DEFAULT_DATETIME) { ?>
                                <div id="storeDateBoxText" class="" style="color: red;margin-bottom: 5px">Anda menutup toko anda dari tanggal&nbsp;<?php echo $dateConvert($storeCloseFrom) ?>&nbsp;sampai dengan tanggal <?php echo $dateConvert($storeCloseTo) ?></div>
                                <input date_type="date" readonly class="form-control-cst full-width" id="store_inf_from" name="store_status" type="text" placeholder ="klik disini untuk menentukan periode tutup toko" value ="<?php echo isset($data_sel[LIST_DATA][0]->from_date) && $data_sel[LIST_DATA][0] != '' ? $data_sel[LIST_DATA][0]->from_date . ' - ' . $data_sel[LIST_DATA][0]->to_date : ''; ?>" required>
                            <?php } else { ?>
                                <input date_type="date" readonly class="form-control-cst full-width" id="store_inf_from" name="store_status" type="text" placeholder ="klik disini untuk menentukan periode tutup toko" value ="<?php echo isset($data_sel[LIST_DATA][0]->from_date) && $data_sel[LIST_DATA][0] != '' ? $data_sel[LIST_DATA][0]->from_date . ' - ' . $data_sel[LIST_DATA][0]->to_date : ''; ?>" required>
                            <?php } ?>
                        </div>

                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-xs-6">
                            <b>Deskripsi Toko</b>
                            <textarea id="description-active" name="description" class="form-control m-textarea pic-profile"><?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->description : "" ); ?></textarea>
                        </div>
                        <div class="col-xs-4">
                            <p class="info-text">
                                <br><br>
                                <b>Deskripsi Toko</b> Gunakan sebuah kalimat yang mendefinisikan toko anda.
                            </p> 
                        </div>
                    </div>
                    <hr>

                    <div class="row">
                        <div class="col-xs-2">
                            <b>Gambar Toko</b>
                            <div class="slim logo" data-size="200,200" data-min-size="150,150" data-ratio="1:1">
                                <?php echo isset($data_sel[LIST_DATA][0]->logo_img) && $data_sel[LIST_DATA][0]->logo_img != '' ? '<img src="' . MERCHANT_LOGO . $data_sel[LIST_DATA][0]->seq . '/' . $data_sel[LIST_DATA][0]->logo_img . '">' : '<img src="' . CDN_IMAGE . 'assets/img/admin_img.jpg' . '">'; ?>
                                <input type="file" name="logo_img">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <p class="info-text">
                                <br><br>
                                <b>Gambar Toko</b> upload sebuah logo atau gambar untuk toko anda, gambar yang di upload harus dalam bentuk JPEG, JPG, PNG dengan resolusi min 500x500 sampai dengan 1500x1500 harus berbentuk persegi  
                            </p>                        
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-xs-6">
                            <b>Banner Toko</b>
                            <div class="slim banner" data-size="1000,200" data-ratio="10:2">

                                <?php echo isset($data_sel[LIST_DATA][0]->banner_img) && $data_sel[LIST_DATA][0]->banner_img != '' ? '<img src="' .  MERCHANT_LOGO . $data_sel[LIST_DATA][0]->seq . '/' . $data_sel[LIST_DATA][0]->banner_img . '">' : '<img src="' . CDN_IMAGE . 'assets/img/home/landing_page.jpg' . '">'; ?> 
                                <input type="file" name="banner_img">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <p class="info-text">
                                <br><br><br><br>
                                <b>Banner Toko </b>gunakan gambar banner untuk menghiasi halaman profil toko anda dengan resolusi tinggi 200px x lebar 1200px.
                            </p>
                        </div>
                    </div>
                    <hr>
                    <div class="col-xs-2 pull-right">
                        <a id="preview" href="javascript:void(0)" class="btn btn-success btn-flat btn-block pull-right" data-toggle="modal" data-target="#myModal">Pratinjau Halaman</a>
                    </div>
                    <div class="col-xs-2 pull-right">
                        <button type="submit" name="<?php echo CONTROL_SAVE_EDIT_NAME; ?>" class="btn btn-success btn-flat btn-block pull-right">Simpan Perubahan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $("img.in").css('opacity',1);
        $('.switch').click(function () {
            var kondisi = $('#storeStatus').prop('checked');
            if (kondisi === false) {
                $('#label-store-status').removeClass('store-label-off');
                $('#label-store-status').addClass('store-label-on');
                $('#storeDateBox').removeClass('no-show');
                $('#storeCloseImage').removeClass('no-show');
                $(this).toggleClass('switchOn')
            } else {
<?php if ($data_sel[LIST_DATA][0]->from_date != DEFAULT_DATETIME) { ?>
                    var accesAuth = confirm('Apakah anda ingin membuka toko ? ');
                    if (accesAuth == true) {
                        $('#label-store-status').addClass('store-label-off');
                        $('#label-store-status').removeClass('store-label-on');
                        $('#storeDateBox').addClass('no-show');
                        $('#storeDateBoxText').remove();
                        $('#storeDateBoxInput').removeClass('no-show');
                        $('#storeCloseImage').addClass('no-show');
                        $(this).toggleClass('switchOn')
                    } else {
                        event.preventDefault();
                    }
<?php } else { ?>
                    $('#label-store-status').addClass('store-label-off');
                    $('#label-store-status').removeClass('store-label-on');
                    $('#storeDateBox').addClass('no-show');
                    $('#storeDateBoxText').remove();
                    $('#storeDateBoxInput').removeClass('no-show');
                    $('#storeCloseImage').addClass('no-show');
                    $(this).toggleClass('switchOn')
<?php } ?>
            }

        });



        $('input[date_type="date"]').daterangepicker({
            "locale": {
                "format": "DD-MMM-YYYY",
                "separator": " - ",
                "applyLabel": "Lanjutkan",
                "cancelLabel": "Batalkan",
                "fromLabel": "Dari tanggal",
                "toLabel": "Sampai tanggal",
                "customRangeLabel": "Custom",
                "weekLabel": "W",
                "daysOfWeek": [
                    "Minggu",
                    "Senin",
                    "Selasa",
                    "Rabu",
                    "Kamis",
                    "Jum'at",
                    "Sabtu"
                ],
                "monthNames": [
                    "Januari",
                    "Febuari",
                    "Maret",
                    "April",
                    "Mei",
                    "Juni",
                    "Juli",
                    "Agustus",
                    "September",
                    "Oktober",
                    "November",
                    "Desember"
                ],
                "firstDay": 1
            },
            "startDate": "<?php echo $storeCloseFrom ?>",
            "endDate": "<?php echo $storeCloseTo ?>",
            "minDate": "<?php echo $DRPdateFormat('today') ?>",
            "timePickerIncrement": 1,
            "opens": "left"
        });
    });


    $('input[date_type="date"]').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });


    $('#preview').click(function () {
        var pic_active = $('.logo').children('.slim-area').children('.slim-result').children('img').attr('src');
        var banner_active = $('.banner').children('.slim-area').children('.slim-result').children('img').attr('src');
        var description = $('#description-active').val();
        $('#pv-pic').attr('src', pic_active);
        $('#pv-banner').attr('src', banner_active);




    });

</script>

<?php
require_once get_include_page_list_merchant_content_footer();
require_once get_include_content_merchant_bottom_page_navigation();
?>
