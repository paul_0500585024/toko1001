<?php
require_once VIEW_BASE_MERCHANT;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">

<html>
    <head>
        <link href="<?php echo get_css_url(); ?>bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_css_url(); ?>merchant/v1/toko1001_merchant.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_css_url(); ?>merchant/toko1001_merchant.css" rel="stylesheet" type="text/css" />
        <style>
            .login-page{
                background: url("<?php echo get_img_url() . 'home/bg_pattern.png' ?>");
            }
        </style>
        <script type="text/javascript" src="<?php echo get_js_url(); ?>jQuery-2.1.4.min.js" ></script>
        <script type="text/javascript" src="<?php echo get_js_url(); ?>bootstrap.min.js" ></script>
        <script type="text/javascript" src="<?php echo get_js_url(); ?>jquery.validate.js" ></script>
    </head>
    <title>Forgot Password Merchant</title>
    <body class="min-width-m-login" style="background-image: url('<?php echo get_img_url() ?>/toko1001_login_background.jpg');background-size:  cover;background-repeat: no-repeat;">
        <div id="login-box" class="row no-margin">
            <div class="col-xs-12">
                <?php
                if (isset($data_err) && $data_err[ERROR] === true) {
                    ?>
                    <div id='msg-shw' class='shw-error'>
                        <span><?php echo $data_err[ERROR_MESSAGE][0] ?></span>
                    </div>
                <?php } elseif (isset($data_suc) && $data_suc[SUCCESS] === true) { ?>
                    <div id='msg-shw' class='shw-success'>
                        </i>&nbsp;&nbsp;<span><?php echo (isset($data_suc[SUCCESS_MESSAGE][0]) ? $data_suc[SUCCESS_MESSAGE][0] : SAVE_DATA_SUCCESS); ?></span>
                    </div>
                    <?php
                }
                ?>
            </div>			
            <div class="col-xs-5 col-xs-offset-6 m-top20">
                <div class="row m-top20">
                    <div class="col-xs-8 col-xs-offset-2 no-pad m-top40">
                        <div class="m-shadow-login">
                            <div class="col-xs-12 m-bottom-10px m-top10">
                                <h3 class="subtitle-login-m-login">Lupa Password Merchant Toko1001</h3>
                            </div><br/>
                            <div class="well no-border no-shadow no-border-radius-m-login-m-login custom-well-m-login">
                                <form class="form-horizontal" id="frmMain" action="<?php echo get_base_url() . "merchant/forgot_password"; ?>" method="post">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label>Email Address<span class="require">&nbsp;*</span></label>
                                            <input type="text" class="form-control" id="email" placeholder="Email" name="email" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label>Captcha</label>
                                            <?php echo $image; ?>
                                        </div>
                                    </div>

                                    <div class="form-group">   
                                        <div class="col-xs-12">
                                            <label>Masukkan Captcha<span class="require">&nbsp;*</span></label>
                                            <input type="text" class="form-control" name="security_code" id="security_code" placeholder="Masukkan captcha" validate="required[]">
                                        </div>
                                        <div class="col-xs-12">
                                            <div id="captcha_verify" class="verify"></div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <a class="pull-right m-bottom-10px" href="<?php echo base_url('merchant/login')?>">Login Merchant Toko1001</a>
                                            <button type="submit" name="btnSave" value="true" class="btn btn-block btn-info btn-block btn-danger"><i class="fa fa-save"></i>&nbsp; Lupa Password</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <center>
                    <p class="l-foot">Copyright &copy;&nbsp; 2015 - <?php echo date('Y') ?>&nbsp;PT. Deltamas Mandiri Sejahtera</p>
                </center>
            </div>
        </div>
    </body>
    <script>
        $(document).ready(function () {
            var screenLayer = ($(window).height() / 4);
            $('#login-box').css('margin-top', screenLayer + 'px');
            $('#msg-shw').delay(3000).fadeOut("slow");
        });

        $('#frmMain').validate({
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
                email: {
                    required: true,
                    email: true
                },
                security_code: {
                    required: true
                }
            }

        });
        $.validator.messages.required = "Data wajib diisi !";
        $.validator.messages.email = "Harap diisi dengan alamat email yang benar !";
    </script>
</body>
</html>