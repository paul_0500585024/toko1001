<?php
require_once VIEW_BASE_MERCHANT;
?>
<div class="row">
    <div class='col-xs-12'>
        <form id ="frmSearch1" url= "<?php echo get_base_url() . $data_auth[FORM_URL]; ?>" onsubmit="return false;">
            <div class="row">
                <div class="col-xs-5">
		    <div class="form-group">
			<label>Nama Produk</label>
			<input class="form-control" name="name" type="input" value="<?php
			if (isset($_GET["name"])) {
			    echo $_GET["name"];
			}
			?>">
		    </div>
		</div>
		<div class="col-xs-5">
		    <div class="form-group">
			<label>Status</label>
			<select class="form-control select2" name="status" style="width: 300px;">
			    <option value="">Semua</option>
			    <?php
			    $pilih = '';
			    if (isset($_GET["status"])) {
				$pilih = $_GET["status"];
			    }
			    echo combostatus(json_decode(STATUS_LC), $pilih);
			    ?>
			</select>
		    </div>
		</div>
		<div class="col-xs-2">
		    <div class="form-group">
			<label> </label>
			<?php echo get_search_button(); ?>
		    </div>
		</div>
                <input type="hidden" id="start1" name="start" value="<?php
		if (isset($_GET["start"])) {
		    echo $_GET["start"];
		} else {
		    echo '1';
		}
		?>">
                <input type="hidden" id="length1" name="length" value="<?php
		       if (isset($_GET["length1"])) {
			   echo $_GET["length1"];
		       } else {
			   echo '10';
		       }
		       ?>">
                <input type="hidden" name="order" value="desc">
                <input type="hidden" name="column" value="name">
                <input type="hidden" name="btnSearch" value="true">

	    </div>
        </form>
    </div>
</div>
</div>
