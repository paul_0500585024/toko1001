<?php
require_once VIEW_BASE_MERCHANT;
require_once get_include_content_merchant_top_page_navigation();
echo $breadcrumb;

function trim_text($input, $length = 100, $ellipses = true, $strip_html = true) {
    if ($strip_html)
        $input = strip_tags($input);
    if (strlen($input) <= $length)
        return $input;
    $last_space = strrpos(substr($input, 0, $length), ' ');
    $trimmed_text = substr($input, 0, $last_space);
    if ($ellipses)
        $trimmed_text .= ' ....';
    return $trimmed_text;
}
?>

<!--INBOX-->
<div role="tabpanel" class="tab-pane" id="t_6">
    <div class="row">
        <div class="box-body">
            <div class="col-xs-12">
                <button id="btnaddinbox" class="btn btn-success btn-flat pull-left" onclick="newmsg(0)" type="button"><i class="fa fa-plus"></i> Tulis Pesan Baru</button><br /><br />
            </div>
            <div class="col-xs-12" id="tabledata11">
                <ul class="list-group">
                    <?php
                    $icount = 0;
                    $clasodd = '';
                    $content = '';
                    $facoment = '';
                    $user_name = '';
                    $merchant_logo = '';
                    if (isset($data_sel[LIST_DATA][1])) {
                        foreach ($data_sel[LIST_DATA][1] as $msgdata) {
                            if ($msgdata->merchant_seq == $_SESSION[SESSION_MERCHANT_SEQ]) {
                                if ($merchant_logo == '')
                                    $merchant_logo = ($msgdata->logo_img == '' ? get_base_url() . IMG_BLANK_100 : get_base_url() . MERCHANT_LOGO . $msgdata->merchant_seq . "/" . $msgdata->logo_img);
                            }

                            $icount++;
                            if ($icount % 2 == true) {
                                $clasodd = ""; // " list-group-item-success";
                            } else {
                                $clasodd = "";
                            }
                            if ($msgdata->status == 'U' && $msgdata->admin_id != NULL) {
                                $content = "<strong>" . trim_text($msgdata->content) . "</strong>";
                                $facoment = "fa-comment";
                            } else {
                                $content = trim_text($msgdata->content);
                                $facoment = "fa-comment-o";
                            }
                            $headers = $msgdata->prev_message_seq != null ? $msgdata->prev_message_seq : $msgdata->seq;
                            if ($msgdata->merchant_seq !== NULL) {
                                $user_name = $msgdata->merchant_name;
                            } else {
                                $user_name = "Admin";
                            }
                            echo '
                                <li class="list-group-item' . $clasodd . '">
                                 <div class="row dataheader" id="header_' . $headers . '">
                                    <a href="javascript:detail(' . $headers . ')">
                                        <div class="col-xs-3"><i class="fa ' . $facoment . '"></i> ' . $user_name . '</div>
                                        <div class="col-xs-8">' . $content . '</div>
                                        <div class="col-xs-1">' . c_date($msgdata->created_date) . '</div>
                                    </a>
                                 </div>
                                 <div class="datadetail" id="detail_' . $headers . '"></div>
                               </li>';
                        }
                    }
                    ?>
                </ul>
                <div class="col-xs-12 no-pad">
                    <div class="box-pagging">
                        <center>
                            <?php echo $this->pagination->create_links(); ?>
                        </center>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<form class ="form-horizontal" id="frmInbox" name="frmInbox" method="post" url="<?php echo get_base_url() . $data_auth[FORM_URL]; ?>">
    <input type="hidden" id="tipe" name="tipe" value="msgsave">
    <input type="hidden" name="<?php echo CONTROL_ADDITIONAL_NAME; ?>" value="<?php echo ACTION_ADDITIONAL; ?>">
    <input type="hidden" id="prev_message_seq" name="prev_message_seq" value="0">
    <!-- Modal -->
    <div class="modal fade" id="InboxModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Pesan Baru</h4>
                </div>
                <div class="modal-body">
                    <textarea cols="80" rows="10" id="message_text" name="message_text" style="resize: none;" class="form-control"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary" onclick="savemsg()">Kirim Pesan</button>
                </div>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">

                        $(document).ready(function() {
                            $('#msg').click(function() {
                                $('#save_edit').hide();
                            });
                        });
                        $(document).ready(function() {
                            $('.no-msg').click(function() {
                                $('#save_edit').show();
                            });
                        });

                        function newmsg(idh) {
                            $('#message_text').val('');
                            $('#prev_message_seq').val(idh);
                            $('#InboxModal').modal('show');
                        }

                        function savemsg() {
                            if ($('#message_text').val() == '') {
                                alert('Pesan <?php echo ERROR_VALIDATION_MUST_FILL; ?>');
                                return false;
                            }
                            $('#InboxModal').modal('hide');
                            var urlmsgs = $("#frmInbox");
                            $.ajax({
                                url: urlmsgs.attr('url'), data: urlmsgs.serialize(), type: 'POST', datatype: 'json',
                                success: function(data) {
                                    location.reload();
                                }
                            });
                        }

                        function detail(idh) {
                            $('.dataheader').show();
                            $('.datadetail').collapse('hide');
                            $('#header_' + idh).hide();
                            htmls = '';
                            thelastreplay = '';
                            $.ajax({
                                url: "<?php echo get_base_url() . $data_auth[FORM_URL]; ?>", data: {'tipe': 'onemsg', '<?php echo CONTROL_ADDITIONAL_NAME; ?>': '<?php echo ACTION_ADDITIONAL; ?>', 'seq': idh}, type: 'POST', datatype: 'json',
                                success: function(JSONString) {
                                    var dataobj = $.parseJSON('[' + JSONString + ']');
                                    $.each(dataobj, function() {
                                        var realdata = this['aaData'];
                                        $.each(realdata, function() {
                                            if (this['member_seq'] == "<?php echo $_SESSION[SESSION_MERCHANT_SEQ]; ?>") {
                                                htmls += '<div class="direct-chat-msg"><div class="direct-chat-info clearfix"><span class="direct-chat-name pull-left">' + this['user_name'] + '</span><span class="direct-chat-timestamp pull-right">' + this['created_date'] + '</span></div><img class="direct-chat-img" src="<?php echo $merchant_logo; ?>">' + '<div class="direct-chat-text" style="color: rgb(255, 255, 255);background: rgb(60, 141, 188);border-color: rgb(60, 141, 188);">' + this['content'] + '</div>' + '</div>';
                                            } else {
                                                htmls += '<div class="direct-chat-msg right"><div class="direct-chat-info clearfix"><span class="direct-chat-name pull-right">Admin</span><span class="direct-chat-timestamp pull-left">' + this['created_date'] + '</span></div><img class="direct-chat-img" src="<?php echo get_image_location(); ?>assets/img/admin_default_img.jpg">' + '<div class="direct-chat-text">' + this['content'] + '</div>' + '</div>';
                                            }
                                            thelastreplay = this['member_seq'];
                                        });
                                    });
                                    if (thelastreplay != "<?php echo $_SESSION[SESSION_MERCHANT_SEQ]; ?>") {
                                        htmls += '<button type="button" onclick="newmsg(' + idh + ')" class="btn btn-default pull-right"><i class=" fa fa-reply"></i> Balas Pesan</button><br /><br />';
                                    }
                                    $('#detail_' + idh).html(htmls);
                                    $('#detail_' + idh).collapse('show');
                                }
                            });
                        }

</script>

<?php
require_once get_include_page_list_merchant_content_footer();
require_once get_include_content_merchant_bottom_page_navigation();
?>