<?php
require_once VIEW_BASE_MERCHANT;
require_once get_include_content_merchant_top_page_navigation();
echo $breadcrumb
?>

<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="p-10px">
        <div class="box box-no-border-top">
            <form class="form-horizontal" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] . "?" . $_SERVER["QUERY_STRING"]; ?>" enctype="multipart/form-data" name="frmMain" id="frmMain">
                <div class="box-body">
                    <section class="col-xs-12">
                        <?php echo get_csrf_merchant_token(); ?>
                        <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                            <input class="form-control"  name ="key" type="hidden" value ="<?php echo empty($data_sel[LIST_DATA][0]->seq) ? $data_sel[LIST_DATA][0]->product_seq : $data_sel[LIST_DATA][0]->seq ?>">
                        <?php } ?>
                        <div >
                            <div class="tab-product" >
                                <h3>Informasi Product</h3>
                                <section>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label class="control-label">Nama Produk&nbsp;<span class="require">*</span></label> 
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="input-group">
                                                        <input type="hidden" name="category_ln_seq" id="category_ln_seq" value="<?php
                                                        echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->category_ln_seq : "");
                                                        ?>">
                                                        <input class="form-control" validate ="required[]" id="name" name="name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->name) : "") ?>" maxlength="150" readonly>
                                                        <span class="input-group-addon bg-abu no-border" style="padding-right: 40px !important;">
                                                            Asuransi &nbsp;<input type="checkbox" name ="include_ins" <?php echo (isset($data_sel[LIST_DATA]) ? (($data_sel[LIST_DATA][0]->include_ins == "1" OR $data_sel[LIST_DATA][0]->include_ins == "on") ? "checked" : "") : "") ?> />
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class ="form-group">
                                                <div class="col-xs-12">
                                                    <label class ="control-label ">Keterangan Garansi</label>
                                                </div>
                                                <div class="col-xs-12">
                                                    <input class="form-control" id="warranty_notes" name="warranty_notes" type="text" maxlength="100" value ="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->warranty_notes) : "") ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class ="form-group">
                                                <div class="row">
                                                    <div class="col-xs-4">
                                                        <div class="col-xs-12"><label class ="control-label">Dimensi Produk (cm)</label></div>
                                                        <div class="col-xs-12"><input class="form-control auto aRight" name="p_length_cm" validate="num[]" maxlength="20" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->p_length_cm : "0") ?>"></div>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <div class="col-xs-12"><label class ="control-label">Lebar Produk (cm)</label></div>
                                                        <div class="col-xs-12"><input class="form-control auto aRight" name="p_width_cm" validate="num[]" maxlength="20" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->p_width_cm : "0") ?>"></div>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <div class="col-xs-12"><label class ="control-label">Tinggi Produk (cm)</label></div>
                                                        <div class="col-xs-12"><input class="form-control auto aRight" name="p_height_cm" validate="num[]" maxlength="20" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->p_height_cm : "0") ?>"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label class="control-label">Berat(Kg)</label>
                                                </div>
                                                <div class="col-xs-12 no-margin">
                                                    <div class="input-group no-pad-right">
                                                        <input id="p_weight_kg" name="p_weight_kg" validate="num[]" class="form-control auto aRight" type="text" maxlength="20" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->p_weight_kg : "0") ?>">
                                                        <div class="clearfix"></div>
                                                        <span class="input-group-addon bg-abu no-border">
                                                            Kg
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <div style="display:none">
                                            <div class ="form-group">
                                                <label class ="control-label col-xs-3">Dimensi dus (cm) PxLxT *</label>
                                                <div class="col-xs-3">
                                                    <input class="form-control auto" validate="num[]" maxlength="20" name="b_length_cm" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->b_length_cm : "0") ?>" >
                                                </div>
                                                <div class="col-xs-3">
                                                    <input class="form-control auto" validate="num[]" maxlength="20" name="b_width_cm" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->b_width_cm : "0") ?>" >
                                                </div>
                                                <div class="col-xs-3">
                                                    <input class="form-control auto" validate="num[]" maxlength="20" name="b_height_cm" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->b_height_cm : "0") ?>" >
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-3 control-label">Berat dengan dus (kg) *</label>
                                                <div class="col-xs-9">
                                                    <input id="b_weight_kg" validate="num[]" maxlength="20" name="b_weight_kg" class="form-control auto" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->b_weight_kg : "0") ?>" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row col-xs-12">
                                        <br>
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <div class="box box-warning " style="border:1px solid;">
                                                    <div class="box-header">
                                                        <h3 class="box-title">Kategori Produk</h3><input type="hidden" name="catseqval" id="catseqval" value="<?php echo (isset($data_sel[LIST_DATA][0]->catseqval) ? ($data_sel[LIST_DATA][0]->catseqval) : "") ?>">
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12"> 
                                                            <div class="box-body" id="procat">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </section><!-- end t1 -->
                                <h3>Deskripsi Produk</h3>
                                <section><br />
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <script type="text/javascript" src="<?php echo get_js_url() ?>ckeditor/ckeditor.js"></script>
                                            <div class ="form-group" style="margin:0 3px 0 3px;">
                                                <b>Deskripsi *</b><br />
                                                <textarea class="form-control" rows="3" id="description" validate="required[]" name="description"><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->description : "") ?></textarea>
                                            </div>
                                            <div class ="form-group" style="margin:15px 3px 10px 3px;">
                                                <b>Isi Kemasan</b><br />
                                                <textarea class="form-control" rows="3" id="content" name="content" ><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->content : "") ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </section><!-- end t2 -->
                                <h3>Spesifikasi Produk</h3>
                                <section><br />
                                    <div class="row">
                                        <div class="col-xs-6" style="">
                                            <div class="box-body" style="overflow-y:auto;height:400px;-webkit-box-shadow: 0 1px 2px #777;
                                                 -moz-box-shadow: 0 2px 1px #777;
                                                 box-shadow: 0 2px 1px #777;">
                                                <div id="spek"></div>
                                                <div id="spesifikasi"></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6" style="">
                                            <div class="box-body" style="overflow-y:auto;height:400px;-webkit-box-shadow: 0 1px 2px #777;
                                                 -moz-box-shadow: 0 2px 1px #777;
                                                 box-shadow: 0 2px 1px #777;">
                                                <h3>Spesifikasi Tambahan</h3><input type="hidden" name="spekname[]"><input type="hidden" name="spekval[]">
                                                <table class="table table-striped" id="customFields" width="100%">
                                                    <tr><td width="45%">Spesifikasi</td><td width="45%">Nilai</td>
                                                        <td width="10%"><a href="javascript:void(0);" id="addCF" class="btn btn-info btn-sm">Tambah</a></td>
                                                    </tr>
                                                    <?php
                                                    if (isset($data_sel[LIST_DATA][2])) {
                                                        foreach ($data_sel[LIST_DATA][2] as $each) {
                                                            ?>
                                                            <tr valign="top">
                                                                <td><input type="text" class="form-control" id="spekname" name="spekname[]" value="<?php echo get_display_value($each->name); ?>" maxlength="50"/></td>
                                                                <td><input type="text" class="form-control" id="spekval" name="spekval[]" value="<?php echo get_display_value($each->value); ?>" maxlength="50"/></td>
                                                                <td><a href="javascript:void(0);" class="remCF btn btn-danger btn-sm">Hapus</a></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row"><hr style="height: 1px;" />
                                        <div class="col-xs-12">
                                            <div class ="form-group" style="margin:0 3px 10px 3px;">
                                                <h3>Spesifikasi Khusus</h3>
                                                <textarea class="form-control" rows="3" id="specification" name="specification"><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->specification : "") ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </section><!-- end t3 -->
                                <h3>Harga dan Gambar Produk</h3>
                                <section><br />
                                    <?php
                                    $dtimg = 0;
                                    $fromvariant = '';
                                    $fromvariant .= product_merchant();
                                    ?>

                                    <?php if (!isset($data_sel[LIST_DATA][3])) { ?>

                                        <?php
                                    } else {
                                        $tabledata0 = '';
                                        if (isset($data_sel[LIST_DATA][3])) {
                                            if ($data_sel[LIST_DATA][3][0]->variant_value_seq == "1") {
                                                $data_sel[LIST_DATA][3][0]->merchant_seq = $data_sel[LIST_DATA][0]->merchant_seq;
                                                $data_sel[LIST_DATA][3][0]->dtimg = 0;
                                                $tabledata0 .= product_merchant($data_sel[LIST_DATA][3][0]);
                                                //$tabledata0 = str_ireplace('{vartext}', 'Info Produk', str_ireplace('{varval}', '1', str_ireplace('{dtimg}', '0', $tabledata0))) . '</tr>';
                                            } else {
                                                foreach ($data_sel[LIST_DATA][3] as $each) {
                                                    $each->dtimg = $each->value;
                                                    $each->merchant_seq = $data_sel[LIST_DATA][0]->merchant_seq;
                                                    $tabledata0 .= product_merchant($each);
                                                }
                                            }
                                        }
                                        ?>
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <div class="switch-style">
                                                    <label style="switch-lbl">
                                                        <input type="checkbox" class="no-show" name="varian"> 
                                                        <div class="switch <?php echo $data_sel[LIST_DATA][3][0]->variant_value_seq != "1" ? " switchOn" : "" ?>">
                                                            <?php if ($data_sel[LIST_DATA][3][0]->variant_value_seq == "1") { ?>
                                                                <span class="varian-label-off" id='label-status-variant'>Non Variant</span>
                                                            <?php } else { ?> 
                                                                <span class="varian-label-on" id='label-status-variant' >Variant</span>                                                                
                                                            <?php } ?>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                            <em class="pull-right"><?php echo IMAGE_PRODUCT_INFO ?></em>

                                        </div>
                                        <div class="row">
                                            <?php if ($data_sel[LIST_DATA][3][0]->variant_value_seq != "1") { ?>
                                                <div class="col-xs-12" id="tipevarian" ></div>
                                            <?php } ?>
                                        </div>
                                        <div id="variasi">
                                            <?php echo $tabledata0; ?>
                                        </div>                                                                 
                                    <?php } ?>                                      
                                </section><!-- end t4 -->
                                <?php //echo $tabledata0; ?>
                            </div>
                            <br>
                            <br>
                            <div class ="form-group">
                                <?php
                                if ($data_auth[FORM_ACTION] == ACTION_EDIT) {
                                    if ($data_sel[LIST_DATA][0]->status == 'L') {
                                        ?>
                                        <input name ="status" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->status; ?>">
                                        <div class ="pull-right col-xs-2">                                            
                                            <input type="hidden" name="<?php echo CONTROL_SAVE_EDIT_NAME ?>">
                                        </div>
                                        <?php
                                    } else {
                                        echo "<div class ='col-xs-6'></div>";
                                    }
                                } else {
                                    ?>
                                    <input name ="status" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->status; ?>">
                                    <div class ="col-xs-6">
                                        <button type="button" > test </button>
                                        <?php
                                        if ($data_sel[LIST_DATA][0]->status == 'N') {
//                                            echo get_save_edit_button();
                                        }
                                        ?> </div>
                                <?php } ?>
                            </div><!-- /.box-footer -->
                        </div>
                    </section>
                </div>
            </form>
        </div>
        <?php
        $dataatribute = '';
//echo $data_sel[LIST_DATA][0]->status;
        if ($data_sel[LIST_DATA][0]->status == "L") {
            if (isset($data_sel[LIST_DATA][1])) {
                foreach ($data_sel[LIST_DATA][1] as $each) {
                    $dataatribute.='{' . $each->attribute_value_seq . '}';
                }
            }
        } else {
            if (isset($data_sel[LIST_DATA][1])) {
                foreach ($data_sel[LIST_DATA][1] as $each) {
                    $dataatribute.='{' . $each->new_attribute_value_seq . '}';
                }
            }
        }
        ?>
        <script type="text/javascript">
                var icount = <?php echo $dtimg; ?>;
                var ivariant = [];
                var number = 0;
                var image = 0;
                var url = "<?php echo get_base_url() . $data_auth[FORM_URL]; ?>";
                var form = $("#frmMain").show();
                ;
                $(".tab-product").steps({
                    headerTag: "h3",
                    bodyTag: "section",
                    transitionEffect: "fade",
                    clearFixCssClass: "no-padding-left",
                    autoFocus: true,
                    enableAllSteps: true,
                    saveState: true,
                    enablePagination: <?php echo $data_sel[LIST_DATA][0]->status == LIVE_STATUS_CODE ? "true" : "false" ?>,
                    labels: {
                        finish: "Simpan",
                        next: "Lanjutkan",
                        previous: "Kembali",
                        loading: "Loading ..."
                    },
                    onStepChanging: function(event, currentIndex, newIndex)
                    {
                        if (currentIndex > newIndex)
                        {
                            return true;
                        }
                        if (newIndex === 0)
                        {
                            return false;
                        }
                        if (currentIndex < newIndex)
                        {
                            form.find(".body:eq(" + newIndex + ") label.error").remove();
                            form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
                        }
                        form.validate().settings.ignore = ":disabled,:hidden";
                        return form.valid();
                    },
                    onStepChanged: function(event, currentIndex, priorIndex)
                    {
                        $('#variasi .box-variant').each(function() {
                            var value = $(this).attr('id');
                            $("#variant-" + value).prop("checked", true);
                            $("#variant-" + value).prop("disabled", true);
                        })
                    },
                    onFinishing: function(event, currentIndex)
                    {
                        form.validate().settings.ignore = ":disabled";
                        return form.valid();
                    },
                    onFinished: function(event, currentIndex)
                    {
                        form.submit();
                    }
                });

                CKEDITOR.replace('description');
                CKEDITOR.replace('content');
                CKEDITOR.replace('specification');
                $('.slim').slim({
                    ratio: '1:1',
                    minSize: {
                        width: 850,
                        height: 850,
                    },
                    maxFileSize: "2",
                    jpegCompression: 8,
                    onRemove: function(slim, data) {
                        $.each(slim._originalElementAttributes, function(key, val) {
                            if (val.name == "number") {
                                number = val.value;
                            } else if (val.name == "image") {
                                image = val.value
                            }
                        })
                        $("#oldfile" + number + "_" + image).val("");
                    }
                })

                function cProcat() {
                    $("#procat").html("");
                    $.ajax({
                        url: url,
                        data: {btnAdditional: "act_s_adt", tipe: "category"
                        },
                        type: "POST",
                        success: function(response) {
                            if (isSessionExpired(response)) {
                                response_object = json_decode(response);
                                url = response_object.url;
                                location.href = url;
                            } else {
                                $("#procat").html(response);
                            }
                        },
                        error: function(request, error) {
                            alert(error_response(request.status));
                        }
                    });
                }
                // cek breadcrumb
                function cekseq(idm) {
                    $("#catseqval").val($('input[name=catseq]:checked', '#frmMain').val());
                    $('#t2').removeClass('hidden');
                    $('#t3').removeClass('hidden');
                    $('#t4').removeClass('hidden');
                    $.ajax({
                        url: url,
                        data: {btnAdditional: "act_s_adt", idh: idm, tipe: "breadcrumb"
                        },
                        type: "POST",
                        success: function(response) {
                            if (isSessionExpired(response)) {
                                response_object = json_decode(response);
                                url = response_object.url;
                                location.href = url;
                            } else {
                                $("#spek").html(response);
                                cekatribute();
                            }
                        },
                        error: function(request, error) {
                            alert(error_response(request.status));
                        }
                    });
                }
                //	cek  attribute
                function cekatribute() {
                    var idm = $("#alcat").val();
                    var nilaiarray = "<?php echo $dataatribute; ?>";
                    $.ajax({
                        url: url,
                        data: {btnAdditional: "act_s_adt", idh: idm, atrval: nilaiarray, tipe: "attribute"
                        },
                        type: "POST",
                        success: function(response) {
                            if (isSessionExpired(response)) {
                                response_object = json_decode(response);
                                url = response_object.url;
                                location.href = url;
                            } else {
                                $("#spesifikasi").html(response);
                            }
                        },
                        error: function(request, error) {
                            alert(error_response(request.status));
                        }
                    });
                }

                function getvarianttitle() {
                    var idm = $("#alcat").val();
                    $.ajax({url: url,
                        data: {btnAdditional: "act_s_adt", idh: idm, tipe: "variant"},
                        type: "POST",
                        success: function(response) {
                            if (isSessionExpired(response)) {
                                response_object = json_decode(response);
                                url = response_object.url;
                                location.href = url;
                            } else {
                                $("#tipevarian").html(response);
                            }
                        },
                        error: function(request, error) {
                            alert(error_response(request.status));
                        }
                    });
                }
                function cekvarian(nilai) {
                    if (nilai != 0) {
                        getvarianttitle();
                    }
                }
                function adcvclick() {
                    $("#variasi").show();
                    icount = (icount + 1);
                    var vartext = $("#varianval option:selected").text();
                    var varval = $("#varianval").val();
                    if ($('#' + varval).length) {
                        alert("Data sudah ada");
                        return;
                    } else {
                        ivariant.push(varval);
                    }
                    var str = '<?php echo str_replace("\r", "", str_replace("\n", "", $fromvariant)) ?>';
                    var newdata = str.replace(/{vartext}/g, vartext);
                    newdata = newdata.replace(/{varval}/g, varval);
                    newdata = newdata.replace(/{dtimg}/g, icount);
                    $("#variasi").append(newdata);
                    $('.auto').autoNumeric('init', {vMin: 0});
                    $('.auto_int').autoNumeric('init', {vMin: 0, mDec: 0});
                    $('.auto_dec').autoNumeric('init', {vMin: 0, mDec: 2});
                    $('html body').animate({
                        scrollTop: $("#" + varval).offset().top - 70
                    }, 1000);
                    $('#' + varval + ' .slim').slim();
                }

                $("#customVarian").on('click', '.remCV', function() {
                    var href = $(this).attr('title');
                    var index = ivariant.indexOf(href);
                    if (index > -1) {
                        ivariant.splice(index, 1);
                    }
                    $(this).parent().parent().parent().parent().remove();
                    return false;
                });

                function remove(id) {
                    if (confirm("Apakah anda yakin menghapus data yang dipilih ?")) {
                        $('#' + id).remove();
                    }
                }

                function addVariant(vartext, varval, id) {
                    if ($(id).is(":checked")) {
                        $("#variasi").show();
                        icount = (icount + 1);
                        if ($('#' + varval).length) {
                            alert("Data sudah ada");
                            return;
                        } else {
                            ivariant.push(varval);
                        }
                        var str = '<?php echo str_replace("\r", "", str_replace("\n", "", $fromvariant)) ?>';
                        var newdata = str.replace(/{vartext}/g, vartext);
                        newdata = newdata.replace(/{varval}/g, varval);
                        newdata = newdata.replace(/{dtimg}/g, icount);
                        $("#variasi").append(newdata);
                        $('.auto').autoNumeric('init', {vMin: 0});
                        $('.auto_int').autoNumeric('init', {vMin: 0, mDec: 0});
                        $('.auto_dec').autoNumeric('init', {vMin: 0, mDec: 2});
                        $('body').animate({
                            scrollTop: $("#" + varval).offset().top - 70
                        }, 1000);
                        $('#' + varval + ' .slim').slim();
                    } else {
                        remove(varval);
                    }
                }

                function cekdiskon(hargap, hargaj, iddisk) {
                    if ($.isNumeric(hargap) && $.isNumeric(hargaj)) {
                        var nilaidiskon = (hargap - hargaj) / hargap * 100;
                        iddisk.val(Math.round(nilaidiskon));
                    }
                }
                function prodpc(idproc, nomor) {
                    var $this = $(idproc);
                    var salep = $('#sell_price' + nomor).autoNumeric('get');
                    var prodp = $this.autoNumeric('get');
                    cekdiskon(prodp, salep, $('#disc_percent' + nomor));
                }
                function salepc(idproc, nomor) {
                    var $this = $(idproc);
                    var prodp = $('#product_price' + nomor).autoNumeric('get');
                    var salep = $this.autoNumeric('get');
                    cekdiskon(prodp, salep, $('#disc_percent' + nomor));
                }

    <?php
    if (isset($data_sel[LIST_DATA])) {
        if (isset($ivariant)) {
            foreach ($ivariant as $datavariant) {
                echo "ivariant.push('" . $datavariant . "');";
            }
        } else {
//            echo "$('#tipevarian').hide();";
        }
        ?>
                    $('#t2').removeClass('hidden');
                    $('#t3').removeClass('hidden');
                    $('#t4').removeClass('hidden');
                    $("#catseq").prop("disabled", true);
                    $.ajax({
                        url: url,
                        data: {btnAdditional: "act_s_adt", idh: '<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->category_ln_seq : ""); ?>', tipe: "breadcrumb"
                        },
                        type: "POST",
                        success: function(response) {
                            if (isSessionExpired(response)) {
                                response_object = json_decode(response);
                                url = response_object.url;
                                location.href = url;
                            } else {
                                $("#procat").html(response);
                                cekatribute();
                                getvarianttitle();
                            }
                        },
                        error: function(request, error) {
                            alert(error_response(request.status));
                        }
                    });
                    $("#spek").html("<h3>Spesifikasi Produk</h3>");
                    $(document).ready(function() {
                        $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
                            localStorage.setItem('activeTab', $(e.target).attr('href'));
                        });
                        var activeTab = localStorage.getItem('activeTab');
                        if (activeTab) {
                            $('#myTab a[href="' + activeTab + '"]').tab('show');
                        }
                        cekvarian(<?php echo $data_sel[LIST_DATA][3][0]->variant_value_seq == "1" ? "1" : "0" ?>);
                    });
        <?php
    }
    ?>
        </script>
    </div>
<?php } else { ?>
    <div class="box-body">
        <div id="header">
            <div id="sub-navbar" class="box-body sub-header">
                <div class="panel-body no-pad">
                    <div class="row no-pad">
                        <div class="col-xs-4">
                            <form id="frmSearchExcl" action ="<?php echo base_url($data_auth[FORM_URL]) ?>" method="get">   
                                <div class="input-group">
                                    <input type="text" name="search" class="form-control src-input" placeholder="<?php echo empty(htmlspecialchars($this->input->get('search'))) ? "Cari Produk" : "Pencarian : " . htmlspecialchars($this->input->get('search')) ?>" value="<?php echo $this->input->get('search') ?>" > 
                                    <span class="input-group-btn">
                                        <button class="btn btn-danger" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                    </span>
                                    <input type="hidden" name ="status" value ="<?php echo $this->input->get('status') ?>">                                    
                                </div>
                            </form>
                        </div>
                    </div>
                    <br>
                    <div class="row no-pad">
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-xs-9 ">
                                    <?php
                                    $codeActive = $this->input->get('status');
                                    ?>

                                    <?php for ($i = 0; $i < count($data_sel[LIST_DATA][0]); $i++) { ?>                        
                                        <a class="btn btn-flat <?php echo (isset($data_sel[LIST_DATA][0][$i]) && ($data_sel[LIST_DATA][0][$i]->status_cd === $codeActive || (!isset($codeActive) && $i == 0)) ? 'btn-active' : '' ); ?>" href="<?php echo base_url($data_auth[FORM_URL]) . "?status=" . $data_sel[LIST_DATA][0][$i]->status_cd . $filter . $filter_sort ?>" ><?php echo $data_sel[LIST_DATA][0][$i]->status ?> (<?php echo number_format($data_sel[LIST_DATA][0][$i]->total) ?>)</a>                         
                                    <?php } ?>
                                </div>
                                <div class="col-xs-3">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <button class="btn btn-success btn-block btn-block flat" type="button" id="btnxls" name="btnxls" onclick="data_report()"><i class="fa fa-file-excel-o"></i> Excel</button>
                                        </div>
                                        <?php echo get_order_filter("created_date", "name", base_url($data_auth[FORM_URL]) . $filter_status . $filter, $this->input->get("sort"), $this->input->get("column_sort")) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <br>
        <?php
        $old_product_seq = "";
        if (isset($data_sel[LIST_DATA][1])) {
            foreach ($data_sel[LIST_DATA][1] as $merchant) {
                if ($merchant->seq != $old_product_seq) {
                    if ($old_product_seq != "") {
                        ?>
                        </tbody>
                        </table>    
                    </div>
                    </div>
                    </div>
                    </div>
                <?php } $old_product_seq = $merchant->seq;
                ?>
                <div class="col-xs-12 no-padding">     
                    <div class="box box-no-border-top no-padding">
                        <div class="box-header">
                            <?php echo $merchant->name ?>
                        </div>
                        <div class="box-body">
                            <div class="col-xs-3">
                                <div class="col-xs-12">
                                    <a href="<?php echo base_url() . strtolower(url_title($merchant->name . get_variant_value($merchant->variant_seq, $merchant->variant_value, " "))) . '-' . $merchant->product_variant_seq ?>" target="_blank"><img src="<?php echo PRODUCT_UPLOAD_IMAGE . $merchant->merchant_seq . '/' . S_IMAGE_UPLOAD . $merchant->pic_1_img; ?>"></a>
                                </div>
                                <div class="col-xs-12">
                                    Status : <label class="label <?php echo $merchant->status == LIVE_STATUS_CODE ? "label-success" : "label-warning" ?>"><?php echo $merchant->status == LIVE_STATUS_CODE ? "Tayang" : "Perubahan" ?></label>                
                                </div>
                                <div class="col-xs-12">
                                    Tanggal Tayang : <?php echo date("d-M-Y", strtotime($merchant->created_date)) ?>
                                </div>
                                <div class="col-xs-12">
                                    Berat : <?php echo $merchant->p_weight_kg ?> Kg
                                </div>
                                <div class="col-xs-12">
                                    Dimensi : <?php echo $merchant->p_length_cm . " cm x " . $merchant->p_width_cm . " cm x " . $merchant->p_height_cm . " cm" ?>
                                </div>
                                <div class="col-xs-12">
                                    <a href="<?php echo base_url($data_auth[FORM_URL]) . "?" . CONTROL_GET_TYPE . "=" . ACTION_EDIT . "&key=" . $merchant->seq ?>" class="btn btn-xs btn-pinterest"> Ubah Produk</a>
                                </div>
                            </div>                

                            <div class="col-xs-9">
                                <table class="table table-bordered" >
                                    <thead class="table-head">
                                    <td width="100px">Variasi</td>                        
                                    <td width="150px">SKU</td>
                                    <td width="100px" class="aRight">Harga Jual (Rp)</td>
                                    <td width="100px" class="aRight">Harga Promo (Rp)</td>
                                    <td width="50px" class="aRight">Stok</td>                        
                                    <td width="70px">Aktif / Non Aktif</td>
                                    </thead>
                                    <tbody>
                                    <?php } ?>
                                    <tr class="font-size-12px">
                                        <td><?php echo $merchant->variant_value ?></td>
                                        <td><input id ="sku_<?php echo $merchant->product_variant_seq ?>" type="text" class="full-width" value="<?php echo $merchant->merchant_sku ?>" onChange ="stock_change(<?php echo $merchant->product_variant_seq ?>)"></td>
                                        <td class="aRight"><input id="price_<?php echo $merchant->product_variant_seq ?>" type="text" class="auto_int aRight chart"  value="<?php echo $merchant->product_price ?>" onchange="product_price_change(<?php echo $merchant->product_variant_seq ?>)" old_value ="<?php echo $merchant->product_price ?>"></td>
                                        <td class="aRight"><input id="sale_<?php echo $merchant->product_variant_seq ?>" type="text" class="auto_int aRight chart"  value="<?php echo $merchant->sell_price ?>" onchange="sell_price_change(<?php echo $merchant->product_variant_seq ?>)" old_value ="<?php echo $merchant->sell_price ?>"></td>
                                        <td class="aRight"><input id="<?php echo $merchant->product_variant_seq ?>" type="text" class="auto_int aRight chart"  value="<?php echo $merchant->stock ?>" onChange ="stock_change(<?php echo $merchant->product_variant_seq ?>)"></td>
                                        <td class="aRight"><input type="checkbox" <?php echo $merchant->active == "1" ? "checked" : "" ?> data-toggle="toggle" data-size="small" onChange="chk_change(this.checked,<?php echo $merchant->product_variant_seq ?>)" ></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    <?php } else { ?>
        <div class="col-xs-12 no-padding">     
            <div class="box box-no-border-top">
                <div class="box-body min-height300">
                    <?php $nameProductActive = htmlspecialchars($this->input->get('search')); ?>
                    <center class="no-found-src" ><?php echo isset($nameProductActive) && $nameProductActive != '' ? 'Tidak ditemukan produk dengan hasil pencarian : <b>' . $nameProductActive . '</b>' : 'Produk Tidak Ditemukan'; ?></center>
                </div>
            </div>
        </div>
    <?php } ?>


    <div class="col-xs-12 no-pad">
        <div class="box-pagging">
            <center>
                <?php echo $this->pagination->create_links(); ?>
            </center>
        </div>
    </div>
    </div>
    </div>

    <script>
                function chk_change(chk_active, seq) {
                    $.ajax({
                        url: "<?php echo base_url($data_auth[FORM_URL]) ?>",
                        type: "POST",
                        data: {btnAdditional: true, tipe: "<?php echo TASK_TYPE_SET_PRODUCT_ACTIVE ?>", active: chk_active, product_variant_seq: seq},
                        success: function(response) {
                            if (isSessionExpired(response)) {
                                response_object = json_decode(response);
                                url = response_object.url;
                                location.href = url;
                            }
                        }
                    })
                }

                function stock_change(seq) {
                    var stock_val = $('#' + seq).autoNumeric('get');
                    var sku_val = $('#sku_' + seq).val();
                    $.ajax({
                        url: "<?php echo base_url($data_auth[FORM_URL]) ?>",
                        type: "POST",
                        data: {btnAdditional: true, tipe: "<?php echo TASK_TYPE_SET_PRODUCT_STOCK ?>", stock: stock_val, merchant_sku: sku_val, product_variant_seq: seq},
                        success: function(response) {
                            if (isSessionExpired(response)) {
                                response_object = json_decode(response);
                                url = response_object.url;
                                location.href = url;
                            }
                        }
                    })
                }

                function product_price_change(seq) {
                    var price_val = $('#price_' + seq).autoNumeric('get');
                    var old_val = $('#price_' + seq).attr("old_value");
                    if (price_val > 0) {
                        $.ajax({
                            url: "<?php echo base_url($data_auth[FORM_URL]) ?>",
                            type: "POST",
                            dataType: "json",
                            data: {btnAdditional: true, tipe: "<?php echo TASK_TYPE_SET_PRODUCT_PRICE ?>", product_price: price_val, product_variant_seq: seq},
                            success: function(response) {
                                if (isSessionExpired(response)) {
                                    response_object = json_decode(response);
                                    url = response_object.url;
                                    location.href = url;
                                } else {
                                    if (response.error) {
                                        alert("Harga Jual tidak boleh lebih kecil dari Harga Promo! ");
                                        $('#price_' + seq).autoNumeric('set', old_val);
                                    } else {
                                        $('#price_' + seq).attr('old_value', price_val);
                                    }
                                }
                            }
                        })
                    } else {
                        alert("Harga Jual harus lebih besar dari 0! ");
                        $('#price_' + seq).autoNumeric('set', old_val);
                    }
                }

                function sell_price_change(seq) {
                    var sale_val = $('#sale_' + seq).autoNumeric('get');
                    var old_val = $('#sale_' + seq).attr("old_value");
                    if (sale_val > 0) {
                        $.ajax({
                            url: "<?php echo base_url($data_auth[FORM_URL]) ?>",
                            type: "POST",
                            dataType: "json",
                            data: {btnAdditional: true, tipe: "<?php echo TASK_TYPE_SET_PRODUCT_SELL_PRICE ?>", sell_price: sale_val, product_variant_seq: seq},
                            success: function(response) {
                                if (isSessionExpired(response)) {
                                    response_object = json_decode(response);
                                    url = response_object.url;
                                    location.href = url;
                                } else {
                                    if (response.error) {
                                        alert("Harga Jual tidak boleh lebih kecil dari Harga Promo! ");
                                        $('#sale_' + seq).autoNumeric('set', old_val);
                                    } else {
                                        $('#sale_' + seq).attr("old_value", sale_val);
                                    }
                                }
                            }
                        })
                    } else {
                        alert("Harga Promo harus lebih besar dari 0! ");
                        $('#sale_' + seq).autoNumeric('set', old_val);
                    }
                }

                function data_report() {
                    var data = $('#frmSearchExcl').serializeArray();
                    data.push({name: 'type', value: 'product'});
                    $.ajax({
                        url: "<?php echo get_base_url(); ?>merchant/report",
                        data: data,
                        type: "get",
                        success: function(response) {
                            if (isSessionExpired(response)) {
                            } else {
                                try {
                                    response = jQuery.parseJSON(response);
                                    if (response != null) {
                                        create_report(response);
                                    } else {
                                        alert("Tidak ada data.");
                                    }
                                } catch (e) {
                                    alert("Tidak ada data.");
                                }
                                return false;
                            }
                        },
                        error: function(request, error) {
                            alert(error_response(request.status));
                        }
                    })
                }
    </script>   
<?php } ?>
<div class="clearfix"></div></div>
<?php
require_once get_include_page_list_merchant_content_footer();
require_once get_include_content_merchant_bottom_page_navigation();
?>
