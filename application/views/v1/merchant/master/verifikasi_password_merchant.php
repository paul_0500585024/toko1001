<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<div class='container'>
<br><br><br><br>
<div class="panel panel-primary">
    <div class="panel-heading">Ubah Password</div>
    <div class="panel-body">
      
    <?php if($value == "failed"){?>
    <center><h2><span class="label label-danger">Link expired, password gagal diubah, silahkan melakukan pengajuan sekali lagi <a href="<?=base_url();?>/merchant/forget_password">di sini</a>. Terima Kasih</span></h2></center>
    <?php } elseif($value == "success") {?>
    <center><h2><span class="label label-success">Password sudah diubah, silahkan cek email anda. Terima Kasih</span></h2></center>
    <?php }
    else{ ?>
    <center><h2><span class="label label-warning">Tidak bisa memproses ulang</span></h2></center>
    <?php }?>
</div>
</div>
</div>
