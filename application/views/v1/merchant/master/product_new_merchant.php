<?php
require_once VIEW_BASE_MERCHANT;
require_once get_include_content_merchant_top_page_navigation();
echo $breadcrumb;
?>

<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT OR $data_auth[FORM_ACTION] == ACTION_VIEW) { ?>
    <div class="p-10px">
        <div class="box no-border">
            <form class="form-horizontal" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] . "?" . $_SERVER["QUERY_STRING"]; ?>" enctype="multipart/form-data" name="frmMain" id="frmMain">
                <div class="box-body">
                    <div class="col-xs-12">
                        <?php echo get_csrf_merchant_token(); ?>
                        <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                            <input class="form-control"  name ="seq" type="hidden" value ="<?php echo $data_sel[LIST_DATA][0]->seq; ?>">
                        <?php } ?>
                        <div>
                            <div class="tab-product">
                                <h3>Informasi Product</h3>
                                <section>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label class="control-label">Nama Produk&nbsp;<span class="require">*</span></label> 
                                                </div>
                                                <div class="col-xs-12">
                                                    <p class="info-text"><?php echo CAPTION_INFO_PRODUCT_NAME_CATEGORI ?></p>
                                                    <div class="input-group">
                                                        <input type="hidden" name="category_ln_seq" id="category_ln_seq" value="<?php
                                                        echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->category_ln_seq : "");
                                                        ?>">
                                                        <input class="form-control" validate ="required[]" id="name" name="name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->name) : "") ?>" maxlength="150">
                                                        <span class="input-group-addon bg-abu no-border" style="padding-right: 40px !important;">
                                                            Asuransi &nbsp;<input type="checkbox" name ="include_ins" <?php echo (isset($data_sel[LIST_DATA]) ? (($data_sel[LIST_DATA][0]->include_ins == "1" OR $data_sel[LIST_DATA][0]->include_ins == "on") ? "checked" : "") : "") ?> />
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-4">
                                            <div class ="form-group">
                                                <div class="col-xs-12">
                                                    <label class ="control-label ">Keterangan Garansi</label>
                                                </div>
                                                <div class="col-xs-12">
                                                    <input class="form-control" id="warranty_notes" name="warranty_notes" type="text" maxlength="100" value ="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->warranty_notes) : "") ?>">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-6">
                                            <div class ="form-group">
                                                <div class="row">
                                                    <div class="col-xs-4">
                                                        <div class="col-xs-12"><label class ="control-label">Panjang Produk (cm)</label></div>
                                                        <div class="col-xs-12"><input class="form-control auto aRight" name="p_length_cm" validate="num[]" maxlength="20" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->p_length_cm : "0") ?>"></div>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <div class="col-xs-12"><label class ="control-label">Lebar Produk (cm)</label></div>
                                                        <div class="col-xs-12"><input class="form-control auto aRight" name="p_width_cm" validate="num[]" maxlength="20" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->p_width_cm : "0") ?>"></div>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <div class="col-xs-12"><label class ="control-label">Tinggi Produk (cm)</label></div>
                                                        <div class="col-xs-12"><input class="form-control auto aRight" name="p_height_cm" validate="num[]" maxlength="20" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->p_height_cm : "0") ?>"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label class="control-label">Berat(Kg)</label>
                                                </div>
                                                <div class="col-xs-12 no-margin">
                                                    <div class="input-group no-pad-right">
                                                        <input id="p_weight_kg" name="p_weight_kg" validate="num[]" class="form-control auto aRight" type="text" maxlength="20" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->p_weight_kg : "0") ?>">
                                                        <div class="clearfix"></div>
                                                        <span class="input-group-addon bg-abu no-border">
                                                            Kg
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>

                                        <div style="display:none">
                                            <div class ="form-group">
                                                <label class ="control-label col-xs-3">Dimensi dus (cm) PxLxT *</label>
                                                <div class="col-xs-3">
                                                    <input class="form-control auto" validate="num[]" maxlength="20" name="b_length_cm" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->b_length_cm : "0") ?>" >
                                                </div>
                                                <div class="col-xs-3">
                                                    <input class="form-control auto" validate="num[]" maxlength="20" name="b_width_cm" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->b_width_cm : "0") ?>" >
                                                </div>
                                                <div class="col-xs-3">
                                                    <input class="form-control auto" validate="num[]" maxlength="20" name="b_height_cm" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->b_height_cm : "0") ?>" >
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-3 control-label">Berat dengan dus (kg) *</label>
                                                <div class="col-xs-9">
                                                    <input id="b_weight_kg" validate="num[]" maxlength="20" name="b_weight_kg" class="form-control auto" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->b_weight_kg : "0") ?>" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="cat-box-trigger" class="row no-margin">
                                        <div class="col-xs-12">
                                            <div class="form-group add-border-1px" style="position: relative;overflow: hidden">
                                                <div id="lock-box" class="no-show"></div>
                                                <div class="box no-border error-box">

                                                    <div class="box-header">
                                                        <h3 class="box-title">Kategori Produk</h3><input type="hidden" name="catseqval" id="catseqval" value="<?php echo (isset($data_sel[LIST_DATA][0]->catseqval) ? ($data_sel[LIST_DATA][0]->catseqval) : "") ?>">
                                                        <p class="info-text"><?php echo CAPTION_INFO_PRODUCT_CHOOSE_CATEGORI ?></p>
                                                        <hr>
                                                    </div> 
                                                    <br>
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <div class="box-body" id="procat" style="overflow-y:auto;height:450px;">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-6">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <div id="spek"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </section>

                                <h3>Deskripsi Produk</h3>
                                <section>
                                    <div class="row">
                                        <script type="text/javascript" src="<?php echo get_js_url() ?>ckeditor/ckeditor.js"></script>
                                        <div class="col-xs-12">
                                            <div class ="form-group" style="margin:0 3px 0 3px;">
                                                <b>Deskripsi <span class="require">*</span></b><br/>
                                                <textarea class="form-control" rows="3" id="description" name="description"><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->description : "") ?></textarea>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class ="form-group" style="margin:15px 3px 10px 3px;">
                                                <b>Isi Kemasan</b><br/>
                                                <textarea class="form-control" rows="3" id="content" name="content" ><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->content : "") ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <h3>Spesifikasi Produk</h3>
                                <section>
                                    <div class="row">
                                        <div class="col-xs-6 spesifikasi">
                                            <div class="spesification" id="spek1">
                                                <div class="col-xs-12">
                                                    <h3>Spesifikasi</h3>
                                                </div>
                                                <div id="spek" class="no-show"></div>
                                                <div id="spesifikasi"></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 spesifikasi">
                                            <div class="spesification" id="spek2">
                                                <div class="col-xs-12">
                                                    <h3>Spesifikasi Tambahan</h3>
                                                </div>
                                                <input type="hidden" name="spekname[]"><input type="hidden" name="spekval[]">
                                                <table class="table table-striped" id="customFields" width="100%">
                                                    <tr><td width="45%">Spesifikasi</td><td width="45%">Nilai</td>
                                                        <td width="10%"><a href="javascript:void(0);" id="addCF" class="btn btn-info btn-sm">Tambah</a></td>
                                                    </tr>
                                                    <?php
                                                    if (isset($data_sel[LIST_DATA][2])) {
                                                        foreach ($data_sel[LIST_DATA][2] as $each) {
                                                            ?>
                                                            <tr valign="top">
                                                                <td><input type="text" class="form-control" id="spekname" name="spekname[]" value="<?php echo get_display_value($each->name); ?>" maxlength="50" /></td>
                                                                <td><input type="text" class="form-control" id="spekval" name="spekval[]" value="<?php echo get_display_value($each->value); ?>" maxlength="50" /></td>
                                                                <td><a href="javascript:void(0);" class="remCF btn btn-danger btn-sm">Hapus</a></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </table>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="row"><hr style="height: 1px;" />
                                        <div class="col-xs-12">
                                            <div class ="form-group">
                                                <h3>Spesifikasi Khusus</h3>
                                                <p class="info-text"><?php echo CAPTION_INFO_PRODUCT_SPESIFICATION ?></p>
                                                <textarea class="form-control" rows="3" id="specification" name="specification"><?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->specification : "") ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <h3>Harga Dan Gambar Produk</h3>
                                <section>
                                    <?php
                                    $dtimg = 0;
                                    $fromvariant = '';
                                    $fromvariant .= product_merchant(null, true);
                                    ?>
                                    <?php
                                    if (!isset($data_sel[LIST_DATA][3])) {
                                        ?>
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <div class="switch-style">
                                                    <label style="switch-lbl">
                                                        <input type="checkbox" class="no-show" id="varian" name="varian"> 
                                                        <div class="switch">
                                                            <span class="varian-label-off" id='label-status-variant'>Non Variant</span>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                            <em class="pull-right"><?php echo IMAGE_PRODUCT_INFO ?></em>
                                            <div class="col-xs-12" id="tipevarian" style="display: none;">Varian produk tidak tersedia</div>
                                        </div>
                                        <div id="allvar">
                                            <table class="table table-striped">
                                                <?php
                                                echo str_ireplace('{vartext}', 'Info Produk', str_ireplace('{varval}', '1', str_ireplace('{dtimg}', '0', str_ireplace('del_item', 'no-show', $fromvariant))));
                                                ?>
                                            </table>
                                        </div>

                                        <div id="variasi" style="display:none;">
                                            <table id="customVarian" class="table table-striped">
                                            </table>
                                        </div>


                                        <?php
                                    } else {
                                        $tabledata0 = '';
                                        $tabledata1 = '';
                                        if (isset($data_sel[LIST_DATA][3])) {
                                            if ($data_sel[LIST_DATA][3][0]->variant_value_seq == "1") {
                                                $data_sel[LIST_DATA][3][0]->merchant_seq = $data_sel[LIST_DATA][0]->merchant_seq;
                                                $data_sel[LIST_DATA][3][0]->dtimg = 0;
                                                $tabledata0 .= product_merchant($data_sel[LIST_DATA][3][0], true);
//                                                $tabledata0 .= str_ireplace('{vartext}', 'Info Produk', str_ireplace('{varval}', '1', str_ireplace('{dtimg}', '0', str_ireplace('del_item', 'no-show', $tabledata0))));
                                                ?>
                                                <div class="row">
                                                    <div class="col-xs-4">
                                                        <div class="switch-style">
                                                            <label style="switch-lbl">
                                                                <input type="checkbox" class="no-show" id="varian" name="varian"> 
                                                                <div class="switch">
                                                                    <span class="varian-label-off" id='label-status-variant'>Non Variant</span>
                                                                </div>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <em class="pull-right"><?php echo IMAGE_PRODUCT_INFO ?></em>
                                                    <div class="col-xs-12" id="tipevarian" style="display: none;">Varian produk tidak tersedia</div>
                                                </div>
                                                <div id="allvar" >
                                                    <table class="table table-striped">
                                                        <?php echo $tabledata0 ?>
                                                    </table>
                                                </div>

                                                <div id="variasi" style="display:none;">
                                                    <table id="customVarian" class="table table-striped">

                                                    </table>
                                                </div>
                                                <?php
                                            } else {
                                                foreach ($data_sel[LIST_DATA][3] as $each) {
                                                    $each->dtimg = $each->variant_value_seq;
                                                    $each->merchant_seq = $data_sel[LIST_DATA][0]->merchant_seq;
                                                    $tabledata1 .= product_merchant($each, true);
                                                }
                                                ?>

                                                <div class="row">
                                                    <div class="col-xs-4">
                                                        <div class="switch-style">
                                                            <label style="switch-lbl">
                                                                <input type="checkbox" class="no-show" id="varian" name="varian" checked> 
                                                                <div class="switch switchOn">
                                                                    <span class="varian-label-on" id='label-status-variant'>Variant</span>
                                                                </div>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <em class="pull-right"><?php echo IMAGE_PRODUCT_INFO ?></em>
                                                    <div class="col-xs-12" id="tipevarian">Varian produk tidak tersedia</div>
                                                </div>

                                                <div id="allvar" <?php echo $data_sel[LIST_DATA][3][0]->variant_value_seq != "1" ? "style='display:none;'" : "" ?>>
                                                    <table class="table table-striped">
                                                        <?php echo str_ireplace('{vartext}', 'Info Produk', str_ireplace('{varval}', '1', str_ireplace('{dtimg}', '0', $fromvariant))) . '</tr>'; ?>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div id="variasi" <?php echo $data_sel[LIST_DATA][3][0]->variant_value_seq == "1" ? "style='display:none;'" : "" ?>>
                                                    <?php echo $tabledata1 ?>
                                                </div>
                                                <?php
                                            }
                                        }
                                        ?>
                                    <?php } ?>                                      
                                </section>
                            </div>
                        </div>
                        <div id="testing"></div>
                        <div class ="form-group">
                            <?php
                            if ($data_auth[FORM_ACTION] == ACTION_EDIT) {
                                ?>
                                <input type="hidden" name="<?php echo CONTROL_SAVE_EDIT_NAME; ?>">
                                <?php
                            } else {
                                ?>
                                <input type="hidden" name="<?php echo CONTROL_SAVE_ADD_NAME; ?>">
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div> 
            </form>
            <?php
            $dataatribute = '';
            if (isset($data_sel[LIST_DATA][1])) {
                foreach ($data_sel[LIST_DATA][1] as $each) {
                    $dataatribute.='{' . $each->attribute_value_seq . '}';
                }
            }
            ?>
            <script type="text/javascript">
                    $(document).ready(function () {
                        $('.switch').click(function () {
                            var kondisi = $('#varian').prop('checked');
                            if (kondisi == false) {
                                cekvarian(1);
                                $('#label-status-variant').text('');
                                $('#label-status-variant').text('Varian');
                                $('#label-status-variant').removeClass('varian-label-off');
                                $('#label-status-variant').addClass('varian-label-on');
                            } else {
                                cekvarian(0);
                                $('#label-status-variant').text('');
                                $('#label-status-variant').text('Non Varian');
                                $('#label-status-variant').removeClass('varian-label-on');
                                $('#label-status-variant').addClass('varian-label-off');
                            }
                            $(this).toggleClass('switchOn')
                        });
                    });
                    var stepStatus = 0;
                    var icount = <?php echo $dtimg; ?>;
                    var ivariant = [];
                    var number = 0;
                    var image = 0;
                    var url = "<?php echo get_base_url() . $data_auth[FORM_URL]; ?>";
                    var form = $("#frmMain").show();
                    $(".tab-product").steps({
                        headerTag: "h3",
                        bodyTag: "section",
                        transitionEffect: "fade",
                        clearFixCssClass: "no-padding-left",
                        autoFocus: true,
    <?php
    if ($data_auth[FORM_ACTION] == ACTION_ADD && !isset($data_sel[LIST_DATA])) {
        echo "enableAllSteps: false,";
    } else {
        echo "enableAllSteps: true,";
    }
    ?>
                        saveState: true,
                        labels: {
                            finish: "Simpan",
                            next: "Lanjutkan",
                            previous: "Kembali",
                            loading: "Loading ..."
                        },
                        onStepChanging: function (event, currentIndex, newIndex)
                        {
                            if (currentIndex === 0) {
                                if (stepStatus === 0) {
                                    var requireOne = $("#name").val();
                                    var requireTwo = $("input[type=radio]:checked", "#frmMain").val();
                                    if (requireOne != '' && requireTwo != undefined) {
                                        var r = confirm("Data kategori tidak dapat dirubah kembali. Lanjutkan ?");
                                        if (r) {
                                            stepStatus = 1;
                                            return true;
                                        } else {
                                            return false;
                                        }
                                    }
                                }
                            }

                            if (currentIndex > newIndex)
                            {
                                $(".radiobtn").attr("disabled", true);
                                $('#lock-box').removeClass('no-show');
                                $('#lock-box').click(function () {
                                    alert('Kategori produk sudah tidak bisa di ubah.')
                                })
                                return true;
                            }
                            if (newIndex === 0)
                            {
                                return false;
                            }
                            if (currentIndex < newIndex)
                            {
                                form.find(".body:eq(" + newIndex + ") label.error").remove();
                                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
                            }
                            form.validate().settings.ignore = ":disabled,:hidden";
                            return form.valid();
                        },
                        onStepChanged: function (event, currentIndex, priorIndex)
                        {
                            if (currentIndex === 3) {
                                $('#variasi .box-variant').each(function () {
                                    var value = $(this).attr('id');
                                    $("#variant-" + value).prop("checked", true);

                                })
                            }


                        },
                        onFinishing: function (event, currentIndex)
                        {
                            form.validate().settings.ignore = ":disabled";
                            return form.valid();
                        },
                        onFinished: function (event, currentIndex)
                        {
                            form.submit();
                        }
                    });
                    CKEDITOR.replace('description');
                    CKEDITOR.replace('content');
                    CKEDITOR.replace('specification');
                    $('.slim').slim({
                        ratio: '1:1',
                        minSize: {
                            width: 850,
                            height: 850,
                        },
                        maxFileSize: "2",
                        onRemove: function (slim, data) {
                            $.each(slim._originalElementAttributes, function (key, val) {
                                if (val.name == "number") {
                                    number = val.value;
                                } else if (val.name == "image") {
                                    image = val.value
                                }
                            })
                            $("#oldfile" + number + "_" + image).val("");
                        }

                    })

                    function cProcat() {
                        $("#procat").html("");
                        $.ajax({
                            url: url,
                            data: {btnAdditional: "act_s_adt", tipe: "category"
                            },
                            type: "POST",
                            success: function (response) {
                                if (isSessionExpired(response)) {
                                    response_object = json_decode(response);
                                    url = response_object.url;
                                    location.href = url;
                                } else {
                                    $("#procat").html(response);
                                }
                            },
                            error: function (request, error) {
                                alert(error_response(request.status));
                            }
                        });
                    }
    <?php
    if (!isset($data_sel[LIST_DATA])) {?>
                        var notconfirm = true;
                        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                            var target = $(e.target).attr("href");
                            if (notconfirm) {
                                var r = confirm("Data kategori tidak dapat dirubah kembali. Lanjutkan ?");
                                if (r == true) {
                                    $(".radiobtn").attr("disabled", true);
                                    notconfirm = false;
                                } else {
                                    $('#myTab a:first').tab('show');
                                }
                            }

                        });
                        cProcat();
    <?php } ?>
                    function cekseq(idm) {
                        $("#catseqval").val($('input[name=catseq]:checked', '#frmMain').val());
                        $.ajax({
                            url: url,
                            data: {btnAdditional: "act_s_adt", idh: idm, tipe: "breadcrumb"
                            },
                            type: "POST",
                            success: function (response) {
                                if (isSessionExpired(response)) {
                                    response_object = json_decode(response);
                                    url = response_object.url;
                                    location.href = url;
                                } else {
                                    $("#spek").html(response);
                                    cekatribute();
                                }
                            },
                            error: function (request, error) {
                                alert(error_response(request.status));
                            }
                        });
                    }
                    function cekatribute() {
                        var idm = $("#alcat").val();
                        var nilaiarray = "<?php echo $dataatribute; ?>";
                        $.ajax({
                            url: url,
                            data: {btnAdditional: "act_s_adt", idh: idm, atrval: nilaiarray, tipe: "attribute"
                            },
                            type: "POST",
                            success: function (response) {
                                if (isSessionExpired(response)) {
                                    response_object = json_decode(response);
                                    url = response_object.url;
                                    location.href = url;
                                } else {
                                    $("#spesifikasi").html(response);
                                }
                            },
                            error: function (request, error) {
                                alert(error_response(request.status));
                            }
                        });
                    }
                    $("#addCF").click(function () {
                        $("#customFields").append('<tr valign="top"><td><input type="text" class="form-control" id="spekname" maxlength="50" name="spekname[]" value="" /></td>\n\<td><input type="text" class="form-control" id="spekval" maxlength="50" name="spekval[]" value="" /></td><td><a href="javascript:void(0);" class="remCF btn btn-danger btn-sm">Hapus</a></td></tr>');
                    });
                    $("#customFields").on('click', '.remCF', function () {
                        $(this).parent().parent().remove();
                    });
                    function getvarianttitle() {
                        var idm = $("#alcat").val();
                        $.ajax({url: url,
                            data: {btnAdditional: "act_s_adt", idh: idm, tipe: "variant"},
                            type: "POST",
                            success: function (response) {
                                if (isSessionExpired(response)) {
                                    response_object = json_decode(response);
                                    url = response_object.url;
                                    location.href = url;
                                } else {
                                    $("#tipevarian").html(response);
                                }
                            },
                            error: function (request, error) {
                                alert(error_response(request.status));
                            }
                        });
                    }
                    function cekvarian(nilai) {
                        if (nilai == 0) {
                            $("#tipevarian").hide();
                            $("#allvar").show();
                            $("#variasi").hide();
                        } else {
                            $("#allvar").hide();
                            $("#tipevarian").show();
    <?php echo isset($data_sel[LIST_DATA][3][0]->variant_value_seq) && $data_sel[LIST_DATA][3][0]->variant_value_seq != '1' ? '$("#variasi").show("slow");' : ''; ?>
                            getvarianttitle();
                        }
                    }


                    function addVariant(vartext, varval, id) {
                        if ($(id).is(":checked")) {
                            $("#variasi").show();
                            icount = (icount + 1);
                            if ($('#' + varval).length) {
                                alert("Data sudah ada");
                                return;
                            } else {
                                ivariant.push(varval);
                            }
                            var str = '<?php echo str_replace("\r", "", str_replace("\n", "", $fromvariant)) ?>';
                            var newdata = str.replace(/{vartext}/g, vartext);
                            newdata = newdata.replace(/{varval}/g, varval);
                            newdata = newdata.replace(/{dtimg}/g, icount);
                            $("#variasi").append(newdata);
                            $('.auto').autoNumeric('init', {vMin: 0});
                            $('.auto_int').autoNumeric('init', {vMin: 0, mDec: 0});
                            $('.auto_dec').autoNumeric('init', {vMin: 0, mDec: 2});
                            $('body').animate({
                                scrollTop: $("#" + varval).offset().top - 70
                            }, 1000);
                            $('#' + varval + ' .slim').slim();
                        } else {
                            remove(varval);
                        }
                    }

                    function adcvclick() {
                        $("#variasi").show();
                        icount = (icount + 1);
                        var vartext = $("#varianval option:selected").text();
                        var varval = $("#varianval").val();
                        if ($('#' + varval).length) {
                            alert("Data sudah ada");
                            return;
                        } else {
                            ivariant.push(varval);
                        }
                        var str = '<?php echo str_replace("\r", "", str_replace("\n", "", $fromvariant)) ?>';
                        var newdata = str.replace(/{vartext}/g, vartext);
                        newdata = newdata.replace(/{varval}/g, varval);
                        newdata = newdata.replace(/{dtimg}/g, icount);
                        $("#variasi").append(newdata);
                        $('.auto').autoNumeric('init', {vMin: 0});
                        $('.auto_int').autoNumeric('init', {vMin: 0, mDec: 0});
                        $('.auto_dec').autoNumeric('init', {vMin: 0, mDec: 2});
                        $('body').animate({
                            scrollTop: $("#" + varval).offset().top - 70
                        }, 1000);
                        $('#' + varval + ' .slim').slim();
                    }

                    $("#customVarian").on('click', '.remCV', function () {
                        var href = $(this).attr('title');
                        var index = ivariant.indexOf(href);
                        if (index > -1) {
                            ivariant.splice(index, 1);
                        }
                        $(this).parent().parent().parent().parent().remove();
                        return false;
                    });


                    function remove(id) {

                        var yesOrNo = confirm("Apakah anda yakin menghapus data yang dipilih ?");
                        if (yesOrNo == true) {
                            $('#' + id).remove();
                            $('#variant-' + id).prop('checked', false);
                        } else {
                            $('#variant-' + id).prop('checked', true);
                        }
                    }

                    function cekdiskon(hargap, hargaj, iddisk) {
                        if ($.isNumeric(hargap) && $.isNumeric(hargaj)) {
                            var nilaidiskon = (hargap - hargaj) / hargap * 100;
                            console.log(nilaidiskon);
                            if (nilaidiskon > 0) {
                                iddisk.val(Math.round(nilaidiskon));
                            }else{
                                iddisk.val(Math.round(0));
                            }
                        }
                    }

                    function prodpc(idproc, nomor) {
                        var $this = $(idproc);
                        var salep = $('#sell_price' + nomor).autoNumeric('get');
                        var prodp = $this.autoNumeric('get');						
                        cekdiskon(prodp, salep, $('#disc_percent' + nomor));
                    }
                    function salepc(idproc, nomor) {
                        var $this = $(idproc);
                        var prodp = $('#product_price' + nomor).autoNumeric('get');
                        var salep = $this.autoNumeric('get');
                        cekdiskon(prodp, salep, $('#disc_percent' + nomor));
                    }

    <?php
    if (isset($data_sel[LIST_DATA])) {
        if (isset($ivariant)) {
            foreach ($ivariant as $datavariant) {
                echo "ivariant.push('" . $datavariant . "');";
            }
        } else {
            
        }
        ?>
                        $('#t2').removeClass('hidden');
                        $('#t3').removeClass('hidden');
                        $('#t4').removeClass('hidden');
                        $("#catseq").prop("disabled", true);
                        $.ajax({
                            url: url,
                            data: {btnAdditional: "act_s_adt", idh: '<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->category_ln_seq : ""); ?>', tipe: "breadcrumb"
                            },
                            type: "POST",
                            success: function (response) {
                                if (isSessionExpired(response)) {
                                    response_object = json_decode(response);
                                    url = response_object.url;
                                    location.href = url;
                                } else {
                                    $("#procat").html(response);
                                    cekatribute();
                                    getvarianttitle();
                                }
                            },
                            error: function (request, error) {
                                alert(error_response(request.status));
                            }
                        });
                        $(document).ready(function () {
                            $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                                localStorage.setItem('activeTab', $(e.target).attr('href'));
                            });
                            var activeTab = localStorage.getItem('activeTab');
                            if (activeTab) {
                                $('#myTab a[href="' + activeTab + '"]').tab('show');
                            }
                        });
        <?php
    }
    ?>
            </script>
        <?php } else { ?>
            <div class="box-body">
                <div id="header">
                    <div id="sub-navbar" class="box-body sub-header">
                        <div class="panel-body no-pad">
                            <div class="row">
                                <div class="col-xs-4">
                                    <form action ="<?php echo base_url($data_auth[FORM_URL]) ?>" method="get">   
                                        <div class="input-group">
                                            <input type="text" name="search" class="form-control src-input" placeholder="<?php echo empty(htmlspecialchars($this->input->get('search'))) ? "Cari Produk" : "Pencarian : " . htmlspecialchars($this->input->get('search')) ?>" value="<?php echo $this->input->get('search') ?>"> 
                                            <span class="input-group-btn">
                                                <button class="btn btn-danger" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                            </span>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <br>
                            <div class="row no-pad">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-9">
                                            <?php $codeActive = $this->input->get('status'); ?>
                                            <?php for ($i = 0; $i < count($data_sel[LIST_DATA][0]); $i++) { ?>  
                                                <a class="btn btn-flat <?php echo (isset($data_sel[LIST_DATA][0][$i]) && ($data_sel[LIST_DATA][0][$i]->status_cd === $codeActive || (!isset($codeActive) && $i == 0)) ? 'btn-active' : '' ); ?>" href="<?php echo base_url($data_auth[FORM_URL]) . "?status=" . $data_sel[LIST_DATA][0][$i]->status_cd . $filter . $filter_sort ?>" ><?php echo $data_sel[LIST_DATA][0][$i]->status ?> (<?php echo number_format($data_sel[LIST_DATA][0][$i]->total) ?>)</a> &nbsp&nbsp&nbsp&nbsp  &nbsp&nbsp&nbsp                        
                                            <?php } ?>
                                        </div>
                                        <div class="col-xs-3">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                </div>
                                                <?php echo get_order_filter("created_date", "name", base_url($data_auth[FORM_URL]) . $filter_status . $filter, $this->input->get("sort"), $this->input->get("column_sort")) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 no-padding m-top-bottom">
                    <a href="<?php echo base_url($data_auth[FORM_URL]) . "?act=" . ACTION_ADD ?>" class="btn btn-success btn-flat pull-left">
                        <i class="fa fa-plus"></i>&nbsp;Ajukan Produk Baru
                    </a>
                </div>
                <?php
                $old_product_seq = "";
                if (isset($data_sel[LIST_DATA][1])) {
                    ?> 
                    <div class="col-xs-12 no-padding">    
                        <?php
                        foreach ($data_sel[LIST_DATA][1] as $merchant) {
                            if ($merchant->seq != $old_product_seq) {
                                if ($old_product_seq != "") {
                                    ?>
                                    </tbody>
                                    </table>    

                                </div>
                            </div>
                        </div>
                    <?php } $old_product_seq = $merchant->seq;
                    ?>

                    <div class="box box-no-border-top no-padding">
                        <div class="box-header">
                            <?php echo $merchant->name ?>
                        </div>
                        <div class="box-body">
                            <div class="col-xs-3">
                                <div class="col-xs-12">
                                    <img src="<?php echo TEMP_PRODUCT_UPLOAD_IMAGE . $merchant->merchant_seq . "/" . $merchant->pic_1_img; ?>" style="height: 175px;width: 175px;">
                                </div>
                                <div class="col-xs-12">
                                    <label>Status : <?php echo $merchant->status === "N" ? "<span class='label label-success'>Baru</span>" : "<span class='label label-warning'>Tolak</span>" ?></label>
                                </div>
                                <div class="col-xs-12">
                                    <label>Berat: <?php echo $merchant->p_weight_kg ?></label>
                                </div>
                                <div class="col-xs-12">
                                    <label>Tanggal Pengajuan:  <?php echo date("d-M-Y", strtotime($merchant->created_date)) ?></label>
                                </div>
                                <div class="col-xs-12">
                                    <a href="<?php echo base_url($data_auth[FORM_URL]) . "?" . CONTROL_GET_TYPE . "=" . ACTION_EDIT . "&key=" . $merchant->seq ?> " class="btn btn-xs btn-pinterest">Ubah Product</a>
                                </div>
                            </div>                

                            <div class="col-xs-9">
                                <table class="table table-bordered" id="test">
                                    <thead class="table-head">
                                    <td width="100px">Variasi</td>                        
                                    <td width="150px">SKU</td>
                                    <td class="aRight" width="50px">Stock</td>
                                    <td class="aRight" width="100px">Harga Jual (Rp)</td>
                                    <td class="aRight" width="100px">Harga Promo (Rp)</td>
                                    </thead>
                                    <tbody>
                                    <?php } ?>
                                    <tr class="font-size-12px">
                                        <td><?php echo $merchant->variant_value ?></td>
                                        <td><?php echo $merchant->merchant_sku != "" ? $merchant->merchant_sku : " - " ?></td>
                                        <td class="aRight"><?php echo $merchant->stock != "" || $merchant->stock != 0 ? $merchant->stock : " 0 " ?></td>
                                        <td class="aRight"><?php echo $merchant->product_price != "" ? number_format($merchant->product_price) : " - " ?></td>
                                        <td class="aRight"><?php echo $merchant->sell_price != "" ? number_format($merchant->sell_price) : " - " ?></td>

                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    <?php } else {
        ?>
        <div class="col-xs-12 no-padding">     
            <div class="box box-no-border-top">
                <div class="box-body min-height300">
                    <?php $nameProductActive = htmlspecialchars($this->input->get('search')); ?>
                    <center class="no-found-src" ><?php echo isset($nameProductActive) && $nameProductActive != '' ? 'Tidak ditemukan produk dengan hasil pencarian : <b>' . $nameProductActive . '</b>' : 'Produk Tidak Ditemukan'; ?></center>
                </div>
            </div>
        </div>
        <?php
    }
    ?>

    <?php
}
?>



<div class="col-xs-12 no-padding">
    <div class="box-pagging">
        <center>
            <?php echo $this->pagination->create_links(); ?>
        </center>
    </div><br>
</div>
</div>
</div>
</div>
<div class="clearfix"></div>
<?php
require_once get_include_page_list_merchant_content_footer();
require_once get_include_content_merchant_bottom_page_navigation();
?>