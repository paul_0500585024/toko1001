<?php
require_once VIEW_BASE_MERCHANT;
require_once get_include_content_merchant_top_page_navigation();
echo $breadcrumb;
?>
<?php if ($data_auth[FORM_ACTION] == ACTION_ADD OR $data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
    <?php require_once get_component_url() . '/dropdown/function_drop_down.php'; ?>
    <div class="row reset-margin">
        <div class="col-xs-12">    
            <div class="box box-no-border-top p-10px no-border-radius m-top5">
                <br>
                <form class ="" id="frmMain" onsubmit ="return validate_form();" method ="post" action= "<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
                    <div class="box-body">
                        <section class="col-xs-12">
                            <?php echo get_csrf_merchant_token(); ?>
                            <?php if ($data_auth[FORM_ACTION] == ACTION_EDIT) { ?>
                                <input class="form-control" name="old_status" type="hidden" value="<?php echo(isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->status : "") ?>">
                                <input class="form-control" name="old_created_date" type="hidden" value="<?php echo(isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->created_date : "") ?>">
                                <input class="form-control" name="old_modified_date" type="hidden" value="<?php echo(isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->modified_date : "") ?>">
                            <?php } ?>
                            <!--===START SECTION 1==-->
                            <div class="row">
                                <div class="box bg-custom-border no-border-radius">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <h5 class="box-title title-form">Informasi Pengajuan</h5>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-4">
                                                <div class ="form-group">
                                                    <label>Status</label>
                                                    <input class="form-control" name="s_status" type="text" value="<?php echo status($data_sel[LIST_DATA][0]->status, (STATUS_ANRL)); ?>" readonly >
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class ="form-group">
                                                    <label class ="">Tanggal Pengajuan</label>
                                                    <input class="form-control" name="s_tgl" type="text" value="<?php echo c_date($data_sel[LIST_DATA][0]->created_date, 1) ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class ="form-group">
                                                    <label>Tanggal Verifikasi</label>
                                                    <input class="form-control" name="s_tgl_k" value="<?php echo c_date($data_sel[LIST_DATA][0]->modified_date, 1) ?>" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--==START SECTION 2==-->
                            <br><br>
                            <div class="row">
                                <div class="box bg-custom-border no-border-radius">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <h5 class="box-title title-form">INFORMASI ALAMAT</h5>
                                            </div>
                                        </div>
                                        <div class="row">

                                            <div class="col-xs-12">
                                                <div class ="form-group col-xs-6">
                                                    <label>Nama PIC <span class="require">*</span></label>
                                                    <input class="form-control" maxlength="50" validate ="required[]" id="pic1_name" name="pic1_name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->pic1_name) : "") ?>">
                                                </div>
                                                <div class ="form-group col-xs-6">
                                                    <label>Telp PIC</label>
                                                    <input class="form-control" maxlength="50" id="pic1_phone_no" name="pic1_phone_no" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->pic1_phone_no : "") ?>">
                                                </div>
                                            </div>

                                            <div class="col-xs-12">
                                                <div class ="form-group col-xs-6">
                                                    <label>Nama Finance</label>
                                                    <input class="form-control" maxlength="50" id="pic2_name" name="pic2_name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->pic2_name) : "") ?>">
                                                </div>
                                                <div class ="form-group col-xs-6">
                                                    <label class ="control-label">Telp Finance</label>
                                                    <input class="form-control" maxlength="50" id="pic2_phone_no" name="pic2_phone_no" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->pic2_phone_no) : "") ?>">
                                                </div>
                                            </div>

                                            <div class="col-xs-12">
                                                <div class ="form-group col-xs-6">
                                                    <label>No. Telp <span class="require">*</span></label>
                                                    <input class="form-control" data-inputmask="&quot;mask&quot;: &quot;9999 9999 9999&quot;" data-mask="" validate ="required[]" id="phone_no" name="phone_no" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->phone_no) : "") ?>">
                                                </div>
                                                <div class ="form-group col-xs-6">
                                                    <label>No. Fax</label>
                                                    <input class="form-control" maxlength="50" id="fax_no" name="fax_no" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->fax_no) : "") ?>">
                                                </div>
                                            </div>

                                            <div class="col-xs-12">
                                                <div class ="form-group">
                                                    <label>Alamat <span class="require">*</span></label>
                                                    <textarea onblur="cekpr('1')" class="form-control" validate ="required[]" id="address" name="address" style="overflow:auto;resize:none" rows="3"><?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->address) : "") ?></textarea>
                                                </div>
                                            </div>

                                            <div class="col-xs-12">

                                                <div class="form-group col-xs-3">
                                                    <label>Propinsi <span class="require">*</span></label>
                                                    <?php echo drop_down('propinsi', $province_name, '', 'validate ="required[]" onchange="cData(this.value, \'city\', \'city\');"'); ?>
                                                </div>

                                                <div class ="form-group col-xs-3">
                                                    <label>Kota <span class="require">*</span></label>
                                                    <?php echo drop_down('city', '', '', 'validate ="required[]" onchange="cData(this.value, \'district_seq\', \'district\');"'); ?>
                                                </div>

                                                <div class ="form-group col-xs-3">
                                                    <label>Kecamatan <span class="require">*</span></label>
                                                    <?php echo drop_down('district_seq', '', '', 'validate ="required[]"  onchange="cekpr(\'2\')"'); ?>
                                                </div>
                                                <div class ="form-group col-xs-3">
                                                    <label>Kode Pos</label>
                                                    <input onblur="cekpr('3')" class="form-control" maxlength="10" id="zip_code" name="zip_code" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->zip_code) : "") ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            </div>
                            <br>
                            <br>

                            <div class="row">
                                <div class="box bg-custom-border no-border-radius">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <h5 class="box-title title-form">Informasi Ekspedisi</h5>
                                            </div>
                                        </div>
                                        <div class="row">

                                            <div class="col-xs-12">
                                                <div class ="form-group col-xs-12">
                                                    <label>Info Pengambilan <span class="require">*</span></label>
                                                    <textarea class="form-control" style="overflow:auto;resize:none" rows="3" id="pickup_addr" name="pickup_addr"><?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->pickup_addr) : "") ?></textarea>
                                                </div>
                                            </div>

                                            <div class="col-xs-12">
                                                <div class="form-group col-xs-3">
                                                    <label>Propinsi</label>
                                                    <?php echo drop_down('pickup_propinsi', $province_name, '', 'onchange="cData(this.value, \'city\', \'city\',\'pickup_\');"'); ?>
                                                </div>
                                                <div class ="form-group col-xs-3">
                                                    <label>Kota</label>
                                                    <?php echo drop_down('pickup_city', '', '', 'onchange="cData(this.value, \'district_seq\', \'district\',\'pickup_\');"'); ?>
                                                </div>
                                                <div class ="form-group col-xs-3">
                                                    <label>Kecamatan <span class="require">*</span></label>
                                                    <?php echo drop_down('pickup_district_seq'); ?>
                                                </div>
                                                <div class ="form-group col-xs-3">
                                                    <label>Kode Pos Pengambilan</label>
                                                    <input maxlength="10" class="form-control" id="pickup_zip_code" name="pickup_zip_code" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->pickup_zip_code) : "") ?>">
                                                </div>
                                            </div>
                                            <div class="col-xs-12">
                                                <label class="col-xs-4 col-xs-offset-8 text-right" style="padding-right:35px;padding-top: 10px; ">
                                                    <input class='pull-right' type="checkbox" id ="pickup_addr_eq" name ="pickup_addr_eq" <?php echo (isset($data_sel[LIST_DATA]) ? (($data_sel[LIST_DATA][0]->pickup_addr_eq == "1" OR $data_sel[LIST_DATA][0]->pickup_addr_eq == "on") ? "checked" : "") : "checked") ?> onclick="aSama(this.checked, 'pickup')" /> 
                                                    Sama dengan alamat&nbsp;&nbsp;
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br><br>
                            <div class="row">
                                <div class="box bg-custom-border no-border-radius">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <h5 class="box-title title-form">Informasi Retur</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class ="form-group col-xs-12">
                                                    <label>Alamat Pengembalian <span class="require">*</span></label>
                                                    <textarea class="form-control" style="overflow:auto;resize:none" rows="3" id="return_addr" name="return_addr"><?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->return_addr) : "") ?></textarea>
                                                </div>
                                            </div>
                                            <div class="col-xs-12">
                                                <div class="form-group col-xs-3">
                                                    <label>Propinsi</label>
                                                    <?php echo drop_down('return_propinsi', $province_name, '', 'onchange="cData(this.value, \'city\', \'city\',\'return_\');"'); ?>
                                                </div>
                                                <div class ="form-group col-xs-3">
                                                    <label>Kota</label>
                                                    <?php echo drop_down('return_city', '', '', 'onchange="cData(this.value, \'district_seq\', \'district\',\'return_\');"'); ?>
                                                </div>
                                                <div class ="form-group col-xs-3">
                                                    <label>Kecamatan <span class="require">*</span></label>
                                                    <?php echo drop_down('return_district_seq'); ?>
                                                </div>
                                                <div class ="form-group col-xs-3">
                                                    <label>Kode Pos Pengembalian</label>
                                                    <input class="form-control" maxlength="10" id="return_zip_code" name="return_zip_code" type="text" value ="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA][0]->return_zip_code) : "") ?>">
                                                </div>
                                            </div>
                                            <div class="col-xs-12">
                                                <label class="col-xs-4 col-xs-offset-8 text-right" style="padding-right:35px;padding-top: 10px; ">
                                                    <input type="checkbox" id ="return_addr_eq" name ="return_addr_eq" <?php echo (isset($data_sel[LIST_DATA]) ? (($data_sel[LIST_DATA][0]->return_addr_eq == "1" OR $data_sel[LIST_DATA][0]->return_addr_eq == "on") ? "checked" : "") : "checked") ?>  onclick="aSama(this.checked, 'return')"  /> 
                                                    Sama dengan alamat&nbsp;&nbsp;
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br><br>
                            <div class="row">
                                <div class="box bg-custom-border no-border-radius">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <h5 class="box-title title-form">Informasi Bank</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 no-pad">
                                                <div class="form-group col-xs-3 no-margin">
                                                    <label class ="control-label col-xs-12">Nama Bank <span class="require">*</span></label>
                                                    <div class="col-xs-12">
                                                        <input class="form-control" validate ="required[]" name="bank_name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->bank_name : "") ?>"maxlength="25">
                                                    </div>
                                                </div>
                                                <div class="form-group col-xs-3 no-margin">
                                                    <label class ="col-xs-12">Cabang <span class="require">*</span></label>
                                                    <div class="col-xs-12">
                                                        <input class="form-control" validate ="required[]" name="bank_branch_name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->bank_branch_name : "") ?>" maxlength="50">
                                                    </div>
                                                </div>
                                                <div class="form-group col-xs-3 no-margin">
                                                    <label class ="col-xs-12">No.Rekening <span class="require">*</span></label>
                                                    <div class="col-xs-12">
                                                        <input class="form-control" validate ="required[]" name="bank_acct_no" type="text"  value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->bank_acct_no : "") ?>"  maxlength="50">
                                                    </div>
                                                </div>
                                                <div class="form-group col-xs-3 no-margin">
                                                    <label class ="col-xs-12">Rekening A/N <span class="require">*</span></label>
                                                    <div class="col-xs-12">
                                                        <input class="form-control" validate ="required[]" name="bank_acct_name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0]) ? $data_sel[LIST_DATA][0]->bank_acct_name : "") ?>"  maxlength="50">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class ="form-group col-xs-12 no-pad">
                        <div class ="col-xs-3 col-xs-offset-9 no-pad"><?php echo get_save_edit_button(); ?> </div>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
    </div>
    <script type="text/javascript">
        $("[data-mask]").inputmask();
        function cekpr($iddata) {
            var eqadr_p = '';
            var eqadr_r = '';
            if ($("#pickup_addr_eq").is(':checked'))
                eqadr_p = '1';
            if ($("#return_addr_eq").is(':checked'))
                eqadr_r = '1';

            if ($iddata == '1' && eqadr_p == '1')
                $('#pickup_addr').val($('#address').val());
            if ($iddata == '1' && eqadr_r == '1')
                $('#return_addr').val($('#address').val());

            if ($iddata == '2' && eqadr_p == '1') {
                if ($('#district_seq').val() != '')
                    cekdata($('#district_seq').val(), 'pickup_');
            }
            if ($iddata == '2' && eqadr_r == '1') {
                if ($('#district_seq').val() != '')
                    setTimeout(function () {
                        cekdata($('#district_seq').val(), 'return_');
                    }, 2000)
            }
            if ($iddata == '3' && eqadr_p == '1')
                $('#pickup_zip_code').val($('#zip_code').val());
            if ($iddata == '3' && eqadr_r == '1')
                $('#return_zip_code').val($('#zip_code').val());

        }

        function aSama(nilai, tab)
        {
            if (nilai == true || nilai == "1") {
                $("#" + tab + "_addr").prop('disabled', true);
                $("#" + tab + "_district_seq").prop('disabled', true);
                $("#" + tab + "_zip_code").prop('disabled', true);
                $("#" + tab + "_propinsi").prop('disabled', true);
                $("#" + tab + "_city").prop('disabled', true);
                $("#" + tab + "_district_seq").prop('required', false);
                $("#" + tab + "_addr").prop('required', false);
            } else {
                $("#" + tab + "_addr").prop('disabled', false);
                $("#" + tab + "_district_seq").prop('disabled', false);
                $("#" + tab + "_zip_code").prop('disabled', false);
                $("#" + tab + "_propinsi").prop('disabled', false);
                $("#" + tab + "_city").prop('disabled', false);
                $("#" + tab + "_district_seq").prop('required', true);
                $("#" + tab + "_addr").prop('required', true);
            }

        }
        var url = "<?php echo get_base_url() . $data_auth[FORM_URL]; ?>";
        function cData(idcombo, iddtl, jenis, grup, nilai) {
            grup = grup || "";
            nilai = nilai || "";
            var list = $("#" + grup + iddtl);
            if (jenis == 'city')
                $("#" + grup + "district_seq").empty();
            inHTML = "";
            list.empty();
            if (idcombo != '') {
                $.ajax({
                    url: url,
                    data: {
                        btnAdditional: "act_s_adt", idh: idcombo, tipe: jenis
                    },
                    type: "POST",
                    success: function (response) {
                        if (isSessionExpired(response)) {
                            response_object = json_decode(response);
                            url = response_object.url;
                            location.href = url;
                        } else {
                            if (nilai != '') {
                                inHTML += '<option value="">-- Pilih --</option>';
                            } else {
                                inHTML += '<option value="" selected>-- Pilih --</option>';
                            }
                            $.each($.parseJSON(response), function () {
                                if (nilai != '') {
                                    selected = "";
                                    if (nilai == this.seq)
                                        selected = " selected";
                                    inHTML += '<option value="' + this.seq + '"' + selected + '>' + this.name + '</option>';
                                } else {
                                    inHTML += '<option value="' + this.seq + '">' + this.name + '</option>';
                                }
                            });
                            list.append(inHTML);
                            if (nilai == '')
                                list.select2("val", "");
                        }
                    },
                    error: function (request, error) {
                        alert(error_response(request.status));
                    }
                });
            }
        }
    <?php if ($data_auth[FORM_ACTION] == ACTION_ADD) { ?>
            function cEmail() {
                var idemail = $("#email").val();
                if (idemail == '') {
                    return false;
                } else {
                    $("#emailerror").html("");
                }
                $.ajax({
                    url: url,
                    data: {
                        btnAdditional: "act_s_adt", idh: idemail, tipe: "email"
                    },
                    type: "POST",
                    success: function (data) {
                        if (isSessionExpired(data)) {
                            response_object = json_decode(data);
                            url = response_object.url;
                            location.href = url;
                        } else {
                            if (data.trim() != "OK") {
                                $("#emailerror").html("Email sudah terdaftar, harap memakai email yang lain !");
                                $("#email").focus();
                            } else {
                                $("#emailerror").html("");
                            }
                        }
                    },
                    error: function (request, error) {
                        alert(error_response(request.status));
                    }
                });
            }
            function cekdata(dtdistrict, grup) {
                grup = grup || "";
                var idcity = grup + "city";
                var iddistrict = grup + "district_seq";
                if (dtdistrict != '') {
                    $.ajax({
                        url: url,
                        data: {
                            btnAdditional: "act_s_adt", idh: dtdistrict, tipe: 'valpcd'
                        },
                        type: "POST",
                        success: function (response) {
                            if (isSessionExpired(response)) {
                                response_object = json_decode(response);
                                url = response_object.url;
                                location.href = url;
                            } else {
                                $.each($.parseJSON(response), function () {
                                    idp = this.idp;
                                    idc = this.idc;
                                    idd = this.idd;
                                });
                                $("#" + grup + "propinsi").select2("val", idp);
                                setTimeout(function () {
                                    filldata(idp, idcity, 'city', idc);
                                }, 500);
                                setTimeout(function () {
                                    filldata(idc, iddistrict, 'district', idd);
                                }, 800);
                            }
                        },
                        error: function (request, error) {
                            alert(error_response(request.status));
                        }
                    });
                }
            }

            function filldata(idcombo, iddtl, jenis, nilai) {
                nilai = nilai || "";
                var list = $("#" + iddtl);
                inHTML = "";
                list.empty();
                if (idcombo != '') {
                    $.ajax({
                        url: url,
                        data: {
                            btnAdditional: "act_s_adt", idh: idcombo, tipe: jenis
                        },
                        type: "POST",
                        success: function (response) {
                            if (isSessionExpired(response)) {
                                response_object = json_decode(response);
                                url = response_object.url;
                                location.href = url;
                            } else {
                                inHTML += "<option value=''>-- Pilih --</option>";
                                $.each($.parseJSON(response), function () {
                                    if (nilai != '') {
                                        selected = "";
                                        if (nilai == this.seq)
                                            selected = " selected";
                                        inHTML += '<option value="' + this.seq + '"' + selected + '>' + this.name + '</option>';
                                    } else {
                                        inHTML += '<option value="' + this.seq + '">' + this.name + '</option>';
                                    }
                                });
                                list.append(inHTML);
                                list.select2("val", nilai);
                            }
                        },
                        error: function (request, error) {
                            alert(error_response(request.status));
                        }
                    });
                }
            }
        <?php if (isset($data_sel[LIST_DATA])) { ?>

                setTimeout(function () {
                    aSama(<?php echo (($data_sel[LIST_DATA][0]->pickup_addr_eq == "1" || $data_sel[LIST_DATA][0]->pickup_addr_eq == "on") ? "'1'" : "''") ?>, 'pickup');
                }, 500);

                setTimeout(function () {
                    aSama(<?php echo (($data_sel[LIST_DATA][0]->return_addr_eq == "1" || $data_sel[LIST_DATA][0]->return_addr_eq == "on") ? "'1'" : "''") ?>, 'return');
                }, 1000);

            <?php
            echo "cekdata('" . $data_sel[LIST_DATA][0]->district_seq . "');";

//if ($data_sel[LIST_DATA][0]->pickup_addr_eq == "1" || $data_sel[LIST_DATA][0]->pickup_addr_eq == "on") {
            echo "setTimeout(function(){cekdata('" . $data_sel[LIST_DATA][0]->pickup_district_seq . "','pickup_');}, 2000);";
//}
//if ($data_sel[LIST_DATA][0]->return_addr_eq == "1" || $data_sel[LIST_DATA][0]->return_addr_eq == "on") {
            echo "setTimeout(function(){cekdata('" . $data_sel[LIST_DATA][0]->return_district_seq . "','return_');}, 3000);";
//}
            echo "cekpr('1');cekpr('3');";
        } else {
            ?>

                $("#propinsi").val("");
                cData("", "city", "city");
                setTimeout(function () {
                    aSama('1', 'pickup');
                }, 500);

                setTimeout(function () {
                    aSama('1', 'return');
                }, 1000);

            <?php
        }
    } else {
        ?>
            $("#email").prop('required', false);
            $("#email").prop('disabled', true);
            setTimeout(function () {
                aSama(<?php echo (($data_sel[LIST_DATA][0]->pickup_addr_eq == "1" || $data_sel[LIST_DATA][0]->pickup_addr_eq == "on") ? "'1'" : "''") ?>, 'pickup');
            }, 500);

            setTimeout(function () {
                aSama(<?php echo (($data_sel[LIST_DATA][0]->return_addr_eq == "1" || $data_sel[LIST_DATA][0]->return_addr_eq == "on") ? "'1'" : "''") ?>, 'return');
            }, 1000);
            function filldata(idcombo, iddtl, jenis, nilai) {
                nilai = nilai || "";
                var list = $("#" + iddtl);
                inHTML = "";
                list.empty();

                if (idcombo != '') {
                    $.ajax({
                        url: url,
                        data: {
                            btnAdditional: "act_s_adt", idh: idcombo, tipe: jenis
                        },
                        type: "POST",
                        success: function (response) {
                            if (isSessionExpired(response)) {
                                response_object = json_decode(response);
                                url = response_object.url;
                                location.href = url;
                            } else {
                                inHTML += "<option value=''>-- Pilih --</option>";
                                $.each($.parseJSON(response), function () {
                                    if (nilai != '') {
                                        selected = "";
                                        if (nilai == this.seq)
                                            selected = " selected";
                                        inHTML += '<option value="' + this.seq + '"' + selected + '>' + this.name + '</option>';
                                    } else {
                                        inHTML += '<option value="' + this.seq + '">' + this.name + '</option>';
                                    }
                                });
                                list.append(inHTML);
                                list.select2("val", nilai);
                            }
                        },
                        error: function (request, error) {
                            alert(error_response(request.status));
                        }
                    });
                }
            }
            function cekdata(dtdistrict, grup) {
                grup = grup || "";
                var idcity = grup + "city";
                var iddistrict = grup + "district_seq";
                if (dtdistrict != '') {
                    $.ajax({
                        url: url,
                        data: {
                            btnAdditional: "act_s_adt", idh: dtdistrict, tipe: 'valpcd'
                        }, type: "POST",
                        success: function (response) {
                            if (isSessionExpired(response)) {
                                response_object = json_decode(response);
                                url = response_object.url;
                                location.href = url;
                            } else {
                                $.each($.parseJSON(response), function () {
                                    idp = this.idp;
                                    idc = this.idc;
                                    idd = this.idd;
                                });
                                $("#" + grup + "propinsi").select2("val", idp);
                                setTimeout(function () {
                                    filldata(idp, idcity, 'city', idc);
                                }, 500);
                                setTimeout(function () {
                                    filldata(idc, iddistrict, 'district', idd);
                                }, 800);
                            }
                            ;
                        },
                        error: function (request, error) {
                            alert(error_response(request.status));
                        }
                    });
                }
            }
        <?php
        if (isset($data_sel[LIST_DATA])) {
            echo "cekdata('" . $data_sel[LIST_DATA][0]->district_seq . "');";
//	    if ($data_sel[LIST_DATA][0]->pickup_addr_eq == "1" || $data_sel[LIST_DATA][0]->pickup_addr_eq == "on") {
            echo "setTimeout(function(){cekdata('" . $data_sel[LIST_DATA][0]->pickup_district_seq . "','pickup_');}, 2000);";
//	    }
//	    if ($data_sel[LIST_DATA][0]->return_addr_eq == "1" || $data_sel[LIST_DATA][0]->return_addr_eq == "on") {
            echo "setTimeout(function(){cekdata('" . $data_sel[LIST_DATA][0]->return_district_seq . "','return_');}, 3000);";
//	    }
            echo "cekpr('1');cekpr('3');";
        }
    }
    ?>
    </script>
<?php } else { ?>

    <?php
}
require_once get_include_page_list_merchant_content_footer();
require_once get_include_content_merchant_bottom_page_navigation();
?>