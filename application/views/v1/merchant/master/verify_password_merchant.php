<?php
require_once VIEW_BASE_MERCHANT;
?>

<html>
    <head>
        <link href="<?php echo get_css_url(); ?>bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_css_url(); ?>merchant/toko1001_merchant.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_css_url(); ?>home/style.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="<?php echo get_js_url(); ?>jQuery-2.1.4.min.js" ></script>
        <script type="text/javascript" src="<?php echo get_js_url(); ?>bootstrap.min.js" ></script>
        <script type="text/javascript" src="<?php echo get_js_url(); ?>jquery.validate.js" ></script>
    </head><title>Ubah Merchant</title>
    <body class="login-page">
        <div class="login-box">
            <div class="login-logo">
                <a><b>MERCHANT</b> Toko1001 </a>
            </div>
            <div class="login-box-body">
                <?php if (isset($data_err) && $data_err[ERROR] == true) { ?>
                    <span class="label label-danger"><?php echo $data_err[ERROR_MESSAGE][0]; ?></span>
                <?php } elseif (isset($data_suc) && $data_suc[SUCCESS] == true) { ?>
                    <center><span class="label label-success"><?php echo $data_suc[SUCCESS_MESSAGE][0]; ?></span></center>
                    <?php } ?>
            </div>
        </div>
    </body>
</html>
