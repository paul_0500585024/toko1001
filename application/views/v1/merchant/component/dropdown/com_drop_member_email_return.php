<select class="form-control select2" style="width : 250px" name="member_email_return">
    <option value="">Email Member</option>
    <?php
    foreach ($email as $each) {
        $selected = "";
        if (isset($_GET['member_email_return'])) {
            if ($each->email == rawurldecode($_GET['member_email_return'])) {
                $selected = "selected";
            } else {
                $selected = "";
            }
        }
        ?>
        <option value="<?php echo $each->email; ?>"<?php echo $selected; ?>><?php echo $each->email; ?></option>
    <?php } ?>
</select>