<select id="drop_province" validate="required[]" class="form-control select2" style='width : 100%' name="province_seq">
    <option value="">-- Pilih --</option>
    <?php
    if (isset($province_name)) {
        foreach ($province_name as $each) {
            ?>
            <option value="<?php echo $each->seq; ?>" <?php echo (isset($data_sel[LIST_DATA]) && $each->seq == $data_sel[LIST_DATA][0]->province_seq) ? "selected" : "" ?>><?php echo get_display_value($each->name); ?></option>
        <?php
        }
    }
    ?>
</select>


