<link href="<?php echo get_css_url(); ?>daterangepicker-bs3.css" rel="stylesheet" type="text/css" />

<input class="form-control pull-right" type="text" name ="date_between" id="date_between" readonly="">

<script type="text/javascript">

    $('#date_between').daterangepicker({
        format: 'DD-MMM-YYYY',
        cancelLabel: 'Clear'
    });

</script>