<?php
require_once VIEW_BASE_MERCHANT;

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">

<html>
    <head>
        <title>Toko1001</title>
        <link rel="icon" href="<?php echo get_img_url(); ?>home/icon/pavicon.png"/>
        <link href="<?php echo get_css_url(); ?>bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_css_url(); ?>merchant/v1/toko1001_merchant.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_css_url(); ?>font-awesome.min.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="<?php echo get_js_url(); ?>jQuery-2.1.4.min.js" ></script>
        <script type="text/javascript" src="<?php echo get_js_url(); ?>jquery.validate.js" ></script>
    </head>
    <body class="min-width-m-login" style="background-image: url('<?php echo get_img_url() ?>/toko1001_login_background.jpg');background-size:  cover;background-repeat: no-repeat;">
        <div id="login-box" class="row no-margin">
            <div class="col-xs-12">
                <?php
                if (isset($data_err) && $data_err[ERROR] === true) {
                    ?>
                    <div id='msg-shw' class='shw-error'>
                        <span><?php echo $data_err[ERROR_MESSAGE][0] ?></span>
                    </div>
                <?php } elseif (isset($data_suc) && $data_suc[SUCCESS] === true) { ?>
                    <div id='msg-shw' class='shw-success'>
                        </i>&nbsp;&nbsp;<span><?php echo (isset($data_suc[SUCCESS_MESSAGE][0]) ? $data_suc[SUCCESS_MESSAGE][0] : SAVE_DATA_SUCCESS); ?></span>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div class="col-xs-2 col-xs-offset-4">
                
            </div>
            <div class="col-xs-5 col-xs-offset-6 m-top20">
                <div class="row m-top20">
                    <div class="col-xs-8 col-xs-offset-2 no-pad m-top40">
                        <div class="m-shadow-login">
                            <div class="col-xs-12 m-bottom-10px m-top10">
                                <h3 class="subtitle-login-m-login">Login Merchant Toko1001</h3>
                            </div><br/>
                            <div class="well no-border no-shadow no-border-radius-m-login-m-login custom-well-m-login">
                                <form id="frmlogin" method = "post" action= "<?php echo get_base_url() ?>merchant/login/sign_in<?php echo isset($data_auth[FORM_AUTH][FORM_URL]) ? $data_auth[FORM_AUTH][FORM_URL] : ""; ?>">
                                    <div class="form-group has-feedback">
                                        <label>Email Address</label>
                                        <input id="txtUserName" name="email" type="text" placeholder="Email Address" 
                                               class="form-control"
                                               value ="<?php echo (isset($data_sel[LIST_DATA]) ? get_display_value($data_sel[LIST_DATA]->user_id) : "") ?>"/>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label>Password</label>
                                        <input id="txtPassword" name="password" type="password"
                                               class="form-control" placeholder="Password" value ="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA]->password : "") ?>"/>
                                        <br/><br/><a class="pull-right"  href="<?php echo get_base_url() ?>merchant/forgot_password">Lupa Password ?</a>
                                    </div>
                                    </br>
                                    <div class="row">
                                        <div class="col-lg-12"><br>
                                                <button name="submit" type="submit" class="btn btn-block btn-info btn-block bg-blue">
                                                    <i class="fa fa-sign-in"></i>&nbsp; Masuk
                                                </button>
                                                <br/>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <center>
                    <p class="l-foot">Copyright &copy;&nbsp; 2015 - <?php echo date('Y') ?>&nbsp;PT. Deltamas Mandiri Sejahtera</p>
                </center>
            </div>
        </div>
    </body>
    <script>


        $(document).ready(function () {
            var screenLayer = ($(window).height() / 4);
            $('#login-box').css('margin-top', screenLayer + 'px');
            $('#msg-shw').delay(3000).fadeOut("slow");
            $(document).on('scroll', function () {
                if ($(window).width() > 1199) {
                    var top = $(this).scrollTop();
                    if (top > 70) {
                        $('#sub-navbar').addClass('posfixed');
                    } else {
                        $('#sub-navbar').removeClass('posfixed');
                    }
                }
            });
        })

        $('#frmlogin').validate({
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true
//                        minlength: 8
                }
            }
        });
        $.validator.messages.required = "Data wajib diisi !";
        $.validator.messages.maxlength = "Harap diisi tidak lebih dari {0} karakter !";
        $.validator.messages.minlength = "Harap diisi dengan minimal {0} karakter !";
        $.validator.messages.email = "Harap diisi dengan alamat email dengan benar !";
        $.validator.messages.date = "Harap diisi dengan tanggal !";
        $.validator.messages.dateiso = "Harap diisi dengan format tanggal yang benar !";
        $.validator.messages.number = "Harap diisi dengan angka !";
    </script>
</body>
</html>