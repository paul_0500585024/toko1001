<?php
require_once VIEW_BASE_MERCHANT;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">

<html>
    <head>
        <link href="<?php echo get_css_url(); ?>bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_css_url(); ?>merchant/toko1001_merchant.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="<?php echo get_js_url(); ?>jQuery-2.1.4.min.js" ></script>
        <script type="text/javascript" src="<?php echo get_js_url(); ?>jquery.validate.js" ></script>
    </head>
    <body class="login-page">   
        <div class="login-box">
            <div class="login-logo">
                <a href="."><b>MERCHANT</b> Toko1001 </a>
            </div>
            <div class="login-box-body">
                <?php if ($data_err[ERROR] === true) { ?>
                    <p class="login-box-msg text-red"><?php echo $data_err[ERROR_MESSAGE][0] ?> </p> 
                <?php } else { ?>
                    <p class="login-box-msg text-red">Harap ubah password anda yang lama dengan yang baru !</p>
                <?php } ?>
                <div class="form-group has-feedback">
                    <input name="email" type="text" class="form-control" value ="<?php echo $email ?>" disabled/>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <form id="frmlogin" method = "post" action= "<?php echo get_base_url() ?>merchant/profile/change_old_password_merchant">
                    <?php echo get_csrf_merchant_token(); ?>
                    <div class="form-group has-feedback">
                        <input name="old_password" type="password" class="form-control" placeholder="Password Lama"/>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input id="newpass" name="new_password" type="password" class="form-control" placeholder="Password Baru"/>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input name="confirm_password" type="password" class="form-control" placeholder="Konfirmasi Password"/>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class ="col-md-6"><?php echo get_save_edit_button(); ?> </div>
                            <!--<button name="submit" type="submit" class="btn bg-navy">Simpan</button>-->
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <script>
            $('#frmlogin').validate({
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                rules: {
                    old_password: {
                        required: true,
                        minlength: 8,
                        maxlength: 20
                    },
                    password: {
                        required: true,
                        minlength: 8,
                        maxlength: 20
                    },
                    confirm_password: {
                        equalTo: "#newpass",
                        required: true,
                        minlength: 8,
                        maxlength: 20
                    }
                }
            });

        </script>
    </body>
</html>