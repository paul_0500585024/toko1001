<?php
require_once VIEW_BASE_MERCHANT;
require_once get_include_content_merchant_top_page_navigation();
?>
<br><br>
<br><br><br>
<div class="row m-top10">
    <div class="col-xs-6 col-xs-offset-3">
        <img src="<?php echo get_img_url() ?>home/welcome_store.png" class="img-responsive center-block">
    </div>
</div>

<?php
require_once get_include_page_list_merchant_content_footer();
require_once get_include_content_merchant_bottom_page_navigation();
?>