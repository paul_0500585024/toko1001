<?php
require_once VIEW_BASE_MERCHANT;
$formauthtitle = "";
$formauthurl = "";
if (isset($data_auth[FORM_AUTH][FORM_TITLE])) {
    $formauthtitle = get_title_list($data_auth[FORM_AUTH][FORM_TITLE]);
    $formauthurl = $data_auth[FORM_URL];
}
?>
<link href="<?php echo get_css_url(); ?>bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_css_url(); ?>font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_css_url(); ?>ionicons.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_css_url(); ?>dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_css_url(); ?>jquery.dataTables.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_css_url(); ?>select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_css_url(); ?>jquery.treeview.css" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/css?family=Righteous" rel="stylesheet">
<link href="<?php echo get_css_url() . "merchant/" . LIVEVIEW; ?>toko1001_merchant.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_css_url() . "merchant/" . LIVEVIEW; ?>slim.min.css" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/css?family=Abel" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="<?php echo get_css_url(); ?>jquery.steps.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="<?php echo get_img_url(); ?>home/icon/pavicon.png"/>
<!--<link href="<?php echo get_css_url(); ?>daterangepicker-bs3.css" rel="stylesheet" type="text/css" />-->
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.24/daterangepicker.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo get_js_url(); ?>jQuery-2.1.4.min.js" ></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>app.min.js" ></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>fastclick.min.js" ></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>jquery.dataTables.min.js" ></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>dataTables.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>jquery.validate.js" ></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>select2.full.min.js" ></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>jquery.treeview.js" ></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>autoNumeric-min.js" ></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>dataTables.responsive.min.js" ></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>morris/morris.min.js"></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>raphael-min.js"></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>dataTables.buttons.min.js" ></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>jszip.min.js" ></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>vfs_fonts.js" ></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>buttons.html5.min.js" ></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>error_response.js"></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>input-mask/jquery.inputmask.js"></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>slim.kickstart.js"></script>
<script type="text/javascript" src="<?php echo get_js_url() ?>moment.min.js"></script>
<!--<script type="text/javascript" src="<?php echo get_js_url() ?>daterangepicker.js"></script>-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.24/daterangepicker.min.js"></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>slim.jquery.js"></script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>jquery.steps.min.js"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.8/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.sticky/1.0.3/jquery.sticky.min.js"></script>

<script>
    var menu = "<?php echo isset($data_auth[FORM_CD]) ? $data_auth[FORM_CD] : "" ?>";
</script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>merchant.js"></script>
<script>
$(function () {
                $('#spek1 , #procat , #spek2').slimScroll({
                    height: '450px'
                });
            });
</script>
