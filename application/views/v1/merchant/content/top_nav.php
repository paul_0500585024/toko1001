<?php
require_once VIEW_BASE_MERCHANT;
?>
<header class="header">
    <nav class="navbar navbar-fixed-top navbar-custom-merchant">
        <div class="navbar-custom-menu">
            <div class="pull-left">
                <a href="<?php echo get_base_url(); ?>merchant/main_page">
                    <img class="logo logo-merchant" src="<?php echo get_image_location() ?>assets/img/title_logo_merchant_panel.png" /> 
                </a>
            </div>
            <!--sisi kanan-->
            <ul class="nav navbar-nav pull-right">
                <li class="dropdown notifications-menu removeMenuActive margin-top-4">
                    <!--a id="n_notif" href="#" class="dropdown-toggle c-616161" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                    </a>
                    <!--ul class="dropdown-menu toko1001-notification" id="clarified_notif">
                        <?php /* <?php if ($n_notification >= 5): ?>
                          <li class="footer"><a href="<?php echo $n_notification ?>">View all</a></li>
                          <?php endif; ?> */ ?>
                    </ul-->
                </li>
                <li class="dropdown user user-menu margin-top-4">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?php echo (isset($data_prof[USER_IMAGE]) ? $data_prof[USER_IMAGE] : ''); ?>" 
                             class="user-image" alt="<?php echo (isset($data_prof[USER_NAME]) ? $data_prof[USER_NAME] : ''); ?>">
                        <span class="c-616161"> Hallo, <?php echo (isset($data_prof[USER_NAME]) ? $data_prof[USER_NAME] : '' ); ?>
                        </span>&nbsp;&nbsp;<span class="fa fa-caret-down" aria-hidden="true"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?php echo (isset($data_prof[USER_IMAGE]) ? $data_prof[USER_IMAGE] : ''); ?>" 
                                 class="img-circle" alt="<?php echo (isset($data_prof[USER_NAME]) ? $data_prof[USER_NAME] : ''); ?>">
                            <p style="color: #000;">
                        <center><?php echo (isset($data_prof[USER_EMAIL]) ? $data_prof[USER_EMAIL] : ''); ?></center>
                        <small><?php echo (isset($data_prof[LAST_LOGIN]) ? $data_prof[LAST_LOGIN] : ''); ?></small>
                        </p>
                </li>
                <li class="user-body">
                    <div class="row">
                        <div class="col-xs-6 text-center">
                            <a href="<?php echo get_base_url(); ?>merchant/master/info_merchant" class="btn btn-default btn-flat btn-block">Profil</a>
                        </div>
                        <div class="col-xs-6 text-center">
                            <a href="<?php echo get_base_url(); ?>merchant/master/store_decoration" class="btn btn-default btn-flat btn-block">Dekorasi Toko</a>
                        </div>
                    </div>
                </li>
                <li class="user-footer no-padding">
                    <a href="<?php echo get_base_url(); ?>merchant/profile/change_password_merchant" class="c-616161 pad-15">Ubah Password</a>
                </li>
                <hr class="no-margin">
                <li class="user-footer no-padding">
                    <a href="<?php echo get_base_url(); ?>logout/merchant" class="pad-15">Sign out</a>
                </li>

            </ul>
            </li>
            </ul>
        </div>
    </nav>
</header>
<script>
    function read_notif(seq) {
        $.ajax({
            url: "<?php echo base_url() ?>merchant/transaction/notification",
            data: "seq=" + seq + "&btnAdditional=true&type=read",
            type: "POST",
            success: function() {
            },
            error: function() {
            }
        });
        location.href = "<?php echo base_url() ?>merchant/transaction/notification?<?php echo CONTROL_GET_TYPE ?>=<?php echo ACTION_VIEW ?>&key=" + seq;
    }
    $(document).ready(function() {
        $.ajax({
            url: "<?php echo base_url() ?>merchant/transaction/notification/",
            data: "seq=" + <?php echo $data_prof[USER_SEQ] ?> + "&btnAdditional=true&type=count_total",
            type: "POST",
            success: function(n_notification) {
                if (n_notification == 0) {
                    $('#clarified_notif').prepend('<li class="header" style="cursor:pointer">Anda tidak mempunyai notifikasi</li>');
                } else {
                    if (n_notification > 0) {
                        $('#n_notif').append('<span class="label label-warning">' + n_notification + '</span>');
                        $('#clarified_notif').append('<li class="header" style="cursor:pointer">Anda mempunyai ' + n_notification + ' notifikasi</li>');
                        show_notif_top(n_notification);
                    }
                }
            },
            error: function() {
            }
        });
        function show_notif_top(n_notification) {
            $.ajax({
                url: "<?php echo base_url() ?>merchant/transaction/notification",
                data: "seq=" + <?php echo $data_prof[USER_SEQ] ?> + "&btnAdditional=true&type=show_notif_top",
                type: "POST",
                dataType: "json",
                success: function(top_data_notif) {

//                    alert(top_data_notif.length);
                    data_head = '<li>';
                    data_head += '<div class="slimScrollDiv" style="position: relative; width: auto; height: auto;">';
                    data_head += '<ul class="menu" style="width: 100%; max-height: 500px;">';
                    data_content = '';
                    $(top_data_notif).each(function(i) {
                        data_content += '<li>';
                        data_content += '<a style="cursor:pointer" onclick="read_notif(\'' + top_data_notif[i].seq + '\')">';
                        data_content += '<div class="col-xs-11">';
                        data_content += '<strong>' + top_data_notif[i].subject + '</strong>';
                        data_content += '</div>';
                        data_content += '</a>';
                        data_content += '</li>';
                    });
                    data_foot = '</ul>';
                    data_foot += '<div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 3px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div><div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>';
                    data_foot += '</div>';
                    data_foot += '</li>';
                    $('#clarified_notif').append(data_head + data_content + data_foot);
                    if (n_notification > 5) {
                        $('#clarified_notif').append('<li class="footer"><a href="<?php echo base_url() ?>merchant/transaction/notification">View all</a></li>');
                    }                    
                },
                error: function() {
                }
            });
        }
    });
</script>
