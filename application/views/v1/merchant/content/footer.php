<?php
require_once VIEW_BASE_MERCHANT;
?>
<br><br>
<footer class="footer f-style">
    <div class='row'>
        <div class="col-xs-10 col-xs-offset-2">
            <center class="p-10px">Copyright &copy; 2015-<?php echo date("Y") ?> <a class='text-weight-400' href="<?php echo base_url() ?>">Toko1001</a> All rights reserved.</center>
        </div>
        <div class="clearfix"></div>
    </div>
</footer>

