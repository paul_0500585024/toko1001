<div id="show_error"></div>
<script>
    $(document).ready(function() {

        $("#sub-navbar").sticky({topSpacing: 70, zIndex: 9999});
        $('#msg-shw').delay(3000).fadeOut("slow");
        $('#sub-navbar').on('sticky-start', function() {
            $(this).addClass("transform-sub-navbar");
        });
        $('#sub-navbar').on('sticky-end', function() {
            $(this).removeClass("transform-sub-navbar");
        });

        $.ajax({
            url: "<?php echo base_url() ?>merchant/transaction/notification/",
            data: "btnAdditional=true&type=count_notif",
            type: "POST",
            dataType: 'json',
            success: function(data) {
                $(data[0]).each(function(i) {
                    if (data[0][i].count_order > 0) {
                        $("#notif_MTRX01003").html(data[0][i].count_order);
                    } else {
                        $("#notif_MTRX01003").html('');
                    }
                    if (data[1][i].count_order_return > 0) {
                        $("#notif_MTRX01005").html(data[1][i].count_order_return);
                    } else {
                        $("#notif_MTRX01005").html('');
                    }
                    if (data[2][i].count_redeem > 0) {
                        $("#notif_MTRX01004").html(data[2][i].count_redeem);
                    } else {
                        $("#notif_MTRX01004").html('');
                    }
                });
            },
            error: function() {
            }
        });
    });
</script>
<script type="text/javascript" src="<?php echo get_js_url(); ?>json2csv.js"></script>
<script type="text/javascript">
    var button;
    var table;
    var exp = '';
    var sorting = '<?php echo isset($tablesort); ?>';
    $(document).ready(function() {
        var url = $('#frmAuth').attr('action');
        var success_html = '<div class="alert alert-info alert-dismissable" style="margin-bottom: 0!important;><h4><i class="fa fa-info"></i></h4><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
        var error_html = '<div class="alert alert-danger alert-dismissable" style="margin-bottom: 0!important;> <h4><i class="fa fa-info"></i></h4><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
        var columns_detail = [];
        $('#tbl thead th').each(function() {
            var sName = $(this).attr("column");
            var bVisible = $(this).attr("visible");
            columns_detail.push({"sName": sName, "bVisible": bVisible, "mData": sName});
        });
        table = $('#tbl').DataTable({
            oLanguage: {
                sProcessing: "<img src='<?php echo base_url() . ASSET_IMG_HOME ?>loading.gif' />"
            },
            dom: '<"top"lB>rt<"bottom"ip><"clear">',
            responsive: true,
            processing: true,
            serverSide: true,
            aoColumns: columns_detail,
            buttons: ['csvHtml5', 'excelHtml5', 'pdfHtml5'],
            ajax: {
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: buildAjaxData,
                error: handleAjaxError
            }
        })
                .on('xhr.dt', function(e, settings, response, xhr) {
            response = JSON.stringify(response);
            if (isSessionExpired(response)) {
                response_object = json_decode(response);
                url = response_object.url;
                location.href = url;
            }
        });
        function handleAjaxError(request, textStatus, error) {
            request = JSON.stringify(request);
            $('#show_error').html(request);
            if (textStatus === 'timeout') {
                alert('The server took too long to send the data.');
            } else {
                var response = error_response(request.status);
                var session = response.search("Sign in to start your session")
                if (session > 0) {
                    alert("Session habis, silahkan login ulang");
                    window.location.href = "<?php echo get_base_url() ?>merchant_page";
                    return false;
                }
                var response = error_response(request.status);
                $('#content-message').html(error_html + " " + response + "</div>");
                $('div#tbl_processing').css('visibility', 'hidden');
            }
        }

        function handleAjaxSuccess(response) {
            alert(response);
        }

        function buildAjaxData() {
            var settings = $("#tbl").dataTable().fnSettings();
            var search = [];
            var displaylength = settings._iDisplayLength;
            if (exp != '') {
                displaylength = exp;
//		$("#tbl").dataTable().fnSetDisplayLength = exp;
            }
            if (sorting != '') {
                sorting = "";
                settings.aaSorting = [[0, 'desc']];
            }
            search = $('#frmSearch').serializeArray();
            search.push(
                    {name: "draw", value: settings.iDraw},
            {name: "start", value: settings._iDisplayStart},
            {name: "length", value: displaylength},
            {name: "column", value: columns_detail[settings.aaSorting[0][0]].sName},
            {name: "order", value: settings.aaSorting[0][1]},
            {name: "btnSearch", value: "true"},
            {name: "export", value: exp}
            );
            exp = '';
            $('.dt-buttons').css("visibility", "hidden");
            return search;
        }

        $("#e_csv").click(function() {
            exp = $("#tbl").dataTable().fnSettings().fnRecordsTotal();
            table.ajax.reload();
            setTimeout(function() {
                table.button(0).trigger()
            }, 1000);
            setTimeout(function() {
                table.ajax.reload();
            }, 1000);
        });
        $("#e_xls").click(function() {
            exp = $("#tbl").dataTable().fnSettings().fnRecordsTotal();
            table.ajax.reload();
            setTimeout(function() {
                table.button(1).trigger()
            }, 1000);
            setTimeout(function() {
                table.ajax.reload();
            }, 1000);
        });
        $("#e_pdf").click(function() {
            exp = $("#tbl").dataTable().fnSettings().fnRecordsTotal();
            table.ajax.reload();
            setTimeout(function() {
                table.button(2).trigger()
            }, 1000);
            setTimeout(function() {
                table.ajax.reload();
            }, 1000);
        });
        $('#tbl tbody').on('click', 'tr', function() {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });
        $("#delete").click(function() {
            var key = $(".selected").attr('id');
            if (key == undefined) {
                alert("Harap pilih data dulu !");
            } else {
                if (confirm("Apakah anda yakin menghapus data yang dipilih ?")) {
                    $.ajax({
                        url: url,
                        type: "POST",
                        dataType: "json",
                        data: {key: key, btnSaveDel: "true"},
                        success: function(response) {
                            if (isSessionExpired(response)) {
                                response_object = json_decode(response);
                                url = response_object.url;
                                location.href = url;
                            } else {
                                if (response.error == true) {
                                    $('#content-message').html(error_html + response.message + "</div>")
                                } else {
                                    table.row('.selected').remove().draw(false);
                                    $('#content-message').html(success_html + " Data telah berhasil dihapus ! </div>")
                                }
                                table.ajax.reload();
                            }
                        },
                        error: function(request, error) {
                            $('#content-message').html(error_html + " " + error_response(request.status) + "</div>");
                        }
                    });
                }
            }
        });
        $("#approve").click(function() {
            var key = $(".selected").attr('id');
            //var key = $(".selected").attr('status');
            if (key == undefined) {
                alert("Harap pilih data dulu !");
            } else {
                if (confirm("Apakah anda yakin menyetujui data yang dipilih ?")) {
                    $.ajax({
                        url: url,
                        type: "POST",
                        dataType: "json",
                        data: {key: key, btnApprove: "true"},
                        success: function(response) {
                            if (isSessionExpired(response)) {
                                response_object = json_decode(response);
                                url = response_object.url;
                                location.href = url;
                            } else {
                                if (response.error == true)
                                {
                                    $('#content-message').html(error_html + response.message + "</div>")
                                } else {
                                    $('#content-message').html(success_html + " Data telah berhasil disetujui ! </div>")
                                }
                                table.ajax.reload();
                            }
                        },
                        error: function(request, error) {
                            alert(error_response(error_response(request.status)));
                            $('#content-message').html(error_html + "" + error_response(request.status) + "</div>");
                        }
                    });
                }
            }
        });
        $("#reject").click(function() {
            var key = $(".selected").attr('id');
            if (key == undefined) {
                alert("Harap pilih data dulu !");
            } else {
                if (confirm("Apakah anda yakin menolak data yang dipilih ?")) {
                    $.ajax({
                        url: url,
                        type: "POST",
                        dataType: "json",
                        data: {key: key, btnReject: "true"},
                        success: function(response) {
                            if (isSessionExpired(response)) {
                                response_object = json_decode(response);
                                url = response_object.url;
                                location.href = url;
                            } else {
                                if (response.error == true) {
                                    $('#content-message').html(error_html + response.message + "</div>")
                                } else {
                                    $('#content-message').html(success_html + " Data telah berhasil ditolak ! </div>")
                                    table.ajax.reload();
                                }
                                ;
                            }
                        },
                        error: function(request, error) {
//                            $('#content-message').html(error_html + " " + request.ResponseText + "</div>");
                            $('#content-message').html(error_html + " " + error_response(request.status) + "</div>");
                            table.ajax.reload();
                        }
                    });
                }
            }
        });
//untuk cek email
        $("#email").click(function() {
            var key = $(".selected").attr('id');
            if (key == undefined) {
                alert("Select the item first !");
            } else {
                if (confirm("Are you sure want to reject this item?")) {
                    $.ajax({
                        url: url,
                        type: "POST",
                        datatype: "JSON",
                        data: {key: key, btnReject: "true"},
                        success: function(response) {
                            if (isSessionExpired(response)) {
                                response_object = json_decode(response);
                                url = response_object.url;
                                location.href = url;
                            } else {
                                if (response.error == true) {
                                    $('#content-message').html(error_html + response.message + "</div>")
                                } else {
                                    $('#content-message').html(success_html + " Data telah berhasil ditolak ! </div>")
                                    table.ajax.reload();
                                }
                                ;
                            }
                        },
                        error: function(request, error) {
                            $('#content-message').html(error_html + " " + error_response(request.status) + "</div>");
                            table.ajax.reload();
                        }
                    });
                }
            }
        });
        $("#refresh").click(function() {
            table.ajax.reload();
        });
        $(document).on('click', '#search', function(e) {
            table.ajax.reload();
        });
        var isValid = $('#frmMain').validate({
            ignore: 'input[type=hidden]',
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else if (element.hasClass('select2')) {
                    error.insertAfter(element.next('span'));
                } else if (element.hasClass('radiobtn')) {
                    error.insertAfter(element.closest('.error-box'));
                } else {
                    error.insertAfter(element);
                }
            }
        });
        $.validator.messages.required = "Data wajib diisi !";
        $.validator.messages.maxlength = "Harap diisi tidak lebih dari {0} karakter !";
        $.validator.messages.minlength = "Harap diisi dengan minimal {0} karakter !";
        $.validator.messages.email = "Harap diisi dengan alamat email yang benar !";
        $.validator.messages.date = "Harap diisi dengan tanggal !";
        $.validator.messages.dateiso = "Harap diisi dengan format tanggal yang benar !";
        $.validator.messages.number = "Harap diisi dengan angka !";
        $.validator.messages.equalTo = "Konfirmasi password harus sama dengan password !";
        $(document).on('submit', '#frmMain', function(e) {

            if (isValid.valid()) {
                $('input[type="text"]').each(function() {
                    if ($(this).hasClass('auto') || $(this).hasClass('auto_int') || $(this).hasClass('auto_dec'))
                    {
                        var v = $(this).autoNumeric('get');
                        $(this).autoNumeric('destroy');
                        $(this).val(v);
                    }
                });
                if ($("#varian").length) {
                    var setVariantStatus = $("#varian").prop("checked");
                    if (setVariantStatus === true) {
                        $("#1").remove();
                    } else {
                        $("#variasi").remove();
                    }
                }
            }
        });
        if ($('#frmMain').length) {
            $('input[type="text"], input[type="file"], input[type="password"], textarea').each(function() {
                var validate = $(this).attr('validate');
                switch (validate) {
                    case  "required[]" :
                        $(this).rules('add', {
                            required: true});
                        break;
                    case  "email[]" :
                        $(this).rules('add', {
                            required: true,
                            email: true});
                        break;
                    case  "password[]" :
                        $(this).rules('add', {
                            required: true,
                            minlength: 8,
                            maxlength: 20});
                        break;
                    case  "email-not-required[]" :
                        $(this).rules('add', {
                            email: true});
                        break;
                    case  "date[]" :
                        $(this).rules('add', {
                            required: true,
                            date: true});
                        break;
                    case  "date-not-required[]" :
                        $(this).rules('add', {
                            date: true
                        });
                        break;
                    case  "num[]" :
                        $(this).rules('add', {
                            required: true,
                            number: true
                        });
                        break;
                    case  "num-not-required[]" :
                        $(this).rules('add', {
                            number: true
                        });
                        break;
                }
            })
        }
        $('select').each(function() {
            if ($(this).attr('validate') == "required[]")
            {
                $(this).rules('add', {
                    required: true
                });
            }
        });
        $('.select2').on('change', function() {
            $(this).valid();
        });
    });
    $('.select2').select2();
    $('.auto').autoNumeric('init', {vMin: 0});
    $('.auto_int').autoNumeric('init', {vMin: 0, mDec: 0});
    $('.auto_dec').autoNumeric('init', {vMin: 0, mDec: 2});
    function validate_form()
    {
        if (button == 'cancel')
        {
            if (confirm("Apakah anda yakin batal ?")) {
                $("#frmMain").validate().cancelSubmit = true;
                return true;
            } else {
                $("#frmMain").validate().cancelSubmit = true;
                button = "";
                return false;
            }
        } else
        {
            return true;
        }

    }

    function get_id()
    {
        if (button == 'add')
        {
            return true;
        } else
        {
            var key = $(".selected").attr('id');
            if (key === undefined) {
                alert("Harap pilih data dulu !");
                return false;
            } else {
                $("<input>").attr("type", "hidden").attr('name', "key").attr('value', key).appendTo('#frmAuth');
                return true;
            }
        }
    }

    function create_report(jsondata) {
        var rpt = new CSVExport(jsondata);
    }
</script>