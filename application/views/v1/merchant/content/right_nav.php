<?php
require_once VIEW_BASE_MERCHANT;
?>

<aside class="control-sidebar control-sidebar-dark">                
    <div class="control-sidebar-heading bg-black">
        <div style="text-align: center">
            <i class="fa fa-search"></i>
            Pencarian
        </div>
    </div>
    <div class="tab-content"> 
        <?php
        if (isset($data_auth)) {
            if ($data_auth[FORM_ACTION] == "") {
                if (isset($filter)) {
                    unset($data_sel[LIST_DATA]);
                    require_once VIEWPATH . $filter;
                }
            }
        }
        ?>
    </div>
</aside>
<div class='control-sidebar-bg'></div>