<?php
require_once VIEW_BASE_HOME;
$test = parse_str($this->input->server('QUERY_STRING'), $query_string);
$get_attribute_name_display = array();
?>
<?php
$getSearchHarga = $this->input->get('pr');
$getMinPrice = 0;
$getMaxPrice = 99999999;
if ($getSearchHarga !== NULL) :
    $searchPrice = explode(',', $getSearchHarga);
    $getMinPrice = isset($searchPrice[0]) ? $searchPrice[0] : 0;
    $getMaxPrice = isset($searchPrice[1]) ? $searchPrice[1] : 99999999;
endif;
$order_category = $this->input->get('g');

$product = isset($data_sel[LIST_DATA]['product']) ? $data_sel[LIST_DATA]['product'] : NULL;
?>
<?php
switch ($iese) {
    case TASK_CATEOGRY:
        ?>
        <div class="container">
            <div class = "cst-breadcrumb">
                <div class = "cst-breadcrumb-inside">
                    <a href = "<?php echo base_url(); ?>"><i class = "fa fa-home"style = "font-size:1.5em;"></i></a>
                </div>
                <div class = "cst-breadcrumb-inside">
                    <img src = "<?php echo get_image_location() . 'assets/img/breadcrumb.png' ?>">
                </div>
                <a class = "wsh" href = '<?php echo base_url(IESE_URL) ?>'><?php echo 'IESE 2017' ?></a>
                <div class="cst-breadcrumb-inside">
                    <img src="<?php echo get_image_location() . 'assets/img/breadcrumb.png' ?>">
                </div>
                <a class="wsh" href='<?php echo base_url() . $data_auth[FORM_URL] ?>'><?php echo $breadcrumb_pages ?></a>
            </div>
            <div class="row">
                <div class="col-xs-2 pl-15px pr-5px">
                    <div id="filter" class="filter-cst">
                        <div class='cat-body'>
                            <form class="form" id="formLogin" method="get" action="<?php echo current_url_with_query_string(array(PRICE_RANGE, START_OFFSET)) ?>">
                                <input type="text" value="" class="slider form-control"
                                       id="harga" name="" data-slider-min="0"
                                       data-slider-max="99999999" data-slider-step="10000"
                                       data-slider-value="[<?php echo $getMinPrice ?>,<?php echo $getMaxPrice ?>]"
                                       data-slider-orientation="horizontal" style="display: block;"
                                       data-slider-selection="before" data-slider-tooltip="show"
                                       data-slider-id="grey">
                                <div id="slideprice"></div>
                                <div class="col-xs-6 p-3px">
                                    <input type="text" id="minrange"  value="<?php echo (isset($getSearchHarga) ? number_format($searchPrice[0]) : number_format("0") ); ?>" class="form-control numeric" maxlength="9" style="padding: 9px !important">
                                </div>
                                <div class="col-xs-6 p-3px">
                                    <input type="text" id="maxrange"  value="<?php echo (isset($getSearchHarga) ? number_format($searchPrice[1]) : number_format("99999999") ); ?>" class="form-control numeric" maxlength="9" style="padding: 9px !important">
                                </div>
                                <input type="hidden" id="pricerange" name="pr" value="0,99999999" class="form-control">
                                <div class="mt-10px">
                                    <button type="submit" id="btnLogin" class="btn btn-block btn-flat btn-green" ><i class="fa fa-search"></i></button>
                                </div>
                            </form>
                            <br>
                        </div>
                    </div>
                </div>
                <div class="col-xs-10 bl-1px">
                    <div class='well header-cat-product p-10px'>
                        <div class='row no-pad no-margin'>
                            <div class='col-xs-6 no-pad no-margin'>
                            </div>
                            <div class='col-xs-6 no-pad no-margin'>
                                <button type="button" class="btn dropdown-toggle pull-right no-border-radius btn-normal width-210" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                    Urutkan
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu btn-normal pull-right cst-zindex width-210">
                                    <li>
                                        <?php if ($order_category == NEW_TO_OLD_PRODUCT): ?>
                                            <a href="<?php echo current_url_with_query_string(array(ORDER_CATEGORY, START_OFFSET), array(ORDER_CATEGORY => NEW_TO_OLD_PRODUCT)) ?>" class="cst-option-act">Produk terbaru ke terlama</a>
                                        <?php else: ?>
                                            <a href="<?php echo current_url_with_query_string(array(ORDER_CATEGORY, START_OFFSET), array(ORDER_CATEGORY => NEW_TO_OLD_PRODUCT)) ?>" class="cst-option">Produk terbaru ke terlama</a>
                                        <?php endif; ?>
                                    </li>
                                    <li>
                                        <?php if ($order_category == OLD_TO_NEW_PRODUCT): ?>
                                            <a href="<?php echo current_url_with_query_string(array(ORDER_CATEGORY, START_OFFSET), array(ORDER_CATEGORY => OLD_TO_NEW_PRODUCT)) ?>" class="cst-option-act">Produk terlama ke terbaru</a>
                                        <?php else: ?>
                                            <a href="<?php echo current_url_with_query_string(array(ORDER_CATEGORY, START_OFFSET), array(ORDER_CATEGORY => OLD_TO_NEW_PRODUCT)) ?>" class="cst-option">Produk terlama ke terbaru</a>
                                        <?php endif; ?>
                                    </li>
                                    <li>
                                        <?php if ($order_category == PRICE_EXPENSIVE_TO_CHEAP): ?>
                                            <a href="<?php echo current_url_with_query_string(array(ORDER_CATEGORY, START_OFFSET), array(ORDER_CATEGORY => PRICE_EXPENSIVE_TO_CHEAP)) ?>" class="cst-option-act">Harga mahal ke murah</a>
                                        <?php else: ?>
                                            <a href="<?php echo current_url_with_query_string(array(ORDER_CATEGORY, START_OFFSET), array(ORDER_CATEGORY => PRICE_EXPENSIVE_TO_CHEAP)) ?>" class="cst-option">Harga mahal ke murah</a>
                                        <?php endif; ?>
                                    </li>
                                    <li>
                                        <?php if ($order_category == PRICE_CHEAP_TO_EXPENSIVE): ?>
                                            <a href="<?php echo current_url_with_query_string(array(ORDER_CATEGORY, START_OFFSET), array(ORDER_CATEGORY => PRICE_CHEAP_TO_EXPENSIVE)) ?>" class="cst-option-act">Harga murah ke mahal</a>
                                        <?php else: ?>  
                                            <a href="<?php echo current_url_with_query_string(array(ORDER_CATEGORY, START_OFFSET), array(ORDER_CATEGORY => PRICE_CHEAP_TO_EXPENSIVE)) ?>" class="cst-option">Harga murah ke mahal</a>
                                        <?php endif; ?>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                    <!--FILTER MESSAGE-->
                    <?php if ($this->input->get(PRICE_RANGE, TRUE) != false): ?>
                        <span>
                            <?php
                            $price_range = $this->input->get(PRICE_RANGE, TRUE);
                            $price_range_exploder = explode(',', $price_range);
                            $start_price = isset($price_range_exploder[0]) ? $price_range_exploder[0] : '';
                            $to_price = isset($price_range_exploder[1]) ? $price_range_exploder[1] : '';
                            $filter_message[] = "Harga : " . RP . cnum($start_price) . " - " . RP . cnum($to_price);
                            ?>
                        </span>
                    <?php endif; ?>



                    <?php
                    $counter_filter = 0;
                    $end_filter_index = count($get_attribute_name_display);
                    ?>
                    <?php foreach ($get_attribute_name_display as $attribute_name_display => $each_get_attribute_name_display): ?>
                        <?php foreach ($each_get_attribute_name_display as $key => $attribute_value_value): ?>
                            <?php $filter_message[] = $attribute_name_display . " : " . $attribute_value_value ?>
                            <?php $counter_filter++; ?>
                        <?php endforeach; ?>
                    <?php endforeach; ?>

                    <?php
                    if (count($product) > 0) {
                        echo display_product($product);
                    } elseif ($this->input->get(SEARCH, TRUE) != FALSE) {
                        ?>
                        <p>Kami tidak menemukan hasil pencarian untuk  &quot;<b><?php echo htmlspecialchars($this->input->get(SEARCH, TRUE)); ?></b>&quot;</p>
                        <p>Tolong periksa pengejaan kata, gunakan kata-kata yang lebih umum &amp; coba lagi!</p>
                        <?php
                    } elseif (count($product) == 0) {
                        echo "Tidak ada produk dengan kategori ini.";
                    }
                    ?><br>
                </div>
            </div>
        </div>
        <?php
        break;

    default:
        ?>
        <style>
            .iese_hover {
                display: inline-block;
                vertical-align: middle;
                -webkit-transform: perspective(1px) translateZ(0);
                transform: perspective(1px) translateZ(0);
                box-shadow: 0 0 1px transparent;
                -webkit-transition-duration: 0.5s;
                transition-duration: 0.5s;
            }
            .iese_hover:hover, .iese_hover:focus, .iese_hover:active {
                -webkit-transform: scale(1.1);
                transform: scale(1.1);
                -webkit-transition-timing-function: cubic-bezier(0.47, 2.02, 0.31, -0.36);
                transition-timing-function: cubic-bezier(0.47, 2.02, 0.31, -0.36);
                z-index: 998;
            }
        </style>
        <div class="container">
            <div class="col-xs-12 no-pad no-margin mb-10px">
                <div class="row no-pad no-margin">
                    <div class="col-xs-12 no-pad no-margin">
                        <img class="img-responsive" src="<?php echo get_image_location() . CAMPAIGN_IMG . IESE_IMG . DIRECTORY_SEPARATOR; ?>front_banner.jpg" alt="">
                    </div>
                </div>
            </div>

            <div id="always_on" class="col-xs-12 no-pad no-margin iese_hover">
                <a href="<?php echo base_url() . $data_auth[FORM_URL] . DIRECTORY_SEPARATOR . IESE_ALWAYS_ON ?>">    
                    <img class="img-responsive width-100" src="<?php echo get_image_location() . CAMPAIGN_IMG . IESE_IMG . DIRECTORY_SEPARATOR; ?>always_on.jpg" alt="">
                </a>
            </div>

            <div class="col-xs-12 no-pad no-margin iese_hover">
                <a href="<?php echo base_url() . $data_auth[FORM_URL] . DIRECTORY_SEPARATOR . IESE_ENJOY_YOUR_TIME ?>">    
                    <img class="img-responsive width-100" src="<?php echo get_image_location() . CAMPAIGN_IMG . IESE_IMG . DIRECTORY_SEPARATOR; ?>enjoy_your_time.jpg" alt="">
                </a>
            </div>
            <div class="col-xs-12 no-pad no-margin iese_hover">
                <a href="<?php echo base_url() . $data_auth[FORM_URL] . DIRECTORY_SEPARATOR . IESE_TAKE_THE_MOMMENT ?>">    
                    <img class="img-responsive width-100" src="<?php echo get_image_location() . CAMPAIGN_IMG . IESE_IMG . DIRECTORY_SEPARATOR; ?>take_the_momment.jpg" alt="">
                </a>
            </div>
            <div class="col-xs-12 no-pad no-margin iese_hover">
                <a href="<?php echo base_url() . $data_auth[FORM_URL] . DIRECTORY_SEPARATOR . IESE_GET_STYLISH ?>">    
                    <img class="img-responsive width-100" src="<?php echo get_image_location() . CAMPAIGN_IMG . IESE_IMG . DIRECTORY_SEPARATOR; ?>get_stylish.jpg" alt="">
                </a>
            </div>
        </div>
        <?php
        break;
}
?>
<?php $this->load->view(LIVEVIEW . 'home/template/segment_main/js_add_product.php'); ?>

<script>
    (function ($) {
        $.fn.extend({
            formatInput: function (settings) {
                var $elem = $(this);
                settings = $.extend({
                    errback: null
                }, settings);
                $elem.bind("keyup.filter_input", $.fn.formatEvent);
            },
            formatEvent: function (e) {
                //split with the "." because of the decimal value
                var valueDecimal = initial_value.split(".");
                //formats the part without the decimal part
                var formatedValue = $.fn.insertSpaces(valueDecimal[0]);
                var finalValue;
                //checks if it has decimal values or not
                if (valueDecimal.length > 1) {
                    finalValue = formatedValue + "." + valueDecimal[1];
                } else {
                    finalValue = formatedValue;
                }
                elem.val(finalValue);
                //I had to add this line because I'm working with knockout, and it wouldn't update the value on IE
                elem.trigger('change');
            },
            insertSpaces: function (number) {
                return number.replace(" ", "").replace(/(\d)(?=(?:\d{3})+$)/g, "$1 ");
            }
        });
    })(jQuery);


    $(document).ready(function () {
        $('.numeric').bind('keyup filter_input', function () {
            $('#minrange').extends();
        });
    });

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    $('#harga').on('slide slideStart slideStop', function () {
        var range = $('#harga').val();
        var subrange = range.split(',');
        $('#minrange').val(numberWithCommas(subrange[0]));
        $('#maxrange').val(numberWithCommas(subrange[1]));
    });

    $('#harga').on('slideStop', function () {
        var price1 = $('#minrange').val().replace(/\D/g, '');
        var price2 = $('#maxrange').val().replace(/\D/g, '');
        $('#pricerange').val(price1 + "," + price2);
    })

    $('#minrange, #maxrange').on('change blur keyup', function () {
        var price1 = $('#minrange').val();
        var price2 = $('#maxrange').val();
        $('#pricerange').val(price1 + "," + price2);
    })

    $('.productDetails').mouseenter(function () {
        var title = 'title-' + $(this).attr('data-img');
        $('#' + title).removeClass('no-show');
    });

    $('.productDetails').mouseleave(function () {
        var title = 'title-' + $(this).attr('data-img');
        $('#' + title).addClass('no-show');
    });
</script>