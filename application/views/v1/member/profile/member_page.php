<?php
require_once VIEW_BASE_HOME;
$tablesort = 'desc';
?>
<div class="container">
    <div class="row">
        <div class="col-xs-3">
            <div class="profile-sidebar">
                <form id="profile-upload" name="profile-upload" method ="post" enctype ="multipart/form-data" action= "">
                    <input id="profile-image-upload" name="profile-image-upload" class="hidden" type="file" accept="image/*" onchange ="previewimg(this)">
                    <input type="hidden" name="profiler" value="1001" />
                </form>
                <div class="profile-userpic text-center">
                    <button class="btn btn-success btn-xs btn-flat btn-green pull-right m-top77" type="button" onclick="uploadfile()" id="btnupload" style="display:none;width:">Simpan perubahan
                    </button>
                    <img src="<?php echo $img_src; ?>" class="img-thumbnail picProfile" id="profile-image">
                </div>
                <div class="rank-label-container">
                    <div class="rank-label">
                        <h3 class="text-center nopadding">
                            <?php echo get_display_value($data_sel[LIST_DATA][0][0]->member_name) ?>
                        </h3>
                        <i class="fa fa-envelope">
                        </i>&nbsp;
                        <?php echo ($data_sel[LIST_DATA][0][0]) ? get_display_value($data_sel[LIST_DATA][0][0]->email) : ""; ?>
                        <br />
                        <i class="fa fa-history">
                        </i>
                        <?php echo cdate($_SESSION[SESSION_MEMBER_LAST_LOGIN], 1) ?>
                        <br />Saldo : Rp.
                        <?php echo ($data_sel[LIST_DATA][2][0]) ? number_format($data_sel[LIST_DATA][2][0]->deposit_amt) : ""; ?>
                        <?php
                        $count_msg = '';
                        if (isset($data_sel[LIST_DATA][6][0])) {
                            if ($data_sel[LIST_DATA][6][0][0]->count_msg > 0) {
                                $count_msg = ' <br /><span class="label label-danger">' . $data_sel[LIST_DATA][6][0][0]->count_msg . '</span>';
                            }
                        }
                        ?>
                    </div>
                </div>
                <div class="profile-usermenu">
                    <ul class="nav tabs-left" id="member_page">
                        <li id="tab-address" class="active">
                            <a href="#address" aria-controls="address" role="tab" data-toggle="tab" class="text-black">Alamat
                            </a>
                        </li>
                        <li id="tab-bank">
                            <a href="#bank" aria-controls="bank" role="tab" data-toggle="tab" class="text-black">Bank
                            </a>
                        </li>
                        <li id="tab-deposit">
                            <a href="#deposit" aria-controls="deposit" role="tab" data-toggle="tab" class="text-black">Tarik Deposit
                            </a>
                        </li>
                        <li id="tab-voucher">
                            <a href="#voucher" aria-controls="payment" role="tab" data-toggle="tab" class="text-black">Voucher
                            </a>
                        </li>
                        <li id="tab-order">
                            <a href="#order" aria-controls="order" role="tab" data-toggle="tab" class="text-black">Order
                            </a>
                        </li>
                        <li id="tab-review">
                            <a href="#review" aria-controls="review" role="tab" data-toggle="tab" class="text-black">Ulasan
                                <?php echo $count_msg; ?>
                            </a>
                        </li>
                        <li id="tab-return">
                            <a href="#return" aria-controls="return" role="tab" data-toggle="tab" class="text-black">Pengembalian
                            </a>
                        </li>
                        <li id="tab-contact">
                            <a href="#contact" aria-controls="contact" role="tab" data-toggle="tab" class="text-black">Info Member
                            </a>
                        </li>
                        <li id="tab-config">
                            <a href="#configuration" aria-controls="configuration" role="tab" data-toggle="tab" class="text-black">Konfigurasi
                            </a>
                        </li>
                        <!--                        <li id="tab-config">
                                                    <a href="#inbox" aria-controls="configuration" role="tab" data-toggle="tab" class="text-black">Pesan
                                                    </a>
                                                </li>-->
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-xs-9">
            <div id ="content-message">
                <?php if (isset($data_err) && $data_err[ERROR] === true) { ?>
                    <div class="alert alert-fail">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×
                        </button>
                        <?php echo $data_err[ERROR_MESSAGE][0] ?>
                    </div>
                <?php } elseif ($data_suc[SUCCESS] === true) { ?>
                    <div class="alert alert-succes-cst">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×
                        </button>
                        <?php echo (isset($data_suc[SUCCESS_MESSAGE][0]) ? $data_suc[SUCCESS_MESSAGE][0] : SAVE_DATA_SUCCESS); ?>
                    </div>
                <?php } else { ?>
                    <div id="error-message">
                    </div>
                <?php } ?>
            </div>
            <!-- Tab panes -->
            <div class="tab-content" id="myTabContent">
                <!--BASIC INFO CHANGE PROFILE-->
                <div role="tabpanel" id="contact" class="tab-pane">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-xs-12">
                            <!-- general form elements -->
                            <div class="box box-default">
                                <div class="box-header with-border">
                                    <b class="member_tittle">Info member
                                    </b>
                                </div>
                                <div class="box-body">
                                    <form class ="form-horizontal text-left" id="frmInfoMember" method ="post" action="member/update">
                                        <input name ="tab" type="hidden" value="<?php echo TAB_BASIC ?>">
                                        <div class="form-group">
                                            <label class="col-xs-3 control-label align-left">Nama
                                            </label>
                                            <div class="col-xs-8">
                                                <input type="text" class="form-control" name="member_name" value="<?php echo ($data_sel[LIST_DATA][0][0]) ? get_display_value($data_sel[LIST_DATA][0][0]->member_name) : ""; ?>" maxlength="50">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-xs-3 align-left">Jenis Kelamin
                                            </label>
                                            <div class="col-xs-8">
                                                <div class="btn-group" data-toggle="buttons">
                                                    <label class="btn btn-default
                                                           <?php echo (!isset($data_sel[LIST_DATA]) ? "active" : (($data_sel[LIST_DATA][0][0]->gender == "M") ? "active" : "")); ?>">
                                                        <span class="glyphicon glyphicon-ok" >
                                                        </span>
                                                        <input type="radio" name="gender"
                                                               <?php if ($data_sel[LIST_DATA][0][0]->gender == "M") echo "checked"; ?> value="M"> Pria
                                                    </label>
                                                    <label class="btn btn-default
                                                           <?php echo (!isset($data_sel[LIST_DATA]) ? "active" : (($data_sel[LIST_DATA][0][0]->gender == "F") ? "active" : "")); ?>">
                                                        <span class="glyphicon glyphicon-ok">
                                                        </span>
                                                        <input type="radio" name="gender"
                                                               <?php if ($data_sel[LIST_DATA][0][0]->gender == "F") echo "checked"; ?> value="F"> Wanita
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-3 control-label align-left">Tanggal Lahir
                                            </label>
                                            <div class="col-xs-8">
                                                <input class="form-control" id="datepicker" date_type="date" name ="birthday" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0][0]) ? $data_sel[LIST_DATA][0][0]->birthday : "" ); ?>" readonly="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-3 control-label align-left">Handphone
                                            </label>
                                            <div class="col-xs-8">
                                                <input class="form-control" data-inputmask="&quot;mask&quot;: &quot;9999 9999 9999&quot;" data-mask="" name ="mobile_phone" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][0][0]) ? $data_sel[LIST_DATA][0][0]->mobile_phone : "" ); ?>">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <div class="col-xs-8 col-xs-offset-3 ">
                                                <button type="submit" class="btn btn-flat btn-green pull-right" >
                                                    <i class="fa fa-save">
                                                    </i>&nbsp; Simpan
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--ADDRESS-->
                <div role="tabpanel" class="tab-pane active" id="address">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-default">
                                <div class="box-header with-border">
                                    <b class="member_tittle">Data Alamat
                                    </b>
                                    <div class="box-tools pull-right">
                                        <a href="#" onclick ="add_address()" class="color-toko1001 btn btn-flat btn-confirm">
                                            <i class="fa fa-plus">
                                            </i> Tambah Alamat Baru
                                        </a>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <?php
                                    if (isset($data_sel[LIST_DATA][1])) {
                                        $i = 0;
                                        foreach ($data_sel[LIST_DATA][1] as $row) {
                                            $i++;
                                            ?>
                                            <div class="col-xs-6" id="<?php echo $row->address_seq; ?>">
                                                <div class="box box-custom box-solid">
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title address-tittle">
                                                            <?php echo get_display_value($row->alias); ?>
                                                        </h3>
                                                        <div class="box-tools pull-right">
                                                            <button type="button" onclick ="edit_address(<?php echo $row->address_seq; ?>)" class="btn btn-box-tool btn-font-color-black">
                                                                <i class="fa fa-edit">&nbsp;Edit
                                                                </i>
                                                            </button>
                                                            <button type="button" onclick ="delete_address(<?php echo $row->address_seq; ?>)" class="btn btn-box-tool btn-font-color-black">
                                                                <i class="fa fa-trash">&nbsp;Delete
                                                                </i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div class="box-body box-body-custom">
                                                        <div class="row">
                                                            <div class="col-xs-4">
                                                                <i class="fa fa-user">
                                                                </i>&nbsp;Penerima
                                                            </div>
                                                            <div class="col-xs-1">:
                                                            </div>
                                                            <div class="col-xs-7">
                                                                <?php echo ucfirst(get_display_value($row->pic_name)); ?>
                                                            </div>
                                                            <div class="col-xs-4">
                                                                <i class="fa fa-map-marker">
                                                                </i>&nbsp;Alamat
                                                            </div>
                                                            <div class="col-xs-1">:
                                                            </div>
                                                            <div class="col-xs-7">
                                                                <?php echo ucfirst(get_display_value($row->address)); ?>
                                                            </div>
                                                            <div class="col-xs-4">
                                                                <i class="fa fa-thumb-tack">
                                                                </i>&nbsp;Kota
                                                            </div>
                                                            <div class="col-xs-1">:
                                                            </div>
                                                            <div class="col-xs-7">
                                                                <?php echo ucfirst(get_display_value($row->province_name) . " - " . ($row->city_name)); ?>
                                                            </div>
                                                            <div class="col-xs-4">
                                                                <i class="fa fa-bookmark">
                                                                </i>&nbsp;Kode Pos
                                                            </div>
                                                            <div class="col-xs-1">:
                                                            </div>
                                                            <div class="col-xs-7">
                                                                <?php echo (isset($row->zip_code) && $row->zip_code != "") ? get_display_value($row->zip_code) : " - "; ?>
                                                            </div>
                                                            <div class="col-xs-4">
                                                                <i class="fa fa-phone">
                                                                </i>&nbsp;Nomor Telp
                                                            </div>
                                                            <div class="col-xs-1">:
                                                            </div>
                                                            <div class="col-xs-7">
                                                                <?php echo ucfirst(str_replace("_", "", get_display_value($row->phone_no))); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Info Bank-->
                <div class="tab-pane" id="bank">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-default">
                                <div class="box-header with-border">
                                    <b class="member_tittle">Data Bank
                                    </b>
                                </div>
                                <div class="box-body">
                                    <form class ="form-horizontal" id="frmInfoBank"  method ="post" action="member/update">
                                        <input name ="tab" type="hidden" value="<?php echo TAB_PAYMENT ?>">
                                        <div class="form-group ">
                                            <label class="col-xs-3 text-left">Bank
                                            </label>
                                            <div class="col-xs-8">
                                                <input class="form-control" name ="bank_name" type="text" placeholder ="Bank" value ="<?php echo (isset($data_sel[LIST_DATA][2][0]) ? $data_sel[LIST_DATA][2][0]->bank_name : "" ); ?>" maxlength="25">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-3 text-left">Cabang
                                            </label>
                                            <div class="col-xs-8">
                                                <input class="form-control" name ="bank_branch_name" type="text" placeholder ="Cabang" value ="<?php echo (isset($data_sel[LIST_DATA][2][0]) ? $data_sel[LIST_DATA][2][0]->bank_branch_name : "" ); ?>" maxlength="50">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-3 text-left">Nomor Rekening
                                            </label>
                                            <div class="col-xs-8">
                                                <input class="form-control" data-inputmask="&quot;mask&quot;: &quot;9999 9999 9999 9999&quot;" data-mask="" name ="bank_acct_no" type="text" placeholder ="No. Rekening" value ="<?php echo (isset($data_sel[LIST_DATA][2][0]) ? $data_sel[LIST_DATA][2][0]->bank_acct_no : "" ); ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-3 text-left">Rekening A/N
                                            </label>
                                            <div class="col-xs-8">
                                                <input class="form-control"  name ="bank_acct_name" type="text" placeholder ="Atas Nama" value ="<?php echo (isset($data_sel[LIST_DATA][2][0]) ? str_replace("_", "", $data_sel[LIST_DATA][2][0]->bank_acct_name) : "" ); ?>" maxlength="50">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <div class="col-xs-12 ">
                                                <button type="submit" class="btn btn-flat btn-green" >
                                                    <i class="fa fa-save">
                                                    </i>&nbsp; Simpan
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--DEPOSIT-->
                <div role="tabpanel" class="tab-pane fade" id="deposit">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-default">
                                <div class="box-header with-border">
                                    <b class="member_tittle">Data Deposit
                                    </b>
                                </div>
                                <div class="box-body">
                                    <form class ="form-horizontal" id="frmDeposit" method ="post" action= "<?php echo get_base_url() ?>member/update">
                                        <input name ="tab" type="hidden" value="<?php echo TAB_DEPOSIT ?>">
                                        <div class="col-sm-12">
                                            <div class="row">
                                                <div class="form-group">
                                                    <label class="col-xs-3 text-left">Saldo
                                                    </label>
                                                    <div class="col-xs-9">
                                                        <input type="text" class="form-control" name="deposit" value="Rp. <?php echo number_format($data_sel[LIST_DATA][2][0]->deposit_amt) ?>" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-xs-3 text-left">Nominal Penarikan (Rp)
                                                    </label>
                                                    <div class="col-xs-9">
                                                        <input id="amount" class="form-control auto_int" name ="amount" type="text" validate="required[]">
                                                    </div>
                                                </div>
                                                &nbsp;
                                                <div class="form-group">
                                                    <label class="col-lg-2 text-left">
                                                        <i class="fa fa-angle-double-right ">
                                                        </i>&nbsp;Tujuan Rekening
                                                    </label>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-xs-3 text-left">Bank
                                                    </label>
                                                    <div class="col-xs-9">
                                                        <input class="form-control" name="bank_name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][2][0]) ? $data_sel[LIST_DATA][2][0]->bank_name : "" ); ?> "readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label  class="col-xs-3 control-labe text-leftl">Cabang
                                                    </label>
                                                    <div class="col-xs-9">
                                                        <input class="form-control" name="bank_branch_name" type="text"value ="<?php echo (isset($data_sel[LIST_DATA][2][0]) ? $data_sel[LIST_DATA][2][0]->bank_branch_name : "" ); ?>"readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label  class="col-xs-3 text-left">No. Rekening
                                                    </label>
                                                    <div class="col-xs-9">
                                                        <input class="form-control" name="bank_acct_no" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][2][0]) ? $data_sel[LIST_DATA][2][0]->bank_acct_no : "" ); ?>"readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label  class="col-xs-3 text-left">Rekening A/N
                                                    </label>
                                                    <div class="col-xs-9">
                                                        <input class="form-control" name="bank_acct_name" type="text" value ="<?php echo (isset($data_sel[LIST_DATA][2][0]) ? get_display_value($data_sel[LIST_DATA][2][0]->bank_acct_name) : "" ); ?>"readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <div class="col-xs-12">
                                                        <button type="submit" class="btn btn-flat btn-green">
                                                            <i class="fa fa-save">
                                                            </i>&nbsp; Simpan
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form id="frmSearch14" method="post" class="form-horizontal" url="<?php echo base_url() . "member" ?>">
                        <input type="hidden" id="start14" name="start" value="1">
                        <input type="hidden" id="length14" name="length" value="10">
                        <input type="hidden" name="order" value="desc">
                        <input type="hidden" name="column" value="trx_date">
                        <input type="hidden" name="btnSearch" value="true">
                        <div class="col-xs-12" id="tabledata14">
                        </div>
                    </form>
                </div>
                <!--VOUCHER-->
                <div role="tabpanel" class="tab-pane fade" id="voucher">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-default">
                                <div class="box-header with-border">
                                    <b class="member_tittle">Data Voucher
                                    </b>
                                </div>
                                <div class="box-body">
                                    <?php if (isset($data_sel[LIST_DATA][5])) : ?>
                                        <?php foreach ($data_sel[LIST_DATA][5] as $row) : ?>
                                            <div class="col-xs-6">
                                                <div class="contact-box-v">
                                                    <div class="row">
                                                        <center>
                                                            <b>
                                                                <?php echo $row->code ?>
                                                            </b>
                                                        </center>
                                                    </div>
                                                    <hr class="mb-5px mt-5px">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <div class="row text-red text-center">
                                                                <h4>
                                                                    <?php echo 'Rp. ' . cnum($row->nominal) ?>
                                                                </h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr class="mt-5px mb-10px">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <div class="col-xs-5 no-pad">
                                                                Aktif
                                                            </div>
                                                            <div class="col-xs-7 no-pad">
                                                                &nbsp;:&nbsp;
                                                                <?php echo cdate($row->active_date) ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <div class="col-xs-5 no-pad">
                                                                Kadaluarsa
                                                            </div>
                                                            <div class="col-xs-7 no-pad">
                                                                &nbsp;:&nbsp;
                                                                <?php echo cdate($row->exp_date) ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <div class="col-xs-5 no-pad">
                                                                Minimum Belanja
                                                            </div>
                                                            <div class="col-xs-7 no-pad">
                                                                &nbsp;:&nbsp;
                                                                <?php echo 'Rp. ' . cnum($row->trx_use_amt) ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <div class="col-xs-5 no-pad">
                                                                Terpakai di Order
                                                            </div>
                                                            <div class="col-xs-7 no-pad">
                                                                <?php if ($row->trx_no == ""): ?>
                                                                    &nbsp;:&nbsp;-
                                                                <?php else: ?>
                                                                    &nbsp;:&nbsp;
                                                                    <a  target="<?php echo BLANK_TARGET ?>" href="<?php echo base_url() . 'member/payment/' . $row->trx_no ?>">
                                                                        <?php echo $row->trx_no ?>
                                                                    </a>
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--ORDER-->
                <div role="tabpanel" class="tab-pane fade" id="order">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-default">
                                <div class="box-header with-border">
                                    <b class="member_tittle">Info Order
                                    </b>
                                </div>
                                <div class="box-body">
                                    <form class ="form-horizontal" id="frmSearch2" method="post" url="<?php echo get_base_url() ?>member">
                                        <input type="hidden" id="start2" name="start" value="1">
                                        <input type="hidden" id="length2" name="length" value="10">
                                        <input type="hidden" name="order" value="desc">
                                        <input type="hidden" name="column" value="order_date">
                                        <input type="hidden" name="btnSearch2" value="true">
                                        <div class="form-group col-xs-3">
                                            <label>Nomor Order</label>
                                            <input class="form-control" type="text" name="order_no" id="search_order_no"  placeholder="Nomor order">
                                        </div>
                                        <div class="form-group col-xs-2">
                                            <label>Tanggal Order</label>
                                            <input class="form-control orderdate" type="text" id="order_date_s" name="order_date_s" placeholder="Pilih tanggal" value="" readonly>
                                        </div>
                                        <div class="form-group col-xs-2">
                                            <label>s / d Tanggal</label>
                                            <input class="form-control orderdate" type="text" id="order_date_e" name="order_date_e" placeholder="Pilih tanggal" value="" readonly>
                                        </div>
                                        <div class="form-group row col-xs-4" >
                                            <label>Status</label>
                                            <select class="form-control" name="payment_status">
                                                <option value="">Semua</option>
                                                <?php echo combostatus(json_decode(STATUS_PAYMENT)); ?>
                                            </select>
                                        </div>
                                        <div class="col-xs-1">
                                            <label>&nbsp;</label>
                                            <button type="button" id="btnSearch3" onclick="listtable('2')" class="form-group btn btn-flat btn-default btn-height-1">
                                                <i class="fa fa-search"></i>&nbsp; Cari
                                            </button>
                                        </div>
                                        <div class="col-xs-12" id="tabledata2">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--CONFIG-->
                <div role="tabpanel" class="tab-pane fade" id="configuration">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-default">
                                <div class="box-header with-border">
                                    <b class="member_tittle">Info Konfigurasi
                                    </b>
                                </div>
                                <div class="box-body">
                                    <form class="form-horizontal text-left" id="frmConfig" method="post" action="<?php echo base_url() ?>member/update">
                                        <input name ="tab" type="hidden" value="<?php echo TAB_CONFIG ?>">
                                        <div class="form-group ">
                                            <label  class="col-xs-3 text-left">Email
                                            </label>
                                            <div class="col-xs-9">
                                                <input type="email" class="form-control" value="<?php echo ($data_sel[LIST_DATA][0][0]->email) ?>" disabled="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label  class="col-xs-3 text-left">Password Lama
                                            </label>
                                            <div class="col-xs-9">
                                                <input type="password" class="form-control" name="old_password">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label  class="col-xs-3 text-left">Password Baru
                                            </label>
                                            <div class="col-xs-9">
                                                <input type="password" class="form-control" id="newpass" name="new_password">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label  class="col-xs-3 text-left">Konfirmasi Password
                                            </label>
                                            <div class="col-xs-9">
                                                <input type="password" class="form-control" name="confirm_password">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <div class="col-xs-12">
                                                <button type="submit" class="btn btn-flat btn-green">
                                                    <i class="fa fa-save">
                                                    </i>&nbsp; Simpan
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--RETURN-->
                <div role="tabpanel" class="tab-pane fade" id="return">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-default">
                                <div class="box-header with-border">
                                    <b class="member_tittle">Info Retur
                                    </b>
                                </div>
                                <div class="box-body">
                                    <div class="form-group">
                                        <button id="btnadd" class="btn btn-flat btn-confirm">Tambah
                                        </button>
                                    </div>
                                    <form class="form-horizontal text-left" id="frmReturn" method="post" action="<?php echo base_url() ?>member/product_return">
                                        <div id="search_order" class="no-display">
                                            <hr>
                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label class="control-label col-xs-3 text-left">No. Order
                                                    </label>
                                                    <div class="col-xs-9">
                                                        <div class="input-group " >
                                                            <input id="r_order_no" class="form-control text-left" type="text" name ="r_order_no" >
                                                            <span class="input-group-btn">
                                                                <button id="btnSearch4" type="button" class="form-group btn btn-flat btn-default btn-height-1 reset-margin">
                                                                    <i class="fa fa-search">
                                                                    </i>&nbsp; Cari
                                                                </button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <table id="tbl4" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr class="display table">
                                                        <th column="img" class="width-20"> Produk
                                                        </th>
                                                        <th column="qty"> Qty
                                                        </th>
                                                        <th column="checked">Pilih
                                                        </th>
                                                        <th column="sell_price"> Harga Produk (Rp)
                                                        </th>
                                                        <th column="merchant_name"> Merchant
                                                        </th>
                                                    </tr>
                                                </thead>
                                            </table>
                                            <hr>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="control-label col-lg-3 text-left">Expedisi
                                                    </label>
                                                    <div class="col-lg-9">
                                                        <?php include get_component_url() . '/dropdown/com_drop_expedition.php' ?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-3 text-left">No. Resi
                                                    </label>
                                                    <div class="col-xs-9">
                                                        <input  id="r_order_no" class="form-control" type="text" name ="r_resi_no"  validate="required[]">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-3 text-left">Komentar
                                                    </label>
                                                    <div class="col-xs-9">
                                                        <textarea rows="3" class="form-control text-area-fixed" name="r_comment"></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-lg-12">
                                                        <button type="submit" name="btnSaveAdd" class="btn btn-flat btn-green">
                                                            <i class="fa fa-save">
                                                            </i>&nbsp; Simpan
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                        </div>
                                        <br>
                                        <input name ="tab" type="hidden" >
                                    </form>
                                    <form class ="form-horizontal" id="frmSearch3" method="post" url="<?php echo get_base_url() ?>member/product_return">
                                        <input type="hidden" id="start3" name="start" value="1">
                                        <input type="hidden" id="length3" name="length" value="5">
                                        <input type="hidden" name="order" value="desc">
                                        <input type="hidden" name="column" value="created_date">
                                        <input type="hidden" name="btnSearch" value="true">
                                        <div class="col-xs-12" id="tabledata3">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--INBOX-->
                <!--                <div role="tabpanel" class="tab-pane fade" id="inbox">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="box box-default">
                                                <div class="box-header with-border">
                                                    <b class="member_tittle">Info Pesan
                                                    </b>
                                                </div>
                                                <div class="box-body">
                                                    <div class="form-group pull-right">
                                                        <button id="btnaddinbox" class="btn btn-flat btn-confirm" onclick="newmsg(0)">
                                                            <i class="fa fa-plus">
                                                            </i> Tulis Pesan Baru
                                                        </button>
                                                    </div>
                                                    <form class ="form-horizontal" id="frmInbox" name="frmInbox" method="post" url="<?php echo get_base_url(); ?>member/update">
                                                        <input type="hidden" id="prev_message_seq" name="prev_message_seq" value="0">
                                                        <input name ="tab" type="hidden" value="<?php echo TAB_MESSAGE ?>">
                                                         Modal 
                                                        <div class="modal fade" id="InboxModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;
                                                                            </span>
                                                                        </button>
                                                                        <h4 class="modal-title" id="myModalLabel">Pesan Baru
                                                                        </h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <textarea id="message_text" name="message_text" style="resize: none;" class="full-width m-height">
                                                                        </textarea>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal
                                                                        </button>
                                                                        <button type="button" class="btn btn-primary" onclick="savemsg()">Kirim Pesan
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <form class ="form-horizontal" id="frmSearch11" method="post" url="<?php echo get_base_url(); ?>member">
                                                        <input type="hidden" id="start11" name="start" value="1">
                                                        <input type="hidden" id="length11" name="length" value="10">
                                                        <input type="hidden" name="order" value="desc">
                                                        <input type="hidden" name="column" value="created_date">
                                                        <input type="hidden" name="btnSearch11" value="true">
                                                        <div class="col-xs-12" id="tabledata11">
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>-->
                <!--REVIEW-->
                <div role="tabpanel" class="tab-pane fade" id="review">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-default">
                                <div class="box-header with-border">
                                    <b class="member_tittle">Info Ulasan
                                    </b>
                                </div>
                                <div class="box-body">
                                    <form class="form-horizontal text-left" id="frmReview" method="post" action="member/update">
                                        <input name ="tab" type="hidden" value="<?php echo TAB_REVIEW; ?>">
                                        <ul class="list-group">
                                            <?php
                                            if (isset($data_sel[LIST_DATA]['sel'][1])) {
                                                $i = 0;
                                                foreach ($data_sel[LIST_DATA]['sel'][1] as $value) {
                                                    $i++;
                                                    ?>
                                                    <li class="list-group-item">
                                                        <input type="hidden" name="orseq[]" value="<?php echo $value->order_seq; ?>" />
                                                        <input type="hidden" name="pvseq[]" value="<?php echo $value->seq; ?>" />
                                                        <div class="row">
                                                            <div class="col-xs-2 wd-auto ml-3px">
                                                                <a href ="<?php echo base_url() . url_title($value->product_name . " " . $value->seq); ?>">
                                                                    <img  src="<?php echo  PRODUCT_UPLOAD_IMAGE . $value->merchant_seq . "/" . XS_IMAGE_UPLOAD . $value->pic_1_img; ?>" class = "img-thumbnail img-width-65px">
                                                                </a>
                                                            </div>
                                                            <div class="col-xs-5">
                                                                <a class="cart-font-color-blue" href ="<?php echo base_url() . url_title($value->product_name . " " . $value->seq); ?>">
                                                                    <b>
                                                                        <?php echo $value->product_name; ?>
                                                                    </b>
                                                                </a>
                                                                <dl class="dl-horizontal">
                                                                    <dt>No. Order :
                                                                    </dt>
                                                                    <dd>
                                                                        <?php echo $value->order_no; ?>
                                                                    </dd>
                                                                    <dt>Tanggal Order :
                                                                    </dt>
                                                                    <dd>
                                                                        <?php echo cdate($value->order_date); ?>
                                                                    </dd>
                                                                </dl>
                                                            </div>
                                                            <div class="col-xs-4">
                                                                <textarea name="review_text[]" class="form-control ulasan-textarea" cols="30" rows="2" placeholder="Ulasan Anda">
                                                                </textarea>
                                                            </div>
                                                            <div class="col-xs-3 mt-5px">
                                                                <div class="row">
                                                                    <div class="col-xs-12">
                                                                        <input type="hidden" class="rating" data-filled="fa fa-star fa-2x color-star-1" data-empty="fa fa-star-o fa-2x color-star-1" data-start="0" data-stop="5"/>
                                                                        <br class="mb-10px"/>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xs-12 mt-5px">
                                                                        <button type="submit" class="btn btn-confirm">Kirim Ulasan
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </ul>
                                    </form>
                                    <form class ="form-horizontal" id="frmSearch10" method="post" url="<?php echo get_base_url() ?>member">
                                        <input type="hidden" id="start10" name="start" value="1">
                                        <input type="hidden" id="length10" name="length" value="10">
                                        <input type="hidden" name="order" value="desc">
                                        <input type="hidden" id="column10" name="column" value="product_name">
                                        <input type="hidden" name="btnSearch10" value="true">
                                        <div class="col-xs-12" id="tabledata10">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
                        var count_msg = '<?php echo $count_msg ?>';
                        var table = '';
                        var table4 = '';
                        var table10 = '';
                        var sorting = '<?php echo isset($tablesort); ?>';
                        var error_html = '<div class="alert alert-error-home"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                        var success_html = '<div class="alert alert-success-home"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                        $(document).ready(function() {
                            $('.rating').each(function() {
                                $('<input type="hidden" name="star[]" value="0" class="star" />')
                                        .insertAfter(this);
                            }
                            );
                            $('.rating').on('change', function() {
                                $(this).next('.star').val($(this).val());
                            }
                            );
                            var url = $('#frmFill').attr('url');
                            var url2 = $('#frmSearch2').attr('url');
                            var columns_detail = [];
                            $('#tbl thead th').each(function() {
                                var sName = $(this).attr("column");
                                var bVisible = $(this).attr("visible");
                                var url = $(this).attr("url");
                                columns_detail.push({
                                    "sName": sName, "bVisible": bVisible, "mData": sName}
                                );
                            }
                            );
                            var columns_detail10 = [];
                            $('#tbl10 thead th').each(function() {
                                var sName = $(this).attr("column");
                                var bVisible = $(this).attr("visible");
                                var url10 = $(this).attr("url");
                                columns_detail10.push({
                                    "sName": sName, "bVisible": bVisible, "mData": sName}
                                );
                            }
                            );
                            var columns_detail14 = [];
                            $('#tbl14 thead th').each(function() {
                                var sName = $(this).attr("column");
                                var bVisible = $(this).attr("visible");
                                var url14 = $(this).attr("url");
                                columns_detail10.push({
                                    "sName": sName, "bVisible": bVisible, "mData": sName}
                                );
                            }
                            );
                            $("ul#member_page li a[data-toggle=\"tab\"], ul#member_page_xs li a[data-toggle=\"tab\"]").on("shown.bs.tab", function(e) {
                                var tab_name = ($(this).attr('href'));
                            }
                            );
                            function handleAjaxError(request, textStatus, error) {
                                if (textStatus === 'timeout') {
                                    alert('The server took too long to send the data.');
                                }
                                else {
                                    var response = error_response(request.status);
                                    $('#error-message').html(error_html + " " + response + "</div>");
                                    $('div#tbl_processing').css('visibility', 'hidden');
                                }
                            }
                            function handleAjaxSuccess(response) {
                                alert(response);
                            }
                            function buildAjaxData() {
                                var settings = $("#tbl").dataTable().fnSettings();
                                var search = [];
                                search = $('#frmFill').serializeArray();
                                search.push(
                                        {
                                            name: "draw", value: settings.iDraw}
                                , {
                                    name: "start", value: settings._iDisplayStart}
                                ,
                                        {
                                            name: "length", value: settings._iDisplayLength}
                                ,
                                        {
                                            name: "column", value: columns_detail[settings.aaSorting[0][0]].sName}
                                ,
                                        {
                                            name: "order", value: settings.aaSorting[0][1]}
                                ,
                                        {
                                            name: "btnSearch", value: "true"}
                                );
                                return search;
                            }
                            $('#tbl tbody').on('click', 'tr', function() {
                                if ($(this).hasClass('selected')) {
                                    $(this).removeClass('selected');
                                }
                                else {
                                    table.$('tr.selected').removeClass('selected');
                                    $(this).addClass('selected');
                                }
                            }
                            );
                        }
                        );
                        $('#frmConfig').validate({
                            highlight: function(element) {
                                $(element).closest('.form-group').addClass('has-error');
                            }
                            ,
                            unhighlight: function(element) {
                                $(element).closest('.form-group').removeClass('has-error');
                            }
                            ,
                            errorElement: 'span',
                            errorClass: 'help-block',
                            errorPlacement: function(error, element) {
                                if (element.parent('.input-group').length) {
                                    error.insertAfter(element.parent());
                                }
                                else {
                                    error.insertAfter(element);
                                }
                            }
                            ,
                            rules: {
                                old_password: {
                                    required: true,
                                    maxlength: 20
                                }
                                , new_password: {
                                    required: true,
                                    minlength: 8,
                                    maxlength: 20
                                }
                                ,
                                confirm_password: {
                                    equalTo: "#newpass",
                                    required: true,
                                    minlength: 8,
                                    maxlength: 20
                                }
                            }
                            ,
                        }
                        );

                        $(document).ready(function() {
                            $('#order_date_s').val('');
                            $('#order_date_e').val('');
                            listtable('2');
                        });
                        $('.orderdate').daterangepicker({
                            "locale": {
                                "format": "DD-MMM-YYYY"
                            },
                            singleDatePicker: true,
                            showDropdowns: true
                        });


                        $('#reservation').daterangepicker({
                            locale: {
                                format: 'DD-MMM-YYYY'}
                        }
                        );
                        $("[data-mask]").inputmask();
                        function edit_address(address_seq) {
                            $.ajax({
                                url: "<?php echo base_url() ?>member/address/edt",
                                type: "POST",
                                data: {
                                    address_seq: address_seq}
                                , success: function(response) {
                                    if (isSessionExpired(response)) {
                                        response_object = json_decode(response);
                                        url = response_object.url;
                                        location.href = url;
                                    }
                                    else {
                                        $('#address_modal').modal('hide');
                                        $("#address_modal").remove();
                                        $('.modal-backdrop').remove();
                                        $(response).modal('show');
                                    }
                                }
                                ,
                                error: function(request, error) {
                                    alert(error_response(request.status));
                                }
                            }
                            )
                        }
                        function add_address() {
                            $.ajax({
                                url: "<?php echo base_url() ?>member/address/edt",
                                type: "POST", success: function(response) {
                                    if (isSessionExpired(response)) {
                                        response_object = json_decode(response);
                                        url = response_object.url;
                                        location.href = url;
                                    }
                                    else {
                                        $('#address_modal').modal('hide');
                                        $("#address_modal").remove();
                                        $('.modal-backdrop').remove();
                                        $(response).modal('show');
                                    }
                                }
                                ,
                                error: function(request, error) {
                                    alert(error_response(request.status));
                                }
                            }
                            )
                        }
                        function delete_address(address_seq) {
                            if (address_seq == undefined) {
                                alert("Harap pilih data dulu !");
                            }
                            else {
                                if (confirm("Apakah anda yakin menghapus data yang dipilih ?")) {
                                    $.ajax({
                                        url: "<?php echo base_url() ?>member/address/del",
                                        type: "POST",
                                        data: {
                                            address_seq: address_seq}
                                        ,
                                        success: function(response) {
                                            if (isSessionExpired(response)) {
                                                response_object = json_decode(response);
                                                url = response_object.url;
                                                location.href = url;
                                            }
                                            else {
                                                $('#' + address_seq).slideUp(1000);
                                            }
                                        }
                                        ,
                                        error: function(request, error) {
                                            alert(error_response(request.status));
                                        }
                                    }
                                    )
                                }
                            }
                        }
                        var isValid = $('#frmMain').validate({
                            ignore: 'input[type=hidden]',
                            highlight: function(element) {
                                $(element).closest('.form-group').addClass('has-error');
                            }
                            ,
                            unhighlight: function(element) {
                                $(element).closest('.form-group').removeClass('has-error');
                            }
                            ,
                            errorElement: 'span',
                            errorClass: 'help-block',
                            errorPlacement: function(error, element) {
                                if (element.parent('.input-group').length) {
                                    error.insertAfter(element.parent());
                                }
                                else if (element.hasClass('select2')) {
                                    error.insertAfter(element.next('span'));
                                }
                                else {
                                    error.insertAfter(element);
                                }
                            }
                        }
                        );
                        $(function() {
                            $('#profile-image').on('click', function() {
                                $('#profile-image-upload').click();
                            }
                            );
                        }
                        );
                        function previewimg(thisval) {
                            if (thisval.files && thisval.files[0]) {
                                var reader = new FileReader();
                                reader.onload = function(e) {
                                    $('#profile-image').attr('src', e.target.result).width('165').height('125');
                                }
                                reader.readAsDataURL(thisval.files[0]);
                            }
                            $("#btnupload").show();
                        }
                        function uploadfile() {
                            $("#profile-upload").submit();
                        }
                        $('#frmInfoBank').validate({
                            highlight: function(element) {
                                $(element).closest('.form-group').addClass('has-error');
                            }
                            ,
                            unhighlight: function(element) {
                                $(element).closest('.form-group').removeClass('has-error');
                            }
                            ,
                            errorElement: 'span',
                            errorClass: 'help-block',
                            errorPlacement: function(error, element) {
                                if (element.parent('.input-group').length) {
                                    error.insertAfter(element.parent());
                                }
                                else {
                                    error.insertAfter(element);
                                }
                            }
                            ,
                            rules: {
                                bank_name: {
                                    required: true
                                }
                                ,
                                bank_branch_name: {
                                    required: true
                                }
                                ,
                                bank_acct_no: {
                                    required: true
                                }
                                ,
                                bank_acct_name: {
                                    required: true
                                }
                            }
                        }
                        );
                        $('#frmInfoMember').validate({
                            highlight: function(element) {
                                $(element).closest('.form-group').addClass('has-error');
                            }
                            ,
                            unhighlight: function(element) {
                                $(element).closest('.form-group').removeClass('has-error');
                            }
                            ,
                            errorElement: 'span',
                            errorClass: 'help-block',
                            errorPlacement: function(error, element) {
                                if (element.parent('.input-group').length) {
                                    error.insertAfter(element.parent());
                                }
                                else {
                                    error.insertAfter(element);
                                }
                            }
                            ,
                            rules: {
                                member_name: {
                                    required: true
                                }
                                ,
                                gender: {
                                    required: true
                                }
                                ,
                                birthday: {
                                    required: true
                                }
                                ,
                                mobile_phone: {
                                    required: true
                                }
                            }
                        }
                        );
                        $('#frmDeposit').validate({
                            highlight: function(element) {
                                $(element).closest('.form-group').addClass('has-error');
                            }
                            ,
                            unhighlight: function(element) {
                                $(element).closest('.form-group').removeClass('has-error');
                            }
                            ,
                            errorElement: 'span',
                            errorClass: 'help-block',
                            errorPlacement: function(error, element) {
                                if (element.parent('.input-group').length) {
                                    error.insertAfter(element.parent());
                                }
                                else {
                                    error.insertAfter(element);
                                }
                            }
                            ,
                            rules: {
                                amount: {
                                    required: true
                                }
                            }
                        }
                        );
                        $(document).on('submit', '#frmFill', function(e) {
                            if (isFrmFillValid.valid()) {
                                $('input[type="text"]').each(function() {
                                    if ($(this).hasClass('auto') || $(this).hasClass('auto_int') || $(this).hasClass('auto_dec'))
                                    {
                                        var v = $(this).autoNumeric('get');
                                        $(this).autoNumeric('destroy');
                                        $(this).val(v);
                                    }
                                }
                                );
                            }
                        }
                        );
                       
                        $('.auto_int').autoNumeric('init', {
                            vMin: 0, mDec: 0}
                        );
                        $(document).ready(function() {
                            $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
                                localStorage.setItem('activeTab', $(e.target).attr('href'));
                            }
                            );
                            var activeTab = localStorage.getItem('activeTab');
                            if (activeTab) {
                                $('#member_page a[href="' + activeTab + '"]').tab('show');
                            }
                        }
                        );
                        $(document).ready(function() {
                            $("#btnadd").click(function() {
                                $("#search_order").toggle();
                            }
                            );
                            $.validator.messages.equalTo = "Harap diisi sama dengan password baru";
                        }
                        );
                        var url4 = $('#frmReturn').attr('action');
                        var columns_detail4 = [];
                        $('#tbl4 thead th').each(function() {
                            var sName = $(this).attr("column");
                            var bVisible = $(this).attr("visible");
                            columns_detail4.push({
                                "sName": sName, "bVisible": bVisible, "mData": sName}
                            );
                        }
                        );
                        $('#btnSearch4').on('click', function() {
                            if (!$.fn.DataTable.isDataTable('#tbl4')) {
                                table4 = $('#tbl4').DataTable({
                                    "oLanguage": {
                                        sProcessing: "<img src='<?php echo get_image_location() . ASSET_IMG_HOME ?>loading.gif' />"
                                    }
                                    ,
                                    "dom": '<"top"l>rt<"bottom"ip><"clear">',
                                    responsive: true,
                                    "processing": true,
                                    "serverSide": true, "aoColumns": columns_detail4, "bLengthChange": false,
                                    "ajax": {
                                        "url": url4,
                                        "type": 'POST',
                                        "dataType": 'JSON',
                                        "data": buildAjaxData4,
                                        "error": handleAjaxError4,
                                    }
                                }
                                )
                                        .on('xhr.dt', function(e, settings, response, xhr) {
                                    response = JSON.stringify(response);
                                    if (isSessionExpired(response)) {
                                        response_object = json_decode(response);
                                        url = response_object.url;
                                        location.href = url;
                                    }
                                }
                                );
                            }
                            table4.ajax.reload();
                        }
                        );
                        function reset_other_checkbox(active_checkbox) {
                            if ($('#printc_' + active_checkbox).is(':checked')) {
                                $('[id^=printc]:not(#printc_' + active_checkbox + ')').attr("disabled", true);
                                $('[id^=prints]:not(#prints_' + active_checkbox + ')').attr("disabled", true);
                                $('[id^=printc]').prop('checked', false);
                                $('#printc_' + active_checkbox).prop('checked', true);
                            }
                            else {
                                $('[id^=printc]').attr("disabled", false);
                                $('[id^=prints]').attr("disabled", false);
                                $('#printc_' + active_checkbox).prop('checked', false);
                            }
                        }
                        function handleAjaxError4(request, textStatus, error) {
                            if (textStatus === 'timeout') {
                                alert('The server took too long to send the data.');
                            }
                            else {
                                var response = error_response(request.status);
                                $('#error-message').html(error_html + " " + response + "</div>");
                                $('div#tbl_processing').css('visibility', 'hidden');
                            }
                        }
                        function buildAjaxData4() {
                            var r_order_no = $('#r_order_no').val();
                            var settings = $("#tbl4").dataTable().fnSettings();
                            var search = [];
                            search = $('#frmReturn').serializeArray();
                            search.push(
                                    {
                                        name: "draw", value: settings.iDraw}
                            ,
                                    {
                                        name: "start", value: settings._iDisplayStart}
                            ,
                                    {
                                        name: "length", value: settings._iDisplayLength}
                            ,
                                    {
                                        name: "column", value: columns_detail4[settings.aaSorting[0][0]].sName}
                            ,
                                    {
                                        name: "order", value: settings.aaSorting[0][1]}
                            ,
                                    {
                                        name: "btnAdditional", value: "true"}
                            ,
                                    {
                                        name: "r_order_no", value: r_order_no}
                            )
                            return search;
                        }
                        $('#tb4 tbody').on('click', 'tr', function() {
                            if ($(this).hasClass('selected')) {
                                $(this).removeClass('selected');
                            }
                            else {
                                table4.$('tr.selected').removeClass('selected');
                                $(this).addClass('selected');
                            }
                        }
                        );
                        var isValid = $('#frmReturn').validate({
                            ignore: 'input[type=hidden]',
                            highlight: function(element) {
                                $(element).closest('.form-group').addClass('has-error');
                            }
                            ,
                            unhighlight: function(element) {
                                $(element).closest('.form-group').removeClass('has-error');
                            }
                            ,
                            errorElement: 'span',
                            errorClass: 'help-block',
                            errorPlacement: function(error, element) {
                                if (element.parent('.input-group').length) {
                                    error.insertAfter(element.parent());
                                }
                                else if (element.hasClass('select2')) {
                                    error.insertAfter(element.next('span'));
                                }
                                else {
                                    error.insertAfter(element);
                                }
                            }
                        }
                        );
                        $(document).on('submit', '#frmReturn', function(e) {
                            if (isValid.valid()) {
                                $('input[type="text"]').each(function() {
                                    if ($(this).hasClass('auto') || $(this).hasClass('auto_int') || $(this).hasClass('auto_dec'))
                                    {
                                        var v = $(this).autoNumeric('get');
                                        $(this).autoNumeric('destroy');
                                        $(this).val(v);
                                    }
                                }
                                );
                            }
                        }
                        );
                        function shipment(return_no) {
                            var url_action = $('#frmReturn').attr('action');
                            var data = [];
                            if (return_no == undefined) {
                                alert("Harap pilih data dulu !");
                            }
                            else {
                                data.push(
                                        {
                                            name: "btnSaveEdit", value: true}
                                ,
                                        {
                                            name: "return_no", value: return_no}
                                );
                                if (confirm("Apakah anda yakin terima data yang dipilih ?")) {
                                    $.ajax({
                                        url: url_action,
                                        type: "POST",
                                        data: data,
                                        dataType: "json",
                                        success: function(response) {
                                            if (isSessionExpired(response)) {
                                                response_object = json_decode(response);
                                                url = response_object.url_action;
                                                location.href = url_action;
                                            }
                                            else {
                                                if (response.error == true) {
                                                    $('#error-message').html(error_html + response.message + "</div>");
                                                }
                                                else {
                                                    $('#error-message').html(success_html + "Data telah berhasil di simpan !" + "</div>");
                                                    listretur(3);
                                                }
                                                ;
                                            }
                                        }
                                        ,
                                        error: function(request, error) {
                                            $('#error-message').html(error_html + " Data Tidak Berhasil Diproses ! </div>");
                                            listretur(3);
                                        }
                                    }
                                    );
                                }
                            }
                        }
                        function reviewlist(idf) {
                            var urls = $("#frmSearch" + idf);
                            var htmls = '';
                            var pagings = '';
                            $.ajax({
                                url: urls.attr('url'), data: urls.serialize(), type: 'POST', datatype: 'json',
                                success: function(JSONString) {
                                    var dataobj = $.parseJSON('[' + JSONString + ']');
                                    $.each(dataobj, function() {
                                        var realdata = this['aaData'];
                                        $.each(realdata, function() {
                                            htmls += '<div class="row review">' +
                                                    '<div class="col-xs-3 wd-auto">' + this['product_name'] + '</div>' +
                                                    '<div class="col-xs-7"><div class="row">' + this['order_date'] + '</div>' +
                                                    '<div class="row">' + this['rate'] + '</div></div>' +
                                                    '</div><div class="height5">&nbsp;</div>';
                                        }
                                        );
                                        var recorddata = this['iTotalRecords'];
                                        pagings = paging(recorddata, idf, "reviewlist");
                                    }
                                    );
                                    $('#tabledata' + idf).html(htmls + pagings);
                                }
                            }
                            );
                        }
                        reviewlist(10);
                        function listtable(idf) {
                            var urls = $("#frmSearch" + idf);
                            var htmls = '';
                            var pagings = '';
                            $.ajax({
                                url: urls.attr('url'), data: urls.serialize(), type: 'POST', datatype: 'json',
                                success: function(JSONString) {
                                    var dataobj = $.parseJSON('[' + JSONString + ']');
                                    $.each(dataobj, function() {
                                        var realdata = this['aaData'];
                                        $.each(realdata, function() {
                                            htmls += '<div class="review"><div class="row"><div class="col-xs-5">NO. INVOICE : <b>' + this['order_no'] + '</b><br />Tanggal Order : ' + this['order_date'] + '</div><div class="col-xs-2">TOTAL <br />' + this['total_order'] + '</div><div class="col-xs-3">PEMBAYARAN<br />' + this['payment_method'] + '</div><div class="col-xs-2">' + this['payment_status'] + '</div></div><div><br />&nbsp;</div></div><br>';
                                        }
                                        );
                                        var recorddata = this['iTotalRecords'];
                                        pagings = paging(recorddata, idf, "listtable");
                                    }
                                    );
                                    $('#tabledata' + idf).html(htmls + pagings);
                                }
                            }
                            );
                        }
                        function paging(recorddata, idf, functioname) {
                            var lengbar = $("#length" + idf).val();
                            var halaman = $("#start" + idf).val();
                            var clas = ''
                            var pagings = '';
                            if (Number(recorddata) > Number(lengbar)) {
                                pagings = '<center><ul class="pagination pagination-sm no-margin">';
                                y = 0;
                                for (i = 1; i <= recorddata; i += Number(lengbar)) {
                                    y++;
                                    if (i == halaman) {
                                        clas = ' class="text-red text-bold"';
                                    }
                                    else {
                                        clas = '';
                                    }
                                    pagings += "<li><a href='javascript:gopage(" + i + "," + idf + "," + functioname + ")' " + clas + ">" + y + "</a></li>";
                                }
                                pagings += "</ul></center>";
                            }
                            return pagings;
                        }
                        function gopage(hal, idf, functioname) {
                            $("#start" + idf).val(hal);
                            functioname(idf);
                        }
                        function listretur(idf) {
                            var urls = $("#frmSearch" + idf);
                            var htmls = '';
                            var pagings = '';
                            $.ajax({
                                url: urls.attr('url'), data: urls.serialize(), type: 'POST', datatype: 'json',
                                success: function(JSONString) {
                                    var dataobj = $.parseJSON('[' + JSONString + ']');
                                    $.each(dataobj, function() {
                                        var realdata = this['aaData'];
                                        $.each(realdata, function() {
                                            htmls += '<div class="well"><div class="row"><div class="col-xs-4">' + this['product_name'] + '</div>';
                                            htmls += '<div class="col-xs-6"><dl class="dl-horizontal"><dt>No. Retur</dt><dd><b>' + this['return_no'] + '</b></dd><dt>No. Order</dt><dd>' + this['order_no'] + '</dd><dt>Qty</dt><dd>' + this['qty'] + '</dd><dt>Tgl. Pengajuan</dt><dd>' + this['created_date'] + '</dd><dt>No. Resi Member</dt><dd>' + this['awb_member_no'] + '</dd><dt>Status Pengembalian</dt><dd>' + this['return_status'] + '</dd><dt>Status Pengiriman</dt><dd>' + this['shipment_status'] + '</dd></dl></div>';
                                            htmls += '<div class="col-xs-2"><p>' + this['accepted'] + '</p></div></div><div class="row">';
                                            htmls += '<div class="col-xs-4"><blockquote class="blsmall"><small>Komentar Member</small><p>' + this['review_member'] + '</p></blockquote></div>';
                                            htmls += '<div class="col-xs-4"><blockquote class="blsmall"><small>Komentar Admin</small><p>' + this['review_admin'] + '</p></blockquote></div>';
                                            htmls += '<div class="col-xs-4"><blockquote class="blsmall"><small>Komentar Merchant</small><p>' + this['review_merchant'] + '</p></blockquote></div></div></div>';
                                        }
                                        );
                                        var recorddata = this['iTotalRecords'];
                                        pagings = paging(recorddata, idf, "listretur");
                                    }
                                    );
                                    $('#tabledata' + idf).html(htmls + pagings);
                                }
                            }
                            );
                        }
                        listretur(3);
                        function listdeposit(idf) {
                            var urls = $("#frmSearch" + idf);
                            var htmls = '';
                            var pagings = '';
                            $.ajax({
                                url: urls.attr('url'), data: urls.serialize(), type: 'POST', datatype: 'json',
                                success: function(JSONString) {
                                    var dataobj = $.parseJSON('[' + JSONString + ']');
                                    $.each(dataobj, function() {
                                        var realdata = this['aaData'];
                                        $.each(realdata, function() {
                                            htmls += '<div class="row deposit">' +
                                                    '<div class="row">' +
                                                    '<div class="col-xs-4">Tanggal : ' + this['trx_date'] + '</div>' +
                                                    '<div class="col-xs-4">No Transaksi : ' + this['trx_no'] + '</div>' +
                                                    '<div class="col-xs-4">Status : ' + this['status'] + '</div>' +
                                                    '</div>' +
                                                    '<div class="row">' +
                                                    '<div class="col-xs-4">Tipe Transaksi : ' + this['trx_type'] + '</div>' +
                                                    '<div class="col-xs-6">Nilai Transaksi : ' + this['nilai_transaksi'] + '</div>' +
                                                    '</div>' +
                                                    '<div class="row">' +
                                                    '<div class="col-xs-4">Deposit Keluar : ' + this['deposit_Keluar'] + '</div>' +
                                                    '<div class="col-xs-6">Saldo : ' + this['saldo'] + '</div>' +
                                                    '</div>' +
                                                    '<div class="row">' +
                                                    '<div class="col-xs-4">Deposit Masuk : ' + this['deposit_Masuk'] + '</div>' +
                                                    '</div>' +
                                                    '<div class="row">' +
                                                    '<div class="col-xs-4">Bank : ' + this['bank_name'] + '</div>' +
                                                    '</div>' +
                                                    '<div class="row">' +
                                                    '<div class="col-xs-4">Rekening : ' + this['bank_acct_no'] + '</div>' +
                                                    '</div>' +
                                                    '</div><div class="height">&nbsp;</div>';
                                        }
                                        );
                                        var recorddata = this['iTotalRecords'];
                                        pagings = paging(recorddata, idf, "listdeposit");
                                    }
                                    );
                                    $('#tabledata' + idf).html(htmls + pagings);
                                }
                            }
                            );
                        }
                        listdeposit(14);
                        function listinbox(idf) {
                            var urls = $("#frmSearch" + idf);
                            var htmls = '';
                            var pagings = '';
                            $.ajax({
                                url: urls.attr('url'), data: urls.serialize(), type: 'POST', datatype: 'json',
                                success: function(JSONString) {
                                    var dataobj = $.parseJSON('[' + JSONString + ']');
                                    $.each(dataobj, function() {
                                        var realdata = this['aaData'];
                                        htmls = '<ul class="list-group">';
                                        icount = 0;
                                        $.each(realdata, function() {
                                            icount++;
                                            if (icount % 2 == true) {
                                                clasodd = " list-group-item-info-cst";
                                            }
                                            else {
                                                clasodd = "";
                                            }
                                            if (this['status'] == 'U' && this['member_seq'] != "<?php echo $_SESSION[SESSION_MEMBER_SEQ]; ?>") {
                                                content = "<strong>" + this['content'] + "</strong>";
                                                facoment = "fa-comment";
                                            }
                                            else {
                                                content = this['content'];
                                                facoment = "fa-comment-o";
                                            }
                                            htmls += '<li id="list_' + this['headers'] + '" class="list-group-item' + clasodd + '">';
                                            htmls += '<div class="row"><a href="javascript:detail(' + this['headers'] + ')"><div class="col-md-3">' + '<i class="fa ' + facoment + '"></i> ' + this['user_name'] + '</div>' + '<div class="col-xs-7">' + content + '</div>' + '<div class="col-xs-2">' + this['created_date'] + '</div></a></div>';
                                            htmls += '</li>';
                                        }
                                        );
                                        var recorddata = this['iTotalRecords'];
                                        htmls += '</ul>';
                                        pagings = paging(recorddata, idf, "listinbox");
                                    }
                                    );
                                    $('#tabledata' + idf).html(htmls + pagings);
                                }
                                ,
                                error: function(request, error) {
                                    alert("Error");
                                }
                            }
                            );
                        }
//                        listinbox(11);
                        function newmsg(idh) {
                            $('#message_text').val('');
                            $('#prev_message_seq').val(idh);
                            $('#InboxModal').modal('show');
                        }
                        function savemsg() {
                            if ($('#message_text').val() == '') {
                                alert('Pesan <?php echo ERROR_VALIDATION_MUST_FILL; ?>');
                                return false;
                            }
                            $('#InboxModal').modal('hide');
                            var urls = $("#frmInbox");
                            $.ajax({
                                url: urls.attr('url'), data: urls.serialize(), type: 'POST', datatype: 'json',
                                success: function() {
//                                    listinbox(11);
                                }
                            }
                            );
                        }
                        function detail(idh) {
                            $('#list_' + idh).html('');
                            htmls = '';
                            thelastreplay = '';
                            $.ajax({
                                url: "<?php echo get_base_url(); ?>member", data: {
                                    'btnDetil11': 'true', 'seq': idh}
                                , type: 'POST', datatype: 'json',
                                success: function(JSONString) {
                                    var dataobj = $.parseJSON('[' + JSONString + ']');
                                    $.each(dataobj, function() {
                                        var realdata = this['aaData'];
                                        $.each(realdata, function() {
                                            if (this['member_seq'] == "<?php echo $_SESSION[SESSION_MEMBER_SEQ]; ?>") {
                                                htmls += '<div class="direct-chat-msg">' + '<div class="direct-chat-info clearfix">' + '<span class="direct-chat-name pull-left">' + this['user_name'] + '</span>' + '<span class="direct-chat-timestamp pull-right">' + this['created_date'] + '</span>' + '</div>' + '<img class="direct-chat-img" src="' + '<?php echo $img_src ?>' + '">' + '<div class="direct-chat-text">' + this['content'] + '</div>' + '</div>';
                                            }
                                            else {
                                                htmls += '<div class="direct-chat-msg right">' + '<div class="direct-chat-info clearfix">' + '<span class="direct-chat-name pull-right">' + this['user_name'] + '</span>' + '<span class="direct-chat-timestamp pull-left">' + this['created_date'] + '</span>' + '</div>' + '<img class="direct-chat-img" src="' + '<?php echo get_image_location() . "assets/img/admin_default_img.jpg" ?>' + '">' + '<div class="direct-chat-text">' + this['content'] + '</div>' + '</div>';
                                            }
                                            thelastreplay = this['member_seq'];
                                        }
                                        );
                                    }
                                    );
                                    if (thelastreplay != "<?php echo $_SESSION[SESSION_MEMBER_SEQ]; ?>") {
                                        htmls += '<br /><button type="button" onclick="newmsg(' + idh + ')" class="pull-right"><i class=" fa fa-reply"> Balas Pesan</i></button><br />';
                                    }
                                    htmls += '<button type="button" onclick="reduceBox(' + idh + ')"><i class="fa fa-chevron-up">&nbsp;Kecilkan</i></button>';
                                    $('#list_' + idh).html(htmls);
                                }
                            }
                            );
                        }
                        function reduceBox(list) {
//                            listinbox(11);
                        }
                        $('#member_page li').click(function() {
                            $('.alert').hide();
                        }
                        )
</script>
