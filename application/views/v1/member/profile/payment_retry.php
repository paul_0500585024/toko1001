<?php
require_once VIEW_BASE_HOME;
?>
<div class="container">
    <br />
    <div class="row">
        <div class="container">
            <?php if (isset($data_err) && $data_err[ERROR] === true) { ?>
                <div class="alert alert-error-home">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $data_err[ERROR_MESSAGE][0] ?>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="row reset-margin">
        <div class="col-xs-12 box-header-cart">
            <div><h4>Detail Order</h4></div>
            <div class="margin-left-15px">
                <div>No. Order : <?php echo $data_ord[0][0]->order_no ?></div>
                <div>Tanggal : <?php echo date_format(date_create($data_ord[0][0]->order_date), "d-M-Y"); ?></div>
                <div>Status : <?php
                    $data = json_decode(STATUS_PAYMENT, true);
                    echo $data[$data_ord[0][0]->payment_status];
                    ?>
                </div>
            </div>
        </div>
    </div>
    <br />
    <div class="row reset-margin">
        <div class="col-xs-12 box-header-cart">
            <div><h4>Penerima</h4></div>
            <div class="margin-left-15px">
                <div><?php echo $data_ord[0][0]->receiver_name; ?></div>
                <div><?php echo $data_ord[0][0]->receiver_address . "-" . $data_ord[0][0]->city_name . " / " . $data_ord[0][0]->district_name . "<br>" . $data_ord[0][0]->province_name ?></div>
                <div><?php echo $data_ord[0][0]->receiver_zip_code; ?></div>
                <div><?php echo $data_ord[0][0]->receiver_phone_no; ?></div>
            </div>
        </div>
    </div>
    <br />
    <div class="row reset-margin">
        <div class="col-xs-12 box-header-cart">
            <div class="row">
                <div class="col-xs-12 box-body-cart">
                    <h4>Produk yang dibeli</h4>
                    <?php
                    $total = 0;

                    $total_product_merchant_expedisi = 0;
                    $totalProductMerchant_price_kg = '';

                    foreach ($data_ord[1] as $merchant) {
                        ?>
                        <div class="row reset-margin merchant-header">
                            <div class="col-xs-4">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <a href = "<?php echo base_url() . "merchant/" . $merchant->merchant_code; ?>" ><h5><b><?php echo $merchant->merchant_name; ?></b></h5></a>
                                    </div>

                                </div>
                            </div>
                            <div class="col-xs-6"><input  type = "text"  class = "form-control input-sm" value="<?php echo $merchant->member_notes; ?>" readonly="" maxlength="100"></div>
                            <div class="col-xs-2">
                                <div style="text-align: right">
                                    <div><?php echo "<b>(" . $merchant->total_product . ")</b> Produk"; ?></div>
                                    <div>
                                        <div>Shipping :  <?php echo $merchant->expedition_name; ?></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php
                        $wpo = 0;
                        $wpp = 0;
                        $wpp2 = 0;
                        $prodPriceTot = 0;
                        $totalProduct_kg = 0;
                        $totalProduct_price_kg = 0;
                        $wpp = 0;
                        $twpmp = 0;

                        foreach ($data_ord[2] as $prod) {
                            if ($merchant->merchant_seq == $prod->merchant_seq) {
                                ?>
                                <div class="row reset-margin un-style">
                                    <div class="col-xs-2">
                                        <a href ="<?php echo base_url() . url_title($prod->display_name . " " . $prod->product_seq); ?>"><center><img src="<?php echo PRODUCT_UPLOAD_IMAGE . $prod->merchant_seq . "/" . S_IMAGE_UPLOAD . $prod->img; ?>" alt="<?php echo $prod->img; ?>" width = "100px"></center></a>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="row">
                                            <div class="col-xs-12 cart-font-color-blue no-pad"><?php echo $prod->display_name ?></div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-xs-12 no-pad">
                                                <div class="col-xs-6 no-pad">
                                                    <div class="col-xs-3 pad-auto">
                                                        Warna
                                                    </div>
                                                    <div class="col-xs-1 pad-auto">
                                                        &nbsp;:&nbsp;
                                                    </div>
                                                    <div class="col-xs-6 no-pad">
                                                        <?php echo $prod->variant_name; ?>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 no-pad">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row cart-font-color-gray">
                                            <div class="col-xs-12 no-pad">
                                                <div class="col-xs-6 no-pad">
                                                    <div class="col-xs-3 pad-auto">
                                                        Berat
                                                    </div>
                                                    <div class="col-xs-1 pad-auto">
                                                        &nbsp;:&nbsp;
                                                    </div>
                                                    <div class="col-xs-6 no-pad">
                                                        <?php echo $prod->weight_kg ?> Kg
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 no-pad">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4 cart-font-color-gray">
                                        <div class="row"><div class="col-xs-2">&nbsp;</div></div>
                                        <div class="row"><div class="col-xs-2">&nbsp;</div></div>

                                        <div class="row">
                                            <div class="col-xs-4 no-pad" style="text-align: right">
                                                Rp. <?php echo number_format($prod->sell_price); ?>
                                            </div>
                                            <div class="col-xs-2">
                                                <center><input class="input-style" readonly value="<?php echo number_format($prod->qty); ?>"/></center>
                                            </div>
                                            <div class="col-xs-5 no-pad">
                                                Rp.  <?php
                                                echo number_format($prod->qty * $prod->sell_price);
                                                $total = $total + $prod->qty * $prod->sell_price + ceil($prod->weight_kg * $prod->qty) * $prod->ship_price_charged;
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                $prodPriceTot = $prodPriceTot + ($prod->qty * $prod->sell_price);
                                $wpo = ($prod->qty * $prod->weight_kg);
                                $wpp = $prod->ship_price_charged * (ceil($wpo));
                                $twpmp = $twpmp + $wpp;
                                $totalProduct_kg = $totalProduct_kg + ($prod->qty * $prod->weight_kg);

                            }
                        }
                        $totalProductMerchant_price_kg = $totalProductMerchant_price_kg + $totalProduct_price_kg;
                        ?>
                        <div class="well expedisi-well">
                                <?php if ($data_ord[2][0]->ship_price_charged !== '0') { ?>
                                    <p class="text-bold text-color-cart">Biaya Pengiriman :<span class="text-bold text-color-cart">
                                            <span style="color: #007dc6"><?php echo $merchant->expedition_name ?></span>
                                            (<span class="expedisi-label-kg"> <?php echo ceil($totalProduct_kg) . ' X ' . number_format($data_ord[2][0]->ship_price_charged) ?>  </span>)
                                            <?php echo RP . ' ' . number_format(ceil($totalProduct_kg) * $data_ord[2][0]->ship_price_charged) ?>
                                        </span>
                                    </p>
                                <?php } else { ?>
                                    <span class="pull-left"><p class="text-bold text-color-cart">Biaya Pengiriman : <span style="color: #007dc6"><?php echo $merchant->expedition_name; ?></span>&nbsp;<span class="text-green">Free</span></p></span>
                                <?php } ?>
                                <span class="pull-right"><p class="pull-right"><span class="text-bold" style="font-size:1.1em"><?php echo RP . number_format($merchant->total_merchant + $merchant->total_ship_charged) ?></span></p></span>
                            </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    Voucher Member / Kupon - Rp. <?php echo number_format($data_ord[0][0]->nominal); ?>
                </div>
                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-8">
                            <div style="text-align: right">Total Belanja : </div>
                        </div>
                        <div class="col-xs-4 no-pad">
                               <b class="pull-right" style="padding-right: 19px;font-size: 1.3em">Rp. <?php echo number_format($data_ord[0][0]->total_order); ?></b>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    Kode Voucher :<?php echo $data_ord[0][0]->voucher_code; ?>
                </div>
                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-8">
                            <div class="text-right">Total Pembayaran : </div>
                        </div>
                        <div class="col-xs-4 no-pad">
                            <b class="pull-right font-color-red" style="padding-right: 19px;font-size: 1.3em">Rp. <?php echo number_format($data_ord[0][0]->total_payment); ?></b>
                        </div>
                    </div>
                    <?php if ($data_ord[0][0]->promo_credit_seq > 0) { ?>
                        <div class="row">
                            <div class="col-xs-8">
                                <div style="text-align: right">Bank : </div>
                            </div>
                            <div class="col-xs-4 no-pad">
                                <b><?php echo $data_credit_info[0]->bank_name; ?></b>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-8">
                                <div style="text-align: right">Periode cicilan : </div>
                            </div>
                            <div class="col-xs-4 no-pad">
                                <b><?php echo number_format($data_credit_info[0]->credit_month); ?> bulan</b>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-8">
                                <div style="text-align: right">Pembayaran bulanan : </div>
                            </div>
                            <div class="col-xs-4 no-pad">
                                <b>Rp. <?php echo number_format($data_ord[0][0]->total_payment / $data_credit_info[0]->credit_month); ?></b>
                            </div>
                        </div>
                    <?php }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <br />
    <form id='frm_payment_info' method="post" action="<?php echo get_base_url() . $data_auth[FORM_URL] ?>">
        <div class="row  reset-margin">
            <div class="col-xs-12 box-header-cart">
                <h4>Metode Pembayaran</h4>
                <?php if ($data_ord[0][0]->promo_credit_seq > 0) { ?>
                    <input type="hidden" id="payment" name="payment" value="<?php echo PAYMENT_SEQ_CREDIT; ?>" />
                    <div class="col-xs-12 content-cart">

                        <div id='<?php echo PAYMENT_SEQ_CREDIT; ?>' class="col-xs-3 border-cart no-pad">
                            <div class="a" style="display:block"></div><div class="b" style="display:block"><i class="fa fa-check"></i></div>
                            <input class="radio-none" name="atm_type" type="radio" value ="<?php echo PAYMENT_SEQ_CREDIT; ?>" checked>
                            <label for="cicilan">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <h5 class="h5-style">Cicilan</h5>
                                        <hr class="clear-padding-margin">
                                    </div><br>
                                    <div class="row">
                                        <img height="100px" src="<?php echo get_img_url() ?>payment_70px/cicilan_0_bca.jpg">
                                    </div>
                                </div>
                            </label>
                        </div>
                        <div class="col-xs-6 pull-right">
                            <div class="row">
                                <div class="col-xs-12 no-pad">
                                    <div class="pull-right">
                                        <div><?php
                                            echo str_repeat("<br />", 6);
                                            ?></div>
                                        <center><button type="submit" name="<?php echo CONTROL_SAVE_EDIT_NAME; ?>" id="btnSaveEdit" class='btn btn-flat btn-confirm'>PROSES ORDER</button></center>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div><br>
            <div class="row reset-margin">
                <div class="col-xs-12 small-margin">
                    <div class="row">
                        <div class="col-xs-8">
                            &nbsp;
                        </div>

                    </div>
                </div>
            </div>
        <?php } else { ?>
            <input type="hidden"  id="payment" name="payment" value="<?php echo isset($payment_info["payment_seq"]) ? $payment_info["payment_seq"] : '' ?>" />
            <div class="col-xs-12 content-cart">
                <div id="1" onclick="onColor(this.id, 'P')" class="col-xs-3 border-cart no-pad">
                    <div class="a" style="<?php if (isset($payment_info)) echo ($payment_info["payment_seq"] == PAYMENT_SEQ_BANK) ? "display:block" : "display:none" ?>"></div><div class="b" style="<?php if (isset($payment_info)) echo ($payment_info["payment_seq"] == PAYMENT_SEQ_BANK) ? "display:block" : "display:none" ?>"><i class="fa fa-check"></i></div>
                    <input class="radio-none" type="radio" name="atm_type" value ="<?php echo PAYMENT_SEQ_BANK; ?>" onclick="f_oyt('<?php echo PAYMENT_SEQ_BANK; ?>', 'P')" <?php echo isset($payment_info) && $payment_info["payment_seq"] == PAYMENT_SEQ_BANK ? "checked" : "" ?>>
                    <label for="bnk">
                        <img class="full-img"src="<?php echo get_img_url() . IMAGE_BANK_PAYMENT ?>">
                    </label>
                </div>
                <!--div id='<?php echo PAYMENT_SEQ_DOCU_ATM; ?>' onclick="onColor(this.id, 'P')" class="col-xs-3 border-cart no-pad">
                    <div class="a" style="<?php if (isset($payment_info)) echo ($payment_info["payment_seq"] == PAYMENT_SEQ_DOCU_ATM) ? "display:block" : "display:none" ?>"></div><div class="b" style="<?php if (isset($payment_info)) echo ($payment_info["payment_seq"] == PAYMENT_SEQ_DOCU_ATM) ? "display:block" : "display:none" ?>"><i class="fa fa-check"></i></div>
                    <input class="radio-none" type="radio" name="atm_type"  value ="<?php echo PAYMENT_SEQ_DOCU_ATM; ?>" <?php echo isset($payment_info) && $payment_info["payment_seq"] == PAYMENT_SEQ_DOCU_ATM ? "checked" : "" ?> onclick="f_oyt('<?php echo PAYMENT_SEQ_BANK; ?>', 'P')">
                    <label for="brs">
                        <div class="col-xs-12">
                            <div class="row">
                                <h5 class="h5-style">Tranfer bank Via ATM Bersama</h5>
                                <hr class="clear-padding-margin">
                            </div><br>
                            <div class="row">
                                <img height="60px" src ="<?php echo get_img_url() ?>payment/logo-atm.png" alt="Docu ATM">
                            </div>
                        </div>
                    </label>
                </div-->
                <div id='<?php echo PAYMENT_SEQ_DEPOSIT; ?>' onclick="onColor(this.id, 'P')" class="col-xs-3 border-cart no-pad">
                    <div class="a" style="<?php if (isset($payment_info)) echo ($payment_info["payment_seq"] == PAYMENT_SEQ_DEPOSIT) ? "display:block" : "display:none" ?>"></div><div class="b" style="<?php if (isset($payment_info)) echo ($payment_info["payment_seq"] == PAYMENT_SEQ_DEPOSIT) ? "display:block" : "display:none" ?>"><i class="fa fa-check"></i></div>
                    <input class="radio-none" name="atm_type" type="radio" value ="<?php echo PAYMENT_SEQ_DEPOSIT; ?>"  onclick="f_oyt('<?php echo PAYMENT_SEQ_DEPOSIT; ?>', 'P')" <?php echo isset($payment_info) && $payment_info["payment_seq"] == PAYMENT_SEQ_DEPOSIT ? "checked" : ""; ?>>
                    <label for="saldo">
                        <div class="col-xs-12">
                            <div class="row">
                                <h5 class="h5-style">Deposit</h5>
                                <hr class="clear-padding-margin">
                            </div><br>
                            <div class="row">
                                <font size="3">Saldo Anda <?php echo "Rp." . number_format($member_info[2][0]->deposit_amt); ?></font>
                            </div>
                        </div>
                    </label>
                </div>
                <div id='<?php echo PAYMENT_SEQ_CREDIT_CARD; ?>' onclick="onColor(this.id, 'P')" class="col-xs-3 border-cart no-pad">
                    <div class="a" style="<?php if (isset($payment_info)) echo ($payment_info["payment_seq"] == PAYMENT_SEQ_CREDIT_CARD) ? "display:block" : "display:none" ?>"></div><div class="b" style="<?php if (isset($payment_info)) echo ($payment_info["payment_seq"] == PAYMENT_SEQ_CREDIT_CARD) ? "display:block" : "display:none" ?>"><i class="fa fa-check"></i></div>
                    <input class="radio-none" name="atm_type" type="radio" value ="<?php echo PAYMENT_SEQ_CREDIT_CARD; ?>" onclick="f_oyt('<?php echo PAYMENT_SEQ_CREDIT_CARD; ?>', 'P')" <?php echo isset($payment_info) && $payment_info["payment_seq"] == PAYMENT_SEQ_CREDIT_CARD ? "checked" : ""; ?>>
                    <label for="kartu-kredit">
                        <div class="col-xs-12">
                            <div class="row">
                                <h5 class="h5-style">Kartu Kredit</h5>
                                <hr class="clear-padding-margin">
                            </div><br>
                            <div class="row">
                                <img height="50px" src="<?php echo get_img_url() . "credit_card.jpg" ?>">
                            </div>
                        </div>
                    </label>
                </div>
            </div>
            <div class="col-xs-12" style="margin-top: 15px">
                <div id='<?php echo PAYMENT_SEQ_MANDIRI_KLIKPAY; ?>' onclick="onColor(this.id, 'M')" class="col-xs-3 border-cart no-pad">
                    <div class="a" style="<?php if (isset($payment_info)) echo ($payment_info["payment_seq"] == PAYMENT_SEQ_MANDIRI_KLIKPAY) ? "display:block" : "display:none" ?>"></div><div class="b" style="<?php if (isset($payment_info)) echo ($payment_info["payment_seq"] == PAYMENT_SEQ_MANDIRI_KLIKPAY) ? "display:block" : "display:none" ?>"><i class="fa fa-check"></i></div>
                    <input  class="radio-none" type="radio" name="bank_type" value ="<?php echo PAYMENT_SEQ_MANDIRI_KLIKPAY; ?>" <?php if (isset($payment_info)) echo ($payment_info["payment_seq"] == PAYMENT_SEQ_MANDIRI_KLIKPAY) ? "checked" : ""; ?> onclick="f_oyt('<?php echo PAYMENT_SEQ_BCA_KLIKPAY; ?>', 'M')">
                    <label for="mandiri">
                        <div class="col-xs-12">
                            <div class="row">
                                <h5 class="h5-style">Mandiri Clickpay</h5>
                                <hr class="clear-padding-margin">
                            </div><br>
                            <div class="row">
                                <center>
                                    <img height="80px" src ="<?php echo get_img_url() ?>payment_70px/mandiriclickpay_70px.png" alt="Mandiri Clikpay">
                                </center>
                            </div>
                        </div>
                    </label>
                </div>
                <div id="<?php echo PAYMENT_SEQ_MANDIRI_ECASH; ?>" onclick="onColor(this.id, 'P')" class="col-xs-3 border-cart no-pad">
                    <div class="a" style="<?php if (isset($payment_info)) echo ($payment_info["payment_seq"] == PAYMENT_SEQ_MANDIRI_ECASH) ? "display:block" : "display:none" ?>"></div><div class="b" style="<?php if (isset($payment_info)) echo ($payment_info["payment_seq"] == PAYMENT_SEQ_MANDIRI_ECASH) ? "display:block" : "display:none" ?>"><i class="fa fa-check"></i></div>
                    <input class="radio-none" type="radio" name="emoney_type"  value ="<?php echo PAYMENT_SEQ_MANDIRI_ECASH; ?>" onclick="f_oyt('<?php echo PAYMENT_SEQ_MANDIRI_ECASH; ?>', 'P')" <?php if (isset($payment_info)) echo ($payment_info["payment_seq"] == PAYMENT_SEQ_MANDIRI_ECASH) ? "checked" : ""; ?>>
                    <label for="ecach">
                        <div class="col-xs-12">
                            <div class="row">
                                <h5  class="h5-style">E - Money</h5>
                                <hr class="clear-padding-margin">
                            </div><br>
                            <div class="row">
                                <center>
                                    <img height="80px" src ="<?php echo get_img_url() ?>payment_70px/mandiri e-cash_70px.png" alt="Mandiri e-cash">
                                </center>
                            </div>
                        </div>
                    </label>
                </div>
            </div>
    </div>
    </div><br>
    <div class="row reset-margin">
        <div class="col-xs-12 small-margin">
            <div class="row">
                <div class="col-xs-8">
                    &nbsp;
                </div>
                <div class="col-xs-4">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-3">
                                &nbsp;
                            </div>
                            <div class="col-xs-9 no-pad">
                                <div class="pull-right">
                                    <div class="font-size-12px text-color-939598">Pilih metode pembayaran sebelum memproses order</div>
                                    <center><button type="submit" name="<?php echo CONTROL_SAVE_EDIT_NAME; ?>" id="btnSaveEdit" class='btn btn-flat btn-confirm'>PROSES ORDER</button></center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php } ?>




</form>
</div>
<script type="text/javascript">
    function onColor(id, type) {
        $(".a, .b").each(function () {
            $(this).css('display', 'none');
        })

        $("input[type='radio']").each(function () {
            $(this).prop("checked", false);
        })

        $('#' + id).find('.a').css('display', 'block');
        $('#' + id).children('.b').css('display', 'block');
        $('#' + id).find("input[type='radio']").prop("checked", true);
        $("#payment").val(id);

    }
</script>
