<?php
require_once VIEW_BASE_HOME;
?>
<div class="container margin-top-5">
    <form class="form-horizontal" id="frmMember" action="<?php echo get_base_url() . "registration"; ?>" method="post">
        <?php if (isset($data_err) && $data_err[ERROR] === true) { ?>
            <div class="alert alert-error-home">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $data_err[ERROR_MESSAGE][0] ?>
            </div>
        <?php } elseif (isset($data_suc) && ($data_suc[SUCCESS] === true)) { ?>
            <div class="alert alert-succes-cst">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo (isset($data_suc[SUCCESS_MESSAGE][0]) ? $data_suc[SUCCESS_MESSAGE][0] : SAVE_DATA_SUCCESS); ?>
            </div>
        <?php }
        ?>
        <div class="col-xs-12 box-header-cart">
            <h4 class="pad-h4">Daftar</h4>
            <div class="col-xs-6">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group has-feedback">
                            <span class="glyphicon glyphicon-envelope form-control-feedback" ></span>
                            <input type="text" name="member_email" id="m_email" class="form-control" placeholder="Email" validate="email[]" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->email : "") ?>"  maxlength="150" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-5">
                        <div class="form-group has-feedback">
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                            <input id="password1" validate="password[]" name="password1" type="password" class="form-control" placeholder="Password" maxlength="50" />
                        </div>
                    </div>
                    <div class="col-xs-2">&nbsp;</div>
                    <div class="col-xs-5">
                        <div class="form-group has-feedback">
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                            <input id="password2" validate="password[]" name="password2" type="password" class="form-control" placeholder="Ulangi password" maxlength="50" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group has-feedback">
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                            <input id="member_name" validate="required[]" name="member_name" type="text" class="form-control" placeholder="Nama Lengkap" maxlength="50" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->name : "") ?>" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-5">
                        <div class="form-group has-feedback">
                            <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                            <input id="member_birthday" date_type="date" validate="required[]" name="member_birthday" type="text" class="form-control" placeholder="Tanggal Lahir" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->date : "") ?>" style="background-color: #fff;" />
                        </div>
                    </div>
                    <div class="col-xs-2">&nbsp;</div>
                    <div class="col-xs-5">
                        <div class="row">
                            <?php
                            $ckp = '';
                            $ckw = '';
                            if (isset($data_sel[LIST_DATA])) {
                                if ($data_sel[LIST_DATA][0]->gender == "M") {
                                    $ckp = 'checked';
                                } else {
                                    $ckw = 'checked';
                                }
                            } else {
                                $ckp = 'checked';
                            }
                            ?>
                            <input type="radio" name="gender_member" value="M"<?php echo $ckp; ?>> Pria &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            <input type="radio" name="gender_member" value="F"<?php echo $ckw; ?> >Wanita
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class = "col-xs-12">
                        <div class = "form-group has-feedback">
                            <span class = "glyphicon glyphicon-earphone form-control-feedback"></span>
                            <input id="member_phone" validate="required[]" name="member_phone" type = "text" class = "form-control" placeholder = "No. Telepon" data-mask data-inputmask = "'mask': ['999 999 999 999 999']" value="<?php echo (isset($data_sel[LIST_DATA]) ? $data_sel[LIST_DATA][0]->phone : "") ?>" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <div class = "row">
                    <div class = "col-xs-6">
                        <?php echo $image ?>
                    </div>
                    <div class = "col-xs-6">
                        <div class = "form-group"><input id="captcha_member" validate="required[]" name="captcha_member" type="text" class="form-control" placeholder="Kode Captcha" maxlength="5"/></div>
                    </div>
                </div>
                <div class = "row add-top-margin">
                    <div class = "col-xs-12">
                        <label><br />
                            <input id = "member_requirement" name = "member_requirement" type = "checkbox" value = "1">
                            &nbsp; Saya sudah membaca dan menyetujui
                            <a href = "<?php echo base_url(); ?>info/syarat-dan-ketentuan"
                               target = "_blank">Syarat dan Ketentuan</a>, serta
                            <a href = "<?php echo base_url(); ?>info/kebijakan-privasi"
                               target = "_blank">Kebijakan Privasi</a>
                        </label>
                    </div>
                </div>
                <div class = "row">
                    <div class = "col-xs-6 text-right"></div>
                    <div class = "col-xs-6 text-right"><br /><br /><br />
                        <button type="submit" name="btnSave" value="true" class = "btn btn-success btn-flat  button-cart width-100">DAFTAR</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    $("[data-mask]").inputmask();
    var isValidMember = $('#frmMember').validate({
        ignore: 'input[type=hidden]',
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.hasClass('select2')) {
                error.insertAfter(element.next('span'));
            } else {
                error.insertAfter(element);
            }
        }
    });

    $('form#frmMember').submit(function() {
        var get_data_member = $(this).serialize();
        if (isValidMember.valid()) {
            $('input[type="text"]').each(function() {
                if ($(this).hasClass('auto') || $(this).hasClass('auto_int') || $(this).hasClass('auto_dec'))
                {
                    var v = $(this).autoNumeric('get');
                    $(this).autoNumeric('destroy');
                    $(this).val(v);
                }
            })
        }
    })
</script>