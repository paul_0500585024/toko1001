<?php
require_once VIEW_BASE_MEMBER;
?>

<aside class="control-sidebar control-sidebar-dark">                
    <div class="control-sidebar-heading bg-black">FILTER</div>
    <div class="tab-content"> 
        <?php
        if ($data_auth[FORM_ACTION] == "") {            
            if (isset($filter)) {
                unset($data_sel[LIST_DATA]);
                require_once VIEWPATH . $filter;
            }
        }
        ?>
    </div>
</aside>
<div class='control-sidebar-bg'></div>