<?php
require_once VIEW_BASE_MEMBER;
?>

<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo get_img_url() ?>avatar.png" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p> MEMBER </p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <ul class="sidebar-menu">
            <li class="treeview">
                <a href="#"><i class='fa fa-folder-o'></i> <span>Master</span> <i class="fa fa-angle-left pull-right"></i></a>
            </li>
    </section>
</aside>