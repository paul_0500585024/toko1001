<?php
require_once VIEW_BASE_MEMBER;
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once get_include_content_member_header(); ?>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <?php
            require_once get_include_content_member_top_navigation();
            require_once get_include_content_member_left_navigation();
            require_once get_include_content_member_right_navigation();
            ?>
            <div class="content-wrapper">
                <div id ="content-message">
                    <?php if (isset($data_auth)) {if ($data_err[ERROR] === true) { ?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <?php echo $data_err[ERROR_MESSAGE][0] ?>
                            </div>
                            <?php } elseif ($data_suc[SUCCESS] === true) { ?>
                            <div class="alert alert-info alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <?php echo SAVE_DATA_SUCCESS ?>
                            </div>
                    <?php }} ?>
                </div>
                <section class="content" id="main-content-table">


