<?php
require_once VIEW_BASE_HOME;
?>
<head><title>Verifikasi Member</title>
    <link href="<?php echo get_css_url(); ?>bootstrap.min.css" rel="stylesheet" type="text/css" />
<div class='container'>
    <br><br><br><br>
    <div class="panel panel-primary">
        <div class="panel-heading">Verifikasi Member</div>
        <div class="panel-body">
            <?php if ($value == "success") { ?>
                <center><h4><span>Selamat, pendaftaran member sudah diverifikasi. Silahkan login dengan email dan password yang sudah anda daftarkan.</span></h4></center>
                <center><h4><span>Terima Kasih. Klik <a href="<?php echo base_url(lOGIN_MEMBER);?>">Disini</a></span></h4></center>
            <?php } elseif ($value == "failed") { ?>
                <center><h2><span>Akun anda sudah diverifikasi sebelumnya.</span></h2></center>
            <?php } else { ?>
                <center><h2><span>Tidak ditemukan</span></h2></center>
            <?php } ?>
        </div>
    </div>
</div>
