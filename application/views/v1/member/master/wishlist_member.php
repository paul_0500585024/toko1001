<?php
require_once VIEW_BASE_HOME;
?>

<div class="container min-wsh-empty" >
    <?php
    if (isset($title)):
        $link = base_url() . "produk/promo";
        ?>
        <div class="cst-breadcrumb">
            <div class="cst-breadcrumb-inside">
                <a href="<?php echo base_url(); ?>"><i class="fa fa-home font-sz-1"></i></a>
            </div>
            <div class="cst-breadcrumb-inside">
                <img src="<?php echo get_image_location() . 'assets/img/breadcrumb.png' ?>">
            </div>
            <a class="wsh" href="<?php echo $link ?>"><?php echo $title ?></a>
        <?php endif; ?>
    </div>

    <?php
    $html = display_product_home($product);
    if ($html == '') {
        echo '<div class="container"><div>Tidak ada produk dalam daftar wishlist anda.</div></div>';
    } else {
        echo $html;
    }
    ?>
    <?php echo $this->pagination->create_links(); ?>
</div>
<?php $this->load->view(LIVEVIEW . 'home/template/segment_main/js_add_product.php'); ?>