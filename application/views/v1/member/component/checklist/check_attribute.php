<table id="tbl_detail" class="diplay table table-bordered table-striped" cellpadding="0" cellspacing="0" border="0" >
    <thead>
        <tr>
            <th class="col-md-1"> Pilih </th>
            <th> Atribut </th>
            <th> Tampilan Atribut </th>
        </tr>
    </thead>
    <tbody>

        <?php
        if (isset($attribute_list)) {
            foreach ($attribute_list as $list) {
                ?>
            <input type ="hidden" name ="seq" id="parent_seq"/>
            <tr>
                <td align="center">
                    <input id="att_check" type="checkbox" name="attribute_seq[]" value="<?php echo $list->seq; ?>"<?php echo ($list->checked == 1 ? "checked" : "") ?> />
                </td>
                <td>
                    <?php echo $list->name; ?>
                </td>
                <td>
                    <?php echo $list->display_name; ?>
                </td>
            </tr>
            <?php
        }
    }
    ?>
</tbody>
</table>
