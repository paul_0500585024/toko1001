<script text="javascript">
    $('.select2').select2();
</script>

<select class="form-control select2" style='width : 100%' name="user_group_seq" type="input" required="type">
    <option value="">-- Pilih --</option>
    <?php foreach ($user_group_name as $each) { ?>
        <option value="<?php echo $each->seq; ?>" <?php echo (isset($data_sel[LIST_DATA]) && $each->seq == $data_sel[LIST_DATA][0]->user_group_seq) ? "selected" : "" ?>><?php echo $each->name; ?></option>
    <?php } ?>
</select>