<?php

require_once CONTROLLER_BASE_MERCHANT;

class C_main_page_merchant Extends controller_base_merchant {

    private $data;

    public function __construct() {

	$this->data = parent::__construct();
	$this->initialize();
    }

    private function initialize() {

	$this->load->model(LIVEVIEW . 'merchant/profile/M_profile_merchant');
	$this->load->model(LIVEVIEW . 'merchant/M_main_page_merchant');
    }

    public function index() {
	$this->get_data();
	$this->main_page_merchant();
	$this->get_chart();
	//$this->load->view(LIVEVIEW . 'merchant/main_page_merchant', $this->data);
	$this->load->view('v1/merchant/main_page_merchant', $this->data);
    }

    protected function get_data() {
	$params = new stdClass();
	$params->ip_address = parent::get_ip_address();
	$params->merchant_seq = parent::get_merchant_user_seq();

	try {
	    $list_data = $this->M_profile_merchant->get_data_merchant($params);

	    if (isset($list_data)) {
		if ($list_data[0][0]->logo_img != '') {
		    $this->data['img_src'] = base_url(PROFILE_IMAGE_MERCHANT) . '/' . $params->merchant_seq . '/' . $list_data[0][0]->logo_img;
		} else {
		    $this->data['img_src'] = base_url() . IMG_BLANK_100;
		}
		parent::set_data($this->data, $list_data);
	    }
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	}
    }

    protected function main_page_merchant() {

	$params = new stdClass();
	$params->new_product = NEW_STATUS_CODE;
	$params->product_change = CHANGE_STATUS;
	$params->merchant_seq = parent::get_merchant_user_seq();
	$params->order_status = NEW_STATUS_CODE;

	try {
	    $list_data = $this->M_main_page_merchant->get_main_page_merchant($params);
	    $this->data[PENDING_NEW_PRODUCT] = $list_data[0][0]->new_product;
	    $this->data[PENDING_PRODUCT_CHANGE] = $list_data[1][0]->product_change;
	    $this->data[OUT_OF_STOCK] = $list_data[2][0]->out_of_stock;
	    $this->data[UNPRINTED] = $list_data[3][0]->unprinted;
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	}
    }

    public function sign_out() {

	unset($_SESSION[SESSION_MERCHANT_UID]);
	unset($_SESSION[SESSION_MERCHANT_UNAME]);
	unset($_SESSION[SESSION_MERCHANT_USER_GROUP]);
	unset($_SESSION[SESSION_MERCHANT_EMAIL]);
	unset($_SESSION[SESSION_MERCHANT_SEQ]);
	unset($_SESSION[SESSION_MERCHANT_IMAGE]);
	unset($_SESSION[SESSION_MERCHANT_FORM_AUTH]);
	unset($_SESSION[SESSION_MERCHANT_CSRF_TOKEN]);
	unset($_SESSION[SESSION_IP_ADDR]);
	unset($_SESSION[SESSION_MERCHANT_LEFT_NAV]);

	$this->data[DATA_ERROR] = False;
	$this->load->view(LIVEVIEW . 'merchant/login_merchant', $this->data);
    }

    public function get_chart() {

	$this->chart_order();
    }

    public function chart_order() {
	$params = new stdClass();
	$params->merchant_seq = parent::get_merchant_user_seq();
	$params->january_from = '2016-01-01 00:00:00';
	$params->january_to = '2016-01-31 00:00:00';
	$params->february_from = '2016-02-01 00:00:00';
	$params->february_to = '2016-02-31 00:00:00';
	$params->march_from = '2016-03-01 00:00:00';
	$params->march_to = '2016-03-31 00:00:00';
	$params->april_from = '2016-04-01 00:00:00';
	$params->april_to = '2016-04-31 00:00:00';
	$params->may_from = '2016-05-01 00:00:00';
	$params->may_to = '2016-05-31 00:00:00';
	$params->june_from = '2016-06-01 00:00:00';
	$params->june_to = '2016-06-31 00:00:00';
	$params->july_from = '2016-07-01 00:00:00';
	$params->july_to = '2016-07-31 00:00:00';
	$params->august_from = '2016-08-01 00:00:00';
	$params->august_to = '2016-08-31 00:00:00';
	$params->septebmer_from = '2016-09-01 00:00:00';
	$params->septebmer_to = '2016-09-31 00:00:00';
	$params->october_from = '2016-10-01 00:00:00';
	$params->october_to = '2016-10-31 00:00:00';
	$params->november_from = '2016-11-01 00:00:00';
	$params->november_to = '2016-11-31 00:00:00';
	$params->december_from = '2016-12-01 00:00:00';
	$params->december_to = '2016-12-31 00:00:00';

	try {
	    $list_data = $this->M_main_page_merchant->charts_order($params);
	    parent::set_list_data($this->data, $list_data);
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	}

	$key = $list_data[0];

	$this->data['order_january'] = $key[0]->january;
	$this->data['order_february'] = $key[0]->february;
	$this->data['order_march'] = $key[0]->march;
	$this->data['order_april'] = $key[0]->april;
	$this->data['order_may'] = $key[0]->may;
	$this->data['order_june'] = $key[0]->june;
	$this->data['order_july'] = $key[0]->july;
	$this->data['order_august'] = $key[0]->august;
	$this->data['order_september'] = $key[0]->september;
	$this->data['order_october'] = $key[0]->october;
	$this->data['order_november'] = $key[0]->november;
	$this->data['order_december'] = $key[0]->december;
    }

}

?>
