<?php

require_once CONTROLLER_BASE_MERCHANT;

class C_notification_merchant extends controller_base_merchant {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("MTRX01009", "merchant/transaction/notification");
        $this->initialize();
    }

    private function initialize() {
        $this->load->library('pagination');
        $this->load->model('component/M_notification');
        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_ADDITIONAL, "get_ajax");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        $this->data['breadcrumb'] = parent::get_breadcrumb($this->data);
//        $data = new stdClass();
//        $data->user_id = parent::get_merchant_user_id();
//        $data->ip_address = parent::get_ip_address();
//        $data->merchant_seq = $this->data[DATA_USER_PROFILE][USER_SEQ];
//        $this->data[DATA_SELECTED][LIST_DATA] = $this->M_notification->get_notif_log_by_merchant_seq($data);
        if (!$this->input->post()) {
            $this->load->view(LIVEVIEW . "merchant/transaction/notification_merchant", $this->data);
        }
        /*
          $params = new stdClass();
          $params->user_id = '1';
          $params->ip_address = '127.0.0.1';
          $params->code = 'PAID_ORDER';
          $params->user_group_seq = '3';
          $params->merchant_seq = '6';
          $params->order_no = '123';
          $params->recipient_name = 'paul';
          $params->paid_before = '17-12-2016';

          $this->notif_template($params); */
    }

    public function search() {
        $filter = new stdClass;
        $filter->user_id = parent::get_merchant_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_get(START_OFFSET) == "" ? 0 : parent::get_input_get(START_OFFSET);
        $filter->length = 10;
        $filter->order = parent::get_input_get("order") == '' ? "DESC" : parent::get_input_get("order");
        $filter->column = parent::get_input_get("column") == '' ? "tnm.created_date" : parent::get_input_get("column");
        $filter->merchant_id = parent::get_merchant_user_seq();

        try {
            $list_data = $this->M_notification->get_list_merchant($filter);
            parent:: set_list_data($this->data, $list_data);
            $this->display_page(base_url("merchant/transaction/notification"), $filter->start, $list_data[0][0]->total_rec, $filter->length);
        } catch (Exception $ex) {
            parent:: set_error($this->data, $ex);
        }
    }

    public function get_ajax() {
        switch (parent::get_input_post('type')) {
            case 'read':
                $filter = new stdClass;
                $filter->user_id = parent::get_merchant_user_id();
                $filter->ip_address = parent::get_ip_address();
                $filter->seq = parent::get_input_post('seq');
                $filter->status = 'R';
                $filter->table = 'MERCHANT';
                $this->M_notification->save_update($filter);
                exit();
                break;
            case 'count_total':
                echo $this->get_notification_total_unread()[0]->total;
                exit();
                break;
            case 'show_notif_top':
                echo json_encode($this->get_top_notif_data(5));
                exit();
                break;
            case 'count_notif':
                echo json_encode($this->get_count_notif());
                exit();
                break;
        }
    }

    public function get_edit() {
        $filter = new stdClass;
        $filter->user_id = parent::get_merchant_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->notif_seq = parent::get_input_get("key");
        $filter->merchant_seq = $this->data[DATA_USER_PROFILE][USER_SEQ];
        try {
            $list_data = $this->M_notification->get_list_merchant_detail($filter);
            parent::set_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent:: set_error($this->data, $ex);
        }
    }

    public function get_notification_total_unread() {
        $filter = new stdClass;
        $filter->user_id = parent::get_merchant_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->status = "U";
        $filter->user_seq = parent::get_input_post("seq");
        $filter->user_group = "MERCHANT";
        $list_data = 0;
        try {
            $list_data = $this->M_notification->get_notification_total_by_status($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        return $list_data;
    }

    public function get_top_notif_data($n) {
        $filter = new stdClass;
        $filter->user_id = parent::get_merchant_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->status = "U";
        $filter->user_seq = parent::get_input_post("seq");
        $filter->user_group = "MERCHANT";
        $filter->top = $n;
        $list_data = '';
        try {
            $list_data = $this->M_notification->get_notification_top_list_by_status($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        return $list_data;
    }

    public function get_count_notif() {
        $filter = new stdClass;
        $filter->user_id = parent::get_merchant_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->user_seq = parent::get_merchant_user_seq();
        $list_data = '';
        try {
            $list_data = $this->M_notification->get_notification_merchant_menu($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        return $list_data;
    }

}

?>