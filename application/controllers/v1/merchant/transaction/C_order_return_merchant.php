<?php

require_once CONTROLLER_BASE_MERCHANT;

class C_order_return_merchant extends controller_base_merchant {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("MTRX01005", "merchant/transaction/return_merchant");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model(LIVEVIEW . 'merchant/transaction/M_order_return_merchant');
        $this->load->model('component/M_dropdown_list');
        $this->load->library('pagination');
        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        $merchant_seq = parent::get_merchant_user_seq();
        $this->data[MEMBER_EMAIL] = $this->M_dropdown_list->get_dropdown_member_email_return($merchant_seq);
        if (!$this->input->post()) {
            $this->data['breadcrumb'] = parent::get_breadcrumb($this->data);
            if ($this->session->tempdata('DATA_MESSAGE') == true) {
                $this->data[DATA_SUCCESS][SUCCESS] = true;
                $this->session->unset_tempdata('DATA_MESSAGE');
            }
            $this->load->view(LIVEVIEW . "merchant/transaction/order_return_merchant", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view(LIVEVIEW . "merchant/transaction/order_return_merchant", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view(LIVEVIEW . "merchant/transaction/order_return_merchant", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $merchant_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($merchant_info);
                    if (!isset($_COOKIE[$this->data[DATA_AUTH][FORM_CD]])) {
                        redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                    } else {
                        redirect($_COOKIE[$this->data[DATA_AUTH][FORM_CD]]);
                    }
                }
            }
        }
    }

    public function get_edit() {
        $selected = new stdClass();
        $selected->merchant_seq = parent::get_merchant_user_seq();
        $selected->merchant_id = parent::get_merchant_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->return_seq = parent::get_input_get("key");
        try {
            $sel_data = $this->M_order_return_merchant->get_data($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
                $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data;
            } else {
                redirect(base_url("merchant/transaction/return_merchant"));
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    public function search() {

        $filter = new stdClass;
        $filter->user_id = parent::get_merchant_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->merchant_seq = parent::get_merchant_user_seq();
        $filter->no_return = parent::get_input_get("no_return");
        $filter->date_search = parent::get_input_get("return_date_from") != '' ? "1" : "0";
        if (parent::get_input_get("return_date_from") !== NULL && parent::get_input_get("return_date_from") != '') {
            if (strtotime(parent::get_input_get("return_date_from"))) {
                $filter->return_date_from = parent::get_input_get("return_date_from");
            } else {
                $filter->return_date_from = '';
            }
        } else {
            $filter->return_date_from = '';
        }
        if (parent::get_input_get("return_date_to") !== NULL && parent::get_input_get("return_date_to") != '') {
            if (strtotime(parent::get_input_get("return_date_to"))) {
                $filter->return_date_to = parent::get_input_get("return_date_to");
            } else {
                $filter->return_date_to = '';
            }
        } else {
            $filter->return_date_to = '';
        }

        if (parent::get_input_get("status") != '') {
            if (parent::is_status_inarray(parent::get_input_get("status"), STATUS_SHIPMENT_BY_MERCHANT)) {
                $filter->ship_status = parent::get_input_get("status");
            } else {
                $filter->ship_status = '';
            }
        } else {
            $filter->ship_status = '';
        }
        $filter->member_email = htmlspecialchars(parent::get_input_get("member_email_return"));
        $filter->new_status_code = NEW_STATUS_CODE; //status N
        $filter->ship_to_member_status = SHIP_TO_MEMBER; // Status T
        $filter->ship_from_member_status = SHIP_FROM_MEMBER; // Status M
        $filter->received_by_member_status = RECEIVED_BY_MEMBER; // Status C
        $filter->received_by_admin_status = RECEIVED_BY_ADMIN; // Status A
        $filter->rejected_status = REJECT_STATUS_CODE; // Status R

        $filter->order = parent::get_input_get("sort") != "asc" ? "desc" : "asc";
        $filter->column = parent::get_input_get("column_sort") == "" ? "created_date" : parent::get_input_get("column_sort");
        $filter->start = parent::get_input_get(START_OFFSET) == "" ? 0 : parent::get_input_get(START_OFFSET);
        $filter->length = PAGINATION_LIMIT;
        
        $list_data = $this->M_order_return_merchant->get_list($filter);

        $this->data['filter'] = "&no_return=" . $filter->no_return . "&return_date_from=" . $filter->return_date_from . "&member_email=" . $filter->member_email;
        $this->data['filter_sort'] = "&sort=" . $filter->order . "&column_sort=" . $filter->column;
        $this->data['filter_status'] = "?status=" . $filter->ship_status;
        parent::set_data($this->data, $list_data);
        $this->display_page(base_url("merchant/transaction/order_return_merchant") . $this->data['filter_status'] . $this->data['filter'] . $this->data['filter_sort'] = "&sort=" . $filter->order . "&column_sort=" . $filter->column, $filter->start, $list_data[0][0]->total_rec, $filter->length);
        setcookie($this->data[DATA_AUTH][FORM_CD], base_url(uri_string() . "?" . $_SERVER["QUERY_STRING"]), time() + COOKIE_TIME, "/");
    }

}

?>