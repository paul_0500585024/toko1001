<?php

require_once CONTROLLER_BASE_MERCHANT;

class C_report_merchant extends controller_base_merchant {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("MTRX01001", "merchant/report");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model(LIVEVIEW . 'merchant/transaction/M_report_merchant');
    }

    public function index() {
        switch (parent::get_input_get("type")) {
            case "order":
                $this->get_order();
                break;
            case "redeem":
                $this->get_redeem();
                break;
            case "product":
                $this->get_product();
                break;
            default:
        }
    }

    private function get_order() {
        $filter = new stdClass;
        $filter->user_id = parent::get_merchant_user_id();
        $filter->merchant_seq = parent::get_merchant_user_seq();
        $filter->ip_address = parent::get_ip_address();
        $filter->order_no_f = parent::get_input_get("order_no_f");
        $filter->column = parent::get_input_get("column");
        $filter->order = parent::get_input_get("order");
        $filter->order_date_s = parent::get_input_get("order_date_s");
        $filter->order_date_e = parent::get_input_get("order_date_e");
        $filter->receiver_name_f = parent::get_input_get("receiver_name_f");
        $filter->status_order_f = parent::get_input_get("status");
        $filter->status_payment_f = parent::get_input_get("status_payment_f");
//        die(json_encode($filter));
        try {
            $list_data = $this->M_report_merchant->get_report_order($filter);
            foreach ($list_data as $data_row) {
                $row = array("No Order" => $data_row->order_no,
                    "Tanggal Order" => parent::cdate($data_row->order_date),
                    "Status Order" => parent::cstdes($data_row->order_status, STATUS_ORDER),
                    "Tanggal Pengiriman" => parent::cdate($data_row->ship_date),
                    "Status Produk" => parent::cstdes($data_row->product_status, STATUS_PRODUCT),
                    "Tanggal Diterima" => parent::cdate($data_row->received_date),
                    "Nama Ekspedisi" => $data_row->name,
                    "Biaya Ekspedisi" => ($data_row->ship_price_real),
                    "Nama Produk" => parent::cdef(str_ireplace(",", " ", $data_row->product_name)) . ($data_row->variant_seq != '1' ? ' ' . $data_row->variant_name : 'x' . $data_row->variant_name . $data_row->variant_seq),
                    "Jumlah" => ($data_row->qty),
                    "Harga Satuan" => ($data_row->sell_price),
                    "Berat" => ($data_row->weight_kg),
                    "Nama Penerima" => parent::cdef(str_ireplace(",", " ", $data_row->receiver_name)),
                    "Alamat" => parent::cdef(str_ireplace(",", " ", preg_replace("/[\n\r]/", "", $data_row->receiver_address))) . ' ' . $data_row->d_name . ' ' . $data_row->c_name . ' ' . $data_row->p_name . ' ' . $data_row->receiver_zip_code,
                    "No Telepon Penerima" => parent::cdef($data_row->receiver_phone_no)
                );
                $output[] = $row;
            }
            echo json_encode($output);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        die();
    }

    private function get_redeem() {
        $filter = new stdClass;
        $filter->user_id = parent::get_merchant_user_id();
        $filter->merchant_seq = parent::get_merchant_user_seq();
        $filter->ip_address = parent::get_ip_address();
        $filter->redeem_seq = parent::get_input_get("key");
        $i = 1;
        try {
            $list_data = $this->M_report_merchant->get_report_redeem($filter);

            foreach ($list_data as $data_row) {
                $row = array("Trxn" => $i,
                    "No Order" => $data_row->order_no,
                    "Tanggal Order" => parent::cdate($data_row->order_date),
                    "Tanggal Pengiriman" => parent::cdate($data_row->ship_date),
                    "Tanggal Diterima" => parent::cdate($data_row->received_date),
                    "Nama Produk" => parent::cdef(str_ireplace(",", " ", $data_row->product_name)) . ($data_row->variant_seq != '1' ? ' ' . $data_row->variant_name : 'x' . $data_row->variant_name . $data_row->variant_seq),
                    "Harga Satuan" => ($data_row->sell_price),
                    "Jumlah" => ($data_row->qty),
                    "Total" => ($data_row->totalorder),
                    "Berat" => ($data_row->weight_kg),
                    "Nama Ekspedisi" => $data_row->name,
                    "Biaya Ekspedisi" => ($data_row->totalexpedition),
                    "Komisi" => ($data_row->totalfee),
                    "Redeem" => ($data_row->totalorder + $data_row->totalexpedition - $data_row->totalfee)
                );
                $i++;
                $output[] = $row;
            }
            echo json_encode($output);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        die();
    }

    private function get_product() {

        $filter = new stdClass;
        $filter->user_id = parent::get_merchant_user_id();
        $filter->merchant_seq = parent::get_merchant_user_seq();
        $filter->ip_address = parent::get_ip_address();
        $filter->default_value_seq = DEFAULT_VALUE_VARIANT_SEQ;
        $filter->search = parent::get_input_get("search");
        $filter->status = parent::get_input_get("status");
        try {
            $list_data = $this->M_report_merchant->get_report_product($filter);
            foreach ($list_data as $data_row) {
                $row = array(
                    "Nama Produk" => $data_row->name,
                    "Varian" => $data_row->value,
                    "Status" => parent::cstdes($data_row->status, STATUS_PRODUCT_MERCHANT),
                    "Panjang (cm)" => $data_row->p_length_cm,
                    "Lebar (cm)" => $data_row->p_width_cm,
                    "Tinggi (cm)" => $data_row->p_height_cm,
                    "Berat (Kg)" => $data_row->p_weight_kg,
                    "SKU" => $data_row->merchant_sku,
                    "Stock" => $data_row->stock,
                    "Harga Jual (Rp)" => $data_row->product_price,
                    "Harga Promo (Rp)" => $data_row->sell_price
                );
                $output[] = $row;
            }
            echo json_encode($output);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

}

?>