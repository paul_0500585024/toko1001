<?php

require_once CONTROLLER_BASE_MERCHANT;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Redeem Merchant
 *
 * @author Jartono
 */
class C_redeem_merchant extends controller_base_merchant {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("MTRX01004", "merchant/transaction/redeem_merchant");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model(LIVEVIEW . 'merchant/transaction/M_redeem_merchant');
        $this->load->library('pagination');
        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_ADDITIONAL, "get_detail_order");
        parent::fire_event($this->data);
    }

    public function index() {
//	$this->data[FILTER] = 'merchant/transaction/redeem_merchant_f.php';

        if (!$this->input->post()) {
            $this->data['breadcrumb'] = parent::get_breadcrumb($this->data);
            $this->load->view(LIVEVIEW . "merchant/transaction/redeem_merchant", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view(LIVEVIEW . "merchant/transaction/redeem_merchant", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view(LIVEVIEW . "merchant/transaction/redeem_merchant", $this->data);
                    }
                } elseif ($this->data[DATA_ERROR][ERROR] === false) {
                    $this->load->view(LIVEVIEW . "merchant/transaction/redeem_merchant", $this->data);
                }
            } else {

            }
        }
    }

    protected function search() {

        $filter = new stdClass();
        $filter->user_id = parent::get_merchant_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->merchant_id = parent::get_merchant_user_seq();
        $filter->sort = parent::get_input_get("sort") != "asc" ? "desc" : "asc";
        $filter->column = "paid_date";
        $filter->start = parent::get_input_get(START_OFFSET) == "" ? 0 : parent::get_input_get(START_OFFSET);
        $filter->length = PAGINATION_LIMIT;
        $filter->status = '';

        if (parent::get_input_get("tanggal1") !== NULL && parent::get_input_get("tanggal1") != '') {
            if (strtotime(parent::get_input_get("tanggal1"))) {
                $filter->from_date = parent::get_input_get("tanggal1");
            } else {
                $filter->from_date = '';
            }
        } else {
            $filter->from_date = '';
        }
        if (parent::get_input_get("tanggal2") !== NULL && parent::get_input_get("tanggal2") != '') {
            if (strtotime(parent::get_input_get("tanggal2"))) {
                $filter->to_date = parent::get_input_get("tanggal2");
            } else {
                $filter->to_date = '';
            }
        } else {
            $filter->to_date = '';
        }
        $list_data = $this->M_redeem_merchant->get_list($filter);
        $this->data['filter'] = "&tanggal1=" . $filter->from_date . "&tanggal2=" . $filter->to_date;
        $this->data['filter_sort'] = "&sort=" . $filter->sort . "&column_sort=" . $filter->column;
        $this->data['filter_status'] = "?";
        parent::set_data($this->data, $list_data);
        $this->display_page(base_url("merchant/transaction/redeem_merchant") . $this->data['filter_status'] . $this->data['filter'] . $this->data['filter_sort'] = "&sort=" . $filter->sort . "&column_sort=" . $filter->column, $filter->start, $list_data[0][0]->total_rec, $filter->length);
    }

    protected function get_edit() {
        $params = new stdClass();
        $params->user_id = parent::get_merchant_user_seq();
        $params->ip_address = parent::get_ip_address();
        $params->seq = parent::get_input_get("key");
        $params->merchant_seq = parent::get_merchant_user_seq();
        try {
            $sel_data = $this->M_redeem_merchant->get_data_detail($params);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
                $sel_data1 = $this->M_redeem_merchant->get_redeem_component_by_merchant($params);
                $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data1;
                $sel_data2 = $this->M_redeem_merchant->get_order_merchant_by_redeem($params);
                $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data2;
                $sel_data3 = $this->M_redeem_merchant->get_expedition_merchant_by_redeem($params);
                $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data3;
                if ($sel_data[0]->notif_status == 'U') {
                    $this->M_redeem_merchant->trans_begin();
                    $sel_data = $this->M_redeem_merchant->redeem_merchant_notif_status_update($params);
                    $this->M_redeem_merchant->trans_commit();
                }
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function get_detail_order() {
        $params = new stdClass();
        $params->user_id = parent::get_merchant_user_seq();
        $params->ip_address = parent::get_ip_address();
        $key = explode("~", parent::get_input_get("key"));
        $params->order_no = $key[0];
        $params->redeem_seq = $key[1];
        $params->merchant_seq = parent::get_merchant_user_seq();

        try {
            $sel_data = $this->M_redeem_merchant->get_redeem_detail_order($params);


            if (isset($sel_data)) {
                $output = array();
                foreach ($sel_data as $data_row) {
                    $row = array("seq" => $data_row->seq,
                        "order_no" => $data_row->order_no,
                        "order_date" => parent::cdate($data_row->order_date),
                        "total_order" => number_format($data_row->total_order),
                        "total_payment" => number_format($data_row->total_payment),
                        "order_status" => parent::cstdes($data_row->order_status, STATUS_ORDER),
                        "total_merchant" => number_format($data_row->total_merchant),
                        "total_ins" => number_format($data_row->total_ins),
                        "total_ship_real" => number_format($data_row->total_ship_real),
                        "total_ship_charged" => number_format($data_row->total_ship_charged),
                        "totalorder" => number_format($data_row->totalorder),
                        "totalexpedition" => number_format($data_row->totalexpedition),
                        "qty" => number_format($data_row->qty),
                        "sell_price" => number_format($data_row->sell_price),
                        "totalins" => number_format($data_row->totalins),
                        "totalfee" => number_format($data_row->totalfee),
                        "totalredeem" => number_format($data_row->totalorder - $data_row->totalfee),
                        "exp_fee_percent" => number_format($data_row->exp_fee_percent),
                        "product_name" => ($data_row->product_name),
                        "variant_name" => ($data_row->variant_name)
                    );
                    $output[] = $row;
                }
                echo json_encode($output);
            }
            die();
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

}

?>