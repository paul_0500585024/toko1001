<?php

require_once CONTROLLER_BASE_MERCHANT;

class C_order_merchant extends controller_base_merchant {

    private $data;

    public function __construct() {
	$this->data = parent::__construct("MTRX01001", "merchant/transaction/order");
	$this->initialize();
    }

    private function initialize() {
	$this->load->model('merchant/transaction/M_order_merchant');
	parent::register_event($this->data, ACTION_SEARCH, "search");
	if ($this->data[DATA_INIT] === true) {
	    parent::fire_event($this->data);
	}
    }

    public function index() {
	if (!$this->input->post()) {
	    $this->data[FILTER] = 'merchant/transaction/order_merchant_f.php';
	    $this->load->view("merchant/transaction/order_merchant", $this->data);
	} else {
	    if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
		$filter = new stdClass;
		$filter->user_id = parent::get_merchant_user_id();
		$filter->ip_address = parent::get_ip_address();
		$filter->merchant_name = '';
		$merchant_list = $this->M_order_merchant->get_merchant_by_name($filter);
		$this->data[MERCHANT_LIST] = $merchant_list;
		if ($this->data[DATA_ERROR][ERROR] === true) {
		    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
			$this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
			$this->load->view("merchant/transaction/order", $this->data);
		    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
			$this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
			$this->load->view("merchant/transaction/order", $this->data);
		    }
		} else {
		    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
			$this->data[DATA_AUTH][FORM_ACTION] = "";
		    }
		    $admin_info[SESSION_DATA] = $this->data;
		    $this->session->set_userdata($admin_info);
		    redirect(base_url("merchant/transaction/order"));
		}
	    }
	}
    }

    public function search() {

	$filter = new stdClass;
	$filter->user_id = parent::get_merchant_user_id();
	$filter->ip_address = parent::get_ip_address();
	$filter->start = parent::get_input_post("start");
	$filter->length = parent::get_input_post("length");
	$filter->order = parent::get_input_post("order");
	$filter->column = parent::get_input_post("column");
	$filter->mt_seq = parent::get_merchant_user_seq();
	$filter->order_no_f = parent::get_input_post("order_no_f");
	$filter->order_date_s = parent::get_input_post("order_date_s");
	$filter->order_date_e = parent::get_input_post("order_date_e");
	$filter->receiver_name_f = parent::get_input_post("receiver_name_f");
	$filter->status_order_f = parent::get_input_post("status_order_f");
	$filter->status_payment_f = parent::get_input_post("status_payment_f");
	try {
	    $list_data = $this->M_order_merchant->get_list($filter);
	    parent::set_list_data($this->data, $list_data);
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	}
	$output = array(
	    "sEcho" => parent::get_input_post("draw"),
	    "iTotalRecords" => $list_data[0][0]->total_rec,
	    "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
	    "aaData" => array()
	);

	if (isset($list_data[1])) {
	    foreach ($list_data[1] as $data_row) {
		$row = array("DT_RowId" => $data_row->mt_seq,
		    "member_name" => parent::cdef($data_row->member_name),
		    "order_no" => parent::cdef($data_row->order_no),
		    "order_date" => parent::cdate($data_row->order_date),
		    "receiver_name" => parent::cdef($data_row->receiver_name),
		    "receiver_address" => parent::cdef($data_row->receiver_address),
		    "order_status" => parent::cstdes($data_row->order_status, STATUS_ORDER),
		    "paid_date" => parent::cdate($data_row->paid_date),
		    "produk" => "<a href='order_list_detail/" . $data_row->seq . "'>Detil</a>"
		);
		$output['aaData'][] = $row;
	    }
	};
	echo json_encode($output);
    }

}

?>