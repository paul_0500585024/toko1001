<?php

require_once CONTROLLER_BASE_MERCHANT;

class C_store_decoration extends controller_base_merchant {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("MTRX00010", "merchant/master/store_decoration", TRUE);
        $this->initialize();
    }

    private function initialize() {
        $this->load->library('slim');
        $this->load->model(LIVEVIEW . "merchant/master/M_store_decoration");
        $this->load->model('component/M_dropdown_list');

        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_ADDITIONAL, "open_store");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        if (!$this->input->post()) {
            if ($this->data[DATA_AUTH][FORM_ACTION] != ACTION_ADDITIONAL) {
                $this->data['breadcrumb'] = parent::get_breadcrumb($this->data);
            }
            $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
            $this->get_edit();
            $this->load->view(LIVEVIEW . "merchant/master/store_decoration", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view(LIVEVIEW . "merchant/master/store_decoration", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view(LIVEVIEW . "merchant/master/store_decoration", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    protected function get_edit() {

        $selected = new stdClass();
        $selected->user_id = parent::get_merchant_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = parent::get_merchant_user_seq();

        try {
            $sel_data = $this->M_store_decoration->get_data($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_update() {
        $b = parent::get_input_post('co-status');
        $date = parent::get_input_post('store_status');
        $a = explode(' - ', $date);
        $merchant_name = parent::get_merchant_user_name();
        $string = preg_replace("/[^A-Za-z0-9 ]/", '', $merchant_name);
        $merchant_name = str_ireplace(" ", "_", $string);
        $logo = $this->save_image('logo_img', parent::get_merchant_user_seq(), 'l', $merchant_name);
        $banner = $this->save_image('banner_img', parent::get_merchant_user_seq(), 'b', $merchant_name);
        $params = new stdClass();
        $params->seq = parent::get_merchant_user_seq();
        $params->userid = parent::get_merchant_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->old_banner_img = parent::get_input_post("old_banner");
        $params->old_logo_img = parent::get_input_post("old_logo");
        $params->logo_img = $logo->error == true ? $params->old_logo_img : $logo->file_name;
        $params->banner_img = $banner->error == true ? $params->old_banner_img : $banner->file_name;
        $params->description = parent::get_input_post("description");
        $this->data[DATA_SELECTED][LIST_DATA][] = $params;
        if ($b === 'on' || $b === 'ON') {
            $params->store_close_start = $a[0];
            $params->store_close_end = $a[1];
        } else {
            $params->store_close_start = DEFAULT_DATE;
            $params->store_close_end = DEFAULT_DATE;
        }
        if ($params->store_close_start == '' && $params->store_close_end == '') {
            $this->data[DATA_ERROR][ERROR] = true;
            $this->data[DATA_ERROR][ERROR_MESSAGE] = CHECK_OPEN_ADN_CLOSE_STORE_DATE;
        }

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_store_decoration->trans_begin();
                $this->M_store_decoration->save_update($params);
                if ($logo->file_name != '' && $banner->file_name != '' || $logo->data != '' && $banner->data != '') {
                    $test = $this->slim->saveFile($logo->data, $logo->file_name, $logo->url, false);
                    $this->slim->saveFile($banner->data, $banner->file_name, $banner->url, false);
                }
                $this->M_store_decoration->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_store_decoration->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_store_decoration->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_store_decoration->trans_rollback();
            }
        }
    }

    protected function save_image($form_name, $merchantSeq, $type, $merchant_name) {
        $folder = PROFILE_IMAGE_MERCHANT . $merchantSeq;
        $image = $this->slim->getImages($form_name); //get image from form input
        if (isset($image[0]['input']['name'])) {
            if (preg_match("/\.(" . IMAGE_TYPE . ")$/", $image[0]['input']['name'])) {
                $ext = end((explode(".", $image[0]['input']['name']))); //jpg, png or other
                if ($type === 'l') {
                    $dimension = (object) unserialize(IMAGE_DIMENSION_LOGO);
                } else if ($type === 'b') {
                    $dimension = (object) unserialize(IMAGE_DIMENSION_BANNER);
                }
                if ($image[0]['input']['size'] > DEFAULT_MAX_LIMIT_IMAGE) {
                    $this->data[DATA_ERROR][ERROR] = true;
                    $this->data[DATA_ERROR][ERROR_MESSAGE][] = "ERROR LOGO :" . ERROR_MAX_LIMIT_IMAGE;
                } else if ($image[0]['output']['width'] > $dimension->max_width) {
                    $this->data[DATA_ERROR][ERROR] = true;
                    $this->data[DATA_ERROR][ERROR_MESSAGE][] = "ERROR LOGO :" . ERROR_DIMENSION_IMAGE;
                }
            }
        }
        if ($this->data[DATA_ERROR][ERROR] == false) {
            if (isset($image[0]['input']['name'])) {
                $a = new stdClass();
                $a->error = false;
                $a->url = $folder . '/';
                $a->file_name = $type . '-' . $merchant_name . '.' . $ext;
                $a->data = $image[0]['output']['data'];
            } else {
                $a = new stdClass();
                $a->error = false;
                $a->url = $folder . '/';
                $a->file_name = '';
                $a->data = '';
            }
        } else {
            $a = new stdClass();
            $a->error = true;
            $a->url = $folder . '/';
            $a->file_name = $type . '-' . $merchant_name . '.' . $ext;
            $a->data = $image[0]['output']['data'];
        }
        return $a;
    }

}
