<?php

require_once CONTROLLER_BASE_MERCHANT;

class C_product_merchant extends controller_base_merchant {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("MMST01002", "merchant/master/product_merchant");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model(LIVEVIEW . 'merchant/master/M_product_merchant');
        $this->load->model(LIVEVIEW . 'merchant/transaction/M_trans_log_merchant');
        $this->load->library('slim');
        $this->load->library('pagination');
        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_APPROVE, "approve");
        parent::register_event($this->data, ACTION_REJECT, "reject");
        parent::register_event($this->data, ACTION_ADDITIONAL, "get_ajax");
//        if ($this->data[DATA_INIT] === true) {
        parent::fire_event($this->data);
//        }
    }

    public function index() {
        $this->data['breadcrumb'] = parent::get_breadcrumb($this->data);
        if (!$this->input->post()) {
            $this->load->view(LIVEVIEW . "merchant/master/product_merchant", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                $filter = new stdClass;
                $filter->user_id = parent::get_merchant_user_seq();
                $filter->ip_address = parent::get_ip_address();
                $filter->merchant_name = '';
                $merchant_list = $this->M_product_merchant->get_merchant_by_name($filter);
                $this->data[MERCHANT_LIST] = $merchant_list;
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view(LIVEVIEW . "merchant/master/product_merchant", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view(LIVEVIEW . "merchant/master/product_merchant", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_SEARCH;
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    if (!isset($_COOKIE[$this->data[DATA_AUTH][FORM_CD]])) {
                        redirect(base_url("merchant/master/product_merchant"));
                    } else {
                        redirect($_COOKIE[$this->data[DATA_AUTH][FORM_CD]]);
                    }
                }
            }
        }
    }

    protected function get_ajax() {

        $tipe = parent::get_input_post("tipe");
        $idh = parent::get_input_post("idh");
        $filter = new stdClass;
        $filter->user_id = parent::get_merchant_user_seq();
        $filter->ip_address = parent::get_ip_address();

        switch ($tipe) {
            case "category":
                $idh = parent::get_merchant_user_seq();
                $output = array();
                $merchsq = array();
                $merchantlvl1 = $this->M_product_merchant->get_merchant_lvl1($idh);
                $menulst = $this->M_product_merchant->get_product_category();
                if (isset($merchantlvl1)) {
                    foreach ($merchantlvl1 as $datar) {
                        $merchsq[] = $datar->category_l1_seq;
                    }
                    if (isset($menulst)) {
                        foreach ($menulst as $data_row) {

                            if (in_array($data_row->seq, $merchsq)) {
                                $output[$data_row->parent_seq][] = $data_row;
                            }
                            if (in_array($data_row->parent_seq, $merchsq)) {
                                $output[$data_row->parent_seq][] = $data_row;
                                $merchsq[] = $data_row->seq;
                            }
                        }
                    }
                }
                die($this->get_treecategory($output, 0));
                break;
            case "breadcrumb":
                $menulst = $this->M_product_merchant->get_product_category_by_child($idh);
                die($this->get_breadcrumb($menulst));
                break;
            case "attribute":
                $filter->idh = $idh;
                $atrval = parent::get_input_post("atrval");
                $menulst = $this->M_product_merchant->get_attribute_by_category($filter);
                die($this->get_atribute($menulst, $atrval));
                break;
            case "variant":
                $idh = explode(",", $idh);
                $filter->idh = $idh[1];
                $menulst = $this->M_product_merchant->get_variant_by_category($filter);
                die($this->get_varian($menulst));
                break;
            case TASK_TYPE_SET_PRODUCT_ACTIVE :
                $this->save_update_active_product();
                die();
                break;
            case TASK_TYPE_SET_PRODUCT_STOCK :
                $this->save_update_stock();
                die();
                break;
            case TASK_TYPE_SET_PRODUCT_PRICE :
                $this->save_update_product_price();
                die();
                break;
            case TASK_TYPE_SET_PRODUCT_SELL_PRICE :
                $this->save_update_sell_price();
                die();
                break;
        }
    }

    function get_varian($idvar) {
        $data = '<div class="row" style="border:1px solid #ddd;"><div class="col-xs-12"><br><br>';
        if ($idvar) {
            foreach ($idvar as $variant) {

                $data .= '<div style="width:100px !important;float:left;margin-right:10px !important;">'
                        . '<div class="well no-pad no-border-radius" style="padding-left:25px !important;position:relative">'
                        . '<label>'
                        . '<input id="variant-' . $variant->seq . '" onclick="addVariant(\'' . $variant->value . '\',\'' . $variant->seq . '\', this)" '
                        . 'type="checkbox" class="variant pull-right">'
                        . $variant->value .
                        '</label></div></div>';
            }
        } else {
            $data.='<p><center>variant tidak tersedia di kategori ini</center></p>';
        }
        $data .= '</div></div>';

        return $data;
    }

    function get_breadcrumb($idcat) {
        $data = '<h4><span class="label label-danger">' . $idcat[0]->path . '<input type="hidden" name="alcat" id="alcat" value="' . $idcat[0]->idpath . '"></span></h4>';
        return $data;
    }

    function get_atribute($idattribute, $atrval = '') { {
            $data = "<table class='table table-striped'>";
            $row = "";
            $i = 0;
            $slected = "";
            $selected = new stdClass;
            $selected->user_id = parent::get_merchant_user_seq();
            $selected->ip_address = parent::get_ip_address();
            if ($idattribute) {
                foreach ($idattribute as $data_row) {
                    $data.="<tr><td>" . $data_row->name . "</td><td><select id='attribut' name='attribut[]' class='form-control'><option value=''></option>";
                    $selected->seq = $data_row->seq;
                    $sel_data = $this->M_product_merchant->get_attribute_value($selected);
                    if (isset($sel_data)) {
                        foreach ($sel_data as $data_rows) {
                            if ($data_rows->seq != '') {
                                $slected = "";
                                $nilai = '{' . $data_rows->seq . '}';
                                $slected = strripos($atrval, $nilai);
                                if ($slected === false) {
                                    $slected = "";
                                } else {
                                    $slected = " selected";
                                }
                            }
                            $data.="<option value='" . $data_rows->seq . "'" . $slected . ">" . $data_rows->value . "</option>";
                        }
                    }
                    $data.="</select></td></tr>";
                }
            }
            $data.="</table>";
            return $data;
        }
    }

    function get_treecategory($datas, $parent = 0, $p_level1 = 0, $p_level2 = 0) {
        static $i = 1;
        if (isset($datas[$parent])) {
            $html = "<ul type=disc>";
            $i++;
            foreach ($datas[$parent] as $vals) {
                $g = $i;
                if ($vals->level == 1) {
                    $this->parent_level1 = $vals->seq;
                    $p_level1 = $vals->seq;
                }
                if ($vals->level == 2) {
                    $this->parent_level2 = $vals->seq;
                    $p_level2 = $vals->seq;
                }
                $child = $this->get_treecategory($datas, $vals->seq, $this->parent_level1, $this->parent_level2);
                if ($i != $g) {
                    $html .= '<li><label for="folder' . $i . '"><b>' . $vals->name . '</b></label>';
                } else {
                    $html.='<li><input onclick="cekseq(' . $vals->seq . ')" type="radio" id="folder' . $vals->seq . '" class="radiobtn" name="catseq" value="' . $vals->seq . '~' . $p_level1 . '~' . $p_level2 . '" required /> ' . $vals->name;
                }
                if ($child) {
                    $i++;
                    $html .= $child;
                }
                $html .= '</li>';
            }
            $html .= "</ul>";
            return $html;
        } else {
            return false;
        }
    }

    public function search() {
//        die('test');    
        $filter = new stdClass();
        $filter->user_id = parent::get_merchant_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->status = parent::get_input_get("status") !== LIVE_STATUS_CODE && parent::get_input_get("status") != CHANGE_STATUS ? "" : parent::get_input_get("status");
        $filter->search = parent::get_input_get("search");
        $filter->merchant_seq = parent::get_merchant_user_seq();
        $filter->sort = parent::get_input_get("sort") != "desc" ? "asc" : "desc";
        $filter->column = parent::get_input_get("column_sort") == "" ? "name" : parent::get_input_get("column_sort");
        $filter->start = parent::get_input_get(START_OFFSET) == "" ? 0 : parent::get_input_get(START_OFFSET);
        $filter->length = PAGINATION_LIMIT;

        $list_data = $this->M_product_merchant->get_merchant_list($filter);
        parent::set_data($this->data, $list_data);
        $this->data['filter'] = "&search=" . $filter->search;
        $this->data['filter_sort'] = "&sort=" . $filter->sort . "&column_sort=" . $filter->column;
        $this->data['filter_status'] = "?status=" . $filter->status;
        $array = json_decode(json_encode($list_data[0]), true);
        $key = array_search($filter->status, array_column($array, 'status_cd'));
        $this->display_page(base_url("merchant/master/product_merchant") . $this->data['filter_status'] . $this->data['filter'] . $this->data['filter_sort'] = "&sort=" . $filter->sort . "&column_sort=" . $filter->column, $filter->start, $list_data[0][$key]->total, $filter->length);
        setcookie($this->data[DATA_AUTH][FORM_CD], base_url(uri_string() . "?" . $_SERVER["QUERY_STRING"]), time() + COOKIE_TIME, "/");
    }

    protected function get_edit() {
        $selected = new stdClass();
        $selected->user_id = parent::get_merchant_user_seq();
        $selected->merchant_seq = parent::get_merchant_user_seq();
        $selected->ip_address = parent::get_ip_address();
        $selected->seq = parent::get_input_get("key");
        try {
            $sel_data = $this->M_product_merchant->get_data($selected);
            if ($sel_data[0]->merchant_seq <> parent::get_merchant_user_seq()) {
                redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
            }
            $status = $sel_data[0]->status;
            if ($status == "L") {
                $sel_data1 = $this->M_product_merchant->get_product_attribute_by_product_seq($selected);
                $sel_data2 = $this->M_product_merchant->get_product_spec_by_product_seq($selected);
                $sel_data3 = $this->M_product_merchant->get_product_variant_by_product_seq($selected);
            } else {
                $getlogseq = $this->M_product_merchant->get_product_log_seq($selected);
                $selected->log_seq = $getlogseq[0]->log_seq;
                $sel_data = $this->M_product_merchant->get_product_log_info_by_product_seq($selected);
                $sel_data1 = $this->M_product_merchant->get_product_log_attribute_by_product_seq($selected);
                $sel_data2 = $this->M_product_merchant->get_product_log_spec_by_product_seq($selected);
                $sel_data3 = $this->M_product_merchant->get_product_log_variant_by_product_seq($selected);
            }
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
                $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data1;
                $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data2;
                $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data3;
            } else {
                redirect(base_url("merchant/master/product_merchant"));
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_update_stock() {
        $params = new stdClass();
        $params->user_id = parent::get_merchant_email();
        $params->merchant_seq = parent::get_merchant_user_seq();
        $params->ip_address = parent::get_ip_address();
        $params->stock = parent::get_input_post("stock", true, STOCK_VALIDATOR, "Stok", $this->data);
        $params->product_variant_seq = parent::get_input_post("product_variant_seq");
        $params->merchant_sku = parent::get_input_post("merchant_sku");
        $params->variant_value_seq = DEFAULT_VALUE_VARIANT_SEQ;
        $params->trx_no = "";
        $params->trx_type = STOCK_ADJUSTMENT_TYPE;

        if ($this->data[DATA_ERROR][ERROR] !== true) {
            try {
                $this->M_product_merchant->trans_begin();
                $stock = $this->M_product_merchant->update_stock_product($params);
                $params->stock_add = $params->stock;
                $params->stock = isset($stock) ? $stock[0]->stock : 0;
                if ($params->stock != $params->stock_add) {
                    $this->get_mutation_type($params);
                }
                $this->M_product_merchant->trans_commit();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_product_merchant->trans_rollback();
            }
        }
    }

    protected function save_update_product_price() {
        $params = new stdClass();
        $params->user_id = parent::get_merchant_email();
        $params->merchant_seq = parent::get_merchant_user_seq();
        $params->ip_address = parent::get_ip_address();
        $params->product_variant_seq = parent::get_input_post("product_variant_seq", true, FILL_VALIDATOR, "Produk", $this->data);
        $params->product_price = parent::get_input_post("product_price", true, FILL_VALIDATOR, "Harga Promo", $this->data);
        if ($this->data[DATA_ERROR][ERROR] !== true) {
            try {
                $this->M_product_merchant->trans_begin();
                $valid = $this->M_product_merchant->update_product_price($params);
                $this->M_product_merchant->trans_commit();
                echo json_encode(array("error" => !$valid[0]->valid));
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_product_merchant->trans_rollback();
            }
        }
    }

    protected function save_update_sell_price() {
        $params = new stdClass();
        $params->user_id = parent::get_merchant_email();
        $params->merchant_seq = parent::get_merchant_user_seq();
        $params->ip_address = parent::get_ip_address();
        $params->product_variant_seq = parent::get_input_post("product_variant_seq", true, FILL_VALIDATOR, "Produk", $this->data);
        $params->sell_price = parent::get_input_post("sell_price", true, FILL_VALIDATOR, "Harga Promo", $this->data);
        if ($this->data[DATA_ERROR][ERROR] !== true) {
            try {
                $this->M_product_merchant->trans_begin();
                $valid = $this->M_product_merchant->update_sell_price($params);
                $this->M_product_merchant->trans_commit();
                echo json_encode(array("error" => !$valid[0]->valid));
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_product_merchant->trans_rollback();
            }
        }
    }

    protected function get_mutation_type($data) {

        $difference_stock = ($data->stock_add - $data->stock);

        Switch ($difference_stock) {
            case $difference_stock < 0 :
                $mutation_type = OUT_MUTATION_TYPE;
                break;
            case $difference_stock > 0 : {
                    $mutation_type = IN_MUTATION_TYPE;
                }
                break;
        }

        $params = new stdClass();
        $params->user_id = $data->user_id;
        $params->ip_address = $data->ip_address;
        $params->product_variant_seq = $data->product_variant_seq;
        $params->variant_value_seq = $data->variant_value_seq;
        $params->trx_no = $data->trx_no;
        $params->trx_type = $data->trx_type;
        $params->qty = ($difference_stock < 0 ? - $difference_stock : $difference_stock);
        $params->mutation_type = $mutation_type;

        try {
            $this->M_trans_log_merchant->save_merchant_stock_log($params);
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_update_active_product() {
        $params = new stdClass();
        $params->user_id = parent::get_merchant_user_id();
        $params->merchant_seq = parent::get_merchant_user_seq();
        $params->ip_address = parent::get_ip_address();
        $params->active = parent::get_input_post("active");
        $params->active = $params->active === "true" ? "1" : "0";
        $params->product_variant_seq = parent::get_input_post("product_variant_seq");
        try {
            $this->M_product_merchant->update_active_product($params);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_update() {
        $params = new stdClass();
        $params->user_id = parent::get_merchant_user_seq();
        $params->ip_address = parent::get_ip_address();
        $params->m_merchant_seq = "M-" . parent::get_merchant_user_seq();
        $params->merchant_seq = parent::get_merchant_user_seq();

        $params->seq = parent::get_input_post("key");
        $sel_data = $this->M_product_merchant->get_data($params);

        if ($sel_data[0]->status != "L") {
            $this->data[DATA_ERROR][ERROR] = true;
            $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_UPDATE;
            $this->get_edit();
        } else {
            $params->name = parent::clength(parent::get_input_post("name", true, FILL_VALIDATOR, "Nama Produk", $this->data), 150, $this->data);
            $params->include_ins = parent::get_input_post("include_ins");
            $params->notes = "";
            $params->description = parent::get_input_post("description", true, FILL_VALIDATOR, "Deskripsi", $this->data);
            $params->content = parent::get_input_post("content");
            $params->specification = parent::get_input_post("specification");
            $params->warranty_notes = parent::clength(parent::get_input_post("warranty_notes"), 100, $this->data);
//            $params->p_length_cm = parent::get_input_post("p_length_cm", true, QTY_VALIDATOR, "Dimensi Panjang Produk", $this->data);
//            $params->p_width_cm = parent::get_input_post("p_width_cm", true, QTY_VALIDATOR, "Dimensi Lebar Produk", $this->data);
//            $params->p_height_cm = parent::get_input_post("p_height_cm", true, QTY_VALIDATOR, "Dimensi Tinggi Produk", $this->data);
//            $params->b_length_cm = parent::get_input_post("b_length_cm", true, QTY_VALIDATOR, "Dimensi Panjang Dus", $this->data);
//            $params->b_width_cm = parent::get_input_post("b_width_cm", true, QTY_VALIDATOR, "Dimensi Lebar Dus", $this->data);
//            $params->b_height_cm = parent::get_input_post("b_height_cm", true, QTY_VALIDATOR, "Dimensi Tinggi Dus", $this->data);
//            $params->p_weight_kg = parent::get_input_post("p_weight_kg");
//            $params->b_weight_kg = parent::get_input_post("b_weight_kg");
//	    $params->p_weight_kg = parent::get_input_post("p_weight_kg", true, QTY_VALIDATOR, "Berat Produk", $this->data);
            $params->p_weight_kg = parent::get_input_post("p_weight_kg");
//	    $params->b_weight_kg = parent::get_input_post("p_weight_kg", true, QTY_VALIDATOR, "Berat Dengan Dus", $this->data);
            $params->b_weight_kg = parent::get_input_post("p_weight_kg");
            $params->p_length_cm = parent::get_input_post("p_length_cm");
            $params->p_width_cm = parent::get_input_post("p_width_cm");
            $params->p_height_cm = parent::get_input_post("p_height_cm");
            $params->b_length_cm = parent::get_input_post("p_length_cm");
            $params->b_width_cm = parent::get_input_post("p_width_cm");
            $params->b_height_cm = parent::get_input_post("p_height_cm");
            $params->attribut = $this->input->post("attribut");
            $params->spekname = $this->input->post("spekname");
            $params->spekseqhid = $this->input->post("spekseqhid");
            $params->spekval = $this->input->post("spekval");
            $params->varian = $this->input->post("varian");
            $params->variant_value_seq = $this->input->post("variant_value_seq");
//            $params->new_variant_value_seq = $this->input->post("new_variant_value_seq");
            $params->disc_percent = $this->input->post("disc_percent");
            $params->product_price = $this->input->post("product_price");
            $params->sell_price = $this->input->post("sell_price");
            $params->order = $this->input->post("order");
            $params->max_buy = $this->input->post("max_buy");
            $params->variant_seq = $this->input->post("variant_seq");
            $params->value = $this->input->post("value");
            $params->active = $this->input->post("aktif");
            $params->status = parent::get_input_post("status");
            $params->category_ln_seq = parent::get_input_post("category_ln_seq");

            $filesave = array();
            $params->product_seq = $params->seq;
            $status = $sel_data[0]->status;
            $maxseq = 1;
            $this->M_product_merchant->trans_begin();
            $cek_log = $this->M_product_merchant->add_product_log($params);
            $params->log_seq = $cek_log[0][0]->log_seq;

            $this->data[DATA_SELECTED][LIST_DATA][] = $params;
            if ($params->attribut != "") {
                foreach ($params->attribut as $key => $val) {
                    $this->data[DATA_SELECTED][LIST_DATA][1][] = (object) array("attribute_value_seq" => $params->attribut[$key], "new_attribute_value_seq" => $params->attribut[$key]);
                }
            }

            foreach ($params->spekname as $key => $val) {
                if ($params->spekname[$key] != '' && $params->spekval[$key] != '')
                    $this->data[DATA_SELECTED][LIST_DATA][2][] = (object) array("name" => $params->spekname[$key], "value" => $params->spekval[$key]);
            }

            $namafolder = str_replace(CDN_IMAGE,"",TEMP_PRODUCT_UPLOAD_IMAGE) . $params->merchant_seq;
            $oldfolder = str_replace(CDN_IMAGE,"",PRODUCT_UPLOAD_IMAGE) . $params->merchant_seq;
            if (!file_exists($namafolder)) {
                mkdir($namafolder, 0777, true);
            }
            $fils = "";
            if (strlen($params->name) > 150) {
                $data[DATA_ERROR][ERROR] = true;
                $data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_VALIDATION_LENGTH_MAX . " 150";
            }
            if ($params->name == "") {
                $data[DATA_ERROR][ERROR] = true;
                $data[DATA_ERROR][ERROR_MESSAGE][] = "Nama" . ERROR_VALIDATION_MUST_FILL;
            } else {
                if (isset($params->variant_value_seq)) {
                    for ($imgs = 0; $imgs < count($this->input->post("variant_value_seq")); $imgs++) {
                        $fils = "OK";
                        $variantseq = $this->input->post("variant_seq")[$imgs];
                        for ($nomori = 1; $nomori < 7; $nomori++) {
                            $oldfile = isset($this->input->post("oldfile" . $nomori)[$imgs]) ? $this->input->post("oldfile" . $nomori)[$imgs] : "";
//                            $image = $this->slim->getImages('ifile' . $nomori);
                            $image = $this->slim->getImages('ifile' . $nomori);
                            if (!empty($image[$imgs]["input"])) {

                                $string = preg_replace("/[^A-Za-z0-9 ]/", '', $params->name);
                                $string = str_ireplace(" ", "_", $string);
                                $name = $image[$imgs]["input"]["name"];
                                if (preg_match("/\.(" . IMAGE_TYPE . ")$/", $name)) {
                                    $ext = end((explode(".", $name)));
                                    $namafile = strtolower($variantseq . "_" . $string . "_" . $nomori . '.' . $ext);
                                    if ($oldfile != '') {
                                        if (!file_exists($namafolder . '/' . $oldfile)) {
                                            $copyimage = copy($oldfolder . '/' . $oldfile, $namafolder . '/' . $namafile);
                                        }
                                    }

                                    $dimension = (object) unserialize(IMAGE_DIMENSION_PRODUCT);

                                    if ($image[$imgs]["input"]["size"] > DEFAULT_MAX_LIMIT_IMAGE) {
                                        $this->data[DATA_ERROR][ERROR] = true;
                                        $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_MAX_LIMIT_IMAGE;
                                    } elseif ($image[$imgs]["input"]["width"] > $dimension->max_width || $image[$imgs]["input"]["width"] < $dimension->min_width || $image[$imgs]["input"]["height"] > $dimension->max_height || $image[$imgs]["input"]["height"] < $dimension->min_height || $image[$imgs]["input"]["height"] != $image[$imgs]["input"]["width"]) {
                                        $this->data[DATA_ERROR][ERROR] = true;
                                        $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_DIMENSION_IMAGE;
                                    }

                                    $namafile = $this->slim->saveFile($image[$imgs]['output']['data'], $namafile, $namafolder, false);
                                    $filesave[$nomori][$imgs] = $namafile["name"];
                                } else {
                                    $this->data[DATA_ERROR][ERROR] = true;
                                    $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_VALIDATION_UPLOAD_TYPE_DOESNT_MATCH;
                                    $filesave[$nomori][$imgs] = "";
                                }
                            } else {
                                if ($oldfile != '') {
                                    $ext = end((explode(".", $oldfile)));
                                    $string = preg_replace("/[^A-Za-z0-9 ]/", '', $params->name);
                                    $string = str_ireplace(" ", "_", $string);
                                    $filesave[$nomori][$imgs] = strtolower($variantseq . "_" . $string . "_" . $nomori . '.' . $ext);
                                    if (file_exists($oldfolder . '/' . $oldfile)) {
                                        $copyimage = copy($oldfolder . '/' . $oldfile, $namafolder . '/' . $filesave[$nomori][$imgs]);
                                    } else {
                                        if (file_exists($namafolder . '/' . $oldfile)) {
                                            $copyimage = copy($namafolder . '/' . $oldfile, $namafolder . '/' . $filesave[$nomori][$imgs]);
                                        }
                                    }
                                } else {
                                    $filesave[$nomori][$imgs] = $oldfile;
                                }
                            }
                        }
                        $maxseq++;
                    }
                    if ($fils == "") {
                        $filesave[1] = '';
                        $filesave[2] = '';
                        $filesave[3] = '';
                        $filesave[4] = '';
                        $filesave[5] = '';
                        $filesave[6] = '';
                    }
                    $params->file_name1 = $filesave[1];
                    $params->file_name2 = $filesave[2];
                    $params->file_name3 = $filesave[3];
                    $params->file_name4 = $filesave[4];
                    $params->file_name5 = $filesave[5];
                    $params->file_name6 = $filesave[6];
                }
                if ($params->variant_value_seq != '') {
                    foreach ($this->input->post("variant_value_seq") as $key => $val) {
                        if ($params->product_price[$key] != "" && $params->product_price[$key] != "0" && $params->sell_price[$key] != "" && $params->sell_price[$key] != "0") {
                            
                        } else {
                            $this->data[DATA_ERROR][ERROR] = true;
                            $this->data[DATA_ERROR][ERROR_MESSAGE][] = "Harga Produk dan Harga Jual " . ERROR_VALIDATION_MUST_FILL;
                        }
                        if ($params->file_name1[$key] == "") {

                            $this->data[DATA_ERROR][ERROR] = true;
                            $this->data[DATA_ERROR][ERROR_MESSAGE][] = "ERROR : Gambar Utama " . ERROR_VALIDATION_MUST_FILL;
                        }

                        $this->data[DATA_SELECTED][LIST_DATA][3][] = (object) array(
                                    "value" => $params->value[$key],
                                    "seq" => $params->variant_seq[$key],
                                    "variant_value_seq" => $params->variant_value_seq[$key],
                                    "disc_percent" => $params->disc_percent[$key],
                                    "product_price" => $params->product_price[$key],
                                    "sell_price" => $params->sell_price[$key],
                                    "order" => $params->order[$key],
                                    "max_buy" => $params->max_buy[$key],
                                    "variant_seq" => $params->variant_seq[$key],
                                    "pic_1_img" => $params->file_name1[$key],
                                    "pic_2_img" => $params->file_name2[$key],
                                    "pic_3_img" => $params->file_name3[$key],
                                    "pic_4_img" => $params->file_name4[$key],
                                    "pic_5_img" => $params->file_name5[$key],
                                    "pic_6_img" => $params->file_name6[$key],
                                    "new_value" => $params->value[$key],
                                    "active" => $params->active[$key]);
                    }

                    if ($params->disc_percent[$key] < 0) {
                        $this->data[DATA_ERROR][ERROR] = true;
                        $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_DISCOUNT;
                    }
                }

                if ($this->data[DATA_ERROR][ERROR] === false) {
                    try {
                        $this->M_product_merchant->trans_begin();
                        $this->M_product_merchant->save_add_log_data_old($params);
                        $this->M_product_merchant->save_add_log_data_new($params);
                        $params->product_seq = $params->seq;
                        if (count($params->attribut) > 0) {
                            foreach ($params->attribut as $params->new_attribut) {
                                if ($params->new_attribut != '') {
                                    $getatrseq = $this->M_product_merchant->get_attribute_seq($params);
                                    $inputatr = new stdClass();
                                    $inputatr->merchant_seq = $params->user_id;
                                    $inputatr->attribute_seq = $getatrseq[0]->atr_seq;
                                    $inputatr->seq = $params->seq;
                                    $getoldatr = $this->M_product_merchant->get_old_attribute_val($inputatr);
                                    if (!empty($getoldatr[0]->attribute_value_seq)) {
                                        $inputatr->old_attribute_value_seq = $getoldatr[0]->attribute_value_seq;
                                    } else {
                                        $inputatr->old_attribute_value_seq = "0";
                                    }
                                    $inputatr->new_attribut = $params->new_attribut;
                                    $inputatr->log_seq = $params->log_seq;
                                    $this->M_product_merchant->update_product_attribute($inputatr);
                                }
                            }
                        }
                        $this->M_product_merchant->add_old_spec($params);
                        $this->M_product_merchant->save_add_spec($params);
                        $this->M_product_merchant->save_add_variant($params);
                        if ($params->new_variant_value_seq != '') {
                            $this->M_product_merchant->save_add_variant_new($params);
                        }
                        $this->M_product_merchant->change_product_status($params);
                        $this->M_product_merchant->trans_commit();
                    } catch (BusisnessException $ex) {
                        parent::set_error($this->data, $ex);
                        $this->M_product_merchant->trans_rollback();
                    } catch (TechnicalException $ex) {
                        parent::set_error($this->data, $ex);
                        $this->M_product_merchant->trans_rollback();
                    } catch (Exception $ex) {
                        parent::set_error($this->data, $ex);
                        $this->M_product_merchant->trans_rollback();
                    }
                }
            }
        }
    }

}

?>