<?php
require_once CONTROLLER_BASE_MERCHANT;

class C_merchant_logo extends controller_base_merchant {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("MCON01002", "merchant/master/logo_banner_merchant");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model(LIVEVIEW . "merchant/master/M_logo_merchant");
        $this->load->model(LIVEVIEW . 'merchant/master/M_info_merchant');
        $this->load->model('admin/master/M_merchant_admin');
        $this->load->model('component/M_dropdown_list');

        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_ADDITIONAL, "get_ajax");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        $this->data[PROVINCE_NAME] = $this->M_dropdown_list->get_dropdown_province();
        $this->data[EXPEDITION_NAME] = $this->M_dropdown_list->get_dropdown_expedition();
        $this->data[CITY_NAME] = "";

        if (!$this->input->post()) {
            $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
            $this->get_edit();
            $this->load->view(LIVEVIEW . "merchant/master/merchant_logo", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view(LIVEVIEW . "merchant/master/merchant_logo", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view(LIVEVIEW . "merchant/master/merchant_logo", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    protected function get_edit() {
        $selected = new stdClass();
        $selected->user_id = parent::get_merchant_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = parent::get_merchant_user_seq();

        try {
            $sel_data = $this->M_logo_merchant->get_data($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $params = new stdClass();
        $params->user_id = parent::get_merchant_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->merchant_seq = parent::get_merchant_user_seq();

        try {
            $data_sel = $this->M_info_merchant->get_data($params);
            if (isset($data_sel)) {
                parent::set_data_header($this->data, $data_sel);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function get_dropdown_city($province_seq) {
        try {
            $this->data[CITY_NAME] = $this->M_dropdown_list->get_dropdown_city_by_province($province_seq);
            die(json_encode($this->data[CITY_NAME]));
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function get_dropdown_district($city_seq) {
        try {
            $list_data = $this->M_dropdown_list->get_dropdown_district_by_city($city_seq);
            die(json_encode($list_data));
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function get_ajax() {
        $tipe = parent::get_input_post("tipe");
        $idh = parent::get_input_post("idh");
        switch ($tipe) {
            case "city":
                $this->get_dropdown_city($idh);
                break;
            case "district":
                $this->get_dropdown_district($idh);
                break;
            case "valpcd":
                $valpcd = $this->M_merchant_admin->get_val_pcd($idh);
                die(json_encode($valpcd));
                break;
            case "email":
                if ($idh != '') {
                    $msg = "OK";
                    $list_data = $this->M_merchant_admin->get_val_email($idh);
                    if (isset($list_data[0])) {
                        $msg = "ERROR";
                    }
                } else {
                    $msg = "ERROR";
                }
                die($msg);
                break;
            case "tblmsg":
                $this->get_message_log_list();
                break;
            case "msgsave":
                $this->update_message();
                break;
            case "onemsg":
                $this->get_message_one_topic();
                break;
        }
    }

    protected function save_update() {

        $merchant_name = parent::get_merchant_user_name();
        $string = preg_replace("/[^A-Za-z0-9 ]/", '', $merchant_name);
        $merchant_name = str_ireplace(" ", "_", $string);

        $banner = parent::upload_file("banner_img", MERCHANT_LOGO . parent::get_merchant_user_seq() . "/", "b-" . $merchant_name, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_BANNER, "Banner ");
        $logo = parent::upload_file("logo_img", MERCHANT_LOGO . parent::get_merchant_user_seq() . "/", "l-" . $merchant_name, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_LOGO, "Logo ");

        $params = new stdClass();
        $params->userid = parent::get_merchant_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->old_banner_img = parent::get_input_post("old_banner");
        $params->old_logo_img = parent::get_input_post("old_logo");
        $params->seq = parent::get_merchant_user_seq();
        $params->logo_img = $logo->error == true ? $params->old_logo_img : $logo->file_name;
        $params->banner_img = $banner->error == true ? $params->old_banner_img : $banner->file_name;
        $params->description = parent::get_input_post("description");

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_logo_merchant->trans_begin();
                $this->M_logo_merchant->save_update($params);
                $this->M_logo_merchant->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_logo_merchant->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_logo_merchant->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_logo_merchant->trans_rollback();
            }
        }

        $params2 = new stdClass();
        $params2->user_id = parent::get_merchant_user_id();
        $params2->ip_address = parent::get_ip_address();
        $params2->merchant_seq = parent::get_merchant_user_seq();

        $data_sel = $this->M_info_merchant->get_data($params2);

        $this->data["update_condition"] = FALSE;

        if ($data_sel[0]->address != parent::get_input_post("address")) {
            $this->data["update_condition"] = TRUE;
        }
        if ($data_sel[0]->district_seq != parent::get_input_post("district_seq")) {
            $this->data["update_condition"] = TRUE;
        }
        if ($data_sel[0]->zip_code != parent::get_input_post("zip_code")) {
            $this->data["update_condition"] = TRUE;
        }
        if ($data_sel[0]->phone_no != parent::get_input_post("phone_no")) {
            $this->data["update_condition"] = TRUE;
        }
        if ($data_sel[0]->fax_no !== parent::get_input_post("fax_no")) {
            $this->data["update_condition"] = TRUE;
        }
        if ($data_sel[0]->pic1_name !== parent::get_input_post("pic1_name")) {
            $this->data["update_condition"] = TRUE;
        }
        if ($data_sel[0]->pic1_phone_no !== parent::get_input_post("pic1_phone_no")) {
            $this->data["update_condition"] = TRUE;
        }
        if ($data_sel[0]->pic2_name !== parent::get_input_post("pic2_name")) {
            $this->data["update_condition"] = TRUE;
        }
        if ($data_sel[0]->pic2_phone_no !== parent::get_input_post("pic2_phone_no")) {
            $this->data["update_condition"] = TRUE;
        }
        if (parent::get_input_post("pickup_addr_eq") == "on") {
            $pickup_addr_eq = (strtolower(parent::get_input_post("pickup_addr_eq")) == 'on' OR
                    parent::get_input_post("pickup_addr_eq") == '1') ? '1' : '0';
            if ($data_sel[0]->pickup_addr_eq !== $pickup_addr_eq) {
                $this->data["update_condition"] = TRUE;
            }
        }
        if ($data_sel[0]->pickup_addr !== parent::get_input_post("pickup_addr")) {
            $this->data["update_condition"] = TRUE;
        }
        if ($data_sel[0]->pickup_zip_code !== parent::get_input_post("pickup_zip_code")) {
            $this->data["update_condition"] = TRUE;
        }
        if ($data_sel[0]->return_zip_code !== parent::get_input_post("return_zip_code")) {
            $this->data["update_condition"] = TRUE;
        }
        if ($data_sel[0]->bank_name != parent::get_input_post("bank_name")) {
            $this->data["update_condition"] = TRUE;
        }
        if ($data_sel[0]->bank_branch_name != parent::get_input_post("bank_branch_name")) {
            $this->data["update_condition"] = TRUE;
        }
        if ($data_sel[0]->bank_acct_no != parent::get_input_post("bank_acct_no")) {
            $this->data["update_condition"] = TRUE;
        }
        if ($data_sel[0]->bank_acct_name != parent::get_input_post("bank_acct_name")) {
            $this->data["update_condition"] = TRUE;
        }


        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_logo_merchant->trans_rollback();
            }
        }
        log_message('error',  http_build_query($_SESSION));
        if ($this->data["update_condition"] == TRUE) {
            $this->save_update_detail();
        } else {
            $this->get_edit();
        }
    }

    protected function save_update_detail() {

        $foo = new stdClass();
        $foo->user_id = parent::get_merchant_user_id();
        $foo->ip_address = parent::get_ip_address();
        $foo->merchant_seq = parent::get_merchant_user_id();
        $foo->address = parent::get_input_post("address", true, FILL_VALIDATOR, "Alamat", $this->data);
        $foo->district_seq = parent::get_input_post("district_seq", true, FILL_VALIDATOR, "Kecamatan", $this->data);
        $foo->zip_code = parent::get_input_post("zip_code");
        $foo->phone_no = parent::get_input_post("phone_no", true, FILL_VALIDATOR, "No. Telp", $this->data);
        $foo->fax_no = parent::get_input_post("fax_no");
        $foo->pic1_name = parent::get_input_post("pic1_name");
        $foo->pic1_phone_no = parent::get_input_post("pic1_phone_no");
        $foo->pic2_name = parent::get_input_post("pic2_name");
        $foo->pic2_phone_no = parent::get_input_post("pic2_phone_no");
        $foo->pickup_addr_eq = parent::get_input_post("pickup_addr_eq");
        if (parent::get_input_post("pickup_addr_eq") == "on") {
            $foo->pickup_addr = parent::get_input_post("pickup_addr");
            $foo->pickup_district_seq = parent::get_input_post("pickup_district_seq");
            $foo->pickup_zip_code = parent::get_input_post("pickup_zip_code");
        } else {
            $foo->pickup_addr = parent::get_input_post("pickup_addr", true, FILL_VALIDATOR, "Alamat Pickup", $this->data);
            $foo->pickup_district_seq = parent::get_input_post("pickup_district_seq", true, FILL_VALIDATOR, "Kecamatan  Pickup", $this->data);
            $foo->pickup_zip_code = parent::clength(parent::get_input_post("pickup_zip_code"), 10, $this->data);
        }

        $foo->return_addr_eq = parent::get_input_post("return_addr_eq");
        if (parent::get_input_post("return_addr_eq") == "on") {
            $foo->return_addr = parent::get_input_post("return_addr");
            $foo->return_district_seq = parent::get_input_post("return_district_seq");
            $foo->return_zip_code = parent::get_input_post("return_zip_code");
        } else {
            $foo->return_addr = parent::get_input_post("return_addr", true, FILL_VALIDATOR, "Alamat Retur", $this->data);
            $foo->return_district_seq = parent::get_input_post("return_district_seq", true, FILL_VALIDATOR, "Kecamatan Retur", $this->data);
            $foo->return_zip_code = parent::clength(parent::get_input_post("return_zip_code"), 10, $this->data);
        }
        $foo->return_zip_code = parent::get_input_post("return_zip_code");
        $foo->bank_name = parent::get_input_post("bank_name", true, FILL_VALIDATOR, "Nama Bank", $this->data);
        $foo->bank_branch_name = parent::get_input_post("bank_branch_name", true, FILL_VALIDATOR, "Cabang", $this->data);
        $foo->bank_acct_no = parent::get_input_post("bank_acct_no", true, FILL_VALIDATOR, "No. Rekening", $this->data);
        $foo->bank_acct_name = parent::get_input_post("bank_acct_name", true, FILL_VALIDATOR, "Rekening A/N", $this->data);
        $foo->merchant_seq = parent::get_merchant_user_seq();
        $foo->status = parent::get_input_post("old_status");
        $foo->created_date = parent::get_input_post("old_created_date");
        $foo->modified_date = parent::get_input_post("old_modified_date");
        $sel_data = $this->M_info_merchant->get_data($foo);
        $this->data[DATA_HEADER][LIST_DATA][] = $foo;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                if (strlen(parent::get_input_post("zip_code")) > MAX_POSTAL_CODE_LENGTH) {
                    throw new Exception("Kode Pos " . ERROR_VALIDATION_LENGTH_MAX . MAX_POSTAL_CODE_LENGTH);
                }
                if (strlen(parent::get_input_post("phone_no")) > MAX_PHONE_LENGTH) {
                    throw new Exception("No. Telp " . ERROR_VALIDATION_LENGTH_MAX . MAX_PHONE_LENGTH);
                }
                if (strlen(parent::get_input_post("fax_no")) > MAX_LENGTH_DEFAULT) {
                    throw new Exception("No. Fax " . ERROR_VALIDATION_LENGTH_MAX . MAX_LENGTH_DEFAULT);
                }
                if (strlen(parent::get_input_post("pic1_name")) > MAX_NAME_LENGTH) {
                    throw new Exception("Nama PIC " . ERROR_VALIDATION_LENGTH_MAX . MAX_NAME_LENGTH);
                }
                if (strlen(parent::get_input_post("pic1_phone_no")) > MAX_PHONE_LENGTH) {
                    throw new Exception("Telp PIC " . ERROR_VALIDATION_LENGTH_MAX . MAX_PHONE_LENGTH);
                }
                if (strlen(parent::get_input_post("pic2_name")) > MAX_NAME_LENGTH) {
                    throw new Exception("Nama Finance " . ERROR_VALIDATION_LENGTH_MAX . MAX_NAME_LENGTH);
                }
                if (strlen(parent::get_input_post("pic2_phone_no")) > MAX_PHONE_LENGTH) {
                    throw new Exception("Telp Finance " . ERROR_VALIDATION_LENGTH_MAX . MAX_PHONE_LENGTH);
                }
                if (strlen(parent::get_input_post("pickup_zip_code")) > MAX_POSTAL_CODE_LENGTH) {
                    throw new Exception("Kode Pos Pengambilan " . ERROR_VALIDATION_LENGTH_MAX . MAX_POSTAL_CODE_LENGTH);
                }
                if (strlen(parent::get_input_post("return_zip_code")) > MAX_POSTAL_CODE_LENGTH) {
                    throw new Exception("Kode Pos Pengembalian " . ERROR_VALIDATION_LENGTH_MAX . MAX_POSTAL_CODE_LENGTH);
                }
                if (strlen(parent::get_input_post("bank_name")) > MAX_BANK_LENGTH) {
                    throw new Exception("Nama Bank " . ERROR_VALIDATION_LENGTH_MAX . MAX_BANK_LENGTH);
                }
                if (strlen(parent::get_input_post("bank_branch_name")) > MAX_BANK_BRANCE_LENGTH) {
                    throw new Exception("Cabang " . ERROR_VALIDATION_LENGTH_MAX . MAX_BANK_BRANCE_LENGTH);
                }
                if (strlen(parent::get_input_post("bank_acct_no")) > MAX_LENGTH_DEFAULT) {
                    throw new Exception("No.Rekening " . ERROR_VALIDATION_LENGTH_MAX . MAX_LENGTH_DEFAULT);
                }
                if (strlen(parent::get_input_post("bank_acct_name")) > MAX_LENGTH_DEFAULT) {
                    throw new Exception("Rekening A/N " . ERROR_VALIDATION_LENGTH_MAX . MAX_LENGTH_DEFAULT);
                }
                if ($sel_data[0]->status == NEW_STATUS_CODE) {
                    throw new Exception(ERROR_CONFIRM);
                } else {
                    $this->M_info_merchant->trans_begin();
                    $sel_data = $this->M_info_merchant->save_add_log($foo);
                    $foo->log_seq = $sel_data[0]->new_seq;
                    $this->M_info_merchant->save_add_auto($foo);
                    $this->M_info_merchant->save_update($foo);
                    $this->M_info_merchant->trans_commit();
                    $this->data[DATA_SUCCESS][SUCCESS] = true;
                }
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_info_merchant->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_info_merchant->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_info_merchant->trans_rollback();
            }
        }
    }

    protected function update_message() {
        $selected = new stdClass();
        $selected->ip_address = parent::get_ip_address();
        $selected->member_seq = parent::get_merchant_user_seq();
        $selected->user_id = parent::get_merchant_user_id();
        $selected->content = parent::get_input_post("message_text");
        $selected->prev_message_seq = parent::get_input_post("prev_message_seq");
        if (parent::get_input_post("message_text") == '' || parent::get_merchant_user_id() == '') {
            $data[DATA_ERROR][ERROR] = true;
            $data[DATA_ERROR][ERROR_MESSAGE][] = "ERROR : Data Kosong";
            die();
        }
        try {
            $this->M_logo_merchant->trans_begin();
            $this->M_logo_merchant->save_insert_message($selected);
            $this->M_logo_merchant->trans_commit();
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_logo_merchant->trans_rollback();
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_logo_merchant->trans_rollback();
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_logo_merchant->trans_rollback();
        }
        die();
    }

    public function get_message_log_list() {
        $filter = new stdClass;
        $filter->user_id = parent::get_merchant_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->member_seq = parent::get_merchant_user_seq();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->status = parent::get_input_post("status");
        try {
            $list_data = $this->M_logo_merchant->get_message_log_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );
        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                if ($data_row->merchant_seq !== NULL) {
                    $user_name = $data_row->merchant_name;
                } else {
                    $user_name = $data_row->admin_name;
                }
                $row = array("DT_RowId" => $data_row->seq,
                    "seq" => parent::cdef($data_row->seq),
                    "headers" => ($data_row->prev_message_seq != null ? $data_row->prev_message_seq : $data_row->seq),
                    "content" => (parent::cdef($data_row->content)),
                    "prev_message_seq" => $data_row->prev_message_seq,
                    "member_seq" => $data_row->merchant_seq,
                    "member_name" => $data_row->merchant_name,
                    "admin_id" => $data_row->admin_id,
                    "admin_name" => $data_row->admin_name,
                    "user_name" => $user_name,
                    "status" => $data_row->status,
                    "created_date" => parent::cdate($data_row->created_date)
                );
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
        die();
    }

    public function get_message_one_topic() {
        $filter = new stdClass;
        $filter->user_id = parent::get_merchant_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->member_seq = parent::get_merchant_user_seq();
        $filter->seq = parent::get_input_post("seq");
        try {
            $list_data = $this->M_logo_merchant->get_message_log_list_by_seq($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        $output = array('' => '',
            "aaData" => array()
        );
        if (isset($list_data)) {
            foreach ($list_data as $data_row) {
                if ($data_row->merchant_seq !== NULL) {
                    $user_name = $data_row->merchant_name;
                } else {
                    $user_name = $data_row->admin_name;
                }
                $row = array("DT_RowId" => $data_row->seq,
                    "seq" => parent::cdef($data_row->seq),
                    "headers" => ($data_row->prev_message_seq != null ? $data_row->prev_message_seq : $data_row->seq),
                    "content" => nl2br(parent::cdef($data_row->content)),
                    "prev_message_seq" => $data_row->prev_message_seq,
                    "member_seq" => $data_row->merchant_seq,
                    "member_name" => $data_row->merchant_name,
                    "admin_id" => $data_row->admin_id,
                    "admin_name" => $data_row->admin_name,
                    "user_name" => $user_name,
                    "status" => $data_row->status,
                    "created_date" => parent::cdate($data_row->created_date)
                );
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
        die();
    }

}

?>