<?php

require_once CONTROLLER_BASE_MERCHANT;

class C_message_merchant extends controller_base_merchant {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("MMST01006", "merchant/message");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model(LIVEVIEW . 'merchant/master/M_message_merchant');
        $this->load->library('pagination');
        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_ADDITIONAL, "get_ajax");
        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        if (!$this->input->post()) {
            $this->data['breadcrumb'] = parent::get_breadcrumb($this->data);
            $this->load->view(LIVEVIEW . "merchant/master/merchant_message", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view(LIVEVIEW . "merchant/master/message", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view(LIVEVIEW . "merchant/master/message", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    protected function get_ajax() {
        $tipe = parent::get_input_post("tipe");
        $idh = parent::get_input_post("idh");
        switch ($tipe) {
            case "msgsave":
                $this->update_message();
                break;
            case "onemsg":
                $this->get_message_one_topic();
                break;
        }
    }

    protected function update_message() {
        $selected = new stdClass();
        $selected->ip_address = parent::get_ip_address();
        $selected->member_seq = parent::get_merchant_user_seq();
        $selected->user_id = parent::get_merchant_user_id();
        $selected->content = parent::get_input_post("message_text");
        $selected->prev_message_seq = parent::get_input_post("prev_message_seq");
        if (parent::get_input_post("message_text") == '' || parent::get_merchant_user_id() == '') {
            $data[DATA_ERROR][ERROR] = true;
            $data[DATA_ERROR][ERROR_MESSAGE][] = "ERROR : Data Kosong";
            die();
        }
        try {
            $this->M_message_merchant->trans_begin();
            $this->M_message_merchant->save_insert_message($selected);
            $this->M_message_merchant->trans_commit();
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_message_merchant->trans_rollback();
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_message_merchant->trans_rollback();
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_message_merchant->trans_rollback();
        }
        die();
    }

    public function search() {
        $filter = new stdClass;
        $filter->user_id = parent::get_merchant_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->member_seq = parent::get_merchant_user_seq();



        $filter->order = "desc";
        $filter->column = "created_date";
        $filter->start = parent::get_input_get(START_OFFSET) == "" ? 0 : parent::get_input_get(START_OFFSET);
        $filter->length = PAGINATION_LIMIT;
        $filter->status = "";

        $list_data = $this->M_message_merchant->get_message_log_list($filter);
        $this->data['filter'] = "";
        $this->data['filter_sort'] = "&order=" . $filter->order . "&column_sort=" . $filter->column;
        $this->data['filter_status'] = "?";
        parent::set_data($this->data, $list_data);
        $this->display_page(base_url("merchant/message") . $this->data['filter_status'], $filter->start, $list_data[0][0]->total_rec, $filter->length);
    }

    public function get_message_one_topic() {
        $filter = new stdClass;
        $filter->user_id = parent::get_merchant_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->member_seq = parent::get_merchant_user_seq();
        $filter->seq = parent::get_input_post("seq");
        try {
            $list_data = $this->M_message_merchant->get_message_log_list_by_seq($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        $output = array('' => '',
            "aaData" => array()
        );
        if (isset($list_data)) {
            foreach ($list_data as $data_row) {
                if ($data_row->merchant_seq !== NULL) {
                    $user_name = $data_row->merchant_name;
                } else {
                    $user_name = $data_row->admin_name;
                }
                $row = array("DT_RowId" => $data_row->seq,
                    "seq" => parent::cdef($data_row->seq),
                    "headers" => ($data_row->prev_message_seq != null ? $data_row->prev_message_seq : $data_row->seq),
                    "content" => nl2br(parent::cdef($data_row->content)),
                    "prev_message_seq" => $data_row->prev_message_seq,
                    "member_seq" => $data_row->merchant_seq,
                    "member_name" => $data_row->merchant_name,
                    "admin_id" => $data_row->admin_id,
                    "admin_name" => $data_row->admin_name,
                    "user_name" => $user_name,
                    "status" => $data_row->status,
                    "created_date" => parent::cdate($data_row->created_date)
                );
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
        die();
    }

}

?>