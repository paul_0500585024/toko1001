
<?php

require_once CONTROLLER_BASE_MERCHANT;

//if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_verify_password_merchant extends controller_base_merchant {

    public function __construct() {
        parent::__construct("", "", false);
    }

    function index() {
        $this->load->model("merchant/master/M_verify_password_merchant");
        $this->load->model("admin/transaction/M_order_admin");
        $now = date('Y-m-d');

        $uri = $this->uri->segment(3);
        $params = new stdClass();
        $params->uri = $uri;
        $params->email_cd = MERCHANT_FORGOT_PASSWORD;

        $security = $this->M_verify_password_merchant->check_merchant_log_security($params);

        try {
            if ($security) {
                throw new Exception(ERROR_VALIDATION_FORGOT_PASSWORD);
            } else {
                $data = $this->M_verify_password_merchant->get_merchant_log($params);

                if (!isset($data)) {
                    redirect(base_url() . "error_404");
                }
                $date2 = date_create(date('Ymd'));

                $params_merchant = new stdClass();
                $params_merchant->user_id = parent::get_merchant_user_id();
                $params_merchant->ip_address = parent::get_ip_address();
                $params_merchant->email = $data[0]->recipient_email;
                $params_merchant->request_date = $data[0]->created_date;
                $params_merchant->name = $data[0]->recipient_name;

                if (date_diff(date_create($params_merchant->request_date), $date2)->d < 2) {
                    $new_password = parent::generate_random_alnum(8);
                    $params_email = new stdClass();
                    $params_email->user_id = parent::get_input_post("email");
                    $params_email->ip_address = parent::get_ip_address();
                    $params_email->code = MERCHANT_FORGOT_PASSWORD_VERIFICATION;
                    $params_email->to_email = $params_merchant->email;
                    $params_email->RECIPIENT_NAME = $params_merchant->name;
                    $params_email->PASSWORD = $new_password;
                    $params_email->EMAIL = $params_merchant->email;

                    $merchant_seq = $this->M_verify_password_merchant->get_merchant_seq($params_merchant);

                    $params_merchant->new_pass = md5(md5($new_password));
                    $params_merchant->type = FORGOT_PASSWORD_TYPE;
                    $params_merchant->code = $uri;
                    $params_merchant->old_pass = $merchant_seq[0]->new_password;
                    $params_merchant->merchant_seq = $merchant_seq[0]->seq;

                    $this->M_verify_password_merchant->trans_begin();
                    $this->M_verify_password_merchant->update_password($params_merchant);
                    $this->M_verify_password_merchant->add_merchant_log_security($params_merchant);
                    $this->M_verify_password_merchant->trans_commit();

                    $this->data[DATA_SUCCESS][SUCCESS] = true;
                    $this->data[DATA_SUCCESS][SUCCESS_MESSAGE][] = SUCCESS_VERIFY_FORGOT_PASSWORD;
                    parent::email_template($params_email);
                } else {
                    throw new Exception(ERROR_FORGOT_PASSWORD_EXPIRED);
                }
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_verify_password_merchant->trans_rollback();
        }
        $this->load->view(LIVEVIEW . "merchant/login_merchant", $this->data);
    }

}

?>