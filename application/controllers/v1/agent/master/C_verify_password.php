
<?php

require_once CONTROLLER_BASE_AGENT;

class C_verify_password extends controller_base_agent {

    public function __construct() {
        $this->data = parent::__construct("", "", false);
    }

    function index() {
        $this->load->model(LIVEVIEW . "agent/master/M_verify_password_agent");
        $this->load->model("admin/transaction/M_order_admin");
        $this->load->helper('cookie');
        $now = date('Y-m-d');

        $uri = $this->uri->segment(3);
        $params = new stdClass();
        $params->uri = $uri;
        $params->email_cd = AGENT_FORGOT_PASSWORD;
        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        $security = $this->M_verify_password_agent->check_agent_log_security($params);
        try {
            if ($security) {
                throw new Exception(ERROR_VALIDATION_FORGOT_PASSWORD);
            } else {
                $data = $this->M_verify_password_agent->get_agent_log($params);

                if (isset($data)) {
                    $date2 = date_create(date('Ymd'));

                    $params_agent = new stdClass();
                    $params_agent->user_id = parent::get_agent_user_id();
                    $params_agent->ip_address = parent::get_ip_address();
                    $params_agent->email = $data[0]->recipient_email;
                    $params_agent->request_date = $data[0]->created_date;
                    $params_agent->name = $data[0]->recipient_name;

                    if (date_diff(date_create($params_agent->request_date), $date2)->d < 2) {
                        $new_password = parent::generate_random_alnum(8);
                        $params_email = new stdClass();
                        $params_email->user_id = parent::get_input_post("email");
                        $params_email->ip_address = parent::get_ip_address();
                        $params_email->code = AGENT_FORGOT_PASSWORD_VERIFICATION;
                        $params_email->to_email = $params_agent->email;
                        $params_email->RECIPIENT_NAME = $params_agent->name;
                        $params_email->PASSWORD = $new_password;
                        $params_email->EMAIL = $params_agent->email;

                        $agent_seq = $this->M_order_admin->get_agent_seq($params_agent);

                        $params_agent->new_pass = md5(md5($new_password));
                        $params_agent->type = FORGOT_PASSWORD_TYPE;
                        $params_agent->code = $uri;
                        $params_agent->old_pass = $agent_seq[0]->password;
                        $params_agent->agent_seq = $agent_seq[0]->seq;

                        $this->M_verify_password_agent->trans_begin();
                        $this->M_verify_password_agent->update_password($params_agent);
                        $this->M_verify_password_agent->add_agent_log_security($params_agent);
                        $this->M_verify_password_agent->trans_commit();

                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_SUCCESS][SUCCESS_MESSAGE][] = SUCCESS_VERIFY_FORGOT_PASSWORD;
                        parent::email_template($params_email);
                    } else {
                        throw new Exception(ERROR_FORGOT_PASSWORD_EXPIRED);
                    }
                    $this->template->view("agent/master/verify_password_agent", $this->data);
                } else {
                    redirect(base_url() . "error_404");
                }
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_verify_password_agent->trans_rollback();
            $this->template->view("agent/master/verify_password_agent", $this->data);
        }
    }

}

?>
