<?php

require_once CONTROLLER_BASE_AGENT;

class C_verifikasi_signup_agent extends controller_base_agent {

    public function __construct() {
        parent::__construct("", "", false);
    }

    public function index() {
        $this->load->model("agent/master/M_verifikasi_signup_agent");
        $uri = $this->uri->segment(3);

        $params = new stdClass();
        $params->uri = $uri;
        $params->EmailCd = AGENT_REG_CODE;
        $pass = "ERROR";
        $get_email = $this->M_verifikasi_signup_agent->get_email_by_log($params);
        if ($get_email) {
            $pass = "OK";
        } else {
            $params->EmailCd = AGENT_REG_PROMO;
            $get_email = $this->M_verifikasi_signup_agent->get_email_by_log($params);
            if ($get_email)
                $pass = "OK";
        }
        if ($pass == "OK") {
            $email = new stdClass();
            $email->email = $get_email[0]->recipient_email;
            $get_agent = $this->M_verifikasi_signup_agent->get_agent_by_email($email);
            $agent_seq = $get_agent[0]->seq;
            $agent_name = $get_agent[0]->name;
            $agent_email = $get_agent[0]->email;
            if ($get_agent[0]->status == NEW_STATUS_CODE) {
                $this->M_verifikasi_signup_agent->save_update_agent($email);
                $params->user_id = '';
                $params->ip_address = parent::get_ip_address();
                $params->node_cd = NODE_REGISTRATION_AGENT;
                $params->agent_seq = $agent_seq;

                $voucher = parent::generate_voucher($params);

                $get_status = SUCCESS_STATUS;
                $get_mail = new stdClass();
                $get_mail->user_id = '';
                $get_mail->ip_address = parent::get_ip_address();
                $get_mail->code = AGENT_REG_SUCCESS_CODE_VOUCHER;
                $get_mail->RECIPIENT_NAME = $agent_name;
                $get_mail->LINK = base_url() . lOGIN_AGENT;
                $get_mail->to_email = $agent_email;
                $get_mail->VOUCHER = $voucher;

                parent::email_template($get_mail);
            } else {
                $get_status = FAILED_STATUS;
            }
        } else {
            redirect(base_url() . "error_404");
        }
        $value[VALUE] = $get_status;
        $this->load->view(LIVEVIEW . "agent/master/verifikasi_regsiter_agent", $value);
    }

}

?>