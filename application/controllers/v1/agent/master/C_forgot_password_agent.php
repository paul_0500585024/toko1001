<?php

require_once CONTROLLER_BASE_AGENT;

//if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_forgot_password_agent extends controller_base_agent {

//    private $temp_data;

    public function __construct() {
        $this->data = parent::__construct("", "", false);
        $this->initialize();
    }

    private function initialize() {
        if (isset($_SESSION[SESSION_TEMP_DATA])) {
            $this->data = $_SESSION[SESSION_TEMP_DATA];
            unset($_SESSION[SESSION_TEMP_DATA]);
        }

        $this->load->model(LIVEVIEW . "agent/master/M_forgot_password_agent");
        $this->load->model(LIVEVIEW . "agent/M_login_agent");
        $this->load->helper('cookie');
    }

    private function create_captcha() {
        $this->load->helper('captcha');        
        $options = array('img_path' => TEMP_CAPTCHA,
            'img_url' => CDN_IMAGE . TEMP_CAPTCHA,
            'img_width' => '250',
            'img_height' => '50',
            'expiration' => 7200,
            'word_length' => '5',
            'font_path' => './system/fonts/ARIALNB.TTF',
            'pool' => CAPTCHA_POOL);

        $cap = create_captcha($options);
        $image = $cap[IMAGE];
        $word = $cap[WORD];
        $this->session->set_userdata(CAPTCHA, $word);
        $image_captcha = array($image, $word);
        return $image_captcha;
    }

    public function index() {

        if (isset($_SESSION[SESSION_AGENT_UID]) && isset($_SESSION[SESSION_AGENT_UNAME]) && isset($_SESSION[SESSION_AGENT_USER_GROUP]) && isset($_SESSION[SESSION_AGENT_EMAIL]) && isset($_SESSION[SESSION_AGENT_SEQ]) && isset($_SESSION[SESSION_AGENT_CSRF_TOKEN]) && isset($_SESSION[SESSION_AGENT_LAST_LOGIN]) && isset($_SESSION[SESSION_TYPE_LOGIN]) && isset($_SESSION[SESSION_IP_ADDR])) {
            redirect(base_url());
        }
        $this->data['tree_category'] = $this->get_tree_category(true);

        if ($this->input->post('btnSave') != "") {
            try {
                $params = new stdClass();
                $params->user_id = parent::get_input_post("email");
                $params->ip_address = parent::get_ip_address();
                $gencode = $gencode = parent::generate_random_text(3, true) . date('Y') . parent::generate_random_text(4, true) . date('m') . parent::generate_random_text(5, true) . date('d');
                $user_captcha = parent::get_input_post("security_code");
                $captcha = $this->session->userdata(CAPTCHA);
                $result = $this->M_login_agent->get_list($params);

                if (!isset($result)) {
                    throw new exception(ERROR_EMAIL_NOT_EXISTS);
                }

                if ($captcha != $user_captcha) {
                    throw new exception(ERROR_CAPTCHA_CHECK);
                }

                $params->code = AGENT_FORGOT_PASSWORD;
                $params->email_code = $gencode;
                $params->RECIPIENT_NAME = $result[0]->user_name;
                $params->LINK = base_url() . VERIFICATION_AGENT_FORGOT_PASSWORD . $gencode;
                $params->to_email = $params->user_id;
                parent::email_template($params);

                $this->data[DATA_SUCCESS][SUCCESS] = true;
                $this->data[DATA_SUCCESS][SUCCESS_MESSAGE][] = SUCCESS_FORGOT_PASSWORD;
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
            }

            $admin_info[SESSION_TEMP_DATA] = $this->data;
            $this->session->set_userdata($admin_info);
            redirect(base_url("agent/forgot_password"));
        }

        $image = $this->create_captcha();
        $this->data[IMAGE] = $image[0];

        $this->template->view("agent/master/forgot_password_agent", $this->data);
    }

}

?>
