<?php



require_once CONTROLLER_BASE_AGENT;



class C_login_agent extends controller_base_agent {

    private $temp_data;
    private $message_facebook;
    private $url;

    public function __construct() {
        parent::__construct("", "", false);
        $this->initialize();
    }

    private function initialize() {
        $err = [ERROR => false, ERROR_MESSAGE => ""];
        $this->temp_data[DATA_ERROR] = $err;
        $this->load->model(LIVEVIEW . 'agent/M_login_agent');
        $this->load->helper('cookie');
    }

    public function index() {
        /* check if member has session */
        if (isset($_SESSION[SESSION_MEMBER_UID]) && isset($_SESSION[SESSION_MEMBER_UNAME]) && isset($_SESSION[SESSION_MEMBER_USER_GROUP]) && isset($_SESSION[SESSION_MEMBER_EMAIL]) && isset($_SESSION[SESSION_MEMBER_SEQ]) && isset($_SESSION[SESSION_MEMBER_CSRF_TOKEN]) && isset($_SESSION[SESSION_MEMBER_LAST_LOGIN]) && isset($_SESSION[SESSION_TYPE_LOGIN]) && isset($_SESSION[SESSION_IP_ADDR])) {
            redirect(base_url());
        }

        /* check if agent has session */
        if (isset($_SESSION[SESSION_AGENT_UID]) && isset($_SESSION[SESSION_AGENT_UNAME]) && isset($_SESSION[SESSION_AGENT_USER_GROUP]) && isset($_SESSION[SESSION_AGENT_EMAIL]) && isset($_SESSION[SESSION_AGENT_SEQ]) && isset($_SESSION[SESSION_AGENT_CSRF_TOKEN]) && isset($_SESSION[SESSION_AGENT_LAST_LOGIN]) && isset($_SESSION[SESSION_TYPE_LOGIN]) && isset($_SESSION[SESSION_IP_ADDR])) {
            redirect(base_url());
        }
        $this->url = parent::get_input_get("url");
        if (isset($_SESSION[SESSION_TEMP_DATA])) {
            $this->temp_data = $_SESSION[SESSION_TEMP_DATA];
            unset($_SESSION[SESSION_TEMP_DATA]);
        }
        $this->temp_data[DATA_AUTH][FORM_AUTH][FORM_URL] = "?url=" . $this->url;
        $this->load->view(LIVEVIEW . "agent/login_agent", $this->temp_data);
    }

    public function sign_out() {
        $continue_url = ($this->input->get(CONTINUE_URL) != FALSE) ? $this->input->get(CONTINUE_URL) : base_url();
        $continue_url_redirect_to_base_url = array(
            base_url() . 'agent/payment',
            base_url() . 'agent/profile/payment_retry',
            base_url() . 'agent/wishlist',
        );

        unset($_SESSION[SESSION_AGENT_UID]);
        unset($_SESSION[SESSION_AGENT_UNAME]);
        unset($_SESSION[SESSION_AGENT_USER_GROUP]);
        unset($_SESSION[SESSION_AGENT_EMAIL]);
        unset($_SESSION[SESSION_AGENT_SEQ]);
        unset($_SESSION[SESSION_AGENT_CSRF_TOKEN]);
        unset($_SESSION[SESSION_AGENT_LAST_LOGIN]);
        unset($_SESSION[SESSION_TYPE_LOGIN]);
        unset($_SESSION[SESSION_PRODUCT_INFO]);
        unset($_SESSION[SESSION_PAYMENT_INFO]);
        unset($_SESSION[SESSION_PERSONAL_INFO]);
        unset($_SESSION[SESSION_IP_ADDR]);
        unset($_SESSION[SESSION_PARTNER_SEQ]);
        unset($_SESSION[SESSION_PARTNER_NAME]);

        //redirect to base_url
        foreach ($continue_url_redirect_to_base_url as $each_continue_url_redirect_to_base_url) {
            if ($this->startsWith($continue_url, $each_continue_url_redirect_to_base_url)) {
                redirect(base_url('agent'));
            }
        }
        redirect($continue_url);
    }

    public function sign_in() {
        $params = new stdClass();
        $params->type_login = $this->input->post("type");
        $params->user_id = $this->input->post("email");
        $params->password = $this->input->post("password");
        $current_url = ($this->input->post("current_url") != FALSE) ? $this->input->post("current_url") : base_url();

        $this->temp_data[DATA_SELECTED][LIST_DATA] = $params;

        if (filter_var($params->user_id, FILTER_VALIDATE_EMAIL) === false) {
            $this->temp_data[DATA_ERROR][ERROR] = true;
            $this->temp_data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_VALIDATION_FILL_EMAIL_FORMAT;
        }

        $params->password = md5(md5($params->password));
        $params->ip_address = $_SERVER['REMOTE_ADDR'];

        if ($this->temp_data[DATA_ERROR][ERROR] === false) {
            try {
                $list_data = $this->M_login_agent->get_list($params);

                if (!isset($list_data[0])) {
                    $this->temp_data [DATA_ERROR][ERROR] = true;
                    $this->temp_data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_INVALID_PASSWORD_AND_EMAIL;

                    $agent_info[SESSION_TEMP_DATA] = $this->temp_data;
                    $this->session->set_userdata($agent_info);
                    redirect(base_url("agent/login") . "?url=" . $this->url);
                }
                parent::set_list_data($this->temp_data, $list_data);
            } catch (Exception $ex) {
                parent::set_error($this->temp_data, $ex);
            }
            if ($list_data[0]->password == $params->password) {
                if (isset($list_data[0])) {
                    $agent_info[SESSION_AGENT_UID] = $list_data[0]->seq;
                    $agent_info[SESSION_AGENT_UNAME] = $list_data[0]->user_name;
                    $agent_info[SESSION_AGENT_USER_GROUP] = AGENT;
                    $agent_info[SESSION_AGENT_EMAIL] = $list_data[0]->email;
                    $agent_info[SESSION_AGENT_SEQ] = $list_data[0]->seq;
                    $agent_info[SESSION_PARTNER_SEQ] = $list_data[0]->partner_seq;
                    $agent_info[SESSION_PARTNER_NAME] = $list_data[0]->partner_name;
                    $agent_info[SESSION_TYPE_LOGIN] = $params->type_login;
                    $agent_info[SESSION_AGENT_LAST_LOGIN] = $list_data[0]->last_login;
                    $agent_info[SESSION_AGENT_CSRF_TOKEN] = base64_encode(openssl_random_pseudo_bytes(32));
                    $agent_info[SESSION_IP_ADDR] = $_SERVER["REMOTE_ADDR"];

                    $cookie = array(
                        'name' => VIEW_MODE,
                        'value' => AGENT,
                        'expire' => (3600 * 1000 * 24),
//                        'domain' => SMTP_HOST,
                        'path' => '/',
//                        'secure' => SECURECONNECTION
                    );
                    $this->input->set_cookie($cookie);
                    $this->session->set_userdata($agent_info);
                    if ($list_data[0]->password == "") {
                        //redirect(base_url("agent/profile/change_old_password_agent"));
                    } else {
                        $this->url = parent::get_input_get("url");
                        if ($this->url <> "") {
                            redirect(base_url($this->url));
                        } else {
                            redirect($current_url);
                        }
                    }
                }
            } else {
                $this->temp_data[DATA_ERROR][ERROR] = true;
                $this->temp_data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_INVALID_PASSWORD_AND_EMAIL;
                $agent_info[SESSION_TEMP_DATA] = $this->temp_data;
                $this->session->set_userdata($agent_info);
                redirect(base_url("agent/login") . "?url=" . $this->url);
            }
        }

        $agent_info[SESSION_TEMP_DATA] = $this->temp_data;
        $this->session->set_userdata($agent_info);
        redirect(base_url("agent/login") . "?url=" . $this->url);
    }

}

?>