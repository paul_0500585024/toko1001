<?php

require_once CONTROLLER_BASE_AGENT;

class C_change_password_agent extends controller_base_agent {

    private $temp_data;
    private $data;

    public function __construct() {

        $this->data = parent::__construct("MST003", "agent/profile/change_password_agent");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('agent/M_login_agent');
        $this->load->model('agent/profile/M_change_password_agent');
        $this->load->model('agent/transaction/M_trans_log_agent');
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {

        if (!$this->input->post()) {
            $this->load->view(LIVEVIEW . "agent/profile/change_password_agent", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view(LIVEVIEW . "agent/profile/change_password_agent", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $agent_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($agent_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    protected function save_update() {

        $params = new stdClass();
        $params->user_name = parent::get_agent_email();
        $params->agent_seq = parent::get_agent_user_seq();
        $params->ip_address = parent::get_ip_address();
        $params->old_password = parent::get_input_post("old_password", true, FILL_VALIDATOR, "Password Lama", $this->data);
        $params->new_password = parent::get_input_post("new_password", true, FILL_VALIDATOR, "Password Baru", $this->data);

        $params->encrypt_old_password_1 = md5($params->old_password);
        $params->encrypt_old_password_2 = md5(md5($params->old_password));
        $params->encrypt_new_password = md5(md5($params->new_password));

        try {
            $list_data = $this->M_login_agent->get_password($params);
            parent::set_list_data($this->temp_data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->temp_data, $ex);
        }
        if ($list_data[0]->new_password == '') {
            if ($list_data[0]->old_password == $params->encrypt_old_password_1) {
                if (isset($list_data[0])) {
                    try {
                        $list_data = new stdClass();
                        $list_data->user_id = parent::get_agent_email();
                        $list_data->agent_seq = parent::get_agent_user_seq();
                        $list_data->ip_address = parent::get_ip_address();
                        $list_data->old_password = $params->encrypt_old_password_1;
                        $list_data->new_password = $params->encrypt_new_password;
                        $this->M_change_password_agent->trans_begin();
                        $this->M_change_password_agent->save_update($list_data);
                        $this->M_trans_log_agent->save_agent_change_password_log($list_data);
                        $this->M_change_password_agent->trans_commit();
                    } catch (BusisnessException $ex) {
                        parent::set_error($this->data, $ex);
                        $this->M_change_password_agent->trans_rollback();
                    } catch (TechnicalException $ex) {
                        parent::set_error($this->data, $ex);
                        $this->M_change_password_agent->trans_rollback();
                    } catch (TechnicalException $ex) {
                        parent::set_error($this->data, $ex);
                        $this->M_trans_log->trans_rollback();
                    } catch (Exception $ex) {
                        parent::set_error($this->data, $ex);
                        $this->M_change_password_agent->trans_rollback();
                    }
                }
            } else {
                $this->data[DATA_ERROR][ERROR] = true;
                $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_INVALID_OLD_PASSWORD;
            }
        } else if ($list_data[0]->new_password != '') {
            if ($list_data[0]->new_password == $params->encrypt_old_password_2) {
                if (isset($list_data[0])) {
                    try {
                        $list_data = new stdClass();
                        $list_data->user_id = parent::get_agent_email();
                        $list_data->agent_seq = parent::get_agent_user_seq();
                        $list_data->ip_address = parent::get_ip_address();
                        $list_data->old_password = $params->encrypt_old_password_2;
                        $list_data->new_password = $params->encrypt_new_password;
                        $this->M_change_password_agent->trans_begin();
                        $this->M_change_password_agent->save_update($list_data);
                        $this->M_trans_log_agent->save_agent_change_password_log($list_data);
                        $this->M_change_password_agent->trans_commit();
                    } catch (BusisnessException $ex) {
                        parent::set_error($this->data, $ex);
                        $this->M_change_password_agent->trans_rollback();
                    } catch (TechnicalException $ex) {
                        parent::set_error($this->data, $ex);
                        $this->M_change_password_agent->trans_rollback();
                    } catch (TechnicalException $ex) {
                        parent::set_error($this->data, $ex);
                        $this->M_trans_log->trans_rollback();
                    } catch (Exception $ex) {
                        parent::set_error($this->data, $ex);
                        $this->M_change_password_agent->trans_rollback();
                    }
                }
            } else {
                $this->data[DATA_ERROR][ERROR] = true;
                $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_INVALID_OLD_PASSWORD;
            }
        }
    }

}

?>
