<?php

require_once CONTROLLER_BASE_AGENT;

class C_agent extends controller_base_agent {

    private $data;

    public function __construct() {
        $segments = explode('/', trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/'));
        if (isset($segments[2])) {
            if ($segments[2] == 'verifikasi') {
                $this->data = parent::__construct("CON002M", ROUTE_AGENT . "verifikasi", true, $segments[3]);
            } else {
                $this->data = parent::__construct("CON002M", "agent");
            }
        } else {
            $this->data = parent::__construct("CON002M", "agent");
        }

        $this->load->helper('url');
        $this->load->helper('cookie');
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('component/M_dropdown_list');
        $this->load->model(LIVEVIEW . ROUTE_AGENT . 'profile/M_profile_agent');
        $this->load->model(LIVEVIEW . ROUTE_AGENT . 'profile/M_trans_log_agent');
        $this->load->model(LIVEVIEW . ROUTE_AGENT . 'master/M_verify_password_agent');
        $this->load->model(LIVEVIEW . ROUTE_AGENT . 'M_order_agent');
        $this->load->model('component/M_radio_button');
    }

    protected function get_trx_no() {
        return parent::generate_random_alnum(3, true) . date("Y") . parent::generate_random_text(4, true) . date("m") . parent::generate_random_text(3, true) . date("d");
    }

    public function index() {

        $search = parent::get_input_post("btnSearch");
        if ($search == "true") {
            $this->get_deposit_agent();
        }

        $search2 = parent::get_input_post("btnSearch2");
        if ($search2 == "true") {
            $this->get_order_list_agent();
        }

        $search10 = parent::get_input_post("btnSearch10");
        if ($search10 == "true") {
            $this->get_review_agent();
        }

        $search16 = parent::get_input_post("btnSearch16");
        if ($search16 == "true") {
            $this->get_redeem_agent();
        }

        $search20 = parent::get_input_post("btnSearch20");
        if ($search20 == "true") {
            $this->get_agent_customer();
        }

        $profiler = parent::get_input_post("profiler");
        if ($profiler == "1001") {
            $this->upload_profile_pic();
        }
        $this->get_data_agent();

        $uri = $this->uri->segment(3);

        $DD = substr($uri, 2, 2);
        $MM = substr($uri, 6, 2);
        $YY = substr($uri, 10, 2);
        $trx_code = substr($uri, 12, 8);
        $data = date('Ymd', strtotime($YY . '-' . $MM . '-' . $DD));
        $date1 = date_create($data);
        $date2 = date_create(date('Ymd'));

        $params = new stdClass();
        $params->trx_no = $trx_code;
        $params->uri = $uri;
        $params->email_cd = AGENT_WITHDRAW_CODE;
        $check_log = $this->M_verify_password_agent->get_agent_log($params);
        if ($this->uri->segment(2) == 'verifikasi') {
            try {
                if (isset($check_log)) {
                    if (date_diff($date1, $date2)->d < 1) {
                        $get_account = $this->M_profile_agent->get_data_account($params);
                        if (isset($get_account)) {
                            if ($get_account[0]->status == WEB_STATUS_CODE) {
                                $this->M_profile_agent->save_update_account($params);
                                $this->data[DATA_SUCCESS][SUCCESS] = true;
                                $this->data[DATA_SUCCESS][SUCCESS_MESSAGE][] = SUCCESS_DEPOSIT_IS_FOUND;
                            } else {
                                $this->data[DATA_ERROR][ERROR] = TRUE;
                                throw new Exception(ERROR_DEPOSIT_EXIST);
                            }
                        } else {
                            redirect(base_url() . "error_404");
                        }
                    } else {
                        $this->data[DATA_ERROR][ERROR] = TRUE;
                        throw new Exception(ERROR_VERIFICATION_DATE_DEPOSIT);
                    }
                } else {
                    redirect(base_url() . "error_404");
                }
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_agent->trans_rollback();
            }
        }
        $this->data['tree_category'] = $this->get_tree_category(true);
        $this->data[EXPEDITION_NAME] = $this->M_dropdown_list->get_dropdown_expedition();
        $this->data[DATA_BANK] = $this->M_radio_button->get_bank_list();
        $params->user_id = parent::get_agent_user_seq();
        $params->ip_address = parent::get_ip_address();
        $params->agent_seq = parent::get_agent_user_seq();
        $this->data[PAYMENT_INFO] = $this->M_profile_agent->get_redeem_info($params);

        $this->data['title'] = 'Toko1001 - Panel Akun';
        $this->template->view(ROUTE_AGENT . "profile/agent_page", $this->data, 'agent');
    }

    protected function upload_profile_pic() {
        $params = new stdClass();
        $params->user_id = parent::get_agent_user_seq();
        $params->ip_address = parent::get_ip_address();
        $params->agent_seq = parent::get_agent_user_seq();
        $list_data = $this->M_profile_agent->get_data_agent($params);
        $file_1 = str_replace(CDN_IMAGE, "", AGENT_UPLOAD_IMAGE);
        $logo_img = parent::upload_file("profile-image-upload", $file_1, $params->agent_seq . strtotime("now"), IMAGE_TYPE, $this->data, IMAGE_DIMENSION_TRANSFER);
        $params->profile_img = $logo_img->file_name;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_profile_agent->trans_begin();
                $this->M_profile_agent->save_update_profile($params);
                $this->M_profile_agent->trans_commit();
                if ($list_data[0][0]->profile_img != "") {
                    $file = str_replace(CDN_IMAGE, "", ORDER_UPLOAD_IMAGE);
                    if (file_exists($file . $list_data[0][0]->profile_img))
                        unlink($file . $list_data[0][0]->profile_img);
                }
                redirect(base_url("agent"));
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_agent->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_agent->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_agent->trans_rollback();
            }
        }
    }

    protected function get_data_agent() {
        $params = new stdClass();
        $params->ip_address = parent::get_ip_address();
        $params->agent_seq = parent::get_agent_user_seq();

        try {
            $list_data = $this->M_profile_agent->get_data_agent($params);
            if (isset($list_data)) {
                if ($list_data[0][0]->profile_img != '') {
                    $this->data['img_src'] = AGENT_UPLOAD_IMAGE . '/' . $list_data[0][0]->profile_img;
                } else {
                    $this->data['img_src'] = IMG_BLANK_100;
                }
                parent::set_data($this->data, $list_data);
                $params->user_id = parent::get_agent_user_id();
                $sel_data1 = $this->M_profile_agent->get_agent_review($params);
                $this->data[DATA_SELECTED][LIST_DATA]['sel'] = $sel_data1;
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    public function get_deposit_agent() {

        $filter = new stdClass;

        $filter->user_id = parent::get_agent_email();
        $filter->ip_address = parent::get_ip_address();
        $filter->agent_seq = parent::get_agent_user_seq();

        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");

        try {
            $list_data = $this->M_profile_agent->get_account_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );

        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                if ($data_row->deposit_Keluar > 0) {
                    $nilai_transaksi = "Deposit " . parent::cnum($data_row->deposit_Keluar);
                } else if ($data_row->deposit_Keluar == 0 && $data_row->mutation_type == NEW_STATUS_CODE && $data_row->deposit_Masuk == 0) {
                    if ($data_row->pg_method_seq == PAYMENT_SEQ_CREDIT_CARD) {
                        $nilai_transaksi = "CC " . parent::cnum($data_row->non_deposit_trx_amt);
                    } else if ($data_row->pg_method_seq == '2') {
                        $nilai_transaksi = "CCc " . parent::cnum($data_row->non_deposit_trx_amt);
                    }
                } else {
                    $nilai_transaksi = "Deposit " . parent::cnum($data_row->deposit_Masuk);
                }
                $row = array("DT_RowId" => $data_row->agent_seq,
                    "agent_seq" => $data_row->agent_seq,
                    "mutation_type" => $data_row->mutation_type,
                    "pg_method_seq" => $data_row->pg_method_seq,
                    "trx_type" => parent::cstdes($data_row->trx_type, DEPOSIT_ACCOUNT_TYPE),
                    "trx_no" => $data_row->trx_no,
                    "trx_date" => parent::cdate($data_row->trx_date),
                    "deposit_trx_amt" => parent::cnum($data_row->deposit_trx_amt),
                    "non_deposit_trx_amt" => parent::cnum($data_row->non_deposit_trx_amt),
                    "bank_name" => $data_row->bank_name,
                    "bank_branch_name" => $data_row->bank_branch_name,
                    "bank_acct_no" => $data_row->bank_acct_no,
                    "bank_acct_name" => $data_row->bank_acct_name,
                    "refund_date" => $data_row->refund_date,
                    "status" => parent::cstdes($data_row->status, STATUS_WITHDRAW),
                    "deposit_Keluar" => parent::cnum($data_row->deposit_Keluar),
                    "deposit_Masuk" => parent::cnum($data_row->deposit_Masuk),
                    "saldo" => parent::cnum($data_row->saldo),
                    "nilai_transaksi" => $nilai_transaksi
                );

                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
        die();
    }

    public function get_order_list_agent() {
        $filter = new stdClass;
        $filter->user_id = parent::get_agent_user_seq();
        $filter->ip_address = parent::get_ip_address();
        $filter->agent_seq = parent::get_agent_user_seq();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->order_no = parent::get_input_post("order_no");
        $filter->identity_no = parent::get_input_post("identity_no");
        $filter->order_date_from = parent::get_input_post("order_date_s");
        $filter->order_date_to = parent::get_input_post("order_date_e");
        $filter->date_search = "0";
        if ($filter->order_date_from != '' && $filter->order_date_to != '')
            $filter->date_search = "1";
        $filter->payment_status = parent::get_input_post("payment_status");

        try {
            $list_data = $this->M_profile_agent->get_order_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );

        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                if ($data_row->payment_status == PAYMENT_FAILED_STATUS_CODE) {
                    $order_no = "<a href='" . base_url() . "member/profile/payment_retry/" . $data_row->order_no . "' target=" . BLANK_TARGET . ">$data_row->order_no</a>";
                } else {
                    $order_no = "<a href='" . base_url() . "member/payment/" . $data_row->order_no . "' target=" . BLANK_TARGET . ">$data_row->order_no</a>";
                }
                $row = array("DT_RowId" => $data_row->seq,
                    "agent_name" => parent::cdef($data_row->agent_name),
                    "order_no" => $order_no,
                    "order_date" => parent::cdate($data_row->order_date),
                    "total_order" => 'Rp. ' . parent::cnum($data_row->total_order),
                    "payment_method" => ($data_row->payment_method),
                    "payment_status" => parent::cstdes($data_row->payment_status, STATUS_PAYMENT),
                    "paid_date" => parent::cdate($data_row->paid_date),
//                    "detail" => "<a href='agent/profile/order_list_detail/" . $data_row->order_no . "'>Lihat</a>",
                );

                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
        die();
    }

    public function get_redeem_agent() {
        $filter = new stdClass;
        $filter->user_id = parent::get_agent_user_seq();
        $filter->ip_address = parent::get_ip_address();
        $filter->agent_seq = parent::get_agent_user_seq();
        $filter->start = parent::get_input_post("start");
        $filter->length = DEFAULT_RECORD_PER_PAGE;
        $filter->payment_status = parent::get_input_post("payment_status");

        try {
            $list_data = $this->M_profile_agent->get_redeem_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );
        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $order_no = "<a href='" . base_url() . "member/payment/" . $data_row->order_no . "' target=" . BLANK_TARGET . ">$data_row->order_no</a>";
                $row = array("DT_RowId" => $data_row->seq,
                    "order_no" => $order_no,
                    "order_date" => parent::cdate($data_row->order_date),
                    "total_order" => 'Rp. ' . parent::cnum($data_row->sell_price),
                    "receiver_name" => $data_row->receiver_name,
                    "receiver_address" => $data_row->receiver_address,
                    "receiver_district_seq" => $data_row->receiver_district_seq,
                    "receiver_zip_code" => $data_row->receiver_zip_code,
                    "receiver_phone_no" => $data_row->receiver_phone_no,
                    "order_status" => parent::cstdes($data_row->order_status, STATUS_ORDER),
                    "received_date" => $data_row->received_date,
                    "qty" => $data_row->qty,
                    "sell_price" => $data_row->sell_price,
                    "weight_kg" => $data_row->weight_kg,
                    "ship_price_real" => $data_row->ship_price_real,
                    "commission_fee_percent" => $data_row->commission_fee_percent,
                    "district_name" => $data_row->district_name,
                    "city_name" => $data_row->city_name,
                    "province_name" => $data_row->province_name,
                    "display_name" => $data_row->display_name,
                    "merchant_seq" => $data_row->merchant_seq,
                    "img" => $data_row->img,
                    "value" => ($data_row->value),
                    "variant_name" => $data_row->variant_name,
                    "total_prd" => ($data_row->qty * $data_row->sell_price),
//                    "nilai_komisi" => (($data_row->commission_fee_percent * $data_row->qty) * ($data_row->sell_price / 100)),
                    "nilai_komisi" => $data_row->komisiAgent,
                );

                $output['aaData'][] = $row;
            }
            echo json_encode($output);
        } else {
            echo json_encode(array('empty'));
        }
        die();
    }

    public function save_update() {
        $tab = parent::get_input_post("tab");
        switch ($tab) {
            case TAB_BASIC:
                $this->update_basic();
                break;
            case TAB_ADDRESS:
                $this->update_address();
                break;
            case TAB_DEPOSIT:
                $deposit_action = $this->input->post('deposit_action');
                if ($deposit_action == 'withdraw') {
                    $this->update_deposit();
                } elseif ($deposit_action == 'topup') {
                    $this->update_deposit_topup();
                }
                break;
            case TAB_PAYMENT:
                $this->update_payment();
                break;
            case TAB_CONFIG:
                $this->update_config();
                break;
            case TAB_ORDER:
                $this->order();
                break;
            case TAB_REVIEW:
                $this->insert_review();
                break;
            case TAB_MESSAGE:
                $this->update_message();
                break;
        }

        $agent_info[SESSION_DATA] = $this->data;
        $this->session->set_userdata($agent_info);
        redirect(base_url('agent'));
    }

    protected function insert_review() {

        $selected = new stdClass();
        $selected->user_id = parent::get_agent_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->orseq = $this->input->post("orseq");
        $selected->pvseq = $this->input->post("pvseq");
        $selected->review = $this->input->post("review_text");
        $selected->star = $this->input->post("star");
        $newcoment = '';

        $this->data[DATA_SELECTED][LIST_DATA][] = $selected;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_profile_agent->trans_begin();
                for ($i = 0; $i <= count($selected->orseq); $i++) {
                    if (trim($selected->review[$i]) != '' && $selected->star[$i] != 0) {
                        $newcoment = 'OK';
                        $selected->product_variant_seq = $selected->pvseq[$i];
                        $selected->order_seq = $selected->orseq[$i];
                        $selected->rate = $selected->star[$i];
                        $selected->review_text = htmlentities($selected->review[$i]);
                        $this->M_profile_agent->save_review($selected);
                        $this->send_notif($selected->order_seq, '', parent::get_agent_user_id());
                    }
                }
                $this->M_profile_agent->trans_commit();
                if ($newcoment != '') {
                    $this->data[DATA_SUCCESS][SUCCESS] = true;
                } else {
                    throw new Exception("Ulasan dan rate " . " " . ERROR_VALIDATION_MUST_FILL);
                }
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_agent->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_agent->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_agent->trans_rollback();
            }
        }
    }

    protected function update_basic() {
        $selected = new stdClass();
        $selected->user_id = parent::get_agent_user_seq();
        $selected->ip_address = parent::get_ip_address();
        $selected->agent_seq = parent::get_agent_user_seq();
        $selected->agent_name = parent::get_input_post("agent_name");
        $selected->gender = parent::get_input_post("gender", TRUE, GENDER_VALIDATOR, '', $this->data);
        $selected->birthday = parent::get_input_post("birthday");
        $selected->mobile_phone = parent::get_input_post("mobile_phone");

        $this->data[DATA_SELECTED][LIST_DATA][] = $selected;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                if (strlen($selected->agent_name) > MAX_NAME_LENGTH) {
                    throw new Exception("Nama agent" . " " . ERROR_VALIDATION_LENGTH_MAX . MAX_NAME_LENGTH);
                }

                $this->M_profile_agent->trans_begin();
                $this->M_profile_agent->save_update_contact($selected);
                $this->M_profile_agent->trans_commit();
                $this->data[DATA_SUCCESS][SUCCESS] = true;
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_agent->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_agent->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_agent->trans_rollback();
            }
        }
    }

    protected function update_address() {

        $selected = new stdClass();
        $selected->user_id = parent::get_agent_email();
        $selected->ip_address = parent::get_ip_address();
        $selected->agent_seq = parent::get_agent_user_seq();
        $selected->address = parent::get_input_post("address");
        $selected->district_seq = parent::get_input_post("district_seq");
        $selected->zip_code = parent::get_input_post("zip_code");
        $selected->pic_name = parent::get_input_post("pic_name");
        $selected->phone_no = parent::get_input_post("phone_no");

        $this->data[DATA_SELECTED][LIST_DATA][] = $selected;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_profile_agent->trans_begin();
                $this->M_profile_agent->save_update_address($selected);
                $this->M_profile_agent->trans_commit();
                $this->data[DATA_SUCCESS][SUCCESS] = true;
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_agent->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_agent->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_agent->trans_rollback();
            }
        }
    }

    protected function update_payment() {

        $selected = new stdClass();
        $selected->user_id = parent::get_agent_user_seq();
        $selected->ip_address = parent::get_ip_address();
        $selected->agent_seq = parent::get_agent_user_seq();
        $selected->bank_name = parent::get_input_post("bank_name");
        $selected->bank_branch_name = parent::get_input_post("bank_branch_name");
        $selected->bank_acct_no = parent::get_input_post("bank_acct_no");
        $selected->bank_acct_name = parent::get_input_post("bank_acct_name");

        $this->data[DATA_SELECTED][LIST_DATA][] = $selected;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                if (strlen($selected->bank_name) > MAX_BANK_LENGTH) {
                    throw new Exception("Bank" . " " . ERROR_VALIDATION_LENGTH_MAX . MAX_BANK_LENGTH);
                }
                if (strlen($selected->bank_branch_name) > MAX_BANK_BRANCE_LENGTH) {
                    throw new Exception("Cabang" . " " . ERROR_VALIDATION_LENGTH_MAX . MAX_BANK_BRANCE_LENGTH);
                }
                if (strlen($selected->bank_acct_name) > MAX_NAME_LENGTH) {
                    throw new Exception("Rekening A/N" . " " . ERROR_VALIDATION_LENGTH_MAX . MAX_NAME_LENGTH);
                }
                $this->M_profile_agent->trans_begin();
                $this->M_profile_agent->save_update_payment($selected);
                $this->M_profile_agent->trans_commit();
                $this->data[DATA_SUCCESS][SUCCESS] = true;
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_agent->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_agent->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_agent->trans_rollback();
            }
        }
    }

    protected function update_deposit() {
        $selected = new stdClass();
        $selected->user_id = parent::get_agent_email();
        $selected->ip_address = parent::get_ip_address();
        $selected->agent_seq = parent::get_agent_user_seq();
        $selected->mutation_type = DEBET_MUTATION_TYPE;
        $selected->trx_type = WITHDRAW_ACCOUNT_TYPE;
        $selected->pg_method_seq = PAYMENT_SEQ_DEPOSIT;
        $selected->agent_name = parent::get_agent_name();
        $selected->conf_topup_date = '';
        $selected->conf_topup_type = 'N';
        $selected->conf_topup_file = '';
        $selected->conf_topup_bank_seq = '';
        $selected->status = WEB_STATUS_CODE;

        $refund_code = REFUND_STATUS_CODE . date('my') . parent::generate_random_text(3, true);
        $gencode = parent::generate_random_alphabet('2', true) . date('d') . parent::generate_random_alphabet(2, true) . date('m') . parent::generate_random_alphabet(2, true) . date('y');
        $code_mail = $gencode . $refund_code;
        $input_amount = str_replace(',', '', parent::get_input_post("amount"));

        $selected->trx_no = $refund_code;
        $selected->deposit_trx_amt = $input_amount;
        $selected->non_deposit_trx_amt = "0";
        $selected->bank_name = parent::get_input_post("bank_name");
        $selected->bank_branch_name = parent::get_input_post("bank_branch_name");
        $selected->bank_acct_no = parent::get_input_post("bank_acct_no");
        $selected->bank_acct_name = parent::get_input_post("bank_acct_name");


        $data_agent = $this->M_profile_agent->get_data_agent($selected);

        $this->data[DATA_SELECTED][LIST_DATA][] = $selected;

        /* check bank exist */
        try {
            $bank_name = $data_agent[2][0]->bank_name;
            $bank_branch_name = $data_agent[2][0]->bank_branch_name;
            $bank_acct_no = $data_agent[2][0]->bank_acct_no;
            $bank_acct_name = $data_agent[2][0]->bank_acct_name;
            if ($bank_name == '' && $bank_branch_name == '' && $bank_acct_no == '' && $bank_acct_name == '') {
                throw new Exception(ERROR_BANK_NOT_EXISTS);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                if ($input_amount != "") {
                    if ($input_amount <= 0) {
                        throw new Exception(ERROR_DEPOSIT_NOMINAL);
                    } else {
                        if ($input_amount > $data_agent[2][0]->deposit_amt) {
                            throw new Exception(ERROR_DEPOSIT);
                        } else {
                            $this->M_profile_agent->trans_begin();
                            $this->M_profile_agent->save_add_agent_account($selected);

                            $params = new stdClass();
                            $params->user_id = parent::get_agent_user_seq();
                            $params->ip_address = parent::get_ip_address();
                            $params->code = AGENT_WITHDRAW_CODE;
                            $params->email_code = $code_mail;
                            $params->RECIPIENT_NAME = parent::get_agent_name();
                            $params->TOTAL_DEPOSIT = parent::cnum($selected->deposit_trx_amt);
                            $params->LINK = base_url(ROUTE_AGENT . "verifikasi/" . $code_mail);
                            $params->to_email = parent::get_agent_email();

                            parent::email_template($params);

                            $this->M_profile_agent->trans_commit();
                            $this->data[DATA_SUCCESS][SUCCESS] = true;
                            $this->data[DATA_SUCCESS][SUCCESS_MESSAGE][] = SUCCESS_SAVE_DEPOSIT;
                        }
                    }
                } else {
                    throw new Exception(ERROR_DEPOSIT_NOMINAL_MUST_FILL);
                }
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_agent->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_agent->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_agent->trans_rollback();
            }
        }
    }

    protected function update_deposit_topup() {
        $refund_code = REFUND_STATUS_CODE . date('my') . parent::generate_random_text(3, true);
        $gencode = parent::generate_random_alphabet('2', true) . date('d') . parent::generate_random_alphabet(2, true) . date('m') . parent::generate_random_alphabet(2, true) . date('y');
        $code_mail = $gencode . $refund_code;
        $logo_img = parent::upload_file("nlogo_img", AGENT_UPLOAD_IMAGE_TOPUP_DEPOSIT . $refund_code . "/", date_format(new datetime, 'Ymd'), IMAGE_TYPE, $this->data, IMAGE_DIMENSION_TRANSFER, "Bukti TopUp Deposit ");
        $topup_amt = parent::get_input_post('topup_amt');
        $topup_date = parent::get_input_post('topup_date');
        $topup_type = parent::get_input_post('topup_type');
        $bank = parent::get_input_post('bank');
        $topup_file = $logo_img->error ? "" : $logo_img->file_name;

        $selected = new stdClass();
        $selected->user_id = parent::get_agent_email();
        $selected->ip_address = parent::get_ip_address();
        $selected->agent_seq = parent::get_agent_user_seq();

        $selected->mutation_type = CREDIT_MUTATION_TYPE;
        $selected->pg_method_seq = PAYMENT_SEQ_DEPOSIT;
        $selected->trx_type = TOPUP_TRX_TYPE;
        $selected->trx_no = $refund_code;
        $selected->trx_date = $topup_date;
        $selected->deposit_trx_amt = $topup_amt;
        $selected->non_deposit_trx_amt = 0;
        $selected->bank_name = '';
        $selected->bank_branch_name = '';
        $selected->bank_acct_no = '';
        $selected->bank_acct_name = '';
        $selected->refund_date = '';
        $selected->conf_topup_date = $topup_date;
        $selected->conf_topup_type = $topup_type;
        $selected->conf_topup_file = $topup_file;
        $selected->conf_topup_bank_seq = $bank;
        $selected->agent_name = parent::get_agent_name();
        $selected->status = TOPUP_STATUS_CODE;

        $this->data[DATA_SELECTED][LIST_DATA][] = $selected;

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                if ($topup_amt != "") {
                    if ($topup_amt <= 0) {
                        throw new Exception(ERROR_DEPOST_TOPUP_NOMINAL);
                    } else {
                        $this->M_profile_agent->trans_begin();
                        $this->M_profile_agent->save_add_agent_account($selected);
                        $params = new stdClass();
                        $params->user_id = parent::get_agent_user_seq();
                        $params->ip_address = parent::get_ip_address();
                        $params->code = AGENT_TOPUP_DEPOSIT;
                        $params->to_email = ADMIN_TOKO1001_EMAIL;
                        $params->RECIPIENT_NAME = ADMIN_TOKO1001_NAME;
                        $params->AGENT_NAME = parent::get_agent_name();
                        $params->TOPUP_NO = $refund_code;
                        $params->email_code = $code_mail;
                        $params->TOTAL_TRANSFER = parent::cnum($selected->deposit_trx_amt);
                        $params->LINK = base_url() . 'admin/master/agent_top_up';

                        parent::email_template($params);

                        $this->M_profile_agent->trans_commit();
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_SUCCESS][SUCCESS_MESSAGE][] = SUCCESS_SAVE_DEPOSIT;
                    }
                } else {
                    throw new Exception(ERROR_DEPOSIT_NOMINAL_MUST_FILL);
                }
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_agent->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_agent->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_agent->trans_rollback();
            }
        }
    }

    protected function update_config() {
        $selected = new stdClass();
        $selected->user_id = parent::get_agent_email();
        $selected->ip_address = parent::get_ip_address();
        $selected->agent_seq = parent::get_agent_user_seq();
        $selected->old_password = parent::get_input_post("old_password", true, FILL_VALIDATOR, "Password Lama", $this->data);
        $selected->new_password = parent::get_input_post("new_password", true, FILL_VALIDATOR, "Password Baru", $this->data);

        if (strlen($selected->old_password . $selected->new_password) < 8 || strlen($selected->old_password . $selected->new_password) > 20) {
            $this->temp_data[DATA_ERROR][ERROR] = true;
            $this->temp_data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_VALIDATION_FILL_PASSWORD_LENGTH;
        }

        $selected->encrypt_old_password = md5(md5($selected->old_password));
        $selected->encrypt_new_password = md5(md5($selected->new_password));

        try {
            $list_data = $this->M_profile_agent->get_info_password($selected);
            parent::set_list_data($this->temp_data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->temp_data, $ex);
        }

        if ($selected->encrypt_old_password == $list_data[0]->old_password) {
            try {
                $list_data = new stdClass();
                $list_data->ip_address = parent::get_ip_address();
                $list_data->user_id = parent::get_agent_user_seq();
                $list_data->new_password = parent::get_input_post("new_password");

                $list_data->encrypt_new_password = md5(md5($selected->new_password));

                $this->M_profile_agent->trans_begin();
                $this->M_profile_agent->save_update_settings($list_data);
                $this->M_trans_log_agent->save_agent_change_password_log($selected);
                $this->M_profile_agent->trans_commit();
                $this->data[DATA_SUCCESS][SUCCESS] = true;
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_agent->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_agent->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_agent->trans_rollback();
            }
        } else {
            $this->data[DATA_ERROR][ERROR] = true;
            $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_INVALID_OLD_PASSWORD;
        }
    }

    public function get_review_agent() {
        $filter = new stdClass;
        $filter->user_id = parent::get_agent_email();
        $filter->ip_address = parent::get_ip_address();
        $filter->agent_seq = parent::get_agent_user_seq();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = "desc";
        $filter->column = "created_date";

        try {
            $list_data = $this->M_profile_agent->get_review_agent_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );


        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $bintang = str_repeat('<i class="fa fa-fw fa-star"></i>', $data_row->rate);
                $row = array("DT_RowId" => $data_row->product_variant_seq . '-' . $data_row->order_seq,
                    "product_name" => '<a href="' . url_title($data_row->product_name . " " . $data_row->seq) . '"><center><img src="' . PRODUCT_UPLOAD_IMAGE . $data_row->merchant_seq . '/' . XS_IMAGE_UPLOAD . $data_row->pic_1_img . '" class = "img-thumbnail"></center></a>',
                    "order_date" => '<a href="' . url_title($data_row->product_name . " " . $data_row->seq) . '"><b>' . $data_row->product_name . '</b></a><br />Nomor Order &nbsp; : <b>' . $data_row->order_no . '</b><br />Tanggal Order &nbsp;: ' . parent::cdate($data_row->order_date),
                    "rate" => '<p class="text-primary">' . $bintang . "<br />" . parent::cdate($data_row->created_date) . "</p>" . $data_row->review . ""
                );
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
        die();
    }

    protected function update_message() {
        $selected = new stdClass();
        $selected->ip_address = parent::get_ip_address();
        $selected->agent_seq = parent::get_agent_user_seq();
        $selected->user_id = parent::get_agent_email();
        $selected->content = parent::get_input_post("message_text");
        $selected->prev_message_seq = parent::get_input_post("prev_message_seq");
        if (parent::get_input_post("message_text") == '' || parent::get_agent_user_seq() == '') {
            $data[DATA_ERROR][ERROR] = true;
            $data[DATA_ERROR][ERROR_MESSAGE][] = "ERROR : Data Kosong";
            die();
        }
        try {
            $this->M_profile_agent->trans_begin();
            $this->M_profile_agent->save_insert_message($selected);
            $this->M_profile_agent->trans_commit();
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_profile_agent->trans_rollback();
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_profile_agent->trans_rollback();
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_profile_agent->trans_rollback();
        }
        die();
    }

    public function get_message_log_list() {
        $filter = new stdClass;
        $filter->user_id = parent::get_agent_email();
        $filter->ip_address = parent::get_ip_address();
        $filter->agent_seq = parent::get_agent_user_seq();
        $filter->start = parent::get_input_post("start");
        $filter->length = PAGINATION_LIMIT;
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->status = parent::get_input_post("status");

        try {
            $list_data = $this->M_profile_agent->get_message_log_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );

        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                if ($data_row->agent_seq !== NULL) {
                    $user_name = "ANDA";
                } else {
                    $user_name = "ADMIN";
                }
                $row = array("DT_RowId" => $data_row->seq,
                    "seq" => parent::cdef($data_row->seq),
                    "headers" => ($data_row->prev_message_seq != null ? $data_row->prev_message_seq : $data_row->seq),
                    "content" => parent::get_trim_text(parent::cdef($data_row->content), 100),
                    "prev_message_seq" => $data_row->prev_message_seq,
                    "agent_seq" => $data_row->agent_seq,
                    "agent_name" => $data_row->agent_name,
                    "admin_id" => $data_row->admin_id,
                    "admin_name" => $data_row->admin_name,
                    "user_name" => $user_name,
                    "status" => $data_row->status,
                    "created_date" => parent::cdate($data_row->created_date)
                );
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
        die();
    }

    public function get_message_one_topic() {
        $filter = new stdClass;
        $filter->user_id = parent::get_agent_email();
        $filter->ip_address = parent::get_ip_address();
        $filter->agent_seq = parent::get_agent_user_seq();
        $filter->seq = parent::get_input_post("seq");
        try {
            $list_data = $this->M_profile_agent->get_message_log_list_by_seq($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        $output = array('' => '',
            "aaData" => array()
        );
        if (isset($list_data)) {
            foreach ($list_data as $data_row) {
                if ($data_row->agent_seq !== NULL) {
                    $user_name = "ANDA";
                } else {
                    $user_name = "ADMIN";
                }
                $row = array("DT_RowId" => $data_row->seq,
                    "seq" => parent::cdef($data_row->seq),
                    "headers" => ($data_row->prev_message_seq != null ? $data_row->prev_message_seq : $data_row->seq),
                    "content" => nl2br(parent::cdef($data_row->content)),
                    "prev_message_seq" => $data_row->prev_message_seq,
                    "agent_seq" => $data_row->agent_seq,
                    "agent_name" => $data_row->agent_name,
                    "admin_id" => $data_row->admin_id,
                    "admin_name" => $data_row->admin_name,
                    "user_name" => $user_name,
                    "status" => $data_row->status,
                    "created_date" => parent::cdate($data_row->created_date)
                );
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
        die();
    }

    private function send_notif($order_no, $member_seq, $agent_seq) {
        //notif member or agent
        $notif = new stdClass();
        $notif->user_id = 'SYSTEM';
        $notif->member_seq = $member_seq;
        $notif->agent_seq = $agent_seq;
        $notif->order_no = $order_no;
        $notif->member_code = RATE_PRODUCT_ORDER_MEMBER;
        $notif->agent_code = RATE_PRODUCT_ORDER_AGENT;
        parent::sendnotif_member_or_agent($notif);
    }

    public function get_notification_agent() {
        $filter = new stdClass;
        $filter->user_id = parent::get_agent_email();
        $filter->ip_address = parent::get_ip_address();
        $filter->agent_seq = parent::get_agent_user_seq();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");

        try {
            $list_data = $this->M_profile_agent->get_notification_agent_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );

        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->seq,
                    "notif_seq" => $data_row->seq,
                    "notif_code" => $data_row->notif_code,
                    "agent_seq" => $data_row->agent_seq,
                    "content" => $data_row->content,
                    "status" => $data_row->status,
                    "created_by" => $data_row->created_by,
                    "created_date" => $data_row->created_date,
                    "subject" => $data_row->subject,
                );

                $output['aaData'][] = $row;
            }
        };
        $output['aatotalData'] = $this->get_notification_total_unread()[0]->total;
        echo json_encode($output);
        die();
    }

    public function get_notification_total_unread() {
        $filter = new stdClass;
        $filter->user_id = parent::get_agent_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->status = "U";
        $filter->user_seq = parent::get_agent_user_seq();
        $filter->user_group = "AGENT";
        $list_data = 0;
        try {
            $list_data = $this->M_profile_agent->get_notification_total_by_status($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        return $list_data;
    }

    public function set_read() {
        $filter = new stdClass;
        $filter->user_id = parent::get_agent_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->seq = parent::get_input_post('seq');
        $filter->status = 'R';
        $filter->table = 'AGENT';
        $this->M_profile_agent->save_update_notification($filter);
    }

    public function get_agent_customer() {
        $filter = new stdClass;
        $filter->user_id = parent::get_agent_email();
        $filter->ip_address = parent::get_ip_address();
        $filter->agent_seq = parent::get_agent_user_seq();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->pic_name = parent::get_input_post("pic_name");
        $filter->identity_no = parent::get_input_post("identity_no");
        $filter->email_address = parent::get_input_post("email_address");
        try {
            $list_data = $this->M_profile_agent->get_agent_customer_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );

        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->seq,
                    "agent_seq" => $data_row->agent_seq,
                    "identity_no" => $data_row->identity_no,
                    "identity_address" => $data_row->identity_address,
                    "birthday" => parent::cdate($data_row->birthday),
                    "address" => $data_row->address . '<br />' . $data_row->sub_district . ', ' . $data_row->district_name . ', ' . $data_row->city_name . ', ' . $data_row->province_name . $data_row->zip_code,
                    "email_address" => $data_row->email_address,
                    "district_seq" => $data_row->district_seq,
                    "sub_district" => $data_row->sub_district,
                    "zip_code" => $data_row->zip_code,
                    "pic_name" => $data_row->pic_name,
                    "phone_no" => $data_row->phone_no,
                    "active" => $data_row->active,
                    "created_date" => $data_row->created_date,
                    "modified_date" => $data_row->modified_date,
                );

                $output['aaData'][] = $row;
            }
        };
        $output['aatotalData'] = $this->get_notification_total_unread()[0]->total;
        echo json_encode($output);
        die();
    }

}
