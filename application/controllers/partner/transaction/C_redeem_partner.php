<?php

require_once CONTROLLER_BASE_PARTNER;

class C_redeem_partner extends controller_base_partner {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("PTRX00002", "partner/redeem_partner");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('partner/transaction/M_redeem_partner');
        $this->load->library('pagination');
        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_ADDITIONAL, "get_ajax");
        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        if (!$this->input->post()) {
            $this->data['breadcrumb'] = parent::get_breadcrumb($this->data);
            $this->load->view("partner/transaction/redeem_partner", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("partner/transaction/agent_order_partner", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("partner/transaction/agent_order_partner", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_ADDITIONAL) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADDITIONAL;
                        $this->load->view("partner/transaction/agent_order_partner", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    unset($_SESSION[SESSION_DATA]);
                    redirect(base_url("partner/transaction/partner_order_list_agen"));
                }
            }
        }
    }

    protected function get_edit() {
        $selected = new stdClass();
        $selected->user_id = parent::get_partner_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->seq = parent::get_input_get("key");
        $selected->partner_seq = parent::get_partner_user_seq();
        $selected->dseq = "";
        $selected->cseq = "";
        $selected->pseq = "";
        $order_status = '';


        try {
            $sel_data = $this->M_agent_order_partner->get_data($selected);

            if (isset($sel_data[0][0])) {
                $selected->dseq = $sel_data[0][0]->receiver_district_seq;
                $selected->cseq = "";
                $selected->pseq = "";
                $order_status = parent::cstdes($sel_data[0][0]->order_status, STATUS_ORDER);
            } else {
                redirect(base_url("partner/agent_order"));
            }

            $sel_data1 = $this->M_agent_order_partner->get_location($selected);
            $sel_data2 = $this->M_agent_order_partner->get_agent($selected);

            if (isset($sel_data)) {
                $this->data[EXPEDITION_NAME] = $this->M_agent_order_partner->get_dropdown_expedition();
                parent:: set_data($this->data, $sel_data);
                $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data1;
                $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data2;
                $this->data[DATA_SELECTED][LIST_DATA][] = $order_status;
            }
        } catch (Exception $ex) {
            parent:: set_error($this->data, $ex);
        }
    }

    public function search() {
        $params = new stdClass;
        $params->user_id = parent::get_partner_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->partner_seq = parent::get_partner_user_seq();
        $params->start = parent::get_input_get(START_OFFSET) == "" ? 0 : parent::get_input_get(START_OFFSET);
        $params->length = parent::get_input_get("length") == '' ? 10 : parent::get_input_get("length");
        $params->order = parent::get_input_get("order") == '' ? 'ASC' : parent::get_input_get("order");
        $params->column = parent::get_input_get("column") == '' ? 'from_date' : parent::get_input_get("column");
        $params->from_date = parent::get_input_get("from_date") == '' ? '' : parent::get_input_get("date1");
        $params->to_date = parent::get_input_get("to_date") == '' ? '' : parent::get_input_get("date2");
        $params->status = parent::get_input_get("status") == '' ? '' : parent::get_input_get("status");
        $params->paid_date = parent::get_input_get("paid_date") == '' ? '' : parent::get_input_get("paid_date");
        $params->paid_date_month = parent::get_input_get("paid_date") == '' ? '' : DateTime::createFromFormat('M-Y', parent::get_input_get("paid_date"))->format('n');
        $params->paid_date_year = parent::get_input_get("paid_date") == '' ? '' : DateTime::createFromFormat('M-Y', parent::get_input_get("paid_date"))->format('Y');
        try {
            $list_data = $this->M_redeem_partner->get_list($params);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $this->data['filter'] = "?paid_date=" . $params->paid_date;
        $this->data['filter_sort'] = "&order=" . $params->order . "&column=" . $params->column;
//        $this->data['filter_payment_status'] = "?payment_status=" . $filter->payment_status;
        $this->display_page(base_url("partner/redeem_partner") . $this->data['filter'] . $this->data['filter_sort'], $params->start, $list_data[0][0]->total_rec, $params->length);
    }

    public function get_ajax() {
        $type = parent::get_input_get('type');
        $params = new stdClass;
        $params->user_id = parent::get_partner_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->partner_seq = parent::get_partner_user_seq();
        $params->order = parent::get_input_get("order") == '' ? 'ASC' : parent::get_input_get("order");
        $params->column = parent::get_input_get("column") == '' ? 'from_date' : parent::get_input_get("column");
        $params->from_date = parent::get_input_get("from_date") == '' ? '' : parent::get_input_get("date1");
        $params->to_date = parent::get_input_get("to_date") == '' ? '' : parent::get_input_get("date2");
        $params->status = parent::get_input_get("status") == '' ? '' : parent::get_input_get("status");
        $params->paid_date = parent::get_input_get("paid_date") == '' ? '' : parent::get_input_get("paid_date");
        $params->paid_date_month = parent::get_input_get("paid_date") == '' ? '' : DateTime::createFromFormat('M-Y', parent::get_input_get("paid_date"))->format('n');
        $params->paid_date_year = parent::get_input_get("paid_date") == '' ? '' : DateTime::createFromFormat('M-Y', parent::get_input_get("paid_date"))->format('Y');
        switch ($type) {
            case 'print_data_excel':
                try {
                    $list_data = $this->M_redeem_partner->get_list_excel($params);
                    parent::set_data($this->data, $list_data);
                    if (isset($list_data)) {
                        foreach ($list_data as $data_row) {
                            $row = array("Periode" => $data_row->from_date . ' - ' . $data_row->to_date,
                                "Tanggal Bayar" => $data_row->paid_date,
                                "Total Komisi(Rp.)" => $data_row->total_commission,
                                "Total Order(Rp.)" => $data_row->total_order,
                            );
                            $output[] = $row;
                        }
                    }
                    echo json_encode($output);
                    exit();
                } catch (Exception $ex) {
                    parent::set_error($this->data, $ex);
                }
                break;
        }
    }

}

?>