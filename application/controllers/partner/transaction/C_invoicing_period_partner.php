<?php

require_once CONTROLLER_BASE_PARTNER;

class C_invoicing_period_partner extends controller_base_partner {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("PTRX00004", "partner/invoicing_period");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('partner/transaction/M_invoicing_period_partner');
        $this->load->model('component/M_radio_button');
        $this->load->library('pagination');
        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_ADDITIONAL, "get_ajax");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        if (!$this->input->post()) {
            $this->data['breadcrumb'] = parent::get_breadcrumb($this->data);
            $this->load->view("partner/transaction/invoicing_period_partner", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("partner/transaction/invoicing_period_partner", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("partner/transaction/invoicing_period_partner", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_ADDITIONAL) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADDITIONAL;
                        $this->load->view("partner/transaction/invoicing_period_partner", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    unset($_SESSION[SESSION_DATA]);
                    redirect(base_url("partner/invoicing_period"));
                }
            }
        }
    }

    public function search() {
        $params = new stdClass;
        $params->user_id = parent::get_partner_user_id();
        $params->ip_address = parent::get_ip_address();
        
        $params->start = parent::get_input_get(START_OFFSET) == "" ? 0 : parent::get_input_get(START_OFFSET);
        $params->length = parent::get_input_get("length") == '' ? 10 : parent::get_input_get("length");
        $params->column = parent::get_input_get("column") == '' ? 'from_date' : parent::get_input_get("column");
        $params->order = parent::get_input_get("order") == '' ? 'ASC' : parent::get_input_get("order");
        $params->partner_seq = parent::get_partner_user_seq();
        $params->from_date = parent::get_input_get("order_date_s") == '' ? '0000-00-00' : parent::get_input_get("order_date_s");
        $params->to_date = parent::get_input_get("order_date_e") == '' ? '' : parent::get_input_get("order_date_e");
        $params->status = parent::get_input_get("status") == '' ? '' : parent::get_input_get("status");
        try {
            $list_data = $this->M_invoicing_period_partner->get_list($params);
            parent::set_list_data($this->data, $list_data);
            $this->data[DATA_HEADER][LIST_DATA][] = $params;
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $this->data['filter'] = "?status=" . $params->status . "&order_date_s=" . $params->from_date . "&order_date_e=" . $params->to_date;
        $this->data['filter_sort'] = "&order=" . $params->order . "&column=" . $params->column;
//        $this->data['filter_payment_status'] = "?payment_status=" . $filter->payment_status;
        $this->display_page(base_url("partner/redeem_partner") . $this->data['filter'] . $this->data['filter_sort'], $params->start, $list_data[0][0]->total_rec, $params->length);
    }

    public function get_ajax() {
        $type = parent::get_input_get('type');
        $params = new stdClass;
        $params->user_id = parent::get_partner_user_id();
        $params->ip_address = parent::get_ip_address();
        
        $selected = new stdClass();
        $selected->user_id = parent::get_partner_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->seq = parent::get_input_get("key");
        $selected->partner_seq = parent::get_partner_user_seq();
        switch ($type) {
            case 'print_data_excel':
                try {
                    $list_data = $this->M_invoicing_period_partner->get_order_agent_by_loan($selected);
                    parent::set_data($this->data, $list_data);
                    if (isset($list_data)) {
                        foreach ($list_data as $data_row) {
                            $row = array("No Order" => $data_row->order_no,
                                "Tanggal Order" => parent::cdate($data_row->order_date),
                                "Nilai Order" => ($data_row->total_payment),
                                "Pembayaran 1" => ($data_row->total_installment),
                                "Total Tagihan" => ($data_row->total_payment - $data_row->total_installment),
                            );
                            $output[] = $row;
                        }
                    }
                    echo json_encode($output);
                    exit();
                } catch (Exception $ex) {
                    parent::set_error($this->data, $ex);
                }
                break;
        }
    }

    protected function get_edit() {
        $selected = new stdClass();
        $selected->user_id = parent::get_partner_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->seq = parent::get_input_get("key");
        $selected->partner_seq = parent::get_partner_user_seq();
        try {
            $sel_data = $this->M_invoicing_period_partner->get_data_detail($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
                $sel_data2 = $this->M_invoicing_period_partner->get_order_agent_by_loan($selected);
                $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data2;
            }
            $sel_data3 = $this->M_radio_button->get_bank_list();
            $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data3;
            $this->data[DATA_SELECTED][LIST_DATA][] = $selected;
        } catch (Exception $ex) {
            parent:: set_error($this->data, $ex);
        }
    }

    protected function save_update() {
        $params = new stdClass();
        $params->user_id = parent::get_partner_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->collect_seq = parent::get_input_post("collect_seq");
        $params->partner_seq = parent::get_input_post("partner_seq");
        $params->paid_date = parent::get_input_post("paid_date", true, DATE_VALIDATOR, "Tanggal Bayar", $this->data);
        $params->seq = $params->collect_seq;
        $sel_data = $this->M_invoicing_period_partner->get_data_detail($params);
        if (isset($sel_data)) {
            $params->status = $sel_data[0]->status;
            $params->total = $sel_data[0]->total;
            $params->bank_name = $sel_data[0]->bank_name;
            $params->bank_branch_name = $sel_data[0]->bank_branch_name;
            $params->bank_acct_no = $sel_data[0]->bank_acct_no;
            $params->bank_acct_name = $sel_data[0]->bank_acct_name;
            $params->name = $sel_data[0]->name;
            $params->partner_seq = $sel_data[0]->partner_seq;
            if ($sel_data[0]->status != "P") {
                $this->data[DATA_ERROR][ERROR] = true;
                $this->data[DATA_ERROR][ERROR_MESSAGE][] = "ERROR : " . ERROR_UPDATE;
            }
        } else {
            $this->data[DATA_ERROR][ERROR] = true;
            $this->data[DATA_ERROR][ERROR_MESSAGE][] = "ERROR : " . ERROR_UPDATE;
        }
        $this->data[DATA_SELECTED][LIST_DATA][] = $params;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_invoicing_period_partner->trans_begin();
                // update t_redeem_agent paid_date
                $this->M_invoicing_period_partner->save_update_detail($params);
                // cek apa semua sudah dibayar
                /*$statusall = $this->get_status_redeem($params->redeem_seq);
                if ($statusall == "ALL") {
                    // update t_redeem_period status semua sudah update
                    $params->status_new = "C";
                    $params->status_old = "P";
                    $this->M_invoicing_partner_admin->status_update_agent_period($params);
                    $this->send_email_merchant_redeem($params->redeem_seq);
                    //$this->send_notif_merchant_redeem($params->redeem_seq, $params->partner_seq);
                }*/
                $this->M_invoicing_period_partner->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_invoicing_period_partner->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_invoicing_period_partner->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_invoicing_period_partner->trans_rollback();
            }
        }

    }

}

?>