<?php

require_once CONTROLLER_BASE_PARTNER;

class C_redeem_detail_partner extends controller_base_partner {

    private $data;
    private $type;
    private $key;
    private $commision = false;

    public function __construct() {
        $this->data = parent::__construct("PTRX00003", "partner/redeem_detail_partner");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('partner/transaction/M_redeem_partner');
        $this->load->library('pagination');
        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_ADDITIONAL, "get_ajax");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        $this->data['breadcrumb'] = parent::get_breadcrumb($this->data);
        if (!$this->input->post()) {
            if ($this->type) {
                $this->load->view("partner/transaction/detail_redeem_partner", $this->data);
            } else {
                $this->load->view("partner/transaction/redeem_detail_partner", $this->data);
            }
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("partner/transaction/agent_order_partner", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("partner/transaction/agent_order_partner", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_ADDITIONAL) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADDITIONAL;
                        $this->load->view("partner/transaction/agent_order_partner", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    unset($_SESSION[SESSION_DATA]);
                    redirect(base_url("partner/redeem_detail_partner?key=" . $this->key . "&type=redem"));
                }
            }
        }
    }

    protected function get_edit() {
        $selected = new stdClass();
        $selected->user_id = parent::get_partner_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->seq = parent::get_input_get("key");
        $selected->partner_seq = parent::get_partner_user_seq();
        $selected->dseq = "";
        $selected->cseq = "";
        $selected->pseq = "";
        $order_status = '';


        try {
            $sel_data = $this->M_agent_order_partner->get_data($selected);

            if (isset($sel_data[0][0])) {
                $selected->dseq = $sel_data[0][0]->receiver_district_seq;
                $selected->cseq = "";
                $selected->pseq = "";
                $order_status = parent::cstdes($sel_data[0][0]->order_status, STATUS_ORDER);
            } else {
                redirect(base_url("partner/agent_order"));
            }

            $sel_data1 = $this->M_agent_order_partner->get_location($selected);
            $sel_data2 = $this->M_agent_order_partner->get_agent($selected);

            if (isset($sel_data)) {
                $this->data[EXPEDITION_NAME] = $this->M_agent_order_partner->get_dropdown_expedition();
                parent:: set_data($this->data, $sel_data);
                $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data1;
                $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data2;
                $this->data[DATA_SELECTED][LIST_DATA][] = $order_status;
            }
        } catch (Exception $ex) {
            parent:: set_error($this->data, $ex);
        }
    }

    public function search() {
        $params = new stdClass;
        $params->user_id = parent::get_partner_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->partner_seq = parent::get_partner_user_seq();
        $params->redeem_agent_seq = parent::get_input_get('key');
        $params->type = parent::get_input_get('type') != '' ? parent::get_input_get('type') : '';
        $params->start = parent::get_input_get(START_OFFSET) == "" ? 0 : parent::get_input_get(START_OFFSET);
        $params->length = parent::get_input_get("length") == '' ? 10 : parent::get_input_get("length");
        $params->order = parent::get_input_get("order") == '' ? 'DESC' : parent::get_input_get("order");
        $params->column = parent::get_input_get("column") == '' ? 'to.seq' : parent::get_input_get("column");
        if (isset($params->type) && $params->type != '') {
            $this->type = TRUE;
            $this->get_search_redeem($params->redeem_agent_seq);
        } else {
            try {
                $list_data = $this->M_redeem_partner->get_list_detail($params);
                parent::set_list_data($this->data, $list_data);
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
            }

//        $this->data['filter'] = "&agent_seq=" . $filter->agent_seq . "&order_no_f=" . $filter->order_no_f . "&order_date_s=" . $filter->order_date_s . "&order_date_e=" . $filter->order_date_e;
            $this->data['filter_sort'] = "&order=" . $params->order . "&column=" . $params->column;
            $this->data['filter_reedem_period'] = "?key=" . $params->redeem_agent_seq;
            $this->display_page(base_url("partner/redeem_detail_partner") . $this->data['filter_reedem_period'] . $this->data['filter_sort'], $params->start, $list_data[0][0]->total_rec, $params->length);
        }
    }

    public function get_ajax() {
        $type = parent::get_input_get('type');
        $params = new stdClass;
        $params->user_id = parent::get_partner_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->partner_seq = parent::get_partner_user_seq();
        $params->redeem_agent_seq = parent::get_input_get('key');
        $params->order = parent::get_input_get("order") == '' ? 'DESC' : parent::get_input_get("order");
        $params->column = parent::get_input_get("column") == '' ? 'to.seq' : parent::get_input_get("column");
        switch ($type) {
            case 'print_excel':
                try {
                    $list_data = $this->M_redeem_partner->get_list_detail_excel($params);
                    parent::set_data($this->data, $list_data);
                    if (isset($list_data)) {
                        foreach ($list_data as $data_row) {
                            $row = array("Order No." => $data_row->order_no,
                                "Nama Agen" => $data_row->name,
                                "Nama Produk" => $data_row->product_name,
                                "Jumlah" => $data_row->qty,
                                "Harga Satuan (Rp.)" => $data_row->sell_price,
                                "Biaya Ekspedisi (Rp.)" => $data_row->total_ship_charged,
                                "Order (Rp.)" => $data_row->total_payment,
                                "Komisi (%)" => $data_row->commission_fee_percent,
                                "Komisi (Rp.)" => $data_row->total_commission,
                            );
                            $output[] = $row;
                        }
                    }
                } catch (Exception $ex) {
                    parent::set_error($this->data, $ex);
                }
                echo json_encode($output);
                exit();
                break;
        }
    }

    protected function get_search_redeem($data) {

        $params = new stdClass;
        $params->user_id = parent::get_partner_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->partner_seq = parent::get_partner_user_seq();
        $params->redeem_agent_seq = $data;
        $params->start = parent::get_input_get(START_OFFSET) == "" ? 0 : parent::get_input_get(START_OFFSET);
        $params->length = parent::get_input_get("length") == '' ? 10 : parent::get_input_get("length");
        $params->order = parent::get_input_get("order") == '' ? 'DESC' : parent::get_input_get("order");
        $params->column = parent::get_input_get("column") == '' ? 'redeem_seq' : parent::get_input_get("column");
        try {
            $list_data = $this->M_redeem_partner->get_detail_redeem_per_agent_only($params);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_update() {
        $this->commision = true;
        $params = new stdClass();
        $params->user_id = parent::get_partner_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->partner_seq = parent::get_partner_user_seq();
        $params->partner_redeem_seq = parent::get_input_post('redeem_seq');
        $params->paid_date = $this->input->post('paid_date');
        $params->agent_selected = $this->input->post('agent_seq');
        $params->status = 'T';
        $this->key = parent::get_input_post('redeem_seq');
        $total_array = count($params->agent_selected);
        
        foreach ($params->agent_selected as $key=>$data) {
            $this->data[PAID_DATE_SELECTED][$data] = $params->paid_date[$key];
        }
        
        if ($total_array < 1) {

            $this->data[DATA_ERROR][ERROR] = true;
            $this->data[DATA_ERROR][ERROR_MESSAGE] = ERROR_NO_DATA_SELECTED;

            $this->session->set_flashdata(SESSION_PAID, ERROR_NO_DATA_SELECTED);
            $this->session->set_flashdata(SESSION_PAID_CHECKED, $params->agent_selected);
            $this->session->set_flashdata(SESSION_PAID_SELECTED, $this->data[PAID_DATE_SELECTED]);
            redirect(base_url("partner/redeem_detail_partner?key=" . $this->key . "&type=redem"));
        }
        for ($i = 0; $i < $total_array; $i++):
            $pay_date = $params->paid_date[$i];
            if ($pay_date == ''):

                $this->data[DATA_ERROR][ERROR] = true;
                $this->data[DATA_ERROR][ERROR_MESSAGE] = ERROR_EMPTY_DATETIME;

                $this->session->set_flashdata(SESSION_PAID, ERROR_EMPTY_DATETIME);
                $this->session->set_flashdata(SESSION_PAID_CHECKED, $params->agent_selected);
                $this->session->set_flashdata(SESSION_PAID_SELECTED, $this->data[PAID_DATE_SELECTED]);
                redirect(base_url("partner/redeem_detail_partner?key=" . $this->key . "&type=redem"));
            endif;
        endfor;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_redeem_partner->trans_begin();
                for ($i = 0; $i < $total_array; $i++):
                    $data = new stdClass();
                    $data->user_id = parent::get_partner_user_id();
                    $data->ip_address = parent::get_ip_address();
                    $data->partner_seq = parent::get_partner_user_seq();
                    $data->partner_redeem_seq = $params->partner_redeem_seq;
                    $data->agent_selected = $params->agent_selected[$i];
                    $data->paid_date = $params->paid_date[$i];
                    $data->status = $params->status;
                    $this->M_redeem_partner->save_paid_redeem_to_agent($data);
                endfor;
                $this->M_redeem_partner->trans_commit();
                $this->session->set_flashdata(SUCCESS_PAID, SUCCESS_SAVE_PAID);
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_redeem_partner->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_redeem_partner->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_redeem_partner->trans_rollback();
            }
        }
    }

}

?>