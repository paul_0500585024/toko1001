<?php

require_once CONTROLLER_BASE_PARTNER;

class C_agent_order_partner extends controller_base_partner {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("PTRX00001", "partner/agent_order");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('partner/transaction/M_agent_order_partner');
        $this->load->model('member/M_order_member');
        $this->load->model('member/M_payment_member');
        $this->load->model('component/M_radio_button');
        $this->load->model('admin/transaction/M_order_merchant_admin');
        $this->load->library('pagination');
        $this->load->library('slim');
        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_ADDITIONAL, "get_ajax");
        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        if (!$this->input->post()) {
            $this->data['breadcrumb'] = parent::get_breadcrumb($this->data);
            $this->load->view("partner/transaction/agent_order_partner", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("partner/transaction/agent_order_partner", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("partner/transaction/agent_order_partner", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_ADDITIONAL) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADDITIONAL;
                        $this->load->view("partner/transaction/agent_order_partner", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    unset($_SESSION[SESSION_DATA]);
                    redirect(base_url("partner/transaction/partner_order_list_agen"));
                }
            }
        }
    }

    protected function get_edit() {
        $selected = new stdClass();
        $selected->user_id = parent::get_partner_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->seq = parent::get_input_get("key");
        $selected->partner_seq = parent::get_partner_user_seq();
        $selected->dseq = "";
        $selected->cseq = "";
        $selected->pseq = "";
        $order_status = '';

        try {

            $sel_data = $this->M_agent_order_partner->get_data($selected);

            $selected->key = $sel_data[0][0]->order_no;
            $order_info = $this->M_order_member->get_order_info_by_order_no($selected, AGENT);
            $this->data[DATA_ORDER] = $order_info;

            $data = new stdClass();
            $data->user_id = parent::get_partner_user_id();
            $data->ip_address = parent::get_ip_address();

            foreach ($order_info[1] as $key => $each_product_info) {
                $params = new stdClass();
                $params->user_id = $selected->user_id;
                $params->ip_address = parent::get_ip_address();
                $params->order_seq = $order_info[0][0]->seq;
                $params->customer_seq = $order_info[0][0]->customer_seq;
                $customer_info = parent::get_customer_info($params);
            }

            if (isset($sel_data[0][0])) {
                $selected->dseq = $sel_data[0][0]->receiver_district_seq;
                $selected->cseq = "";
                $selected->pseq = "";
                $order_status = parent::cstdes($sel_data[0][0]->order_status, STATUS_ORDER);
            } else {
                redirect(base_url("partner/agent_order"));
            }

            $sel_data1 = $this->M_agent_order_partner->get_location($selected);
            $sel_data2 = $this->M_agent_order_partner->get_agent($selected);

            if (isset($sel_data)) {
                $this->data[EXPEDITION_NAME] = $this->M_agent_order_partner->get_dropdown_expedition();
                parent:: set_data($this->data, $sel_data);
                $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data1;
                $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data2;
                $this->data[DATA_SELECTED][LIST_DATA][] = $order_status;
                $this->data[DATA_SELECTED][LIST_DATA][] = $customer_info;
            }
        } catch (Exception $ex) {
            parent:: set_error($this->data, $ex);
        }
    }

    public function search() {
        $filter = new stdClass();
        $filter->user_id = parent::get_partner_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->partner_seq = parent::get_partner_user_seq();
        $filter->agent_seq = parent::get_input_get("agent_seq");
        $filter->sort = parent::get_input_get("sort") != "asc" ? "desc" : "asc";
        $filter->column = parent::get_input_get("column_sort") == "order_date" ? "order_date" : "order_date";
        $filter->start = parent::get_input_get(START_OFFSET) == "" ? 0 : parent::get_input_get(START_OFFSET);
        $filter->length = PAGINATION_LIMIT;
        $filter->order_no_f = parent::get_input_get("order_no_f");
        if (parent::get_input_get("payment_status") != '') {
            if (parent::is_status_inarray(parent::get_input_get("payment_status"), STATUS_PAYMENT)) {
                $filter->payment_status = parent::get_input_get("payment_status");
            } else {
                $filter->payment_status = '';
                die();
            }
        } else {
            $filter->payment_status = '';
        }
        if (parent::get_input_get("order_date_s") !== NULL && parent::get_input_get("order_date_s") != '') {
            if (strtotime(parent::get_input_get("order_date_s"))) {
                $filter->order_date_s = parent::get_input_get("order_date_s");
            } else {
                $filter->order_date_s = '';
            }
        } else {
            $filter->order_date_s = '';
        }
        if (parent::get_input_get("order_date_e") !== NULL && parent::get_input_get("order_date_e") != '') {
            if (strtotime(parent::get_input_get("order_date_e"))) {
                $filter->order_date_e = parent::get_input_get("order_date_e");
            } else {
                $filter->order_date_e = '';
            }
        } else {
            $filter->order_date_e = '';
        }
        try {
            $list_data = $this->M_agent_order_partner->get_list($filter);
            parent::set_list_data($this->data, $list_data);

            $status_data_partner = $this->M_agent_order_partner->get_data_status($filter);
            $this->data[DATA_SELECTED][LIST_DATA][0] = $status_data_partner;

            $agent_dropdown = $this->M_agent_order_partner->get_agent_dropdown($filter);
            $this->data[DATA_SELECTED][LIST_DATA][1] = $agent_dropdown;
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $this->data['filter'] = "&agent_seq=" . $filter->agent_seq . "&order_no_f=" . $filter->order_no_f . "&order_date_s=" . $filter->order_date_s . "&order_date_e=" . $filter->order_date_e;
        $this->data['filter_sort'] = "&sort=" . $filter->sort . "&column_sort=" . $filter->column;
        $this->data['filter_payment_status'] = "?payment_status=" . $filter->payment_status;
        $array = json_decode(json_encode($list_data[0]), true);
        $this->display_page(base_url("partner/agent_order") . $this->data['filter_payment_status'] . $this->data['filter'] . $this->data['filter_sort'] = "&sort=" . $filter->sort . "&column_sort=" . $filter->column, $filter->start, $list_data[0][0]->total_rec, $filter->length);
    }

    protected function get_ajax() {
        $type = parent::get_input_get('type');
        $seq = parent::get_input_get('seq');

        $params = new stdClass();
        $params->user_id = parent::get_partner_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->seq = $seq;

        $params->partner_seq = parent::get_partner_user_seq();
        switch ($type) {
            case 'update_status';

                $status = parent::get_input_get('status');
                $po_no = parent::get_input_get('po_no');
                $params->status = $status;
                $params->po_no = $po_no;
                $this->M_agent_order_partner->update_status($params);

                $params = new stdClass();
                $params->user_id = parent::get_partner_user_id();
                $params->ip_address = parent::get_ip_address();
                $params->order_seq = $seq;
                $params->order_no = parent::get_input_get('order_no');
                $params->payment_status = $status == APPROVE_STATUS_CODE ? PAYMENT_WAIT_CONFIRM_STATUS_CODE : PAYMENT_CANCEL_BY_ADMIN_STATUS_CODE;
                $params->order_status = $status == APPROVE_STATUS_CODE ? ORDER_PREORDER_STATUS_CODE : ORDER_CANCEL_BY_PARTNER_STATUS_CODE;
                $params->paid_date = "0000-00-00 00:00:00";
                $this->M_payment_member->save_update_status_order($params);

                $params->order_no = parent::get_input_get('order_no');
                echo json_encode(array('status' => 'success'));
                if ($status == REJECT_BY_PARTNER) {
                    $this->sendemail($params->order_no);
                    $this->sendemail_reject_agent($params->order_no);
                    $this->sendemail_reject_customer($params->order_no);
                }

                if ($status == PAYMENT_WAIT_CONFIRM_ADIRA_APPROVAL) {
                    $this->sendemail_approve($params->order_no);
                    $this->sendemail_approve_customer($params->order_no);
                }
                die();
                break;
            case 'unpaid_xls':
                $this->create_excel_partner($params);
                break;
        };
        exit();
    }

    protected function sendemail($order_no) {
        $selected = new stdClass();
        $selected->user_id = parent::get_partner_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = $order_no;

        $order_info = $this->M_order_member->get_order_info_by_order_no($selected, AGENT);
        $order_content = "<table border='1'>
                           <thead>
                            <tr>
                                <th>Produk</th>
                                <th>Qty</th>
                                <th>Harga (Rp)</th>
                                <th>Biaya Kirim (Rp)</th>
                                <th>Total (Rp)</th>
                            </tr>
                           </thead><tbody>";

        foreach ($order_info[2] as $product) {
            $order_content = $order_content . "<tr>
                                               <td>" . $product->display_name . "</td>
                                               <td>" . $product->qty . "</td>
                                               <td >" . number_format($product->sell_price) . "</td>
                                               <td >" . number_format(ceil($product->weight_kg * $product->qty) * $product->ship_price_charged) . "</td>
                                               <td > " . number_format(ceil($product->weight_kg * $product->qty) * $product->ship_price_charged + $product->sell_price * $product->qty) . "</td></tr>";
        }
        $rowcredit = '';
        if ($order_info[0][0]->promo_credit_seq > 0) {
            $selected->promo_credit_seq = $order_info[0][0]->promo_credit_seq;
            $data_credit = $this->M_order_member->get_credit_info_by_credit_seq($selected);
            if (isset($data_credit)) {
                $rowcredit = "<tr>
                                <td colspan=4>Kredit Bank</td>
                                <td>" . $data_credit[0]->bank_name . "</td>
                            </tr>
			    <tr>
                                <td colspan=4>Periode cicilan</td>
                                <td>" . $data_credit[0]->credit_month . " bulan</td>
                            </tr>
			    <tr>
                                <td colspan=4>Pembayaran bulanan</td>
                                <td>" . number_format($order_info[0][0]->total_payment / $data_credit[0]->credit_month) . "</td>
                            </tr>";
            }
        }

        $order_content = $order_content . "</tbody>
                                          <tr>
                                            <td colspan=4>Voucher / Kupon</td>
                                            <td>" . number_format($order_info[0][0]->nominal) . "</td>
                                          </tr>
                                          <tr>
                                            <td colspan=4>Total Bayar</td>
                                            <td>" . number_format($order_info[0][0]->total_payment) . "</td>
                                          </tr>" . $rowcredit . "
                                          </table>";

        $address_content = "Penerima : " . $order_info[0][0]->receiver_name . "<br>
                            Alamat : " . $order_info[0][0]->receiver_address . "-" . $order_info[0][0]->province_name . "-" . $order_info[0][0]->city_name . "-" . $order_info[0][0]->district_name . "<br>
                            No Tlp : " . $order_info[0][0]->receiver_phone_no . "<br>";


        $info_bank = "<strong>3. Info Bank</strong><br/><table border='1'><thead>
                    <tr>
                        <th>Bank</th>
                        <th>Nomor Rekening</th>
                        <th>A/n</th>
                    </tr></thead><tbody>";

        $params = new stdClass();
        $params->user_id = parent::get_partner_user_id();
        $params->ip_address = parent::get_ip_address();


        foreach ($order_info[1] as $value) {
            $params_email_merchant = new stdClass();
            $params_email_merchant->user_id = $selected->user_id;
            $params_email_merchant->ip_address = parent::get_ip_address();
            $params_email_merchant->code = REJECT_ORDER_BY_PARTNER;
            $params_email_merchant->to_email = $value->email;
            $params->order_seq = $order_info[0][0]->seq;
            $params->merchant_info_seq = $value->merchant_info_seq;
            $merchant_product = $this->M_order_merchant_admin->get_produk($params);

            $product_order_content = "";
            $product_order_content = "<table border='1'>
                                                            <thead>
                                                             <tr>
                                                                 <th>Produk</th>
                                                                 <th>Tipe Product</th>
                                                                 <th>Qty</th>
                                                             </tr>
                                                            </thead><tbody>";

            foreach ($merchant_product as $product_merchant) {
                $product_order_content = $product_order_content . "<tr>
                                                                    <td>" . $product_merchant->product_name . "</td>
                                                                    <td>" . $product_merchant->variant_name . "</td>
                                                                    <td>" . $product_merchant->qty . "</td></tr>";
            }
            $product_order_content = $product_order_content . "</tbody> </table>";

            $params_email_merchant->ORDER_ITEMS = $product_order_content;
            $params_email_merchant->RECIPIENT_NAME = $value->merchant_name;
            $params_email_merchant->ORDER_NO = $order_info[0][0]->order_no;
            $params_email_merchant->ORDER_DATE = parent::cdate($order_info[0][0]->order_date);
            $params_email_merchant->RECIPIENT_ADDRESS = $address_content;
            $params_email_merchant->email_code = parent::generate_random_alnum(20, true);
            $params_email_merchant->CONFIRM_LINK = base_url() . "merchant/transaction/merchant_delivery";

            $params_email_merchant->to_email = $value->email;
            parent::email_template($params_email_merchant);
        }
    }

    protected function sendemail_approve($order_no) {
        $selected = new stdClass();
        $selected->user_id = parent::get_partner_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = $order_no;

        $order_info = $this->M_order_member->get_order_info_by_order_no($selected, AGENT);

        $order_content = "<table border='1'>
                           <thead>
                            <tr>
                                <th>Produk</th>
                                <th>Qty</th>
                                <th>Harga (Rp)</th>
                                <th>Biaya Kirim (Rp)</th>
                                <th>Total (Rp)</th>
                            </tr>
                           </thead><tbody>";

        foreach ($order_info[2] as $product) {
            $order_content = $order_content . "<tr>
                                               <td>" . $product->display_name . "</td>
                                               <td>" . $product->qty . "</td>
                                               <td >" . number_format($product->sell_price) . "</td>
                                               <td >" . number_format(ceil($product->weight_kg * $product->qty) * $product->ship_price_charged) . "</td>
                                               <td > " . number_format(ceil($product->weight_kg * $product->qty) * $product->ship_price_charged + $product->sell_price * $product->qty) . "</td></tr>";
        }
        $rowcredit = '';

        if ($order_info[0][0]->promo_credit_seq > 0) {
            $selected->promo_credit_seq = $order_info[0][0]->promo_credit_seq;
            $data_credit = $this->M_order_member->get_credit_info_by_credit_seq($selected);
            if (isset($data_credit)) {
                $rowcredit = "<tr>
                                <td colspan=4>Kredit Bank</td>
                                <td>" . $data_credit[0]->bank_name . "</td>
                            </tr>
			    <tr>
                                <td colspan=4>Periode cicilan</td>
                                <td>" . $data_credit[0]->credit_month . " bulan</td>
                            </tr>
			    <tr>
                                <td colspan=4>Pembayaran bulanan</td>
                                <td>" . number_format($order_info[0][0]->total_payment / $data_credit[0]->credit_month) . "</td>
                            </tr>";
            }
        }

        $order_content = $order_content . "</tbody>
                                          <tr>
                                            <td colspan=4>Voucher / Kupon</td>
                                            <td>" . number_format($order_info[0][0]->nominal) . "</td>
                                          </tr>
                                          <tr>
                                            <td colspan=4>Total Bayar</td>
                                            <td>" . number_format($order_info[0][0]->total_payment) . "</td>
                                          </tr>" . $rowcredit . "
                                          </table>";

        $address_content = "Penerima : " . $order_info[0][0]->receiver_name . "<br>
                            Alamat : " . $order_info[0][0]->receiver_address . "-" . $order_info[0][0]->province_name . "-" . $order_info[0][0]->city_name . "-" . $order_info[0][0]->district_name . "<br>
                            No Tlp : " . $order_info[0][0]->receiver_phone_no . "<br>";


        $info_bank = "<strong>3. Info Bank</strong><br/><table border='1'><thead>
                    <tr>
                        <th>Bank</th>
                        <th>Nomor Rekening</th>
                        <th>A/n</th>
                    </tr></thead><tbody>";
        $bank_list = $this->M_radio_button->get_bank_list();

        foreach ($bank_list as $bank) {
            $info_bank = $info_bank . "<tr>
                                         <td>" . $bank->bank_name . "</td>
                                         <td>" . $bank->bank_acct_no . "</td>
                                         <td>" . $bank->bank_acct_name . "</td>
                                       </tr>";
        }
        $info_bank = $info_bank . "</tbody></table><br>";

        $data = new stdClass();
        $data->user_id = parent::get_partner_user_id();
        $data->ip_address = parent::get_ip_address();

        foreach ($order_info[0] as $key => $each_product_info) {
            $data->order_seq = $each_product_info->seq;
            $data->seq = $each_product_info->seq;
            $order_loan = parent::get_customer_simulation_agent($data);

            $data->customer_seq = $order_loan[0]->customer_seq;
            $customer_info = parent::get_customer_info($data);
        }


        $params = new stdClass();
        $params->user_id = parent::get_partner_user_id();
        $params->ip_address = parent::get_ip_address();

        foreach ($order_info[0] as $value) {
            $params_agent_email = new stdClass();
            $params_agent_email->user_id = $selected->user_id;
            $params_agent_email->ip_address = parent::get_ip_address();
            $params_agent_email->code = APPROVE_ORDER_BY_PARTNER;
            $params_agent_email->to_email = $value->email;
            $params_agent_email->ORDER_NO = $value->order_no;
            $params_agent_email->RECIPIENT_NAME = $value->member_name;
            $params_agent_email->PAYMENT_LINK = base_url() . "member/payment/" . $order_no;
            $params_agent_email->RECIPIENT_ADDRESS = $address_content;
            $params_agent_email->ORDER_DATE = parent::cdate($value->order_date);
            $params_agent_email->ORDER_ITEMS = $order_content;
            $params_agent_email->INFO_BANK = $info_bank;
            $params_agent_email->CONFIRM_LINK = base_url() . "member/payment/" . $value->order_no;
            $params_agent_email->TOTAL_INSTALLMENT = 'Rp. ' . number_format($order_loan[0]->total_installment);
            $params_agent_email->EXPIRED_DATE = date("d-M-Y", strtotime("+4 day"));
            $params_agent_email->SMS = '';


            parent::email_template($params_agent_email);
        }
    }

    protected function sendemail_approve_customer($order_no) {
        $selected = new stdClass();
        $selected->user_id = parent::get_partner_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = $order_no;

        $order_info = $this->M_order_member->get_order_info_by_order_no($selected, AGENT);

        $order_content = "<table border='1'>
                           <thead>
                            <tr>
                                <th>Produk</th>
                                <th>Qty</th>
                                <th>Harga (Rp)</th>
                                <th>Biaya Kirim (Rp)</th>
                                <th>Total (Rp)</th>
                            </tr>
                           </thead><tbody>";

        foreach ($order_info[2] as $product) {
            $order_content = $order_content . "<tr>
                                               <td>" . $product->display_name . "</td>
                                               <td>" . $product->qty . "</td>
                                               <td >" . number_format($product->sell_price) . "</td>
                                               <td >" . number_format(ceil($product->weight_kg * $product->qty) * $product->ship_price_charged) . "</td>
                                               <td > " . number_format(ceil($product->weight_kg * $product->qty) * $product->ship_price_charged + $product->sell_price * $product->qty) . "</td></tr>";
        }
        $rowcredit = '';

        if ($order_info[0][0]->promo_credit_seq > 0) {
            $selected->promo_credit_seq = $order_info[0][0]->promo_credit_seq;
            $data_credit = $this->M_order_member->get_credit_info_by_credit_seq($selected);
            if (isset($data_credit)) {
                $rowcredit = "<tr>
                                <td colspan=4>Kredit Bank</td>
                                <td>" . $data_credit[0]->bank_name . "</td>
                            </tr>
			    <tr>
                                <td colspan=4>Periode cicilan</td>
                                <td>" . $data_credit[0]->credit_month . " bulan</td>
                            </tr>
			    <tr>
                                <td colspan=4>Pembayaran bulanan</td>
                                <td>" . number_format($order_info[0][0]->total_payment / $data_credit[0]->credit_month) . "</td>
                            </tr>";
            }
        }

        $order_content = $order_content . "</tbody>
                                          <tr>
                                            <td colspan=4>Voucher / Kupon</td>
                                            <td>" . number_format($order_info[0][0]->nominal) . "</td>
                                          </tr>
                                          <tr>
                                            <td colspan=4>Total Bayar</td>
                                            <td>" . number_format($order_info[0][0]->total_payment) . "</td>
                                          </tr>" . $rowcredit . "
                                          </table>";

        $address_content = "Penerima : " . $order_info[0][0]->receiver_name . "<br>
                            Alamat : " . $order_info[0][0]->receiver_address . "-" . $order_info[0][0]->province_name . "-" . $order_info[0][0]->city_name . "-" . $order_info[0][0]->district_name . "<br>
                            No Tlp : " . $order_info[0][0]->receiver_phone_no . "<br>";


        $info_bank = "<strong>3. Info Bank</strong><br/><table border='1'><thead>
                    <tr>
                        <th>Bank</th>
                        <th>Nomor Rekening</th>
                        <th>A/n</th>
                    </tr></thead><tbody>";
        $bank_list = $this->M_radio_button->get_bank_list();

        foreach ($bank_list as $bank) {
            $info_bank = $info_bank . "<tr>
                                         <td>" . $bank->bank_name . "</td>
                                         <td>" . $bank->bank_acct_no . "</td>
                                         <td>" . $bank->bank_acct_name . "</td>
                                       </tr>";
        }
        $info_bank = $info_bank . "</tbody></table><br>";

        $data = new stdClass();
        $data->user_id = parent::get_partner_user_id();
        $data->ip_address = parent::get_ip_address();

        foreach ($order_info[0] as $key => $each_product_info) {
            $data->order_seq = $each_product_info->seq;
            $data->seq = $each_product_info->seq;
            $order_loan = parent::get_customer_simulation_agent($data);

            $data->customer_seq = $order_loan[0]->customer_seq;
            $customer_info = parent::get_customer_info($data);
        }


        $params = new stdClass();
        $params->user_id = parent::get_partner_user_id();
        $params->ip_address = parent::get_ip_address();

        foreach ($order_info[0] as $value) {
            $params_customer_email = new stdClass();
            $params_customer_email->user_id = $selected->user_id;
            $params_customer_email->ip_address = parent::get_ip_address();
            $params_customer_email->code = APPROVE_ORDER_BY_PARTNER;
            $params_customer_email->to_email = $customer_info[0]->email_address;

            if ($params_customer_email->to_email != '') {

                $params_customer_email->ORDER_NO = $value->order_no;
                $params_customer_email->RECIPIENT_NAME = $customer_info[0]->pic_name;
                $params_customer_email->PAYMENT_LINK = base_url() . "member/payment/" . $order_no;
                $params_customer_email->RECIPIENT_ADDRESS = $address_content;
                $params_customer_email->ORDER_DATE = parent::cdate($value->order_date);
                $params_customer_email->ORDER_ITEMS = $order_content;
                $params_customer_email->INFO_BANK = $info_bank;
                $params_customer_email->CONFIRM_LINK = base_url() . "member/payment/" . $value->order_no;
                $params_customer_email->TOTAL_INSTALLMENT = 'Rp. ' . number_format($order_loan[0]->total_installment);
                $params_customer_email->EXPIRED_DATE = date("d-M-Y", strtotime("+4 day"));
                $params_customer_email->SMS = '';

                parent::email_template($params_customer_email);
            }
        }
    }

    protected function sendemail_reject_agent($order_no) {
        $selected = new stdClass();
        $selected->user_id = parent::get_partner_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = $order_no;

        $order_info = $this->M_order_member->get_order_info_by_order_no($selected, AGENT);

        $order_content = "<table border='1'>
                           <thead>
                            <tr>
                                <th>Produk</th>
                                <th>Qty</th>
                                <th>Harga (Rp)</th>
                                <th>Biaya Kirim (Rp)</th>
                                <th>Total (Rp)</th>
                            </tr>
                           </thead><tbody>";

        foreach ($order_info[2] as $product) {
            $order_content = $order_content . "<tr>
                                               <td>" . $product->display_name . "</td>
                                               <td>" . $product->qty . "</td>
                                               <td >" . number_format($product->sell_price) . "</td>
                                               <td >" . number_format(ceil($product->weight_kg * $product->qty) * $product->ship_price_charged) . "</td>
                                               <td > " . number_format(ceil($product->weight_kg * $product->qty) * $product->ship_price_charged + $product->sell_price * $product->qty) . "</td></tr>";
        }
        $rowcredit = '';

        if ($order_info[0][0]->promo_credit_seq > 0) {
            $selected->promo_credit_seq = $order_info[0][0]->promo_credit_seq;
            $data_credit = $this->M_order_member->get_credit_info_by_credit_seq($selected);
            if (isset($data_credit)) {
                $rowcredit = "<tr>
                                <td colspan=4>Kredit Bank</td>
                                <td>" . $data_credit[0]->bank_name . "</td>
                            </tr>
			    <tr>
                                <td colspan=4>Periode cicilan</td>
                                <td>" . $data_credit[0]->credit_month . " bulan</td>
                            </tr>
			    <tr>
                                <td colspan=4>Pembayaran bulanan</td>
                                <td>" . number_format($order_info[0][0]->total_payment / $data_credit[0]->credit_month) . "</td>
                            </tr>";
            }
        }

        $order_content = $order_content . "</tbody>
                                          <tr>
                                            <td colspan=4>Voucher / Kupon</td>
                                            <td>" . number_format($order_info[0][0]->nominal) . "</td>
                                          </tr>
                                          <tr>
                                            <td colspan=4>Total Bayar</td>
                                            <td>" . number_format($order_info[0][0]->total_payment) . "</td>
                                          </tr>" . $rowcredit . "
                                          </table>";

        $address_content = "Penerima : " . $order_info[0][0]->receiver_name . "<br>
                            Alamat : " . $order_info[0][0]->receiver_address . "-" . $order_info[0][0]->province_name . "-" . $order_info[0][0]->city_name . "-" . $order_info[0][0]->district_name . "<br>
                            No Tlp : " . $order_info[0][0]->receiver_phone_no . "<br>";


        $info_bank = "<strong>3. Info Bank</strong><br/><table border='1'><thead>
                    <tr>
                        <th>Bank</th>
                        <th>Nomor Rekening</th>
                        <th>A/n</th>
                    </tr></thead><tbody>";
        $bank_list = $this->M_radio_button->get_bank_list();

        foreach ($bank_list as $bank) {
            $info_bank = $info_bank . "<tr>
                                         <td>" . $bank->bank_name . "</td>
                                         <td>" . $bank->bank_acct_no . "</td>
                                         <td>" . $bank->bank_acct_name . "</td>
                                       </tr>";
        }
        $info_bank = $info_bank . "</tbody></table><br>";

        $data = new stdClass();
        $data->user_id = parent::get_partner_user_id();
        $data->ip_address = parent::get_ip_address();

        foreach ($order_info[0] as $key => $each_product_info) {
            $data->order_seq = $each_product_info->seq;
            $data->seq = $each_product_info->seq;
            $order_loan = parent::get_customer_simulation_agent($data);

            $data->customer_seq = $order_loan[0]->customer_seq;
            $customer_info = parent::get_customer_info($data);
        }


        $params = new stdClass();
        $params->user_id = parent::get_partner_user_id();
        $params->ip_address = parent::get_ip_address();

        foreach ($order_info[0] as $value) {
            $params_agent_email = new stdClass();
            $params_agent_email->user_id = $selected->user_id;
            $params_agent_email->ip_address = parent::get_ip_address();
            $params_agent_email->code = REJECT_ORDER_BY_PARTNER_TO_AGENT_AND_CUSTOMER;
            $params_agent_email->to_email = $value->email;
            $params_agent_email->ORDER_NO = $value->order_no;
            $params_agent_email->RECIPIENT_NAME = $value->member_name;
            $params_agent_email->PAYMENT_LINK = base_url() . "member/payment/" . $order_no;
            $params_agent_email->RECIPIENT_ADDRESS = $address_content;
            $params_agent_email->ORDER_DATE = parent::cdate($value->order_date);
            $params_agent_email->ORDER_ITEMS = $order_content;
            $params_agent_email->INFO_BANK = $value->payment_code == PAYMENT_TYPE_BANK ? $info_bank : "";
            $params_agent_email->CONFIRM_LINK = base_url() . "member/payment/" . $value->order_no;
            $params_agent_email->TOTAL_INSTALLMENT = 'Rp. ' . number_format($order_loan[0]->total_installment);
            $params_agent_email->EXPIRED_DATE = date("d-M-Y", strtotime("+4 day"));

            parent::email_template($params_agent_email);
        }
    }

    protected function sendemail_reject_customer($order_no) {
        $selected = new stdClass();
        $selected->user_id = parent::get_partner_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = $order_no;

        $order_info = $this->M_order_member->get_order_info_by_order_no($selected, AGENT);

        $order_content = "<table border='1'>
                           <thead>
                            <tr>
                                <th>Produk</th>
                                <th>Qty</th>
                                <th>Harga (Rp)</th>
                                <th>Biaya Kirim (Rp)</th>
                                <th>Total (Rp)</th>
                            </tr>
                           </thead><tbody>";

        foreach ($order_info[2] as $product) {
            $order_content = $order_content . "<tr>
                                               <td>" . $product->display_name . "</td>
                                               <td>" . $product->qty . "</td>
                                               <td >" . number_format($product->sell_price) . "</td>
                                               <td >" . number_format(ceil($product->weight_kg * $product->qty) * $product->ship_price_charged) . "</td>
                                               <td > " . number_format(ceil($product->weight_kg * $product->qty) * $product->ship_price_charged + $product->sell_price * $product->qty) . "</td></tr>";
        }
        $rowcredit = '';

        if ($order_info[0][0]->promo_credit_seq > 0) {
            $selected->promo_credit_seq = $order_info[0][0]->promo_credit_seq;
            $data_credit = $this->M_order_member->get_credit_info_by_credit_seq($selected);
            if (isset($data_credit)) {
                $rowcredit = "<tr>
                                <td colspan=4>Kredit Bank</td>
                                <td>" . $data_credit[0]->bank_name . "</td>
                            </tr>
			    <tr>
                                <td colspan=4>Periode cicilan</td>
                                <td>" . $data_credit[0]->credit_month . " bulan</td>
                            </tr>
			    <tr>
                                <td colspan=4>Pembayaran bulanan</td>
                                <td>" . number_format($order_info[0][0]->total_payment / $data_credit[0]->credit_month) . "</td>
                            </tr>";
            }
        }

        $order_content = $order_content . "</tbody>
                                          <tr>
                                            <td colspan=4>Voucher / Kupon</td>
                                            <td>" . number_format($order_info[0][0]->nominal) . "</td>
                                          </tr>
                                          <tr>
                                            <td colspan=4>Total Bayar</td>
                                            <td>" . number_format($order_info[0][0]->total_payment) . "</td>
                                          </tr>" . $rowcredit . "
                                          </table>";

        $address_content = "Penerima : " . $order_info[0][0]->receiver_name . "<br>
                            Alamat : " . $order_info[0][0]->receiver_address . "-" . $order_info[0][0]->province_name . "-" . $order_info[0][0]->city_name . "-" . $order_info[0][0]->district_name . "<br>
                            No Tlp : " . $order_info[0][0]->receiver_phone_no . "<br>";


        $info_bank = "<strong>3. Info Bank</strong><br/><table border='1'><thead>
                    <tr>
                        <th>Bank</th>
                        <th>Nomor Rekening</th>
                        <th>A/n</th>
                    </tr></thead><tbody>";
        $bank_list = $this->M_radio_button->get_bank_list();

        foreach ($bank_list as $bank) {
            $info_bank = $info_bank . "<tr>
                                         <td>" . $bank->bank_name . "</td>
                                         <td>" . $bank->bank_acct_no . "</td>
                                         <td>" . $bank->bank_acct_name . "</td>
                                       </tr>";
        }
        $info_bank = $info_bank . "</tbody></table><br>";

        $data = new stdClass();
        $data->user_id = parent::get_partner_user_id();
        $data->ip_address = parent::get_ip_address();

        foreach ($order_info[0] as $key => $each_product_info) {
            $data->order_seq = $each_product_info->seq;
            $data->seq = $each_product_info->seq;
            $order_loan = parent::get_customer_simulation_agent($data);

            $data->customer_seq = $order_loan[0]->customer_seq;
            $customer_info = parent::get_customer_info($data);
        }


        $params = new stdClass();
        $params->user_id = parent::get_partner_user_id();
        $params->ip_address = parent::get_ip_address();

        foreach ($order_info[0] as $value) {
            $params_customer_email = new stdClass();
            $params_customer_email->user_id = $selected->user_id;
            $params_customer_email->ip_address = parent::get_ip_address();
            $params_customer_email->code = REJECT_ORDER_BY_PARTNER_TO_AGENT_AND_CUSTOMER;
            $params_customer_email->to_email = $customer_info[0]->email_address;

            if ($params_customer_email->to_email != '') {

                $params_customer_email->ORDER_NO = $value->order_no;
                $params_customer_email->RECIPIENT_NAME = $customer_info[0]->pic_name;
                $params_customer_email->PAYMENT_LINK = base_url() . "member/payment/" . $order_no;
                $params_customer_email->RECIPIENT_ADDRESS = $address_content;
                $params_customer_email->ORDER_DATE = parent::cdate($value->order_date);
                $params_customer_email->ORDER_ITEMS = $order_content;
                $params_customer_email->INFO_BANK = $value->payment_code == PAYMENT_TYPE_BANK ? $info_bank : "";
                $params_customer_email->CONFIRM_LINK = base_url() . "member/payment/" . $value->order_no;
                $params_customer_email->TOTAL_INSTALLMENT = 'Rp. ' . number_format($order_loan[0]->total_installment);
                $params_customer_email->EXPIRED_DATE = date("d-M-Y", strtotime("+4 day"));
                $params_customer_email->SMS = '';

                parent::email_template($params_customer_email);
            }
        }
    }

    private function create_excel_partner($data) {
        $data->pg_method_seq = PAYMENT_SEQ_ADIRA . ',' . PAYMENT_SEQ_ADIRA_DEPOSIT;
        $data->payment_status = PAYMENT_WAIT_CONFIRM_ADIRA_APPROVAL;
        $order_info = $this->M_agent_order_partner->get_partner_order_by_status($data);
        if (!isset($order_info)) {
            die('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html><head><title>' . DEFAULT_TITLE . '</title></head><body>Tidak ada data</body></html>');
        }
        $this->load->library("Excel/PHPExcel");
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("Toko1001")
                ->setLastModifiedBy("Toko1001")
                ->setTitle("Member Adira Applying Loan")
                ->setSubject("Member Adira Applying Loan")
                ->setDescription("Member Adira Applying Loan");

        $objPHPExcel->getActiveSheet()->setTitle("Adira customer");
        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'No')
                ->setCellValue('B1', 'Kode / No Referensi (Pesanan)')
                ->setCellValue('C1', 'Tgl Transaksi')
                ->setCellValue('D1', 'Jam Transaksi')
                ->setCellValue('E1', 'Nama Keday')
                ->setCellValue('F1', 'Nama')
                ->setCellValue('G1', 'Alamat Sesuai KTP')
                ->setCellValue('H1', 'Alamat Tinggal Sekarang')
                ->setCellValue('I1', 'Tgl Lahir')
                ->setCellValue('J1', 'No Telepon')
                ->setCellValue('K1', 'Email')
                ->setCellValue('L1', 'Terdaftar Nasabah')
                ->setCellValue('M1', 'No. KTP')
                ->setCellValue('N1', 'Kode Pos Wilayah')
                ->setCellValue('O1', 'Nama Kecamatan')
                ->setCellValue('P1', 'Nama Produk')
                ->setCellValue('Q1', 'Harga Barang')
                ->setCellValue('R1', 'Down Payment')
                ->setCellValue('S1', 'Jumlah Pinjaman')
                ->setCellValue('T1', 'Tenor');
        $add_width = 1;
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4.43 + $add_width);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20.57 + $add_width);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10 + $add_width);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(8.43 + $add_width);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8.43 + $add_width);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8.43 + $add_width);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(16.14 + $add_width);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(18.57 + $add_width);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10 + $add_width);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(9.29 + $add_width);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(18.43 + $add_width);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(8.43 + $add_width);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(11.29 + $add_width);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(8.43 + $add_width);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(10.86 + $add_width);
        $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(8.43 + $add_width);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(8.43 + $add_width);
        $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(8.43 + $add_width);
        $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(8.43 + $add_width);
        $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(8.43 + $add_width);
        $objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(26.25 + $add_width);

        $style_header = array(
            'borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '00000000'),
                ),
                'inside' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '00000000'),
                ),
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
        );
        $style_header_fill = array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'startcolor' => array(
                'rgb' => '99CCFF'
            ),
        );
        $objPHPExcel->getActiveSheet()->getStyle('A1:T1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A1:T1')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('A1:T1')->getFill()->applyFromArray($style_header_fill);
        $objPHPExcel->getActiveSheet()->getStyle('A1:T1')->getAlignment()->setWrapText(true);

        $counter = 2;
        foreach ($order_info as $key => $each_product_info) {
            $data->order_seq = $each_product_info->seq;
            $data->seq = $each_product_info->seq;
            $order_loan = parent::get_customer_simulation_agent($data);
            if (!isset($order_loan))
                continue;
            $data->customer_seq = $order_loan[0]->customer_seq;
            $customer_info = parent::get_customer_info($data);
            $sel_data = $this->M_agent_order_partner->get_data($data);

            $customer_name = $customer_info[0]->pic_name;
            $customer_identity_address = $customer_info[0]->identity_address;
            $customer_address = $customer_info[0]->address;
            $customer_birthday = date("n/j/Y", strtotime($customer_info[0]->birthday));
            $customer_phone_no = $customer_info[0]->phone_no;
            $customer_email = $customer_info[0]->email_address;
            $customer_identity_no = $customer_info[0]->identity_no;
            $customer_zip_code = $customer_info[0]->zip_code;
            $customer_district_name = $customer_info[0]->kecamatan;
            $customer_register = '-';
            $order_no = $each_product_info->order_no;
            $order_date = date('n/j/Y', strtotime($order_loan[0]->created_date));
            $order_time = date('G:i:s', strtotime($order_loan[0]->created_date));
            $product_name = $sel_data[1][0]->name;
            $dp = $order_loan[0]->dp;
            $loan_after_pay_dp = $order_loan[0]->total_loan;
            $agent_name = $each_product_info->name;
            $tenor = $order_loan[0]->tenor;
            $total_product_price = $order_loan[0]->product_price;

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue("A$counter", $counter - 1)
                    ->setCellValue("B$counter", $order_no)
                    ->setCellValue("C$counter", $order_date)
                    ->setCellValue("D$counter", $order_time)
                    ->setCellValue("E$counter", $agent_name)
                    ->setCellValue("F$counter", $customer_name)
                    ->setCellValue("G$counter", $customer_identity_address)
                    ->setCellValue("H$counter", $customer_address)
                    ->setCellValue("I$counter", $customer_birthday)
                    ->setCellValue("J$counter", $customer_phone_no)
                    ->setCellValue("K$counter", $customer_email)
                    ->setCellValue("L$counter", $customer_register)
                    ->setCellValue("M$counter", $customer_identity_no)
                    ->setCellValue("N$counter", $customer_zip_code)
                    ->setCellValue("O$counter", $customer_district_name)
                    ->setCellValue("P$counter", $product_name)
                    ->setCellValue("Q$counter", $total_product_price)
                    ->setCellValue("R$counter", $dp)
                    ->setCellValue("S$counter", $loan_after_pay_dp)
                    ->setCellValue("T$counter", $tenor);
            $counter++;
        }


        $objPHPExcel->setActiveSheetIndex(0);

        $file = 'CustomerApplyLoan';
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $file_dir = str_replace(CDN_IMAGE, "", AGENT_UPLOAD_EXCEL_CUSTOMER_DATA);
        $objWriter->save(str_replace('.php', '.xlsx', $file_dir . $file . '.xlsx'));
        redirect(base_url($file_dir . $file . '.xlsx'));
        die();
//        $file = 'CustomerApplyLoan-' . $order_no;
//        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//        $objWriter->save(str_replace('.php', '.xlsx', AGENT_UPLOAD_EXCEL_CUSTOMER_DATA . $file . '.xlsx'));
//        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
//        $objWriter->save(str_replace('.php', '.xls', AGENT_UPLOAD_EXCEL_CUSTOMER_DATA . $file . '.xls'));
// We'll be outputting an excel file
//        header('Content-type: application/vnd.ms-excel');
// It will be called file.xls
//        header('Content-Disposition: attachment; filename="' . $file . '.xls"');
// Write file to the browser
//        $objWriter->save('php://output');
//
//
// Redirect output to a client’s web browser (Excel2007)
//        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
//        header('Content-Disposition: attachment; filename="' . $file . '.xlsx"');
//        header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
//        header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
//        header('Expires: Mon, 05 Jan 2015 05:06:07 GMT'); // Date in the past
//        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
//        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
//        header('Pragma: public'); // HTTP/1.0
//        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//        $objWriter->save('php://output');
//        die();
    }

}

?>