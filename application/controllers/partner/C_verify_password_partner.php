<?php

require_once CONTROLLER_BASE_PARTNER;

class C_verify_password_partner extends controller_base_partner {

    public function __construct() {
        parent::__construct("", "", false);
    }

    function index() {
        $this->load->model('partner/M_verify_password_partner');        
        $this->load->model('admin/transaction/M_order_admin');
        $now = date('Y-m-d');

        $uri = $this->uri->segment(3);
        $params = new stdClass();
        $params->uri = $uri;
        $params->email_cd = PARTNER_FORGOT_PASSWORD;

        $security = $this->M_verify_password_partner->check_partner_log_security($params);
        
        try {
            if ($security) {
                throw new Exception(ERROR_VALIDATION_FORGOT_PASSWORD);
            } else {
                $data = $this->M_verify_password_partner->get_partner_log($params);

                if (!isset($data)){
                    redirect(base_url()."error_404");
                }    
                $date2 = date_create(date('Ymd'));

                $params_partner = new stdClass();
                $params_partner->user_id = parent::get_partner_user_id();
                $params_partner->ip_address = parent::get_ip_address();
                $params_partner->email = $data[0]->recipient_email;
                $params_partner->request_date = $data[0]->created_date;
                $params_partner->name = $data[0]->recipient_name;

                if (date_diff(date_create($params_partner->request_date), $date2)->d < 2) {
                    $new_password = parent::generate_random_alnum(8);
                    $params_email = new stdClass();
                    $params_email->user_id = parent::get_input_post("email");
                    $params_email->ip_address = parent::get_ip_address();
                    $params_email->code = PARTNER_FORGOT_PASSWORD_VERIFICATION;
                    $params_email->to_email = $params_partner->email;
                    $params_email->RECIPIENT_NAME = $params_partner->name;
                    $params_email->PASSWORD = $new_password;
                    $params_email->EMAIL = $params_partner->email;

                    $partner_seq = $this->M_verify_password_partner->get_partner_seq($params_partner);

                    $params_partner->new_pass = md5(md5($new_password));
                    $params_partner->type = FORGOT_PASSWORD_TYPE;
                    $params_partner->code = $uri;
                    $params_partner->old_pass = $partner_seq[0]->password;
                    $params_partner->partner_seq = $partner_seq[0]->seq;

                    $this->M_verify_password_partner->trans_begin();
                    $this->M_verify_password_partner->update_password($params_partner);
                    $this->M_verify_password_partner->add_partner_log_security($params_partner);
                    $this->M_verify_password_partner->trans_commit();
                    
                    $this->data[DATA_SUCCESS][SUCCESS] = true;
                    $this->data[DATA_SUCCESS][SUCCESS_MESSAGE][] = SUCCESS_VERIFY_FORGOT_PASSWORD;
                    parent::email_template($params_email);                    
                }else{
                    throw new Exception(ERROR_FORGOT_PASSWORD_EXPIRED);
                }
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_verify_password_partner->trans_rollback();
        }
        $this->load->view("partner/login_partner", $this->data);
    }


}

?>