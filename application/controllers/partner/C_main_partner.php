<?php

require_once CONTROLLER_BASE_PARTNER;

class C_main_partner extends controller_base_partner {
    
    private $data;
    

    public function __construct() {
        $this->data = parent::__construct();
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('partner/transaction/M_agent_order_partner');
    }

    public function index() {
        $this->get_data();
        $this->load->view('partner/main_partner', $this->data);
    }

    protected function get_data() {
        $params = new stdClass();
        $params->user_id = parent::get_partner_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->partner_seq = parent::get_partner_user_seq();
        try {
            $sel_data = $this->M_agent_order_partner->get_order_product($params);
            parent::set_data($this->data, $sel_data);
                        
        }catch(Exception $ex){
            parent::set_error($this->data, $ex);
        }        
    }

    public function sign_out() {
        
        unset($_SESSION[SESSION_PARTNER_UID]);
        unset($_SESSION[SESSION_PARTNER_UNAME]);
        unset($_SESSION[SESSION_PARTNER_USER_GROUP]);
        unset($_SESSION[SESSION_PARTNER_EMAIL]);
        unset($_SESSION[SESSION_PARTNER_SEQ]);
        unset($_SESSION[SESSION_PARTNER_IMAGE]);
        unset($_SESSION[SESSION_PARTNER_FORM_AUTH]);
        unset($_SESSION[SESSION_PARTNER_CSRF_TOKEN]);
        unset($_SESSION[SESSION_IP_ADDR]);
        unset($_SESSION[SESSION_PARTNER_LEFT_NAV]);
        
        $this->data[DATA_ERROR] = False;
        $this->load->view('partner/login_partner', $this->data);
    }
}

?>
