<?php

require_once CONTROLLER_BASE_PARTNER;

class C_group_agent_partner extends controller_base_partner {

    private $data;

    public function __construct() {

        $this->data = parent::__construct("PMST00004", "partner/group_agent");
        $this->initialize();
    }

    private function initialize() {
        $this->load->library('pagination');
        $this->load->model('partner/master/M_group_agent_partner');
        parent::register_event($this->data, ACTION_ADDITIONAL, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_SAVE_DELETE, "save_delete");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        $data_sel = new stdClass();
        if (isset($_GET["agent_group_name"])) {
            $data_sel->agent_group_name = $this->get_input_get("agent_group_name");
            $this->data[DATA_SELECTED][LIST_DATA][0] = $data_sel;
        }

        if (!$this->input->post()) {
            $this->data['breadcrumb'] = parent::get_breadcrumb($this->data);
            $this->load->view("partner/master/group_agent_partner", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    $this->data['breadcrumb'] = parent::get_breadcrumb($this->data);
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("partner/master/group_agent_partner", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("partner/master/group_agent_partner", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    public function search() {
        $filter = new stdClass;
        $filter->user_id = parent::get_partner_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post('start');
        $filter->length = parent::get_input_post('length');
        $filter->order = parent::get_input_post('order');
        $filter->column = parent::get_input_post('column');
        $filter->partner_seq = parent::get_partner_user_seq();
        $filter->agent_group_name = parent::get_input_post('agent_group_name');
        try {
            $list_data = $this->M_group_agent_partner->get_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );

        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->agent_group_seq,
                    "agent_group_seq" => $data_row->agent_group_seq,
                    "agent_group_name" => $data_row->agent_group_name,
                    "commission_fee_percent" => $data_row->commission_fee_percent,
                    "created_by" => $data_row->created_by,
                    "created_date" => parent::cdate($data_row->created_date, 1),
                    "modified_by" => $data_row->modified_by,
                    "modified_date" => parent::cdate($data_row->modified_date, 1));
                $output['aaData'][] = $row;
            }
        };

        echo json_encode($output);
        die();
    }

    protected function get_edit() {
        $selected = new stdClass();
        $selected->user_id = parent::get_partner_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->partner_seq = parent::get_partner_user_seq();
        $selected->key = parent::get_input_get("key");

        try {
            $sel_data = $this->M_group_agent_partner->get_data($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_update() {
        $params = new stdClass();
        $params->user_id = parent::get_partner_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->partner_seq = parent::get_partner_user_seq();
        $params->agent_group_seq = parent::get_input_post('agent_group_seq');
        $params->agent_group_name = parent::get_input_post('agent_group_name', TRUE, FILL_VALIDATOR, 'Nama Grup', $this->data);
        $params->commission_fee_percent = parent::get_input_post('commission_fee_percent', TRUE, PERCENTAGE_VALIDATOR, 'Komisi Agent', $this->data);
        $this->data[DATA_SELECTED][LIST_DATA][] = $params;
        if ($this->data[DATA_ERROR][ERROR] == FALSE) {
            try {
                $this->M_group_agent_partner->trans_begin();
                $this->M_group_agent_partner->save_update($params);
                $this->M_group_agent_partner->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_group_agent_partner->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_group_agent_partner->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_group_agent_partner->trans_rollback();
            }
        }
    }

    protected function save_add() {
        $params = new stdClass();
        $params->user_id = parent::get_partner_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->partner_seq = parent::get_partner_user_seq();
        $params->agent_group_name = parent::get_input_post('agent_group_name', TRUE, FILL_VALIDATOR, 'Nama Grup', $this->data);
        $params->commission_fee_percent = parent::get_input_post('commission_fee_percent', TRUE, PERCENTAGE_VALIDATOR, 'Komisi Agent', $this->data);
        $this->data[DATA_SELECTED][LIST_DATA][] = $params;
        if ($this->data[DATA_ERROR][ERROR] == FALSE) {
            try {
                $this->M_group_agent_partner->trans_begin();
                $this->M_group_agent_partner->save_add($params);
                $this->M_group_agent_partner->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_group_agent_partner->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_group_agent_partner->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_group_agent_partner->trans_rollback();
            }
        }
    }

    protected function save_delete() {
        $params = new stdClass();
        $params->user_id = parent::get_partner_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->partner_seq = parent::get_partner_user_seq();
        $params->key = parent::get_input_post("key");
        try {
            $this->M_group_agent_partner->trans_begin();
            $this->M_group_agent_partner->save_delete($params);
            $this->M_group_agent_partner->trans_commit();
            parent::set_json_success();
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_group_agent_partner->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_group_agent_partner->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_group_agent_partner->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        }
    }

}
