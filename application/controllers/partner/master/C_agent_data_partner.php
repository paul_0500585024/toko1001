<?php

set_include_path(get_include_path() . PATH_SEPARATOR . '../../../Classes/');
require_once APPPATH . 'libraries/Excel/PHPExcel/IOFactory.php';
require_once CONTROLLER_BASE_PARTNER;

class C_agent_data_partner extends controller_base_partner {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("PMST00002", "partner/agent_data");
        $this->initialize();
    }

    private function initialize() {
        $this->load->library('pagination');
        $this->load->library('slim');
        $this->load->model('partner/master/M_agent_data_partner');
        $this->load->model('component/M_dropdown_list');
        $this->load->library("Excel/PHPExcel");
        $this->load->model(LIVEVIEW . 'agent/profile/M_change_password_agent');

        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_ADDITIONAL, "get_ajax");
//        parent::register_event($this->data, ACTION_ADDITIONAL, "generate_password_agent");
//        if ($this->data[DATA_INIT] === true) {
        parent::fire_event($this->data);
//        }
    }

    public function index() {

        if (!$this->input->post()) {
            if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_ADD) {
                $this->get_edit();
            }
            $this->data['breadcrumb'] = parent::get_breadcrumb($this->data);
            $this->load->view("partner/master/agent_data_partner", $this->data);
        } else {

            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->input->method(false) == FORM_POST) {
                        $this->post_edit();
                    } else {
                        $this->get_edit();
                    }
                    $this->data['breadcrumb'] = parent::get_breadcrumb($this->data);
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("partner/master/agent_data_partner", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("partner/master/agent_data_partner", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE or $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_SEARCH;
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    if (!isset($_COOKIE[$this->data[DATA_AUTH][FORM_CD]])) {
                        redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                    } else {
                        redirect($_COOKIE[$this->data[DATA_AUTH][FORM_CD]]);
                    }
                }
            }
        }
    }

    /*
     * public function search() {
     *
     * $filter = new stdClass;
     * $filter->user_id = parent::get_partner_user_id();
     * $filter->ip_address = parent::get_ip_address();
     * $filter->start = parent::get_input_get("start");
     * $filter->length = parent::get_input_get("length") == '' ? 10 : parent::get_input_get("length");
     * $filter->order = parent::get_input_get("order") == '' ? 'desc' : parent::get_input_get("order");
     * $filter->column = parent::get_input_get("column") == '' ? 'modified_date' : parent::get_input_get("column");
     * $filter->email = parent::get_input_get("email");
     * $filter->name = parent::get_input_get("name");
     * $filter->status = parent::get_input_get("status");
     * $filter->partner_seq = $this->data[DATA_USER_PROFILE][USER_SEQ];
     * $filter->start = parent::get_input_get(START_OFFSET) == "" ? 0 : parent::get_input_get(START_OFFSET);
     * $filter->length = "8";
     * try {
     * $list_data = $this->M_agent_data_partner->get_list($filter);
     * parent::set_list_data($this->data, $list_data);
     * $this->display_page(base_url("partner/agent_new"), $filter->start, $list_data[0][0]->total_rec, $filter->length);
     * } catch (Exception $ex) {
     * parent::set_error($this->data, $ex);
     * }
     * // echo json_encode($output);
     * }
     */

    public function search() {

        $filter = new stdClass();
        $filter->user_id = parent::get_partner_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->length = parent::get_input_get("length") == '' ? 10 : parent::get_input_get("length");
        $filter->sort = parent::get_input_get("sort") == '' ? 'asc' : parent::get_input_get("sort");
        $filter->column_sort = parent::get_input_get("column_sort") == '' ? 'name' : parent::get_input_get("column_sort");
        $filter->email = parent::get_input_get("email");
        $filter->name = parent::get_input_get("name");
        $filter->status = parent::get_input_get("status");
        $filter->partner_seq = $this->data[DATA_USER_PROFILE][USER_SEQ];
        $filter->start = parent::get_input_get(START_OFFSET) == "" ? 0 : parent::get_input_get(START_OFFSET);
        $filter->length = "9";
        try {
            $list_data = $this->M_agent_data_partner->get_list($filter);
            parent::set_list_data($this->data, $list_data);
            $status_data_partner = $this->M_agent_data_partner->get_data_status($filter);
            parent::set_data($this->data, $status_data_partner);

            $this->data['filter_sort'] = "&sort=" . $filter->sort . "&column_sort=" . $filter->column_sort;
            $this->data['filter_agent_data'] = "?email=" . $filter->email . "&name=" . $filter->name;
            $this->display_page(base_url("partner/agent_data") . $this->data['filter_agent_data'] . '&status=' . $filter->status . $this->data['filter_sort'], $filter->start, $list_data[0][0]->total_rec, $filter->length);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        setcookie($this->data[DATA_AUTH][FORM_CD], base_url(uri_string() . "?" . $_SERVER["QUERY_STRING"]), time() + COOKIE_TIME, "/");
    }

    protected function get_edit() {
        $selected = new stdClass();
        $selected->user_id = parent::get_partner_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = parent::get_input_get("key");
        try {
            $sel_data[0] = $this->M_agent_data_partner->get_data($selected);
            $sel_data[1] = $this->M_agent_data_partner->get_data_address_by_agent_seq($selected);
//            $sel_data[2] = $this->M_agent_data_partner->get_partner_commision_data($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function post_edit() {
        $selected = new stdClass();
        $selected->user_id = parent::get_partner_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = parent::get_input_post("seq");
        try {
            $sel_data[0] = $this->M_agent_data_partner->get_data($selected);
            $sel_data[1] = $this->M_agent_data_partner->get_data_address_by_agent_seq($selected);
            $sel_data[2] = $this->M_agent_data_partner->get_partner_commision_data($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_add() {
        $params = new stdClass();
        $params->user_id = parent::get_partner_user_id();
        $params->ip_address = parent::get_ip_address();

        /* data info login */
        $params->info_login_email = parent::get_input_post("info_login_email", true, FILL_VALIDATOR, "Email", $this->data);
        $params->email = $params->info_login_email;
        $check_email = $this->M_agent_data_partner->check_user_exist($params);
        if (isset($check_email)) {
            $this->data[DATA_ERROR][ERROR] = true;
            $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_EMAIL_CHECK;
        }

        $params->info_login_password = parent::get_input_post("info_login_password", true, FILL_VALIDATOR, "Password", $this->data);
        if (strlen($params->info_login_password) < 8 || strlen($params->info_login_password) > 20) {
            $this->temp_data[DATA_ERROR][ERROR] = true;
            $this->temp_data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_VALIDATION_FILL_PASSWORD_LENGTH;
        } else {
            $params->info_login_password = md5(md5(parent::get_input_post("info_login_password")));
        }
        if (parent::get_input_post("info_login_account_status") == 'on') {
            $params->info_login_account_status = 'A';
        } else {
            $params->info_login_account_status = 'S';
        }

        /* data info agent */
        $params->info_agent_name = parent::get_input_post("info_agent_name", true, FILL_VALIDATOR, "Nama", $this->data);
        $params->info_agent_birthday = parent::get_input_post("info_agent_birthday", true, FILL_VALIDATOR, "Tanggal Lahir", $this->data);
        $params->info_agent_gender = parent::get_input_post("info_agent_gender", true, GENDER_VALIDATOR, "Jenis Kelamin", $this->data);
        $params->info_agent_mobile_phone = parent::get_input_post("info_agent_mobile_phone", true, FILL_VALIDATOR, "No Telepon", $this->data);
        $params->commision_pro_rate = parent::get_input_post("commision_pro_rate", true, PERCENTAGE_VALIDATOR, "Komisi", $this->data);

        /* data info bank */
        $params->info_bank_name = parent::get_input_post("info_bank_name", true, FILL_VALIDATOR, "Nama Bank", $this->data);
        $params->info_bank_branch = parent::get_input_post("info_bank_branch", true, FILL_VALIDATOR, "Cabang Bank", $this->data);
        $params->info_bank_acct_no = parent::get_input_post("info_bank_acct_no", true, FILL_VALIDATOR, "No. Rekening Bank", $this->data);
        $params->info_bank_acct_name = parent::get_input_post("info_bank_acct_name", true, FILL_VALIDATOR, "Nama Rekening Bank", $this->data);

        /* data address agent */
        $params->address_agent_uniqId = $this->input->post('address_agent_uniqId') == '' ? array() : $this->input->post('address_agent_uniqId');
        $params->address_agent_agent_seq = $this->input->post('address_agent_agent_seq') == '' ? array() : $this->input->post('address_agent_agent_seq');
        $params->address_agent_address_seq = $this->input->post('address_agent_address_seq') == '' ? array() : $this->input->post('address_agent_address_seq');
        $params->address_agent_alias = $this->input->post('address_agent_alias') == '' ? array() : $this->input->post('address_agent_alias');
        $params->address_agent_pic_name = $this->input->post('address_agent_pic_name') == '' ? array() : $this->input->post('address_agent_pic_name');
        $params->address_agent_address = $this->input->post('address_agent_address') == '' ? array() : $this->input->post('address_agent_address');
        $params->address_agent_phone_no = $this->input->post('address_agent_phone_no') == '' ? array() : $this->input->post('address_agent_phone_no');
        $params->address_agent_province_seq = $this->input->post('address_agent_province_seq') == '' ? array() : $this->input->post('address_agent_province_seq');
        $params->address_agent_province_name = $this->input->post('address_agent_province_name') == '' ? array() : $this->input->post('address_agent_province_name');
        $params->address_agent_city_seq = $this->input->post('address_agent_city_seq') == '' ? array() : $this->input->post('address_agent_city_seq');
        $params->address_agent_city_name = $this->input->post('address_agent_city_name') == '' ? array() : $this->input->post('address_agent_city_name');
        $params->address_agent_district_seq = $this->input->post('address_agent_district_seq') == '' ? array() : $this->input->post('address_agent_district_seq');
        $params->address_agent_district_name = $this->input->post('address_agent_district_name') == '' ? array() : $this->input->post('address_agent_district_name');
        $params->address_agent_zip_code = $this->input->post('address_agent_zip_code') == '' ? array() : $this->input->post('address_agent_zip_code');

        /* data commission agent */
//            $params->agent_commission_name = $this->input->post('agent_commission_name');
//            $params->agent_commission_email = $this->input->post('agent_commission_email');
//            $params->agent_commission_seq = is_array($this->input->post('agent_commission_seq')) ? $this->input->post('agent_commission_seq') : array();
//            $params->agent_commission_lvl_cat = is_array($this->input->post('agent_commission_lvl_cat')) ? $this->input->post('agent_commission_lvl_cat') : array();
//            $params->agent_commission_fee = $this->input->post('agent_commission_fee');

        /* data picture agent */
        $params->profile_img = "";
        $params->partner_seq = $this->data[DATA_USER_PROFILE][USER_SEQ];
        $params->image_name = "agent_photo_profil_img";
        $params->file_type = IMAGE_TYPE;
        $params->dimension = IMAGE_DIMENSION_LOGO;
        $params->file_name = "a_" . strtotime("now");
        $params->folder = AGENT_UPLOAD_IMAGE . '/';

        $file_name = $this->upload_file_image($params);
        $params->profile_img = $file_name;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_agent_data_partner->trans_begin();
                $this->M_agent_data_partner->save_add($params);
                $this->M_agent_data_partner->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_data_partner->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_data_partner->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_data_partner->trans_rollback();
            }
        }
        $check_email = $this->M_agent_data_partner->check_user_exist($params);
        $agent_seq = isset($check_email[0]->seq) ? $check_email[0]->seq : '';
        $params->agent_seq = $agent_seq;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_agent_data_partner->trans_begin();
                $counter = 0;
                $params_address = new stdClass();
                $params_address->user_id = parent::get_partner_user_id();
                $params_address->ip_address = parent::get_ip_address();
                $params_address->agent_seq = $agent_seq;
                $this->M_agent_data_partner->save_delete_agent_address_by_agent_seq($params_address);
                while ($counter < count($params->address_agent_uniqId)) {
                    $params_address = new stdClass();
                    $params_address->user_id = parent::get_partner_user_id();
                    $params_address->ip_address = parent::get_ip_address();
                    $params_address->agent_seq = $agent_seq;
                    $params_address->address_seq = $params->address_agent_address_seq[$counter];
                    $params_address->alias = $params->address_agent_alias[$counter];
                    $params_address->pic_name = $params->address_agent_pic_name[$counter];
                    $params_address->address = $params->address_agent_address[$counter];
                    $params_address->phone_no = $params->address_agent_phone_no[$counter];
                    $params_address->province_seq = $params->address_agent_province_seq[$counter];
                    $params_address->province_name = $params->address_agent_province_name[$counter];
                    $params_address->city_seq = $params->address_agent_city_seq[$counter];
                    $params_address->city_name = $params->address_agent_city_name[$counter];
                    $params_address->district_seq = $params->address_agent_district_seq[$counter];
                    $params_address->district_name = $params->address_agent_district_name[$counter];
                    $params_address->zip_code = $params->address_agent_zip_code[$counter];
                    $this->M_agent_data_partner->save_add_agent_address($params_address);
                    $counter++;
                }
                $this->M_agent_data_partner->trans_commit();
                $this->search();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_data_partner->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_data_partner->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_data_partner->trans_rollback();
            }
        }
    }

    private function delete_file_image() {
        $this->post_edit();
        $sel_data = $this->data[DATA_SELECTED][LIST_DATA][0];
        if ($sel_data[0]->profile_img != '') {
            if (file_exists(AGENT_UPLOAD_IMAGE . $sel_data[0]->profile_img)) {
                unlink(AGENT_UPLOAD_IMAGE . $sel_data[0]->profile_img);
            }
        }
    }

    private function upload_file_image($params_upload_file) {
        $retval = "";
        $image = $this->slim->getImages($params_upload_file->image_name);
        if (!empty($image[0])) {
            $ext = str_replace('image/', '', $image[0]["input"]["type"]);

            if (isset($params_upload_file->file_type)) {
                if ($params_upload_file->file_type != '') {
                    if (!in_array($ext, explode('|', $params_upload_file->file_type))) {
                        $this->data[DATA_ERROR][ERROR] = TRUE;
                        $this->data[DATA_ERROR][ERROR_MESSAGE][] = "File Types tidak tepat";
                    }
                    ;
                }
            }

            if (isset($params_upload_file->dimension)) {
                if ($params_upload_file->dimension != '') {
                    $dimension = (object) unserialize($params_upload_file->dimension);
                    $max_height = $dimension->max_height;
                    $max_width = $dimension->max_width;
                    $min_width = $dimension->min_width;
                    $min_height = $dimension->min_height;
                    if ($image[0]['output']['width'] < $min_width || $image[0]['output']['width'] > $max_width || $image[0]['output']['height'] < $min_height || $image[0]['output']['height'] > $max_height) {
                        $this->data[DATA_ERROR][ERROR] = TRUE;
                        $this->data[DATA_ERROR][ERROR_MESSAGE][] = "File Dimension tidak tepat";
                    }
                }
            }

            $this->slim->saveFile($image[0]['output']['data'], $params_upload_file->file_name . '.' . $ext, $params_upload_file->folder, false);
            $retval = $params_upload_file->file_name . '.' . $ext;
        } else {
            $retval = "";
        }
        return $retval;
    }

    public function save_update() {
        $params = new stdClass();
        $params->user_id = parent::get_partner_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->seq = parent::get_input_post("seq", true, FILL_VALIDATOR, "ID ", $this->data);
        $agent_seq = $params->seq;
        $params->agent_seq = $params->seq;

        /* data info login */
        $params->info_login_password = parent::get_input_post("info_login_password");
        if ($params->info_login_password != '') {
            if (strlen($params->info_login_password) < 8 || strlen($params->info_login_password) > 20) {
                $this->temp_data[DATA_ERROR][ERROR] = true;
                $this->temp_data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_VALIDATION_FILL_PASSWORD_LENGTH;
            } else {
                $params->info_login_password = md5(md5(parent::get_input_post("info_login_password")));
            }
        }
        if (parent::get_input_post("info_login_account_status") == 'on') {
            $params->info_login_account_status = 'A';
        } else {
            $params->info_login_account_status = 'S';
        }

        /* data info agent */
        $params->info_agent_name = parent::get_input_post("info_agent_name", true, FILL_VALIDATOR, "Nama", $this->data);
        $params->info_agent_birthday = parent::get_input_post("info_agent_birthday");
        $params->info_agent_gender = parent::get_input_post("info_agent_gender", true, GENDER_VALIDATOR, "Jenis Kelamin", $this->data);
        $params->info_agent_mobile_phone = parent::get_input_post("info_agent_mobile_phone");
        $params->commision_pro_rate = parent::get_input_post("commision_pro_rate", true, PERCENTAGE_VALIDATOR, "Komisi", $this->data);

        /* data info bank */
        $params->info_bank_name = parent::get_input_post("info_bank_name", true, FILL_VALIDATOR, "Nama Bank", $this->data);
        $params->info_bank_branch = parent::get_input_post("info_bank_branch", true, FILL_VALIDATOR, "Cabang Bank", $this->data);
        $params->info_bank_acct_no = parent::get_input_post("info_bank_acct_no", true, FILL_VALIDATOR, "No. Rekening Bank", $this->data);
        $params->info_bank_acct_name = parent::get_input_post("info_bank_acct_name", true, FILL_VALIDATOR, "Nama Rekening Bank", $this->data);

        /* data address agent */
        $params->address_agent_uniqId = $this->input->post('address_agent_uniqId') == '' ? array() : $this->input->post('address_agent_uniqId');
        $params->address_agent_agent_seq = $this->input->post('address_agent_agent_seq') == '' ? array() : $this->input->post('address_agent_agent_seq');
        $params->address_agent_address_seq = $this->input->post('address_agent_address_seq') == '' ? array() : $this->input->post('address_agent_address_seq');
        $params->address_agent_alias = $this->input->post('address_agent_alias') == '' ? array() : $this->input->post('address_agent_alias');
        $params->address_agent_pic_name = $this->input->post('address_agent_pic_name') == '' ? array() : $this->input->post('address_agent_pic_name');
        $params->address_agent_address = $this->input->post('address_agent_address') == '' ? array() : $this->input->post('address_agent_address');
        $params->address_agent_phone_no = $this->input->post('address_agent_phone_no') == '' ? array() : $this->input->post('address_agent_phone_no');
        $params->address_agent_province_seq = $this->input->post('address_agent_province_seq') == '' ? array() : $this->input->post('address_agent_province_seq');
        $params->address_agent_province_name = $this->input->post('address_agent_province_name') == '' ? array() : $this->input->post('address_agent_province_name');
        $params->address_agent_city_seq = $this->input->post('address_agent_city_seq') == '' ? array() : $this->input->post('address_agent_city_seq');
        $params->address_agent_city_name = $this->input->post('address_agent_city_name') == '' ? array() : $this->input->post('address_agent_city_name');
        $params->address_agent_district_seq = $this->input->post('address_agent_district_seq') == '' ? array() : $this->input->post('address_agent_district_seq');
        $params->address_agent_district_name = $this->input->post('address_agent_district_name') == '' ? array() : $this->input->post('address_agent_district_name');
        $params->address_agent_zip_code = $this->input->post('address_agent_zip_code') == '' ? array() : $this->input->post('address_agent_zip_code');

//        /* data commission agent */
//        $params->agent_commission_name = $this->input->post('agent_commission_name');
//        $params->agent_commission_email = $this->input->post('agent_commission_email');
//        $params->agent_commission_seq = is_array($this->input->post('agent_commission_seq')) ? $this->input->post('agent_commission_seq') : array();
//        $params->agent_commission_lvl_cat = is_array($this->input->post('agent_commission_lvl_cat')) ? $this->input->post('agent_commission_lvl_cat') : array();
//        $params->agent_commission_fee = $this->input->post('agent_commission_fee');

        /* data picture agent */
        $params->profile_img = "";
        $params->partner_seq = $this->data[DATA_USER_PROFILE][USER_SEQ];
        $params->image_name = "agent_photo_profil_img";
        $params->file_type = IMAGE_TYPE;
        $params->dimension = IMAGE_DIMENSION_LOGO;
        $params->file_name = "a_" . strtotime("now");
        $params->folder = AGENT_UPLOAD_IMAGE . '/';
        $params->seq = parent::get_input_post("seq", true, FILL_VALIDATOR, "ID ", $this->data);
        $this->delete_file_image($params);
        $file_name = $this->upload_file_image($params);
        $params->profile_img = $file_name;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_agent_data_partner->trans_begin();
                $this->M_agent_data_partner->save_update($params);
                $this->M_agent_data_partner->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_data_partner->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_data_partner->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_data_partner->trans_rollback();
            }
        }
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_agent_data_partner->trans_begin();
                $counter = 0;
                $params_address = new stdClass();
                $params_address->user_id = parent::get_partner_user_id();
                $params_address->ip_address = parent::get_ip_address();
                $params_address->agent_seq = $agent_seq;
                $this->M_agent_data_partner->save_delete_agent_address_by_agent_seq($params_address);
                while ($counter < count($params->address_agent_uniqId)) {
                    $params_address = new stdClass();
                    $params_address->user_id = parent::get_partner_user_id();
                    $params_address->ip_address = parent::get_ip_address();
                    $params_address->agent_seq = $agent_seq;
                    $params_address->address_seq = $params->address_agent_address_seq[$counter];
                    $params_address->alias = $params->address_agent_alias[$counter];
                    $params_address->pic_name = $params->address_agent_pic_name[$counter];
                    $params_address->address = $params->address_agent_address[$counter];
                    $params_address->phone_no = $params->address_agent_phone_no[$counter];
                    $params_address->province_seq = $params->address_agent_province_seq[$counter];
                    $params_address->province_name = $params->address_agent_province_name[$counter];
                    $params_address->city_seq = $params->address_agent_city_seq[$counter];
                    $params_address->city_name = $params->address_agent_city_name[$counter];
                    $params_address->district_seq = $params->address_agent_district_seq[$counter];
                    $params_address->district_name = $params->address_agent_district_name[$counter];
                    $params_address->zip_code = $params->address_agent_zip_code[$counter];
                    $this->M_agent_data_partner->save_add_agent_address($params_address);
                    $counter++;
                }
                $this->M_agent_data_partner->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_data_partner->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_data_partner->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_data_partner->trans_rollback();
            }
        }
    }

    public function get_ajax() {
        $type = parent::get_input_get('type');
        switch ($type) {
            case 'print_data_excel':
                $filter = new stdClass;
                $filter->user_id = parent::get_partner_user_id();
                $filter->ip_address = parent::get_ip_address();
                $filter->partner_seq = parent::get_partner_user_seq();
                $filter->email = parent::get_input_get("email");
                $filter->name = parent::get_input_get("name");
                $filter->status = parent::get_input_get("status");
                $filter->sort = parent::get_input_get("sort") == '' ? 'desc' : parent::get_input_get("sort");
                $filter->column_sort = parent::get_input_get("column_sort") == '' ? 'a.modified_date' : parent::get_input_get("column_sort");
                try {
                    $list_data = $this->M_agent_data_partner->get_list_export($filter);
                    foreach ($list_data as $data_row) {
                        $row = array("Email" => $data_row->email,
                            "Status" => parent::cstdes($data_row->status, STATUS_AGENT),
                            "Nama" => $data_row->name,
                            "Tanggal Lahir" => parent::cdate($data_row->birthday),
                            "Jenis Kelamin" => parent::cstdes($data_row->gender, STATUS_GENDER),
                            "No Telp" => $data_row->mobile_phone,
                            "Deposit Amount" => $data_row->deposit_amt,
                            "Alias Penerima" => $data_row->alias,
                            "Nama Penerima" => $data_row->pic_name,
                            "Alamat Penerima" => $data_row->address,
                            "No. Telp Penerima" => $data_row->phone_no,
                            "Propinsi Penerima" => $data_row->province,
                            "Kota / Kabupaten Penerima" => $data_row->city,
                            "Kecamatan Penerima" => $data_row->district,
                            "Kode Pos Penerima" => $data_row->zip_code,
                        );
                        $output[] = $row;
                    }
                    echo json_encode($output);
                    exit();
                } catch (Exception $ex) {
                    parent::set_error($this->data, $ex);
                    exit();
                }
                break;
            case 'agent_excel': {
                    $this->SaveAgentExcel();
                };
                break;
            default : {
                    $this->generate_password_agent();
                }break;
        }
    }

    protected function generate_password_agent() {
        $params = new stdClass();
        $params->user_id = parent::get_partner_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->agent_seq = parent::get_input_post("agent_seq");
        $params->seq = parent::get_input_post("agent_seq");
        $params->random_password = parent::generate_random_text(8);
        $params->new_password = md5(md5($params->random_password));
        $params->key = parent::get_input_post("agent_seq");

        $agent_data = $this->M_agent_data_partner->get_data($params);

        if ($params->random_password !== "" && $params->agent_seq != "") {
            try {

                $this->M_change_password_agent->trans_begin();
                $this->M_change_password_agent->save_update($params);
                $this->M_change_password_agent->trans_commit();
                $email = new stdClass();
                $email->code = GENERATE_PASSWORD_AGENT;
                $email->ip_address = $params->ip_address;
                $email->user_id = $params->user_id;
                $email->RECIPIENT_NAME = $agent_data[0]->name;
                $email->to_email = $agent_data[0]->email;
                $email->password = $params->random_password;
                $email->email = $agent_data[0]->email;
                parent::email_template($email);

                parent::set_json_success();
            } catch (Exception $ex) {
                parent::set_json_error($ex);
            }
        }
    }

    protected function SaveAgentExcel() {

        $finalFileName = parent::generate_random_alnum();
        $file = $_FILES['excel-Agent'];
        $real_name = $file['name'];
        $upload_destination = '.' . DIRECTORY_SEPARATOR . EXCEL_FOLDER_TEMPLATE . DIRECTORY_SEPARATOR;
        $ext = $ext = pathinfo($real_name, PATHINFO_EXTENSION);

        if ($ext == EXCEL_EXTENTION_NAME && isset($file)) {
            move_uploaded_file($file['tmp_name'], $upload_destination . DIRECTORY_SEPARATOR . TEMP_FOLDER_EXCEL . DIRECTORY_SEPARATOR . $finalFileName . '.' . EXCEL_EXTENTION_NAME);
            $inputFileName = realpath(EXCEL_FOLDER_TEMPLATE) . DIRECTORY_SEPARATOR . TEMP_FOLDER_EXCEL . DIRECTORY_SEPARATOR . $finalFileName . '.' . EXCEL_EXTENTION_NAME;
            $objReader = new PHPExcel_Reader_Excel2007();
            $objPHPExcel = $objReader->load($inputFileName);
            $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
            $total_data = count($sheetData);
            (array) $data_list_excel = [];
            $array = 0;
            for ($i = 2; $i <= $total_data; $i++) {
                if ($sheetData[$i][EXCEL_COLUMN_A] == null && $sheetData[$i][EXCEL_COLUMN_B] == null && $sheetData[$i][EXCEL_COLUMN_C] == null && $sheetData[$i][EXCEL_COLUMN_D] == null && $sheetData[$i][EXCEL_COLUMN_E] == null && $sheetData[$i][EXCEL_COLUMN_F] == null && $sheetData[$i][EXCEL_COLUMN_G] == null && $sheetData[$i][EXCEL_COLUMN_H] == null) {
                    break;
                } else {
                    $storeData = new stdClass();

                    $storeData->user_id = parent::get_partner_user_id();
                    $storeData->ip_address = parent::get_ip_address();
                    $storeData->info_login_email = htmlspecialchars($sheetData[$i][EXCEL_COLUMN_A]);
                    $storeData->info_agent_name = htmlspecialchars($sheetData[$i][EXCEL_COLUMN_B]);
                    $storeData->info_agent_birthday = date('Y-m-d');
                    $storeData->info_agent_gender = DEFAULT_MALE_GENDER;
                    $storeData->info_agent_mobile_phone = isset($sheetData[$i][EXCEL_COLUMN_C]) ? $sheetData[$i][EXCEL_COLUMN_C] : '';
                    $storeData->info_login_password = '';
                    $storeData->profile_img = '';
                    $storeData->partner_seq = parent::get_partner_user_seq();
                    $storeData->info_login_account_status = 'A';
                    $storeData->info_bank_name = isset($sheetData[$i][EXCEL_COLUMN_D]) ? htmlspecialchars($sheetData[$i][EXCEL_COLUMN_D]) : '';
                    $storeData->info_bank_branch = isset($sheetData[$i][EXCEL_COLUMN_E]) ? htmlspecialchars($sheetData[$i][EXCEL_COLUMN_E]) : '';
                    $storeData->info_bank_acct_no = isset($sheetData[$i][EXCEL_COLUMN_F]) ? htmlspecialchars($sheetData[$i][EXCEL_COLUMN_F]) : '';
                    $storeData->info_bank_acct_name = isset($sheetData[$i][EXCEL_COLUMN_G]) ? htmlspecialchars($sheetData[$i][EXCEL_COLUMN_G]) : '';
                    $storeData->commision_pro_rate = isset($sheetData[$i][EXCEL_COLUMN_H]) ? htmlspecialchars($sheetData[$i][EXCEL_COLUMN_H]) : '';

                    if ($storeData->info_login_email == null || !filter_var($storeData->info_login_email, FILTER_VALIDATE_EMAIL)) {
                        $this->data[DATA_ERROR][ERROR] = true;
                        $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_EMAIL_FORMAT . ', ' . CHECK_EXCEL_COLUMN . ' ' . EXCEL_COLUMN_A . $i;
                        break;
                    }

                    if ($storeData->info_agent_name == null || $storeData->info_agent_name == '') {
                        $this->data[DATA_ERROR][ERROR] = true;
                        $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_NAME_MUST_FILL . ', ' . CHECK_EXCEL_COLUMN . ' ' . EXCEL_COLUMN_B . $i;
                    }


                    if (!is_numeric($storeData->commision_pro_rate) OR $storeData->commision_pro_rate < 0 OR $storeData->commision_pro_rate > 100 OR $storeData->commision_pro_rate == null) {
                        $this->data[DATA_ERROR][ERROR] = true;
                        $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_COMMISION_MUST_FILL . ', ' . CHECK_EXCEL_COLUMN . ' ' . EXCEL_COLUMN_H . $i;
                        break;
                    }
                    $data_list_excel[$array] = $storeData;
                    $array = $array + 1;
                }
            }

            if ($this->data[DATA_ERROR][ERROR] === false) {
                try {
                    $this->M_agent_data_partner->trans_begin();
                    foreach ($data_list_excel as $dataAgentSave) {
                        $this->M_agent_data_partner->save_add($dataAgentSave);
                    }
                    $this->M_agent_data_partner->trans_commit();
                    $this->data[DATA_SUCCESS][SUCCESS] = true;
                    $this->data[DATA_SUCCESS][SUCCESS_MESSAGE][] = SUCCESS_SAVE_PAID;
                } catch (BusisnessException $ex) {
                    parent::set_error($this->data, $ex);
                    $this->M_agent_data_partner->trans_rollback();
                } catch (TechnicalException $ex) {
                    parent::set_error($this->data, $ex);
                    $this->M_agent_data_partner->trans_rollback();
                } catch (Exception $ex) {
                    parent::set_error($this->data, $ex);
                    $this->M_agent_data_partner->trans_rollback();
                }
            }
        } else {
            $this->data[DATA_ERROR][ERROR] = true;
            $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_EXCEL_FILE_NO_COMPATIBLE;
        }

        if (file_exists(realpath(EXCEL_FOLDER_TEMPLATE) . DIRECTORY_SEPARATOR . TEMP_FOLDER_EXCEL . DIRECTORY_SEPARATOR . $finalFileName . '.xlsx')) {
            unlink(realpath(EXCEL_FOLDER_TEMPLATE) . DIRECTORY_SEPARATOR . TEMP_FOLDER_EXCEL . DIRECTORY_SEPARATOR . $finalFileName . '.xlsx');
        }


        if ($this->data[DATA_ERROR][ERROR] == true) {
            $this->session->set_flashdata('error_paid', $this->data[DATA_ERROR][ERROR_MESSAGE][0]);
        } elseif ($this->data[DATA_SUCCESS][SUCCESS] == true) {
            $this->session->set_flashdata('succes_paid', $this->data[DATA_SUCCESS][SUCCESS_MESSAGE][0]);
        }

        redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
    }

}

?>