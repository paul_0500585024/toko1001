<?php

require_once CONTROLLER_BASE_PARTNER;

class C_customer_agent_data_partner extends controller_base_partner {

    private $data;

    public function __construct() {

        $this->data = parent::__construct("PMST00003", "partner/customer_agent_data");
        $this->initialize();
    }

    private function initialize() {
        $this->load->library('pagination');
        $this->load->model('partner/master/M_customer_agent_data_partner');
        $this->load->model('component/M_dropdown_list');

        parent::register_event($this->data, ACTION_ADDITIONAL, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_SAVE_DELETE, "save_delete");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        $data_sel = new stdClass();
        $this->data[PARTNER_NAME] = $this->M_dropdown_list->get_dropdown_partner_name();
        $this->data[CATEGORY_NAME] = $this->M_dropdown_list->get_dropdown_category_name_lvl();
        if (isset($_GET["identity_no"])) {
            $data_sel->identity_no = $this->get_input_get("identity_no");
            $data_sel->email_address = $this->get_input_get("email_address");
            $this->data[DATA_SELECTED][LIST_DATA][0] = $data_sel;
        }
        if (!$this->input->post()) {
            $this->data['breadcrumb'] = parent::get_breadcrumb($this->data);
            $this->load->view("partner/master/customer_agent_data", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    $this->data['breadcrumb'] = parent::get_breadcrumb($this->data);
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("partner/master/customer_agent_data", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("partner/master/customer_agent_data", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    public function search() {

        $filter = new stdClass;
        $filter->user_id = parent::get_partner_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->identity_no = parent::get_input_post("identity_no");
        $filter->email_address = parent::get_input_post("email_address");
        $filter->partner_seq = $filter->partner_seq = $this->data[DATA_USER_PROFILE][USER_SEQ];
        $filter->active = "on"; //parent::get_input_post("active");

        try {
            $list_data = $this->M_customer_agent_data_partner->get_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );

        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->seq,
                    "identity_no" => parent::cdef($data_row->identity_no),
                    "identity_address" => parent::cdef($data_row->identity_address),
                    "phone_no" => parent::cdef($data_row->phone_no),
                    "email_address" => parent::cdef($data_row->email_address),
                    "sub_district" => parent::cdef($data_row->sub_district),
                    "address" => parent::cdef($data_row->address),
                    "birthday" => parent::cdef($data_row->birthday),
                    "created_date" => parent::cdate($data_row->created_date, 1));
                $output['aaData'][] = $row;
            }
        };

        echo json_encode($output);
        die();
    }

   protected function get_edit() {

        $selected = new stdClass();
        $selected->user_id = parent::get_partner_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = parent::get_input_get("key");

        try {
            $sel_data = $this->M_customer_agent_data_partner->get_data($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }
}

?>