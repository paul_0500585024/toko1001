<?php

require_once CONTROLLER_BASE_PARTNER;

class C_agent_address_partner extends controller_base_partner {
    
    private $data;

    public function __construct() {
        $this->data = parent::__construct("PMST00001", "partner/agent_address_partner");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('partner/master/M_agent_address_partner');
        $this->load->model('component/M_dropdown_list');
        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_ADDITIONAL, "get_data_ajax");
        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    
    }

    public function index() {
        if (isset($this->data[DATA_SELECTED][LIST_DATA])) {
            $this->data[ADDRESS_SEQ] = parent::get_input_get('address_seq');
            $this->data[PROVINCE_NAME] = $this->M_dropdown_list->get_dropdown_province($this->data[DATA_SELECTED][LIST_DATA][0]->province_seq);
            $this->data[CITY_NAME] = $this->M_dropdown_list->get_dropdown_city_by_province($this->data[DATA_SELECTED][LIST_DATA][0]->province_seq);
            $this->data[DISTRICT_NAME] = $this->M_dropdown_list->get_dropdown_district_by_city($this->data[DATA_SELECTED][LIST_DATA][0]->city_seq);
            $this->data[DATA_ADDRESS] = DEFAULT_MODULE_SEQ;
            
            $data[ADDRESS_SEQ] = parent::get_input_get('address_seq');
            $data[PROVINCE_NAME] = $this->M_dropdown_list->get_dropdown_province($this->data[DATA_SELECTED][LIST_DATA][0]->province_seq);
            $data[CITY_NAME] = $this->M_dropdown_list->get_dropdown_city_by_province($this->data[DATA_SELECTED][LIST_DATA][0]->province_seq);
            $data[DISTRICT_NAME] = $this->M_dropdown_list->get_dropdown_district_by_city($this->data[DATA_SELECTED][LIST_DATA][0]->city_seq);
            $data[DATA_ADDRESS] = DEFAULT_MODULE_SEQ;
            $data[DATA_SELECTED] = $this->data[DATA_SELECTED];
        } else {
            $this->data[ADDRESS_SEQ] = "";
            $this->data[PROVINCE_NAME] = $this->M_dropdown_list->get_dropdown_province();
            $this->data[CITY_NAME] = "";
            $this->data[DISTRICT_NAME] = "";
            $this->data[DATA_ADDRESS] = DEFAULT_ROOT;
            
            $data[ADDRESS_SEQ] = "";
            $data[PROVINCE_NAME] = $this->M_dropdown_list->get_dropdown_province();
            $data[CITY_NAME] = "";
            $data[DISTRICT_NAME] = "";
            $data[DATA_ADDRESS] = DEFAULT_ROOT;
            $data[DATA_SELECTED] = ""; 
        }
        $this->data['agent_seq'] = parent::get_input_get('agent_seq');
        $data['agent_seq'] = parent::get_input_get('agent_seq');
        echo json_encode($data);
        exit();
        $this->load->view("partner/master/agent_address_partner", $this->data);
    }

    public function get_edit() {
        $selected = new stdClass();
        $selected->partner_seq = parent::get_partner_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->address_seq = parent::get_input_get('address_seq');
        $selected->agent_seq = parent::get_input_get('agent_seq');
        try {
            $sel_data = $this->M_agent_address_partner->get_data($selected);
            
            if (isset($sel_data[0])) {
                parent::set_data($this->data, $sel_data);
            }
        } catch ( Exception $ex ) {
            parent::set_error($this->data, $ex);
        }
    }

    public function save_add() {
        $params = new stdClass();
        $params->user_id = parent::get_partner_user_seq();
        $params->ip_address = parent::get_ip_address();
        $params->agent_seq = parent::get_input_post("agent_seq");
        $params->alias = parent::get_input_post("alias");
        $params->pic_name = parent::get_input_post("pic_name");
        $params->address = parent::get_input_post("address");
        $params->district_seq = parent::get_input_post("district_seq");
        $params->phone_no = parent::get_input_post("phone_no");
        $params->zip_code = parent::get_input_post("zip_code");
        $this->data[DATA_SELECTED][LIST_DATA][] = $params;
        
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_agent_address_partner->trans_begin();
                $this->M_agent_address_partner->save_add($params);
                $this->M_agent_address_partner->trans_commit();
                $this->data[DATA_SUCCESS][SUCCESS] = true;
            } catch ( BusisnessException $ex ) {
                parent::set_error($this->data, $ex);
                $this->M_agent_address_partner->trans_rollback();
            } catch ( TechnicalException $ex ) {
                parent::set_error($this->data, $ex);
                $this->M_agent_address_partner->trans_rollback();
            } catch ( Exception $ex ) {
                parent::set_error($this->data, $ex);
                $this->M_agent_address_partner->trans_rollback();
            }
        }
        redirect(base_url() . 'partner/agent_data?' . CONTROL_GET_TYPE . '=' . ACTION_EDIT . '&key=' . parent::get_input_post("agent_seq"));
    }

    public function save_update() {
        
        $params = new stdClass();
        $params->user_id = parent::get_input_post("agent_seq");
        $params->ip_address = parent::get_ip_address();
        $params->alias = parent::get_input_post("alias");
        $params->pic_name = parent::get_input_post("pic_name");
        $params->address = parent::get_input_post("address");
        $params->district_seq = parent::get_input_post("district_seq");
        $params->phone_no = parent::get_input_post("phone_no");
        $params->zip_code = parent::get_input_post("zip_code");
        $params->address_seq = parent::get_input_post("address_seq");
        
        $this->data[DATA_SELECTED][LIST_DATA][] = $params;
        
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_agent_address_partner->trans_begin();
                $this->M_agent_address_partner->save_update($params);
                $this->M_agent_address_partner->trans_commit();
                $this->data[DATA_SUCCESS][SUCCESS] = true;
            } catch ( BusisnessException $ex ) {
                parent::set_error($this->data, $ex);
                $this->M_agent_address_partner->trans_rollback();
            } catch ( TechnicalException $ex ) {
                parent::set_error($this->data, $ex);
                $this->M_agent_address_partner->trans_rollback();
            } catch ( Exception $ex ) {
                parent::set_error($this->data, $ex);
                $this->M_agent_address_partner->trans_rollback();
            }
        }
        redirect(base_url() . 'partner/agent_data?' . CONTROL_GET_TYPE . '=' . ACTION_EDIT . '&key=' . parent::get_input_post("agent_seq"));
    }

    public function save_delete() {
        $params = new stdClass();
        $params->member_seq = parent::get_member_user_seq();
        $params->ip_address = parent::get_ip_address();
        $params->address_seq = parent::get_input_post("address_seq");
        
        try {
            $this->M_agent_address_partner->trans_begin();
            $this->M_agent_address_partner->save_delete($params);
            $this->M_agent_address_partner->trans_commit();
        } catch ( BusisnessException $ex ) {
            parent::set_error($this->data, $ex);
            $this->M_agent_address_partner->trans_rollback();
        } catch ( TechnicalException $ex ) {
            parent::set_error($this->data, $ex);
            $this->M_agent_address_partner->trans_rollback();
        } catch ( Exception $ex ) {
            parent::set_error($this->data, $ex);
            $this->M_agent_address_partner->trans_rollback();
        }
        
        redirect(base_url() . 'partner/agent_data?' . CONTROL_GET_TYPE . '=' . ACTION_EDIT . '&key=' . parent::get_input_post("agent_seq"));
    }

    public function get_data_ajax() {
        $type = parent::get_input_get("type");
        switch ($type) {
            case TASK_PROVINCE_CHANGE :
                $this->get_city_by_province();
                break;
            case TASK_CITY_CHANGE :
                $this->get_district_by_city();
                break;
            case 'task_province_city_district_change':
                $this->get_province_city_district();
                break;
        }
    }

    protected function get_city_by_province() {
        
        $province_seq = parent::get_input_get("province_seq");
        try {
            $list_data = $this->M_dropdown_list->get_dropdown_city_by_province($province_seq);
        } catch ( Exception $ex ) {
            parent::set_error($this->data, $ex);
        }
        echo json_encode($list_data);
        die();
    }

    protected function get_district_by_city() {
        $city_seq = parent::get_input_get("city_seq");
        
        try {
            $list_data = $this->M_dropdown_list->get_dropdown_district_by_city($city_seq);
        } catch ( Exception $ex ) {
            parent::set_error($this->data, $ex);
        }
        echo json_encode($list_data);
        die();
    }
    
    protected function get_province_city_district() {
        $province_seq = parent::get_input_get("province_seq");
        $city_seq = parent::get_input_get("city_seq");
        $district_seq = parent::get_input_get("district_seq");
        try {            
            $list_data[PROVINCE_NAME] = $this->M_dropdown_list->get_dropdown_province();
            $list_data[CITY_NAME] = $this->M_dropdown_list->get_dropdown_city_by_province($province_seq);
            $list_data[DISTRICT_NAME] = $this->M_dropdown_list->get_dropdown_district_by_city($city_seq);
        } catch ( Exception $ex ) {
            parent::set_error($this->data, $ex);
        }
        echo json_encode($list_data);
        die();
    }

}

?>