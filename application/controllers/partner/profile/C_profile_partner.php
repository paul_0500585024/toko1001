<?php

require_once CONTROLLER_BASE_PARTNER;

class C_profile_partner extends controller_base_partner {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("PCON00003", "partner/profile", TRUE);
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('partner/profile/M_info_partner');
        $this->load->model('admin/master/M_partner_admin');
        $this->load->model('component/M_dropdown_list');

        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_ADDITIONAL, "get_ajax");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        $this->data[PROVINCE_NAME] = $this->M_dropdown_list->get_dropdown_province();
        $this->data[EXPEDITION_NAME] = $this->M_dropdown_list->get_dropdown_expedition();
        $this->data[CITY_NAME] = "";
        $this->data['breadcrumb'] = parent::get_breadcrumb($this->data);
        if (!$this->input->post()) {
            $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
            $this->get_edit();
            $this->load->view("partner/profile/profile_partner", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("partner/profile/profile_partner", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $partner_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($partner_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq));
                }
            }
        }
    }

    protected function get_ajax() {
        $tipe = parent::get_input_post("tipe");
        $idh = parent::get_input_post("idh");
        switch ($tipe) {
            case "city":
                $this->get_dropdown_city($idh);
                break;
            case "district":
                $this->get_dropdown_district($idh);
                break;
            case "valpcd":
                $valpcd = $this->M_partner_admin->get_val_pcd($idh);
                die(json_encode($valpcd));
                break;
            case "email":
                if ($idh != '') {
                    $msg = "OK";
                    $list_data = $this->M_partner_admin->get_val_email($idh);
                    if (isset($list_data[0])) {
                        $msg = "ERROR";
                    }
                } else {
                    $msg = "ERROR";
                }
                die($msg);
                break;
        }
    }

    protected function get_dropdown_city($province_seq) {
        try {
            $this->data[CITY_NAME] = $this->M_dropdown_list->get_dropdown_city_by_province($province_seq);
            die(json_encode($this->data[CITY_NAME]));
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function get_dropdown_district($city_seq) {
        try {
            $list_data = $this->M_dropdown_list->get_dropdown_district_by_city($city_seq);
            die(json_encode($list_data));
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function get_edit() {
        $selected = new stdClass();
        $selected->user_id = parent::get_partner_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->partner_seq = parent::get_partner_user_seq();

        try {
            $sel_data = $this->M_info_partner->get_data($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            } else {
                $this->load->view("errors/html/error_404");
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->load->view("errors/html/error_404");
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_update() {

        $params = new stdClass();
        $params->user_id = parent::get_partner_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->partner_seq = parent::get_partner_user_id();
        $params->phone_no = parent::get_input_post("phone_no", true, FILL_VALIDATOR, "No. Telp", $this->data);

        $params->bank_name = parent::get_input_post("bank_name", true, FILL_VALIDATOR, "Nama Bank", $this->data);
        $params->bank_branch_name = parent::get_input_post("bank_branch_name", true, FILL_VALIDATOR, "Cabang", $this->data);
        $params->bank_acct_no = parent::get_input_post("bank_acct_no", true, FILL_VALIDATOR, "No. Rekening", $this->data);
        $params->bank_acct_name = parent::get_input_post("bank_acct_name", true, FILL_VALIDATOR, "Rekening A/N", $this->data);
        $params->partner_seq = parent::get_partner_user_seq();
        $params->status = parent::get_input_post("old_status");
        $params->created_date = parent::get_input_post("old_created_date");
        $params->modified_date = parent::get_input_post("old_modified_date");
        $sel_data = $this->M_info_partner->get_data($params);

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                if (strlen(parent::get_input_post("phone_no")) > MAX_PHONE_LENGTH) {
                    throw new Exception("No. Telp " . ERROR_VALIDATION_LENGTH_MAX . MAX_PHONE_LENGTH);
                }
                if (strlen(parent::get_input_post("pic1_name")) > MAX_NAME_LENGTH) {
                    throw new Exception("Nama PIC " . ERROR_VALIDATION_LENGTH_MAX . MAX_NAME_LENGTH);
                }
                if (strlen(parent::get_input_post("bank_name")) > MAX_BANK_LENGTH) {
                    throw new Exception("Nama Bank " . ERROR_VALIDATION_LENGTH_MAX . MAX_BANK_LENGTH);
                }
                if (strlen(parent::get_input_post("bank_branch_name")) > MAX_BANK_BRANCE_LENGTH) {
                    throw new Exception("Cabang " . ERROR_VALIDATION_LENGTH_MAX . MAX_BANK_BRANCE_LENGTH);
                }
                if (strlen(parent::get_input_post("bank_acct_no")) > MAX_LENGTH_DEFAULT) {
                    throw new Exception("No.Rekening " . ERROR_VALIDATION_LENGTH_MAX . MAX_LENGTH_DEFAULT);
                }
                if (strlen(parent::get_input_post("bank_acct_name")) > MAX_LENGTH_DEFAULT) {
                    throw new Exception("Rekening A/N " . ERROR_VALIDATION_LENGTH_MAX . MAX_LENGTH_DEFAULT);
                }
                $this->M_info_partner->trans_begin();
                $this->M_info_partner->save_update($params);
                $this->M_info_partner->trans_commit();
                $this->data[DATA_SUCCESS][SUCCESS] = true;
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_info_partner->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_info_partner->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_info_partner->trans_rollback();
            }
        }
    }

}
