<?php

require_once CONTROLLER_BASE_PARTNER;

class C_change_picture_partner extends controller_base_partner {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("PCON00002", "partner/change_picture", TRUE);
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('partner/profile/M_change_picture_partner');
        $this->load->model('component/M_dropdown_list');
        $this->load->library('slim');

        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        $this->data['breadcrumb'] = parent::get_breadcrumb($this->data);
        if (!$this->input->post()) {
            $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
            $this->get_edit();
            $this->load->view("partner/profile/change_picture_partner", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("partner/profile/change_picture_partner", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("partner/profile/change_picture_partner", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    protected function get_edit() {

        $selected = new stdClass();
        $selected->user_id = parent::get_partner_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = parent::get_partner_user_seq();

        try {
            $sel_data = $this->M_change_picture_partner->get_data($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_update() {
        $params_upload_file = new stdClass();
        $params_upload_file->user_id = parent::get_partner_user_id();
        $params_upload_file->ip_address = parent::get_ip_address();
        $params_upload_file->key = $this->data[DATA_USER_PROFILE][USER_SEQ];
        $params_upload_file->folder = PARTNER_LOGO . $this->data[DATA_USER_PROFILE][USER_SEQ] . '/';
        $params_upload_file->image_name = 'profil_img';
        $params_upload_file->file_name = $this->data[DATA_USER_PROFILE][USER_SEQ] . strtotime("now");

        $params_upload_file->file_type = IMAGE_TYPE;
        $params_upload_file->dimension = IMAGE_DIMENSION_LOGO;
        $params_upload_file->error_name = 'Foto Profil';
        $file_name = $this->upload_file_image($params_upload_file);

        $params = new stdClass();
        $params->userid = parent::get_partner_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->old_profile_img = parent::get_input_post("profile_img");
        $params->seq = parent::get_partner_user_seq();
        $params->profile_img = $file_name;
        $params->name = parent::get_input_post("name");
        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_change_picture_partner->trans_begin();
                $this->M_change_picture_partner->save_update($params);
                $this->M_change_picture_partner->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_change_picture_partner->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_change_picture_partner->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_change_picture_partner->trans_rollback();
            }
        }
    }

    private function upload_file_image($params_upload_file) {
        $retval = "";
        $image = $this->slim->getImages($params_upload_file->image_name);
        if (!empty($image[0])) {
            $ext = str_replace('image/', '', $image[0]["input"]["type"]);

            if (isset($params_upload_file->file_type)) {
                if ($params_upload_file->file_type != '') {
                    if (!in_array($ext, explode('|', $params_upload_file->file_type))) {
                        $this->data[DATA_ERROR][ERROR] = TRUE;
                        $this->data[DATA_ERROR][ERROR_MESSAGE][] = "File Types tidak tepat";
                    };
                }
            }

            if (isset($params_upload_file->dimension)) {
                if ($params_upload_file->dimension != '') {
                    $dimension = (object) unserialize($params_upload_file->dimension);
                    $max_height = $dimension->max_height;
                    $max_width = $dimension->max_width;
                    $min_width = $dimension->min_width;
                    $min_height = $dimension->min_height;
                    if ($image[0]['output']['width'] < $min_width || $image[0]['output']['width'] > $max_width || $image[0]['output']['height'] < $min_height || $image[0]['output']['height'] > $max_height) {
                        $this->data[DATA_ERROR][ERROR] = TRUE;
                        $this->data[DATA_ERROR][ERROR_MESSAGE][] = "File Dimension tidak tepat";
                    }
                }
            }

            $this->slim->saveFile($image[0]['output']['data'], $params_upload_file->file_name . '.' . $ext, $params_upload_file->folder, false);
            $retval = $params_upload_file->file_name . '.' . $ext;
        } else {
            $retval = "";
        }

        $data = $this->M_change_picture_partner->get_data($params_upload_file);
        if ($data[0]->profile_img != "") {
            if (file_exists($params_upload_file->folder . $data[0]->profile_img)) {
                unlink($params_upload_file->folder . '/' . $data[0]->profile_img);
            }
        }
        return $retval;
        
    }

}
