<?php

require_once CONTROLLER_BASE_PARTNER;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Bank Account
 *
 * @author Jartono
 */
class C_partner_down_payment_partner extends controller_base_partner {

    private $data;

    public function __construct() {

        $this->data = parent::__construct("PCST00002", "partner/partner_down_payment");
        $this->initialize();
    }

    private function initialize() {

        $this->load->model('partner/loan/M_partner_down_payment_partner');
        $this->load->model('component/M_dropdown_list');

        parent::register_event($this->data, ACTION_ADDITIONAL, "get_ajax_loading");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_SAVE_DELETE, "save_delete");
//        parent::register_event($this->data, ACTION_ADDITIONAL, "get_dropdown_category_on_change");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        $data_sel = new stdClass();
        $this->data[CATEGORY_LOAN] = $this->M_dropdown_list->get_dropdown_partner_dp_category_by_seq(parent::get_partner_user_seq());
        $this->data[PARTNER_NAME] = $this->M_dropdown_list->get_dropdown_partner_dp_get_partner_name();
        if (isset($_GET["product_category_seq"])) {
            $data_sel->product_category_seq = $this->get_input_get("product_category_seq");
            $this->data[DATA_SELECTED][LIST_DATA][0] = $data_sel;
        }


        if (!$this->input->post()) {
            $this->data['breadcrumb'] = parent::get_breadcrumb($this->data);
            $this->load->view("partner/loan/partner_down_payment_partner", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    $this->data['breadcrumb'] = parent::get_breadcrumb($this->data);
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->data[TENOR_LOAN] = $this->M_dropdown_list->get_dropdown_partner_dp_tenor_by_category($this->data[DATA_SELECTED][LIST_DATA][0]->product_category_seq);
                        $this->load->view("partner/loan/partner_down_payment_partner", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->data[TENOR_LOAN] = $this->M_dropdown_list->get_dropdown_partner_dp_tenor_by_category($this->data[DATA_SELECTED][LIST_DATA][0]->product_category_seq);
                        $this->load->view("partner/loan/partner_down_payment_partner", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    public function search() {

        $filter = new stdClass;
        $filter->user_id = parent::get_partner_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->category_loan = parent::get_input_post("category_loan");
        $filter->tenor_loan = parent::get_input_post("tenor_loan");
        $filter->partner_seq = parent::get_partner_user_seq();
        $filter->product_category_seq = parent::get_input_post("product_category_seq");
        $filter->partner_loan_seq = parent::get_input_post("partner_loan_seq");
        $filter->active = "on";

        try {
            $list_data = $this->M_partner_down_payment_partner->get_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );

        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->seq,
                    "category_loan" => parent::cdef($data_row->category_loan),
                    "tenor_loan" => parent::cdef($data_row->tenor_loan),
                    "minimal_loan" => parent::cnum($data_row->minimal_loan),
                    "maximal_loan" => parent::cnum($data_row->maximal_loan),
                    "min_dp_percent" => parent::cdef($data_row->min_dp_percent),
                    "active" => parent::cstat($data_row->active),
                    "created_by" => $data_row->created_by,
                    "created_date" => parent::cdate($data_row->created_date, 1),
                    "modified_by" => $data_row->modified_by,
                    "modified_date" => parent::cdate($data_row->modified_date, 1));
                $output['aaData'][] = $row;
            }
        };

        echo json_encode($output);
        die();
    }

    protected function get_edit() {


        $selected = new stdClass();
        $selected->user_id = parent::get_partner_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = parent::get_input_get("key");

        try {
            $sel_data = $this->M_partner_down_payment_partner->get_data($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
            $this->data[CATEGORY_LOAN] = $this->M_dropdown_list->get_dropdown_partner_dp_category_by_seq(parent::get_partner_user_seq());
            $this->data[TENOR_LOAN] = $this->M_dropdown_list->get_dropdown_partner_dp_tenor_by_category($sel_data[0]->partner_loan_seq);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_add() {

        $params = new stdClass();
        $params->user_id = parent::get_partner_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->partner_seq = parent::get_partner_user_seq();
        $params->product_category_seq = parent::get_input_post("product_category_seq", true, FILL_VALIDATOR, "Category", $this->data);
        $params->partner_loan_seq = parent::get_input_post("partner_loan_seq", true, FILL_VALIDATOR, "Toner", $this->data);
        $params->minimal_loan = parent::get_input_post("minimal_loan", true, FILL_VALIDATOR, "Minimal Loan", $this->data);
        $params->maximal_loan = parent::get_input_post("maximal_loan", true, FILL_VALIDATOR, "Maximal Loan", $this->data);
        $params->min_dp_percent = parent::get_input_post("min_dp_percent", true, FILL_VALIDATOR, "Minimal DP", $this->data);
        $params->active = parent::get_input_post("active");

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_partner_down_payment_partner->trans_begin();
                $this->M_partner_down_payment_partner->save_add($params);
                $this->M_partner_down_payment_partner->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_partner_down_payment_partner->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_partner_down_payment_partner->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_partner_down_payment_partner->trans_rollback();
            }
        }
    }

    protected function save_update() {

        $params = new stdClass();
        $params->user_id = parent::get_partner_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->seq = parent::get_input_post("seq");
        $params->product_category_seq = parent::get_input_post("product_category_seq", true, FILL_VALIDATOR, "Category", $this->data);
        $params->partner_loan_seq = parent::get_input_post("partner_loan_seq", true, FILL_VALIDATOR, "Tenor", $this->data);
        $params->minimal_loan = parent::get_input_post("minimal_loan", true, FILL_VALIDATOR, "Minimal Loan", $this->data);
        $params->maximal_loan = parent::get_input_post("maximal_loan", true, FILL_VALIDATOR, "Maximal Loan", $this->data);
        $params->min_dp_percent = parent::get_input_post("min_dp_percent", true, FILL_VALIDATOR, "Minimal DP", $this->data);
        $params->active = parent::get_input_post("active");

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_partner_down_payment_partner->trans_begin();
                $this->M_partner_down_payment_partner->save_update($params);
                $this->M_partner_down_payment_partner->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_partner_down_payment_partner->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_partner_down_payment_partner->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_partner_down_payment_partner->trans_rollback();
            }
        }
    }

    protected function save_delete() {
        
        $params = new stdClass();

        $params->user_id = parent::get_partner_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = parent::get_input_post("key");

        try {
            $this->M_partner_down_payment_partner->trans_begin();
            $this->M_partner_down_payment_partner->save_delete($params);
            $this->M_partner_down_payment_partner->trans_commit();
            parent::set_json_success();
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_partner_down_payment_partner->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_partner_down_payment_partner->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_partner_down_payment_partner->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        }
    }

    protected function get_ajax_loading() {

        $type= parent::get_input_post("type");
        if ($type === "") {
            $this->get_dropdown_category_on_change();
        } else {
            $this->search();
        }
    }

    protected function get_dropdown_category_on_change() {
        $product_category_seq = parent::get_input_post("product_category_seq");

        try {
            $sel_data = $this->M_dropdown_list->get_dropdown_partner_dp_tenor_by_category($product_category_seq);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        echo json_encode($sel_data);
        die();
    }

}

?>