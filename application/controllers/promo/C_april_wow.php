<?php

require_once CONTROLLER_BASE_HOME;

class C_april_wow extends controller_base_home {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("", "april-wow", false);
        $this->data[DATA_AUTH][FORM_URL] = 'april-wow';
        $this->load->model('home/M_thumbs_new_products');
        $this->load->library('pagination');
    }

    public function index() {
        $this->data[APRIL_WOW] = '';
        $this->data[TREE_CATEGORY] = $this->get_tree_category(true);
        $this->data[DATA_AUTH][FORM_TITLE] = $this->create_title('April Wow');
        $this->template->view('promo/april-wow', $this->data);
    }

    public function t_shirt() {
        $where = 'WHERE pv.active = 1 AND pv.status IN ("L","C") and pv.seq in ("2448","2445","2444","2443","2442","2436","2435","2434","2433",
                "2432","2231","2437","2227","2226","2225","2224","2223","2220","2219","2181","2179")';
        $this->get_data_product($where);
        $this->data[APRIL_WOW] = TASK_CATEOGRY;
        $this->data[DATA_AUTH][FORM_AUTH][FORM_TITLE] = true;
        $this->data[DATA_AUTH][FORM_TITLE] = $this->create_title('Pakaian Pria - April Wow');
        $this->data[BREADCRUMB_PAGES] = 'Pakaian Pria';
        $this->template->view('promo/april-wow', $this->data);
    }

    public function electronic() {
        $where = 'WHERE pv.active = 1 AND pv.status IN ("L","C") and pv.seq in ("2817","2815","2668","2665","2631","1564","1563","1562","1561",
                "3025","3024","2930","2929","2927","2926","2924","2920","2910","2909","2907","2902","2900","2896","2859","2844",
                "2843","2842","2841","2839","2838","2837","2836")';
        $this->get_data_product($where);
        $this->data[APRIL_WOW] = TASK_CATEOGRY;
        $this->data[TREE_CATEGORY] = $this->get_tree_category(true);
        $this->data[DATA_AUTH][FORM_AUTH][FORM_TITLE] = true;
        $this->data[DATA_AUTH][FORM_TITLE] = $this->create_title('Elektronik - April Wow');
        $this->data[BREADCRUMB_PAGES] = 'Elektronik';
        $this->template->view('promo/april-wow', $this->data);
    }

    public function woman_bag() {
        $where = 'WHERE pv.active = 1 AND pv.status IN ("L","C") and pv.seq in ("2694","2583","2582","2534","2532","2536","2530","2527","2526",
                "2520","2519","2518","2517","2516","2515","2514","2513","2512","2507","2500","2498","2495","2055","2053","2051",
                "2048","2046","2045","2044","2037","2033","2032","2031","2030","2029","2028","2027","2026","2025","1680","1679","1678",
                "1677","1676","1675","1673","1565","2584")';
        $this->get_data_product($where);
        $this->data[APRIL_WOW] = TASK_CATEOGRY;
        $this->data[TREE_CATEGORY] = $this->get_tree_category(true);
        $this->data[DATA_AUTH][FORM_AUTH][FORM_TITLE] = true;
        $this->data[DATA_AUTH][FORM_TITLE] = $this->create_title('Tas Wanita - April Wow');
        $this->data['breadcrumb_pages'] = 'Tas Wanita';
        $this->template->view('promo/april-wow', $this->data);
    }

    public function wristwatch() {
        $where = 'WHERE pv.active = 1 AND pv.status IN ("L","C") and pv.seq in ("3028","3021","3020","3019","3018","3017","3016","3015","3014",
                "3013","3005","3004","3003","2998","2992","2991","2984","2982","2978","2977","2976","2974","2652","2643","2642",
                "2641","2638","2618","2615","2614","2613","2611","2610","2609","2607","2606","2603","2602","2600")';
        $this->get_data_product($where);
        $this->data[APRIL_WOW] = TASK_CATEOGRY;
        $this->data[TREE_CATEGORY] = $this->get_tree_category(true);
        $this->data[DATA_AUTH][FORM_AUTH][FORM_TITLE] = true;
        $this->data[DATA_AUTH][FORM_TITLE] = $this->create_title('Jam Tangan - April Wow');
        $this->data[BREADCRUMB_PAGES] = 'Jam Tangan';
        $this->template->view('promo/april-wow', $this->data);
    }

    public function toys() {
        $where = 'WHERE pv.active = 1 AND pv.status IN ("L","C") and pv.seq in ("2804","2802","2754","2735","2688","2686","2684","2683","2682",
                "2681","2680","2679","2568","2556","2555","2554","2546","2545","2544","2543","2259")';
        $this->get_data_product($where);
        $this->data[APRIL_WOW] = TASK_CATEOGRY;
        $this->data[TREE_CATEGORY] = $this->get_tree_category(true);
        $this->data[DATA_AUTH][FORM_AUTH][FORM_TITLE] = true;
        $this->data[DATA_AUTH][FORM_TITLE] = $this->create_title('Mainan Buah Hati - April Wow');
        $this->data[BREADCRUMB_PAGES] = 'Mainan Buah Hati';
        $this->template->view('promo/april-wow', $this->data);
    }

    private function get_data_product($data = "") {
        $price_range = $this->input->get(PRICE_RANGE, TRUE);
        if ($price_range != FALSE AND ! preg_match("/^[0-9]+,[0-9]+$/", $price_range)) {
            redirect($this->current_url_with_query_string(array(PRICE_RANGE)));
        }

        $order_category = $this->input->get(ORDER_CATEGORY, TRUE);
        $order_category_list = array(NEW_TO_OLD_PRODUCT, OLD_TO_NEW_PRODUCT, PRICE_EXPENSIVE_TO_CHEAP, PRICE_CHEAP_TO_EXPENSIVE);
        if ($order_category != FALSE AND ! in_array($order_category, $order_category_list)) {
            redirect($this->current_url_with_query_string(array(ORDER_CATEGORY)));
        }

        $condition_where = function()use($price_range) {
            $where = '';
            if ($price_range != '') {
                $price = explode(',', $price_range);
                $where .= 'AND sell_price BETWEEN ' . $price[0] . ' AND ' . $price[1];
            }
            return $where;
        };


        $condition_order = function()use($order_category) {
            $order = '';
            $order_data = [
                'no' => 'ORDER BY pv.created_date DESC',
                'on' => 'ORDER BY pv.created_date ASC',
                'ec' => 'ORDER BY sell_price DESC',
                'ce' => 'ORDER BY sell_price ASC'
            ];
            if ($order_category != '') {
                $order .= $order_data[$order_category];
            }
            return $order;
        };

        $removed_parameter = array(START_OFFSET);
        $base_url = $this->current_url_with_query_string($removed_parameter);
        $start = $this->input->get(START_OFFSET);
        if ($start == '') {
            $start = '0';
        }
        if (!ctype_digit($start)) {
            redirect($base_url);
        }
        $params = new stdClass();
        $params->start = $start;
        $params->limit = 1000;
        $params->where = $data . $condition_where();
        $params->order = $condition_order();
        $params->agent = isset($_SESSION[SESSION_AGENT_CSRF_TOKEN]) ? 'agent' : '';
        $params->agent_seq = isset($_SESSION[SESSION_AGENT_CSRF_TOKEN]) ? parent::get_agent_user_seq() : '';

        $list_data = $this->M_thumbs_new_products->get_thumbs_new_products($params);
        $query_row = $list_data['row'];
        $query_row_result = $query_row->result();
        $query_row_result_first = $query_row_result[0];
        $total_row = $query_row_result_first->total_row;
        $this->display_page($base_url, $params->start, $total_row, $params->limit);

        parent::set_data($this->data, $list_data);
    }

}
