<?php

require_once CONTROLLER_BASE_HOME;

class C_iese_2017 extends controller_base_home {

    private $data;
    private $url;

    public function __construct() {
        $this->data = parent::__construct('', 'promo/iese_2017', false);
        $this->data[DATA_AUTH][FORM_URL] = IESE_URL;
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('home/M_thumbs_new_products');
        $this->load->library('pagination');
        $this->load->helper('cookie');
        $this->data[BREADCRUMB_PAGE] = '';
        $this->data[IESE_DATA] = '';
        $this->url = $this->uri->segment(3);
    }

    public function index() {
        $data_function_exist = array(IESE_ALWAYS_ON, IESE_ENJOY_YOUR_TIME, IESE_GET_STYLISH, IESE_TAKE_THE_MOMMENT);
        if ($this->url != null || $this->url != '') {
            if (in_array($this->url, $data_function_exist)) {
                call_user_func(array($this, $this->url));
            } else {
                redirect('error_404');
            }
        }
        $this->data[TREE_CATEGORY] = $this->get_tree_category(true);
        $this->data[DATA_AUTH][FORM_TITLE] = $this->create_title('Iese 2017');
        $this->template->view('promo/iese_2017', $this->data);
    }

    protected function always_on() {
        $dataProduct = [
//DATA IESE FLYER
            3289, 2776, 2775, 3181, 2949, 2703,
//DATA IESE
            3108, 3105, 3107, 2947, 2946, 2939, 2938, 3344, 3343, 3342, 3341, 3340,
//DATA WEB
            1992, 1994, 1785, 1786, 2572, 3199, 3152, 2703, 2567, 2549,
            2548, 3240, 3241, 3242, 3243, 3244, 3245, 3246, 3247, 3248,
            3249, 3254, 3250, 3251, 3252, 3253, 3255, 3256, 3257, 3258,
            3260, 3295, 3296, 3297, 3298, 3299, 3300, 3309, 3310, 3311,
            3312, 3313, 3314, 3315, 3316, 3294, 3293, 3292, 3291, 3290,
            3289, 3286, 3322, 3321, 3320, 3319,
        ];

        $this->data[DATA_AUTH][FORM_TITLE] = $this->create_title('Iese 2017 | always on');
        $this->data[BREADCRUMB_PAGE] = 'Always On';
        $this->data['iese'] = TASK_CATEOGRY;
        $this->data[DATA_AUTH][FORM_URL] = IESE_URL . DIRECTORY_SEPARATOR . IESE_ALWAYS_ON;

        $Product = $this->get_data_product($this->setSqlCommand($dataProduct));
    }

    protected function enjoy_your_time() {
        $dataProduct = [
//DATA IESE FLYER
            2550, 3308, 3307, 3306, 3303, 3302, 1691,
            1687, 1686, 1688, 2934,
//DATA IESE
//DATA WEB
            2554, 2540, 2538, 2537, 2651, 2665, 2664, 2409, 2404, 2403, 2300, 2299,
            2634, 2633, 1819, 1583, 1560, 1559, 1250, 1248, 1690, 1697, 1696, 1695,
            1694, 1693, 1692, 2389, 2449, 2566, 2161, 2852, 2820, 2663, 2822, 2672,
            2670, 2669, 2851, 2850, 2846, 2821, 2635, 2018, 1998, 1731, 1730, 1729,
            1722, 1378, 2825, 2824, 2662, 2388, 2461
        ];

        $this->data[DATA_AUTH][FORM_TITLE] = $this->create_title('Iese 2017 | Enjoy Your Time');
        $this->data[BREADCRUMB_PAGE] = 'Enjoy Your Time';
        $this->data['iese'] = TASK_CATEOGRY;
        $this->data[DATA_AUTH][FORM_URL] = IESE_URL . DIRECTORY_SEPARATOR . IESE_ENJOY_YOUR_TIME;
        $Product = $this->get_data_product($this->setSqlCommand($dataProduct));
    }

    protected function get_stylish() {
        $dataProduct = [
//DATA IESE FLYER
//DATA IESE
            2507, 2694, 2584,
//DATA WEB
            2379, 2378, 2376, 2531, 2529, 2528, 3118, 3119, 3121, 3116, 2437,
            3143, 3142, 3138, 3129, 2232, 2231, 2230, 2229, 2228, 2227, 2226,
            2225, 2224, 2222, 2223, 2221, 2220, 2217, 2216, 2214, 2215, 2448,
            2445, 2444, 2436, 2435, 2434, 2433, 2432, 1779, 1782, 1775, 1781,
            3264, 3263, 3262, 3261, 3259, 2848, 2534, 2533, 2520, 2516, 2500,
            2495, 2494, 2493, 2464, 2463, 2197, 2601, 2598, 2606, 2610, 3015,
            2623, 2965, 3084, 2654, 2653, 2655, 2642, 2607
        ];

        $this->data[DATA_AUTH][FORM_TITLE] = $this->create_title('Iese 2017 | Get Stylish');
        $this->data[BREADCRUMB_PAGE] = 'Get Stylish';
        $this->data['iese'] = TASK_CATEOGRY;
        $this->data[DATA_AUTH][FORM_URL] = IESE_URL . DIRECTORY_SEPARATOR . IESE_GET_STYLISH;
        $Product = $this->get_data_product($this->setSqlCommand($dataProduct));
    }

    protected function take_the_momment() {
        $dataProduct = [
//DATA IESE FLYER
            3345,3301,
//DATA WEB
            3074, 3011, 2853, 3010, 2827, 2826, 2381, 2135, 2269, 2551, 1827, 1761,
            1242, 3009, 3007, 3006
        ];

        $this->data[DATA_AUTH][FORM_TITLE] = $this->create_title('Iese 2017 | Take The Moment');
        $this->data[BREADCRUMB_PAGE] = 'Take The Moment';
        $this->data['iese'] = TASK_CATEOGRY;
        $this->data[DATA_AUTH][FORM_URL] = IESE_URL . DIRECTORY_SEPARATOR . IESE_TAKE_THE_MOMMENT;
        $Product = $this->get_data_product($this->setSqlCommand($dataProduct));
    }

    private function get_data_product($sqlCommand = '') {
        $price_range = $this->input->get(PRICE_RANGE, TRUE);
        if ($price_range != FALSE AND ! preg_match("/^[0-9]+,[0-9]+$/", $price_range)) {
            redirect($this->current_url_with_query_string(array(PRICE_RANGE)));
        }

        $order_category = $this->input->get(ORDER_CATEGORY, TRUE);
        $order_category_list = array(NEW_TO_OLD_PRODUCT, OLD_TO_NEW_PRODUCT, PRICE_EXPENSIVE_TO_CHEAP, PRICE_CHEAP_TO_EXPENSIVE);
        if ($order_category != FALSE AND ! in_array($order_category, $order_category_list)) {
            redirect($this->current_url_with_query_string(array(ORDER_CATEGORY)));
        }
        $where = 'WHERE pv.active = 1 AND pv.status IN ("L","C") and pv.seq IN(' . $sqlCommand . ')';
        $condition_where = function()use($price_range, $where) {
            if ($price_range != '') {
                $price = explode(',', $price_range);
                $where .= 'AND sell_price BETWEEN ' . $price[0] . ' AND ' . $price[1];
            }
            return $where;
        };

        $condition_order = function()use($order_category, $sqlCommand) {
            $order = '';
            $order_data = [
                'no' => 'ORDER BY pv.created_date DESC',
                'on' => 'ORDER BY pv.created_date ASC',
                'ec' => 'ORDER BY sell_price DESC',
                'ce' => 'ORDER BY sell_price ASC'
            ];
            if ($order_category != '') {
                $order .= $order_data[$order_category];
            } else {
                $order .= ' ORDER BY FIELD(pv.seq,' . $sqlCommand . ')';
            }
            return $order;
        };

        $removed_parameter = array(START_OFFSET);
        $base_url = $this->current_url_with_query_string($removed_parameter);
        $start = $this->input->get(START_OFFSET);
        if ($start == '') {
            $start = '0';
        }
        if (!ctype_digit($start)) {
            redirect($base_url);
        }
        $params = new stdClass();
        $params->start = $start;
        $params->limit = 100000;
        $params->where = $condition_where();
        $params->order = $condition_order();
        $params->agent = isset($_SESSION[SESSION_AGENT_CSRF_TOKEN]) ? 'agent' : '';
        $params->agent_seq = isset($_SESSION[SESSION_AGENT_CSRF_TOKEN]) ? parent::get_agent_user_seq() : '';

        $list_data = $this->M_thumbs_new_products->get_thumbs_new_products($params);
        $query_row = $list_data['row'];
        $query_row_result = $query_row->result();
        $query_row_result_first = $query_row_result[0];
        $total_row = $query_row_result_first->total_row;
        $this->display_page($base_url, $params->start, $total_row, $params->limit);

        parent::set_data($this->data, $list_data);
    }

    function setSqlCommand($dataProduct = []) {
        (string) $sqlCommand = '';
        $a = '';
        foreach ($dataProduct as $product) {
            $a .= $product . ',';
        }
        (string) $sqlCommand = rtrim($a, ',');
        return $sqlCommand;
    }

}
