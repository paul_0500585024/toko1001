<?php

require_once CONTROLLER_BASE_HOME;

class C_iese_super_great_sale extends controller_base_home {

    private $data;

    public function __construct() {
        parent::__construct("", "iese_super_great_sale", false);
        $this->load->helper('url');
    }

    public function index() {
        $this->data['title'] = 'Promo IESE 2016 Super Great Sale Diskon Lucky Dip Cashback Photobooth Competition.';
        $this->data['tree_category'] = $this->get_tree_category(true);
        $this->template->view("promo/iese_super_great_sale", $this->data);
    }

}

?>
