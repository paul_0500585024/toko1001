<?php

require_once CONTROLLER_BASE_HOME;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of C_prosperity
 *
 * @author Jaka
 */
class C_prosperity extends controller_base_home {

    private $data;

    public function __construct() {
        $this->data = parent::__construct('', 'campaign/prosperity_promo', FALSE);
    }

    public function index() {
        $this->data['title'] = 'Prosperity Promo';
        $this->data['tree_category'] = $this->get_tree_category(true);
        $this->template->view("home/campaign/prosperity_promo", $this->data);
    }

}
