<?php

require_once CONTROLLER_BASE_HOME;

class C_celebrate_your_graduation extends controller_base_home {

    private $data;

    public function __construct() {
        parent::__construct("", "celebrate_your_graduation", false);
        $this->load->helper('url');
    }

    public function index() {
        $this->data['title'] = 'Celebrate your Graduation';		
        $this->data['tree_category'] = $this->get_tree_category(true);		
        $this->template->view("promo/celebrate_your_graduation", $this->data);
    }

}

?>
