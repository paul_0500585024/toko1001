<?php

require_once CONTROLLER_BASE_HOME;

class C_promo_ramadhan extends controller_base_home {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("", "april-wow", false);
        $this->data[DATA_AUTH][FORM_URL] = 'berkah_ramadhan';
        $this->load->model('home/M_thumbs_new_products');
        $this->load->library('pagination');
        $this->load->helper('cookie');
        $this->load->helper('url');
        $this->initialize();
    }

    protected function initialize() {
        $this->data[DATA_RAMADHAN] = '';
        $method = $this->uri->segment(2);
        switch ($method) {
            case RAMADHAN_CATEGORI_SMARTPHONE_EVERCOSS:
                call_user_func(array($this, FUNCTION_SMARTPHONE_EVERCOSS));
                break;
            case RAMADHAN_CATEGORI_TAB_EVERCOSS: 
                call_user_func(array($this, FUNCTION_TAB_EVERCOSS));
                break;
            case RAMADHAN_CATEGORI_TELEVISION:
                call_user_func(array($this, FUNCTION_TELEVISION));
                break;
            case RAMADHAN_CATEGORI_MUSLIM_WEAR:
                call_user_func(array($this, FUNCTION_MUSLIM_WEAR));
                break;
            case RAMADHAN_CATEGORI_SARANGE_COSMETICT :
                call_user_func(array($this, FUNCTION_SARANGE));
                break;
            case RAMADHAN_CATEGORI_POWER_BANK:
                call_user_func(array($this, FUNCTION_POWER_BANK));
                break;
            case RAMADHAN_CATEGORI_HOUSE_HOLD_EQUIPMENT:
                call_user_func(array($this, FUNCTION_HOUSE_HOLD_EQUIPMENT));
                break;
            case RAMADHAN_CATEGORI_SHOES_SPORT:
                call_user_func(array($this, FUNCTION_SHOES_SPORT));
                break;
            case RAMADHAN_CATEGORI_WATCH:
                call_user_func(array($this, FUNCTION_WATCH));
                break;
            case RAMADHAN_CATEGORY_LUNA;
                call_user_func(array($this, FUNCTION_LUNA));
                break;
            default:
                return false;
                break;
        }
    }

    public function index() {
//        if (!parent::is_agent()) {
//            redirect('error_404');
//        }
        $this->data[TREE_CATEGORY] = $this->get_tree_category(true);
        $this->template->view('promo/promo_ramadhan', $this->data);
    }

    /*
     * SMARTPHONE
     */

    protected function smartphone_evercoss() {
        $where = 'WHERE pv.active = 1 AND pv.status IN ("L","C") and pv.seq in ("3558","3557","3556","3555","3554","3553","3552","3551","3550","3549","3548","3547")';
        $this->get_data_product($where);
        $this->data[DATA_RAMADHAN] = TASK_CATEOGRY;
        $this->data[DATA_AUTH][FORM_AUTH][FORM_TITLE] = true;
        $this->data[DATA_AUTH][FORM_TITLE] = $this->create_title('Smartphone Evercoss');
        $this->data[BREADCRUMB_PAGES] = 'Smartphone Evercoss';
    }

    /*
     * TABLET EVERCOSS
     */

    protected function tablet_evercoss() {
        $where = 'WHERE pv.active = 1 AND pv.status IN ("L","C") and pv.seq in ("3560","3559","3546","3545","3544","3543","3541","3542","3539","3538","3537","3536")';
        $this->get_data_product($where);
        $this->data[DATA_RAMADHAN] = TASK_CATEOGRY;
        $this->data[DATA_AUTH][FORM_AUTH][FORM_TITLE] = true;
        $this->data[DATA_AUTH][FORM_TITLE] = $this->create_title('Ramadhan Berkah - Tablet Evercoss');
        $this->data[BREADCRUMB_PAGES] = 'Tablet Evercoss';
    }

    /*
     * LUNA
     */

    protected function luna() {
        $where = 'WHERE pv.active = 1 AND pv.status IN ("L","C") AND m.seq = "69" AND  pv.seq in ("3535","3534","3533","3532")';
        $this->get_data_product($where);
        $this->data[DATA_RAMADHAN] = TASK_CATEOGRY;
        $this->data[DATA_AUTH][FORM_AUTH][FORM_TITLE] = true;
        $this->data[DATA_AUTH][FORM_TITLE] = $this->create_title('Ramadhan Berkah - Smartphone Luna');
        $this->data[BREADCRUMB_PAGES] = 'Luna Smartphone';
    }

    /*
     * TELEVISION
     */

    protected function television() {
        $where = 'WHERE pv.active = 1 AND pv.status IN ("L","C") and pv.seq in ("2817","2816","2815","2668","1564","1563","1562","1561")';
        $this->get_data_product($where);
        $this->data[DATA_RAMADHAN] = TASK_CATEOGRY;
        $this->data[DATA_AUTH][FORM_AUTH][FORM_TITLE] = true;
        $this->data[DATA_AUTH][FORM_TITLE] = $this->create_title('Ramadhan Berkah - Televisi');
        $this->data[BREADCRUMB_PAGES] = 'Televisi';
    }

    /*
     * PAKAIAN MUSLIM
     */

    protected function muslim_wear() {
        $where = 'WHERE pv.active = 1 AND pv.status IN ("L","C") and pv.seq in ("679","678","677","675","674","673","672","671","670","669","338","337","336","335","334","333") OR (p.category_l2_seq = "53" AND m.seq = "55")';
        $this->get_data_product($where);
        $this->data[DATA_RAMADHAN] = TASK_CATEOGRY;
        $this->data[DATA_AUTH][FORM_AUTH][FORM_TITLE] = true;
        $this->data[DATA_AUTH][FORM_TITLE] = $this->create_title('Ramadhan Berkah - Pakaian Muslim');
        $this->data[BREADCRUMB_PAGES] = 'Pakaian Muslim';
    }

    /*
     * KOSMETIK SARANGE
     */

    protected function sarange_kosmetik() {
        $where = 'WHERE pv.active = 1 AND pv.status IN ("L","C") and p.name like "%sarange%"';
        $this->get_data_product($where);
        $this->data[DATA_RAMADHAN] = TASK_CATEOGRY;
        $this->data[DATA_AUTH][FORM_AUTH][FORM_TITLE] = true;
        $this->data[DATA_AUTH][FORM_TITLE] = $this->create_title('Ramadhan Berkah - Sarange Kosmetik');
        $this->data[BREADCRUMB_PAGES] = 'Pakaian Muslim';
    }

    /*
     * POWERBANK
     */

    protected function powerbank() {
        $where = 'WHERE pv.active = 1 AND pv.status IN ("L","C") and p.category_ln_seq = "199" ';
        $this->get_data_product($where);
        $this->data[DATA_RAMADHAN] = TASK_CATEOGRY;
        $this->data[DATA_AUTH][FORM_AUTH][FORM_TITLE] = true;
        $this->data[DATA_AUTH][FORM_TITLE] = $this->create_title('Ramadhan Berkah - Power Bank');
        $this->data[BREADCRUMB_PAGES] = 'Power Bank';
    }

    /*
     * PERALATAN RUMAH TANGGA
     */

    protected function household_equipment() {
        $where = 'WHERE pv.active = 1 AND pv.status IN ("L","C") and p.category_ln_seq = "619" ';
        $this->get_data_product($where);
        $this->data[DATA_RAMADHAN] = TASK_CATEOGRY;
        $this->data[DATA_AUTH][FORM_AUTH][FORM_TITLE] = true;
        $this->data[DATA_AUTH][FORM_TITLE] = $this->create_title('Ramadhan Berkah - Perlengkapan Rumah Tangga');
        $this->data[BREADCRUMB_PAGES] = 'Perlengkapan Rumah tangga';
    }

    /*
     * SEPATU OLAHRAGA
     */

    protected function shoes_sport() {
        $where = 'WHERE pv.active = 1 AND pv.status IN ("L","C") and m.code ="League"';
        $this->get_data_product($where);
        $this->data[DATA_RAMADHAN] = TASK_CATEOGRY;
        $this->data[DATA_AUTH][FORM_AUTH][FORM_TITLE] = true;
        $this->data[DATA_AUTH][FORM_TITLE] = $this->create_title('Ramadhan Berkah - Sepatu olahraga');
        $this->data[BREADCRUMB_PAGES] = 'Sepatu Olahraga';
    }

    /*
     * JAM TANGAN
     */

    protected function watch() {
        $where = 'WHERE pv.active = 1 AND pv.status IN ("L","C") and m.code ="bless-watch"';
        $this->get_data_product($where);
        $this->data[DATA_RAMADHAN] = TASK_CATEOGRY;
        $this->data[DATA_AUTH][FORM_AUTH][FORM_TITLE] = true;
        $this->data[DATA_AUTH][FORM_TITLE] = $this->create_title('Ramadhan Berkah - Jam Tangan');
        $this->data[BREADCRUMB_PAGES] = 'Jam Tangan';
    }

    private function get_data_product($data = "") {
        $price_range = $this->input->get(PRICE_RANGE, TRUE);
        if ($price_range != FALSE AND ! preg_match("/^[0-9]+,[0-9]+$/", $price_range)) {
            redirect($this->current_url_with_query_string(array(PRICE_RANGE)));
        }

        $order_category = $this->input->get(ORDER_CATEGORY, TRUE);
        $order_category_list = array(NEW_TO_OLD_PRODUCT, OLD_TO_NEW_PRODUCT, PRICE_EXPENSIVE_TO_CHEAP, PRICE_CHEAP_TO_EXPENSIVE);
        if ($order_category != FALSE AND ! in_array($order_category, $order_category_list)) {
            redirect($this->current_url_with_query_string(array(ORDER_CATEGORY)));
        }

        $condition_where = function()use($price_range) {
            $where = '';
            if ($price_range != '') {
                $price = explode(',', $price_range);
                $where .= 'AND sell_price BETWEEN ' . $price[0] . ' AND ' . $price[1];
            }
            return $where;
        };


        $condition_order = function()use($order_category) {
            $order = '';
            $order_data = [
                'no' => 'ORDER BY pv.created_date DESC',
                'on' => 'ORDER BY pv.created_date ASC',
                'ec' => 'ORDER BY sell_price DESC',
                'ce' => 'ORDER BY sell_price ASC'
            ];
            if ($order_category != '') {
                $order .= $order_data[$order_category];
            }
            return $order;
        };

        $removed_parameter = array(START_OFFSET);
        $base_url = $this->current_url_with_query_string($removed_parameter);
        $start = $this->input->get(START_OFFSET);
        if ($start == '') {
            $start = '0';
        }
        if (!ctype_digit($start)) {
            redirect($base_url);
        }
        $params = new stdClass();
        $params->start = $start;
        $params->limit = 1000;
        $params->where = $data . $condition_where();
        $params->order = $condition_order();
        $params->agent = isset($_SESSION[SESSION_AGENT_CSRF_TOKEN]) ? 'agent' : '';
        $params->agent_seq = isset($_SESSION[SESSION_AGENT_CSRF_TOKEN]) ? parent::get_agent_user_seq() : '';

        $list_data = $this->M_thumbs_new_products->get_thumbs_new_products($params);
        $query_row = $list_data['row'];
        $query_row_result = $query_row->result();
        $query_row_result_first = $query_row_result[0];
        $total_row = $query_row_result_first->total_row;
        $this->display_page($base_url, $params->start, $total_row, $params->limit);
        parent::set_data($this->data, $list_data);
    }

}
