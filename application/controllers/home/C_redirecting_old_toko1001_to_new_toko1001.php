<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_redirecting_old_toko1001_to_new_toko1001 extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('home/M_thumbs_detail');
    }
    
    public function index() {
        $product_variant_seq =  $this->uri->segment(3);
        $params = new stdClass;
        $params->product_variant_seq = $product_variant_seq;
        $detail = $this->M_thumbs_detail->get_thumbs_detail($params);
        $name = isset($detail[0][0]->name)?$detail[0][0]->name:'';
        $variant_value = isset($detail[0][0]->variant_value)?$detail[0][0]->variant_value:'';
        
        if($name == '' AND $variant_value == ''){
            redirect(base_url().'error_404');
        }                
        
        $redirect_uri = strtolower(url_title($name . ' ' . $variant_value)) . '-' . $product_variant_seq;
        $redirect_page = base_url().$redirect_uri;
        
        redirect($redirect_page);
    }

}
