<?php

require_once CONTROLLER_BASE_HOME;

class C_credit_product extends controller_base_home {

    private $data;

    public function __construct() {
        $this->data = parent::__construct('', 'produk/cicilan', false);
        $this->initialize();
    }

    protected function initialize() {
        $this->load->model('home/M_credit_product');
        $this->load->library('pagination');
    }

    public function index() {
        $removed_parameter = array(START_OFFSET);
        $base_url = $this->current_url_with_query_string($removed_parameter);
        $this->data['breadcrumb_pages'] = 'Cicilan Produk';

        $price_range = $this->input->get(PRICE_RANGE, TRUE);
        if ($price_range != FALSE AND ! preg_match("/^[0-9]+,[0-9]+$/", $price_range)) {
            redirect($this->current_url_with_query_string(array(PRICE_RANGE)));
        }

        $order_category = $this->input->get(ORDER_CATEGORY, TRUE);
        $order_category_list = array(NEW_TO_OLD_PRODUCT, OLD_TO_NEW_PRODUCT, PRICE_EXPENSIVE_TO_CHEAP, PRICE_CHEAP_TO_EXPENSIVE);
        if ($order_category != FALSE AND ! in_array($order_category, $order_category_list)) {
            redirect($this->current_url_with_query_string(array(ORDER_CATEGORY)));
        }

        $condition_where = function()use($price_range) {
            $where = '';
            if ($price_range != '') {
                $price = explode(',', $price_range);
                $where .= 'AND sell_price BETWEEN ' . $price[0] . ' AND ' . $price[1];
            }
            return $where;
        };


        $condition_order = function()use($order_category) {
            $order = '';
            $order_data = [
                'no' => 'ORDER BY pv.created_date DESC',
                'on' => 'ORDER BY pv.created_date ASC',
                'ec' => 'ORDER BY sell_price DESC',
                'ce' => 'ORDER BY sell_price ASC'
            ];
            if ($order_category != '') {
                $order .= $order_data[$order_category];
            }
            return $order;
        };

        $perpage = (4 * 5);
        $start = $this->input->get(START_OFFSET);
        if ($start == '') {
            $start = '0';
        }
        if (!ctype_digit($start)) {
            redirect($base_url);
        }
        $params = new stdClass();
        $params->start = $start;
        $params->limit = $perpage;
        $params->where = $condition_where();
        $params->order = $condition_order();
        $this->data['tree_category'] = $this->get_tree_category(true);
        $sel_data = $this->M_credit_product->get_product_list($params);

        $query_row = $sel_data['row'];
        $query_row_result = $query_row->result();
        $query_row_result_first = $query_row_result[0];
        $row = $query_row_result_first->total_row;

        parent::set_data($this->data, $sel_data);
        $this->display_page($base_url, $start, $row, $perpage);
        $this->template->view('home/credit_product', $this->data);
    }

}
