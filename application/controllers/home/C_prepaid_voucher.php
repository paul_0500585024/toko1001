<?php

require_once CONTROLLER_BASE_HOME;

class C_prepaid_voucher extends controller_base_home {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("", "home/prepaid_voucher", false);
    }

    public function index() {
        $this->load->helper('cookie');
        $this->load->model("component/M_dropdown_list");
        $this->load->model('member/profile/M_profile_member');
        $this->load->model('member/M_prepaid_voucher');
        $this->load->model(LIVEVIEW . 'agent/profile/M_profile_agent');
        $this->data['tree_category'] = $this->get_tree_category(true);
        $this->data[PROVIDER_NAME] = $this->M_dropdown_list->get_dropdown_provider();
        $type = parent::get_input_post("type");
        $step = parent::get_input_get("step");

        if (isset($_SESSION[SESSION_PROVIDER]) && $_SESSION[SESSION_PROVIDER] !== null) {
            $this->data[SESSION_PROVIDER] = $_SESSION[SESSION_PROVIDER];
        }

        $params = new stdClass();
        $params->ip_address = parent::get_ip_address();
        $params->member_seq = parent::get_member_user_id();
        $params->agent_seq = parent::get_agent_user_id();
        $this->data[MEMBER_INFO] = $this->M_profile_member->get_data_member($params);
        $this->data[AGENT_INFO] = $this->M_profile_agent->get_data_agent($params);

        switch ($step) {
            case TASK_SECOND_STEP : {
                    if (!isset($_SESSION[SESSION_PROVIDER])) {
                        redirect(base_url("pulsa"));
                    }
                }
                break;
        }


        switch ($type) {
            case TASK_PROVIDER_CHANGE : {
                    $provider_seq = parent::get_input_post("provider_seq");
                    die(json_encode($this->M_dropdown_list->get_dropdown_provider_service($provider_seq)));
                }break;

            case TASK_PROVIDER_SERVICE_CHANGE : {
                    $provider_service_seq = parent::get_input_post("provider_service_seq");
                    die(json_encode($this->M_dropdown_list->get_dropdown_provider_service_nominal($provider_service_seq)));
                }break;

            case TASK_PHONE_NUMBER_CHANGE : {
                    $phone_number_code = parent::get_input_post("phone_number_code");
                    $phone_number_code = str_replace(" ", "", $phone_number_code);                    
                    $phone_number_code = substr($phone_number_code, 0, 4);
                    die(json_encode($this->M_dropdown_list->get_dropdown_phone_number_code($phone_number_code)));
                }break;
            case TASK_SAVE_PROVIDER : {
                    $params = new stdClass();
                    $params->phone_number = parent::get_input_post("phone_number");
                    $params->provider_seq = parent::get_input_post("provider_seq");
                    $params->provider_service_seq = parent::get_input_post("provider_service_seq");
                    $params->provider_service_nominal_seq = parent::get_input_post("provider_service_nominal_seq");

                    $temp[SESSION_PROVIDER] = $params;
                    $this->session->set_userdata($temp);
                    die();
                }break;
            case TASK_PROVIDER_SAVE : {
                    $params = new stdClass();
                    $params->phone_number = parent::get_input_post("phone_number", TRUE, FILL_VALIDATOR, "No HP", $this->data);
                    if (strlen(str_replace(" ", "", $params->phone_number)) < 7 || strlen(str_replace(" ", "", $params->phone_number)) > 13) {
                        $this->data[DATA_ERROR][ERROR] = true;
                        $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_VALIDATION_PHONE_NUMBER_LENGTH;
                    }
                    $params->provider_seq = parent::get_input_post("provider_seq", TRUE, FILL_VALIDATOR, "Provider", $this->data);
                    $params->provider_service_seq = parent::get_input_post("provider_service_seq", TRUE, FILL_VALIDATOR, "Service", $this->data);
                    $params->provider_service_nominal_seq = parent::get_input_post("provider_service_nominal_seq", TRUE, FILL_VALIDATOR, "Nominal", $this->data);
                    $params->pg_method_seq = parent::get_input_post("payment_method", TRUE, FILL_VALIDATOR, "Metode Pembayaran", $this->data);

                    $provider_info = $this->M_prepaid_voucher->get_info_provider($params);
                    if (isset($provider_info[0][0])) {
                        $params->provider_name = $provider_info[0][0]->provider_name;
                        $params->provider_service_name = $provider_info[0][0]->provider_service_name;
                        $params->nominal = $provider_info[0][0]->nominal;
                        $params->sell_price = $provider_info[0][0]->sell_price;
                        $params->pg_method_name = $provider_info[1][0]->name;
                        $params->logo = $provider_info[0][0]->logo;
                    }
                    $temp[SESSION_PROVIDER] = $params;
                    $this->session->set_userdata($temp);
                    if (!isset($this->data[DATA_ERROR][ERROR]) || $this->data[DATA_ERROR][ERROR] === false) {
                        redirect(base_url("pulsa?step=2"));
                    } else {
                        $admin_info[SESSION_DATA] = $this->data;
                        $this->session->set_userdata($admin_info);
                        redirect(base_url("pulsa"));
                    }
                }break;
            case TASK_PROVIDER_PROCESS : {
                    $this->save_order_voucher();
                }break;
        }

        $this->template->view("home/prepaid_voucher", $this->data);
    }

    protected function save_order_voucher() {
        $provider = $_SESSION[SESSION_PROVIDER];
        $type = '';
        $customer = '';
        if (isset($_SESSION[SESSION_MEMBER_CSRF_TOKEN])) {
            $type = MEMBER;
            $customer = parent::get_member_user_id();
        } elseif (isset($_SESSION[SESSION_AGENT_CSRF_TOKEN])) {
            $type = AGENT;
            $customer = parent::get_agent_user_id();
        } else {
            redirect(base_url());
        }

        try {
            $this->M_prepaid_voucher->trans_begin();
//            $customer = parent::get_member_user_id();
            $provider->user_id = $customer;
            $provider->type = $type;
            $provider->order_no = $this->get_order_no();
            $provider->payment_status = PAYMENT_UNPAID_STATUS_CODE;
            $provider->order_status = ORDER_REQUEST_STATUS_CODE;
            $this->check_payment_gateway_method($provider->pg_method_seq, $provider->sell_price, $provider->type);
            $this->M_prepaid_voucher->save_add_order_voucher($provider);
            $this->M_prepaid_voucher->trans_commit();
            $this->session->unset_userdata(SESSION_PROVIDER);
            redirect(base_url("member/payment/" . $provider->order_no));
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_prepaid_voucher->trans_rollback();
        }
    }

    protected function get_order_no() {
        return parent::generate_random_alnum(2, true) . substr(date("Y"), 2, 2) . parent::generate_random_text(2, true) . date("m") . parent::generate_random_text(2, true) . date("d");
    }

    protected function check_payment_gateway_method($pg_method_seq, $total_payment, $type) {
        $data = new stdClass();
        $data->ip_address = parent::get_ip_address();
        $data->member_seq = parent::get_member_user_seq() != '' ? parent::get_member_user_seq() : '';
        $data->agent_seq = parent::get_agent_user_id() != '' ? parent::get_agent_user_seq() : '';
        switch ($pg_method_seq) {
            case PAYMENT_SEQ_DEPOSIT: {
                    switch ($type) {
                        case AGENT: {
                                $agent = $this->M_profile_agent->get_data_agent($data);
                                if ($agent[2][0]->deposit_amt < $total_payment) {
                                    throw new Exception(ERROR_DEPOSIT_EXCEDEED);
                                };
                            }
                            break;
                        case MEMBER: {
                                $member = $this->M_profile_member->get_data_member($data);
                                if ($member[2][0]->deposit_amt < $total_payment) {
                                    throw new Exception(ERROR_DEPOSIT_EXCEDEED);
                                };
                            }
                            break;
                    }
                }break;
        }
    }

}

?>
