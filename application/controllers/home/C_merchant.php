<?php

require_once CONTROLLER_BASE_HOME;

class C_merchant extends controller_base_home {

    private $data;
    private $hseq;
    private $col_num = 5;

    public function __construct() {
        parent::__construct("", "home/C_merchant_page", false);
        $this->load->model("home/M_merchant");
        $this->load->model('home/M_thumbs_new_products');
        $this->load->helper('url');
        $this->load->helper('cookie');
        $this->load->library('pagination');
        $this->hseq = $this->uri->segment(2);
    }

    public function index() {
        $this->get_data();
        $perpage = ($this->col_num * 6) + 2;
        $start = $this->input->get(START_OFFSET);

        $params = new stdClass;
        $params->start = $start;
        $params->limit = $perpage;
        $params->where = " where pv.active = '1' AND pv.status IN ('L','C') and p.merchant_seq= " . $this->data[DATA_SELECTED][LIST_DATA][0]->seq;
        $params->order = "";
        $params->agent = parent::is_agent() ? AGENT : "";
        $params->agent_seq = parent::is_agent() ? parent::get_agent_user_id() : "";

        $this->data['tree_category'] = $this->get_tree_category(true);
        $query = $this->M_thumbs_new_products->get_thumbs_new_products($params);
        $query_row = $query['row'];
        $query_row_result = $query_row->result();
        $query_row_result_first = $query_row_result[0];
        $total_row = $query_row_result_first->total_row;

        $this->data['row'] = $total_row;
        $this->data['product'] = $query['product'];

        $this->data['description'] = isset($this->data[DATA_SELECTED][LIST_DATA]) ? $this->get_display_value($this->data[DATA_SELECTED][LIST_DATA][0]->name) : "";

        $base_url = base_url() . 'merchant/' . $this->hseq;
        $this->display_page($base_url, $start, $total_row, $perpage);
        $this->template->view("home/merchant_page", $this->data);
    }

    protected function get_data() {
        $params = new stdClass();
        $params->user_id = parent::get_member_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->m_code = $this->hseq;
        try {
            $sel_data = $this->M_merchant->get_merchant_info($params);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
                if ($sel_data[0]->pickup_addr_eq == "1") {
                    $params->district_seq = $sel_data[0]->district_seq;
                } else {
                    $params->district_seq = $sel_data[0]->pickup_district_seq;
                }

                $this->data[DATA_SELECTED][LIST_DATA][] = $this->M_merchant->get_district_info($params);
                $params->merchant_seq = $sel_data[0]->seq;
                $this->data[DATA_SELECTED][LIST_DATA][] = $this->M_merchant->get_promo_free_fee($params);
            } else {
                redirect(base_url() . "error_404");
            }
        } catch (Exception $ex) {
            redirect(base_url() . "error_404");
        }
    }

}
