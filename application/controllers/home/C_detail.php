<?php

require_once CONTROLLER_BASE_HOME;

class C_detail extends controller_base_home {

    private $data;
    private $product_variant_seq;

    public function __construct() {
        parent::__construct("", "home/detail", false);
        $this->load->model('member/M_checkout_member');
        $this->load->helper('url');
        $this->load->helper('cookie');
        $this->load->library('pagination');
        $product_name_variant_seq = $this->uri->segment(1);
        $exploder_product_name_variant_seq = explode('-', $product_name_variant_seq);
        $this->product_variant_seq = end($exploder_product_name_variant_seq);
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('home/M_thumbs_detail');
        $this->load->model('home/M_thumbs_new_products');
        $this->load->model('member/M_wishlist_member');
        $this->load->model('component/M_dropdown_list');
    }

    public function index() {
        $category_l2_seq = '';
        $category_ln_seq = '';
        $seq_self_parent_category = array();
        $product_variant_seq = $this->product_variant_seq;
        $delivery_city = '';
        $product_related = '';
        $mywish = '';
        $params = new stdClass;
        $params_delivery = new stdClass;
        $params_product_related = new stdClass;
        $params->product_variant_seq = $product_variant_seq;
        $params->agent = parent::is_agent() ? AGENT : "";
        $params->agent_seq = parent::is_agent() ? parent::get_agent_user_id() : "";


        if (parent::get_member_user_id() > 0) {
            $params->user_id = parent::get_member_user_id();
            $params->ip_address = parent::get_ip_address();
            $params->member_seq = parent::get_member_user_id();
            if ($this->input->get('q')) {
                $params->pstatus = parent::get_input_get("q");
                $this->M_wishlist_member->update_wishlist($params);
            }
            $mywish = $this->M_wishlist_member->get_wishlist($params);
            if (isset($mywish)) {
                $mywish = ' <a href="?q=dw" class="btn btn-danger btn-sm"><i class="fa fa-minus-circle"></i> Hapus Dari Wishlist</a>';
            } else {
                $mywish = ' <a href="?q=aw" class="btn btn-info btn-sm"><i class="fa fa-plus-circle"></i> Tambah ke Wishlist</a>';
            }
        }
        $data['wishlist'] = $mywish;
        $detail = $this->M_thumbs_detail->get_thumbs_detail($params);
        $data['credit'] = $this->M_dropdown_list->get_dropdown_credit($params);
        $data['tree_category'] = $this->get_tree_category(true);
        $data['tree_category_computed'] = $this->get_tree_category();
        if (parent::is_agent()) {
            $agent_seq = parent::get_agent_user_seq();
            $total = $detail[0][0]->sell_price;
            if ($total >= ADIRA_MINIMUM_CREDIT_PAYMENT) {
                $data['info_product_loan'] = $this->M_checkout_member->get_info_loan($product_variant_seq, $total, 0, $agent_seq);
            }
        }
        $prdsee = '';
        $prdseebef = '';
        if (isset($detail[0][0])) {
            if ($this->input->cookie('prdsee') == '') {
                $prdsee = base64_encode($product_variant_seq);
            } else {
                $prdseebef = explode(",", base64_decode($this->input->cookie('prdsee')));
                $prdseearr = array();
                if (count($prdseebef) >= 6) {
                    for ($i = 0; $i < count($prdseebef); $i++) {
                        if ($prdseebef[$i] != '' && $prdseebef[$i] != $product_variant_seq)
                            $prdseearr[] = $prdseebef[$i];
                    }
                    $prdsee = implode(',', $prdseearr);
                } else {
                    $prdsee = base64_decode($this->input->cookie('prdsee'));
                }
                $prdsee = base64_encode($product_variant_seq . ',' . $prdsee);
                $prdseebef = base64_decode($this->input->cookie('prdsee'));
            }
            $cookie = array(
                'name' => 'prdsee',
                'value' => $prdsee,
                'expire' => (3600 * 1000 * 24),
                'domain' => SMTP_HOST,
                'path' => '/',
                'secure' => SECURECONNECTION
            );
            $this->input->set_cookie($cookie);

            $data_detail_description = $detail[0][0];
            $data_detail_description->description = str_replace( chr( 194 ) . chr( 160 ), ' ', $data_detail_description->description);
            $data['detail_description'] = $data_detail_description;
            $data['detail_specification'] = isset($detail[1]) ? $detail[1] : array();
            if (isset($data_detail_description->merchant_seq)) {
                $merchant_seq = $data_detail_description->merchant_seq;
                $params_delivery->merchant_seq = $merchant_seq;
                $delivery = $this->M_thumbs_detail->get_thumbs_delivery_promo_city($params_delivery);
                $data['delivery'] = $delivery;
                if ($delivery != "") {
                    $delivery_city .= "<ul>";
                    foreach ($delivery as $each_delivery) {
                        $delivery_city .= "<li>" . $each_delivery->city_name . "</li>";
                    }
                    $delivery_city .= "</ul>";
                }
                $data['delivery_city'] = $delivery_city;
            }
            $category_l2_seq = isset($data_detail_description->category_l2_seq) ? $data_detail_description->category_l2_seq : '';
            $category_ln_seq = isset($data_detail_description->category_ln_seq) ? $data_detail_description->category_ln_seq : '';
            $product_seq = isset($data_detail_description->product_seq) ? $data_detail_description->product_seq : '';
            $product_variant_seq = isset($data_detail_description->product_variant_seq) ? $data_detail_description->product_variant_seq : '';
            if ($category_ln_seq != '') {
                $this->get_category_seq_self_parent_with_name($seq_self_parent_category, $data['tree_category_computed'], $category_ln_seq); // get self and parent result:self category to its parent
            }
            $data['seq_self_parent_category'] = array_reverse($seq_self_parent_category, TRUE);
            $data['product_variant_seq'] = $product_variant_seq;
            if ($product_seq != '') {
                $params_product_related->start = 0;
                $params_product_related->limit = 1000;
                $params_product_related->where = "WHERE pv.active = 1 AND pv.status IN ('L','C') AND pv.seq != " . $product_variant_seq . " AND p.seq=" . $product_seq;
                $params_product_related->order = '';
                $params_product_related->agent = parent::is_agent() ? AGENT : '';
                $params_product_related->agent_seq = parent::is_agent() ? parent::get_agent_user_id() : "";
                $product_related = $this->M_thumbs_new_products->get_thumbs_new_products($params_product_related);
                $data['product_related'] = $product_related['product'];

                $params->start = 0;
                $params->limit = 6;
                $params->where = 'WHERE pv.active = 1 AND pv.product_seq in (SELECT product_variant_seq FROM `t_order_product` where `order_seq` in '
                        . '(select order_seq from `t_order_product` where product_variant_seq=' . $product_variant_seq . ' and product_status="R") and product_status="R" and product_variant_seq <> ' . $product_variant_seq . ' '
                        . 'group by `product_variant_seq`)';
                $params->order = 'order by rand()';

                if ($this->input->cookie('prdsee') != '') {
                    $params->start = 0;
                    $params->limit = 6;
                    $params->where = 'WHERE pv.active = 1 AND pv.seq in (' . $prdseebef . ')';
                    $params->order = 'ORDER BY FIELD(pv.seq, ' . $prdseebef . ')';
                    $params->agent = parent::is_agent() ? AGENT : '';
                    $params->agent_seq = parent::is_agent() ? parent::get_agent_user_id() : "";
                    $query_recent_product = $this->M_thumbs_new_products->get_thumbs_new_products($params);
                    $data['product_recent'] = $query_recent_product ['product'];
                }
            }
            $name = isset($detail[0][0]->name) ? $detail[0][0]->name : '';
            $variant_value = $this->get_variant_value($data['detail_description']->variant_seq, $data['detail_description']->variant_value, "-");
            $data['product_review'] = $this->product_review($product_variant_seq);
            $product_price = isset($data['detail_description']->product_price) ? $data['detail_description']->product_price : '';
            $sell_price = isset($data['detail_description']->sell_price) ? $data['detail_description']->sell_price : '';
            $discount = $this->get_percentage_discount($product_price, $sell_price);
            $title = ucwords($name . ' ' . $variant_value . ' - Harga & Spesifikasi');
            $description = "Jual " . ucwords($name . ' ' . $variant_value) . " di Toko1001.id, diskon {$discount}";
            $data['title'] = $this->create_title($title);
            $data['description'] = $description;
            $data['keyword'] = ucwords($name . ' ' . $variant_value) . ' ' . end($data['seq_self_parent_category']);
            $data['is_detail'] = 'true';
            $this->data = $data;
            $add_header = '<meta property = "og:title" content = "' . $data['title'] . '"/>';
            $add_header .= '<meta property = "og:description" content = "' . htmlentities($data['description']) . '"/>';
            $add_header .= '<meta property="og:type" content="website"/>';
            $add_header .= '<meta property = "og:image" content = "' . $this->get_file_exists(PRODUCT_UPLOAD_IMAGE . "/" . $data['detail_description']->merchant_seq . "/" . $data['detail_description']->{"image1"}) . '"/>';
            $add_header .= '<meta property = "og:url" content = "' . $this->current_url_with_query_string() . '"/>';
            $add_header .= '<meta property = "og:site_name" content = "' . base_url() . '"/>';

            $add_header .= '<meta name="twitter:card" content="summary">';
            $add_header .= '<meta name="twitter:url" content = "' . $this->current_url_with_query_string() . '"/>';
            $add_header .= '<meta name="twitter:title" content = "' . $data['title'] . '"/>';
            $add_header .= '<meta name="twitter:description" content = "' . htmlentities($data['description']) . '"/>';
            $add_header .= '<meta name="twitter:image" content = "' . $this->get_file_exists(PRODUCT_UPLOAD_IMAGE . "/" . $data['detail_description']->merchant_seq . "/" . $data['detail_description']->{"image1"}) . '"/>';

//            die(var_dump($this->data));

            $this->template->view("home/detail", $this->data, 'main', $add_header);
        } else {
            $param_empty_detail_description = new stdClass;
            $param_empty_detail_description->name = "";
            $param_empty_detail_description->product_seq = "";
            $param_empty_detail_description->merchant_seq = "";
            $param_empty_detail_description->variant_value = "";
            $param_empty_detail_description->product_price = "0";
            $param_empty_detail_description->sell_price = "0";
            $param_empty_detail_description->product_variant_seq = "";
            $param_empty_detail_description->description = "";
            $param_empty_detail_description->specifictation = "";
            $param_empty_detail_description->merchant_name = "";
            $param_empty_detail_description->image1 = "";
            $param_empty_detail_description->image2 = "";
            $param_empty_detail_description->image3 = "";
            $param_empty_detail_description->image4 = "";
            $param_empty_detail_description->image5 = "";
            $param_empty_detail_description->image6 = "";
            $param_empty_detail_description->code = "";
            $param_empty_detail_description->category_l2_seq = "";
            $param_empty_detail_description->category_ln_seq = "";
            $param_empty_detail_description->stock = "";
            $param_empty_detail_description->sku = "";
            $data['detail_description'] = $param_empty_detail_description;
            $data['detail_specification'] = isset($detail[1]) ? $detail[1] : array();
            $this->data = $data;
            redirect(base_url() . 'error_404');
        }
    }

    public function product_review($product_variant_seq = '', $start = '0', $limit = '3', $where = '', $order = '') {
        $perpage = 1;
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $product_variant_seq = $this->input->get('product_variant_seq');
            $start = $this->input->get('start');
            $limit = $this->input->get('limit');
            $where = $this->input->get('where');
            $order = $this->input->get('order');
        } else {
            $product_variant_seq = $product_variant_seq;
            $start = $start;
            $limit = $limit;
            $where = $where;
            $order = $order;
        }
        switch ($order) {
            case '1':
                $order = 'ORDER BY pr.created_date ASC';
                break;
            case '2':
                $order = 'ORDER BY pr.rate DESC';
                break;
            case '3':
                $order = 'ORDER BY pr.rate ASC';
                break;
        }

        $params = new stdClass;
        $params->product_variant_seq = $product_variant_seq;
        $params->start = $start;
        $params->limit = $limit;
        $params->where = $where;
        $params->order = $order;
        $data = $this->M_thumbs_detail->get_thumbs_product_review($params);
        $product_review = $data['product_review'];
        $row = $data['row'];
        $total = '';
        foreach ($row->result() as $each_total) {
            $total = $each_total->total_row;
        }

        //pagination
        $base_url = base_url() . 'home/c_detail/product_review?pv=' . $product_variant_seq;
        if ($start == '') {
            $start = '0';
        }
        if (!ctype_digit($start)) {
            redirect($base_url);
        }
        $this->ajax_display_page($base_url, $start, $total, $limit);
        $pagination = $this->pagination->create_links();
        $result = array_merge(array('product_review' => $product_review), array('total_product_review' => $total), array('pagination' => $pagination));

        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            echo json_encode($result);
        } else {
            return $result;
        }
    }

}
