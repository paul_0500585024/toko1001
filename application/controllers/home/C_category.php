<?php

require_once CONTROLLER_BASE_HOME;

class C_category extends controller_base_home {

    private $data;
    private $product_category_seq;
    private $product_name_variant_seq;
    private $col_num = 5;

    public function __construct() {
        parent::__construct("", "home/category", false);
        $this->load->helper('url');
        $this->load->library('pagination');
        $this->load->helper('cookie');
        $this->product_name_variant_seq = $this->uri->segment(1);
        if ($this->product_name_variant_seq == ALL_CATEGORY) {
            $this->product_category_seq = '0';
        } else {
            $exploder_product_category_seq = explode('-', $this->product_name_variant_seq);
            $product_category_seq_unclear = end($exploder_product_category_seq);
            $this->product_category_seq = ltrim($product_category_seq_unclear, CATEGORY);
        }
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('home/M_thumbs_new_products');
        $this->load->model('home/M_thumbs_category');
    }

    public function index() {
        $this->category_display();
    }

    private function category_display() {
        //initialize
        $list_of_product_seq = "''";
        $product_seq = "";
        $attribute_value = "";
        $ordering_category = '';
        $exploder_parameter = ',';
        $exploder_parent_child = '-';
        $order_category = '';
        $category_seq_self_with_name = '';
        $title = '';
        $description = '';
        $keyword = '';

        $params = new stdClass;
        $params_image = new stdClass;
        $params_category = new stdClass;
        $params_product_seq = new stdClass();
        $query_string_parameter_name = array();
        $query_string_parameter_name_without_0 = array();
        $query_string_array_combination = array();
        $level = '';
        $seq_self_parent_category = array();
        $category_seq_self_parent_with_name = array();

        // get list of category attribute
        $attribute_seq_attribute_value = $this->input->get(PARAMETER_CATEGORY_ATTRIBUTE);

        //get search parameter
        $search = htmlspecialchars($this->input->get(SEARCH, TRUE));

        $price_range = $this->input->get(PRICE_RANGE, TRUE);
        if ($price_range != FALSE AND ! preg_match("/^[0-9]+,[0-9]+$/", $price_range)) {
            redirect($this->current_url_with_query_string(array(PRICE_RANGE)));
        }

        $order_category = $this->input->get(ORDER_CATEGORY, TRUE);
        $order_category_list = array(NEW_TO_OLD_PRODUCT, OLD_TO_NEW_PRODUCT, PRICE_EXPENSIVE_TO_CHEAP, PRICE_CHEAP_TO_EXPENSIVE);
        if ($order_category != FALSE AND ! in_array($order_category, $order_category_list)) {
            redirect($this->current_url_with_query_string(array(ORDER_CATEGORY)));
        }

        //get tree category
        $this->data['tree_category'] = $this->get_tree_category(true);
        $this->data['tree_category_computed'] = $this->get_tree_category();

        if ($this->product_category_seq != 0) {
            //if not all category
            $this->get_category_seq_self_parent($seq_self_parent_category, $this->data['tree_category_computed'], $this->product_category_seq); // get self and parent
            $this->get_category_seq_self_child($seq_self_child_category, $this->data['tree_category_computed'], $this->product_category_seq); // get self and child
            $category_seq_self_parent_with_name = $this->get_data_product_category_seq_array($this->data['tree_category_computed'], $seq_self_parent_category);
            $category_seq_self_with_name_data = $this->get_data_product_category_seq($this->data['tree_category_computed'], $this->product_category_seq);
            $category_seq_self_with_name = isset($category_seq_self_with_name_data->name) ? $category_seq_self_with_name_data->name : '';
            $category_id_self_parent = "'" . implode("','", $seq_self_parent_category) . "'";
            $category_id_self_child = "'" . implode("','", $seq_self_child_category) . "'";

            $data_product_category_seq = $this->get_data_product_category_seq($this->data['tree_category_computed'], $this->product_category_seq);
            $level = isset($data_product_category_seq->level) ? $data_product_category_seq->level : '';
            $title = ucwords('Jual ' . $category_seq_self_with_name . ' - Harga & Spesifikasi');
            $description = 'Jual berbagai jenis ' . $category_seq_self_with_name . '  - Hanya ada di Toko1001.id';
            $keyword = '';
        }

        //if level not found redirect error 404
        if ($level == '' AND $this->product_category_seq != 0) {
            redirect(base_url() . 'error_404');
        }

        //set param page parameter for paging
        $removed_parameter = array(START_OFFSET);
        $base_url = $this->current_url_with_query_string($removed_parameter);
        $start = ($this->input->get(START_OFFSET) == NULL) ? 0 : $this->input->get(START_OFFSET);
        $perpage = ($this->col_num * 6) + 2; //edit +2 for view category
        $params->start = $start;
        $params->limit = $perpage;
        $params->order = '';
        $params->agent = parent::is_agent() ? AGENT : '';
        $params->agent_seq = parent::is_agent() ? parent::get_agent_user_id() : "";

        //set param image
        $params_image->category_seq = $this->product_category_seq;

        if ($this->product_category_seq != 0) { //if not all category
            //set param category for filter
            $params_category->category_seq = $category_id_self_parent;
        }
        //get_attribute_value
        if ($attribute_seq_attribute_value != FALSE) {
            //check format attribute_seq_attribute_value
            if (!preg_match("/^([0-9]+-[0-9]+,*)+$/", $attribute_seq_attribute_value)) {
                //redirect when unmatched
                redirect($this->current_url_with_query_string(array(PARAMETER_CATEGORY_ATTRIBUTE)));
            }
            $attribute_seq_attribute_value = rtrim($attribute_seq_attribute_value, $exploder_parameter);
            $parameter_value_list = explode($exploder_parameter, $attribute_seq_attribute_value);
            foreach ($parameter_value_list as $each_parameter_value_list) {
                $exploder_for_parent_child = explode($exploder_parent_child, $each_parameter_value_list);
                $parent_uri = $exploder_for_parent_child[0];
                $child_uri = $exploder_for_parent_child[1];
                $query_string_parameter_name[$parent_uri][] = $child_uri;
            }
            //make query_string_unique for double value
            foreach ($query_string_parameter_name as $parent_uri => $each_query_string_parameter_name) {
                $query_string_parameter_name[$parent_uri] = array_unique($query_string_parameter_name[$parent_uri]);
            }
            //if uri has child value 0 then set parent url value to 0
            foreach ($query_string_parameter_name as $parent_uri => $each_query_string_parameter_name) {
                if (in_array('0', $query_string_parameter_name[$parent_uri])) {
                    $query_string_parameter_name[$parent_uri] = array('0');
                }
            }
            foreach ($query_string_parameter_name as $parent_uri => $each_query_string_parameter_name) {
                if (!in_array('0', $query_string_parameter_name[$parent_uri])) {
                    $query_string_parameter_name_without_0[$parent_uri] = $query_string_parameter_name[$parent_uri];
                }
            }
//            $query_string_array_combination = $this->generate_combinations($query_string_parameter_name_without_0);
        }
        //get list of product_seq
        if ($this->input->get(PARAMETER_CATEGORY_ATTRIBUTE) == TRUE) {
//            $params_product_seq->attribute_value_seq = $attribute_value;
            $product_seq = $this->M_thumbs_category->get_product_seq($query_string_parameter_name_without_0);
        }
        //set list of product seq

        if ($product_seq !== "''" AND $product_seq !== "") {
            foreach ($product_seq as $each_product_seq) {
                $list_of_product_array[] = $each_product_seq->product_seq;
            }
            if (count($list_of_product_array) > 0) {
                $list_of_product_seq = "'" . implode("','", $list_of_product_array) . "'";
            }
        }

        //set param where for product (self and child)
        $params->where = "WHERE pv.active = 1 AND pv.status IN ('L','C')";
//        if($list_of_product_seq != ""){
        if (count($query_string_parameter_name_without_0) > 0) {
            $params->where .= " AND p.seq IN (" . $list_of_product_seq . ")";
        }

        //set param where for product with attribute_value exists but list_of_product_seq is empty
        if ($list_of_product_seq == '' AND $attribute_value != '') {
            $params->where .= " AND FALSE";
        }

        if ($this->product_category_seq != 0) { //if not all category        
            if (count($seq_self_child_category) > 0) {
                $params->where .= " AND p.category_ln_seq IN(" . $category_id_self_child . ")";
            }
        }

        //with search
        if ($search != FALSE) {
            $search_string = explode(" ", htmlspecialchars($search));

            for ($i = 0; $i < count($search_string); $i++) {
                $params->where .= " AND p.name LIKE '%" . addslashes($search_string[$i]) . "%'";
            }
            $title = 'Cari ' . $search;
            $description = 'Cari ' . $search . ' di Toko1001.id';
            $keyword = '';
        }

        //with sliderprice
        if ($price_range != FALSE) {
            $exploder_price_range = explode(',', $price_range);
            $price_from = isset($exploder_price_range[0]) ? $exploder_price_range[0] : "0";
            $price_to = isset($exploder_price_range[1]) ? $exploder_price_range[1] : "0";
            $params->where .= " AND pv.sell_price BETWEEN " . $price_from . " AND " . $price_to;
        }

        //with order
        if ($order_category != FALSE) {
            switch ($order_category) {
                case NEW_TO_OLD_PRODUCT:
                    $params->order .= " ORDER BY pv.seq DESC";
                    $ordering_category = "Produk terbaru ke terlama";
                    break;
                case OLD_TO_NEW_PRODUCT:
                    $params->order .= " ORDER BY pv.seq ASC";
                    $ordering_category = "Produk terlama ke terbaru";
                    break;
                case PRICE_EXPENSIVE_TO_CHEAP:
                    $params->order .= " ORDER BY pv.sell_price DESC";
                    $ordering_category = "Harga mahal ke murah";
                    break;
                case PRICE_CHEAP_TO_EXPENSIVE:
                    $params->order .= " ORDER BY pv.sell_price ASC";
                    $ordering_category = "Harga murah ke mahal";
                    break;
            }
        }

        if ($this->product_category_seq != 0) { //if not all category
            $query['category_img'] = $this->M_thumbs_category->get_thumbs_category_img($params_image); //query image banner
            $query['filter'] = $this->M_thumbs_category->get_thumbs_category_filter($params_category); //query filter
        } else {
            $empty_image = new stdClass;
            $empty_image->banner_image = '';
            $empty_image->banner_image_url = '';
            $query['category_img'] = array($empty_image);

            $query['filter']['name'] = array();
            $query['filter']['detail'] = array();
        }

        $query['product_category'] = $this->M_thumbs_new_products->get_thumbs_new_products($params); //query product     
        $query_row = $query['product_category']['row'];
        $query_row_result = $query_row->result();
        $query_row_result_first = $query_row_result[0];
        $total_row = $query_row_result_first->total_row;

        $this->data['row'] = $total_row;
        $this->data['product'] = $query['product_category']['product'];
        $this->data['category_img'] = $query['category_img'];
        $this->data['filter'] = $query['filter'];
        $this->data['product_category_seq'] = $this->product_category_seq;
        $this->data['ordering_category'] = $ordering_category;
        $this->data['order_category'] = $order_category;
        $this->data['is_category'] = true;
        $this->data['level'] = $level;
        $this->data['seq_self_parent_category'] = $seq_self_parent_category; //not ordering 
        $this->data['category_seq_self_parent_with_name'] = $this->sort_level_category($category_seq_self_parent_with_name); //ordering
        if ($title != '') {
            $this->data['title'] = $this->create_title($title);
        }
        if ($description != '') {
            $this->data['description'] = $description;
        }
        if ($keyword != '') {
            $this->data['keyword'] = $keyword;
        }
        $this->display_page($base_url, $start, $total_row, $perpage);
        $this->template->view("home/category", $this->data);
    }

}

?>