<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class C_redirect_merchant_to_landing_page_merchant extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('merchant/profile/M_profile_merchant');
    }

    public function index() {
        
        $merchant_seq = $this->uri->segment(3);
        
        $params = new stdClass;
        $params->ip_address = "";
        $params->merchant_seq = $merchant_seq;
        $merchant_info = $this->M_profile_merchant->get_data_merchant($params);
        
        if (isset($merchant_info)) {
            redirect(base_url() . "merchant/" . $merchant_info[0][0]->code);
        } else {
            redirect(base_url() . 'error_404');
        }
    }

}