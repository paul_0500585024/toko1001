<?php

require_once CONTROLLER_BASE_HOME;

class C_cart extends controller_base_home {
    
    private $data;

    public function __construct() {
	parent::__construct("", "home/cart",false);
	//$this->load->model("system/m_login");
	$this->load->helper('url');
    }
    
    public function index(){
        $this->load->view("home/cart", $this->data);
    }
}
