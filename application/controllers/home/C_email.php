<?php

require_once CONTROLLER_BASE_HOME;

class C_email extends controller_base_home {

    private $data;
    private $product_variant_seq;

    public function __construct() {
	parent::__construct("", "home/email", false);
	$this->load->helper('url');
	$this->uri->segment(2);
	$this->initialize();
    }

    private function initialize() {

    }

    public function index() {
	$this->load->model("component/M_email");
	$params = new stdClass();
	$params->user_id = parent::get_member_email();
	$params->ip_address = parent::get_ip_address();
	$params->email_code = substr($this->uri->segment(2), 0, 50);
	$maillog = $this->M_email->get_email_log($params);
	if (isset($maillog)) {
	    $this->data['informasi'] = $maillog[0]->email_content;
//	    $this->template->view("home/email", $this->data);
	    $this->load->view("home/email", $this->data);
	} else {
	    redirect(base_url() . "error_404");
	}
    }

}
