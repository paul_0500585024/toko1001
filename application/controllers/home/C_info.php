<?php

require_once CONTROLLER_BASE_HOME;

class C_info extends controller_base_home {

    private $data;
    private $product_variant_seq;

    public function __construct() {
	parent::__construct("", "home/info", false);
	$this->load->helper('url');
	$this->data['informasi'] = $this->uri->segment(2);
	$this->initialize();
    }

    private function initialize() {

    }

    public function index() {
	$this->data['tree_category'] = $this->get_tree_category(true);
        $title = '';
        switch ($this->data['informasi']) {
            case "kebijakan-privasi":
                $title = 'kebijakan privasi';
                break;
            case "syarat-dan-ketentuan":
                $title = 'syarat dan ketentuan';
                break;
            case "panduan-belanja":
                $title = 'panduan dan belanja';
                break;
            case "tentang-kami":
                $title = 'tentang kami';
                break;
            case "pengembalian-barang":
                $title = 'pengembalian barang';
                break;
            case "pengembalian-dana":
                $title = 'pengembalian dana';
                break;
            case "bantuan":
                $title = 'bantuan';
                break; 
            case "syarat-dan-ketentuan-penggunaan-voucher":
                $title = 'syarat dan ketentuan umum penggunaan voucher';
                break;
			case "detail-paket-data-simpati":
                $title = 'Detail Paket Data Telkomsel';
                break;
            case "detail-paket-data-three":
                $title = 'Detail Paket Data three';
                break;
            case "detail-paket-data-indosat":
                $title = 'Detail Paket Data Indosat';
                break;
            default :
        }
        if($title != ''){
            $title = ucwords($title);
            $this->data['title'] = $this->create_title($title);
        }
        
	$this->template->view("home/info", $this->data);
    }

}
