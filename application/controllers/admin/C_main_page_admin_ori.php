<?php

require_once CONTROLLER_BASE_ADMIN;

class C_main_page_admin Extends controller_base_admin {

    private $data;

    public function __construct() {

        $this->data = parent::__construct("", "admin/main_page");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('admin/M_main_page_admin');
    }

    public function index() {

        $this->get_data();
        $this->get_chart();
        $this->load->view('admin/main_page_admin', $this->data);
    }

    public function sign_out() {

        unset($_SESSION[SESSION_ADMIN_UID]);
        unset($_SESSION[SESSION_ADMIN_UNAME]);
        unset($_SESSION[SESSION_ADMIN_USER_GROUP]);
        unset($_SESSION[SESSION_ADMIN_FORM_AUTH]);
        unset($_SESSION[SESSION_ADMIN_CSRF_TOKEN]);
        unset($_SESSION[SESSION_IP_ADDR]);
        unset($_SESSION[SESSION_ADMIN_LEFT_NAV]);

        $this->data[DATA_ERROR] = False;
        $this->load->view('admin/login_admin', $this->data);
    }

    public function get_data() {

        $params = new stdClass();
        $params->payment_status = PAYMENT_CONFIRM_STATUS_CODE;
        $params->trx_type = WITHDRAW_ACCOUNT_TYPE;
        $params->withdraw_status = NEW_STATUS_CODE;
        $params->new_merchant = WEB_STATUS_CODE;
        $params->product_status = NEW_STATUS_CODE;
        $params->cancel_trx_type = CANCEL_ACCOUNT_TYPE;
        $params->new_status_code = NEW_STATUS_CODE;
        $params->order_status = PROCESS_STATUS_CODE;

        try {
            $list_data = $this->M_main_page_admin->get_data($params);
            parent::set_list_data($this->data, $list_data);
            $this->data[ORDER_NEED_CONFIRMATION] = $list_data[0][0]->order;
            $this->data[WITHDRAW_NEED_CONFIRMATION] = $list_data[1][0]->withdraw;
            $this->data[NEW_MERCHANT] = $list_data[2][0]->new_merchant;
            $this->data[NEW_PRODUCT] = $list_data[3][0]->product;
            $this->data[PENDING_REFUND] = $list_data[4][0]->pending_refund;
            $this->data[PENDING_DELIVERY] = $list_data[5][0]->pending_delivery;
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    public function get_chart() {

        $this->chart_member();
        $this->chart_merchant();
    }

    public function chart_member() {
        $params = new stdClass();
        $params->january_from = '2016-01-01 00:00:00';
        $params->january_to = '2016-01-31 00:00:00';
        $params->february_from = '2016-02-01 00:00:00';
        $params->february_to = '2016-02-31 00:00:00';
        $params->march_from = '2016-03-01 00:00:00';
        $params->march_to = '2016-03-31 00:00:00';
        $params->april_from = '2016-04-01 00:00:00';
        $params->april_to = '2016-04-31 00:00:00';
        $params->may_from = '2016-05-01 00:00:00';
        $params->may_to = '2016-05-31 00:00:00';
        $params->june_from = '2016-06-01 00:00:00';
        $params->june_to = '2016-06-31 00:00:00';
        $params->july_from = '2016-07-01 00:00:00';
        $params->july_to = '2016-07-31 00:00:00';
        $params->august_from = '2016-08-01 00:00:00';
        $params->august_to = '2016-08-31 00:00:00';
        $params->septebmer_from = '2016-09-01 00:00:00';
        $params->septebmer_to = '2016-09-31 00:00:00';
        $params->october_from = '2016-10-01 00:00:00';
        $params->october_to = '2016-10-31 00:00:00';
        $params->november_from = '2016-11-01 00:00:00';
        $params->november_to = '2016-11-31 00:00:00';
        $params->december_from = '2016-12-01 00:00:00';
        $params->december_to = '2016-12-31 00:00:00';

        try {
            $list_data = $this->M_main_page_admin->charts_member($params);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $key = $list_data[0];

        $this->data['member_january'] = $key[0]->january;
        $this->data['member_february'] = $key[0]->february;
        $this->data['member_march'] = $key[0]->march;
        $this->data['member_april'] = $key[0]->april;
        $this->data['member_may'] = $key[0]->may;
        $this->data['member_june'] = $key[0]->june;
        $this->data['member_july'] = $key[0]->july;
        $this->data['member_august'] = $key[0]->august;
        $this->data['member_september'] = $key[0]->september;
        $this->data['member_october'] = $key[0]->october;
        $this->data['member_november'] = $key[0]->november;
        $this->data['member_december'] = $key[0]->december;
    }

    public function chart_merchant() {

        $params = new stdClass();
        $params->january_from = '2016-01-01 00:00:00';
        $params->january_to = '2016-01-31 00:00:00';
        $params->february_from = '2016-02-01 00:00:00';
        $params->february_to = '2016-02-31 00:00:00';
        $params->march_from = '2016-03-01 00:00:00';
        $params->march_to = '2016-03-31 00:00:00';
        $params->april_from = '2016-04-01 00:00:00';
        $params->april_to = '2016-04-31 00:00:00';
        $params->may_from = '2016-05-01 00:00:00';
        $params->may_to = '2016-05-31 00:00:00';
        $params->june_from = '2016-06-01 00:00:00';
        $params->june_to = '2016-06-31 00:00:00';
        $params->july_from = '2016-07-01 00:00:00';
        $params->july_to = '2016-07-31 00:00:00';
        $params->august_from = '2016-08-01 00:00:00';
        $params->august_to = '2016-08-31 00:00:00';
        $params->septebmer_from = '2016-09-01 00:00:00';
        $params->septebmer_to = '2016-09-31 00:00:00';
        $params->october_from = '2016-10-01 00:00:00';
        $params->october_to = '2016-10-31 00:00:00';
        $params->november_from = '2016-11-01 00:00:00';
        $params->november_to = '2016-11-31 00:00:00';
        $params->december_from = '2016-12-01 00:00:00';
        $params->december_to = '2016-12-31 00:00:00';

        try {
            $list_data = $this->M_main_page_admin->charts_merchant($params);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $key = $list_data[0];

        $this->data['merchant_january'] = $key[0]->january;
        $this->data['merchant_february'] = $key[0]->february;
        $this->data['merchant_march'] = $key[0]->march;
        $this->data['merchant_april'] = $key[0]->april;
        $this->data['merchant_may'] = $key[0]->may;
        $this->data['merchant_june'] = $key[0]->june;
        $this->data['merchant_july'] = $key[0]->july;
        $this->data['merchant_august'] = $key[0]->august;
        $this->data['merchant_september'] = $key[0]->september;
        $this->data['merchant_october'] = $key[0]->october;
        $this->data['merchant_november'] = $key[0]->november;
        $this->data['merchant_december'] = $key[0]->december;
    }

}

?>
