<?php

require_once CONTROLLER_BASE_ADMIN;

class C_attribute_category_admin extends controller_base_admin {

    private $data;
    private $parent_level;

    public function __construct() {
        $this->data = parent::__construct("MST02003", "admin/master/attribute_category");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('admin/master/M_attribute_category_admin');
        $this->load->model('component/M_tree_view');
        $this->load->model('component/M_check_list');
        $this->load->model('component/M_dropdown_list');

        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_DELETE, "save_delete");
        parent::register_event($this->data, ACTION_ADDITIONAL, "get_attribute_list");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {

        $menulst = $this->M_tree_view->get_menu_product_category();

        $this->data[TREE_MENU] = $this->get_menu($menulst, 0);
        $this->data[CATEGORY_NAME] = $this->M_dropdown_list->get_dropdown_product_category();
        $this->data[ATTRIBUTE_NAME] = $this->M_dropdown_list->get_dropdown_attribute();

        if (!$this->input->post()) {
            $this->data['filter'] = 'admin/master/attribute_category_admin_f.php';
            $this->load->view("admin/master/attribute_category_admin", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/master/attribute_category_admin", $this->data);
                    }
                } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                    $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                    $this->load->view("admin/master/attribute_category_admin", $this->data);
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] != ACTION_ADDITIONAL) {
                        if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                            $this->data[DATA_SUCCESS][SUCCESS] = true;
                            $this->data[DATA_AUTH][FORM_ACTION] = "";
                        }
                        $admin_info[SESSION_DATA] = $this->data;
                        $this->session->set_userdata($admin_info);
                        redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                    }
                }
            }
        }
    }

    public function search() {
        $filter = new stdClass;
        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->category_seq = parent::get_input_post("category_seq");
        $filter->attribute_seq = parent::get_input_post("attribute_seq");

        try {
            $list_data = $this->M_attribute_category_admin->get_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );

        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->parent_seq . "/" . $data_row->category_name . "/" . $data_row->attribute_seq,
                    "category_name" => $data_row->category_name,
                    "name" => $data_row->name,
                    "created_by" => $data_row->created_by,
                    "created_date" => $data_row->created_date,
                    "modified_by" => $data_row->modified_by,
                    "modified_date" => $data_row->modified_date);
                $output['aaData'][] = $row;
            }
        };

        echo json_encode($output);
    }

    function get_menu($datas, $parent = 0, $p_level = 0) {
        static $i = 1;
        $tab = str_repeat(" ", $i);
        if (isset($datas[$parent])) {
            $html = "<ul id='menu-tree'>";
            $i++;
            foreach ($datas[$parent] as $vals) {

                if ($vals->level == 1) {
                    $this->parent_level = $vals->seq;
                    $p_level = $vals->seq;
                }
                $child = $this->get_menu($datas, $vals->seq, $this->parent_level);
                $html .= "$tab";
                $html.='<li><a href="javascript:pilihmenu(\'' . $vals->seq . '~' . $vals->level . '~' . $vals->name . '~' . $p_level . '\')">' . $vals->name . '</a>';
                if ($child) {
                    $i++;
                    $html .= $child;
                    $html .= "$tab";
                }
                $html .= '</li>';
            }
            $html .= "$tab</ul>";
            return $html;
        } else {
            return false;
        }
    }

    protected function get_attribute_list() {
        $selected = new stdClass;
        $selected->seq = parent::get_input_post("seq");
        $selected->parent_seq = parent::get_input_post("parent_seq");
        $this->data[ATTRIBUTE_LIST] = $this->M_check_list->get_attribute_check_list($selected);
        return $this->load->view("admin/component/checklist/check_attribute", $this->data);
        exit();
    }

    protected function save_add() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = "";
        $params->name = "";
        $params->parent_seq = parent::get_input_post("parent_seq", true, FILL_VALIDATOR, "Kategori", $this->data);
        $params->attribute_seq = $this->input->post("attribute_seq");

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_attribute_category_admin->trans_begin();
                $this->M_attribute_category_admin->save_add($params);
                $this->M_attribute_category_admin->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_attribute_category_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_attribute_category_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_attribute_category_admin->trans_rollback();
            }
        }
    }

    protected function get_edit() {
        $selected = new stdClass();
        $key = explode("/", parent::get_input_post("key"));
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = $key[0];

        try {
            $this->data[ATTRIBUTE_LIST] = $this->M_attribute_category_admin->get_data($selected);
            $this->data[DATA_SELECTED][LIST_DATA][0]->key = $key[0];
            $this->data[DATA_SELECTED][LIST_DATA][0]->name = $key[1];
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

}

?>
