<?php

require_once CONTROLLER_BASE_ADMIN;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Product New
 *
 * @author Jartono
 */
class C_product_new_admin extends controller_base_admin {

    private $data;
    private $parent_level1;
    private $parent_level2;

    public function __construct() {
        $this->data = parent::__construct("MST02007", "admin/master/product_new");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('admin/master/M_product_new_admin');
        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_APPROVE, "approve");
        parent::register_event($this->data, ACTION_REJECT, "reject");
        parent::register_event($this->data, ACTION_ADDITIONAL, "get_ajax");
        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        $filter = new stdClass;
        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->merchant_name = '';
        $merchant_list = $this->M_product_new_admin->get_merchant_by_name($filter);
        $this->data[MERCHANT_LIST] = $merchant_list;
        if (!$this->input->post()) {
            $this->data['filter'] = 'admin/master/product_new_admin_f.php';
            $this->load->view("admin/master/product_new_admin", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/master/product_new_admin", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/master/product_new_admin", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url("admin/master/product_new"));
                }
            }
        }
    }

    protected function get_ajax() {

        $tipe = parent::get_input_post("tipe");
        $idh = parent::get_input_post("idh");
        $filter = new stdClass;
        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();

        switch ($tipe) {
            case "category":
                $output = array();
                $merchsq = array();
                $merchantlvl1 = $this->M_product_new_admin->get_merchant_lvl1($idh);
                $menulst = $this->M_product_new_admin->get_product_category();
                if (isset($merchantlvl1)) {
                    foreach ($merchantlvl1 as $datar) {
                        $merchsq[] = $datar->category_l1_seq;
                    }
                    if (isset($menulst)) {
                        foreach ($menulst as $data_row) {

                            if (in_array($data_row->seq, $merchsq)) {
                                $output[$data_row->parent_seq][] = $data_row;
                            }
                            if (in_array($data_row->parent_seq, $merchsq)) {
                                $output[$data_row->parent_seq][] = $data_row;
                                $merchsq[] = $data_row->seq;
                            }
                        }
                    }
                }
                die($this->get_treecategory($output, 0));
                break;
            case "breadcrumb":
                $menulst = $this->M_product_new_admin->get_product_category_by_child($idh);
                die($this->get_breadcrumb($menulst));
                break;
            case "attribute":
                $filter->idh = $idh;
                $atrval = parent::get_input_post("atrval");
                $menulst = $this->M_product_new_admin->get_attribute_by_category($filter);
                die($this->get_atribute($menulst, $atrval));
                break;
            case "variant":
                $idh = explode(",", $idh);
                $filter->idh = $idh[1];
                $menulst = $this->M_product_new_admin->get_variant_by_category($filter);
                die($this->get_varian($menulst));
                break;
        }
    }

    function get_varian($idvar) {
        $data = '<div class ="form-group">
	    <label class ="col-md-2">';
        $i = 0;
        if ($idvar) {
            foreach ($idvar as $data_row) {
                $i++;
                if ($i == 1)
                    $data.=$data_row->display_name . '</label><div class ="col-md-8 input-group"><select class="form-control select2" name="varianval" id="varianval">';
                $data.="<option value=" . $data_row->seq . ">" . $data_row->value . "</option>";
            }
            $data.='</select>
	    <span class="input-group-btn"><a href="javascript:adcvclick();" id="addCV" class="btn btn-info">Tambah</a></span></div></div>';
        }else {
            $data = 'Data varian tidak ada';
        }
        return $data;
    }

    function get_breadcrumb($idcat) {
        $data = '<h4 style="margin-left:10px"><span class="label label-primary">' . $idcat[0]->path . '<input type="hidden" name="alcat" id="alcat" value="' . $idcat[0]->idpath . '"></span></h4>';
        return $data;
    }

    function get_atribute($idattribute, $atrval = '') {

        $data = "<table class='table table-striped'>";
        $row = "";
        $i = 0;
        $slected = "";
        $selected = new stdClass;
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        if ($idattribute) {
            foreach ($idattribute as $data_row) {
                $data.="<tr><td>" . $data_row->name . "</td><td><select id='attribut' name='attribut[]' class='form-control'><option value=''></option>";
                $selected->seq = $data_row->seq;
                $sel_data = $this->M_product_new_admin->get_attribute_value($selected);
                if (isset($sel_data)) {
                    foreach ($sel_data as $data_rows) {
                        if ($data_rows->seq != '') {
                            $slected = "";
                            $nilai = '{' . $data_rows->seq . '}';
                            $slected = strripos($atrval, $nilai);
                            if ($slected === false) {
                                $slected = "";
                            } else {
                                $slected = " selected";
                            }
                        }
                        $data.="<option value='" . $data_rows->seq . "'" . $slected . ">" . $data_rows->value . "</option>";
                    }
                }
                $data.="</select></td></tr>";
            }
        }
        $data.="</table>";
        return $data;
    }

    function get_treecategory($datas, $parent = 0, $p_level1 = 0, $p_level2 = 0) {
        static $i = 1;
        if (isset($datas[$parent])) {
            $html = "<ul type=disc>";
            $i++;
            foreach ($datas[$parent] as $vals) {
                $g = $i;
                if ($vals->level == 1) {
                    $this->parent_level1 = $vals->seq;
                    $p_level1 = $vals->seq;
                }
                if ($vals->level == 2) {
                    $this->parent_level2 = $vals->seq;
                    $p_level2 = $vals->seq;
                }
                $child = $this->get_treecategory($datas, $vals->seq, $this->parent_level1, $this->parent_level2);
                if ($i != $g) {
                    $html .= '<li><label for="folder' . $i . '"><b>' . $vals->name . '</b></label>';
                } else {
                    $html.='<li><input onclick="cekseq(' . $vals->seq . ')" type="radio" id="folder' . $vals->seq . '" class="radiobtn" name="catseq" value="' . $vals->seq . '~' . $p_level1 . '~' . $p_level2 . '" required  /> ' . $vals->name;
                }
                if ($child) {
                    $i++;
                    $html .= $child;
                }
                $html .= '</li>';
            }
            $html .= "</ul>";
            return $html;
        } else {
            return false;
        }
    }

    public function search() {

        $filter = new stdClass;
        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->name = parent::get_input_post("name");
        $filter->description = parent::get_input_post("description");
        $filter->merchant_seq = parent::get_input_post("merchant_info_seq");
        $filter->merchant_name = parent::get_input_post("merchant_name");
        $filter->status = parent::get_input_post("status");

        try {
            $list_data = $this->M_product_new_admin->get_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );
        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->seq,
                    "merchant_name" => parent::cdef($data_row->merchant_name),
                    "merchant_seq" => $data_row->merchant_seq,
                    "seq" => $data_row->seq,
                    "description" => parent::cdef($data_row->description),
                    "name" => parent::cdef($data_row->name),
                    "status" => parent::cstdes($data_row->status, STATUS_FNR),
                    "created_by" => $data_row->created_by,
                    "created_date" => $data_row->created_date,
                    "modified_by" => $data_row->modified_by,
                    "modified_date" => parent::cdate($data_row->modified_date, 1)
                );
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
    }

    protected function get_edit() {
        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->seq = parent::get_input_post("key");

        try {
            $sel_data = $this->M_product_new_admin->get_data($selected);
            $sel_data1 = $this->M_product_new_admin->get_product_attribute_new_by_product_seq($selected);
            $sel_data2 = $this->M_product_new_admin->get_product_spec_new_by_product_seq($selected);
            $sel_data3 = $this->M_product_new_admin->get_product_variant_new_by_product_seq($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
                $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data1;
                $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data2;
                $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data3;
            } else {
                redirect(base_url("admin/master/product_new"));
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_add() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->merchant_seq = parent::get_input_post("merchant_seq_val", true, FILL_VALIDATOR, "Merchant", $this->data);
        $params->name = parent::get_input_post("name", true, FILL_VALIDATOR, "Nama Produk", $this->data);
        $params->include_ins = parent::get_input_post("include_ins");
        $catseq = parent::get_input_post("catseqval");
        if ($catseq != '') {
            $categoryseq = explode("~", $catseq);
            $params->category_l2_seq = $categoryseq[2];
            $params->category_ln_seq = $categoryseq[0];
        } else {
            $params->category_l2_seq = 0;
            $params->category_ln_seq = 0;
        }
        $oldfile = "";
        $params->notes = parent::get_input_post("notes");
        $params->description = parent::get_input_post("description", true, FILL_VALIDATOR, "Deskripsi", $this->data);
        $params->content = parent::get_input_post("content");
        $params->specification = parent::get_input_post("specification");
        $params->warranty_notes = parent::get_input_post("warranty_notes");
        $params->p_length_cm = parent::get_input_post("p_length_cm");
        $params->p_width_cm = parent::get_input_post("p_width_cm");
        $params->p_height_cm = parent::get_input_post("p_height_cm");
        $params->p_weight_kg = parent::get_input_post("p_weight_kg", true, QTY_VALIDATOR, "Berat Produk", $this->data);
        $params->b_length_cm = parent::get_input_post("p_length_cm");
        $params->b_width_cm = parent::get_input_post("p_width_cm");
        $params->b_height_cm = parent::get_input_post("p_height_cm");
        $params->b_weight_kg = parent::get_input_post("p_weight_kg");
        $params->attribut = $this->input->post("attribut");
        $params->spekname = $this->input->post("spekname");
        $params->spekval = $this->input->post("spekval");
        $params->varian = $this->input->post("varian");
        $params->variant_value_seq = $this->input->post("variant_value_seq");
        $params->disc_percent = $this->input->post("disc_percent");
        $params->product_price = $this->input->post("product_price");
        $params->sell_price = $this->input->post("sell_price");
        $params->order = $this->input->post("order");
        $params->max_buy = $this->input->post("max_buy");
        $params->variant_seq = $this->input->post("variant_seq");
        if (isset($params->product_price[$params->varian])) {
            if ($params->product_price[$params->varian] == 0) {
                $this->data[DATA_ERROR][ERROR] = true;
                $this->data[DATA_ERROR][ERROR_MESSAGE][] = "Harga Produk dan Harga Promo " . ERROR_VALIDATION_MUST_FILL;
            }
        } else {
            $this->data[DATA_ERROR][ERROR] = true;
            $this->data[DATA_ERROR][ERROR_MESSAGE][] = "Harga Produk dan Harga Promo " . ERROR_VALIDATION_MUST_FILL;
        }
        $params->values = $this->input->post("value");
        $params->status = parent::get_input_post("status");
        $maxseq = 1;
        $maxseq = $this->M_product_new_admin->get_max_seq('m_product_variant_new');
        if (isset($maxseq)) {
            $maxseq = ($maxseq[0]->seq + 1);
        }
        $filesave = array();
        $fils = "";
        for ($imgs = 0; $imgs < count($this->input->post("variant_value_seq")); $imgs++) {
            $fils = "OK";
            for ($nomori = 1; $nomori < 7; $nomori++) {
                if (!empty($_FILES['ifile' . $nomori]['tmp_name'][$imgs])) {
                    $idfile = $_FILES['ifile' . $nomori]['tmp_name'][$imgs];
                    $string = preg_replace("/[^A-Za-z0-9 ]/", '', $params->name);
                    $string = str_ireplace(" ", "_", $string);
                    $name = $_FILES['ifile' . $nomori]["name"][$imgs];
                    if (preg_match("/\.(" . IMAGE_TYPE . ")$/", $name)) {
                        $ext = end((explode(".", $name)));
                        $namafile = strtolower($maxseq . "_" . $string . "_" . $nomori . '.' . $ext);
                        $namafolder = TEMP_PRODUCT_UPLOAD_IMAGE . $params->merchant_seq;
                        if (!file_exists($namafolder)) {
                            mkdir($namafolder, 0777, true);
                        }
                        $namafile = parent::upload_product($nomori, $imgs, $namafolder, $namafile, $this->data);
                        //move_uploaded_file($idfile, $namafolder . '/' . $namafile);
                        $filesave[$nomori][$imgs] = $namafile;
                    } else {
                        $this->data[DATA_ERROR][ERROR] = true;
                        $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_VALIDATION_UPLOAD_TYPE_DOESNT_MATCH;
                        $filesave[$nomori][$imgs] = "";
                    }
                } else {
                    $oldfile = $this->input->post("oldfile" . $nomori)[$imgs];
                    $filesave[$nomori][$imgs] = $oldfile;
                }
            }
            $maxseq++;
        }
        if ($fils == "") {
            $filesave[1] = '';
            $filesave[2] = '';
            $filesave[3] = '';
            $filesave[4] = '';
            $filesave[5] = '';
            $filesave[6] = '';
        }
        $params->file_name1 = $filesave[1];
        $params->file_name2 = $filesave[2];
        $params->file_name3 = $filesave[3];
        $params->file_name4 = $filesave[4];
        $params->file_name5 = $filesave[5];
        $params->file_name6 = $filesave[6];
        $params->catseqval = $this->input->post("catseqval");
        $params->alcat = $this->input->post("alcat");
        $this->data[DATA_SELECTED][LIST_DATA][] = $params;
        if ($params->attribut != '') {
            foreach ($params->attribut as $key => $val) {
                $this->data[DATA_SELECTED][LIST_DATA][1][] = (object) array("attribute_value_seq" => $params->attribut[$key]);
            }
        }
        foreach ($params->spekname as $key => $val) {
            if ($params->spekname[$key] != '' && $params->spekval[$key] != '')
                $this->data[DATA_SELECTED][LIST_DATA][2][] = (object) array("name" => $params->spekname[$key], "value" => $params->spekval[$key]);
        }

        if ($params->variant_value_seq != '') {
            foreach ($params->variant_value_seq as $key => $val) {
                if ($params->varian == 1) {
                    if ($params->variant_value_seq[$key] <= "1") {
                        continue;
                    } else {
                        if ($params->product_price[$key] != "" && $params->product_price[$key] != "0" && $params->sell_price[$key] != "" && $params->sell_price[$key] != "0") {
                            
                        } else {
                            $this->data[DATA_ERROR][ERROR] = true;
                            $this->data[DATA_ERROR][ERROR_MESSAGE][] = "Harga Produk dan Harga Promo " . ERROR_VALIDATION_MUST_FILL;
                        }
                        $this->data[DATA_SELECTED][LIST_DATA][3][] = (object) array("value" => $params->values[$key], "seq" => $params->variant_seq[$key], "variant_value_seq" => $params->variant_value_seq[$key], "disc_percent" => $params->disc_percent[$key], "product_price" => $params->product_price[$key], "sell_price" => $params->sell_price[$key], "order" => $params->order[$key], "max_buy" => $params->max_buy[$key], "variant_seq" => $params->variant_seq[$key], "pic_1_img" => $params->file_name1[$key], "pic_2_img" => $params->file_name2[$key], "pic_3_img" => $params->file_name3[$key], "pic_4_img" => $params->file_name4[$key], "pic_5_img" => $params->file_name5[$key], "pic_6_img" => $params->file_name6[$key]);
                        if ($params->file_name1[$key] == "") {
                            $this->data[DATA_ERROR][ERROR] = true;
                            $this->data[DATA_ERROR][ERROR_MESSAGE][] = "ERROR : Gambar Utama " . ERROR_VALIDATION_MUST_FILL;
                        }
                    }
                } else {
                    if ($params->product_price[$key] != "" && $params->product_price[$key] != "0" && $params->sell_price[$key] != "" && $params->sell_price[$key] != "0") {
                        
                    } else {
                        $this->data[DATA_ERROR][ERROR] = true;
                        $this->data[DATA_ERROR][ERROR_MESSAGE][] = "Harga Produk dan Harga Promo " . ERROR_VALIDATION_MUST_FILL;
                    }
                    $this->data[DATA_SELECTED][LIST_DATA][3][] = (object) array("value" => $params->values[$key], "seq" => $params->variant_seq[$key], "variant_value_seq" => $params->variant_value_seq[$key], "disc_percent" => $params->disc_percent[$key], "product_price" => $params->product_price[$key], "sell_price" => $params->sell_price[$key], "order" => $params->order[$key], "max_buy" => $params->max_buy[$key], "variant_seq" => $params->variant_seq[$key], "pic_1_img" => $params->file_name1[$key], "pic_2_img" => $params->file_name2[$key], "pic_3_img" => $params->file_name3[$key], "pic_4_img" => $params->file_name4[$key], "pic_5_img" => $params->file_name5[$key], "pic_6_img" => $params->file_name6[$key]);
                    if ($params->file_name1[$key] == "") {
                        $this->data[DATA_ERROR][ERROR] = true;
                        $this->data[DATA_ERROR][ERROR_MESSAGE][] = "ERROR : Gambar Utama " . ERROR_VALIDATION_MUST_FILL;
                    }
                }
                if ($params->disc_percent[$key] < 0) {
                    $this->data[DATA_ERROR][ERROR] = true;
                    $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_DISCOUNT;
                }
            }
        }

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_product_new_admin->trans_begin();
                $product_new = $this->M_product_new_admin->save_add($params);
                $params->product_seq = $product_new[0]->new_seq;
                $this->M_product_new_admin->save_add_attribute($params);
                $this->M_product_new_admin->save_add_spec($params);
                $this->M_product_new_admin->save_add_variant($params);
                $this->M_product_new_admin->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_product_new_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_product_new_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_product_new_admin->trans_rollback();
            }
        }
    }

    protected function save_update() {

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->merchant_seq = parent::get_input_post("merchant_seq_val");
        $params->seq = parent::get_input_post("seq");
        $params->name = parent::get_input_post("name", true, FILL_VALIDATOR, "Nama Produk", $this->data);
        $params->include_ins = parent::get_input_post("include_ins");
        $params->notes = parent::get_input_post("notes");
        $params->description = parent::get_input_post("description", true, FILL_VALIDATOR, "Deskripsi", $this->data);
        $params->content = parent::get_input_post("content");
        $params->specification = parent::get_input_post("specification");
        $params->warranty_notes = parent::get_input_post("warranty_notes");
        $params->p_length_cm = parent::get_input_post("p_length_cm");
        $params->p_width_cm = parent::get_input_post("p_width_cm");
        $params->p_height_cm = parent::get_input_post("p_height_cm");
        $params->p_weight_kg = parent::get_input_post("p_weight_kg", true, QTY_VALIDATOR, "Berat Produk", $this->data);
        $params->b_length_cm = parent::get_input_post("p_length_cm");
        $params->b_width_cm = parent::get_input_post("p_width_cm");
        $params->b_height_cm = parent::get_input_post("p_height_cm");
        $params->b_weight_kg = parent::get_input_post("p_weight_kg");
        $params->attribut = $this->input->post("attribut");
        $params->spekname = $this->input->post("spekname");
        $params->spekval = $this->input->post("spekval");
        $params->varian = $this->input->post("varian");
        $params->variant_value_seq = $this->input->post("variant_value_seq");
        $params->disc_percent = $this->input->post("disc_percent");
        $params->product_price = $this->input->post("product_price");
        $params->sell_price = $this->input->post("sell_price");
        $params->order = $this->input->post("order");
        $params->max_buy = $this->input->post("max_buy");
        $params->variant_seq = $this->input->post("variant_seq");
        $params->value = $this->input->post("value");
        $params->status = parent::get_input_post("status");
        $params->category_ln_seq = parent::get_input_post("category_ln_seq");
        $filesave = array();
        $maxseq = 1;
        $params->product_seq = $params->seq;
        $this->M_product_new_admin->trans_begin();
        $this->M_product_new_admin->save_delete_variant($params);
        $maxseq = $this->M_product_new_admin->get_max_seq('m_product_variant_new');
        if (isset($params->product_price[$params->varian])) {
            if ($params->product_price[$params->varian] == 0) {
                $this->data[DATA_ERROR][ERROR] = true;
                $this->data[DATA_ERROR][ERROR_MESSAGE][] = "Harga Produk dan Harga Promo " . ERROR_VALIDATION_MUST_FILL;
            }
        } else {
            $this->data[DATA_ERROR][ERROR] = true;
            $this->data[DATA_ERROR][ERROR_MESSAGE][] = "Harga Produk dan Harga Promo " . ERROR_VALIDATION_MUST_FILL;
        }
        if (isset($maxseq)) {
            $maxseq = ($maxseq[0]->seq + 1);
        }
        $namafolder = TEMP_PRODUCT_UPLOAD_IMAGE . $params->merchant_seq;
        if (!file_exists($namafolder)) {
            mkdir($namafolder, 0777, true);
        }
        $fils = "";
        for ($imgs = 0; $imgs < count($this->input->post("variant_value_seq")); $imgs++) {
            $fils = "OK";
            for ($nomori = 1; $nomori < 7; $nomori++) {
                $oldfile = $this->input->post("oldfile" . $nomori)[$imgs];
                if (!empty($_FILES['ifile' . $nomori]['tmp_name'][$imgs])) {
                    $idfile = $_FILES['ifile' . $nomori]['tmp_name'][$imgs];
                    $string = preg_replace("/[^A-Za-z0-9 ]/", '', $params->name);
                    $string = str_ireplace(" ", "_", $string);
                    $name = $_FILES['ifile' . $nomori]["name"][$imgs];
                    if (preg_match("/\.(" . IMAGE_TYPE . ")$/", $name)) {
                        $ext = end((explode(".", $name)));
                        $namafile = strtolower($maxseq . "_" . $string . "_" . $nomori . '.' . $ext);
                        if ($oldfile != '') {
                            if (file_exists($namafolder . '/' . $oldfile))
                                unlink($namafolder . '/' . $oldfile);
                        }
                        $namafile = parent::upload_product($nomori, $imgs, $namafolder, $namafile, $this->data);
                        //move_uploaded_file($idfile, $namafolder . '/' . $namafile);
                        $filesave[$nomori][$imgs] = $namafile;
                    } else {
                        $this->data[DATA_ERROR][ERROR] = true;
                        $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_VALIDATION_UPLOAD_TYPE_DOESNT_MATCH;
                        $filesave[$nomori][$imgs] = "";
                    }
                } else {
                    $filesave[$nomori][$imgs] = $oldfile;
                }
            }
            $maxseq++;
        }
        if ($fils == "") {
            $filesave[1] = '';
            $filesave[2] = '';
            $filesave[3] = '';
            $filesave[4] = '';
            $filesave[5] = '';
            $filesave[6] = '';
        }
        $params->file_name1 = $filesave[1];
        $params->file_name2 = $filesave[2];
        $params->file_name3 = $filesave[3];
        $params->file_name4 = $filesave[4];
        $params->file_name5 = $filesave[5];
        $params->file_name6 = $filesave[6];

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;
        foreach ($params->attribut as $key => $val) {
            $this->data[DATA_SELECTED][LIST_DATA][1][] = (object) array("attribute_value_seq" => $params->attribut[$key]);
        }
        foreach ($params->spekname as $key => $val) {
            if ($params->spekname[$key] != '' && $params->spekval[$key] != '')
                $this->data[DATA_SELECTED][LIST_DATA][2][] = (object) array("name" => $params->spekname[$key], "value" => $params->spekval[$key]);
        }
        if ($params->variant_value_seq != '') {
            foreach ($this->input->post("variant_value_seq") as $key => $val) {
                if ($params->varian == 1) {
                    if ($params->variant_value_seq[$key] <= "1") {
                        continue;
                    } else {
                        if ($params->product_price[$key] != "" && $params->product_price[$key] != "0" && $params->sell_price[$key] != "" && $params->sell_price[$key] != "0") {
                            
                        } else {
                            $this->data[DATA_ERROR][ERROR] = true;
                            $this->data[DATA_ERROR][ERROR_MESSAGE][] = "Harga Produk dan Harga Promo " . ERROR_VALIDATION_MUST_FILL;
                        }
                        $this->data[DATA_SELECTED][LIST_DATA][3][] = (object) array(
                                    "value" => $params->value[$key],
                                    "seq" => $params->variant_seq[$key],
                                    "variant_value_seq" => $params->variant_value_seq[$key],
                                    "disc_percent" => $params->disc_percent[$key],
                                    "product_price" => $params->product_price[$key],
                                    "sell_price" => $params->sell_price[$key],
                                    "order" => $params->order[$key],
                                    "max_buy" => $params->max_buy[$key],
                                    "variant_seq" => $params->variant_seq[$key],
                                    "pic_1_img" => $params->file_name1[$key],
                                    "pic_2_img" => $params->file_name2[$key],
                                    "pic_3_img" => $params->file_name3[$key],
                                    "pic_4_img" => $params->file_name4[$key],
                                    "pic_5_img" => $params->file_name5[$key],
                                    "pic_6_img" => $params->file_name6[$key]);
                        if ($params->file_name1[$key] == "") {
                            $this->data[DATA_ERROR][ERROR] = true;
                            $this->data[DATA_ERROR][ERROR_MESSAGE][] = "ERROR : Gambar Utama " . ERROR_VALIDATION_MUST_FILL;
                        }
                    }
                } else {
                    if ($params->product_price[$key] != "" && $params->product_price[$key] != "0" && $params->sell_price[$key] != "" && $params->sell_price[$key] != "0") {
                        
                    } else {
                        $this->data[DATA_ERROR][ERROR] = true;
                        $this->data[DATA_ERROR][ERROR_MESSAGE][] = "Harga Produk dan Harga Promo " . ERROR_VALIDATION_MUST_FILL;
                    }
                    if ($params->file_name1[$key] == "") {
                        $this->data[DATA_ERROR][ERROR] = true;
                        $this->data[DATA_ERROR][ERROR_MESSAGE][] = "ERROR : Gambar Utama " . ERROR_VALIDATION_MUST_FILL;
                    }
                    $this->data[DATA_SELECTED][LIST_DATA][3][] = (object) array(
                                "value" => $params->value[$key],
                                "seq" => $params->variant_seq[$key],
                                "variant_value_seq" => $params->variant_value_seq[$key],
                                "disc_percent" => $params->disc_percent[$key],
                                "product_price" => $params->product_price[$key],
                                "sell_price" => $params->sell_price[$key],
                                "order" => $params->order[$key],
                                "max_buy" => $params->max_buy[$key],
                                "variant_seq" => $params->variant_seq[$key],
                                "pic_1_img" => $params->file_name1[$key],
                                "pic_2_img" => $params->file_name2[$key],
                                "pic_3_img" => $params->file_name3[$key],
                                "pic_4_img" => $params->file_name4[$key],
                                "pic_5_img" => $params->file_name5[$key],
                                "pic_6_img" => $params->file_name6[$key]);
                }
                if ($params->disc_percent[$key] < 0) {
                    $this->data[DATA_ERROR][ERROR] = true;
                    $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_DISCOUNT;
                }
            }
        }

        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->seq = parent::get_input_post("seq");
        $sel_data = $this->M_product_new_admin->get_data($selected);

        if (!isset($sel_data) || $sel_data[0]->status != NEW_STATUS_CODE) {
            $this->data[DATA_ERROR][ERROR] = true;
            $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_UPDATE;
        }

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_product_new_admin->save_update($params);
                $params->product_seq = $params->seq;
                $this->M_product_new_admin->save_add_attribute($params);
                $this->M_product_new_admin->save_add_spec($params);
                $this->M_product_new_admin->save_add_variant($params);
                $this->M_product_new_admin->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_product_new_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_product_new_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_product_new_admin->trans_rollback();
            }
        } else {
            $this->M_product_new_admin->trans_rollback();
        }
    }

    protected function reject() {

        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->seq = parent::get_input_post("key");
        $sel_data = $this->M_product_new_admin->get_data($selected);
        $status = "";
        $output = "";

        try {
            if (!isset($sel_data) || $sel_data[0]->status != NEW_STATUS_CODE) {
                throw new Exception(ERROR_REJECT);
            }
            $this->M_product_new_admin->trans_begin();
            $this->M_product_new_admin->save_reject($selected);
            $this->M_product_new_admin->trans_commit();
            parent::set_json_success();
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_product_new_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_REJECT);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_product_new_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_REJECT);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_product_new_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_REJECT);
        }
    }

    protected function approve() {
        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->seq = parent::get_input_post("key");
        $selected->default_value_seq = DEFAULT_VALUE_VARIANT_SEQ;
        $status = "";
        $image_del;
        $sel_data = $this->M_product_new_admin->get_data($selected);
        try {
            if (!isset($sel_data) || $sel_data[0]->status != NEW_STATUS_CODE) {
                throw new Exception(ERROR_APPROVED);
            }
            $selected->product_seq = $sel_data[0]->seq;
            $this->M_product_new_admin->trans_begin();
            $maxseq = $this->M_product_new_admin->save_copy_product($selected);
            if (isset($maxseq)) {
                $maxseq = ($maxseq[0]->new_seq );
            } else {
                $maxseq = 1;
            }
            $selected->new_seq = $maxseq;
            $this->M_product_new_admin->save_copy_attribute($selected);
            $this->M_product_new_admin->save_copy_spec($selected);
            $this->M_product_new_admin->save_copy_variant($selected);
            $sel_data_variant = $this->M_product_new_admin->get_variant($selected);
            if (isset($sel_data_variant)) {
                $string = preg_replace("/[^A-Za-z0-9 ]/", "", $sel_data[0]->name);
                $string = str_ireplace(" ", "_", $string);
                $namafolder = str_replace(CDN_IMAGE, "", PRODUCT_UPLOAD_IMAGE) . $sel_data[0]->merchant_seq . "/";
                $folderlama = str_replace(CDN_IMAGE, "", TEMP_PRODUCT_UPLOAD_IMAGE) . $sel_data[0]->merchant_seq . "/";
                
                if (!file_exists($namafolder)) {
                    mkdir($namafolder, 0777, true);
                }
                foreach ($sel_data_variant as $datav) {
                    $sequence = $datav->seq;
                    $selected->newvarseq = $sequence;
                    $oldfile = $datav->pic_1_img;
                    if ($oldfile != "") {
                        $ext = end((explode(".", $oldfile)));
                        $image_del[] = $folderlama . $oldfile;
                        $namafile = strtolower($sequence . "_" . $string . "_1." . $ext);
                        copy($folderlama . $oldfile, $namafolder . $namafile);
                        //rename($folderlama . $oldfile, $namafolder . $namafile);
                        parent::set_all_image($namafolder . $namafile, $namafolder, $namafile, $this->data);
                        $selected->img1 = $namafile;
                    } else {
                        $selected->img1 = "";
                    }
                    $oldfile = $datav->pic_2_img;
                    if ($oldfile != "") {
                        $ext = end((explode(".", $oldfile)));
                        $image_del[] = $folderlama . $oldfile;
                        $namafile = strtolower($sequence . "_" . $string . "_2." . $ext);
                        copy($folderlama . $oldfile, $namafolder . $namafile);
                        //rename($folderlama . $oldfile, $namafolder . $namafile);
                        parent::set_all_image($namafolder . $namafile, $namafolder, $namafile, $this->data);
                        $selected->img2 = $namafile;
                    } else {
                        $selected->img2 = "";
                    }
                    $oldfile = $datav->pic_3_img;
                    if ($oldfile != "") {
                        $ext = end((explode(".", $oldfile)));
                        $image_del[] = $folderlama . $oldfile;
                        $namafile = strtolower($sequence . "_" . $string . "_3." . $ext);
                        copy($folderlama . $oldfile, $namafolder . $namafile);
                        //rename($folderlama . $oldfile, $namafolder . $namafile);
                        parent::set_all_image($namafolder . $namafile, $namafolder, $namafile, $this->data);
                        $selected->img3 = $namafile;
                    } else {
                        $selected->img3 = "";
                    }
                    $oldfile = $datav->pic_4_img;
                    if ($oldfile != "") {
                        $ext = end((explode(".", $oldfile)));
                        $image_del[] = $folderlama . $oldfile;
                        $namafile = strtolower($sequence . "_" . $string . "_4." . $ext);
                        copy($folderlama . $oldfile, $namafolder . $namafile);
                        parent::set_all_image($namafolder . $namafile, $namafolder, $namafile, $this->data);
                        $selected->img4 = $namafile;
                    } else {
                        $selected->img4 = "";
                    }
                    $oldfile = $datav->pic_5_img;
                    if ($oldfile != "") {
                        $ext = end((explode(".", $oldfile)));
                        $image_del[] = $folderlama . $oldfile;
                        $namafile = strtolower($sequence . "_" . $string . "_5." . $ext);
                        copy($folderlama . $oldfile, $namafolder . $namafile);
                        parent::set_all_image($namafolder . $namafile, $namafolder, $namafile, $this->data);
                        $selected->img5 = $namafile;
                    } else {
                        $selected->img5 = "";
                    }
                    $oldfile = $datav->pic_6_img;
                    if ($oldfile != "") {
                        $ext = end((explode(".", $oldfile)));
                        $image_del[] = $folderlama . $oldfile;
                        $namafile = strtolower($sequence . "_" . $string . "_6." . $ext);
                        copy($folderlama . $oldfile, $namafolder . $namafile);
                        parent::set_all_image($namafolder . $namafile, $namafolder, $namafile, $this->data);
                        $selected->img6 = $namafile;
                    } else {
                        $selected->img6 = "";
                    }
                    $this->M_product_new_admin->save_img_variant($selected);
                }
            }
            $this->M_product_new_admin->save_delete($selected);

            if ($this->data[DATA_ERROR][ERROR] === true) {
                throw new Exception($this->data[DATA_ERROR][ERROR_MESSAGE][0]);
            }
            $this->M_product_new_admin->trans_commit();

            foreach ($image_del as $image) {
                parent::Unlink_file($image);
            }

            parent::set_json_success();
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_product_new_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_APPROVED);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_product_new_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_APPROVED);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_product_new_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_APPROVED);
        }
    }

}

?>