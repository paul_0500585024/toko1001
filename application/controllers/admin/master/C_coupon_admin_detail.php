<?php

require_once CONTROLLER_BASE_ADMIN;

class C_coupon_admin_detail extends controller_base_admin {

    private $data;
    private $hseq;
    private $menulst;

    public function __construct() {
        $this->data = parent::__construct("CON05008", "admin/master/coupon_detail");
        $this->initialize();
    }

    private function initialize() {

        $this->hseq = $this->uri->segment(4);
        $this->load->model('admin/master/M_coupon_admin');
        $this->load->model('admin/master/M_promo_credit');
        $this->load->model('component/M_dropdown_list');
        $this->load->model('component/M_tree_view');
        $this->menulst = $this->M_tree_view->get_menu_product_category();
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_ADDITIONAL, "get_dropdown_var");
        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        $this->data[TREE_MENU] = $this->get_menu($this->menulst, 0);
        $this->data[PRODUCT_NAME] = $this->M_dropdown_list->get_dropdown_product();
        if (!$this->input->post()) {
            if ($this->data[DATA_AUTH][FORM_ACTION] == "") {
                $this->get_data_header();
            }
            $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
            $this->load->view("admin/master/coupon_admin_detail", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/master/coupon_admin_detail", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/master/coupon_admin_detail", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_ADDITIONAL) {
                        
                    } else {
                        if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                            $this->data[DATA_SUCCESS][SUCCESS] = true;
                            $this->data[DATA_AUTH][FORM_ACTION] = "";
                        }
                        $admin_info[SESSION_DATA] = $this->data;
                        $this->session->set_userdata($admin_info);
                        redirect(base_url($this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq));
                    }
                }
            }
        }
    }

    protected function get_menu($datas, $parent = 0, $p_level = 0) {
        static $i = 1;
        $tab = str_repeat(" ", $i);
        if (isset($datas[$parent])) {
            $html = "<ul>";
            $i++;
            foreach ($datas[$parent] as $vals) {
                if ($vals->level == 1) {
                    $this->parent_level = $vals->seq;
                    $p_level = $vals->seq;
                }
                $child = $this->get_menu($datas, $vals->seq, $this->parent_level);
                $html .= "$tab";
                $html.='<li><a href="javascript:pilihmenu(\'' . $vals->seq . '~' . $vals->level . '~' . $vals->name . '~' . $p_level . '\')">' . $vals->name . '</a>';
                if ($child) {
                    $i++;
                    $html .= $child;
                    $html .= "$tab";
                }
                $html .= '</li>';
            }
            $html .= "$tab</ul>";
            return $html;
        } else {
            return false;
        }
    }

    protected function get_category_seq_self_parent(&$categories, $datas, $seq, $parent = 0, $p_level = 0, $get_self_seq = TRUE) {
        if ($get_self_seq) {
            $categories[] = $seq;
        }
        foreach ($datas as $each_datas) {
            foreach ($each_datas as $each_datas_detail) {
                if (isset($each_datas_detail->seq)) {
                    if ($each_datas_detail->seq == $seq) {
                        if ($each_datas_detail->parent_seq != 0)
                            $categories[] = $each_datas_detail->parent_seq;
                        $this->get_category_seq_self_parent($categories, $datas, $each_datas_detail->parent_seq, 0, 0, FALSE);
                        return false;
                    }
                }
            }
        }
    }

    protected function get_category_seq_self_child(&$categories, $datas, $parent = 0, $p_level = 0, $get_self_seq = TRUE) {
        $parent_level = "";
        if ($get_self_seq) {
            $categories[] = $parent;
        }
        if (isset($datas[$parent])) {
            foreach ($datas[$parent] as $vals) {
                if ($vals->level == 1) {
                    $parent_level = $vals->seq;
                    $p_level = $vals->seq;
                }
                $categories[] = $vals->seq;
                $this->get_category_seq_self_child($categories, $datas, $vals->seq, $parent_level, FALSE);
            }
//            return $seq;
        }
    }

    protected function get_variant_value($variant_seq, $variant_value, $separator = "") {
        $retval = "";
        switch ($variant_seq) {
            case "1": //all product
                $retval = "";
                break;
            default:
                $retval = $separator . $variant_value;
        }
        return $retval;
    }

    protected function get_dropdown_var() {
        $data = $this->input->post('data');
        $ex = explode('~', $data);
        $this->product_category_seq = $ex[0];
        $level = $ex[1];
        $params = new stdClass;

        if ($this->product_category_seq != 0) {
            //if not all category                        
            $this->get_category_seq_self_parent($seq_self_parent_category, $this->menulst, $this->product_category_seq); // get self and parent
            $this->get_category_seq_self_child($seq_self_child_category, $this->menulst, $this->product_category_seq); // get self and child
            $category_id_self_parent = "'" . implode("','", $seq_self_parent_category) . "'";
            $category_id_self_child = "'" . implode("','", $seq_self_child_category) . "'";
        }

        if ($level == '' AND $this->product_category_seq != 0) {
            redirect(base_url() . 'error_404');
        }

        $params->order = '';

        //set param where for product (self and child)
        $params->where = "WHERE pv.active = 1 AND pv.status IN ('L','C')";

        if ($this->product_category_seq != 0) { //if not all category        
            if (count($seq_self_child_category) > 0) {
                $params->where .= " AND p.category_ln_seq IN(" . $category_id_self_child . ")";
            }
        }
        $query['product_category'] = $this->M_promo_credit->get_thumbs_new_products_no_limit($params); //query product        
        $query_row = $query['product_category']['row'];
        $query_row_result = $query_row->result();
        $query_row_result_first = $query_row_result[0];
        $total_row = $query_row_result_first->total_row;
        $row = $total_row;
        $product = $query['product_category']['product'];

        $total_product = count($product);
        $list_product = array();
        if ($total_product > 0) {
            foreach ($product as $key => $each_product) {
                //$list_product[$each_product->product_variant_seq] = $each_product->name . $this->get_variant_value($each_product->variant_seq, $each_product->variant_value, "-");
                $list_product[$each_product->product_seq] = $each_product->name;
            }
        }
        echo json_encode(array('list_product' => $list_product));
    }

    protected function get_data_header() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = $this->hseq;

        $get_data = $this->M_coupon_admin->get_data_detail($params);
        try {

            $sel_data = $this->M_coupon_admin->get_data($params);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
                $this->data[DATA_SELECTED][LIST_DATA][] = $get_data;
            } else {
                $this->load->view("errors/html/error_404");
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_coupon_admin->trans_rollback();
            $this->load->view("errors/html/error_404");
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_coupon_admin->trans_rollback();
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_coupon_admin->trans_rollback();
        }
    }

    protected function save_add() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $result = $this->input->post("select_product");
        $params->key = $this->hseq;
        $params->coupon_period_from = parent::get_input_post('coupon_period_from');
        $params->coupon_period_to = parent::get_input_post('coupon_period_to');
        $params->coupon_code = parent::get_input_post('coupon_code');
        $params->coupon_limit = parent::get_input_post('coupon_limit');
        $params->nominal = parent::get_input_post('nominal');
        
        $this->data[DATA_SELECTED][LIST_DATA][] = $params;
        $get_data = $this->M_coupon_admin->get_data_detail($params);
        $get_status = $this->M_coupon_admin->get_data($params);

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                if ($get_status[0]->status == NEW_STATUS_CODE) {
                    if ($get_data == NULL) {
                        foreach ($result as $data) {
                            $params->coupon_seq = $this->hseq;
                            $params->product_seq = $data;
                            $this->M_coupon_admin->trans_begin();
                            $this->M_coupon_admin->save_add_detail($params);
                            $this->M_coupon_admin->trans_commit();
                        }
                    } else {
                        $selected = new stdClass();
                        $selected->user_id = parent::get_admin_user_id();
                        $selected->ip_address = parent::get_ip_address();
                        $selected->coupon_seq = $this->hseq;
                        $data_new = $selected->product_seq = $this->input->post('select_product');

                        $this->M_coupon_admin->save_delete_detail($params);
                        $this->M_coupon_admin->trans_begin();
                        foreach ($data_new as $data) {
                            $params->coupon_seq = $this->hseq;
                            $params->product_seq = $data;
                            $this->M_coupon_admin->save_add_detail($params);
                        }
                        $this->M_coupon_admin->trans_commit();
                    }
                } else {
                    throw new Exception(ERROR_SAVE_ADD);
                }
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_coupon_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_coupon_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_coupon_admin->trans_rollback();
            }
        }
    }

}

?>