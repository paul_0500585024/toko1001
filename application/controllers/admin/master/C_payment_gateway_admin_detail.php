<?php

require_once CONTROLLER_BASE_ADMIN;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Ekspedisi Service
 *
 * @author Jartono
 */
class C_payment_gateway_admin_detail extends controller_base_admin {

    private $data;
    private $hseq;

    public function __construct() {
        $this->data = parent::__construct("MST06002", "admin/master/payment_gateway_detail");
        $this->initialize();
    }

    private function initialize() {

        $this->hseq = $this->uri->segment(4);
        $this->load->model('admin/master/M_payment_gateway_admin');
        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_SAVE_DELETE, "save_delete");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        if (!$this->input->post()) {
            if ($this->data[DATA_AUTH][FORM_ACTION] == "") {
                $this->get_data_header();
            }
            $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
            $this->load->view("admin/master/payment_gateway_admin_detail", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/master/payment_gateway_admin_detail", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/master/payment_gateway_admin_detail", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq));
                }
            }
        }
    }

    public function search() {

        $filter = new stdClass;
        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->hseq = $this->hseq;

        try {
            $list_data = $this->M_payment_gateway_admin->get_list_detail($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );
        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->seq,
                    "code" => parent::cdef($data_row->code),
                    "name" => parent::cdef($data_row->name),
                    "order" => parent::cnum($data_row->order),
                    "notes" => $data_row->notes,
                    "active" => parent::cstat($data_row->active),
                    "created_by" => $data_row->created_by,
                    "created_date" => parent::cdate($data_row->created_date, 1),
                    "modified_by" => $data_row->modified_by,
                    "modified_date" => parent::cdate($data_row->modified_date, 1));
                ;

                $output['aaData'][] = $row;
            }
        };

        echo json_encode($output);
    }

    protected function get_edit() {

        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = parent::get_input_post("key");

        try {
            $sel_data = $this->M_payment_gateway_admin->get_data_detail($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function get_data_header() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = $this->hseq;

        try {
            $sel_data = $this->M_payment_gateway_admin->get_data($params);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            } else {
                $this->load->view("errors/html/error_404");
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_payment_gateway_admin->trans_rollback();
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_payment_gateway_admin->trans_rollback();
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_payment_gateway_admin->trans_rollback();
        }
    }

    protected function save_add() {

        $logo_img = parent::upload_file(LOGO_IMG, TEMP_PAYMENT_UPLOAD_IMAGE, "TMP_PG_" . parent::get_input_post("code"), IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PAYMENT_GATEWAY, "Payment Gateway ");

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->pg_seq = $this->hseq;
        $params->name = parent::get_input_post("name", TRUE, FILL_VALIDATOR, "Tipe Pembayaran", $this->data);
        $params->code = parent::get_input_post("code", TRUE, FILL_VALIDATOR, "Kode", $this->data);
        $params->logo_img = $logo_img->file_name;
        $params->order = parent::get_input_post("p_order", TRUE, NUMERIC_VALIDATOR, "Urutan", $this->data);
        $params->notes = parent::get_input_post("notes");
        $params->active = parent::get_input_post("active");

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_payment_gateway_admin->trans_begin();
                $this->M_payment_gateway_admin->save_add_detail($params);
                $this->M_payment_gateway_admin->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_payment_gateway_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_payment_gateway_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_payment_gateway_admin->trans_rollback();
            }
        }
    }

    protected function save_update() {

        $logo_img = parent::upload_file(LOGO_IMG, TEMP_PAYMENT_UPLOAD_IMAGE, "TMP_PG_" . parent::get_input_post("code"), IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PAYMENT_GATEWAY, "Payment Gateway ");

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->seq = parent::get_input_post("seq");
        $params->name = parent::get_input_post("name", TRUE, FILL_VALIDATOR, "Tipe Pembayaran", $this->data);
        $params->code = parent::get_input_post("code", TRUE, FILL_VALIDATOR, "Kode", $this->data);
        $params->old_name = parent::get_input_post("oldname");
        $params->logo_img = $logo_img->error == true ? $params->old_name : $logo_img->file_name;
        $params->order = parent::get_input_post("p_order", TRUE, NUMERIC_VALIDATOR, "Urutan", $this->data);
        $params->notes = parent::get_input_post("notes");
        $params->active = parent::get_input_post("active");

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_payment_gateway_admin->trans_begin();
                $this->M_payment_gateway_admin->save_update_detail($params);
                $this->M_payment_gateway_admin->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_payment_gateway_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_payment_gateway_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_payment_gateway_admin->trans_rollback();
            }
        }
    }

    protected function save_delete() {
        $params = new stdClass();

        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = parent::get_input_post("key");

        try {
            $this->M_payment_gateway_admin->trans_begin();
            $this->M_payment_gateway_admin->save_delete_detail($params);
            $this->M_payment_gateway_admin->trans_commit();
            $output = array("error" => FALSE, "message" => "");
            echo json_encode($output);
            die();
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_payment_gateway_admin->trans_rollback();
            $output = array("error" => TRUE, "message" => ERROR_DELETE);
            echo json_encode($output);
            die();
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_payment_gateway_admin->trans_rollback();
            $output = array("error" => TRUE, "message" => ERROR_DELETE);
            echo json_encode($output);
            die();
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_payment_gateway_admin->trans_rollback();
            $output = array("error" => TRUE, "message" => ERROR_DELETE);
            echo json_encode($output);
            die();
        }
    }

}

?>