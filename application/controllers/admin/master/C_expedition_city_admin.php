<?php

require_once CONTROLLER_BASE_ADMIN;

class C_expedition_city_admin extends controller_base_admin {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("MST03004", "admin/master/expedition_city");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('admin/master/M_expedition_city_admin');
        $this->load->model('component/M_dropdown_list');

        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_ADDITIONAL, "save_upload");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        $this->data[EXPEDITION_NAME] = $this->M_dropdown_list->get_dropdown_expedition();
        $this->data[PROVINCE_NAME] = $this->M_dropdown_list->get_dropdown_province();

        $type = parent::get_input_post("type");
        switch ($type) {
            case TASK_PROVINCE_CHANGE : {
                    $this->get_dropdown_city_by_province();
                }break;
            case TASK_CITY_CHANGE : {
                    $this->get_dropdown_district_by_city();
                }break;
        }

        if (!$this->input->post()) {
            $this->data[FILTER] = 'admin/master/expedition_city_admin_f.php';
            $this->load->view("admin/master/expedition_city_admin", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/master/expedition_city_admin", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/master/expedition_city_admin", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_ADDITIONAL) {
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                        $admin_info[SESSION_DATA] = $this->data;
                        $this->session->set_userdata($admin_info);
                        redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_ADDITIONAL) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    public function search() {
        $filter = new stdClass;
        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->expedition_seq = parent::get_input_post("expedition_seq");
        $filter->province_seq = parent::get_input_post("province_seq");
        $filter->city_seq = parent::get_input_post("city_seq");
        $filter->district_seq = parent::get_input_post("district_seq");
        $filter->exp_district_code = parent::get_input_post("exp_district_code");
        $filter->active = parent::get_input_post("active");

        try {
            $list_data = $this->M_expedition_city_admin->get_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );

        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->exp_seq . '/' . $data_row->district_seq,
                    "exp_code" => parent::cdef($data_row->exp_code),
                    "exp_name" => parent::cdef($data_row->exp_name),
                    "province_name" => parent::cdef($data_row->province_name),
                    "city_name" => parent::cdef($data_row->city_name),
                    "district_name" => parent::cdef($data_row->district_name),
                    "exp_district_code" => parent::cdef($data_row->exp_district_code),
                    "created_by" => $data_row->created_by,
                    "created_date" => parent::cdate($data_row->created_date, 1),
                    "modified_by" => $data_row->modified_by,
                    "modified_date" => parent::cdate($data_row->modified_date, 1));
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
    }

    protected function save_upload() {
        try {
            $file = parent::upload_file(UPLOADID, UPLOADPATH, FILENAME, CSV_FILE);

            if ($file->error === false) {
                $this->load->library(CSV);
                $result = $this->csv_reader->parse_file($file->url_file . ".csv");

                foreach ($result as $field) {
                    $params = new stdClass();
                    $params->user_id = parent::get_admin_user_id();
                    $params->ip_address = parent::get_ip_address();
                    $params->exp_seq = $field["exp_seq"];
                    $params->district_seq = $field['district_seq'];
                    $params->exp_district_code = $field["exp_district_code"];

                    if ($this->data[DATA_AUTH][FORM_AUTH][FORM_AUTH_EDIT] === false) {
                        $this->data[DATA_ERROR][ERROR] = true;
                        $this->data[DATA_ERROR][ERROR_MESSAGE] = ERROR_VALIDATION_CANT_ADD_NO_ACCESS_FORM;
                    }

                    if ($this->data[DATA_ERROR][ERROR] === false) {

                        $this->M_expedition_city_admin->trans_begin();
                        $this->M_expedition_city_admin->save_upload($params);
                        $this->M_expedition_city_admin->trans_commit();
                        parent::Unlink_file($file->url_file);
                    }
                }
            } else {
                throw new Exception(ERROR_UPLOAD);
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_expedition_city_admin->trans_rollback();
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_expedition_city_admin->trans_rollback();
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            echo $this->data[DATA_AUTH][FORM_ACTION];
            $this->M_expedition_city_admin->trans_rollback();
        }
    }

    protected function get_edit() {
        $selected = new stdClass();
        $key = explode("/", parent::get_input_post("key"));
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->exp_seq = $key[0];
        $selected->district_seq = $key[1];

        try {
            $sel_data = $this->M_expedition_city_admin->get_data($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_update() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->exp_seq = parent::get_input_post("exp_seq");
        $params->district_seq = parent::get_input_post("district_seq");
        $params->exp_district_code = parent::get_input_post("exp_district_code", true, FILL_VALIDATOR, "Kecamatan Ekspedisi", $this->data);

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_expedition_city_admin->trans_begin();
                $this->M_expedition_city_admin->save_update($params);
                $this->M_expedition_city_admin->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_expedition_city_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_expedition_city_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_expedition_city_admin->trans_rollback();
            }
        }
    }

    protected function get_dropdown_city_by_province() {
        $province_seq = parent::get_input_post("province_seq");

        try {
            $list_data = $this->M_dropdown_list->get_dropdown_city_by_province($province_seq);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        echo json_encode($list_data);
        die();
    }

    protected function get_dropdown_district_by_city() {
        $city_seq = parent::get_input_post("city_seq");

        try {
            $list_data = $this->M_dropdown_list->get_dropdown_district_by_city($city_seq);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        echo json_encode($list_data);
        die();
    }

}

?>
