<?php

require_once CONTROLLER_BASE_ADMIN;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Ekspedisi Service
 *
 * @author Jartono
 */
class C_merchant_log_admin_detail extends controller_base_admin {

    private $data;
    private $hseq;

    public function __construct() {
        $this->data = parent::__construct("MST05004", "admin/master/merchant_log_detail");
        $this->initialize();
    }

    private function initialize() {

        $this->hseq = $this->uri->segment(4);
        $this->load->model('admin/master/M_merchant_log_admin');
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::fire_event($this->data);
    }

    public function index() {
        if (!$this->input->post()) {
            if ($this->data[DATA_AUTH][FORM_ACTION] == "") {
                
            }
            $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
            $this->get_data_detail();
            $this->load->view("admin/master/merchant_log_admin_detail", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/master/merchant_log_admin_detail", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/master/merchant_log_admin_detail", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                        //$this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq));
                }
            } else {
                
            }
        }
    }

    protected function get_data_detail() {
        $key = explode("_", $this->hseq);

        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();

        try {
            if (isset($key[1]) && isset($key[0])) {
                $selected->merchant_seq = $key[0];
                $selected->log_seq = $key[1];
                $sel_data = $this->M_merchant_log_admin->get_data_detail($selected);
                if (isset($sel_data)) {
                    parent::set_data($this->data, $sel_data);
                } else {
                    $this->load->view("errors/html/error_404");
                }
            } else {
                throw new BusisnessException(ERROR_MESSAGE);
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_merchant_log_admin->trans_rollback();
            $this->load->view("errors/html/error_404");
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_merchant_log_admin->trans_rollback();
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_merchant_log_admin->trans_rollback();
        }
    }

}

?>