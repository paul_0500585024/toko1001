<?php

require_once CONTROLLER_BASE_ADMIN;

class C_promo_voucher_periode_admin extends controller_base_admin {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("CON05003", "admin/master/promo_voucher_periode");
        $this->initialize();
    }

    private function initialize() {

        $this->load->model('admin/master/M_promo_voucher_periode_admin');
        $this->load->model('component/M_dropdown_list');

        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_SAVE_DELETE, "save_delete");
        parent::register_event($this->data, ACTION_APPROVE, "approve");
        parent::register_event($this->data, ACTION_REJECT, "reject");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {

        $this->data[NODE_NAME] = $this->M_dropdown_list->get_dropdown_promo_voucher_node();
        if (!$this->input->post()) {
            $this->data[FILTER] = 'admin/master/promo_voucher_periode_admin_f.php';
            $this->load->view("admin/master/promo_voucher_periode_admin", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/master/promo_voucher_periode_admin", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/master/promo_voucher_periode_admin", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    public function search() {

        $filter = new stdClass;

        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->name = parent::get_input_post("v_name");
        $filter->code = parent::get_input_post("v_code");
        $filter->node_cd = parent::get_input_post("node_cd");
        $filter->active = parent::get_input_post("active");
        $filter->status = parent::get_input_post("status");
        $filter->date_from = parent::get_input_post("date_from");
        $filter->date_to = parent::get_input_post("date_to");

        try {
            $list_data = $this->M_promo_voucher_periode_admin->get_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );
        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->seq,
                    "date_from" => parent::cdate($data_row->date_from),
                    "date_to" => parent::cdate($data_row->date_to),
                    "name" => parent::cdef($data_row->name),
                    "code" => parent::cdef($data_row->code),
                    "node_name" => $data_row->node_name,
                    "type" => parent::cstdes($data_row->type, VOUCHER_TYPE),
                    "voucher_count" => parent::cnum($data_row->voucher_count),
                    "nominal" => parent::cnum($data_row->nominal),
                    "exp_days" => parent::cnum($data_row->exp_days),
                    "trx_get_amt" => parent::cnum($data_row->trx_get_amt),
                    "trx_use_amt" => parent::cnum($data_row->trx_use_amt),
                    "notes" => parent::cdef($data_row->notes),
                    "status" => parent::cstdes($data_row->status, STATUS_ANR),
                    "auth_by" => $data_row->auth_by,
                    "auth_date" => parent::cdate($data_row->auth_date, 1),
                    "action" => "<a href='promo_voucher_periode_detail/" . $data_row->seq . "'>Detail</a>",
                    "active" => parent::cstat($data_row->active));
                $output['aaData'][] = $row;
            }
        };

        echo json_encode($output);
    }

    protected function get_edit() {
        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = parent::get_input_post("key");

        try {
            $sel_data = $this->M_promo_voucher_periode_admin->get_data($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_add() {
        $date = explode(" - ", parent::get_input_post("date_from"));

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->date_from = $date[0];
        $params->date_to = $date[1];
        $params->name = parent::get_input_post("v_name", TRUE, FILL_VALIDATOR, "Nama Voucher", $this->data);
        $params->code = parent::get_input_post("v_code", TRUE, FILL_VALIDATOR, "Kode Voucher", $this->data);
        $params->node_cd = parent::get_input_post("node_cd", TRUE, FILL_VALIDATOR, "Transaksi", $this->data);
        $params->type = parent::get_input_post("t_type", TRUE, FILL_VALIDATOR, "Cara Buat Voucher", $this->data);
        if ($params->type == "A") {
            $params->voucher_count = parent::get_input_post("voucher_count");
        } else {
            $params->voucher_count = parent::get_input_post("voucher_count", TRUE, QTY_VALIDATOR, "Jumlah Voucher", $this->data);
        }
        $params->nominal = parent::get_input_post("nominal", TRUE, QTY_VALIDATOR, "Nominal", $this->data);
        $params->exp_days = parent::get_input_post("exp_days", TRUE, FILL_VALIDATOR, "Kadarluarsa", $this->data);
        $params->trx_get_amt = parent::get_input_post("trx_get_amt");
        $params->trx_use_amt = parent::get_input_post("trx_use_amt", TRUE, QTY_VALIDATOR, "Mininum Belanja", $this->data);
        $params->notes = parent::get_input_post("notes");
        $params->active = parent::get_input_post("active");

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_promo_voucher_periode_admin->trans_begin();
                $this->M_promo_voucher_periode_admin->save_add($params);
                $this->M_promo_voucher_periode_admin->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_promo_voucher_periode_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_promo_voucher_periode_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_promo_voucher_periode_admin->trans_rollback();
            }
        }
    }

    protected function save_update() {
        $date = explode(" - ", parent::get_input_post("date_from"));
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = parent::get_input_post("seq");
        $params->seq = parent::get_input_post("seq");
        $params->date_from = $date[0];
        $params->date_to = $date[1];
        $params->name = parent::get_input_post("v_name", TRUE, FILL_VALIDATOR, "Nama Voucher", $this->data);
        $params->code = strtoupper(parent::get_input_post("v_code", TRUE, FILL_VALIDATOR, "Kode Voucher", $this->data));
        $params->node_cd = parent::get_input_post("node_cd", TRUE, FILL_VALIDATOR, "Transaksi", $this->data);
        $params->type = parent::get_input_post("t_type", TRUE, FILL_VALIDATOR, "Cara buat voucher", $this->data);
        if ($params->type == "A") {
            $params->voucher_count = parent::get_input_post("voucher_count");
        } else {
            $params->voucher_count = parent::get_input_post("voucher_count", TRUE, QTY_VALIDATOR, "Jumlah Voucher", $this->data);
        }
        $params->nominal = parent::get_input_post("nominal", TRUE, QTY_VALIDATOR, "Nominal", $this->data);
        $params->exp_days = parent::get_input_post("exp_days", TRUE, QTY_VALIDATOR, "Kadarluarsa", $this->data);
        $params->trx_get_amt = parent::get_input_post("trx_get_amt");
        $params->trx_use_amt = parent::get_input_post("trx_use_amt", TRUE, QTY_VALIDATOR, "Mininum Belanja", $this->data);
        $params->notes = parent::get_input_post("notes");
        $params->active = parent::get_input_post("active");

        $sel_data = $this->M_promo_voucher_periode_admin->get_data($params);

        $params->status = parent::get_input_post("status");

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                if ($sel_data[0]->status == NEW_STATUS_CODE) {
                    $this->M_promo_voucher_periode_admin->trans_begin();
                    $this->M_promo_voucher_periode_admin->save_update($params);
                    $this->M_promo_voucher_periode_admin->trans_commit();
                } else {
                    throw new Exception(ERROR_UPDATE);
                }
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_promo_voucher_periode_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_promo_voucher_periode_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_promo_voucher_periode_admin->trans_rollback();
            }
        }
    }

    protected function save_delete() {
        $params = new stdClass();

        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = parent::get_input_post("key");
        $sel_data = $this->M_promo_voucher_periode_admin->get_data($params);
        try {
            if ($sel_data[0]->status == NEW_STATUS_CODE) {
                $this->M_promo_voucher_periode_admin->trans_begin();
                $this->M_promo_voucher_periode_admin->save_delete($params);
                $this->M_promo_voucher_periode_admin->trans_commit();
            } else {
                throw new Exception(ERROR_DELETE_STATUS);
            }
            parent::set_json_success();
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_promo_voucher_periode_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_promo_voucher_periode_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_promo_voucher_periode_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        }
    }

    protected function approve() {
        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = parent::get_input_post("key");
        $arrvchr = array();

        $sel_data = $this->M_promo_voucher_periode_admin->get_data($selected);

        for ($x = 1; $x <= $sel_data[0]->voucher_count; $x++) {
            if ($sel_data[0]->type == "M") {
                $panjang = strlen($sel_data[0]->code);
                $arrvchr[] = $sel_data[0]->code . parent::generate_random_text(20 - $panjang, (true));
            } else {
                $arrvchr[] = $sel_data[0]->code . parent::generate_random_text(20, true);
            }
        }

        try {
            $this->M_promo_voucher_periode_admin->trans_begin();
            $this->M_promo_voucher_periode_admin->get_approve($selected);
            if ($sel_data[0]->status == NEW_STATUS_CODE) {
                $selected->nominal = $sel_data[0]->nominal;
                $selected->trx_use_amt = $sel_data[0]->trx_use_amt;
                $selected->code = $arrvchr;
                $this->M_promo_voucher_periode_admin->save_add_voucher($selected);
                $this->M_promo_voucher_periode_admin->trans_commit();

                parent::set_json_success();
            } else {
                throw new Exception(ERROR_APPROVED);
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_promo_voucher_periode_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_APPROVED);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_promo_voucher_periode_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_APPROVED);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_promo_voucher_periode_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_APPROVED);
        }
    }

    protected function reject() {
        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = parent::get_input_post("key");
        $sel_data = $this->M_promo_voucher_periode_admin->get_data($selected);
        try {
            if ($sel_data[0]->status == NEW_STATUS_CODE) {
                $this->M_promo_voucher_periode_admin->trans_begin();
                $this->M_promo_voucher_periode_admin->get_reject($selected);
                $this->M_promo_voucher_periode_admin->trans_commit();
                parent::set_json_success();
            } else {
                parent::set_json_error($ex, ERROR_REJECT);
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_promo_voucher_periode_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_REJECT);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_promo_voucher_periode_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_REJECT);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_promo_voucher_periode_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_REJECT);
        }
    }

}

?>
