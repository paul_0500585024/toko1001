<?php

require_once CONTROLLER_BASE_ADMIN;

class C_partner_group_detail extends controller_base_admin {

    public function __construct() {
        $this->data = parent::__construct("MST70009", "admin/master/partner_group_detail");
        $this->initialize();
    }

    private function initialize() {
        $this->hseq = $this->uri->segment(4);
        $this->load->model('admin/master/M_partner_group');
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {

        if (!$this->input->post()) {
            if ($this->data[DATA_AUTH][FORM_ACTION] == "") {
                $this->get_edit();
                $this->get_data_header();
            }
            $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
            $this->load->view("admin/master/partner_group_detail", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/master/merchant_admin_detail", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/master/partner_group_detail", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_URL] = 'admin/master/partner_group';
                        $this->data[DATA_AUTH][FORM_ACTION] = '';
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url("admin/master/partner_group"));
                }
            }
        }
    }

    protected function get_data_header() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = $this->hseq;
        try {
            $sel_data = $this->M_partner_group->get_data($params);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function get_edit() {
        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = $this->hseq;
        try {
            $this->data[TREE_MENU] = $this->M_partner_group->get_commission_list($selected);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_update() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->seq = $this->input->post("seq");
        $params->hseq = $this->input->post("hseq");
        $params->lvlcat = $this->input->post("lvlcat");
        $params->mfee = $this->input->post("mfee");

        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = '';
        $tree = $this->M_partner_group->get_commission_list($selected);

        $this->get_data_header();
        (array) $this->data[TREE_MENU] = [];


        foreach ($params->lvlcat as $lvl) {
            $infoLevel = explode('~', $lvl);
            $dataReturn[$infoLevel[1]] = ['lvl1' => $infoLevel[2], 'lvl2' => $infoLevel[0]];
        }
        
        foreach ($tree as $key => $value) {
            $data = new stdClass;
            $data->seq = $value->seq;
            $data->name = $value->name;
            $data->trx_fee_percent = $value->trx_fee_percent;
            $data->hseq = $value->hseq;
            $data->hname = $value->hname;
            $data->mcatlvl1 = isset($dataReturn[$key]) ? $dataReturn[$key]['lvl1'] : $value->mcatlvl1;
            $data->mcatlvl2 = isset($dataReturn[$key]) ? $dataReturn[$key]['lvl2'] : $value->mcatlvl2;
            $data->mfee = $params->mfee[$key];
            $this->data[TREE_MENU][$key] = $data;
        }

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_partner_group->trans_begin();
                $this->M_partner_group->save_update_detail($params);
                $this->M_partner_group->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_partner_group->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_partner_group->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_partner_group->trans_rollback();
            }
        }
    }

}
