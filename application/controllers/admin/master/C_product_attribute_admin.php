<?php

require_once CONTROLLER_BASE_ADMIN;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of c_product_attribute
 *
 * @author Jartono
 */
class C_product_attribute_admin extends controller_base_admin {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("MST02002", "admin/master/product_attribute");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('admin/master/M_product_attribute_admin');
        $this->load->model('component/M_dropdown_list');
        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_SAVE_DELETE, "save_delete");
        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {

        $this->data[CATEGORY_NAME] = $this->M_dropdown_list->get_dropdown_product_category();
        
        if (!$this->input->post()) {
            $this->data[FILTER] = 'admin/master/product_attribute_admin_f.php';
            $this->load->view("admin/master/product_attribute_admin", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/master/product_attribute_admin", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/master/product_attribute_admin", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    protected function search() {

        $filter = new stdClass;

        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->category_seq = parent::get_input_post("category_seq");
        $filter->name = parent::get_input_post("name");
        $filter->display_name = parent::get_input_post("display_name");
        $filter->filter = parent::get_input_post("filter");
        $filter->active = parent::get_input_post("active");

        try {
            $list_data = $this->M_product_attribute_admin->get_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );


        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->seq,
                    "category_name" => $data_row->category_name,
                    "name" => parent::cdef($data_row->name),
                    "display_name" => parent::cdef($data_row->display_name),
                    "order_search" => $data_row->order_search,
                    "filter" => parent::cstat($data_row->filter),
                    "active" => parent::cstat($data_row->active),
                    "created_by" => $data_row->created_by,
                    "created_date" => parent::cdate($data_row->created_date, 1),
                    "modified_by" => $data_row->modified_by,
                    "modified_date" => parent::cdate($data_row->modified_date, 1),
                    "action" => "<a href='product_attribute_detail/" . $data_row->seq . "'>Nilai</a>");
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
    }

    protected function get_edit() {

        $selected = new stdClass();

        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = parent::get_input_post("key");

        try {
            $sel_data = $this->M_product_attribute_admin->get_data($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
            $category_name = $this->M_dropdown_list->get_dropdown_product_category();
            $this->data['category_name'] = $category_name;
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_add() {

        $params = new stdClass();

        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->category_seq = parent::get_input_post("category_seq", TRUE, FILL_VALIDATOR, "Kategori", $this->data);
        $params->name = parent::get_input_post("name", TRUE, FILL_VALIDATOR, "Nama Atribut", $this->data);
        $params->display_name = parent::get_input_post("display_name", true, FILL_VALIDATOR, "Nama Tampilan", $this->data);
        $params->order = parent::get_input_post("order", TRUE, NUMERIC_VALIDATOR, "Urutan Pencarian", $this->data);
        $params->filter = parent::get_input_post("filter");
        $params->active = parent::get_input_post("active");

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_product_attribute_admin->trans_begin();
                $this->M_product_attribute_admin->save_add($params);
                $this->M_product_attribute_admin->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_product_attribute_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_product_attribute_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_product_attribute_admin->trans_rollback();
            }
        }
    }

    protected function save_delete() {

        $params = new stdClass();

        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = parent::get_input_post("key");

        try {
            $this->M_product_attribute_admin->trans_begin();
            $this->M_product_attribute_admin->save_delete($params);
            $this->M_product_attribute_admin->trans_commit();
            parent::set_json_success();
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_product_attribute_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_product_attribute_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_product_attribute_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        }
    }

    protected function save_update() {

        $params = new stdClass();

        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->seq = parent::get_input_post("seq");
        $params->category_seq = parent::get_input_post("category_seq", TRUE, FILL_VALIDATOR, "Kategori", $this->data);
        $params->name = parent::get_input_post("name", TRUE, FILL_VALIDATOR, "Nama Atribut", $this->data);
        $params->display_name = parent::get_input_post("display_name", true, FILL_VALIDATOR, "Nama Tampilan", $this->data);
        $params->order = parent::get_input_post("order", TRUE, NUMERIC_VALIDATOR, "Urutan Pencarian", $this->data);
        $params->filter = parent::get_input_post("filter");
        $params->active = parent::get_input_post("active");

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_product_attribute_admin->trans_begin();
                $this->M_product_attribute_admin->save_update($params);
                $this->M_product_attribute_admin->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_product_attribute_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_product_attribute_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_product_attribute_admin->trans_rollback();
            }
        }
    }

}

?>