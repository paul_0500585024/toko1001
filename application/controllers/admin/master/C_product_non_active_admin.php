<?php

require_once CONTROLLER_BASE_ADMIN;

class C_product_non_active_admin extends controller_base_admin {

    private $data;
    private $parent_level1;
    private $parent_level2;

    public function __construct() {
        $this->data = parent::__construct("MST02010", "admin/master/product_non_active_admin");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('admin/master/M_product_non_active_admin');
        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        $filter = new stdClass;
        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->merchant_name = '';        
        $merchant_list = $this->M_product_non_active_admin->get_merchant_by_name($filter);
        $this->data[MERCHANT_LIST] = $merchant_list;        
        if (!$this->input->post()) {
            $this->data['filter'] = 'admin/master/product_non_active_admin_f.php';
            $this->load->view("admin/master/product_non_active_admin", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/master/product_non_active_admin", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    public function search() {
        $filter = new stdClass;
        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->merchant_seq = parent::get_input_post("merchant_info_seq");
        $filter->name = parent::get_input_post("name");
        $filter->status = parent::get_input_post("status");
        $filter->active = parent::get_input_post("active");
        
        try {
            $list_data = $this->M_product_non_active_admin->get_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );
        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => parent::cdef($data_row->product_variant_seq),
                    "merchant_name" => parent::cdef($data_row->merchant_name),
                    "product_name" => parent::cdef($data_row->product_name),
                    "variant_name" => $data_row->variant_name,
                    "status_product" => parent::cstdes($data_row->status_product, STATUS_LC),
                    "product_active" => parent::cstat($data_row->product_active),
                    "product_stock" => parent::cdef($data_row->stock_product),
                    "tgl_buat" => parent::cdate($data_row->tgl_buat, 1),
                    "tgl_ubah" => parent::cdate($data_row->tgl_ubah, 1)
                );
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
    }

    protected function get_edit() {
        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();        
        $selected->ip_address = parent::get_ip_address();
        $selected->seq = parent::get_input_post("key");
        try {
            $sel_data = $this->M_product_non_active_admin->get_data($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
                $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data;
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

}

?>