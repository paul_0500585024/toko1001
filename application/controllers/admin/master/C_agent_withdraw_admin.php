<?php

require_once CONTROLLER_BASE_ADMIN;

class C_agent_withdraw_admin extends controller_base_admin {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("MST09005", "admin/master/agent_withdraw_admin");
        $this->initialize();
    }

    private function initialize() {

        $this->load->model('component/M_dropdown_list');
        $this->load->model('admin/master/M_user_admin');
        $this->load->model('admin/master/M_agent_withdraw_admin');

        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_APPROVE, "save_approve");
        parent::register_event($this->data, ACTION_REJECT, "save_reject");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {

        $this->data[AGENT_EMAIL] = $this->M_dropdown_list->get_dropdown_agent_email();
        
        if (!$this->input->post()) {
            $this->data[FILTER] = 'admin/master/agent_withdraw_admin_f.php';
            $this->load->view("admin/master/agent_withdraw_admin", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;

                        $this->load->view("admin/master/agent_withdraw_admin", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/master/agent_withdraw_admin", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }

    }

    public function search() {

        $filter = new stdClass;
        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->agent_email = parent::get_input_post("agent_email");
        $filter->status = parent::get_input_post("status");
        try {
            
            $list_data = $this->M_agent_withdraw_admin->get_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );
        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {

                $row = array("DT_RowId" => $data_row->seq . "/" . $data_row->agent_seq,
                    "trx_no" => parent::cdef($data_row->trx_no),
                    "trx_date" => parent::cdate($data_row->trx_date),
                    "agent_email" => parent::cdef($data_row->agent_email),
                    "deposit_amt" => parent::cnum($data_row->deposit_amt),
                    "deposit_trx_amt" => parent::cnum($data_row->deposit_trx_amt),
                    "non_deposit_trx_amt" => parent::cnum($data_row->non_deposit_trx_amt),
                    "bank_name" => parent::cdef($data_row->bank_name),
                    "bank_acct_no" => parent::cdef($data_row->bank_acct_no),
                    "bank_acct_name" => parent::cdef($data_row->bank_acct_name),
                    "refund_date" => parent::cdate($data_row->refund_date),
                    "status" => parent::cstdes($data_row->status, STATUS_WITHDRAW));
                $output['aaData'][] = $row;
            }
        };

        echo json_encode($output);
    }

    protected function save_approve() {

        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();

        $key = explode("/", parent::get_input_post("key"));
        $selected->seq = $key[0];
        $selected->agent_seq = $key[1];

        try {
            $sel_data = $this->M_agent_withdraw_admin->get_data($selected);

            $selected->seq = $sel_data[0]->seq;
            $selected->agent_seq = $sel_data[0]->agent_seq;
            $selected->deposit_trx_amt = $sel_data[0]->deposit_trx_amt;
            $selected->non_deposit_trx_amt = $sel_data[0]->non_deposit_trx_amt;
            $selected->status = $sel_data[0]->status;
            $selected->deposit_amt = $sel_data[0]->deposit_amt;

            if ($selected->status == NEW_STATUS_CODE) {
                if ($selected->deposit_trx_amt < $selected->deposit_amt) {
                    $this->M_agent_withdraw_admin->trans_begin();
                    $this->M_agent_withdraw_admin->save_approve($selected);

                    $selected->code = AGENT_WITHDRAW_SUCCESS;
                    $selected->to_email = $sel_data[0]->email;
                    $selected->RECIPIENT_NAME = $sel_data[0]->name;
                    $selected->TRX_NO = $sel_data[0]->trx_no;
                    $selected->TOTAL_WITHDRAW = parent::cnum($sel_data[0]->deposit_trx_amt);
                    $selected->LINK = base_url() . AGENT_CEK_DEPOSIT;

                    $this->email_template($selected);
                    $this->M_agent_withdraw_admin->trans_commit();
                    parent::set_json_success();
                } else {
                    throw new Exception(ERROR_APPROVED_AMOUNT);
                }
            } else {
                throw new Exception(ERROR_APPROVED_STATUS);
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_agent_withdraw_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_APPROVED);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_agent_withdraw_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_APPROVED);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_agent_withdraw_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_APPROVED);
        }
    }

    protected function save_reject() {

        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();

        $key = explode("/", parent::get_input_post("key"));
        $selected->seq = $key[0];
        $selected->agent_seq = $key[1];

        try {
            $sel_data = $this->M_agent_withdraw_admin->get_data($selected);

            $selected->status = $sel_data[0]->status;

            if ($selected->status == "N") {
                $this->M_agent_withdraw_admin->trans_begin();
                $this->M_agent_withdraw_admin->save_reject($selected);

                $selected->code = AGENT_WITHDRAW_FAIL;
                $selected->to_email = $sel_data[0]->email;
                $selected->RECIPIENT_NAME = $sel_data[0]->name;
                $selected->TRX_NO = $sel_data[0]->trx_no;
                $selected->TOTAL_WITHDRAW = parent::cnum($sel_data[0]->deposit_trx_amt);
                $selected->LINK = base_url() . AGENT_CEK_DEPOSIT;

                $this->email_template($selected);
                $this->M_agent_withdraw_admin->trans_commit();
                parent::set_json_success();
            } else {
                parent::set_json_error(ERROR_REJECT);
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_agent_withdraw_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_REJECT);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_agent_withdraw_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_REJECT);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_agent_withdraw_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_REJECT);
        }
    }

}

?>