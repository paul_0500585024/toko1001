<?php

require_once CONTROLLER_BASE_ADMIN;

class C_promo_credit_admin extends controller_base_admin {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("CON05010", "admin/master/promo_credit");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('admin/master/M_promo_credit');

        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_SAVE_DELETE, "save_delete");
        parent::register_event($this->data, ACTION_APPROVE, "approve");
        parent::register_event($this->data, ACTION_REJECT, "reject");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        $this->data[FILTER] = 'admin/master/promo_credit_f.php';
        if (!$this->input->post()) {
            $this->load->view("admin/master/promo_credit", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/master/promo_credit", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/master/promo_credit", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    protected function search() {

        $filter = new stdClass;
        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->promo_credit_name = parent::get_input_post("promo_credit_name");
        $filter->promo_credit_period_from = parent::get_input_post('promo_credit_period_from');
        $filter->promo_credit_period_to = parent::get_input_post('promo_credit_period_to');
        $filter->status = parent::get_input_post('status');

        try {
            $list_data = $this->M_promo_credit->get_list($filter);

            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );

        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->seq,
                    "promo_credit_name" => $data_row->promo_credit_name,
                    "promo_credit_period_from" => parent::cdate($data_row->promo_credit_period_from),
                    "promo_credit_period_to" => parent::cdate($data_row->promo_credit_period_to),
                    "minimum_nominal" => parent::cnum($data_row->minimum_nominal),
                    "maximum_nominal" => parent::cnum($data_row->maximum_nominal),
                    "status" => parent::cstdes($data_row->status, STATUS_ANR),
                    "detail" => '<a href=promo_credit_product/' . $data_row->seq . '>Detail</a>',
                    "credit" => '<a href=promo_credit_bank/' . $data_row->seq . '>Cicilan</a>',
                    "created_by" => $data_row->created_by,
                    "created_date" => parent::cdate($data_row->created_date, 1),
                    "modified_by" => $data_row->modified_by,
                    "modified_date" => parent::cdate($data_row->modified_date, 1)
                );
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
    }

    protected function get_edit() {

        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = parent::get_input_post("key");

        try {
            $sel_data = $this->M_promo_credit->get_data($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_add() {
        $date = explode(" - ", parent::get_input_post("date_from"));

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->promo_credit_name = parent::get_input_post("promo_credit_name", true, FILL_VALIDATOR, "Nama Kupon", $this->data);
        $params->promo_credit_period_from = $date[0];
        $params->promo_credit_period_to = $date[1];
        $params->minimum_nominal = parent::get_input_post("minimum_nominal", true, FILL_VALIDATOR, "Minimal Nominal", $this->data);
        $params->maximum_nominal = parent::get_input_post("maximum_nominal", true, FILL_VALIDATOR, "Maximal Nominal", $this->data);
        $params->status = NEW_STATUS_CODE;

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_promo_credit->trans_begin();
                $this->M_promo_credit->save_add($params);
                $this->M_promo_credit->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_promo_credit->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_promo_credit->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_promo_credit->trans_rollback();
            }
        }
    }

    protected function save_delete() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = parent::get_input_post("key");
        $get_data = $sel_data = $this->M_promo_credit->get_data($params);
        try {
            if ($get_data[0]->status == NEW_STATUS_CODE) {
                $this->M_promo_credit->trans_begin();
                $this->M_promo_credit->save_delete($params);
                $this->M_promo_credit->trans_commit();
            } else {
                throw new Exception(ERROR_DELETE_STATUS);
            }
            parent::set_json_success();
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_promo_credit->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_promo_credit->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_promo_credit->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        }
    }

    protected function save_update() {
        $date = explode(" - ", parent::get_input_post("date_from"));

        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = parent::get_input_post("seq");

        $get_data = $sel_data = $this->M_promo_credit->get_data($selected);

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->seq = parent::get_input_post("seq");
        $params->promo_credit_name = parent::get_input_post("promo_credit_name", true, FILL_VALIDATOR, "Nama Kupon", $this->data);
        $params->promo_credit_period_from = $date[0];
        $params->promo_credit_period_to = $date[1];
        $params->minimum_nominal = parent::get_input_post("minimum_nominal", true, FILL_VALIDATOR, "Minimal Nominal", $this->data);
        $params->maximum_nominal = parent::get_input_post("maximum_nominal", true, FILL_VALIDATOR, "Maximal Nominal", $this->data);
        $params->status = parent::get_input_post("status");
        
        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                if ($get_data[0]->status == NEW_STATUS_CODE) {
                    $this->M_promo_credit->trans_begin();
                    $this->M_promo_credit->save_update($params);
                    $this->M_promo_credit->trans_commit();
                } else {
                    throw new Exception(ERROR_UPDATE);
                }
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_promo_credit->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_promo_credit->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_promo_credit->trans_rollback();
            }
        }
    }

    protected function approve() {
        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = parent::get_input_post("key");

        $get_data = $sel_data = $this->M_promo_credit->get_data($selected);

        try {
            if ($get_data[0]->status == NEW_STATUS_CODE) {
                $this->M_promo_credit->trans_begin();
                $this->M_promo_credit->get_approve($selected);
                $this->M_promo_credit->trans_commit();
                parent::set_json_success();
            } else {
                throw new Exception(ERROR_APPROVED);
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_promo_credit->trans_rollback();
            parent::set_json_error($ex, ERROR_APPROVED);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_promo_credit->trans_rollback();
            parent::set_json_error($ex, ERROR_APPROVED);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_promo_credit->trans_rollback();
            parent::set_json_error($ex, ERROR_APPROVED);
        }
    }

    protected function reject() {
        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = parent::get_input_post("key");

        $get_data = $sel_data = $this->M_promo_credit->get_data($selected);

        try {
            if ($get_data[0]->status == NEW_STATUS_CODE) {
                $this->M_promo_credit->trans_begin();
                $this->M_promo_credit->get_reject($selected);
                $this->M_promo_credit->trans_commit();
                parent::set_json_success();
            } else {
                parent::set_json_error($ex, ERROR_REJECT);
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_promo_credit->trans_rollback();
            parent::set_json_error($ex, ERROR_REJECT);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_promo_credit->trans_rollback();
            parent::set_json_error($ex, ERROR_REJECT);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_promo_credit->trans_rollback();
            parent::set_json_error($ex, ERROR_REJECT);
        }
    }

}

?>
