<?php

require_once CONTROLLER_BASE_ADMIN;

class C_menu_admin extends controller_base_admin {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("CON03004", "admin/master/menu");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('admin/master/M_menu_admin');
        $this->load->model('component/M_dropdown_list');
        $this->load->model('component/M_tree_view');

        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_SAVE_DELETE, "save_delete");
        parent::register_event($this->data, ACTION_ADDITIONAL, "get_module_menu");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        if (!$this->input->post()) {
            $this->data[MODULE_NAME] = $this->M_dropdown_list->get_dropdown_menu_module();
            if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_ADD OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_EDIT) {
                $menulst = $this->M_tree_view->get_menu_form(DEFAULT_MODULE_SEQ);
                $this->data[TREE_MENU] = $this->get_menu($menulst, ROOT);
            }
            $this->data[FILTER] = 'admin/master/menu_admin_f.php';
            $this->load->view("admin/master/menu_admin", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                $menulst = $this->M_tree_view->get_menu_form(1);
                $this->data[TREE_MENU] = $this->get_menu($menulst, ROOT);
                $this->data[MODULE_NAME] = $this->M_dropdown_list->get_dropdown_menu_module();

                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/master/menu_admin", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/master/menu_admin", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] != ACTION_ADDITIONAL) {
                        if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                            $this->data[DATA_SUCCESS][SUCCESS] = true;
                            $this->data[DATA_AUTH][FORM_ACTION] = "";
                        }
                        $admin_info[SESSION_DATA] = $this->data;
                        $this->session->set_userdata($admin_info);
                        redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                    }
                }
            }
        }
    }

    public function search() {

        $filter = new stdClass;
        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->module_seq = parent::get_input_post("module_seq");
        $filter->menu_cd = parent::get_input_post("menu_cd");
        $filter->menu_name = parent::get_input_post("menu_name");
        $filter->active = parent::get_input_post("active");

        try {
            $list_data = $this->M_menu_admin->get_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );

        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->module_seq . "/" . $data_row->menu_cd,
                    "module_seq" => $data_row->module_name,
                    "menu_cd" => $data_row->menu_cd,
                    "name" => $data_row->name,
                    "title_name" => $data_row->title_name,
                    "url" => $data_row->url,
                    "order" => $data_row->order,
                    "icon" => $data_row->icon,
                    "detail" => $data_row->detail,
                    "active" => $data_row->active,
                    "created_by" => $data_row->created_by,
                    "created_date" => $data_row->created_date);
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
    }

    protected function get_edit() {

        $selected = new stdClass();
        $key = explode("/", parent::get_input_post("key"));
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->module_seq = $key[0];
        $selected->menu_cd = $key[1];

        try {
            $sel_data = $this->M_menu_admin->get_data($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function get_module_menu() {
        $module_seq = parent::get_input_post("module_seq");
        $menulst = $this->M_tree_view->get_menu_form($module_seq);
        $this->data[TREE_MENU] = $this->get_menu($menulst, ROOT);
        $this->data[MODULE_NAME] = $this->M_dropdown_list->get_dropdown_menu_module();
        $this->load->view("admin/component/treeview/tree_view_menu", $this->data);
    }

    protected function save_add() {

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->module_seq = parent::get_input_post("module_seq", true, FILL_VALIDATOR, "Modul", $this->data);
        $params->parent_menu_cd = parent::get_input_post("parent_menu_cd", true, FILL_VALIDATOR, "Parent Menu", $this->data);
        $params->parent_menu_name = parent::get_input_post("parent_menu_name");
        $params->menu_cd = parent::get_input_post("menu_cd", true, FILL_VALIDATOR, "Menu ", $this->data);
        $params->name = parent::get_input_post("menu_name", true, FILL_VALIDATOR, "Nama Menu", $this->data);
        $params->title_name = parent::get_input_post("title_name", true, FILL_VALIDATOR, "Nama Tampilan", $this->data);
        $params->url = parent::get_input_post("url");
        $params->icon = parent::get_input_post("icon");
        $params->order = parent::get_input_post("order", true, NUMERIC_VALIDATOR, "order", $this->data);
        $params->active = parent::get_input_post("active");
        $params->detail = parent::get_input_post("detail");

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_menu_admin->trans_begin();
                $this->M_menu_admin->save_add($params);
                $this->M_menu_admin->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_menu_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_menu_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_menu_admin->trans_rollback();
            }
        }
    }

    protected function save_update() {

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->old_menu_cd = parent::get_input_post("old_menu_cd");
        $params->old_module_seq = parent::get_input_post("old_module_seq");
        $params->module_seq = parent::get_input_post("module_seq", true, FILL_VALIDATOR, "Kode Menu", $this->data);
        $params->menu_cd = parent::get_input_post("menu_cd", true, FILL_VALIDATOR, "Kode Menu", $this->data);
        $params->parent_menu_cd = parent::get_input_post("parent_menu_cd", true, FILL_VALIDATOR, "Parent Menu", $this->data);
        $params->parent_menu_name = parent::get_input_post("parent_menu_name");
        $params->title_name = parent::get_input_post("title_name", true, FILL_VALIDATOR, "Nama Tampilan", $this->data);
        $params->name = parent::get_input_post("menu_name", true, FILL_VALIDATOR, "Menu", $this->data);
        $params->url = parent::get_input_post("url");
        $params->icon = parent::get_input_post("icon");
        $params->order = parent::get_input_post("order", true, NUMERIC_VALIDATOR, "order", $this->data);
        $params->active = parent::get_input_post("active");
        $params->detail = parent::get_input_post("detail");
        
//        var_dump($params);die();
        
        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_menu_admin->trans_begin();
                $this->M_menu_admin->save_update($params);
                $this->M_menu_admin->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_menu_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_menu_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_menu_admin->trans_rollback();
            }
        }
    }

    protected function save_delete() {

        $params = new stdClass();
        $key = explode("/", parent::get_input_post("key"));
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = $key[1];

        try {
            $this->M_menu_admin->trans_begin();
            $this->M_menu_admin->save_delete($params);
            $this->M_menu_admin->trans_commit();
            parent::set_json_success();
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_menu_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_menu_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_menu_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        }
    }

    protected function get_menu($datas, $parent) {
        static $i = 1;
        $tab = str_repeat(" ", $i);
        if (isset($datas[$parent])) {
            $html = "<ul>";
            $i++;
            foreach ($datas[$parent] as $vals) {

                $child = $this->get_menu($datas, $vals->menu_cd);
                $html .= "$tab";
                if ($vals->active == "1") {
                    $html.='<li><a href="javascript:pilihmenu(\'' . $vals->menu_cd . '~' . $vals->name . '~' . '\')">' . $vals->name . '</a>';
                } else {
                    $html.='<li><a href="javascript:pilihmenu(\'' . $vals->menu_cd . '~' . $vals->name . '~' . '\')"><strike>' . $vals->name . '</strike></a>';
                }
                if ($child) {
                    $i++;
                    $html .= $child;
                    $html .= "$tab";
                }
                $html .= '</li>';
            }
            $html .= "$tab</ul>";
            return $html;
        } else {
            return false;
        }
    }

}
?>

