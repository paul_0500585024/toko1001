<?php

require_once CONTROLLER_BASE_ADMIN;

class C_promo_credit_admin_bank extends controller_base_admin {

    private $data;
    private $hseq;

    public function __construct() {
        $this->data = parent::__construct("CON05012", "admin/master/promo_credit_bank");
        $this->initialize();
    }

    private function initialize() {
        $this->hseq = $this->uri->segment(4);
        $this->load->model('admin/master/M_promo_credit');

        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_ADDITIONAL, "get_dropdown_var");
        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        if (!$this->input->post()) {
            if ($this->data[DATA_AUTH][FORM_ACTION] == "") {
                $this->get_data_header();
            }
            $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
            $this->load->view("admin/master/promo_credit_bank", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/master/promo_credit_bank", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/master/promo_credit_bank", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq));
                }
            }
        }
    }

    protected function get_data_header() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = $this->hseq;
        $params->promo_seq = $this->hseq;

        $get_data = $this->M_promo_credit->get_data_credit_bank($params);
        try {
            $sel_data = $this->M_promo_credit->get_data($params);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
                $this->data[DATA_SELECTED][LIST_DATA][] = $get_data;
            } else {
                $this->load->view("errors/html/error_404");
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_promo_credit->trans_rollback();
            $this->load->view("errors/html/error_404");
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_promo_credit->trans_rollback();
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_promo_credit->trans_rollback();
        }
    }

    protected function save_add() {
        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->promo_seq = $this->hseq;
        $selected->key = $this->hseq;
        $result = $this->input->post("bank");
        $selected->promo_credit_period_from = parent::get_input_post("old_promo_credit_period_from");
        $selected->promo_credit_period_to = parent::get_input_post('old_promo_credit_period_to');
        $selected->promo_credit_name = parent::get_input_post('old_promo_credit_name');

        $get_data = $this->M_promo_credit->get_data_credit_bank($selected);
        $get_status = $this->M_promo_credit->get_data($selected);
        $this->data[DATA_SELECTED][LIST_DATA][] = $selected;
        $this->data[DATA_SELECTED][LIST_DATA][1] = $get_data;

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                if ($get_status[0]->status == NEW_STATUS_CODE) {
                    if ($get_data == NULL) {
                        $this->M_promo_credit->trans_begin();
                        foreach ($result as $each) {
                            $selected->promo_seq = $this->hseq;
                            $selected->bank_seq = $each;
                            $this->M_promo_credit->save_add_bank($selected);
                        }
                        $this->M_promo_credit->trans_commit();
                    } else {
                        $this->M_promo_credit->save_delete_bank($selected);
                        $params = new stdClass();
                        $params->user_id = parent::get_admin_user_id();
                        $params->ip_address = parent::get_ip_address();
                        $params->promo_seq = $this->hseq;
                        $data_new = $this->input->post("bank");
                        $this->M_promo_credit->trans_begin();

                        foreach ($data_new as $data) {

                            $params->promo_seq = $this->hseq;
                            $params->bank_seq = $data;
                            $this->M_promo_credit->save_add_bank($params);
                        }
                        $this->M_promo_credit->trans_commit();
                    }
                } else {
                    throw new Exception(ERROR_SAVE_ADD);
                }
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_promo_credit->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_promo_credit->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_promo_credit->trans_rollback();
            }
        }
    }

}

?>
