<?php

require_once CONTROLLER_BASE_ADMIN;

class C_member_admin extends controller_base_admin {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("MST07000", "admin/master/member");
        $this->initialize();
    }

    private function initialize() {

        $this->load->model('admin/master/M_member_admin');
        $this->load->model('component/M_voucher');

        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
//        parent::register_event($this->data, ACTION_APPROVE, "save_approve");
//        parent::register_event($this->data, ACTION_REJECT, "save_reject");
        parent::register_event($this->data, ACTION_ADDITIONAL, "save_upload");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {

        if (!$this->input->post()) {
            $this->data[FILTER] = 'admin/master/member_admin_f.php';
            $this->load->view("admin/master/member_admin", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/master/member_admin", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/master/member_admin", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_ADDITIONAL) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_SEARCH;
                        $this->load->view("admin/master/member_admin", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    public function search() {

        $filter = new stdClass;
        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->email = parent::get_input_post("email");
        $filter->name = parent::get_input_post("name");
        $filter->status = parent::get_input_post("status");


        try {
            $list_data = $this->M_member_admin->get_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );
        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                if ($data_row->status == UNVERIFIED) {
                    $chekbox = "<input id='printc' type='checkbox' name='printc[]' value='{$data_row->seq}' class='prntorder'>";
                } else {
                    $chekbox = "";
                }
                $row = array("DT_RowId" => $data_row->seq,
                    "email" => parent::cdef($data_row->email),
                    "name" => parent::cdef($data_row->name),
                    "birthday" => parent::cdate($data_row->birthday),
                    "gender" => parent::cgend($data_row->gender),
                    "mobile_phone" => parent::cdef($data_row->mobile_phone),
                    "bank_name" => parent::cdef($data_row->bank_name),
                    "bank_branch_name" => parent::cdef($data_row->bank_branch_name),
                    "bank_acct_no" => parent::cdef($data_row->bank_acct_no),
                    "bank_acct_name" => parent::cdef($data_row->bank_acct_name),
                    "profile_img" => $data_row->profile_img,
                    "deposit_amt" => parent::cnum($data_row->deposit_amt),
                    "status" => parent::cstdes($data_row->status, STATUS_MEMBER),
                    "checked" => $chekbox,
                    "created_by" => $data_row->created_by,
                    "created_date" => parent::cdate($data_row->created_date, 1),
                    "modified_by" => $data_row->modified_by,
                    "modified_date" => parent::cdate($data_row->modified_date, 1));

                $output['aaData'][] = $row;
            }
        };

        echo json_encode($output);
    }

    protected function get_edit() {

        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = parent::get_input_post("key");

        try {
            $sel_data = $this->M_member_admin->get_data($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function validateDate($date) {
        $d = DateTime::createFromFormat('Y-m-d', $date);
        return $d && $d->format('Y-m-d') == $date;
    }

    protected function save_upload() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $file = parent::upload_file(UPLOADID, UPLOADPATH, FILENAME, CSV_FILE, $this->data);

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                if ($file->error === false) {
                    $this->load->library(CSV);
                    $result = $this->csv_reader->parse_file($file->url_file . ".csv");
                    $this->M_member_admin->trans_begin();
                    foreach ($result as $field) {
                        $params->email = $field['email'];
                        $params->name = $field['name'];
                        $params->birthday = $field['birthday'];
                        $params->gender = $field['gender'];
                        $params->mobile_phone = $field['mobile_phone'];
                        $params->status = UNVERIFIED;

                        if ($params->birthday == "") {
                            throw new Exception(ERROR_VALIDATION_MUST_BIRTHDAY);
                        }
                        if ($this->validateDate($params->birthday) == FALSE) {
                            throw new Exception(ERROR_DATE_INVALID);
                        }
//                        if ($params->gender != DEFAULT_FEMALE_GENDER || $params->gender != DEFAULT_MALE_GENDER) {
//                            throw new Exception(ERROR_GENDER);
//                        }
                        if (filter_var($params->email, FILTER_VALIDATE_EMAIL) === false) {
                            throw new Exception(ERROR_VALIDATION_MUST_BIRTHDAY);
                        }
                        $this->M_member_admin->save_upload_member($params);
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        parent::Unlink_file($file->url_file);
                    }
                    $this->M_member_admin->trans_commit();
                } else {
                    throw new Exception(ERROR_UPLOAD);
                }
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_member_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_member_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_member_admin->trans_rollback();
            }
        }
    }

    public function save_approve_all() {
        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->member = $this->input->post('key');

        try {
            $this->M_member_admin->trans_begin();
            foreach ($selected->member as $data) {
                $password = parent::generate_random_alnum(8);
                $active_date = date('d-M-Y');
                $selected->key = $data;
                $selected->new_password = md5(md5($password));
                $get_data = $this->M_member_admin->get_data($selected);
                $this->M_member_admin->save_approve($selected);
                $get_vocher = $this->M_member_admin->get_data_voucher($selected);

                $exp_days = $get_vocher[0]->exp_days;
                $date = date('d-M-Y', strtotime('+' . $exp_days . 'days', strtotime($active_date)));

                $params = new stdClass();
                $params->user_id = parent::get_admin_user_id();
                $params->ip_address = parent::get_ip_address();
                $params->code = "MEMBER_PROMO";
                $params->RECIPIENT_NAME = $get_data[0]->name;
                $params->NOMINAL_PROMO = "Rp." . number_format($get_vocher[0]->nominal);
                $params->EMAIL_MEMBER = $get_data[0]->email;
                $params->PASSWORD = $password;
                $params->LINK = base_url();
                $params->to_email = $get_data[0]->email;
                parent::email_template($params);

                $filter = new stdClass();
                $filter->member_seq = $data;
                $filter->active_date = $active_date;
                $filter->exp_date = $date;
                $this->M_member_admin->update_voucher($filter);
            }
            $this->M_member_admin->trans_commit();
            parent::set_json_success();
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_member_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_APPROVED);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_member_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_APPROVED);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_member_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_APPROVED);
        }
    }

}

?>