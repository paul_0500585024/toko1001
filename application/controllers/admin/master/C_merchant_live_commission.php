<?php

require_once CONTROLLER_BASE_ADMIN;

class C_merchant_live_commission extends controller_base_admin {

    private $data;
    private $hseq;

    public function __construct() {
	$this->data = parent::__construct("MST05008", "admin/master/merchant_live_commission");
	$this->initialize();
    }

    private function initialize() {
	$this->hseq = $this->uri->segment(4);
	$this->load->model('admin/master/M_merchant_live');
	parent::register_event($this->data, ACTION_EDIT, "get_edit");
	parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");

	if ($this->data[DATA_INIT] === true) {
	    parent::fire_event($this->data);
	}
    }

    public function index() {
	if (!$this->input->post()) {
	    if ($this->data[DATA_AUTH][FORM_ACTION] == "") {
		$this->get_edit($this->input->get("log_date"));
		$this->get_data_header();
	    }
	    $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
	    $this->load->view("admin/master/merchant_live_commission", $this->data);
	} else {
	    if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
		if ($this->data[DATA_ERROR][ERROR] === true) {
		    $this->get_edit();
		    $this->get_data_header();
		    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
			$this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
			$this->load->view("admin/master/merchant_live_commission", $this->data);
		    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
			$this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
			$this->load->view("admin/master/merchant_live_commission", $this->data);
		    }
		} else {
		    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
			$this->data[DATA_SUCCESS][SUCCESS] = true;
			$this->data[DATA_AUTH][FORM_ACTION] = "";
		    }
		    $admin_info[SESSION_DATA] = $this->data;
		    $this->session->set_userdata($admin_info);
		    redirect(base_url("admin/master/merchant_live_commission/" . $this->hseq));
		}
	    }
	}
    }

    protected function get_data_header() {
	$params = new stdClass();
	$params->user_id = parent::get_admin_user_id();
	$params->ip_address = parent::get_ip_address();
	$params->seq = $this->hseq;
	
	try {
	    $sel_data = $this->M_merchant_live->get_data($params);
	    if (isset($sel_data)) {
		parent::set_data($this->data, $sel_data);
	    }
	} catch (BusisnessException $ex) {
	    parent::set_error($this->data, $ex);
	} catch (TechnicalException $ex) {
	    parent::set_error($this->data, $ex);
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	}
    }

    protected function get_edit($log_date = '') {
	$selected = new stdClass();
	$selected->user_id = parent::get_admin_user_id();
	$selected->ip_address = parent::get_ip_address();
	$selected->seq = $this->hseq;
	$selected->log_date = $log_date;
	try {
	    $this->data[LIST_DATA] = $this->M_merchant_live->get_commission_log_list($selected);
	    if ($log_date == '') {
		$this->data[KEY] = '';
		$this->data[TREE_MENU] = $this->M_merchant_live->get_data_commission($selected);
	    } else {
		$this->data[KEY] = $log_date;
		$this->data[TREE_MENU] = $this->M_merchant_live->get_data_commission_log($selected);
	    }
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	}
    }

    protected function save_update() {
	$params = new stdClass();
	$params->user_id = parent::get_admin_user_id();
	$params->ip_address = parent::get_ip_address();
	$params->seq = $this->input->post("seq");
	$params->hseq = $this->input->post("hseq");
	$params->lvlcat = $this->input->post("lvlcat");
	$params->mfee = $this->input->post("mfee");

	$this->data[DATA_SELECTED][LIST_DATA][] = $params;

	if ($this->data[DATA_ERROR][ERROR] === false) {
	    try {
		$this->M_merchant_live->trans_begin();
		$this->M_merchant_live->save_update_commission($params);
		$this->M_merchant_live->trans_commit();
	    } catch (BusisnessException $ex) {
		parent::set_error($this->data, $ex);
		$this->M_merchant_live->trans_rollback();
	    } catch (TechnicalException $ex) {
		parent::set_error($this->data, $ex);
		$this->M_merchant_live->trans_rollback();
	    } catch (Exception $ex) {
		parent::set_error($this->data, $ex);
		$this->M_merchant_live->trans_rollback();
	    }
	}
    }

}

?>