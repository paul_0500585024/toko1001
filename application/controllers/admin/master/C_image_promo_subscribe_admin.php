<?php

require_once CONTROLLER_BASE_ADMIN;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Redeem Period
 *
 * @author Jartono
 */
class C_image_promo_subscribe_admin extends controller_base_admin {

    private $data;

    public function __construct() {
	$this->data = parent::__construct("CON05006", "admin/master/promo_subscribe");
	$this->initialize();
    }

    private function initialize() {
	$this->load->model('admin/master/M_image_promo_subscribe_admin');
	$this->load->model('component/M_email');
	parent::register_event($this->data, ACTION_SEARCH, "search");
	parent::register_event($this->data, ACTION_EDIT, "get_edit");
	parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
	parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
	parent::register_event($this->data, ACTION_SAVE_DELETE, "save_delete");
	parent::register_event($this->data, ACTION_APPROVE, "approve");
	parent::register_event($this->data, ACTION_REJECT, "reject");
	parent::fire_event($this->data);
    }

    public function index() {
	if (!$this->input->post()) {
	    $this->data[FILTER] = 'admin/master/image_promo_subscribe_admin_f.php';
	    $this->load->view("admin/master/image_promo_subscribe_admin", $this->data);
	} else {
	    if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
		if ($this->data[DATA_ERROR][ERROR] === true) {
		    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
			$this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
			$this->load->view("admin/master/image_promo_subscribe_admin", $this->data);
		    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
			$this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
			$this->load->view("admin/master/image_promo_subscribe_admin", $this->data);
		    }
		} else {
		    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
			$this->data[DATA_SUCCESS][SUCCESS] = true;
			$this->data[DATA_AUTH][FORM_ACTION] = "";
		    }
		    $admin_info[SESSION_DATA] = $this->data;
		    $this->session->set_userdata($admin_info);
		    redirect(base_url("admin/master/promo_subscribe"));
		}
	    }
	}
    }

    protected function get_status_redeem($idh) {
	$params = new stdClass();
	$params->user_id = parent::get_admin_user_id();
	$params->ip_address = parent::get_ip_address();
	$params->seq = $idh;
	$return = "ALL";
	$sel_data = $this->M_image_promo_subscribe_admin->get_list_detail_status($params);
	if (isset($sel_data)) {
	    foreach ($sel_data as $data_row) {
		if ($data_row->status == "U")
		    $return = "NOTALL";
	    }
	}
	return $return;
    }

    protected function search() {
	$params = new stdClass;
	$params->user_id = parent::get_admin_user_id();
	$params->ip_address = parent::get_ip_address();
	$params->start = parent::get_input_post("start");
	$params->length = parent::get_input_post("length");
	$params->order = parent::get_input_post("order");
	$params->column = parent::get_input_post("column");
	$params->promo = parent::get_input_post("promo");
	$params->promo_img_from_date = parent::get_input_post("tanggal1");
	$params->promo_img_to_date = parent::get_input_post("tanggal2");
	$params->status = parent::get_input_post("status");
	try {
	    $list_data = $this->M_image_promo_subscribe_admin->get_list($params);
	    parent::set_list_data($this->data, $list_data);
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	}

	$output = array(
	    "sEcho" => parent::get_input_post("draw"),
	    "iTotalRecords" => $list_data[0][0]->total_rec,
	    "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
	    "aaData" => array()
	);

	if (isset($list_data[1])) {
	    foreach ($list_data[1] as $data_row) {
		$row = array("DT_RowId" => $data_row->seq,
		    "promo_name" => $data_row->promo_name,
		    "promo_img_from_date" => parent::cdate($data_row->promo_img_from_date) . ' s/d ' . parent::cdate($data_row->promo_img_to_date),
		    "status" => parent::cstdes($data_row->status, STATUS_ANR),
		    "created_by" => $data_row->created_by,
		    "created_date" => parent::cdate($data_row->created_date, 1),
		    "modified_by" => $data_row->modified_by,
		    "modified_date" => parent::cdate($data_row->modified_date, 1));
		$output['aaData'][] = $row;
	    }
	};
	echo json_encode($output);
    }

    protected function get_edit() {
	$params = new stdClass();
	$params->user_id = parent::get_admin_user_id();
	$params->ip_address = parent::get_ip_address();
	$params->seq = parent::get_input_post("key");
	try {
	    $sel_data = $this->M_image_promo_subscribe_admin->get_data($params);
	    if (isset($sel_data)) {
		parent::set_data($this->data, $sel_data);
	    }
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	}
    }

    protected function save_add() {
	$params = new stdClass();
	$params->user_id = parent::get_admin_user_id();
	$params->ip_address = parent::get_ip_address();
	$params->promo_name = parent::clength(parent::get_input_post("promo_name", true, FILL_VALIDATOR, "Nama Promo", $this->data), 200, $this->data);
	$params->promo_img_from_date = parent::get_input_post("promo_img_from_date", true, DATE_VALIDATOR, "Periode Awal", $this->data);
	$params->promo_img_to_date = parent::get_input_post("promo_img_to_date", true, DATE_VALIDATOR, "Periode Akhir", $this->data);
	$params->promo_img_text = parent::get_input_post("promo_img_text", true, FILL_VALIDATOR, "Isi Promo", $this->data);
	$this->data[DATA_SELECTED][LIST_DATA][] = $params;
	if ($this->data[DATA_ERROR][ERROR] === false) {
	    try {
		$this->M_image_promo_subscribe_admin->trans_begin();
		$this->M_image_promo_subscribe_admin->save_add($params);
		$this->M_image_promo_subscribe_admin->trans_commit();
	    } catch (BusisnessException $ex) {
		parent::set_error($this->data, $ex);
		$this->M_image_promo_subscribe_admin->trans_rollback();
	    } catch (TechnicalException $ex) {
		parent::set_error($this->data, $ex);
		$this->M_image_promo_subscribe_admin->trans_rollback();
	    } catch (Exception $ex) {
		parent::set_error($this->data, $ex);
		$this->M_image_promo_subscribe_admin->trans_rollback();
	    }
	}
    }

    protected function save_delete() {
	$params = new stdClass();
	$params->user_id = parent::get_admin_user_id();
	$params->ip_address = parent::get_ip_address();
	$params->seq = parent::get_input_post("key");
	$sel_data = $this->M_image_promo_subscribe_admin->get_data($params);
	if (isset($sel_data)) {
	    if ($sel_data[0]->status != NEW_STATUS_CODE)
		parent::set_json_error('', ERROR_DELETE);
	} else {
	    parent::set_json_error('', ERROR_DELETE);
	}
	try {
	    $this->M_image_promo_subscribe_admin->trans_begin();
	    $this->M_image_promo_subscribe_admin->save_delete($params);
	    $this->M_image_promo_subscribe_admin->trans_commit();
	    parent::set_json_success();
	} catch (BusisnessException $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_image_promo_subscribe_admin->trans_rollback();
	    parent::set_json_error($ex, ERROR_DELETE);
	} catch (TechnicalException $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_image_promo_subscribe_admin->trans_rollback();
	    parent::set_json_error($ex, ERROR_DELETE);
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_image_promo_subscribe_admin->trans_rollback();
	    parent::set_json_error($ex, ERROR_DELETE);
	}
	die();
    }

    protected function save_update() {
	$params = new stdClass();
	$params->user_id = parent::get_admin_user_id();
	$params->ip_address = parent::get_ip_address();
	$params->seq = parent::get_input_post("seq");
	$sel_data = $this->M_image_promo_subscribe_admin->get_data($params);
	$params->status = '';
	if (isset($sel_data)) {
	    if ($sel_data[0]->status != NEW_STATUS_CODE) {
		$params->status = $sel_data[0]->status;
		$this->data[DATA_ERROR][ERROR] = true;
		$this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_UPDATE;
	    }
	} else {
	    $params->status = 0;
	    $this->data[DATA_ERROR][ERROR] = true;
	    $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_SAVE;
	}
	$params->promo_name = parent::clength(parent::get_input_post("promo_name", true, FILL_VALIDATOR, "Nama Promo", $this->data), 200, $this->data);
	$params->promo_img_from_date = parent::get_input_post("promo_img_from_date", true, DATE_VALIDATOR, "Periode Awal", $this->data);
	$params->promo_img_to_date = parent::get_input_post("promo_img_to_date", true, DATE_VALIDATOR, "Periode Akhir", $this->data);
	$params->promo_img_text = parent::get_input_post("promo_img_text", true, FILL_VALIDATOR, "Isi Promo", $this->data);
	$this->data[DATA_SELECTED][LIST_DATA][] = $params;
	if ($this->data[DATA_ERROR][ERROR] === false) {
	    try {
		$this->M_image_promo_subscribe_admin->trans_begin();
		$this->M_image_promo_subscribe_admin->save_update($params);
		$this->M_image_promo_subscribe_admin->trans_commit();
	    } catch (BusisnessException $ex) {
		parent::set_error($this->data, $ex);
		$this->M_image_promo_subscribe_admin->trans_rollback();
	    } catch (TechnicalException $ex) {
		parent::set_error($this->data, $ex);
		$this->M_image_promo_subscribe_admin->trans_rollback();
	    } catch (Exception $ex) {
		parent::set_error($this->data, $ex);
		$this->M_image_promo_subscribe_admin->trans_rollback();
	    }
	}
    }

    protected function reject() {
	$selected = new stdClass();
	$selected->user_id = parent::get_admin_user_id();
	$selected->ip_address = parent::get_ip_address();
	$selected->seq = parent::get_input_post("key");
	$sel_data = $this->M_image_promo_subscribe_admin->get_data($selected);
	$status = "";
	$output = "";
	if (!isset($sel_data) || ($sel_data[0]->status != NEW_STATUS_CODE )) {
	    parent::set_json_error('', ERROR_REJECT);
	}
	try {
	    $this->M_image_promo_subscribe_admin->trans_begin();
	    $selected->status = "R";
	    $this->M_image_promo_subscribe_admin->save_status($selected);
	    $this->M_image_promo_subscribe_admin->trans_commit();
	    $status = "OK";
	    parent::set_json_success();
	} catch (BusisnessException $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_image_promo_subscribe_admin->trans_rollback();
	    parent::set_json_error($ex, ERROR_REJECT);
	} catch (TechnicalException $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_image_promo_subscribe_admin->trans_rollback();
	    parent::set_json_error($ex, ERROR_REJECT);
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_image_promo_subscribe_admin->trans_rollback();
	    parent::set_json_error($ex, ERROR_REJECT);
	}
	die();
    }

    protected function approve() {
	$selected = new stdClass();
	$selected->user_id = parent::get_admin_user_id();
	$selected->ip_address = parent::get_ip_address();
	$selected->seq = parent::get_input_post("key");
	$status = "";
	$gen_pass = "";
	$sel_data = $this->M_image_promo_subscribe_admin->get_data($selected);
	if (!isset($sel_data) || ($sel_data[0]->status != NEW_STATUS_CODE)) {
	    parent::set_json_error('', ERROR_APPROVED);
	}
	try {
	    $this->M_image_promo_subscribe_admin->trans_begin();
	    $selected->status = "A";
	    $this->M_image_promo_subscribe_admin->save_status($selected);
	    $this->M_image_promo_subscribe_admin->trans_commit();
	    $status = "OK";
	    parent::set_json_success();
	} catch (BusisnessException $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_image_promo_subscribe_admin->trans_rollback();
	    parent::set_json_error($ex, ERROR_APPROVED);
	} catch (TechnicalException $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_image_promo_subscribe_admin->trans_rollback();
	    parent::set_json_error($ex, ERROR_APPROVED);
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_image_promo_subscribe_admin->trans_rollback();
	    parent::set_json_error($ex, ERROR_APPROVED);
	}
	die();
    }

}

?>