<?php

require_once CONTROLLER_BASE_ADMIN;

class C_user_admin extends controller_base_admin {

    private $data;

    public function __construct() {

        $this->data = parent::__construct("CON02002", "admin/master/user");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('admin/master/M_user_admin');
        $this->load->model('component/M_dropdown_list');

        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_SAVE_DELETE, "save_delete");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        $this->data[USER_GROUP_NAME] = $this->M_dropdown_list->get_dropdown_user_group();

        if (!$this->input->post()) {
            $this->data[FILTER] = 'admin/master/user_admin_f.php';
            $this->load->view("admin/master/user_admin", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/master/user_admin", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/master/user_admin", $this->data);
                    }
                } elseif ($this->data[DATA_ERROR][ERROR] === false) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    public function search() {

        $filter = new stdClass;
        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->account_id = parent::get_input_post("user_id");
        $filter->user_name = parent::get_input_post("user_name");
        $filter->user_group_seq = parent::get_input_post("user_group_seq");
        $filter->active = parent::get_input_post("active");

        try {
            $list_data = $this->M_user_admin->get_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );

        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->user_id,
                    "user_id" => $data_row->user_id,
                    "user_name" => $data_row->user_name,
                    "name" => $data_row->name,
                    "password" => $data_row->password,
                    "email" => $data_row->email,
                    "description" => $data_row->description,
                    "active" => parent::cstat($data_row->active),
                    "created_by" => $data_row->created_by,
                    "created_date" => parent::cdate($data_row->created_date, 1),
                    "modified_by" => $data_row->modified_by,
                    "modified_date" => parent::cdate($data_row->modified_date, 1));
                $output['aaData'][] = $row;
            }
        };

        echo json_encode($output);
    }

    protected function get_edit() {

        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = parent::get_input_post("key");

        try {
            $sel_data = $this->M_user_admin->get_data($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_add() {

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->account_id = parent::get_input_post("account_id", true, FILL_VALIDATOR, "ID User", $this->data);
        $params->user_name = parent::get_input_post("user_name", true, FILL_VALIDATOR, "Nama User", $this->data);
        $params->user_group_seq = parent::get_input_post("user_group_seq", true, FILL_VALIDATOR, "Kelompok User", $this->data);
        $params->password = md5(md5($this->input->post("password", true, FILL_VALIDATOR, "Password", $this->data)));
        $params->email = parent::get_input_post("email");
        $params->description = parent::get_input_post("description");
        $params->active = parent::get_input_post("active");

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_user_admin->trans_begin();
                $this->M_user_admin->save_add($params);
                $this->M_user_admin->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_user_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_user_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_user_admin->trans_rollback();
            }
        }
    }

    protected function save_update() {

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->old_account_id = parent::get_input_post("old_user_id");
        $params->account_id = parent::get_input_post("account_id", true, FILL_VALIDATOR, "ID User", $this->data);
        $params->user_name = parent::get_input_post("user_name", true, FILL_VALIDATOR, "Nama User", $this->data);
        ;
        $params->user_group_seq = parent::get_input_post("user_group_seq", true, FILL_VALIDATOR, "Kelompok User", $this->data);
        if ($this->input->post("password") != DEFAULT_PASSWORD) {
            $params->password = md5(md5($this->input->post("password")));
        } else {
            $params->password = "";
        }
        $params->email = parent::get_input_post("email");
        $params->description = parent::get_input_post("description");
        $params->active = parent::get_input_post("active");

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_user_admin->trans_begin();
                $this->M_user_admin->save_update($params);
                $this->M_user_admin->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_user_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_user_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_user_admin->trans_rollback();
            }
        }
    }

    protected function save_delete() {

        $params = new stdClass();

        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = parent::get_input_post("key");
        
        try {
            $this->M_user_admin->trans_begin();
            $this->M_user_admin->save_delete($params);
            $this->M_user_admin->trans_commit();
            parent::set_json_success();
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_user_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_user_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_user_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        }
    }

}

?>
