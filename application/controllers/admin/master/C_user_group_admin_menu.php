<?php

require_once CONTROLLER_BASE_ADMIN;

class C_user_group_admin_menu extends controller_base_admin {

    private $data;
    private $hseq;

    public function __construct() {
        $this->data = parent::__construct("CON02003", "admin/master/user_group_menu");
        $this->initialize();
    }

    private function initialize() {

        $this->hseq = $this->uri->segment(4);

        $this->load->model('admin/master/M_user_group_admin');
        $this->load->model('component/M_dropdown_list');
        $this->load->model('component/M_tree_view');

        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_SAVE_DELETE, "save_delete");
        parent::register_event($this->data, ACTION_ADDITIONAL, "get_module_menu");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {

        if (!$this->input->post()) {
            $this->get_data_header();
            $this->data[MODULE_NAME] = $this->M_dropdown_list->get_dropdown_menu_module();
            if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_ADD OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_EDIT) {
                $menulst = $this->M_tree_view->get_menu_form(DEFAULT_MODULE_SEQ);
                $this->data[TREE_MENU] = $this->get_menu($menulst, ROOT);
            }
            $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
            $this->load->view("admin/master/user_group_admin_menu", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                $menulst = $this->M_tree_view->get_menu_form(DEFAULT_MODULE_SEQ);
                $this->data[TREE_MENU] = $this->get_menu($menulst, ROOT);
                $this->data[MODULE_NAME] = $this->M_dropdown_list->get_dropdown_menu_module();
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/master/user_group_admin_menu", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/master/user_group_admin_menu", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] != ACTION_ADDITIONAL) {
                        if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                            $this->data[DATA_SUCCESS][SUCCESS] = true;
                            $this->data[DATA_AUTH][FORM_ACTION] = "";
                        }
                        $admin_info[SESSION_DATA] = $this->data;
                        $this->session->set_userdata($admin_info);
                        redirect(base_url($this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq));
                    }
                }
            }
        }
    }

    public function search() {

        $filter = new stdClass;
        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->user_group_seq = $this->hseq;

        try {
            $list_data = $this->M_user_group_admin->get_list_menu($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );

        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->menu_cd,
                    "menu_cd" => parent::cdef($data_row->menu_cd),
                    "name" => parent::cdef($data_row->name),
                    "can_add" => parent::cstat($data_row->can_add),
                    "can_edit" => parent::cstat($data_row->can_edit),
                    "can_view" => parent::cstat($data_row->can_view),
                    "can_delete" => parent::cstat($data_row->can_delete),
                    "can_print" => parent::cstat($data_row->can_print),
                    "can_auth" => parent::cstat($data_row->can_auth),
                    "active" => parent::cstat($data_row->active),
                    "created_by" => $data_row->created_by,
                    "created_date" => parent::cdate($data_row->created_date, 1),
                    "modified_by" => $data_row->modified_by,
                    "modified_date" => parent::cdate($data_row->modified_date, 1));
                $output['aaData'][] = $row;
            }
        };

        echo json_encode($output);
    }

    protected function get_data_header() {

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = $this->hseq;

        try {
            $sel_data = $this->M_user_group_admin->get_data($params);
            if (isset($sel_data)) {
                parent::set_data_header($this->data, $sel_data);
            } else {
                $this->load->view("errors/html/error_404");
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function get_edit() {

        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->user_group_seq = $this->hseq;
        $selected->key = parent::get_input_post("key");

        try {
            $sel_data = $this->M_user_group_admin->get_data_menu($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function get_module_menu() {
        $task = parent::get_input_post("task");
        switch ($task) {
            case TASK_MENU_CHANGE : {
                    $menu_cd = parent::get_input_post("menu_cd");
                    $module_seq = parent::get_input_post("module_seq");
                    
                    $selected = new stdClass();
                    $selected->user_id = parent::get_admin_user_id();
                    $selected->ip_address = parent::get_ip_address();
                    $selected->user_group_seq = $this->hseq;
                    $selected->key = $menu_cd;                    
                    echo json_encode($this->M_user_group_admin->get_data_menu($selected));
                    die();
                }break;
            default : {
                    $module_seq = parent::get_input_post("module_seq");
                    $menulst = $this->M_tree_view->get_menu_form($module_seq);
                    $this->data[TREE_MENU] = $this->get_menu($menulst, ROOT);
                    $this->data[MODULE_NAME] = $this->M_dropdown_list->get_dropdown_menu_module();
                    $this->load->view("admin/component/treeview/tree_view_menu", $this->data);
                }break;
        }
    }

    protected function save_add() {

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->menu_cd = parent::get_input_post("menu_cd", true, FILL_VALIDATOR, "Menu", $this->data);
        $params->module_seq = parent::get_input_post("module_seq");
        $params->menu_name = parent::get_input_post("menu_name");
        $params->name = parent::get_input_post("name");
        $params->seq = parent::get_input_post("seq", true, FILL_VALIDATOR, "Kelompok User", $this->data);
        $params->can_add = parent::get_input_post("can_add");
        $params->can_edit = parent::get_input_post("can_edit");
        $params->can_view = parent::get_input_post("can_view");
        $params->can_delete = parent::get_input_post("can_delete");
        $params->can_print = parent::get_input_post("can_print");
        $params->can_auth = parent::get_input_post("can_auth");

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_user_group_admin->trans_begin();
                $this->M_user_group_admin->save_add_menu($params);
                $this->M_user_group_admin->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_user_group_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_user_group_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_user_group_admin->trans_rollback();
            }
        }
    }

    protected function save_update() {

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->seq = parent::get_input_post("seq", true, FILL_VALIDATOR, "Kelompok User", $this->data);
        $params->old_menu_cd = parent::get_input_post("old_menu_cd");
        $params->menu_cd = parent::get_input_post("menu_cd", true, FILL_VALIDATOR, "Menu", $this->data);
        $params->module_seq = parent::get_input_post("module_seq");
        $params->menu_name = parent::get_input_post("menu_name");
        $params->name = parent::get_input_post("name");
        $params->can_add = parent::get_input_post("can_add");
        $params->can_edit = parent::get_input_post("can_edit");
        $params->can_view = parent::get_input_post("can_view");
        $params->can_delete = parent::get_input_post("can_delete");
        $params->can_print = parent::get_input_post("can_print");
        $params->can_auth = parent::get_input_post("can_auth");

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_user_group_admin->trans_begin();
                $this->M_user_group_admin->save_update_menu($params);
                $this->M_user_group_admin->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_user_group_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_user_group_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_user_group_admin->trans_rollback();
            }
        }
    }

    protected function save_delete() {

        $params = new stdClass();
        
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = parent::get_input_post("key");

        try {
            $this->M_user_group_admin->trans_begin();
            $this->M_user_group_admin->save_delete_menu($params);
            $this->M_user_group_admin->trans_commit();
            parent::set_json_success();
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_user_group_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_user_group_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_user_group_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        }
    }

    protected function get_menu($datas, $parent) {
        static $i = 1;
        $tab = str_repeat(" ", $i);
        if (isset($datas[$parent])) {
            $html = "<ul>";
            $i++;
            foreach ($datas[$parent] as $vals) {

                $child = $this->get_menu($datas, $vals->menu_cd);
                $html .= "$tab";

                if (!($child)) {
                    if ($vals->active == "1") {
                        $html.='<li><a href="javascript:pilihmenu(\'' . $vals->menu_cd . '~' . $vals->name . '~' . '\')">' . $vals->name . '</a>';
                    } else {
                        $html.='<li><a href="javascript:pilihmenu(\'' . $vals->menu_cd . '~' . $vals->name . '~' . '\')"><strike>' . $vals->name . '</strike></a>';
                    }
                } else {
                    if ($vals->active == "1") {
                        $html.='<li>' . $vals->name . '</a>';
                    } else {
                        $html.='<li><strike>' . $vals->name . '</strike></a>';
                    }
                }
                if ($child) {
                    $i++;
                    $html .= $child;
                    $html .= "$tab";
                }
                $html .= '</li>';
            }
            $html .= "$tab</ul>";
            return $html;
        } else {
            return false;
        }
    }

}

?>
