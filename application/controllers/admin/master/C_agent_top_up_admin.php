<?php

require_once CONTROLLER_BASE_ADMIN;

class C_agent_top_up_admin extends controller_base_admin {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("MST09002", "admin/master/agent_top_up");
        $this->initialize();
    }

    private function initialize() {

        $this->load->model('admin/master/M_agent_top_up_admin');
        $this->load->model('component/M_dropdown_list');
        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_APPROVE, "save_approve");
        parent::register_event($this->data, ACTION_REJECT, "save_reject");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_SAVE_DELETE, "save_delete");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        $this->data[MEMBER_EMAIL] = $this->M_dropdown_list->get_dropdown_agent_email();
        if (!$this->input->post()) {
            $this->data[FILTER] = 'admin/master/agent_top_up_admin_f.php';
            $this->load->view("admin/master/agent_top_up_admin", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/master/agent_top_up_admin", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/master/agent_top_up_admin", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    public function search() {

        $filter = new stdClass;
        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->agent_seq = parent::get_input_post("agent_seq");
        $filter->status = parent::get_input_post("status");


        try {
            $list_data = $this->M_agent_top_up_admin->get_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );
        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->seq . "/" . $data_row->agent_seq,
                    "trx_no" => parent::cdef($data_row->trx_no),
                    "trx_date" => parent::cdate($data_row->trx_date),
                    "agent_email" => parent::cdef($data_row->agent_email),
                    "deposit_amt" => parent::cnum($data_row->deposit_amt),
                    "deposit_trx_amt" => parent::cnum($data_row->deposit_trx_amt),
                    "non_deposit_trx_amt" => parent::cnum($data_row->non_deposit_trx_amt),
                    "bank_name" => parent::cdef($data_row->bank_name),
                    "bank_acct_no" => parent::cdef($data_row->bank_acct_no),
                    "bank_acct_name" => parent::cdef($data_row->bank_acct_name),
                    "refund_date" => parent::cdate($data_row->refund_date),
                    "status" => parent::cstdes($data_row->status, STATUS_WITHDRAW));
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
    }

    protected function get_edit() {
        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $key = explode("/", parent::get_input_post("key"));
        $selected->seq = $key[0];
        $selected->agent_seq = $key[1];
//        die('asdad');
        try {
            $sel_data = $this->M_agent_top_up_admin->get_data($selected);
            if (isset($sel_data)) {
                if ($sel_data[0]->status != "T")
                    $this->data[DATA_AUTH][FORM_ACTION] = ACTION_VIEW;
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_add() {

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->agent_seq = parent::get_input_post("agent_seq", true, FILL_VALIDATOR, "Email Agen", $this->data);
        $params->mutation_type = CREDIT_MUTATION_TYPE;
        $params->pg_method_seq = PAYMENT_SEQ_DEPOSIT;
        $params->trx_type = TOPUP_TRX_TYPE;
        $params->trx_no = TOPUP_STATUS_CODE . date('my') . parent::generate_random_text(3, true);
        $params->deposit_trx_amt = parent::get_input_post("deposit_trx_amt", true, NUMERIC_VALIDATOR, "Besaran Top Up", $this->data);
        $params->status = TOPUP_STATUS_CODE;
        $params->trx_date = parent::get_input_post("trx_date", true, DATE_VALIDATOR, "Tanggal Pengajuan", $this->data);

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_agent_top_up_admin->trans_begin();
                $this->M_agent_top_up_admin->save_add($params);
                $this->M_agent_top_up_admin->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_top_up_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_top_up_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_top_up_admin->trans_rollback();
            }
        }
    }

    protected function save_update() {

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->seq = parent::get_input_post("seq");
        $params->agent_seq = parent::get_input_post("agent_seq_old");
        $params->deposit_trx_amt = parent::get_input_post("deposit_trx_amt", true, NUMERIC_VALIDATOR, "Besaran Top Up", $this->data);
        $params->trx_no = parent::get_input_post("trx_no");
        $params->trx_date = parent::get_input_post("trx_date");
        $params->status = TOPUP_STATUS_CODE;
        $sel_data = $this->M_agent_top_up_admin->get_data($params);
        if (isset($sel_data)) {
            if ($sel_data[0]->status != TOPUP_STATUS_CODE) {
                $this->data[DATA_ERROR][ERROR] = true;
                $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_UPDATE;
            }
        }
        $this->data[DATA_SELECTED][LIST_DATA][] = $params;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_agent_top_up_admin->trans_begin();
                $this->M_agent_top_up_admin->save_update($params);
                $this->M_agent_top_up_admin->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_top_up_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_top_up_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_top_up_admin->trans_rollback();
            }
        }
    }

    protected function save_delete() {

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $key = explode("/", parent::get_input_post("key"));
        $params->seq = $key[0];
        $params->agent_seq = $key[1];
        $sel_data = $this->M_agent_top_up_admin->get_data($params);
        if (isset($sel_data)) {
            if ($sel_data[0]->status != TOPUP_STATUS_CODE) {
                $this->data[DATA_ERROR][ERROR] = true;
                $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_DELETE_STATUS;
            }
        }
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_agent_top_up_admin->trans_begin();
                $this->M_agent_top_up_admin->save_delete($params);
                $this->M_agent_top_up_admin->trans_commit();
                parent::set_json_success();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_top_up_admin->trans_rollback();
                parent::set_json_error($ex, ERROR_DELETE);
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_top_up_admin->trans_rollback();
                parent::set_json_error($ex, ERROR_DELETE);
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_top_up_admin->trans_rollback();
                parent::set_json_error($ex, ERROR_DELETE);
            }
        } else {
            parent::set_json_error('', ERROR_DELETE);
        }
    }

    protected function save_approve() {
        $refund_code = REFUND_STATUS_CODE . date('my') . parent::generate_random_text(3, true);
        $gencode = parent::generate_random_alphabet('2', true) . date('d') . parent::generate_random_alphabet(2, true) . date('m') . parent::generate_random_alphabet(2, true) . date('y');
        $code_mail = $gencode . $refund_code;
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $key = explode("/", parent::get_input_post("key"));
        $params->seq = $key[0];
        $params->agent_seq = $key[1];
        $sel_data = $this->M_agent_top_up_admin->get_data($params);
        if (isset($sel_data)) {
            if ($sel_data[0]->status != TOPUP_STATUS_CODE) {
                $this->data[DATA_ERROR][ERROR] = true;
                $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_APPROVED_STATUS;
            }
        }
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {                
                $this->M_agent_top_up_admin->trans_begin();
                $params->status = APPROVE_STATUS_CODE;
                $this->M_agent_top_up_admin->save_update_status($params);
                $params->deposit_trx_amt = $sel_data[0]->deposit_trx_amt;
                $this->M_agent_top_up_admin->save_approve($params);
                $this->M_agent_top_up_admin->trans_commit();                

                $params_email = new stdClass();
                $params_email->user_id = parent::get_admin_user_id();
                $params_email->ip_address = parent::get_ip_address();
                $params_email->code = AGENT_TOPUP_DEPOSIT_APPROVE;
                $params_email->to_email = $sel_data[0]->agent_email;
                $params_email->RECIPIENT_NAME = $sel_data[0]->agent_name;
                $params_email->AGENT_NAME = $sel_data[0]->agent_name;
                $params_email->TOPUP_NO = $sel_data[0]->trx_no;
                $params_email->email_code = $code_mail;
                $params_email->TOTAL_TRANSFER = parent::cnum($sel_data[0]->deposit_trx_amt);
                $params_email->LINK = base_url().'admin/master/agent_top_up';

                parent::email_template($params_email);                                        
                                
                parent::set_json_success();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_top_up_admin->trans_rollback();
                parent::set_json_error($ex, ERROR_APPROVED_STATUS);
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_top_up_admin->trans_rollback();
                parent::set_json_error($ex, ERROR_APPROVED_STATUS);
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_top_up_admin->trans_rollback();
                parent::set_json_error($ex, ERROR_APPROVED_STATUS);
            }
        } else {
            parent::set_json_error('', ERROR_APPROVED_STATUS);
        }
    }

    protected function save_reject() {

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $key = explode("/", parent::get_input_post("key"));
        $params->seq = $key[0];
        $params->agent_seq = $key[1];
        $sel_data = $this->M_agent_top_up_admin->get_data($params);

        if (isset($sel_data)) {
            if ($sel_data[0]->status != TOPUP_STATUS_CODE) {
                $this->data[DATA_ERROR][ERROR] = true;
                $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_REJECT;
            }
        }
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_agent_top_up_admin->trans_begin();
                $params->status = REJECT_STATUS_CODE;
                $this->M_agent_top_up_admin->save_update_status($params);
                $this->M_agent_top_up_admin->trans_commit();
                parent::set_json_success();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_top_up_admin->trans_rollback();
                parent::set_json_error($ex, ERROR_REJECT);
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_top_up_admin->trans_rollback();
                parent::set_json_error($ex, ERROR_REJECT);
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_top_up_admin->trans_rollback();
                parent::set_json_error($ex, ERROR_REJECT);
            }
        } else {
            parent::set_json_error('', ERROR_REJECT);
        }
    }

}

?>