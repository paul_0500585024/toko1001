<?php

require_once CONTROLLER_BASE_ADMIN;

class C_agent_admin extends controller_base_admin {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("MST09001", "admin/master/agent");
        $this->initialize();
    }

    private function initialize() {

        $this->load->model('admin/master/M_agent_admin');
//        die(print_r($this->data));
        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_APPROVE, "save_approve");
        parent::register_event($this->data, ACTION_REJECT, "save_reject");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_SAVE_DELETE, "save_delete");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {

        if (!$this->input->post()) {
            $this->data[FILTER] = 'admin/master/agent_admin_f.php';
            $this->load->view("admin/master/agent_admin", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/master/agent_admin", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/master/agent_admin", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    public function search() {

        $filter = new stdClass;
        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->email = parent::get_input_post("email");
        $filter->name = parent::get_input_post("name");
        $filter->status = parent::get_input_post("status");


        try {
            $list_data = $this->M_agent_admin->get_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );
        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                if ($data_row->status == UNVERIFIED) {
                    $chekbox = "<input id='printc' type='checkbox' name='printc[]' value='{$data_row->seq}' class='prntorder'>";
                } else {
                    $chekbox = "";
                }
                $row = array("DT_RowId" => $data_row->seq,
                    "email" => parent::cdef($data_row->email),
                    "name" => parent::cdef($data_row->name),
                    "birthday" => parent::cdate($data_row->birthday),
                    "gender" => parent::cgend($data_row->gender),
                    "mobile_phone" => parent::cdef($data_row->mobile_phone),
                    "profile_img" => $data_row->profile_img,
                    "deposit_amt" => parent::cnum($data_row->deposit_amt),
                    "status" => parent::cstdes($data_row->status, STATUS_MEMBER),
                    "checked" => $chekbox,
                    "created_by" => $data_row->created_by,
                    "created_date" => parent::cdate($data_row->created_date, 1),
                    "modified_by" => $data_row->modified_by,
                    "modified_date" => parent::cdate($data_row->modified_date, 1));
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
    }

    protected function get_edit() {

        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = parent::get_input_post("key");
        try {
            $sel_data = $this->M_agent_admin->get_data($selected);
            if (isset($sel_data)) {
                if ($sel_data[0]->status != NEW_STATUS_CODE)
                    $this->data[DATA_AUTH][FORM_ACTION] = ACTION_VIEW;
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function validateDate($date) {
        $d = DateTime::createFromFormat('Y-m-d', $date);
        return $d && $d->format('Y-m-d') == $date;
    }

    protected function save_add() {

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->email = parent::get_input_post("email", true, FILL_VALIDATOR, "Email", $this->data);
        $check_email = $this->M_agent_admin->check_user_exist($params);
        if (isset($check_email)) {
            $this->data[DATA_ERROR][ERROR] = true;
            $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_EMAIL_CHECK;
        }
        $params->password = parent::get_input_post("password", true, FILL_VALIDATOR, "Password", $this->data);
        if (strlen($params->password) < 8 || strlen($params->password) > 20) {
            $this->temp_data[DATA_ERROR][ERROR] = true;
            $this->temp_data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_VALIDATION_FILL_PASSWORD_LENGTH;
        } else {
            $params->password = md5(md5(parent::get_input_post("password")));
        }
        $params->name = parent::get_input_post("name", true, FILL_VALIDATOR, "Nama", $this->data);
        $params->birthday = parent::get_input_post("birthday");
        $params->gender = parent::get_input_post("gender", true, GENDER_VALIDATOR, "Jenis Kelamin", $this->data);
        $params->mobile_phone = parent::get_input_post("mobile_phone", true, FILL_VALIDATOR, "No Telepon", $this->data);
        $params->profile_img = "";
        if ($this->data[DATA_ERROR][ERROR] === false) {
            $logo_img = parent::upload_file("profile-image-upload", AGENT_UPLOAD_IMAGE, "a_" . strtotime("now"), IMAGE_TYPE, $this->data, IMAGE_DIMENSION_TRANSFER);
            if ($this->data[DATA_ERROR][ERROR] === false) {
                if (is_uploaded_file($_FILES["profile-image-upload"][TEMP_NAME])) {
                    $params->profile_img = $logo_img->file_name;
                } else {
                    $params->profile_img = parent::get_input_post("profile_img");
                }
            }
        }

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_agent_admin->trans_begin();
                $this->M_agent_admin->save_add($params);
                $this->M_agent_admin->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_admin->trans_rollback();
            }
        }
    }

    protected function save_update() {

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->seq = parent::get_input_post("seq");
        $params->key = parent::get_input_post("seq");
        $sel_data = $this->M_agent_admin->get_data($params);
        if (isset($sel_data)) {
            if ($sel_data[0]->status != NEW_STATUS_CODE) {
                $this->data[DATA_ERROR][ERROR] = true;
                $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_UPDATE;
            }
        }
        $params->password = parent::get_input_post("password");
        if ($params->password != "") {
            if (strlen($params->password) < 8 || strlen($params->password) > 20) {
                $this->temp_data[DATA_ERROR][ERROR] = true;
                $this->temp_data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_VALIDATION_FILL_PASSWORD_LENGTH;
            }
        }
        $params->name = parent::get_input_post("name", true, FILL_VALIDATOR, "Nama", $this->data);
        $params->birthday = parent::get_input_post("birthday");
        $params->gender = parent::get_input_post("gender", true, GENDER_VALIDATOR, "Jenis Kelamin", $this->data);
        $params->mobile_phone = parent::get_input_post("mobile_phone", true, FILL_VALIDATOR, "No Telepon", $this->data);
        $logo_img = parent::upload_file("profile-image-upload", AGENT_UPLOAD_IMAGE, $params->seq . strtotime("now"), IMAGE_TYPE, $this->data, IMAGE_DIMENSION_TRANSFER);
        $params->profile_img = $logo_img->file_name;
        $this->data[DATA_SELECTED][LIST_DATA][] = $params;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $params->agent_seq = parent::get_input_post("seq");
                $this->M_agent_admin->trans_begin();
                if ($params->password != '') {
                    $params->encrypt_new_password = md5(md5($params->password));
                    $this->M_agent_admin->save_update_settings($params);
                }
                if (is_uploaded_file($_FILES["profile-image-upload"][TEMP_NAME])) {
                    $this->M_agent_admin->save_update_profile($params);
                }
                $this->M_agent_admin->save_update($params);
                $this->M_agent_admin->trans_commit();
                if ($sel_data[0]->profile_img != "") {
                    if (file_exists(AGENT_UPLOAD_IMAGE . $sel_data[0]->profile_img))
                        unlink(AGENT_UPLOAD_IMAGE . $sel_data[0]->profile_img);
                }
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_admin->trans_rollback();
            }
        }
    }

    protected function save_delete() {

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = parent::get_input_post("key");
        $sel_data = $this->M_agent_admin->get_data($params);

        if (isset($sel_data)) {
            if ($sel_data[0]->status != NEW_STATUS_CODE) {
                $this->data[DATA_ERROR][ERROR] = true;
                $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_DELETE_STATUS;
            }
        }
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_agent_admin->trans_begin();
                $this->M_agent_admin->save_delete($params);
                $this->M_agent_admin->trans_commit();
                if ($sel_data[0]->profile_img != "") {
                    if (file_exists(AGENT_UPLOAD_IMAGE . $sel_data[0]->profile_img))
                        unlink(AGENT_UPLOAD_IMAGE . $sel_data[0]->profile_img);
                }
                parent::set_json_success();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_admin->trans_rollback();
                parent::set_json_error($ex, ERROR_DELETE);
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_admin->trans_rollback();
                parent::set_json_error($ex, ERROR_DELETE);
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_admin->trans_rollback();
                parent::set_json_error($ex, ERROR_DELETE);
            }
        } else {
            parent::set_json_error('', ERROR_DELETE);
        }
    }

    protected function save_approve() {

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = parent::get_input_post("key");
        $sel_data = $this->M_agent_admin->get_data($params);

        if (isset($sel_data)) {
            if ($sel_data[0]->status != NEW_STATUS_CODE) {
                $this->data[DATA_ERROR][ERROR] = true;
                $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_APPROVED_STATUS;
            }
        }
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_agent_admin->trans_begin();
                $params->status = APPROVE_STATUS_CODE;
                $this->M_agent_admin->save_update_status($params);
                $this->M_agent_admin->trans_commit();
                parent::set_json_success();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_admin->trans_rollback();
                parent::set_json_error($ex, ERROR_APPROVED_STATUS);
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_admin->trans_rollback();
                parent::set_json_error($ex, ERROR_APPROVED_STATUS);
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_admin->trans_rollback();
                parent::set_json_error($ex, ERROR_APPROVED_STATUS);
            }
        } else {
            parent::set_json_error('', ERROR_APPROVED_STATUS);
        }
    }

    protected function save_reject() {

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = parent::get_input_post("key");
        $sel_data = $this->M_agent_admin->get_data($params);

        if (isset($sel_data)) {
            if ($sel_data[0]->status != NEW_STATUS_CODE) {
                $this->data[DATA_ERROR][ERROR] = true;
                $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_REJECT;
            }
        }
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_agent_admin->trans_begin();
                $params->status = "S";
                $this->M_agent_admin->save_update_status($params);
                $this->M_agent_admin->trans_commit();
                parent::set_json_success();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_admin->trans_rollback();
                parent::set_json_error($ex, ERROR_REJECT);
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_admin->trans_rollback();
                parent::set_json_error($ex, ERROR_REJECT);
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_agent_admin->trans_rollback();
                parent::set_json_error($ex, ERROR_REJECT);
            }
        } else {
            parent::set_json_error('', ERROR_REJECT);
        }
    }

}

?>