<?php

require_once CONTROLLER_BASE_ADMIN;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Merchant New
 *
 * @author Jartono
 */
class C_merchant_admin extends controller_base_admin {

    private $data;

    public function __construct() {
	$this->data = parent::__construct("MST05001", "admin/master/merchant");
	$this->initialize();
    }

    private function initialize() {
	$this->load->model('admin/master/M_merchant_admin');
	$this->load->model('component/M_dropdown_list');
	$this->load->model('component/M_email');
	parent::register_event($this->data, ACTION_SEARCH, "search");
	parent::register_event($this->data, ACTION_EDIT, "get_edit");
	parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
	parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
	parent::register_event($this->data, ACTION_APPROVE, "approve");
	parent::register_event($this->data, ACTION_REJECT, "reject");
	parent::register_event($this->data, ACTION_ADDITIONAL, "get_ajax");
	if ($this->data[DATA_INIT] === true) {
	    parent::fire_event($this->data);
	}
    }

    public function index() {
	if (!$this->input->post()) {
	    $this->data[FILTER] = 'admin/master/merchant_admin_f.php';
	    $this->load->view("admin/master/merchant_admin", $this->data);
	} else {
	    if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
		$this->data[PROVINCE_NAME] = $this->M_dropdown_list->get_dropdown_province();
		$this->data[EXPEDITION_NAME] = $this->M_dropdown_list->get_dropdown_expedition();
		$this->data[CITY_NAME] = "";
		if ($this->data[DATA_ERROR][ERROR] === true) {
		    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
			$this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
			$this->load->view("admin/master/merchant_admin", $this->data);
		    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
			$this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
			$this->load->view("admin/master/merchant_admin", $this->data);
		    }
		} else {
		    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
			$this->data[DATA_SUCCESS][SUCCESS] = true;
			$this->data[DATA_AUTH][FORM_ACTION] = "";
		    }
		    $admin_info[SESSION_DATA] = $this->data;
		    $this->session->set_userdata($admin_info);
		    redirect(base_url("admin/master/merchant"));
		}
	    }
	}
    }

    protected function get_ajax() {
	$tipe = parent::get_input_post("tipe");
	$idh = parent::get_input_post("idh");
	switch ($tipe) {
	    case "city":
		$this->get_dropdown_city($idh);
		die();
		break;
	    case "district":
		$this->get_dropdown_district($idh);
		die();
		break;
	    case "valpcd":
		$valpcd = $this->M_merchant_admin->get_val_pcd($idh);
		die(json_encode($valpcd));
		break;
	    case "email":
		if ($idh != '') {
		    $msg = "OK";
		    $list_data = $this->M_merchant_admin->get_val_email($idh);
		    if (isset($list_data[0])) {
			$msg = "ERROR";
		    }
		} else {
		    $msg = "ERROR";
		}
		die($msg);
		break;
	}
    }

    protected function get_dropdown_city($province_seq) {
	try {
	    $this->data[CITY_NAME] = $this->M_dropdown_list->get_dropdown_city_by_province($province_seq);
	    die(json_encode($this->data[CITY_NAME]));
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	}
    }

    protected function get_dropdown_district($city_seq) {
	try {
	    $list_data = $this->M_dropdown_list->get_dropdown_district_by_city($city_seq);
	    die(json_encode($list_data));
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	}
    }

    public function search() {
	$filter = new stdClass;
	$filter->user_id = parent::get_admin_user_id();
	$filter->ip_address = parent::get_ip_address();
	$filter->start = parent::get_input_post("start");
	$filter->length = parent::get_input_post("length");
	$filter->order = parent::get_input_post("order");
	$filter->column = parent::get_input_post("column");
	$filter->name = parent::get_input_post("name");
	$filter->email = parent::get_input_post("email");
	$filter->status = parent::get_input_post("status");
	try {
	    $list_data = $this->M_merchant_admin->get_list($filter);
	    parent::set_list_data($this->data, $list_data);
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	}
	$output = array(
	    "sEcho" => parent::get_input_post("draw"),
	    "iTotalRecords" => $list_data[0][0]->total_rec,
	    "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
	    "aaData" => array()
	);
	if (isset($list_data[1])) {
	    foreach ($list_data[1] as $data_row) {
		$row = array("DT_RowId" => $data_row->seq,
		    "seq" => $data_row->seq,
		    "email" => parent::cdef($data_row->email),
		    "name" => parent::cdef($data_row->name),
		    "status" => parent::cstdes($data_row->status, STATUS_WNR),
		    "created_by" => $data_row->created_by,
		    "created_date" => parent::cdate($data_row->created_date, 1),
		    "modified_by" => $data_row->modified_by,
		    "modified_date" => parent::cdate($data_row->modified_date, 1),
		    "detail" => "<a href='merchant_detail/" . $data_row->seq . "'>Komisi</a>"
		);
		$output['aaData'][] = $row;
	    }
	};
	echo json_encode($output);
	die();
    }

    protected function get_edit() {
	$selected = new stdClass();
	$selected->user_id = parent::get_admin_user_id();
	$selected->ip_address = parent::get_ip_address();
	$selected->seq = parent::get_input_post("key");
	try {
	    $sel_data = $this->M_merchant_admin->get_data($selected);
	    if (isset($sel_data)) {
		parent::set_data($this->data, $sel_data);
	    } else {
		redirect(base_url("admin/master/merchant"));
	    }
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	}
    }

    protected function save_add() {
	$params = new stdClass();
	$params->user_id = parent::get_admin_user_id();
	$params->ip_address = parent::get_ip_address();
	$params->email = parent::clength(parent::get_input_post("email", true, FILL_VALIDATOR, "Email", $this->data), 50, $this->data);
	$params->name = parent::clength(parent::get_input_post("name", true, FILL_VALIDATOR, "Nama", $this->data), 100, $this->data);
	$params->address = parent::get_input_post("address", true, FILL_VALIDATOR, "Alamat", $this->data);
	$params->district_seq = parent::get_input_post("district_seq", true, FILL_VALIDATOR, "Kecamatan", $this->data);
	$params->zip_code = parent::clength(parent::get_input_post("zip_code"), 10, $this->data);
	$params->phone_no = parent::clength(parent::get_input_post("phone_no", true, FILL_VALIDATOR, "No. Telp", $this->data), 50, $this->data);
	$params->fax_no = parent::clength(parent::get_input_post("fax_no"), 50, $this->data);
	$params->pic1_name = parent::clength(parent::get_input_post("pic1_name", true, FILL_VALIDATOR, "Nama PIC", $this->data), 50, $this->data);
	$params->pic1_phone_no = parent::clength(parent::get_input_post("pic1_phone_no"), 50, $this->data);
	$params->pic2_name = parent::clength(parent::get_input_post("pic2_name"), 50, $this->data);
	$params->pic2_phone_no = parent::clength(parent::get_input_post("pic2_phone_no"), 50, $this->data);
	$params->expedition_seq = parent::get_input_post("expedition_seq", true, FILL_VALIDATOR, "Ekspedisi", $this->data);
	$params->pickup_addr_eq = parent::get_input_post("pickup_addr_eq");
	if (parent::get_input_post("pickup_addr_eq") == "on") {
	    $params->pickup_addr = parent::get_input_post("pickup_addr");
	    $params->pickup_district_seq = parent::get_input_post("pickup_district_seq");
	    $params->pickup_zip_code = parent::get_input_post("pickup_zip_code");
	} else {
	    $params->pickup_addr = parent::get_input_post("pickup_addr", true, FILL_VALIDATOR, "Alamat Pickup", $this->data);
	    $params->pickup_district_seq = parent::get_input_post("pickup_district_seq", true, FILL_VALIDATOR, "Kecamatan  Pickup", $this->data);
	    $params->pickup_zip_code = parent::clength(parent::get_input_post("pickup_zip_code"), 10, $this->data);
	}

	$params->return_addr_eq = parent::get_input_post("return_addr_eq");
	if (parent::get_input_post("return_addr_eq") == "on") {
	    $params->return_addr = parent::get_input_post("return_addr");
	    $params->return_district_seq = parent::get_input_post("return_district_seq");
	    $params->return_zip_code = parent::get_input_post("return_zip_code");
	} else {
	    $params->return_addr = parent::get_input_post("return_addr", true, FILL_VALIDATOR, "Alamat Retur", $this->data);
	    $params->return_district_seq = parent::get_input_post("return_district_seq", true, FILL_VALIDATOR, "Kecamatan Retur", $this->data);
	    $params->return_zip_code = parent::clength(parent::get_input_post("return_zip_code"), 10, $this->data);
	}
	$params->exp_fee_percent = parent::get_input_post("exp_fee_percent", true, PERCENTAGE_VALIDATOR, "Biaya Ekspedisi", $this->data);
	$params->ins_fee_percent = parent::get_input_post("ins_fee_percent", true, PERCENTAGE_VALIDATOR, "Biaya Asuransi", $this->data);
	$params->notes = parent::get_input_post("notes");
	$this->data[DATA_SELECTED][LIST_DATA][] = $params;
	if ($this->data[DATA_ERROR][ERROR] === false) {
	    try {
		$this->M_merchant_admin->trans_begin();
		$this->M_merchant_admin->save_add($params);
		$this->M_merchant_admin->trans_commit();
	    } catch (BusisnessException $ex) {
		parent::set_error($this->data, $ex);
		$this->M_merchant_admin->trans_rollback();
	    } catch (TechnicalException $ex) {
		parent::set_error($this->data, $ex);
		$this->M_merchant_admin->trans_rollback();
	    } catch (Exception $ex) {
		parent::set_error($this->data, $ex);
		$this->M_merchant_admin->trans_rollback();
	    }
	}
    }

    protected function save_update() {
	$params = new stdClass();
	$params->user_id = parent::get_admin_user_id();
	$params->ip_address = parent::get_ip_address();
	$params->seq = parent::get_input_post("seq");
	$params->email = parent::clength(parent::get_input_post("email"), 50, $this->data);
	$params->status = parent::get_input_post("status");
	$params->name = parent::clength(parent::get_input_post("name", true, FILL_VALIDATOR, "Nama", $this->data), 100, $this->data);
	$params->address = parent::get_input_post("address", true, FILL_VALIDATOR, "Alamat", $this->data);
	$params->district_seq = parent::get_input_post("district_seq", true, FILL_VALIDATOR, "Kecamatan", $this->data);
	$params->zip_code = parent::clength(parent::get_input_post("zip_code"), 10, $this->data);
	$params->phone_no = parent::clength(parent::get_input_post("phone_no", true, FILL_VALIDATOR, "No. Telp", $this->data), 50, $this->data);
	$params->fax_no = parent::clength(parent::get_input_post("fax_no"), 50, $this->data);
	$params->pic1_name = parent::clength(parent::get_input_post("pic1_name", true, FILL_VALIDATOR, "Nama PIC", $this->data), 50, $this->data);
	$params->pic1_phone_no = parent::clength(parent::get_input_post("pic1_phone_no"), 50, $this->data);
	$params->pic2_name = parent::clength(parent::get_input_post("pic2_name"), 50, $this->data);
	$params->pic2_phone_no = parent::clength(parent::get_input_post("pic2_phone_no"), 50, $this->data);
	$params->expedition_seq = parent::get_input_post("expedition_seq", true, FILL_VALIDATOR, "Ekspedisi", $this->data);
	$params->pickup_addr_eq = parent::get_input_post("pickup_addr_eq");
	if (parent::get_input_post("pickup_addr_eq") == "on") {
	    $params->pickup_addr = parent::get_input_post("pickup_addr");
	    $params->pickup_district_seq = parent::get_input_post("pickup_district_seq");
	    $params->pickup_zip_code = parent::get_input_post("pickup_zip_code");
	} else {
	    $params->pickup_addr = parent::get_input_post("pickup_addr", true, FILL_VALIDATOR, "Alamat Pickup", $this->data);
	    $params->pickup_district_seq = parent::get_input_post("pickup_district_seq", true, FILL_VALIDATOR, "Kecamatan  Pickup", $this->data);
	    $params->pickup_zip_code = parent::clength(parent::get_input_post("pickup_zip_code"), 10, $this->data);
	}

	$params->return_addr_eq = parent::get_input_post("return_addr_eq");
	if (parent::get_input_post("return_addr_eq") == "on") {
	    $params->return_addr = parent::get_input_post("return_addr");
	    $params->return_district_seq = parent::get_input_post("return_district_seq");
	    $params->return_zip_code = parent::get_input_post("return_zip_code");
	} else {
	    $params->return_addr = parent::get_input_post("return_addr", true, FILL_VALIDATOR, "Alamat Retur", $this->data);
	    $params->return_district_seq = parent::get_input_post("return_district_seq", true, FILL_VALIDATOR, "Kecamatan Retur", $this->data);
	    $params->return_zip_code = parent::clength(parent::get_input_post("return_zip_code"), 10, $this->data);
	}
	$params->exp_fee_percent = parent::get_input_post("exp_fee_percent", true, PERCENTAGE_VALIDATOR, "Biaya Ekspedisi", $this->data);
	$params->ins_fee_percent = parent::get_input_post("ins_fee_percent", true, PERCENTAGE_VALIDATOR, "Biaya Asuransi", $this->data);
	$params->notes = parent::get_input_post("notes");

	$selected = new stdClass();
	$selected->user_id = parent::get_admin_user_id();
	$selected->ip_address = parent::get_ip_address();
	$selected->seq = parent::get_input_post("seq");
	$sel_data = $this->M_merchant_admin->get_data($selected);

	if (!isset($sel_data) || ($sel_data[0]->status != NEW_STATUS_CODE && $sel_data[0]->status != WEB_STATUS_CODE )) {
	    $this->data[DATA_ERROR][ERROR] = true;
	    $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_UPDATE;
	}

	$this->data[DATA_SELECTED][LIST_DATA][] = $params;
	if ($this->data[DATA_ERROR][ERROR] === false) {
	    try {
		$this->M_merchant_admin->trans_begin();
		$this->M_merchant_admin->save_update($params);
		$this->M_merchant_admin->trans_commit();
	    } catch (BusisnessException $ex) {
		parent::set_error($this->data, $ex);
		$this->M_merchant_admin->trans_rollback();
	    } catch (TechnicalException $ex) {
		parent::set_error($this->data, $ex);
		$this->M_merchant_admin->trans_rollback();
	    } catch (Exception $ex) {
		parent::set_error($this->data, $ex);
		$this->M_merchant_admin->trans_rollback();
	    }
	}
    }

    protected function reject() {
	$selected = new stdClass();
	$selected->user_id = parent::get_admin_user_id();
	$selected->ip_address = parent::get_ip_address();
	$selected->seq = parent::get_input_post("key");
	$sel_data = $this->M_merchant_admin->get_data($selected);
	$status = "";
	$output = "";

	try {
	    if (!isset($sel_data) || ($sel_data[0]->status != NEW_STATUS_CODE && $sel_data[0]->status != WEB_STATUS_CODE ) ) {
		throw new Exception(ERROR_APPROVED);
	    }

	    $this->M_merchant_admin->trans_begin();
	    $this->M_merchant_admin->save_reject($selected);
	    $this->M_merchant_admin->trans_commit();
	    $status = "OK";
	    parent::set_json_success();
	} catch (BusisnessException $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_merchant_admin->trans_rollback();
	    parent::set_json_error($ex, ERROR_REJECT);
	} catch (TechnicalException $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_merchant_admin->trans_rollback();
	    parent::set_json_error($ex, ERROR_REJECT);
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_merchant_admin->trans_rollback();
	    parent::set_json_error($ex, ERROR_REJECT);
	}
	die();
    }

    protected function approve() {
	$selected = new stdClass();
	$selected->user_id = parent::get_admin_user_id();
	$selected->ip_address = parent::get_ip_address();
	$selected->seq = parent::get_input_post("key");
	$status = "";
	$gen_pass = "";

	try {
	    $sel_data = $this->M_merchant_admin->get_data($selected);
	    if (!isset($sel_data) || ($sel_data[0]->status != NEW_STATUS_CODE && $sel_data[0]->status != WEB_STATUS_CODE ) || $sel_data[0]->district_seq == "" || $sel_data[0]->name == "" || $sel_data[0]->address == "" || $sel_data[0]->phone_no == "" || $sel_data[0]->pic1_name == "" || $sel_data[0]->exp_fee_percent == "" || $sel_data[0]->ins_fee_percent == "") {
		throw new Exception(ERROR_APPROVED);
	    }
	    $data_detail = $this->M_merchant_admin->get_detail_trx_fee($selected);
	    if (!isset($data_detail)) {
		throw new Exception(ERROR_APPROVED);
	    }
	    $gen_pass = parent::generate_random_text(); // generate password merchant
	    $selected->email = $sel_data[0]->email;
	    $selected->name = $sel_data[0]->name;
	    $selected->address = $sel_data[0]->address;
	    $selected->district_seq = $sel_data[0]->district_seq;
	    $selected->zip_code = $sel_data[0]->zip_code;
	    $selected->phone_no = $sel_data[0]->phone_no;
	    $selected->fax_no = $sel_data[0]->fax_no;
	    $selected->pic1_name = $sel_data[0]->pic1_name;
	    $selected->pic1_phone_no = $sel_data[0]->pic1_phone_no;
	    $selected->pic2_name = $sel_data[0]->pic2_name;
	    $selected->pic2_phone_no = $sel_data[0]->pic2_phone_no;
	    $selected->expedition_seq = $sel_data[0]->expedition_seq;
	    $selected->pickup_addr_eq = $sel_data[0]->pickup_addr_eq;
	    $selected->pickup_addr = $sel_data[0]->pickup_addr;
	    $selected->pickup_district_seq = $sel_data[0]->pickup_district_seq;
	    $selected->pickup_zip_code = $sel_data[0]->pickup_zip_code;
	    $selected->return_addr_eq = $sel_data[0]->return_addr_eq;
	    $selected->return_addr = $sel_data[0]->return_addr;
	    $selected->return_district_seq = $sel_data[0]->return_district_seq;
	    $selected->return_zip_code = $sel_data[0]->return_zip_code;
	    $selected->exp_fee_percent = $sel_data[0]->exp_fee_percent;
	    $selected->ins_fee_percent = $sel_data[0]->ins_fee_percent;
	    $selected->notes = $sel_data[0]->notes;
	    $selected->password = $gen_pass;
	    $selected->pCode = parent::generate_random_alnum(10); // generate code merchant
	    $selected->email_code = parent::generate_random_text(20);
	    $this->M_merchant_admin->trans_begin();
	    $seq_merchant_new = $this->M_merchant_admin->save_copy($selected);
	    if (!isset($seq_merchant_new)) {
		parent::set_json_error($ex, ERROR_APPROVED);
	    } else {
		$selected->seq_merchant = $seq_merchant_new[0]->new_seq;
		$this->M_merchant_admin->save_add_info($selected);
		$this->M_merchant_admin->save_add_log_sec($selected);
		$this->M_merchant_admin->save_copy_detail($selected);
		$this->M_merchant_admin->save_delete_detail($selected);
		$this->M_merchant_admin->save_delete($selected);
		$params = new stdClass;
		$params->user_id = parent::get_admin_user_id();
		$params->ip_address = parent::get_ip_address();
		$params->code = "MERCHANT_REG_SUCCESS";
		$params->to_email = $selected->email;
		$params->email_code = $selected->email_code;
		$params->USERNAME = $selected->email;
		$params->RECIPIENT_NAME = $selected->name;
		$params->PASSWORD = $selected->password;
		$params->WEBSITE_URL = base_url();
		parent::email_template($params);
		$this->M_merchant_admin->trans_commit();
		parent::set_json_success();
	    }
	} catch (BusisnessException $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_merchant_admin->trans_rollback();
	    parent::set_json_error($ex, ERROR_APPROVED);
	} catch (TechnicalException $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_merchant_admin->trans_rollback();
	    parent::set_json_error($ex, ERROR_APPROVED);
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_merchant_admin->trans_rollback();
	    parent::set_json_error($ex, ERROR_APPROVED);
	}
	die();
    }

}

?>