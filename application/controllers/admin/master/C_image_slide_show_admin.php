<?php

require_once CONTROLLER_BASE_ADMIN;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of c_province
 *
 * @author Jartono
 */
class C_image_slide_show_admin extends controller_base_admin {

    private $data;

    public function __construct() {

        $this->data = parent::__construct("CON03001", "admin/master/image_slide_show");
        $this->initialize();
    }

    private function initialize() {

        $this->load->model('admin/master/M_image_slide_show_admin');
        $this->load->model('component/M_dropdown_list');

        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_SAVE_DELETE, "save_delete");
        parent::register_event($this->data, ACTION_APPROVE, "approve");
        parent::register_event($this->data, ACTION_REJECT, "reject");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        if (!$this->input->post()) {
            $this->data[FILTER] = 'admin/master/image_slide_show_admin_f.php';
            $this->load->view("admin/master/image_slide_show_admin", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/master/image_slide_show_admin", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/master/image_slide_show_admin", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    public function search() {

        $filter = new stdClass;
        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->status = parent::get_input_post("status");
        $filter->order_slide_show = parent::get_input_post("order_slide_show");
        $filter->active = parent::get_input_post("active");

        try {
            $list_data = $this->M_image_slide_show_admin->get_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );

        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->seq,
                    "seq" => $data_row->seq,
                    "status" => parent::cstdes($data_row->status, STATUS_ANR),
                    "order" => parent::cnum($data_row->order),
                    "active" => parent::cstat($data_row->active),
                    "created_by" => $data_row->created_by,
                    "created_date" => parent::cdate($data_row->created_date, 1),
                    "modified_by" => $data_row->modified_by,
                    "modified_date" => parent::cdate($data_row->modified_date, 1));
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
    }

    protected function get_edit() {

        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = parent::get_input_post("key");

        try {
            $sel_data = $this->M_image_slide_show_admin->get_data($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function approve() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = parent::get_input_post("key");
        $params->status = APPROVE_STATUS_CODE;

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $status = $this->M_image_slide_show_admin->check_status_image_slide_show_by_seq($params);

                if ($status[0]->status == NEW_STATUS_CODE) {
                    $this->M_image_slide_show_admin->trans_begin();
                    $this->M_image_slide_show_admin->save_update_status($params);
                    $this->M_image_slide_show_admin->trans_commit();
                    parent::set_json_success();
                } else {
                    throw new BusisnessException(ERROR_APPROVED);
                }
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_image_slide_show_admin->trans_rollback();
                parent::set_json_error($ex, ERROR_APPROVED);
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_image_slide_show_admin->trans_rollback();
                parent::set_json_error($ex, ERROR_APPROVED);
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_image_slide_show_admin->trans_rollback();
                parent::set_json_error($ex, ERROR_APPROVED);
            }
        }
    }

    protected function reject() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = parent::get_input_post("key");
        $params->status = REJECT_STATUS_CODE;

        try {
            $status = $this->M_image_slide_show_admin->check_status_image_slide_show_by_seq($params);
            if ($status[0]->status == NEW_STATUS_CODE) {
                $this->M_image_slide_show_admin->trans_begin();
                $this->M_image_slide_show_admin->save_update_status($params);
                $this->M_image_slide_show_admin->trans_commit();
                parent::set_json_success();
            } else {
                throw new BusisnessException(ERROR_REJECT);
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_image_slide_show_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_REJECT);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_image_slide_show_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_REJECT);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_image_slide_show_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_REJECT);
        }
    }

    private function save_image_tmp_to_slide($img_name) {
        $ext = '.' . pathinfo($img_name, PATHINFO_EXTENSION);
        $img_name = basename($img_name, $ext);
        $string = preg_replace("/[^A-Za-z0-9 ]/", '', $img_name);
        $img_name = str_ireplace(" ", "_", $string);

        //copy to tmp slide 
        $namafolder = str_replace(CDN_IMAGE,"",SLIDE_UPLOAD_IMAGE);

        $img = parent::upload_file("img", $namafolder, $img_name, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_SLIDE_SHOW, "Slide Show ");
        if ($this->data[DATA_ERROR][ERROR] === false) {
            $oldfile = $img->file_name;
            $ext = '.' . pathinfo($oldfile, PATHINFO_EXTENSION);
            $namafile = $oldfile;
            parent::set_all_image_banner($namafolder . $namafile, $namafolder, $namafile, $this->data);
        }
        return $img;
    }

    protected function save_add() {

        $max_seq = $this->M_image_slide_show_admin->get_max_seq();
        $img_name = parent::get_file_name("img");

        $img = $this->save_image_tmp_to_slide($img_name);

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->img = $img->file_name;
        $params->img_url = parent::get_input_post("img_url", true, FILL_VALIDATOR, "Url Link", $this->data);
        $params->order = parent::get_input_post("order", true, NUMERIC_VALIDATOR, "Order", $this->data);
        $params->active = parent::get_input_post("active");
        $params->status = NEW_STATUS_CODE;


        $this->data[DATA_SELECTED][LIST_DATA][] = $params;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_image_slide_show_admin->trans_begin();
                $this->M_image_slide_show_admin->save_add($params);
                $this->M_image_slide_show_admin->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_image_slide_show_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_image_slide_show_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_image_slide_show_admin->trans_rollback();
            }
        }else{
            $this->data[DATA_SELECTED][LIST_DATA] = NULL;
        }
    }

    protected function save_update() {

        $img_name = parent::get_file_name("img");

//        $string = preg_replace("/[^A-Za-z0-9 ]/", '', $img_name);
//        $img_name = str_ireplace(" ", "_", $string);
//        $img = parent::upload_file("img", SLIDE_UPLOAD_IMAGE, parent::get_input_post("seq") . '-' . $img_name, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_SLIDE_SHOW, "Slide Show ");

        $sequence = parent::get_input_post("seq");
        $img = $this->save_image_tmp_to_slide($sequence, $img_name);

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->seq = parent::get_input_post("seq");
        $params->key = parent::get_input_post("seq");
        $params->old_img = parent::get_input_post("old_img");
        $params->img = $img->error == true ? $params->old_img : $img->file_name;
        $params->img_url = parent::get_input_post("img_url", true, FILL_VALIDATOR, "Url Link", $this->data);
        $params->order = parent::get_input_post("order", true, NUMERIC_VALIDATOR, "Order", $this->data);
        $params->active = parent::get_input_post("active");

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $status = $this->M_image_slide_show_admin->check_status_image_slide_show_by_seq($params);

                if ($status[0]->status == NEW_STATUS_CODE) {
                    $this->M_image_slide_show_admin->trans_begin();
                    $this->M_image_slide_show_admin->save_update($params);
                    $this->M_image_slide_show_admin->trans_commit();
                } else {
                    throw new BusisnessException(ERROR_UPDATE);
                }
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_image_slide_show_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_image_slide_show_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_image_slide_show_admin->trans_rollback();
            }
        } else {
            $this->get_edit_key($params);
        }
    }

    private function get_edit_key($params) {
        try {
            $sel_data = $this->M_image_slide_show_admin->get_data($params);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_delete() {

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = parent::get_input_post("key");
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $status = $this->M_image_slide_show_admin->check_status_image_slide_show_by_seq($params);

                if ($status[0]->status == APPROVE_STATUS_CODE) {
                    $this->M_image_slide_show_admin->trans_begin();
                    $this->M_image_slide_show_admin->save_delete($params);
                    $this->M_image_slide_show_admin->trans_commit();
                    parent::set_json_success();
                } else {
                    throw new BusisnessException(ERROR_DELETE_SLIDE_SHOW);
                }
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_image_slide_show_admin->trans_rollback();
                parent::set_json_error($ex, "");
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_image_slide_show_admin->trans_rollback();
                parent::set_json_error($ex, ERROR_DELETE);
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_image_slide_show_admin->trans_rollback();
                parent::set_json_error($ex, ERROR_DELETE);
            }
        }
    }

}

?>
