<?php

require_once CONTROLLER_BASE_ADMIN;

class C_agent_refund_detail extends controller_base_admin {

    private $data;
    private $hseq;

    public function __construct() {
        $this->data = parent::__construct("MST09004", "admin/master/agent/refund_detail/");
        $this->initialize();
    }

    private function initialize() {
        $this->trx_no = $this->uri->segment(5);
        $this->load->model('admin/master/M_agent_refund_admin');
        $this->load->model('admin/transaction/M_member_refund_admin');
        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        if (!$this->input->post()) {
            $this->get_data_header();
            $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
            $this->load->view("admin/master/agent_refund_admin_detail", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/master/agent_refund_admin_detail", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/master/agent_refund_admin_detail", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                }
            }
        }
    }

    private function get_data_header() {
        $filter = new stdClass;
        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->trx_no = $this->trx_no;
        $get_data = $this->M_agent_refund_admin->get_data_refund($filter);
        if ($get_data[0]->trx_type == RETUR_ACCOUNT_TYPE) {
            $filter->order_seq = '';
            $filter->merchant_info_seq = '';
            $filter->type = RETUR_ACCOUNT_TYPE;
            $params1 = new stdClass();
            $params1->nominal = 0;
            $params2 = new stdClass();
            $params2->deposit_trx_amt = $get_data[0]->deposit_trx_amt;
            
            $sel_data[1][0] = $params1;
            $sel_data[2] = $this->M_member_refund_admin->get_return_product_detail($filter);
            $sel_data[3][0] = $params2;   //get nilai batal  
        } else {
            $filter->cancel_type = CANCEL_ACCOUNT_TYPE;
            $get_data_cnl = $this->M_agent_refund_admin->get_data_refund_cancel($filter);
            $filter->order_seq = $get_data_cnl[0]->order_seq;
            $filter->merchant_info_seq = $get_data_cnl[0]->merchant_info_seq;
            $filter->return_no = '';
            $filter->type = '';
            $sel_data[1] = $this->M_agent_refund_admin->get_voucher_nominal($filter);
            $sel_data[2] = $this->M_member_refund_admin->get_list_product_detail($filter);
            $sel_data[3] = $this->M_agent_refund_admin->get_agent_account_info($filter);
        }
        $filter->product_status = PRODUCT_CANCEL_BY_MERCHANT_STATUS_CODE;
        try {
            $sel_data[0] = $this->M_agent_refund_admin->st($filter);
            if (isset($sel_data)) {
                parent::set_data_header($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

}

?>