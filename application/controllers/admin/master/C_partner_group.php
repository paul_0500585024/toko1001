<?php

require_once CONTROLLER_BASE_ADMIN;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of C_partner_group
 *
 * @author Jaka
 */
class C_partner_group extends controller_base_admin {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("MST70009", "admin/master/partner_group");
        $this->initialize();
    }

    protected function initialize() {
        $this->load->model('admin/master/M_partner_group');
        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_SAVE_DELETE, "save_delete");
        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        if (!$this->input->post()) {
            $this->data[FILTER] = 'admin/master/partner_group_f.php';
            $this->load->view("admin/master/partner_group", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/master/partner_group", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/master/partner_group", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    public function search() {
        $filter = new stdClass;
        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->name = parent::get_input_post("partner_group_name");
        try {
            $list_data = $this->M_partner_group->get_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );
        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->partner_group_seq,
                    "partner_group_name" => $data_row->partner_group_name,
                    "detail" => "<a href='" . base_url() . "admin/master/partner_group_detail/" . $data_row->partner_group_seq . "'>Detail</a>",
                    "created_by" => $data_row->created_by,
                    "created_date" => parent::cdate($data_row->created_date, 1),
                    "modified_by" => $data_row->modified_by,
                    "modified_date" => parent::cdate($data_row->modified_date, 1));
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
    }

    protected function get_edit() {
        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = parent::get_input_post("key");
        try {
            $sel_data = $this->M_partner_group->get_data($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_add() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->partner_group_name = parent::get_input_post("partner_group_name", true, FILL_VALIDATOR, "Nama Grup", $this->data);
        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_partner_group->trans_begin();
                $this->M_partner_group->save_add($params);
                $this->M_partner_group->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_partner_group->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_partner_group->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_partner_group->trans_rollback();
            }
        }
    }

    protected function save_update() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->partner_group_seq = parent::get_input_post("partner_group_seq");
        $params->partner_group_name = parent::get_input_post("partner_group_name", true, FILL_VALIDATOR, "Nama Grup", $this->data);
        $this->data[DATA_SELECTED][LIST_DATA][] = $params;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_partner_group->trans_begin();
                $this->M_partner_group->save_update($params);
                $this->M_partner_group->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_partner_group->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_partner_group->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_partner_group->trans_rollback();
            }
        }
    }

    protected function save_delete() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = parent::get_input_post("key");
        try {
            $this->M_partner_group->trans_begin();
            $this->M_partner_group->save_delete($params);
            $this->M_partner_group->trans_commit();
            parent::set_json_success();
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_partner_group->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_partner_group->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_partner_group->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        }
    }

}
