<?php

require_once CONTROLLER_BASE_ADMIN;

class C_expedition_admin_awb extends controller_base_admin {

    private $data;
    private $hseq;

    public function __construct() {
        $this->data = parent::__construct("MST03003", "admin/master/expedition_awb");
        $this->initialize();
    }

    private function initialize() {

        $this->hseq = $this->uri->segment(4);
        $this->load->model('component/M_dropdown_list');
        $this->load->model('admin/master/M_expedition_admin');
        $this->load->helper('date');

        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_SAVE_DELETE, "save_delete");
        parent::register_event($this->data, ACTION_ADDITIONAL, "save_upload");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        $this->data[MERCHANT_LIST] = $this->M_dropdown_list->get_dropdown_merchant();

        if (!$this->input->post()) {
            if ($this->data[DATA_AUTH][FORM_ACTION] == "") {
                $this->get_data_header();
            }
            $key = explode("/", $this->data[DATA_AUTH][FORM_URL]);
            If (!isset($key[3])) {
                $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
            }
            $this->data[FILTER] = 'admin/master/expedition_admin_awb_f.php';
            $this->load->view("admin/master/expedition_admin_awb", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/master/expedition_admin_awb", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/master/expedition_admin_awb", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_ADDITIONAL) {
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                        $admin_info[SESSION_DATA] = $this->data;
                        $this->session->set_userdata($admin_info);
                        redirect(base_url($this->data[DATA_AUTH][FORM_URL]) . "/" . $this->hseq);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_ADDITIONAL) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]) . "/" . $this->hseq);
                }
            }
        }
    }

    public function search() {
        $filter = new stdClass;
        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->hseq = $this->hseq;
        $filter->merchant_info_seq = parent::get_input_post("merchant_info_seq");
        $filter->awb_no = parent::get_input_post("awb_no");
        $filter->trx_no = parent::get_input_post("trx_no");

        try {
            $list_data = $this->M_expedition_admin->get_awb_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );
        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->seq,
                    "awb_no" => parent::cdef($data_row->awb_no),
                    "trx_no" => parent::cdef($data_row->trx_no),
                    "ship_date" => parent::cdate($data_row->ship_date, 1),
                    "name" => parent::cdef($data_row->name),
                    "created_by" => $data_row->created_by,
                    "created_date" => parent::cdate($data_row->created_date, 1),
                    "modified_by" => $data_row->modified_by,
                    "modified_date" => parent::cdate($data_row->modified_date, 1));
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
    }

    protected function get_data_header() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = $this->hseq;

        try {
            $sel_data = $this->M_expedition_admin->get_data($params);
            parent::set_data_header($this->data, $sel_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_upload() {

        try {
            $file = parent::upload_file(UPLOADID, UPLOADPATH, FILENAME, CSV_FILE, $this->data);

            if ($file->error === false) {
                $this->load->library(CSV);
                $result = $this->csv_reader->parse_file($file->url_file . ".csv");

                foreach ($result as $field) {
                    $params = new stdClass();
                    $params->user_id = parent::get_admin_user_id();
                    $params->ip_address = parent::get_ip_address();
                    $params->exp_seq = $this->hseq;
                    $params->awb_no = $field['awb_no'];

                    $this->data[DATA_SELECTED][LIST_DATA][] = $params;

                    if ($this->data[DATA_ERROR][ERROR] === false) {
                        $this->M_expedition_admin->trans_begin();
                        $this->M_expedition_admin->save_upload_awb($params);

                        parent::Unlink_file($file->url_file);
                        $this->M_expedition_admin->trans_commit();
                    }
                }
            } else {
                throw new Exception(ERROR_UPLOAD);
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_expedition_admin->trans_rollback();
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_expedition_admin->trans_rollback();
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_expedition_admin->trans_rollback();
        }
    }

    protected function save_delete() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->seq = parent::get_input_post("key");

        try {
            $this->M_expedition_admin->trans_begin();
            $order_no = $this->M_expedition_admin->save_delete_awb($params);
            $this->M_expedition_admin->trans_commit();

            if ($order_no[0]->trx_no == "") {
                parent::set_json_success();
            } else {                
                throw new Exception(ERROR_DELETE);
            }
            
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_expedition_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_expedition_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_expedition_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        }
    }

}

?>
