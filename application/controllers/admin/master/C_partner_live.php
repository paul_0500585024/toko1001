<?php

require_once CONTROLLER_BASE_ADMIN;

class C_partner_live extends controller_base_admin {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("MST10001", "admin/master/partner_live");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('admin/master/M_partner_live');
        $this->load->model('component/M_dropdown_list');
        $this->load->model('partner/profile/M_change_password_partner');
        $this->load->library('slim');


        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_SAVE_DELETE, "save_delete");
        parent::register_event($this->data, ACTION_ADDITIONAL, "generate_password_partner");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        $this->data[PARTNER_LIST] = $this->M_dropdown_list->get_dropdown_partner();

        if (!$this->input->post()) {
            $this->data[FILTER] = 'admin/master/partner_live_f.php';
            $this->load->view("admin/master/partner_live", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/master/partner_live", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/master/partner_live", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_ADDITIONAL) {
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                        $admin_info[SESSION_DATA] = $this->data;
                        $this->session->set_userdata($admin_info);
                        redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_ADDITIONAL) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    public function search() {
        $filter = new stdClass;
        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->code = parent::get_input_post("code");
        $filter->partner_name = parent::get_input_post("partner_name");
        $filter->partner_email = parent::get_input_post("email");
        $filter->admin_fee = parent::get_input_post("admin_fee");

        try {
            $list_data = $this->M_partner_live->get_list($filter);
            if (isset($list_data[1])) {
                foreach ($list_data[1] as $key => $each_list_data) {
                    $list_data[1][$key]->admin_fee = number_format($each_list_data->admin_fee);
                }
            }
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );

        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->seq,
                    "email" => parent::cdef($data_row->email),
                    "name" => parent::cdef($data_row->name),
                    "admin_fee" => parent::cdef($data_row->admin_fee),
                    "created_by" => parent::cdef($data_row->created_by),
                    "created_date" => parent::cdate($data_row->created_date, 1),
                    "modified_by" => parent::cdef($data_row->modified_by),
                    "modified_date" => parent::cdate($data_row->modified_date, 1),
                    "generate_password" => "<button onclick='generate_password_partner(" . $data_row->seq . ")' class='generate-" . $data_row->seq . " btn btn-warning btn-flat btn-xs btn-block'>Buat Password Baru</button>");
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
    }

    protected function get_edit() {
        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->seq = parent::get_input_post("key");

        try {
            $sel_data = $this->M_partner_live->get_data($selected);
            if (isset($sel_data)) {
                $new_sel_data = array();
                $counter = 0;
                foreach ($sel_data as $each_sel_data) {
                    $new_sel_data[$counter] = new stdClass();
                    $new_sel_data[$counter]->seq = $each_sel_data->seq;
                    $new_sel_data[$counter]->email = $each_sel_data->email;
                    $new_sel_data[$counter]->name = $each_sel_data->name;
                    $new_sel_data[$counter]->password = $each_sel_data->password;
                    $new_sel_data[$counter]->mobile_phone = $each_sel_data->mobile_phone;
                    $new_sel_data[$counter]->admin_fee = $each_sel_data->admin_fee;
                    $new_sel_data[$counter]->profile_img = $each_sel_data->profile_img;
                    $new_sel_data[$counter]->status = parent::cstdes($each_sel_data->status, STATUS_MEMBER);
                    $new_sel_data[$counter]->bank_name = $each_sel_data->bank_name;
                    $new_sel_data[$counter]->bank_branch_name = $each_sel_data->bank_branch_name;
                    $new_sel_data[$counter]->bank_acct_no = $each_sel_data->bank_acct_no;
                    $new_sel_data[$counter]->bank_acct_name = $each_sel_data->bank_acct_name;
                    $new_sel_data[$counter]->last_login = $each_sel_data->last_login;
                    $new_sel_data[$counter]->ip_address = $each_sel_data->ip_address;
                    $new_sel_data[$counter]->created_by = $each_sel_data->created_by;
                    $new_sel_data[$counter]->created_date = $each_sel_data->created_date;
                    $new_sel_data[$counter]->modified_by = $each_sel_data->modified_by;
                    $new_sel_data[$counter]->modified_date = $each_sel_data->modified_date;
                    $counter++;
                };
                parent::set_data($this->data, $new_sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_update() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->seq = parent::get_input_post("seq");
        $params->email = parent::get_input_post("email", true, FILL_VALIDATOR, "Email", $this->data);
        $params->name = parent::get_input_post("name", true, FILL_VALIDATOR, "Nama", $this->data);
        $params->mobile_phone = parent::get_input_post("mobile_phone", true, FILL_VALIDATOR, "No. Telp", $this->data);
        $params->admin_fee = parent::get_input_post("admin_fee", true, FILL_VALIDATOR, "Admin Fee", $this->data);
        $params->status = parent::get_input_post("status", true, FILL_VALIDATOR, "Status", $this->data);
        $params->bank_name = parent::get_input_post("bank_name", true, FILL_VALIDATOR, "Nama Bank", $this->data);
        $params->bank_branch_name = parent::get_input_post("bank_branch_name", true, FILL_VALIDATOR, "Cabang", $this->data);
        $params->bank_acct_no = parent::get_input_post("bank_acct_no", true, FILL_VALIDATOR, "No. Rekening", $this->data);
        $params->bank_acct_name = parent::get_input_post("bank_acct_name", true, FILL_VALIDATOR, "Rekening A/N", $this->data);
        /* data picture */
        $params->image = "";
        $params->image_name = "image";
        $params->file_type = IMAGE_TYPE;
        $params->dimension = IMAGE_DIMENSION_LOGO;
        $params->file_name = "c_" . strtotime("now");
        $params->folder = PROFILE_IMAGE_PARTNER . '/' . $params->seq . '/';
        $file_name = $this->upload_file_image($params);
        $params->profile_img = $file_name;
        $this->data[DATA_SELECTED][LIST_DATA][] = $params;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_partner_live->trans_begin();
                $this->delete_file_image($params->seq);
                $this->M_partner_live->save_update($params);
                $this->M_partner_live->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_partner_live->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_partner_live->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_partner_live->trans_rollback();
            }
        }
    }

    protected function save_add() {

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();

        $params->email = parent::get_input_post("email", true, FILL_VALIDATOR, "Email", $this->data);
        $check_email = $this->M_partner_live->check_user_exist($params);
        if (isset($check_email)) {
            $this->data[DATA_ERROR][ERROR] = true;
            $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_EMAIL_CHECK;
        }
        $params->password = parent::get_input_post("password", true, FILL_VALIDATOR, "Password", $this->data);
        if (strlen($params->password) < 8 || strlen($params->password) > 20) {
            $this->data[DATA_ERROR][ERROR] = true;
            $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_VALIDATION_FILL_PASSWORD_LENGTH;
        } else {
            $params->password = md5(md5(parent::get_input_post("password")));
        }
        $params->name = parent::get_input_post("name", true, FILL_VALIDATOR, "Nama", $this->data);
        $params->mobile_phone = parent::get_input_post("mobile_phone", true, FILL_VALIDATOR, "No. Telp", $this->data);
        $params->admin_fee = parent::get_input_post("admin_fee", true, FILL_VALIDATOR, "Admin Fee", $this->data);
        $params->status = 'A';
        $params->bank_name = parent::get_input_post("bank_name", true, FILL_VALIDATOR, "Nama Bank", $this->data);
        $params->bank_branch_name = parent::get_input_post("bank_branch_name", true, FILL_VALIDATOR, "Cabang", $this->data);
        $params->bank_acct_no = parent::get_input_post("bank_acct_no", true, FILL_VALIDATOR, "No. Rekening", $this->data);
        $params->bank_acct_name = parent::get_input_post("bank_acct_name", true, FILL_VALIDATOR, "Rekening A/N", $this->data);
        $params->profile_img = '';
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_partner_live->trans_begin();
                $partnerseq = $this->M_partner_live->save_add($params);
                $params->partner_seq = $partnerseq[0]->new_seq;
                $params->seq = $partnerseq[0]->new_seq;
                $this->M_partner_live->trans_commit();

                /* data picture */
                $params->image = "";
                $params->image_name = "image";
                $params->file_type = IMAGE_TYPE;
                $params->dimension = IMAGE_DIMENSION_LOGO;
                $params->file_name = "c_" . strtotime("now");
                $params->folder = PROFILE_IMAGE_PARTNER . '/' . $params->partner_seq . '/';
                $file_name = $this->upload_file_image($params);
                $params->profile_img = $file_name;
                $this->M_partner_live->trans_begin();
                $this->M_partner_live->save_update($params);
                $this->M_partner_live->trans_commit();
                $this->data[DATA_SELECTED][LIST_DATA][] = $params;
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_partner_live->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_partner_live->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_partner_live->trans_rollback();
            }
        }
    }

    protected function save_delete() {

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->seq = parent::get_input_post("key");
        $sel_data = $this->M_partner_live->get_data($params);

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_partner_live->trans_begin();
                $this->M_partner_live->save_delete($params);
                $this->M_partner_live->trans_commit();
                if ($sel_data[0]->profile_img != "") {
                    if (file_exists(PROFILE_IMAGE_PARTNER . '/' . $params->seq . '/' . $sel_data[0]->profile_img))
                        unlink(PROFILE_IMAGE_PARTNER . '/' . $params->seq . '/' . $sel_data[0]->profile_img);
                }
                parent::set_json_success();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_partner_live->trans_rollback();
                parent::set_json_error($ex, ERROR_DELETE);
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_partner_live->trans_rollback();
                parent::set_json_error($ex, ERROR_DELETE);
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_partner_live->trans_rollback();
                parent::set_json_error($ex, ERROR_DELETE);
            }
        } else {
            parent::set_json_error('', ERROR_DELETE);
        }
    }

    private function upload_file_image($params_upload_file) {
        $retval = "";
        $image = $this->slim->getImages($params_upload_file->image_name);
        if (!empty($image[0])) {
            $ext = str_replace('image/', '', $image[0]["input"]["type"]);

            if (isset($params_upload_file->file_type)) {
                if ($params_upload_file->file_type != '') {
                    if (!in_array($ext, explode('|', $params_upload_file->file_type))) {
                        $this->data[DATA_ERROR][ERROR] = TRUE;
                        $this->data[DATA_ERROR][ERROR_MESSAGE][] = "File Types tidak tepat";
                    }
                    ;
                }
            }
            if (isset($params_upload_file->dimension)) {
                if ($params_upload_file->dimension != '') {
                    $dimension = (object) unserialize($params_upload_file->dimension);
                    $max_height = $dimension->max_height;
                    $max_width = $dimension->max_width;
                    $min_width = $dimension->min_width;
                    $min_height = $dimension->min_height;
                    if ($image[0]['output']['width'] < $min_width || $image[0]['output']['width'] > $max_width || $image[0]['output']['height'] < $min_height || $image[0]['output']['height'] > $max_height) {
                        $this->data[DATA_ERROR][ERROR] = TRUE;
                        $this->data[DATA_ERROR][ERROR_MESSAGE][] = "File Dimension tidak tepat";
                    }
                }
            }
            $this->slim->saveFile($image[0]['output']['data'], $params_upload_file->file_name . '.' . $ext, $params_upload_file->folder, false);
            $retval = $params_upload_file->file_name . '.' . $ext;
        } else {
            $retval = "";
        }
        return $retval;
    }

    private function delete_file_image($partner_seq) {
        $this->get_edit();
        $sel_data = $this->data[DATA_SELECTED][LIST_DATA];
        if ($sel_data[0]->profile_img != '') {
            if (file_exists(PROFILE_IMAGE_PARTNER . '/' . $partner_seq . '/' . $sel_data[0]->profile_img)) {
                unlink(PROFILE_IMAGE_PARTNER . '/' . $partner_seq . '/' . $sel_data[0]->profile_img);
            }
        }
    }

    protected function generate_password_partner() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->partner_seq = parent::get_input_post("partner_seq");
        $params->seq = parent::get_input_post("partner_seq");
        $params->random_password = parent::generate_random_text(8);
        $params->new_password = md5(md5($params->random_password));

        $partner_data = $this->M_partner_live->get_data($params);

        if ($params->random_password !== "" && $params->partner_seq != "") {
            try {

                $this->M_change_password_partner->trans_begin();
                $this->M_change_password_partner->save_update($params);
                $this->M_change_password_partner->trans_commit();
                $email = new stdClass();
                $email->code = GENERATE_PASSWORD_PARTNER;
                $email->ip_address = $params->ip_address;
                $email->user_id = $params->user_id;
                $email->RECIPIENT_NAME = $partner_data[0]->name;
                $email->to_email = $partner_data[0]->email;
                $email->password = $params->random_password;
                $email->email = $partner_data[0]->email;
                parent::email_template($email);

                parent::set_json_success();
            } catch (Exception $ex) {
                parent::set_json_error($ex);
            }
        }
    }

}

?>
