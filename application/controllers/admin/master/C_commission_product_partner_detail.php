<?php

require_once CONTROLLER_BASE_ADMIN;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of C_commission_product_partner_detail
 *
 * @author Jaka
 */
class C_commission_product_partner_detail extends controller_base_admin {

    private $data;
    private $hseq;

    public function __construct() {
        $this->data = parent::__construct('MST71002', 'admin/master/commission_product_partner_detail');
        $this->initialize();
    }

    private function initialize() {
        $this->hseq = $this->uri->segment(4);
        $this->load->model('admin/master/M_commission_product_partner_detail');
        $this->data['data_dropdown_group_partner'] = $this->M_commission_product_partner_detail->get_dropdown_group_partner();
        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_ADDITIONAL, "aditional");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {

        if (!isset($this->hseq))
            redirect(base_url('admin/master/commission_product_partner'));

        if (!$this->input->post()) {
            if ($this->data[DATA_AUTH][FORM_ACTION] == "") {
                $this->get_data_header();
            }
            $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
            $this->load->view("admin/master/commission_product_partner_detail", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/master/commission_product_partner_detail", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/master/commission_product_partner_detail", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_URL] = 'admin/master/commission_product_partner';
                        $this->data[DATA_AUTH][FORM_ACTION] = '';
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);

                    redirect(base_url('admin/master/commission_product_partner'));
                }
            }
        }
    }

    private function get_data_header() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->seq = $this->hseq;
        $sel_data = $this->M_commission_product_partner_detail->get_list($params);
        parent::set_data_header($this->data, $sel_data);
    }

    protected function aditional() {
        $type = parent::get_input_post('type');
        switch ($type) {
            case 'data_dropdown_product':
                $this->get_dropdown_product();
                break;
            default:
                break;
        }
    }

    private function get_dropdown_product() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->product_name = parent::get_input_post('product_name');
        try {
            $sel_data = $this->M_commission_product_partner_detail->get_dropdown_product_by_name($params);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        echo '{ "results": ' . json_encode($sel_data) . ' }';
        die();
    }

    protected function save_add() {
        $params = new stdClass;
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();

        $params->partner_product_seq = parent::get_input_post('partner_product_seq', TRUE, FILL_VALIDATOR, 'Partner Produk', $this->data);
        $params->product_seq = $this->input->post('product_seq');
        $params->partner_group_seq = $this->input->post('partner_group_seq');
        $params->nominal_commission_partner = $this->input->post('nominal_commission_partner');
        $params->nominal_commission_agent = $this->input->post('nominal_commission_agent');


        $count_product = count($params->product_seq);

        (array) $arayPreviousData = [];

        if (!isset($params->partner_product_seq) || $params->partner_product_seq == '') {
            $this->data[DATA_ERROR][ERROR] = TRUE;
            $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_PARTNER_PRODUCT_SEQ_NOT_EXIST;
        }


        for ($i = 0; $i < $count_product; $i++) {
            (array) $key = [];
            (array) $key_2 = [];
            $key = explode('$#', $params->product_seq[$i]);
            $key_2 = explode('$#', $params->partner_group_seq[$i]);

            $previousData = new stdClass;
            $previousData->name = (is_array($key) && isset($key[1]) ? $key[1] : $key[0]);
            $previousData->partner_product_seq = $params->partner_product_seq;
            $previousData->product_seq = $params->product_seq[$i];
            $previousData->partner_group_name = (is_array($key_2) && isset($key_2[1]) ? $key_2[1] : $key_2[0]);
            $previousData->partner_group_seq = $params->partner_group_seq[$i];
            $previousData->nominal_commission_partner = $params->nominal_commission_partner[$i];
            $previousData->nominal_commission_agent = $params->nominal_commission_agent[$i];

            if (!isset($key[1]) AND $params->product_seq[$i] == '') {
                $this->data[DATA_ERROR][ERROR] = TRUE;
                $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_PRODUCT_NOT_EXIST;
            }


            if (!$this->is_commission_corect($previousData->nominal_commission_partner)) {
                $this->data[DATA_ERROR][ERROR] = TRUE;
                $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_PARTNER_COMMISSION_NOT_EXIST;
            }

            if (!$this->is_commission_corect($previousData->nominal_commission_agent)) {
                $this->data[DATA_ERROR][ERROR] = TRUE;
                $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_PARTNER_COMMISSION_NOT_EXIST;
            }


            if ($previousData->partner_group_seq == '') {
                $this->data[DATA_ERROR][ERROR] = TRUE;
                $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_PARTNER_GROUP_SEQ;
            }

            $arayPreviousData[] = $previousData;
        }


        $this->get_data_header();
        $this->data[DATA_HEADER][LIST_DATA][1] = $arayPreviousData;

        if ($this->data[DATA_ERROR][ERROR] !== TRUE) {
            try {
                $this->M_commission_product_partner_detail->trans_begin();

                $this->M_commission_product_partner_detail->delete_partner_product($params);

                for ($i = 0; $i < $count_product; $i++) {
                    $key = explode('$#', $params->product_seq[$i]);
                    $key_2 = explode('$#', $params->partner_group_seq[$i]);
                    $save = new stdClass;
                    $save->user_id = parent::get_admin_user_id();
                    $save->ip_address = parent::get_ip_address();
                    //header
                    $save->partner_product_seq = $params->partner_product_seq;
                    //detail
                    $save->product_seq = $key[0];
                    $save->partner_group_seq = $key_2[0];
                    $save->nominal_commission_partner = $params->nominal_commission_partner[$i];
                    $save->nominal_commission_agent = $params->nominal_commission_agent[$i];
                    $this->M_commission_product_partner_detail->save_product_commission($save);
                }
                $this->data[DATA_SUCCESS][SUCCESS] = TRUE;
                $this->data[DATA_SUCCESS][SUCCESS_MESSAGE] = 'Data Berhasil Dieksekusi';
                $this->M_commission_product_partner_detail->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_commission_product_partner_detail->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_commission_product_partner_detail->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_commission_product_partner_detail->trans_rollback();
            }
        }
    }

    private function is_commission_exist($nominal, $percent) {
        $status = FALSE;
        (int) $nk = isset($nominal) && is_numeric($nominal) && $nominal > 0 ? $nominal : 0;
        (int) $np = isset($percent) && is_numeric($percent) && $this->is_valid_percent($percent) ? $percent : 0;
        if ((int) $nk != 0 || (int) $np !== 0) {
            $status = TRUE;
        }
        return $status;
    }

    private function is_valid_percent($data) {
        return (is_numeric($data) && $data > 0 && $data <= 100) ? TRUE : FALSE;
    }

    private function is_commission_corect($commission) {
        return is_numeric($commission) && $commission > 0 ? TRUE : FALSE;
    }

}
