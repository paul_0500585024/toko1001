<?php

require_once CONTROLLER_BASE_ADMIN;

class C_promo_credit_admin_product extends controller_base_admin {

    private $data;
    private $hseq;
    private $product_category_seq;
    private $menulst;

    public function __construct() {
        $this->data = parent::__construct("CON05011", "admin/master/promo_credit_product");
        $this->initialize();
    }

    private function initialize() {

        $this->hseq = $this->uri->segment(4);
        $this->load->model('admin/master/M_promo_credit');
        $this->load->model('component/M_dropdown_list');
        $this->load->model('component/M_tree_view');
        $this->load->model('home/M_promo_credit');
        $this->menulst = $this->M_tree_view->get_menu_product_category();
        $action_save_category = $this->input->post('btn_save_category');

        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_ADDITIONAL, "get_dropdown_var");
        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        $this->data[TREE_MENU] = $this->get_menu($this->menulst, 0);
        $this->data[DATA_HEADER][LIST_DATA][0] = $this->menulst[0];
        if (!$this->input->post()) {
            if ($this->data[DATA_AUTH][FORM_ACTION] == "") {
                $this->get_data_header();
            }
            $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;

            $this->load->view("admin/master/promo_credit_product", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/master/promo_credit_product", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/master/promo_credit_product", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_ADDITIONAL) {
                        
                    } else {
                        if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                            $this->data[DATA_SUCCESS][SUCCESS] = true;
                            $this->data[DATA_AUTH][FORM_ACTION] = "";
                        }
                        $admin_info[SESSION_DATA] = $this->data;
                        $this->session->set_userdata($admin_info);
                        redirect(base_url($this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq));
                    }
                }
            }
        }
    }

    function get_menu($datas, $parent = 0, $p_level = 0) {
        static $i = 1;
        $tab = str_repeat(" ", $i);
        if (isset($datas[$parent])) {
            $html = "<ul>";
            $i++;
            foreach ($datas[$parent] as $vals) {
                if ($vals->level == 1) {
                    $this->parent_level = $vals->seq;
                    $p_level = $vals->seq;
                }
                $child = $this->get_menu($datas, $vals->seq, $this->parent_level);
                $html .= "$tab";
                $html.='<li><a href="javascript:pilihmenu(\'' . $vals->seq . '~' . $vals->level . '~' . $vals->name . '~' . $p_level . '\')">' . $vals->name . '</a>';
                if ($child) {
                    $i++;
                    $html .= $child;
                    $html .= "$tab";
                }
                $html .= '</li>';
            }
            $html .= "$tab</ul>";
            return $html;
        } else {
            return false;
        }
    }

    protected function get_category_seq_self_parent(&$categories, $datas, $seq, $parent = 0, $p_level = 0, $get_self_seq = TRUE) {
        if ($get_self_seq) {
            $categories[] = $seq;
        }
        foreach ($datas as $each_datas) {
            foreach ($each_datas as $each_datas_detail) {
                if (isset($each_datas_detail->seq)) {
                    if ($each_datas_detail->seq == $seq) {
                        if ($each_datas_detail->parent_seq != 0)
                            $categories[] = $each_datas_detail->parent_seq;
                        $this->get_category_seq_self_parent($categories, $datas, $each_datas_detail->parent_seq, 0, 0, FALSE);
                        return false;
                    }
                }
            }
        }
    }

    protected function get_category_seq_self_child(&$categories, $datas, $parent = 0, $p_level = 0, $get_self_seq = TRUE) {
        $parent_level = "";
        if ($get_self_seq) {
            $categories[] = $parent;
        }
        if (isset($datas[$parent])) {
            foreach ($datas[$parent] as $vals) {
                if ($vals->level == 1) {
                    $parent_level = $vals->seq;
                    $p_level = $vals->seq;
                }
                $categories[] = $vals->seq;
                $this->get_category_seq_self_child($categories, $datas, $vals->seq, $parent_level, FALSE);
            }
//            return $seq;
        }
    }

    protected function get_variant_value($variant_seq, $variant_value, $separator = "") {
        $retval = "";
        switch ($variant_seq) {
            case "1": //all product
                $retval = "";
                break;
            default:
                $retval = $separator . $variant_value;
        }
        return $retval;
    }

    protected function get_dropdown_var() {
        $data = $this->input->post('data');
        $ex = explode('~', $data);
        $this->product_category_seq = $ex[0];
        $level = $ex[1];
        $params = new stdClass;

        if ($this->product_category_seq != 0) {
            //if not all category                        
            $this->get_category_seq_self_parent($seq_self_parent_category, $this->menulst, $this->product_category_seq); // get self and parent
            $this->get_category_seq_self_child($seq_self_child_category, $this->menulst, $this->product_category_seq); // get self and child
            $category_id_self_parent = "'" . implode("','", $seq_self_parent_category) . "'";
            $category_id_self_child = "'" . implode("','", $seq_self_child_category) . "'";
        }

        if ($level == '' AND $this->product_category_seq != 0) {
            redirect(base_url() . 'error_404');
        }

        $params->order = '';

        //set param where for product (self and child)
        $params->where = "WHERE pv.active = 1 AND pv.status IN ('L','C')";

        if ($this->product_category_seq != 0) { //if not all category        
            if (count($seq_self_child_category) > 0) {
                $params->where .= " AND p.category_ln_seq IN(" . $category_id_self_child . ")";
            }
        }
        $query['product_category'] = $this->M_promo_credit->get_thumbs_new_products_no_limit($params); //query product        
        $query_row = $query['product_category']['row'];
        $query_row_result = $query_row->result();
        $query_row_result_first = $query_row_result[0];
        $total_row = $query_row_result_first->total_row;
        $row = $total_row;
        $product = $query['product_category']['product'];

        $total_product = count($product);
        $list_product = array();
        if ($total_product > 0) {
            foreach ($product as $key => $each_product) {
                $list_product[$each_product->product_variant_seq] = $each_product->name . $this->get_variant_value($each_product->variant_seq, $each_product->variant_value, "-");
            }
        }
        echo json_encode(array('list_product' => $list_product));
    }

    protected function get_data_header() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = $this->hseq;

        $get_data = $this->M_promo_credit->get_data_detail($params);

        $get_category = $this->M_promo_credit->get_data_category_detail($params);
        try {
            $sel_data = $this->M_promo_credit->get_data($params);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
                $this->data[DATA_SELECTED][LIST_DATA][] = $get_data;
                if (isset($get_category)) {
                    $this->data[DATA_HEADER][LIST_DATA][1] = $get_category;
                }
            } else {
                $this->load->view("errors/html/error_404");
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_promo_credit->trans_rollback();
            $this->load->view("errors/html/error_404");
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_promo_credit->trans_rollback();
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_promo_credit->trans_rollback();
        }
    }

    protected function save_add() {
        $type = parent::get_input_post('type');
        $select = new stdClass();
        $result = $this->input->post("select_product");
        $select->user_id = parent::get_admin_user_id();
        $select->ip_address = parent::get_ip_address();
        $select->promo_credit_seq = $this->hseq;
        $select->key = $this->hseq;
        $select->promo_credit_period_from = parent::get_input_post('a0');
        $select->promo_credit_period_to = parent::get_input_post('a1');
        $select->promo_credit_name = parent::get_input_post('a2');
        $select->status = parent::get_input_post('a2');

        $get_data = $this->M_promo_credit->get_data_detail($select);
        $get_status = $this->M_promo_credit->get_data($select);
        $get_category = $this->M_promo_credit->get_data_category_detail($select);

        $this->data[DATA_SELECTED][LIST_DATA][] = $select;
        $this->data[DATA_SELECTED][LIST_DATA][] = $get_data;
        $this->data[DATA_HEADER][LIST_DATA][1] = $get_category;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                if ($type == "product") {
                    if ($get_status[0]->status == NEW_STATUS_CODE) {
                        if ($get_category != NULL) {
                            $this->M_promo_credit->save_delete_detail($select, PRODUCT_CATEGORY);
                        }
                        if ($get_data == NULL) {
                            foreach ($result as $data) {
                                $select->promo_credit_seq = $this->hseq;
                                $select->product_variant_seq = $data;
                                $this->M_promo_credit->trans_begin();
                                $this->M_promo_credit->save_add_detail($select);
                                $this->M_promo_credit->trans_commit();
                            }
                        } else {
                            $params = new stdClass();
                            $params->user_id = parent::get_admin_user_id();
                            $params->ip_address = parent::get_ip_address();
                            $params->promo_credit_seq = $this->hseq;
                            $data_new = $params->product_variant_seq = $this->input->post('select_product');
                            $params->type = "";

                            $this->M_promo_credit->save_delete_detail($select, $params->type);
                            $this->M_promo_credit->trans_begin();
                            foreach ($data_new as $data) {
                                $params->promo_credit_seq = $this->hseq;
                                $params->product_variant_seq = $data;
                                $this->M_promo_credit->save_add_detail($params);
                            }
                            $this->M_promo_credit->trans_commit();
                        }
                    } else {
                        throw new Exception(ERROR_SAVE_ADD);
                    }
                } elseif ($type == "category") {
                    $result_category = $this->input->post('credit_category');
                    if ($get_status[0]->status == NEW_STATUS_CODE) {
                        if ($get_data != NULL) {
                            $this->M_promo_credit->save_delete_detail($select, "");
                        }
                        if ($get_category == NULL) {
                            $this->M_promo_credit->trans_begin();
                            foreach ($result_category as $each) {
                                $select->promo_credit_seq = $this->hseq;
                                $select->category_seq = $each;
                                $this->M_promo_credit->save_add_category($select);
                            }
                            $this->M_promo_credit->trans_commit();
                        } else {
                            $params->type = PRODUCT_CATEGORY;
                            $this->M_promo_credit->save_delete_detail($select, $params->type);
                            $this->M_promo_credit->trans_begin();
                            foreach ($result_category as $each) {
                                $select->promo_credit_seq = $this->hseq;
                                $select->category_seq = $each;
                                $this->M_promo_credit->save_add_category($select);
                            }
                            $this->M_promo_credit->trans_commit();
                        }
                    } else {
                        throw new Exception(ERROR_SAVE_ADD);
                    }
                }
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_promo_credit->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_promo_credit->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_promo_credit->trans_rollback();
            }
        }
    }

}

?>