<?php

require_once CONTROLLER_BASE_ADMIN;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Promo Free Fee Merchant
 *
 * @author Jartono
 */
class C_promo_free_fee_admin_merchant extends controller_base_admin {

    private $data;
    private $hseq;

    public function __construct() {
	$this->data = parent::__construct("CON05005", "admin/master/promo_free_fee_merchant");
	$this->initialize();
    }

    private function initialize() {
	$this->hseq = $this->uri->segment(4);
	$this->load->model('admin/master/M_promo_free_fee_admin');
	parent::register_event($this->data, ACTION_SEARCH, "search");
	parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
	parent::fire_event($this->data);
    }

    public function index($pseq = '') {
	if (!$this->input->post()) {
	    if ($this->data[DATA_AUTH][FORM_ACTION] == "") {
		$this->search();
		$this->get_data_header();
	    }
	    $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
	    $this->load->view("admin/master/promo_free_fee_admin_merchant", $this->data);
	} else {
	    if ($this->input->post(CONTROL_SEARCH_NAME) === null) {

		if ($this->data[DATA_ERROR][ERROR] === true) {
		    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
			$this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
			$this->load->view("admin/master/promo_free_fee_admin_merchant", $this->data);
		    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
			$this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
			$this->load->view("admin/master/promo_free_fee_admin_merchant", $this->data);
		    }
		} else {
		    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_ADDITIONAL) {

		    } else {
			if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
			    $this->data[DATA_SUCCESS][SUCCESS] = true;
			    $this->data[DATA_AUTH][FORM_ACTION] = "";
			    //$this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
			}
			$admin_info[SESSION_DATA] = $this->data;
			$this->session->set_userdata($admin_info);
			redirect(base_url("admin/master/promo_free_fee_merchant" . "/" . $this->hseq));
			$this->load->view("admin/master/promo_free_fee_admin_merchant", $this->data);
		    }
		}
	    } else {

	    }
	}
    }

    public function search() {
	$filter = new stdClass;
	$filter->user_id = parent::get_admin_user_id();
	$filter->ip_address = parent::get_ip_address();
	$filter->hseq = $this->hseq;
	try {
	    $tes = $this->data[TREE_MENU] = $this->M_promo_free_fee_admin->get_list_merchant($filter);
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	}
    }

    protected function get_data_header() {
	$params = new stdClass();
	$params->user_id = parent::get_admin_user_id();
	$params->ip_address = parent::get_ip_address();
	$params->key = $this->hseq;
	try {
	    $sel_data = $this->M_promo_free_fee_admin->get_data($params);
	    if (isset($sel_data)) {
		parent::set_data($this->data, $sel_data);
	    } else {
		$this->load->view("errors/html/error_404");
	    }
	} catch (BusisnessException $ex) {
	    parent::set_error($this->data, $ex);
	    $this->load->view("errors/html/error_404");
	} catch (TechnicalException $ex) {
	    parent::set_error($this->data, $ex);
	    $this->load->view("errors/html/error_404");
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	    $this->load->view("errors/html/error_404");
	}
    }

    protected function save_add() {
	$params = new stdClass();
	$a = array();
	$params->user_id = parent::get_admin_user_id();
	$params->ip_address = parent::get_ip_address();
	$params->key = $this->hseq;
	$sel_data = $this->M_promo_free_fee_admin->get_data($params);
	$params->promo_seq = $this->hseq;
	$params->merchant_seq = $this->input->post("merchant_seq");
	if ($this->data[DATA_ERROR][ERROR] === false) {
	    try {
		if (!isset($sel_data)) {
		    throw new Exception();
		} else {
		    if ($sel_data[0]->status != "N") {
			throw new Exception();
		    }
		}
		$this->M_promo_free_fee_admin->trans_begin();
		$this->M_promo_free_fee_admin->save_add_merchant($params);
		$this->M_promo_free_fee_admin->trans_commit();
		//
	    } catch (BusisnessException $ex) {
		parent::set_error($this->data, $ex);
		$this->M_promo_free_fee_admin->trans_rollback();
	    } catch (TechnicalException $ex) {
		parent::set_error($this->data, $ex);
		$this->M_promo_free_fee_admin->trans_rollback();
	    } catch (Exception $ex) {
		parent::set_error($this->data, $ex);
		$this->M_promo_free_fee_admin->trans_rollback();
	    }
	} else {
	    $params->seq = $this->input->post("seq");
	    $params->status = $this->input->post("status");
	    $params->date_from = $this->input->post("date_from");
	    $params->date_to = $this->input->post("date_to");
	    $params->active = $this->input->post("active");
	    $params->all_origin_city = $this->input->post("all_origin_city");
	    $params->all_destination_city = $this->input->post("all_destination_city");
	    $params->notes = $this->input->post("notes");
	    $this->data[DATA_SELECTED][LIST_DATA][] = $params;
	}
    }

}

?>