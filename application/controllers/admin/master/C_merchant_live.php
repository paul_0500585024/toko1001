<?php

require_once CONTROLLER_BASE_ADMIN;

class C_merchant_live extends controller_base_admin {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("MST05007", "admin/master/merchant_live");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('admin/master/M_merchant_live');
        $this->load->model('component/M_dropdown_list');
        $this->load->model('merchant/profile/M_change_password_merchant');
        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_ADDITIONAL, "generate_password");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        $this->data[MERCHANT_LIST] = $this->M_dropdown_list->get_dropdown_merchant();

        if (!$this->input->post()) {
            $this->data[FILTER] = 'admin/master/merchant_live_f.php';
            $this->load->view("admin/master/merchant_live", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/master/merchant_live", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/master/merchant_live", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_ADDITIONAL) {
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                        $admin_info[SESSION_DATA] = $this->data;
                        $this->session->set_userdata($admin_info);
                        redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_ADDITIONAL) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    public function search() {
        $filter = new stdClass;
        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->code = parent::get_input_post("code");
        $filter->merchant_seq = parent::get_input_post("merchant_info_seq");

        try {
            $list_data = $this->M_merchant_live->get_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );

        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->seq,
                    "code" => parent::cdef($data_row->code),
                    "email" => parent::cdef($data_row->email),
                    "name" => parent::cdef($data_row->name),
                    "commission" => "<a href='merchant_live_commission/" . $data_row->seq . "'>Komisi</a>",
                    "created_by" => parent::cdef($data_row->created_by),
                    "created_date" => parent::cdate($data_row->created_date, 1),
                    "modified_by" => parent::cdef($data_row->modified_by),
                    "modified_date" => parent::cdate($data_row->modified_date, 1),
                    "generate_password" => "<button onclick='generate_password(" . $data_row->seq . ")' class='generate-" . $data_row->seq . " btn btn-warning btn-flat btn-xs btn-block'>Buat Password Baru</button>");
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
    }

    protected function get_edit() {
        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->seq = parent::get_input_post("key");

        try {
            $sel_data = $this->M_merchant_live->get_data($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_update() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->seq = parent::get_input_post("seq");
        $params->exp_fee_percent = parent::get_input_post("exp_fee_percent", true, PERCENTAGE_VALIDATOR, "Biaya Ekspedisi", $this->data);
        $params->ins_fee_percent = parent::get_input_post("ins_fee_percent", true, PERCENTAGE_VALIDATOR, "Biaya Asuransi", $this->data);
        $params->notes = parent::get_input_post("notes");
        $data_change = '';

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_merchant_live->trans_begin();
                $sel_data = $this->M_merchant_live->get_data($params);
                if (isset($sel_data)) {
                    if ($params->exp_fee_percent != $sel_data[0]->exp_fee_percent)
                        $data_change = 'YES';
                    if ($params->ins_fee_percent != $sel_data[0]->ins_fee_percent)
                        $data_change = 'YES';
                }
                $this->M_merchant_live->save_update($params);
                if ($data_change == 'YES') {
                    $this->M_merchant_live->save_info($params);
                }
                $this->M_merchant_live->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_merchant_live->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_merchant_live->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_merchant_live->trans_rollback();
            }
        }
    }

    protected function generate_password() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->merchant_seq = parent::get_input_post("merchant_seq");
        $params->seq = parent::get_input_post("merchant_seq");
        $params->random_password = parent::generate_random_text(8);
        $params->new_password = md5(md5($params->random_password));

        $merchant_data = $this->M_merchant_live->get_data($params);

        if ($params->random_password !== "" && $params->merchant_seq != "") {
            try {

                $this->M_change_password_merchant->trans_begin();
                $this->M_change_password_merchant->save_update($params);
				$this->M_change_password_merchant->trans_commit();
                $email = new stdClass();
                $email->code = GENERATE_PASSWORD_MERCHANT;
                $email->ip_address = $params->ip_address;
                $email->user_id = $params->user_id;
                $email->RECIPIENT_NAME = $merchant_data[0]->name;
                $email->to_email = $merchant_data[0]->email;
                $email->password = $params->random_password;
                $email->email = $merchant_data[0]->email;
                parent::email_template($email);
                
                parent::set_json_success();
            } catch (Exception $ex) {
                parent::set_json_error($ex);
            }
        }
    }

}

?>
