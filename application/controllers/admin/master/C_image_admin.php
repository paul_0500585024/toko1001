<?php

require_once CONTROLLER_BASE_ADMIN;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of c_province
 *
 * @author Jartono
 */
class C_image_admin extends controller_base_admin {

    private $data;

    public function __construct() {

        $this->data = parent::__construct("CON03002", "admin/master/image");
        $this->initialize();
    }

    private function initialize() {

        $this->load->model('admin/master/M_image_admin');
        $this->load->model('component/M_dropdown_list');
        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_SAVE_DELETE, "save_delete");
        parent::register_event($this->data, ACTION_APPROVE, "approve");
        parent::register_event($this->data, ACTION_REJECT, "reject");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {


        $this->data[CATEGORY_NAME] = $this->M_dropdown_list->get_dropdown_all_product_category();

        if (!$this->input->post()) {
            $this->data[FILTER] = 'admin/master/image_admin_f.php';
            $this->load->view("admin/master/image_admin", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/master/image_admin", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/master/image_admin", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    public function search() {

        $filter = new stdClass;
        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->category_seq = parent::get_input_post("category_seq");
        $filter->status = parent::get_input_post("status");
        $filter->active = parent::get_input_post("active");
        try {
            $list_data = $this->M_image_admin->get_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );

        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->seq . "/" . $data_row->category_seq,
                    "status" => parent::cstdes($data_row->status, STATUS_ANR),
                    "seq" => $data_row->seq,
                    "category_name" => parent::cdef($data_row->category_name),
                    "active" => parent::cstat($data_row->active),
                    "auth_by" => $data_row->auth_by,
                    "auth_date" => parent::cdate($data_row->auth_date, 1),
                    "created_by" => $data_row->created_by,
                    "created_date" => parent::cdate($data_row->created_date, 1),
                    "modified_by" => $data_row->modified_by,
                    "modified_date" => parent::cdate($data_row->modified_date, 1));
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
    }

    protected function get_edit() {

        $key = explode("/", parent::get_input_post("key"));

        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->seq = $key[0];
        $selected->category_seq = $key[1];

        try {
            $sel_data = $this->M_image_admin->get_data($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function approve() {
        $key = explode("/", parent::get_input_post("key"));

        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->seq = $key[0];
        $selected->category_seq = $key[1];
        $temp_file = str_replace(CDN_IMAGE, "",ADV_TMP_UPLOAD_IMAGE );
        $file = str_replace(CDN_IMAGE, "", ADV_UPLOAD_IMAGE);
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $status = $this->M_image_admin->cek_status_by_seq($selected);
                if ($status[0]->status == NEW_STATUS_CODE) {

                    if ($status[0]->adv_1_img != "") {
                        copy($temp_file . $status[0]->adv_1_img, $file . $status[0]->adv_1_img);
                    }
                    if ($status[0]->adv_2_img != "") {
                        copy($temp_file . $status[0]->adv_2_img, $file . $status[0]->adv_2_img);
                    }
                    if ($status[0]->adv_3_img != "") {
                        copy($temp_file . $status[0]->adv_3_img, $file . $status[0]->adv_3_img);
                    }
                    if ($status[0]->adv_4_img != "") {
                        copy($temp_file . $status[0]->adv_4_img, $file . $status[0]->adv_4_img);
                    }
                    if ($status[0]->adv_5_img != "") {
                        copy($temp_file . $status[0]->adv_5_img, $file . $status[0]->adv_5_img);
                    }
                    if ($status[0]->adv_6_img != "") {
                        copy($temp_file . $status[0]->adv_6_img, $file . $status[0]->adv_6_img);
                    }
                    if ($status[0]->adv_7_img != "") {
                        copy($temp_file . $status[0]->adv_7_img, $file . $status[0]->adv_7_img);
                    }
                    if ($status[0]->banner_img != "") {
                        copy($temp_file . $status[0]->banner_img, $file . $status[0]->banner_img);
                    }

                    $this->M_image_admin->trans_begin();
                    $this->M_image_admin->save_approve($selected);
                    $this->M_image_admin->trans_commit();
                    parent::set_json_success();
                } else {
                    throw new Exception(ERROR_APPROVED_STATUS);
                }
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_image_admin->trans_rollback();
//                parent::set_json_error($ex, ERROR_APPROVED);
                parent::set_json_error($ex, "");
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_image_admin->trans_rollback();
//                parent::set_json_error($ex, ERROR_APPROVED);
                parent::set_json_error($ex, "");
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_image_admin->trans_rollback();
//                parent::set_json_error($ex, ERROR_APPROVED);
                parent::set_json_error($ex, "");
            }
        }
    }

    protected function reject() {
        $key = explode("/", parent::get_input_post("key"));

        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->seq = $key[0];
        $selected->category_seq = $key[1];

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $status = $this->M_image_admin->cek_status_by_seq($selected);
                if ($status[0]->status == NEW_STATUS_CODE) {
                    $this->M_image_admin->trans_begin();
                    $this->M_image_admin->save_reject($selected);
                    $this->M_image_admin->trans_commit();
                    parent::set_json_success();
                } else {
                    throw new Exception(ERROR_APPROVED_STATUS);
                }
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_image_admin->trans_rollback();
                parent::set_json_error($ex, ERROR_REJECT);
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_image_admin->trans_rollback();
                parent::set_json_error($ex, ERROR_REJECT);
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_image_admin->trans_rollback();
                parent::set_json_error($ex, ERROR_REJECT);
            }
        }
    }

    protected function save_add() {
        $file = str_replace(CDN_IMAGE, "", ADV_TMP_UPLOAD_IMAGE);
        if (parent::get_input_post("category_seq") != DEFAULT_PRODUCT_CATEGORY_HOME) {
            parent::upload_file('img1', $file, "", IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_MAIN, "Gambar Kategori 1 ");
            parent::upload_file('img2', $file, "", IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_ADDITIONAL, "Gambar Kategori 2 ");
            parent::upload_file('img3', $file, "", IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_ADDITIONAL, "Gambar Kategori 3 ");
            parent::upload_file('img4', $file, "", IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_ADDITIONAL, "Gambar Kategori 4 ");
            parent::upload_file('img5', $file, "", IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_ADDITIONAL, "Gambar Kategori 5 ");
            parent::upload_file('img6', $file, "", IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_ADDITIONAL, "Gambar Kategori 6 ");
            parent::upload_file('img7', $file, "", IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_ADDITIONAL, "Gambar Kategori 7 ");
            parent::upload_file('banner', $file, "", IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_BANNER, "Banner ");
        } else {
            parent::upload_file('img1', $file, "", IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_HOME, "Gambar Kategori 1 ");
            parent::upload_file('img2', $file, "", IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_HOME_ADDITIONAL, "Gambar Kategori 2 ");
            parent::upload_file('img3', $file, "", IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_HOME_ADDITIONAL, "Gambar Kategori 3 ");
            parent::upload_file('img4', $file, "", IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_HOME_ADDITIONAL, "Gambar Kategori 4 ");
            parent::upload_file('img5', $file, "", IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_HOME_ADDITIONAL, "Gambar Kategori 5 ");
            parent::upload_file('img6', $file, "", IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_HOME_ADDITIONAL, "Gambar Kategori 6 ");
            parent::upload_file('img7', $file, "", IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_HOME_ADDITIONAL, "Gambar Kategori 7 ");
            parent::upload_file('banner', $file, "", IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_PROMO, "Banner ");
        }

        $params_img = new stdClass();

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->category_seq = parent::get_input_post("category_seq");
        $params->img_1 = parent::get_file_name("img1");
        $params->img_1_url = parent::get_input_post("img1_url");
        $params->img_2 = parent::get_file_name("img2");
        $params->img_2_url = parent::get_input_post("img2_url");
        $params->img_3 = parent::get_file_name("img3");
        $params->img_3_url = parent::get_input_post("img3_url");
        $params->img_4 = parent::get_file_name("img4");
        $params->img_4_url = parent::get_input_post("img4_url");
        $params->img_5 = parent::get_file_name("img5");
        $params->img_5_url = parent::get_input_post("img5_url");
        $params->img_6 = parent::get_file_name("img6");
        $params->img_6_url = parent::get_input_post("img6_url");
        $params->img_7 = parent::get_file_name("img7");
        $params->img_7_url = parent::get_input_post("img7_url");
        $params->img_banner = parent::get_file_name("banner");
        $params->img_banner_url = parent::get_input_post("banner_url");
        $params->status = NEW_STATUS_CODE;

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $check = $this->M_image_admin->cek_status();
                $check = null;
                if (isset($check[0])) {
                    throw new Exception(ERROR_VALIDATION_HAVE_NEW_REQUEST);
                } elseif ($params->img_1 == "" && $params->img_2 == "" && $params->img_3 == "" && $params->img_4 == "" && $params->img_5 == "" && $params->img_6 == "" && $params->img_7 == "" && $params->img_banner == "") {
                    throw new Exception(ERROR_VALIDATION_MUST_FILL_ITEM);
                } else {
                    $this->M_image_admin->trans_begin();
                    $request_seq = $this->M_image_admin->save_add($params);

                    if (parent::get_input_post("category_seq") != DEFAULT_PRODUCT_CATEGORY_HOME) {
                        $logo_img1 = parent::upload_file('img1', $file, $params->category_seq . "-" . $request_seq[0]->new_seq . "-" . "1-" . $params->img_1, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_MAIN, "Gambar Kategori 1 ");
                        $logo_img2 = parent::upload_file('img2', $file, $params->category_seq . "-" . $request_seq[0]->new_seq . "-" . "2-" . $params->img_2, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_ADDITIONAL, "Gambar Kategori 2 ");
                        $logo_img3 = parent::upload_file('img3', $file, $params->category_seq . "-" . $request_seq[0]->new_seq . "-" . "3-" . $params->img_3, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_ADDITIONAL, "Gambar Kategori 3 ");
                        $logo_img4 = parent::upload_file('img4', $file, $params->category_seq . "-" . $request_seq[0]->new_seq . "-" . "4-" . $params->img_4, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_ADDITIONAL, "Gambar Kategori 4 ");
                        $logo_img5 = parent::upload_file('img5', $file, $params->category_seq . "-" . $request_seq[0]->new_seq . "-" . "5-" . $params->img_5, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_ADDITIONAL, "Gambar Kategori 5 ");
                        $logo_img6 = parent::upload_file('img6', $file, $params->category_seq . "-" . $request_seq[0]->new_seq . "-" . "6-" . $params->img_6, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_ADDITIONAL, "Gambar Kategori 6 ");
                        $logo_img7 = parent::upload_file('img7', $file, $params->category_seq . "-" . $request_seq[0]->new_seq . "-" . "7-" . $params->img_7, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_ADDITIONAL, "Gambar Kategori 7 ");
                        $banner_img = parent::upload_file('banner', $file, $params->category_seq . "-" . $request_seq[0]->new_seq . "-" . "b-" . $params->img_banner, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_BANNER, "Banner ");
                    } else {
                        $logo_img1 = parent::upload_file('img1', $file, $params->category_seq . "-" . $request_seq[0]->new_seq . "-" . "1-" . $params->img_1, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_HOME, "Gambar Kategori 1 ");
                        $logo_img2 = parent::upload_file('img2', $file, $params->category_seq . "-" . $request_seq[0]->new_seq . "-" . "2-" . $params->img_2, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_HOME_ADDITIONAL, "Gambar Kategori 2 ");
                        $logo_img3 = parent::upload_file('img3', $file, $params->category_seq . "-" . $request_seq[0]->new_seq . "-" . "3-" . $params->img_3, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_HOME_ADDITIONAL, "Gambar Kategori 3 ");
                        $logo_img4 = parent::upload_file('img4', $file, $params->category_seq . "-" . $request_seq[0]->new_seq . "-" . "4-" . $params->img_4, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_HOME_ADDITIONAL, "Gambar Kategori 4 ");
                        $logo_img5 = parent::upload_file('img5', $file, $params->category_seq . "-" . $request_seq[0]->new_seq . "-" . "5-" . $params->img_5, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_HOME_ADDITIONAL, "Gambar Kategori 5 ");
                        $logo_img6 = parent::upload_file('img6', $file, $params->category_seq . "-" . $request_seq[0]->new_seq . "-" . "6-" . $params->img_6, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_HOME_ADDITIONAL, "Gambar Kategori 6 ");
                        $logo_img7 = parent::upload_file('img7', $file, $params->category_seq . "-" . $request_seq[0]->new_seq . "-" . "7-" . $params->img_7, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_HOME_ADDITIONAL, "Gambar Kategori 7 ");
                        $banner_img = parent::upload_file('banner', $file, $params->category_seq . "-" . $request_seq[0]->new_seq . "-" . "b-" . $params->img_banner, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_PROMO, "Banner ");
                    }

                    $params_img->img_1 = $logo_img1->error == true ? "" : $logo_img1->file_name;
                    $params_img->img_2 = $logo_img2->error == true ? "" : $logo_img2->file_name;
                    $params_img->img_3 = $logo_img3->error == true ? "" : $logo_img3->file_name;
                    $params_img->img_4 = $logo_img4->error == true ? "" : $logo_img4->file_name;
                    $params_img->img_5 = $logo_img5->error == true ? "" : $logo_img5->file_name;
                    $params_img->img_6 = $logo_img6->error == true ? "" : $logo_img6->file_name;
                    $params_img->img_7 = $logo_img7->error == true ? "" : $logo_img7->file_name;
                    $params_img->banner = $banner_img->error == true ? "" : $banner_img->file_name;
                    $params_img->category_seq = $params->category_seq;
                    $params_img->seq = $request_seq[0]->new_seq;

                    $this->M_image_admin->save_update_img($params_img);
                    $this->M_image_admin->trans_commit();

//                    parent::Unlink_file(ADV_TMP_UPLOAD_IMAGE . $params->img_1);
//                    parent::Unlink_file(ADV_TMP_UPLOAD_IMAGE . $params->img_2);
//                    parent::Unlink_file(ADV_TMP_UPLOAD_IMAGE . $params->img_3);
//                    parent::Unlink_file(ADV_TMP_UPLOAD_IMAGE . $params->img_4);
//                    parent::Unlink_file(ADV_TMP_UPLOAD_IMAGE . $params->img_5);
//                    parent::Unlink_file(ADV_TMP_UPLOAD_IMAGE . $params->img_6);
//                    parent::Unlink_file(ADV_TMP_UPLOAD_IMAGE . $params->img_7);
//                    parent::Unlink_file(ADV_TMP_UPLOAD_IMAGE . $params->banner);
                }
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_image_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_image_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_image_admin->trans_rollback();
            }
        }
    }

    function save_update() {
        $file = str_replace(CDN_IMAGE, "", ADV_TMP_UPLOAD_IMAGE);
        
        if (parent::get_input_post("category_seq") != DEFAULT_PRODUCT_CATEGORY_HOME) {
            parent::upload_file('img1', $file, "", IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_MAIN, "Gambar Kategori 1 ");
            parent::upload_file('img2', $file, "", IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_ADDITIONAL, "Gambar Kategori 2 ");
            parent::upload_file('img3', $file, "", IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_ADDITIONAL, "Gambar Kategori 3 ");
            parent::upload_file('img4', $file, "", IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_ADDITIONAL, "Gambar Kategori 4 ");
            parent::upload_file('img5', $file, "", IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_ADDITIONAL, "Gambar Kategori 5 ");
            parent::upload_file('img6', $file, "", IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_ADDITIONAL, "Gambar Kategori 6 ");
            parent::upload_file('img7', $file, "", IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_ADDITIONAL, "Gambar Kategori 7 ");
            parent::upload_file('banner', $file, "", IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_BANNER, "Banner ");
        } else {
            parent::upload_file('img1', $file, "", IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_HOME, "Gambar Kategori 1 ");
            parent::upload_file('img2', $file, "", IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_HOME_ADDITIONAL, "Gambar Kategori 2 ");
            parent::upload_file('img3', $file, "", IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_HOME_ADDITIONAL, "Gambar Kategori 3 ");
            parent::upload_file('img4', $file, "", IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_HOME_ADDITIONAL, "Gambar Kategori 4 ");
            parent::upload_file('img5', $file, "", IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_HOME_ADDITIONAL, "Gambar Kategori 5 ");
            parent::upload_file('img6', $file, "", IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_HOME_ADDITIONAL, "Gambar Kategori 6 ");
            parent::upload_file('img7', $file, "", IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_HOME_ADDITIONAL, "Gambar Kategori 7 ");
            parent::upload_file('banner', $file, "", IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_PROMO, "Banner ");
        }

        $params_img = new stdClass();
        $params_check = new stdClass();

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->category_seq = parent::get_input_post("category_seq");
        $params->old_category_seq = parent::get_input_post("old_category_seq");
        $params->seq = parent::get_input_post("seq");
        $params->old_img_1 = parent::get_input_post("old_img_1");
        $params->old_img_2 = parent::get_input_post("old_img_2");
        $params->old_img_3 = parent::get_input_post("old_img_3");
        $params->old_img_4 = parent::get_input_post("old_img_4");
        $params->old_img_5 = parent::get_input_post("old_img_5");
        $params->old_img_6 = parent::get_input_post("old_img_6");
        $params->old_img_7 = parent::get_input_post("old_img_7");
        $params->old_img_banner = parent::get_input_post("old_img_banner");

        $params->img_1 = parent::get_file_name("img1", $params->old_img_1);
        $params->img_1_url = parent::get_input_post("img1_url");
        $params->img_2 = parent::get_file_name("img2", $params->old_img_2);
        $params->img_2_url = parent::get_input_post("img2_url");
        $params->img_3 = parent::get_file_name("img3", $params->old_img_3);
        $params->img_3_url = parent::get_input_post("img3_url");
        $params->img_4 = parent::get_file_name("img4", $params->old_img_4);
        $params->img_4_url = parent::get_input_post("img4_url");
        $params->img_5 = parent::get_file_name("img5", $params->old_img_5);
        $params->img_5_url = parent::get_input_post("img5_url");
        $params->img_6 = parent::get_file_name("img6", $params->old_img_6);
        $params->img_6_url = parent::get_input_post("img6_url");
        $params->img_7 = parent::get_file_name("img7", $params->old_img_7);
        $params->img_7_url = parent::get_input_post("img7_url");
        $params->img_banner = parent::get_file_name("banner", $params->old_img_banner);
        $params->img_banner_url = parent::get_input_post("banner_url");

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->category_seq = parent::get_input_post("old_category_seq");

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $status = $this->M_image_admin->cek_status_by_seq($params);
                if ($status[0]->status == NEW_STATUS_CODE) {
                    $this->M_image_admin->trans_begin();
                    $this->M_image_admin->save_update($params);

                    if ($params->old_category_seq != DEFAULT_PRODUCT_CATEGORY_HOME) {
                        $logo_img1 = parent::upload_file('img1', $file, $params->category_seq . "-" . $params->seq . "-" . "1-" . $params->img_1, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_MAIN, "Gambar Kategori 1 ");
                        $logo_img2 = parent::upload_file('img2', $file, $params->category_seq . "-" . $params->seq . "-" . "2-" . $params->img_2, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_ADDITIONAL, "Gambar Kategori 2 ");
                        $logo_img3 = parent::upload_file('img3', $file, $params->category_seq . "-" . $params->seq . "-" . "3-" . $params->img_3, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_ADDITIONAL, "Gambar Kategori 3 ");
                        $logo_img4 = parent::upload_file('img4', $file, $params->category_seq . "-" . $params->seq . "-" . "4-" . $params->img_4, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_ADDITIONAL, "Gambar Kategori 4 ");
                        $logo_img5 = parent::upload_file('img5', $file, $params->category_seq . "-" . $params->seq . "-" . "5-" . $params->img_5, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_ADDITIONAL, "Gambar Kategori 5 ");
                        $logo_img6 = parent::upload_file('img6', $file, $params->category_seq . "-" . $params->seq . "-" . "6-" . $params->img_6, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_ADDITIONAL, "Gambar Kategori 6 ");
                        $logo_img7 = parent::upload_file('img7', $file, $params->category_seq . "-" . $params->seq . "-" . "7-" . $params->img_7, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_ADDITIONAL, "Gambar Kategori 7 ");
                        $banner_img = parent::upload_file('banner', $file, $params->category_seq . "-" . $params->seq . "-" . "b-" . $params->img_banner, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_BANNER, "Banner ");
                    } else {
                        $logo_img1 = parent::upload_file('img1', $file, $params->category_seq . "-" . $params->seq . "-" . "1-" . $params->img_1, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_HOME, "Gambar Kategori 1 ");
                        $logo_img2 = parent::upload_file('img2', $file, $params->category_seq . "-" . $params->seq . "-" . "2-" . $params->img_2, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_HOME_ADDITIONAL, "Gambar Kategori 2 ");
                        $logo_img3 = parent::upload_file('img3', $file, $params->category_seq . "-" . $params->seq . "-" . "3-" . $params->img_3, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_HOME_ADDITIONAL, "Gambar Kategori 3 ");
                        $logo_img4 = parent::upload_file('img4', $file, $params->category_seq . "-" . $params->seq . "-" . "4-" . $params->img_4, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_HOME_ADDITIONAL, "Gambar Kategori 4 ");
                        $logo_img5 = parent::upload_file('img5', $file, $params->category_seq . "-" . $params->seq . "-" . "5-" . $params->img_5, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_HOME_ADDITIONAL, "Gambar Kategori 5 ");
                        $logo_img6 = parent::upload_file('img6', $file, $params->category_seq . "-" . $params->seq . "-" . "6-" . $params->img_6, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_HOME_ADDITIONAL, "Gambar Kategori 6 ");
                        $logo_img7 = parent::upload_file('img7', $file, $params->category_seq . "-" . $params->seq . "-" . "7-" . $params->img_7, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_HOME_ADDITIONAL, "Gambar Kategori 7 ");
                        $banner_img = parent::upload_file('banner', $file, $params->category_seq . "-" . $params->seq . "-" . "b-" . $params->img_banner, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_PRODUCT_CATEGORY_PROMO, "Banner ");
                    }

                    $params_img->img_1 = $logo_img1->error == true ? $params->old_img_1 : $logo_img1->file_name;
                    $params_img->img_2 = $logo_img2->error == true ? $params->old_img_2 : $logo_img2->file_name;
                    $params_img->img_3 = $logo_img3->error == true ? $params->old_img_3 : $logo_img3->file_name;
                    $params_img->img_4 = $logo_img4->error == true ? $params->old_img_4 : $logo_img4->file_name;
                    $params_img->img_5 = $logo_img5->error == true ? $params->old_img_5 : $logo_img5->file_name;
                    $params_img->img_6 = $logo_img6->error == true ? $params->old_img_6 : $logo_img6->file_name;
                    $params_img->img_7 = $logo_img7->error == true ? $params->old_img_7 : $logo_img7->file_name;
                    $params_img->banner = $banner_img->error == true ? $params->old_img_banner : $banner_img->file_name;
                    $params_img->category_seq = $params->category_seq;
                    $params_img->seq = $params->seq;

                    $this->M_image_admin->save_update_img($params_img);
                    $this->M_image_admin->trans_commit();
                } else {
                    throw new Exception(ERROR_REQUEST);
                }
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_image_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_image_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_image_admin->trans_rollback();
            }
        }
    }

//    protected function save_delete() {
//
//        $params = new stdClass();
//
//        $params->user_id = parent::get_admin_user_id();
//        $params->ip_address = parent::get_ip_address();
//        $params->key = parent::get_input_post("key");
//
//        try {
//            $sel_data = $this->M_image_admin->get_data($params);
//            $img[] = $sel_data[0]->adv_1_img;
//            $img[] = $sel_data[0]->adv_2_img;
//            $img[] = $sel_data[0]->adv_3_img;
//            $img[] = $sel_data[0]->adv_4_img;
//            $img[] = $sel_data[0]->adv_5_img;
//            $img[] = $sel_data[0]->adv_6_img;
//            $img[] = $sel_data[0]->banner_img;
//
//            $dir = "assets/img/admin/tmp_advertise/";
//
//            $this->M_image_admin->trans_begin();
//            $this->M_image_admin->save_delete($params);
//            $this->M_image_admin->trans_commit();
//
//            for ($i = 0; $i < count($img); $i++) {
//                $del = unlink($dir . $img[$i]);
//            }
//        } catch (BusisnessException $ex) {
//            parent::set_error($this->data, $ex);
//            $this->M_image_admin->trans_rollback();
//        } catch (TechnicalException $ex) {
//            parent::set_error($this->data, $ex);
//            $this->M_image_admin->trans_rollback();
//        } catch (Exception $ex) {
//            parent::set_error($this->data, $ex);
//            $this->M_image_admin->trans_rollback();
//        }
//    }
}

?>
