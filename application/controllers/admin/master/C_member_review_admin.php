<?php

require_once CONTROLLER_BASE_ADMIN;

class C_member_review_admin extends controller_base_admin {

    private $data;

    public function __construct() {
	$this->data = parent::__construct("MST07003", "admin/master/member_review");
	$this->initialize();
    }

    private function initialize() {

	$this->load->model('admin/master/M_member_review_admin');
	parent::register_event($this->data, ACTION_SEARCH, "search");
	parent::register_event($this->data, ACTION_EDIT, "get_edit");
	parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
	parent::register_event($this->data, ACTION_APPROVE, "approve");
	parent::register_event($this->data, ACTION_REJECT, "reject");
	if ($this->data[DATA_INIT] === true) {
	    parent::fire_event($this->data);
	}
    }

    public function index() {

	if (!$this->input->post()) {
	    $this->data[FILTER] = 'admin/master/member_review_f.php';
	    $this->load->view("admin/master/member_review", $this->data);
	} else {
	    if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
		if ($this->data[DATA_ERROR][ERROR] === true) {
		    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
			$this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
			$this->load->view("admin/master/member_review", $this->data);
		    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
			$this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
			$this->load->view("admin/master/member_review", $this->data);
		    }
		} else {
		    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
			$this->data[DATA_SUCCESS][SUCCESS] = true;
			$this->data[DATA_AUTH][FORM_ACTION] = "";
		    }
		    $admin_info[SESSION_DATA] = $this->data;
		    $this->session->set_userdata($admin_info);
		    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
		}
	    }
	}
    }

    public function search() {
	$filter = new stdClass;
	$filter->user_id = parent::get_admin_user_id();
	$filter->ip_address = parent::get_ip_address();
	$filter->start = parent::get_input_post("start");
	$filter->length = parent::get_input_post("length");
	$filter->order = parent::get_input_post("order");
	$filter->column = parent::get_input_post("column");
	$filter->status = parent::get_input_post("status");


	try {
	    $list_data = $this->M_member_review_admin->get_list($filter);
	    parent::set_list_data($this->data, $list_data);
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	}
	$output = array(
	    "sEcho" => parent::get_input_post("draw"),
	    "iTotalRecords" => $list_data[0][0]->total_rec,
	    "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
	    "aaData" => array()
	);
	if (isset($list_data[1])) {
	    foreach ($list_data[1] as $data_row) {

		$row = array("DT_RowId" => $data_row->order_seq . '-' . $data_row->product_variant_seq,
		    "product_name" => $data_row->product_name,
		    "order_no" => $data_row->order_no,
		    "rate" => $data_row->rate,
		    "review" => $data_row->review,
		    "review_admin" => $data_row->review_admin,
		    "status" => parent::cstdes($data_row->status, STATUS_NRAV),
		    "created_by" => $data_row->created_by,
		    "created_date" => parent::cdate($data_row->created_date, 1),
		    "modified_by" => $data_row->modified_by,
		    "modified_date" => parent::cdate($data_row->modified_date, 1));

		$output['aaData'][] = $row;
	    }
	};

	echo json_encode($output);
    }

    protected function get_edit() {

	$selected = new stdClass();
	$selected->user_id = parent::get_admin_user_id();
	$selected->ip_address = parent::get_ip_address();
	$key = explode('-', parent::get_input_post("key"));
	$selected->order_seq = $key[0];
	$selected->product_variant_seq = $key[1];

	try {
	    $sel_data = $this->M_member_review_admin->get_data($selected);
	    if (isset($sel_data)) {
		parent::set_data($this->data, $sel_data);
	    }
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	}
    }

    protected function save_update() {

	$params = new stdClass();
	$params->user_id = parent::get_admin_user_id();
	$params->ip_address = parent::get_ip_address();
	$params->order_seq = parent::get_input_post("order_seq");
	$params->product_variant_seq = parent::get_input_post("product_variant_seq");
	$params->review_admin = parent::get_input_post("review_admin");
	$sel_data = $this->M_member_review_admin->get_data($params);
	$params->status = NEW_STATUS_CODE;
	if (isset($sel_data)) {
	    $params->status = $sel_data[0]->status;
	    if ($sel_data[0]->status != NEW_STATUS_CODE) {
		$this->data[DATA_ERROR][ERROR] = true;
		$this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_UPDATE;
	    }
	}
	$params->review_admin = parent::get_input_post("review_admin");
	$params->product_name = parent::get_input_post("product_name");
	$params->rate = parent::get_input_post("rate");
	$params->review = parent::get_input_post("review");
	$this->data[DATA_SELECTED][LIST_DATA][] = $params;

	if ($this->data[DATA_ERROR][ERROR] === false) {
	    try {
		$this->M_member_review_admin->trans_begin();
		$this->M_member_review_admin->save_update($params);
		$this->M_member_review_admin->trans_commit();
	    } catch (BusisnessException $ex) {
		parent::set_error($this->data, $ex);
		$this->M_member_review_admin->trans_rollback();
	    } catch (TechnicalException $ex) {
		parent::set_error($this->data, $ex);
		$this->M_member_review_admin->trans_rollback();
	    } catch (Exception $ex) {
		parent::set_error($this->data, $ex);
		$this->M_member_review_admin->trans_rollback();
	    }
	}
    }

    protected function reject() {

	$params = new stdClass();
	$params->user_id = parent::get_admin_user_id();
	$params->ip_address = parent::get_ip_address();
	$key = explode('-', parent::get_input_post("key"));
	$params->order_seq = $key[0];
	$params->product_variant_seq = $key[1];
	$sel_data = $this->M_member_review_admin->get_data($params);
	$params->status = REJECT_STATUS_CODE;
	try {
	    if (!isset($sel_data)) {
		throw new Exception();
	    } else {
		if ($sel_data[0]->status != NEW_STATUS_CODE) {
		    throw new Exception();
		}
	    }
	    $params->review_admin = $sel_data[0]->review_admin;
	    $this->M_member_review_admin->trans_begin();
	    $this->M_member_review_admin->save_status($params);
	    $this->M_member_review_admin->trans_commit();
	    parent::set_json_success();
	} catch (BusisnessException $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_member_review_admin->trans_rollback();
	    parent::set_json_error($ex, ERROR_REJECT);
	} catch (TechnicalException $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_member_review_admin->trans_rollback();
	    parent::set_json_error($ex, ERROR_REJECT);
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_member_review_admin->trans_rollback();
	    parent::set_json_error($ex, ERROR_REJECT);
	}
	die();
    }

    protected function approve() {
	$params = new stdClass();
	$params->user_id = parent::get_admin_user_id();
	$params->ip_address = parent::get_ip_address();
	$key = explode('-', parent::get_input_post("key"));
	$params->order_seq = $key[0];
	$params->product_variant_seq = $key[1];
	$sel_data = $this->M_member_review_admin->get_data($params);
	$params->status = APPROVE_STATUS_CODE;
	try {
	    if (!isset($sel_data)) {
		throw new Exception();
	    } else {
		if ($sel_data[0]->status != NEW_STATUS_CODE) {
		    throw new Exception();
		}
	    }
	    $this->M_member_review_admin->trans_begin();
	    $params->review_admin = $sel_data[0]->review_admin;
	    if ($sel_data[0]->review_admin == '')
		$params->review_admin = $sel_data[0]->review;
	    $this->M_member_review_admin->save_status($params);
	    $params->colom = $sel_data[0]->rate . 'star';
	    $this->M_member_review_admin->save_rate($params);
	    $this->M_member_review_admin->trans_commit();
	    parent::set_json_success();
	} catch (BusisnessException $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_member_review_admin->trans_rollback();
	    parent::set_json_error($ex, ERROR_APPROVED);
	} catch (TechnicalException $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_member_review_admin->trans_rollback();
	    parent::set_json_error($ex, ERROR_APPROVED);
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_member_review_admin->trans_rollback();
	    parent::set_json_error($ex, ERROR_APPROVED);
	}
	die();
    }

}

?>