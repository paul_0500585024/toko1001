<?php

require_once CONTROLLER_BASE_ADMIN;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Promo Free Fee
 *
 * @author Jartono
 */
class C_promo_free_fee_admin extends controller_base_admin {

    private $data;

    public function __construct() {
	$this->data = parent::__construct("CON05001", "admin/master/promo_free_fee");
	$this->initialize();
    }

    private function initialize() {
	$this->load->model('admin/master/M_promo_free_fee_admin');
	parent::register_event($this->data, ACTION_SEARCH, "search");
	parent::register_event($this->data, ACTION_EDIT, "get_edit");
	parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
	parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
	parent::register_event($this->data, ACTION_SAVE_DELETE, "save_delete");
	parent::register_event($this->data, ACTION_APPROVE, "approve");
	parent::register_event($this->data, ACTION_REJECT, "reject");
	if ($this->data[DATA_INIT] === true) {
	    parent::fire_event($this->data);
	}
    }

    public function index() {
	if (!$this->input->post()) {
	    $this->data[FILTER] = 'admin/master/promo_free_fee_admin_f.php';
	    $this->load->view("admin/master/promo_free_fee_admin", $this->data);
	} else {
	    if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
		if ($this->data[DATA_ERROR][ERROR] === true) {
		    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
			$this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
			$this->load->view("admin/master/promo_free_fee_admin", $this->data);
		    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
			$this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
			$this->load->view("admin/master/promo_free_fee_admin", $this->data);
		    }
		} else {
		    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
			$this->data[DATA_SUCCESS][SUCCESS] = true;
			$this->data[DATA_AUTH][FORM_ACTION] = "";
		    }
		    $admin_info[SESSION_DATA] = $this->data;
		    $this->session->set_userdata($admin_info);
		    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
		}
	    }
	}
    }

    public function search() {
	$params = new stdClass;
	$params->user_id = parent::get_admin_user_id();
	$params->ip_address = parent::get_ip_address();
	$params->start = parent::get_input_post("start");
	$params->length = parent::get_input_post("length");
	$params->order = parent::get_input_post("order");
	$params->column = parent::get_input_post("column");
	$params->notes = parent::get_input_post("notes");
	$params->active = parent::get_input_post("active");
	$params->status = parent::get_input_post("status");
	try {
	    $list_data = $this->M_promo_free_fee_admin->get_list($params);
	    parent::set_list_data($this->data, $list_data);
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	}

	$output = array(
	    "sEcho" => parent::get_input_post("draw"),
	    "iTotalRecords" => $list_data[0][0]->total_rec,
	    "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
	    "aaData" => array()
	);
	if (isset($list_data[1])) {
	    foreach ($list_data[1] as $data_row) {
		$row = array("DT_RowId" => $data_row->seq,
		    "date_from" => parent::cdate($data_row->date_from),
		    "date_to" => parent::cdate($data_row->date_to),
		    "notes" => parent::cdef($data_row->notes),
		    "status" => parent::cstdes($data_row->status, STATUS_ANR),
		    "auth_by" => $data_row->auth_by,
		    "auth_date" => $data_row->auth_date,
		    "all_origin_city" => parent::cstat($data_row->all_origin_city),
		    "all_destination_city" => parent::cstat($data_row->all_destination_city),
		    "active" => parent::cstat($data_row->active),
		    "created_by" => $data_row->created_by,
		    "created_date" => parent::cdate($data_row->created_date, 1),
		    "modified_by" => $data_row->modified_by,
		    "modified_date" => parent::cdate($data_row->modified_date, 1),
		    "merchant" => "<a href='promo_free_fee_merchant/" . $data_row->seq . "'>Merchant</a>",
		    "detail" => "<a href='promo_free_fee_detail/" . $data_row->seq . "'>Kota</a>");
		$output['aaData'][] = $row;
	    }
	};
	echo json_encode($output);
    }

    protected function get_edit() {
	$params = new stdClass();
	$params->user_id = parent::get_admin_user_id();
	$params->ip_address = parent::get_ip_address();
	$params->key = parent::get_input_post("key");
	try {
	    $sel_data = $this->M_promo_free_fee_admin->get_data($params);
	    if (isset($sel_data)) {
		parent::set_data($this->data, $sel_data);
	    }
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	}
    }

    protected function save_add() {
	$params = new stdClass();
	$params->user_id = parent::get_admin_user_id();
	$params->ip_address = parent::get_ip_address();
	$tanggal = explode(' - ', parent::get_input_post("date_from", true, FILL_VALIDATOR, "Periode", $this->data));
	$params->date_from = $tanggal[0];
	$params->date_to = $tanggal[1];
	$params->all_origin_city = parent::get_input_post("all_origin_city");
	$params->all_destination_city = parent::get_input_post("all_destination_city");
	$params->notes = parent::get_input_post("notes");
	$params->active = parent::get_input_post("active");
	$params->status = parent::get_input_post("status");
	$this->data[DATA_SELECTED][LIST_DATA][] = $params;
	if ($this->data[DATA_ERROR][ERROR] === false) {
	    try {
		$this->M_promo_free_fee_admin->trans_begin();
		$this->M_promo_free_fee_admin->save_add($params);
		$this->M_promo_free_fee_admin->trans_commit();
	    } catch (BusisnessException $ex) {
		parent::set_error($this->data, $ex);
		$this->M_promo_free_fee_admin->trans_rollback();
	    } catch (TechnicalException $ex) {
		parent::set_error($this->data, $ex);
		$this->M_promo_free_fee_admin->trans_rollback();
	    } catch (Exception $ex) {
		parent::set_error($this->data, $ex);
		$this->M_promo_free_fee_admin->trans_rollback();
	    }
	}
    }

    protected function save_update() {
	$params = new stdClass();
	$params->user_id = parent::get_admin_user_id();
	$params->ip_address = parent::get_ip_address();
	$params->seq = parent::get_input_post("seq");
	$tanggal = explode(' - ', parent::get_input_post("date_from", true, FILL_VALIDATOR, "Periode", $this->data));
	$params->date_from = $tanggal[0];
	$params->date_to = $tanggal[1];
	$params->all_origin_city = parent::get_input_post("all_origin_city");
	$params->all_destination_city = parent::get_input_post("all_destination_city");
	$params->notes = parent::get_input_post("notes");
	$params->active = parent::get_input_post("active");
	$params->status = parent::get_input_post("status");
	$this->data[DATA_SELECTED][LIST_DATA][] = $params;
	if ($this->data[DATA_ERROR][ERROR] === false) {
	    try {
		$this->M_promo_free_fee_admin->trans_begin();
		$this->M_promo_free_fee_admin->save_update($params);
		$this->M_promo_free_fee_admin->trans_commit();
	    } catch (BusisnessException $ex) {
		parent::set_error($this->data, $ex);
		$this->M_promo_free_fee_admin->trans_rollback();
	    } catch (TechnicalException $ex) {
		parent::set_error($this->data, $ex);
		$this->M_promo_free_fee_admin->trans_rollback();
	    } catch (Exception $ex) {
		parent::set_error($this->data, $ex);
		$this->M_promo_free_fee_admin->trans_rollback();
	    }
	}
    }

    protected function reject() {
	$params = new stdClass();
	$params->user_id = parent::get_admin_user_id();
	$params->ip_address = parent::get_ip_address();
	$params->key = parent::get_input_post("key");
	$sel_data = $this->M_promo_free_fee_admin->get_data($params);
	$params->seq = parent::get_input_post("key");
	$params->status = REJECT_STATUS_CODE;
	try {
	    if (!isset($sel_data)) {
		throw new Exception();
	    } else {
		if ($sel_data[0]->status != "N") {
		    throw new Exception();
		}
	    }
	    $this->M_promo_free_fee_admin->trans_begin();
	    $this->M_promo_free_fee_admin->save_status($params);
	    $this->M_promo_free_fee_admin->trans_commit();
	    parent::set_json_success();
	} catch (BusisnessException $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_promo_free_fee_admin->trans_rollback();
	    parent::set_json_error($ex, ERROR_REJECT);
	} catch (TechnicalException $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_promo_free_fee_admin->trans_rollback();
	    parent::set_json_error($ex, ERROR_REJECT);
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_promo_free_fee_admin->trans_rollback();
	    parent::set_json_error($ex, ERROR_REJECT);
	}
	die();
    }

    protected function approve() {
	$params = new stdClass();
	$params->user_id = parent::get_admin_user_id();
	$params->ip_address = parent::get_ip_address();
	$params->key = parent::get_input_post("key");
	$sel_data = $this->M_promo_free_fee_admin->get_data($params);
	$params->status = APPROVE_STATUS_CODE;
	$params->seq = parent::get_input_post("key");
	try {
	    if (!isset($sel_data)) {
		throw new Exception();
	    } else {
		if ($sel_data[0]->status != "N") {
		    throw new Exception();
		}
	    }
	    $this->M_promo_free_fee_admin->trans_begin();
	    $this->M_promo_free_fee_admin->save_status($params);
	    $this->M_promo_free_fee_admin->trans_commit();
	    parent::set_json_success();
	} catch (BusisnessException $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_promo_free_fee_admin->trans_rollback();
	    parent::set_json_error($ex, ERROR_APPROVED);
	} catch (TechnicalException $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_promo_free_fee_admin->trans_rollback();
	    parent::set_json_error($ex, ERROR_APPROVED);
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_promo_free_fee_admin->trans_rollback();
	    parent::set_json_error($ex, ERROR_APPROVED);
	}

	die();
    }

    protected function save_delete() {
	$params = new stdClass();
	$params->user_id = parent::get_admin_user_id();
	$params->ip_address = parent::get_ip_address();
	$params->key = parent::get_input_post("key");
	$sel_data = $this->M_promo_free_fee_admin->get_data($params);
	$params->seq = parent::get_input_post("key");
	try {
	    if (!isset($sel_data)) {
		throw new Exception();
	    } else {
		if ($sel_data[0]->status != "N") {
		    throw new Exception();
		}
	    }
	    $this->M_promo_free_fee_admin->trans_begin();
	    $this->M_promo_free_fee_admin->save_delete($params);
	    $this->M_promo_free_fee_admin->trans_commit();
	    parent::set_json_success();
	} catch (BusisnessException $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_promo_free_fee_admin->trans_rollback();
	    parent::set_json_error($ex, ERROR_DELETE);
	} catch (TechnicalException $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_promo_free_fee_admin->trans_rollback();
	    parent::set_json_error($ex, ERROR_DELETE);
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_promo_free_fee_admin->trans_rollback();
	    parent::set_json_error($ex, ERROR_DELETE);
	}
	die();
    }

}

?>