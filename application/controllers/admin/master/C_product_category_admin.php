<?php

require_once CONTROLLER_BASE_ADMIN;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Product Categoy
 *
 * @author Jartono
 */
class C_product_category_admin extends controller_base_admin {

    private $data;
    private $parent_level;

    public function __construct() {
        $this->data = parent::__construct("MST02001", "admin/master/product_category");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('admin/master/M_product_category_admin');
        $this->load->model('component/M_tree_view');
        $this->load->model('component/M_dropdown_list');

        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_SAVE_DELETE, "save_delete");
        parent::register_event($this->data, ACTION_ADDITIONAL, "get_dropdown_var");
        parent::fire_event($this->data);
    }

    public function index() {

        if (!$this->input->post()) {
            $this->data[FILTER] = 'admin/master/product_category_admin_f.php';
            $this->load->view("admin/master/product_category_admin", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                $menulst = $this->M_tree_view->get_menu_product_category();
                $this->data[TREE_MENU] = $this->get_menu($menulst, 0);
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/master/product_category_admin", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/master/product_category_admin", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_ADDITIONAL) {
                        
                    } else {
                        if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                            $this->data[DATA_SUCCESS][SUCCESS] = true;
                            $this->data[DATA_AUTH][FORM_ACTION] = "";
                        }
                        $admin_info[SESSION_DATA] = $this->data;
                        $this->session->set_userdata($admin_info);
                        redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                    }
                }
            }
        }
    }

    protected function get_dropdown_var($pTipe = 0) {
        $variant_seq = parent::get_input_post("variant_seq");
        $stipe = parent::get_input_post("stipe");
        if ($stipe == 1) {
            $pTipe = 1;
        }
        try {
            $this->data[VARIANT] = $this->M_dropdown_list->get_dropdown_variant($variant_seq, $pTipe);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        $this->load->view("admin/component/dropdown/com_drop_varian", $this->data);
    }

    public function search() {
        $filter = new stdClass;
        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->name = parent::get_input_post("name");
        $filter->parent_name = parent::get_input_post("parent_name");
        $filter->active = parent::get_input_post("active");
        $filter->level = parent::get_input_post("level");

        try {
            $list_data = $this->M_product_category_admin->get_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        if (isset($list_data)) {
            $output = array(
                "sEcho" => parent::get_input_post("draw"),
                "iTotalRecords" => $list_data[0][0]->total_rec,
                "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
                "aaData" => array()
            );
            if (isset($list_data[1])) {
                foreach ($list_data[1] as $data_row) {
                    $row = array("DT_RowId" => $data_row->seq,
                        "name" => parent::cdef($data_row->name),
                        "parent_seq" => $data_row->parent_seq,
                        "level" => $data_row->level,
                        "order" => $data_row->order,
                        "include_ins" => $data_row->include_ins,
                        "exp_method" => $data_row->exp_method,
                        "variant_seq" => $data_row->variant_seq,
                        "trx_fee_percent" => $data_row->trx_fee_percent,
                        "active" => parent::cstat($data_row->active),
                        "parent_name" => $data_row->parent_name,
                        "created_by" => $data_row->created_by,
                        "created_date" => parent::cdate($data_row->created_date, 1),
                        "modified_by" => $data_row->modified_by,
                        "modified_date" => parent::cdate($data_row->modified_date, 1));
                    $output['aaData'][] = $row;
                }
            }
        } else {
            $output = array(
                "sEcho" => "",
                "iTotalRecords" => 0,
                "iTotalDisplayRecords" => 0,
                "aaData" => array()
            );
        }

        echo json_encode($output);
    }

    protected function get_edit() {
        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = parent::get_input_post("key");
        try {
            $sel_data = $this->M_product_category_admin->get_data($selected);
            if (isset($sel_data))
                $this->data[VARIANT] = $this->M_dropdown_list->get_dropdown_variant($sel_data[0]->parent_seq, 0);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_add() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->name = parent::get_input_post("name", true, FILL_VALIDATOR, "Nama Kategori", $this->data);
        $params->parent_seq = parent::get_input_post("parent_seq");
        $params->level = parent::get_input_post("level");
        $params->order = parent::get_input_post("order", true, NUMERIC_VALIDATOR, "Urutan", $this->data);
        $params->include_ins = parent::get_input_post("include_ins");
        $params->exp_method = parent::get_input_post("exp_method");
        $params->variant_seq = parent::get_input_post("variant_seq");
        $params->trx_fee_percent = parent::get_input_post("trx_fee_percent", true, NUMERIC_VALIDATOR, "Default Komisi", $this->data);
        $params->active = parent::get_input_post("active");
        $params->parent_name = parent::get_input_post("parent_name");
        $this->data[DATA_SELECTED][LIST_DATA][] = $params;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_product_category_admin->trans_begin();
                $this->M_product_category_admin->save_add($params);
                $this->M_product_category_admin->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_product_category_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_product_category_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_product_category_admin->trans_rollback();
            }
        }
    }

    protected function save_update() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->seq = parent::get_input_post("seq", true, NUMERIC_VALIDATOR, "Seq", $this->data);
        $params->name = parent::get_input_post("name", true, FILL_VALIDATOR, "Nama Kategori", $this->data);
        $params->parent_seq = parent::get_input_post("parent_seq");
        $params->level = parent::get_input_post("level");
        $params->order = parent::get_input_post("order", true, NUMERIC_VALIDATOR, "Urutan", $this->data);
        $params->include_ins = parent::get_input_post("include_ins");
        $params->exp_method = parent::get_input_post("exp_method");
        $params->variant_seq = parent::get_input_post("variant_seq");
        $params->trx_fee_percent = parent::get_input_post("trx_fee_percent", true, NUMERIC_VALIDATOR, "Default Komisi", $this->data);
        $params->active = parent::get_input_post("active");
        $params->parent_name = parent::get_input_post("parent_name");
        $this->data[DATA_SELECTED][LIST_DATA][] = $params;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_product_category_admin->trans_begin();
                $this->M_product_category_admin->save_update($params);
                $this->M_product_category_admin->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_product_category_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_product_category_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_product_category_admin->trans_rollback();
            }
        }
    }

    protected function save_delete() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = parent::get_input_post("key");
        try {
            $this->M_product_category_admin->trans_begin();
            $this->M_product_category_admin->save_delete($params);
            $this->M_product_category_admin->trans_commit();
            $output = array("error" => false, "message" => "");
            echo json_encode($output);
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_product_category_admin->trans_rollback();
            $output = array("error" => true, "message" => ERROR_DELETE);
            echo json_encode($output);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_product_category_admin->trans_rollback();
            $output = array("error" => true, "message" => ERROR_DELETE);
            echo json_encode($output);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_product_category_admin->trans_rollback();
            $output = array("error" => true, "message" => ERROR_DELETE);
            echo json_encode($output);
        }
        die();
    }

    function get_menu($datas, $parent = 0, $p_level = 0) {
        static $i = 1;
        $tab = str_repeat(" ", $i);
        if (isset($datas[$parent])) {
            $html = "<ul>";
            $i++;
            foreach ($datas[$parent] as $vals) {
                if ($vals->level == 1) {
                    $this->parent_level = $vals->seq;
                    $p_level = $vals->seq;
                }
                $child = $this->get_menu($datas, $vals->seq, $this->parent_level);
                $html .= "$tab";
                $html.='<li><a href="javascript:pilihmenu(\'' . $vals->seq . '~' . $vals->level . '~' . $vals->name . '~' . $p_level . '\')">' . $vals->name . '</a>';
                if ($child) {
                    $i++;
                    $html .= $child;
                    $html .= "$tab";
                }
                $html .= '</li>';
            }
            $html .= "$tab</ul>";
            return $html;
        } else {
            return false;
        }
    }

}

?>