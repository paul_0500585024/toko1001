<?php

require_once CONTROLLER_BASE_ADMIN;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of C_commission_product_partner
 *
 * @author Jaka
 */
class C_commission_product_partner extends controller_base_admin {

    private $data;
    private $hseq;

    public function __construct() {
        $this->data = parent::__construct("MST71001", "admin/master/commission_product_partner");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('admin/master/M_commission_product_partner');
        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_SAVE_DELETE, "save_delete");
        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        if (!$this->input->post()) {
            $this->data[FILTER] = 'admin/master/commission_product_partner_f.php';
            $this->load->view("admin/master/commission_product_partner", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/master/commission_product_partner", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/master/commission_product_partner", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    public function search() {
        $filter = new stdClass;
        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->period_from = parent::get_input_post("period_from") == '' ? '0000-00-00' : parent::get_input_post("period_from") ;
        $filter->period_to = parent::get_input_post("period_to") == '' ? '0000-00-00' : parent::get_input_post("period_to") ;
        try {
            $list_data = $this->M_commission_product_partner->get_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );
        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->seq,
                    "seq" => $data_row->seq,
                    "period_from" => parent::cdate($data_row->period_from),
                    "period_to" => parent::cdate($data_row->period_to),
                    "detail" => "<a href='" . base_url() . 'admin/master/commission_product_partner_detail/' . $data_row->seq . "'>Detil</a>",
                    "notes" => $data_row->notes,
                    "created_by" => $data_row->created_by,
                    "created_date" => parent::cdate($data_row->created_date, 1),
                    "modified_by" => $data_row->modified_by,
                    "modified_date" => parent::cdate($data_row->modified_date, 1));
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
    }

    protected function get_edit() {
        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = parent::get_input_post("key");
        try {
            $sel_data = $this->M_commission_product_partner->get_data($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_add() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->period_from = parent::get_input_post("period_from", true, DATE_VALIDATOR, 'period from', $this->data);
        $params->period_to = parent::get_input_post("period_to", true, DATE_VALIDATOR, 'period to', $this->data);
        $params->notes = parent::get_input_post("notes", true, FILL_VALIDATOR, "notes", $this->data);
        $this->data[DATA_SELECTED][LIST_DATA][] = $params;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_commission_product_partner->trans_begin();
                $this->M_commission_product_partner->save_add($params);
                $this->M_commission_product_partner->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_commission_product_partner->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_commission_product_partner->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_commission_product_partner->trans_rollback();
            }
        }
    }

    protected function save_update() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->seq = parent::get_input_post('seq');
        $params->period_from = parent::get_input_post("period_from", true, DATE_VALIDATOR, 'period from', $this->data);
        $params->period_to = parent::get_input_post("period_to", true, DATE_VALIDATOR, 'period to', $this->data);
        $params->notes = parent::get_input_post("notes", true, FILL_VALIDATOR, "notes", $this->data);
        $this->data[DATA_SELECTED][LIST_DATA][] = $params;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_commission_product_partner->trans_begin();
                $this->M_commission_product_partner->save_update($params);
                $this->M_commission_product_partner->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_commission_product_partner->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_commission_product_partner->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_commission_product_partner->trans_rollback();
            }
        }
    }

    protected function save_delete() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = parent::get_input_post("key");
        try {
            $this->M_commission_product_partner->trans_begin();
            $this->M_commission_product_partner->save_delete($params);
            $this->M_commission_product_partner->trans_commit();
            parent::set_json_success();
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_commission_product_partner->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_commission_product_partner->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_commission_product_partner->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        }
    }

}
