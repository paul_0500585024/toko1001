<?php

require_once CONTROLLER_BASE_ADMIN;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of c_merchant_log
 *
 * @author Jartono
 */
class C_merchant_log_admin extends controller_base_admin {

    private $data;

    public function __construct() {

        $this->data = parent::__construct("MST05002", "admin/master/merchant_log");
        $this->initialize();
    }

    private function initialize() {

        $this->load->model('admin/master/M_merchant_log_admin');
        $this->load->model('component/M_dropdown_list');
        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_SAVE_DELETE, "save_delete");
        parent::register_event($this->data, ACTION_APPROVE, "approve");
        parent::register_event($this->data, ACTION_REJECT, "reject");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        $this->data[MERCHANT_NAME] = $this->M_dropdown_list->get_dropdown_merchant();
        if (!$this->input->post()) {
            $this->data[FILTER] = 'admin/master/merchant_log_admin_f.php';
            $this->load->view("admin/master/merchant_log_admin", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/master/merchant_log_admin", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/master/merchant_log_admin", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    public function search() {

        $filter = new stdClass;
        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->merchant_seq = parent::get_input_post("merchant_info_seq");
        $filter->status = parent::get_input_post("status");

        try {
            $list_data = $this->M_merchant_log_admin->get_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );

        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->seq . "/" . $data_row->merchant_seq,
                    "merchant_name" => parent::cdef($data_row->merchant_name),
                    "seq" =>$data_row->seq,
                    "status" => parent::cstdes($data_row->status, STATUS_ANR),
                    "action" => "<a href='merchant_log_detail/" . $data_row->merchant_seq . "_" . $data_row->seq . "'>Detil</a>",
                    "auth_by" => $data_row->auth_by,
                    "auth_date" => parent::cdate($data_row->auth_date, 1),
                    "created_by" => $data_row->created_by,
                    "created_date" => parent::cdate($data_row->created_date, 1),
                    "modified_by" => $data_row->modified_by,
                    "modified_date" => parent::cdate($data_row->modified_date, 1));
                $output['aaData'][] = $row;
            }
        };

        echo json_encode($output);
    }

    protected function approve() {
        $key = explode("/", parent::get_input_post("key"));

        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->merchant_seq = $key[1];
        $selected->seq = $key[0];

        $sel_data = $this->M_merchant_log_admin->get_data($selected);

        try {
            if ($sel_data[0]->status == NEW_STATUS_CODE) {
                $this->M_merchant_log_admin->trans_begin();
                $this->M_merchant_log_admin->get_approve($selected);
                $this->M_merchant_log_admin->save_update($selected);
                $this->M_merchant_log_admin->save_info($selected);
                $this->M_merchant_log_admin->trans_commit();
                parent::set_json_success();
            } else {
                throw new Exception(ERROR_APPROVED);
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_merchant_log_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_APPROVED);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_merchant_log_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_APPROVED);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_merchant_log_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_APPROVED);
        }
    }

    protected function reject() {
        $key = explode("/", parent::get_input_post("key"));

        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = parent::get_input_post("key");
        $selected->merchant_seq = $key[1];
        $selected->seq = $key[0];

        $sel_data = $this->M_merchant_log_admin->get_data($selected);

        try {
            if ($sel_data[0]->status == NEW_STATUS_CODE) {
                $sel_data = $this->M_merchant_log_admin->get_reject($selected);
                parent::set_json_success();
            } else {
                throw new Exception(ERROR_REJECT);
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_merchant_log_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_REJECT);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_merchant_log_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_REJECT);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_merchant_log_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_REJECT);
        }
    }

}

?>
