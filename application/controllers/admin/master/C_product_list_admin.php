<?php

require_once CONTROLLER_BASE_ADMIN;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Product  *
 * @author Jartono
 */
class C_product_list_admin extends controller_base_admin {

    private $data;
    private $parent_level1;
    private $parent_level2;

    public function __construct() {
	$this->data = parent::__construct("MST02009", "admin/product_list");
	$this->initialize();
    }

    private function initialize() {
	$this->load->model('admin/master/M_product_list_admin');
	parent::register_event($this->data, ACTION_SEARCH, "search");
	parent::register_event($this->data, ACTION_EDIT, "get_edit");
	parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
	parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
	parent::register_event($this->data, ACTION_ADDITIONAL, "get_ajax");
	if ($this->data[DATA_INIT] === true) {
	    parent::fire_event($this->data);
	}
    }

    public function index() {
	$filter = new stdClass;
	$filter->user_id = parent::get_admin_user_id();
	$filter->ip_address = parent::get_ip_address();
	$filter->merchant_name = '';
	$merchant_list = $this->M_product_list_admin->get_merchant_by_name($filter);
	$this->data[MERCHANT_LIST] = $merchant_list;
	if (!$this->input->post()) {
	    $this->data['filter'] = 'admin/master/product_list_admin_f.php';
	    $this->load->view("admin/master/product_list_admin", $this->data);
	} else {
	    if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
		if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
		    $this->data[DATA_SUCCESS][SUCCESS] = true;
		    $this->data[DATA_AUTH][FORM_ACTION] = "";
		}
		$admin_info[SESSION_DATA] = $this->data;
		$this->session->set_userdata($admin_info);
		redirect(base_url("admin/product_list"));
	    }
	}
    }

    protected function get_ajax() {
	$tipe = parent::get_input_post("tipe");
	$idh = parent::get_input_post("idh");
	$filter = new stdClass;
	$filter->user_id = parent::get_admin_user_id();
	$filter->ip_address = parent::get_ip_address();

	switch ($tipe) {
	    case "category":
		$idh = parent::get_admin_user_seq();
		$output = array();
		$merchsq = array();
		$merchantlvl1 = $this->M_product_list_admin->get_merchant_lvl1($idh);
		$menulst = $this->M_product_list_admin->get_product_category();
		if (isset($merchantlvl1)) {
		    foreach ($merchantlvl1 as $datar) {
			$merchsq[] = $datar->category_l1_seq;
		    }
		    if (isset($menulst)) {
			foreach ($menulst as $data_row) {

			    if (in_array($data_row->seq, $merchsq)) {
				$output[$data_row->parent_seq][] = $data_row;
			    }
			    if (in_array($data_row->parent_seq, $merchsq)) {
				$output[$data_row->parent_seq][] = $data_row;
				$merchsq[] = $data_row->seq;
			    }
			}
		    }
		}
		die($this->get_treecategory($output, 0));
		break;
	    case "breadcrumb":
		$menulst = $this->M_product_list_admin->get_product_category_by_child($idh);
		die($this->get_breadcrumb($menulst));
		break;
	    case "attribute":
		$filter->idh = $idh;
		$atrval = parent::get_input_post("atrval");
		$menulst = $this->M_product_list_admin->get_attribute_by_category($filter);
//		die(print_r($filter));
		die($this->get_atribute($menulst, $atrval));
		break;
	    case "variant":
		$idh = explode(",", $idh);
		$filter->idh = $idh[1];
		$menulst = $this->M_product_list_admin->get_variant_by_category($filter);
		die($this->get_varian($menulst));
		break;
	}
    }

    function get_varian($idvar) {
	$data = '<div class ="form-group">
	    <label class ="col-md-5">';
	$i = 0;
	if ($idvar) {
	    foreach ($idvar as $data_row) {
		$i++;
		if ($i == 1)
		    $data.=$data_row->display_name . '</label><div class ="col-md-7 input-group"><select class="form-control" name="varianval" id="varianval">';
		$data.="<option value=" . $data_row->seq . ">" . $data_row->value . "</option>";
	    }
	    $data.='</select>
	    <span class="input-group-addon"></span></div></div>';
	}else {
	    $data = 'Data varian tidak ada';
	}
	return $data;
    }

    function get_breadcrumb($idcat) {
	$data = '<h4><span class="label label-primary">' . $idcat[0]->path . '<input type="hidden" name="alcat" id="alcat" value="' . $idcat[0]->idpath . '"></span></h4>';
	return $data;
    }

    function get_atribute($idattribute, $atrval = '') {
	$data = "<table class='table table-striped'>";
	$row = "";
	$i = 0;
	$slected = "";
	$selected = new stdClass;
	$selected->user_id = parent::get_admin_user_id();
	$selected->ip_address = parent::get_ip_address();
	if ($idattribute) {
	    foreach ($idattribute as $data_row) {
		$data.="<tr><td>" . $data_row->name . "</td><td><select id='attribut' name='attribut[]' class='form-control'><option value=''></option>";
		$selected->seq = $data_row->seq;
		$sel_data = $this->M_product_list_admin->get_attribute_value($selected);
		if (isset($sel_data)) {
		    foreach ($sel_data as $data_rows) {
			if ($data_rows->seq != '') {
			    $slected = "";
			    $nilai = '{' . $data_rows->seq . '}';
			    $slected = strripos($atrval, $nilai);
			    if ($slected === false) {
				$slected = "";
			    } else {
				$slected = " selected";
			    }
			}
			$data.="<option value='" . $data_rows->seq . "'" . $slected . ">" . $data_rows->value . "</option>";
		    }
		}
		$data.="</select></td></tr>";
	    }
	}
	$data.="</table>";
	return $data;
    }

    function get_treecategory($datas, $parent = 0, $p_level1 = 0, $p_level2 = 0) {
	static $i = 1;
	if (isset($datas[$parent])) {
	    $html = "<ul type=disc>";
	    $i++;
	    foreach ($datas[$parent] as $vals) {
		$g = $i;
		if ($vals->level == 1) {
		    $this->parent_level1 = $vals->seq;
		    $p_level1 = $vals->seq;
		}
		if ($vals->level == 2) {
		    $this->parent_level2 = $vals->seq;
		    $p_level2 = $vals->seq;
		}
		$child = $this->get_treecategory($datas, $vals->seq, $this->parent_level1, $this->parent_level2);
		if ($i != $g) {
		    $html .= '<li><label for="folder' . $i . '"><b>' . $vals->name . '</b></label>';
		} else {
		    $html.='<li><input onclick="cekseq(' . $vals->seq . ')" type="radio" id="folder' . $vals->seq . '" class="radiobtn" name="catseq" value="' . $vals->seq . '~' . $p_level1 . '~' . $p_level2 . '" required /> ' . $vals->name;
		}
		if ($child) {
		    $i++;
		    $html .= $child;
		}
		$html .= '</li>';
	    }
	    $html .= "</ul>";
	    return $html;
	} else {
	    return false;
	}
    }

    public function search() {
	$filter = new stdClass;
	$filter->user_id = parent::get_admin_user_id();
	$filter->ip_address = parent::get_ip_address();
	$filter->start = parent::get_input_post("start");
	$filter->length = parent::get_input_post("length");
	$filter->order = parent::get_input_post("order");
	$filter->column = parent::get_input_post("column");
	$filter->name = parent::get_input_post("name");
	$filter->description = parent::get_input_post("description");
	$filter->merchant_seq = parent::get_input_post("merchant_info_seq");
	$filter->merchant_name = "";
	$filter->status = parent::get_input_post("status");
	;

	try {
	    $list_data = $this->M_product_list_admin->get_list($filter);
	    parent::set_list_data($this->data, $list_data);
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	}
	$output = array(
	    "sEcho" => parent::get_input_post("draw"),
	    "iTotalRecords" => $list_data[0][0]->total_rec,
	    "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
	    "aaData" => array()
	);
	if (isset($list_data[1])) {
	    foreach ($list_data[1] as $data_row) {
		$row = array("DT_RowId" => $data_row->seq,
		    "merchant_name" => parent::cdef($data_row->merchant_name),
		    "merchant_seq" => $data_row->merchant_seq,
		    "seq" => $data_row->seq,
		    "description" => parent::cdef($data_row->description),
		    "name" => parent::cdef($data_row->name),
		    "status" => parent::cstdes($data_row->status, STATUS_LC),
		    "created_by" => $data_row->created_by,
		    "created_date" => parent::cdate($data_row->created_date, 1),
		    "modified_by" => $data_row->modified_by,
		    "modified_date" => parent::cdate($data_row->modified_date, 1)
		);
		$output['aaData'][] = $row;
	    }
	};
	echo json_encode($output);
    }

    protected function get_edit() {
	$selected = new stdClass();
	$selected->user_id = parent::get_admin_user_id();
	$selected->ip_address = parent::get_ip_address();
	$selected->seq = parent::get_input_post("key");
	try {
	    $sel_data = $this->M_product_list_admin->get_data($selected);
	    $sel_data1 = $this->M_product_list_admin->get_product_attribute_new_by_product_seq($selected);
	    $sel_data2 = $this->M_product_list_admin->get_product_spec_new_by_product_seq($selected);
	    $sel_data3 = $this->M_product_list_admin->get_product_variant_new_by_product_seq($selected);
	    if (isset($sel_data)) {
		parent::set_data($this->data, $sel_data);
		$this->data[DATA_SELECTED][LIST_DATA][] = $sel_data1;
		$this->data[DATA_SELECTED][LIST_DATA][] = $sel_data2;
		$this->data[DATA_SELECTED][LIST_DATA][] = $sel_data3;
	    }
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	}
    }

}

?>