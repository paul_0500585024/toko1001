<?php

require_once CONTROLLER_BASE_ADMIN;

class C_coupon_admin_trx extends controller_base_admin {

    private $data;
    private $hseq;

    public function __construct() {
        $this->data = parent::__construct("CON05009", "admin/master/coupon_transaction");
        $this->initialize();
    }

    private function initialize() {

        $this->hseq = $this->uri->segment(4);
        $this->load->model('admin/master/M_coupon_admin');
        parent::register_event($this->data, ACTION_SEARCH, "search");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        if (!$this->input->post()) {
            if ($this->data[DATA_AUTH][FORM_ACTION] == "") {
                $this->get_data_header();
            }

//            $this->data[FILTER] = 'admin/master/coupon_admin_detail_f.php';
            $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
            $this->load->view("admin/master/coupon_admin_trx", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/master/coupon_admin_trx", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/master/coupon_admin_trx", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq));
                }
            }
        }
    }

    public function search() {

        $filter = new stdClass;
        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->hseq = $this->hseq;

        try {
            $list_data = $this->M_coupon_admin->get_list_trx($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );
        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->seq,
                    "coupon_seq" => $data_row->coupon_seq,
                    "seq" => $data_row->seq,
                    "trx_no" => "<a href='" . base_url() . 'admin/transaction/order_detail/' . $data_row->trx_no . "'>" . $data_row->trx_no . "</a>",
                    "created_by" => $data_row->created_by,
                    "created_date" => parent::cdate($data_row->created_date, 1)
                );

                $output['aaData'][] = $row;
            }
        };

        echo json_encode($output);
    }

    protected function get_data_header() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = $this->hseq;

        try {
            $sel_data = $this->M_coupon_admin->get_data($params);
            if (isset($sel_data)) {
                parent::set_data_header($this->data, $sel_data);
            } else {
                $this->load->view("errors/html/error_404");
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_coupon_admin->trans_rollback();
            $this->load->view("errors/html/error_404");
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_coupon_admin->trans_rollback();
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_coupon_admin->trans_rollback();
        }
    }

}

?>
