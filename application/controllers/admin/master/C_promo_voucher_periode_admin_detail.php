<?php

require_once CONTROLLER_BASE_ADMIN;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Ekspedisi Service
 *
 * @author Jartono
 */
class C_promo_voucher_periode_admin_detail extends controller_base_admin {

    private $data;
    private $hseq;

    public function __construct() {
        $this->data = parent::__construct("CON05004", "admin/master/promo_voucher_periode_detail");
        $this->initialize();
    }

    private function initialize() {

        $this->hseq = $this->uri->segment(4);
        $this->load->model('admin/master/M_promo_voucher_periode_admin');
        $this->load->model('merchant/transaction/M_merchant_delivery');
        $this->load->model('component/M_dropdown_list');
        $this->load->model('component/M_voucher');
        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_SAVE_DELETE, "save_delete");
        parent::register_event($this->data, ACTION_ADDITIONAL, "save_upload");
        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {

        if (!$this->input->post()) {
            if ($this->data[DATA_AUTH][FORM_ACTION] == "") {
                $this->get_data_header();
            }
            $this->data[FILTER] = 'admin/master/promo_voucher_periode_admin_detail_f.php';
            $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
            $this->load->view("admin/master/promo_voucher_periode_admin_detail", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/master/promo_voucher_periode_admin_detail", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->data[MEMBER_NAME] = $this->M_dropdown_list->get_dropdown_member();
                        $this->load->view("admin/master/promo_voucher_periode_admin_detail", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_ADDITIONAL) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_SEARCH;
                        $this->data[MEMBER_NAME] = $this->M_dropdown_list->get_dropdown_member();
                        $this->load->view("admin/master/promo_voucher_periode_admin_detail", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_ADDITIONAL) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq));
                }
            }
        }
    }

    public function search() {

        $time = explode(";", parent::get_input_post("range_2"));
        $filter = new stdClass;
        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->hseq = $this->hseq;
        $filter->pFrom = $time[0];
        $filter->pTo = $time[1];

        try {
            $list_data = $this->M_promo_voucher_periode_admin->get_list_detail($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );
        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->seq,
                    "code" => parent::cdef($data_row->code),
                    "nominal" => parent::cnum($data_row->nominal),
                    "active_date" => parent::cdate($data_row->active_date),
                    "exp_date" => parent::cdate($data_row->exp_date),
                    "trx_use_amt" => parent::cnum($data_row->trx_use_amt),
                    "member_name" => parent::cdef($data_row->member_name),
                    "trx_no" => $data_row->trx_no);
                ;

                $output['aaData'][] = $row;
            }
        };

        echo json_encode($output);
    }

    protected function get_edit() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = parent::get_input_post("key");

        try {
            $sel_data = $this->M_promo_voucher_periode_admin->get_data_detail($params);
            $this->data[MEMBER_NAME] = $this->M_dropdown_list->get_dropdown_member();
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_promo_voucher_periode_admin->trans_rollback();
        }
    }

    protected function get_data_header() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = $this->hseq;

        try {
            $sel_data = $this->M_promo_voucher_periode_admin->get_data($params);
            if (isset($sel_data)) {
                parent::set_data_header($this->data, $sel_data);
            } else {
                $this->load->view("errors/html/error_404");
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_promo_voucher_periode_admin->trans_rollback();
            $this->load->view("errors/html/error_404");
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_promo_voucher_periode_admin->trans_rollback();
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_promo_voucher_periode_admin->trans_rollback();
        }
    }

    protected function save_update() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = parent::get_input_post("seq");
        $params->seq = parent::get_input_post("seq");
        $params->member_seq = parent::get_input_post("member_seq", true, FILL_VALIDATOR, "Member", $this->data);
        $params->active_date = date('d-M-Y');
        $params->exp_date = date('d-M-Y', strtotime('+' . parent::get_input_post("exp_days") . 'days', strtotime(date('d-M-Y'))));

        $get_data = $this->M_promo_voucher_periode_admin->get_data_detail($params);
        $params->voucher_seq = parent::get_input_post("seq");
        $params->exp_days = parent::get_input_post("exp_days");
        $params->code = parent::get_input_post("code");
        $params->nominal = parent::get_input_post("nominal");
        $params->trx_use_amt = parent::get_input_post("trx_use_amt");
        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                if ($get_data[0]->trx_no == '') {
                    $this->M_promo_voucher_periode_admin->trans_begin();
                    $this->M_promo_voucher_periode_admin->save_update_detail($params);
                    $this->M_promo_voucher_periode_admin->trans_commit();
                } else {
                    throw new Exception(ERROR_VOUCHER_WAS_USED);
                }
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_promo_voucher_periode_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_promo_voucher_periode_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_promo_voucher_periode_admin->trans_rollback();
            }
        }
    }

    protected function save_upload() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->promo_seq = $this->hseq;

        try {
            $file = parent::upload_file(UPLOADID, UPLOADPATH, FILENAME, CSV_FILE, $this->data);
            if ($file->error === false) {
                $this->load->library(CSV);
                $result = $this->csv_reader->parse_file($file->url_file . ".csv");
                $this->M_voucher->trans_begin();
                foreach ($result as $field) {
                    $params->email = $field["email"];
                    $this->M_voucher->save_update_voucher_member_upload($params);
                }
                $this->M_voucher->trans_commit();
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_expedition_city_admin->trans_rollback();
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_expedition_city_admin->trans_rollback();
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_expedition_city_admin->trans_rollback();
        }
    }

}
?>