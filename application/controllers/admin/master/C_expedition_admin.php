<?php

require_once CONTROLLER_BASE_ADMIN;

class C_expedition_admin extends controller_base_admin {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("MST03001", "admin/master/expedition");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('admin/master/M_expedition_admin');
        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_SAVE_DELETE, "save_delete");
        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        if (!$this->input->post()) {
            $this->data[FILTER] = 'admin/master/expedition_admin_f.php';
            $this->load->view("admin/master/expedition_admin", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/master/expedition_admin", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/master/expedition_admin", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url("admin/master/expedition"));
                }
            }
        }
    }

    public function search() {
        $filter = new stdClass;
        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->code = parent::get_input_post("code");
        $filter->name = parent::get_input_post("name");
        $filter->active = parent::get_input_post("active");
        try {
            $list_data = $this->M_expedition_admin->get_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );
        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {

                $row = array("DT_RowId" => $data_row->seq,
                    "code" => parent::cdef($data_row->code),
                    "name" => parent::cdef($data_row->name),
                    "active" => parent::cstat($data_row->active),
                    "created_by" => $data_row->created_by,
                    "created_date" => parent::cdate($data_row->created_date, 1),
                    "modified_by" => $data_row->modified_by,
                    "modified_date" => parent::cdate($data_row->modified_date, 1),
                    "detail" => "<a href='expedition_detail/" . $data_row->seq . "'>Paket</a>",
                    "awb" => "<a href='expedition_awb/" . $data_row->seq . "'>Resi</a>");

                if ($data_row->awb_method == "A") {
                    $row["awb"] = "";
                }
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
        die();
    }

    protected function get_edit() {
        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = parent::get_input_post("key");
        try {
            $sel_data = $this->M_expedition_admin->get_data($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_add() {
        $logo_img = parent::upload_file("nlogo_img", EXPEDITION_UPLOAD_IMAGE, "ex_" . parent::get_input_post("code"), IMAGE_TYPE, $this->data, IMAGE_DIMENSION_EXPEDITION, "Ekspedisi ");
        
        
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->code = parent::get_input_post("code", true, FILL_VALIDATOR, "Kode Ekspedisi", $this->data);
        $params->name = parent::get_input_post("name", true, FILL_VALIDATOR, "Nama Ekspedisi", $this->data);
        $params->logo_img = $logo_img->file_name;
        $params->awb_method = parent::get_input_post("awb_method");
        $params->ins_rate_percent = parent::get_input_post("ins_rate_percent", true, PERCENTAGE_VALIDATOR, "Premi Ekspedisi", $this->data);
        $params->notes = parent::get_input_post("notes");
        $params->active = parent::get_input_post("active");
        
        $this->data[DATA_SELECTED][LIST_DATA][] = $params;
        
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_expedition_admin->trans_begin();
                $this->M_expedition_admin->save_add($params);
                $this->M_expedition_admin->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_expedition_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_expedition_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_expedition_admin->trans_rollback();
            }
        }
    }

    protected function save_update() {
        $logo_img = parent::upload_file("nlogo_img", EXPEDITION_UPLOAD_IMAGE, "ex_" . parent::get_input_post("code"), IMAGE_TYPE, $this->data, IMAGE_DIMENSION_EXPEDITION, "Ekspedisi ");
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->seq = parent::get_input_post("seq", true, NUMERIC_VALIDATOR, "Seq", $this->data);
        $params->code = parent::get_input_post("code", true, FILL_VALIDATOR, "Kode Ekspedisi", $this->data);
        $params->name = parent::get_input_post("name", true, FILL_VALIDATOR, "Nama Ekspedisi", $this->data);
        $params->old_img =  parent::get_input_post("old_img");
        $params->logo_img = $logo_img->error == true ? $params->old_img : $logo_img->file_name;
        $params->awb_method = parent::get_input_post("awb_method");
        $params->ins_rate_percent = parent::get_input_post("ins_rate_percent", true, PERCENTAGE_VALIDATOR, "Premi Ekspedisi", $this->data);
        $params->notes = parent::get_input_post("notes");
        $params->active = parent::get_input_post("active");
        
        $this->data[DATA_SELECTED][LIST_DATA][] = $params;
        
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_expedition_admin->trans_begin();
                $this->M_expedition_admin->save_update($params);
                $this->M_expedition_admin->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_expedition_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_expedition_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_expedition_admin->trans_rollback();
            }
        }
    }

    protected function save_delete() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = parent::get_input_post("key");
        $sel_data = $this->M_expedition_admin->get_data($params);
        $gambar = '';
        if (isset($sel_data)) {
            $gambar = $sel_data[0]->logo_img;
        }
        try {
            $this->M_expedition_admin->trans_begin();
            $this->M_expedition_admin->save_delete($params);
            $this->M_expedition_admin->trans_commit();
            if ($gambar != '')
                unlink(EXPEDITION_UPLOAD_IMAGE . $gambar);
            parent::set_json_success();
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_expedition_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_expedition_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_expedition_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        }
    }

}

?>