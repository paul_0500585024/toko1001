<?php

require_once CONTROLLER_BASE_ADMIN;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Merchant Trx Fee
 *
 * @author Jartono
 */
class C_merchant_admin_detail extends controller_base_admin {

    private $data;
    private $hseq;

    public function __construct() {
	$this->data = parent::__construct("MST05005", "admin/master/merchant_detail");
	$this->initialize();
    }

    private function initialize() {
	$this->hseq = $this->uri->segment(4);
	$this->load->model('admin/master/M_merchant_admin');
	parent::register_event($this->data, ACTION_EDIT, "get_edit");
	parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        
	if ($this->data[DATA_INIT] === true) {
	    parent::fire_event($this->data);
	}
    }

    public function index() {
	if (!$this->input->post()) {
	    if ($this->data[DATA_AUTH][FORM_ACTION] == "") {
		$this->get_edit();
		$this->get_data_header();
	    }
	    $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
	    $this->load->view("admin/master/merchant_admin_detail", $this->data);
	} else {
	    if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
		if ($this->data[DATA_ERROR][ERROR] === true) {
		    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
			$this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
			$this->load->view("admin/master/merchant_admin_detail", $this->data);
		    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
			$this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
			$this->load->view("admin/master/merchant_admin_detail", $this->data);
		    }
		} else {
		    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
			$this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
		    }
		    $admin_info[SESSION_DATA] = $this->data;
		    $this->session->set_userdata($admin_info);
		    redirect(base_url("admin/master/merchant_detail/" . $this->hseq));
		}
	    }
	}
    }

    protected function get_data_header() {
	$params = new stdClass();
	$params->user_id = parent::get_admin_user_id();
	$params->ip_address = parent::get_ip_address();
	$params->seq = $this->hseq;
	try {
	    $sel_data = $this->M_merchant_admin->get_data($params);
	    if (isset($sel_data)) {
		parent::set_data($this->data, $sel_data);
	    }
	} catch (BusisnessException $ex) {
	    parent::set_error($this->data, $ex);
	} catch (TechnicalException $ex) {
	    parent::set_error($this->data, $ex);
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	}
    }

    protected function get_edit() {
	$selected = new stdClass();
	$selected->user_id = parent::get_admin_user_id();
	$selected->ip_address = parent::get_ip_address();
	$selected->seq = $this->hseq;
	try {
	    $this->data[TREE_MENU] = $this->M_merchant_admin->get_data_detail($selected);
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	}
    }

    protected function save_update() {
	$params = new stdClass();
	$params->user_id = parent::get_admin_user_id();
	$params->ip_address = parent::get_ip_address();
	$params->seq = $this->input->post("seq");
	$params->hseq = $this->input->post("hseq");
	$params->lvlcat = $this->input->post("lvlcat");
	$params->mfee = $this->input->post("mfee");
	$this->data[DATA_SELECTED][LIST_DATA][] = $params;
	if ($this->data[DATA_ERROR][ERROR] === false) {
	    try {
		$this->M_merchant_admin->trans_begin();
		$this->M_merchant_admin->save_update_detail($params);
		$this->M_merchant_admin->trans_commit();
	    } catch (BusisnessException $ex) {
		parent::set_error($this->data, $ex);
		$this->M_merchant_admin->trans_rollback();
	    } catch (TechnicalException $ex) {
		parent::set_error($this->data, $ex);
		$this->M_merchant_admin->trans_rollback();
	    } catch (Exception $ex) {
		parent::set_error($this->data, $ex);
		$this->M_merchant_admin->trans_rollback();
	    }
	}
    }

}

?>