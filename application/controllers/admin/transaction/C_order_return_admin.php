<?php

require_once CONTROLLER_BASE_ADMIN;

class C_order_return_admin extends controller_base_admin {

    private $data;

    public function __construct() {

        $this->data = parent::__construct("TRX07001", "admin/transaction/order_return_admin");        
        $this->initialize();
    }

    private function initialize() {

        $this->load->model('component/M_dropdown_list');
        $this->load->model('admin/transaction/M_order_return_admin');

        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {        
        $this->data[MEMBER_EMAIL] = $this->M_dropdown_list->get_dropdown_member_email();

        if (!$this->input->post()) {
            if ($this->session->tempdata(DATA_MESSAGE_SUCCESS) == true) {
                $this->data[DATA_SUCCESS][SUCCESS] = true;
                $this->session->unset_tempdata(DATA_MESSAGE_SUCCESS);
            }
            $this->data[FILTER] = 'admin/transaction/order_return_admin_f.php';
            $this->load->view("admin/transaction/order_return_admin", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/transaction/order_return_admin", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/transaction/order_return_admin", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }

                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    protected function search() {
        $return_date_between = explode(' - ', parent::get_input_post("date_between"));
        $filter = new stdClass;
        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->return_no = parent::get_input_post("return_no");
        $filter->member_email = parent::get_input_post("member_email");
        $filter->date_search = isset($return_date_between[1]) ? "1" : "0";
        $filter->return_date_from = $return_date_between[0];
        $filter->return_date_to = isset($return_date_between[1]) ? $return_date_between[1] : "";
        $filter->return_status = parent::get_input_post("return_status");
       
        try {
            $list_data = $this->M_order_return_admin->get_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );

        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                if ($data_row->return_status == NEW_STATUS_CODE) {
                    if ($data_row->shipment_status == SHIP_FROM_MEMBER) {
                        $action = "<a class='btn btn-xs btn-block btn-success' href='order_return_admin_received/" . $data_row->return_no . "'>Terima</a>";
                    } else if ($data_row->shipment_status == RECEIVED_BY_ADMIN) {
                        $action = "<a class='btn btn-xs btn-block btn-danger' href='ship_to_member/" . $data_row->return_no . "'>Member &nbsp;</a> 
                        <a class='btn btn-xs btn-block btn-warning' href='ship_to_merchant/" . $data_row->return_no . "'> Merchant</a>";
                    } else if ($data_row->shipment_status == SHIP_TO_MERCHANT) {
                        $action = "<a class='btn btn-xs btn-block btn-warning' href='ship_to_merchant/" . $data_row->return_no . "'> Merchant</a>";
                    } else if ($data_row->shipment_status == SHIP_TO_MEMBER) {
                        $action = "<a class='btn btn-xs btn-block btn-danger' href='ship_to_member/" . $data_row->return_no . "'>Member</a>";
                    } else if ($data_row->shipment_status == RECEIVED_BY_MERCHANT) {
                        $action = "";
                    } else if ($data_row->shipment_status == RECEIVED_BY_MEMBER) {
                        $action = "";
                    } else {
                        $action = "";
                    }
                } else if ($data_row->shipment_status == SHIP_TO_MEMBER && $data_row->return_status == REJECT_STATUS_CODE) {
                    $action = "<a class='btn btn-xs btn-block btn-danger' href='ship_to_member/" . $data_row->return_no . "'>Member</a>";
                } else {
                    $action = "";
                }

                $row = array("DT_RowId" => $data_row->seq,
                    "return_no" => $data_row->return_no,
                    "return_date" => parent::cdate($data_row->return_date),
                    "awb_member_no" => parent::cdef($data_row->awb_member_no),
                    "shipment_status" => parent::cstdes($data_row->shipment_status, STATUS_SHIPMENT),
                    "order_no" => $data_row->order_no,
                    "qty" => $data_row->qty,
                    "return_status" => parent::cstdes($data_row->return_status, STATUS_RETURN),
                    "member_email" => $data_row->member_email,
                    "product_name" => $data_row->product_name,
                    "action" => $action);
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
    }

    protected function get_edit() {
        $key = parent::get_input_post("key");

        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->seq = $key;

        try {
            $sel_data = $this->M_order_return_admin->get_data($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

}

?>