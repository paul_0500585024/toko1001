<?php

require_once CONTROLLER_BASE_ADMIN;

class C_order_return_admin_ship_to_merchant extends controller_base_admin {

    private $hseq;
    private $data;

    public function __construct() {
        $segments = explode('/', trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/'));
        $this->data = parent::__construct("TRX07003", "admin/transaction/ship_to_merchant", true, $segments[3]);
        $this->initialize();
    }

    private function initialize() {

        $this->hseq = $this->uri->segment(4);
        $this->load->model('component/M_dropdown_list');
        $this->load->model('admin/transaction/M_order_return_admin');

        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {

        $this->get_data_header();

        if (!$this->input->post()) {
            $this->data[EXPEDITION_NAME] = $this->M_dropdown_list->get_dropdown_expedition();
            $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
            $this->load->view("admin/transaction/order_return_admin_ship_to_merchant", $this->data);
        } else {
            if ($this->data[DATA_SUCCESS][SUCCESS] == true) {
                $this->session->set_tempdata(DATA_MESSAGE_SUCCESS, true);
                redirect(base_url("admin/transaction/order_return_admin"));
            } else if ($this->data[DATA_ERROR][ERROR] == true) {
                $this->data[DATA_ERROR][ERROR] = true;
                $admin_info[SESSION_DATA] = $this->data;
                $this->session->set_userdata($admin_info);
                redirect(base_url($this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq));
            }
        }
    }

    protected function get_data_header() {

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = $this->hseq;

        try {
            $sel_data = $this->M_order_return_admin->get_data_order_return($params);
            if (isset($sel_data)) {
                parent::set_data_header($this->data, $sel_data);
                parent::set_data($this->data, $sel_data);
            } else {
                $this->load->view("errors/html/error_404");
            }
        } catch (BusisnessException $ex) {
            $this->load->view("errors/html/error_404");
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_update() {

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->return_no = parent::get_input_post('return_no');
        $params->exp_seq_to_merchant = parent::get_input_post('expedition_seq');
        $params->awb_admin_no = parent::get_input_post('awb_admin_no');
        $params->review_admin = parent::get_input_post('review_admin');
        $params->shipment_status = parent::get_input_post('shipment_status');
        $params->status = SHIP_TO_MERCHANT;

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        try {
            if ($params->shipment_status == RECEIVED_BY_ADMIN || $params->shipment_status == SHIP_TO_MERCHANT) {
                $this->M_order_return_admin->trans_begin();
                $list_data = $this->M_order_return_admin->save_update_ship_to_merchant($params);

                if ($list_data[0][0]->Valid == '1') {
                    if ($list_data[0][0]->ValidDate == '1') {
                        $this->M_order_return_admin->trans_commit();
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                    } else {
                        throw new Exception(ERROR_RECEIVED_DATE);
                    }
                } else {
                    throw new Exception(ERROR_UPDATE);
                }
            } else {
                throw new Exception(ERROR_UPDATE);
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_order_return_admin->trans_rollback();
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_order_return_admin->trans_rollback();
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_order_return_admin->trans_rollback();
        }
    }

}

?>