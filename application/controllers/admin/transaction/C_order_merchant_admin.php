<?php

require_once CONTROLLER_BASE_ADMIN;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Promo Free Fee
 *
 * @author Jartono
 */
class C_order_merchant_admin extends controller_base_admin {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("TRX01003", "admin/transaction/order_merchant");
        $this->initialize();
    }

    private function initialize() {

        $this->load->model('admin/transaction/M_order_merchant_admin');
        $this->load->model('component/M_dropdown_list');
        $this->load->model('merchant/master/M_forgot_password_merchant');
        $this->load->model('admin/transaction/M_order_admin');
        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_SAVE_DELETE, "save_delete");
        parent::register_event($this->data, ACTION_APPROVE, "approve");
        parent::register_event($this->data, ACTION_REJECT, "reject");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        $this->data[MERCHANT_NAME] = $this->M_dropdown_list->get_dropdown_merchant();
        $this->data[PAYMENT_NAME] = $this->M_dropdown_list->get_dropdown_payment_gateway_method();
        if (!$this->input->post()) {
            $this->data[FILTER] = 'admin/transaction/order_merchant_admin_f.php';
            $this->load->view("admin/transaction/order_merchant_admin", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/transaction/order_merchant_admin", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/transaction/order_merchant_admin", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    public function search() {

        $filter = new stdClass;

        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->order_no = parent::get_input_post("order_no");
        $filter->pSDate = parent::get_input_post("tanggal1");
        $filter->pEDate = parent::get_input_post("tanggal2");
        $filter->pg_method_seq = parent::get_input_post("pg_method_seq");
        $filter->merchant_info_seq = parent::get_input_post("merchant_info_seq");
        $filter->order_status = parent::get_input_post("order_status");
        $filter->paymnet_status = parent::get_input_post("paymnet_status");
        try {
            $list_data = $this->M_order_merchant_admin->get_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );
        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->order_seq . "/" . $data_row->merchant_info_seq,
                    "tgl_order" => parent::cdate($data_row->tgl_order),
                    "no_order" => parent::cdef($data_row->no_order),
                    "merchant_name" => $data_row->merchant_name,
                    "payment_name" => $data_row->payment_name,
                    "awb_no" => parent::cdef($data_row->awb_no),
                    "ship_date" => parent::cdate($data_row->ship_date),
                    "receiver_address" => parent::cdef($data_row->receiver_address),
                    "payment_status" => parent::cstdes($data_row->payment_status, STATUS_PAYMENT),
                    "order_status" => parent::cstdes($data_row->order_status, STATUS_ORDER));

                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
    }

    protected function get_edit() {
        $a = explode("/", parent::get_input_post("key"));

        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->order_seq = $a[0];
        $selected->merchant_info_seq = $a[1];

        $sel_data1 = $this->M_order_merchant_admin->get_produk($selected);
        try {
            $sel_data = $this->M_order_merchant_admin->get_data($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
                $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data1;
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_update() {
        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->order_seq = parent::get_input_post("order_seq");
        $selected->merchant_info_seq = parent::get_input_post("merchant_info_seq");
        $selected->order_status = parent::get_input_post("order_status");
        $selected->received_date = parent::get_input_post("received_date", TRUE, DATE_VALIDATOR, "", $this->data);
        $selected->ship_note_file = parent::get_input_post("ship_note_file");

        $sel_data = $this->M_order_merchant_admin->get_data($selected);
        $merchant_name = $sel_data[0]->merchant_name;
        $merchant_email = $sel_data[0]->merchant_email;
        $raddress = $sel_data[0]->receiver_address;
        $order_no = $sel_data[0]->order_no;
        $order_date = $sel_data[0]->order_date;
        $awb_no = $sel_data[0]->awb_no;
        $member_name = $sel_data[0]->member_name;
        $esp_name = $sel_data[0]->esp_name;
        $order_status = $sel_data[0]->order_status;
        $payment_status = $sel_data[0]->payment_status;

        $sel_data = $this->M_order_merchant_admin->get_produk($selected);
        $order_content = "<table border='1'>
                           <thead>
                            <tr>
                                <th>Produk</th>
                                <th>Tipe Produk</th>
                                <th>Qty</th>
                            </tr>
                           </thead><tbody>";

        foreach ($sel_data as $product) {
            $order_content = $order_content . "<tr>
                                               <td>" . $product->product_name . "</td>
                                               <td>" . $product->variant_name . "</td>
                                               <td>" . $product->qty . "</td></tr>";
        }

        $order_content = $order_content . "</tbody>
                                          
                                          </table>";

        $this->data[DATA_SELECTED][LIST_DATA][] = $selected;
        $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data = $this->M_order_merchant_admin->get_produk($selected);
        $selected->order_no = $order_no;
        $selected->order_date = $order_date;
        $selected->member_name = $member_name;
        $selected->receiver_address = $raddress;
        $selected->esp_name = $esp_name;
        $selected->awb_no = $awb_no;

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                if (strtotime(parent::get_input_post("received_date")) < $order_date) {
                    $selected->order_status = ORDER_READY_TO_SHIP_STATUS_CODE;
                    throw new Exception(ERROR_RECEIVE_DATE);
                } else {
                    if ($order_status == ORDER_SHIPPING_STATUS_CODE && $payment_status == ORDER_PREORDER_STATUS_CODE) {
                        $this->M_order_merchant_admin->trans_begin();
                        $this->M_order_merchant_admin->save_update($selected);
                        $params = new stdClass();
                        $params->user_id = parent::get_admin_user_id();
                        $params->ip_address = parent::get_ip_address();
                        $params->code = ORDER_SENT_CODE;
                        $params->ORDER_NO = $order_no;
                        $params->RECIPIENT_NAME = $merchant_name;
                        $params->ORDER_ITEMS = $order_content;
                        $params->RECIPIENT_DATE = $selected->received_date;
                        $params->to_email = $merchant_email;
                        parent::email_template($params);
                        $this->M_order_merchant_admin->trans_commit();
                    } else {
                        throw new Exception(ERROR_UPDATE);
                    }
                }
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_order_merchant_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_order_merchant_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_order_merchant_admin->trans_rollback();
            }
        }
    }

}

?>
