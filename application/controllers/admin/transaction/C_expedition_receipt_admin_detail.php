<?php

require_once CONTROLLER_BASE_ADMIN;

class C_expedition_receipt_admin_detail extends controller_base_admin {

    private $data;
    private $hseq;

    public function __construct() {
        $this->data = parent::__construct("TRX08002", "admin/transaction/expedition_receipt_detail");
        $this->initialize();
    }

    private function initialize() {

        $this->hseq = $this->uri->segment(4);
        $this->load->model('component/M_dropdown_list');
        $this->load->model('admin/transaction/M_expedition_receipt_admin');

        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_SAVE_DELETE, "save_delete");
        parent::register_event($this->data, ACTION_ADDITIONAL, "get_dropdown_merchant_by_order");
        parent::register_event($this->data, ACTION_APPROVE, "save_approve");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {

        $this->get_data_header();

        $this->data[ORDER_NO] = $this->get_dropdown_order_no_by_order();

        if (!$this->input->post()) {
            $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;

            $this->load->view("admin/transaction/expedition_receipt_admin_detail", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/transaction/expedition_receipt_admin_detail", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/transaction/expedition_receipt_admin_detail", $this->data);
                    }
                } else {

                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq));
                }
            }
        }
    }

    public function search() {

        $filter = new stdClass;
        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->hseq = $this->hseq;

        try {
            $list_data = $this->M_expedition_receipt_admin->get_list_detail($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );

        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->seq,
                    "awb_no" => parent::cdef($data_row->awb_no),
                    "awb_date" => parent::cdate($data_row->awb_date),
                    "destination" => parent::cdef($data_row->destination),
                    "ship_price" => parent::cnum($data_row->ship_price),
                    "ins_price" => parent::cnum($data_row->ins_price),
                    "total_price" => parent::cnum($data_row->total_price),
                    "status" => parent::cstdes($data_row->status, STATUS_VERIFIED),
                    "order_no" => parent::cdef($data_row->order_no),
                    "merchant_name" => parent::cdef($data_row->merchant_name)
                );
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
    }

    protected function get_data_header() {

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = $this->hseq;

        try {
            $sel_data = $this->M_expedition_receipt_admin->get_data($params);
            if (isset($sel_data)) {
                parent::set_data_header($this->data, $sel_data);
            } else {
                $this->load->view("errors/html/error_404");
            }
        } catch (BusisnessException $ex) {
            $this->load->view("errors/html/error_404");
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $this->expedition_seq = $sel_data[0]->expedition_seq;
    }

    protected function get_edit() {

        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->seq = parent::get_input_post("key");

        try {
            $sel_data = $this->M_expedition_receipt_admin->get_data_detail($selected);

            if (isset($sel_data)) {
                $this->data[ORDER_SEQ] = $sel_data[0]->order_seq;
                $this->data[MERCHANT_LIST] = $this->M_dropdown_list->get_dropdown_merchant_by_order($this->data[ORDER_SEQ]);
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_add() {

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->invoice_seq = $this->hseq;
        $params->awb_no = parent::get_input_post("awb_no");
        $params->awb_date = parent::get_input_post("awb_date");
        $params->destination = parent::get_input_post("destination");
        $params->ship_price = parent::get_input_post("ship_price");
        $params->ins_price = parent::get_input_post("ins_price");
        $params->order_seq = parent::get_input_post("order_seq");
        $params->merchant_seq = parent::get_input_post("merchant_seq");

        if ($params->order_seq == "") {
            $status = UNVERIFIED;
        } else {
            $status = VERIFIED;
        }

        $params->status = $status;

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        try {
            $invoice_status = $this->M_expedition_receipt_admin->get_invoice_status($params);
            if ($invoice_status[0]->status == OPEN_STATUS_CODE) {
                $awb = $this->M_expedition_receipt_admin->get_data_awb($params);
                if ($awb[0]->awb_no != $params->awb_no && $awb[0]->exp_seq != $invoice_status[0]->exp_seq) {
                    $this->M_expedition_receipt_admin->trans_begin();
                    $list_data = $this->M_expedition_receipt_admin->save_add_detail($params);
                    $params->awb_seq = $list_data[0]->new_seq;
                    $this->M_expedition_receipt_admin->order_merchant_update($params);
                    $this->M_expedition_receipt_admin->trans_commit();
                } else {
                    throw new Exception(ERROR_AWB_NO);
                }
            } else {
                throw new Exception(ERROR_STATUS_INVOICE);
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_expedition_receipt_admin->trans_rollback();
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_expedition_receipt_admin->trans_rollback();
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_expedition_receipt_admin->trans_rollback();
        }
    }

    protected function save_update() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->invoice_seq = $this->hseq;
        $params->seq = parent::get_input_post("seq");
        $params->awb_no = parent::get_input_post("awb_no");
        $params->awb_date = parent::get_input_post("awb_date");
        $params->destination = parent::get_input_post("destination");
        $params->ship_price = parent::get_input_post("ship_price");
        $params->ins_price = parent::get_input_post("ins_price");
        $params->order_seq = parent::get_input_post("order_seq");
        $params->merchant_seq = parent::get_input_post("merchant_seq");

        if ($params->order_seq == "") {
            $status = UNVERIFIED;
        } else {
            $status = VERIFIED;
        }
        $params->status = $status;

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;
        try {
            $invoice_status = $this->M_expedition_receipt_admin->get_invoice_status($params);
            if ($invoice_status[0]->status != VERIFIED) {
                $awb = $this->M_expedition_receipt_admin->get_data_awb($params);
                if ($awb[0]->awb_no != $params->awb_no) {
                    $this->M_expedition_receipt_admin->trans_begin();
                    $list_data = $this->M_expedition_receipt_admin->save_update_detail($params);
                    $params->old_order_seq = $list_data[0]->old_order_seq;
                    $params->old_merchant_info_seq = $list_data[0]->old_merchant_info_seq;
                    $this->M_expedition_receipt_admin->order_merchant_update_detail($params);
                    $this->M_expedition_receipt_admin->trans_commit();
                } else {
                    throw new Exception(ERROR_AWB_NO);
                }
            } else {
                throw new Exception(ERROR_STATUS_INVOICE);
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_expedition_receipt_admin->trans_rollback();
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_expedition_receipt_admin->trans_rollback();
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_expedition_receipt_admin->trans_rollback();
        }
    }

    protected function save_delete() {

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = parent::get_input_post("key");
        $params->invoice_seq = $this->hseq;

        try {
            $invoice_status = $this->M_expedition_receipt_admin->get_invoice_status($params);
            if ($invoice_status[0]->status != VERIFIED) {
                $this->M_expedition_receipt_admin->trans_begin();
                $this->M_expedition_receipt_admin->save_delete_detail($params);
                $this->M_expedition_receipt_admin->trans_commit();
                parent::set_json_success();
            } else {
                throw new Exception(ERROR_STATUS_INVOICE);
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_expedition_receipt_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_expedition_receipt_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_expedition_receipt_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        }
    }

    protected function get_dropdown_order_no_by_order() {

        $params = new stdClass();
        $params->exp_seq = $this->expedition_seq;

        try {
            return $list_data = $this->M_dropdown_list->get_dropdown_order_no_by_order_status($params);
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function get_dropdown_merchant_by_order() {
        $order_seq = parent::get_input_post('order_seq');

        try {
            $list_data = $this->M_dropdown_list->get_dropdown_merchant_by_order($order_seq);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        echo json_encode($list_data);
        die();
    }

    protected function save_approve() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->seq = parent::get_input_post('seq');
        $params->invoice_seq = $this->hseq;

        try {
            $list_data = $this->M_expedition_receipt_admin->get_check_order($params);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

}

?>