<?php

require_once CONTROLLER_BASE_ADMIN;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Invoicing Period Partner
 *
 * @author Jartono
 */
class C_invoicing_period_partner_admin extends controller_base_admin {

    private $data;
    private $hseq;

    public function __construct() {
        $this->data = parent::__construct("TRX10002", "admin/transaction/invoicing_period_partner");
        $this->initialize();
    }

    private function initialize() {
        $this->hseq = $this->uri->segment(4);
        $this->load->model('admin/transaction/M_invoicing_partner_admin');
        $this->load->model('component/M_email');
        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_ADDITIONAL, "get_additional");
        parent::fire_event($this->data);
    }

    public function index($pseq = '') {
        $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
        if (!$this->input->post()) {
            if ($this->data[DATA_AUTH][FORM_ACTION] == "") {
                $this->get_data_header();
            }
            $this->load->view("admin/transaction/invoicing_period_partner_admin", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/transaction/invoicing_period_partner_admin", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/transaction/invoicing_period_partner_admin", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        //$this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
                        redirect(base_url("admin/transaction/invoicing_period_partner/" . $this->hseq));
                    }
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_VIEW) {
                        $this->get_view();
                    }
                    $this->load->view("admin/transaction/invoicing_period_partner_admin", $this->data);
                }
            } else {
                
            }
        }
    }

    public function search() {
        $params = new stdClass;
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->start = parent::get_input_post("start");
        $params->length = parent::get_input_post("length");
        $params->order = parent::get_input_post("order");
        $params->column = parent::get_input_post("column");
        $params->seq = $this->hseq;
        try {
            $list_data = $this->M_invoicing_partner_admin->get_list_detail($params);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $output = array(
                "sEcho" => "",
                "iTotalRecords" => "0",
                "iTotalDisplayRecords" => "0",
                "aaData" => []
            );
            echo json_encode($output);
            die();
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );
        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
//                $link = "<a href='#' onclick='detail_order(".$data_row->collect_seq . '-' . $data_row->partner_seq.")'>Detil Order</a>";
                $link = "<form method='POST' action='" . base_url() . $this->data[DATA_AUTH][FORM_URL] . '/' . $params->seq . "' >";
                $link .= "<input type='hidden' name='key' value='" . $data_row->collect_seq . '-' . $data_row->partner_seq . "' />";
                $link .= "<input type='hidden' name='btnView' value='test' />";
                $link .= "<a href='#' onclick='detail_order(this)'>Detil Order</a>";
                $link .= "</form>";
                $row = array("DT_RowId" => $data_row->collect_seq . '-' . $data_row->partner_seq,
                    "partner_seq" => $data_row->partner_seq,
                    "name" => parent::cdef($data_row->name),
                    "total" => number_format($data_row->total),
                    "paid_date" => parent::cdate($data_row->paid_date),
                    "status" => parent::cstdes($data_row->status, STATUS_LOAN),
                    "detail_order" => $link,
                );
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
        die();
    }

    protected function get_edit() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $key = parent::get_input_post("key");
        $key = explode("-", $key);
        $params->seq = $key[0];
        $params->partner_seq = $key[1];
        try {

            $sel_data = $this->M_invoicing_partner_admin->get_data_detail($params);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function get_view() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();

        $key = parent::get_input_post("key");
        $key = explode("-", $key);
        $params->seq = $key[0];
        $params->partner_seq = $key[1];
        try {
            $sel_data = $this->M_invoicing_partner_admin->get_data_detail($params);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
                $sel_data1 = $this->M_invoicing_partner_admin->get_invoicing_component_by_partner($params);
                $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data1;
                $params->payment_code = PAYMENT_TYPE_AGENT;
                $sel_data2 = $this->M_invoicing_partner_admin->get_order_agent_by_loan($params);
                $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data2;
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function get_data_header() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->seq = $this->hseq;
        try {
            $sel_data = $this->M_invoicing_partner_admin->get_data($params);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_update() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->collect_seq = parent::get_input_post("collect_seq");
        $params->partner_seq = parent::get_input_post("partner_seq");
        $params->paid_date = parent::get_input_post("paid_date");
        $params->seq = $params->collect_seq;
        $sel_data = $this->M_invoicing_partner_admin->get_data_detail($params);
        if (isset($sel_data)) {
            $params->status = $sel_data[0]->status;
            $params->total = $sel_data[0]->total;
            $params->bank_name = $sel_data[0]->bank_name;
            $params->bank_branch_name = $sel_data[0]->bank_branch_name;
            $params->bank_acct_no = $sel_data[0]->bank_acct_no;
            $params->bank_acct_name = $sel_data[0]->bank_acct_name;
            $params->name = $sel_data[0]->name;
            $params->partner_seq = $sel_data[0]->partner_seq;
            if ($sel_data[0]->status != "D") {
                $this->data[DATA_ERROR][ERROR] = true;
                $this->data[DATA_ERROR][ERROR_MESSAGE][] = "ERROR : " . ERROR_UPDATE;
            }
        } else {
            $this->data[DATA_ERROR][ERROR] = true;
            $this->data[DATA_ERROR][ERROR_MESSAGE][] = "ERROR : " . ERROR_UPDATE;
        }
        $this->data[DATA_SELECTED][LIST_DATA][] = $params;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_invoicing_partner_admin->trans_begin();
                // update t_redeem_agent paid_date
                $this->M_invoicing_partner_admin->save_update_detail($params);

                $params->status_new = "C";
                $params->status_old = "P";
                $this->M_invoicing_partner_admin->status_update_agent_period($params);

                // cek apa semua sudah dibayar
                /* $statusall = $this->get_status_redeem($params->redeem_seq);
                  if ($statusall == "ALL") {
                  // update t_redeem_period status semua sudah update
                  $params->status_new = "C";
                  $params->status_old = "P";
                  $this->M_invoicing_partner_admin->status_update_agent_period($params);
                  $this->send_email_merchant_redeem($params->redeem_seq);
                  //$this->send_notif_merchant_redeem($params->redeem_seq, $params->partner_seq);
                  } */
                $this->M_invoicing_partner_admin->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_invoicing_partner_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_invoicing_partner_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_invoicing_partner_admin->trans_rollback();
            }
        }
    }

    protected function get_additional() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->key = parent::get_input_post('key');
        try {
            $list_data = $this->M_invoicing_partner_admin->get_order_period_partner_by_loan($params);
            parent::set_data($this->data, $list_data);
            if (isset($list_data)) {
                foreach ($list_data as $data_row) {
                    $row = array("No Order" => $data_row->order_no,
                        "Nama Produk" => $data_row->product_name,
                        "Tanggal Order" => parent::cdate($data_row->order_date),
                        "Nilai Order" => ($data_row->total_payment),
                        "Pembayaran 1" => ($data_row->total_installment),
                        "Total Tagihan" => ($data_row->total_payment - $data_row->total_installment),
                    );
                    $output[] = $row;
                }
            }
            echo json_encode($output);
            exit();
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

}

?>