<?php

require_once CONTROLLER_BASE_ADMIN;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Promo Free Fee
 *
 * @author Jartono
 */
class C_expedition_order extends controller_base_admin {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("TRX05001", "admin/transaction/expedition_order");
        $this->initialize();
    }

    private function initialize() {

        $this->load->model('admin/transaction/M_expedition_order');
        $this->load->model('admin/transaction/M_order_merchant_admin');
        $this->load->model('component/M_dropdown_list');
        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_ADDITIONAL, "get_dropdown_city_on_change");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        $this->data[MERCHANT_NAME] = $this->M_dropdown_list->get_dropdown_merchant();
        $this->data[PAYMENT_NAME] = $this->M_dropdown_list->get_dropdown_payment_gateway_method();
        $this->data[EXPEDITION_NAME] = $this->M_dropdown_list->get_dropdown_expedition();
        if (!$this->input->post()) {
            $this->data[FILTER] = 'admin/transaction/expedition_order_f.php';
            $this->load->view("admin/transaction/expedition_order", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->data[EXPEDITION_SERVICE_NAME] = $this->M_dropdown_list->get_dropdown_expedition_service($this->data[DATA_SELECTED][LIST_DATA][0]->expedition_seq);
                        $this->load->view("admin/transaction/expedition_order", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->data[EXPEDITION_SERVICE_NAME] = $this->M_dropdown_list->get_dropdown_expedition_service($this->data[DATA_SELECTED][LIST_DATA][0]->expedition_seq);
                        $this->load->view("admin/transaction/expedition_order", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    public function search() {

        $filter = new stdClass;

        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->order_no = parent::get_input_post("order_no");
        $filter->pSDate = parent::get_input_post("tanggal1");
        $filter->pEDate = parent::get_input_post("tanggal2");
        $filter->pg_method_seq = parent::get_input_post("pg_method_seq");
        $filter->merchant_info_seq = parent::get_input_post("merchant_info_seq");
        $filter->order_status = parent::get_input_post("order_status");
        $filter->payment_status = parent::get_input_post("payment_status");

        try {
            $list_data = $this->M_expedition_order->get_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );
        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->order_seq . "/" . $data_row->merchant_info_seq,
                    "tgl_order" => parent::cdate($data_row->tgl_order),
                    "no_order" => parent::cdef($data_row->no_order),
                    "merchant_name" => parent::cdef($data_row->merchant_name),
                    "expedition_name" => parent::cdef($data_row->expedition_name),
                    "expedition_name_real" => parent::cdef($data_row->expedition_name_real),
                    "payment_status" => parent::cstdes($data_row->payment_status, STATUS_PAYMENT),
                    "order_status" => parent::cstdes($data_row->order_status, STATUS_ORDER));

                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
    }

    protected function get_edit() {

        $a = explode("/", parent::get_input_post("key"));

        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = $a[0];
        $selected->order_seq = $a[0];
        $selected->merchant_info_seq = $a[1];

        $sel_data1 = $this->M_order_merchant_admin->get_produk($selected);
        try {
            $sel_data = $this->M_expedition_order->get_data($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
                $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data1;
            }
            $this->data[EXPEDITION_NAME] = $this->M_dropdown_list->get_dropdown_expedition();
            $this->data[EXPEDITION_SERVICE_NAME] = $this->M_dropdown_list->get_dropdown_expedition_service($sel_data[0]->expedition_seq);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_update() {
        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->order_seq = parent::get_input_post("order_seq");
        $selected->key = parent::get_input_post("order_seq");
        $selected->merchant_info_seq = parent::get_input_post("merchant_info_seq");
        $selected->real_expedition_service_seq = parent::get_input_post("real_expedition_service_seq");

        $sel_data = $this->M_expedition_order->get_data($selected);
        $selected->exs_name = $sel_data[0]->exp_name;
        $selected->es_name = $sel_data[0]->es_name;
        $selected->receiver_address = $sel_data[0]->receiver_address;
        $selected->order_no = $sel_data[0]->order_no;
        $selected->member_name = $sel_data[0]->member_name;
        $selected->order_date = $sel_data[0]->order_date;
        $selected->expedition_seq = $sel_data[0]->expedition_seq;
        $selected->order_status = $sel_data[0]->order_status;
        $this->data[DATA_SELECTED][LIST_DATA][] = $selected;
        $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data1 = $this->M_order_merchant_admin->get_produk($selected);

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_expedition_order->trans_begin();
                if ($sel_data[0]->order_status == NEW_STATUS_CODE && $sel_data[0]->payment_status == ORDER_PREORDER_STATUS_CODE) {
                    $this->M_expedition_order->save_update($selected);
                } else {
                    $selected->order_status = NEW_STATUS_CODE;
                    throw new Exception(ERROR_UPDATE);
                }
                $this->M_expedition_order->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_expedition_order->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_expedition_order->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_expedition_order->trans_rollback();
            }
        }
    }

    protected function get_dropdown_city_on_change() {
        $expedition_seq = parent::get_input_post("expedition_seq");

        try {
            $this->data[EXPEDITION_SERVICE_NAME] = $this->M_dropdown_list->get_dropdown_expedition_service($expedition_seq);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $expedition_service_name[] = array("display_name" => "", "value" => "");

        if (isset($this->data[EXPEDITION_SERVICE_NAME])) {
            foreach ($this->data[EXPEDITION_SERVICE_NAME] as $data_row) {
                $expedition_service_name[] = array(
                    "display_name" => $data_row->name,
                    "value" => $data_row->seq
                );
            }
        }
        echo json_encode($expedition_service_name);
        die();
    }

}

?>
