<?php

require_once CONTROLLER_BASE_ADMIN;

class C_member_get_axa extends controller_base_admin {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("TRX09000", "admin/transaction/member_get_axa");
        $this->initialize();
    }

    private function initialize() {

        $this->load->model('admin/transaction/M_order_insurance');
        parent::register_event($this->data, ACTION_SEARCH, "search");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        if (!$this->input->post()) {
            $this->data[FILTER] = 'admin/transaction/member_get_axa_f.php';
            $this->load->view("admin/transaction/member_get_axa", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    
                }else{
                    
                }
            }
        }
    }

    public function search() {

        $filter = new stdClass;

        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = "toi.created_date";
        $filter->order_no = parent::get_input_post("order_no");
        $filter->date_reg_start = parent::get_input_post("date_reg1");
        $filter->date_reg_end = parent::get_input_post("date_reg2");
        $filter->member_name = parent::get_input_post("member_name");
        $filter->phone_number = parent::get_input_post("phone_number");
        $filter->email_address = parent::get_input_post("email_address");
        $filter->gender = parent::get_input_post("gender");

        try {
            $list_data = $this->M_order_insurance->get_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );
        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->order_seq,
                    "order_no" => parent::cdef($data_row->order_no),
                    "total_order" => parent::cnum($data_row->total_order),
                    "created_date" => parent::cdate($data_row->created_date),
                    "member_name" => parent::cdef($data_row->member_name),
                    "member_birthday" => parent::cdate($data_row->member_birthday),
                    "member_phone" => parent::cdef($data_row->member_phone),
                    "member_email" => parent::cdef($data_row->member_email),
                    "gender" => parent::cstdes($data_row->gender, STATUS_GENDER));

                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
    }

}

?>
