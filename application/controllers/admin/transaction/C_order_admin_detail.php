<?php

require_once CONTROLLER_BASE_ADMIN;

class C_order_admin_detail extends controller_base_admin {

    private $data;
    private $hseq;

    public function __construct() {
        $this->data = parent::__construct("TRX01004", "admin/transaction/order_detail");
        $this->initialize();
    }

    private function initialize() {
        $this->hseq = $this->uri->segment(4);
        if (strlen($this->hseq) > 20) {
            $this->load->view("errors/html/error_404");
        }

        $this->load->model('admin/transaction/M_order_admin');

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        if (!$this->input->post()) {
            if ($this->data[DATA_AUTH][FORM_ACTION] == "") {
                $this->get_data_header();
            }
            $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
            $this->load->view("admin/transaction/order_admin_detail", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/transaction/order_admin_detail", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/transaction/order_admin_detail", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq));
                }
            }
        }
    }

    protected function get_data_header() {
        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = $this->hseq;

        try {
            $sel_data = $this->M_order_admin->get_data_detail($selected);
            
            /*
             * check this order info, from agent or member
             */
            
//            var_dump($sel_data[0][0]->member_name );die();
            
            if($sel_data[0][0]->member_name == null){
                $selected->order_seq = $sel_data[1][0]->order_seq;
                $info_loan = $this->M_order_admin->get_order_info_loan_admin($selected);
                $selected->customer_seq = isset($info_loan) ? $info_loan[0]->customer_seq : "";
                $this->data['customer_table'] = parent::create_table_customer_info($selected);
                $this->data['table_simulation'] = parent::create_table_customer_simulation_agent($selected);
            }
            
            
            if ($sel_data != "") {
                parent::set_data($this->data, $sel_data);
            } else {
                $this->load->view("errors/html/error_404");
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

}

?>