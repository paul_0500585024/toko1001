<?php

require_once CONTROLLER_BASE_ADMIN;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Redeem Period
 *
 * @author Jartono
 */
class C_invoicing_period_admin extends controller_base_admin {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("TRX10001", "admin/transaction/invoicing_period");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('admin/transaction/M_invoicing_period_admin');
        $this->load->model('component/M_email');
        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_SAVE_DELETE, "save_delete");
        parent::register_event($this->data, ACTION_ADDITIONAL, "get_ajax");
        parent::fire_event($this->data);
    }

    public function index() {
        $this->data[FILTER] = 'admin/transaction/invoicing_period_admin_f.php';

        if (!$this->input->post()) {
            $this->load->view("admin/transaction/invoicing_period_admin", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/transaction/invoicing_period_admin", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/transaction/invoicing_period_admin", $this->data);
                    }
                } elseif ($this->data[DATA_ERROR][ERROR] === false) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                    }
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_ADD)
                        $this->get_add();
                    $this->load->view("admin/transaction/invoicing_period_admin", $this->data);
                }
            } else {

            }
        }
    }

    protected function get_ajax() {
        $tipe = parent::get_input_post("tipe");
        $idh = parent::get_input_post("idh");
        $params = new stdClass;
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->seq = $idh;
        $sel_data = $this->M_invoicing_period_admin->get_data($params);
        if (isset($sel_data)) {
            if ($sel_data[0]->status != "O") {
                parent::set_json_error('', ERROR_PROCESS);
                throw new Exception();
            }
        } else {
            parent::set_json_error('', ERROR_PROCESS);
            throw new Exception();
        }
        switch ($tipe) {
            case "calculate_invoice":
                if (isset($sel_data[0])) {
                    $params->to_date = $sel_data[0]->to_date;
                    // select t_order_merchant khusus order agent yang masuk range periode
                    $t_rc_ord = $this->M_invoicing_period_admin->get_order_loan($params);
                }
                try {
                    $this->M_invoicing_period_admin->trans_begin();
                    if (isset($t_rc_ord)) {
                        // insert ke t_collecting_period
                        foreach ($t_rc_ord as $data_row) {
                            $params->partner_seq = $data_row->partner_seq;
                            $params->total_payment = $data_row->total_payment;
                            $params->total_installment = $data_row->total_installment;
                            $params->type = 'ORD';
                            $params->mutation_type = 'D';
                            $this->M_invoicing_period_admin->save_add_invoicing_component_by_partner_seq($params);
                        }
                    }
                    // update t_order_loan yang masuk range periode
                    $this->M_invoicing_period_admin->save_update_order_loan($params);
                    // update t_collecting_period status
                    $params->status_new = "P";
                    $params->status_old = "O";
                    $this->M_invoicing_period_admin->status_update_collect_period($params);
                    $this->M_invoicing_period_admin->trans_commit();
                    $msg = "OK";
                } catch (BusisnessException $ex) {
                    parent::set_error($this->data, $ex);
                    $this->M_invoicing_period_admin->trans_rollback();
                    $msg = "Error" . $ex;
                } catch (TechnicalException $ex) {
                    $msg = "Error" . $ex;
                    $this->M_invoicing_period_admin->trans_rollback();
                } catch (Exception $ex) {
                    $msg = "Error" . $ex;
                    $this->M_invoicing_period_admin->trans_rollback();
                }
                die($msg);
                break;
            default:
                die();
        }
    }

    protected function search() {
        $params = new stdClass;
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->start = parent::get_input_post("start");
        $params->length = parent::get_input_post("length");
        $params->order = parent::get_input_post("order");
        $params->column = parent::get_input_post("column");
        $params->from_date = parent::get_input_post("tanggal1");
        $params->to_date = parent::get_input_post("tanggal2");
        $params->status = parent::get_input_post("status");
        try {
            $list_data = $this->M_invoicing_period_admin->get_list($params);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );

        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                if ($data_row->status == "O") {
                    $link = '<input type="button" class="btnproses" value="Kalkulasi" onclick="calculate_invoice(\'' . $data_row->seq . '\',this)">';
                } else {
                    $link = "<a href='invoicing_period_partner/" . $data_row->seq . "'>Detil Partner</a>";
                }
                $row = array("DT_RowId" => $data_row->seq,
                    "from_date" => parent::cdate($data_row->from_date),
                    "to_date" => parent::cdate($data_row->to_date),
                    "status" => parent::cstdes($data_row->status, STATUS_OPC),
                    "action" => $link,
                    "created_by" => $data_row->created_by,
                    "created_date" => parent::cdate($data_row->created_date, 1),
                    "modified_by" => $data_row->modified_by,
                    "modified_date" => parent::cdate($data_row->modified_date, 1));
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
    }

    protected function get_edit() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->seq = parent::get_input_post("key");
        try {
            $sel_data = $this->M_invoicing_period_admin->get_data($params);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function get_add() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $sel_data = $this->M_invoicing_period_admin->get_last_date($params);
        parent::set_data($this->data, $sel_data);
    }

    protected function save_add() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->from_date = parent::get_input_post("from_date", true, DATE_VALIDATOR, "Tanggal Awal", $this->data);
        $params->to_date = parent::get_input_post("to_date", true, DATE_VALIDATOR, "Tanggal Akhir", $this->data);
        $params->status = "O";
        if (strtotime($params->from_date) > strtotime(date("d-M-Y")) || strtotime($params->to_date) > strtotime(date("d-M-Y"))) {
            $this->data[DATA_ERROR][ERROR] = true;
            $this->data[DATA_ERROR][ERROR_MESSAGE][] = "ERROR : " . ERROR_DATE_VALIDATION;
        }
        $this->data[DATA_SELECTED][LIST_DATA][] = $params;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_invoicing_period_admin->trans_begin();
                $this->M_invoicing_period_admin->save_add($params);
                $this->M_invoicing_period_admin->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_invoicing_period_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_invoicing_period_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_invoicing_period_admin->trans_rollback();
            }
        }
    }

}

?>