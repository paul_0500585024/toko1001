<?php

require_once CONTROLLER_BASE_ADMIN;

class C_member_refund extends controller_base_admin {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("TRX02001", "admin/transaction/member/refund");
        $this->initialize();
    }

    private function initialize() {

        $this->load->model('admin/transaction/M_member_refund_admin');
        $this->load->model('component/M_dropdown_list');
        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_SAVE_DELETE, "save_delete");
        parent::register_event($this->data, ACTION_APPROVE, "approve");
        parent::register_event($this->data, ACTION_REJECT, "reject");
        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        $this->data[PAYMENT_NAME] = $this->M_dropdown_list->get_dropdown_payment_method();
        $this->data[MEMBER_EMAIL] = $this->M_dropdown_list->get_dropdown_member_email();
        if (!$this->input->post()) {
            $this->data[FILTER] = 'admin/transaction/member_refund_admin_f.php';
            $this->load->view("admin/transaction/member_refund_admin", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;

                        $this->load->view("admin/master/member_refund_admin", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/master/member_refund_admin", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    public function search() {

        $filter = new stdClass;
        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->trx_no = parent::get_input_post("trx_no");
        $filter->pgmethod = parent::get_input_post("pg_method");
        $filter->trx_type = parent::get_input_post("trx_type");
        $filter->member_email = parent::get_input_post("member_email");
        $filter->status = parent::get_input_post("status");

        try {
            $list_data = $this->M_member_refund_admin->get_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );
        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {

                $row = array("DT_RowId" => $data_row->seq . "/" . $data_row->member_seq,
                    "pg_name" => parent::cdef($data_row->pg_name),
                    "trx_date" => parent::cdate($data_row->trx_date),
                    "member_name" => parent::cdef($data_row->member_name),
                    "trx_no" => parent::cdef($data_row->trx_no),
                    "trx_type" => parent::cstdes($data_row->trx_type, DEPOSIT_ACCOUNT_TYPE),
                    "refund_date" => parent::cdate($data_row->refund_date),
                    "created_date" => parent::cdate($data_row->created_date),
                    "deposit_trx_amt" => parent::cnum($data_row->deposit_trx_amt),
                    "detail" => "<a href='refund_detail/" . $data_row->trx_no ."'>Detail Produk</a>",
                    "status" => parent::cstdes($data_row->status, STATUS_ANR));
                $output['aaData'][] = $row;
            }
        };

        echo json_encode($output);
    }

    protected function approve() {
        $a = explode("/", parent::get_input_post("key"));
        $params = new stdClass();
        $params->seq = $a[0];
        $params->member_seq = $a[1];
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $get_refund_data = $this->M_member_refund_admin->get_refund_data($params);
        $params->member_seq = $get_refund_data[0]->member_seq;
        $params->deposit_trx_amt = $get_refund_data[0]->deposit_trx_amt;
        $params->deposit_amt = $get_refund_data[0]->deposit_amt;        
        $params->email = $get_refund_data[0]->email_member;
        $params->name = $get_refund_data[0]->name;
        $params->pgm = $get_refund_data[0]->pgm_method;
        $params->status = $get_refund_data[0]->status_now;

        try {
            if ($params->status != NEW_STATUS_CODE) {
                throw new Exception(ERROR_APPROVED_STATUS);
            }
            $this->M_member_refund_admin->trans_begin();
            $approve = $this->M_member_refund_admin->approve($params);
            if ($get_refund_data[0]->deposit_trx_amt != "") {
                $deposit = "DEPOSIT";
                $params->new_deposit = $params->deposit_trx_amt + $params->deposit_amt;
                 
            }
            $email_params = new stdClass();
            $email_params->user_id = parent::get_admin_user_id();
            $email_params->ip_address = parent::get_ip_address();
            $email_params->code = MEMBER_REFUND_SUCCESS_CODE;
            $email_params->RECIPIENT_NAME = $params->name;
            $email_params->TOTAL_REFUND = parent::cnum($params->deposit_trx_amt);
            $email_params->PAYMENT_METHOD =  $deposit ;
            $email_params->ACCOUNT_LINK = base_url() . "member/";
            $email_params->to_email = $params->email;
            parent::email_template($email_params);
            $this->M_member_refund_admin->trans_commit();
            parent::set_json_success();
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_member_refund_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_APPROVED);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_member_refund_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_APPROVED);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_member_refund_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_APPROVED);
        }
    }

    protected function reject() {
        $a = explode("/", parent::get_input_post("key"));
        $params = new stdClass();
        $params->seq = $a[0];
        $params->member_seq = $a[1];
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $get_refund_data = $this->M_member_refund_admin->get_refund_data($params);
        $params->member_seq = $get_refund_data[0]->member_seq;
        $params->name = $get_refund_data[0]->name;
        $params->email = $get_refund_data[0]->email_member;
        $params->status = $get_refund_data[0]->status_now;
        $params->deposit_trx_amt = $get_refund_data[0]->deposit_trx_amt;
        
        try {
            if ($params->status != NEW_STATUS_CODE) {
                throw new Exception(ERROR_UPDATE);
            }
            $this->M_member_refund_admin->trans_begin();
            $this->M_member_refund_admin->reject($params);
            $email_params = new stdClass();
            $email_params->user_id = parent::get_admin_user_id();
            $email_params->ip_address = parent::get_ip_address();
            $email_params->code = MEMBER_REFUND_FAIL_CODE;
            $email_params->RECIPIENT_NAME = $params->name;
            $email_params->TOTAL_REFUND = $params->deposit_trx_amt;
            $email_params->ACCOUNT_LINK = base_url() . "member/profile/profile_member";
            $email_params->to_email = $params->email;
            parent::email_template($email_params);
            $this->M_member_refund_admin->trans_commit();
            parent::set_json_success();
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_member_refund_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_REJECT);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_member_refund_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_REJECT);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_member_refund_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_REJECT);
        }
    }

}

?>