<?php

require_once CONTROLLER_BASE_ADMIN;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Redeem Merchant
 *
 * @author Jartono
 */
class C_redeem_period_agent_admin extends controller_base_admin {

    private $data;
    private $hseq;

    public function __construct() {
        $this->data = parent::__construct("TRX04004", "admin/transaction/redeem_period_agent");
        $this->initialize();
    }

    private function initialize() {
        $this->hseq = $this->uri->segment(4);
        $this->load->model('admin/transaction/M_redeem_agent_admin');
        $this->load->model('component/M_email');
        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_ADDITIONAL, "get_detail_order");
        parent::fire_event($this->data);
    }

    public function index($pseq = '') {
        $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
        if (!$this->input->post()) {
            if ($this->data[DATA_AUTH][FORM_ACTION] == "") {
                $this->get_data_header();
            }
            $this->load->view("admin/transaction/redeem_period_agent_admin", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                //die($this->data[DATA_AUTH][FORM_URL]);
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/transaction/redeem_period_agent_admin", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/transaction/redeem_period_agent_admin", $this->data);
                    }
                } else {

                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        //$this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
                        redirect(base_url("admin/transaction/redeem_period_agent/" . $this->hseq));
                    }
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_VIEW) {
                        $this->get_view();
                    }
//		    $admin_info[SESSION_DATA] = $this->data;
//		    $this->session->set_userdata($admin_info);
                    $this->load->view("admin/transaction/redeem_period_agent_admin", $this->data);
                }
            } else {

            }
        }
    }

    public function search() {
        $params = new stdClass;
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->start = parent::get_input_post("start");
        $params->length = parent::get_input_post("length");
        $params->order = parent::get_input_post("order");
        $params->column = parent::get_input_post("column");
        $params->seq = $this->hseq;
        try {
            $list_data = $this->M_redeem_agent_admin->get_list_detail($params);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $output = array(
                "sEcho" => "",
                "iTotalRecords" => "0",
                "iTotalDisplayRecords" => "0",
                "aaData" => []
            );
            echo json_encode($output);
            die();
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );
        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->redeem_seq . '-' . $data_row->partner_seq,
                    "partner_seq" => $data_row->partner_seq,
                    "total" => number_format($data_row->total),
                    "status" => parent::cstdes($data_row->status, STATUS_REDEEM_AGENT),
                    "paid_date" => parent::cdate($data_row->paid_date),
                    "bank_name" => parent::cdef($data_row->bank_name),
                    "bank_branch_name" => parent::cdef($data_row->bank_branch_name),
                    "bank_acct_no" => parent::cdef($data_row->bank_acct_no),
                    "bank_acct_name" => parent::cdef($data_row->bank_acct_name),
                    "name" => parent::cdef($data_row->name)
                );
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
        die();
    }

    protected function get_edit() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $key = parent::get_input_post("key");
        $key = explode("-", $key);
        $params->seq = $key[0];
        $params->partner_seq = $key[1];
        try {

            $sel_data = $this->M_redeem_agent_admin->get_data_detail($params);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function get_view() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $key = parent::get_input_post("key");
        $key = explode("-", $key);
        $params->seq = $key[0];
        $params->partner_seq = $key[1];
        try {
            $sel_data = $this->M_redeem_agent_admin->get_data_detail($params);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
                $sel_data1 = $this->M_redeem_agent_admin->get_redeem_component_by_merchant($params);
                $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data1;
                $params->payment_code = PAYMENT_TYPE_AGENT;
                $sel_data2 = $this->M_redeem_agent_admin->get_order_agent_pg_by_redeem($params);
                $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data2;
                $sel_data3 = $this->M_redeem_agent_admin->get_redeem_agent_by_partner($params);
                $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data3;
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function get_data_header() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->seq = $this->hseq;
        try {
            $sel_data = $this->M_redeem_agent_admin->get_data($params);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_update() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->redeem_seq = parent::get_input_post("redeem_seq");
        $params->partner_seq = parent::get_input_post("partner_seq");
        $params->paid_date = parent::get_input_post("paid_date", true, DATE_VALIDATOR, "Tanggal Bayar", $this->data);
        $params->seq = $params->redeem_seq;
        $sel_data = $this->M_redeem_agent_admin->get_data_detail($params);
        if (isset($sel_data)) {
            $params->status = $sel_data[0]->status;
            $params->total = $sel_data[0]->total;
            $params->bank_name = $sel_data[0]->bank_name;
            $params->bank_branch_name = $sel_data[0]->bank_branch_name;
            $params->bank_acct_no = $sel_data[0]->bank_acct_no;
            $params->bank_acct_name = $sel_data[0]->bank_acct_name;
            $params->name = $sel_data[0]->name;
            $params->partner_seq = $sel_data[0]->partner_seq;
            if ($sel_data[0]->status != "U") {
                $this->data[DATA_ERROR][ERROR] = true;
                $this->data[DATA_ERROR][ERROR_MESSAGE][] = "ERROR : " . ERROR_UPDATE;
            }
        } else {
            $this->data[DATA_ERROR][ERROR] = true;
            $this->data[DATA_ERROR][ERROR_MESSAGE][] = "ERROR : " . ERROR_UPDATE;
        }
        $this->data[DATA_SELECTED][LIST_DATA][] = $params;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_redeem_agent_admin->trans_begin();
                // update t_redeem_agent paid_date
                $this->M_redeem_agent_admin->save_update_detail($params);
                // cek apa semua sudah dibayar
                $statusall = $this->get_status_redeem($params->redeem_seq);
                if ($statusall == "ALL") {
                    // update t_redeem_period status semua sudah update
                    $params->status_new = "C";
                    $params->status_old = "P";
                    $this->M_redeem_agent_admin->status_update_agent_period($params);
                    $this->send_email_merchant_redeem($params->redeem_seq);
                    //$this->send_notif_merchant_redeem($params->redeem_seq, $params->partner_seq);
                }
                $this->M_redeem_agent_admin->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_redeem_agent_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_redeem_agent_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_redeem_agent_admin->trans_rollback();
            }
        }
    }

    protected function get_status_redeem($idh) {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->seq = $idh;
        $return = "ALL";
        $sel_data = $this->M_redeem_agent_admin->get_list_detail_status($params);
        if (isset($sel_data)) {
            foreach ($sel_data as $data_row) {
                if ($data_row->status == "U")
                    $return = "NOTALL";
            }
        }
        return $return;
    }

    protected function send_email_merchant_redeem($idh) {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->seq = $idh;
        $sel_data = $this->M_redeem_agent_admin->get_redeem_agent_partner_list($params);
        if (isset($sel_data)) {
            $periode_data = $this->M_redeem_agent_admin->get_data($params);
            $params->code = "PARTNER_REDEEM_INFO";
            $params->PERIODE = parent::cdate($periode_data[0]->from_date) . ' s/d ' . parent::cdate($periode_data[0]->to_date);
            foreach ($sel_data as $data_row) {
                $params->RECIPIENT_NAME = $data_row->name;
                $params->to_email = $data_row->email;
                parent::email_template($params);
            }
        }
    }

    protected function send_notif_merchant_redeem($idh, $partner_seq) {
        $params_notif = new stdClass();
        $params_notif->user_id = parent::get_admin_user_id();
        $params_notif->ip_address = parent::get_ip_address();
        $params_notif->seq = $idh;
        $sel_data = $this->M_redeem_agent_admin->get_list_detail_status($params_notif);
        if (isset($sel_data)) {
            $periode_data = $this->M_redeem_agent_admin->get_data($params_notif);
            $params_notif->code = REDEEM_MERCHANT;
            $params_notif->user_group_seq = '3';
            $params_notif->partner_seq = $partner_seq;
            $params_notif->period = parent::cdate($periode_data[0]->from_date) . ' s/d ' . parent::cdate($periode_data[0]->to_date);
            //$url_detail = base_url().'merchant/transaction/redeem_merchant?'.CONTROL_GET_TYPE.'='.ACTION_VIEW.'&key='.$idh;
            $url_detail = $idh;
            foreach ($sel_data as $data_row) {
                $params_notif->total_order = RP . $data_row->total;
                parent::notif_template($params_notif);
            }
        }
    }

    protected function get_detail_order() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $key = explode("~", parent::get_input_post("key"));
        $params->redeem_seq = $key[0];
        $params->agent_seq = $key[1];
        try {
            $sel_data = $this->M_redeem_agent_admin->get_redeem_detail_order($params);
            if (isset($sel_data)) {
                $output = array();
                foreach ($sel_data as $data_row) {
                    $row = array("seq" => $data_row->seq,
                        "order_no" => $data_row->order_no,
                        "order_date" => parent::cdate($data_row->order_date),
                        "total_order" => number_format($data_row->total_order),
                        "total_payment" => number_format($data_row->total_payment),
                        "order_status" => parent::cstdes($data_row->order_status, STATUS_ORDER),
                        "total_merchant" => number_format($data_row->total_merchant),
                        "total_ins" => number_format($data_row->total_ins),
                        "total_ship_real" => number_format($data_row->total_ship_real),
                        "total_ship_charged" => number_format($data_row->total_ship_charged),
                        "totalorder" => number_format($data_row->totalorder),
                        "qty" => number_format($data_row->qty),
                        "sell_price" => number_format($data_row->sell_price),
                        "totalfee" => number_format($data_row->totalfee),
                        "product_name" => ($data_row->product_name),
                        "variant_name" => ($data_row->variant_name)
                    );
                    $output[] = $row;
                }
                echo json_encode($output);
            }
            die();
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

}

?>