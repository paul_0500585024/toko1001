<?php

require_once CONTROLLER_BASE_ADMIN;

class C_expedition_receipt_admin extends controller_base_admin {

    private $data;

    public function __construct() {

        $this->data = parent::__construct("TRX08001", "admin/transaction/expedition_receipt");
        $this->initialize();
    }

    private function initialize() {

        $this->load->model('component/M_dropdown_list');
        $this->load->model('admin/transaction/M_expedition_receipt_admin');

        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_SAVE_DELETE, "save_delete");
        parent::register_event($this->data, ACTION_APPROVE, "save_approve");
        parent::register_event($this->data, ACTION_REJECT, "");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {

        $this->data[EXPEDITION_NAME] = $this->M_dropdown_list->get_dropdown_expedition();

        if (!$this->input->post()) {

            if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_ADD) {
                $params = new stdClass();
                $params->exp_seq = '';
                $params->seq = '';
                $this->data[REDEEM_PERIOD] = $this->M_dropdown_list->get_dropdown_redeem_period($params);
            }

            $this->data[FILTER] = 'admin/transaction/expedition_receipt_admin_f.php';
            $this->load->view("admin/transaction/expedition_receipt_admin", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/transaction/expedition_receipt_admin", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/transaction/expedition_receipt_admin", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    protected function search() {

        $filter = new stdClass;
        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->expedition_seq = parent::get_input_post("expedition_seq");
        $filter->invoice_no = parent::get_input_post("invoice_no");
        $filter->status = parent::get_input_post("status");

        try {
            $list_data = $this->M_expedition_receipt_admin->get_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );

        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->expedition_seq . '/' . $data_row->seq,
                    "expedition_seq" => parent::cdef($data_row->expedition_seq),
                    "expedition_name" => parent::cdef($data_row->expedition_name),
                    "invoice_no" => parent::cdef($data_row->invoice_no),
                    "invoice_date" => parent::cdate($data_row->invoice_date, 1),
                    "status" => parent::cstdes($data_row->status, STATUS_RECEIPT),
                    "total_amt" => parent::cnum($data_row->total_amt),
                    "detail" => "<a href='expedition_receipt_detail/" . $data_row->seq . "'>detail</a>",
                    "created_by" => $data_row->created_by,
                    "created_date" => parent::cdate($data_row->created_date, 1),
                    "modified_by" => $data_row->modified_by,
                    "modified_date" => parent::cdate($data_row->modified_date, 1));
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
    }

    protected function get_edit() {

        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();

        $key = explode("/", parent::get_input_post("key"));
        $selected->exp_seq = $key[0];
        $selected->seq = $key[1];

        try {
            $sel_data = $this->M_expedition_receipt_admin->get_edit($selected);
            if (isset($sel_data)) {
                $this->data[REDEEM_PERIOD] = $this->M_dropdown_list->get_dropdown_redeem_period($selected);
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_add() {

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->expedition_seq = parent::get_input_post("expedition_seq", true, FILL_VALIDATOR, "Nama Ekspedisi", $this->data);
        $params->invoice_no = parent::get_input_post("invoice_no");
        $params->invoice_date = parent::get_input_post("date", true, DATE_VALIDATOR, "Tanggal Faktur,", $this->data);
        $params->date = parent::get_input_post("date");
        $params->period_seq = parent::get_input_post("period_seq", true, FILL_VALIDATOR, "Periode Redeem", $this->data);
        $params->status = 'O';

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_expedition_receipt_admin->trans_begin();
                $this->M_expedition_receipt_admin->save_add($params);
                $this->M_expedition_receipt_admin->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_expedition_receipt_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_expedition_receipt_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_expedition_receipt_admin->trans_rollback();
            }
        }
    }

    protected function save_update() {

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->seq = parent::get_input_post('seq');
        $params->expedition_seq = parent::get_input_post("expedition_seq", true, FILL_VALIDATOR, "Nama Ekspedisi", $this->data);
        $params->invoice_no = parent::get_input_post("invoice_no");
        $params->invoice_date = parent::get_input_post("date", true, DATE_VALIDATOR, "Tanggal Faktur", $this->data);
        $params->date = parent::get_input_post("date");
        $params->period_seq = parent::get_input_post("period_seq", true, FILL_VALIDATOR, "Periode Redeem", $this->data);

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        if ($this->data[DATA_ERROR][ERROR] === false) {

            try {
                $sel_data = $this->M_expedition_receipt_admin->get_status_receipt($params);
                if ($sel_data[0]->status == OPEN_STATUS_CODE) {
                    $this->M_expedition_receipt_admin->trans_begin();
                    $this->M_expedition_receipt_admin->save_update($params);
                    $this->M_expedition_receipt_admin->trans_commit();
                } else {
                    throw new Exception(ERROR_APPROVED_INVOICE);
                }
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->m_city_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_expedition_receipt_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_expedition_receipt_admin->trans_rollback();
            }
        }
    }

    protected function save_delete() {

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();

        $key = explode("/", parent::get_input_post("key"));
        $params->exp_seq = $key[0];
        $params->seq = $key[1];

        try {
            $sel_data = $this->M_expedition_receipt_admin->get_status_receipt($params);
            if ($sel_data[0]->status == OPEN_STATUS_CODE) {
                $this->M_expedition_receipt_admin->trans_begin();
                $this->M_expedition_receipt_admin->save_delete($params);
                $this->M_expedition_receipt_admin->trans_commit();
                parent::set_json_success();
            } else {
                throw new Exception(ERROR_DELETE_STATUS);
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_expedition_receipt_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_expedition_receipt_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_expedition_receipt_admin->trans_rollback();
            parent::set_json_error($ex, ERROR_DELETE);
        }
    }

    protected function save_approve() {

        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();

        $key = explode("/", parent::get_input_post("key"));
        $selected->exp_seq = $key[0];
        $selected->seq = $key[1];

        try {
            $sel_data = $this->M_expedition_receipt_admin->get_data_for_approve($selected);
            if ($sel_data[0]->awb_seq != Null) {
                if ($sel_data[0]->exp_receipt_status == OPEN_STATUS_CODE) {
                    if ($sel_data[0]->redeem_status == OPEN_STATUS_CODE) {
                        $this->M_expedition_receipt_admin->trans_begin();
                        $this->M_expedition_receipt_admin->save_approve($selected);
                        $this->M_expedition_receipt_admin->trans_commit();
                        parent::set_json_success();
                    } else {
                        throw new Exception(ERROR_REDEEM_STATUS);
                    }
                } else {
                    throw new Exception(ERROR_APPROVED_INVOICE);
                }
            } else {
                throw new Exception(ERROR_APPROVED_INVOICE_DETAIL);
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_expedition_receipt_admin->trans_rollback();
            parent::set_json_error($ex);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_expedition_receipt_admin->trans_rollback();
            parent::set_json_error($ex);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_expedition_receipt_admin->trans_rollback();
            parent::set_json_error($ex);
        }
    }

//    public function save_reject() {
//        
//        $selected = new stdClass();
//        $selected->user_id = parent::get_admin_user_id();
//        $selected->ip_address = parent::get_ip_address();
//
//        $key = explode("/", parent::get_input_post("key"));
//        $selected->exp_seq = $key[0];
//        $selected->seq = $key[1];
//
//        try {
//            $sel_data = $this->M_expedition_receipt_admin->get_data_for_approve($selected);
//
//            $selected->status = $sel_data[0]->status;
//
//            if ($selected->status == OPEN_STATUS_CODE) {
//                $this->M_expedition_receipt_admin->trans_begin();
//                $this->M_expedition_receipt_admin->save_reject($selected);
//                $this->M_expedition_receipt_admin->trans_commit();
//                parent::set_json_success();
//            } else {
//                parent::set_json_error(ERROR_REJECT);
//            }
//        } catch (BusisnessException $ex) {
//            parent::set_error($this->data, $ex);
//            $this->M_expedition_receipt_admin->trans_rollback();
//            parent::set_json_error($ex, ERROR_REJECT);
//        } catch (TechnicalException $ex) {
//            parent::set_error($this->data, $ex);
//            $this->M_expedition_receipt_admin->trans_rollback();
//            parent::set_json_error($ex, ERROR_REJECT);
//        } catch (Exception $ex) {
//            parent::set_error($this->data, $ex);
//            $this->M_expedition_receipt_admin->trans_rollback();
//            parent::set_json_error($ex, ERROR_REJECT);
//        }
//    }

}

?>