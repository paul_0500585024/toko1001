<?php

require_once CONTROLLER_BASE_ADMIN;

class C_order_admin extends controller_base_admin {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("TRX01002", "admin/transaction/order");
        $this->initialize();
    }

    private function initialize() {

        $this->load->model('admin/transaction/M_order_admin');
        $this->load->model('admin/transaction/M_order_merchant_admin');
        $this->load->model('merchant/master/M_product_stock_merchant');
        $this->load->model('merchant/transaction/M_trans_log_merchant');
        $this->load->model('component/M_dropdown_list');
        $this->load->model('component/M_voucher');
        $this->load->model('member/M_order_member');
        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        $this->data[PAYMENT_NAME] = $this->M_dropdown_list->get_dropdown_payment_gateway_method();
        if (!$this->input->post()) {
            $this->data[FILTER] = 'admin/transaction/order_admin_f.php';
            $this->load->view("admin/transaction/order_admin", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("admin/transaction/order_admin", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->get_edit();
                        $this->load->view("admin/transaction/order_admin", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    public function search() {

        $filter = new stdClass;

        $filter->user_id = parent::get_admin_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->order_no = parent::get_input_post("order_no");
        $filter->pSDate = parent::get_input_post("tanggal1");
        $filter->pEDate = parent::get_input_post("tanggal2");
        $filter->payment_status = parent::get_input_post("status");
        $filter->pg_method_seq = parent::get_input_post("pg_method_seq");

        try {
            $list_data = $this->M_order_admin->get_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );
        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->seq,
                    "order_no" => '<a href ="order_detail/' . $data_row->order_no . '">' . parent::cdef($data_row->order_no) . '</a>',
                    "order_date" => parent::cdate($data_row->order_date),
                    "member_name" => parent::cdef($data_row->member_name),
                    "payment_name" => $data_row->payment_name,
                    "total_order" => parent::cnum($data_row->total_order),
                    "voucher_nominal" => parent::cnum($data_row->voucher_nominal),
                    "total_payment" => parent::cnum($data_row->total_payment),
                    "conf_pay_amt_member" => parent::cnum($data_row->conf_pay_amt_member),
                    "payment_status" => parent::cstdes($data_row->payment_status, STATUS_PAYMENT));

                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
    }

    protected function get_edit() {
        $selected = new stdClass();
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = parent::get_input_post("key");

        $sel_data1 = $this->M_order_admin->get_produk($selected);
        try {
            $sel_data = $this->M_order_admin->get_data($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
                $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data1;
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_update() {

        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->seq = parent::get_input_post("seq");
        $params->key = parent::get_input_post("seq");
        $params->order_no = parent::get_input_post("order_no");
        $params->order_date = parent::get_input_post("order_date");
        $params->member_name = parent::get_input_post("member_name");
        $params->payment_status = parent::get_input_post("payment_status");
        $params->order_payment_status = parent::get_input_post("order_payment_status");
        $params->total_payment = parent::get_input_post("total_payment");
        $params->total_order = parent::get_input_post("total_order");
        $params->nominal = parent::get_input_post("nominal");
        $params->conf_pay_amt_admin = parent::get_input_post("conf_pay_amt_admin");
        $params->pg_name = parent::get_input_post("pg_name");
        $params->conf_pay_amt_member = parent::get_input_post("conf_pay_amt_member");
        $params->conf_pay_date = parent::get_input_post("conf_pay_date");
        $params->paid_date = parent::get_input_post("paid_date");
        $params->conf_pay_bank_seq = parent::get_input_post("conf_pay_bank_seq");
        $params->bank_acct_name = parent::get_input_post("bank_acct_name");
        $params->bank_acct_no = parent::get_input_post("bank_acct_no");
        $params->logo_img = parent::get_input_post("logo_img");
        $params->conf_pay_note_file = parent::get_input_post("conf_pay_note_file");
        $order_info = $this->M_order_admin->get_produk($params);
        $order_content = "<table border='1'>
                           <thead>
                            <tr>
                                <th>Produk</th>
                                <th>Qty</th>
                                <th>Harga (Rp)</th>
                                <th>Total (Rp)</th>
                            </tr>
                           </thead><tbody>";

        foreach ($order_info as $product) {
            $order_content = $order_content . "<tr>
                                               <td>" . $product->product_name . "</td>
                                               <td>" . $product->qty . "</td>
                                               <td>" . number_format($product->sell_price) . "</td>
                                               <td>" . number_format($product->sell_price * $product->qty) . "</td></tr>";
        }

        $order_content = $order_content . "<tr><td colspan='3'>Total Biaya Pengiriman</td><td>" . number_format($product->total_ship) . "</td><tr>";

        $order_content = $order_content . "</tbody>
                                          </table>";
        $address_content = "Penerima : " . $order_info[0]->receiver_name . "<br>
                            Alamat : " . $order_info[0]->receiver_address . "-" . $order_info[0]->province_name . "-" . $order_info[0]->city_name . "-" . $order_info[0]->district_name . "<br>
                            No Tlp : " . $order_info[0]->receiver_phone_no . "<br>";
        $sel_data = $this->M_order_admin->get_produk($params);
        $pro = '';
        if (isset($sel_data)) {
            foreach ($sel_data as $each) {
                $pro .= $each->product_name . '<br>';
            }
        }
//        if ($logo_img->file_name == "") {
//            $params->logo_img = parent::get_input_post("oldname");
//        } else {
//            $params->logo_img = $logo_img->file_name;
//        }
        $data = $this->M_order_admin->get_data($params);
        $params->order_seq = $params->key;

        $data_merchant = $this->M_order_admin->get_data_merchant($params);
        $this->data[DATA_SELECTED][LIST_DATA] = $data;
        $this->data[DATA_SELECTED][LIST_DATA][] = $this->M_order_admin->get_produk($params);
        $this->data[DATA_SELECTED][LIST_DATA][] = $params;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                if ($data[0]->payment_status == PAYMENT_CONFIRM_STATUS_CODE) {

                    // Check ADIRA Finance Purpose for First Installment
                    if ($data[0]->pg_method_seq === PAYMENT_SEQ_ADIRA) {
                        $params->total_payment = $data[0]->dp;
                    } else {
                        $params->total_payment = $data[0]->total_payment;
                    }

                    if ($params->conf_pay_amt_admin >= $params->total_payment) {
                        if ($params->payment_status == PAYMENT_PAID_STATUS_CODE) {
                            $params->payment_status = parent::get_input_post("payment_status");
                            $params->order_status = ORDER_NEW_ORDER_STATUS_CODE;
                            $params->paid_date = parent::get_input_post("paid_date");
                            $params->conf_pay_amt_admin = parent::get_input_post("conf_pay_amt_admin");


                            $email_order3 = new stdClass();
                            $email_order3->user_id = parent::get_admin_user_id();
                            $email_order3->ip_address = parent::get_ip_address();

                            $params_member = new stdClass();
                            $params_member->user_id = $data[0]->member_seq === NULL ? $data[0]->agent_seq : $data[0]->member_seq;
                            $tipe = "";
                            if ($data[0]->member_seq === NULL) {
                                $tipe = AGENT;
                            }
                            $params_member->ip_address = parent::get_ip_address();
                            $params_member->key = $data[0]->order_no;
                            $order_info_new = $this->M_order_member->get_order_info_by_order_no($params_member, $tipe);

                            $order_info_new[0][0]->payment_status = 'P';
                            if ($tipe !== AGENT && $this->is_member_get_axa($order_info_new)) {
                                $email_order3->code = ORDER_PAY_CONFIRM_SUCCESS_WITH_AXA_CODE;
                            } else {
                                $email_order3->code = ORDER_PAY_CONFIRM_SUCCESS_CODE;
                            }
                            $email_order3->RECIPIENT_NAME = $params->member_name;
                            $email_order3->ORDER_NO = $params->order_no;
                            $email_order3->ORDER_DATE = parent::cdate($params->order_date);
                            $email_order3->RECIPIENT_ADDRESS = $address_content;
                            if ($data[0]->pg_method_seq == PAYMENT_SEQ_ADIRA) {
                                $payment_info = parent::cnum($data[0]->total_installment);
                            } else {
                                $payment_info = parent::cnum($data[0]->total_payment);
                            }
                            $email_order3->TOTAL_PAYMENT = $payment_info;
                            $email_order3->PAYMENT_METHOD = $data[0]->pg_name;
                            $email_order3->ORDER_ITEMS = $order_content;
                            $email_order3->CONFIRM_LINK = base_url("member/payment" . "/" . $params->order_no);
                            $email_order3->to_email = $data[0]->email_member === NULL ? $data[0]->email_agent : $data[0]->email_member;

                            parent::email_template($email_order3);

                            $email_order4 = new stdClass();
                            $email_order4->user_id = parent::get_admin_user_id();
                            $email_order4->ip_address = parent::get_ip_address();
                            $email_order4->code = ORDER_NEW_CODE;
                            $params->order_seq = $order_info[0]->order_seq;
                            $params->img = $order_info[0]->pic_1_img;

                            foreach ($data_merchant as $value) {
                                $params->merchant_info_seq = $value->merchant_info_seq;
                                $merchant_product = $this->M_order_merchant_admin->get_produk($params);
                                $product_order_content = "";
                                $product_order_content = "<table border='1'>
                                                            <thead>
                                                             <tr>
                                                                 <th>Produk</th>
                                                                 <th>Tipe Product</th>
                                                                 <th>Qty</th>
                                                             </tr>
                                                            </thead><tbody>";

                                foreach ($merchant_product as $product_merchant) {
                                    $product_order_content = $product_order_content . "<tr>
                                                                                       <td>" . $product_merchant->product_name . "</td>
                                                                                       <td>" . $product_merchant->variant_name . "</td>
                                                                                       <td>" . $product_merchant->qty . "</td></tr>";
                                }
                                $product_order_content = $product_order_content . "</tbody>
                                    
                                                                                   </table>";
                                $email_order4->ORDER_ITEMS = $product_order_content;
                                $email_order4->RECIPIENT_NAME = $value->merchant_name;
                                $email_order4->ORDER_NO = $params->order_no;
                                $email_order4->ORDER_DATE = parent::cdate($params->order_date);
                                $email_order4->RECIPIENT_ADDRESS = $address_content;
                                $email_order4->CONFIRM_LINK = base_url() . "merchant/transaction/merchant_delivery";
                                $email_order4->to_email = $value->email_merchant;
                                parent::email_template($email_order4);
                            }
                            if (isset($data[0]->member_seq)) {
                                $voucher = new stdClass();
                                $voucher->user_id = $data[0]->member_seq;
                                $voucher->ip_address = parent::get_ip_address();
                                $voucher->node_cd = NODE_SHOPPING_MEMBER;
                                $voucher->member_seq = $data[0]->member_seq;
                                $voucher->total_order = $params->total_order;
                                $promo = $this->M_voucher->get_promo($voucher);
                                if (isset($promo)) {
                                    if ($params->conf_pay_amt_admin > $promo[0]->trx_get_amt) {
                                        parent::generate_voucher($voucher, $total_order = $params->total_order);
                                    }
                                }
                            }
                        } else {
                            
                        }
                    } else {
                        if ($params->payment_status == PAYMENT_CANCEL_BY_ADMIN_STATUS_CODE) {
                            $params->payment_status = parent::get_input_post("payment_status");
                            $params->order_status = ORDER_CANCEL_BY_SYSTEM_STATUS_CODE;
                            $params->paid_date = parent::get_input_post("paid_date");
                            $params->conf_pay_amt_admin = parent::get_input_post("conf_pay_amt_admin");
                            $email_order1 = new stdClass();
                            $email_order1->user_id = parent::get_admin_user_id();
                            $email_order1->ip_address = parent::get_ip_address();
                            $email_order1->code = ORDER_PAY_CONFIRM_CANCEL_CODE;
                            $email_order1->RECIPIENT_NAME = $params->member_name;
                            $email_order1->ORDER_NO = $params->order_no;
                            $email_order1->ORDER_DATE = parent::cdate($params->order_date);
                            $email_order1->to_email = $data[0]->email_member === NULL ? $data[0]->email_agent : $data[0]->email_member;
                            parent::email_template($email_order1);

                            if ($data[0]->voucher_seq != "") {
                                $params->voucherseq = $data[0]->voucher_seq;
                                $list_voucher = parent::generate_voucher_refund($params);
                                $params->new_voucher_seq = $list_voucher->newvoucherseq;
                                $this->M_order_admin->save_new_voucher($params);
                            }
                            if (isset($sel_data)) {
                                foreach ($sel_data as $each) {
                                    $params->product_variant_seq = $each->product_variant_seq;
                                    $params->variant_value_seq = $each->variant_seq;
                                    $params->qty = $each->qty;
                                    $params->mutation_type = IN_MUTATION_TYPE;
                                    $params->trx_type = CANCEL_ACCOUNT_TYPE;
                                    $params->trx_no = $each->order_no;
                                    $this->M_product_stock_merchant->save_add_stock($params);
                                    $this->M_trans_log_merchant->save_merchant_stock_log($params);
                                }
                            }
                        } elseif ($params->payment_status == PAYMENT_FAILED_STATUS_CODE) {
                            $params->payment_status = parent::get_input_post("payment_status");
                            $params->order_status = ORDER_PREORDER_STATUS_CODE;
                            $params->paid_date = parent::get_input_post("paid_date");
                            $params->conf_pay_amt_admin = parent::get_input_post("conf_pay_amt_admin");
                            $email_order2 = new stdClass();
                            $email_order2->user_id = parent::get_admin_user_id();
                            $email_order2->ip_address = parent::get_ip_address();
                            $email_order2->code = ORDER_PAY_CONFIRM_FAIL_CODE;
                            $email_order2->RECIPIENT_NAME = $params->member_name;
                            $email_order2->ORDER_NO = $params->order_no;
                            $email_order2->ORDER_DATE = parent::cdate($params->order_date);
                            $email_order2->LINK = base_url("member/profile/payment_retry" . "/" . $params->order_no);
                            $email_order2->to_email = $data[0]->email_member === NULL ? $data[0]->email_agent : $data[0]->email_member;
                            parent::email_template($email_order2);
                        } else {
                            throw new Exception(ERROR_TOTAL);
                        }
                    }
                } else {
                    throw new Exception(ERROR_UPDATE);
                }
                $this->M_order_admin->trans_begin();
                $this->M_order_admin->save_update($params);
                $this->M_order_admin->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_order_admin->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_order_admin->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_order_admin->trans_rollback();
            }
        }
    }

//    protected function sendnotif_fail_payment($member_seq, $order_no) {
//        $params_notif_member->user_id = 'ADMIN';
//        $params_notif_member->ip_address = parent::get_ip_address();
//        $params_notif_member->code = FAIL_ORDER_BY_PAYMENT_ADMIN_MEMBER;
//        $params_notif_member->user_group_seq = '4';
//        $params_notif_member->member_seq = $member_seq;
//        $url_detail = base_url() . 'member/payment/' . $order_no;
//        $params_notif_member->order_no = "<a href='" . $url_detail . "' target='_blank'>" . $order_no . "</a>";
//        parent::notif_template($params_notif_member);
//    }
}

?>
