<?php

require_once CONTROLLER_BASE_ADMIN;

class C_change_password extends controller_base_admin {

    private $temp_data;
    private $data;

    public function __construct() {

        $this->data = parent::__construct("CON01005", "admin/profile/change_password");
        $this->initialize();
    }

    private function initialize() {

        $this->load->model('admin/profile/M_change_password');
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {

        if (!$this->input->post()) {
            $this->load->view("admin/profile/change_password", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("admin/profile/change_password", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    protected function save_update() {
        $params = new stdClass;
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->old_password = parent::get_input_post("old_password", true, FILL_VALIDATOR, "Password Lama", $this->data);
        $params->new_password = parent::get_input_post("new_password", true, FILL_VALIDATOR, "Password Baru", $this->data);

        if (strlen($params->old_password . $params->new_password) < 8 || strlen($params->old_password . $params->new_password) > 20) {
            $this->temp_data[DATA_ERROR][ERROR] = true;
            $this->temp_data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_VALIDATION_FILL_PASSWORD_LENGTH;
        }

        $params->encrypt_old_password = md5(md5($params->old_password));
        $params->encrypt_new_password = md5(md5($params->new_password));

        try {
            $list_data = $this->M_change_password->get_info($params);
            parent::set_list_data($this->temp_data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->temp_data, $ex);
        }

        if (isset($list_data[0])) {
            try {
                $list_data = new stdClass;
                $list_data->user_id = parent::get_admin_user_id();
                $list_data->ip_address = parent::get_ip_address();
                $list_data->new_password = parent::get_input_post("new_password");

                $list_data->encrypt_new_password = md5(md5($params->new_password));

                $this->M_change_password->trans_begin();
                $this->M_change_password->save_update($list_data);
                $this->M_change_password->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_change_password->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_change_password->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_change_password->trans_rollback();
            }
        } else {
            $this->data[DATA_ERROR][ERROR] = true;
            $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_INVALID_OLD_PASSWORD;
        }
    }

}

?>
