<?php

require_once CONTROLLER_BASE_ADMIN;

class C_login_admin extends controller_base_admin {

    private $temp_data;
    private $url;

    public function __construct() {
        parent::__construct("", "", false);
        $this->initialize();
    }

    private function initialize() {
        $err = [ERROR => false, ERROR_MESSAGE => ""];
        $this->temp_data[DATA_ERROR] = $err;
        $this->load->model('admin/M_login_admin');
    }

    public function index() {
        $this->url = parent::get_input_get("url");
        if (isset($_SESSION[SESSION_TEMP_DATA])) {
            $this->temp_data = $_SESSION[SESSION_TEMP_DATA];
            unset($_SESSION[SESSION_TEMP_DATA]);
        }
        $this->temp_data[DATA_AUTH][FORM_AUTH][FORM_URL] = "?url=" . $this->url;
        $this->load->view("admin/login_admin", $this->temp_data);
    }

    public function sign_in() {
        $params = new stdClass;
        $params->user_id = $this->input->post("user_id");
        $params->password = $this->input->post("password");
        $params->ip_address = $_SERVER['REMOTE_ADDR'];

        $this->temp_data[DATA_SELECTED][LIST_DATA] = $params;

        if (strlen($params->password) < 8 || strlen($params->password) > 20) {
            $this->temp_data[DATA_ERROR][ERROR] = true;
            $this->temp_data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_VALIDATION_FILL_PASSWORD_LENGTH;
        }

        $params->encrypt_password = md5(md5($params->password));

        if ($this->temp_data[DATA_ERROR][ERROR] === false) {

            try {
                $list_data = $this->M_login_admin->get_list($params);
                parent::set_list_data($this->temp_data, $list_data);
            } catch (Exception $ex) {
                parent::set_error($this->temp_data, $ex);
            }

            if (isset($list_data[0])) {
                foreach ($list_data[1] as $menu) {
                    $form_auth_list[$menu->menu_cd] = [
                        FORM_CD => $menu->menu_cd,
                        FORM_NAME => $menu->menu_name,
                        FORM_TITLE => $menu->title_name,
                        FORM_AUTH_SEARCH => ($menu->can_view == "1" ? true : false),
                        FORM_AUTH_VIEW => ($menu->can_view == "1" ? true : false),
                        FORM_AUTH_ADD => ($menu->can_add == "1" ? true : false),
                        FORM_AUTH_EDIT => ($menu->can_edit == "1" ? true : false),
                        FORM_AUTH_DELETE => ($menu->can_delete == "1" ? true : false),
                        FORM_AUTH_APPROVE => ($menu->can_auth == "1" ? true : false),
                        FORM_AUTH_PRINT => ($menu->can_print == "1" ? true : false)
                    ];
                }

                $admin_info[SESSION_ADMIN_UID] = $list_data[0][0]->user_id;
                $admin_info[SESSION_ADMIN_UNAME] = $list_data[0][0]->user_name;
                $admin_info[SESSION_ADMIN_USER_GROUP] = $list_data[0][0]->name;
                $admin_info[SESSION_ADMIN_LAST_LOGIN] = parent::cdate($list_data[0][0]->last_login, 1);
                $admin_info[SESSION_ADMIN_FORM_AUTH] = $form_auth_list;
                $admin_info[SESSION_ADMIN_CSRF_TOKEN] = base64_encode(openssl_random_pseudo_bytes(32));
                $admin_info[SESSION_IP_ADDR] = $_SERVER["REMOTE_ADDR"];

                $menulst = $this->M_login_admin->get_left_nav_admin($params->user_id);
//                var_dump($menulst); die();
                $admin_info[SESSION_ADMIN_LEFT_NAV] = $this->get_menu_left_nav($menulst, ROOT);
                $this->session->set_userdata($admin_info);
                $this->url = parent::get_input_get("url");
                if ($this->url<>"") {
                    redirect(base_url($this->url));
                } else {
                    redirect(base_url("admin/main_page"));
                }
            } else {
                $this->temp_data[DATA_ERROR][ERROR] = true;
                $this->temp_data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_INVALID_PASSWORD_AND_USER_NAME;
            }
        }
        $admin_info[SESSION_TEMP_DATA] = $this->temp_data;
        $this->session->set_userdata($admin_info);
        redirect(base_url("admin/login") . "?url=" . $this->url);
    }

//    protected function get_menu_left_nav($datas, $menu_cd, $parent_menu_cd = ROOT) {
//        static $i = 1;
//        $tab = str_repeat(" ", $i);
//        if (isset($datas[$menu_cd])) {
//            if ($parent_menu_cd == ROOT) {
//              $html = "<li class=\"treeview\">";
//            } else {
//                $html = "";
//            };          
//            $html.= "<ul class=\"treeview-menu\">" ; 
//            $i++;
//            foreach ($datas[$menu_cd] as $vals) {
//                $child = $this->get_menu_left_nav($datas, $vals->menu_cd, $vals->parent_menu_cd);
//                $html.= "$tab";
//                if ($child) {
//                    $i++;
//                    $html.="<li><a href=\"#\"> <i class='fa fa-gears'></i> <span>" . $vals->name . "</span> <i class=\"fa fa-angle-left pull-right\"></i></a>";
//                    $html .= $child;
//                    $html .= "$tab";
//                } //else {
////                    $i++;
////                    $html.="<li><a href=\"#\"> <i class='fa fa-gears'></i> <span>" . $vals->name . "</span> <i class=\"fa fa-angle-left pull-right\"></i></a>";
////                }
//                $html .= '</li>';
//            }
//            $html .= "$tab";
//            return $html;
//        } else {
//            return false;
//        }
//    }

    protected function get_menu_left_nav($datas, $parent) {
        static $i = 1;
        $tab = str_repeat(" ", $i);
        if (isset($datas[$parent])) {
            $html = "";
            $i++;
            foreach ($datas[$parent] as $vals) {
                $child = $this->get_menu_left_nav($datas, $vals->menu_cd);
                $html .= "$tab";
                if ($child) {
                    $i++;
                    $html.="<li class=\"treeview\"><a href=" . base_url() . $vals->url . "><i class='fa " . $vals->icon . "'></i> <span>" . $vals->name . "</span> <i class=\"fa fa-angle-left pull-right\"></i></a><ul class=\"treeview-menu\">";
                    $html .= $child;
                    $html .= "$tab";
                    $html .= '</ul></li>';
                } else {
                    $html.="<li><a href=" . base_url() . $vals->url . "> <i class='fa " . $vals->icon . "'></i> <span>" . $vals->name . "</span> <i class=\"fa fa-angle\"></i></a>";
                    $html .= '</li>';
                }
            }
            $html .= "$tab";
            return $html;
        } else {
            return false;
        }
    }

}

?>