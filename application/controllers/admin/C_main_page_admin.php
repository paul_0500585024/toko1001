<?php

require_once CONTROLLER_BASE_ADMIN;

class C_main_page_admin Extends controller_base_admin {

    private $data;

    public function __construct() {

        $this->data = parent::__construct("", "admin/main_page");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('admin/M_main_page_admin');
    }

    public function index() {

        $this->get_data();
        $this->get_chart();
        $this->load->view('admin/main_page_admin', $this->data);
    }

    public function sign_out() {

        unset($_SESSION[SESSION_ADMIN_UID]);
        unset($_SESSION[SESSION_ADMIN_UNAME]);
        unset($_SESSION[SESSION_ADMIN_USER_GROUP]);
        unset($_SESSION[SESSION_ADMIN_FORM_AUTH]);
        unset($_SESSION[SESSION_ADMIN_CSRF_TOKEN]);
        unset($_SESSION[SESSION_IP_ADDR]);
        unset($_SESSION[SESSION_ADMIN_LEFT_NAV]);

        $this->data[DATA_ERROR] = False;
        $this->load->view('admin/login_admin', $this->data);
    }

    public function get_data() {

        $params = new stdClass();
        $params->payment_status = PAYMENT_CONFIRM_STATUS_CODE;
        $params->trx_type = WITHDRAW_ACCOUNT_TYPE;
        $params->withdraw_status = NEW_STATUS_CODE;
        $params->new_merchant = WEB_STATUS_CODE;
        $params->product_status = NEW_STATUS_CODE;
        $params->cancel_trx_type = CANCEL_ACCOUNT_TYPE;
        $params->new_status_code = NEW_STATUS_CODE;
        $params->order_status = PROCESS_STATUS_CODE;

        try {
            $list_data = $this->M_main_page_admin->get_data($params);
            parent::set_list_data($this->data, $list_data);
            $this->data[ORDER_NEED_CONFIRMATION] = $list_data[0][0]->order;
            $this->data[WITHDRAW_NEED_CONFIRMATION] = $list_data[1][0]->withdraw;
            $this->data[NEW_MERCHANT] = $list_data[2][0]->new_merchant;
            $this->data[NEW_PRODUCT] = $list_data[3][0]->product;
            $this->data[PENDING_REFUND] = $list_data[6][0]->active_user;
            $this->data[PENDING_DELIVERY] = $list_data[5][0]->pending_delivery;
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    public function get_chart() {

        $this->chart_member();
        $this->chart_merchant();
    }

    public function chart_member() {
        $params = new stdClass();
        $params->january_from = '2016-01-01';
        $params->december_to = '2017-05-31';

        try {
            $list_data = $this->M_main_page_admin->charts_member($params);
            parent::set_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    public function chart_merchant() {

        $params = new stdClass();
        $params->from_date = '2016-09-01';
        $params->to_date = '2017-05-31';

        try {
            $list_data = $this->M_main_page_admin->charts_merchant($params);
            $this->data["active_user"] = $list_data;
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

}

?>
