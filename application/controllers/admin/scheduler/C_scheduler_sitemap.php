<?php

require_once CONTROLLER_BASE_ADMIN;

class C_scheduler_sitemap extends controller_base_admin {

    public function __construct() {
        $this->data = parent::__construct("", "", false);
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('admin/scheduler/M_scheduler_sitemap');
    }

    public function index() {

        $product_info = $this->M_scheduler_sitemap->get_all_product_list();
        $sitemap_handphone_content = SEO_HEADER_CONTENT;
        $sitemap_electronic_content = SEO_HEADER_CONTENT;
        $sitemap_computer_content = SEO_HEADER_CONTENT;
        $sitemap_fashion_content = SEO_HEADER_CONTENT;
        $sitemap_health_content = SEO_HEADER_CONTENT;
        $sitemap_camera_content = SEO_HEADER_CONTENT;
        $sitemap_household_content = SEO_HEADER_CONTENT;
        $sitemap_baby_and_toy_content = SEO_HEADER_CONTENT;
        $sitemap_bag_content = SEO_HEADER_CONTENT;
        $sitemap_music_and_sport_content = SEO_HEADER_CONTENT;
        $sitemap_otomotif_content = SEO_HEADER_CONTENT;
        $sitemap_food_and_beverage_content = SEO_HEADER_CONTENT;
        $sitemap_stationary_content = SEO_HEADER_CONTENT;
        foreach ($product_info as $product) {
            switch ($product->parent_seq) {
                case PRODUCT_CATEGORY_HANDPHONE : $sitemap_handphone_content .= $this->get_content($product);
                    break;
                case PRODUCT_CATEGORY_ELECTRONIC_AUDIO : $sitemap_electronic_content .= $this->get_content($product);
                    break;
                case PRODUCT_CATEGORY_COMPUTER : $sitemap_computer_content .=$this->get_content($product);
                    break;
                case PRODUCT_CATEGORY_FASHION : $sitemap_fashion_content .= $this->get_content($product);
                    break;
                case PRODUCT_CATEGORY_HEALTH_AND_COSMETIC : $sitemap_health_content .= $this->get_content($product);
                    break;
                case PRODUCT_CATEGORY_CAMERA : $sitemap_camera_content .= $this->get_content($product);
                    break;
                case PRODUCT_CATEGORY_HOUSEHOLD : $sitemap_household_content .= $this->get_content($product);
                    break;
                case PRODUCT_CATEGORY_BABY_AND_TOY : $sitemap_baby_and_toy_content .= $this->get_content($product);
                    break;
                case PRODUCT_CATEGORY_BAG : $sitemap_bag_content .= $this->get_content($product);
                    break;
                case PRODUCT_CATEGORY_MUSIC_AND_SPORT : $sitemap_music_and_sport_content .= $this->get_content($product);
                    break;
                case PRODUCT_CATEGORY_OTOMOTIF : $sitemap_otomotif_content .= $this->get_content($product);
                    break;
                case PRODUCT_CATEGORY_FOOD_AND_BEVERAGE : $sitemap_food_and_beverage_content .= $this->get_content($product);
                    break;
                case PRODUCT_CATEGORY_STATIONARY : $sitemap_stationary_content .= $this->get_content($product);
                    break;
            }
        }

        $myfile = fopen(SEO_PRODUCT_CATEGORY_HANDPHONE, "w") or die("Unable to open file!");
        fwrite($myfile, $sitemap_handphone_content . "\n" . SEO_FOOTER_CONTENT);
        fclose($myfile);

        $myfile = fopen(SEO_PRODUCT_CATEGORY_ELECTRONIC_AUDIO, "w") or die("Unable to open file!");
        fwrite($myfile, $sitemap_electronic_content . "\n" . SEO_FOOTER_CONTENT);
        fclose($myfile);

        $myfile = fopen(SEO_PRODUCT_CATEGORY_COMPUTER, "w") or die("Unable to open file!");
        fwrite($myfile, $sitemap_computer_content . "\n" . SEO_FOOTER_CONTENT);
        fclose($myfile);

        $myfile = fopen(SEO_PRODUCT_CATEGORY_FASHION, "w") or die("Unable to open file!");
        fwrite($myfile, $sitemap_fashion_content . "\n" . SEO_FOOTER_CONTENT);
        fclose($myfile);

        $myfile = fopen(SEO_PRODUCT_CATEGORY_HEALTH_AND_COSMETIC, "w") or die("Unable to open file!");
        fwrite($myfile, $sitemap_health_content . "\n" . SEO_FOOTER_CONTENT);
        fclose($myfile);

        $myfile = fopen(SEO_PRODUCT_CATEGORY_CAMERA, "w") or die("Unable to open file!");
        fwrite($myfile, $sitemap_camera_content . "\n" . SEO_FOOTER_CONTENT);
        fclose($myfile);

        $myfile = fopen(SEO_PRODUCT_CATEGORY_HOUSEHOLD, "w") or die("Unable to open file!");
        fwrite($myfile, $sitemap_household_content . "\n" . SEO_FOOTER_CONTENT);
        fclose($myfile);

        $myfile = fopen(SEO_PRODUCT_CATEGORY_BABY_AND_TOY, "w") or die("Unable to open file!");
        fwrite($myfile, $sitemap_baby_and_toy_content . "\n" . SEO_FOOTER_CONTENT);
        fclose($myfile);

        $myfile = fopen(SEO_PRODUCT_CATEGORY_BAG, "w") or die("Unable to open file!");
        fwrite($myfile, $sitemap_bag_content . "\n" . SEO_FOOTER_CONTENT);
        fclose($myfile);

        $myfile = fopen(SEO_PRODUCT_CATEGORY_MUSIC_AND_SPORT, "w") or die("Unable to open file!");
        fwrite($myfile, $sitemap_music_and_sport_content . "\n" . SEO_FOOTER_CONTENT);
        fclose($myfile);

        $myfile = fopen(SEO_PRODUCT_CATEGORY_OTOMOTIF, "w") or die("Unable to open file!");
        fwrite($myfile, $sitemap_otomotif_content . "\n" . SEO_FOOTER_CONTENT);
        fclose($myfile);

        $myfile = fopen(SEO_PRODUCT_CATEGORY_FOOD_AND_BEVERAGE, "w") or die("Unable to open file!");
        fwrite($myfile, $sitemap_food_and_beverage_content . "\n" . SEO_FOOTER_CONTENT);
        fclose($myfile);

        $myfile = fopen(SEO_PRODUCT_CATEGORY_STATIONARY, "w") or die("Unable to open file!");
        fwrite($myfile, $sitemap_stationary_content . "\n" . SEO_FOOTER_CONTENT);
        fclose($myfile);

    }

    private function get_content($product) {
        $product_variant = $product->variant_value_seq == DEFAULT_VALUE_VARIANT_SEQ ? "" : "-" . $product->value;
        $data_auth = date_create($product->auth_date);
//        return"";
        return "
                <url>
                    <loc>" . base_url() . strtolower(url_title($product->name) . $product_variant . "-" . $product->seq) . "</loc>
                    <lastmod>" . date_format($data_auth, "Y-m-d") . "</lastmod>
                    <changefreq>daily</changefreq>
                    <priority>1</priority>
                </url>";
    }

}

?>
