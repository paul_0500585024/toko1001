<?php

require_once CONTROLLER_BASE_ADMIN;

class C_scheduler_cashback extends controller_base_admin {

    public function __construct() {
        $this->data = parent::__construct("", "", false);
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('admin/scheduler/M_scheduler_cashback');
        $this->load->model('admin/master/M_agent_top_up_admin');
        $this->load->model(LIVEVIEW . ROUTE_AGENT . 'profile/M_profile_agent');
    }

    public function index() {
        $params = new stdClass();
        $params->user_id = parent::get_admin_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->order_status = ORDER_DELIVERED_STATUS_CODE;
        $params->settled = DEFAULT_SETTLED_OFF;
        $params->product_status = PRODUCT_READY_STATUS_CODE;
        try {
            $list_data = $this->M_scheduler_cashback->get_list($params);
//            var_dump($list_data);die();
            if (isset($list_data)) {
                $x = "";
                $view = "";
                $agent = "";
                $email = "";
                $total = "";
                foreach ($list_data as $each) {
                    $selected = new stdClass();
                    $selected->order_seq = $each->order_seq;
                    $selected->order_no = $each->order_no;
                    $selected->product_variant_seq = $each->product_variant_seq;
                    $selected->variant_value_seq = $each->variant_value_seq;
                    $selected->commission_fee_percent = $each->commission_fee_percent;
                    $selected->sell_price = $each->sell_price;
                    $selected->qty = $each->qty;
                    $selected->total = ($each->nominal_commision_agent);
                    /* insert to m_agent_account */
                    $selected->trx_no = $selected->order_no . "#" . $selected->product_variant_seq . "#" . $selected->variant_value_seq;
                    $selected->user_id = 'SYSTEM';
                    $selected->ip_address = parent::get_ip_address();
                    $selected->agent_seq = $each->agent_seq;
                    $selected->pg_method_seq = PAYMENT_SEQ_DEPOSIT;
                    $selected->non_deposit_trx_amt = '0';
                    $selected->mutation_type = DEBET_MUTATION_TYPE;
                    $selected->trx_type = CASH_ACCOUNT_TYPE;
                    $selected->deposit_trx_amt = $selected->total;
                    $selected->bank_name = $each->agent_bank_branch_name;
                    $selected->bank_branch_name = $each->agent_bank_branch_name;
                    $selected->bank_acct_no = $each->agent_bank_acct_no;
                    $selected->bank_acct_name = $each->agent_bank_acct_name;
                    $selected->conf_topup_date = DEFAULT_DATE;
                    $selected->conf_topup_type = NEW_STATUS_CODE;
                    $selected->conf_topup_file = '';
                    $selected->conf_topup_bank_seq = '';
                    $selected->settled_update = DEFAULT_SETTLED_ON;
                    $selected->status = APPROVE_STATUS_CODE;
                    if ($each->settled == DEFAULT_SETTLED_OFF) {
                        $this->M_profile_agent->trans_begin();
                        $this->M_profile_agent->save_add_agent_account($selected);
                        /* update for m_agent */
                        $this->M_agent_top_up_admin->save_approve($selected);
                        /* update for m_order_product */
                        $this->M_scheduler_cashback->save_update_product($selected);
                        $this->M_profile_agent->trans_commit();
                    }
                    if ($x != $each->order_seq) {
                        if ($x != "") {
                            $view.="</table>";
                            $email_params = new stdClass();
                            $email_params->user_id = "SYSTEM";
                            $email_params->ip_address = parent::get_ip_address();
                            $email_params->code = AGENT_COMMISSION_SUCCESS;
                            $email_params->RECIPIENT_NAME = $agent;
                            $email_params->ORDER_ITEMS = $view;
                            $email_params->TOTAL = number_format($total);
                            $email_params->PAYMENT_METHOD = 'CASH';
                            $email_params->ACCOUNT_LINK = base_url() . "member/";
                            $email_params->to_email = $email;
                            parent::email_template($email_params);
                            $view = "";
                        }
                        $view.="<style>.class {border: 1px solid black;}</style><table class='class' style='width:100%'>";
                        $view.="<tr style='border: 1px solid black;'>";
                        $view.="<td class='class'>No Order</td>";
                        $view.="<td class='class'>Tgl Order</td>";
                        $view.="<td class='class'>Produk</td>";
                        $view.="<td align='right' class='class'>Qty</td>";
                        $view.="<td align='right' class='class'>Harga</td>";
                        $view.="<td align='right' class='class'>Total</td>";
                        $view.="<td align='right' class='class'>Komisi</td>";
                        $view.="</tr>";
                        $view.="<tr>";
                        $view.="<td class='class'>" . $each->order_no . "</td>";
                        $view.="<td class='class'>" . parent::cdate($each->order_date) . "</td>";
                        $view.="<td class='class'>" . $each->product_name . "</td>";
                        $view.="<td align='right' class='class'>" . number_format($each->qty) . "</td>";
                        $view.="<td align='right' class='class'>" . number_format($each->sell_price) . "</td>";
                        $view.="<td align='right' class='class'>" . number_format($each->sell_price * $each->qty) . "</td>";
                        $view.="<td align='right' class='class'>" . number_format($each->nominal_commision_agent) . "</td>";
                        $view.="</tr>";
                        $x = $each->order_seq;
                    } else {
                        $view.="<tr>";
                        $view.="<td class='class'>" . $each->order_no . "</td>";
                        $view.="<td class='class'>" . parent::cdate($each->order_date) . "</td>";
                        $view.="<td class='class'>" . $each->product_name . "</td>";
                        $view.="<td align='right' class='class'>" . number_format($each->qty) . "</td>";
                        $view.="<td align='right' class='class'>" . number_format($each->sell_price) . "</td>";
                        $view.="<td align='right' class='class'>" . number_format($each->sell_price * $each->qty) . "</td>";
                        $view.="<td align='right' class='class'>" . number_format($each->nominal_commision_agent) . "</td>";
                        $view.="</tr>";
                    }
                    $agent = $each->agent_name;
                    $email = $each->agent_email;
                    $total = $each->total_new;
                }
                $view .="</table>";
                $email_params = new stdClass();
                $email_params->user_id = "SYSTEM";
                $email_params->ip_address = parent::get_ip_address();
                $email_params->code = AGENT_COMMISSION_SUCCESS;
                $email_params->RECIPIENT_NAME = $agent;
                $email_params->ORDER_ITEMS = $view;
                $email_params->TOTAL = number_format($total);
                $email_params->PAYMENT_METHOD = 'CASH';
                $email_params->ACCOUNT_LINK = base_url() . "member/";
                $email_params->to_email = $email;
                parent::email_template($email_params);
                $view = "";
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_profile_agent->trans_rollback();
        }
    }

}

?>
