<?php

require_once CONTROLLER_BASE_ADMIN;

class C_scheduler_rate_expedition_admin extends controller_base_admin {

    public function __construct() {
        $this->data = parent::__construct("", "", false);
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('admin/scheduler/M_scheduler_rate_expeidition_admin');
        $this->load->model('component/M_web_service');
        $this->load->library('curl');
    }

    public function index() {

        $rate_exp_list = $this->M_scheduler_rate_expeidition_admin->get_rate_expedition_list();

        $params = new stdClass();
        $params->user_id = "SYSTEM"; //parent::get_member_user_id();
        $params->ip_address = parent::get_ip_address();

//        var_dump($rate_exp_list);
        foreach ($rate_exp_list as $rate_exp) {
            $params->expedition_seq = $rate_exp->exp_seq;
            $params->from_district_code = $rate_exp->from_district_code;
            $params->to_district_code = $rate_exp->to_district_code;
            $params->exp_service_seq = $rate_exp->exp_service_seq;
            
            switch ($rate_exp->exp_code) {
                case JNE_EXPEDITION_CODE : $rate = $this->get_jne_expedition_rate($rate_exp);
                    break;
                case RAJA_KIRIM_EXPEDITION_CODE : $rate = $this->get_raja_kirim_expedition_rate($rate_exp);
                    break;
                default: $rate = null;
            }

            if (isset($rate) && $rate != 0) {
                $params->rate = $rate;
                $this->M_web_service->save_update_rate_cache($params);
            }
        }
        echo "finish";
    }

    protected function get_jne_expedition_rate($exp_info) {

        $params = 'username=' . API_USERNAME_JNE . '&api_key=' . API_KEY_JNE . '&from=' . $exp_info->from_district_code . '&thru=' . $exp_info->to_district_code . '&weight=1';

        $url = URL_JNE_API . '/tracing/' . strtolower(API_USERNAME_JNE) . '/price/';
        $this->curl->set_url($url);
        $this->curl->setIsUsingUserAgent(true);
        $this->curl->setParams($params);
        $this->curl->setData();
        $json_encoded = $this->curl->getResponse();
        $data_jsons = json_decode($json_encoded, true);

        if (!isset($data_jsons)) {
            return null;
        }

        foreach ($data_jsons as $key => $data) {
            if ($data == false) {
                return null;
            } else {
                foreach ($data as $rate) {
                    if ($exp_info->service_code == $rate["service_code"] || JNE_CTC_REG_CODE == $rate["service_code"]) {
                        return $rate["price"];
                    }
                }
            }
        }
        return null;
    }

    protected function get_raja_kirim_expedition_rate($exp_info) {

        $params = "method=pda_json_cek_harga&apiKey=" . API_KEY_RAJA_KIRIM . "&apiUser=" . API_USER_RAJA_KIRIM . "  &city_code=" . $exp_info->to_district_code . "&weight=1";

        $url = URL_RAJA_KIRIM_API;
        $this->curl->set_url($url);
        $this->curl->setIsUsingUserAgent(true);
        $this->curl->setParams($params);
        $this->curl->setData();
        $json_encoded = $this->curl->getResponse();
        $data_jsons = json_decode($json_encoded, true);

        foreach ($data_jsons as $key => $data) {
            foreach ($data as $rate) {
                if (isset($rate["err"])) {
                    return null;
                } else {
                    return $rate["harga"];
                }
            }
        }
        return null;
    }

}

?>