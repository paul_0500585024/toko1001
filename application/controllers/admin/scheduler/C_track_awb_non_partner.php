<?php

require_once CONTROLLER_BASE_ADMIN;

class C_track_awb_non_partner extends controller_base_admin {

    public function __construct() {

        $this->data = parent::__construct("", "", false);
        $this->initialize();
    }

    private function initialize() {
//        parent::__construct();
        $this->load->model('admin/scheduler/M_track_awb');
        $this->load->model('component/M_web_service');
        $this->load->library('curl');

        $this->load->library('track_awb/Request');
        $this->load->library('track_awb/Couriers');
//        $params = array("key" => '7e7d86a1-226d-4217-a8a8-80a94e14dde2');
        $this->load->library('track_awb/Trackings');
        $this->load->library('track_awb/LastCheckPoint');
    }

    public function index() {

        $selected = new stdClass;
        $selected->user_id = parent::get_admin_user_id();
        $selected->ip_address = parent::get_ip_address();
        $sel_data = $this->M_track_awb->get_data_awb($selected);
        try {
            if (isset($sel_data)) {				
                foreach ($sel_data as $value) {
                    //$this->M_track_awb->trans_begin();
                    $this->check_awb($value);
                    //$this->M_track_awb->trans_commit();
                }
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    public function check_awb($data) {
        $key = '7e7d86a1-226d-4217-a8a8-80a94e14dde2';
        $params = new stdClass();
        $params->user_id = "SYSTEM";
        $params->ip_address = parent::get_ip_address();
        $params->order_seq = $data->order_seq;
        $params->merchant_info_seq = $data->merchant_info_seq;
        
        try {
            $this->trackings->init($key);
            $this->couriers->init($key);
            $courier = $this->couriers->detect($data->awb_no);
            $tracking_info = array(
                'slug' => $courier["data"]["couriers"][0]["slug"],
                'title' => date('Ymd'),
            );

            //$this->trackings->delete($courier["data"]["couriers"][0]["slug"], $data->awb_no);
            $this->trackings->create($data->awb_no);
			sleep(30);
            $response = null;
            $response = $this->trackings->get($courier["data"]["couriers"][0]["slug"], $data->awb_no);
			var_dump($response);
            if (isset($response)) {
                if (strtoupper($response["data"]["tracking"]["tag"]) == DELIVERED_EXPEDITION_STATUS) {
                    $params->order_status = ORDER_DELIVERED_STATUS_CODE;
                    $params->received_date = $response["data"]["tracking"]["shipment_delivery_date"];
					$params->ship_date = $response["data"]["tracking"]["shipment_pickup_date"];
                    $params->received_by = "SYSTEM";                    
					$this->M_track_awb->save_update($params);
                }else if (strtoupper($response["data"]["tracking"]["tag"]) == ON_PROCCESS_EXPEDITION_STATUS){
                    $params->order_status = ORDER_SHIPPING_STATUS_CODE;
                    $params->received_date = "0000-00-00 00:00:00";
                    $params->received_by = "";
                    $params->ship_date = $response["data"]["tracking"]["shipment_pickup_date"];
					$this->M_track_awb->save_update($params);
                }
            }
        } catch (AftershipException $e) {
            echo $e->getMessage();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

}

?>
