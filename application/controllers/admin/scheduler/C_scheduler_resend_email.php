<?php

require_once CONTROLLER_BASE_ADMIN;

class C_scheduler_resend_email extends controller_base_admin {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("", "", false);
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('member/profile/M_profile_member');
    }

    public function index() {
        $params = new stdClass();
        $params->user_id = '';
        $params->ip_address = parent::get_ip_address();
        $params->member_seq = '3077';
        $data_member = $this->M_profile_member->get_data_member($params);

        if (isset($data_member)) {
            $params_email = new stdClass();
            $gencode = parent::generate_random_text(20, true);
            $params_email->user_id = '';
            $params_email->ip_address = parent::get_ip_address();
            $params_email->code = MEMBER_REG_CODE;
            $params_email->RECIPIENT_NAME = $data_member[0][0]->member_name;
            $params_email->email = $data_member[0][0]->email;
            $params_email->email_code = $gencode;
            $params_email->LINK = base_url() . VERIFICATION_MEMBER . $gencode;
            $params_email->to_email = $data_member[0][0]->email;
            parent::email_template($params_email);
        }
    }

}

?>
