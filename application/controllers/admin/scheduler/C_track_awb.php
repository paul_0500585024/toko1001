<?php

require_once CONTROLLER_BASE_ADMIN;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of c_province
 *
 * @author Jartono
 */
class C_track_awb extends controller_base_admin {

    public function __construct() {

	$this->data = parent::__construct("", "", false);
	$this->initialize();
    }

    private function initialize() {
	$this->load->model('admin/scheduler/M_track_awb');
	$this->load->library('curl');
	$this->load->model('component/M_web_service');
    }

    public function index() {

	$selected = new stdClass;
	$selected->user_id = parent::get_admin_user_id();
	$selected->ip_address = parent::get_ip_address();
	$selected->key = EXPEDITION_CHECK;
	$sel_data = $this->M_track_awb->get_data($selected);
	try {
	    foreach ($sel_data as $value) {
		$this->M_track_awb->trans_begin();
		$this->check_awb($value);
		$this->M_track_awb->trans_commit();
	    }
	    if (isset($sel_data)) {
		parent::set_data($this->data, $sel_data);
	    }
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	}
    }

    function check_awb($dataorder) {
	$expseq = $dataorder->exp_seq;
	$awbno = $dataorder->awb_no;
	$web_params = new stdClass();
	$awb_data = new stdClass();
	$awb_data->user_id = 'SYSTEM';
	$awb_data->ip_address = parent::get_ip_address();
	$awb_data->order_seq = $dataorder->order_seq;
	$awb_data->merchant_info_seq = $dataorder->merchant_info_seq;
	$status = '';
	$params = '';
	switch ($expseq) {
	    case "1": //    JNE
		$params = 'username=' . API_USERNAME_JNE . '&api_key=' . API_KEY_JNE;
		$url = URL_JNE_API_TRACE . strtolower(API_USERNAME_JNE) . '/list/cnote/' . $awbno;
		$this->curl->set_url($url);
		$this->curl->setIsUsingUserAgent(true);
		$this->curl->setParams($params);
		$this->curl->setData();
		$json_encoded = $this->curl->getResponse();
		$data_jsons = json_decode($json_encoded, true);
		if (isset($data_jsons['cnote']['pod_status']) && strtolower($data_jsons['cnote']['pod_status']) == 'delivered') {
		    $awb_data->order_status = 'D';
		    $awb_data->ship_date = $data_jsons['cnote']['cnote_date'];
		    $awb_data->received_date = date('Y-m-d H:i:s', strtotime($data_jsons['cnote']['cnote_pod_date']));
		    $awb_data->received_by = $data_jsons['cnote']['cnote_receiver_name'];
		    $this->save_update($awb_data);
		}
		$web_params->type = GET_AWB_TYPE;
		$web_params->function_name = FUNCTION_GET_JNE_AWB_TRACKING;
		$status = 'cheked';
		break;

	    case "5": //    Raja Kirim
		$params = 'method=' . FUNCTION_GET_RK_AWB_TRACKING . '&apiKey=' . API_KEY_RAJA_KIRIM . '&apiUser=' . API_USER_RAJA_KIRIM . '&no_awb=' . $awbno;
		$url = URL_RAJA_KIRIM_API_CHECK;
		$this->curl->set_url($url);
		$this->curl->setIsUsingUserAgent(true);
		$this->curl->setParams($params);
		$this->curl->setData();
		$json_encoded = $this->curl->getResponse();
		$data_jsons = json_decode($json_encoded, true);
		$status = 'cheked';
		$web_params->type = GET_AWB_TYPE;
		$web_params->function_name = FUNCTION_GET_RK_AWB_TRACKING;
		if (isset($data_jsons['result'][0]) && strtolower($data_jsons['result'][0]['status_barang']) == 'telah diterima') {
		    $awb_data->order_status = 'D';
		    $awb_data->ship_date = $data_jsons['result'][0]['tanggal_kirim'];
		    $awb_data->received_date = $data_jsons['result'][0]['tanggal_terima'];
		    $awb_data->received_by = $data_jsons['result'][0]['nama_penerima_surat'];
		    $this->save_update($awb_data);
		}
		break;
	}
	if ($status == 'cheked') {
	    $web_params->user_id = 'SYSTEM';
	    $web_params->ip_address = parent::get_ip_address();
	    $web_params->request_datetime = date('Y-m-d H:i:s');
	    $web_service_seq = $this->M_web_service->save_web_service($web_params);

	    $web_params->web_service_seq = $web_service_seq[0]->new_seq;
	    $web_params->request = $this->curl->getRequestHeaders() . $this->curl->getRequest();
	    $web_params->response = $this->curl->getResponseHeaders() . $this->curl->getResponse();
	    $web_params->response_datetime = date('Y-m-d H:i:s');
	    $this->M_web_service->save_update_service($web_params);
	}
    }

    function save_update($dataorder) {
	$this->M_track_awb->save_update($dataorder);
    }

}

?>
