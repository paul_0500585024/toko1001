<?php

require_once CONTROLLER_BASE_MERCHANT;

class C_scheduler_refund_order extends controller_base_merchant {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("", "", false);
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('admin/scheduler/M_scheduler_refund_order');
        $this->load->model('merchant/transaction/M_merchant_delivery');
    }

    public function index() {
        set_time_limit(500);
        $params = new stdClass();
        $params->user_id = '';
        $params->ip_address = parent::get_ip_address();
        $params->order_status = ORDER_CANCEL_BY_SYSTEM_STATUS_CODE;
        $params->product_status = ORDER_CANCEL_BY_MERCHANT_STATUS_CODE;
        $order_info = $this->M_scheduler_refund_order->get_list($params);
        if (isset($order_info)) {
            foreach ($order_info as $value) {
                $order_seq = $value->order_seq;
                $merchant_seq = $value->merchant_seq;
                $merchant_email = $value->email;
                $merchant_name = $value->name;
                $this->batal_order($order_seq, $merchant_seq, $merchant_email, $merchant_name);
                $params->seq = $order_seq;
                $params->merchant_seq = $merchant_seq;
                $this->M_scheduler_refund_order->save_update_by_system($params);
                $this->M_scheduler_refund_order->save_update_product_by_system($params);
            }
        }
    }

    protected function batal_order($orderseq, $merchant_seq, $merchant_email, $merchant_name) { // cancel order merchant
        $params = new stdClass();
        $params->refund_val = '0';
        $params->ttl_refund = '0';
        $params->old_voucher = '';
        $params->new_voucher = '';
        $selected = new stdClass();
        $selected->user_id = $merchant_seq;
        $selected->ip_address = parent::get_ip_address();
        $selected->seq = $orderseq;
        $selected->merchant_seq = $merchant_seq;
        $sel_data = $this->M_merchant_delivery->get_data($selected);
        $order_content = '';
        if (isset($sel_data)) {
            $totalmerchant = $sel_data[0][0]->total_merchant; // total order merchant
            $totalshipping = $sel_data[0][0]->total_ship_charged; // total order merchant
            $total_order_merchant_ship = ($totalmerchant + $totalshipping);
            $totalrefund = $total_order_merchant_ship;  // nilai yang dikembalikan sama dengan total order merchant+biaya kirim
            $params->refund_val = $totalrefund;
            $voucherseq = $sel_data[0][0]->voucher_seq; // cek ada voucher
            $order_content = "<table border='1'>
                           <thead>
                            <tr>
                                <th>Produk</th>
                                <th>Varian</th>
                                <th>Qty</th>
                                <th>Harga (Rp)</th>
                                <th>Biaya Kirim (Rp)</th>
                                <th>Total (Rp)</th>
                            </tr>
                           </thead><tbody>";
            $totalref = 0;
            foreach ($sel_data[1] as $item) {
                $order_content.="<tr><td>" . $item->name . "</td>
				<td>" . $item->variant_value . "</td>
				<td>" . number_format($item->qty) . "</td>
				<td>" . number_format($item->sell_price) . "</td>
				<td>" . number_format(ceil($item->qty * $item->weight_kg) * $item->ship_price_charged) . "</td>
				<td>" . number_format((ceil($item->qty * $item->weight_kg) * $item->ship_price_charged) +
                                ($item->qty * $item->sell_price)) . "</td></tr>";
                $totalref = $totalref + (ceil($item->qty * $item->weight_kg) * $item->ship_price_charged) +
                        ($item->qty * $item->sell_price);
            }
            $order_content .= "
			<tr><td colspan=5>Total</td><td>" . number_format($totalref) . "</td></tr>
			</tbody>
			    </table>";
            $params->order_item = $order_content;
// cek voucher
            if ($voucherseq > 0) {
                $voucherrefund = $sel_data[0][0]->voucher_refunded;   // cek sudah pernah refund voucher
                if ($voucherrefund == 0) {
                    $selected->voucherseq = $voucherseq;
                    $sel_voucher = $this->M_merchant_delivery->get_data_promo_voucher($selected);
                    $params->old_voucher = $sel_voucher[0]->code;
                    if ($totalmerchant >= $sel_voucher[0]->nominal) {   // cek nilai belanja >= nilai voucher
                        $newvoucherdata = parent::generate_voucher_refund($selected);
                        $params->new_voucher = $newvoucherdata->newvoucher;
                        $selected->newvoucherseq = $newvoucherdata->newvoucherseq;
                        $this->M_merchant_delivery->save_update_voucher_status($selected);
                        $totalrefund = ($total_order_merchant_ship - $sel_voucher[0]->nominal); // nilai yang dikembalikan setelah dikurang voucher
                    }
                }
            } else {
// cek coupon
                $nominal = 0;
                if ($sel_data[0][0]->coupon_seq > 0) {
                    $voucherrefund = $sel_data[0][0]->voucher_refunded;   // cek sudah pernah dibatalkan kuponnya
                    if ($voucherrefund == 0) {
                        $selected->couponseq = $sel_data[0][0]->coupon_seq;
                        $selected->statusproduct = "";
                        $sel_coupon = $this->M_merchant_delivery->get_data_promo_coupon($selected); // cek ada order produk dalam kupon dan mendapatkan nilai dari kupon
                        if ($sel_coupon[0]->nominal > 0) {   // cek nilai belanja >= nilai voucher
                            $this->M_merchant_delivery->save_update_coupon_status($selected); // update voucher_refunded di t_order
//			    $totalrefund = ($total_order_merchant_ship ); // nilai yang dikembalikan tetap
                        }
                    }
                }
            }
            $params->ttl_refund = $totalrefund;
            $selected->member_seq = $sel_data[0][0]->member_seq;
            $selected->agent_seq = $sel_data[0][0]->agent_seq;
            $selected->trxno = $sel_data[0][0]->ref_awb_no; // penggantian dari order_no biar bisa uniq di t_member_account    15-12-2015
            if ($sel_data[0][0]->pg_method_seq == PAYMENT_SEQ_CREDIT_CARD) {    // payment menggunakan CC, sementara dulu selama cancel pg masih kosong
                $selected->mutation_type = 'N';
                $selected->pg_method_seq = $sel_data[0][0]->pg_method_seq;
                $selected->depositamount = 0;
                $selected->nondepositamount = $totalrefund;
            } else {
                $selected->mutation_type = 'C';
                $selected->pg_method_seq = $sel_data[0][0]->pg_method_seq;
                $selected->depositamount = $totalrefund;
                $selected->nondepositamount = 0;
                if ($selected->agent_seq) {
                    $selected->conf_topup_bank_seq = $sel_data[0][0]->conf_pay_bank_seq;
                }
            }
            parent::generate_member_refund($selected);
            //$this->M_merchant_delivery->save_add_refund($selected);
            /* 	 batal per 10/12/2015 agar bisa dicek nilai aslinya
              $selected->new_sell_price = 0;
              $selected->new_ship_price_real = 0;
              $selected->new_ship_price_charged = 0;
              $selected->new_ins_rate_percent = 0;
              $this->M_merchant_delivery->save_update_order_merchant($selected);  // save new value t_order_merchant
             */

            //data member
            $selected = new stdClass();
            $selected->user_id = $merchant_seq;
            $selected->ip_address = parent::get_ip_address();
            $selected->member_seq = $sel_data[0][0]->member_seq;
            $selected->agent_seq = $sel_data[0][0]->agent_seq;
            if ($selected->member_seq) {
                $data_member = $this->M_merchant_delivery->get_data_member($selected);
            } else {
                $data_member = $this->M_merchant_delivery->get_data_agent($selected);
            }

            //data admin
            $admin_email = explode(', ', ADMIN_TOKO1001_EMAIL);
            foreach ($admin_email as $each_admin_email) {
                $each_data_admin = new stdClass();
                $each_data_admin->email = $each_admin_email;
                $each_data_admin->name = ADMIN_TOKO1001_NAME;
                $data_admin[] = $each_data_admin;
            }

            //data merchant
            $each_data_merchant = new stdClass();
            $each_data_merchant->email = $merchant_email;
            $each_data_merchant->name = $merchant_name;
            $data_merchant[] = $each_data_merchant;
            if ($selected->member_seq) {
                $this->sendemail($sel_data, $params, $merchant_seq, $data_member, "SYSTEM_CANCEL_ORDER_TO_MEMBER");
            } else {
                $this->sendemail($sel_data, $params, $merchant_seq, $data_member, "SYSTEM_CANCEL_ORDER_TO_AGENT");
            }

            $this->sendemail($sel_data, $params, $merchant_seq, $data_admin, "SYSTEM_CANCEL_ORDER_TO_ADMIN");
            $this->sendemail($sel_data, $params, $merchant_seq, $data_merchant, "SYSTEM_CANCEL_ORDER_TO_MERCHANT");
        }
    }

    protected function sendemail($dataorder, $data_dtl, $merchant_seq, $data_receiver, $code) {
        foreach ($data_receiver as $each_data_receiver) {
            $params = new stdClass;
            $params->user_id = $merchant_seq;
            $params->ip_address = parent::get_ip_address();
            $params->code = $code;
            $params->to_email = $each_data_receiver->email;
            $params->ORDER_NO = $dataorder[0][0]->order_no;
            $params->RECIPIENT_NAME = $each_data_receiver->name;
            $params->ORDER_DATE = parent::cdate($dataorder[0][0]->order_date);
            $params->ORDER_ITEMS = $data_dtl->order_item;
            $params->RECIPIENT_ADDRESS = $dataorder[0][0]->receiver_address;
            $params->REFUND_VALUE = parent::cnum($data_dtl->refund_val);
            $params->TOTAL_REFUND = parent::cnum($data_dtl->ttl_refund);
            $params->DEPOSIT_LINK = base_url() . 'member';
            $params->OLD_VOUCHER = $data_dtl->old_voucher;
            $params->NEW_VOUCHER = $data_dtl->new_voucher;
            $params->VOUCHER_LINK = base_url() . 'member';
            parent::email_template($params);
        }
    }

}