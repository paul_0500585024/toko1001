<?php

require_once CONTROLLER_BASE_ADMIN;

class C_scheduler_order_admin extends controller_base_admin {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("", "", false);
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('admin/scheduler/M_scheduler_order_admin');
        $this->load->model('merchant/master/M_product_stock_merchant');
        $this->load->model('merchant/transaction/M_trans_log_merchant');
    }

    public function index() {
        set_time_limit(500);
        $params = new stdClass();
        $params->user_id = '';
        $params->ip_address = parent::get_ip_address();
        $order_info = $this->M_scheduler_order_admin->get_list($params);

        if (isset($order_info)) {

            if ($order_info[0]->payment_status == PAYMENT_UNPAID_STATUS_CODE || $order_info[0]->payment_status == PAYMENT_FAILED_STATUS_CODE || $order_info[0]->payment_status == PAYMENT_WAIT_CONFIRM_STATUS_CODE || $order_info[0]->payment_status == PAYMENT_WAIT_CONFIRM_THIRD_PARTY) {
                foreach ($order_info as $value) {
                    $params->key = $value->seq;
                    $params->seq = $value->seq;
                    $params->payment_status = PAYMENT_CANCEL_BY_ADMIN_STATUS_CODE;
                    $params->paid_date = date('d-M-Y');
                    $params->conf_pay_amt_admin = '0';
                    $params->order_status = ORDER_CANCEL_BY_SYSTEM_STATUS_CODE;
                    $params->voucherseq = $value->voucher_seq;

                    $params->code = ORDER_PAY_CONFIRM_CANCEL_CODE;
                    $params->RECIPIENT_NAME = $value->member_name;
                    $params->ORDER_NO = $value->order_no;
                    $params->ORDER_DATE = parent::cdate($value->order_date);
                    $params->to_email = $value->email_member;

                    $list_data = $this->M_scheduler_order_admin->save_update($params);
                    parent::email_template($params);
                    if ($params->voucherseq != "") {
                        $list_voucher = parent::generate_voucher_refund($params);
                        $params->new_voucher_seq = $list_voucher->newvoucherseq;
                        $this->M_scheduler_order_admin->save_new_voucher($params);
                    }
                    $params->key = $list_data[0]->seq;
                    $product = $this->M_scheduler_order_admin->get_produk($params);
                    foreach ($product as $data) {
                        $params->product_variant_seq = $data->product_variant_seq;
                        $params->variant_value_seq = $data->variant_value_seq;
                        $params->qty = $data->qty;
                        $params->mutation_type = IN_MUTATION_TYPE;
                        $params->trx_type = CANCEL_ACCOUNT_TYPE;
                        $params->trx_no = $data->order_no;
                        $this->M_product_stock_merchant->save_add_stock($params);
                        $this->M_trans_log_merchant->save_merchant_stock_log($params);
                    }
                }
            }
        }
    }

}

?>
