<?php

require_once APPPATH . 'system/base.php';

class C_test_email extends CI_Controller {

    public function index(){
        $this->send_email();
    }
    
    protected function send_email() {
        $ci = get_instance();
        $ci->load->library('email');

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => "mail.evercoss.co.id",
            'smtp_port' => 26,
            'smtp_user' => "no-reply@evercoss.co.id",
            'smtp_pass' => "n234987!",
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE,
            'newline' => "\r\n"
        );
		
        $content = '<html>
    <head>
        <title>Toko1001 - Email</title>
		<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Ubuntu" />
        <style>
		body{
			font-family: Ubuntu;
			background:#eee;
		}
		table{
			font-size: 0.9em;
			width: 600px;
			background: #fff;
			border: 1px solid #bbb;
			border-collapse: collapse;
		}
		td{
			text-align: center;
		}
		.text-right {text-align: right} 
		.text-left {text-align: left} 
		.text-center {text-align: center} 
		p{
			font-size:0.75em;
		}
		a.sosmed{
			font-size:0.75em;
			color:#fff;
			text-decoration: none;
		}
        </style>
    </head>
    <body>
        <center>        
            <table><caption>Jika Anda tidak bisa membaca email ini, silahkan klik <a href="https://toko1001.id/email/1484037693md2b5w9IBp">disini</a></caption>
                <tr bgcolor="#042E3A">
                    <td style="padding-top:5px;padding-bottom:5px;"><img src="https://toko1001.id/assets/img/home/file/logo_w.png" alt="Toko1001"></td>
                </tr>
                <tr>
                    <td style="padding-top:30px;padding-bottom:30px;"><center>Halo Toko,<b> Intex Indonesia</b></center><br>
 <center>Password login anda telah diubah, silahkan login sebagai merchant dengan email <br> <b>Yenli@hip.co.id</b> <br>dan password sementara <br><b style="font-size:20px;color:#007dc6;">[PASSWORD]</b><br>
 
 Segera login dan ubah password anda. Terima Kasih.<br><br>
<a href="https://toko1001.id/merchant/login" target="_blank"> Login Sebagai Merchant</a>
</center></td>
                </tr>
                <tr>
                    <td>Salam Hangat,<br/><br/>Toko1001<br/><br/></td>
                </tr>
                <tr>
                    <td>
                        <hr>
                        <p><b>Hubungi Kami</b></p><br />
                        <div>
                            <a class="sosmed" href="https://instagram.com/toko1001.id"><img src="https://toko1001.id/assets/img/home/INSTAGRAM.png"></a>
                            <a class="sosmed" href="https://twitter.com/toko1001_id"><img src="https://toko1001.id/assets/img/home/TWITTER.png"></a>
                            <a class="sosmed" href="https://www.facebook.com/toko1001.id"><img src="https://toko1001.id/assets/img/home/FACEBOOK.png"></a>
                        </div>
						<br />
                        <p>Jl. Indokarya Barat I Blok D No. 1<br/> Jakarta 14340, Indonesia</p>
                        <p>Telp: 021-6514224/5</p>
                        <p>Email: cc@toko1001.id</p>
						<br />
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>';
        $ci->email->initialize($config);
        $ci->email->from("no-reply@evercoss.co.id", "Evercoss id");
        $ci->email->to("sc11270@gmail.com");
        $ci->email->subject("perubahan password test oleh admin Toko1001");
        $ci->email->message($content);
        if (!$ci->email->send()) {
			echo "gagal";
			echo $ci->email->print_debugger();
            $data[DATA_ERROR][ERROR] = true;
            $data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_SEND_EMAIL;
            return '0';
        } else {
			echo "berhasil";
            return '1';
        }
    }

}

?>
