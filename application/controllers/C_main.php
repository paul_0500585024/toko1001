<?php

require_once CONTROLLER_BASE_HOME;

class C_main extends controller_base_home {

    public function __construct() {
        parent::__construct("", "home/home", false);
        //$this->load->model("system/m_login");
        $this->load->helper('url');
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('home/M_thumbs_new_products');
        $this->load->model('home/M_thumbs_floor');
        $this->load->model('home/M_thumbs_banner');
        $this->load->helper('cookie');
    }

    public function index() {
        //$this->session->unset_userdata(SESSION_LOGGED_IN);
        //$this->session->unset_userdata(SESSION_CSRF_TOKEN);
        //$this->session->unset_userdata(SESSION_FORM_AUTH);

        if (parent::get_input_get("e")) {
            $this->search();
        }

        $params = new stdClass;
        $params->start = 0;
        $params->limit = 5;
        $params->where = '';
        $params->order = '';
        $params->agent = parent::is_agent() ? AGENT : '';
        $params->agent_seq = parent::is_agent() ? parent::get_agent_user_id() : "";

        $query_new_product = $this->M_thumbs_new_products->get_thumbs_new_products($params);
        $query_floor = $this->M_thumbs_floor->get_floor();
        $query_banner = $this->M_thumbs_banner->get_banner();
        $data['row'] = $query_new_product['row'];
        $data['new_products'] = $query_new_product['product'];
        $data['floor'] = $query_floor;
        $data['banner'] = $query_banner;
        $data['tree_category'] = $this->get_tree_category(true);

        if ($this->session->userdata(SESSION_MEMBER_CSRF_TOKEN) != Null) {
            $data[SESSION_MEMBER_UNAME] = $this->session->userdata(SESSION_MEMBER_UNAME);
            $data[MEMBER_SESSION] = $this->session->userdata(SESSION_MEMBER_CSRF_TOKEN);
        }

        if (parent::get_input_post('newm')) {
            $this->register_member();
            die();
        }

        if ($this->input->cookie('newcomer') == '') {
            $cookie = array(
                'name' => 'newcomer',
                'value' => 1,
                'expire' => (3600 * 1000 * 24 * 30),
                'secure' => false
            );
            $this->input->set_cookie($cookie);
            $data['popupbanner'] = 0;
        } else {
            $data['popupbanner'] = 1;
        }

        $this->template->view('home/home', $data);
    }

    function register_member() {
        $msg = '';
        if (parent::get_input_post('snk') == '') {
            $msg = "Persetujuan " . " " . ERROR_VALIDATION_MUST_FILL;
        }
        if (parent::get_input_post('jk') == '') {
            $msg = "Jenis kelamin " . " " . ERROR_VALIDATION_MUST_FILL;
        }
        if (parent::get_input_post('qemail') == '') {
            $msg = "Email" . " " . ERROR_VALIDATION_MUST_FILL;
        }
        if (strlen(parent::get_input_post('qemail')) > MAX_EMAIL_LENGTH) {
            $msg = "Email" . " " . ERROR_VALIDATION_LENGTH_MAX . MAX_EMAIL_LENGTH;
        }
        if (filter_var(parent::get_input_post('qemail'), FILTER_VALIDATE_EMAIL) === false) {
            $msg = ERROR_VALIDATION_FILL_EMAIL_FORMAT;
        }
        if ($msg == '') {
            $this->load->model("member/master/M_signup_member");
            $params = new stdClass();
            $params->email = parent::get_input_post('qemail');
            $result = $this->M_signup_member->check_user_exist($params);
            if (isset($result)) {
                $msg = ERROR_EMAIL_CHECK;
            } else {
                $gencode = parent::generate_random_text(20, true);
                $params->user_id = '';
                $params->ip_address = parent::get_ip_address();
                $params->name = $params->email;
                $params->date = "";
                $params->phone = "";
                $params->gender = parent::get_input_post('jk');
                $passid = parent::generate_random_alnum(8);
                $params->password = md5(md5($passid));
                $params->code = MEMBER_REG_PROMO;
                $params->email_code = $gencode;
                $params->RECIPIENT_NAME = "Member";
                $params->LINK = base_url() . VERIFICATION_MEMBER . $gencode;
                $params->to_email = $params->email;
                try {
                    $this->M_signup_member->trans_begin();
                    $this->M_signup_member->save_add($params);
                    $params->password = $passid;
                    parent::email_template($params);
                    $this->M_signup_member->trans_commit();
                    $msg = "OK";
                } catch (BusisnessException $ex) {
                    parent::set_error($this->data, $ex);
                    $this->M_signup_member->trans_rollback();
                    $msg = "Pendaftaran gagal";
                } catch (TechnicalException $ex) {
                    parent::set_error($this->data, $ex);
                    $this->M_signup_member->trans_rollback();
                    $msg = "Pendaftaran gagal";
                } catch (Exception $ex) {
                    parent::set_error($this->data, $ex);
                    $this->M_signup_member->trans_rollback();
                    $msg = "Pendaftaran gagal";
                }
            }
        }
        die($msg);
    }

    function search() {
        $params = new stdClass;
        $params->category = parent::get_input_get("c");
        $params->limit = 5;
        $params->search = str_replace('%', '', parent::get_input_get("e"));
        $product = '';
        $merchant = '';
        if ($params->search == '' || strlen($params->search) < 2)
            die('{"product":"","merchant":""}');
        try {
            $list_data = $this->M_thumbs_new_products->get_search_home($params);
            if (isset($list_data[0])) {
                $product = '"product" : ' . json_encode($list_data[0]);
            } else {
                $product = '"product":""';
            }
            if (isset($list_data[1])) {
                $merchant = '"merchant" : ' . json_encode($list_data[1]);
            } else {
                $merchant = '"merchant":""';
            }
            die("{" . $product . "," . $merchant . "}");
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        die('{"product":"","merchant":""}');
    }

}

?>
