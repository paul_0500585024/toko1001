<?php

require_once CONTROLLER_BASE_MEMBER;
require_once APPPATH . "controllers/member/wsdl/Client.php";

class C_payment_member extends controller_base_member {

    private $data;
    private $order_no;
    private $tipe;
    private $get_user_id;
    private $get_user_seq;

    public function __construct() {
        $segments = explode('/', trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/'));
        $this->data = parent::__construct("TRX01002", "member/payment", true, $segments[2]);
        $this->load->helper('cookie');
        $this->initialize();
    }

    private function initialize() {
        $this->order_no = $this->uri->segment(3);

        $this->load->model('member/M_order_member');
        $this->load->model('member/M_payment_member');
        $this->load->model('member/M_deposit_member');
        $this->load->model('admin/transaction/M_order_merchant_admin');

        $this->load->model('member/profile/M_profile_member');
        $this->load->model('component/M_radio_button');
        $this->load->model('admin/component/M_radio_button');
        $this->load->library('curl');
        $this->load->library('curl_dimo');

        if (parent::is_agent()) {
            $this->tipe = AGENT;
            $this->get_user_id = parent::get_agent_user_id();
            $this->get_user_seq = parent::get_agent_user_seq();
        } else {
            $this->tipe = "";
            $this->get_user_id = parent::get_member_user_id();
            $this->get_user_seq = parent::get_member_user_seq();
        }

        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_add");
        parent::register_event($this->data, ACTION_SAVE_DELETE, "save_delete");
        parent::register_event($this->data, ACTION_ADDITIONAL, "get_ajax");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {

        $params = new stdClass();
        $params->user_id = $this->get_user_id;
        $params->ip_address = parent::get_ip_address();
        $params->key = $this->order_no;
        $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $params->key = $this->order_no;
        $this->data['tree_category'] = $this->get_tree_category(true);
        $this->data['title'] = 'Daftar Pesanan';

        try {
            $order_info = $this->M_order_member->get_order_info_by_order_no($params, $this->tipe);
            $this->data["total_instalment"] = isset($order_info[0][0]->total_installment) ? $order_info[0][0]->total_installment : "";
            if (isset($order_info[0][0])) {
                if (isset($order_info[0][0]->pg_method_seq) && ($order_info[0][0]->pg_method_seq == PAYMENT_SEQ_ADIRA_DEPOSIT || $order_info[0][0]->pg_method_seq == PAYMENT_SEQ_ADIRA)) {
                    $params->customer_seq = $order_info[0][0]->customer_seq;
                    $this->data['customer_table'] = parent::create_table_customer_info($params);
                    $params->order_seq = $order_info[0][0]->seq;
                    $this->data['table_simulation'] = parent::create_table_customer_simulation_agent($params);
                    $params->agent_seq = $this->get_user_seq;
                    $list_data = $this->M_profile_member->get_data_agent($params);
                    $this->data['member_info'] = $list_data;
                } else {
                    $this->data['table_simulation'] = '';
                }
            }
            $task_type = parent::get_input_post("type");

            if ($task_type == TASK_UPDATE_CC_LOG) {
                $this->save_log_payment_credit_card($order_info[0][0]);
            }

            if ($task_type == TASK_UPDATE_DOCU_LOG) {
                $this->save_log_payment_docu($order_info[0][0]);
            }

            if (isset($order_info[0])) {
                $data_seq = $this->get_user_seq;
                if (parent::is_agent()) {
                    $order_info_seq = $order_info[0][0]->agent_seq;
                } else {
                    $order_info_seq = $order_info[0][0]->member_seq;
                }

                if ($order_info_seq != $data_seq) {
                    redirect(base_url() . "error_404");
                } elseif ($order_info[0][0]->payment_status == PAYMENT_UNPAID_STATUS_CODE || $order_info[0][0]->payment_status == PAYMENT_WAIT_CONFIRM_STATUS_CODE) {
                    if ($order_info[0][0]->total_payment > 0) {
                        $this->check_payment_gateway_method($order_info[0][0]);
                        $this->M_payment_member->trans_begin();
                        $this->get_payment_type_order($order_info[0][0], $order_info);
                        $this->M_payment_member->trans_commit();
                    } else {
                        $this->M_payment_member->trans_begin();
                        $this->save_success_order_payment($order_info[0][0]);
                        $this->M_payment_member->trans_commit();
                    }
                    $order_info_new = $this->M_order_member->get_order_info_by_order_no($params);
                    if ($this->is_member_get_axa($order_info_new)) {
                        //Show axa form
                        $params->member_seq = $order_info_new[0][0]->member_seq;
                        $list_data = $this->M_profile_member->get_data_member($params);
                        $this->data[DATA_SELECTED][LIST_DATA] = $list_data;
                        $this->template->view('home/axa', $this->data);
                    } else {
                        $this->data[DATA_ORDER] = $this->M_order_member->get_order_info_by_order_no($params, $this->tipe);
                        $this->data[DATA_BANK] = $this->M_radio_button->get_bank_list();
                        if (isset($order_info[0][0]->promo_credit_seq) && $order_info[0][0]->promo_credit_seq > 0) {
                            $params->promo_credit_seq = $order_info[0][0]->promo_credit_seq;
                            $this->data[DATA_CREDIT_INFO] = $this->M_order_member->get_credit_info_by_credit_seq($params);
                        }
                        $this->template->view('home/payment/confirm', $this->data);
                    }
                } elseif ($order_info[0][0]->payment_status == PAYMENT_FAILED_STATUS_CODE && $order_info[0][0]->payment_code != PAYMENT_TYPE_BCA_KLIKPAY && strlen($order_info[0][0]->order_no) > 15) {
                    redirect(base_url('member/profile/payment_retry' . '/' . $order_info[0][0]->order_no));
                } else {
                    if ($this->is_member_get_axa($order_info)) {
                        //Show axa form
                        $params->member_seq = $order_info[0][0]->member_seq;
                        $list_data = $this->M_profile_member->get_data_member($params);
                        $this->data[DATA_SELECTED][LIST_DATA] = $list_data;
                        $this->template->view('home/axa', $this->data);
                    } else {
                        if (isset($order_info[0][0]->promo_credit_seq) && $order_info[0][0]->promo_credit_seq > 0) {
                            $params->promo_credit_seq = $order_info[0][0]->promo_credit_seq;
                            $this->data[DATA_CREDIT_INFO] = $this->M_order_member->get_credit_info_by_credit_seq($params);
                        }

                        $this->data[DATA_ORDER] = $order_info;
                        $this->data[DATA_BANK] = $this->M_radio_button->get_bank_list();
                        $this->data['title'] = 'Konfirmasi Pembayaran Anda';
                        $this->template->view('home/payment/confirm', $this->data);
                    }
                };
            } else {
                redirect(base_url() . "error_404");
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_profile_member->trans_rollback();
            $this->data[DATA_ORDER] = $order_info;
            $this->data[DATA_BANK] = $this->M_radio_button->get_bank_list();
            $this->data['title'] = 'Konfirmasi Pembayaran Anda';
            $this->template->view('home/payment/confirm', $this->data);
        }
    }

    protected function check_payment_gateway_method($data) {
        $data->user_id = $this->get_user_id;
        $data->ip_address = parent::get_ip_address();
        switch ($data->payment_code) {
            case PAYMENT_TYPE_BANK: {
//do nothing
                };
                break;
            case PAYMENT_TYPE_BCA_KLIKPAY: {
//do nothing
                };
                break;
            case PAYMENT_TYPE_CREDIT_CARD: {
                    
                };
                break;
            case PAYMENT_TYPE_DEPOSIT: {
                    if (parent::is_agent()) {
                        $member = $this->M_profile_member->get_data_agent($data);
                    } else {
                        $member = $this->M_profile_member->get_data_member($data);
                    }
                    if ($member[2][0]->deposit_amt < $data->total_payment) {
                        throw new Exception(ERROR_DEPOSIT_EXCEDEED);
                    };
                };
                break;
            case PAYMENT_TYPE_MANDIRI_KLIKPAY: {
//do nothing
                };
                break;
        }
    }

    public function get_payment_type_order($data, $data_order_merchant_product) {
        switch ($data->payment_code) {
            case PAYMENT_TYPE_BANK:$this->payment_type_bank($data);
                break;
            case PAYMENT_TYPE_DEPOSIT: $this->payment_type_deposit($data);
                break;
            case PAYMENT_TYPE_MANDIRI_KLIKPAY: $this->payment_type_mandiri_click_pay($data);
                break;
            case PAYMENT_TYPE_BCA_KLIKPAY: $this->payment_type_bca_klikpay($data, $data_order_merchant_product);
                break;
            case PAYMENT_TYPE_CREDIT: $this->payment_type_credit_card($data);
                break;
            case PAYMENT_TYPE_CREDIT_CARD: $this->payment_type_credit_card($data);
                break;
            case PAYMENT_TYPE_MANDIRI_ECASH : $this->payment_type_mandiri_ecash($data);
                break;
            case PAYMENT_TYPE_DOCU_ATM : $this->payment_type_docu_atm($data);
                break;
            case PAYMENT_TYPE_DOCU_ALFAMART : $this->payment_type_docu_alfamart($data);
                break;
            case PAYMENT_TYPE_QR_CODE : $this->payment_type_qr_code($data);
                break;
            case PAYMENT_TYPE_ADIRA_BANK:
                $payment_type_a = parent::get_input_post("payment_type_a");

                if ($data->payment_status == PAYMENT_UNPAID_STATUS_CODE) {
                    $this->payment_type_adira($data);
                }
                if ($payment_type_a != "") {
                    if ($payment_type_a == PAYMENT_TYPE_DEPOSIT) {

                        $member = $this->M_profile_member->get_data_agent($data);
                        if ($member[2][0]->deposit_amt < $data->total_installment) {
                            throw new Exception(ERROR_DEPOSIT_EXCEDEED);
                        } else {
                            $this->payment_type_deposit($data);
                        }
                    } else {
                        $this->payment_type_bank($data);
                    }
                }
                break;
        }
    }

    public function payment_type_qr_code($data) {
        $params = new stdClass();
        $params->user_id = $this->get_user_id;
        $params->ip_address = parent::get_ip_address();
        $params->order_seq = $data->seq;
        $params->order_no = $data->order_no;
        $params->payment_status = PAYMENT_WAIT_CONFIRM_STATUS_CODE;
        $params->order_status = ORDER_PREORDER_STATUS_CODE;
        $params->paid_date = date('Y-m-d H:i:s');

        $order_params = new stdClass();
        $order_params->user_id = $this->get_user_id;
        $order_params->ip_address = parent::get_ip_address();
        $order_params->key = $data->order_no;

        $order_info = $this->M_order_member->get_order_info_by_order_no($order_params);
        try {
            if ($order_info[0][0]->signature == "") {
                $params->signature = $this->create_invoice_qr($data, $order_info[0][0]->total_payment);
                $this->M_payment_member->save_update_status_order($params);
                $this->M_order_member->save_update_order_signature($params);
            } else {
                $params->signature = $order_info[0][0]->signature;
            }
            $params->display_issuer_list = $this->get_issuer($data, $params->signature);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_payment_member->trans_rollback();
        }

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;
    }

    public function payment_type_deposit($data) {
        $params = new stdClass();
        $params->user_id = $this->get_user_id;
        $params->ip_address = parent::get_ip_address();
        $params->order_seq = $data->seq;
        $params->order_no = $data->order_no;
        $params->payment_status = PAYMENT_PAID_STATUS_CODE;
        $params->order_status = ORDER_NEW_ORDER_STATUS_CODE;
        $params->paid_date = date('Y-m-d H:i:s');

        $params_payment = new stdClass();
        $params_payment->user_id = $this->get_user_id;
        $params_payment->ip_address = parent::get_ip_address();
        $params_payment->order_seq = $data->seq;
        $params_payment->order_no = $data->order_no;
        $params_payment->payment_method_seq = $data->pg_method_seq;
        $params_payment->request = "";
        $params_payment->request_date = "";
        $params_payment->response = "";
        $params_payment->response_date = "";
        $params_payment->signature = "";

        $params_voucher = new stdClass();
        $params_voucher->user_id = $this->get_user_id;
        $params_voucher->ip_address = parent::get_ip_address();
        $params_voucher->node_cd = NODE_SHOPPING_MEMBER;
        $params_voucher->member_seq = $this->get_user_seq;

        $this->M_payment_member->save_update_status_order($params);

        if ($data->payment_code == PAYMENT_TYPE_ADIRA_BANK || $data->payment_code == PAYMENT_TYPE_ADIRA_DEPOSIT) {
            parent::generate_voucher($params_voucher, $data->total_installment);
            $params->order_seq = $data->seq;
            $params->status_order = "F";
            $this->M_payment_member->trans_begin();
            $params->pg_method_seq = PAYMENT_SEQ_DEPOSIT;
            $this->M_payment_member->update_status_order_loan($params);
            $this->M_payment_member->trans_commit();
        } else {
            parent::generate_voucher($params_voucher, $data->total_order);
        }

        $this->M_payment_member->save_payment_log($params_payment);
        $this->substract_member_account_deposit($data);
        $params->ip_address = parent::get_ip_address();
        $params->key = $this->order_no;

        $order_info = $this->M_order_member->get_order_info_by_order_no($params, $this->tipe);
        if (isset($order_info[1])) {
            $order_content = "<table border='1'>
                           <thead>
                            <tr>
                                <th>Produk</th>
                                <th>Qty</th>
                                <th>Harga (Rp)</th>
                                <th>Total (Rp)</th>
                            </tr>
                           </thead><tbody>";
            foreach ($order_info[2] as $product) {
                $order_content = $order_content . "<tr>
                                               <td>" . $product->display_name . "</td>
                                               <td>" . $product->qty . "</td>
                                               <td>" . number_format($product->sell_price) . "</td>
                                               <td>" . number_format($product->sell_price * $product->qty) . "</td></tr>";
            }

            $order_content = $order_content . "</tbody>
                                          <tr>
                                            <td colspan=3>Total Biaya Pengiriman</td>
                                            <td>" . number_format($order_info[1][0]->total_ship) . "</td>
                                          </tr>
                                          <tr>
                                            <td colspan=3>Voucher</td>
                                            <td>" . number_format($order_info[0][0]->nominal) . "</td>
                                          </tr>
                                          <tr>
                                            <td colspan=3>Total Bayar</td>
                                            <td>" . number_format($order_info[0][0]->total_payment) . "</td>
                                          </tr>
                                          </table>";

            $address_content = "Penerima : " . $order_info[0][0]->receiver_name . "<br>
        Alamat : " . $order_info[0][0]->receiver_address . "-" . $order_info[0][0]->province_name . "-" . $order_info[0][0]->city_name . "-" . $order_info[0][0]->district_name . "<br>
        No Tlp : " . $order_info[0][0]->receiver_phone_no . "<br>";

            $params_email_member = new stdClass();
            $params_email_member->user_id = $this->get_user_id;
            $params_email_member->ip_address = parent::get_ip_address();

            $params_member = new stdClass();
            $params_member->user_id = $this->get_user_id;
            $params_member->ip_address = parent::get_ip_address();
            $params_member->key = $order_info[0][0]->order_no;
            $order_info_new = $this->M_order_member->get_order_info_by_order_no($params_member, $this->tipe);
            $order_info_new[0][0]->payment_status = 'P';
            if ($this->is_member_get_axa($order_info_new)) {
                $params_email_member->code = ORDER_PAY_CONFIRM_SUCCESS_WITH_AXA_CODE;
            } else {
                $params_email_member->code = ORDER_PAY_CONFIRM_SUCCESS_CODE;
            }

            $params_email_member->RECIPIENT_NAME = $order_info[0][0]->member_name;
            $params_email_member->ORDER_NO = $order_info[0][0]->order_no;
            $params_email_member->ORDER_DATE = parent::cdate($order_info[0][0]->order_date);
            $params_email_member->RECIPIENT_ADDRESS = $address_content;
            if ($order_info[0][0]->pg_method_seq == PAYMENT_SEQ_ADIRA) {
                $payment_info = parent::cnum($order_info[0][0]->total_installment);
            } else {
                $payment_info = parent::cnum($order_info[0][0]->total_payment);
            }
            $params_email_member->TOTAL_PAYMENT = $payment_info;
            $params_email_member->PAYMENT_METHOD = $data->payment_name;
            $params_email_member->ORDER_ITEMS = $order_content;
            $params_email_member->CONFIRM_LINK = base_url("member/payment/" . $order_info[0][0]->order_no);
            $params_email_member->to_email = $order_info[0][0]->email;
            parent::email_template($params_email_member);

            $params_email_merchant = new stdClass();

            foreach ($order_info[1] as $value) {
                $params_email_merchant->user_id = $this->get_user_id;
                $params_email_merchant->ip_address = parent::get_ip_address();
                $params_email_merchant->code = ORDER_NEW_CODE;
                $params->order_seq = $order_info[0][0]->seq;
                $params->merchant_info_seq = $value->merchant_info_seq;
                $merchant_product = $this->M_order_merchant_admin->get_produk($params);

                $product_order_content = "";
                $product_order_content = "<table border='1'>
                                                            <thead>
                                                             <tr>
                                                                 <th>Produk</th>
                                                                 <th>Tipe Product</th>
                                                                 <th>Qty</th>
                                                             </tr>
                                                            </thead><tbody>";

                foreach ($merchant_product as $product_merchant) {
                    $product_order_content = $product_order_content . "<tr>
                                                                    <td>" . $product_merchant->product_name . "</td>
                                                                    <td>" . $product_merchant->variant_name . "</td>
                                                                    <td>" . $product_merchant->qty . "</td></tr>";
                }
                $product_order_content = $product_order_content . "</tbody> </table>";

                $params_email_merchant->ORDER_ITEMS = $product_order_content;
                $params_email_merchant->RECIPIENT_NAME = $value->merchant_name;
                $params_email_merchant->ORDER_NO = $order_info[0][0]->order_no;
                $params_email_merchant->ORDER_DATE = parent::cdate($order_info[0][0]->order_date);
                $params_email_merchant->RECIPIENT_ADDRESS = $address_content;
                $params_email_merchant->email_code = parent::generate_random_alnum(20, true);
                $params_email_merchant->to_email = $value->email;
                parent::email_template($params_email_merchant);
            }
        } else {
            date_default_timezone_set("Asia/Bangkok");
            $signature = md5(md5(SMTEL_SERVER_PIN . SMTEL_SERVER_SECRET_KEY . $data->order_no) . md5(SMTEL_CHANNEL_ID . SMTEL_STORE_ID . SMTEL_POS_ID . date("Ymdhis")));
            $this->curl->set_url(SMTEL_URL_REQUEST . "?password=" . $signature . "&channelid=" . SMTEL_CHANNEL_ID . "&cmd=TOPUP&hp=" . str_replace(" ", "", $order_info[0][0]->phone_number) . "&vtype=" . $order_info[0][0]->mapping_code . "&trxtime=" . date("Ymdhis") . "&partner_trxid=" . $data->order_no . "&storeid=" . SMTEL_STORE_ID . "&posid=" . SMTEL_POS_ID);
            $this->curl->setGet(true);
            $this->curl->setIsUsingUserAgent(true);
            $this->curl->setData();
            $response = $this->curl->getResponse();
            $xmlObject = simplexml_load_string($response);

            $xmlString = '';
            if (isset($xmlObject)) {
                foreach ($xmlObject as $key => $xml_string) {
                    $xmlString .= '[' . $key . '] = ' . $xml_string . '
						';
                }
            }

            $params_payment = new stdClass();
            $params_payment->user_id = $this->get_user_id;
            $params_payment->ip_address = parent::get_ip_address();
            $params_payment->order_seq = $data->seq;
            $params_payment->order_no = $data->order_no;
            $params_payment->payment_method_seq = $data->pg_method_seq;
            $params_payment->signature = $signature;
            $params_payment->request = $this->curl->getRequest();
            $params_payment->request_date = date('Y-m-d H:i:s');
            $params_payment->response = $xmlString;
            $params_payment->response_date = date('Y-m-d H:i:s');
            $this->M_payment_member->save_voucher_log($params_payment);

            if (isset($xmlObject)) {
                if ($xmlObject->rescode == '4') {
                    $data->order_status = ORDER_SUCCESS_STATUS_CODE;
                    $this->M_payment_member->save_update_prepaid_voucher_status($data);
                } elseif ($xmlObject->response_code == '2') {
                    $data->order_status = ORDER_FAIL_STATUS_CODE;
                    $this->M_payment_member->save_update_prepaid_voucher_status($data);
                }
            }
        }
    }

    public function payment_type_credit_card($data) {
        $params = new stdClass();
        $params->user_id = $this->get_user_id;
        $params->ip_address = parent::get_ip_address();

        $params->order_seq = $data->seq;
        $params->order_no = $data->order_no;
        if ($data->promo_credit_seq > 0) {
            $params->promo_credit_seq = $data->promo_credit_seq;
            $data_credit = $this->M_order_member->get_credit_info_by_credit_seq($params);
            if (isset($data_credit)) {
                switch ($data_credit[0]->bank_credit_seq) {
                    case BCA_PERIOD_3_SEQ :
                        $this->data[DATA_CREDIT_MID] = BCA_PERIOD_3_MID;
                        $this->data[DATA_CREDIT_PAS] = BCA_PERIOD_3_PASSWORD;
                        break;
                    case BCA_PERIOD_6_SEQ :
                        $this->data[DATA_CREDIT_MID] = BCA_PERIOD_6_MID;
                        $this->data[DATA_CREDIT_PAS] = BCA_PERIOD_6_PASSWORD;
                        break;
                    default :
                        throw new Exception(ERROR_CREDIT_SEQ);
                }
            }

            $str = "##" . strtoupper($this->data[DATA_CREDIT_MID]) . "##" . strtoupper($this->data[DATA_CREDIT_PAS]) . "##" . strtoupper($data->order_trans_no) . "##" . strtoupper($data->total_payment . ".00") . "##0##";
        } else {

            $str = "##" . strtoupper(PGS_MERCHANT_ID) . "##" . strtoupper(PGS_TXN_PASSWORD) . "##" . strtoupper($data->order_trans_no) . "##" . strtoupper($data->total_payment . ".00") . "##0##";
        }
        $this->data[DATA_SIGNATURE] = sha1($str);
        $params->signature = $this->data[DATA_SIGNATURE];

        try {
            $this->M_payment_member->trans_begin();
            $this->M_order_member->save_update_order_signature($params);
            $this->M_payment_member->trans_commit();
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_payment_member->trans_rollback();
        }
    }

    protected function save_log_payment_credit_card($data) {
        $params = new stdClass();
        $params->user_id = $this->get_user_id;
        $params->ip_address = parent::get_ip_address();
        $params->key = $this->order_no;

        $request = "";
        foreach ($_POST as $key => $value)
            $request .= $key . ' => ' . $value . '
                ';

        $params_payment = new stdClass();
        $params_payment->user_id = $this->get_user_id;
        $params_payment->ip_address = parent::get_ip_address();
        $params_payment->order_seq = $data->seq;
        $params_payment->order_no = $data->order_no;
        $params_payment->payment_method_seq = $data->pg_method_seq;
        $params_payment->request = $request;
        $params_payment->request_date = date('Y-m-d H:i:s');
        $params_payment->response = "";
        $params_payment->response_date = "";
        $params_payment->signature = $data->signature;
        $this->M_payment_member->save_payment_log($params_payment);
        die();
    }

    protected function save_log_payment_docu($data) {
        $params = new stdClass();
        $params->user_id = $this->get_user_id;
        $params->ip_address = parent::get_ip_address();
        $params->key = $this->order_no;

        $request = "";
        foreach ($_POST as $key => $value)
            $request .= $key . ' => ' . $value . '
                ';

        $params_payment = new stdClass();
        $params_payment->user_id = $this->get_user_id;
        $params_payment->ip_address = parent::get_ip_address();
        $params_payment->order_seq = $data->seq;
        $params_payment->order_no = $data->order_no;
        $params_payment->payment_method_seq = $data->pg_method_seq;
        $params_payment->request = $request;
        $params_payment->request_date = date('Y-m-d H:i:s');
        $params_payment->response = "";
        $params_payment->response_date = "";
        $params_payment->signature = $data->signature;
        $this->M_payment_member->save_payment_log($params_payment);
        die();
    }

    public function substract_member_account_deposit($data) {

        $params = new stdClass();
        $params->user_id = $this->get_user_id;
        $params->ip_address = parent::get_ip_address();
        $params->member_seq = $this->get_user_seq;
        $params->account_type = ORDER_ACCOUNT_TYPE;
        $params->mutation_type = DEBET_MUTATION_TYPE;
        $params->payment_method_seq = $data->pg_method_seq;
        $params->trx_no = $data->order_no;
        if ($data->pg_method_seq == PAYMENT_SEQ_ADIRA) {
            $params->deposit_amt = $data->total_installment;
        } else {
            $params->deposit_amt = $data->total_payment;
        }
        $params->non_deposit_amt = 0;
        $params->bank_name = "";
        $params->bank_branch_name = "";
        $params->bank_account_no = "";
        $params->bank_account_name = "";
        $params->status = APPROVE_STATUS_CODE;

        $params->topup_tipe = "N";
        $this->M_deposit_member->save_member_account($params, $this->tipe);
        $this->M_deposit_member->substract_deposit_member($params, $this->tipe);
    }

    public function payment_type_bank($data) {
        $params = new stdClass();
        $params->user_id = $this->get_user_id;
        $params->ip_address = parent::get_ip_address();
        $params->order_seq = $data->seq;
        $params->order_no = $data->order_no;
        $params->payment_status = PAYMENT_WAIT_CONFIRM_STATUS_CODE;
        $params->order_status = ORDER_PREORDER_STATUS_CODE;
        $params->paid_date = date('Y-m-d H:i:s');

        $params_payment = new stdClass();
        $params_payment->user_id = $this->get_user_id;
        $params_payment->ip_address = parent::get_ip_address();
        $params_payment->order_seq = $data->seq;
        $params_payment->order_no = $data->order_no;
        $params_payment->payment_method_seq = $data->pg_method_seq;
        $params_payment->request = "";
        $params_payment->request_date = "";
        $params_payment->response = "";
        $params_payment->response_date = "";
        $params_payment->signature = "";

        $this->M_payment_member->save_update_status_order($params);
    }

    public function payment_type_adira($data) {
        $params = new stdClass();
        $params->user_id = $this->get_user_id;
        $params->ip_address = parent::get_ip_address();
        $params->order_seq = $data->seq;
        $params->order_no = $data->order_no;
        $params->payment_status = PAYMENT_WAIT_CONFIRM_ADIRA_APPROVAL;
        $params->order_status = ORDER_PREORDER_STATUS_CODE;
        $params->paid_date = date('Y-m-d H:i:s');
        $this->M_payment_member->save_update_status_order($params);
    }

    private function payment_type_mandiri_click_pay($data) {
        $params = new stdClass();
        $params->user_id = $this->get_user_id;
        $params->ip_address = parent::get_ip_address();
        $params->order_seq = $data->seq;

        try {
            $data_items = array(array("product_title" => "TOTAL_PRODUCT", 'qty' => '1', 'price' => $data->total_payment));

            $signature = md5(MANDIRI_CLICKPAY_MERCHANT_ID . $data->total_payment . $data->order_trans_no . MANDIRI_CLICKPAY_MERCHANT_PWD);
            $xmlParams = ' <inputtrx_request>
                            <user_id>' . MANDIRI_CLICKPAY_MERCHANT_ID . '</user_id>
                            <amount>' . $data->total_payment . '</amount>
                            <transaction_id>' . $data->order_trans_no . '</transaction_id>
                            <date_time>' . date("YmdHis") . '</date_time>
                            <bank_id>1</bank_id>
                            <signature>' . $signature . '</signature>';
            $xmlParams .= '<items count="' . count($data_items) . '">';

            foreach ($data_items as $idx => $data_item) {
                $xmlParams .= '<item no="' . ($idx + 1) . '" name="' . $data_item['product_title'] . '" qty="' . $data_item['qty'] . '" price="' . str_replace(',', '', number_format($data_item['price'], 0)) . '" /> ';
            }

            $this->curl->set_url(MANDIRI_CLICKPAY_INSERTPAYMENT);
            $this->curl->setIsUsingUserAgent(true);
            $xmlParams .= '</items> </inputtrx_request>';
            $this->curl->setParams($xmlParams);
            $this->curl->setData();
            $response = $this->curl->getResponse();
            $xmlObject = simplexml_load_string($response);
            $params_payment = new stdClass();
            $params_payment->user_id = parent::get_member_user_id();
            $params_payment->ip_address = parent::get_ip_address();
            $params_payment->order_seq = $data->seq;
            $params_payment->order_no = $data->order_no;
            $params_payment->payment_method_seq = $data->pg_method_seq;
            $params_payment->request = "[user_id]=" . $xmlObject->user_id . "
                                        [transaction_id]=" . $xmlObject->transaction_id . "
                                        [billreferenceno    ]=" . $xmlObject->billreferenceno . "
                                        [response_code]=" . $xmlObject->response_code . "
                                        [response_desc]" . $xmlObject->response_desc;
            $params_payment->request_date = date('Y-m-d H:i:s');
            $params_payment->response = "";
            $params_payment->response_date = "";
            $params_payment->signature = $xmlObject->billreferenceno;

            $params->signature = $xmlObject->billreferenceno;
            $params->order_no = $data->order_no;
            $this->M_order_member->save_update_order_signature($params);
            $this->M_payment_member->save_payment_log($params_payment);
            $this->M_order_member->trans_commit();

            if ($xmlObject->response_code == '0000') {
                header("Location: " . MANDIRI_CLICKPAY_REDIRECT_URL . "?user_id=" . MANDIRI_CLICKPAY_MERCHANT_ID . "&billreferenceno=" . $xmlObject->billreferenceno);
                die();
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_profile_member->trans_rollback();
            redirect(base_url('member/payment/' . $this->order_no));
        }
    }

    private function payment_type_mandiri_ecash($data) {
        $request = new stdClass();
        $request->params = new stdClass();
        $request->params->amount = ENVIRONMENT == "development" || ENVIRONMENT == "qa" ? "10000" : $data->total_payment;
        $request->params->clientAddress = gethostname();
        $request->params->description = PAYMENT_DESC;
        $request->params->currency = CURENCY_CODE;
        $request->params->memberAddress = gethostname();
        $request->params->returnUrl = base_url("member/payment/proccess") . "/" . $data->order_no;
        $request->params->toUsername = MANDIRI_ECASH_MERCHANT_ID;
        $request->params->trxid = $data->order_trans_no;
        $request->params->hash = sha1(strtoupper($request->params->toUsername) . $request->params->amount . $request->params->clientAddress);

        $params = new stdClass();
        $params->user_id = $this->get_user_id;
        $params->ip_address = parent::get_ip_address();
        $params->order_seq = $data->seq;
        $params->order_no = $data->order_no;

        try {
            $soapclient = new Soap_Client(APPPATH . "controllers/member/" . MANDIRI_ECASH_WSDL, array('encoding' => 'UTF8', 'cache_wsdl' => WSDL_CACHE_NONE));
            $soapclient->setConnectionTimeout(CURL_CONNECTION_TIMEOUT);
            $soapclient->setOperationTimeout(OPERATION_TIMEOUT);
            $soapclient->setIsSoap(true);
            $soapclient->setUserPassword(MANDIRI_ECASH_USERNAME . ":" . MANDIRI_ECASH_PASSWORD);

            $request_headers = $soapclient->__getLastRequestHeaders();
            $response = $soapclient->generate($request);

            if ($soapclient instanceof Soap_Client) {
                $soapRequestHeader = $soapclient->__getLastRequestHeaders();
                $soapRequest = $soapclient->__getLastRequest();
                $soapResponseHeader = $soapclient->__getLastResponseHeaders();
                $soapResponse = $soapclient->__getLastResponse();
            }
            if (isset($response->return)) {
                $emoney_trx_id = $response->return;
                $params->signature = $emoney_trx_id;

                $params_payment = new stdClass();
                $params_payment->user_id = $this->get_user_id;
                $params_payment->ip_address = parent::get_ip_address();
                $params_payment->order_seq = $data->seq;
                $params_payment->order_no = $data->order_no;
                $params_payment->payment_method_seq = $data->pg_method_seq;
                $params_payment->request = $soapRequest;
                $params_payment->request_date = date('Y-m-d H:i:s');
                $params_payment->response = "";
                $params_payment->signature = $emoney_trx_id;
                $params_payment->response_date = "";

                $this->M_payment_member->save_payment_log($params_payment);
                $this->M_order_member->save_update_order_signature($params);
                $this->M_order_member->trans_commit();

                if ($emoney_trx_id == 'INVALID DATA') {
                    echo 'Transaction failed. Please try it again.';
                    exit(0);
                } else {
                    $this->M_payment_member->trans_commit();
                    header("Location: " . MANDIRI_ECASH_PAYMENT_URL . "?id=" . $emoney_trx_id);
                    exit(0);
                }
            } else {
                echo 'Transaction failed. Please try it again.';
                exit(0);
            }
        } catch (SoapFault $e) {
            die('soapfault');
            echo 'Transaction failed. Please try it again.';
            exit(0);
        } catch (Exception $e) {
            die('exception');
            echo 'Transaction failed. Please try it again.';
            exit(0);
        }
    }

    private function payment_type_docu_atm($data) {
        $params = new stdClass();
        $params->user_id = $this->get_user_id;
        $params->ip_address = parent::get_ip_address();
        $params->order_seq = $data->seq;
        $params->order_no = $data->order_no;

        $str = $data->total_payment . ".00" . MALLID . SHARED_KEY . $data->order_trans_no;
        $this->data[DATA_SIGNATURE] = sha1($str);
        $params->signature = $this->data[DATA_SIGNATURE];

        try {
            $this->M_payment_member->trans_begin();
            $this->M_order_member->save_update_order_signature($params);
            $this->M_payment_member->trans_commit();
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_payment_member->trans_rollback();
        }
    }

    private function payment_type_docu_alfamart($data) {
        $params = new stdClass();
        $params->user_id = $this->get_user_id;
        $params->ip_address = parent::get_ip_address();
        $params->order_seq = $data->seq;
        $params->order_no = $data->order_no;

        $str = $data->total_payment . ".00" . MALLID . SHARED_KEY . $data->order_trans_no;
        $this->data[DATA_SIGNATURE] = sha1($str);
        $params->signature = $this->data[DATA_SIGNATURE];

        try {
            $this->M_payment_member->trans_begin();
            $this->M_order_member->save_update_order_signature($params);
            $this->M_payment_member->trans_commit();
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_payment_member->trans_rollback();
        }
    }

    public function save_update() {

        $params = new stdClass();

        $order_params = new stdClass();
        $order_params->user_id = $this->get_user_id;
        $order_params->ip_address = parent::get_ip_address();
        $order_params->key = $this->order_no;
        $order_info = $this->M_order_member->get_order_info_by_order_no($order_params, $this->tipe);

        try {
            if ($order_info[0][0]->payment_status == PAYMENT_WAIT_CONFIRM_STATUS_CODE || ($order_info[0][0]->payment_status == PAYMENT_UNPAID_STATUS_CODE && $order_info[0][0]->pg_method_seq == PAYMENT_SEQ_ADIRA)) {
                $params->user_id = $this->get_user_id;
                $params->ip_address = parent::get_ip_address();
                $params->payment_type = parent::get_input_post('payment_type');
                $params->payment_amt = parent::get_input_post('payment_amt');
                $params->payment_date = parent::get_input_post('payment_date');
                $file = str_replace(CDN_IMAGE, "", ORDER_UPLOAD_IMAGE);
                $logo_img = parent::upload_file("nlogo_img", $file . "/" . $this->order_no, date_format(new datetime, 'Ymd'), IMAGE_TYPE, $this->data, IMAGE_DIMENSION_TRANSFER, "Bukti Setoran ");
                $params->confirm_pay_note_file = $logo_img->error ? "" : $logo_img->file_name;
                $params->confirm_pay_bank_seq = parent::get_input_post('bank', true, FILL_VALIDATOR, "Bank", $this->data);
                $params->order_no = $this->order_no;
                $params->status = PAYMENT_CONFIRM_STATUS_CODE;
                $this->data[DATA_SELECTED][LIST_DATA][] = $params;
            } else {
                throw new Exception(ERROR_PAYMENT_STATUS);
            }

            if ($order_info[0][0]->pg_method_seq == PAYMENT_SEQ_ADIRA) {
                if ($params->payment_amt < $order_info[0][0]->total_installment) {
                    throw new Exception(ERROR_PAYMENT_LESS_THAN_TOTAL_DP);
                } else {
                    $params->order_seq = $order_info[0][0]->seq;
                    $params->status_order = PAYMENT_PAID_STATUS_CODE;
                    $this->M_payment_member->trans_begin();
                    $params->pg_method_seq = PAYMENT_SEQ_BANK;
                    $this->M_payment_member->update_status_order_loan($params);
                    $this->M_payment_member->trans_commit();
                }
            } else {
                if ($params->payment_amt < $order_info[0][0]->total_payment) {
                    throw new Exception(ERROR_PAYMENT_LESS_THAN_TOTAL_PAYMENT);
                }
            }

            if ($this->data[DATA_ERROR][ERROR] === false) {
                $this->M_payment_member->trans_begin();
                $this->M_payment_member->save_update_payment_bank($params);
                $this->M_payment_member->trans_commit();

                $email_params = new stdClass();
                $email_params->user_id = $this->get_user_id;
                $email_params->ip_address = parent::get_ip_address();
                $email_params->code = MEMBER_CONFIRM_PAYMENT;
                $email_params->to_email = ADMIN_TOKO1001_EMAIL;
                $email_params->RECIPIENT_NAME = ADMIN_TOKO1001_NAME;
                $email_params->ORDER_NO = $params->order_no;
                $email_params->MEMBER_NAME = $order_info[0][0]->member_name;
                $email_params->TOTAL_TRANSFER = "Rp. " . number_format($params->payment_amt);
                $email_params->LINK = base_url() . "admin/transaction/order";
                parent::email_template($email_params);
                redirect($this->data[DATA_AUTH][FORM_URL] . '/' . $this->order_no);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_profile_member->trans_rollback();
        }
//        redirect(base_url('member/payment/' . $this->order_no));
    }

    private function save_success_order_payment($data) {

        $params = new stdClass();
        $params->user_id = $this->get_user_id;
        $params->ip_address = parent::get_ip_address();
        $params->order_seq = $data->seq;
        $params->order_no = $data->order_no;
        $params->payment_status = PAYMENT_PAID_STATUS_CODE;
        $params->order_status = ORDER_NEW_ORDER_STATUS_CODE;
        $params->paid_date = date('Y-m-d H:i:s');

        $params_voucher = new stdClass();
        $params_voucher->user_id = parent::get_member_user_id();
        $params_voucher->ip_address = parent::get_ip_address();
        $params_voucher->node_cd = NODE_SHOPPING_MEMBER;
        $params_voucher->member_seq = $this->get_user_seq;
        $params_voucher->total_order = $data->total_order;

        try {
            $this->M_payment_member->trans_begin();
            $this->M_payment_member->save_update_status_order($params);
            parent::generate_voucher($params_voucher, $data->total_order);
            $this->M_payment_member->trans_commit();

            $params->ip_address = parent::get_ip_address();
            $params->key = $this->order_no;

            $order_info = $this->M_order_member->get_order_info_by_order_no($params, $this->tipe);
            $order_content = "<table border='1'>
                           <thead>
                            <tr>
                                <th>Produk</th>
                                <th>Qty</th>
                                <th>Harga (Rp)</th>
                                <th>Total (Rp)</th>
                            </tr>
                           </thead><tbody>";

            foreach ($order_info[2] as $product) {
                $order_content = $order_content . "<tr>
                                               <td>" . $product->display_name . "</td>
                                               <td>" . $product->qty . "</td>
                                               <td>" . number_format($product->sell_price) . "</td>
                                               <td>" . number_format($product->sell_price * $product->qty) . "</td></tr>";
            }
            $rowcredit = '';
            if ($order_info[0][0]->promo_credit_seq > 0) {
                $params->promo_credit_seq = $order_info[0][0]->promo_credit_seq;
                $data_credit = $this->M_order_member->get_credit_info_by_credit_seq($params);
                if (isset($data_credit)) {
                    $rowcredit = "<tr>
                                <td colspan=3>Kredit Bank</td>
                                <td>" . $data_credit[0]->bank_name . "</td>
                            </tr>
			    <tr>
                                <td colspan=3>Periode cicilan</td>
                                <td>" . $data_credit[0]->credit_month . " bulan</td>
                            </tr>
			    <tr>
                                <td colspan=3Pembayaran bulanan</td>
                                <td>" . number_format($order_info[0][0]->total_payment / $data_credit[0]->credit_month) . "</td>
                            </tr>";
                }
            }

            $order_content = $order_content . "</tbody>
                                          <tr>
                                            <td colspan=3>Total Biaya Pengiriman</td>
                                            <td>" . number_format($order_info[1][0]->total_ship) . "</td>
                                          </tr>
                                          <tr>
                                            <td colspan=3>Voucher</td>
                                            <td>" . number_format($order_info[0][0]->nominal) . "</td>
                                          </tr>
                                          <tr>
                                            <td colspan=3>Total Bayar</td>
                                            <td>" . number_format($order_info[0][0]->total_payment) . "</td>
                                          </tr>" . $rowcredit . "
                                          </table>";

            $address_content = "Penerima : " . $order_info[0][0]->receiver_name . "<br>
                                Alamat : " . $order_info[0][0]->receiver_address . "-" . $order_info[0][0]->province_name . "-" . $order_info[0][0]->city_name . "-" . $order_info[0][0]->district_name . "<br>
                                No Tlp : " . $order_info[0][0]->receiver_phone_no . "<br>";

            $params_email_member = new stdClass();
            $params_email_member->user_id = $this->get_user_id;
            $params_email_member->ip_address = parent::get_ip_address();

            $params_member = new stdClass();
            $params_member->user_id = $this->get_user_id;
            $params_member->ip_address = parent::get_ip_address();
            $params_member->key = $order_info[0][0]->order_no;
            $order_info_new = $this->M_order_member->get_order_info_by_order_no($params_member, $this->tipe);
            $order_info_new[0][0]->payment_status = 'P';
            if ($this->is_member_get_axa($order_info_new)) {
                $params_email_member->code = ORDER_PAY_CONFIRM_SUCCESS_WITH_AXA_CODE;
            } else {
                $params_email_member->code = ORDER_PAY_CONFIRM_SUCCESS_CODE;
            }

            $params_email_member->RECIPIENT_NAME = $order_info[0][0]->member_name;
            $params_email_member->ORDER_NO = $order_info[0][0]->order_no;
            $params_email_member->ORDER_DATE = parent::cdate($order_info[0][0]->order_date);
            $params_email_member->RECIPIENT_ADDRESS = $address_content;
            $params_email_member->TOTAL_PAYMENT = parent::cnum($order_info[0][0]->total_payment);
            $params_email_member->PAYMENT_METHOD = $data->payment_name;
            $params_email_member->ORDER_ITEMS = $order_content;
            $params_email_member->CONFIRM_LINK = base_url("member/payment/" . $order_info[0][0]->order_no);
            $params_email_member->to_email = $order_info[0][0]->email;
            parent::email_template($params_email_member);

            foreach ($order_info[1] as $value) {
                $params_email_merchant = new stdClass();
                $params_email_merchant->user_id = $this->get_user_id;
                $params_email_merchant->ip_address = parent::get_ip_address();
                $params_email_merchant->code = ORDER_NEW_CODE;
                $params->order_seq = $order_info[0][0]->seq;
                $params->merchant_info_seq = $value->merchant_info_seq;
                $merchant_product = $this->M_order_merchant_admin->get_produk($params);

                $product_order_content = "";
                $product_order_content = "<table border='1'>
                                                            <thead>
                                                             <tr>
                                                                 <th>Produk</th>
                                                                 <th>Tipe Product</th>
                                                                 <th>Qty</th>
                                                             </tr>
                                                            </thead><tbody>";

                foreach ($merchant_product as $product_merchant) {
                    $product_order_content = $product_order_content . "<tr>
                                                                    <td>" . $product_merchant->product_name . "</td>
                                                                    <td>" . $product_merchant->variant_name . "</td>
                                                                    <td>" . $product_merchant->qty . "</td></tr>";
                }
                $product_order_content = $product_order_content . "</tbody> </table>";

                $params_email_merchant->ORDER_ITEMS = $product_order_content;
                $params_email_merchant->RECIPIENT_NAME = $value->merchant_name;
                $params_email_merchant->ORDER_NO = $order_info[0][0]->order_no;
                $params_email_merchant->ORDER_DATE = parent::cdate($order_info[0][0]->order_date);
                $params_email_merchant->RECIPIENT_ADDRESS = $address_content;
                $params_email_merchant->email_code = parent::generate_random_alnum(20, true);
                $params_email_merchant->CONFIRM_LINK = base_url() . "merchant/transaction/merchant_delivery";

                $params_email_merchant->to_email = $value->email;
                parent::email_template($params_email_merchant);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_payment_member->trans_rollback();
        }
    }

    protected function save_add() {
        $params = new stdClass();
        $params->user_id = $this->get_user_id;
        $params->ip_address = parent::get_ip_address();
        $params->key = $this->order_no;
        $order_info = $this->M_order_member->get_order_info_by_order_no($params);
        if ($this->is_member_get_axa($order_info)) {
            $params->member_email = parent::get_input_post("member_email", true, FILL_VALIDATOR, ERROR_VALIDATION_MUST_FILL_EMAIL, $this->data);
            $params->member_name = parent::get_input_post("member_name", true, FILL_VALIDATOR, "Nama ", $this->data);
            $params->member_birthday = parent::get_input_post("member_birthday", true, DATE_VALIDATOR, ERROR_DATE_VALIDATION, $this->data);
            $params->gender = parent::get_input_post("gender_member", TRUE, GENDER_VALIDATOR, '', $this->data);
            $params->member_phone = parent::get_input_post("member_phone", true, FILL_VALIDATOR, "Nomor telepon " . ERROR_VALIDATION_MUST_FILL, $this->data);
            $params->order_seq = $order_info[0][0]->seq;

            if (parent::get_input_post("member_requirement") != 1) {
                parent::set_error($this->data, new Exception(ERROR_REQUIREMENT));
            }
            if ($this->data[DATA_ERROR][ERROR] == FALSE) {
                try {
                    $this->M_order_member->trans_begin();
                    $this->M_order_member->save_axa_member($params);
                    $this->M_order_member->trans_commit();
                    $this->data[DATA_SUCCESS][SUCCESS] = true;
                    $this->data[DATA_SUCCESS][SUCCESS_MESSAGE][] = SUCCESS_REGISTRATION_AXA;
                } catch (BusisnessException $ex) {
                    parent::set_error($this->data, $ex);
                    $this->M_order_member->trans_rollback();
                } catch (TechnicalException $ex) {
                    parent::set_error($this->data, $ex);
                    $this->M_order_member->trans_rollback();
                } catch (Exception $ex) {
                    parent::set_error($this->data, $ex);
                    $this->M_order_member->trans_rollback();
                }
            }
            $message[SESSION_DATA] = $this->data;
            $this->session->set_userdata($message);
            redirect($this->data[DATA_AUTH][FORM_URL] . '/' . $this->order_no);
        }
    }

    protected function save_delete() {
        $params = new stdClass();
        $params->user_id = $this->get_user_id;
        $params->ip_address = parent::get_ip_address();
        $params->key = $this->order_no;
        $order_info = $this->M_order_member->get_order_info_by_order_no($params);
        if ($this->is_member_get_axa($order_info)) {
            $params->member_email = "";
            $params->member_name = "";
            $params->member_birthday = "";
            $params->gender = "";
            $params->member_phone = "";
            $params->order_seq = $order_info[0][0]->seq;
            if ($this->data[DATA_ERROR][ERROR] == FALSE) {
                try {
                    $this->M_order_member->trans_begin();
                    $this->M_order_member->save_axa_member($params);
                    $this->M_order_member->trans_commit();
                } catch (BusisnessException $ex) {
                    parent::set_error($this->data, $ex);
                    $this->M_order_member->trans_rollback();
                } catch (TechnicalException $ex) {
                    parent::set_error($this->data, $ex);
                    $this->M_order_member->trans_rollback();
                } catch (Exception $ex) {
                    parent::set_error($this->data, $ex);
                    $this->M_order_member->trans_rollback();
                }
            }
            redirect($this->data[DATA_AUTH][FORM_URL] . '/' . $this->order_no);
        }
    }

    private function create_invoice_qr($data, $amount) {
        $url = URL_DIMO_API . 'api/createInvoice';
        $invoice_tag_start = false;
        $callback = 'true';
        $secured_call_back = 'true';
        $params = 'apiKey=' . DIMO_API_KEY . '&amount=' . $amount . '&pinCode=' . DIMO_PIN_CODE . '&invoiceTagStart=' . $invoice_tag_start . '&callback=' . $callback . '&securedCallBack=' . $secured_call_back;
        $this->curl_dimo->set_url($url . '?' . $params);
        $this->curl_dimo->setGet(true);
        $this->curl_dimo->setIsUsingUserAgent(true);
        $this->curl_dimo->setParams($params);
        $this->curl_dimo->setData();
        $invoice_no = $this->curl_dimo->getResponse();

        $params_payment = new stdClass();
        $params_payment->user_id = $this->get_member_user_id();
        $params_payment->ip_address = parent::get_ip_address();
        $params_payment->order_seq = $data->seq;
        $params_payment->order_no = $data->order_no;
        $params_payment->payment_method_seq = $data->pg_method_seq;
        $params_payment->request = $url . '?' . $params;
        $params_payment->request_date = date('Y-m-d H:i:s');
        $params_payment->response = $invoice_no;
        $params_payment->response_date = date('Y-m-d H:i:s');
        $params_payment->signature = $invoice_no;
        $this->M_payment_member->save_payment_log($params_payment);

        return $invoice_no;
    }

    private function get_issuer($data, $invoice_no) {
        $code = 2;
        $url = URL_DIMO_API . 'DppApi/rest/issuer.html?code=' . $code;
        $this->curl_dimo->set_url($url);
        $this->curl_dimo->setGet(true);
        $this->curl_dimo->setIsUsingUserAgent(true);
        $this->curl_dimo->setData();

        $issuer = $this->curl_dimo->getResponse();

        $params_payment = new stdClass();
        $params_payment->user_id = $this->get_user_id;
        $params_payment->ip_address = parent::get_ip_address();
        $params_payment->order_seq = $data->seq;
        $params_payment->order_no = $data->order_no;
        $params_payment->payment_method_seq = $data->pg_method_seq;
        $params_payment->request = $url;
        $params_payment->request_date = date('Y-m-d H:i:s');
        $params_payment->response = $issuer;
        $params_payment->response_date = date('Y-m-d H:i:s');
        $params_payment->signature = $invoice_no;
        $this->M_payment_member->save_payment_log($params_payment);

        return $issuer;
    }

    protected function get_ajax() {
        $params = new stdClass();
        $params->user_id = $this->get_user_id;
        $params->ip_address = parent::get_ip_address();
        $params->order_no = parent::get_input_post("order_no");
        $params->signature = parent::get_input_post("signature");
        $params->payment_type = parent::get_input_post("payment_type");
        $data = array();
        switch ($params->payment_type) {
            case PAYMENT_TYPE_QR_CODE:
                $data = $this->M_payment_member->get_order_payment_by_order_no_signature($params);
                $status = "0";
                $get_axa = "0";
                if (isset($data[0]->payment_status)) {
                    if ($data[0]->payment_status == PAYMENT_PAID_STATUS_CODE) {
                        $status = "1";
                        $params->key = parent::get_input_post("order_no");
                        $order_info = $this->M_order_member->get_order_info_by_order_no($params);
                        if ($this->is_member_get_axa($order_info)) {
                            $get_axa = "1";
                        }
                    }
                }
                echo json_encode(array("status" => $status, "get_axa" => $get_axa));
                break;
        }
        die();
    }

    public function payment_type_bca_klikpay($data, $data_order_merchant_product) {
        $transaction_no = $data->order_trans_no;
        $total_amount_number = $data->total_payment;
        $total_amount = number_format($total_amount_number, 2, ".", "");
        $misc_fee_number = 0;
        $misc_fee = number_format($misc_fee_number, 2, ".", "");
        $amount_number = $total_amount_number + $misc_fee_number;
        $amount = number_format($amount_number, 2, ".", "");
        $date = $data->created_date;

        $product_name = '';
        foreach ($data_order_merchant_product[2] as $each_product) {
            $product_name.= $each_product->display_name . ', ';
        }
        $product_name = rtrim($product_name, ", ");
        $data_bca = array(
            'transactionNo' => $transaction_no,
            'transactionDate' => date('d/m/Y H:i:s', strtotime($date)),
            'totalAmount' => $total_amount,
            'descp' => $product_name,
            'miscFee' => $misc_fee,
            'klikPayCode' => BCA_KLIKPAYCODE,
            'clearkey' => BCA_CLEARKEY,
            'payType' => BCA_PAYTYPE,
            'currency' => BCA_CURRENCY,
            'callback' => BCA_CALLBACK . $transaction_no,
        );
        $request_string = "";
        foreach ($data_bca as $key => $each) {
            $request_string.= $key . "=>" . $each . "\r\n";
        }

        $this->curl->set_url(URL_BCA);
        $this->curl->setIsUsingUserAgent(true);
        $this->curl->setParams($data_bca);
        $this->curl->setData();
        $response = $this->curl->getResponse();
        if ($response === FALSE) {
            echo('Transaksi Anda tidak dapat diproses. <a href="' . base_url() . '">Back to home</a>');
            exit();
        }
        $this->update_signature_order($data->seq, $transaction_no, $response);
        $this->save_payment_log($data->seq, $transaction_no, $data->pg_method_seq, $request_string, $response);

        //execute post
        $result_decode = json_decode($response);
        array_push($data_bca, $result_decode);
        $this->data[DATA_BCA] = $data_bca;

        //Check Result
        If (!isset($result_decode->code)) {
            echo('Transaksi Anda tidak dapat diproses. <a href="' . base_url() . '">Back to home</a>');
            exit();
            exit;
        }
        If ($result_decode->code != "00") {
            echo('Transaksi Anda tidak dapat diproses. <a href="' . base_url() . '">Back to home</a>');
            exit();
            exit;
        }
    }

    private function update_signature_order($seq, $transaction_no, $response) {
        $params = new stdClass();
        $params->user_id = "SYSTEM";
        $params->ip_address = parent::get_ip_address();
        $params->order_seq = $seq;
        $params->signature = json_decode($response)->signature;
        $params->order_no = $transaction_no;
        $this->M_order_member->save_update_order_signature($params);
    }

    private function save_payment_log($seq, $transaction_no, $pg_method_seq, $request, $response) {

        $params = new stdClass();
        $params->user_id = "SYSTEM";
        $params->ip_address = parent::get_ip_address();
        $params->order_no = $transaction_no;
        $params->order_seq = $seq;
        $params->payment_method_seq = $pg_method_seq;
        $params->request = $request;
        $params->request_date = date('Y-m-d H:i:s');
        $params->response = $response;
        $params->response_date = date('Y-m-d H:i:s');
        $params->signature = json_decode($response)->signature;
        $this->M_payment_member->save_payment_log($params);
    }

}
?>

