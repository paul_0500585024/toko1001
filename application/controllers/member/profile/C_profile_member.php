<?php

require_once CONTROLLER_BASE_MEMBER;

class C_profile_member extends controller_base_member {

    private $temp_data;
    private $data;

    public function __construct() {

	$this->data = parent::__construct("MST001", "member/profile/profile_member");
	$this->initialize();
    }

    private function initialize() {

	$this->load->model('component/M_dropdown_list');
	$this->load->model('member/profile/M_profile_member');
	$this->load->model('member/M_order_member');

	parent::register_event($this->data, ACTION_SEARCH, "search");
	parent::register_event($this->data, ACTION_ADDITIONAL, "additional");
	parent::register_event($this->data, ACTION_EDIT, "get_edit");
	parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");

	if ($this->data[DATA_INIT] === true) {
	    parent::fire_event($this->data);
	    $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
	    parent::fire_event($this->data);
	}
    }

    public function index() {

	$uri = $this->uri->segment(4);

	if ($uri != "") {
	    //echo "ada";
	    $getdate = $this->M_profile_member->get_date_account();
	    $waktuserver = substr($getdate, 0, 10);
	    $tglpengajuan = date('Y-m-d');
	    $params = new stdClass();
	    $params->uri = $uri;
	    $this->data[DATA_SELECTED][LIST_DATA][] = $params;
	    $seconds_diff = (strtotime($tglpengajuan) - strtotime($waktuserver)) / (60 * 60 * 24);
	    if ($seconds_diff >= 2) {
		echo "<center><h2><spa>Link expired</span></h2></center>";
	    } else {
		echo "<script>alert('Success')</script>";
		$a = explode("-", $uri);
		$selected = new stdClass();
		$selected->ip_address = parent::get_ip_address();
		$selected->member_seq = $a[0];
		$selected->seq = $a[1];
		$this->M_profile_member->save_update_account($selected);
		echo "<center><h2><spa>Terima Kasih</span></h2></center>";
	    }
	} else {
	    if (!$this->input->post()) {
		$this->load->view(LIVEVIEW . "member/profile/profile_member", $this->data);
	    } else {
		if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
		    if ($this->data[DATA_ERROR][ERROR] === true) {
			if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
			    $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
			    $this->load->view(LIVEVIEW . "member/profile/profile_member", $this->data);
			}
		    } else {
			if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
			    $this->data[DATA_SUCCESS][SUCCESS] = true;
			    $this->data[DATA_AUTH][FORM_ACTION] = "";
			}
			$member_info[SESSION_DATA] = $this->data;
			$this->session->set_userdata($member_info);
			redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
		    }
		}
	    }
	}
    }

    public function search() {
	$order_date_between = explode("-", parent::get_input_post("order_date_between"));
	$filter = new stdClass;
	$filter->user_id = parent::get_member_email();
	$filter->ip_address = parent::get_ip_address();
	$filter->member_seq = parent::get_member_user_seq();
	$filter->start = parent::get_input_post("start");
	$filter->length = parent::get_input_post("length");
	$filter->order = parent::get_input_post("order");
	$filter->column = parent::get_input_post("column");
	$filter->order_no = parent::get_input_post("order_no");
	$filter->date_search = isset($order_date_between[1]) ? "1" : "0";
	$filter->order_date_from = $order_date_between[0];
	$filter->order_date_to = isset($order_date_between[1]) ? $order_date_between[1] : "";
	$filter->payment_status = parent::get_input_post("payment_status");

	try {
	    $list_data = $this->M_profile_member->get_order_list($filter);
	    parent::set_list_data($this->data, $list_data);
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	}

	$output = array(
	    "sEcho" => parent::get_input_post("draw"),
	    "iTotalRecords" => $list_data[0][0]->total_rec,
	    "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
	    "aaData" => array()
	);

	if (isset($list_data[1])) {
	    foreach ($list_data[1] as $data_row) {
		$row = array("DT_RowId" => $data_row->seq,
		    "member_name" => $data_row->member_name,
		    "order_no" => $data_row->order_no,
		    "order_date" => $data_row->order_date,
		    "receiver_name" => $data_row->receiver_name,
		    "receiver_address" => $data_row->receiver_address,
		    "payment_status" => $data_row->payment_status,
		    "paid_date" => $data_row->paid_date,
		    "detail" => "<a href='order_list_detail/" . $data_row->seq . "'>Detail</a>",
		    "payment" => "<a href='payment_retry/" . $data_row->order_no . "'>Detail</a>");
		$output['aaData'][] = $row;
	    }
	};
	echo json_encode($output);
	die();
    }

    public function additional() {

	$filter = new stdClass;
	$filter->user_id = parent::get_member_email();
	$filter->ip_address = parent::get_ip_address();
	$filter->member_seq = parent::get_member_user_seq();
	$filter->start = parent::get_input_post("start");
	$filter->length = parent::get_input_post("length");
	$filter->order = parent::get_input_post("order");
	$filter->column = parent::get_input_post("column");
//        $filter->order_no = parent::get_input_post("order_no");
//        $filter->payment_status = parent::get_input_post("payment_status");

	try {
	    $list_data = $this->M_profile_member->get_account_list($filter);
	    parent::set_list_data($this->data, $list_data);
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	}

	$output = array(
	    "sEcho" => parent::get_input_post("draw"),
	    "iTotalRecords" => $list_data[0][0]->total_rec,
	    "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
	    "aaData" => array()
	);
	if (isset($list_data[1])) {
	    foreach ($list_data[1] as $data_row) {

		$row = array("DT_RowId" => $data_row->member_seq,
		    "member_seq" => $data_row->member_seq,
		    "mutation_type" => $data_row->mutation_type,
		    "pg_method_seq" => $data_row->pg_method_seq,
		    "trx_type" => $data_row->trx_type,
		    "trx_no" => $data_row->trx_no,
		    "trx_date" => $data_row->trx_date,
		    "deposit_trx_amt" => $data_row->deposit_trx_amt,
		    "non_deposit_trx_amt" => $data_row->non_deposit_trx_amt,
		    "bank_name" => $data_row->bank_name,
		    "bank_branch_name" => $data_row->bank_branch_name,
		    "bank_acct_no" => $data_row->bank_acct_no,
		    "bank_acct_name" => $data_row->bank_acct_name,
		    "refund_date" => $data_row->refund_date,
		    "status" => $data_row->status,
		    "deposit_Keluar" => $data_row->deposit_Keluar,
		    "deposit_Masuk" => $data_row->deposit_Masuk,
		    "saldo" => $data_row->saldo,
		    "nilai_transaksi" => $data_row->nilai_transaksi
		);

		$output['aaData'][] = $row;
	    }
	};
	echo json_encode($output);
	die();
    }

    protected function get_edit() {

	$params = new stdClass();
	$params->user_id = parent::get_member_email();
	$params->ip_address = parent::get_ip_address();
	$params->member_seq = parent::get_member_user_seq();

	try {
	    $list_data = $this->M_profile_member->get_data_member($params);
	    if (isset($list_data)) {
		parent::set_data($this->data, $list_data);
	    }
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	}
    }

    protected function save_update() {
	$tab = parent::get_input_post("tab");
	switch ($tab) {
	    case TAB_CONTACT:
		$this->update_contact();
		break;
	    case TAB_ADDRESS:
		$this->update_address();
		break;
	    case TAB_PAYMENT:
		$this->update_payment();
		break;
	    case TAB_DEPOSIT:
		$this->update_deposit();
		break;
	    case TAB_SETTING:
		$this->update_settings();
		break;
	    case TAB_ORDER_LIST:
		$this->order_list();
		break;
	}
	redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
    }

    protected function update_contact() {

	$selected = new stdClass();
	$selected->user_id = parent::get_member_email();
	$selected->ip_address = parent::get_ip_address();
	$selected->member_seq = parent::get_member_user_seq();
	$selected->member_name = parent::get_input_post("member_name");
	$selected->birthday = parent::get_input_post("birthday");
	$selected->gender = parent::get_input_post("gender");
	$selected->mobile_phone = parent::get_input_post("mobile_phone");

	try {
	    $this->M_profile_member->trans_begin();
	    $this->M_profile_member->save_update_contact($selected);
	    $this->M_profile_member->trans_commit();
	} catch (BusisnessException $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_profile_member->trans_rollback();
	} catch (TechnicalException $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_profile_member->trans_rollback();
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_profile_member->trans_rollback();
	}
    }

    protected function update_address() {

	$selected = new stdClass();
	$selected->user_id = parent::get_member_email();
	$selected->ip_address = parent::get_ip_address();
	$selected->member_seq = parent::get_member_user_seq();
	$selected->address = parent::get_input_post("address");
	$selected->district_seq = parent::get_input_post("district_seq");
	$selected->zip_code = parent::get_input_post("zip_code");
	$selected->pic_name = parent::get_input_post("pic_name");
	$selected->phone_no = parent::get_input_post("phone_no");

	try {
	    $this->M_profile_member->trans_begin();
	    $this->M_profile_member->save_update_address($selected);
	    $this->M_profile_member->trans_commit();
	} catch (BusisnessException $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_profile_member->trans_rollback();
	} catch (TechnicalException $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_profile_member->trans_rollback();
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_profile_member->trans_rollback();
	}
    }

    protected function update_payment() {

	$selected = new stdClass();
	$selected->ip_address = parent::get_ip_address();
	$selected->member_seq = parent::get_member_user_seq();
	$selected->bank_name = parent::get_input_post("bank_name");
	$selected->bank_branch_name = parent::get_input_post("bank_branch_name");
	$selected->bank_acct_no = parent::get_input_post("bank_acct_no");
	$selected->bank_acct_name = parent::get_input_post("bank_acct_name");

	try {
	    $this->M_profile_member->trans_begin();
	    $this->M_profile_member->save_update_payment($selected);
	    $this->M_profile_member->trans_commit();
	} catch (BusisnessException $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_profile_member->trans_rollback();
	} catch (TechnicalException $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_profile_member->trans_rollback();
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_profile_member->trans_rollback();
	}
    }

    protected function update_deposit() {
	$selected = new stdClass();
	$selected->user_id = parent::get_member_email();
	$selected->ip_address = parent::get_ip_address();
	$selected->member_seq = parent::get_member_user_seq();

	$getdate = $this->M_profile_member->get_date_account();
	$newdate = substr($getdate, 0, 10);
	$tgl = substr($newdate, 8);
	$bln = substr($newdate, 5, 2);
	$thn = substr($newdate, 2, 2);

	$length1 = 6;

	$characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
	$charactersLength = strlen($characters);
	$rand = '';

	for ($i = 0; $i < $length1; $i++) {
	    $rand .= $characters[rand(0, $charactersLength - 3)];
	}
	//echo $rand;
	$w = 'R';
	$r1 = substr($rand, 0, 3);
	$r2 = substr($rand, 3, 4);
	$r3 = substr($rand, 3);
	$gencode = $w . $bln . $thn . $r3;

	$selected->trx_no = $gencode;
	$selected->deposit_trx_amt = parent::get_input_post("nominal");
	$selected->bank_name = parent::get_input_post("bank");
	$selected->bank_branch_name = parent::get_input_post("cabang");
	$selected->bank_acct_no = parent::get_input_post("rekening");
	$selected->bank_acct_name = parent::get_input_post("nama_rek");


	$sel_data = $this->M_profile_member->get_data_member($selected);
	$name = $sel_data[0][0]->member_name;

	try {
	    $this->M_profile_member->trans_begin();
	    $sel_data = $this->M_profile_member->save_add_member_account($selected);
	    $seq = $sel_data[0]->new_seq;
	    $selected->email = parent::get_member_email();
	    $selected->email_cd = "MEMBER_WITHDRAW";
	    $selected->recipient_name = $name;
	    $selected->gencode = $gencode;

	    $this->load->model('component/m_email');

	    $mailtemplate = $this->M_email->get_email_template($selected);
	    $subject = $mailtemplate[0]->subject;
	    $content = $mailtemplate[0]->content;
	    $newsubject = str_replace('[RECIPIENT_NAME]', $selected->recipient_name, $subject);
	    $contentchange = array('[RECIPIENT_NAME]', '[TOTAL_DEPOSIT]', '[LINK]');
	    $contenttochange = array($selected->recipient_name, $selected->deposit_trx_amt, base_url() . "member/profile/verifikasi_deposit/" . parent::get_member_user_seq() . "-" . $seq);
	    $newcontent = str_replace($contentchange, $contenttochange, $content);

	    $selected->subject = $newsubject;
	    $selected->content = $newcontent;
	    $this->M_profile_member->add_mail_log($selected);
	    $this->M_profile_member->trans_commit();
	} catch (BusisnessException $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_profile_member->trans_rollback();
	} catch (TechnicalException $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_profile_member->trans_rollback();
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_profile_member->trans_rollback();
	}
    }

    protected function update_settings() {
	$selected = new stdClass();
	$selected->user_id = parent::get_member_email();
	$selected->ip_address = parent::get_ip_address();
	$selected->member_seq = parent::get_member_user_seq();
	$selected->old_password = parent::get_input_post("old_password", true, FILL_VALIDATOR, "Password Lama", $this->data);
	$selected->new_password = parent::get_input_post("new_password", true, FILL_VALIDATOR, "Password Baru", $this->data);

	if (strlen($selected->old_password . $selected->new_password) < 8 || strlen($selected->old_password . $selected->new_password) > 20) {
	    $this->temp_data[DATA_ERROR][ERROR] = true;
	    $this->temp_data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_VALIDATION_FILL_PASSWORD_LENGTH;
	}

	$selected->encrypt_old_password = md5(md5($selected->old_password));
	$selected->encrypt_new_password = md5(md5($selected->new_password));

	try {
	    $list_data = $this->M_profile_member->get_info_password($selected);
	    parent::set_list_data($this->temp_data, $list_data);
	} catch (Exception $ex) {
	    parent::set_error($this->temp_data, $ex);
	}

	if (isset($list_data[0])) {
	    try {
		$list_data = new stdClass();
		$list_data->ip_address = parent::get_ip_address();
		$list_data->member_seq = parent::get_member_user_seq();
		$list_data->new_password = parent::get_input_post("new_password");

		$list_data->encrypt_new_password = md5(md5($selected->new_password));

		$this->M_profile_member->trans_begin();
		$this->M_profile_member->save_update_settings($list_data);
		$this->M_profile_member->trans_commit();
	    } catch (BusisnessException $ex) {
		parent::set_error($this->data, $ex);
		$this->M_profile_member->trans_rollback();
	    } catch (TechnicalException $ex) {
		parent::set_error($this->data, $ex);
		$this->M_profile_member->trans_rollback();
	    } catch (Exception $ex) {
		parent::set_error($this->data, $ex);
		$this->M_profile_member->trans_rollback();
	    }
	} else {
	    $this->data[DATA_ERROR][ERROR] = true;
	    $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_INVALID_OLD_PASSWORD;
	}
    }

}

?>