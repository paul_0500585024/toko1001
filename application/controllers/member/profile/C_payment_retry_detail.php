<?php

require_once CONTROLLER_BASE_HOME;

class C_payment_retry_detail extends controller_base_home {

    private $data;
    private $hseq;
    private $get_user_id;
    private $get_user_seq;
    private $tipe;

    public function __construct() {
        $segments = explode('/', trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/'));
        $this->data = parent::__construct("MST005", "member/profile/payment_retry", true, $segments[3]);
        $this->initialize();
    }

    private function initialize() {
        $this->hseq = $this->uri->segment(4);
        $this->load->helper('cookie');
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_SEARCH, "search");
        $this->load->model('member/profile/M_profile_member');
        $this->load->model(LIVEVIEW . 'agent/profile/M_profile_agent');
        $this->load->model('member/M_order_member');
        $this->load->model('component/M_tree_view');
        $this->load->model('component/M_radio_button');
        $this->load->helper('url');
        $this->data['tree_category'] = $this->get_tree_category(true);

        if (parent::is_agent()) {
            $this->get_user_id = parent::get_agent_user_id();
            $this->get_user_seq = parent::get_agent_user_seq();
            $this->tipe = AGENT;
        } else {
            $this->get_user_id = parent::get_member_user_id();
            $this->get_user_seq = parent::get_member_user_seq();
            $this->tipe = '';
        }

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        if (!$this->input->post()) {
            if ($this->data[DATA_AUTH][FORM_ACTION] == "") {
                $this->get_data_header();
                $this->get_data_member();
                $this->search();
            }
            $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
            $this->template->view("member/profile/payment_retry", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->template->view("member/profile/payment_retry", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->template->view("member/profile/payment_retry", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $member_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($member_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq));
                }
            }
        }
    }

    public function get_data_member() {
        $params = new stdClass();
        $params->user_id = $this->get_user_id;
        $params->ip_address = parent::get_ip_address();
        $params->member_seq = $this->get_user_seq;
        $params->agent_seq = $this->get_user_seq;
        $menulst = $this->M_tree_view->get_payment_gateway();
        $payment_info = $this->session->userdata(SESSION_PAYMENT_INFO);

        $this->data[PAYMENT_INFO] = $payment_info;
        $this->data[TREE_PAYMENT_GATEWAY] = $this->get_menu($menulst, DEFAULT_ROOT, $payment_info["payment_seq"]);
        try {
            if (parent::is_agent()) {
                $list_data = $this->data[MEMBER_INFO] = $this->M_profile_agent->get_data_agent($params);
            } else {
                $list_data = $this->data[MEMBER_INFO] = $this->M_profile_member->get_data_member($params);
            }
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    public function search() {
        $filter = new stdClass;
        $filter->user_id = parent::get_member_email();
        $filter->ip_address = parent::get_ip_address();
        $filter->agent_seq = $this->get_user_seq;
        $filter->member_seq = $this->get_user_seq;
        $filter->hseq = $this->hseq;

        try {

            if (parent::is_agent()) {
                $list_data = $this->M_profile_agent->get_payment_retry_detail($filter);
            } else {
                $list_data = $this->M_profile_member->get_payment_retry_detail($filter);
            }


            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    public function get_data_header() {
        $params = new stdClass();
//        $params->user_id = parent::get_member_email();
        $params->user_id = $this->get_user_id;
        $params->ip_address = parent::get_ip_address();
        $params->key = $this->hseq;

        try {
            $this->data[DATA_BANK] = $this->M_radio_button->get_bank_list();
            $sel_data = $this->M_order_member->get_order_info_by_order_no($params, $this->tipe);
            if (isset($sel_data[0][0])) {
                $this->order_seq = $sel_data[0][0]->seq;
                $order_info = $this->M_order_member->get_order_info_by_order_no($params, $this->tipe);
                $this->data[DATA_ORDER] = $this->M_order_member->get_order_info_by_order_no($params, $this->tipe);
                if ($order_info[0][0]->payment_code === PAYMENT_TYPE_BCA_KLIKPAY || strlen($order_info[0][0]->order_no) < 15){
                    redirect(redirect(base_url("member/payment") . "/" . $params->key));
                }
                if ($sel_data[0][0]->promo_credit_seq > 0) {
                    $params->promo_credit_seq = $order_info[0][0]->promo_credit_seq;
                    $this->data[DATA_CREDIT_INFO] = $this->M_order_member->get_credit_info_by_credit_seq($params);
                    $this->save_update($order_info[0][0]->pg_method_seq);
                }
                parent::set_data($this->data, $sel_data);
            } else {
                redirect(base_url() . "error_404");
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->load->view(LIVEVIEW . "errors/html/error_404");
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->load->view(LIVEVIEW . "errors/html/error_404");
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->load->view(LIVEVIEW . "errors/html/error_404");
        }
    }

    public function get_menu($datas, $parent, $pg_seq = 0) {
        static $i = 1;
        $tab = str_repeat(" ", $i);
        if (isset($datas[$parent])) {
            $html = "<ul>";
            $i++;
            foreach ($datas[$parent] as $vals) {
                $child = $this->get_menu($datas, $vals->seq, $pg_seq);
                if ($child) {
                    $i++;
                    $html .= "$tab";
                    $html.='<li>' . $vals->name;
                    $html .= $child;
                    $html .= "$tab";
                } else {
                    $html .= "$tab";
                    $html.='<li><input type="radio" name=pg value ="' . $vals->seq . '"' . ($pg_seq == $vals->seq ? "checked" : "") . '/><label class = "control-label">' . $vals->name . ' </label>';
                }

                $html .= '</li>';
            }
            $html .= "$tab</ul>";
            return $html;
        } else {
            return false;
        }
    }

    protected function set_payment_info() {
        $payment_type = parent::get_input_post("payment", true, FILL_VALIDATOR, "Payment ", $this->data);
        $payment_seq = 0;

        switch ($payment_type) {
            case PAYMENT_SEQ_BANK :$payment_seq = $payment_type;
                break;
            case PAYMENT_SEQ_DEPOSIT : $payment_seq = $payment_type;
                break;
            case PAYMENT_SEQ_CREDIT : $payment_seq = $payment_type;
                break;
            case PAYMENT_SEQ_CREDIT_CARD : $payment_seq = $payment_type;
                break;
            case PAYMENT_SEQ_BCA_KLIKPAY : $payment_seq = parent::get_input_post("bank_type", true, FILL_VALIDATOR, "Tipe Internet Banking ", $this->data);
                break;
            case PAYMENT_SEQ_MANDIRI_ECASH : $payment_seq = parent::get_input_post("emoney_type", true, FILL_VALIDATOR, "Tipe Internet Banking ", $this->data);
                break;
        }
        $params = new stdClass;
        $params->user_id = parent::get_ip_address();
        $params->ip_address = parent::get_ip_address();
        $params->pg_seq = $payment_seq;

        $payment_code = $this->M_checkout_member->get_code_payment_gateway($params);
        $payment_info = array("payment_seq" => $payment_seq, "payment_code" => $payment_code[0]->code, "payment_name" => $payment_code[0]->name);
        $this->session->unset_userdata(SESSION_PAYMENT_INFO);
        $member_info[SESSION_PAYMENT_INFO] = $payment_info;
        $this->session->set_userdata($member_info);
    }

    public function save_update($paymentSeq = '') {
        $payment_type = isset($paymentSeq) && $paymentSeq != '' ? $paymentSeq : parent::get_input_post("payment", true, FILL_VALIDATOR, "Payment ", $this->data);
        switch ($payment_type) {
            case PAYMENT_SEQ_BANK :$payment_seq = parent::get_input_post("atm_type", true, FILL_VALIDATOR, "Tipe Internet Banking ", $this->data);
                break;
            case PAYMENT_SEQ_DEPOSIT : $payment_seq = $payment_type;
                break;
            case PAYMENT_SEQ_CREDIT : $payment_seq = $payment_type;
                break;
            case PAYMENT_SEQ_CREDIT_CARD : $payment_seq = $payment_type;
                break;
            case PAYMENT_SEQ_BCA_KLIKPAY : $payment_seq = parent::get_input_post("bank_type", true, FILL_VALIDATOR, "Tipe Internet Banking ", $this->data);
                break;
            case PAYMENT_SEQ_MANDIRI_ECASH : $payment_seq = parent::get_input_post("emoney_type", true, FILL_VALIDATOR, "Tipe Internet Banking ", $this->data);
                break;
            case PAYMENT_SEQ_DOCU_ALFAMART : $payment_seq = $payment_type;
                break;
            case PAYMENT_SEQ_MANDIRI_KLIKPAY : $payment_seq = parent::get_input_post("bank_type", true, FILL_VALIDATOR, "Tipe Internet Banking ", $this->data);
                break;
            case PAYMENT_SEQ_DOCU_ATM : $payment_seq = parent::get_input_post("atm_type", true, FILL_VALIDATOR, "Tipe Internet Banking ", $this->data);
                break;
        }
        $selected = new stdClass();
        $selected->user_id = $this->get_user_id;
        $selected->ip_address = parent::get_ip_address();
        $selected->member_seq = $this->get_user_seq;
        $selected->agent_seq = $this->get_user_seq;
        $selected->order_no = $this->hseq;
        $selected->hseq = $this->hseq;
        $selected->key = $this->hseq;
        $selected->payment_status = PAYMENT_UNPAID_STATUS_CODE;
        $selected->pg_method_seq = $payment_seq;

        if (parent::is_agent()) {
            $this->data[MEMBER_INFO] = $this->M_profile_agent->get_data_agent($selected);
        } else {
            $this->data[MEMBER_INFO] = $this->M_profile_member->get_data_member($selected);
        }
        $this->data[PAYMENT_INFO]["payment_seq"] = $selected->pg_method_seq;
        $this->data[DATA_SELECTED][LIST_DATA][] = $selected;
        $this->data[DATA_BANK] = $this->M_radio_button->get_bank_list();

        $order_info = $this->M_order_member->get_order_info_by_order_no($selected, $this->tipe);
        parent::set_data($this->data, $order_info);
        $this->data[DATA_ORDER] = $order_info;
        if ($order_info[0][0]->promo_credit_seq > 0) {
            $selected->promo_credit_seq = $order_info[0][0]->promo_credit_seq;
            $this->data[DATA_CREDIT_INFO] = $this->M_order_member->get_credit_info_by_credit_seq($selected);
        }


        if (parent::is_agent()) {
            $member = $this->M_profile_agent->get_data_agent($selected);
        } else {
            $member = $this->M_profile_member->get_data_member($selected);
        }

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                if ($order_info[0][0]->payment_status == PAYMENT_FAILED_STATUS_CODE) {
                    if ($selected->pg_method_seq == PAYMENT_SEQ_DEPOSIT) {
                        if ($member[2][0]->deposit_amt < $order_info[0][0]->total_payment) {
                            throw new Exception(ERROR_DEPOSIT_EXCEDEED);
                        } else {
                            if (parent::is_agent()) {
                                $this->M_profile_agent->trans_begin();
                                $this->M_profile_agent->save_update_payment_retry_detail($selected);
                                $this->M_profile_member->save_update_apps($selected);
                                $this->M_profile_agent->trans_commit();
                            } else {
                                $this->M_profile_member->trans_begin();
                                $this->M_profile_member->save_update_payment_retry_detail($selected);
                                $this->M_profile_member->save_update_apps($selected);
                                $this->M_profile_member->trans_commit();
                            }
                        }
                    } else {
                        if (parent::is_agent()) {
                            $this->M_profile_agent->trans_begin();
                            $this->M_profile_agent->save_update_payment_retry_detail($selected);
                            $this->M_profile_member->save_update_apps($selected);
                            $this->M_profile_agent->trans_commit();
                        } else {
                            $this->M_profile_member->trans_begin();
                            $this->M_profile_member->save_update_payment_retry_detail($selected);
                            $this->M_profile_member->save_update_apps($selected);
                            $this->M_profile_member->trans_commit();
                        }

                        redirect(base_url("member/payment" . "/" . $selected->hseq));
                    }
                } else {
                    throw new Exception(ERROR_UPDATE);
                }
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_member->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_member->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_member->trans_rollback();
            }
        }
    }

}

?>