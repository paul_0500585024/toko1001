<?php

require_once CONTROLLER_BASE_HOME;

class C_member extends controller_base_home {

    private $data;

    public function __construct() {

        $segments = explode('/', trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/'));
        if (isset($segments[2])) {
            if ($segments[2] == 'verifikasi') {
                $this->data = parent::__construct("CON002M", "member/verifikasi", true, $segments[3]);
            } else {
                $this->data = parent::__construct("CON002M", "member");
            }
        } else {
            $this->data = parent::__construct("CON002M", "member");
        }

        $this->load->helper('url');
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('component/M_dropdown_list');
        $this->load->model('member/profile/M_profile_member');
        $this->load->model('member/profile/M_trans_log_member');
        $this->load->model('member/master/M_verify_password_member');
        $this->load->model('member/M_order_member');
    }

    public function index() {

        $search = parent::get_input_post("btnSearch");
        if ($search == "true") {
            $this->get_deposit_member();
        }

        $search2 = parent::get_input_post("btnSearch2");
        if ($search2 == "true") {
            $this->get_order_list_member();
        }

        $search10 = parent::get_input_post("btnSearch10");
        if ($search10 == "true") {
            $this->get_review_member();
        }

        $search11 = parent::get_input_post("btnSearch11");
        if ($search11 == "true") {
            $this->get_message_log_list();
        }

        $detail11 = parent::get_input_post("btnDetil11");
        if ($detail11 == "true") {
            $this->get_message_one_topic();
        }


        $profiler = parent::get_input_post("profiler");
        if ($profiler == "1001") {
            $this->upload_profile_pic();
        }
        $this->get_data_member();

        $uri = $this->uri->segment(3);

        $DD = substr($uri, 2, 2);
        $MM = substr($uri, 6, 2);
        $YY = substr($uri, 10, 2);
        $trx_code = substr($uri, 12, 8);
        $data = date('Ymd', strtotime($YY . '-' . $MM . '-' . $DD));
        $date1 = date_create($data);
        $date2 = date_create(date('Ymd'));

        $params = new stdClass();
        $params->trx_no = $trx_code;
        $params->uri = $uri;
        $params->email_cd = MEMBER_WITHDRAW_CODE;
        $check_log = $this->M_verify_password_member->get_member_log($params);
        if ($this->uri->segment(2) == 'verifikasi') {
            try {
                if (isset($check_log)) {
                    if (date_diff($date1, $date2)->d < 1) {
                        $get_account = $this->M_profile_member->get_data_account($params);
                        if (isset($get_account)) {
                            if ($get_account[0]->status == WEB_STATUS_CODE) {
                                $this->M_profile_member->save_update_account($params);
                                $this->data[DATA_SUCCESS][SUCCESS] = true;
                                $this->data[DATA_SUCCESS][SUCCESS_MESSAGE][] = SUCCESS_DEPOSIT_IS_FOUND;
                            } else {
                                $this->data[DATA_ERROR][ERROR] = TRUE;
                                throw new Exception(ERROR_DEPOSIT_EXIST);
                            }
                        } else {
                            redirect(base_url() . "error_404");
                        }
                    } else {
                        $this->data[DATA_ERROR][ERROR] = TRUE;
                        throw new Exception(ERROR_VERIFICATION_DATE_DEPOSIT);
                    }
                } else {
                    redirect(base_url() . "error_404");
                }
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_member->trans_rollback();
            }
        }
        $this->data['tree_category'] = $this->get_tree_category(true);
        $this->data[EXPEDITION_NAME] = $this->M_dropdown_list->get_dropdown_expedition();
        $this->data['title'] = 'Toko1001 - Panel Akun';
        $this->template->view("member/profile/member_page", $this->data);
    }

    protected function upload_profile_pic() {
        $params = new stdClass();
        $params->user_id = parent::get_member_user_seq();
        $params->ip_address = parent::get_ip_address();
        $params->member_seq = parent::get_member_user_seq();
        $list_data = $this->M_profile_member->get_data_member($params);
        $file = str_replace(CDN_IMAGE,"",ORDER_UPLOAD_IMAGE);
        $logo_img = parent::upload_file("profile-image-upload", $file, $params->member_seq . strtotime("now"), IMAGE_TYPE, $this->data, IMAGE_DIMENSION_TRANSFER);
        $params->profile_img = $logo_img->file_name;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_profile_member->trans_begin();
                $this->M_profile_member->save_update_profile($params);
                $this->M_profile_member->trans_commit();
                if ($list_data[0][0]->profile_img != "") {
                    if (file_exists($file . $list_data[0][0]->profile_img))
                        unlink($file . $list_data[0][0]->profile_img);
                }
                redirect(base_url("member"));
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_member->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_member->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_member->trans_rollback();
            }
        }
    }

    protected function get_data_member() {
        $params = new stdClass();
        $params->ip_address = parent::get_ip_address();
        $params->member_seq = parent::get_member_user_seq();

        try {
            $list_data = $this->M_profile_member->get_data_member($params);
            if (isset($list_data)) {
                if ($list_data[0][0]->profile_img != '') {
                    $this->data['img_src'] = PROFILE_IMAGE . '/' . $list_data[0][0]->profile_img;
                } else {
                    $this->data['img_src'] = IMG_BLANK_100;
                }
                parent::set_data($this->data, $list_data);
                $params->user_id = parent::get_member_user_id();
                $sel_data1 = $this->M_profile_member->get_member_review($params);
                $this->data[DATA_SELECTED][LIST_DATA]['sel'] = $sel_data1;
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    public function get_deposit_member() {
        $filter = new stdClass;
        $filter->user_id = parent::get_member_email();
        $filter->ip_address = parent::get_ip_address();
        $filter->member_seq = parent::get_member_user_seq();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");

        try {
            $list_data = $this->M_profile_member->get_account_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );

        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                if ($data_row->deposit_Keluar > 0) {
                    $nilai_transaksi = "Deposit " . parent::cnum($data_row->deposit_Keluar);
                } else if ($data_row->deposit_Keluar == 0 && $data_row->mutation_type == NEW_STATUS_CODE && $data_row->deposit_Masuk == 0) {
                    if ($data_row->pg_method_seq == PAYMENT_SEQ_CREDIT_CARD) {
                        $nilai_transaksi = "CC " . parent::cnum($data_row->non_deposit_trx_amt);
                    } else if ($data_row->pg_method_seq == '2') {
                        $nilai_transaksi = "CCc " . parent::cnum($data_row->non_deposit_trx_amt);
                    }
                } else {
                    $nilai_transaksi = "Deposit " . parent::cnum($data_row->deposit_Masuk);
                }
                $row = array("DT_RowId" => $data_row->member_seq,
                    "member_seq" => $data_row->member_seq,
                    "mutation_type" => $data_row->mutation_type,
                    "pg_method_seq" => $data_row->pg_method_seq,
                    "trx_type" => parent::cstdes($data_row->trx_type, DEPOSIT_ACCOUNT_TYPE),
                    "trx_no" => $data_row->trx_no,
                    "trx_date" => parent::cdate($data_row->trx_date),
                    "deposit_trx_amt" => parent::cnum($data_row->deposit_trx_amt),
                    "non_deposit_trx_amt" => parent::cnum($data_row->non_deposit_trx_amt),
                    "bank_name" => $data_row->bank_name,
                    "bank_branch_name" => $data_row->bank_branch_name,
                    "bank_acct_no" => $data_row->bank_acct_no,
                    "bank_acct_name" => $data_row->bank_acct_name,
                    "refund_date" => $data_row->refund_date,
                    "status" => parent::cstdes($data_row->status, STATUS_WITHDRAW),
                    "deposit_Keluar" => parent::cnum($data_row->deposit_Keluar),
                    "deposit_Masuk" => parent::cnum($data_row->deposit_Masuk),
                    "saldo" => parent::cnum($data_row->saldo),
                    "nilai_transaksi" => $nilai_transaksi
                );

                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
        die();
    }

    public function get_order_list_member() {

        $order_date_between = explode(' - ', parent::get_input_post("order_date_between"));

        $filter = new stdClass;
        $filter->user_id = parent::get_member_user_seq();
        $filter->ip_address = parent::get_ip_address();
        $filter->member_seq = parent::get_member_user_seq();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->order_no = parent::get_input_post("order_no");
        $filter->date_search = isset($order_date_between[1]) ? "1" : "0";
        $filter->order_date_from = $order_date_between[0];
        $filter->order_date_to = isset($order_date_between[1]) ? $order_date_between[1] : "";
        $filter->payment_status = parent::get_input_post("payment_status");
        try {
            $list_data = $this->M_profile_member->get_order_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );

        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                if ($data_row->payment_status == PAYMENT_FAILED_STATUS_CODE) {
                    $order_no = "<a href='" . base_url() . "member/profile/payment_retry/" . $data_row->order_no . "' target=" . BLANK_TARGET . ">$data_row->order_no</a>";
                } else {
                    $order_no = "<a href='" . base_url() . "member/payment/" . $data_row->order_no . "' target=" . BLANK_TARGET . ">$data_row->order_no</a>";
                }
                $row = array("DT_RowId" => $data_row->seq,
                    "member_name" => parent::cdef($data_row->member_name),
                    "order_no" => $order_no,
                    "order_date" => parent::cdate($data_row->order_date),
                    "total_order" => 'Rp. ' . parent::cnum($data_row->total_order),
                    "payment_method" => ($data_row->payment_method),
                    "payment_status" => parent::cstdes($data_row->payment_status, STATUS_PAYMENT),
                    "paid_date" => parent::cdate($data_row->paid_date),
//                    "detail" => "<a href='member/profile/order_list_detail/" . $data_row->order_no . "'>Lihat</a>",
                );

                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
        die();
    }

    public function save_update() {
        $tab = parent::get_input_post("tab");
        switch ($tab) {
            case TAB_BASIC:
                $this->update_basic();
                break;
            case TAB_ADDRESS:
                $this->update_address();
                break;
            case TAB_DEPOSIT:
                $this->update_deposit();
                break;
            case TAB_PAYMENT:
                $this->update_payment();
                break;
            case TAB_CONFIG:
                $this->update_config();
                break;
            case TAB_ORDER:
                $this->order();
                break;
            case TAB_REVIEW:
                $this->insert_review();
                break;
            case TAB_MESSAGE:
                $this->update_message();
                break;
        }

        $member_info[SESSION_DATA] = $this->data;
        $this->session->set_userdata($member_info);
        redirect(base_url('member'));
    }

    protected function insert_review() {

        $selected = new stdClass();
        $selected->user_id = parent::get_member_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->orseq = $this->input->post("orseq");
        $selected->pvseq = $this->input->post("pvseq");
        $selected->review = $this->input->post("review_text");
        $selected->star = $this->input->post("star");
        $newcoment = '';

        $this->data[DATA_SELECTED][LIST_DATA][] = $selected;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_profile_member->trans_begin();
                for ($i = 0; $i <= count($selected->orseq); $i++) {
                    if ($selected->review[$i] != '' && $selected->star[$i] != 0) {
                        $newcoment = 'OK';
                        $selected->product_variant_seq = $selected->pvseq[$i];
                        $selected->order_seq = $selected->orseq[$i];
                        $selected->rate = $selected->star[$i];
                        $selected->review_text = $selected->review[$i];
                        $this->M_profile_member->save_review($selected);
                    }
                }
                $this->M_profile_member->trans_commit();
                if ($newcoment != '') {
                    $this->data[DATA_SUCCESS][SUCCESS] = true;
                } else {
                    throw new Exception("Ulasan dan rate " . " " . ERROR_VALIDATION_MUST_FILL);
                }
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_member->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_member->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_member->trans_rollback();
            }
        }
    }

    protected function update_basic() {
        $selected = new stdClass();
        $selected->user_id = parent::get_member_user_seq();
        $selected->ip_address = parent::get_ip_address();
        $selected->member_seq = parent::get_member_user_seq();
        $selected->member_name = parent::get_input_post("member_name");
        $selected->gender = parent::get_input_post("gender");
        $selected->birthday = parent::get_input_post("birthday");
        $selected->mobile_phone = parent::get_input_post("mobile_phone");

        $this->data[DATA_SELECTED][LIST_DATA][] = $selected;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                if (strlen($selected->member_name) > MAX_NAME_LENGTH) {
                    throw new Exception("Nama member" . " " . ERROR_VALIDATION_LENGTH_MAX . MAX_NAME_LENGTH);
                }

                $this->M_profile_member->trans_begin();
                $this->M_profile_member->save_update_contact($selected);
                $this->M_profile_member->trans_commit();
                $this->data[DATA_SUCCESS][SUCCESS] = true;
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_member->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_member->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_member->trans_rollback();
            }
        }
    }

    protected function update_address() {

        $selected = new stdClass();
        $selected->user_id = parent::get_member_email();
        $selected->ip_address = parent::get_ip_address();
        $selected->member_seq = parent::get_member_user_seq();
        $selected->address = parent::get_input_post("address");
        $selected->district_seq = parent::get_input_post("district_seq");
        $selected->zip_code = parent::get_input_post("zip_code");
        $selected->pic_name = parent::get_input_post("pic_name");
        $selected->phone_no = parent::get_input_post("phone_no");

        $this->data[DATA_SELECTED][LIST_DATA][] = $selected;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_profile_member->trans_begin();
                $this->M_profile_member->save_update_address($selected);
                $this->M_profile_member->trans_commit();
                $this->data[DATA_SUCCESS][SUCCESS] = true;
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_member->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_member->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_member->trans_rollback();
            }
        }
    }

    protected function update_payment() {

        $selected = new stdClass();
        $selected->user_id = parent::get_member_user_seq();
        $selected->ip_address = parent::get_ip_address();
        $selected->member_seq = parent::get_member_user_seq();
        $selected->bank_name = parent::get_input_post("bank_name");
        $selected->bank_branch_name = parent::get_input_post("bank_branch_name");
        $selected->bank_acct_no = parent::get_input_post("bank_acct_no");
        $selected->bank_acct_name = parent::get_input_post("bank_acct_name");

        $this->data[DATA_SELECTED][LIST_DATA][] = $selected;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                if (strlen($selected->bank_name) > MAX_BANK_LENGTH) {
                    throw new Exception("Bank" . " " . ERROR_VALIDATION_LENGTH_MAX . MAX_BANK_LENGTH);
                }
                if (strlen($selected->bank_branch_name) > MAX_BANK_BRANCE_LENGTH) {
                    throw new Exception("Cabang" . " " . ERROR_VALIDATION_LENGTH_MAX . MAX_BANK_BRANCE_LENGTH);
                }
                if (strlen($selected->bank_acct_name) > MAX_NAME_LENGTH) {
                    throw new Exception("Rekening A/N" . " " . ERROR_VALIDATION_LENGTH_MAX . MAX_NAME_LENGTH);
                }
                $this->M_profile_member->trans_begin();
                $this->M_profile_member->save_update_payment($selected);
                $this->M_profile_member->trans_commit();
                $this->data[DATA_SUCCESS][SUCCESS] = true;
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_member->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_member->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_member->trans_rollback();
            }
        }
    }

    protected function update_deposit() {
        $selected = new stdClass();
        $selected->user_id = parent::get_member_email();
        $selected->ip_address = parent::get_ip_address();
        $selected->member_seq = parent::get_member_user_seq();
        $selected->mutation_type = DEBET_MUTATION_TYPE;
        $selected->trx_type = WITHDRAW_ACCOUNT_TYPE;
        $selected->pg_method_seq = PAYMENT_SEQ_DEPOSIT;
        $selected->member_name = parent::get_member_name();
        $selected->status = WEB_STATUS_CODE;


        $refund_code = REFUND_STATUS_CODE . date('my') . parent::generate_random_text(3, true);
        $gencode = parent::generate_random_alphabet('2', true) . date('d') . parent::generate_random_alphabet(2, true) . date('m') . parent::generate_random_alphabet(2, true) . date('y');
        $code_mail = $gencode . $refund_code;

        $selected->trx_no = $refund_code;
        $selected->deposit_trx_amt = str_replace(",","",parent::get_input_post("amount"));
        $selected->non_deposit_trx_amt = "0";
        $selected->bank_name = parent::get_input_post("bank_name");
        $selected->bank_branch_name = parent::get_input_post("bank_branch_name");
        $selected->bank_acct_no = parent::get_input_post("bank_acct_no");
        $selected->bank_acct_name = parent::get_input_post("bank_acct_name");

        $data_member = $this->M_profile_member->get_data_member($selected);

        $this->data[DATA_SELECTED][LIST_DATA][] = $selected;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                if (parent::get_input_post("amount") != "") {
                    if (parent::get_input_post("amount") <= 0) {
                        throw new Exception(ERROR_DEPOSIT_NOMINAL);
                    } else {
                        if (str_replace(",","",parent::get_input_post("amount")) > $data_member[2][0]->deposit_amt) {
                            throw new Exception(ERROR_DEPOSIT);
                        } else {
                            $this->M_profile_member->trans_begin();
                            $this->M_profile_member->save_add_member_account($selected);

                            $params = new stdClass();
                            $params->user_id = parent::get_member_user_seq();
                            $params->ip_address = parent::get_ip_address();
                            $params->code = MEMBER_WITHDRAW_CODE;
                            $params->email_code = $code_mail;
                            $params->RECIPIENT_NAME = parent::get_member_name();
                            $params->TOTAL_DEPOSIT = parent::cnum($selected->deposit_trx_amt);
                            $params->LINK = base_url("member/verifikasi/" . $code_mail);
                            $params->to_email = parent::get_member_email();

                            parent::email_template($params);

                            $this->M_profile_member->trans_commit();
                            $this->data[DATA_SUCCESS][SUCCESS] = true;
                            $this->data[DATA_SUCCESS][SUCCESS_MESSAGE][] = SUCCESS_SAVE_DEPOSIT;
                        }
                    }
                } else {
                    throw new Exception(ERROR_DEPOSIT_NOMINAL_MUST_FILL);
                }
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_member->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_member->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_member->trans_rollback();
            }
        }
    }

    protected function update_config() {
        $selected = new stdClass();
        $selected->user_id = parent::get_member_email();
        $selected->ip_address = parent::get_ip_address();
        $selected->member_seq = parent::get_member_user_seq();
        $selected->old_password = parent::get_input_post("old_password", true, FILL_VALIDATOR, "Password Lama", $this->data);
        $selected->new_password = parent::get_input_post("new_password", true, FILL_VALIDATOR, "Password Baru", $this->data);

        if (strlen($selected->old_password . $selected->new_password) < 8 || strlen($selected->old_password . $selected->new_password) > 20) {
            $this->temp_data[DATA_ERROR][ERROR] = true;
            $this->temp_data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_VALIDATION_FILL_PASSWORD_LENGTH;
        }

        $selected->encrypt_old_password = md5(md5($selected->old_password));
        $selected->encrypt_new_password = md5(md5($selected->new_password));

        try {
            $list_data = $this->M_profile_member->get_info_password($selected);
            parent::set_list_data($this->temp_data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->temp_data, $ex);
        }

        if ($selected->encrypt_old_password == $list_data[0]->old_password) {
            try {
                $list_data = new stdClass();
                $list_data->ip_address = parent::get_ip_address();
                $list_data->user_id = parent::get_member_user_seq();
                $list_data->new_password = parent::get_input_post("new_password");

                $list_data->encrypt_new_password = md5(md5($selected->new_password));

                $this->M_profile_member->trans_begin();
                $this->M_profile_member->save_update_settings($list_data);
                $this->M_trans_log_member->save_member_change_password_log($selected);
                $this->M_profile_member->trans_commit();
                $this->data[DATA_SUCCESS][SUCCESS] = true;
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_member->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_member->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_profile_member->trans_rollback();
            }
        } else {
            $this->data[DATA_ERROR][ERROR] = true;
            $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_INVALID_OLD_PASSWORD;
        }
    }

    public function get_review_member() {
        $filter = new stdClass;
        $filter->user_id = parent::get_member_email();
        $filter->ip_address = parent::get_ip_address();
        $filter->member_seq = parent::get_member_user_seq();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");

        try {
            $list_data = $this->M_profile_member->get_review_member_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );


        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $bintang = str_repeat('<i class="fa fa-fw fa-star"></i>', $data_row->rate);
                $row = array("DT_RowId" => $data_row->product_variant_seq . '-' . $data_row->order_seq,
                    "product_name" => '<a href="' . url_title($data_row->product_name . " " . $data_row->seq) . '"><center><img src="' . PRODUCT_UPLOAD_IMAGE . $data_row->merchant_seq . '/' . XS_IMAGE_UPLOAD . $data_row->pic_1_img . '" class = "img-thumbnail"></center></a>',
                    "order_date" => '<a href="' . url_title($data_row->product_name . " " . $data_row->seq) . '"><b>' . $data_row->product_name . '</b></a><br />Nomor Order &nbsp; : <b>' . $data_row->order_no . '</b><br />Tanggal Order &nbsp;: ' . parent::cdate($data_row->order_date),
                    "rate" => '<p class="text-primary">' . $bintang . "<br />" . parent::cdate($data_row->created_date) . "</p>" . $data_row->review . ""
                );
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
        die();
    }

    protected function update_message() {
        $selected = new stdClass();
        $selected->ip_address = parent::get_ip_address();
        $selected->member_seq = parent::get_member_user_seq();
        $selected->user_id = parent::get_member_email();
        $selected->content = parent::get_input_post("message_text");
        $selected->prev_message_seq = parent::get_input_post("prev_message_seq");
        if (parent::get_input_post("message_text") == '' || parent::get_member_user_seq() == '') {
            $data[DATA_ERROR][ERROR] = true;
            $data[DATA_ERROR][ERROR_MESSAGE][] = "ERROR : Data Kosong";
            die();
        }
        try {
            $this->M_profile_member->trans_begin();
            $this->M_profile_member->save_insert_message($selected);
            $this->M_profile_member->trans_commit();
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_profile_member->trans_rollback();
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_profile_member->trans_rollback();
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_profile_member->trans_rollback();
        }
        die();
    }

    public function get_message_log_list() {
        $filter = new stdClass;
        $filter->user_id = parent::get_member_email();
        $filter->ip_address = parent::get_ip_address();
        $filter->member_seq = parent::get_member_user_seq();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
	$filter->status = parent::get_input_post("status");

        try {
            $list_data = $this->M_profile_member->get_message_log_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );

	if (isset($list_data[1])) {
	    foreach ($list_data[1] as $data_row) {
		if ($data_row->member_seq !== NULL) {
		    $user_name = "ANDA";
		} else {
		    $user_name = "ADMIN";
		}
		$row = array("DT_RowId" => $data_row->seq,
		    "seq" => parent::cdef($data_row->seq),
		    "headers" => ($data_row->prev_message_seq != null ? $data_row->prev_message_seq : $data_row->seq),
		    "content" => parent::get_trim_text(parent::cdef($data_row->content), 100),
		    "prev_message_seq" => $data_row->prev_message_seq,
		    "member_seq" => $data_row->member_seq,
		    "member_name" => $data_row->member_name,
		    "admin_id" => $data_row->admin_id,
		    "admin_name" => $data_row->admin_name,
		    "user_name" => $user_name,
		    "status" => $data_row->status,
		    "created_date" => parent::cdate($data_row->created_date)
		);
		$output['aaData'][] = $row;
	    }
	};
	echo json_encode($output);
	die();

    }

    public function get_message_one_topic() {
        $filter = new stdClass;
        $filter->user_id = parent::get_member_email();
        $filter->ip_address = parent::get_ip_address();
        $filter->member_seq = parent::get_member_user_seq();
        $filter->seq = parent::get_input_post("seq");
        try {
            $list_data = $this->M_profile_member->get_message_log_list_by_seq($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        $output = array('' => '',
            "aaData" => array()
        );
        if (isset($list_data)) {
            foreach ($list_data as $data_row) {
                if ($data_row->member_seq !== NULL) {
                    $user_name = "ANDA";
                } else {
                    $user_name = "ADMIN";
                }
                $row = array("DT_RowId" => $data_row->seq,
                    "seq" => parent::cdef($data_row->seq),
                    "headers" => ($data_row->prev_message_seq != null ? $data_row->prev_message_seq : $data_row->seq),
                    "content" => nl2br(parent::cdef($data_row->content)),
                    "prev_message_seq" => $data_row->prev_message_seq,
                    "member_seq" => $data_row->member_seq,
                    "member_name" => $data_row->member_name,
                    "admin_id" => $data_row->admin_id,
                    "admin_name" => $data_row->admin_name,
                    "user_name" => $user_name,
                    "status" => $data_row->status,
                    "created_date" => parent::cdate($data_row->created_date)
                );
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
        die();
    }

}
