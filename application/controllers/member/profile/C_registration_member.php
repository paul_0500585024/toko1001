<?php

require_once CONTROLLER_BASE_HOME;

class C_registration_member extends controller_base_home {

//    private $data;

    public function __construct() {
        $this->data = parent::__construct("", "", FALSE);
        $this->initialize();
    }

    private function initialize() {
        if (isset($_SESSION[SESSION_TEMP_DATA])) {
            $this->data = $_SESSION[SESSION_TEMP_DATA];
            unset($_SESSION[SESSION_TEMP_DATA]);
        }
        $this->load->model("member/master/M_signup_member");
    }

    private function create_captcha() {
        $this->load->helper('captcha');

        $options = array('img_path' => TEMP_CAPTCHA,
            'img_url' => CDN_IMAGE . TEMP_CAPTCHA,
            'img_width' => '188',
            'img_height' => '65',
            'expiration' => 7200,
            'word_length' => '5',
            'font_path' => './system/fonts/ARIALNB.TTF',
            'pool' => CAPTCHA_POOL);

        $cap = create_captcha($options);
        $image = $cap[IMAGE];
        $word = $cap[WORD];
        $this->session->set_userdata(CAPTCHA, $word);
        $image_captcha = array($image, $word);
        return $image_captcha;
    }

    public function index() {
        if (isset($_SESSION[SESSION_MEMBER_UID]) && isset($_SESSION[SESSION_MEMBER_UNAME]) && isset($_SESSION[SESSION_MEMBER_USER_GROUP]) && isset($_SESSION[SESSION_MEMBER_EMAIL]) && isset($_SESSION[SESSION_MEMBER_SEQ]) && isset($_SESSION[SESSION_MEMBER_CSRF_TOKEN]) && isset($_SESSION[SESSION_MEMBER_LAST_LOGIN]) && isset($_SESSION[SESSION_TYPE_LOGIN]) && isset($_SESSION[SESSION_IP_ADDR])) {
            redirect(base_url());
        }
        $this->data['tree_category'] = $this->get_tree_category(true);

        if ($this->input->post('btnSave') != "") {
            $gencode = parent::generate_random_text(20, true);
            $params = new stdClass();
            $params->user_id = '';
            $params->ip_address = parent::get_ip_address();
            $params->email = parent::get_input_post("member_email");
            $params->name = parent::get_input_post("member_name");
            $params->date = parent::get_input_post("member_birthday");
            $params->phone = parent::get_input_post("member_phone");
            $params->gender = parent::get_input_post("gender_member");
            $params->password = md5(md5(parent::get_input_post("password1")));
            $params->code = MEMBER_REG_CODE;
            $params->email_code = $gencode;
            $params->RECIPIENT_NAME = $params->name;
            $params->LINK = base_url() . VERIFICATION_MEMBER . $gencode;
            $params->to_email = $params->email;

            $this->data[DATA_SELECTED][LIST_DATA][] = $params;
            try {
                $selected = new stdClass();
                $selected->email = parent::get_input_post('member_email');
                $user_captcha = parent::get_input_post("captcha_member");
                $captcha = $this->session->userdata(CAPTCHA);
                $check_password = parent::get_input_post("password1");
                $result = $this->M_signup_member->check_user_exist($selected);

                if (filter_var($selected->email, FILTER_VALIDATE_EMAIL) === false) {
                    throw new Exception(ERROR_VALIDATION_FILL_EMAIL_FORMAT);
                }

                if (isset($result)) {
                    throw new exception(ERROR_EMAIL_CHECK);
                }

                if (parent::get_input_post("member_requirement") != 1) {
                    throw new exception(ERROR_REQUIREMENT);
                }

                if ($check_password != parent::get_input_post("password2")) {
                    throw new exception(ERROR_PASSWORD);
                }

                if ($captcha != $user_captcha) {
                    throw new exception(ERROR_CAPTCHA_CHECK);
                }
                $this->M_signup_member->trans_begin();
                $this->M_signup_member->save_add($params);
                parent::email_template($params);
                $this->data[DATA_SUCCESS][SUCCESS] = true;
                $this->data[DATA_SUCCESS][SUCCESS_MESSAGE][] = SUCCESS_REGISTRATION_MEMBER;
                $this->M_signup_member->trans_commit();
                unset($this->data[DATA_SELECTED][LIST_DATA]);
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_signup_member->trans_rollback();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_signup_member->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_signup_member->trans_rollback();
            }
            $admin_info[SESSION_TEMP_DATA] = $this->data;
            $this->session->set_userdata($admin_info);
            redirect(base_url("registration"));
        }

        $image = $this->create_captcha();
        $this->data[IMAGE] = $image[0];
        $this->template->view("member/profile/registration_member", $this->data);
    }

//
//    public function save_register() {
//        die('a');
//        $params = new stdClass();
//
//        $gencode = parent::generate_random_text(20, true);
//        $params->user_id = '';
//        $params->ip_address = parent::get_ip_address();
//        $params->email = parent::get_input_post("member_email");
//        $params->name = parent::get_input_post("member_name");
//        $params->date = parent::get_input_post("member_birthday");
//        $params->phone = parent::get_input_post("member_phone");
//        $params->gender = parent::get_input_post("gender_member");
//        $params->password = md5(md5(parent::get_input_post("password1")));
//        $params->code = MEMBER_REG_CODE;
//        $params->email_code = $gencode;
//        $params->RECIPIENT_NAME = $params->name;
//        $params->LINK = base_url() . VERIFICATION_MEMBER . $gencode;
//        $params->to_email = $params->email;
//        $this->data[DATA_SELECTED][LIST_DATA][] = $params;
//
//        try {
//            $this->M_signup_member->trans_begin();
//            $this->M_signup_member->save_add($params);
//            parent::email_template($params);
//            $this->M_signup_member->trans_commit();
//        } catch (BusisnessException $ex) {
//            parent::set_error($this->data, $ex);
//            $this->M_signup_member->trans_rollback();
//        } catch (TechnicalException $ex) {
//            parent::set_error($this->data, $ex);
//            $this->M_signup_member->trans_rollback();
//        } catch (Exception $ex) {
//            parent::set_error($this->data, $ex);
//            $this->M_signup_member->trans_rollback();
//        }
//    }
}

?>
