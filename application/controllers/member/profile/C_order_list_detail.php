<?php

require_once CONTROLLER_BASE_HOME;

class C_order_list_detail extends controller_base_home {

    private $data;
    private $hseq;

    public function __construct() {

        $this->data = parent::__construct("MST004", "member/profile/order_list_detail");
        $this->initialize();
    }

    private function initialize() {

        $this->hseq = $this->uri->segment(4);
        $this->load->model('member/profile/M_profile_member');
        $this->load->model('member/M_order_member');
    }

    public function index() {

        $search = parent::get_input_post("btnSearch");
        if ($search == "true") {
            $this->search();
        }
        $this->get_data_header();

        $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
        $this->template->view("member/profile/order_list_detail", $this->data);
    }

    public function search() {

        $filter = new stdClass;
        $filter->user_id = parent::get_member_email();
        $filter->ip_address = parent::get_ip_address();
        $filter->member_seq = parent::get_member_user_seq();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->hseq = $this->hseq;

        try {
            $list_data = $this->M_order_member->get_order_list_detail($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );

        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->order_seq,
                    "img_src" => "<a href ='" . base_url() . url_title($data_row->product_name . " " .
                            $data_row->product_variant_seq) . "'><img src='" . base_url(PRODUCT_UPLOAD_IMAGE) . "/" .
                    $data_row->merchant_seq . "/" . $data_row->img_src .
                    "'width = 100px  height=100px style='max-width:100px; max-height:100px'></a>&nbsp;" .
                    parent::cdef($data_row->product_name) . ' - ' . parent::cdef($data_row->value),
                    "sell_price" => parent::cnum($data_row->sell_price),
                    "qty" => parent::cnum($data_row->qty),
                    "p_weight_kg" => parent::cnum($data_row->p_weight_kg, 2),
                    "total_ship_charged" => parent::cnum($data_row->ship_price_charged * ceil($data_row->p_weight_kg * $data_row->qty)),
//                    "total_ship_charged" => parent::cnum($data_row->total_ship_charged),
                    "total_price" => parent::cnum($data_row->sell_price + $data_row->ship_price_charged * ceil($data_row->p_weight_kg)),
                    "merchant_name" => "<a href ='" . base_url() . 'merchant/' . url_title($data_row->code) . "'> $data_row->merchant_name </a>",
                    "order_status" => parent::cstdes($data_row->order_status, STATUS_ORDER),
                    "awb_no" => $data_row->awb_no,
                    "ship_date" => parent::cdate($data_row->ship_date)
                );
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
        die();
    }

    public function get_data_header() {

        $params = new stdClass();
        $params->user_id = parent::get_member_email();
        $params->ip_address = parent::get_ip_address();
        $params->hseq = $this->hseq;

        try {
            $sel_data = $this->M_order_member->get_order_info($params);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            } else {
                redirect(base_url() . "error_404");
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

}

?>