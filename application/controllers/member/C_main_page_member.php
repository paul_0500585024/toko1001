<?php

require_once CONTROLLER_BASE_MEMBER;

class C_main_page_member Extends controller_base_member {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("HM01001", "member/main_page");
    }

    public function index() {
        $this->load->model('member/M_main_page_member');
        $this->load->model('home/M_product');

        $action = parent::get_input_post("type");
//            $this->session->unset_userdata(SESSION_PRODUCT_INFO);
        switch ($action) {
            case TASK_TYPE_ADD : {
                    $this->add_cart_to_session();
                }break;
            case TASK_TYPE_DELETE : {
                    $this->delete_cart_from_session();
                }break;
            case TASK_TYPE_ADD_QTY : {
                    $this->checkout_product();
                }break;
            default : $this->search();
        }
    }

    public function add_cart_to_session() {

        $filter = new stdClass();
        $filter->user_id = parent::get_member_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->product_seq = parent::get_input_post('product_seq');
        $filter->exp_seq = DEFAULT_EXPEDITION_SEQ_RATE;
        $filter->agent = parent::is_agent() ? AGENT : "";
        $filter->agent_seq = parent::is_agent() ? parent::get_agent_user_id() : "";

        try {
            if ($this->session->userdata(SESSION_PRODUCT_INFO) == null || !array_key_exists($filter->product_seq, $this->session->userdata(SESSION_PRODUCT_INFO))) {
                $list_data = $this->M_product->get_product_info($filter);
                if (isset($list_data)) {
                    $data_credit = $this->M_product->get_product_credit($filter);   // search promo credit
                    $credit_seq = 0;
                    if (isset($data_credit)) {
                        $credit_seq = $data_credit[0]->seq;
                    }
                    $member_info[SESSION_PRODUCT_INFO] = $this->session->userdata(SESSION_PRODUCT_INFO);
                    $product_volum_weight = round(($list_data[0]->b_length_cm * $list_data[0]->b_width_cm * $list_data[0]->b_height_cm) / $list_data[0]->volume_divider, 2);
                    $product_weight = $list_data[0]->p_weight_kg > $product_volum_weight ? $list_data[0]->p_weight_kg : $product_volum_weight;
                    $promo_credit_bank_seq = $this->get_input_post("credit");
                    $product_info[$filter->product_seq] = array(
                        "count" => sizeof($member_info[SESSION_PRODUCT_INFO]) + 1,
                        "merchant_seq" => $list_data[0]->merchant_seq,
                        "merchant_name" => $list_data[0]->merchant_name,
                        "merchant_code" => $list_data[0]->merchant_code,
                        "product_seq" => $list_data[0]->product_seq,
                        "product_name" => $list_data[0]->product_name,
                        "product_price" => $list_data[0]->product_price,
                        "disc_percent" => $list_data[0]->disc_percent,
                        "sell_price" => $list_data[0]->sell_price,
                        "max_buy" => $list_data[0]->max_buy,
                        "img" => $list_data[0]->pic_1_img,
                        "exp_code" => $list_data[0]->exp_code,
                        "exp_name" => $list_data[0]->exp_name,
                        "variant_value" => $list_data[0]->variant_value,
                        "variant_name" => $list_data[0]->variant_name,
                        "volume_divider" => $list_data[0]->volume_divider,
                        "product_weight" => $product_weight,
                        "p_weight_kg" => $list_data[0]->p_weight_kg,
                        "p_length_cm" => $list_data[0]->p_length_cm,
                        "p_width_cm" => $list_data[0]->p_width_cm,
                        "p_height_cm" => $list_data[0]->p_height_cm,
                        "b_weight_kg" => $list_data[0]->b_weight_kg,
                        "b_length_cm" => $list_data[0]->b_length_cm,
                        "b_width_cm" => $list_data[0]->b_width_cm,
                        "b_height_cm" => $list_data[0]->b_height_cm,
                        "qty" => DEFAULT_QTY_PRODUCT,
                        "exp_fee" => 0,
                        "exp_real_fee" => 0,
                        "promo_seq" => 0,
                        "member_message" => "",
                        "credit_seq" => $credit_seq,
                        "promo_credit_bank_seq" => $promo_credit_bank_seq === "" ? 0 : $promo_credit_bank_seq,
                        "commission_fee" => (parent::is_agent() ? $list_data[0]->commission_fee : 0),
                        "commission_fee_percent" => (parent::is_agent() ? $list_data[0]->commission_fee_percent : 0)
                    );
//
                    $member_info[SESSION_PRODUCT_INFO][$filter->product_seq] = $product_info;
                    $this->session->set_userdata($member_info);
                    echo json_encode($product_info);
                    die();
                }
            } else {
                $product_exists[$filter->product_seq] = array("count" => 0);
                echo json_encode($product_exists);
                die();
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    public function delete_cart_from_session() {
        $product_seq = parent::get_input_post("product_seq");
        $product_info = $this->session->userdata(SESSION_PRODUCT_INFO);
        unset($product_info[$product_seq]);
        $member_info[SESSION_PRODUCT_INFO] = $product_info;
        $this->session->unset_userdata(SESSION_PRODUCT_INFO);
        $this->session->set_userdata($member_info);
        parent::set_json_success();
    }

    public function search() {

        $filter = new stdClass();
        $filter->user_id = parent::get_member_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->page = parent::get_input_get("page");
        $filter->record_per_page = DEFAULT_RECORD_PER_PAGE;
        $filter->filter = "";
        $filter->search = "";

        try {
            $list_data = $this->M_main_page_member->get_list_product($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        $this->load->view(LIVEVIEW . "member/main_page_member", $this->data);
    }

    protected function checkout_product() {
        $product_seq = parent::get_input_post("product_seq");
        $product_qty = parent::get_input_post("qty_product");
        $product_info = $this->session->userdata(SESSION_PRODUCT_INFO);

        if ($product_qty < DEFAULT_QTY_PRODUCT) {
            $product_qty = DEFAULT_QTY_PRODUCT;
        }

        if ($product_qty > $product_info[$product_seq][$product_seq]["max_buy"]) {
            $product_qty = $product_info[$product_seq][$product_seq]["max_buy"];
        }

        $product_info[$product_seq][$product_seq]["qty"] = $product_qty;
        $member_info[SESSION_PRODUCT_INFO] = $product_info;
        $this->session->unset_userdata(SESSION_PRODUCT_INFO);
        $this->session->set_userdata($member_info);
    }

}
?>

