<?php

require_once CONTROLLER_BASE_MEMBER;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of c_province
 *
 * @author Jartono
 */
class C_shopping_cart_member extends controller_base_member {

    private $data;

    public function __construct() {
	$this->data = parent::__construct("TRX01001", "member/shopping_cart");
	$this->initialize();
    }

    private function initialize() {
	$this->load->model('admin/master/M_province_admin');
    }

    public function index() {
	$action = parent::get_input_post("type");

	switch ($action) {
	    case TASK_TYPE_DELETE : {
		    $this->delete_product();
		}break;
	    case TASK_TYPE_CHECKOUT : {
		    $this->checkout_product();
		}break;
	    default : $this->shopping_cart();
	}
    }

    public function shopping_cart() {
	$this->data[DATA_PROD] = $this->session->userdata(SESSION_PRODUCT_INFO);
	$this->load->view(LIVEVIEW . 'member/shopping_cart_member', $this->data);
    }

    protected function delete_product() {
	$product_seq = parent::get_input_post("product_seq");
	$product_info = $this->session->userdata(SESSION_PRODUCT_INFO);
	unset($product_info[$product_seq]);
	$member_info[SESSION_PRODUCT_INFO] = $product_info;
	$this->session->unset_userdata(SESSION_PRODUCT_INFO);
	$this->session->set_userdata($member_info);
    }

    protected function checkout_product() {
	$product_seq = parent::get_input_post("product_seq");
	$product_qty = parent::get_input_post("qty_product");
	$product_info = $this->session->userdata(SESSION_PRODUCT_INFO);


	$product_info[$product_seq][$product_seq]["qty"] = $product_qty;
	$member_info[SESSION_PRODUCT_INFO] = $product_info;
	$this->session->unset_userdata(SESSION_PRODUCT_INFO);
	$this->session->set_userdata($member_info);
    }

}

?>