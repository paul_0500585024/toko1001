<?php

require_once CONTROLLER_BASE_MEMBER;

class C_signup_member extends controller_base_member {

    public function __construct() {
        parent::__construct("", "", false);
        $this->initialize();
    }

    private function initialize() {
        $this->load->model("member/master/M_signup_member");
    }

    private function _create_captcha() {
        $this->load->helper('captcha');
        
        $options = array(
            'img_path' => TEMP_CAPTCHA,
            'img_url' => CDN_IMAGE . TEMP_CAPTCHA,
            'img_width' => '200',
            'img_height' => '45',
            'expiration' => 7200,
            'word_length' => '5',
            'font_path' => './system/fonts/ARIALNB.TTF',
            'pool' => CAPTCHA_POOL);

        $cap = create_captcha($options);
        
        $image = $cap['image'];
        $word = $cap['word'];
        $this->session->set_userdata("captcha", $word);
        $image_captcha = array($image, $word);
        return $image_captcha;
    }

    public function index() {
        if ($this->input->post('btnemail') == 'true') {
            $member_email = parent::get_input_post('member_email');
            if (filter_var($member_email, FILTER_VALIDATE_EMAIL) === false) {
                echo "false";
                die();
            }
            echo $this->check_user($member_email);
            die();
        }
    }

    public function check_user($usr) {
        $email_member = new stdClass();
        $email_member->email = $usr;
        $result = $this->M_signup_member->check_user_exist($email_member);
        return $result ? "false" : "true";
    }

    public function captcha() {        
        $data['image_captcha'] = $this->_create_captcha();
        echo ($data['image_captcha'][0]);
        die();
    }

    public function check_member() {
        if ($this->input->post()) {

            $msg = "";
            $member_email = parent::get_input_post('member_email');
            $mails = $this->check_user($member_email);
            $captcha = $this->session->userdata('captcha');
            $incaptcha = $this->check_captcha(parent::get_input_post("captcha_member"), $captcha);
            $check_password = parent::get_input_post("password1");

            if (filter_var($member_email, FILTER_VALIDATE_EMAIL) === false) {
                $msg = ERROR_VALIDATION_FILL_EMAIL_FORMAT;
            }

            if ($member_email == "") {
                $msg = ERROR_VALIDATION_MUST_FILL_EMAIL;
            }

            if ($mails == "false") {
                $msg = ERROR_EMAIL_CHECK;
            }

            if (parent::get_input_post("member_requirement") != 1) {
                $msg = ERROR_REQUIREMENT;
            }

            if ($check_password != parent::get_input_post("password2")) {
                $msg = ERROR_PASSWORD;
            }

            if ($incaptcha == "false") {
                $msg = ERROR_CAPTCHA_CHECK;
            }
            if (strlen(parent::get_input_post('member_email')) > MAX_EMAIL_LENGTH) {
                $msg = "Email" . " " . ERROR_VALIDATION_LENGTH_MAX . MAX_EMAIL_LENGTH;
            }
            if (strlen(parent::get_input_post('member_name')) > MAX_NAME_LENGTH) {
                $msg = "Nama Member" . " " . ERROR_VALIDATION_LENGTH_MAX . MAX_NAME_LENGTH;
            }
            if ($msg == "") {
                $msg = "true";
                $this->save_register();
            }
            die(json_encode(array($msg, $this->_create_captcha())));
        } else {
            die("Error");
        }
    }

    public function check_captcha($code_captcha_member, $code) {

        if ($code_captcha_member == $code) {
            return "true";
        } else {

            return "false";
        }
    }

    private function save_register() {
        $params = new stdClass();

        $gencode = parent::generate_random_text(20, true);
        $params->user_id = '';
        $params->ip_address = parent::get_ip_address();
        $params->email = parent::get_input_post("member_email");
        $params->name = parent::get_input_post("member_name");
        $params->date = parent::get_input_post("member_birthday");
        $params->phone = parent::get_input_post("member_phone");
        $params->gender = parent::get_input_post("gender_member");
        $params->password = md5(md5(parent::get_input_post("password1")));
        $params->code = MEMBER_REG_CODE;
        $params->email_code = $gencode;
        $params->RECIPIENT_NAME = $params->name;
        $params->LINK = base_url() . VERIFICATION_MEMBER . $gencode;
        $params->to_email = $params->email;
        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        try {
            $this->M_signup_member->trans_begin();
            $this->M_signup_member->save_add($params);
            parent::email_template($params);
            $this->M_signup_member->trans_commit();
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_signup_member->trans_rollback();
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_signup_member->trans_rollback();
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_signup_member->trans_rollback();
        }
    }

}

?>
