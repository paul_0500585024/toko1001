<?php

require_once CONTROLLER_BASE_MEMBER;

class C_login_member_fb extends controller_base_member {

    private $data;

    public function __construct() {
        parent::__construct("", "", false);
        $this->initialize();
    }

    public function initialize() {
        $this->load->model('member/M_login_member');
    }

    public function index() {

        $fb_email = parent::get_input_post('email');

        if ($fb_email != Null) {
            $this->login_with_facebook();
            echo json_encode(array("error" => false));
        } else {
            $this->set_email_error();
        }
    }

    public function login_with_facebook() {

        $params = new stdClass();
        $params->ip_address = parent::get_ip_address();
        $params->user_id = parent::get_input_post('email');
        $params->name = parent::get_input_post('name');
        $params->type = parent::get_input_post('type');
        if (parent::get_input_post('birthday') != Null) {
            $params->birthday = parent::get_input_post('birthday');
        } else {
            $params->birthday = '0000-00-00';
        }
        $gender = parent::get_input_post('gender');
        if ($gender == "male") {
            $params->gender = DEFAULT_MALE_GENDER;
        } else if ($gender == "female") {
            $params->gender = DEFAULT_FEMALE_GENDER;
        } else if ($gender == "") {
            $params->gender = DEFAULT_MALE_GENDER;
        }

        if ($params->user_id == '') {
            $this->set_email_error();
        }

        $member_facebook = $this->M_login_member->get_info_member($params);

        if ($member_facebook == Null) {

            $fb_password = parent::generate_random_alnum(8);
            $params->password = (md5(md5($fb_password)));

            try {
                $this->M_login_member->trans_begin();
                $data_list = $this->M_login_member->save_facebook_member($params);

                /* Voucher for new member */
                $params->member_seq = $data_list[0]->seq;
                $params->node_cd = NODE_REGISTRATION_MEMBER;

                $this->generate_voucher($params);

                /* Send email */
                $params->user_id = $data_list[0]->seq;
                $params->code = MEMBER_REG_FROM_FACEBOOK;
                $params->email = parent::get_input_post('email');
                $params->to_email = parent::get_input_post('email');
                $params->RECIPIENT_NAME = $params->name;
                $params->password = $fb_password;

                parent::email_template($params);

                $this->M_login_member->trans_commit();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_login_member->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_login_member->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_login_member->trans_rollback();
            }
            $this->login_with_facebook();
        } else {
            $member_info[SESSION_MEMBER_UID] = $member_facebook[0]->seq;
            $member_info[SESSION_MEMBER_UNAME] = $member_facebook[0]->user_name;
            $member_info[SESSION_MEMBER_USER_GROUP] = MEMBER;
            $member_info[SESSION_MEMBER_EMAIL] = $member_facebook[0]->email;
            $member_info[SESSION_MEMBER_SEQ] = $member_facebook[0]->seq;
            $member_info[SESSION_TYPE_LOGIN] = $params->type;
            $member_info[SESSION_MEMBER_LAST_LOGIN] = $member_facebook[0]->last_login;

            $member_info[SESSION_MEMBER_CSRF_TOKEN] = base64_encode(openssl_random_pseudo_bytes(32));
            $member_info[SESSION_IP_ADDR] = $params->ip_address;

            $this->session->set_userdata($member_info);
        }
    }

    protected function set_email_error() {
        $data = new stdClass();
        $data->error = true;
        $data->message = ERROR_FACEBOOK_LOGIN_EMAIL;
        echo json_encode($data);
        exit();
    }

}
