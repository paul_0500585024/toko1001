<?php

require_once CONTROLLER_BASE_HOME;

class C_order_return_member extends controller_base_home {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("CON010M", "member/product_return");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('member/M_order_return_member');
        $this->load->model('component/M_dropdown_list');

        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_SAVE_ADD, "save_return");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_ADDITIONAL, "get_retur_member_by_no");


        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {

        $member_info[SESSION_DATA] = $this->data;
        $this->session->set_userdata($member_info);
        redirect(base_url('member'));
    }

    public function get_retur_member_by_no() {
        $filter = new stdClass;
        $filter->user_id = parent::get_member_user_seq();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->order_no = parent::get_input_post("r_order_no");
        $filter->member_seq = parent::get_member_user_seq();
        $filter->product_status = PRODUCT_READY_STATUS_CODE;
        $filter->order_status = ORDER_DELIVERED_STATUS_CODE;

        try {
            $list_data = $this->M_order_return_member->get_return_list_by_no($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );

        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $checkbox_identity = $data_row->seq . '_' . $data_row->pro_seq;
                $twoseq = $data_row->product_variant_seq . '_' . $data_row->seq;
                $chekbox = "<input validate='required[]' type='checkbox' id='printc_{$checkbox_identity}'
                        name='printc' value='{$twoseq}' class='prntorder' onclick='reset_other_checkbox(\"{$checkbox_identity}\")'>";
                $qty = $data_row->qty;
                $qty_content = "<select id='prints_{$checkbox_identity}' name='c_qty'>";
                for ($i = 1; $i < 9; $i++) {
                    if ($i <= $qty) {
                        $qty_content = $qty_content . '<option value="' . $i . '" ' . (($i == $qty) ? 'selected' : '') . '>' . $i . '</option>';
                    }
                }
                if ($data_row->variant_value_seq == 1) {
                    $img_back = parent::cdef($data_row->product_name);
                } else {
                    $img_back = parent::cdef($data_row->product_name) . ' - ' . parent::cdef($data_row->variant_name);
                }

                $qty_content .= '</select>';
                $row = array("DT_RowId" => 'row_table_return_' . $checkbox_identity,
                    "merchant_name" => $data_row->merchant_name,
                    "sell_price" => parent::cnum($data_row->sell_price),
                    "qty" => $qty_content,
                    "img" => "<img src='" . base_url(PRODUCT_UPLOAD_IMAGE) . "/" . $data_row->merchant_seq . "/" . $data_row->img . "'width = 100px  height=100px style='max-width:100px; max-height:100px'>&nbsp;<br>" . $img_back,
                    "checked" => $chekbox
                );
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
        die();
    }

    public function search() {
        $filter = new stdClass;
        $filter->user_id = parent::get_member_user_seq();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->member_seq = parent::get_member_user_seq();

        try {
            $list_data = $this->M_order_return_member->get_return_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );

        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                if ($data_row->shipment_status == SHIP_TO_MEMBER) {
                    $accepted = '<button type="button" name="btnSaveEdit" class="btn btn-flat btn-confirm" onclick="shipment(\'' . $data_row->return_no . '\')" >Terima</button>';
                } else {
                    $accepted = '';
                }
                $link = base_url() . strtolower(url_title($data_row->product_name . ' ' . $data_row->variant_name)) . '-' . $data_row->product_variant_seq;
                if ($data_row->variant_value_seq == DEFAULT_VALUE_VARIANT_SEQ) {
                    $img = "<a href=" . $link . " target=" . BLANK_TARGET . "><img src='" . base_url(PRODUCT_UPLOAD_IMAGE) . "/" . $data_row->me_seq . "/" . $data_row->img_product . "'width = 100px  height=100px style='max-width:100px; max-height:100px'>&nbsp;<br></a>" . $data_row->product_name;
                } else {
                    $img = "<a href=" . $link . " target=" . BLANK_TARGET . "><img src='" . base_url(PRODUCT_UPLOAD_IMAGE) . "/" . $data_row->me_seq . "/" . $data_row->img_product . "'width = 100px  height=100px style='max-width:100px; max-height:100px'>&nbsp;<br></a>" . $data_row->product_name . $data_row->variant_name;
                }
                $row = array("DT_RowId" => $data_row->member_seq,
                    "return_no" => $data_row->return_no,
                    "created_date" => parent::cdate($data_row->created_date),
                    "awb_member_no" => $data_row->awb_member_no,
                    "order_no" => $data_row->order_no,
                    "product_name" => $img,
                    "qty" => parent::cnum($data_row->qty),
                    "return_status" => parent::cstdes($data_row->return_status, STATUS_RETURN),
                    "shipment_status" => parent::cstdes($data_row->shipment_status, STATUS_SHIPMENT),
                    "review_member" => parent::cdef($data_row->review_member),
                    "review_admin" => parent::cdef($data_row->review_admin),
                    "review_merchant" => parent::cdef($data_row->review_merchant),
                    "accepted" => $accepted
                );
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
        die();
    }

    public function save_return() {

        $data_seq = explode('_', parent::get_input_post("printc", true, FILL_VALIDATOR, "Produk", $this->data));

        $params = new stdClass();
        $params->user_id = parent::get_member_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->return_no = time() . parent::generate_random_text(10, true);
        $params->order_seq = $data_seq[1];
        $params->product_variant_seq = $data_seq[0];
        $params->variant_variant_seq = DEFAULT_VALUE_VARIANT_SEQ;
        $params->qty = parent::get_input_post("c_qty");
        $params->return_status = NEW_STATUS_CODE;
        $params->shipment_status = SHIP_FROM_MEMBER;
        $params->review_member = parent::get_input_post("r_comment");
        $params->awb_member_no = parent::get_input_post("r_resi_no");
        $params->exp_seq_to_admin = parent::get_input_post("expedition_seq");

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_order_return_member->trans_begin();
                $valid = $this->M_order_return_member->save_add_return($params);
                if ($valid[0]->valid == '0') {
                    $this->M_order_return_member->trans_rollback();
                    throw new Exception(ERROR_QTY_TOO_MUCH);
                } else {
                    $this->M_order_return_member->trans_commit();
                    $this->data[DATA_SUCCESS][SUCCESS] = true;
                }
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_order_return_member->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_order_return_member->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_order_return_member->trans_rollback();
            }
        }
    }

    public function save_update() {
        $params = new stdClass();
        $params->user_id = parent::get_member_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->return_no = parent::get_input_post("return_no");
        $params->shipment_status = RECEIVED_BY_MEMBER;

        $get_data = $this->M_order_return_member->get_data_return($params);
        $this->data[DATA_SELECTED][LIST_DATA][] = $params;
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                if ($get_data[0]->shipment_status == SHIP_TO_MEMBER) {
                    $this->M_order_return_member->trans_begin();
                    $this->M_order_return_member->save_update($params);
                    $this->M_order_return_member->trans_commit();
                    parent::set_json_success();
                } else {
                    throw new Exception(ERROR_UPDATE);
                }
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_order_return_member->trans_rollback();
                parent::set_json_error($ex, ERROR_UPDATE);
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_order_return_member->trans_rollback();
                parent::set_json_error($ex, ERROR_UPDATE);
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_order_return_member->trans_rollback();
                parent::set_json_error($ex, ERROR_UPDATE);
            }
        }
    }

}

?>
