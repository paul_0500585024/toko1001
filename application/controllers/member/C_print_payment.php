<?php

require_once CONTROLLER_BASE_MEMBER;

class C_print_payment extends controller_base_member {

    private $data;
    private $get_user_id;
    private $get_user_seq;

    public function __construct() {
        $this->data = parent::__construct("", "member/print_payment", true);
        $this->initialize();
    }

    private function initialize() {
        if (parent::is_agent()) {
            $this->tipe = AGENT;
            $this->get_user_id = parent::get_agent_user_id();
            $this->get_user_seq = parent::get_agent_user_seq();
        } else {
            $this->tipe = "";
            $this->get_user_id = parent::get_member_user_id();
            $this->get_user_seq = parent::get_member_user_seq();
        }
        $this->load->model('member/M_order_member');
        $this->load->model('member/profile/M_profile_member');
    }

    public function index() {
        $params = new stdClass();
        $params->user_id = $this->get_user_id;
        $params->ip_address = parent::get_ip_address();
        $params->key = parent::get_input_post('ordSeq');
        try {
            $sel_data = $this->M_order_member->get_order_info_by_order_no($params, $this->tipe);
            $params->promo_credit_seq = $sel_data[0][0]->promo_credit_seq;
            $this->data[DATA_CREDIT_INFO] = $this->M_order_member->get_credit_info_by_credit_seq($params);
            parent::set_data($this->data, $sel_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        
        $this->data["total_instalment"] = isset($sel_data[0][0]->total_installment) ? $sel_data[0][0]->total_installment : "";
            if (isset($sel_data[0][0])) {
                if (isset($sel_data[0][0]->pg_method_seq) && ($sel_data[0][0]->pg_method_seq == PAYMENT_SEQ_ADIRA_DEPOSIT || $sel_data[0][0]->pg_method_seq == PAYMENT_SEQ_ADIRA)) {
                    $params->customer_seq = $sel_data[0][0]->customer_seq;
                    $this->data['customer_table'] = parent::create_table_customer_info($params);
//                    var_dump($this->data['customer_table']);die();
                    $params->order_seq = $sel_data[0][0]->seq;
                    $this->data['table_simulation'] = parent::create_table_customer_simulation_agent($params);
                    $params->agent_seq = $this->get_user_seq;
                    $list_data = $this->M_profile_member->get_data_agent($params);
                    $this->data['member_info'] = $list_data;
                } else {
                    $this->data['table_simulation'] = '';
                }
            }
        
        
        
        

        $this->load->view(LIVEVIEW . 'home/payment/print_payment', $this->data);
    }

}
