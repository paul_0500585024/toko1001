<?php

require_once CONTROLLER_BASE_MEMBER;

class C_payment_thankyou_member extends controller_base_member {

    private $data;
    
    public function __construct() {
        $this->data = parent::__construct("", "thankyou", false);
        $this->load->helper('cookie');
    }

    public function index() {
        $this->template->view('home/payment/thankyou', $this->data);
    }

}

?>