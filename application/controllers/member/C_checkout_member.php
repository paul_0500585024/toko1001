<?php

require_once CONTROLLER_BASE_MEMBER;

class C_checkout_member extends controller_base_member {

    private $data;
    private $expedition_service_seq;

    public function __construct() {
        $this->data = parent::__construct("TRX01002", "member/checkout");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('component/M_dropdown_list');
        $this->load->model('component/M_tree_view');
        $this->load->model('member/M_checkout_member');
        $this->load->model('member/M_order_member');
        $this->load->model('admin/master/M_partner_live');
        $this->load->model('admin/transaction/M_order_merchant_admin');
        $this->load->model('component/M_web_service');
        $this->load->model('component/M_radio_button');
        $this->load->model('merchant/master/M_product_stock_merchant');
        $this->load->model('merchant/transaction/M_trans_log_merchant');
        $this->load->model('member/profile/M_profile_member');
        $this->load->model('home/M_product');
        $this->load->library('curl');
        $this->load->library('Nusoap_library');
    }

    public function index() {
        $step = parent::get_input_get("step");
        $type = parent::get_input_post("type");

        $this->data[DATA_STEP] = $step;

        switch ($step) {
            case TASK_FIRST_STEP : {
                    $this->first_step();
                }break;
            case TASK_SECOND_STEP: {
                    $this->second_step();
                }break;
            case TASK_FINAL_STEP : {
                    $this->final_step();
                }break;
        }

        switch ($type) {
            case TASK_FIRST_STEP : {
                    $this->set_personal_info();
                }break;
            case TASK_SECOND_STEP : {
                    $this->set_payment_info();
                }break;
            case TASK_FINAL_STEP : {
                    if (parent::is_agent()) {
                        try {
                            if (parent::get_input_post('tenor') != 0) {
                                $this->check_data_loan();
                                if (parent::get_input_post('customer_seq') == '') {
                                    $this->save_customer_data();
                                } else {
                                    $this->data['customer_seq'] = parent::get_input_post('customer_seq');
                                    $this->save_customer_data(true);
                                }
                                $tenor = parent::get_input_post('tenor');
                                $dp = parent::get_input_post('dp');
                                $loan_info = $this->get_data_simulation($dp, $tenor, "", $total);

                                if ($dp >= $total) {
                                    throw new Exception("Uang Muka harus lebih kecil dari Harga Barang. ");
                                }

                                if ($loan_info->min_dp_value > $dp) {
                                    throw new Exception("Minimal Uang Muka harus Rp. " . number_format($loan_info->min_dp_value));
                                }
                            } else {
                                $payment_info = $this->session->userdata(SESSION_PAYMENT_INFO);
                                if ($payment_info["payment_seq"] == PAYMENT_SEQ_ADIRA || $payment_info["payment_seq"] == PAYMENT_SEQ_ADIRA_DEPOSIT) {
                                    try {
                                        throw new Exception("Tenor harus dipilih dahulu");
                                    } catch (Exception $ex) {
                                        parent::set_error($this->data, $ex);
                                    }
                                }
                            }
                            $this->data[CITY_NAME] = $this->M_dropdown_list->get_dropdown_city_by_province(isset($_SESSION[SESSION_CUSTOMER_DATA]->province_seq) ? $_SESSION[SESSION_CUSTOMER_DATA]->province_seq : 0);
                            $this->data[DISTRICT_NAME] = $this->M_dropdown_list->get_dropdown_district_by_city(isset($_SESSION[SESSION_CUSTOMER_DATA]->city_seq) ? $_SESSION[SESSION_CUSTOMER_DATA]->city_seq : 0);
                            $this->check_final_payment();
                        } catch (Exception $ex) {
                            parent::set_error($this->data, $ex);
                            $admin_info[SESSION_DATA] = $this->data;
                            $this->session->set_userdata($admin_info);
                        }
                    }
                    $this->set_order();
                }break;
            case TASK_PROVINCE_CHANGE : {
                    $this->get_city_by_province();
                }break;
            case TASK_CITY_CHANGE : {
                    $this->get_district_by_city();
                }break;
            case TASK_MEMBER_ADDRESS_CHANGE: {
                    $this->get_member_address();
                }break;
            case TASK_GET_COUPON: {
                    $this->get_coupon();
                }break;
            case TASK_CLEAR_COUPON: {
                    $this->clear_coupon();
                }break;
            case TASK_GET_BANK: {
                    $this->get_bank_credit();
                }break;
            case TASK_GET_BANK_MONTH: {
                    $this->get_bank_credit_month();
                }break;
            case TASK_GET_ONE_CREDIT: {
                    $this->get_one_credit();
                }break;
            case TASK_GET_VALUE_CREDIT: {
                    $this->get_bank_credit_month_val();
                }break;
            case "get_payment_method": {
                    $this->payment_method();
                }break;
            case TASK_CUSTOMER_SEARCH : {
                    $this->get_customer_search();
                }break;
            case TASK_CUSTOMER_DATA : {
                    $this->get_customer_data();
                }break;
            case TASK_LOAN_SIMULATION : {
                    $this->get_simulation();
                }break;
        }

        $this->template->view('home/cart', $this->data, "main_cart");
    }

    protected function get_minimal_dp($product_variant_seq, $total, $tenor, $agent_id) {
        $list_data = $this->M_checkout_member->get_info_loan($product_variant_seq, $total, $tenor, $agent_id);
    }

    protected function get_data_simulation($dp, $tenor, $sim_type, &$total) {
        $agent_id = parent::get_agent_user_id();
        if ($sim_type === "") {
            $personal_info = $this->session->userdata(SESSION_PERSONAL_INFO);
            $product_info = $this->session->userdata(SESSION_PRODUCT_INFO);
            $city_seq = $personal_info["city_seq"];
            $district_seq = $personal_info["district_seq"];
            $merchant_seq = 0;
            $product_variant_seq = 0;
            $shipping_total = 0;
            foreach ($product_info as $product => $n) {
                $product_variant_seq .= $product_variant_seq != "" ? "," . $n[$product]["product_seq"] : $n[$product]["product_seq"];
                if ($merchant_seq !== $n[$product]["merchant_seq"]) {
                    $merchant_seq = $n[$product]["merchant_seq"];
                    $promo_seq = $this->get_promo_free_fee($merchant_seq, $city_seq);

                    if (!isset($promo_seq)) {
//                        if ($this->data[DATA_ERROR][ERROR] === false) {
                        $exp_info = $this->get_expedition_info($merchant_seq, $district_seq);
                        $exp_info[0]->merchant_seq = $merchant_seq;
                        $exp_info[0]->district_seq = $district_seq;
                        $exp_service_seq = $exp_info[0]->exp_service_seq;
                        $this->expedition_service_seq = $exp_info[0]->exp_service_seq;
                        $rate_exp = $this->get_rate_expedition($exp_info);
                        $rate_real_exp = $rate_exp;
//                        } else {
//                        }
                    } else {
                        $exp_info = $this->get_expedition_info($merchant_seq, $district_seq);
                        $exp_service_seq = $exp_info[0]->exp_service_seq;
                        $this->expedition_service_seq = $exp_info[0]->exp_service_seq;
                        $exp_info[0]->merchant_seq = $merchant_seq;
                        $exp_info[0]->district_seq = $district_seq;
                        $rate_real_exp = $this->get_rate_expedition($exp_info);
                        $rate_exp = 0;
                        $product_info[$product][$product]["promo_seq"] = $promo_seq[0]->seq;
                    }
                }
                $product_info[$product][$product]["exp_fee"] = $rate_exp;
                $product_info[$product][$product]["exp_real_fee"] = $rate_real_exp;
                $product_info[$product][$product]["exp_service_seq"] = $exp_service_seq;
                $product_info[$product][$product]["exp_real_service_seq"] = $exp_service_seq == DEFAULT_EXPEDITION_SEQ_MERCHANT ? $exp_service_seq : $this->expedition_service_seq;
                $total+= $n[$product]["sell_price"] * $n[$product]["qty"];
                $shipping_total = $shipping_total + ($rate_exp * ceil($n[$product]["product_weight"] * $n[$product]["qty"]));
            }
        } else {
            $product_variant_seq = parent::get_input_post("product_variant_seq");
            $shipping_total = 0;
            $total = parent::get_input_post("total");
        }
        $ph = ($total - $dp);
        try {
            $list_data = $this->M_checkout_member->get_info_loan($product_variant_seq, $total, $tenor, $agent_id);
            if (isset($list_data[0])) {
                $admin_fee = $list_data[0][0]->admin_fee;
            }
            if (isset($list_data[1])) {
                $params = new stdClass();
                $params->minimal_loan = $list_data[1][0]->minimal_loan;
                $params->maximal_loan = $list_data[1][0]->maximal_loan;
                $params->mn_dp_percent = $list_data[1][0]->minimal_loan;
                $params->loan_interest = $list_data[1][0]->loan_interest;
                $params->min_dp_percent = $list_data[1][0]->min_dp_percent;
                $params->min_dp_value = ($list_data[1][0]->min_dp_percent / 100 * $total);
                $params->min_dp_value_num = number_format($params->min_dp_value);
                $params->tenor_value = $list_data[1][0]->tenor;
                $params->min_dp_error = "";
                if ($params->min_dp_value > $dp) {
                    $params->min_dp_error = "Minimal Uang Muka / DP (" . $params->min_dp_percent . " %) = Rp. " . number_format($params->min_dp_value);
                }
                $params->ph = number_format($ph);
                $interest = ($list_data[1][0]->loan_interest / 100);
                $angsuran = (ceil((($ph + ($ph * $params->tenor_value * $interest)) / $params->tenor_value) / 1000) * 1000);
                $params->angsuran = number_format($angsuran);
//$params->dp = number_format($dp);
                $params->loan_admin = number_format($admin_fee);
                $params->total_pay_1 = number_format($params->min_dp_value + $angsuran + $admin_fee + $shipping_total);

                $params->phs = ($ph);
                $params->angsurans = ($angsuran);
                $params->dps = ($dp);
                $params->loan_admins = ($admin_fee);
                $params->total_pay_1s = ($params->min_dp_value + $angsuran + $admin_fee + $shipping_total);

                return $params;
            } else {
                return '';
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function get_simulation() {
        $tenor = parent::get_input_post("tenor");
        $dp = parent::get_input_post("dp");
        $sim_type = parent::get_input_post("sim_type");
        $result = $this->get_data_simulation($dp, $tenor, $sim_type, $total);
        if ($result != '') {
            echo json_encode($result);
        }
        die();
    }

    protected function get_customer_search() {
        $agent_id = parent::get_agent_user_id();
        $keyword = parent::get_input_post("q");
        try {
            $list_data = $this->M_dropdown_list->get_dropdown_customer_search($keyword, $agent_id, 0);
            echo ' { "items" : ' . json_encode($list_data[0]) . ' } ';
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        die();
    }

    protected function get_customer_data() {
        $agent_id = parent::get_agent_user_seq();
        $customer_seq = parent::get_input_post("customer_seq");
        try {
            $list_data = $this->M_dropdown_list->get_dropdown_customer_search("", $agent_id, $customer_seq);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        echo json_encode($list_data);
        die();
    }

    protected function get_bank_credit_month_val() {
        $promo_credit_seq = parent::get_input_post("period");
        if ($promo_credit_seq > 0 && $promo_credit_seq != "") {
            try {
                $list_data = $this->M_dropdown_list->get_bank_credit_month_by_promoseq($promo_credit_seq);
                if (!isset($list_data))
                    $list_data = array('credit_month' => 0);
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
            }
        } else {
            $list_data = array('credit_month' => "0");
        }
        echo json_encode($list_data);
        die();
    }

    protected function get_one_credit() {
        $product_seq = parent::get_input_post("product_seq");
        $product_info = $this->session->userdata(SESSION_PRODUCT_INFO);
        foreach ($product_info as $data) {
            foreach ($data as $data1) {
                if ($data1['product_seq'] != $product_seq) {
                    unset($product_info[$data1['product_seq']]);
                    $member_info[SESSION_PRODUCT_INFO] = $product_info;
                    $this->session->unset_userdata(SESSION_PRODUCT_INFO);
                    $this->session->set_userdata($member_info);
                }
            }
        }
        die();
    }

    protected function get_bank_credit_month() {
        $product_variant_seq = parent::get_input_post("product_variant_seq");
        $bankid = parent::get_input_post("bankid");
        try {
            $list_data = $this->M_dropdown_list->get_dropdown_bank_credit_month_by_variant($product_variant_seq, $bankid);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        echo json_encode($list_data);
        die();
    }

    protected function get_bank_credit() {
        $product_variant_seq = parent::get_input_post("product_variant_seq");
        try {
            $list_data = $this->M_dropdown_list->get_dropdown_bank_credit_by_variant($product_variant_seq);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        echo json_encode($list_data);
        die();
    }

    protected function get_city_by_province() {

        $province_seq = parent::get_input_post("province_seq");
        try {
            $list_data = $this->M_dropdown_list->get_dropdown_city_by_province($province_seq);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        echo json_encode($list_data);
        die();
    }

    protected function get_district_by_city() {
        $city_seq = parent::get_input_post("city_seq");

        try {
            $list_data = $this->M_dropdown_list->get_dropdown_district_by_city($city_seq);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        echo json_encode($list_data);
        die();
    }

    protected function get_coupon() {

        $params = new stdClass();
        $params->user_id = parent::is_agent() ? parent::get_agent_user_id() : parent::get_member_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->product_variant_seq = parent::get_input_post("product_variant_seq");
        $params->coupon_code = parent::get_input_post("coupon_code");
        $params->voucher_node = NODE_COUPON_MEMBER;
        $params->total_order = parent::get_input_post("total_order");

        try {
            $list_data = $this->M_checkout_member->get_coupon($params);
            if (!isset($list_data)) {
                echo json_encode(array("error" => "true", "message" => ERROR_VALIDATION_COUPON));
            } else {
//                if ($list_data[0]->nominal > $params->total_order) {
//                    echo json_encode(array("error" => "true", "message" => ERROR_VALIDATION_NOMINAL_COUPON));
//                } else
                if ($list_data[0]->min_order > $params->total_order) {
                    echo json_encode(array("error" => "true", "message" => ERROR_VALIDATION_NOMINAL_ORDER_COUPON));
                } else {
                    $payment_info = $this->session->userdata(SESSION_PAYMENT_INFO);
                    if ($list_data[0]->coupon === '1') {
                        $payment_info["coupon_seq"] = $list_data[0]->seq;
                    } else {
                        $payment_info["voucher_seq"] = $list_data[0]->seq;
                    }
                    $this->session->unset_userdata(SESSION_PAYMENT_INFO);
                    $member_info[SESSION_PAYMENT_INFO] = $payment_info;
                    $this->session->set_userdata($member_info);
                    echo json_encode($list_data);
                }
            }
            die();
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function clear_coupon() {
        $payment_info = $this->session->userdata(SESSION_PAYMENT_INFO);
        $payment_info["coupon_seq"] = 0;
        $this->session->unset_userdata(SESSION_PAYMENT_INFO);
        $member_info[SESSION_PAYMENT_INFO] = $payment_info;
        $this->session->set_userdata($member_info);
        echo json_encode(array("error" => false));
        die();
    }

    protected function get_member_address() {

        $params = new stdClass();
        if (parent::is_agent()) {
            $params->agent_seq = parent::get_agent_user_seq();
        } else {
            $params->member_seq = parent::get_member_user_seq();
        }
        $params->seq = parent::get_input_post("member_address_seq");

        try {
            if (parent::is_agent()) {
                $list_data = $this->M_dropdown_list->get_dropdown_address_agent_by_seq($params);
            } else {
                $list_data = $this->M_dropdown_list->get_dropdown_address_member_by_seq($params);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        echo json_encode($list_data);
        die();
    }

    protected function set_personal_info() {

        try {
            $step = parent::get_input_post("step");
            $receiver_name = parent::get_input_post("receiver_name");
            $address = parent::get_input_post("address");
            $province_seq = parent::get_input_post("province_seq");
            $province_name = parent::get_input_post("province_name");
            $city_seq = parent::get_input_post("city_seq");
            $city_name = parent::get_input_post("city_name");
            $district_seq = parent::get_input_post("district_seq");
            $district_name = parent::get_input_post("district_name");
            $phone_no = parent::get_input_post("phone_no");
            $postal_code = parent::get_input_post("postal_code");
            $chk_alias = parent::get_input_post("chk_alias");
            $alias = parent::get_input_post("alias");
            $member_address_seq = parent::get_input_post("member_address_seq");

            $personal_info = array(
                "receiver_name" => $receiver_name,
                "address" => $address,
                "province_seq" => $province_seq,
                "province_name" => $province_name,
                "city_seq" => $city_seq,
                "city_name" => $city_name,
                "district_seq" => $district_seq,
                "district_name" => $district_name,
                "phone_no" => $phone_no,
                "postal_code" => $postal_code,
                "alias" => $alias,
                "chk_alias" => $chk_alias,
                "member_address" => $member_address_seq,
            );

            $this->session->unset_userdata(SESSION_PERSONAL_INFO);
            $member_info[SESSION_PERSONAL_INFO] = $personal_info;
            $this->session->set_userdata($member_info);

            if ($chk_alias == "on") {
                if ($alias == "") {
                    throw new Exception("Alias " . ERROR_VALIDATION_MUST_FILL);
                }
            }

            if (strlen($alias) > MAX_BANK_LENGTH) {
                throw new Exception("Alias " . ERROR_VALIDATION_LENGTH_MAX . MAX_BANK_LENGTH);
            }

            if (strlen($phone_no) > MAX_PHONE_LENGTH) {
                throw new Exception("No Telp " . ERROR_VALIDATION_LENGTH_MAX . MAX_PHONE_LENGTH);
            }

            if (strlen($postal_code) > MAX_POSTAL_CODE_LENGTH) {
                throw new Exception("Kode Pos " . ERROR_VALIDATION_LENGTH_MAX . MAX_POSTAL_CODE_LENGTH);
            }

            if ($city_seq == 0) {
                throw new Exception("Kota " . ERROR_VALIDATION_MUST_FILL);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $step = TASK_FIRST_STEP;
            $admin_info[SESSION_DATA] = $this->data;
            $this->session->set_userdata($admin_info);
        }

        redirect(base_url("member/checkout") . "?step=" . $step);
    }

    protected function set_payment_info() {

        $step = parent::get_input_post("step");
        $payment_type = 0;
        $payment_head = parent::get_input_post("payment_head", true, FILL_VALIDATOR, "Payment Head ", $this->data);
        $payment_type = parent::get_input_post("payment", true, FILL_VALIDATOR, "Payment ", $this->data);

//        switch ($payment_type) {
//            case PAYMENT_SEQ_BANK :$payment_seq = parent::get_input_post("atm_type", true, FILL_VALIDATOR, "Tipe Internet Banking ", $this->data);
//                break;
//            case PAYMENT_SEQ_DEPOSIT : $payment_seq = $payment_type;
//                break;
//            case PAYMENT_SEQ_CREDIT_CARD : $payment_seq = $payment_type;
//                break;
//            case PAYMENT_SEQ_BCA_KLIKPAY : $payment_seq = parent::get_input_post("bank_type", true, FILL_VALIDATOR, "Tipe Internet Banking ", $this->data);
//                break;
//            case PAYMENT_SEQ_MANDIRI_ECASH : $payment_seq = parent::get_input_post("emoney_type", true, FILL_VALIDATOR, "Tipe Internet Banking ", $this->data);
//                break;
//            case PAYMENT_SEQ_DOCU_ALFAMART : $payment_seq = $payment_type;
//                break;
//            case PAYMENT_SEQ_MANDIRI_KLIKPAY : $payment_seq = parent::get_input_post("bank_type", true, FILL_VALIDATOR, "Tipe Internet Banking ", $this->data);
//                break;
//            case PAYMENT_SEQ_DOCU_ATM : $payment_seq = parent::get_input_post("atm_type", true, FILL_VALIDATOR, "Tipe Internet Banking ", $this->data);
//                break;
//            case PAYMENT_SEQ_QR_CODE : $payment_seq = parent::get_input_post("qr_type", true, FILL_VALIDATOR, "Payment QR ", $this->data);
//                break;
//            case PAYMENT_SEQ_BNI : $payment_seq = parent::get_input_post("bni_type", true, FILL_VALIDATOR, "Payment BNI ", $this->data);
//                break;
//            case PAYMENT_SEQ_CREDIT : $payment_seq = parent::get_input_post("credit_type", true, FILL_VALIDATOR, "Cicilan ", $this->data);
//                break;
//            case PAYMENT_SEQ_ADIRA : $payment_seq = parent::get_input_post("partner_type", true, FILL_VALIDATOR, "Adira ", $this->data);
//                break;
//            case PAYMENT_SEQ_ADIRA_DEPOSIT : $payment_seq = parent::get_input_post("partner_type", true, FILL_VALIDATOR, "Adira ", $this->data);
//                break;
//        }

        $params = new stdClass;
        $params->user_id = parent::get_ip_address();
        $params->ip_address = parent::get_ip_address();
        $params->pg_seq = $payment_type;
        $payment_code = $this->M_checkout_member->get_code_payment_gateway($params);
        if ($params->pg_seq == 0) {
            $payment_info = array("payment_head" => $payment_head, "coupon_seq" => 0, "voucher_seq" => 0);
            $this->session->unset_userdata(SESSION_PAYMENT_INFO);
            $member_info[SESSION_PAYMENT_INFO] = $payment_info;
            $this->session->set_userdata($member_info);
        } else {
            $payment_info = array("payment_head" => $payment_head, "payment_seq" => $payment_type, "payment_code" => $payment_code[0]->code, "payment_name" => $payment_code[0]->name, "coupon_seq" => 0, "voucher_seq" => 0);
            $this->session->unset_userdata(SESSION_PAYMENT_INFO);
            $member_info[SESSION_PAYMENT_INFO] = $payment_info;
            $this->session->set_userdata($member_info);
            redirect(base_url("member/checkout") . "?step=" . $step);
        }
    }

    protected function first_step() {
        if ($this->session->userdata(SESSION_PRODUCT_INFO) == null) {
            $this->data[DATA_ERROR][ERROR] = true;
            $this->data[DATA_ERROR][ERROR_MESSAGE][0] = ERROR_INPUT_PRODUCT_INFO_FIRST;
            redirect(base_url());
        }
        if ($this->session->userdata(SESSION_PERSONAL_INFO) !== null) {
            $personal_info = $this->session->userdata(SESSION_PERSONAL_INFO);
            $this->data[DATA_SELECTED][LIST_DATA][0] = (object) $personal_info;
            $this->data[CITY_NAME] = $this->M_dropdown_list->get_dropdown_city_by_province($personal_info["province_seq"]);
            $this->data[DISTRICT_NAME] = $this->M_dropdown_list->get_dropdown_district_by_city($personal_info["city_seq"]);
        }
        if (parent::is_agent()) {
            $this->data[MEMBER_INFO] = $this->M_dropdown_list->get_dropdown_address_agent(parent::get_agent_user_seq());
        } else {
            $this->data[MEMBER_INFO] = $this->M_dropdown_list->get_dropdown_address_member(parent::get_member_user_seq());
        }
        $this->data[PROVINCE_NAME] = $this->M_dropdown_list->get_dropdown_province();
        $this->data['title'] = 'Isi Informasi Penerima';
    }

    protected function second_step() {
        if (parent::is_agent()) {
            $this->adira_payment_check();
            if (isset($_SESSION[SESSION_PAYMENT_INFO])) {
                if ($_SESSION[SESSION_PAYMENT_INFO]['payment_head'] == PAYMENT_SEQ_ADIRA_HEAD && count($_SESSION[SESSION_PRODUCT_INFO]) > 1) {
                    $this->session->unset_userdata(SESSION_PAYMENT_INFO);
                    $this->data[DATA_ERROR][ERROR] = true;
                    $this->data[DATA_ERROR][ERROR_MESSAGE][0] = ERROR_ADIRA_PAYMENT_COUNT_PRODUCT;
                } elseif ($_SESSION[SESSION_PAYMENT_INFO]['payment_head'] == PAYMENT_SEQ_ADIRA_HEAD && $this->get_product_qty($_SESSION[SESSION_PRODUCT_INFO]) > 1) {
                    $this->session->unset_userdata(SESSION_PAYMENT_INFO);
                    $this->data[DATA_ERROR][ERROR] = true;
                    $this->data[DATA_ERROR][ERROR_MESSAGE][0] = ERROR_ADIRA_PAYMENT_QTY;
                }
            }
        }
        if ($this->session->userdata(SESSION_PERSONAL_INFO) == null) {
            $this->data[DATA_ERROR][ERROR] = true;
            $this->data[DATA_ERROR][ERROR_MESSAGE][0] = ERROR_INPUT_PERSONAL_INFO_FIRST;
            $admin_info[SESSION_DATA] = $this->data;
            $this->session->set_userdata($admin_info);
            redirect(base_url("member/checkout") . "?step=" . TASK_FIRST_STEP);
        }

        $menulst = $this->M_tree_view->get_payment_gateway_type();
        $data_prod = $this->session->userdata(SESSION_PRODUCT_INFO);
        $new_credit = "";
        if (isset($data_prod) && sizeof($data_prod) > 0) {
            foreach ($data_prod as $key => $prod) {
                foreach ($prod as $key => $prod1) {
                    if ($prod1["credit_seq"] > 0)
                        $new_credit = "OK";
                }
            }
        }
        $this->data[DATA_PROD] = $new_credit;
        $params = new stdClass();

        $params->ip_address = parent::get_ip_address();
        if (parent::is_agent()) {
            $params->agent_seq = parent::get_agent_user_seq();
            $params->user_id = parent::get_agent_user_id();
            $this->data[MEMBER_INFO] = $this->M_profile_member->get_data_agent($params);
        } else {
            $params->member_seq = parent::get_member_user_seq();
            $params->user_id = parent::get_member_user_id();
            $this->data[MEMBER_INFO] = $this->M_profile_member->get_data_member($params);
        }
        $this->data[DATA_BANK] = $this->M_radio_button->get_bank_list();

        if ($this->session->userdata(SESSION_PAYMENT_INFO) !== null) {
            $payment_info = $this->session->userdata(SESSION_PAYMENT_INFO);
            $this->data[PAYMENT_INFO] = $payment_info;
            $this->data[TREE_PAYMENT_GATEWAY] = $menulst;
        } else {
            $this->data[TREE_PAYMENT_GATEWAY] = $menulst;
        }
        $this->data['title'] = 'Pilih Metode Pembayaran';
    }

    protected function final_step() {
        $this->check_final_payment();
        $payment_info = $this->session->userdata(SESSION_PAYMENT_INFO);

        $this->data[PROVINCE_NAME] = $this->M_dropdown_list->get_dropdown_province();

        if ($this->session->userdata(SESSION_PERSONAL_INFO) == null) {
            $this->data[DATA_ERROR][ERROR] = true;
            $this->data[DATA_ERROR][ERROR_MESSAGE][0] = ERROR_INPUT_PERSONAL_INFO_FIRST;
            $admin_info[SESSION_DATA] = $this->data;
            $this->session->set_userdata($admin_info);
            redirect(base_url("member/checkout") . "?step=" . TASK_FIRST_STEP);
        }

        if ($payment_info["payment_seq"] === 0 || !isset($payment_info["payment_seq"])) {
            $this->data[DATA_ERROR][ERROR] = true;
            $this->data[DATA_ERROR][ERROR_MESSAGE][0] = ERROR_INPUT_PAYMENT_INFO_FIRST;
            $admin_info[SESSION_DATA] = $this->data;
            $this->session->set_userdata($admin_info);
            redirect(base_url("member/checkout") . "?step=" . TASK_SECOND_STEP);
        }

        if (parent::is_agent() && isset($_SESSION[SESSION_CUSTOMER_DATA])) {
            $this->data[CITY_NAME] = $this->M_dropdown_list->get_dropdown_city_by_province($_SESSION[SESSION_CUSTOMER_DATA]->province_seq);
            $this->data[DISTRICT_NAME] = $this->M_dropdown_list->get_dropdown_district_by_city($_SESSION[SESSION_CUSTOMER_DATA]->city_seq);
        }

        $merchant_seq = 0;
        $rate_exp = 0;
        $exp_service_seq = 0;
        $total = 0;

        $personal_info = $this->session->userdata(SESSION_PERSONAL_INFO);
        $product_info = $this->session->userdata(SESSION_PRODUCT_INFO);
        $payment_info = $this->session->userdata(SESSION_PAYMENT_INFO);
        $city_seq = $personal_info["city_seq"];
        $district_seq = $personal_info["district_seq"];

        $params = new stdclass();
        $params->user_id = parent::get_member_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->member_seq = parent::get_member_user_seq();
        $product_variant_seq = "";
        if ($product_info == "") {
            redirect(base_url("index.php"));
        }
        try {
            foreach ($product_info as $product => $n) {
                $product_variant_seq .= $product_variant_seq != "" ? "," . $n[$product]["product_seq"] : $n[$product]["product_seq"];
                if ($merchant_seq !== $n[$product]["merchant_seq"]) {
                    $merchant_seq = $n[$product]["merchant_seq"];
                    $promo_seq = $this->get_promo_free_fee($merchant_seq, $city_seq);

                    if (!isset($promo_seq)) {
//                        if ($this->data[DATA_ERROR][ERROR] === false) {
                        $exp_info = $this->get_expedition_info($merchant_seq, $district_seq);
                        $exp_info[0]->merchant_seq = $merchant_seq;
                        $exp_info[0]->district_seq = $district_seq;
                        $exp_service_seq = $exp_info[0]->exp_service_seq;
                        $this->expedition_service_seq = $exp_info[0]->exp_service_seq;
                        $rate_exp = $this->get_rate_expedition($exp_info);
                        $rate_real_exp = $rate_exp;
//                        } else {
//
//                        }
                    } else {
                        $exp_info = $this->get_expedition_info($merchant_seq, $district_seq);
                        $exp_service_seq = $exp_info[0]->exp_service_seq;
                        $this->expedition_service_seq = $exp_info[0]->exp_service_seq;
                        $exp_info[0]->merchant_seq = $merchant_seq;
                        $exp_info[0]->district_seq = $district_seq;
                        $rate_real_exp = $this->get_rate_expedition($exp_info);
                        $rate_exp = 0;
                        $product_info[$product][$product]["promo_seq"] = $promo_seq[0]->seq;
                    }
                }
                $product_info[$product][$product]["exp_fee"] = $rate_exp;
                $product_info[$product][$product]["exp_real_fee"] = $rate_real_exp;
                $product_info[$product][$product]["exp_service_seq"] = $exp_service_seq;
                $product_info[$product][$product]["exp_real_service_seq"] = $exp_service_seq == DEFAULT_EXPEDITION_SEQ_MERCHANT ? $exp_service_seq : $this->expedition_service_seq;
                $total+= $n[$product]["sell_price"] * $n[$product]["qty"];
            }

            $params->total_order = $total;
            $member_info[SESSION_PRODUCT_INFO] = $product_info;
            $this->session->unset_userdata(SESSION_PRODUCT_INFO);
            $this->session->set_userdata($member_info);
            if (count($product_info) == 1) {
                foreach ($product_info as $key => $item) {
                    if ($product_info[$product][$product]["promo_credit_bank_seq"] > 0) {
                        $product_variant_seq = $product_info[$product][$product]["product_seq"];
//$bank_credits = $this->M_dropdown_list->get_dropdown_bank_credit_by_variant($product_variant_seq);
//$this->data[DATA_CREDIT_INFO] = $bank_credits;
//$this->data[DATA_CREDIT_MID] = $this->M_dropdown_list->get_dropdown_bank_credit_month_by_variant($product_variant_seq, $bank_credits[0]->seq);
                    }
                }
                $params->product_variant_seq = $product_variant_seq;
                $this->data['credit'] = $this->M_dropdown_list->get_dropdown_credit($params);
                if (parent::is_agent()) {
                    $agent_seq = parent::get_agent_user_seq();
                    if ($_SESSION[SESSION_PAYMENT_INFO]['payment_head'] == PAYMENT_SEQ_ADIRA_HEAD) {
                        if ($total < ADIRA_MINIMUM_CREDIT_PAYMENT) {
                            $this->session->unset_userdata(SESSION_PAYMENT_INFO);
                            redirect(base_url() . 'member/checkout?step=2');
                        }
                        $this->data['info_product_loan'] = $this->M_checkout_member->get_info_loan($product_variant_seq, $total, 0, $agent_seq);
                    }
                }
            }
            $this->data[DATA_SELECTED][LIST_DATA][0] = $personal_info;
            $this->data[PAYMENT_INFO] = $payment_info;
            $this->data[DATA_PROD] = $product_info;
            $this->data[VOUCHER_NAME] = $this->M_checkout_member->get_promo_voucher_by_member($params);
            $this->data['title'] = 'Ringkasan Belanja';
            $this->data[PRODUCT_SEQ_STRING] = $product_variant_seq;
            $this->data[COUPON] = true; //$this->check_coupon_period($product_variant_seq);
            $this->data[CUSTOMER_INFO] = $this->M_dropdown_list->get_dropdown_customer_search("", parent::get_agent_user_seq(), 0);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function check_coupon_period($product_variant_seq) {
        $params = new stdClass();
        $params->user_id = parent::get_member_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->product_variant_seq = $product_variant_seq;

        try {
            $coupon = $this->M_checkout_member->get_coupon_period($params);
            return $coupon[0]->valid;
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function get_rate_expedition($exp_info) {

        $rate = 0;
        $params = new stdClass();
        $params->user_id = parent::get_member_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->expedition_seq = $exp_info[0]->exp_seq;
        $params->from_district_code = $exp_info[0]->from_district_code;
        $params->to_district_code = $exp_info[0]->to_district_code;
        $params->exp_service_seq = $exp_info[0]->exp_service_seq;
        try {
            $list_data = $this->M_web_service->get_rate_cache($params);
            if (!isset($list_data)) {
                switch ($exp_info[0]->exp_code) {
                    case JNE_EXPEDITION_CODE : $rate = $this->get_jne_expedition_rate($exp_info);
                        break;
                    case PANDU_EXPEDITION_CODE : $rate = $this->get_pandu_expedition_rate($exp_info);
                        break;
                    case RAJA_KIRIM_EXPEDITION_CODE : $rate = $this->get_raja_kirim_expedition_rate($exp_info);
                        break;
                    case TIKI_EXPEDITION_CODE : $rate = $this->get_tiki_expedition_rate($exp_info);
                        break;
                    case RPX_EXPEDITION_CODE : $rate = $this->get_rpx_expedition_rate($exp_info);
                        break;
                    default: $rate = null;
                }
                if (isset($rate) && $rate != 0) {
                    $params->rate = $rate;
                    $this->M_web_service->save_rate_cache($params);
                }

                if (!isset($rate) || $rate == 0) {
                    $rate = $this->get_all_expedition_rate($exp_info);
                    if (!isset($rate) || $rate == 0) {
                        throw new Exception(ERROR_RATE_NOT_FOUND);
                    }
                };
            } else {
                $rate = $list_data[0]->rate;
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        return $rate;
    }

    protected function get_tiki_expedition_rate($exp_info) {

        $url = URL_TIKI_API . '/services/api.cfc?method=tariff&origin=' . $exp_info->from_district_code . '&destination=' . $exp_info->to_district_code . '&weight=1';
        $this->curl->set_url($url);
        $this->curl->setGet(true);
        $this->curl->setIsUsingUserAgent(true);
        $this->curl->setData();
        $json_encoded = $this->curl->getResponse();
        $data_jsons = json_decode($json_encoded, true);
    }

    protected function get_jne_expedition_rate($exp_info) {

        $web_params = new stdClass();
        $web_params->user_id = parent::get_member_user_id();
        $web_params->ip_address = parent::get_ip_address();
        $web_params->type = GET_RATE_TYPE;
        $web_params->function_name = FUNCTION_GET_JNE_RATE_EXPEDITION;
        $web_params->request_datetime = date('Y-m-d H:i:s');
        $web_service_seq = $this->M_web_service->save_web_service($web_params);
        $params = 'username=' . API_USERNAME_JNE . '&api_key=' . API_KEY_JNE . '&from=' . $exp_info[0]->from_district_code . '&thru=' . $exp_info[0]->to_district_code . '&weight=1';

        $url = URL_JNE_API . '/tracing/' . strtolower(API_USERNAME_JNE) . '/price/';
        $this->curl->set_url($url);
        $this->curl->setIsUsingUserAgent(true);
        $this->curl->setParams($params);
        $this->curl->setData();

        $json_encoded = $this->curl->getResponse();
        $data_jsons = json_decode($json_encoded, true);
        $web_params->web_service_seq = $web_service_seq[0]->new_seq;
        $web_params->request = $this->curl->getRequestHeaders() . $this->curl->getRequest();
        $web_params->response = $this->curl->getResponseHeaders() . $this->curl->getResponse();
        $web_params->response_datetime = date('Y-m-d H:i:s');
        $this->M_web_service->save_update_service($web_params);

        if (!isset($data_jsons)) {
            return null;
//            throw new Exception(ERROR_PLEASE_TRY_AGAIN);
        }

        foreach ($data_jsons as $key => $data) {
            if ($data == false) {
//                throw new Exception(ERROR_PLEASE_TRY_AGAIN);
                return null;
            } else {
                foreach ($data as $rate) {
                    if ($exp_info[0]->service_code == $rate["service_code"] || JNE_CTC_REG_CODE == $rate["service_code"]) {
                        return $rate["price"];
                    }
                }
            }
        }
        return null;
    }

    protected function get_pandu_expedition_rate($exp_info) {
        $params = array(
            'PASS' => generate_pandu_api_key(),
            'ACCOUNT_NO' => PANDU_ACCOUNT_NO, // nomor akun
            'TRANSPORT' => 'AIR', // Pilihan : AIR/LAND/SEA (sesuai yang terdapat dalam tariff)
            'SERVICETYPE' => $service_code, // REG/ONS/LAND/LTL cek tariff yg di berikan marketing
            'ORIG' => $kota_asal,
            'DEST' => $kota_tujuan,
            'WEIGHT' => '1',
        );
    }

    protected function get_all_expedition_rate($exp_info) {

        $expedition = $this->M_dropdown_list->get_dropdown_expedition();
        $params = new stdClass();
        $params->user_id = parent::get_member_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->merchant_seq = $exp_info[0]->merchant_seq;
        $params->district_seq = $exp_info[0]->district_seq;

        foreach ($expedition as $exp) {

            if ($exp->seq != $exp_info[0]->exp_seq) {
                $params->exp_seq = $exp->seq;

                $expedition_info = $this->M_checkout_member->get_expedition_merchant_info($params);

                $params->expedition_seq = $expedition_info[0]->exp_seq;
                $params->from_district_code = $expedition_info[0]->from_district_code;
                $params->to_district_code = $expedition_info[0]->to_district_code;
                $params->exp_service_seq = $expedition_info[0]->exp_service_seq;

                $this->expedition_service_seq = $expedition_info[0]->exp_service_seq;

                $rate = 0;
                try {
                    $list_data = $this->M_web_service->get_rate_cache($params);
                    if (!isset($list_data)) {
                        switch ($expedition_info[0]->exp_code) {
                            case JNE_EXPEDITION_CODE : $rate = $this->get_jne_expedition_rate($expedition_info);
                                break;
                            case PANDU_EXPEDITION_CODE : $rate = $this->get_pandu_expedition_rate($expedition_info);
                                break;
                            case RAJA_KIRIM_EXPEDITION_CODE : $rate = $this->get_raja_kirim_expedition_rate($expedition_info);
                                break;
                            case TIKI_EXPEDITION_CODE : $rate = $this->get_tiki_expedition_rate($expedition_info);
                                break;
                            case RPX_EXPEDITION_CODE : $rate = $this->get_rpx_expedition_rate($expedition_info);
                                break;
                            default: $rate = 0;
                        }

                        if ($rate != 0) {
                            return $rate;
                        }
                    } else {
                        return $list_data[0]->rate;
                    }
                } catch (Exception $ex) {
                    parent::set_error($this->data, $ex);
                }
            }
        }
        return null;
    }

    protected function get_rpx_expedition_rate($exp_info) {

        $web_params = new stdClass();
        $web_params->user_id = parent::get_member_user_id();
        $web_params->ip_address = parent::get_ip_address();
        $web_params->type = GET_RATE_TYPE;
        $web_params->function_name = "getRates";
        $web_params->request_datetime = date('Y-m-d H:i:s');
        $web_service_seq = $this->M_web_service->save_web_service($web_params);
        $client = new nusoap_client(URL_RPX_API, true);
        $result = $client->call('getRates', array('user' => API_USER_RPX, 'password' => API_PASSWORD_RPX, 'origin' => $exp_info[0]->from_district_code, 'destination' => $exp_info[0]->to_district_code, 'service_type' => $exp_info[0]->service_code, ' weight' => '1'));

        if ($client->fault) {
            return null;
        } else {
            $err = $client->getError();
            if ($err) {
                return null;
            } else {
                $rpxres = new SimpleXMLElement($result);
                if (isset($rpxres->data[0])) {
                    if ((int) $rpxres->data[0]->price > 0) {
                        $rates = (int) $rpxres->data[0]->price;
                        $web_params->web_service_seq = $web_service_seq[0]->new_seq;
                        $web_params->request = "";
                        $web_params->response = $result;
                        $web_params->response_datetime = date('Y-m-d H:i:s');
                        $this->M_web_service->save_update_service($web_params);
                        return $rates;
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            }
        }
        return null;
    }

    protected function generate_pandu_api_key() {
        return md5(API_KEY_PANDU . date("Ymdh"));
    }

    protected function get_raja_kirim_expedition_rate($exp_info) {

        if ($exp_info[0]->province_seq == JAKARTA_PROVINCE_SEQ) {
            return null;
        }

        $web_params = new stdClass();
        $web_params->user_id = parent::get_member_user_id();
        $web_params->ip_address = parent::get_ip_address();
        $web_params->type = GET_RATE_TYPE;
        $web_params->function_name = FUNCTION_GET_RAJA_KIRIM_RATE_EXPEDITION;
        $web_params->request_datetime = date('Y-m-d H:i:s');
        $web_service_seq = $this->M_web_service->save_web_service($web_params);
        $params = "method=pda_json_cek_harga&apiKey=" . API_KEY_RAJA_KIRIM . "&apiUser=" . API_USER_RAJA_KIRIM . "  &city_code=" . $exp_info[0]->to_district_code . "&weight=1";

        $url = URL_RAJA_KIRIM_API;
        $this->curl->set_url($url);
        $this->curl->setIsUsingUserAgent(true);
        $this->curl->setParams($params);
        $this->curl->setData();
        $json_encoded = $this->curl->getResponse();
        $data_jsons = json_decode($json_encoded, true);

        $web_params->web_service_seq = $web_service_seq[0]->new_seq;
        $web_params->request = $this->curl->getRequestHeaders() . $this->curl->getRequest();
        $web_params->response = $this->curl->getResponseHeaders() . $this->curl->getResponse();
        $web_params->response_datetime = date('Y-m-d H:i:s');
        $this->M_web_service->save_update_service($web_params);

        if (isset($data_jsons)) {
            foreach ($data_jsons as $key => $data) {
                foreach ($data as $rate) {
                    if (isset($rate["err"])) {
                        return null;
//                    throw new Exception(ERROR_PLEASE_TRY_AGAIN);
                    } else {
                        return $rate["harga"];
                    }
                }
            }
        }
        return null;
    }

    protected function get_expedition_info($merchant_seq, $district_seq) {
        $data = new stdClass();
        $data->user_id = parent::get_member_user_id();
        $data->ip_address = parent::get_ip_address();
        $data->merchant_seq = $merchant_seq;
        $data->district_seq = $district_seq;
        $data->exp_seq = DEFAULT_EXPEDITION_SEQ_RATE;
        try {
            $list_data = $this->M_checkout_member->get_expedition_info($data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        return $list_data;
    }

    protected function get_promo_free_fee($merchant_seq, $city_seq) {

        $data = new stdClass();
        $data->user_id = parent::get_member_user_id();
        $data->ip_address = parent::get_ip_address();
        $data->merchant_seq = $merchant_seq;
        $data->city_seq = $city_seq;

        try {
            $list_data = $this->M_checkout_member->get_promo_free_fee($data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        return $list_data;
    }

    protected function get_menu($datas, $parent, $pg_seq = 0) {
        static $i = 1;
        $tab = str_repeat(" ", $i);
        if (isset($datas[$parent])) {
            $html = "<ul>";
            $i++;
            foreach ($datas[$parent] as $vals) {
                $child = $this->get_menu($datas, $vals->seq, $pg_seq);
                if ($child) {
                    $i++;
                    $html .= "$tab";
                    $html.='<li>' . $vals->name;
                    $html .= $child;
                    $html .= "$tab";
                } else {
                    $html .= "$tab";
                    $html.='<li><input type="radio" name=pg value ="' . $vals->seq . '"' . ($pg_seq == $vals->seq ? "checked" : "") . '/><label class = "control-label">' . $vals->name . ' </label>';
                }

                $html .= '</li>';
            }
            $html .= "$tab</ul>";
            return $html;
        } else {
            return false;
        }
    }

    protected function set_order() {

        $merchant_seq = 0;
        $i = 0;
        $credit_product_variant_seq = '';
        $product_info = $this->session->userdata(SESSION_PRODUCT_INFO);
        $payment_info = $this->session->userdata(SESSION_PAYMENT_INFO);
        $personal_info = $this->session->userdata(SESSION_PERSONAL_INFO);
        $error = parent::check_auth_form($this->data[DATA_AUTH], FORM_AUTH_ADD, MEMBER_LOGIN);


        try {

            if ($this->data[DATA_ERROR][ERROR] == true) {
                throw new Exception($this->data[DATA_ERROR][ERROR_MESSAGE][0]);
            }

            if (sizeof($this->session->userdata(SESSION_PRODUCT_INFO)) < 1) {
                throw new Exception(ERROR_INPUT_PRODUCT_INFO_FIRST);
            }

            $member_message = $this->input->post("member_message");
            foreach ($member_message as $key => $message) {
                foreach ($product_info as $idx => $prod) {
                    if ($prod[$idx]["merchant_seq"] == $key) {
                        $product_info[$idx][$idx]["member_message"] = $message;
                        $credit_product_variant_seq = $prod[$idx]["product_seq"];
                    }
                }
            }

            if ($payment_info["payment_seq"] == PAYMENT_SEQ_CREDIT) {

                if (parent::get_input_post("promo_credit_seq") == 0) {

                    $product_info[$credit_product_variant_seq][$credit_product_variant_seq]["promo_credit_bank_seq"] = 0;
                    $member_info[SESSION_PRODUCT_INFO] = $product_info;
                    $this->session->unset_userdata(SESSION_PRODUCT_INFO);
                    $this->session->set_userdata($member_info);
                    $this->data[DATA_CREDIT_MID] = array();
                    $this->final_step();
                    throw new Exception(ERROR_CREDIT_SEQ);
                }
            }
            $this->session->unset_userdata(SESSION_PRODUCT_INFO);
            $member_info[SESSION_PRODUCT_INFO] = $product_info;
            $this->session->set_userdata($member_info);
            $params_order = new stdclass();
            $data_user_id = "";
            if (parent::is_agent()) {
                $params_order->user_id = parent::get_agent_user_id();
                $params_order->member_seq = "";
                $params_order->agent_seq = parent::get_agent_user_seq();
                $data_user_id = $params_order->user_id;
                $params_order->agent = AGENT;
            } else {
                $params_order->user_id = parent::get_member_user_id();
                $params_order->member_seq = parent::get_member_user_seq();
                $params_order->agent_seq = "";
                $data_user_id = $params_order->user_id;
                $params_order->agent = "";
            }

            $params_order->ip_address = parent::get_ip_address();
            $params_order->order_no = $this->get_order_no();
            $params_order->receiver_name = $personal_info["receiver_name"];
            $params_order->receiver_address = $personal_info["address"];
            $params_order->receiver_district_seq = $personal_info["district_seq"];
            $params_order->receiver_zip_code = $personal_info["postal_code"];
            $params_order->receiver_phone_no = $personal_info["phone_no"];
            $params_order->payment_status = PAYMENT_UNPAID_STATUS_CODE;
            $params_order->payment_method = $payment_info["payment_seq"];
            $params_order->payment_code = $payment_info["payment_code"];
            $params_order->coupon_seq = $payment_info["coupon_seq"];
            $params_order->voucher_seq = $payment_info["voucher_seq"] !== 0 ? $payment_info["voucher_seq"] : parent::get_input_post("voucher_seq");
            $params_order->credit_seq = parent::get_input_post("promo_credit_seq");
            $params_order_merchant = new stdClass();
            $params_order_merchant->user_id = $data_user_id;
            $params_order_merchant->ip_address = parent::get_ip_address();

            $params_order_product = new stdClass();
            $params_order_product->user_id = $data_user_id;
            $params_order_product->ip_address = parent::get_ip_address();

            if (parent::get_input_post("promo_credit_seq") > 0) {
                if (sizeof($this->session->userdata(SESSION_PRODUCT_INFO)) <> 1) {
                    throw new Exception(ERROR_CREDIT_PRODUCT);
                } else {
                    $params_order->credit_product_variant_seq = $credit_product_variant_seq;
                    $this->check_valid_credit($params_order);
                }
            }

            $this->M_checkout_member->trans_begin();
            $params_order->voucher_node = NODE_COUPON_MEMBER;
            if ($payment_info["voucher_seq"] > 0)
                $this->check_valid_voucher($params_order);

            $order_seq = $this->M_checkout_member->save_add_order($params_order);
            $params_order->order_seq = $order_seq;

            if ($payment_info["payment_seq"] === PAYMENT_SEQ_ADIRA) {
                $params_order->customer_seq = $this->data['customer_seq'];
                if ($params_order->customer_seq != '' && $params_order->customer_seq != 0) {
                    $params_order->order_loan_seq = $order_seq[0]->new_seq;
                    $params_order->dp = parent::get_input_post("dp");
                    $params_order->tenor = $this->data[LOAN_PARTNER]->tenor_value;
                    $params_order->admin_fee = $this->data[LOAN_PARTNER]->loan_admins;
                    $params_order->loan_interest = $this->data[LOAN_PARTNER]->loan_interest;
                    $params_order->installment = $this->data[LOAN_PARTNER]->angsurans;
                    $params_order->total_installment = $this->data[LOAN_PARTNER]->total_pay_1s;
                    $this->M_checkout_member->save_add_order_loan($params_order);
                }
            }

            if ($payment_info["coupon_seq"] > 0)
                $this->check_valid_coupon($params_order);

            $merchant_exists;
            foreach ($product_info as $product => $n) {
                $i++;
                if (!isset($merchant_exists[$n[$product]["merchant_seq"]])) {
                    $merchant_exists[$n[$product]["merchant_seq"]] = array("merchant_seq" => $n[$product]["merchant_seq"]);
                    $merchant_seq = $n[$product]["merchant_seq"];
                    $params_order_merchant->order_seq = $order_seq[0]->new_seq;
                    $params_order_merchant->merchant_seq = $merchant_seq;
                    $params_order_merchant->exp_real_service_seq = $n[$product]["exp_real_service_seq"];
                    $params_order_merchant->exp_service_seq = $n[$product]["exp_service_seq"];
                    $params_order_merchant->free_fee_seq = $n[$product]["promo_seq"];
                    $params_order_merchant->order_status = ORDER_PREORDER_STATUS_CODE;
                    $params_order_merchant->member_notes = $n[$product]["member_message"];
                    if (strlen($n[$product]["member_message"]) > MAX_MEMBER_MESSAGE_LENGTH) {
                        throw new Exception("Pesan " . ERROR_VALIDATION_LENGTH_MAX . MAX_MEMBER_MESSAGE_LENGTH);
                    }

                    $params_order_merchant->ref_awb_no = $params_order->order_no . ($i > 9 ? $i : "0" . $i);
                    $this->M_checkout_member->save_add_order_merchant($params_order_merchant);
                }


                if (parent::is_agent()):
                    $search_product = new stdClass;
                    $search_product->user_id = $params_order->user_id;
                    $search_product->ip_address = parent::get_ip_address();
                    $search_product->product_seq = $n[$product]["product_seq"];
                    $search_product->exp_seq = $params_order_merchant->exp_service_seq;
                    $search_product->agent = AGENT;
                    $search_product->agent_seq = $params_order->agent_seq;
                    $detail_commission = $this->M_product->get_product_info($search_product);
                endif;

                (array) $commission_partner = [];
                (array) $commission_agent = [];

                /* ---------------------------------------------------------
                 * CALCULATE PARTNER COMMISSION AND AGENT COMMISSION
                  -------------------------------------------------------- */
                if (parent::is_agent()) {
                    if (isset($detail_commission[0]->ppc_commission_fee_partner_percent) && $detail_commission[0]->ppc_commission_fee_partner_percent != '0.00') {
                        $commission_partner['percent'] = $detail_commission[0]->sell_price / $detail_commission[0]->ppc_nominal_commission_partner;
                        $commission_partner['nominal'] = $detail_commission[0]->ppc_nominal_commission_partner;
                        $commission_agent['percent'] = $detail_commission[0]->ppc_nominal_commission_partner / $detail_commission[0]->ppc_nominal_commission_agent;
                        $commission_agent['nominal'] = $detail_commission[0]->ppc_nominal_commission_agent;
                    } elseif (isset($detail_commission[0]->ppc_nominal_commission_partner) && $detail_commission[0]->ppc_nominal_commission_partner != '') {
                        $commission_partner['percent'] = $detail_commission[0]->sell_price / $detail_commission[0]->ppc_nominal_commission_partner;
                        $commission_partner['nominal'] = $detail_commission[0]->ppc_nominal_commission_partner;
                        $commission_agent['percent'] = $detail_commission[0]->ppc_nominal_commission_partner / $detail_commission[0]->ppc_nominal_commission_agent;
                        $commission_agent['nominal'] = $detail_commission[0]->ppc_nominal_commission_agent;
                    } elseif (isset($detail_commission[0]->group_commission_fee_percent) && $detail_commission[0]->group_commission_fee_percent != '') {
                        $partnerMinus = 0;
                        $partnerMinus = ($detail_commission[0]->sell_price * ( $detail_commission[0]->group_commission_fee_percent / 100 )) * (($detail_commission[0]->commission_pro_rate_agent / 100));
                        $commission_partner['percent'] = $detail_commission[0]->group_commission_fee_percent;
                        $commission_partner['nominal'] = ($detail_commission[0]->sell_price * ( $detail_commission[0]->group_commission_fee_percent / 100 )) - $partnerMinus;
                        $commission_agent['percent'] = $detail_commission[0]->commission_pro_rate_agent;
                        $commission_agent['nominal'] = ($commission_partner['nominal'] + $partnerMinus) * ($detail_commission[0]->commission_pro_rate_agent / 100);
                    } else {
                        $partnerMinus = 0;
                        $commission_partner['percent'] = $detail_commission[0]->commission_pro_rate_partner;
                        $commission_partner['nominal'] = ($detail_commission[0]->sell_price * $detail_commission[0]->commission_pro_rate_partner * ( 100 - $detail_commission[0]->commission_pro_rate_agent) / 10000);
                        $commission_agent['percent'] = (($detail_commission[0]->commission_pro_rate_partner) * ($detail_commission[0]->commission_pro_rate_partner) / 100);
                        $commission_agent['nominal'] = (($detail_commission[0]->sell_price * $detail_commission[0]->commission_pro_rate_partner * $detail_commission[0]->commission_pro_rate_agent) / 10000);
                    }
                } else {
                    $commission_partner['percent'] = 0;
                    $commission_partner['nominal'] = 0;
                    $commission_agent['percent'] = 0;
                    $commission_agent['nominal'] = 0;
                }

                $params_order_product->commission_partner_percent = $commission_partner['percent'];
                $params_order_product->commission_partner_nominal = $commission_partner['nominal'];
                $params_order_product->commission_agent_percent = $commission_agent['percent'];
                $params_order_product->commission_agent_nominal = $commission_agent['nominal'];
                $params_order_product->order_seq = $order_seq[0]->new_seq;
                $params_order->order_seq = $order_seq[0]->new_seq;
                $params_order_product->product_variant_seq = $n[$product]["product_seq"];
                $params_order_product->order_no = $params_order->order_no;
                $params_order_product->merchant_seq = $n[$product]["merchant_seq"];
                $params_order_product->variant_value_seq = DEFAULT_VALUE_VARIANT_SEQ;
                $params_order_product->qty = $n[$product]["qty"];
                $params_order_product->sell_price = $n[$product]["sell_price"];
                $params_order_product->weight_kg = $n[$product]["product_weight"];
                $params_order_product->ship_price_real = $n[$product]["exp_real_fee"];
                $params_order_product->ship_price_charged = ($n[$product]["promo_seq"] = 0 ? 0 : $n[$product]["exp_fee"]);
                $params_order_product->product_status = PRODUCT_READY_STATUS_CODE;
                $params_order_product->agent_seq = $params_order->agent_seq;
                $this->M_checkout_member->save_add_order_product($params_order_product);
                $this->save_substract_stock($params_order_product, $this->data);
            }
            $total_order = $this->M_checkout_member->update_data_header_order($order_seq[0]->new_seq);


            $params_order->total_payment = $total_order[0]->total_payment;
            $this->check_payment_gateway_method($params_order);
            $this->save_personal_info();
            $this->sendemail($params_order->order_no);
            $this->M_checkout_member->trans_commit();

            $this->session->unset_userdata(SESSION_PRODUCT_INFO);
            $this->session->unset_userdata(SESSION_PAYMENT_INFO);
            $this->session->unset_userdata(SESSION_PERSONAL_INFO);
            $this->session->unset_userdata(SESSION_CUSTOMER_DATA);
            $this->session->unset_userdata(SESSION_DATA_SIMULATION);

            redirect(base_url("member/payment/" . $params_order->order_no));
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_checkout_member->trans_rollback();
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_checkout_member->trans_rollback();
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_checkout_member->trans_rollback();
        }
    }

    protected function get_order_no() {
        return parent::generate_random_alnum(3, true) . date("Y") . parent::generate_random_text(4, true) . date("m") . parent::generate_random_text(3, true) . date("d");
    }

    protected function save_personal_info() {

        $personal_info = (object) $this->session->userdata(SESSION_PERSONAL_INFO);

        if ($personal_info->chk_alias == "on") {
            $params = new stdClass();
            $params->ip_address = parent::get_ip_address();
            $params->receiver_name = $personal_info->receiver_name;
            $params->address = $personal_info->address;
            $params->district_seq = $personal_info->district_seq;
            $params->phone_no = $personal_info->phone_no;
            $params->postal_code = $personal_info->postal_code;
            $params->alias = $personal_info->alias;
            if (parent::is_agent()) {
                $params->user_id = parent::get_agent_user_id();
                $params->member_seq = parent::get_agent_user_seq();
                $params->tipe = AGENT;
            } else {
                $params->user_id = parent::get_member_user_id();
                $params->member_seq = parent::get_member_user_seq();
                $params->tipe = "";
            }
            $this->M_checkout_member->save_personal_info($params, $params->tipe);
        }
    }

    protected function save_substract_stock($data_stock) {

        $params = new stdClass();
        $params->user_id = $data_stock->user_id;
        $params->ip_address = $data_stock->ip_address;
        $params->product_variant_seq = $data_stock->product_variant_seq;
        $params->variant_value_seq = $data_stock->variant_value_seq;
        $params->qty = $data_stock->qty;
        $params->mutation_type = OUT_MUTATION_TYPE;
        $params->trx_type = STOCK_ORDER_MEMBER_TYPE;
        $params->trx_no = $data_stock->order_no;

        $stock = $this->M_product_stock_merchant->get_latest_product_stock($params);
        if ($stock[0]->stock < $params->qty) {
            throw new Exception(ERROR_PRODUCT_STOCK_QTY_EXCEEDED);
        }

        $this->M_product_stock_merchant->save_subtract_stock($params);
        $this->M_trans_log_merchant->save_merchant_stock_log($params);

        $stock = $this->M_product_stock_merchant->get_latest_product_stock($params);

        if ($stock[0]->stock == 3) {

            $email_params = new stdClass();
            $email_params->user_id = parent::get_member_user_id();
            $email_params->ip_address = parent::get_ip_address();
            $email_params->RECIPIENT_NAME = $stock[0]->merchant_name;
            $email_params->PRODUCT_NAME = $stock[0]->product_name;
            $email_params->code = MERCHANT_STOCK_EXCEEDED;
            $email_params->PRODUCT_VALUE = $stock[0]->value;
            $email_params->to_email = $stock[0]->merchant_email;
            $email_params->LINK_MERCHANT = base_url() . "merchant/master/product_merchant";

            parent::email_template($email_params);
        }
    }

    protected function check_payment_gateway_method($data) {

        switch ($data->payment_code) {
            case PAYMENT_TYPE_BANK: {
//do nothing
                };
                break;
            case PAYMENT_TYPE_BCA_KLIKPAY: {
//do nothing
                };
                break;
            case PAYMENT_TYPE_CREDIT: {
//do nothing
                }
            case PAYMENT_TYPE_CREDIT_CARD: {
//do nothing
                };
                break;
            case PAYMENT_TYPE_DEPOSIT: {
                    if (parent::is_agent()) {
                        $member = $this->M_profile_member->get_data_agent($data);
                    } else {
                        $member = $this->M_profile_member->get_data_member($data);
                    }
                    if ($member[2][0]->deposit_amt < $data->total_payment) {
                        throw new Exception(ERROR_DEPOSIT_EXCEDEED);
                    };
                };
                break;
            case PAYMENT_TYPE_MANDIRI_KLIKPAY: {
//do nothing
                };
                break;
        }
    }

    protected function check_valid_voucher($data) {
        $voucher = $this->M_checkout_member->get_valid_voucher($data);
        if (!isset($voucher)) {
            throw new Exception(ERROR_VOUCHER_CANT_USE);
        }
    }

    protected function check_valid_coupon($data) {
        $voucher = $this->M_checkout_member->get_valid_coupon($data);
        if ($voucher[0][0]->valid == "1") {
            throw new Exception(ERROR_COUPON_CANT_USE);
        } elseif ($voucher[1][0]->coupon_count == "0") {
            throw new Exception(ERROR_COUPON_REACH_LIMIT);
        }
    }

    protected function check_valid_credit($data) {
        $credit = $this->M_checkout_member->get_valid_credit($data);
        if (!isset($credit)) {
            throw new Exception(ERROR_CREDIT_SEQ);
        }
    }

    protected function sendemail($order_no) {
        $selected = new stdClass();

        $selected->ip_address = parent::get_ip_address();
        $selected->key = $order_no;
        $tipe = "";
        if (parent::is_agent()) {
            $selected->user_id = parent::get_agent_user_id();
            $tipe = AGENT;
        } else {
            $selected->user_id = parent::get_member_user_id();
            $tipe = "";
        }
        $order_info = $this->M_order_member->get_order_info_by_order_no($selected, $tipe);
        $order_content = "<table border='1'>
                           <thead>
                            <tr>
                                <th>Produk</th>
                                <th>Qty</th>
                                <th>Harga (Rp)</th>
                                <th>Total (Rp)</th>
                            </tr>
                           </thead><tbody>";

        foreach ($order_info[2] as $product) {
            $order_content = $order_content . "<tr>
                                               <td>" . $product->display_name . "</td>
                                               <td>" . $product->qty . "</td>
                                               <td >" . number_format($product->sell_price) . "</td>
                                               <td > " . number_format($product->sell_price * $product->qty) . "</td></tr>";
        }
        $rowcredit = '';
        if ($order_info[0][0]->promo_credit_seq > 0) {
            $selected->promo_credit_seq = $order_info[0][0]->promo_credit_seq;
            $data_credit = $this->M_order_member->get_credit_info_by_credit_seq($selected);
            if (isset($data_credit)) {
                $rowcredit = "<tr>
                                <td colspan=3>Kredit Bank</td>
                                <td>" . $data_credit[0]->bank_name . "</td>
                            </tr>
			    <tr>
                                <td colspan=3>Periode cicilan</td>
                                <td>" . $data_credit[0]->credit_month . " bulan</td>
                            </tr>
			    <tr>
                                <td colspan=3>Pembayaran bulanan</td>
                                <td>" . number_format($order_info[0][0]->total_payment / $data_credit[0]->credit_month) . "</td>
                            </tr>";
            }
        }

        $order_content = $order_content . "</tbody>
                                          <tr>
                                            <td colspan=3>Total Biaya Pengiriman</td>
                                            <td>" . number_format($order_info[1][0]->total_ship) . "</td>
                                          </tr>
                                          <tr>
                                            <td colspan=3>Voucher / Kupon</td>
                                            <td>" . number_format($order_info[0][0]->nominal) . "</td>
                                          </tr>
                                          <tr>
                                            <td colspan=3>Total Bayar</td>
                                            <td>" . number_format($order_info[0][0]->total_payment) . "</td>
                                          </tr>" . $rowcredit . "
                                          </table>";

        $address_content = "Penerima : " . $order_info[0][0]->receiver_name . "<br>
                            Alamat : " . $order_info[0][0]->receiver_address . "-" . $order_info[0][0]->province_name . "-" . $order_info[0][0]->city_name . "-" . $order_info[0][0]->district_name . "<br>
                            No Tlp : " . $order_info[0][0]->receiver_phone_no . "<br>";

        $info_bank = "<strong>3. Info Bank</strong><br/><table border='1'><thead>
                    <tr>
                        <th>Bank</th>
                        <th>Nomor Rekening</th>
                        <th>A/n</th>
                    </tr></thead><tbody>";
        $bank_list = $this->M_radio_button->get_bank_list();
        foreach ($bank_list as $bank) {
            $info_bank = $info_bank . "<tr>
                                         <td>" . $bank->bank_name . "</td>
                                         <td>" . $bank->bank_acct_no . "</td>
                                         <td>" . $bank->bank_acct_name . "</td>
                                       </tr>";
        }
        $info_bank = $info_bank . "</tbody></table><br>";

        $params = new stdClass();
        if (parent::is_agent()) {
            $params->user_id = parent::get_agent_user_id();
        } else {
            $params->user_id = parent::get_member_user_id();
        }
        $params->ip_address = parent::get_ip_address();
        $params->code = ORDER_INFO;
        $params->to_email = $order_info[0][0]->email;
        $params->ORDER_NO = $order_info[0][0]->order_no;
        $params->RECIPIENT_NAME = $order_info[0][0]->member_name;
        $params->PAYMENT_LINK = base_url() . "member/payment/" . $order_no;
        $params->RECIPIENT_ADDRESS = $address_content;
        $params->ORDER_DATE = parent::cdate($order_info[0][0]->order_date);
        $params->ORDER_ITEMS = $order_content;
        $params->INFO_BANK = $order_info[0][0]->payment_code == PAYMENT_TYPE_BANK ? $info_bank : "";
        $params->CONFIRM_LINK = base_url() . "member/payment/" . $order_info[0][0]->order_no;
        parent::email_template($params);

        if ($order_info[0][0]->pg_method_seq == PAYMENT_SEQ_ADIRA || $order_info[0][0]->pg_method_seq == PAYMENT_SEQ_ADIRA_DEPOSIT) {
            foreach ($order_info[1] as $value) {
                $params_email_merchant = new stdClass();
                $params_email_merchant->user_id = $params->user_id;
                $params_email_merchant->ip_address = parent::get_ip_address();
                $params_email_merchant->code = ORDER_NEW_PARTNER_TO_MERCHANT;
                $params_email_merchant->to_email = $value->email;
                $params->order_seq = $order_info[0][0]->seq;
                $params->merchant_info_seq = $value->merchant_info_seq;
                $merchant_product = $this->M_order_merchant_admin->get_produk($params);

                $product_order_content = "";
                $product_order_content = "<table border='1'>
                                                            <thead>
                                                             <tr>
                                                                 <th>Produk</th>
                                                                 <th>Tipe Product</th>
                                                                 <th>Qty</th>
                                                             </tr>
                                                            </thead><tbody>";

                foreach ($merchant_product as $product_merchant) {
                    $product_order_content = $product_order_content . "<tr>
                                                                    <td>" . $product_merchant->product_name . "</td>
                                                                    <td>" . $product_merchant->variant_name . "</td>
                                                                    <td>" . $product_merchant->qty . "</td></tr>";
                }
                $product_order_content = $product_order_content . "</tbody> </table>";

                $params_email_merchant->ORDER_ITEMS = $product_order_content;
                $params_email_merchant->RECIPIENT_NAME = $value->merchant_name;
                $params_email_merchant->ORDER_NO = $order_info[0][0]->order_no;
                $params_email_merchant->ORDER_DATE = parent::cdate($order_info[0][0]->order_date);
                $params_email_merchant->RECIPIENT_ADDRESS = $address_content;
                $params_email_merchant->email_code = parent::generate_random_alnum(20, true);
                $params_email_merchant->CONFIRM_LINK = base_url() . "merchant/transaction/merchant_delivery";

                $params_email_merchant->to_email = $value->email;
                parent::email_template($params_email_merchant);
            }

            $selected = new stdClass();
            $selected->ip_address = parent::get_ip_address();
            $selected->key = $order_no;
            $selected->user_id = parent::get_agent_user_id();
            $order_info = $this->M_order_member->get_order_info_by_order_no($selected, AGENT);
            foreach ($order_info[1] as $key => $each_product_info) {
                $params = new stdClass();
                $params->user_id = $selected->user_id;
                $params->ip_address = parent::get_ip_address();
                $params->order_seq = $order_info[0][0]->seq;
                $params->customer_seq = $order_info[0][0]->customer_seq;
                $customer_info = parent::get_customer_info($params);
            }

            $order_content = "<table border='1'>
                           <thead>
                            <tr>
                                <th>Produk</th>
                                <th>Qty</th>
                                <th>Harga (Rp)</th>
                                <th>Total (Rp)</th>
                            </tr>
                           </thead><tbody>";

            foreach ($order_info[2] as $product) {
                $order_content = $order_content . "<tr>
                                               <td>" . $product->display_name . "</td>
                                               <td>" . $product->qty . "</td>
                                               <td >" . number_format($product->sell_price) . "</td>
                                               <td > " . number_format($product->sell_price * $product->qty) . "</td></tr>";
            }
            $rowcredit = '';
            if ($order_info[0][0]->promo_credit_seq > 0) {
                $selected->promo_credit_seq = $order_info[0][0]->promo_credit_seq;
                $data_credit = $this->M_order_member->get_credit_info_by_credit_seq($selected);
                if (isset($data_credit)) {
                    $rowcredit = "<tr>
                                <td colspan=3>Kredit Bank</td>
                                <td>" . $data_credit[0]->bank_name . "</td>
                            </tr>
			    <tr>
                                <td colspan=3>Periode cicilan</td>
                                <td>" . $data_credit[0]->credit_month . " bulan</td>
                            </tr>
			    <tr>
                                <td colspan=3>Pembayaran bulanan</td>
                                <td>" . number_format($order_info[0][0]->total_payment / $data_credit[0]->credit_month) . "</td>
                            </tr>";
                }
            }

            $order_content = $order_content . "</tbody>
                                          <tr>
                                            <td colspan=3>Total Biaya Pengiriman</td>
                                            <td>" . number_format($order_info[1][0]->total_ship) . "</td>
                                          </tr>
                                          <tr>
                                            <td colspan=3>Voucher / Kupon</td>
                                            <td>" . number_format($order_info[0][0]->nominal) . "</td>
                                          </tr>
                                          <tr>
                                            <td colspan=3>Total Bayar</td>
                                            <td>" . number_format($order_info[0][0]->total_payment) . "</td>
                                          </tr>" . $rowcredit . "
                                          </table>";

            $address_content = "Penerima : " . $order_info[0][0]->receiver_name . "<br>
                            Alamat : " . $customer_info[0]->sub_district . "-" . $order_info[0][0]->receiver_address . "-" . $order_info[0][0]->province_name . "-" . $order_info[0][0]->city_name . "-" . $order_info[0][0]->district_name . "-" . $customer_info[0]->sub_district . "<br>
                            No Tlp : " . $order_info[0][0]->receiver_phone_no . "<br>";


            $info_bank = "<strong>3. Info Bank</strong><br/><table border='1'><thead>
                    <tr>
                        <th>Bank</th>
                        <th>Nomor Rekening</th>
                        <th>A/n</th>
                    </tr></thead><tbody>";
            $bank_list = $this->M_radio_button->get_bank_list();
            foreach ($bank_list as $bank) {
                $info_bank = $info_bank . "<tr>
                                         <td>" . $bank->bank_name . "</td>
                                         <td>" . $bank->bank_acct_no . "</td>
                                         <td>" . $bank->bank_acct_name . "</td>
                                       </tr>";
            }
            $info_bank = $info_bank . "</tbody></table><br>";

            $selected = new stdClass();
            $selected->user_id = parent::get_agent_user_id();
            $selected->ip_address = parent::get_ip_address();
            $selected->seq = $this->data[DATA_USER_PROFILE][PARTNER_SEQ];

            $data_partner = $this->M_partner_live->get_data($selected);

            $params_partner_email = new stdClass();
            $params_partner_email->user_id = parent::get_agent_user_id();
            $params_partner_email->ip_address = parent::get_ip_address();
            $params_partner_email->code = ORDER_NEW_PARTNER_TO_PARTNER;
            $params_partner_email->to_email = $data_partner[0]->email;
            $params_partner_email->ORDER_NO = $order_info[0][0]->order_no;
            $params_partner_email->RECIPIENT_NAME = $data_partner[0]->name;
            $params_partner_email->PAYMENT_LINK = base_url() . "member/payment/" . $order_no;
            $params_partner_email->RECIPIENT_ADDRESS = $address_content;
            $params_partner_email->ORDER_DATE = parent::cdate($order_info[0][0]->order_date);
            $params_partner_email->ORDER_ITEMS = $order_content;
            $params_partner_email->INFO_BANK = $order_info[0][0]->payment_code == PAYMENT_TYPE_BANK ? $info_bank : "";
            $params_partner_email->CONFIRM_LINK = base_url() . "partner/agent_order/";
            $params_partner_email->EXPIRED_DATE = date("d-M-Y", strtotime("+4 day"));
            parent::email_template($params_partner_email);
        }
    }

    private function sendnotif($params_order, $product_info, $payment_info) {
        $qty = 0;
        foreach ($product_info as $product => $n) {
            $qty += $n[$product][QTY];
        }

        $notif = new stdClass();
        $notif->user_id = (parent::is_agent() ? parent::get_agent_user_id() : parent::get_member_user_id());
        if ($payment_info['payment_code'] == PAYMENT_TYPE_BANK OR $payment_info['payment_code'] == PAYMENT_TYPE_DOCU_ATM) {
            $notif->member_code = NEW_ORDER_TRANSFER_BANK_MEMBER;
            $notif->agent_code = NEW_ORDER_TRANSFER_BANK_MEMBER;
            $notif->expire_order_date = date("d-M-Y H:i:s", strtotime("+1 day"));
        } else {
            $notif->member_code = NEW_ORDER_MEMBER;
            $notif->agent_code = NEW_ORDER_AGENT;
        }
        $notif->member_seq = $params_order->member_seq;
        $notif->agent_seq = $params_order->agent_seq;
        $notif->order_no = $params_order->order_no;
        $notif->total_product = $qty;
        parent::sendnotif_member_or_agent($notif);
    }

    protected function payment_method() {
        $pg_seq = parent::get_input_post("pg_seq");
        $pg_method_list = $this->M_tree_view->get_payment_gateway_method_list($pg_seq);
        echo json_encode($pg_method_list);
        die();
    }

    protected function check_data_loan() {
        $tenor = parent::get_input_post('tenor');
        $dp = parent::get_input_post('dp');
        $result = $this->get_data_simulation($dp, $tenor, "", $total);
        $simulation[SESSION_DATA_SIMULATION] = $result;
        $this->session->set_userdata($simulation);
        $this->data[LOAN_PARTNER] = $result;
        try {
            if ($this->data[DATA_ERROR][ERROR] == true) {
                throw new Exception($this->data[DATA_ERROR][ERROR_MESSAGE][0]);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_customer_data($have_customer_seq = false) {
        $customer_info = new stdClass();
        $customer_info->province_seq = parent::get_input_post("province_seq");
        $customer_info->city_seq = parent::get_input_post("city_seq");
        $customer_info->district_seq = parent::get_input_post("district_seq");

        $params = new stdClass();
        $params->user_id = parent::get_agent_user_seq();
        $params->ip_address = parent::get_ip_address();
        $params->customer_seq = parent::get_input_post("customer_seq");
        $params->identity_no = parent::get_input_post("identity_no", TRUE, FILL_VALIDATOR, 'No identitas harus diisi', $this->data);
        $params->pic_name = $_SESSION[SESSION_PERSONAL_INFO]['receiver_name'];
        $params->birthday = parent::get_input_post("birthday", TRUE, DATE_VALIDATOR, 'Tanggal lahir', $this->data);
        $params->phone_no = $_SESSION[SESSION_PERSONAL_INFO]["phone_no"];
        $params->identity_address = parent::get_input_post("identity_address", TRUE, FILL_VALIDATOR, 'Alamat Sesuai KTP', $this->data);
        $params->address = $_SESSION[SESSION_PERSONAL_INFO]["address"];
        $params->sub_district = parent::get_input_post("sub_district", TRUE, FILL_VALIDATOR, 'Kelurahan', $this->data);
        $params->zip_code = isset($_SESSION[SESSION_PERSONAL_INFO]["postal_code"]) && strlen($_SESSION[SESSION_PERSONAL_INFO]["postal_code"]) > 4 ?
                $_SESSION[SESSION_PERSONAL_INFO]["postal_code"] : parent::get_input_post("zip_code", TRUE, ZIP_CODE_VALIDATOR, 'zip code', $this->data);
        $params->province_name = $_SESSION[SESSION_PERSONAL_INFO]["province_name"];
        $params->city_name = $_SESSION[SESSION_PERSONAL_INFO]["city_name"];
        $params->district_name = $_SESSION[SESSION_PERSONAL_INFO]["district_name"];
        $params->email_address = parent::get_input_post("pic_email", TRUE, EMAIL_VALIDATOR, '', $this->data);
        $params->province_seq = $_SESSION[SESSION_PERSONAL_INFO]["province_seq"];
        $params->city_seq = $_SESSION[SESSION_PERSONAL_INFO]["city_seq"];
        $params->district_seq = $_SESSION[SESSION_PERSONAL_INFO]["district_seq"];
        $data_customer[SESSION_CUSTOMER_DATA] = $params;
        $this->session->set_userdata($data_customer);
        if ($params->customer_seq == '' || $have_customer_seq = false) {
            if ($this->data[DATA_ERROR][ERROR] == false) {
                $seq = $this->M_checkout_member->save_new_customer($params);
                $this->data['customer_seq'] = $seq[0]->new_seq;
            }
        }
    }

    protected function adira_payment_check() {
        $adira = false;
        $product_count = count($_SESSION[SESSION_PRODUCT_INFO]);
        (int) $product_price = 0;
        if ($product_count == 1) {
            $product_list = reset($_SESSION[SESSION_PRODUCT_INFO]);
            $key = key($product_list);
            $product_price = $product_list[$key][PRODUCT_PRICE];
            if ($product_price > ADIRA_MINIMUM_CREDIT_PAYMENT) {
                $adira = true;
            }
        } elseif ($product_count < 1) {
            $this->data[DATA_ERROR][ERROR] = true;
            $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_EMPTY_PRODUCT_ON_CART;
        }


        if ($this->data[DATA_ERROR] == false) {
            foreach ($_SESSION[SESSION_PRODUCT_INFO] as $key => $value) {
                $product_variant_seq = $key;
            }
            if (isset($product_variant_seq)) {
                $final_check_adira_payment = $this->M_checkout_member->get_info_loan($product_variant_seq, $product_price, 0, parent::get_agent_user_seq());

                if ($final_check_adira_payment && $adira) {
                    $this->data[ADIRA_PAYMENT_STATUS] = $adira;
                } else {
                    $this->data[ADIRA_PAYMENT_STATUS] = FALSE;
                }
                return $this->data[ADIRA_PAYMENT_STATUS];
            }
        }
    }

    private function check_final_payment() {
        $payment_info = [
            PAYMENT_HEAD => $_SESSION[SESSION_PAYMENT_INFO][PAYMENT_HEAD],
        ];
        $product_info = [
            'count' => count($_SESSION[SESSION_PRODUCT_INFO])
        ];
        if ($payment_info[PAYMENT_HEAD] == PAYMENT_SEQ_ADIRA_HEAD) {
            if ($product_info['count'] == 1) {
                $product_list = reset($_SESSION[SESSION_PRODUCT_INFO]);
                $key = key($product_list);
                $product_qty = $product_list[$key][QTY];
                if ($product_qty > 1) {
                    redirect(base_url() . 'member/checkout?step=2');
                } else {
                    return;
                }
            }
            redirect(base_url() . 'member/checkout?step=2');
        }
    }

    protected function get_single_price_product() {
        $product_list = reset($_SESSION[SESSION_PRODUCT_INFO]);
        $key = key($product_list);
        $product_main = $product_list[$key];
        return $product_main[SELL_PRICE];
    }

    protected function get_product_qty($product) {
        $qty_product = 0;
        if ($product > 0) {
            foreach ($product as $key => $product) {
                foreach ($product as $a => $b) {
                    $qty_product = $qty_product + $b[QTY];
                }
            }
        }
        return $qty_product;
    }

}
?>

