<?php

require_once CONTROLLER_BASE_MEMBER;

class C_payment_inquiry_member extends controller_base_member {

    private $data;
    private $order_no;
    private $get_user_id;
    private $tipe;

    public function __construct() {
        $this->data = parent::__construct("", "", false);
        $this->initialize();
    }

    private function initialize() {
        $this->order_no = $this->uri->segment(4);

        $this->load->model('member/M_order_member');
        $this->load->model('member/M_payment_member');
        $this->load->model('member/M_deposit_member');
        $this->load->model('admin/transaction/M_order_merchant_admin');
        $this->load->library('curl');
        $this->load->library('curl_dimo');
        $this->load->library('keygenerator');

        if (parent::is_agent()) {
            $this->get_user_id = parent::get_agent_user_id();
            $this->tipe = AGENT;
        } else {
            $this->get_user_id = parent::get_member_user_id();
            $this->tipe = '';
        }
    }

    public function index() {
        $remote_addr = $_SERVER['REMOTE_ADDR'];
        $payment_type = $this->callback_ip_address($remote_addr);
        $request = '';

        // for BCA KLIKPAY
        if (parent::get_input_post("transactionNo")) {
            $this->order_no = parent::get_input_post("transactionNo");
            foreach ($_POST as $key => $value)
                $request .= "[" . $key . '] => [' . $value . ']
                                    ';
        }

        $params = new stdClass();
        $params->user_id = $this->get_user_id;
        $params->ip_address = parent::get_ip_address();
        $params->key = $this->order_no;
        $order_info = $this->M_order_member->get_order_info_by_order_no($params, $this->tipe);

        try {
            $this->data[DATA_SELECTED][LIST_DATA] = $order_info;
            if ($payment_type == PAYMENT_TYPE_BCA_KLIKPAY) {
                if (!isset($order_info[0][0])) {
                    exit('not allow to test');
                }
                $this->get_payment_type_order($order_info[0][0], $order_info);

                $params_payment = new stdClass();
                $params_payment->user_id = parent::get_member_user_id();
                $params_payment->ip_address = parent::get_ip_address();
                $params_payment->order_seq = $order_info[0][0]->seq;
                $params_payment->order_no = $order_info[0][0]->order_no;
                $params_payment->payment_method_seq = $order_info[0][0]->pg_method_seq;
                $params_payment->signature = $order_info[0][0]->signature;
                $params_payment->request = $request;
                $params_payment->response = $this->data[DATA_PAYMENT];
                $params_payment->request_date = date('Y-m-d H:i:s');
                $params_payment->response_date = date('Y-m-d H:i:s');
                $this->M_payment_member->save_payment_log($params_payment);
                exit();
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        redirect(base_url("member/payment") . "/" . $this->order_no);
    }

    private function callback_ip_address($remote_addr) {
        if (in_array($remote_addr, json_decode(GSP_BCA_IP_ADDRESS))) {
            return PAYMENT_TYPE_BCA_KLIKPAY;
        } else {
            return "";
        }
    }

    private function save_payment_log($request, $response) {
        $params = new stdClass();
        $params->user_id = "SYSTEM";
        $params->ip_address = parent::get_ip_address();
        $params->order_no = $this->data[DATA_SELECTED][LIST_DATA][0][0]->order_no;
        $params->order_seq = $this->data[DATA_SELECTED][LIST_DATA][1][0]->order_seq;
        $params->payment_method_seq = $this->data[DATA_SELECTED][LIST_DATA][0][0]->pg_method_seq;
        $params->request = $request;
        $params->request_date = date('Y-m-d H:i:s');
        $params->response = $response;
        $params->response_date = date('Y-m-d H:i:s');
        $params->signature = $this->data[DATA_SELECTED][LIST_DATA][0][0]->signature;
        $this->M_payment_member->save_payment_log($params);
    }

    private function get_payment_type_order($data) {
        switch ($data->payment_code) {
            case PAYMENT_TYPE_BCA_KLIKPAY: $this->proccess_payment_bca_klikpay($data);
                break;
        }
    }

    public function proccess_payment_bca_klikpay($data) {
        $this->set_inquiry($data);
    }
    private function set_inquiry($data) {
        $klikPayCode = parent::get_input_post('klikPayCode');
        $transactionNo = parent::get_input_post('transactionNo');
        $additionalData = parent::get_input_post('additionalData');
        $signature = parent::get_input_post('signature');

        //SOME OF YOUR VALIDATION BEFORE PROCEED
        if ($signature != $data->signature) {
            //PUT YOUR STATUS AND COMMENT HERE
            echo '';
        }
        //AFTER VALIDATION IF SUCCESSFULL
        //parameter that will merchant pass
        $currency = 'IDR';
        //$transactionDate = date('m/d/Y H:i:s'); //FOR DATE this is only a dummy merchant must get a date from database
        //$totalAmount = '0.00'; //FOR total amount, must be a total from list item that user bought (exclude miscFee)
        $miscFee = '0.00'; //FOR misc fee is additional <if you have tax, or many price that not included to items price>
//        $transaction = $this->merchant_db->getTransaction($_POST['transactionNo']); //to take payType value from merchant database

        $arrFullTrx = array();
        $arrInstTrx = array();

        $arrFullTrx = array(
            'amount' => $data->total_payment . '.00', //EXCLUDE MISCFEE
            'description' => ''
        );
        $newIntTrx = NULL;
        $_additionalData = 'TESTING INQUIRY FROM MERCHANT';
        $newarrFullTrx = implode("^", $arrFullTrx);
        //create string that will you pass into our gateway
        $resp = $klikPayCode . ';' . $transactionNo . ';' . $currency . ';' . $newarrFullTrx . ';' . $newIntTrx . ';' . $miscFee . ';' . $_additionalData;
        //Please Check that you must echo it without another HTML TAG
        $this->data[DATA_PAYMENT] = $resp;
        echo $resp;
    }

}

?>