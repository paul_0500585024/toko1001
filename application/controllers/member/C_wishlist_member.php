<?php

require_once CONTROLLER_BASE_MEMBER;

class C_wishlist_member Extends controller_base_member {

    public function __construct() {
        $this->data = parent::__construct("CON002M", "member/wishlist");
        $this->load->helper('url');
        $this->load->library('pagination');
        $this->hseq = $this->uri->segment(2);
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('home/M_thumbs_new_products');
    }

    public function index() {
        $base_url = base_url() . 'member/wishlist';
        $perpage = 5 * 6;
        $start = $this->input->get(START_OFFSET);
        if ($start == '') {
            $start = '0';
        }
        if (!ctype_digit($start)) {
            redirect($base_url);
        }
        $params = new stdClass;
        $params->start = $start;
        $params->limit = $perpage;
        $params->where = " where pv.seq in ( select product_variant_seq from m_member_wishlist where member_seq=" . parent::get_member_user_id() . ")";
        $params->order = "";
        $params->agent = parent::is_agent() ? AGENT : '';
        $params->agent_seq = parent::is_agent() ? parent::get_agent_user_id() : "";

        $this->data['tree_category'] = $this->get_tree_category(true);
        if (!$this->session->userdata) {
            
        }
        $query = $this->M_thumbs_new_products->get_thumbs_new_products($params);
        $query_row = $query['row'];
        $query_row_result = $query_row->result();
        $query_row_result_first = $query_row_result[0];
        $total_row = $query_row_result_first->total_row;
        $this->data['row'] = $total_row;
        $this->data['product'] = $query['product'];
        $this->data['title'] = 'Wishlist';
        $this->display_page($base_url, $start, $total_row, $perpage);
        $this->template->view("member/master/wishlist_member", $this->data);
    }

}
