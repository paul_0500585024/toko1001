<?php

require_once CONTROLLER_BASE_MEMBER;

class C_verifikasi_signup_member extends controller_base_member {

    public function __construct() {
	parent::__construct("", "", false);
    }

    public function index() {
	$this->load->model("member/master/M_verifikasi_signup_member");
	$uri = $this->uri->segment(3);

	$params = new stdClass();
	$params->uri = $uri;
	$params->EmailCd = MEMBER_REG_CODE;
	$pass = "ERROR";
	$get_email = $this->M_verifikasi_signup_member->get_email_by_log($params);
	if ($get_email) {
	    $pass = "OK";
	} else {
	    $params->EmailCd = MEMBER_REG_PROMO;
	    $get_email = $this->M_verifikasi_signup_member->get_email_by_log($params);
	    if ($get_email)
		$pass = "OK";
	}
	if ($pass == "OK") {
	    $email = new stdClass();
	    $email->email = $get_email[0]->recipient_email;
	    $get_member = $this->M_verifikasi_signup_member->get_member_by_email($email);

	    $member_seq = $get_member[0]->seq;
	    $member_name = $get_member[0]->name;
	    $member_email = $get_member[0]->email;
	    if ($get_member[0]->status == NEW_STATUS_CODE) {
		$this->M_verifikasi_signup_member->save_update_member($email);
		$params->user_id = '';
		$params->ip_address = parent::get_ip_address();
		$params->node_cd = NODE_REGISTRATION_MEMBER;
		$params->member_seq = $member_seq;

		$voucher = parent::generate_voucher($params);

		$get_status = SUCCESS_STATUS;
		$get_mail = new stdClass();
		$get_mail->user_id = '';
		$get_mail->ip_address = parent::get_ip_address();
		$get_mail->code = MEMBER_REG_CODE_SUCCESS;
		$get_mail->RECIPIENT_NAME = $member_name;
		$get_mail->LINK = base_url() . lOGIN_MEMBER;
		$get_mail->to_email = $member_email;
		$get_mail->VOUCHER = $voucher;
		
		parent::email_template($get_mail);
	    } else {
		$get_status = FAILED_STATUS;
	    }
	} else {
	    redirect(base_url() . "error_404");
	}
	$value[VALUE] = $get_status;
	$this->load->view(LIVEVIEW . "member/master/verifikasi_regsiter_member", $value);
    }

}

?>