
<?php

require_once CONTROLLER_BASE_MEMBER;

class C_verify_password extends controller_base_member {

    public function __construct() {
        $this->data = parent::__construct("", "", false);
    }

    function index() {
        $this->load->model("member/master/M_verify_password_member");
        $this->load->model("admin/transaction/M_order_admin");
        $now = date('Y-m-d');

        $uri = $this->uri->segment(3);
        $params = new stdClass();
        $params->uri = $uri;
        $params->email_cd = MEMBER_FORGOT_PASSWORD;
        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        $security = $this->M_verify_password_member->check_member_log_security($params);
        try {
            if ($security) {
                throw new Exception(ERROR_VALIDATION_FORGOT_PASSWORD);
            } else {
                $data = $this->M_verify_password_member->get_member_log($params);

                if (isset($data)) {
                    $date2 = date_create(date('Ymd'));

                    $params_member = new stdClass();
                    $params_member->user_id = parent::get_member_user_id();
                    $params_member->ip_address = parent::get_ip_address();
                    $params_member->email = $data[0]->recipient_email;
                    $params_member->request_date = $data[0]->created_date;
                    $params_member->name = $data[0]->recipient_name;

                    if (date_diff(date_create($params_member->request_date), $date2)->d < 2) {
                        $new_password = parent::generate_random_alnum(8);
                        $params_email = new stdClass();
                        $params_email->user_id = parent::get_input_post("email");
                        $params_email->ip_address = parent::get_ip_address();
                        $params_email->code = MEMBER_FORGOT_PASSWORD_VERIFICATION;
                        $params_email->to_email = $params_member->email;
                        $params_email->RECIPIENT_NAME = $params_member->name;
                        $params_email->PASSWORD = $new_password;
                        $params_email->EMAIL = $params_member->email;

                        $member_seq = $this->M_order_admin->get_member_seq($params_member);

                        $params_member->new_pass = md5(md5($new_password));
                        $params_member->type = FORGOT_PASSWORD_TYPE;
                        $params_member->code = $uri;
                        $params_member->old_pass = $member_seq[0]->new_password;
                        $params_member->member_seq = $member_seq[0]->seq;

                        $this->M_verify_password_member->trans_begin();
                        $this->M_verify_password_member->update_password($params_member);
                        $this->M_verify_password_member->add_member_log_security($params_member);
                        $this->M_verify_password_member->trans_commit();

                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_SUCCESS][SUCCESS_MESSAGE][] = SUCCESS_VERIFY_FORGOT_PASSWORD;
                        parent::email_template($params_email);
                    } else {
                        throw new Exception(ERROR_FORGOT_PASSWORD_EXPIRED);
                    }
                    $this->template->view("member/master/verify_password_member", $this->data);
                } else {
                    redirect(base_url(). "error_404");
                }
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_verify_password_member->trans_rollback();
            $this->template->view("member/master/verify_password_member", $this->data);
        }
    }

}

?>
