<?php

require_once CONTROLLER_BASE_HOME;

class C_member_address extends controller_base_home {

    private $data;

    public function __construct() {

	$this->data = parent::__construct("MST006", "member/master/member_address");
	$this->initialize();
    }

    public function initialize() {

	$this->load->model('member/master/M_member_address');
	$this->load->model('component/M_dropdown_list');
    }

    public function index() {

    }

    public function get_edit() {

	$selected = new stdClass();
	$selected->member_seq = parent::get_member_user_seq();
	$selected->ip_address = parent::get_ip_address();
	$selected->address_seq = parent::get_input_post('address_seq');

	try {
	    $sel_data = $this->M_member_address->get_data($selected);

	    if (isset($sel_data[0])) {
		parent::set_data($this->data, $sel_data);
	    }
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	}

	if (isset($sel_data)) {
	    $this->data[ADDRESS_SEQ] = $selected->address_seq;
	    $this->data[PROVINCE_NAME] = $this->M_dropdown_list->get_dropdown_province($sel_data[0]->province_seq);
	    $this->data[CITY_NAME] = $this->M_dropdown_list->get_dropdown_city_by_province($sel_data[0]->province_seq);
	    $this->data[DISTRICT_NAME] = $this->M_dropdown_list->get_dropdown_district_by_city($sel_data[0]->city_seq);
	    $this->data[DATA_ADDRESS] = DEFAULT_MODULE_SEQ;
	} else {
	    $this->data[ADDRESS_SEQ] = "";
	    $this->data[PROVINCE_NAME] = $this->M_dropdown_list->get_dropdown_province();
	    $this->data[CITY_NAME] = "";
	    $this->data[DISTRICT_NAME] = "";
	    $this->data[DATA_ADDRESS] = DEFAULT_ROOT;
	}
	$this->load->view(LIVEVIEW . "member/master/member_address", $this->data);
    }

    public function save_add() {

	$params = new stdClass();
	$params->user_id = parent::get_member_user_id();
	$params->ip_address = parent::get_ip_address();
	$params->member_seq = parent::get_member_user_seq();
	$params->pic_name = parent::get_input_post("pic_name");
	$params->address = parent::get_input_post("address");
	$params->district_seq = parent::get_input_post("district_seq");
	$params->phone_no = parent::get_input_post("phone_no");
	$params->zip_code = parent::get_input_post("zip_code");
	$params->alias = parent::get_input_post("alias");

	$this->data[DATA_SELECTED][LIST_DATA][] = $params;

	if ($this->data[DATA_ERROR][ERROR] === false) {
	    try {
		$this->M_member_address->trans_begin();
		$this->M_member_address->save_add($params);
		$this->M_member_address->trans_commit();
		$this->data[DATA_SUCCESS][SUCCESS] = true;
	    } catch (BusisnessException $ex) {
		parent::set_error($this->data, $ex);
		$this->M_member_address->trans_rollback();
	    } catch (TechnicalException $ex) {
		parent::set_error($this->data, $ex);
		$this->M_member_address->trans_rollback();
	    } catch (Exception $ex) {
		parent::set_error($this->data, $ex);
		$this->M_member_address->trans_rollback();
	    }
	}

	redirect(base_url('member'));
    }

    public function save_update() {

	$params = new stdClass();
	$params->user_id = parent::get_member_user_seq();
	$params->ip_address = parent::get_ip_address();
	$params->address_seq = parent::get_input_post("address_seq");
	$params->alias = parent::get_input_post("alias");
	$params->pic_name = parent::get_input_post("pic_name");
	$params->address = parent::get_input_post("address");
	$params->district_seq = parent::get_input_post("district_seq");
	$params->zip_code = parent::get_input_post("zip_code");
	$params->phone_no = parent::get_input_post("phone_no");

	$this->data[DATA_SELECTED][LIST_DATA][] = $params;

	if ($this->data[DATA_ERROR][ERROR] === false) {
	    try {
		$this->M_member_address->trans_begin();
		$this->M_member_address->save_update($params);
		$this->M_member_address->trans_commit();
		$this->data[DATA_SUCCESS][SUCCESS] = true;
	    } catch (BusisnessException $ex) {
		parent::set_error($this->data, $ex);
		$this->M_member_address->trans_rollback();
	    } catch (TechnicalException $ex) {
		parent::set_error($this->data, $ex);
		$this->M_member_address->trans_rollback();
	    } catch (Exception $ex) {
		parent::set_error($this->data, $ex);
		$this->M_member_address->trans_rollback();
	    }
	}

	redirect(base_url('member'));
    }

    public function save_delete() {

	$params = new stdClass();
	$params->member_seq = parent::get_member_user_seq();
	$params->ip_address = parent::get_ip_address();
	$params->address_seq = parent::get_input_post("address_seq");

	try {
	    $this->M_member_address->trans_begin();
	    $this->M_member_address->save_delete($params);
	    $this->M_member_address->trans_commit();
	} catch (BusisnessException $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_member_address->trans_rollback();
	} catch (TechnicalException $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_member_address->trans_rollback();
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	    $this->M_member_address->trans_rollback();
	}

	redirect(base_url('member'));
    }

    public function get_dropdown_city_on_change() {

	$province_seq = parent::get_input_post("province_seq");

	try {
	    $sel_data = $this->M_dropdown_list->get_dropdown_city_by_province($province_seq);
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	}

	echo json_encode($sel_data);
	die();
    }

}

?>