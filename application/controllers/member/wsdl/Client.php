<?php
//custom soap client
//add timeout property for handling soap timeout
//credit to: http://www.darqbyte.com/2009/10/21/timing-out-php-soap-calls/
class Soap_Client extends SoapClient
{
    private $_userPwd;
    private $_operationTimeout;
    private $_connectionTimeout;
    private $_curlErrNo;
    private $_lastRequestHeader;
    private $_lastRequest;
    private $_lastResponseHeader;
    private $_lastResponse;
    private $_isSoap;
    
    public function setIsSoap($isSoap) {
        $this->_isSoap = $isSoap;
    }

    public function setOperationTimeout($timeout)
    {
        if (!is_int($timeout) && !is_null($timeout)){
            throw new Exception("Invalid timeout value");
        }

        $this->_operationTimeout = $timeout;
    }

    public function setConnectionTimeout($timeout)
    {
        if (!is_int($timeout) && !is_null($timeout)){
            throw new Exception("Invalid timeout value");
        }

        $this->_connectionTimeout = $timeout;
    }

    public function setUserPassword($userPassword)
    {
        $this->_userPwd = $userPassword;
    }
    
    public function __doRequest($request, $location, $action, $version, $one_way = FALSE)
    {
        //Hardcode namespace alias	
        //		if(strpos($request,'xmlns:ns3')===FALSE) {
        //			$request = str_replace("ServiceHeader>","ns2:ServiceHeader>",$request);
        //			$request = str_replace("ServiceBody>","ns2:ServiceBody>",$request);
        //			//in case the serviceHeader and the ServiceBody is empty
        //			$request = str_replace("ServiceHeader/>","ns2:ServiceHeader/>",$request);
        //			$request = str_replace("ServiceBody/>","ns2:ServiceBody/>",$request);
        //		}
        //		else {
        //			$request = str_replace("ServiceHeader>","ns3:ServiceHeader>",$request);
        //			$request = str_replace("ServiceBody>","ns3:ServiceBody>",$request);
        //			//in case the serviceHeader and the ServiceBody is empty
        //			$request = str_replace("ServiceHeader/>","ns3:ServiceHeader/>",$request);
        //			$request = str_replace("ServiceBody/>","ns3:ServiceBody/>",$request);
        //		}
        //Zend_Debug::dump($request);
        $this->_lastResponseHeader = '';

        if(!isset($this->_connectionTimeout))
            throw new Exception('Connection timeout is not set');
        if(!isset($this->_operationTimeout))
            throw new Exception('Operation timeout is not set');	

        //Zend_Debug::dump($request, 'Param Request: ');
        //Zend_Debug::dump($action, 'Param Action: ');
        //Zend_Debug::dump($location, 'Param Location: ');
        //Zend_Debug::dump($version, 'Param Version: ');
//echo realpath("cacert.pem");die;
        // Call via Curl and use the timeout
		
        $curl = curl_init($location);
        
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
//        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, TRUE); 
//        curl_setopt($curl, CURLOPT_CAINFO, file_get_contents("./cacert.pem"));
        curl_setopt($curl, CURLOPT_VERBOSE, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
        curl_setopt($curl, CURLOPT_HEADER, false);
		
        if ( $this->_isSoap ) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('SoapAction: '.$action,    //THIS HEADER IS IMPORTANT AND HAS TO EXIST!
                "Expect:",
                "Content-Type: text/xml"
            ));   
        }
        
        curl_setopt($curl, CURLOPT_TIMEOUT, $this->_operationTimeout);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $this->_connectionTimeout);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        curl_setopt($curl, CURLOPT_HEADERFUNCTION, array($this,'readResponseHeader')); 

        if(!is_null($this->_userPwd)){
            curl_setopt($curl, CURLOPT_USERPWD, $this->_userPwd);
        } 

        $response=curl_exec($curl);

        $this->_lastRequestHeader=curl_getinfo($curl,CURLINFO_HEADER_OUT);
        $this->_lastRequest = (string) $request;
        $this->_lastResponse = $response;

        if (curl_errno($curl))
        {
            $this->_curlErrNo=curl_errno($curl);
            throw new Exception(curl_error($curl));
        }

        curl_close($curl);
//            print '<pre>';            
//            print_r($this->_lastRequestHeader);
//            print_r($request);
//            print_r($this->_lastResponseHeader);

        // Return?
        if(!$one_way)
            return ($response);
    }

    public function __getLastRequestHeaders() {
        return $this->_lastRequestHeader;
    }

    public function __getLastResponseHeaders() {
        return $this->_lastResponseHeader;
    }

    public function __getLastRequest() {
        return $this->_lastRequest;
    }

    public function __getLastResponse() {
        return $this->_lastResponse;
    }

    public function getCurlErrNo() {
        return $this->_curlErrNo;
    }

    public function readResponseHeader($curl, $header) {
        $this->_lastResponseHeader .= $header;
        //Zend_Debug::dump($header,'Header: ');
        return strlen($header);
    }

}