<?php

require_once CONTROLLER_BASE_MEMBER;

class C_login_member extends controller_base_member {

    private $temp_data;
    private $message_facebook;
    private $url;

    public function __construct() {
        parent::__construct("", "", false);
        $this->initialize();
    }

    private function initialize() {
        $err = [ERROR => false, ERROR_MESSAGE => ""];
        $this->temp_data[DATA_ERROR] = $err;
        $this->load->model('member/M_login_member');
    }

    public function index() {        
        /*check if member has session*/
        if (isset($_SESSION[SESSION_MEMBER_UID]) && isset($_SESSION[SESSION_MEMBER_UNAME]) && isset($_SESSION[SESSION_MEMBER_USER_GROUP]) && isset($_SESSION[SESSION_MEMBER_EMAIL]) && isset($_SESSION[SESSION_MEMBER_SEQ]) && isset($_SESSION[SESSION_MEMBER_CSRF_TOKEN]) && isset($_SESSION[SESSION_MEMBER_LAST_LOGIN]) && isset($_SESSION[SESSION_TYPE_LOGIN]) && isset($_SESSION[SESSION_IP_ADDR])){
            redirect(base_url());
        }
        $this->url = parent::get_input_get("url");
        if (isset($_SESSION[SESSION_TEMP_DATA])) {
            $this->temp_data = $_SESSION[SESSION_TEMP_DATA];
            unset($_SESSION[SESSION_TEMP_DATA]);
        }
        $this->temp_data[DATA_AUTH][FORM_AUTH][FORM_URL] = "?url=" . $this->url;
        $this->load->view(LIVEVIEW . "member/login_member", $this->temp_data);
    }

    public function sign_out() {
        $continue_url = ($this->input->get(CONTINUE_URL) != FALSE) ? $this->input->get(CONTINUE_URL) : base_url();
        $continue_url_redirect_to_base_url = array(
            base_url() . 'member/payment',
            base_url() . 'member/profile/payment_retry',
            base_url() . 'member/wishlist',
        );

        unset($_SESSION[SESSION_MEMBER_UID]);
        unset($_SESSION[SESSION_MEMBER_UNAME]);
        unset($_SESSION[SESSION_MEMBER_USER_GROUP]);
        unset($_SESSION[SESSION_MEMBER_EMAIL]);
        unset($_SESSION[SESSION_MEMBER_SEQ]);
        unset($_SESSION[SESSION_MEMBER_CSRF_TOKEN]);
        unset($_SESSION[SESSION_MEMBER_LAST_LOGIN]);
        unset($_SESSION[SESSION_TYPE_LOGIN]);
        unset($_SESSION[SESSION_PRODUCT_INFO]);
        unset($_SESSION[SESSION_PAYMENT_INFO]);
        unset($_SESSION[SESSION_PERSONAL_INFO]);
        unset($_SESSION[SESSION_IP_ADDR]);

        //redirect to base_url
        foreach ($continue_url_redirect_to_base_url as $each_continue_url_redirect_to_base_url) {
            if ($this->startsWith($continue_url, $each_continue_url_redirect_to_base_url)) {
                redirect(base_url());
            }
        }

        redirect($continue_url);
    }

    public function sign_in() {
        $params = new stdClass();
        $params->type_login = $this->input->post("type");
        $params->user_id = $this->input->post("email");
        $params->password = $this->input->post("password");
        $current_url = ($this->input->post("current_url") != FALSE) ? $this->input->post("current_url") : base_url();

        $this->temp_data[DATA_SELECTED][LIST_DATA] = $params;

        if (filter_var($params->user_id, FILTER_VALIDATE_EMAIL) === false) {
            $this->temp_data[DATA_ERROR][ERROR] = true;
            $this->temp_data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_VALIDATION_FILL_EMAIL_FORMAT;
        }

        $params->password_1 = md5($params->password);
        $params->password_2 = md5(md5($params->password));
        $params->ip_address = $_SERVER['REMOTE_ADDR'];

        if ($this->temp_data[DATA_ERROR][ERROR] === false) {
            try {
                $list_data = $this->M_login_member->get_list($params);

                if (!isset($list_data[0])) {
                    $this->temp_data [DATA_ERROR][ERROR] = true;
                    $this->temp_data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_INVALID_PASSWORD_AND_EMAIL;

                    $member_info[SESSION_TEMP_DATA] = $this->temp_data;
                    $this->session->set_userdata($member_info);
                    redirect(base_url("member/login") . "?url=" . $this->url);
                }
                parent::set_list_data($this->temp_data, $list_data);
            } catch (Exception $ex) {
                parent::set_error($this->temp_data, $ex);
            }

            if ($list_data[0]->new_password == $params->password_2 || $list_data[0]->old_password == $params->password_1) {
                if (isset($list_data[0])) {
                    $member_info[SESSION_MEMBER_UID] = $list_data[0]->seq;
                    $member_info[SESSION_MEMBER_UNAME] = $list_data[0]->user_name;
                    $member_info[SESSION_MEMBER_USER_GROUP] = MEMBER;
                    $member_info[SESSION_MEMBER_EMAIL] = $list_data[0]->email;
                    $member_info[SESSION_MEMBER_SEQ] = $list_data[0]->seq;
                    $member_info[SESSION_TYPE_LOGIN] = $params->type_login;
                    $member_info[SESSION_MEMBER_LAST_LOGIN] = $list_data[0]->last_login;
                    $member_info[SESSION_MEMBER_CSRF_TOKEN] = base64_encode(openssl_random_pseudo_bytes(32));
                    $member_info[SESSION_IP_ADDR] = $_SERVER["REMOTE_ADDR"];

                    $this->session->set_userdata($member_info);
                    if ($list_data[0]->new_password == "") {
                        redirect(base_url("member/profile/change_old_password_member"));
                    } else {
                        unset($_SESSION[FACEBOOK_ID]);
                        unset($_SESSION[FACEBOOK_EMAIL]);
                        unset($_SESSION[FACEBOOK_NAME]);
                        unset($_SESSION[FACEBOOK_GENDER]);

                        $this->url = parent::get_input_get("url");
                        if ($this->url <> "") {
                            redirect(base_url($this->url));
                        } else {
                            redirect($current_url);
                        }
                    }
                }
            } else {
                $this->temp_data[DATA_ERROR][ERROR] = true;
                $this->temp_data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_INVALID_PASSWORD_AND_EMAIL;
                $member_info[SESSION_TEMP_DATA] = $this->temp_data;
                $this->session->set_userdata($member_info);
                redirect(base_url("member/login") . "?url=" . $this->url);
            }
        }

        $member_info[SESSION_TEMP_DATA] = $this->temp_data;
        $this->session->set_userdata($member_info);
        redirect(base_url("member/login") . "?url=" . $this->url);
    }

}

?>