<?php

require_once CONTROLLER_BASE_MEMBER;

class C_payment_proccess_member extends controller_base_member {

    private $data;
    private $order_no;
    private $get_user_id;
    private $tipe;
    private $apps;

    public function __construct() {
        $this->data = parent::__construct("", "", false);
        $this->initialize();
    }

    private function initialize() {
        $this->order_no = $this->uri->segment(4);

        $this->load->model('member/M_order_member');
        $this->load->model('member/M_payment_member');
        $this->load->model('member/M_deposit_member');
        $this->load->model('admin/transaction/M_order_merchant_admin');
        $this->load->library('curl');
        $this->load->library('curl_dimo');
        $this->load->library('keygenerator');
        
        $this->apps = false;
        if (parent::is_agent()) {
            $this->get_user_id = parent::get_agent_user_id();
            $this->tipe = AGENT;
        } else {
            $this->get_user_id = parent::get_member_user_id();
            $this->tipe = '';
        }
    }

    public function index() {
        $remote_addr = $_SERVER['REMOTE_ADDR'];
        $payment_type = $this->callback_ip_address($remote_addr);
        $request = '';

        //for Mandiri Click Pay
        if (parent::get_input_get("billreferenceno") != "") {
            $this->get_order_no_from_signature(parent::get_input_get("billreferenceno"));
        }

        //for QR code 
        if ($payment_type == PAYMENT_TYPE_QR_CODE) {
            $this->get_order_no_from_signature(parent::get_input_get("invoiceId"));
        }

        // for BCA KLIKPAY
        if (parent::get_input_post("transactionNo")) {
            $this->order_no = parent::get_input_post("transactionNo");
            foreach ($_POST as $key => $value)
                $request .= "[" . $key . '] => [' . $value . ']
                                    ';
        }

        $params = new stdClass();
        $params->user_id = $this->get_user_id;
        $params->ip_address = parent::get_ip_address();
        $params->key = $this->order_no;

        $order_info = $this->M_order_member->get_order_info_by_order_no($params, $this->tipe);

        if (!isset($order_info[0])) {
            $order_info = $this->M_order_member->get_order_info_by_order_no($params, AGENT);
        }

        try {
            $this->data[DATA_SELECTED][LIST_DATA] = $order_info;
            //for QR code 
            if ($payment_type == PAYMENT_TYPE_QR_CODE) {
                $this->save_payment_log('', json_encode($this->input->get()));
                $status_callback_invoice_qr_code = $this->callback_invoice_qr_code();
                $qr_code_data = $this->process_confirm_qr_code($status_callback_invoice_qr_code);
                $this->save_success_order_payment($order_info[0][0]);
                exit();
            } elseif ($payment_type == PAYMENT_TYPE_BCA_KLIKPAY) {
                if (!isset($order_info[0][0])) {
                    exit('not allow to test');
                }
                $this->get_payment_type_order($order_info[0][0]);
                $params_payment = new stdClass();
                $params_payment->user_id = "SYSTEM";
                $params_payment->ip_address = parent::get_ip_address();
                $params_payment->order_seq = $order_info[0][0]->seq;
                $params_payment->order_no = $order_info[0][0]->order_no;
                $params_payment->payment_method_seq = $order_info[0][0]->pg_method_seq;
                $params_payment->signature = $order_info[0][0]->signature;
                $params_payment->request = $request;
                $params_payment->response = $this->data[DATA_PAYMENT];
                $params_payment->request_date = date('Y-m-d H:i:s');
                $params_payment->response_date = date('Y-m-d H:i:s');
                $this->M_payment_member->save_payment_log($params_payment);
                exit();
            }

            //for docu payment
            if (!isset($order_info[0])) {
                $params->key = $_POST["TRANSIDMERCHANT"];
                $this->order_no = trim(substr($params->key, 0, 18));
                $order_info = $this->M_order_member->get_order_info_by_order_no($params, $this->tipe);
                if ($order_info[0][0]->payment_status == PAYMENT_WAIT_CONFIRM_THIRD_PARTY) {
                    $this->get_payment_type_order($order_info[0][0]);
                    $response = "";
                    foreach ($_POST as $key => $value)
                        $response .= "[" . $key . '] => [' . $value . ']
                                    ';
                    $params_payment = new stdClass();
                    $params_payment->user_id = $this->get_user_id;
                    $params_payment->ip_address = parent::get_ip_address();
                    $params_payment->order_seq = $order_info[0][0]->seq;
                    $params_payment->order_no = $order_info[0][0]->order_no;
                    $params_payment->payment_method_seq = $order_info[0][0]->pg_method_seq;
                    $params_payment->signature = $order_info[0][0]->signature;
                    $params_payment->response = $response;
                    $params_payment->response_date = date('Y-m-d H:i:s');
                    $this->M_payment_member->save_payment_update_log($params_payment);
                }
            } elseif (isset($order_info[0])) {
                if ($order_info[0][0]->member_seq != $this->get_user_id && $order_info[0][0]->apps === "0") {
                    redirect(base_url() . "error_404");
                } else if ($order_info[0][0]->payment_status == PAYMENT_UNPAID_STATUS_CODE && $order_info[0][0]->payment_code == PAYMENT_TYPE_BCA_KLIKPAY) {
                    $this->save_failed_order_payment($order_info[0][0]);
                } else if ($order_info[0][0]->payment_status == PAYMENT_UNPAID_STATUS_CODE) {
                    $this->get_payment_type_order($order_info[0][0]);
                    $response = "";
                    foreach ($_POST as $key => $value)
                        $response .= "[" . $key . '] => [' . $value . ']
                                    ';
                    $params_payment = new stdClass();
                    $params_payment->user_id = $this->get_user_id;
                    $params_payment->ip_address = parent::get_ip_address();
                    $params_payment->order_seq = $order_info[0][0]->seq;
                    $params_payment->order_no = $order_info[0][0]->order_no;
                    $params_payment->payment_method_seq = $order_info[0][0]->pg_method_seq;
                    $params_payment->signature = $order_info[0][0]->signature;
                    $params_payment->response = $response;
                    $params_payment->response_date = date('Y-m-d H:i:s');
                    $this->M_payment_member->save_payment_update_log($params_payment);
                }
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        if ($order_info[0][0]->apps === "0") {
            redirect(base_url("member/payment") . "/" . $this->order_no);
        } else {
            redirect(base_url("thankyou") . "/" . $this->order_no);
        }
    }

    private function get_payment_type_order($data) {
        switch ($data->payment_code) {
            case PAYMENT_TYPE_BANK:$this->payment_type_bank($data);
                break;
            case PAYMENT_TYPE_DEPOSIT: $this->payment_type_deposit($data);
                break;
            case PAYMENT_TYPE_MANDIRI_KLIKPAY: $this->proccess_payment_mandiri_click_pay($data);
                break;
            case PAYMENT_TYPE_BCA_KLIKPAY: $this->proccess_payment_bca_klikpay($data);
                break;
            case PAYMENT_TYPE_MANDIRI_ECASH : $this->proccess_payment_mandiri_ecash($data);
                break;
            case PAYMENT_TYPE_CREDIT:$this->proccess_payment_credit_card($data);
                break;
            case PAYMENT_TYPE_CREDIT_CARD: $this->proccess_payment_credit_card($data);
                break;
            case PAYMENT_TYPE_DOCU_ATM: $this->proccess_payment_docu_atm($data);
                break;
            case PAYMENT_TYPE_DOCU_ALFAMART: $this->proccess_payment_docu_alfamart($data);
                break;
            case PAYMENT_TYPE_QR_CODE: $this->proccess_payment_qr($data);
                break;
        }
    }

    private function proccess_payment_credit_card($data) {
        try {
            if ($_POST['MERCHANT_TRANID'] == $data->order_trans_no) {
                if ($_POST['TXN_STATUS'] == 'A' && $_POST['EUI'] == 'SUC') {
                    if ($this->get_order_payment_credit_card_status($data)) {
                        $this->save_success_order_payment($data);
                    } else {
                        $this->save_failed_order_payment($data);
                    }
                } else if ($_POST['TXN_STATUS'] == 'A' && $_POST['EUI'] == 'ALT') {
                    if ($this->get_order_payment_credit_card_status($data)) {
                        $this->save_success_order_payment($data);
                    } else {
                        $this->save_failed_order_payment($data);
                    }
                } else {
                    $this->save_failed_order_payment($data);
                }
            } else {
                $this->save_failed_order_payment($data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->save_failed_order_payment($data);
        }
    }

    private function proccess_payment_docu_atm($data) {
        try {
            $paymentchannel = isset($_POST['PAYMENTCHANNEL']) ? $_POST['PAYMENTCHANNEL'] : '';
            $sessionid = isset($_POST['SESSIONID']) ? $_POST['SESSIONID'] : '';

            $words = isset($_POST['WORDS']) ? $_POST['WORDS'] : '';
            $amount = isset($_POST['AMOUNT']) ? $_POST['AMOUNT'] : '';
            $transidmerchant = isset($_POST['TRANSIDMERCHANT']) ? $_POST['TRANSIDMERCHANT'] : '';
            $resultmsg = isset($_POST['RESULTMSG']) ? $_POST['RESULTMSG'] : '';
            $verifystatus = isset($_POST['VERIFYSTATUS']) ? $_POST['VERIFYSTATUS'] : '';

            $word_generate = sha1($amount . MALLID . SHARED_KEY . $transidmerchant . $resultmsg . $verifystatus);
            if ($_POST['WORDS'] == $word_generate) {
                if (!$_POST) {
                    throw new Exception(ERROR_ACCESS_NOT_VALID);
                }
                if (!$this->is_ip_doku()) {
                    throw new Exception(ERROR_IP_NOT_ALLOWED);
                } else {
                    $verifystatus = isset($_POST['VERIFYSTATUS']) ? $_POST['VERIFYSTATUS'] : '';
                    $edustatus = isset($_POST['EDUSTATUS']) ? $_POST['EDUSTATUS'] : '';
                    $words = sha1(trim(MALLID) . trim(SHARED_KEY) . trim($data->order_trans_no));
                    if ($transidmerchant != '' AND $amount != '' AND $sessionid != '') {
                        if (
                                strtoupper(trim($resultmsg)) == 'SUCCESS' AND
                                strtoupper(trim($paymentchannel)) != CREDIT_CARD_VISA_MASTER_MULTI_CURRENCY
                        ) {
                            $this->curl->set_url(URL_CHECK_STATUS . "?MALLID=" . MALLID . "&CHAINMERCHANT=" . CHAIN_MERCHANT . "&TRANSIDMERCHANT=" . $data->order_trans_no . "&WORDS=" . $words . "&SESSIONID=" . $sessionid . "&PAYMENTCHANNEL=" . ATM_PERMATA_VA_LITE);
                            $this->curl->setIsUsingUserAgent(true);
                            $this->curl->setData();
                            $response = $this->curl->getResponse();
                            $xml = simplexml_load_string($response);
                            $transidmerchant = isset($xml->TRANSIDMERCHANT) ? $xml->TRANSIDMERCHANT : '';
                            $resultmsg = isset($xml->RESULTMSG) ? $xml->RESULTMSG : '';
                            $paymentchannel = isset($xml->PAYMENTCHANNEL) ? $xml->PAYMENTCHANNEL : '';
                            $amount = isset($xml->AMOUNT) ? $xml->AMOUNT : '';
                            $sessionid = isset($xml->SESSIONID) ? $xml->SESSIONID : '';

                            if ($transidmerchant != '' AND $amount != '' AND $sessionid != '') {
                                if (
                                        strtoupper(trim($resultmsg)) == 'SUCCESS' AND
                                        strtoupper(trim($paymentchannel)) != CREDIT_CARD_VISA_MASTER_MULTI_CURRENCY
                                ) {
                                    $this->save_success_order_payment($data);
                                }
                            }
                        } elseif (
                                strtoupper(trim($resultmsg)) == 'SUCCESS' AND
                                strtoupper(trim($paymentchannel)) == CREDIT_CARD_VISA_MASTER_MULTI_CURRENCY AND
                                strtoupper(trim($edustatus)) != 'NA'
                        ) {
                            $transaction_status = 'PENDING';
                        } elseif (
                                strtoupper(trim($resultmsg)) == 'SUCCESS' AND
                                strtoupper(trim($paymentchannel)) == CREDIT_CARD_VISA_MASTER_MULTI_CURRENCY AND
                                strtoupper(trim($edustatus)) == 'NA'
                        ) {
                            $transaction_status = 'SUCCESS';
                        } elseif (
                                strtoupper(trim($resultmsg)) == 'FAILED'
                        ) {
                            $transaction_status = 'FAILED';
                        }
                    }
                }
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    private function proccess_payment_docu_alfamart($data) {
        try {
            $paymentchannel = isset($_POST['PAYMENTCHANNEL']) ? $_POST['PAYMENTCHANNEL'] : '';
            $sessionid = isset($_POST['SESSIONID']) ? $_POST['SESSIONID'] : '';

            $words = isset($_POST['WORDS']) ? $_POST['WORDS'] : '';
            $amount = isset($_POST['AMOUNT']) ? $_POST['AMOUNT'] : '';
            $transidmerchant = isset($_POST['TRANSIDMERCHANT']) ? $_POST['TRANSIDMERCHANT'] : '';
            $resultmsg = isset($_POST['RESULTMSG']) ? $_POST['RESULTMSG'] : '';
            $verifystatus = isset($_POST['VERIFYSTATUS']) ? $_POST['VERIFYSTATUS'] : '';

            $word_generate = sha1($amount . MALLID . SHARED_KEY . $transidmerchant . $resultmsg . $verifystatus);

            if ($_POST['WORDS'] == $word_generate) {
                if (!$_POST) {
                    throw new Exception(ERROR_ACCESS_NOT_VALID);
                }
                if (!$this->is_ip_doku()) {
                    throw new Exception(ERROR_IP_NOT_ALLOWED);
                } else {
                    $verifystatus = isset($_POST['VERIFYSTATUS']) ? $_POST['VERIFYSTATUS'] : '';
                    $edustatus = isset($_POST['EDUSTATUS']) ? $_POST['EDUSTATUS'] : '';
                    $words = sha1(trim(MALLID) . trim(SHARED_KEY) . trim($data->order_trans_no));
                    if ($transidmerchant != '' AND $amount != '' AND $sessionid != '') {
                        if (
                                strtoupper(trim($resultmsg)) == 'SUCCESS' AND
                                strtoupper(trim($paymentchannel)) != CREDIT_CARD_VISA_MASTER_MULTI_CURRENCY
                        ) {
                            $this->curl->set_url(URL_CHECK_STATUS . "?MALLID=" . MALLID . "&CHAINMERCHANT=" . CHAIN_MERCHANT . "&TRANSIDMERCHANT=" . $data->order_trans_no . "&WORDS=" . $words . "&SESSIONID=" . $sessionid . "&PAYMENTCHANNEL=" . ATM_PERMATA_VA_LITE);
                            $this->curl->setIsUsingUserAgent(true);
                            $this->curl->setData();
                            $response = $this->curl->getResponse();
                            $xml = simplexml_load_string($response);
                            $transidmerchant = isset($xml->TRANSIDMERCHANT) ? $xml->TRANSIDMERCHANT : '';
                            $resultmsg = isset($xml->RESULTMSG) ? $xml->RESULTMSG : '';
                            $paymentchannel = isset($xml->PAYMENTCHANNEL) ? $xml->PAYMENTCHANNEL : '';
                            $amount = isset($xml->AMOUNT) ? $xml->AMOUNT : '';
                            $sessionid = isset($xml->SESSIONID) ? $xml->SESSIONID : '';
                            if ($transidmerchant != '' AND $amount != '' AND $sessionid != '') {
                                if (
                                        strtoupper(trim($resultmsg)) == 'SUCCESS' AND
                                        strtoupper(trim($paymentchannel)) != CREDIT_CARD_VISA_MASTER_MULTI_CURRENCY
                                ) {
                                    $this->save_success_order_payment($data);
                                }
                            }
                        } elseif (
                                strtoupper(trim($resultmsg)) == 'SUCCESS' AND
                                strtoupper(trim($paymentchannel)) == CREDIT_CARD_VISA_MASTER_MULTI_CURRENCY AND
                                strtoupper(trim($edustatus)) != 'NA'
                        ) {
                            $transaction_status = 'PENDING';
                        } elseif (
                                strtoupper(trim($resultmsg)) == 'SUCCESS' AND
                                strtoupper(trim($paymentchannel)) == CREDIT_CARD_VISA_MASTER_MULTI_CURRENCY AND
                                strtoupper(trim($edustatus)) == 'NA'
                        ) {
                            $transaction_status = 'SUCCESS';
                        } elseif (
                                strtoupper(trim($resultmsg)) == 'FAILED'
                        ) {
                            $transaction_status = 'FAILED';
                        }
                    }
                }
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    private function is_ip_doku() {
        $ip_low = '103.10.129.0';
        $ip_high = '103.10.129.24';
        $ip = $_SERVER['REMOTE_ADDR'];
        return ($ip <= $ip_high && $ip_low <= $ip) ? true : false;
    }

    private function proccess_payment_mandiri_ecash($data) {
        try {
            if ($_GET['id'] == $data->signature) {
                $this->curl->set_url(MANDIRI_ECASH_VALIDATION_URL . "?id=" . $data->signature);
                $this->curl->setIsUsingUserAgent(true);
                $this->curl->setData();
                $response = $this->curl->getResponse();

                $data_responses = explode(',', $response);
                if (count($data_responses) > 0 && isset($data_responses[4])) {
                    if (trim($data_responses[4]) == 'SUCCESS') {
                        $this->save_success_order_payment($data);
                    } else {
                        $this->save_failed_order_payment($data);
                    }
                }
            } else {
                $this->save_failed_order_payment($data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->save_failed_order_payment($data);
        }
    }

    private function proccess_payment_mandiri_click_pay($data) {

        $signature = md5(MANDIRI_CLICKPAY_MERCHANT_ID . $data->signature . MANDIRI_CLICKPAY_MERCHANT_PWD);
        $url = MANDIRI_CLICKPAY_CHECKPAYMENT . "?user_id=" . MANDIRI_CLICKPAY_MERCHANT_ID . "&billreferenceno=" . $data->signature . "&signature=" . $signature;
        $this->curl->set_url($url);
        $this->curl->setIsUsingUserAgent(true);
        $this->curl->setData();
        $response = $this->curl->getResponse();
        $xmlObject = simplexml_load_string($response);

        if ($xmlObject === false) {
            $this->save_failed_order_payment($data);
        } elseif ($xmlObject->response_code == '0000') {
            $this->save_success_order_payment($data);
        } else if ($xmlObject->response_code == '3203' || $xmlObject->response_code == '0204') {
            $signature = md5(MANDIRI_CLICKPAY_MERCHANT_ID . $data->signature . $data->total_payment . MANDIRI_CLICKPAY_MERCHANT_PWD);
            $param = "?user_id=" . MANDIRI_CLICKPAY_MERCHANT_ID . "&billreferenceno=" . $data->order_trans_no . "&amount=" . $data->total_payment . "&date_time=" . date('YmdHis', $data->order_date) . "=&signature=" . $signature;
            $urlrev = MANDIRI_CLICKPAY_REVERSAL . $param;
            $this->curl->set_url($urlrev);
            $this->curl->setIsUsingUserAgent(true);
            $this->curl->setData();
            $response2 = $this->curl->getResponse();
            $xmlObject2 = simplexml_load_string($response2);

            if ($xmlObject2 === false) {
                $this->save_failed_order_payment($data);
            } else if ($xmlObject2->response_code == '0000') {
                $this->save_success_order_payment($data);
            } else {
                $this->save_failed_order_payment($data);
            }
        } else {
            $this->save_failed_order_payment($data);
        }
    }

    private function get_order_payment_credit_card_status($data) {
        if ($data->promo_credit_seq > 0) {
            $credit_info = new stdClass();
            $credit_info->user_id = $this->get_user_id;
            $credit_info->ip_address = parent::get_ip_address();
            $credit_info->order_seq = $data->seq;
            $credit_info->order_no = $data->order_no;
            $credit_info->promo_credit_seq = $data->promo_credit_seq;
            $data_credit = $this->M_order_member->get_credit_info_by_credit_seq($credit_info);
            if (isset($data_credit)) {
                switch ($data_credit[0]->bank_credit_seq) {
                    case BCA_PERIOD_3_SEQ :
                        $this->data[DATA_CREDIT_MID] = BCA_PERIOD_3_MID;
                        $this->data[DATA_CREDIT_PAS] = BCA_PERIOD_3_PASSWORD;
                        break;
                    case BCA_PERIOD_6_SEQ :
                        $this->data[DATA_CREDIT_MID] = BCA_PERIOD_6_MID;
                        $this->data[DATA_CREDIT_PAS] = BCA_PERIOD_6_PASSWORD;
                        break;
                    default :
                        throw new Exception(ERROR_CREDIT_SEQ);
                }
            }
            $params = "TRANSACTIONTYPE=4&MERCHANTID=" . $this->data[DATA_CREDIT_MID] . "&MERCHANT_TRANID=" . $data->order_trans_no . "&AMOUNT=" . $data->total_payment . ".00&RESPONSE_TYPE=3&TXN_PASSWORD=" . $this->data[DATA_CREDIT_PAS] . "&SIGNATURE=" . $data->signature . "&GEN_HASH=Yes";
        } else {
            $params = "TRANSACTIONTYPE=4&MERCHANTID=" . PGS_MERCHANT_ID . "&MERCHANT_TRANID=" . $data->order_trans_no . "&AMOUNT=" . $data->total_payment . ".00&RESPONSE_TYPE=3&TXN_PASSWORD=" . PGS_TXN_PASSWORD . "&SIGNATURE=" . $data->signature . "&GEN_HASH=Yes";
        }
        $url = PGS_URL_TRACE;
        $this->curl->set_url($url . "?" . $params);
        $this->curl->setIsUsingUserAgent(true);
        $this->curl->setData();
        $response = $this->curl->getResponse();
        $result = explode(';', $response);
        foreach ($result as $each) {
            $exploder = explode('=', $each);
            $$exploder[0] = $exploder[1];
        }
        if ($ERR_CODE == '0' && ($TXN_STATUS == 'A' || $TXN_STATUS == 'C') && $EUI == 'SUC') {
            return true;
        } else if ($TXN_STATUS == 'A' && $EUI == 'ALT') {
            return false;
        } else {
            return false;
        }
    }

    public function save_docu_payment_code() {

        try {
//            if ($this->is_ip_doku()) {
            $params = new stdClass();
            $params->user_id = $this->get_user_id;
            $params->ip_address = parent::get_ip_address();
            $params->order_no = $_POST["TRANSIDMERCHANT"];
            $params->payment_code = $_POST["PAYMENTCODE"];
            $this->M_order_member->save_update_payment_code_docu($params);

//            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    private function save_success_order_payment($data) {

        $params = new stdClass();
        $params->user_id = $this->get_user_id;
        $params->ip_address = parent::get_ip_address();
        $params->order_seq = $data->seq;
        $params->order_no = $data->order_no;
        $params->payment_status = PAYMENT_PAID_STATUS_CODE;
        $params->order_status = ORDER_NEW_ORDER_STATUS_CODE;
        $params->paid_date = date('Y-m-d H:i:s');

        $params_voucher = new stdClass();
        $params_voucher->user_id = $this->get_user_id;
        $params_voucher->ip_address = parent::get_ip_address();
        $params_voucher->node_cd = NODE_SHOPPING_MEMBER;
        $params_voucher->member_seq = $data->member_seq;
        $params_voucher->total_order = $data->total_order;

        try {
            if (strlen($data->order_no) > 15) {
                $this->M_payment_member->trans_begin();
                $this->M_payment_member->save_update_status_order($params);
                parent::generate_voucher($params_voucher, $data->total_order);
                $this->M_payment_member->trans_commit();

                //for android apps purpose
                if ($data->apps === "1") {
                    $this->sendNotification($data, SUCCESS_STATUS);
                }

                $params->ip_address = parent::get_ip_address();
                $params->key = $this->order_no;

                $order_info = $this->M_order_member->get_order_info_by_order_no($params, $this->tipe);

                if (!isset($order_info[0])) {
                    $this->apps = true;
                    $order_info = $this->M_order_member->get_order_info_by_order_no($params, AGENT);
                }

                $order_content = "<table border='1'>
                           <thead>
                            <tr>
                                <th>Produk</th>
                                <th>Qty</th>
                                <th>Harga (Rp)</th>
                                <th>Biaya Kirim (Rp)</th>
                                <th>Total (Rp)</th>
                            </tr>
                           </thead><tbody>";

                foreach ($order_info[2] as $product) {
                    $order_content = $order_content . "<tr>
                                               <td>" . $product->display_name . "</td>
                                               <td>" . $product->qty . "</td>
                                               <td>" . number_format($product->sell_price) . "</td>
                                               <td>" . number_format(ceil($product->weight_kg * $product->qty) * $product->ship_price_charged) . "</td>
                                               <td>" . number_format(ceil($product->weight_kg * $product->qty) * $product->ship_price_charged + $product->sell_price * $product->qty) . "</td></tr>";
                }
                $rowcredit = '';
                if (isset($order_info[0][0]->promo_credit_seq) && $order_info[0][0]->promo_credit_seq > 0) {
                    $params->promo_credit_seq = $order_info[0][0]->promo_credit_seq;
                    $data_credit = $this->M_order_member->get_credit_info_by_credit_seq($params);
                    if (isset($data_credit)) {
                        $rowcredit = "<tr>
                                <td colspan=4>Kredit Bank</td>
                                <td>" . $data_credit[0]->bank_name . "</td>
                            </tr>
			    <tr>
                                <td colspan=4>Periode cicilan</td>
                                <td>" . $data_credit[0]->credit_month . " bulan</td>
                            </tr>
			    <tr>
                                <td colspan=4>Pembayaran bulanan</td>
                                <td>" . number_format($order_info[0][0]->total_payment / $data_credit[0]->credit_month) . "</td>
                            </tr>";
                    }
                }
                $order_content = $order_content . "</tbody>
                                          <tr>
                                            <td colspan=4>Voucher</td>
                                            <td>" . number_format($order_info[0][0]->nominal) . "</td>
                                          </tr>
                                          <tr>
                                            <td colspan=4>Total Bayar</td>
                                            <td>" . number_format($order_info[0][0]->total_payment) . "</td>
                                          </tr>" . $rowcredit . "
                                          </table>";

                $address_content = "Penerima : " . $order_info[0][0]->receiver_name . "<br>
                                Alamat : " . $order_info[0][0]->receiver_address . "-" . $order_info[0][0]->province_name . "-" . $order_info[0][0]->city_name . "-" . $order_info[0][0]->district_name . "<br>
                                No Tlp : " . $order_info[0][0]->receiver_phone_no . "<br>";

                $params_email_member = new stdClass();
                $params_email_member->user_id = $this->get_user_id;
                $params_email_member->ip_address = parent::get_ip_address();


                $params_member = new stdClass();
                $params_member->user_id = $this->get_user_id;
                $params_member->ip_address = parent::get_ip_address();
                $params_member->key = $order_info[0][0]->order_no;
                $order_info_new = $this->M_order_member->get_order_info_by_order_no($params_member);
                
                if ($this->apps === false && $this->is_member_get_axa($order_info_new)) {
                    $order_info_new[0][0]->payment_status = 'P';
                    $params_email_member->code = ORDER_PAY_CONFIRM_SUCCESS_WITH_AXA_CODE;
                } else {
                    $params_email_member->code = ORDER_PAY_CONFIRM_SUCCESS_CODE;
                }

                $params_email_member->RECIPIENT_NAME = $order_info[0][0]->member_name;
                $params_email_member->ORDER_NO = $order_info[0][0]->order_no;
                $params_email_member->ORDER_DATE = parent::cdate($order_info[0][0]->order_date);
                $params_email_member->RECIPIENT_ADDRESS = $address_content;
                $params_email_member->TOTAL_PAYMENT = parent::cnum($order_info[0][0]->total_payment);
                $params_email_member->PAYMENT_METHOD = $data->payment_name;
                $params_email_member->ORDER_ITEMS = $order_content;
                $params_email_member->CONFIRM_LINK = base_url("member/payment/" . $order_info[0][0]->order_no);
                $params_email_member->to_email = $order_info[0][0]->email;
                parent::email_template($params_email_member);

                foreach ($order_info[1] as $value) {
                    $params_email_merchant = new stdClass();
                    $params_email_merchant->user_id = $this->get_user_id;
                    $params_email_merchant->ip_address = parent::get_ip_address();
                    $params_email_merchant->code = ORDER_NEW_CODE;
                    $params->order_seq = $order_info[0][0]->seq;
                    $params->merchant_info_seq = $value->merchant_info_seq;
                    $merchant_product = $this->M_order_merchant_admin->get_produk($params);

                    $product_order_content = "";
                    $product_order_content = "<table border='1'>
                                                            <thead>
                                                             <tr>
                                                                 <th>Produk</th>
                                                                 <th>Tipe Product</th>
                                                                 <th>Qty</th>
                                                             </tr>
                                                            </thead><tbody>";

                    foreach ($merchant_product as $product_merchant) {
                        $product_order_content = $product_order_content . "<tr>
                                                                    <td>" . $product_merchant->product_name . "</td>
                                                                    <td>" . $product_merchant->variant_name . "</td>
                                                                    <td>" . $product_merchant->qty . "</td></tr>";
                    }
                    $product_order_content = $product_order_content . "</tbody> </table>";

                    $params_email_merchant->ORDER_ITEMS = $product_order_content;
                    $params_email_merchant->RECIPIENT_NAME = $value->merchant_name;
                    $params_email_merchant->ORDER_NO = $order_info[0][0]->order_no;
                    $params_email_merchant->ORDER_DATE = parent::cdate($order_info[0][0]->order_date);
                    $params_email_merchant->RECIPIENT_ADDRESS = $address_content;
                    $params_email_merchant->email_code = parent::generate_random_alnum(20, true);
                    $params_email_merchant->CONFIRM_LINK = base_url() . "merchant/transaction/merchant_delivery";

                    $params_email_merchant->to_email = $value->email;
                    parent::email_template($params_email_merchant);

                    /*
                      $params_notif = new stdClass();
                      $params_notif->user_id = $this->get_user_id;
                      $params_notif->ip_address = parent::get_ip_address();
                      $params_notif->code = PAID_ORDER_MERCHANT;
                      $params_notif->user_group_seq = '3';
                      $params_notif->merchant_seq = $value->merchant_seq;
                      $url_detail = base_url().'merchant/transaction/merchant_delivery?'.CONTROL_GET_TYPE.'='.ACTION_EDIT.'&key='.$params->order_seq;
                      //$url_detail = 'order_seq_'.$params->order_seq;
                      $params_notif->order_no = "<a id='".$url_detail."'>".$order_info[0][0]->order_no."</a>";
                      $params_notif->recipient_name = $order_info_new[0][0]->receiver_name;
                      $params_notif->paid_before = date("j F, Y", strtotime("+3 day"));
                      parent::notif_template($params_notif);

                      $params_notif_member = new stdClass();
                      $params_notif_member->user_id = $this->get_user_id;
                      $params_notif_member->ip_address = parent::get_ip_address();
                      $params_notif_member->code = PAID_ORDER_MEMBER;
                      $params_notif_member->user_group_seq = '4';
                      $params_notif_member->member_seq = $data->member_seq;
                      $url_detail = base_url().'member/payment/'.$order_info[0][0]->order_no;
                      $params_notif_member->order_no = "<a href='".$url_detail."'>".$order_info[0][0]->order_no."</a>";
                      parent::notif_template($params_notif_member);
                     */
                }
            } else {
                $this->M_payment_member->trans_begin();
                $this->M_payment_member->save_update_status_order_voucher($params);

                date_default_timezone_set("Asia/Bangkok");
                $signature = md5(md5(SMTEL_SERVER_PIN . SMTEL_SERVER_SECRET_KEY . $data->order_no) . md5(SMTEL_CHANNEL_ID . SMTEL_STORE_ID . SMTEL_POS_ID . date("YmdHis")));
                $this->curl->set_url(SMTEL_URL_REQUEST . "?password=" . $signature . "&channelid=" . SMTEL_CHANNEL_ID . "&cmd=TOPUP&hp=" . str_replace(" ", "", $data->phone_number) . "&vtype=" . $data->mapping_code . "&trxtime=" . date("YmdHis") . "&partner_trxid=" . $data->order_no . "&storeid=" . SMTEL_STORE_ID . "&posid=" . SMTEL_POS_ID);
                $this->curl->setGet(true);
                $this->curl->setIsUsingUserAgent(true);
                $this->curl->setData();
                $response = $this->curl->getResponse();

                $xmlObject = simplexml_load_string($response);

                $xmlString = '';

                if (isset($xmlObject)) {
                    foreach ($xmlObject as $key => $xml_string) {
                        $xmlString .= '[' . $key . '] = ' . $xml_string . '
						';
                    }
                }

                $params_payment = new stdClass();
                $params_payment->user_id = $this->get_user_id;
                $params_payment->ip_address = parent::get_ip_address();
                $params_payment->order_seq = $data->seq;
                $params_payment->order_no = $data->order_no;
                $params_payment->payment_method_seq = $data->pg_method_seq;
                $params_payment->signature = $signature;
                $params_payment->request = $this->curl->getRequest();
                $params_payment->request_date = date('Y-m-d H:i:s');
                $params_payment->response = $xmlString;
                $params_payment->response_date = date('Y-m-d H:i:s');
                $this->M_payment_member->save_voucher_log($params_payment);


                if (isset($xmlObject)) {
                    if ($xmlObject->rescode == '4') {
                        $data->order_status = ORDER_SUCCESS_STATUS_CODE;
                        $this->M_payment_member->save_update_prepaid_voucher_status($data);
                    } elseif ($xmlObject->response_code == '2') {
                        $data->order_status = ORDER_FAIL_STATUS_CODE;
                        $this->M_payment_member->save_update_prepaid_voucher_status($data);
                    }
                }
                $this->M_payment_member->trans_commit();
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_payment_member->trans_rollback();
        }
    }

    private function save_failed_order_payment($data) {

        $params = new stdClass();
        $params->user_id = $this->get_user_id;
        $params->ip_address = parent::get_ip_address();
        $params->order_seq = $data->seq;
        $params->order_no = $data->order_no;
        $params->payment_status = PAYMENT_FAILED_STATUS_CODE;
        $params->order_status = ORDER_PREORDER_STATUS_CODE;
        $params->paid_date = date('00-00-00 00:00:00');

        try {
            $this->M_payment_member->trans_begin();
            $this->M_payment_member->save_update_status_order($params);
            $this->M_payment_member->trans_commit();

            //for android apps purpose
            if ($data->apps === "1") {
                $this->sendNotification($data, FAILED_STATUS);
            }

            $email_order = new stdClass();
            $email_order->user_id = $this->get_user_id;
            $email_order->ip_address = parent::get_ip_address();
            $email_order->code = ORDER_PAY_CONFIRM_FAIL_CODE;
            $email_order->RECIPIENT_NAME = $data->member_name;
            $email_order->ORDER_NO = $data->order_no;
            $email_order->ORDER_DATE = parent::cdate($data->order_date);
            $email_order->to_email = $data->email;
            $email_order->LINK = base_url() . "member/profile/payment_retry/" . $data->order_no;
            parent::email_template($email_order);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_payment_member->trans_rollback();
        }
    }

    private function get_order_no_from_signature($signature) {
        $params = new stdClass();
        $params->user_id = $this->get_user_id;
        $params->ip_address = parent::get_ip_address();
        $params->signature = $signature;

        $order_no = $this->M_payment_member->get_order_no_from_signature($params);
        $this->order_no = isset($order_no) ? $order_no[0]->order_no : "";
    }

    private function callback_invoice_qr_code() {
        $seal = $this->input->get("seal");

        $invoice_id = $this->input->get("invoiceId");
        $timestamp = $this->input->get("timestamp");
        $api_key = DIMO_API_KEY;
        $data = '<' . $invoice_id . '*' . $api_key . '*' . $timestamp . '>';
        $computed_seal = hash_hmac("sha1", $data, pack("H*", $api_key));

        if ($seal == $computed_seal) {
            $status = true;
        } else {
            $status = false;
        }
        return $status;
    }

    private function callback_ip_address($remote_addr) {
        if (in_array($remote_addr, json_decode(DIMO_IP_ADDRESS))) {
            return PAYMENT_TYPE_QR_CODE;
        } elseif (in_array($remote_addr, json_decode(GSP_BCA_IP_ADDRESS))) {
            return PAYMENT_TYPE_BCA_KLIKPAY;
        } else {
            return "";
        }
    }

    private function process_confirm_qr_code($status_callback_invoice) {
        if ($status_callback_invoice) {
            $confirm_invoice = $this->confirm_invoice_qr_code();
            return $this->process_data_invoice_qr_code($confirm_invoice);
        }
    }

    private function confirm_invoice_qr_code() {
        $api_key = DIMO_API_KEY;
        $pin_code = DIMO_PIN_CODE;
        $invoice_id = $this->input->get("invoiceId");

        $params = 'apiKey=' . $api_key . '&pinCode=' . $pin_code . '&invoiceId=' . $invoice_id;

        $url = URL_DIMO_API . 'flashiz_api_in_idr/banking/v2/merchant/confirmInvoice';
        $this->curl_dimo->set_url($url);
        $this->curl_dimo->setIsUsingUserAgent(true);
        $this->curl_dimo->setParams($params);
        $this->curl_dimo->setData();
        $result = $this->curl_dimo->getResponse(); //{"result":"success","content":{"tipAmount":0,"time":"13:12:44","merchantInformation":"168;UANGKU SMARTFREN;LA7u0ubrAbck;948240615076;;0;","date":"18-10-2016"}}
        $this->save_payment_log($params, $result);
        return $result;
    }

    private function process_data_invoice_qr_code($confirm_invoice) {
        $data_inf = array();
        $data = json_decode($confirm_invoice);
        $result = $data->result;
        if ($result == "success") {
            $content = $data->content;
            $merchant_information = $content->merchantInformation;
            $m_inf = explode(';', $merchant_information);

            $data_inf['bank_code'] = isset($m_inf[0]) ? $m_inf[0] : '';
            $data_inf['bank_name'] = isset($m_inf[1]) ? $m_inf[1] : '';
            $data_inf['dimo_invoice_id'] = isset($m_inf[2]) ? $m_inf[2] : '';
            $data_inf['bank_reference_id'] = isset($m_inf[3]) ? $m_inf[3] : '';
            $data_inf['loyalty_name'] = isset($m_inf[4]) ? $m_inf[4] : '';
            $data_inf['discount_amount'] = isset($m_inf[5]) ? $m_inf[5] : '';
            $data_inf['loyalty_type'] = isset($m_inf[6]) ? $m_inf[6] : '';
            $data_inf['point_redeemed'] = isset($m_inf[7]) ? $m_inf[7] : '';
            return array('data_status' => $result, 'data_inf' => $data_inf, 'data_source' => $confirm_invoice);
            //save bank_code, bank_name, dimo_invoice_id, bank_reference_id, loyalty_name, discount_amount, loyalty_type, point_redeemed to recon
        } else {
            return array('data_status' => $result, 'data_inf' => $data_inf, 'data_source' => $confirm_invoice);
        }
    }

    private function save_payment_log($request, $response) {
        $params = new stdClass();
        $params->user_id = "SYSTEM";
        $params->ip_address = parent::get_ip_address();
        $params->order_no = $this->data[DATA_SELECTED][LIST_DATA][0][0]->order_no;
        $params->order_seq = $this->data[DATA_SELECTED][LIST_DATA][1][0]->order_seq;
        $params->payment_method_seq = $this->data[DATA_SELECTED][LIST_DATA][0][0]->pg_method_seq;
        $params->request = $request;
        $params->request_date = date('Y-m-d H:i:s');
        $params->response = $response;
        $params->response_date = date('Y-m-d H:i:s');
        $params->signature = $this->data[DATA_SELECTED][LIST_DATA][0][0]->signature;
        $this->M_payment_member->save_payment_log($params);
    }

    public function proccess_payment_bca_klikpay($data) {
        $this->set_payment_flag($data);
    }

    private function set_payment_flag($data) {
        $remote_addr = $_SERVER['REMOTE_ADDR'];
        $payment_type = $this->callback_ip_address($remote_addr);
        if ($payment_type == PAYMENT_TYPE_BCA_KLIKPAY) {
            $_additionalData = parent::get_input_post('additionalData');
            $status = '00';
            $reasonIndonesian = 'Sukses';
            $reasonEnglish = 'Success';

            $keyId = $this->keygenerator->genKeyId(BCA_CLEARKEY);

            if ($data->payment_status == PAYMENT_PAID_STATUS_CODE) {
                $status = '01';
                $reasonIndonesian = 'Transaksi Anda telah dibayar.';
                $reasonEnglish = 'Your transaction has been paid.';
                //$this->save_success_order_payment($data);
            }

            if (parent::get_input_post('totalAmount') != $data->total_payment) {
                //Validation if amount doesn't same
                $status = '01';
                $reasonIndonesian = 'Transaksi Anda tidak dapat diproses.';
                $reasonEnglish = 'Your transaction cannot be processed.';
                $this->save_failed_order_payment($data);
            }

            if (!empty(parent::get_input_post('acFull')) || !empty(parent::get_input_post('acInst'))) {
                
            }

            $authKey = $this->keygenerator->genAuthKey(parent::get_input_post('klikpaycode'), $data->order_no, parent::get_input_post('currency'), date('d/m/Y H:i:s', strtotime($data->created_date)), $keyId);
            if ($authKey != parent::get_input_post('authKey')) {
                $status = '01';
                $reasonIndonesian = 'Transaksi Anda tidak dapat diproses.';
                $reasonEnglish = 'Your transaction cannot be processed.';
                $this->save_failed_order_payment($data);
            }

            if ($status == '00') {
                $this->save_success_order_payment($data);
            }
            $resp = $status . ';' . $reasonIndonesian . ';' . $reasonEnglish . ';' . $_additionalData;
            $this->data[DATA_PAYMENT] = $resp;
            echo $resp;
        }
    }

    public function sendNotification($data, $status_payment) {
        // Set POST variables
        $url = SEND_NOTIF_URL;
        $headers = array(
            "Authorization: key=" . (isset($data->member_seq) || $data->member_seq == 0 ? SEND_NOTIF_MEMBER_KEY : SEND_NOTIF_AGENT_KEY),
            "Content-Type: application/json"
        );

        // TODO get user gcm
        $users = array($data->gcm_id);

        // Isi notifikasi, isinya bebas yg penting array. bisa juga array dalam array
        $content = array(
            'type' => 1,
            'title' => "Pembayaran " . $data->order_no,
            'description' => "Status Pembayaran  : " . $status_payment
        );
        // DO PUSH NOTIFICATION !!!
        $fields = array(
            'registration_ids' => $users,
            'data' => $content
        );

        // Open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);

        // Close connection
        curl_close($ch);
    }

}
?>

