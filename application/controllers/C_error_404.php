<?php

require_once CONTROLLER_BASE_HOME;

class C_error_404 extends controller_base_home {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("", "home/error_404", false);
        
        //$this->load->model("system/m_login");
//        $this->load->helper('url');
    }

    public function index() {
        $this->load->helper('cookie');
        $this->data['tree_category'] = $this->get_tree_category(true);
		$this->data['is_detail'] = 'true';
		$this->output->set_status_header('404');
        $this->template->view("errors/html/error_404_front_end", $this->data);
    }

}

?>