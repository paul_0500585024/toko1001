<?php

require_once CONTROLLER_BASE_MERCHANT;

class C_product_stock_merchant extends controller_base_merchant {

    private $data;

    public function __construct() {

        $this->data = parent::__construct("MMST01003", "merchant/master/product_stock_merchant");
        $this->initialize();
    }

    private function initialize() {

        $this->load->model('merchant/master/M_product_stock_merchant');
        $this->load->model('merchant/transaction/M_trans_log_merchant');

        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_SAVE_DELETE, "save_delete");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {

        if (!$this->input->post()) {
            $this->data[FILTER] = 'merchant/master/product_stock_merchant_f.php';
            $this->load->view("merchant/master/product_stock_merchant", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("merchant/master/product_stock_merchant", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("merchant/master/product_stock_merchant", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $merchant_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($merchant_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    public function search() {

        $this->data;

        $filter = new stdClass;
        $filter->user_id = parent::get_merchant_email();
        $filter->merchant_seq = parent::get_merchant_user_seq();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->product_name = parent::get_input_post("product_name");
        $filter->value = parent::get_input_post("value");
        $filter->merchant_sku = parent::get_input_post("merchant_sku");

        try {
            $list_data = $this->M_product_stock_merchant->get_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );

        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                $row = array("DT_RowId" => $data_row->product_variant_seq . '/' . DEFAULT_VALUE_VARIANT_SEQ,
                    "product_name" => parent::cdef($data_row->product_name),
                    "value" => parent::cdef($data_row->value),
                    "merchant_sku" => parent::cdef($data_row->merchant_sku)
                );
                $output['aaData'][] = $row;
            }
        };

        echo json_encode($output);
    }

    protected function get_edit() {

        $selected = new stdClass();
        $key = explode("/", parent::get_input_post("key"));
        $selected->user_id = parent::get_merchant_user_seq();
        $selected->ip_address = parent::get_ip_address();
        $selected->product_variant_seq = $key[0];
        $selected->variant_value_seq = $key[1];

        try {
            $sel_data = $this->M_product_stock_merchant->get_data($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_update() {

        $size = $this->input->post('size');
        $merchant_sku = $this->input->post('merchant_sku');
        $stock = $this->input->post('stock');
        $pending_payment = $this->input->post('pending_payment');
        $pending_delivery = $this->input->post('pending_delivery');
        $stock_add = $this->input->post('stock_add');
        $value_seq = $this->input->post('value_seq');

        foreach ($value_seq as $key => $n) {
            $filter = new stdClass();
            $filter->user_id = parent::get_merchant_user_seq();
            $filter->ip_address = parent::get_ip_address();
            $filter->product_variant_seq = parent::get_input_post("product_variant_seq");
            $filter->product_name = parent::get_input_post("product_name");
            $filter->variant_value_seq = $n[$key];
            $filter->value = parent::get_input_post("value");

            $this->data[DATA_SELECTED][LIST_DATA][0][] = $filter;

            $filter->value_seq = $n;
            $filter->size = $size[$key];
            $filter->merchant_sku = $merchant_sku[$key];
            $filter->stock = $stock[$key];
            $filter->pending_payment = $pending_payment[$key];
            $filter->pending_delivery = $pending_delivery[$key];
            $filter->trx_no = "";
            $filter->trx_type = STOCK_ADJUSTMENT_TYPE;

            parent::validate_form(STOCK_VALIDATOR, $stock_add[$key], 'Tambah Stok', $this->data);
            $filter->stock_add = $stock_add[$key];

            $this->data[DATA_SELECTED][LIST_DATA][1][] = $filter;

//            if ($filter->stock_add > 0 && $filter->merchant_sku == "") {
//                $this->data[DATA_ERROR][ERROR] = true;
//                $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_REQUIRED_MERCHANT_SKU;
//            } else {
            if ($this->data[DATA_ERROR][ERROR] === false) {
                try {
                    $this->M_product_stock_merchant->trans_begin();
                    $this->M_product_stock_merchant->save_update($filter);
                    $this->get_mutation_type($filter);
                    $this->M_product_stock_merchant->trans_commit();
                } catch (BusisnessException $ex) {
                    parent::set_error($this->data, $ex);
                    $this->M_product_stock_merchant->trans_rollback();
                } catch (TechnicalException $ex) {
                    parent::set_error($this->data, $ex);
                    $this->M_product_stock_merchant->trans_rollback();
                } catch (Exception $ex) {
                    parent::set_error($this->data, $ex);
                    $this->M_product_stock_merchant->trans_rollback();
                }
            }
//            }
        }
    }

    protected function get_mutation_type($filter) {

        $difference_stock = ($filter->stock_add - $filter->stock);

        Switch ($difference_stock) {
            case $difference_stock < 0 :
                $mutation_type = OUT_MUTATION_TYPE;
                break;
            case $difference_stock > 0 : {
                    $mutation_type = IN_MUTATION_TYPE;
                }
                break;
        }

        $params = new stdClass();
        $params->user_id = $filter->user_id;
        $params->ip_address = $filter->ip_address;
        $params->product_variant_seq = $filter->product_variant_seq;
        $params->variant_value_seq = $filter->variant_value_seq;
        $params->trx_no = $filter->trx_no;
        $params->trx_type = $filter->trx_type;
//        $params->value_seq = $filter->value_seq;
//        $params->stock = $filter->stock;
//        $params->stock_add = $filter->stock_add;
        $params->qty = ($difference_stock < 0 ? - $difference_stock : $difference_stock);
        $params->mutation_type = $mutation_type;

        try {
            $this->M_trans_log_merchant->save_merchant_stock_log($params);
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_substract_stock($data_stock, &$data) {

        $params[] = new stdClass();
        $params->user_id = $data_stock->user_id;
        $params->ip_address = $data_stock->ip_address;
        $params->product_variant_seq = $data_stock->product_variant_seq;
        $params->value_variant_seq = $data_stock->value_variant_seq;
        $params->qty = $data_stock->qty;
        $params->mutation_type = OUT_MUTATION_TYPE;

        try {
            $stock = $this->M_product_stock_merchant->get_latest_product_stock($params);

            if ($stock[0]->qty < $params->qty) {
                throw new BusisnessException(ERROR_PRODUCT_STOCK_QTY_EXCEEDED);
            }

            $this->M_product_stock_merchant->trans_begin();
            $this->M_product_stock_merchant->save_subtract_stock($params);
            $this->M_trans_log_merchant->save_merchant_stock_log($params);
            $this->M_product_stock_merchant->trans_commit();
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_product_stock_merchant->trans_rollback();
        }
    }

}

?>