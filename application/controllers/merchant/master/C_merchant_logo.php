<?php

require_once CONTROLLER_BASE_MERCHANT;

class C_merchant_logo extends controller_base_merchant {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("MCON01002", "merchant/master/logo_banner_merchant");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model("merchant/master/M_logo_merchant");

        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {

        if (!$this->input->post()) {
            $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
            $this->get_edit();
            $this->load->view("merchant/master/merchant_logo", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("merchant/master/merchant_logo", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("merchant/master/merchant_logo", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    protected function get_edit() {
        $selected = new stdClass();
        $selected->user_id = parent::get_merchant_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->key = parent::get_merchant_user_seq();

        try {
            $sel_data = $this->M_logo_merchant->get_data($selected);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_update() {

        $merchant_name = parent::get_merchant_user_name();
        $string = preg_replace("/[^A-Za-z0-9 ]/", '', $merchant_name);
        $merchant_name = str_ireplace(" ", "_", $string);

        $banner = parent::upload_file("banner_img", MERCHANT_LOGO . parent::get_merchant_user_seq() . "/", "b-" . $merchant_name, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_BANNER, "Banner ");
        $logo = parent::upload_file("logo_img", MERCHANT_LOGO . parent::get_merchant_user_seq() . "/", "l-" . $merchant_name, IMAGE_TYPE, $this->data, IMAGE_DIMENSION_LOGO, "Logo ");

        $params = new stdClass();
        $params->userid = parent::get_merchant_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->old_banner_img = parent::get_input_post("old_banner");
        $params->old_logo_img = parent::get_input_post("old_logo");
        $params->seq = parent::get_merchant_user_seq();
        $params->logo_img = $logo->error == true ? $params->old_logo_img : $logo->file_name;
        $params->banner_img = $banner->error == true ? $params->old_banner_img : $banner->file_name;
        $params->description = parent::get_input_post("description");

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;

        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_logo_merchant->trans_begin();
                $this->M_logo_merchant->save_update($params);
                $this->M_logo_merchant->trans_commit();
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_logo_merchant->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_logo_merchant->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_logo_merchant->trans_rollback();
            }
        }
    }

}

?>