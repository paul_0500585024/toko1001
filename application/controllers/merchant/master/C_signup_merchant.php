<?php

require_once CONTROLLER_BASE_MERCHANT;

class C_signup_merchant extends controller_base_merchant {

    public function __construct() {
        parent::__construct("", "", false);
        $this->initialize();
    }

    private function initialize() {
        $this->load->model("merchant/master/M_signup_merchant");
    }

    private function _create_captcha() {
        $this->load->helper('captcha');

        $options = array(
            'img_path' => TEMP_CAPTCHA,
            'img_url' => CDN_IMAGE . TEMP_CAPTCHA,
            'img_width' => '188',
            'img_height' => '65',
            'expiration' => 7200,
            'word_length' => '5',
            'font_path' => './system/fonts/ARIALNB.TTF',
            'pool' => CAPTCHA_POOL);

        $cap = create_captcha($options);
        $image = $cap['image'];
        $word = $cap['word'];
        $this->session->set_userdata("captcha", $word);
        $gambar = array($image, $word);
        return $gambar;
    }

    public function index() {
        if ($this->input->post('btnApprove') == 'true') {
            $email = parent::get_input_post('email');
            if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
                echo "false";
                die();
            }
            echo $this->check_user($email);
            die();
        }
    }

    public function check_user($idh) {
        if ($idh != '') {
            $list_data = $this->M_signup_merchant->check_merchant_exist($idh);
            if (isset($list_data[0])) {
                return 'false';
            } else {
                return 'true';
            }
        } else {
            
        }
    }

    public function get_cap() {
        $data['gambar'] = $this->_create_captcha();
        echo ($data['gambar'][0]);
        die();
    }

    public function cekcaptcha($code_captcha_merchant, $code) {
        if ($code_captcha_merchant == $code) {
            return "true";
        } else {
            return "false";
        }
    }

    public function get_check() {
        if ($this->input->post()) {
            $msg = "";

            $email = parent::get_input_post('email_merchant');
            if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
                $msg = ERROR_VALIDATION_FILL_EMAIL_FORMAT;
            }
            if ($email == "") {
                $msg = ERROR_VALIDATION_MUST_FILL_EMAIL;
            }
            $inmails = $this->check_user($email);
            if ($inmails == "false") {
                $msg = ERROR_EMAIL_CHECK;
            }
            $captcha = $this->session->userdata('captcha');
            $incaptcha = $this->cekcaptcha(parent::get_input_post("captcha_merchant"), $captcha);
            if ($incaptcha == "false") {
                $msg = ERROR_CAPTCHA_CHECK;
            }
            if (strlen(parent::get_input_post('email_merchant')) > MAX_EMAIL_LENGTH) {
                $msg = "Email" . " " . ERROR_VALIDATION_LENGTH_MAX . MAX_EMAIL_LENGTH;
            }
            if (strlen(parent::get_input_post('merchant_name')) > MAX_NAME_LENGTH) {
                $msg = "Nama Merchant" . " " . ERROR_VALIDATION_LENGTH_MAX . MAX_NAME_LENGTH;
            }
            if ($msg == "") {
                $msg = "true";
                $this->register_merchant();
            }
            die(json_encode(array($msg, $this->_create_captcha())));
        } else {
            die("Error");
        }
    }

    public function register_merchant() {
        $params = new stdClass();
        $params->user_id = '';
        $params->ip_address = parent::get_ip_address();
        $params->email = parent::get_input_post("email_merchant");
        $params->name = parent::get_input_post("merchant_name");
        $params->phone = parent::get_input_post("phone_merchant");
        $params->address = parent::get_input_post("merchant_address");
        $params->captcha = parent::get_input_post("captcha_merchant");

        $params->code = MERCHANT_REG_CODE;
        $params->RECIPIENT_NAME = $params->name;
        $params->to_email = $params->email;

        $this->data[DATA_SELECTED][LIST_DATA][] = $params;
        try {
            $this->M_signup_merchant->trans_begin();
            $this->M_signup_merchant->save_add($params);
            parent::email_template($params);

            $params->code = MERCHANT_REG_ADMIN_CODE;
            $params->RECIPIENT_NAME = ADMIN_TOKO1001_NAME;
            $params->MERCHANT_NAME = $params->name;
            $params->to_email = ADMIN_TOKO1001_EMAIL;
            parent::email_template($params);
            $this->M_signup_merchant->trans_commit();
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_signup_merchant->trans_rollback();
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_signup_merchant->trans_rollback();
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_signup_merchant->trans_rollback();
        }
    }

}
