<?php

require_once CONTROLLER_BASE_MERCHANT;

class C_order_return_change_status_merchant extends controller_base_merchant {

    private $data;
    private $return_no;

    public function __construct() {
        $this->data = parent::__construct("MTRX01008", "merchant/transaction/return_merchant/change_status");
        $this->initialize();
    }

    private function initialize() {
        $this->return_no = $this->uri->segment(5);
        $this->load->model('merchant/transaction/M_order_return_merchant');
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        $this->get_data_header();
        if (!$this->input->post()) {
            $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->return_no;
            $this->load->view("merchant/transaction/order_return_change_status_merchant", $this->data);
        } else {
            if ($this->data[DATA_SUCCESS][SUCCESS] == true) {
                $this->session->set_tempdata(DATA_MESSAGE, true);
                redirect(base_url("merchant/transaction/return_merchant"));
            } else if ($this->data[DATA_ERROR][ERROR] == true) {
                $this->data[DATA_ERROR][ERROR] = true;
                $merchant_info[SESSION_DATA] = $this->data;
                $this->session->set_userdata($merchant_info);
                redirect(base_url($this->data[DATA_AUTH][FORM_URL] . "/" . $this->return_no));
            }
        }
    }

    protected function get_data_header() {
        $params = new stdClass;
        $params->merchant_seq = parent::get_merchant_user_seq();
        $params->ip_address = parent::get_ip_address();
        $params->return_no = $this->return_no;
        try {
            $sel_data = $this->M_order_return_merchant->get_data_detail($params);
            if ($sel_data[0]->merchant_seq != $params->merchant_seq) {
                redirect(base_url("merchant/transaction/return_merchant"));
            } else {
                if (isset($sel_data)) {
                    parent::set_data($this->data, $sel_data);
                } else {
                    $this->load->view("errors/html/error_404");
                }
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_update() {
        $selected = new stdClass();
        $selected->merchant_id = parent::get_merchant_user_id();
        $selected->user_id = parent::get_merchant_user_id();
        $selected->merchant_seq = parent::get_merchant_user_seq();
        $selected->ip_address = parent::get_ip_address();
        $selected->return_no = parent::get_input_post("return_no");
        $selected->return_status = parent::get_input_post("return_status");
        $selected->merchant_comment = parent::get_input_post("merchant_comment");
        $this->data[DATA_SELECTED][LIST_DATA][] = $selected;
        $sel_data = $this->M_order_return_merchant->get_data_detail($selected);
        try {
            if ($sel_data[0]->return_status != NEW_STATUS_CODE) {
                throw new Exception(ERROR_UPDATE);
            } else {
                $this->M_order_return_merchant->trans_begin();
                if ($selected->return_status == REFUND_BY_MERCHANT) {
                    $data_return = $this->M_order_return_merchant->get_data_return_to_refund($selected);
                    $selected->member_seq = $data_return[0]->member_seq;
                    $selected->pg_method_seq = $data_return[0]->pg_method_seq;
                    $selected->trx_date = substr($data_return[0]->trx_date, 0, 10);
                    $selected->deposit_trx_amt = $data_return[0]->deposit_trx_amt;
                    $selected->mutation_type = CREDIT_MUTATION_TYPE;
                    $selected->trx_type = RETUR_ACCOUNT_TYPE;
                    $selected->new_status = NEW_STATUS_CODE;
                    $this->M_order_return_merchant->return_update_refund($selected);
                    $selected->code = ORDER_RETURN_MERCHANT_REFUND;
                    $selected->to_email = $sel_data[0]->member_email;
//                    $selected->to_email = "toriq.354@gmail.com";
                    $selected->RECIPIENT_NAME = $sel_data[0]->member_name;
                    $selected->MODIFIED_DATE = parent::cdate($sel_data[0]->modified_date);
                    $selected->TRX_NO = $selected->return_no;
                    $selected->TOTAL = $sel_data[0]->product_sell_price * $sel_data[0]->qty;
                    $selected->ACCOUNT = base_url() . MEMBER_ORDER_RETURN;
                    $sent_email = $this->email_template($selected);
                }                
                $this->M_order_return_merchant->return_update_change_status($selected);
                $this->M_order_return_merchant->trans_commit();
                $this->data[DATA_SUCCESS][SUCCESS] = true;
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_order_return_merchant->trans_rollback();
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_order_return_merchant->trans_rollback();
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_order_return_merchant->trans_rollback();
        }
    }

}

?>