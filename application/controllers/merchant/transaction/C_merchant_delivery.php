<?php

require_once CONTROLLER_BASE_MERCHANT;

class C_merchant_delivery extends controller_base_merchant {

    private $data;
    private $data_setting;
    private $data_ekspedisi;
    private $voucher;
    private $coupons = 0;

    public function __construct() {
        $this->data = parent::__construct("MTRX01003", "merchant/transaction/merchant_delivery");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('merchant/transaction/M_merchant_delivery');
        $this->load->library('curl');
        $this->load->model('component/M_web_service');
        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        parent::register_event($this->data, ACTION_ADDITIONAL, "get_ajax");
        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        if (!$this->input->post()) {
            $this->data[FILTER] = 'merchant/transaction/merchant_delivery_f.php';
            $this->load->view("merchant/transaction/merchant_delivery", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("merchant/transaction/merchant_delivery", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("merchant/transaction/merchant_delivery", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_ADDITIONAL) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADDITIONAL;
                        $this->load->view("merchant/transaction/merchant_delivery", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $admin_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($admin_info);
                    redirect(base_url("merchant/transaction/merchant_delivery"));
                }
            }
        }
    }

    protected function get_ajax() {

        $tipe = parent::get_input_post("tipe");
        $idh = parent::get_input_post("idh");
        $filter = new stdClass;
        $filter->user_id = parent::get_merchant_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->merchant_seq = parent::get_merchant_user_seq();
        if (parent::get_input_post("btnCancel") != "") {
            redirect(base_url("merchant/transaction/merchant_delivery"));
        }
        switch ($tipe) {
            case "prosesresi":
                die("ok");
                break;
            case "bukti_kirim":
                $filter->seq = parent::get_input_post("orderid");
                $filter->tipe = 'bukti_kirim';
                $sel_order = $this->M_merchant_delivery->get_data($filter);

                $this->data[EXPEDITION_NAME] = $this->M_merchant_delivery->get_dropdown_expedition();
                parent::set_list_data($this->data, $sel_order);
                $this->data[DATA_SELECTED][LIST_DATA][] = $filter;
                if (isset($sel_order[0][0])) {
                    $filter->dseq = $sel_order[0][0]->receiver_district_seq;
                    $filter->cseq = "";
                    $filter->pseq = "";
                }
                $sel_data1 = $this->M_merchant_delivery->get_location($filter);
                $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data1;
                break;
            case "save_resi":
                $filter->seq = parent::get_input_post("seq");
                $sel_data = $this->M_merchant_delivery->get_data($filter);
                if (!isset($sel_data) && ($sel_data[0][0]->order_status != READY_STATUS_CODE || $sel_data[0][0]->order_status != PROCESS_STATUS_CODE)) {
                    $this->data[DATA_ERROR][ERROR] = true;
                    $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_UPDATE;
                }
                $filter->ship_by_exp_seq = parent::get_input_post("ship_by_exp_seq", true, FILL_VALIDATOR, "Ekspedisi", $this->data);
                if (parent::get_input_post("ship_by_exp_seq") == 0) {
                    $filter->ship_by = parent::get_input_post("ship_by");
                } else {
                    $filter->ship_by = "";
                }
                $filter->awb_no = parent::get_input_post("awb_no", true, FILL_VALIDATOR, "No resi ", $this->data);
                $filter->ship_date = parent::get_input_post("ship_date", true, FILL_VALIDATOR, "Tanggal Resi", $this->data);
                if (is_uploaded_file($_FILES["ifile"][TEMP_NAME])) {
                    $logo_img = parent::upload_file("ifile", RESI_UPLOAD_IMAGE, parent::get_input_post("seq") . "_" . $filter->merchant_seq . "_" . parent::get_input_post("awb_no"), IMAGE_TYPE, $this->data, IMAGE_DIMENSION_TRANSFER, "Bukti Resi");
                    if (isset($logo_img->error)) {
                        if ($logo_img->error === true) {
                            $filter->ship_note_file = parent::get_input_post("ofile");
                        } else {
                            $filter->ship_note_file = $logo_img->file_name;
                        }
                    }
                } else {
                    $filter->ship_note_file = parent::get_input_post("ofile");
                }
                $filter->tipe = 'bukti_kirim';
                if ($this->data[DATA_ERROR][ERROR] === false) {
                    try {
                        $this->M_merchant_delivery->trans_begin();
                        $sel_data = $this->M_merchant_delivery->save_resi($filter);
                        $this->M_merchant_delivery->trans_commit();
                    } catch (BusisnessException $ex) {
                        parent::set_error($this->data, $ex);
                        $this->M_merchant_delivery->trans_rollback();
                    } catch (TechnicalException $ex) {
                        parent::set_error($this->data, $ex);
                        $this->M_merchant_delivery->trans_rollback();
                    } catch (Exception $ex) {
                        parent::set_error($this->data, $ex);
                        $this->M_merchant_delivery->trans_rollback();
                    }
                    redirect(base_url("merchant/transaction/merchant_delivery"));
                } else {
                    $selected = new stdClass();
                    $selected->seq = parent::get_input_post("seq");
                    $selected->user_id = parent::get_merchant_user_id();
                    $selected->ip_address = parent::get_ip_address();
                    $selected->merchant_seq = parent::get_merchant_user_seq();
                    $selected->tipe = 'bukti_kirim';
                    $sel_order = $this->M_merchant_delivery->get_data($filter);
                    $this->data[EXPEDITION_NAME] = $this->M_merchant_delivery->get_dropdown_expedition();
                    parent::set_list_data($this->data, $sel_order);
                    $this->data[DATA_SELECTED][LIST_DATA][] = $selected;
                    if (isset($sel_order[0][0])) {
                        $selected->dseq = $sel_order[0][0]->receiver_district_seq;
                        $selected->cseq = "";
                        $selected->pseq = "";
                    }
                    $sel_data1 = $this->M_merchant_delivery->get_location($selected);
                    $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data1;
                    $this->data[DATA_LIST][LIST_RECORD][0]->ship_by_exp_seq = $filter->ship_by_exp_seq;
                    $this->data[DATA_LIST][LIST_RECORD][0]->awb_no = $filter->awb_no;
                    $this->data[DATA_LIST][LIST_RECORD][0]->ship_date = $filter->ship_date;
                }
                break;
            case "docprint":
                $dataprint = '';
                $selected = new stdClass();
                $selected->user_id = parent::get_merchant_user_id();
                $selected->ip_address = parent::get_ip_address();
                $selected->settingseq = 5;
                $sel_setting = $this->M_merchant_delivery->get_setting($selected);
                $this->data_setting['gambar'] = $sel_setting[0]->value;
                $selected->merchant_seq = parent::get_merchant_user_seq();
                $format_type = parent::get_input_post("format_type");
                $dataprint = '<head>'
                        . '<title> Toko1001</title><link href="' . base_url() . '/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" /></head>';
                foreach ($this->input->post("printc") as $idorder) {
                    $selected->seq = $idorder;
                    $data_order_merchant = $this->M_merchant_delivery->get_data_order_merchant($selected);
                    if ($data_order_merchant[0]->order_status == 'X' || $data_order_merchant[0]->order_status == "P")
                        continue;
                    if ($data_order_merchant[0]->order_status == 'N') {
                        try {
                            $this->M_merchant_delivery->trans_begin();
                            $this->cek_batal_item($idorder);    // cek cancel product item
                            $this->get_awb($idorder); // ambil resi untuk yang kirim via rekanan
                            $this->update_status_print($idorder);   // update status print
                            $this->sendemail_confirm($idorder);   // kirim email konfirmasi pengiriman ke member
                            $this->M_merchant_delivery->trans_commit();
                        } catch (Exception $ex) {
                            parent::set_error($this->data, $ex);
                            $this->M_merchant_delivery->trans_rollback();
                        }
                    }

                    $sel_order = $this->M_merchant_delivery->get_data($selected);
                    $sel_product = $this->M_merchant_delivery->get_data_product($selected, 'R');
                    $sel_ekpedisi = $this->M_merchant_delivery->get_expedition($selected);
                    parent::set_list_data($this->data, $sel_order);
                    $this->data[DATA_SELECTED][LIST_DATA][] = $sel_product;
                    $this->data[DATA_SELECTED][LIST_DATA][] = $sel_ekpedisi;
                    switch ($format_type) {
                        case "A1":
                            $dataprint .= '
			    <table width="100%">
				<tr height="555px">
				     <td colspan=2 valign="top">' . $this->printinvoice($this->data) . '</td>
				</tr>
				<tr>
				     <td valign="top">' . $this->printresi($this->data) . '</td>
				     <td valign=top>' . $this->printretur($this->data) . '</td>
				</tr>
			    </table><br style="page-break-after: always;"/>';
                            break;
                        case "A2":
                            $dataprint .= $this->printinvoice($this->data) . '<br style="page-break-after: always;"/>';
                            $dataprint .='
			    <table>
				<tr>
				     <td>' . $this->printresi($this->data) . '</td>
				     <td valign=top>' . $this->printretur($this->data) . '</td>
				</tr>
			    </table><br style="page-break-after: always;"/>';
                            break;
                        case "A3":
                            $dataprint.=$this->printinvoice($this->data) . '<br style="page-break-after: always;"/>';
                            break;
                        case "A4":
                            $dataprint.=$this->printresi($this->data) . '<br style="page-break-after: always;"/>';
                            break;
                    }
                    unset($this->data[DATA_SELECTED][LIST_DATA]);
                }
                if ($dataprint == '')
                    $dataprint = 'Tidak Ada data';
                die($dataprint);
                break;
        }
    }

    public function update_status_print($orderseq) {
        $selected = new stdClass();
        $selected->user_id = parent::get_merchant_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->seq = $orderseq;
        $selected->merchant_seq = parent::get_merchant_user_seq();
        $this->M_merchant_delivery->save_update_order_merchant_print($selected); // update status print t_order_merchant
    }

    public function printinvoice($datalist) {
        $dataprint = '';
        $total = 0;
        $dataprint = '
	<div style="font-family:tahoma;font-size:12px;margin: 0;padding: 10px;">
	    <div style="margin-bottom: 20px;"><img src="' . base_url() . ASSET_IMG_HOME . $this->data_setting['gambar'] . '" style="width:150px"><span style="font-weight: bold;font-size: 30px;color: #178CBA;margin-left: 20px;">INVOICE</span></div>
	    <ul style="line-height:20px;">
	    <li>' . $datalist['data_list']['list_rec'][0]->receiver_name . '</li>
	    <li>' . $datalist['data_list']['list_rec'][0]->receiver_phone_no . '</li>
	    <li>' . $datalist['data_list']['list_rec'][0]->receiver_address . '</li>
	    <li style="font-weight: bold;">No Order : ' . $datalist['data_list']['list_rec'][0]->order_no . '</li>
	    </ul>
	    <div style="font-weight: bold;margin: 20px 0px;text-align: center;font-size: 18px">
		Daftar Produk
	    </div>
	    <div>
		<table cellspacing=0 cellpadding=6 style="width: 90%;" border="1">
		    <tbody><tr>
			<th>Nama Produk</th>
			<th>Qty</th>
			<th>Harga Rp. </th>
			<th>Total Rp. </th>
		    </tr>';

        if (isset($datalist['data_sel']['list_data'][0])) {
            foreach ($datalist['data_sel']['list_data'][0] as $product) {
                if ($product->variant_value_seq != DEFAULT_VALUE_VARIANT_SEQ) {
                    $variant = " - " . $product->variant_name;
                } else {
                    $variant = '';
                }
                $dataprint .='<tr><td>' . $product->name . $variant . ' </td><td align=right>' . $product->qty . '</td><td align=right>' . number_format($product->sell_price) . '</td><td align=right>' . number_format($product->sell_price * $product->qty) . '</td></tr>';
                $total = $total + ($product->sell_price * $product->qty);
            }
//	    $total = $datalist['data_list']['list_rec'][0]->total_merchant; ,	batal 10/12/2015
        } else {
            $total = 0;
        }
        $dataprint .=
                ' </tbody></table>
	    </div>
	    <ul>
	    <li>Total : Rp. ' . number_format($total) . '</li>
	    </ul>
	</div>
	';
        return $dataprint;
    }

    public function printresi($datalist) {
        $tanpaexp = $datalist['data_list']['list_rec'][0]->real_expedition_service_seq;
        $kotaasal = '';
        $alamatasal = '';
        $dataprint = '';
        $listproduk = '';
        $totalship = 0;
        $datakirim = $this->M_merchant_delivery->get_location_dtl($datalist['data_sel']['list_data'][1][0]->pickup_district_seq);
        if (isset($datakirim)) {
            $alamatasal = $datalist['data_sel']['list_data'][1][0]->pickup_addr . ', ' . $datalist['data_sel']['list_data'][1][0]->pickup_zip_code;
            $kotaasal = $datakirim[0]->named . ', ' . $datakirim[0]->namec . ', ' . $datakirim[0]->namep;
        }
        $dataterima = $this->M_merchant_delivery->get_location_dtl($datalist['data_list']['list_rec'][0]->receiver_district_seq);
        $kotatujuan = $dataterima[0]->named . ', ' . $dataterima[0]->namec . ', ' . $dataterima[0]->namep;
        $img_barcode_awb = $this->create_barcode($datalist['data_list']['list_rec'][0]->awb_no);
        if (isset($datalist['data_sel']['list_data'][0])) {
            foreach ($datalist['data_sel']['list_data'][0] as $product) {
                if ($product->variant_value_seq != DEFAULT_VALUE_VARIANT_SEQ) {
                    $variant = " - " . $product->variant_name;
                } else {
                    $variant = '';
                }
                $listproduk .= '<div>' . $product->qty . ' - ' . $product->name . $variant . '</div>';
                $totalship = $totalship + ($product->ship_price_charged * ceil($product->qty * $product->weight_kg));
            }
        }
        $dataprint = '
	    <div style="width:360px;border: 1px dashed #000;padding: 5px">
		<div>
		    <div style="float:left;margin-bottom: 20px;">
			<img src="' . base_url() . ASSET_IMG_HOME . $this->data_setting ['gambar'] . '" width="100px">
		    </div>
		    <div style="float:right;">
			' . (($tanpaexp == 0) ? '' : '<img src="' . base_url() . EXPEDITION_UPLOAD_IMAGE . $datalist['data_sel']['list_data'][1][0]->expedisilogo . '"  height="40px" border="0">' ) . '
		    </div>
		</div>
		<div style="clear:both"></div>
		' . (($tanpaexp == 0) ? '' :
                        '<div style="margin: 0px auto;margin-bottom: 10px;text-align: center">' .
                        (($datalist['data_list']['list_rec'] [0]->awb_no != '') ? '<img src="data:image/png;base64,' . base64_encode($img_barcode_awb) . '">' : '') .
                        '<br><div style="text-align: center">No Resi : ' . $datalist['data_list']['list_rec'] [0]->awb_no . '</div></div>' );
        $img_barcode_order = $this->create_barcode($datalist['data_list']['list_rec'][0]->order_no);
        $dataprint .=
                '<div style="margin-bottom: 10px;text-align: center">' . (($datalist['data_list']['list_rec'] [0]->order_no != '') ? '<img src="data:image/png;base64,' . base64_encode($img_barcode_order) . '">' : '') .
                '<br>
	<div style = "text-align: center">No Order : ' . $datalist['data_list']['list_rec'] [0]->order_no . '</div>
	</div>
	<div style = "float:left;line-height: 15px;">
	<div>Kota asal : ' . $kotaasal . '</div>
	<div>Kota tujuan :<b>' . $kotatujuan . '</b></div>
	<div style = "clear:both"></div>
	<div style = "float:left;width:60px">Pesan : </div><div style = "float:left;">' . $datalist['data_list']['list_rec'] [0]->ship_notes . '</div>
	<div style = "clear:both"></div>
	</div>
	<div style = "clear:both"></div>
	<div>
	<div style = "line-height:15px;">
	<div style = "font-weight:bold;margin-top:5px;border-bottom: 1px solid #555;">Pengirim</div>
	<div style = "float:left;"><i class="fa fa-fw fa-user"></i> ' . $datalist['data_sel']['list_data'][1][0]->merchantname . ' (<i class="fa fa-fw fa-phone"></i> ' . $datalist['data_sel']['list_data'][1] [0]->phone_no . ')</div>
	<div style = "clear:both"></div>
	</div>
	<div style = "line-height:15px;">
	<div style = "font-weight:bold;margin-top:5px;border-bottom: 1px solid #555;">Penerima</div>
	<div style = "float:left;"><i class="fa fa-fw fa-user"></i> ' . $datalist['data_list']['list_rec'][0]->receiver_name . ' (<i class="fa fa-fw fa-phone"></i> ' . $datalist['data_list']['list_rec'][0]->receiver_phone_no . ')</div>
	<div style = "clear:both"></div>
	<div style = "float:left;"><i class="fa fa-fw fa-home"></i> ' . $datalist['data_list']['list_rec'][0]->receiver_address . ', ' . $datalist['data_list']['list_rec'][0]->receiver_zip_code . '<br />
	' . $kotatujuan . '</div>
	<div style = "clear:both"></div>
	</div>
	</div>
	<div style = "clear:both"></div><br>
	<div><div style = "border-bottom:1px dashed #000"></div></div>

	<div style = "margin-top:5px;width:372px">
	<div style = "font-weight:bold">Daftar Produk</div>';

        $dataprint.=$listproduk . '</div>
	</div>';
        return $dataprint;
    }

    public function printretur($datalist) {
        $kotaretur = '';
        $alamatretur = '';
        $dataprint = '';

        /*
          if ($datalist['data_sel']['list_data'][1][0]->return_addr_eq == 1) {
          $datakirim = $this->M_merchant_delivery->get_location_dtl($datalist['data_sel']['list_data'][1][0]->district_seq);
          if (isset($datakirim)) {
          $alamatretur = $datalist['data_sel']['list_data'] [1][0]->address . ', ' . $datalist['data_sel']['list_data'][1][0]->zip_code;
          $kotaretur = $datakirim [0]->named . ', ' . $datakirim [0]->namec . ', ' . $datakirim[0]->namep;
          }
          } else {
          $datakirim = $this->M_merchant_delivery->get_location_dtl($datalist['data_sel']['list_data'][1][0]->return_district_seq);
          if (isset($datakirim)) {
          $alamatretur = $datalist['data_sel']['list_data'][1][0]->return_addr . ', ' . $datalist['data_sel']['list_data'][1][0]->return_zip_code;
          $kotaretur = $datakirim [0]->named . ', ' . $datakirim [0]->namec . ', ' . $datakirim[0]->namep;
          }
          }
         *
         */
        $dataterima = $this->M_merchant_delivery->get_location_dtl($datalist['data_list']['list_rec'][0]->receiver_district_seq);
        $kotaasal = $dataterima [0]->named . ', ' . $dataterima [0]->namec . ', ' . $dataterima[0]->namep;
        $dataprint = '<div style = "width:360px;border: 1px dashed #000;padding: 5px">
	    <div>
	    <div style = "text-align: center;margin-bottom: 12px;">
	    <img src = "' . base_url() . ASSET_IMG_HOME . $this->data_setting ['gambar'] . '" style = "width:100px">
	    </div>
	    </div>
	    <div style = "clear:both"></div>
	    <div style = "font-weight: bold;font-size: 14px;margin: 3px 0px;text-align: center">
	    FORM PENGEMBALIAN BARANG
	    </div>
	    <div style = "text-align: center">No Order : ' . $datalist['data_list']['list_rec'] [0]->order_no . '</div>
	    <div style = "line-height:18px;">
	    <div style = "font-weight:bold;margin-bottom:5px;border-bottom: 1px solid #555;">Pengirim</div>
	    <div style = "float:left;"><i class="fa fa-fw fa-user"></i> ' . $datalist['data_list']['list_rec'][0]->receiver_name . ' (<i class="fa fa-fw fa-phone"></i> ' . $datalist['data_list']['list_rec'][0]->receiver_phone_no . ')</div>
	    <div style = "clear:both"></div>
	    <div style = "float:left;"><i class="fa fa-fw fa-home"></i> ' . $datalist['data_list']['list_rec'][0]->receiver_address . ', ' . $datalist['data_list']['list_rec'][0]->receiver_zip_code . '<br />' . $kotaasal . '</div>
	    <div style = "clear:both"></div>
	    </div>
	    <div style = "margin-bottom: 10px;line-height:18px;">
	    <div style = "font-weight:bold;margin:5px 0 5px 0;border-bottom: 1px solid #555;">Penerima</div>
	    	<i class="fa fa-fw fa-user"></i>' . RETUR_OFFICE . ' (<i class="fa fa-fw fa-phone"></i> ' . RETUR_PHONE . ')<br><i class = "fa fa-fw fa-home"></i>' . RETUR_ADDRESS . '
	    </div>
	    <div style = "clear:both"></div>
	    <div style = "margin-top:5px;width:360px;">
	    <div style = "font-weight:bold;padding-bottom: 3px;border-bottom: 1px solid #000">Daftar Produk</div>';
        if (isset($datalist['data_sel']['list_data'][0])) {
            foreach ($datalist['data_sel']['list_data'][0] as $product) {
                if ($product->variant_value_seq != DEFAULT_VALUE_VARIANT_SEQ) {
                    $variant = " - " . $product->variant_name;
                } else {
                    $variant = '';
                }
                $dataprint.='<div style = "border-bottom:1px solid #aaa;font-weight:bold">' . $product->name . $variant . ' / ' . $product->merchant_sku . ' <span style = "font-weight:normal;font-size:10px;padding:15px;">Qty Retur : </span> </div>';
            }
        }
        $dataprint.='
	    </div>
	    <div style = "margin-top: 5px;font-weight: bold;">
	    *Mohon ditempelkan pada luar kemasan pengiriman dan dikirim melalui ekspedisi yang bekerja sama dengan Toko1001
	    </div>
	    </div>';
        return $dataprint;
    }

    public function printstiker($noorder) {
        
    }

    public function search() {

        $filter = new stdClass;
        $filter->user_id = parent::get_merchant_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->merchant_id = parent::get_merchant_user_seq();
        $filter->order_no_f = parent::get_input_post("order_no_f");
        $filter->order_date_s = parent::get_input_post("order_date_s");
        $filter->order_date_e = parent::get_input_post("order_date_e");
        $filter->status_order_f = parent::get_input_post("status_order_f");

        try {
            $list_data = $this->M_merchant_delivery->get_list($filter);
            parent:: set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent:: set_error($this->data, $ex);
        }
        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );

        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {

                if ($data_row->real_expedition_service_seq == "0") {
                    if ($data_row->order_status == READY_STATUS_CODE || $data_row->order_status == PROCESS_STATUS_CODE) {
                        if ($data_row->awb_no == "") {
                            $dataresi = '<input type = "button" value = "Upload Bukti kirim" onclick = "bukti_kirim(' . $data_row->seq . ')" id = "pbukti" name = "pbukti">';
                        } else {
                            $dataresi = '<input type = "button" value = "Revisi ' . $data_row->awb_no . '" onclick = "bukti_kirim(' . $data_row->seq . ')" id = "pbukti" name = "pbukti">';
                        }
                    } else {
                        $dataresi = $data_row->awb_no;
                    }
                } else {
                    if ($data_row->awb_no == "") {
                        $dataresi = ''; //<input type = "button" value = "Proses No. Resi" onclick = "cekresi(' . $data_row->seq . ')" id="pnoresi" name="pnoresi">';
                    } else {
                        $dataresi = $data_row->awb_no;
                    }
                }
                if ($data_row->order_status == "X" || $data_row->order_status == "P") {
                    $chekbox = "";
                    $dataresi = "";
                } else {
                    $chekbox = "<input type='checkbox' id='printc' name='printc[ ] ' value='" . $data_row->seq . "' class='prntorder'>";
                }
                $row = array("DT_RowId" => $data_row->seq,
                    "seq" => $data_row->seq,
                    "print_c" => $chekbox,
                    "order_no" => $data_row->order_no,
                    "order_date" => parent::cdate($data_row->order_date),
                    "order_status" => parent::cstdes($data_row->order_status, STATUS_ORDER),
                    "total_merchant" => parent::cnum($data_row->total_merchant),
                    "awb_no" => $dataresi,
                    "member_notes" => parent::cdef($data_row->member_notes),
                    "districtname" => $data_row->districtname
                );
                $output['aaData'][] = $row;
            }
        };
        echo json_encode(
                $output);
    }

    public function get_awb($orderseq) {
        $selected = new stdClass();
        $selected->user_id = parent::get_merchant_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->seq = $orderseq;
        $selected->merchant_seq = parent::get_merchant_user_seq();
        $data_expedition = $this->M_merchant_delivery->get_expedition($selected);
        if (isset($data_expedition)) {

            switch ($data_expedition[0]->awb_method) {
                case "A":    // API AWB
                    switch ($data_expedition[0]->exp_code) {

                        case JNE_EXPEDITION_CODE :
                            $data_product_total = $this->M_merchant_delivery->get_total_product($selected);
                            $params = '';
                            $params = 'username=' . API_USERNAME_JNE . '&api_key=' . API_KEY_JNE;
                            $params .= '&OLSHOP_CUST=' . JNE_CUST_ID;
                            $params .= '&OLSHOP_ORIG=' . $data_expedition[0]->city_from;       //done
                            $params .= '&OLSHOP_BRANCH=' . substr($data_expedition[0]->city_from, 0, 3) . '000';     //done
                            $params .= '&OLSHOP_ORDERID=' . $data_expedition[0]->ref_awb_no;   //done

                            $params .= '&OLSHOP_SHIPPER_NAME=' . $data_expedition[0]->merchantname;     //done
                            $params .= '&OLSHOP_SHIPPER_ADDR1=' . substr($data_expedition[0]->pickup_addr, 0, 30);    //done
                            $params .= '&OLSHOP_SHIPPER_ADDR2=' . substr($data_expedition[0]->pickup_addr, 30, 30);
                            $params .= '&OLSHOP_SHIPPER_ADDR3=' . substr($data_expedition[0]->pickup_addr, 60, 30);

                            $params .= '&OLSHOP_SHIPPER_CITY=' . $data_expedition[0]->city_from;
                            $params .= '&OLSHOP_SHIPPER_REGION=' . $data_expedition[0]->city_from;
                            $params .= '&OLSHOP_SHIPPER_ZIP=' . $data_expedition[0]->pickup_zip_code;
                            $params .= '&OLSHOP_SHIPPER_PHONE=' . $data_expedition[0]->phone_no;

                            $params .= '&OLSHOP_RECEIVER_NAME=' . $data_expedition[0]->receiver_name;
                            $params .= '&OLSHOP_RECEIVER_ADDR1=' . substr($data_expedition[0]->receiver_address, 0, 30);
                            $params .= '&OLSHOP_RECEIVER_ADDR2=' . substr($data_expedition[0]->receiver_address, 30, 30);
                            $params .= '&OLSHOP_RECEIVER_ADDR3=' . substr($data_expedition[0]->receiver_address, 60, 30);
                            $params .= '&OLSHOP_RECEIVER_CITY=' . $data_expedition[0]->city_to;
                            $params .= '&OLSHOP_RECEIVER_REGION=' . $data_expedition[0]->city_to;
                            $params .= '&OLSHOP_RECEIVER_ZIP=' . $data_expedition[0]->receiver_zip_code;

                            $params .= '&OLSHOP_RECEIVER_PHONE=' . $data_expedition[0]->receiver_phone_no;   //done
                            $params .= '&OLSHOP_DEST=' . $data_expedition[0]->city_from;   //done
                            $params .= '&OLSHOP_SERVICE=' . $data_expedition[0]->code;      //done
                            $params .= '&OLSHOP_QTY=' . $data_product_total[0]->total_qty;      //done
                            $params .= '&OLSHOP_WEIGHT=' . $data_product_total[0]->total_weight;       //done
                            $params .= '&OLSHOP_GOODSTYPE=2';    //harcoded 2
                            $params .= '&OLSHOP_GOODSDESC=';
                            $params .= '&OLSHOP_INST=';
                            $params .= '&OLSHOP_GOODSVALUE=' . $data_product_total[0]->total_price;  //done
                            $params .= '&OLSHOP_INS_FLAG=' . (($data_product_total[0]->total_insurance > 0) ? 'Y' : 'N');     //harcoded Y for insurance  ? harus cek lg
                            $url = URL_JNE_API_TRACE . strtolower(SUB_URL_JNE_API);
                            $this->curl->set_url($url);
                            $this->curl->setIsUsingUserAgent(true);
                            $this->curl->setParams($params);
                            $this->curl->setData();
                            $json_encoded = $this->curl->getResponse();
                            $data_jsons = json_decode($json_encoded, true);

//			    $this->M_merchant_delivery->trans_rollback();
//			    die(print_r($json_encoded . ' ## ' . $url . '?' . $params));

                            $web_params = new stdClass();
                            $web_params->user_id = parent::get_merchant_user_id();
                            $web_params->ip_address = parent::get_ip_address();
                            $web_params->type = GET_AWB_TYPE;
                            $web_params->function_name = FUNCTION_GET_JNE_AWB_EXPEDITION;
                            $web_params->request_datetime = date('Y-m-d H:i:s');
                            $web_service_seq = $this->M_web_service->save_web_service($web_params);

                            $web_params->web_service_seq = $web_service_seq[0]->new_seq;
                            $web_params->request = $this->curl->getRequestHeaders() . $this->curl->getRequest();
                            $web_params->response = $this->curl->getResponseHeaders() . $this->curl->getResponse();
                            $web_params->response_datetime = date('Y-m-d H:i:s');
                            $this->M_web_service->save_update_service($web_params);

                            if (isset($data_jsons['detail'][0]['status']) && strtolower($data_jsons['detail'][0]['status']) == 'sukses') {
                                $selected->awb_no = trim($data_jsons['detail'][0]['cnote_no']);
                                $this->M_merchant_delivery->save_update_awb($selected);
                            } else {

                                $params = new stdClass;
                                $params->user_id = parent::get_merchant_user_id();
                                $params->ip_address = parent::get_ip_address();
                                $params->code = "MERCHANT_AWB_ERROR";
                                $params->to_email = ADMIN_TOKO1001_EMAIL;
                                $params->ORDER_NO = $data_expedition[0]->order_no;
                                $params->RECIPIENT_NAME = ADMIN_TOKO1001_NAME;
                                $params->MERCHANT_NAME = parent::get_merchant_user_name();
                                $params->EKSPEDISI_NAME = $data_expedition[0]->expedisiname;
                                parent::email_template($params);

                                die(AWB_EXPEDITION_ERROR);
                            }
                            break;
                        case RAJA_KIRIM_EXPEDITION_CODE :
                            break;
                        case TIKI_EXPEDITION_CODE :
                            break;
                        case PANDU_EXPEDITION_CODE :
                            break;
                    }
                    break;
                case "L":    // Local AWB
                    $selected->exp_seq = $data_expedition[0]->exp_seq;
                    $data_resi = $this->M_merchant_delivery->get_local_awb($selected); // cari no resi lokal yang masih kosong
                    if (isset($data_resi)) {
                        $selected->new_awb_seq = $data_resi[0]->seq;
                        $selected->new_awb = $data_resi[0]->awb_no;
                        $this->M_merchant_delivery->save_update_merchant_awb_local($selected); // update noresi dan seq di t_order_merchant dan database no_trx resi local
                    }
                    break;
                case "N":    // Kirim sendiri
                    break;
            }
        }
    }

    function cek_batal_item($orderseq) {

        $selected = new stdClass();
        $selected->user_id = parent::get_merchant_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->seq = $orderseq;
        $selected->merchant_seq = parent::get_merchant_user_seq();

        $dataproduct = $this->M_merchant_delivery->get_data_product_status($selected, "X"); // cek ada product cancel

        if (isset($dataproduct)) {
            $valcancelproduct = 0;
            $valcancelship = 0;
//	    $valcancelins = 0;
            $product_variant_seq = array();
            $product_status = array();
            foreach ($dataproduct as $val) {
                $product_variant_seq[] = $val->product_variant_seq;
                $product_status[] = "X";
                $valcancelproduct+=($val->sell_price * $val->qty);  // nilai cancel product
                $valcancelship+=($val->ship_price_charged * ceil($val->qty * $val->weight_kg)); // nilai cancel shipping
//		$valcancelins+=$val->sell_price;
            }

            $total_order_merchant_ship = ($valcancelproduct + $valcancelship); // total value refund
            $sel_data = $this->M_merchant_delivery->get_data($selected);
            $voucherseq = $sel_data[0][0]->voucher_seq;
            // cek ada voucher
            if ($voucherseq > 0) {
                $voucherrefund = $sel_data[0][0]->voucher_refunded;   // cek sudah pernah refund
                if ($voucherrefund == 0) {
                    $selected->voucherseq = $voucherseq;
                    $sel_voucher = $this->M_merchant_delivery->get_data_promo_voucher($selected);
                    if ($valcancelproduct >= $sel_voucher[0]->nominal) {   // cek nilai belanja >= nilai voucher
                        $this->voucher = 1;
                        $newvoucherdata = parent::generate_voucher_refund($selected);
//			$params->new_voucher = $newvoucherdata->newvoucher;
                        $selected->newvoucherseq = $newvoucherdata->newvoucherseq;
                        $this->M_merchant_delivery->save_update_voucher_status($selected); // update t_order status voucher
                        $total_order_merchant_ship = $total_order_merchant_ship - $sel_voucher[0]->nominal;
                    }
                }
            } else {
// cek coupon
                $nominal = 0;
                if ($sel_data[0][0]->coupon_seq > 0) {
                    $voucherrefund = $sel_data[0][0]->voucher_refunded;   // cek sudah pernah dibatalkan kuponnya
                    if ($voucherrefund == 0) {
                        $selected->couponseq = $sel_data[0][0]->coupon_seq;
                        $selected->statusproduct = "X";
                        $sel_coupon = $this->M_merchant_delivery->get_data_promo_coupon($selected); // cek ada order produk yang dicancel dalam kupon dan mendapatkan nilai dari kupon
                        if (isset($sel_coupon)) {   // cek nilai belanja >= nilai voucher
                            $this->coupons = $sel_coupon[0]->nominal;
                            $this->M_merchant_delivery->save_update_coupon_status($selected); // update voucher_refunded di t_order
                            $total_order_merchant_ship = ($total_order_merchant_ship - $sel_coupon[0]->nominal); // nilai yang dikembalikan setelah dikurang kupon
                        }
                    }
                }
            }
            $selected->member_seq = $sel_data[0][0]->member_seq;
            $selected->trxno = $sel_data[0][0]->ref_awb_no; // penggantian dari order_no biar bisa uniq di t_member_account    15-12-2015
            if ($sel_data[0][0]->pg_method_seq == PAYMENT_SEQ_CREDIT_CARD) {    // payment menggunakan CC, sementara dulu selama cancel pg masih kosong
                $selected->mutation_type = 'N';
                $selected->pg_method_seq = $sel_data[0][0]->pg_method_seq;
                $selected->depositamount = 0;
                $selected->nondepositamount = $total_order_merchant_ship;
            } else {
                $selected->mutation_type = 'C';
                $selected->pg_method_seq = $sel_data[0][0]->pg_method_seq;
                $selected->depositamount = $total_order_merchant_ship;
                $selected->nondepositamount = 0;
            }
            $selected->product_variant_seq = $product_variant_seq;
            $selected->product_status = $product_status;
            $selected->old_voucher = $voucherseq;

            $this->send_email_failure_item($selected, $dataproduct);
            parent::generate_member_refund($selected);
            //$this->M_merchant_delivery->save_add_refund($selected);
            $this->M_merchant_delivery->save_update_product($selected); //	update product cancel

            /* 	  batal 10/12/2015 biar bisa ditrace nilai awal
              $adaproduct = $this->M_merchant_delivery->get_data_product_status($selected, "R"); // cek product yang tidak cancel
              $sell_price = 0;
              $ship_price_real = 0;
              $ship_price_charged = 0;
              $ins_rate_percent = 0;
              foreach ($adaproduct as $val) {
              $sell_price+= ($val->sell_price * $val->qty);
              $ship_price_real+=ceil($val->ship_price_real * $val->qty * $val->weight_kg);
              $ship_price_charged+=ceil($val->ship_price_charged * $val->qty * $val->weight_kg);
              $ins_rate_percent+=(($val->ins_rate_percent / 100) * $val->sell_price * $val->qty);
              }
              $selected->new_sell_price = $sell_price;
              $selected->new_ship_price_real = $ship_price_real;
              $selected->new_ship_price_charged = $ship_price_charged;
              $selected->new_ins_rate_percent = $ins_rate_percent;

              $this->M_merchant_delivery->save_update_order_merchant($selected);  // save new value t_order_merchant
             *
             */
        }
    }

    protected function sendemail_confirm($orderseq) {
        $selected = new stdClass();
        $selected->user_id = parent::get_merchant_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->seq = $orderseq;
        $selected->merchant_seq = parent::get_merchant_user_seq();

        $sel_order = $this->M_merchant_delivery->get_data($selected);
        $selected->member_seq = $sel_order[0][0]->member_seq;
        $sel_member = $this->M_merchant_delivery->get_data_member($selected);

        $params = new stdClass;
        $params->user_id = parent::get_merchant_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->code = "ORDER_IN_SHIPPING";

        $params->to_email = $sel_member[0]->email;
        $params->RECIPIENT_NAME = $sel_member[0]->name;
        $params->ORDER_NO = $sel_order[0][0]->order_no;
        $params->TANGGAL = parent::cdate($sel_order[0][0]->print_date);
        $orderitem = '';
        if (isset($sel_order[1])) {
            $orderitem = '<table border=1><tr><td>Produk</td><td>Qty</td></tr>';
            foreach ($sel_order[1] as $product) {
                $orderitem .= '<tr><td>' . $product->name . '</td><td>' . $product->qty . '</td></tr>';
            }
            $orderitem.='</table>';
        }
        $params->ORDER_ITEMS = $orderitem;
        $params->MERCHANT_NAME = parent::get_merchant_user_name();
        $info = "";
        if ($sel_order[0][0]->real_expedition_service_seq != 0) {
            $sel_ekpedisi = $this->M_merchant_delivery->get_expedition($selected);
            $info = " Dengan ekspedisi <strong>" . $sel_ekpedisi[0]->expedisiname . "</strong><br/><br/>NO RESI : <strong>" . $sel_ekpedisi[0]->awb_no . "</strong>.";
        }
        $params->INFO_EKSPEDISI = $info;
        parent::email_template($params);
    }

    public function send_email_failure_item($data1, $data2) {

        $selected = new stdClass();
        $selected->ip_address = parent::get_ip_address();
        $selected->user_id = parent::get_merchant_user_id();
        $selected->ORDER_NO = $data2[0]->order_no;
        $selected->ORDER_DATE = parent::cdate($data2[0]->order_date);
        $selected->REFUND_VALUE = parent::cnum($data1->depositamount);
        $selected->TOTAL_REFUND = parent::cnum($data1->depositamount);
        $selected->DEPOSIT_LINK = base_url() . 'member';

        if ($this->voucher == 1) {
            $selected->old_voucher = $data1->voucherseq;
            $selected->new_voucher = $data1->newvoucherseq;

            $voucher = $this->M_merchant_delivery->get_data_voucher($selected);

            $selected->old_voucher = $voucher[0]->old_voucher;
            $selected->nominal_old_voucher = $voucher[0]->nominal_old_voucher;
            $selected->new_voucher = $voucher[0]->new_voucher;
            $selected->VOUCHER_LINK = base_url() . 'member';
        } else {
            $selected->old_voucher = "";
            $selected->nominal_old_voucher = $this->coupons;
            $selected->new_voucher = "";
            $selected->VOUCHER_LINK = base_url() . 'member';
        }

        $selected->code = MERCHANT_CANCEL_ORDER;
        $selected->to_email = $data2[0]->member_email;

        $order_content = "<table border='1'>
                           <thead>
                            <tr>
                                <th>Produk</th>
                                <th>Varian</th>
                                <th>Qty</th>
                                <th>Harga (Rp)</th>
                                <th>Biaya Kirim (Rp)</th>
                                <th>Total (Rp)</th>
                            </tr>
                           </thead><tbody>";

        foreach ($data2 as $item) {
            $order_content = $order_content . "<tr><td>" . $item->product_name . "</td>
                                               <td>" . $item->variant_value . "</td>
                                               <td>" . number_format($item->qty) . "</td>
                                               <td>" . number_format($item->sell_price) . "</td>
                                               <td>" . number_format(ceil($item->qty * $item->weight_kg) * $item->ship_price_charged) . "</td>
                                               <td>" . number_format((ceil($item->qty * $item->weight_kg) * $item->ship_price_charged) +
                            ($item->qty * $item->sell_price)) . "</td></tr>";
        }

        $order_content = $order_content . "</tbody>
                                          <tr>
                                            <td colspan=5>" . ($this->coupons > 0 ? "Kupon" : "Voucher") . "</td>
                                            <td>" . number_format($selected->nominal_old_voucher) . "</td>
                                          </tr>
                                          <tr>
                                            <td colspan=5>Total Bayar</td>
                                            <td>" . number_format(($data1->depositamount)) . "</td>
                                          </tr>
                                          </table>";

        $address_content = "Penerima : " . $data2[0]->receiver_name . "<br>
                            Alamat : " . $data2[0]->receiver_address . "-" . $data2[0]->province_name . "-" . $data2[0]->city_name . "-" . $data2[0]->district_name . "-" . $data2[0]->receiver_zip_code . "<br>
                            No Tlp : " . $data2[0]->receiver_phone_no . "<br>";

        $selected->ORDER_ITEMS = $order_content;
        $selected->RECIPIENT_ADDRESS = $address_content;
        $selected->RECIPIENT_NAME = $data2[0]->member_name;

        parent::email_template($selected);
    }

    protected function get_edit() {

        $selected = new stdClass();
        $selected->user_id = parent::get_merchant_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->seq = parent::get_input_post("key");
        $selected->merchant_seq = parent::get_merchant_user_seq();

        try {
            $sel_data = $this->M_merchant_delivery->get_data($selected);

            if (isset($sel_data[0][0])) {
                $selected->dseq = $sel_data[0][0]->receiver_district_seq;
                $selected->cseq = "";
                $selected->pseq = "";
            }

            $sel_data1 = $this->M_merchant_delivery->get_location($selected);
            $sel_data2 = $this->M_merchant_delivery->get_merchant($selected);

            if (isset($sel_data)) {
                $this->data[EXPEDITION_NAME] = $this->M_merchant_delivery->get_dropdown_expedition();
                parent:: set_data($this->data, $sel_data);
                $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data1;
                $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data2;
            }
        } catch (Exception $ex) {
            parent:: set_error($this->data, $ex);
        }
    }

    protected function sendemail($dataorder, $data_dtl) {
        $selected = new stdClass();
        $selected->user_id = parent::get_merchant_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->member_seq = $dataorder[0][0]->member_seq;
        $data_member = $this->M_merchant_delivery->get_data_member($selected);

        $params = new stdClass;
        $params->user_id = parent::get_merchant_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->code = "MERCHANT_CANCEL_ORDER";
        $params->to_email = $data_member[0]->email;
        $params->ORDER_NO = $dataorder[0][0]->order_no;
        $params->RECIPIENT_NAME = $data_member[0]->name;
        $params->ORDER_DATE = parent::cdate($dataorder[0][0]->order_date);
        $params->ORDER_ITEMS = $data_dtl->order_item;
        $params->RECIPIENT_ADDRESS = $dataorder[0][0]->receiver_address;
        $params->REFUND_VALUE = parent::cnum($data_dtl->refund_val);
        $params->TOTAL_REFUND = parent::cnum($data_dtl->ttl_refund);
        $params->DEPOSIT_LINK = base_url() . 'member';
        $params->OLD_VOUCHER = $data_dtl->old_voucher;
        $params->NEW_VOUCHER = $data_dtl->new_voucher;
        $params->VOUCHER_LINK = base_url() . 'member';
        parent::email_template($params);
    }

    protected function batal_order($orderseq) { // cancel order merchant
        $params = new stdClass();
        $params->refund_val = '0';
        $params->ttl_refund = '0';
        $params->old_voucher = '';
        $params->new_voucher = '';
        $selected = new stdClass();
        $selected->user_id = parent::get_merchant_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->seq = $orderseq;
        $selected->merchant_seq = parent::get_merchant_user_seq();
        $sel_data = $this->M_merchant_delivery->get_data($selected);
        $order_content = '';
        if (isset($sel_data)) {
            $totalmerchant = $sel_data[0][0]->total_merchant; // total order merchant
            $totalshipping = $sel_data[0][0]->total_ship_charged; // total order merchant
            $total_order_merchant_ship = ($totalmerchant + $totalshipping);
            $totalrefund = $total_order_merchant_ship;  // nilai yang dikembalikan sama dengan total order merchant+biaya kirim
            $params->refund_val = $totalrefund;
            $voucherseq = $sel_data[0][0]->voucher_seq; // cek ada voucher
            $order_content = "<table border='1'>
                           <thead>
                            <tr>
                                <th>Produk</th>
                                <th>Varian</th>
                                <th>Qty</th>
                                <th>Harga (Rp)</th>
                                <th>Biaya Kirim (Rp)</th>
                                <th>Total (Rp)</th>
                            </tr>
                           </thead><tbody>";
            $totalref = 0;
            foreach ($sel_data[1] as $item) {
                $order_content.="<tr><td>" . $item->name . "</td>
				<td>" . $item->variant_value . "</td>
				<td>" . number_format($item->qty) . "</td>
				<td>" . number_format($item->sell_price) . "</td>
				<td>" . number_format(ceil($item->qty * $item->weight_kg) * $item->ship_price_charged) . "</td>
				<td>" . number_format((ceil($item->qty * $item->weight_kg) * $item->ship_price_charged) +
                                ($item->qty * $item->sell_price)) . "</td></tr>";
                $totalref = $totalref + (ceil($item->qty * $item->weight_kg) * $item->ship_price_charged) +
                        ($item->qty * $item->sell_price);
            }
            $order_content .= "
			<tr><td colspan=5>Total</td><td>" . number_format($totalref) . "</td></tr>
			</tbody>
			    </table>";
            $params->order_item = $order_content;
// cek voucher
            if ($voucherseq > 0) {
                $voucherrefund = $sel_data[0][0]->voucher_refunded;   // cek sudah pernah refund voucher
                if ($voucherrefund == 0) {
                    $selected->voucherseq = $voucherseq;
                    $sel_voucher = $this->M_merchant_delivery->get_data_promo_voucher($selected);
                    $params->old_voucher = $sel_voucher[0]->code;
                    if ($totalmerchant >= $sel_voucher[0]->nominal) {   // cek nilai belanja >= nilai voucher
                        $newvoucherdata = parent::generate_voucher_refund($selected);
                        $params->new_voucher = $newvoucherdata->newvoucher;
                        $selected->newvoucherseq = $newvoucherdata->newvoucherseq;
                        $this->M_merchant_delivery->save_update_voucher_status($selected);
                        $totalrefund = ($total_order_merchant_ship - $sel_voucher[0]->nominal); // nilai yang dikembalikan setelah dikurang voucher
                    }
                }
            } else {
// cek coupon
                $nominal = 0;
                if ($sel_data[0][0]->coupon_seq > 0) {
                    $voucherrefund = $sel_data[0][0]->voucher_refunded;   // cek sudah pernah dibatalkan kuponnya
                    if ($voucherrefund == 0) {
                        $selected->couponseq = $sel_data[0][0]->coupon_seq;
                        $selected->statusproduct = "";
                        $sel_coupon = $this->M_merchant_delivery->get_data_promo_coupon($selected); // cek ada order produk dalam kupon dan mendapatkan nilai dari kupon
                        if ($sel_coupon[0]->nominal > 0) {   // cek nilai belanja >= nilai voucher
                            $this->M_merchant_delivery->save_update_coupon_status($selected); // update voucher_refunded di t_order
//			    $totalrefund = ($total_order_merchant_ship ); // nilai yang dikembalikan tetap
                        }
                    }
                }
            }
            $params->ttl_refund = $totalrefund;
            $selected->member_seq = $sel_data[0][0]->member_seq;
            $selected->trxno = $sel_data[0][0]->ref_awb_no; // penggantian dari order_no biar bisa uniq di t_member_account    15-12-2015
            if ($sel_data[0][0]->pg_method_seq == PAYMENT_SEQ_CREDIT_CARD) {    // payment menggunakan CC, sementara dulu selama cancel pg masih kosong
                $selected->mutation_type = 'N';
                $selected->pg_method_seq = $sel_data[0][0]->pg_method_seq;
                $selected->depositamount = 0;
                $selected->nondepositamount = $totalrefund;
            } else {
                $selected->mutation_type = 'C';
                $selected->pg_method_seq = $sel_data[0][0]->pg_method_seq;
                $selected->depositamount = $totalrefund;
                $selected->nondepositamount = 0;
            }
            parent::generate_member_refund($selected);
            //$this->M_merchant_delivery->save_add_refund($selected);
            /* 	 batal per 10/12/2015 agar bisa dicek nilai aslinya
              $selected->new_sell_price = 0;
              $selected->new_ship_price_real = 0;
              $selected->new_ship_price_charged = 0;
              $selected->new_ins_rate_percent = 0;
              $this->M_merchant_delivery->save_update_order_merchant($selected);  // save new value t_order_merchant
             */
            $this->sendemail($sel_data, $params);
        }
    }

    protected function save_update() {
        $selected = new stdClass();
        $selected->user_id = parent::get_merchant_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->seq = parent::get_input_post("seq");
        $selected->merchant_seq = parent::get_merchant_user_seq();
        $sel_data = $this->M_merchant_delivery->get_data($selected);
        if (!isset($sel_data) || $sel_data[0][0]->order_status != NEW_STATUS_CODE) {
            $this->data[DATA_ERROR][ERROR] = true;
            $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_UPDATE;
//	    throw new Exception(ERROR_UPDATE);
        }
        $params = new stdClass();
        $params->user_id = parent::get_merchant_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->seq = parent::get_input_post("seq");
        $params->merchant_seq = parent::get_merchant_user_seq();
        $params->ship_notes = parent::get_input_post("ship_notes");
        $params->order_status = parent::get_input_post("order_status");
        $params->product_variant_seq = $this->input->post("product_variant_seq");
        $params->product_status = $this->input->post("product_status");
        $cancel_order = "";
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                $this->M_merchant_delivery->trans_begin();
                if ($params->order_status == "X") {
                    $this->batal_order($params->seq); // batal semua order
                } else {
                    foreach ($params->product_status as $status) {
                        if ($status == "R")
                            $cancel_order = "NOT";
                        echo $status;
                    }
                    if ($cancel_order == "") {
                        $params->order_status = "X";
                        $this->batal_order($params->seq); // batal semua order
                    }
                }
                $this->M_merchant_delivery->save_update($params);
                $this->M_merchant_delivery->save_update_product($params);
                $this->M_merchant_delivery->trans_commit();
            } catch (BusisnessException $ex) {
                parent:: set_error($this->data, $ex);
                $this->M_merchant_delivery->trans_rollback();
            } catch (TechnicalException $ex) {
                parent:: set_error($this->data, $ex);
                $this->M_merchant_delivery->trans_rollback();
            } catch (Exception $ex) {
                parent:: set_error($this->data, $ex);
                $this->M_merchant_delivery->trans_rollback();
            }
        } else {
            $selected = new stdClass();
            $selected->user_id = parent::get_merchant_user_id();
            $selected->ip_address = parent::get_ip_address();
            $selected->seq = parent::get_input_post("seq");
            $selected->merchant_seq = parent::get_merchant_user_seq();
            $sel_data = $this->M_merchant_delivery->get_data($selected);
            if (isset($sel_data[0][0])) {
                $selected->dseq = $sel_data[0][0]->receiver_district_seq;
                $selected->cseq = "";
                $selected->pseq = "";
            }
            $sel_data1 = $this->M_merchant_delivery->get_location($selected);
            $sel_data2 = $this->M_merchant_delivery->get_merchant($selected);
            if (isset($sel_data)) {
                parent:: set_data($this->data, $sel_data);
                $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data1;
                $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data2;
            }
        }
    }

}

?>