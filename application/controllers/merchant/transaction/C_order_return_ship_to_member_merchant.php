<?php

require_once CONTROLLER_BASE_MERCHANT;

class C_order_return_ship_to_member_merchant extends controller_base_merchant {

    private $data;
    private $return_no;

    public function __construct() {
        $this->data = parent::__construct("MTRX01007", "merchant/transaction/return_merchant/ship_to_member");
        $this->initialize();
    }

    private function initialize() {
        $this->return_no = $this->uri->segment(5);
        $this->load->model('merchant/transaction/M_order_return_merchant');
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        $this->get_data_header();
        if (!$this->input->post()) {
            $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->return_no;
            $this->data[EXPEDITION_NAME] = $this->M_order_return_merchant->get_dropdown_expedition();
            $this->load->view("merchant/transaction/order_return_ship_to_member_merchant", $this->data);
        } else {
            if ($this->data[DATA_SUCCESS][SUCCESS] == true) {
                $this->session->set_tempdata(DATA_MESSAGE, true);
                redirect(base_url("merchant/transaction/return_merchant"));
            } else if ($this->data[DATA_ERROR][ERROR] == true) {
                $this->data[DATA_ERROR][ERROR] = true;
                $merchant_info[SESSION_DATA] = $this->data;
                $this->session->set_userdata($merchant_info);
                redirect(base_url($this->data[DATA_AUTH][FORM_URL] . "/" . $this->return_no));
            }
        }
    }

    protected function get_data_header() {
        $params = new stdClass;
        $params->merchant_seq = parent::get_merchant_user_seq();
        $params->ip_address = parent::get_ip_address();
        $params->return_no = $this->return_no;
        try {
            $sel_data = $this->M_order_return_merchant->get_data_detail($params);
            if ($sel_data[0]->merchant_seq != $params->merchant_seq) {
                redirect(base_url("merchant/transaction/return_merchant"));
            } else {
                if (isset($sel_data)) {
                    parent::set_data_header($this->data, $sel_data);
                } else {
                    $this->load->view("errors/html/error_404");
                }
            }
        } catch (BusisnessException $ex) {
            $this->load->view("errors/html/error_404");
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_update() {
        $selected = new stdClass();
        $selected->merchant_id = parent::get_merchant_user_id();
        $selected->user_id = parent::get_merchant_user_id();
        $selected->merchant_seq = parent::get_merchant_user_seq();
        $selected->ip_address = parent::get_ip_address();
        $selected->return_no = parent::get_input_post("return_no");
        $selected->tgl_kirim = parent::get_input_post("tgl_kirim");
        $selected->expedition_seq = parent::get_input_post("expedition_seq");
        $selected->no_awb_merchant = parent::get_input_post("no_resi");
        $selected->merchant_comment = parent::get_input_post("merchant_comment");
        
        $sel_data = $this->M_order_return_merchant->get_data_detail($selected);
        $send_date = strtotime($selected->tgl_kirim);
        $send_date = date('Y-m-d', $send_date);
        $merchant_received_date = substr($sel_data[0]->merchant_received_date, 0, 10);
        
        $this->data[DATA_SELECTED][LIST_DATA][] = $selected;
//        echo $send_date ; echo $received_date; die();\
        if ($this->data[DATA_ERROR][ERROR] === false) {
            try {
                if ($sel_data[0]->shipment_status == RECEIVED_BY_MEMBER) {
                    throw new Exception(ERROR_UPDATE);
                } else {
                    if ($send_date < $merchant_received_date) {                        
                        throw new Exception(ERROR_DATE_SENT_FROM_MERCHANT);
                    } else {
                        if ($selected->no_awb_merchant == "") {
                            throw new Exception("No. Resi " . ERROR_VALIDATION_MUST_FILL);                            
                        } else {

                            $this->M_order_return_merchant->trans_begin();
                            $this->M_order_return_merchant->return_update_ship_to_member($selected);
                            $sel_data2 = $this->M_order_return_merchant->get_data_detail_approve($selected);
                            if ($sel_data2[0]->return_status == RETURN_ITEM_BY_MERCHANT) {
                                $selected->code = ORDER_RETURN_MERCHANT_APPROVE;
                                $selected->to_email = $sel_data[0]->member_email;
//                            $selected->to_email = "toriq.354@gmail.com";
                                $selected->RECIPIENT_NAME = $sel_data2[0]->member_name;
                                $selected->SHIP_TO_MEMBER_DATE = parent::cdate($sel_data2[0]->ship_to_member_date);
                                $selected->EXPEDITION_NAME = $sel_data2[0]->exp_name_to_member;
                                $selected->AWB_MERCHANT_NO = $sel_data2[0]->awb_merchant_no;
                                $selected->TRX_NO = $selected->return_no;
                                $selected->LINK = base_url() . MEMBER_ORDER_RETURN;
                                $sent_email = $this->email_template($selected);
                            }
                            if ($sel_data2[0]->return_status == REJECT_BY_MERCHANT) {

                                $selected->code = ORDER_RETURN_REJECT;
                                $selected->to_email = $sel_data2[0]->member_email;
//                            $selected->to_email = "toriq.354@gmail.com";
                                $selected->RECIPIENT_NAME = $sel_data2[0]->member_name;
                                $selected->NOTES = $sel_data2[0]->review_merchant;
                                $selected->TRX_NO = $selected->return_no;
                                $selected->LINK = base_url() . MEMBER_ORDER_RETURN;
                                $sent_email = $this->email_template($selected);
                            }
                            $this->M_order_return_merchant->trans_commit();
                            $this->data[DATA_SUCCESS][SUCCESS] = true;
                        }
                    }
                }
            } catch (BusisnessException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_order_return_merchant->trans_rollback();
            } catch (TechnicalException $ex) {
                parent::set_error($this->data, $ex);
                $this->M_order_return_merchant->trans_rollback();
            } catch (Exception $ex) {
                parent::set_error($this->data, $ex);
                $this->M_order_return_merchant->trans_rollback();
            }
        }
    }

}

?>