<?php

require_once CONTROLLER_BASE_MERCHANT;

class C_order_return_merchant extends controller_base_merchant {

    private $data;

    public function __construct() {
        $this->data = parent::__construct("MTRX01005", "merchant/transaction/return_merchant");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('merchant/transaction/M_order_return_merchant');
        $this->load->model('component/M_dropdown_list');
        parent::register_event($this->data, ACTION_SEARCH, "search");
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        $merchant_seq = parent::get_merchant_user_seq();
        $this->data[MEMBER_EMAIL] = $this->M_dropdown_list->get_dropdown_member_email_return($merchant_seq);
        if (!$this->input->post()) {

            if ($this->session->tempdata('DATA_MESSAGE') == true) {
                $this->data[DATA_SUCCESS][SUCCESS] = true;
                $this->session->unset_tempdata('DATA_MESSAGE');
            }

            $this->data[FILTER] = 'merchant/transaction/order_return_merchant_f.php';
            $this->load->view("merchant/transaction/order_return_merchant", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("merchant/transaction/order_return_merchant", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("merchant/transaction/order_return_merchant", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $merchant_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($merchant_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL]));
                }
            }
        }
    }

    public function get_edit() {
        $selected = new stdClass();
        $selected->merchant_seq = parent::get_merchant_user_seq();
        $selected->merchant_id = parent::get_merchant_user_id();
        $selected->ip_address = parent::get_ip_address();
        $selected->return_seq = parent::get_input_post("key");
        try {
            $sel_data = $this->M_order_return_merchant->get_data($selected);            
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
                $this->data[DATA_SELECTED][LIST_DATA][] = $sel_data;
            } else {
                redirect(base_url("merchant/transaction/order_return_merchant"));
            }
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    public function search() {
        $return_date_between = explode(' - ', parent::get_input_post("date_between"));
        $filter = new stdClass;
        $filter->user_id = parent::get_merchant_user_id();
        $filter->ip_address = parent::get_ip_address();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->merchant_seq = parent::get_merchant_user_seq();
        $filter->no_return = parent::get_input_post("no_return");
        $filter->date_search = isset($return_date_between[1]) ? "1" : "0";
        $filter->return_date_from = $return_date_between[0];
        $filter->return_date_to = isset($return_date_between[1]) ? $return_date_between[1] : "";
        $filter->ship_status = parent::get_input_post("ship_status");
        $filter->member_email = parent::get_input_post("member_email");
        $filter->new_status_code = NEW_STATUS_CODE; //status N 
        $filter->ship_to_member_status = SHIP_TO_MEMBER; // Status T
        $filter->ship_from_member_status = SHIP_FROM_MEMBER; // Status M
        $filter->received_by_member_status = RECEIVED_BY_MEMBER; // Status C
        $filter->received_by_admin_status = RECEIVED_BY_ADMIN; // Status A
        $filter->rejected_status = REJECT_STATUS_CODE; // Status R

        try {
            $list_data = $this->M_order_return_merchant->get_list($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );
        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                if ($data_row->return_status == NEW_STATUS_CODE AND $data_row->shipment_status == SHIP_TO_MERCHANT) {
                    $action = "<a class='btn btn-xs btn-success btn-block btn-flat' href='" . base_url() . "merchant/transaction/return_merchant/received/$data_row->return_no'>Terima</a>";
                } elseif ($data_row->return_status == NEW_STATUS_CODE AND $data_row->shipment_status == RECEIVED_BY_MERCHANT) {
                    $action = "<a  class='btn btn-flat btn-xs btn-primary btn-block' href='" . base_url() . "merchant/transaction/return_merchant/change_status/$data_row->return_no'><center>Ubah Status</center></a>";
                } elseif (($data_row->return_status == RETURN_ITEM_BY_MERCHANT OR $data_row->return_status == REJECT_STATUS_CODE) AND $data_row->shipment_status == RECEIVED_BY_MERCHANT) {
                    $action = "<a  class='btn btn-xs btn-flat btn-warning btn-block' href='" . base_url() . "merchant/transaction/return_merchant/ship_to_member/$data_row->return_no'><center>Kirim ke Member</center></a>";
                } elseif (($data_row->return_status == RETURN_ITEM_BY_MERCHANT OR $data_row->return_status == REJECT_STATUS_CODE) AND $data_row->shipment_status == SHIP_TO_MEMBER) {
                    $action = "<a  class='btn btn-xs btn-flat btn-warning btn-block' href='" . base_url() . "merchant/transaction/return_merchant/ship_to_member/$data_row->return_no'><center>Sedang dikirim ... </center></a>";
                } elseif ($data_row->return_status == REFUND_BY_MERCHANT AND $data_row->shipment_status == RECEIVED_BY_MERCHANT) {
                    $action = "";
                } elseif (($data_row->return_status == RETURN_ITEM_BY_MERCHANT OR $data_row->return_status == REJECT_STATUS_CODE) AND $data_row->shipment_status == RECEIVED_BY_MEMBER) {
                    $action = "";
                } else {
                    $action = "";
                }
                if ($data_row->variant_value_seq == ALL_PRODUCT_VARIANT) {
                    $product_name = parent::cdef($data_row->product_name);
                } else {
                    $product_name = parent::cdef($data_row->product_name . ' - ' . $data_row->variant_name);
                }
                $row = array("DT_RowId" => $data_row->return_seq,
                    "return_no" => $data_row->return_no,
                    "order_seq" => $data_row->order_seq,
                    "order_no" => $data_row->order_no,
                    "product_name" => $product_name,
                    "qty" => parent::cdef($data_row->qty),
                    "shipment_status" => parent::cstdes($data_row->shipment_status, STATUS_SHIPMENT),
                    "return_status" => parent::cstdes($data_row->return_status, STATUS_RETURN),
                    "awb_admin_no" => $data_row->awb_admin_no,
                    "created_date" => parent::cdate($data_row->created_date),
                    "action" => $action
                );
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
    }

}

?>