<?php

require_once CONTROLLER_BASE_MERCHANT;

class C_order_return_received_merchant extends controller_base_merchant {

    private $data;
    private $return_no;

    public function __construct() {
        $this->data = parent::__construct("MTRX01006", "merchant/transaction/return_merchant/received");
        $this->initialize();
    }

    private function initialize() {
        $this->return_no = $this->uri->segment(5);
        $this->load->model('merchant/transaction/M_order_return_merchant');
        parent::register_event($this->data, ACTION_EDIT, "get_edit");
        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");
        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        $this->get_data_header();
        if (!$this->input->post()) {
            $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->return_no;
            $this->load->view("merchant/transaction/order_return_received_merchant", $this->data);
        } else {
            if ($this->data[DATA_SUCCESS][SUCCESS] == true) {
                $this->session->set_tempdata(DATA_MESSAGE, true);
                redirect(base_url("merchant/transaction/return_merchant"));
            } else if ($this->data[DATA_ERROR][ERROR] == true) {
                $this->data[DATA_ERROR][ERROR] = true;
                $merchant_info[SESSION_DATA] = $this->data;
                $this->session->set_userdata($merchant_info);
                redirect(base_url($this->data[DATA_AUTH][FORM_URL] . "/" . $this->return_no));
            }
        }
    }

    protected function get_data_header() {
        $params = new stdClass;
        $params->merchant_seq = parent::get_merchant_user_seq();
        $params->ip_address = parent::get_ip_address();
        $params->return_no = $this->return_no;
        try {
            $sel_data = $this->M_order_return_merchant->get_data_detail($params);
            if ($sel_data[0]->merchant_seq != $params->merchant_seq) {
                redirect(base_url("merchant/transaction/return_merchant"));
            } else {
                if (isset($sel_data)) {
                    parent::set_data($this->data, $sel_data);
                } else {
                    $this->load->view("errors/html/error_404");
                }
            }
        } catch (BusisnessException $ex) {
            $this->load->view("errors/html/error_404");
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

    protected function save_update() {
        $selected = new stdClass();
        $selected->merchant_id = parent::get_merchant_user_id();
        $selected->merchant_seq = parent::get_merchant_user_seq();
        $selected->ip_address = parent::get_ip_address();
        $selected->return_no = parent::get_input_post("return_no");
        $selected->tgl_terima = parent::get_input_post("tgl_terima");
        $this->data[DATA_SELECTED][LIST_DATA][] = $selected;
        $sel_data = $this->M_order_return_merchant->get_data_detail($selected);
        $received_date = strtotime($selected->tgl_terima);
        $received_date = date('Y-m-d', $received_date);
        $return_date = substr($sel_data[0]->created_date, 0, 10);
        try {

            if ($params->received_date == " ") {
                throw new Exception("Tgl. Terima " . ERROR_VALIDATION_MUST_FILL);
            }
            if ($sel_data[0]->shipment_status == RECEIVED_BY_MERCHANT) {
                throw new Exception(ERROR_UPDATE);
            } elseif ($received_date < $return_date) {
                $this->data[DATA_ERROR][ERROR] = true;
                throw new Exception(ERROR_DATE_RECEIVED);
            } else {
                $this->M_order_return_merchant->trans_begin();
                $this->M_order_return_merchant->return_update_received($selected);
                $this->M_order_return_merchant->trans_commit();
                $this->data[DATA_SUCCESS][SUCCESS] = true;
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_order_return_merchant->trans_rollback();
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
            $this->M_order_return_merchant->trans_rollback();
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
            $this->M_order_return_merchant->trans_rollback();
        }
    }

}
?>