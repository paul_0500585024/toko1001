<?php

require_once CONTROLLER_BASE_MERCHANT;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Redeem Merchant
 *
 * @author Jartono
 */
class C_redeem_merchant extends controller_base_merchant {

    private $data;

    public function __construct() {
	$this->data = parent::__construct("MTRX01004", "merchant/transaction/redeem_merchant");
	$this->initialize();
    }

    private function initialize() {
	$this->load->model('merchant/transaction/M_redeem_merchant');
	parent::register_event($this->data, ACTION_SEARCH, "search");
	parent::register_event($this->data, ACTION_EDIT, "get_edit");
	parent::register_event($this->data, ACTION_ADDITIONAL, "get_ajax");
	parent::fire_event($this->data);
    }

    public function index() {
	$this->data[FILTER] = 'merchant/transaction/redeem_merchant_f.php';

	if (!$this->input->post()) {
	    $this->load->view("merchant/transaction/redeem_merchant", $this->data);
	} else {
	    if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
		if ($this->data[DATA_ERROR][ERROR] === true) {
		    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
			$this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
			$this->load->view("merchant/transaction/redeem_merchant", $this->data);
		    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
			$this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
			$this->load->view("merchant/transaction/redeem_merchant", $this->data);
		    }
		} elseif ($this->data[DATA_ERROR][ERROR] === false) {
		    $this->load->view("merchant/transaction/redeem_merchant", $this->data);
		}
	    } else {

	    }
	}
    }

    protected function search() {
	$params = new stdClass;
	$params->user_id = parent::get_merchant_user_seq();
	$params->ip_address = parent::get_ip_address();
	$params->start = parent::get_input_post("start");
	$params->length = parent::get_input_post("length");
	$params->order = parent::get_input_post("order");
	$params->column = parent::get_input_post("column");
	$params->merchant_id = parent::get_merchant_user_seq();
	$params->from_date = parent::get_input_post("tanggal1");
	$params->to_date = parent::get_input_post("tanggal2");
	try {
	    $list_data = $this->M_redeem_merchant->get_list($params);
	    parent::set_list_data($this->data, $list_data);
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	}

	$output = array(
	    "sEcho" => parent::get_input_post("draw"),
	    "iTotalRecords" => $list_data[0][0]->total_rec,
	    "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
	    "aaData" => array()
	);

	if (isset($list_data[1])) {
	    foreach ($list_data[1] as $data_row) {

		$row = array("DT_RowId" => $data_row->redeem_seq,
		    "merchant_seq" => $data_row->merchant_seq,
		    "merchant_info_seq" => $data_row->merchant_info_seq,
		    "paid_date" => parent::cdate($data_row->paid_date),
		    "total" => number_format($data_row->total));
		$output['aaData'][] = $row;
	    }
	};
	echo json_encode($output);
    }

    protected function get_edit() {
	$params = new stdClass();
	$params->user_id = parent::get_merchant_user_seq();
	$params->ip_address = parent::get_ip_address();
	$params->seq = parent::get_input_post("key");
	$params->merchant_seq = parent::get_merchant_user_seq();

	try {
	    $sel_data = $this->M_redeem_merchant->get_data_detail($params);
	    if (isset($sel_data)) {
		parent::set_data($this->data, $sel_data);
		$sel_data1 = $this->M_redeem_merchant->get_redeem_component_by_merchant($params);
		$this->data[DATA_SELECTED][LIST_DATA][] = $sel_data1;
		$sel_data2 = $this->M_redeem_merchant->get_order_merchant_by_redeem($params);
		$this->data[DATA_SELECTED][LIST_DATA][] = $sel_data2;
		$sel_data3 = $this->M_redeem_merchant->get_expedition_merchant_by_redeem($params);
		$this->data[DATA_SELECTED][LIST_DATA][] = $sel_data3;
	    }
	} catch (Exception $ex) {
	    parent::set_error($this->data, $ex);
	}
    }

}

?>