<?php

require_once CONTROLLER_BASE_MERCHANT;

class C_order_merchant_list_detail extends controller_base_merchant {

    private $data;
    private $hseq;

    public function __construct() {

        $this->data = parent::__construct("MTRX01002", "merchant/transaction/order_list_detail");
        $this->initialize();
    }

    private function initialize() {

        $this->hseq = $this->uri->segment(4);
        $this->load->model('merchant/transaction/M_order_merchant');
        parent::register_event($this->data, ACTION_SEARCH, "search");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {

        if (!$this->input->post()) {
            if ($this->data[DATA_AUTH][FORM_ACTION] == "") {
                $this->get_data_header();
            }
            $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
            $this->load->view("merchant/transaction/order_merchant_list_detail", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    $this->data[DATA_AUTH][FORM_URL] = $this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq;
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_ADD;
                        $this->load->view("merchant/transaction/order_merchant_list_detail", $this->data);
                    } elseif ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE) {
                        $this->data[DATA_AUTH][FORM_ACTION] = ACTION_EDIT;
                        $this->load->view("merchant/transaction/order_merchant_list_detail", $this->data);
                    }
                } else {
                    if ($this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_UPDATE OR $this->data[DATA_AUTH][FORM_ACTION] == ACTION_SAVE_ADD) {
                        $this->data[DATA_SUCCESS][SUCCESS] = true;
                        $this->data[DATA_AUTH][FORM_ACTION] = "";
                    }
                    $member_info[SESSION_DATA] = $this->data;
                    $this->session->set_userdata($member_info);
                    redirect(base_url($this->data[DATA_AUTH][FORM_URL] . "/" . $this->hseq));
                }
            }
        }
    }

    public function search() {

        $filter = new stdClass;
        $filter->user_id = parent::get_merchant_email();
        $filter->ip_address = parent::get_ip_address();
        $filter->merchant_seq = parent::get_merchant_user_seq();
        $filter->start = parent::get_input_post("start");
        $filter->length = parent::get_input_post("length");
        $filter->order = parent::get_input_post("order");
        $filter->column = parent::get_input_post("column");
        $filter->hseq = $this->hseq;

        try {
            $list_data = $this->M_order_merchant->get_order_list_detail($filter);
            parent::set_list_data($this->data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }

        $output = array(
            "sEcho" => parent::get_input_post("draw"),
            "iTotalRecords" => $list_data[0][0]->total_rec,
            "iTotalDisplayRecords" => $list_data[0][0]->total_rec,
            "aaData" => array()
        );

        if (isset($list_data[1])) {
            foreach ($list_data[1] as $data_row) {
                if ($data_row->pic_1_img == "") {
                    $image = base_url() . IMG_BLANK_100;
                } else {
                    $image = base_url(PRODUCT_UPLOAD_IMAGE) . "/" . $data_row->seq . "/" . $data_row->pic_1_img;
//                    $product = explode(" ", $data_row->product_name);
//                    $variant = explode(" ", $data_row->variant_name);
                    $link = base_url() . strtolower(url_title($data_row->product_name . ' ' . $data_row->variant_name)) . '-' . $data_row->product_variant_seq;
                }

                $row = array("DT_RowId" => $data_row->order_seq,
                    "product_name" => parent::cdef($data_row->product_name),
                    "variant_name" => $data_row->variant_name,
                    "p_weight_kg" => $data_row->p_weight_kg,
                    "p_length_cm" => $data_row->p_length_cm,
                    "p_width_cm" => $data_row->p_width_cm,
                    "p_height_cm" => $data_row->p_height_cm,
                    "qty" => $data_row->qty,
                    "sell_price" => parent::cnum($data_row->sell_price),
                    "img_src" => "<a href=" . $link . " target=" . BLANK_TARGET . "><img src='" . $image . "'width = 100px  height=100px style='max-width:100px; max-height:100px'></a></a>&nbsp;");
                $output['aaData'][] = $row;
            }
        };
        echo json_encode($output);
    }

    protected function get_data_header() {

        $params = new stdClass();
        $params->user_id = parent::get_merchant_email();
        $params->ip_address = parent::get_ip_address();
        $params->key = $this->hseq;
        $params->merchant_seq = parent::get_merchant_user_seq();
        try {
            $sel_data = $this->M_order_merchant->get_order_info($params);
            if (isset($sel_data)) {
                parent::set_data($this->data, $sel_data);
            } else {
                $this->load->view("errors/html/error_404");
            }
        } catch (BusisnessException $ex) {
            parent::set_error($this->data, $ex);
            $this->load->view("errors/html/error_404");
        } catch (TechnicalException $ex) {
            parent::set_error($this->data, $ex);
        } catch (Exception $ex) {
            parent::set_error($this->data, $ex);
        }
    }

}

?>