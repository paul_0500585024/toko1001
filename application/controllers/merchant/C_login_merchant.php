<?php

require_once CONTROLLER_BASE_MERCHANT;

class C_login_merchant extends controller_base_merchant {

    private $temp_data;

    public function __construct() {
        parent::__construct("", "", false);
        $this->initialize();
    }

    private function initialize() {
        $err = [ERROR => false, ERROR_MESSAGE => ""];
        $this->temp_data[DATA_ERROR] = $err;
        $this->load->model('merchant/M_login_merchant');
    }

    public function index() {
        $this->url = parent::get_input_get("url");
        if (isset($_SESSION[SESSION_TEMP_DATA])) {
            $this->temp_data = $_SESSION[SESSION_TEMP_DATA];
            unset($_SESSION[SESSION_TEMP_DATA]);
        }
        $this->temp_data[DATA_AUTH][FORM_AUTH][FORM_URL] = "?url=" . $this->url;
        $this->load->view("merchant/login_merchant", $this->temp_data);
    }

    public function sign_in() {
        $params = new stdClass;
        $params->user_id = $this->input->post("email");
        $params->password = $this->input->post("password");

        $this->temp_data[DATA_SELECTED][LIST_DATA] = $params;
        if (filter_var($params->user_id, FILTER_VALIDATE_EMAIL) === false) {
            $this->temp_data[DATA_ERROR][ERROR] = true;
            $this->temp_data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_VALIDATION_FILL_EMAIL_FORMAT;
        }

//        if (strlen($params->password) < 8 || strlen($params->password) > 20) {
//            $this->temp_data[DATA_ERROR][ERROR] = true;
//            $this->temp_data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_VALIDATION_FILL_PASSWORD_LENGTH;
//        }

        $params->password_1 = md5($params->password);
        $params->password_2 = md5(md5($params->password));
        $params->ip_address = $_SERVER['REMOTE_ADDR'];

        if ($this->temp_data[DATA_ERROR][ERROR] === false) {
            try {
                $list_data = $this->M_login_merchant->get_list($params);
                if (!isset($list_data[0])) {
                    $this->temp_data[DATA_ERROR][ERROR] = true;
                    $this->temp_data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_INVALID_PASSWORD_AND_USER_NAME;

                    $merchant_info[SESSION_TEMP_DATA] = $this->temp_data;
                    $this->session->set_userdata($merchant_info);
                    redirect(base_url("merchant/login"));
                }
                parent::set_list_data($this->temp_data, $list_data);
            } catch (Exception $ex) {
                parent::set_error($this->temp_data, $ex);
            }

            if ($list_data[0][0]->new_password == $params->password_2 || $list_data[0][0]->old_password == $params->password_1) {
                if (isset($list_data[0])) {
                    foreach ($list_data[1] as $menu) {
                        $form_auth_list[$menu->menu_cd] = [
                            FORM_CD => $menu->menu_cd,
                            FORM_NAME => $menu->menu_name,
                            FORM_TITLE => $menu->title_name,
                            FORM_AUTH_SEARCH => ($menu->can_view == "1" ? true : false),
                            FORM_AUTH_VIEW => ($menu->can_view == "1" ? true : false),
                            FORM_AUTH_ADD => ($menu->can_add == "1" ? true : false),
                            FORM_AUTH_EDIT => ($menu->can_edit == "1" ? true : false),
                            FORM_AUTH_DELETE => ($menu->can_delete == "1" ? true : false),
                            FORM_AUTH_APPROVE => ($menu->can_auth == "1" ? true : false),
                            FORM_AUTH_PRINT => ($menu->can_print == "1" ? true : false)
                        ];
                    }

                    $merchant_info[SESSION_MERCHANT_UID] = "M-" . $list_data[0][0]->seq;
                    $merchant_info[SESSION_MERCHANT_UNAME] = $list_data[0][0]->user_name;
                    $merchant_info[SESSION_MERCHANT_USER_GROUP] = $list_data[0][0]->name;
                    $merchant_info[SESSION_MERCHANT_LAST_LOGIN] = parent::cdate($list_data[0][0]->last_login, 1);
                    $merchant_info[SESSION_MERCHANT_EMAIL] = $list_data[0][0]->email;
                    $merchant_info[SESSION_MERCHANT_SEQ] = $list_data[0][0]->seq;

                    if ($list_data[0][0]->logo_img != "") {
                        $merchant_info[SESSION_MERCHANT_IMAGE] = PROFILE_IMAGE_MERCHANT . '/' . $list_data[0][0]->seq . '/' . $list_data[0][0]->logo_img;
                    } else {
                        $merchant_info[SESSION_MERCHANT_IMAGE] = base_url(IMG_NO_LOGO);
                    }

                    $merchant_info[SESSION_MERCHANT_FORM_AUTH] = $form_auth_list;
                    $merchant_info[SESSION_MERCHANT_CSRF_TOKEN] = base64_encode(openssl_random_pseudo_bytes(32));
                    $merchant_info[SESSION_IP_ADDR] = $_SERVER["REMOTE_ADDR"];

                    $menulst = $this->M_login_merchant->get_left_nav_merchant($params->user_id);
                    $merchant_info[SESSION_MERCHANT_LEFT_NAV] = $this->get_menu_left_nav($menulst, ROOT);

                    $this->session->set_userdata($merchant_info);
                    if ($list_data[0][0]->new_password == "") {
                        redirect(base_url("merchant/profile/change_old_password_merchant"));
                    } else {
                        $this->url = parent::get_input_get("url");
                        if ($this->url <> "") {
                            redirect(base_url($this->url));
                        } else {
                            redirect(base_url("merchant/main_page"));
                        }
                    }
                }
            } else {
                $this->temp_data[DATA_ERROR][ERROR] = true;
                $this->temp_data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_INVALID_PASSWORD_AND_USER_NAME;

                $merchant_info[SESSION_TEMP_DATA] = $this->temp_data;
                $this->session->set_userdata($merchant_info);
                redirect(base_url("merchant/login"));
            }
        }
    }

    protected function get_menu_left_nav($datas, $parent) {
        static $i = 1;
        $tab = str_repeat(" ", $i);
        if (isset($datas[$parent])) {
            $html = "";
            $i++;
            foreach ($datas[$parent] as $vals) {
                $child = $this->get_menu_left_nav($datas, $vals->menu_cd);
                $html .= "$tab";
                if ($child) {
                    $i++;
                    $html.="<li class=\"treeview\"><a href=" . base_url() . $vals->url . "><i class='fa " . $vals->icon . "'></i> <span>" . $vals->name . "</span> <i class=\"fa fa-angle-left pull-right\"></i></a><ul class=\"treeview-menu\">";
                    $html .= $child;
                    $html .= "$tab";
                    $html .= '</ul></li>';
                } else {
                    $html.="<li><a href=" . base_url() . $vals->url . "> <i class='fa " . $vals->icon . "'></i> <span>" . $vals->name . "</span> <i class=\"fa fa-angle\"></i></a>";
                    $html .= '</li>';
                }
            }
            $html .= "$tab";
            return $html;
        } else {
            return false;
        }
    }

}

?>