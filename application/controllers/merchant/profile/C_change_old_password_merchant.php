<?php

require_once CONTROLLER_BASE_MERCHANT;

class C_change_old_password_merchant extends controller_base_merchant {

    private $temp_data;
    private $data;

    public function __construct() {
        $this->data = parent::__construct("MCON01004", "merchant/profile/change_old_password_merchant");
        $this->initialize();
    }

    private function initialize() {
        $this->load->model('merchant/M_login_merchant');
        $this->load->model('merchant/profile/M_change_password_merchant');

        parent::register_event($this->data, ACTION_SAVE_UPDATE, "save_update");

        if ($this->data[DATA_INIT] === true) {
            parent::fire_event($this->data);
        }
    }

    public function index() {
        $this->data[MEMBER_EMAIL] = parent::get_merchant_email();
        if (!$this->input->post()) {
            $this->load->view("merchant/profile/change_old_password_merchant", $this->data);
        } else {
            if ($this->input->post(CONTROL_SEARCH_NAME) === null) {
                if ($this->data[DATA_ERROR][ERROR] === true) {
                    $this->load->view("merchant/profile/change_old_password_merchant", $this->data);
                } else {
                    $this->temp_data[DATA_ERROR][ERROR] = true;
                    $this->temp_data[DATA_ERROR][ERROR_MESSAGE][] = CHANGE_PASSWORD_SUCCESS;

                    $merchant_info[SESSION_TEMP_DATA] = $this->temp_data;
                    $this->session->set_userdata($merchant_info);
                    redirect(base_url("merchant/login"));
                }
            }
        }
    }

    protected function save_update() {
        $params = new stdClass();
        $params->user_id = parent::get_merchant_user_id();
        $params->ip_address = parent::get_ip_address();
        $params->merchant_seq = parent::get_merchant_user_seq();
        $params->old_password = parent::get_input_post("old_password", true, FILL_VALIDATOR, "Password Lama", $this->data);
        $params->new_password = parent::get_input_post("new_password", true, FILL_VALIDATOR, "Password Baru", $this->data);
        $params->confirm_password = parent::get_input_post("confirm_password", true, FILL_VALIDATOR, "Konfirmasi Password Baru", $this->data);

        if (strlen($params->old_password . $params->new_password) < 8 || strlen($params->old_password . $params->new_password) > 20) {
            $this->temp_data[DATA_ERROR][ERROR] = true;
            $this->temp_data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_VALIDATION_FILL_PASSWORD_LENGTH;
        }

        $params->encrypt_old_password_1 = md5($params->old_password);
        $params->encrypt_old_password_2 = md5(md5($params->old_password));
        $params->encrypt_new_password = md5(md5($params->new_password));

        try {
            $list_data = $this->M_login_merchant->get_password($params);
            parent::set_list_data($this->temp_data, $list_data);
        } catch (Exception $ex) {
            parent::set_error($this->temp_data, $ex);
        }

        if ($list_data[0]->new_password == '') {
            if ($list_data[0]->old_password == $params->encrypt_old_password_1) {
                if (isset($list_data[0])) {
                    try {
                        $list_data = new stdClass();
                        $list_data->user_id = parent::get_merchant_email();
                        $list_data->ip_address = parent::get_ip_address();
                        $list_data->merchant_seq = parent::get_merchant_user_seq();
                        $list_data->old_password = $params->encrypt_old_password_1;
                        $list_data->new_password = $params->encrypt_new_password;

                        $this->M_change_password_merchant->trans_begin();
                        $this->M_change_password_merchant->save_change_old_password($list_data);
                        $this->M_change_password_merchant->trans_commit();
                    } catch (BusisnessException $ex) {
                        parent::set_error($this->data, $ex);
                        $this->M_change_password_merchant->trans_rollback();
                    } catch (TechnicalException $ex) {
                        parent::set_error($this->data, $ex);
                        $this->M_change_password_merchant->trans_rollback();
                    } catch (Exception $ex) {
                        parent::set_error($this->data, $ex);
                        $this->M_change_password_merchant->trans_rollback();
                    }
                }
            } else {
                $this->data[DATA_ERROR][ERROR] = true;
                $this->data[DATA_ERROR][ERROR_MESSAGE][] = ERROR_INVALID_OLD_PASSWORD;
            }
        }
    }

}

?>
