CREATE DEFINER=`root`@`%` PROCEDURE `sp_attribute_category_delete`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pCategorySeq MediumInt Unsigned,
        pAttributeSeq MediumInt Unsigned
	)
BEGIN

Delete 
From 
	m_attribute_category 
Where 
	category_seq = pCategorySeq And attribute_seq = pAttributeSeq;

END