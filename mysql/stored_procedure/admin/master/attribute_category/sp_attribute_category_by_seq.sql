CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_attribute_category_by_seq`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
	pSeq MediumInt Unsigned
)
BEGIN

Declare pParentSeq Int;
Declare pLevel MediumInt Unsigned;

Set pParentSeq = pSeq;
Select `level` into pLevel from m_product_category where seq = pSeq;

WHILE pLevel <> 1 Do

Select parent_seq into pParentSeq from m_product_category where seq = pParentSeq;
Select `level` into pLevel from m_product_category where seq = pParentSeq;
    
End WHILE;    

SELECT
	a.seq,
	a.name,
    a.display_name,
    1 as checked
FROM m_attribute_category c JOIN m_attribute a
	ON c.attribute_seq = a.seq
WHERE c.category_seq = pSeq
Union All
SELECT
	a.seq,
	a.name,
    a.display_name,
    0 as checked
FROM m_product_category c JOIN m_attribute a
	ON c.seq = a.category_seq
					 Left JOIN m_attribute_category ac
	ON ac.category_seq = pSeq and
	   ac.attribute_seq = a.seq
WHERE a.category_seq = pParentSeq And ac.category_seq is null
order by name;
        
        
        
END