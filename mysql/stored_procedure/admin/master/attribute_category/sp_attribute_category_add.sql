CREATE DEFINER=`root`@`%` PROCEDURE `sp_attribute_category_add`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
	pParentSeq MediumInt Unsigned,
    pAttributeSeq MediumInt Unsigned
)
BEGIN
INSERT INTO m_attribute_category(
	category_seq,
	attribute_seq,
    created_by,
    created_date,
    modified_by,
    modified_date
)
VALUES(
	pParentSeq,
    pAttributeSeq,
    pUserID,
    now(),
    pUserID,
    now()
);
END