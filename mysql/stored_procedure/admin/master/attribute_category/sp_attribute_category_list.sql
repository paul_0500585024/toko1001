CREATE DEFINER=`root`@`%` PROCEDURE `sp_attribute_category_list`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pCurrPage Int unsigned,
        pRecPerPage Int unsigned,
        pDirSort VarChar(4),
        pColumnSort Varchar(50),
        pCategorySeq MediumInt unsigned,
        pAttributeSeq MediumInt unsigned
    )
BEGIN
    -- Begin variables for Paging
    Declare startRec Int unsigned;
    Declare totalPage Int unsigned;
    Declare totalRec Int unsigned;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";

    If pCategorySeq <> 0 Then
        Set sqlWhere = Concat(sqlWhere, " And ac.category_seq ='" , pCategorySeq, "'");
    End If;
     If pAttributeSeq <> 0 Then
        Set sqlWhere = Concat(sqlWhere, " And ac.attribute_seq ='" , pAttributeSeq, "'");
    End If;
    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "
	Select
		Count(ac.category_seq) Into @totalRec 
	From m_attribute_category ac 
	Join m_product_category pc On 
		ac.category_seq = pc.seq
	Join m_attribute a On 
		ac.attribute_seq = a.seq";
 
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
	Select
		ac.category_seq as parent_seq,
		ac.attribute_seq,
		pc.name as category_name,           
		a.name,
		ac.created_by,
		ac.created_date,
		ac.modified_by,
		ac.modified_date
	From m_attribute_category ac 
	Join m_product_category pc On 
		ac.category_seq = pc.seq
	Join m_attribute a On 
		ac.attribute_seq = a.seq";

    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, ",a.`name` " , pDirSort);

    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END