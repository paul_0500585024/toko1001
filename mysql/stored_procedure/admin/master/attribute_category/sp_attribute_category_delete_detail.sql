CREATE DEFINER=`root`@`%` PROCEDURE `sp_attribute_category_delete_detail`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq MediumInt Unsigned
    )
BEGIN

Delete 
From 
	m_attribute_category 
Where 
	category_seq = pSeq;

END