CREATE DEFINER=`root`@`%` PROCEDURE `sp_coupon_by_seq`(
	  pUserID VarChar(25),
	  pIPAddr VarChar(50),
      pSeq int unsigned
)
BEGIN
SELECT 
	seq,
	coupon_code,
	coupon_name,
	coupon_period_from,
	coupon_period_to,
	coupon_limit,
    nominal,
	`status`,
	created_by,
	created_date,
	modified_by,
	modified_date
FROM
	m_coupon
		where
        seq = pSeq;
END