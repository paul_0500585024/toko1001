CREATE DEFINER=`root`@`%` PROCEDURE `sp_coupon_list`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pCurrPage Int,
        pRecPerPage Int,
        pDirSort VarChar(4),
        pColumnSort Varchar(50),
        pCouponCode Varchar(50),
        pStatus char(1),
        pCouponPeriodFrom date,
        pCouponPeriodTo date
    )
BEGIN
    -- Begin variables for Paging
    Declare startRec Int;
    Declare totalPage Int;
    Declare totalRec Int;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";

    If pCouponCode <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And coupon_code like '%" , escape_string(pCouponCode), "%'");
    End If;
    
    If pStatus <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And status = '" , pStatus, "'");
    End If;
    
    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "Select Count(seq) Into @totalRec From m_coupon";
	Set @sqlCommand = Concat(@sqlCommand, " Where (coupon_period_from between '" , 
								pCouponPeriodFrom, "' and '", pCouponPeriodTo, "') or (coupon_period_to between '",
									pCouponPeriodFrom, "' and '", pCouponPeriodTo, "')");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
        SELECT 
			seq,
			coupon_code,
			coupon_name,
			coupon_period_from,
			coupon_period_to,
			coupon_limit,
            nominal,
			`status`,
			'' AS detail,
            '' AS transaction,
			created_by,
			created_date,
			modified_by,
			modified_date
		FROM
			m_coupon
    ";

	Set @sqlCommand = Concat(@sqlCommand, " Where (coupon_period_from between '" , 
									pCouponPeriodFrom, "' and '", pCouponPeriodTo, "') or (coupon_period_to between '",
										pCouponPeriodFrom, "' and '", pCouponPeriodTo, "')");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;

    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);

    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

SELECT pCurrPage, totalPage, totalRec AS total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END