CREATE DEFINER=`root`@`%` PROCEDURE `sp_coupon_product_by_seq`(
	pUserID VarChar(25),
    pIPAddr VarChar(50),
	pCouponSeq int unsigned

)
BEGIN
SELECT 
    cp.coupon_seq,
    cp.product_seq,
    cp.created_by,
    cp.created_date,
    cp.modified_by,
    cp.modified_date,
    p.name as product_name
FROM
    m_coupon_product cp
        JOIN
    m_product p ON cp.product_seq = p.seq
WHERE
    coupon_seq = pCouponSeq;
END