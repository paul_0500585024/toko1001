CREATE DEFINER=`root`@`%` PROCEDURE `sp_coupon_product_delete`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pCouponSeq int unsigned
    )
BEGIN

Delete From m_coupon_product Where coupon_seq = pCouponSeq;

END