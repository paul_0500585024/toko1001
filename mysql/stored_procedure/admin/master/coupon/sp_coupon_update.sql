CREATE DEFINER=`root`@`%` PROCEDURE `sp_coupon_update`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq TinyInt unsigned,
		pCouponCode VARCHAR(50),
		pCouponName VARCHAR(200),
		pCouponPeriodFrom date,
		pCouponPeriodTo date,
		pCouponLimit mediumint unsigned,
        pNominal BIGINT(20)
    )
BEGIN

update m_coupon set
		coupon_code = pCouponCode,
		coupon_name = pCouponName,
        coupon_period_from = pCouponPeriodFrom,
        coupon_period_to = pCouponPeriodTo,
        coupon_limit = pCouponLimit,
        nominal = pNominal,
        modified_by = pUserID,
        modified_date = now()
Where
	seq = pSeq;

END