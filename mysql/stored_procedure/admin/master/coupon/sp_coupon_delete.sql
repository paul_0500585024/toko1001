CREATE DEFINER=`root`@`%` PROCEDURE `sp_coupon_delete`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq int unsigned
    )
BEGIN

Delete From m_coupon Where seq = pSeq;

END