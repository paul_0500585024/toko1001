CREATE DEFINER=`root`@`%` PROCEDURE `sp_coupon_add`(
    pUserID VarChar(25),
    pIPAddr VarChar(50),
	pCouponCode VARCHAR(50),
    pCouponName VARCHAR(200),
    pCouponPeriodFrom date,
    pCouponPeriodTo date,
	pCouponLimit mediumint unsigned,
    pNominal BIGINT(20),
    pStatus char(1)
)
BEGIN
    Declare new_seq SmallInt Unsigned;

SELECT 
    MAX(seq) + 1
INTO new_seq FROM
    m_coupon;

    If new_seq Is Null Then
        Set new_seq = 1;
    End If;

    Insert Into m_coupon(
		seq,
        coupon_code,
		coupon_name,
        coupon_period_from,
        coupon_period_to,
        coupon_limit,
        nominal,
        `status`,
        created_by,
        created_date,
        modified_by,
        modified_date
    ) Values (
        new_seq,
        pCouponCode,
        pCouponName,
        pCouponPeriodFrom,
        pCouponPeriodTo,
        pCouponLimit,
        pNominal,
        pStatus,
        pUserID,
        Now(),
        pUserID,
        Now()
    );
END