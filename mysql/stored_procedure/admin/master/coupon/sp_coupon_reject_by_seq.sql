CREATE DEFINER=`root`@`%` PROCEDURE `sp_coupon_reject_by_seq`(
        pUserID VarChar(25),
		pIPAddr VarChar(50),
        pSeq	int unsigned
        
    )
BEGIN

update m_coupon set    
	`status` = 'R',
	modified_by = pUserID,  
    modified_date = now()
    
Where
	seq = pSeq and `status` = 'N';

END