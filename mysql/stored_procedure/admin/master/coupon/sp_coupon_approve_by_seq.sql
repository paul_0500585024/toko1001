CREATE DEFINER=`root`@`%` PROCEDURE `sp_coupon_approve_by_seq`(
        pUserID VarChar(25),
		pIPAddr VarChar(50),
        pSeq	int unsigned
        
    )
BEGIN

update m_coupon set    
	`status` = 'A',
    modified_by = pUserID,
    modified_date = Now()    
Where
	seq = pSeq AND `status` = 'N';
END