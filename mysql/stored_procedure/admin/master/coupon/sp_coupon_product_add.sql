CREATE DEFINER=`root`@`%` PROCEDURE `sp_coupon_product_add`(
    pUserID VarChar(25),
    pIPAddr VarChar(50),
	pCouponSeq int unsigned,
    pProductSeq bigint unsigned
)
BEGIN
    Insert Into m_coupon_product(
		coupon_seq,
        product_seq,
        created_by,
        created_date,
        modified_by,
        modified_date
    ) Values (
        pCouponSeq,
        pProductSeq,
        pUserID,
        Now(),
        pUserID,
        Now()
    );
END