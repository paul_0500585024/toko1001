CREATE DEFINER=`root`@`%` PROCEDURE `sp_province_update`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pSeq TinyInt unsigned,
    pName VarChar(50),
    pActive Char(1)
    )
BEGIN

update m_province set
	name = pName,
    active = pActive,
    modified_by = pUserID,
    modified_date = now()
Where
	seq = pSeq;

END