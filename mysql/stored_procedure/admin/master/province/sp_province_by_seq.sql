CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_province_by_seq`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
	pSeq TinyInt unsigned
)
BEGIN

	Select 
		seq,
		name,
		active
	From m_province
	Where
		seq = pSeq;
END