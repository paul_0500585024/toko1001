CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_province_add`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pName VarChar(50),
    pActive Char(1)
)
BEGIN
	Declare new_seq tinyint unsigned;

	Select 
		Max(seq) + 1 Into new_seq
	From m_province;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;

	Insert Into m_province(
		seq,
		name,
		active,
		created_by,
		created_date,
		modified_by,
		modified_date
	) Values (
		new_seq,
		pName,
		pActive,
		pUserID,
		Now(),
		pUserID,
		Now()
	);

END