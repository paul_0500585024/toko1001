CREATE DEFINER=`root`@`%` PROCEDURE `sp_city_add`(
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pProvinceSeq TinyInt unsigned,
    pName VarChar(50),
    pActive char(1)
)
BEGIN
    Declare new_seq SmallInt Unsigned;

    Select 
        Max(seq) + 1 Into new_seq
    From m_city;

    If new_seq Is Null Then
        Set new_seq = 1;
    End If;

    Insert Into m_city(
        province_seq,
        seq,
        name,
        active,
        created_by,
        created_date,
        modified_by,
        modified_date
    ) Values (
        pProvinceSeq,
        new_seq,
        pName,
        pActive,
        pUserID,
        Now(),
        pUserID,
        Now()
    );
END