CREATE DEFINER=`root`@`%` PROCEDURE `sp_city_list`(
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pCurrPage Int unsigned,
    pRecPerPage Int unsigned,
    pDirSort VarChar(4),
    pColumnSort Varchar(50),
    pProvinceSeq TinyInt unsigned,
    pName VarChar(50),
    pActive Char(1)

)
BEGIN
    -- Begin variables for Paging
    Declare startRec Int unsigned;
    Declare totalPage Int unsigned;
    Declare totalRec Int unsigned;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
      
    If pName <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And c.name like '%" ,escape_string(pName), "%'");
    End If;

    If pProvinceSeq <> 0 Then
        Set sqlWhere = Concat(sqlWhere, " And p.seq = '" , pProvinceSeq , "'");
    End If;
    
	If pActive <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And c.active='" , pActive, "'");
    End If; 
    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "Select Count(c.seq) Into @totalRec From m_city c Join m_province p On p.seq = c.province_seq";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
        Select
			c.seq,
            c.name,
            p.name as province_name,
            c.active,
            c.created_by,
            c.created_date,
            c.modified_by,
            c.modified_date
		From m_city c
        Join m_province p on
            c.province_seq = p.seq

    ";

    If sqlWhere <> "" Then
		Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;
    
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END