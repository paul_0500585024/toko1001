CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_city_by_seq`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
	pSeq smallint unsigned
)
BEGIN

	Select 
		province_seq,
        seq,
		name,
		active
	From m_city
	Where
		seq = pSeq;
END