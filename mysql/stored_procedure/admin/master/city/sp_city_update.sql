CREATE DEFINER=`root`@`%` PROCEDURE `sp_city_update`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pSeq SmallInt unsigned,
	pProvince_Seq VarChar(50),
    pName VarChar(50),
    pActive Char(1)
    )
BEGIN

update m_city set
	province_seq = pProvince_Seq,
    name = pName,
    active = pActive,
    modified_by = pUserID,
    modified_date = now()
Where
	seq = pSeq;

END