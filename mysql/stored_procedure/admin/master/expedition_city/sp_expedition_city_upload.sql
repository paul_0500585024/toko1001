CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_expedition_city_upload`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
	pExpSeq TinyInt Unsigned,
	pDistrictSeq MediumInt Unsigned,
	pExpDistrictCode VarChar(50)
    )
BEGIN

IF EXISTS (
	SELECT exp_district_code 
    FROM m_expedition_district
	WHERE exp_seq = pExpSeq And district_seq = pDistrictSeq
)
THEN
BEGIN
	UPDATE m_expedition_district
    SET exp_district_code = pExpDistrictCode,
		modified_by = pUserID,
		modified_date = Now()
	WHERE exp_seq = pExpSeq And district_seq = pDistrictSeq;
END;
ELSE
BEGIN
	INSERT INTO m_expedition_district(     
		exp_seq,
		district_seq,
		exp_district_code,
		active,
		created_by,
		created_date,
		modified_by,
		modified_date
	) VALUES (
		pExpSeq,
		pDistrictSeq,
		pExpDistrictCode,
		'1',
		pUserID,
		Now(),
		pUserID,
		Now()
	);
	END;
END IF;
END