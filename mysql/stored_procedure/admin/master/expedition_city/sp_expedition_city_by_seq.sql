CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_expedition_city_by_seq`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
	pExpSeq TinyInt Unsigned,
    pDistrictSeq MediumInt Unsigned
)
BEGIN

	Select
		e.seq as exp_seq,
        e.name as exp_name,
        d.seq as district_seq,
		d.name as district_name,
		ed.exp_district_code
	From m_district d 
		Join m_city c
			on c.seq = d.city_seq
		Join m_province p
			On p.seq = c.province_seq
		Cross Join m_expedition e
		Left Join m_expedition_district ed
			On ed.exp_seq = e.seq And ed.district_seq = d.seq
	Where
		e.seq = pExpSeq And d.seq = pDistrictSeq;
        
END