CREATE DEFINER=`root`@`%` PROCEDURE `sp_expedition_city_list`(
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pCurrPage Int unsigned,
    pRecPerPage Int unsigned,
    pDirSort VarChar(4),
    pColumnSort Varchar(50),
    pExpeditionSeq TinyInt Unsigned,
    pProvinceSeq TinyInt Unsigned,
    pCitySeq MediumInt Unsigned,
    pDistrictSeq MediumInt Unsigned,
    pExpDistrictCode VarChar(50),
    pActive Char(1) 
)
BEGIN
    -- Begin variables for Paging
    Declare startRec Int unsigned;
    Declare totalPage Int unsigned;
    Declare totalRec Int unsigned;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(5000);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
    
    If pExpeditionSeq <> 0 Then
        Set sqlWhere = Concat(sqlWhere, " And e.seq = '" , escape_string(pExpeditionSeq), "'");
    End If;
    
	If pProvinceSeq <> 0 Then
        Set sqlWhere = Concat(sqlWhere, " And p.seq = '" , escape_string(pProvinceSeq), "'");
    End If;
    
    If pCitySeq <> 0 Then
        Set sqlWhere = Concat(sqlWhere, " And c.seq = '" , escape_string(pCitySeq), "'");
    End If;
    
    If pDistrictSeq <> 0 Then
        Set sqlWhere = Concat(sqlWhere, " And d.seq = '" , escape_string(pDistrictSeq), "'");
    End If;
    
    If pExpDistrictCode <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And ed.exp_district_code like '%" , escape_string(pExpDistrictCode), "%'");
    End If;
    
    If pActive <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And e.active = '", pActive ,"'");
    End If;
    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "Select Count(e.seq) Into @totalRec 
					   From m_district d 
					   Join m_city c On c.seq = d.city_seq
					   Join m_province p On p.seq = c.province_seq
					   CROSS Join m_expedition e
					   Left Join m_expedition_district ed On ed.exp_seq = e.seq And ed.district_seq = d.seq
                       ";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1 And e.seq > 0 and e.active = '1' " , sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
        Select
			e.seq as exp_seq,
            d.seq as district_seq,
			e.code as exp_code,
			e.name as exp_name,
			p.seq as province_seq,
			p.name as province_name,
			c.seq as city_seq,
			c.name as city_name,
			d.seq as district_seq,
			d.name as district_name,
			ed.exp_district_code,
            e.active,
            ed.created_by,
            ed.created_date,
            ed.modified_by,
            ed.modified_date
		From m_district d 
			Join m_city c 
				On c.seq = d.city_seq
			Join m_province p 
				On p.seq = c.province_seq
			Cross Join m_expedition e
			Left Join m_expedition_district ed 
				On ed.exp_seq = e.seq And ed.district_seq = d.seq";

    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1 And e.seq > 0 and e.active = '1'" , sqlWhere);
    End If;
    
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END