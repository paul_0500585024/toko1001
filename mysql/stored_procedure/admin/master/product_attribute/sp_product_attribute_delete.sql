CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_attribute_delete`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pSeq smallint unsigned
)
BEGIN

Delete From m_attribute Where seq = pSeq;


END