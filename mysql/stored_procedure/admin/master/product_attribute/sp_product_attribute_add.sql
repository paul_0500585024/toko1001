CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_product_attribute_add`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
	pCategorySeq TinyInt unsigned,
    pName VarChar(50),
	pDisplayName VarChar(50),
	pOrder tinyint unsigned,
	pFilter Char(1),
    pActive Char(1)
)
BEGIN
	Declare new_seq SmallInt Unsigned;

	Select 
		Max(seq) + 1 Into new_seq
	From m_attribute;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;

	Insert Into m_attribute(
		category_seq,
        seq,
		`name`,
        Display_name,
        `Order`,
        Filter,
		active,
		created_by,
		created_date,
		modified_by,
		modified_date
	) Values (
		pCategorySeq,
		new_seq,
		pName,
        pDisplayName,
        POrder,
        Pfilter,
		pActive,
		pUserID,
		Now(),
		pUserID,
		Now()
	);

END