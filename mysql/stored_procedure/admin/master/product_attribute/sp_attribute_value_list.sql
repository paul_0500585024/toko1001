CREATE DEFINER=`sqladm`@`%` PROCEDURE `sp_product_attribute_list`(
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pCurrPage Int,
    pRecPerPage Int,
    pDirSort VarChar(4),
    pColumnSort Varchar(50),
	pCategorySeq smallint unsigned,
    pName VarChar(50),
    pDisplayName VarChar(50),
    pFilter Char(1),
	pActive Char(1)
)
BEGIN
    -- Begin variables for Paging
    Declare startRec Int;
    Declare totalPage Int;
    Declare totalRec Int;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
    
    If pName <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And a.name like '%" ,escape_string(pName), "%'");
    End If;

	If pDisplayName <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And a.display_name like '%" ,escape_string(pDisplayName), "%'");
    End If;

	If pCategorySeq <> 0 Then
        Set sqlWhere = Concat(sqlWhere, " And a.category_seq = '" , pCategorySeq , "'");
    End If;
  
    If pFilter <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And a.filter = '" , pFilter, "'");
    End If; 
    
    If pActive <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And a.active = '" , pActive, "'");
    End If; 

    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "
		Select Count(a.seq) Into @totalRec 
			 From m_attribute a join m_product_category p 
        on a.category_seq = p.seq ";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
        Select
			p.`name` as category_name,
            a.seq,
            a.`name`,
            a.display_name,
            a.`order` as order_search,
            a.filter,
            a.active,
            a.created_by,
            a.created_date,
            a.modified_by,
            a.modified_date
        From m_attribute a join m_product_category p
        on a.category_seq = p.seq 
    ";

    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;
    
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END