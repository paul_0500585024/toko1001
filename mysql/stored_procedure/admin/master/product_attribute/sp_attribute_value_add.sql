CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_attribute_value_add`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
	pAttribute_Seq smallint unsigned,
    pValue VarChar(50),
	pOrder smallint(3),
    pActive Char(1)
)
BEGIN
	Declare new_seq smallint unsigned;

	Select 
		Max(seq) + 1 Into new_seq
	From m_attribute_value;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;

	Insert Into m_attribute_value(
		attribute_seq,
        seq,
		`value`,
        `Order`,
		active,
		created_by,
		created_date,
		modified_by,
		modified_date
	) Values (
		pAttribute_Seq,
		new_seq,
		pValue,
        POrder,
		pActive,
		pUserID,
		Now(),
		pUserID,
		Now()
	);

END