CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_product_attribute_by_seq`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
	pSeq smallint unsigned
)
BEGIN

	Select 
		a.seq,
        p.seq as category_seq,
        p.name as category_name,
        a.name,
		a.display_name,
		a.`order`,
        a.filter,
		a.active
	From m_attribute a join m_product_category p
    on p.seq= a.category_seq
	Where
		a.seq = pSeq;
END