CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_attribute_update`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pSeq smallint unsigned,
	pCategorySeq  tinyint unsigned,
    pName VarChar(50),
	pDisplayName VarChar(50),
	pOrder tinyint unsigned,
	pFilter Char(1),
    pActive Char(1)
    )
BEGIN

update m_attribute set
	category_seq = pCategorySeq,
    `name` = pName,
    display_name = pDisplayName,
    `order` = pOrder,
    filter = pFilter,
    active = pActive,
    modified_by = pUserID,
    modified_date = now()
Where
	seq = pSeq;

END