CREATE DEFINER=`root`@`%` PROCEDURE `sp_attribute_value_delete`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pSeq smallint unsigned
)
BEGIN

Delete From m_attribute_value Where seq = pSeq;

END