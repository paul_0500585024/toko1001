CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_attribute_value_by_seq`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
	pSeq smallint unsigned
)
BEGIN

	Select 
		attribute_seq,
		seq,
        `value`,
		`order`,
		active
	From m_attribute_value
	Where
		seq = pSeq;
END