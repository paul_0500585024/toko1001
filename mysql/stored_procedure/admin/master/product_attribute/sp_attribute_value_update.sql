CREATE DEFINER=`root`@`%` PROCEDURE `sp_attribute_value_update`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pSeq smallint unsigned,
    pValue VarChar(50),
	pOrder smallint(3),
    pActive Char(1)
    )
BEGIN

update m_attribute_value set
    `value` = pValue,
    `order` = pOrder,
    active = pActive,
    modified_by = pUserID,
    modified_date = now()
Where
	seq = pSeq;

END