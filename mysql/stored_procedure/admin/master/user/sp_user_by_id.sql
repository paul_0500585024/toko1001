CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_user_by_id`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
	pAccountID Varchar(25)
)
BEGIN
	Select
		user_id as account_id,
		user_name,
		user_group_seq,
        password,
        email,
        description,
		active
	From m_user
	Where
		user_id = pAccountID;
END