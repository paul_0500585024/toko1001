CREATE DEFINER=`root`@`%` PROCEDURE `sp_user_list`(
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pCurrPage Int,
    pRecPerPage Int,
    pDirSort VarChar(4),
    pColumnSort Varchar(50),
    pAccountID Varchar(25),
    pUserName VarChar(50),
    pUserGroupSeq TinyInt(3),
    pActive Char(1)
)
BEGIN
    -- Begin variables for Paging
    Declare startRec Int;
    Declare totalPage Int;
    Declare totalRec Int;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
      
    If pAccountID <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And u.user_id like '%" , escape_string(pAccountID), "%'");
    End If;
    
    If pUserName <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And u.user_name like '%" , escape_string(pUserName), "%'");
    End If;
    
    If pUserGroupSeq <> 0 Then
        Set sqlWhere = Concat(sqlWhere, " And g.seq = '" , pUserGroupSeq, "'");
    End If;
    
	If pActive <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And u.active='" , pActive, "'");
    End If; 
    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "Select Count(u.user_id) Into @totalRec From m_user u Join m_user_group g On u.user_group_seq = g.seq";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
        Select
            u.user_id,
            u.user_name,
            g.name,
            u.password,
            u.email,
            u.description,
            u.active,
            u.created_by,
            u.created_date,
            u.modified_by,
            u.modified_date
        From m_user u Join m_user_group g
			On u.user_group_seq = g.seq
    ";

    If sqlWhere <> "" Then
		Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;
    
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END