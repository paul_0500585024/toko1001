CREATE DEFINER=`root`@`%` PROCEDURE `sp_user_update`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pOldAccountID VarChar(25),
    pAccountID VarChar(25),
    pUserName VarChar(50),
    pUserGroupSeq TinyInt(3),
    pPassword Varchar(1000),
    pEmail Varchar(100),
    pDescription VarChar(1000),
    pActive Char(1)
    )
BEGIN

if pPassword <> "" then
update m_user set
	password = pPassword
where
	user_id = pOldAccountID;
end if;

update m_user set
	user_id = pAccountID,
    user_name = pUserName,
	user_group_seq = pUserGroupSeq,
    email = pEmail,
	description = pDescription,
	active = pActive,
    modified_by = pUserID,
    modified_date = now()
Where
	user_id = pOldAccountID;
    


END