CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_user_add`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pAccountID VarChar(25),
    pUserName VarChar(50),
    pUserGroupSeq TinyInt unsigned,
    pPassword Varchar(1000),
    pEmail Varchar(100),
    pDescription VarChar(1000),
    pActive Char(1)
)
BEGIN
	Insert Into m_user(
		user_id,
        user_name,
		user_group_seq,
        password,
        email,
        description,
		active,
		created_by,
		created_date,
		modified_by,
		modified_date
	) Values (
		pAccountID,
		pUserName,
		pUserGroupSeq,
		pPassword,
        pEmail,
        pDescription,
        pActive,
		pUserID,
		Now(),
		pUserID,
		Now()
	);

END