CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_attribute_value_by_attribute_seq`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq MEDIUMINT(8) unsigned
    )
BEGIN

	Select 
		attribute_seq,
		seq,
        `value`,
		`order`,
		active
	From m_attribute_value
	Where
		attribute_seq = pSeq order by `order`,`value`;
END