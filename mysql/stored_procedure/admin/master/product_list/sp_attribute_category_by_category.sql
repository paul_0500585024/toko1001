CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_attribute_category_by_category`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pSeq VARCHAR(150)
    )
BEGIN

Set @SQLCommand = "  
		select a.`seq`,a.`name` from m_attribute a where a.`seq` in 
        (select attribute_seq from m_attribute_category where category_seq in (";
        
        
Set @SQLCommand = concat(@SQLCommand, pSeq, ")) order by a.`order`,a.`name`");

  Prepare sql_stmt FROM @SQLCommand;
  Execute sql_stmt;
  Deallocate prepare sql_stmt;       
       
END