DROP PROCEDURE IF EXISTS `sp_partner_group_update`;
DELIMITER $$
CREATE PROCEDURE `sp_partner_group_update` (
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pGroupname varchar(500),
    pGroupSeq INT
)
BEGIN
UPDATE m_partner_group
SET
`partner_group_name` = pGroupname,
`modified_by` = pUserID,
`modified_date` = now()
WHERE `partner_group_seq` = pGroupSeq;
END$$

