-- DROP PROCEDURE IF EXISTS `sp_partner_group_commission_detail_delete`;
DELIMITER $$
CREATE PROCEDURE `sp_partner_group_commission_detail_delete` (
        pUserID VarChar(25),
	pIPAddr VarChar(50),
	pSeq Int unsigned
)
BEGIN

    DELETE FROM 
        m_partner_group_commission 
    WHERE
    partner_group_seq = pSeq;

END$$

