DROP PROCEDURE IF EXISTS `sp_partner_group_delete`;
DELIMITER $$
CREATE PROCEDURE `sp_partner_group_delete` (
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pGroupSeq INT
)
BEGIN
    DELETE FROM m_partner_group
    WHERE partner_group_seq = pGroupSeq;
END$$

