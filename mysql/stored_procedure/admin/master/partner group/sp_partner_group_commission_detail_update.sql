DROP PROCEDURE IF EXISTS `sp_partner_group_commission_detail_update`;
DELIMITER $$
CREATE PROCEDURE `sp_partner_group_commission_detail_update` (
        pUserID VARCHAR(25),
	pIPAddr VARCHAR(50),
	pSeq Int unsigned,
	pCatSeq1 MediumInt unsigned,
	pCatSeq2 MediumInt unsigned,
	pFee Decimal(4,2)
)
BEGIN

 Declare new_seq Int unsigned;

	Insert Into m_partner_group_commission(
		`partner_group_seq`,
		`category_l1_seq`,
		`category_l2_seq`,
		`commission_fee_percent`,
		`created_by`,
		`created_date`,
		`modified_by`,
		`modified_date`
	) Values (
		pSeq,
		pCatSeq1,
		pCatSeq2,
		pFee,
		pUserID,
		Now(),
		pUserID,
		Now()
	);

END$$

