DROP PROCEDURE IF EXISTS `sp_partner_group_add`;
DELIMITER $$
CREATE PROCEDURE `sp_partner_group_add` (
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pGroupname varchar(500)
)
BEGIN
    
    Declare new_seq Int unsigned;
    Select 
	Max(partner_group_seq) + 1 Into new_seq
    From m_partner_group;
    If new_seq Is Null Then
	Set new_seq = 1;
    End If;

    INSERT INTO `tk1001_dev`.`m_partner_group`
    (partner_group_seq,
    partner_group_name,
    created_by,
    created_date,
    modified_by,
    modified_date)
    VALUES
    (new_seq,
     pGroupname,
     pUserID,
     now(),
     pUserID,
     now()
);
END$$

