DROP PROCEDURE IF EXISTS `sp_partner_group_commission_list`;
DELIMITER $$
CREATE PROCEDURE `sp_partner_group_commission_list` (
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pSeq Int unsigned
)
BEGIN

SELECT 
DISTINCT
    a.`seq`,
    a.`name`,
    a.`trx_fee_percent`,
    b.hseq,
    b.hname,
    c.mcatlvl1,
    d.mcatlvl2,
    d.mfee
FROM
    `m_product_category` a
        JOIN
    (SELECT 
        `seq` AS hseq, `name` AS hname, `order` AS horder
    FROM
        `m_product_category`
    WHERE
        level = 1 AND parent_seq = 0 AND seq > 0) b ON a.`parent_seq` = b.hseq
        LEFT OUTER JOIN
    (SELECT 
        category_l1_seq AS mcatlvl1
    FROM
        m_partner_group_commission
    WHERE
        partner_group_seq = pSeq ) c ON a.parent_seq = c.mcatlvl1
        LEFT OUTER JOIN
    (SELECT 
        category_l2_seq AS mcatlvl2, commission_fee_percent AS mfee
    FROM
        m_partner_group_commission
    WHERE
        partner_group_seq = pSeq
            AND category_l2_seq <> 0) d ON a.seq = d.mcatlvl2
WHERE
    a.`level` = 2
ORDER BY b.horder , b.hseq , a.`order` , a.`seq`;

END$$

