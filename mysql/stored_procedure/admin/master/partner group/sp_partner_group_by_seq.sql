DROP PROCEDURE IF EXISTS `sp_partner_group_by_seq`;
DELIMITER $$
CREATE PROCEDURE `sp_partner_group_by_seq` (
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pSeq BIGINT(20) UNSIGNED
)
BEGIN
SELECT 
    partner_group_seq,
    partner_group_name,
    created_by,
    created_date,
    modified_by,
    modified_date
FROM 
    m_partner_group
WHERE
    partner_group_seq = pSeq;
END$$

