CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_product_log_attribute_by_seq`(
        IN pUserID VARCHAR(25),
        IN pIPAddr VARCHAR(50),
        IN pSeq BIGINT,
        IN pLog_seq int(10)
        
    )
BEGIN

 	Select 
    `new_attribute_value_seq`
	From 
    m_product_log_attribute
	Where 
    product_seq = pSeq and log_seq = pLog_seq;
END