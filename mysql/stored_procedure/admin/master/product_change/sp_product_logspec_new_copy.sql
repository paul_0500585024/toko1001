CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_logspec_new_copy`(
         pUserID VARCHAR(25),
         pIPAddr VARCHAR(50),
         pProduct_seq BIGINT(20),
         pLog_seq BIGINT(10)
    )
BEGIN      

DELETE FROM m_product_spec WHERE `product_seq`= pProduct_seq;

insert into m_product_spec  (
product_seq, seq, `name`, `value`, created_by, created_date, modified_by, modified_date
)
select 
product_seq, spec_seq,`name`, `value`, pUserID as created_by, now() as created_date, pUserID as modified_by, now() as modified_date
from m_product_log_spec where product_seq = pProduct_seq and `type` = '2' and log_seq = pLog_seq ;

END