CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_logstatus_product_by_seq`(
pUserid varchar(100),
pIPaddr varchar(20),
pLog_seq int(10)
)
BEGIN

select 
log.status as status ,
log.product_seq as product_seq , 
merchant.seq as merchant_seq ,
merchant.name as merchant_name

from 
m_product_log log,
m_product product,
m_merchant merchant 

where 
log.product_seq = product.seq 
and product.merchant_seq = merchant.seq 
and log.seq = pLog_seq;

END