CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_log_info_by_seq`(
IN pUserID VARCHAR(25),
        IN pIPAddr VARCHAR(50),
        IN pSeq int,        
        IN pLog_seq int(10)
)
BEGIN

select
m_product.status,m_product.merchant_seq,
m_product.category_ln_seq,m_product.category_l2_seq, 
m_log.product_seq, m_log.log_seq, 
m_log.type, m_log.name, 
m_log.include_ins, m_log.description, 
m_log.content, m_log.specification, 
m_log.warranty_notes, m_log.p_weight_kg, 
m_log.p_length_cm, m_log.p_width_cm, 
m_log.p_height_cm, m_log.b_weight_kg, 
m_log.b_length_cm, m_log.b_width_cm, 
m_log.b_height_cm

from 
m_product_log_data m_log , m_product m_product 

where 
m_log.log_seq = pLog_seq 
and m_log.type= '2'
and m_log.product_seq = pSeq 
and m_product.seq = pSeq ; 
END