CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_change_reject`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq BIGINT(20),
        pLog_seq BIGINT(20)
    )
BEGIN

update m_product set `status` = 'L' Where	seq = pSeq ;
update m_product_log set `status` = 'R' where seq = pLog_seq;

END