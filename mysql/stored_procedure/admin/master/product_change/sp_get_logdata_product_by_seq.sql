CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_logdata_product_by_seq`(
pUserid varchar(100),
pIPaddr varchar(20),
pSeq int(10)
)
BEGIN

select  
product_seq,log_seq, 
`type`, `name`, 
include_ins, description, 
content, specification, 
warranty_notes,p_weight_kg, 
p_length_cm, p_width_cm, 
p_height_cm, b_weight_kg, 
b_length_cm, b_width_cm, 
b_height_cm, created_by, 
created_date, modified_by, 
modified_date

from 
m_product_log_data 
where log_seq = pLog_seq 
and type = '2';


END