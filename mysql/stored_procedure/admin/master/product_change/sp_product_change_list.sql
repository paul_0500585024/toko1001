CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_change_list`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pCurrPage Int,
        pRecPerPage Int,
        pDirSort VarChar(4),
        pColumnSort Varchar(50),
        pName VarChar(50),
        pMerchantId INT (10) UNSIGNED,
        pMerchantName VARCHAR (100),
        pStatus CHAR(1)
    )
BEGIN

Declare startRec Int;
    Declare totalPage Int;
    Declare totalRec Int;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";

  
    If pName <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And p.`name` like '%" , escape_string(pName), "%'");
    End If;
    If pMerchantId <> "0" Then
        Set sqlWhere = Concat(sqlWhere, " And m.`seq` = '" , pMerchantId, "'");
    End If; 
    If pStatus <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And log.`status` = '" , escape_string(pStatus), "'");
    End If; 
    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = 
    "Select Count(m.seq) Into @totalRec 
    From `m_product_log` log, 
    m_product p , 
    m_merchant m 
    where 
    log.product_seq = p.seq 
    and 
    m.seq = p.merchant_seq ";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " " , sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

Set @sqlCommand = "
select 
log.seq as log_seq, 
m.name as merchant_name,
m.seq as merchant_seq, 
log.product_seq as product_seq , 
p.name as name_product , 
log.status  , 
log.modified_by,
log.modified_date,
log.created_date,
log.created_by 
from
m_product_log as log 
inner join m_product as p on p.seq = log.product_seq
inner join m_merchant as m on m.seq = log.created_by

";
If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);

    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END