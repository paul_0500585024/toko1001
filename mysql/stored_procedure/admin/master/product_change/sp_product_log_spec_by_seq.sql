CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_log_spec_by_seq`(
IN pUserID VARCHAR(25),
        IN pIPAddr VARCHAR(50),
        IN pSeq int,        
        IN pLog_seq int(10)
)
BEGIN
	select 
	`name`,`value` 

	from 
	m_product_log_spec 

	where 
	log_seq = pLog_seq 
	and product_seq = pSeq 
	and `type` = '2';
END