CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_product_log_variant_by_product`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pLog_Seq int(10)
    )
BEGIN

	Select  
    product_seq, 
    log_seq, 
    old_variant_value_seq, 
    new_variant_value_seq, 
    product_variant_seq, 
    old_product_price, 
    old_disc_percent, 
    old_sell_price, 
    old_order, 
    old_max_buy, 
    old_pic_1_img, 
    old_pic_2_img, 
    old_pic_3_img, 
    old_pic_4_img, 
    old_pic_5_img, 
    old_pic_6_img, 
    new_product_price, 
    new_disc_percent, 
    new_sell_price, 
    new_order, 
    new_max_buy, 
    new_pic_1_img, 
    new_pic_2_img, 
    new_pic_3_img, 
    new_pic_4_img, 
    new_pic_5_img, 
    new_pic_6_img, created_by, created_date, modified_by, modified_date
    From m_product_log_variant 
	Where
	log_seq = pLog_Seq order by product_variant_seq;
END