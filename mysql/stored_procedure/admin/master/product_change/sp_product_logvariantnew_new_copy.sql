CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_product_logvariantnew_new_copy`(
            pUserID VARCHAR(25),
         pIPAddr VARCHAR(50),
         pProduct_seq int(20),
         pLog_seq int(10),   
         pNew_variant_value_seq BIGINT,
         pPic1 varchar(255),
         pPic2 varchar(255),
         pPic3 varchar(255),
         pPic4 varchar(255),
         pPic5 varchar(255),
         pPic6 varchar(255)
)
BEGIN
Declare new_seq bigint unsigned;
Select Max(seq) + 1 Into new_seq From m_product_variant;
insert into m_product_variant (
product_seq, 
variant_value_seq, 
seq, 
product_price, 
disc_percent, 
sell_price, 
`order`, 
max_buy,  
pic_1_img, 
pic_2_img, 
pic_3_img, 
pic_4_img, 
pic_5_img, 
pic_6_img, 
1star, 
2star, 
3star, 
4star, 
5star, 
created_by, 
created_date, 
modified_by, 
modified_date
)
select 
product_seq, 
new_variant_value_seq, 
new_seq, 
new_product_price, 
new_disc_percent, 
new_sell_price, 
new_order, 
new_max_buy, 
pPic1, 
pPic2, 
pPic3, 
pPic4, 
pPic5, 
pPic6, 
'0', 
'0',
'0',
'0',
'0',
created_by, 
now(), 
modified_by,
now() 
from m_product_log_variant 

where product_seq = pProduct_seq and log_seq = pLog_seq and new_variant_value_seq = pNew_variant_value_seq ;

END