CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_change_log_status`(
			pUserID VARCHAR(25),
			pIPAddr VARCHAR(50),			
			pLog_seq BIGINT(10)
)
BEGIN

update m_product_log set `status` = 'A' , auth_by = pUserID, auth_date = now(), modified_by = pUserID , modified_date = now() where seq = pLog_seq;

END