CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_log_product_by_seq`(
pUserid varchar(100),
pIPaddr varchar(20),
pLogSeq int(10)
)
BEGIN

select 
log.seq as seq,
log.product_seq as product_seq,
log.status as `status`, 
log.auth_by, 
log.auth_date, 
log.created_by, 
log.created_date, 
log.modified_by, 
log.modified_date,
p.category_l2_seq,
p.category_ln_seq

from 
m_product_log as log, 
m_product as p 

where log.seq = pLogSeq 
and log.product_seq = p.seq;
END