CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_logattribute_new_copy`(
         pUserID VARCHAR(25),
         pIPAddr VARCHAR(50),
         pProduct_seq BIGINT(20),
         pLog_seq BIGINT(10)
    )
BEGIN      

DELETE FROM m_product_attribute WHERE `product_seq`= pProduct_seq;

insert into m_product_attribute  (
product_seq, attribute_value_seq, created_by, created_date, modified_by, modified_date
)
select 
product_seq, new_attribute_value_seq,pUserID as created_by, now() as created_date, pUserID as modified_by, now() as modified_date from m_product_log_attribute
where product_seq = pProduct_seq and log_seq = pLog_seq;

END