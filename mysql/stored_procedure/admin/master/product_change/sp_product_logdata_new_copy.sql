CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_logdata_new_copy`(
         pUserID VARCHAR(25),
         pIPAddr VARCHAR(50),
         pProduct_seq BIGINT(20),
         pLog_seq BIGINT(10)
    )
BEGIN      

-- DELETE FROM m_product_variant WHERE `product_seq`=pProduct_seq;

update m_product pr
left join (
select 
product_seq ,
name as new_name, 
include_ins as new_include_ins, 
description as new_desc, 
content as new_content, 
warranty_notes as new_warr_notes, 
p_weight_kg as new_pweightkg , 
p_length_cm as new_plengthcm, 
p_width_cm as new_pwidthcm, 
p_height_cm as new_pheightcm, 
b_weight_kg as new_bweightkg, 
b_length_cm as new_blegthcm, 
b_width_cm as new_bwidthcm, 
b_height_cm as new_bheightcm
from m_product_log_data where log_seq = pLog_seq and product_seq = pProduct_seq and type = '2' ) 

 log_pr on log_pr.product_seq = pr.seq

set 
`name` =  log_pr.new_name, 
include_ins = log_pr.new_include_ins, 
description = log_pr.new_desc, 
content = log_pr.new_content, 
warranty_notes = log_pr.new_warr_notes, 
p_weight_kg = log_pr.new_pweightkg, 
p_length_cm = log_pr.new_plengthcm, 
p_width_cm = log_pr.new_pwidthcm, 
p_height_cm = log_pr.new_pheightcm, 
b_weight_kg = log_pr.new_bweightkg, 
b_length_cm = log_pr.new_blegthcm,  
b_width_cm = log_pr.new_bwidthcm, 
b_height_cm = log_pr.new_bheightcm, 
`status` = 'L', 
modified_by = pUserID, 
modified_date = now()
where pr.seq = pProduct_seq;
END