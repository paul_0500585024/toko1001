CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_log_variant_by_seq`(
		IN pUserID VARCHAR(25),
        IN pIPAddr VARCHAR(50),
        IN pSeq int,        
        IN pLog_seq int(10)
)
BEGIN

select 
a.log_seq,a.product_seq,a.new_variant_value_seq, 
b.value,a.product_variant_seq, 
a.new_product_price, a.new_disc_percent, 
a.new_sell_price, a.new_order, 
a.new_max_buy, a.new_pic_1_img, 
a.new_pic_2_img, a.new_pic_3_img, 
a.new_pic_4_img, a.new_pic_5_img, 
a.new_pic_6_img 

from 
m_product_log_variant a

left outer join `m_variant_value` b on a.`new_variant_value_seq`= b.`seq`
    
where 
a.log_seq = pLog_seq 
and a.product_seq = pSeq 
order by b.value asc;
END