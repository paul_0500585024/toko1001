CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_table_seq`(
        IN pTable VARCHAR(100)
    )
BEGIN


set @table_name = pTable;
SET @sql_text = concat('SELECT Max(seq) as seq FROM ',@table_name);

PREPARE stmt FROM @sql_text;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
    
END