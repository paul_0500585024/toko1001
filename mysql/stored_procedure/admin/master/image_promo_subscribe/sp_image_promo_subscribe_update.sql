CREATE DEFINER = 'root'@'%' PROCEDURE `sp_image_promo_subscribe_update`(
        pUserID Varchar(25),
        pIPAddress Varchar(50),
        pSeq INT UNSIGNED,
        pName varchar(200),
        pDate1 DATE,
        pDate2 DATE,
        pText TEXT
    )
BEGIN

  update m_img_promo_subscribe set
	 `promo_name`=pName,
 	 `promo_img_text`=pText,
 	 `promo_img_from_date`=pDate1,
 	 `promo_img_to_date`=pDate2,
	  modified_by = pUserID,
	  modified_date = now() 
  Where
	`seq` = pSeq and `status` = 'N';

END;