CREATE DEFINER = 'root'@'%' PROCEDURE `sp_image_promo_subscribe_delete`(
        pUserID Varchar(25),
        pIPAddress Varchar(50),
        pSeq INT unsigned
    )
BEGIN

DELETE from  
	m_img_promo_subscribe
where
	seq = pSeq and status='N';

END;