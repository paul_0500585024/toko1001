CREATE DEFINER = 'root'@'%' PROCEDURE `sp_image_promo_subscribe_list`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pCurrPage Int,
        pRecPerPage Int,
        pDirSort VarChar(20),
        pColumnSort Varchar(50),
        pPromo Varchar(150),
        pFdate Varchar(10),
        pTdate VarChar(10),
        pStatus Char(1)
    )
BEGIN
    -- Begin variables for Paging
    Declare startRec Int;
    Declare totalPage Int;
    Declare totalRec Int;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
    If pPromo <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And promo_name like '%" , escape_string(pPromo), "%'");
    End If;
	If pFdate <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And promo_img_from_date >= '" , pFdate, "'");
    End If;
    If pTdate <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And promo_img_to_date <= '" , pTdate, "'");
    End If;
    If pStatus <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And status= '" , pStatus, "'");
    End If;
    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "Select Count(seq) Into @totalRec From m_img_promo_subscribe ";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
   	SELECT 
		`seq`,
		`promo_name`,
		`promo_img_text`,
		`promo_img_from_date`,
		`promo_img_to_date`,
		`status`,
		`created_by`,
		`created_date`,
		`modified_by`,
		`modified_date`
	FROM 
		`m_img_promo_subscribe`
    ";

    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);

    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END;