CREATE DEFINER = 'root'@'%' PROCEDURE `sp_image_promo_subscribe_status_update`(
        pUserID Varchar(25),
        pIPAddress Varchar(50),
        pSeq INT UNSIGNED,
        pStatus CHAR(1)
    )
BEGIN

  update m_img_promo_subscribe set
     `status`=pStatus,
	  modified_by = pUserID,
	  modified_date = now() 
  Where
	`seq` = pSeq and `status` = 'N';

END;