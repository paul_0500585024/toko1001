CREATE DEFINER = 'root'@'%' PROCEDURE `sp_image_promo_subscribe_add`(
        pUserID VarChar(100),
        pIPAddr VarChar(50),
        pName varchar(200),
        pDate1 DATE,
        pDate2 DATE,
        pText TEXT
    )
BEGIN
Declare new_seq int unsigned;
	
	Select 
		Max(seq) + 1 Into new_seq
	From m_img_promo_subscribe ;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
    
	Insert Into m_img_promo_subscribe (
  	 `seq`,
 	 `promo_name`,
 	 `promo_img_text`,
 	 `promo_img_from_date`,
 	 `promo_img_to_date`,
  	 `status`,
  	 `created_by`,
 	 `created_date`,
 	 `modified_by`,
	 `modified_date`
  )
	Value (
		new_seq,
        pName,
        pText,
        pDate1,
        pDate2,
        'N',
        pUserID,
        now(),
        pUserID,
        now()
);


END;