CREATE DEFINER = 'root'@'%' PROCEDURE `sp_image_promo_subscribe_by_seq`(
        pUserID Varchar(25),
        pIPAddress Varchar(50),
        pSeq INT unsigned
    )
BEGIN

select	`seq`,
		`promo_name`,
		`promo_img_text`,
		`promo_img_from_date`,
		`promo_img_to_date`,
		`status`,
		`created_by`,
		`created_date`,
		`modified_by`,
		`modified_date`

from 
	m_img_promo_subscribe
where
	seq = pSeq;

END;