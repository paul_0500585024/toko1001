CREATE DEFINER=`root`@`%` PROCEDURE `sp_menu_add`(
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pModuleSeq tinyint(3),
    pParentMenuCD VarChar(25),
    pMenuCD VarChar(25),
    pName VarChar(50),
    pTitleName VarChar(50),
    pURL Varchar(100),
    pIcon Varchar(50),
    pOrder TinyInt unsigned,
    pActive char(1),
    pDetail char(1)
)
BEGIN

declare pLevel TinyInt unsigned;

if pParentMenuCD = '0' then
	Set pParentMenuCD = NULL;
    Set pLevel = 0;
else
	select
		`level` into pLevel
	from s_menu
    where 
		menu_cd = pParentMenuCD;	
end if;

Set pLevel = pLevel + 1;

Insert Into s_menu(
	module_seq,
	parent_menu_cd,
	menu_cd,
	`name`,
    title_name,
	url,
	icon,
	`order`,
	`level`,
	active,
	detail,
	created_by,
	created_date
  
) Values (
	pModuleSeq,
	pParentMenuCD,
	pMenuCD,
	pName,
    pTitleName,
	pURL,
	pIcon,
	pOrder,
	pLevel,
	pActive,
	pDetail,
	pUserID,
	Now()
);
END