CREATE DEFINER=`root`@`%` PROCEDURE `sp_menu_list`(
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pCurrPage Int unsigned,
    pRecPerPage Int unsigned,
    pDirSort VarChar(4),
    pColumnSort Varchar(50),
	pModuleSeq TinyInt unsigned,
    pMenuCD VarChar(25),
    pName VarChar(50),
    pActive Char(1)

)
BEGIN
    -- Begin variables for Paging
    Declare startRec Int unsigned;
    Declare totalPage Int unsigned;
    Declare totalRec Int unsigned;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
      
    If pName <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And m.name like '%" , pName, "%'");
    End If;
    
	If pMenuCD <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And m.menu_cd like '%" , pMenuCD, "%'");
    End If;
    
    If pModuleSeq <> 0 Then
        Set sqlWhere = Concat(sqlWhere, " And mm.seq = '" , pModuleSeq , "'");
    End If;
    
	If pActive <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And m.active='" , pActive, "'");
    End If;
    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "Select Count(m.menu_cd) Into @totalRec From s_menu m Join s_menu_module mm on mm.seq = m.module_seq ";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
        Select
			m.module_seq,
			mm.name as module_name,
			m.menu_cd,            
			m.name,
            m.title_name,
			m.order,
            m.url,
            m.icon,
            m.detail,
			m.active,
			m.created_by,
			m.created_date
		From s_menu m Join s_menu_module mm 
			on m.module_seq = mm.seq

    ";

    If sqlWhere <> "" Then
		Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;
    
    Set @sqlCommand = Concat(@sqlCommand, " order by ", "m.", pColumnSort, " ", pDirSort);
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END