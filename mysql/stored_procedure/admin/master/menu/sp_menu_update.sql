CREATE DEFINER=`root`@`%` PROCEDURE `sp_menu_update`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
	pOldMenuCD varchar(25),
    pOldModuleSeq TinyInt unsigned,
    pModuleSeq TinyInt unsigned,
	pParentMenuCD VarChar(25),
    pMenuCD VarChar(25),
    pName VarChar(50),
    pTitleName VarChar(50),	
    pUrl VarChar(100),
    pIcon VarChar(50),
    pOrder TinyInt unsigned,
    pActive Char(1),
    pDetail Char(1)
    )
BEGIN

declare pLevel TinyInt unsigned;

if pParentMenuCD = '0' then
	Set pParentMenuCD = NULL;
    Set pLevel = 0;
else
	select
		`level` into pLevel
	from s_menu
    where 
		menu_cd = pParentMenuCD;	
end if;

Set pLevel = pLevel + 1;

update s_menu set
	module_seq = pModuleSeq,
    parent_menu_cd = pParentMenuCD,
    menu_cd = pMenuCD,
    `name` = pName,
    title_name = pTitleName,
    url = pUrl,
    icon = pIcon,
    `order` = pOrder,    
    active = pActive,
    level =  pLevel,
    detail = pDetail
Where
	menu_cd = pOldMenuCD and
    module_seq = pModuleSeq;

END