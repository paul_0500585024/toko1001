CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_menu_by_seq`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
	pMenuCD varchar(25),
    pModuleSeq TinyInt unsigned
)
BEGIN

	Select 
		s.module_seq,
        case when s.parent_menu_cd is null then '' else s.parent_menu_cd end as parent_menu_cd,
        case when m.name is null then 'ROOT' else m.name end as parent_menu_name,         
        s.menu_cd,
		s.`name`,
        s.title_name,
        s.url,
        s.icon,        
        s.`order`,
        s.detail,
		s.active
	From s_menu s left join s_menu m
			On m.menu_cd = s.parent_menu_cd
	Where
		s.menu_cd = pMenuCD And
        s.module_seq = pModuleSeq;
END