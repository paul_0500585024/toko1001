CREATE DEFINER = 'root'@'%' PROCEDURE `sp_review_product_admin_update`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pOrderSeq BIGINT unsigned,
        pProductVSeq BIGINT UNSIGNED,
        pReviewAdmin TEXT
    )
BEGIN

	update m_product_review set
		review_admin = pReviewAdmin,
		modified_by = pUserID,
		modified_date = now()
	Where
		order_seq = pOrderSeq and product_variant_seq=pProductVSeq and `status`='N';

END;