CREATE DEFINER = 'root'@'%' PROCEDURE `sp_review_product_admin_rate`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pProductVSeq BIGINT UNSIGNED,
        pStar CHAR(5)
    )
BEGIN
	Declare sqlWhere VarChar(500);
	Set sqlWhere = "";
	Set @sqlCommand = "update m_product_variant set ";
	Set @sqlCommand = Concat(@sqlCommand, pStar," = (" , pStar,"+1)", " Where seq= ",pProductVSeq);

	Prepare sql_stmt FROM @sqlCommand;
	Execute sql_stmt;
	Deallocate prepare sql_stmt; 

END;