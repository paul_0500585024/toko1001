CREATE DEFINER = 'root'@'localhost' PROCEDURE `sp_review_product_list_admin`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pCurrPage INTEGER,
        pRecPerPage INTEGER,
        pDirSort VARCHAR(4),
        pColumnSort VARCHAR(50),
        pStatus CHAR(10),
        pMember BIGINT UNSIGNED
    )
BEGIN
    -- Begin variables for Paging
    Declare startRec Int;
    Declare totalPage Int;
    Declare totalRec Int;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
    If pStatus <> "" Then
        Set sqlWhere = Concat(sqlWhere, " and mpr.status in ('" , pStatus,"')");
    End If;
    If pMember <> "0" Then
        Set sqlWhere = Concat(sqlWhere, " and mpr.created_by='" , pMember,"'");
    End If;
   

    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "Select Count(mpr.status) Into @totalRec From m_product_review as mpr ";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1 " , sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
Select
		t_op.order_seq,
		t_op.merchant_info_seq,
		t_op.product_variant_seq,
		t_op.variant_value_seq,
		t_op.product_status,
		t_op.qty,
		t_op.sell_price,
		t_op.`product_status`,
		t_o.order_no,
		t_o.order_date,
		p.`name` as product_name,
		pv.`pic_1_img`,
		pv.`seq`,
		vv.`value` as variant_value,
		mmi.`merchant_seq`,
        mpr.`created_by`,
        mpr.`rate`,
        mpr.`review`,
        mpr.`review_admin`,
        mpr.`status`,
        mpr.`created_by`,
        mpr.`created_date`,
        mpr.`modified_by`,
        mpr.`modified_date`
		From `t_order_product` t_op
		Join `t_order_merchant` t_om 
		On t_op.order_seq = t_om.order_seq and t_op.`merchant_info_seq` = t_om.`merchant_info_seq`
		Join m_merchant_info mmi 
		On t_op.`merchant_info_seq`=mmi.seq
		Join t_order t_o 
		On t_op.order_seq = t_o.seq
		Join m_product_variant pv 
		On t_op.product_variant_seq = pv.seq
		Join m_product p 
		On pv.product_seq = p.seq
		Join m_variant_value vv
		On pv.variant_value_seq = vv.seq
		left join m_product_review mpr on
		t_o.`seq`=mpr.order_seq and t_op.`product_variant_seq`=mpr.product_variant_seq
    ";

    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1 " , sqlWhere);
    End If;

    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);

    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END;