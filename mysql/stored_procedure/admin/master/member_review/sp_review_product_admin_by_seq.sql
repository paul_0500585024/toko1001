CREATE DEFINER = 'root'@'localhost' PROCEDURE `sp_review_product_admin_by_seq`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pOrderSeq BIGINT unsigned,
        pProductVSeq BIGINT UNSIGNED
    )
BEGIN

Select
	t_op.order_seq,
	t_op.merchant_info_seq,
	t_op.product_variant_seq,
	t_op.variant_value_seq,
	t_op.product_status,
	t_o.order_no,
	t_o.order_date,
	p.`name` as product_name,
	pv.`pic_1_img`,
	pv.`seq`,
	vv.`value` as variant_value,
	mmi.`merchant_seq`,
	mpr.`created_by`,
	mpr.`rate`,
	mpr.`review`,
	mpr.`review_admin`,
	mpr.`status`,
	mpr.`created_by`,
	mpr.`created_date`,
	mpr.`modified_by`,
	mpr.`modified_date`
	From `t_order_product` t_op
	Join `t_order_merchant` t_om 
	On t_op.order_seq = t_om.order_seq and t_op.`merchant_info_seq` = t_om.`merchant_info_seq`
	Join m_merchant_info mmi 
	On t_op.`merchant_info_seq`=mmi.seq
	Join t_order t_o 
	On t_op.order_seq = t_o.seq
	Join m_product_variant pv 
	On t_op.product_variant_seq = pv.seq
	Join m_product p 
	On pv.product_seq = p.seq
	Left Join m_variant_value vv
	On pv.variant_value_seq = vv.seq
	join m_product_review mpr on
	t_o.`seq`=mpr.order_seq and t_op.`product_variant_seq`=mpr.product_variant_seq
where mpr.`order_seq`=pOrderSeq and mpr.`product_variant_seq`=pProductVSeq
;
END;