DROP PROCEDURE `sp_review_product_admin_status`;
CREATE DEFINER = 'root'@'%' PROCEDURE `sp_review_product_admin_status`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pOrderSeq BIGINT unsigned,
        pProductVSeq BIGINT UNSIGNED,
        pStatus CHAR(1),
        pReviewAdmin TEXT
    )
BEGIN

update m_product_review set
	status = pStatus, review_admin=pReviewAdmin
Where
	order_seq = pOrderSeq and product_variant_seq=pProductVSeq and `status`='N';

END;