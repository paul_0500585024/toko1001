CREATE DEFINER = 'root'@'%' PROCEDURE `sp_merchant_trx_fee_log_list`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq Int unsigned
    )
BEGIN
select DISTINCT(modified_date) from m_merchant_trx_fee_log
where merchant_seq=pSeq order by modified_date desc;

END;