CREATE DEFINER=`root`@`%` PROCEDURE `sp_merchant_reject`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
	pSeq Int unsigned
)
BEGIN

	Update m_merchant_new Set
		`status` = 'R'
	Where
		seq = pSeq;

END