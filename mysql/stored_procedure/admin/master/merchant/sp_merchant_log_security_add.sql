CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_merchant_log_security_add`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
	pMseq Int unsigned,
	pType Char(1),
	pCode Char(20),
	pOldPass VarChar(100)
)
BEGIN
	Declare new_seq SmallInt unsigned;

	Select 
		Max(seq) + 1 Into new_seq
	From m_merchant_log_security;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
    
	Insert Into `m_merchant_log_security`(  
		merchant_seq,
		seq,
		`type`,
        code,
		old_pass,
		ip_addr,
		created_by,
		created_date
	) Values (
		pMseq,
		new_seq,
		pType,
        pCode,
		md5(md5(pOldPass)),
		pIPAddr ,
		pUserID,
		Now()
	);

END