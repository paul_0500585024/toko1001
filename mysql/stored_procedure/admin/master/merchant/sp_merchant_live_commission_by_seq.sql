CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_merchant_live_commission_by_seq`(
	pUserID VarChar(50),
    pIPAddr VarChar(50),
    pSeq TinyInt unsigned
)
BEGIN

	Select 
		  a.`seq`,
		  a.`name`,
		  a.`trx_fee_percent`,
		  b.hseq,
		  b.hname,
		  c.mcatlvl1,
		  d.mcatlvl2,
		  d.mfee  
	From `m_product_category` a 
	Inner Join (
		Select 
			`seq` as hseq, `name` as hname, `order` as horder From `m_product_category` 
		Where 
			level = 1 And 
			parent_seq = 0 And 
			seq > 0 
	) b On 
		a.`parent_seq`=b.hseq 
	Left Outer Join (
		Select 
			category_l1_seq as mcatlvl1 
		From `m_merchant_trx_fee` 
		Where 
			`merchant_seq` = pSeq And 
			category_l2_seq = 0 
	) c On 
		a.parent_seq=c.mcatlvl1
	Left Outer Join (
		Select 
			category_l2_seq as mcatlvl2,
			trx_fee_percent as mfee 
		From `m_merchant_trx_fee` 
		Where `merchant_seq` = pSeq And 
		category_l2_seq <> 0
	) d On 
		a.seq=d.mcatlvl2 
	Where 
		a.`level`=2
	Order By 
		b.horder,
		b.hseq,
		a.`order`,
		a.`seq`;
      
END