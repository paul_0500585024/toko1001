DROP PROCEDURE IF EXISTS `sp_promo_commission_merchant_add`;
DELIMITER $$
CREATE PROCEDURE `sp_promo_commission_merchant_add` (
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pPromoSeq SMALLINT(5) UNSIGNED,
    pMerchantSeq SMALLINT(5) UNSIGNED
)
BEGIN

Delete From m_promo_commission_merchant Where merchant_seq = pMerchantSeq;

insert into m_promo_commission_merchant(
    promo_commission_seq,
    merchant_seq,
    created_by,
    created_date
)
VALUES(
    pPromoSeq,
    pMerchantSeq,
    pUserID,
    now()
);

END$$

