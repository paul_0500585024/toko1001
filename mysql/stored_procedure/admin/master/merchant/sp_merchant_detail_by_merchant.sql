CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_merchant_detail_by_merchant`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq Int unsigned
    )
BEGIN

	Select
	`merchant_seq`,
	`seq`,
	`category_l1_seq`,
	`category_l2_seq`,
	`trx_fee_percent`
	From `m_merchant_new_trx_fee`
	Where 
	`merchant_seq` = pSeq ;

END