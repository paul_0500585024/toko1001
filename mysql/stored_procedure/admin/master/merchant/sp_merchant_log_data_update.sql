CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_merchant_log_data_update`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
	pMerchantSeq Int unsigned,
	pSeq SmallInt unsigned
)
BEGIN

	Update m_merchant m 
		Join m_merchant_log_data mld On 
			m.seq = mld.merchant_seq 
		Join m_merchant_log ml On 
			mld.merchant_seq = ml.merchant_seq And 
			mld.log_seq = ml.seq
	Set
		m.address = mld.address,
		m.district_seq = mld.district_seq,
		m.zip_code = mld.zip_code,
		m.phone_no = mld.phone_no,
		m.fax_no = mld.fax_no,
		m.pic1_name = mld.pic1_name,
		m.pic1_phone_no = mld.pic1_phone_no,
		m.pic2_name = mld.pic2_name,
		m.pic2_phone_no = mld.pic2_phone_no,
		m.pickup_addr_eq = mld.pickup_addr_eq,
		m.pickup_addr = mld.pickup_addr,
		m.pickup_district_seq = mld.pickup_district_seq,
		m.pickup_zip_code = mld.pickup_zip_code,
		m.return_addr_eq = mld.return_addr_eq,
		m.return_addr = mld.return_addr,
		m.return_district_seq = mld.return_district_seq,
		m.return_zip_code = mld.return_zip_code,
		m.bank_name = mld.bank_name,
		m.bank_branch_name = mld.bank_branch_name,
		m.bank_acct_no = mld.bank_acct_no,
		m.bank_acct_name = mld.bank_acct_name,
		m.`status` = 'L'
	Where 
		m.seq = pMerchantSeq And 
		mld.type = 2 And 
		ml.seq = pSeq;

END