CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_merchant_detail_copy`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq Int unsigned,
        pMseq Int unsigned
    )
BEGIN

	Insert Into `m_merchant_trx_fee` (
		`merchant_seq`,
		`seq`,
		`category_l1_seq`,
		`category_l2_seq`,
		`trx_fee_percent`,
		`created_by`,
		`created_date`,
		`modified_by`,
		`modified_date`
	)
	Select 
		pMseq,
		`seq`,
		`category_l1_seq`,
		`category_l2_seq`,
		`trx_fee_percent`,
		pUserID,
		Now(),
		pUserID,
		Now()
	From m_merchant_new_trx_fee 
	Where 
		`merchant_seq` = pSeq;

END