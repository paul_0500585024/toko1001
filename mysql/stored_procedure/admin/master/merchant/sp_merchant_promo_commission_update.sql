DROP PROCEDURE IF EXISTS `sp_merchant_promo_commission_update`;
DELIMITER $$
CREATE PROCEDURE `sp_merchant_promo_commission_update` (
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq smallint(5) UNSIGNED,
        pPromoPeriodFrom date,
        pPromoPeriodTo date,
        pAllMerchant char(1),
        pNotes text
)
BEGIN




update m_promo_commission set
    period_from = pPromoPeriodFrom,
    period_to = pPromoPeriodTo,
    all_merchant = pAllMerchant,
    notes = pNotes,
    modified_by = pUserID,
    modified_date = now()
Where
	seq = pSeq and `status`='N';

END$$

