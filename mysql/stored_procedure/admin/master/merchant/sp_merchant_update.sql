CREATE DEFINER=`root`@`%` PROCEDURE `sp_merchant_update`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
	pSeq SmallInt unsigned,
	pName VarChar(100),
	pAddress Text,
	pDistrictSeq SmallInt unsigned,
	pZipCode VarChar(10),
	pPhoneNo VarChar(50),
	pFaxNo VarChar(50),
	pPic1Name VarChar(50),
	pPic1PhoneNo VarChar(50),
	pPic2Name VarChar(50),
	pPic2PhoneNo VarChar(50),
	pExpeditionSeq TinyInt unsigned,
	pPickupAddrEq Char(1),
	pPickupAddr Text,
	pPickupDistrictSeq SmallInt(5),
	pPickupZipCode VarChar(10),
	pReturnAddrEq Char(1),
	pReturnAddr Text,
	pReturnDistrictSeq SmallInt unsigned,
	pReturnZipCode VarChar(10),
	pExpFeePercent TinyInt unsigned,
	pInsFeePercent TinyInt unsigned,
	pNotes Text
)
BEGIN

    If pPickupAddrEq <> "0" Then
		Set pPickupAddr = "";
		Set pPickupDistrictSeq = Null;
		Set pPickupZipCode = "";    
    End If;

    If pReturnAddrEq <> "0" Then
		set pReturnAddr = "";
		set pReturnDistrictSeq = Null;
		set pReturnZipCode = "";    
    End If;
    
	Update m_merchant_new set
		`name`=pName,
		address=pAddress,
		district_seq=pDistrictSeq,
		zip_code=pZipCode,
		phone_no=pPhoneNo,
		fax_no=pFaxNo,
		pic1_name=pPic1Name,
		pic1_phone_no=pPic1PhoneNo,
		pic2_name=pPic2Name,
		pic2_phone_no=pPic2PhoneNo,
		expedition_seq=pExpeditionSeq,
		pickup_addr_eq=pPickupAddrEq,
		pickup_addr=pPickupAddr,
		pickup_district_seq=pPickupDistrictSeq,
		pickup_zip_code=pPickupZipCode,
		return_addr_eq=pReturnAddrEq,
		return_addr=pReturnAddr,
		return_district_seq=pReturnDistrictSeq,
		return_zip_code=pReturnZipCode,
		exp_fee_percent=pExpFeePercent,
		ins_fee_percent=pInsFeePercent,
		`notes`=pNotes,
		modified_by=pUserID,
		modified_date=Now()
	Where
		seq = pSeq And 
		`status`!='R';

END