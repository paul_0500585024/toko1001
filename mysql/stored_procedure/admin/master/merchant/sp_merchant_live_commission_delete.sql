CREATE DEFINER=`root`@`%` PROCEDURE `sp_merchant_live_commission_delete`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
	pSeq tinyint unsigned
)
BEGIN

	Delete From m_merchant_trx_fee 
	Where 
		`merchant_seq` = pSeq;

END