DROP PROCEDURE IF EXISTS `sp_merchant_promo_commission_status`;
DELIMITER $$
CREATE PROCEDURE `sp_merchant_promo_commission_status` (
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq smallint(5) UNSIGNED,
        pStatus Char(1)
)
BEGIN

update m_promo_commission set
    `status` = pStatus,
    modified_by = pUserID,
    modified_date = now()
Where
	seq = pSeq and `status`='N';

END$$
