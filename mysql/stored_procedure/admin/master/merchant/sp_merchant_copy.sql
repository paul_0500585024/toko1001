CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_merchant_copy`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
	pSeq Int unsigned,
	pPassword VarChar(100),
	pCode VarChar(10)
)
BEGIN
	Declare new_seq Int unsigned;
    Declare user_gr_sq Int unsigned;

    Select
		`value` Into user_gr_sq
	From s_setting
	Where
		seq = 10;

	Select 
		Max(seq) + 1 Into new_seq
	From m_merchant;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;

	Insert Into m_merchant(     
		`seq`,
		`user_group_seq`,
		`email`,
		`code`,
		`old_password`,
		`new_password`,
		`name`,
		`address`,
		`district_seq`,
		`zip_code`,
		`phone_no`,
		`fax_no`,
		`pic1_name`,
		`pic1_phone_no`,
		`pic2_name`,
		`pic2_phone_no`,
		`expedition_seq`,
		`pickup_addr_eq`,
		`pickup_addr`,
		`pickup_district_seq`,
		`pickup_zip_code`,
		`return_addr_eq`,
		`return_addr`,
		`return_district_seq`,
		`return_zip_code`,
		`bank_name`, `bank_branch_name`, `bank_acct_no`, `bank_acct_name`,
		`exp_fee_percent`,
		`ins_fee_percent`,
		`logo_img`, `banner_img`, `welcome_notes`,
		`notes`,
		`status`,
		`auth_by`,
		`auth_date`,
		`created_by`,
		`created_date`,
		`modified_by`,
		`modified_date`
	) 
		Select
			new_seq,
			user_gr_sq,
			`email`,
			pCode,'',
			md5(md5(pPassword)),
			`name`,
			`address`,
			`district_seq`,
			`zip_code`,
			`phone_no`,
			`fax_no`,
			`pic1_name`,
			`pic1_phone_no`,
			`pic2_name`,
			`pic2_phone_no`,
			`expedition_seq`,
			`pickup_addr_eq`,
			`pickup_addr`,
			`pickup_district_seq`,
			`pickup_zip_code`,
			`return_addr_eq`,
			`return_addr`,
			`return_district_seq`,
			`return_zip_code`,
			'','','','',
			`exp_fee_percent`,
			`ins_fee_percent`,
			'','','',
			`notes`,
			'L',
			pUserID,
			Now(),
			pUserID,
			Now(),
			pUserID,
			Now()
		From m_merchant_new 
		Where
			seq = pSeq;

	Select new_seq ;
END