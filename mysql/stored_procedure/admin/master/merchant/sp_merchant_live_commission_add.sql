CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_merchant_live_commission_add`(
	pUserID VARCHAR(25),
	pIPAddr VARCHAR(50),
	pSeq Int unsigned,
	pCatSeq1 MediumInt unsigned,
	pCatSeq2 MediumInt unsigned,
	pFee Decimal(4,2)
)
BEGIN

    Declare new_seq Int;

    Select 
        Max(seq) + 1 Into new_seq
    From m_merchant_trx_fee 
	Where 
		`merchant_seq` = pSeq;

    If new_seq Is Null Then
        Set new_seq = 1;
    End If;
	
	Insert Into m_merchant_trx_fee(
		merchant_seq,
		seq,
		category_l1_seq,
		category_l2_seq,
		trx_fee_percent,
		created_by,
		created_date,
		modified_by,
		modified_date
	) Values (
		pSeq,
		new_seq,
		pCatSeq1,
		pCatSeq2,
		pFee,
		pUserID,
		Now(),
		pUserID,
		Now()
	);
    
END