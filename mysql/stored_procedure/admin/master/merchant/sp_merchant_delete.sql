CREATE DEFINER=`root`@`%` PROCEDURE `sp_merchant_delete`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
	pSeq Int unsigned
)
BEGIN
	
	Delete From m_merchant_new
	Where
		seq = pSeq And 
		`status` <> 'R';
END