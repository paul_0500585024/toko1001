CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_merchant_add`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
	pEmail VarChar(50),
	pName VarChar(100),
	pAddress Text,
	pDistrictSeq SmallInt unsigned,
	pZipCode VarChar(10),
	pPhoneNo VarChar(50),
	pFaxNo VarChar(50),
	pPic1Name VarChar(50),
	pPic1PhoneNo VarChar(50),
	pPic2Name VarChar(50),
	pPic2PhoneNo VarChar(50),
	pExpeditionSeq TinyInt unsigned,
	pPickupAddrEq Char(1),
	pPickupAddr TEXT,
	pPickupDistrictSeq SmallInt unsigned,
	pPickupZipCode VarChar(10),
	pReturnAddrEq Char(1),
	pReturnAddr TEXT,
	pReturnDistrictSeq SmallInt unsigned,
	pReturnZipCode VarChar(10),
	pExpFeePercent SmallInt unsigned,
	pInsFeePercent SmallInt unsigned,
	pNotes Text,
	pStatus Char(1)
)
BEGIN
	Declare new_seq Int unsigned;

	Select 
		Max(seq) + 1 Into new_seq
	From m_merchant_new;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
    
    If pPickupAddrEq <> "0" THEN
		set pPickupAddr = "";
		set pPickupDistrictSeq = NULL;
		set pPickupZipCode = "";    
    End If;

    If pReturnAddrEq <> "0" THEN
		set pReturnAddr = "";
		set pReturnDistrictSeq = NULL;
		set pReturnZipCode = "";    
    End If;

	Insert Into m_merchant_new(     
		seq,
		email,
		`name`,
		address,
		district_seq,
		zip_code,
		phone_no,
		fax_no,
		pic1_name,
		pic1_phone_no,
		pic2_name,
		pic2_phone_no,
		expedition_seq,
		pickup_addr_eq,
		pickup_addr,
		pickup_district_seq,
		pickup_zip_code,
		return_addr_eq,
		return_addr,
		return_district_seq,
		return_zip_code,
		exp_fee_percent,
		ins_fee_percent,
		`notes`,
		`status`,
		created_by,
		created_date,
		modified_by,
		modified_date
	) Values (
		new_seq,
		pEmail,
		pName,
		pAddress,
		pDistrictSeq,
		pZipCode,
		pPhoneNo,
		pFaxNo,
		pPic1Name,
		pPic1PhoneNo,
		pPic2Name,
		pPic2PhoneNo,
		pExpeditionSeq,
		pPickupAddrEq,
		pPickupAddr,
		pPickupDistrictSeq,
		pPickupZipCode,
		pReturnAddrEq,
		pReturnAddr,
		pReturnDistrictSeq,
		pReturnZipCode,
		pExpFeePercent,
		pInsFeePercent,
		pNotes,
		pStatus,
		pUserID,
		Now(),
		pUserID,
		Now()
	);

END