DROP PROCEDURE IF EXISTS `sp_promo_commission_merchant_delete_all`;
DELIMITER $$
CREATE PROCEDURE `sp_promo_commission_merchant_delete_all` (
 pUserID VarChar(25),
    pIPAddr VarChar(50),
    pPromoSeq SMALLINT(5) UNSIGNED,
    pMerchantSeq SMALLINT(5) UNSIGNED
)
BEGIN
Delete From m_promo_commission_merchant Where promo_commission_seq = pPromoSeq;
END$$

