CREATE DEFINER = 'root'@'localhost' PROCEDURE `sp_merchant_trx_fee_log_by_seq_date`(
        pUserID VarChar(50),
        pIPAddr VarChar(50),
        pSeq INT unsigned,
        pMdate varchar(20)
    )
BEGIN

	Select 
		  a.`seq`,
		  a.`name`,
		  a.`trx_fee_percent`,
		  b.hseq,
		  b.hname,
		  c.mcatlvl1,
		  d.mcatlvl2,
		  d.mfee ,
          e.parent_seq  
	From `m_product_category` a 
	Inner Join (
		Select 
			`seq` as hseq, `name` as hname, `order` as horder From `m_product_category` 
		Where 
			level = 1 And 
			parent_seq = 0 And 
			seq > 0 
	) b On 
		a.`parent_seq`=b.hseq 
	Left Outer Join (
		Select 
			category_l1_seq as mcatlvl1 
		From `m_merchant_trx_fee_log` 
		Where 
			`merchant_seq` = pSeq And 
			category_l2_seq = 0 
            and modified_date = pMdate
	) c On 
		a.parent_seq=c.mcatlvl1
	Left Outer Join (
		Select 
			category_l2_seq as mcatlvl2,
			trx_fee_percent as mfee 
		From `m_merchant_trx_fee_log` 
		Where `merchant_seq` = pSeq And 
		category_l2_seq <> 0
        and modified_date = pMdate
	) d On 
		a.seq=d.mcatlvl2 
        
left outer join ( select DISTINCT (mpc.parent_seq)  from m_product mp join 
        (select seq,parent_seq from `m_product_category`) 
        mpc on mp.category_l2_seq=mpc.seq where mp.merchant_seq= pSeq group by mpc.parent_seq) e on
        c.mcatlvl1=e.parent_seq        
        
	Where 
		a.`level`=2
	Order By 
		b.horder,
		b.hseq,
		a.`order`,
		a.`seq`;
      
END;