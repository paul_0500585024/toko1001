CREATE DEFINER=`root`@`%` PROCEDURE `sp_merchant_detail_delete`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
	pSeq Int unsigned
)
BEGIN

	Delete From m_merchant_new_trx_fee 
	Where 
		`merchant_seq` = pSeq;

END