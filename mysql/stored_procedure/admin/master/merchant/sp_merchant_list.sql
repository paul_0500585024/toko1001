CREATE DEFINER=`root`@`%` PROCEDURE `sp_merchant_list`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
	pCurrPage Int unsigned,
	pRecPerPage Int unsigned,
	pDirSort VarChar(4),
	pColumnSort Varchar(50),
	pName VarChar(100),
	pEmail VarChar(50),
	pStatus Char(1)
)
BEGIN
    -- Begin variables for Paging
    Declare startRec Int unsigned;
    Declare totalPage Int unsigned;
    Declare totalRec Int unsigned;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";

    If pEmail <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And email like '%" , escape_string(pEmail), "%'");
    End If; 
    If pStatus <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And status = '" , escape_string(pStatus), "'");
    End If;
    If pName <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And name like '%" , escape_string(pName), "%'");
    End If;

    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "Select Count(seq) Into @totalRec From m_merchant_new";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
	Select
		seq,
		email,
		`name`,
		address,
		district_seq,
		zip_code,
		phone_no,
		fax_no,
		pic1_name,
		pic1_phone_no,
		pic2_name,
		pic2_phone_no,
		expedition_seq,
		pickup_addr_eq,
		pickup_addr,
		pickup_district_seq,
		pickup_zip_code,
		return_addr_eq,
		return_addr,
		return_district_seq,
		return_zip_code,
		exp_fee_percent,
		ins_fee_percent,
		notes,
		`status`,
		created_by,
		created_date,
		modified_by,
		modified_date
	From m_merchant_new";

    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);

    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END