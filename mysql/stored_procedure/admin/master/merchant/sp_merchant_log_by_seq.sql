CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_merchant_log_by_seq`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
	pMerchantSeq Int unsigned,
    pLogSeq SmallInt unsigned
)
BEGIN

	Select 
		merchant_seq,
        seq,
		`status`,
        auth_by,
        auth_date
	From m_merchant_log
	Where 
		merchant_seq = pMerchantSeq And
		seq = pLogSeq;
        
	Select merchant_seq and seq;

END