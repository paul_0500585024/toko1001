CREATE DEFINER=`root`@`%` PROCEDURE `sp_merchant_log_reject_by_seq`(
        pUserID VarChar(25),
		pIPAddr VarChar(50),
        pMerchant_seq Int unsigned,
        pSeq SmallInt unsigned
)
BEGIN
	Update m_merchant_log Set
		`status` = 'R',
		auth_by = pUserID,
		auth_date = now()
	Where
		merchant_seq = pMerchant_seq And
		seq = pSeq And
		`status` = 'N';

	Update m_merchant Set
		`status` = 'L'
	Where 
		seq = pMerchant_seq;

END