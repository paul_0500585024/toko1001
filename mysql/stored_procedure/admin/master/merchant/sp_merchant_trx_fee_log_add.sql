CREATE DEFINER = 'root'@'%' PROCEDURE `sp_merchant_trx_fee_log_add`(
        pUserID VarChar(25),
        pIPAddress VarChar(25),
        pSeq Int unsigned
    )
BEGIN

    Declare new_seq Int;
    Declare new_update_seq Int;

    Select 
        Max(seq),Max(update_seq)+1 Into new_seq,new_update_seq
    From `m_merchant_trx_fee_log` 
	Where 
		`merchant_seq` = pSeq;

    If new_seq Is Null Then
        Set new_seq = 0;
        set new_update_seq=1;
    End If;
    
    insert into m_merchant_trx_fee_log (
		merchant_seq,
		seq,
        update_seq,
		category_l1_seq,
		category_l2_seq,
		trx_fee_percent,
		created_by,
		created_date,
		modified_by,
		modified_date
	)  
		select 
        merchant_seq,
		(seq+new_seq),
        new_update_seq,
		category_l1_seq,
		category_l2_seq,
		trx_fee_percent,
		created_by,
		created_date,
		pUserID,
		Now()
        from m_merchant_trx_fee
        Where 
		`merchant_seq` = pSeq 
	;

END;