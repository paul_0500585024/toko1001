DROP PROCEDURE IF EXISTS `sp_merchant_promo_commission_delete`;
DELIMITER $$
CREATE PROCEDURE `sp_merchant_promo_commission_delete` (
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq SMALLINT(5) UNSIGNED
)
BEGIN

Delete From m_promo_commission Where seq = pSeq;

END$$

