DROP PROCEDURE IF EXISTS `sp_get_data_detail_promo_commission`;
DELIMITER $$
CREATE PROCEDURE `sp_get_data_detail_promo_commission` (
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq SMALLINT(5) UNSIGNED
)
BEGIN
    Select
	seq,
        period_from,
        period_to,
        all_merchant,
        notes,
        status,
        created_by,
        created_date,
        modified_by,
        modified_date
    From 
        m_promo_commission
    Where
	seq = pSeq;

select 
    seq,
    `name`,
    email,
    case when pcm.promo_commission_seq is null then '0' else '1' end as `status`
from 
    m_merchant m left join m_promo_commission_merchant pcm
    on m.seq = pcm.merchant_seq and pcm.promo_commission_seq = pSeq;
END$$

