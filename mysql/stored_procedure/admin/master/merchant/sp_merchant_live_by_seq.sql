CREATE DEFINER = 'root'@'localhost' PROCEDURE `sp_merchant_live_by_seq`(
        pUserID VarChar(25),
        pIPAddr Varchar(50),
        pSeq TinyInt unsigned
    )
BEGIN

	Select
		m.seq,
		m.`name`,
        m.email,
		m.address,
		d.`name` as district_name,
        c.`name` as city_name,
        p.`name` as province_name,
		m.zip_code,
		m.phone_no,
		m.fax_no,
		m.pic1_name,
		m.pic1_phone_no,
		m.pic2_name,
		m.pic2_phone_no,
		e.`name` as exp_name,
		m.pickup_addr,
		CASE WHEN dd.`name` IS NULL THEN "-" else dd.`name` END as pickup_district_name,
		CASE WHEN cc.`name` IS Null THEN "-" else cc.`name` END as pickup_city_name,
        CASE WHEN pp.`name` IS Null THEN "-" else pp.`name` END as pickup_province_name,
		m.pickup_zip_code,
		m.return_addr,
        CASE WHEN ddd.`name` IS NULL THEN "-" else ddd.`name` END as return_district_name,
		CASE WHEN ccc.`name` IS Null THEN "-" else ccc.`name` END as return_city_name,
        CASE WHEN ppp.`name` IS Null THEN "-" else ppp.`name` END as return_province_name,
		m.return_zip_code,
        m.bank_name,
        m.bank_branch_name,
        m.bank_acct_no,
        m.bank_acct_name,
		m.exp_fee_percent,
		m.ins_fee_percent,
        m.logo_img,
        m.banner_img,
        m.welcome_notes,
		m.notes,
		m.`status`,
        m.auth_by,
        m.auth_date,
		m.ap_amt
	From 
		m_merchant m
	Join
		m_expedition e On m.expedition_seq = e.seq
	Join
		m_district d On m.district_seq = d.seq
	Join
		m_city c On d.city_seq = c.seq
	Join
		m_province p On c.province_seq = p.seq
	Left Join
		m_district dd On m.pickup_district_seq = dd.seq
	Left Join
		m_city cc On dd.city_seq = cc.seq
	Left Join
		m_province pp On cc.province_seq = pp.seq
	Left Join
		m_district ddd On m.return_district_seq = ddd.seq
	Left Join
		m_city ccc On ddd.city_seq = ccc.seq
    Left Join
		m_province ppp On ccc.province_seq = ppp.seq
	Where
		m.seq = pSeq;
        
END;