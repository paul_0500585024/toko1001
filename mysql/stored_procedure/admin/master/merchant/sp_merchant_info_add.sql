CREATE DEFINER=`root`@`%` PROCEDURE `sp_merchant_info_add`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
	pMseq Int unsigned,
	pPickupAddr Text,
	pPickupDistrictSeq SmallInt unsigned,
	pPickupZipCode VarChar(10),
	pExpFee TinyInt unsigned,
	pInsFee TinyInt unsigned
)
BEGIN

	Declare new_seq TinyInt unsigned;

	Select 
		Max(seq) + 1 Into new_seq
	From m_merchant_info;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
    
	Insert Into `m_merchant_info`(  
		merchant_seq,
		seq,
		pickup_addr,
		pickup_district_seq,
		pickup_zip_code,
		exp_fee_percent,
		ins_fee_percent,
        `bank_name`,
        `bank_branch_name`,
        `bank_acct_no`,
        `bank_acct_name`,
		created_by,
		created_date
	) Values (
		pMseq,
		new_seq,
		pPickupAddr,
		pPickupDistrictSeq,
		pPickupZipCode,
		pExpFee,
		pInsFee,
        '',
        '',
        '',
        '',
		pUserID,
		Now()
	);

END