CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_merchant_new_by_seq`(
	 pUserID VarChar(25),
	 pIPAddr VarChar(50),
	 pSeq Int unsigned
)
BEGIN

	Select
		seq,
		email,
		`name`,
		address,
		district_seq,
		zip_code,
		phone_no,
		fax_no,
		pic1_name,
		pic1_phone_no,
		pic2_name,
		pic2_phone_no,
		expedition_seq,
		pickup_addr_eq,
		pickup_addr,
		pickup_district_seq,
		pickup_zip_code,
		return_addr_eq,
		return_addr,
		return_district_seq,
		return_zip_code,
		exp_fee_percent,
		ins_fee_percent,
		notes,
		`status`,
		created_by,
		created_date,
		modified_by,
		modified_date
	From m_merchant_new
	Where
		seq = pSeq;

END