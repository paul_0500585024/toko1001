DROP PROCEDURE IF EXISTS `sp_get_merchant_promo_commission_by_seq`;
DELIMITER $$
CREATE PROCEDURE `sp_get_merchant_promo_commission_by_seq` (
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq SMALLINT(5) UNSIGNED
)
BEGIN
    Select
	seq,
        period_from,
        period_to,
        all_merchant,
        notes,
        status,
        created_by,
        created_date,
        modified_by,
        modified_date
    From 
        m_promo_commission
    Where
	seq = pSeq;


END$$

