DROP PROCEDURE IF EXISTS `sp_get_merchant_promo_commission_list_admin`;
DELIMITER $$
CREATE PROCEDURE `sp_get_merchant_promo_commission_list_admin` (
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pCurrPage Int,
    pRecPerPage Int,
    pDirSort VarChar(4),
    pColumnSort Varchar(50),
    pPromoPeriodFrom date,
    pPromoPeriodTo date,
    pStatus char(1)
    
)
BEGIN
    -- Begin variables for Paging
    Declare startRec Int;
    Declare totalPage Int;
    Declare totalRec Int;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";


    If pStatus <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And status = '",pStatus,"'");
    End If;



     


    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "Select Count(seq) Into @totalRec From m_promo_commission";
    set @sqlCommand = Concat(@sqlCommand, " Where ('", pPromoPeriodFrom , "' between period_from and period_to  or '", pPromoPeriodTo , "' between period_from and period_to) ");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
            Select
                    seq,
                    period_from,
                    period_to,
                    all_merchant,
                    notes,
                    status,
                    created_by,
                    created_date,
                    modified_by,
                    modified_date
            From 
                    m_promo_commission
    ";

    set @sqlCommand = Concat(@sqlCommand, " Where ('", pPromoPeriodFrom , "' between period_from and period_to or '", pPromoPeriodTo , "' between period_from and period_to) ");

    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;

    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);

    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END$$

