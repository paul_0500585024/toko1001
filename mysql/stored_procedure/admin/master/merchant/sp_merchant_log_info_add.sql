CREATE DEFINER=`root`@`%` PROCEDURE `sp_merchant_log_info_add`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pMerchantSeq Int unsigned
)
BEGIN
	Declare new_seq BigInt unsigned;

	Select
		Max(seq) + 1 Into new_seq
	From m_merchant_info;

    If new_seq Is Null Then
        Set new_seq = 1;
    End If;

	Insert Into m_merchant_info(
		merchant_seq, 
        seq,
        pickup_addr,
        pickup_district_seq,
        pickup_zip_code,
        exp_fee_percent,
        ins_fee_percent,
        bank_name,
        bank_branch_name,
        bank_acct_no,
        bank_acct_name,
        created_by,
        created_date
    ) 
	Select 
		seq,
		new_seq,
		Case When pickup_addr_eq = '1' Then address Else pickup_addr End pickup_addr,
		Case When pickup_addr_eq = '1' Then district_seq Else pickup_district_seq End pickup_district_seq,
		Case When pickup_addr_eq = '1' Then zip_code Else pickup_zip_code End pickup_zip_code,
		exp_fee_percent, 
		ins_fee_percent,
		bank_name, 
		bank_branch_name, 
		bank_acct_no,
		bank_acct_name,
		pUserID,
		now()
	From m_merchant
	Where
		seq = pMerchantSeq;

END