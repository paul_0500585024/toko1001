CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_merchant_by_email`(
        pEmail VarChar(50)
    )
BEGIN

	Select
		seq,
		email
	From m_merchant_new
	Where
		email = pEmail
	Union All
	Select
		seq,
		email
	From m_merchant
	Where
		email = pEmail;

END