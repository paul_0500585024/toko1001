CREATE DEFINER=`root`@`%` PROCEDURE `sp_merchant_live_update`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
	pSeq TinyInt unsigned,
	pExpFeePercent TinyInt unsigned,
	pInsFeePercent TinyInt unsigned,
	pNotes Text
)
BEGIN
    
	Update m_merchant Set
		exp_fee_percent = pExpFeePercent,
		ins_fee_percent = pInsFeePercent,
		`notes` = pNotes,
		modified_by = pUserID,
		modified_date = Now()
	Where seq = pSeq;

END