DROP PROCEDURE IF EXISTS `sp_merchant_promo_commission_add`;
DELIMITER $$
CREATE PROCEDURE `sp_merchant_promo_commission_add` (
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pPromoPeriodFrom date,
    pPromoPeriodTo date,
    pAllMerchant char(1),
    pNotes text
)
BEGIN

Declare new_seq TinyInt;
	Select 
		Max(seq) + 1 Into new_seq
	From m_promo_commission;
	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
	Insert Into m_promo_commission(     
		seq,
		period_from,
                period_to,
                all_merchant,
                notes,
                status,
                created_by,
                created_date,
                modified_by,
                modified_date
	) Values (
		new_seq,
                pPromoPeriodFrom,
                pPromoPeriodTo,
                pAllMerchant,
                pNotes,
                'N',
                pUserID,
                now(),
                pUserID,
                now()
	);
END$$

