CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_merchant_log_data_by_seq`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
	pMerchantSeq Int unsigned,
	pLogSeq SmallInt unsigned
)
BEGIN
	Select
		ml.`status`,
		mld.`type`,
		mld.address, 
		mld.district_seq, 
		mld.zip_code, 
		mld.phone_no, 
		mld.fax_no, 
		mld.pic1_name, 
		mld.pic1_phone_no, 
		mld.pic2_name, 
		mld.pic2_phone_no,
		mld.pickup_addr_eq,
		mld.pickup_addr,
		mld.pickup_district_seq,
		mld.pickup_zip_code,
		mld.return_addr_eq,
		mld.return_addr,
		mld.return_district_seq,
		mld.return_zip_code,
		mld.bank_name,
		mld.bank_branch_name,
		mld.bank_acct_no,
		mld.bank_acct_name,
		/* */
		p.name as province_name,
		c.name as city_name,
		d.name as district_name,
		/* */
		pp.name as pickup_province_name,
		pc.name as pickup_city_name,
		pd.name as pickup_district_name,
		/* */
		mpp.name as return_province_name,
		mpc.name as return_city_name,
		mpd.name as return_district_name
	From m_merchant_log ml 
	join m_merchant_log_data mld
		on ml.merchant_seq = mld.merchant_seq 
			AND ml.seq = mld.log_seq
	join m_district d 
		on mld.district_seq=d.seq
	join m_city c 
		on d.city_seq=c.seq
	join m_province p 
		on c.province_seq = p.seq
	left join m_district pd 
		on mld.pickup_district_seq=pd.seq
	left join m_city pc 
		on pd.city_seq=pc.seq
	left join m_province pp 
		on pc.province_seq = pp.seq
	left outer join m_district mpd 
		on mld.return_district_seq=mpd.seq
	left outer join m_city mpc 
		on mpd.city_seq=mpc.seq
	left outer join m_province mpp 
		on mpc.province_seq = mpp.seq
	Where 
		mld.merchant_seq = pMerchantSeq And  
		mld.log_seq = pLogSeq;

END