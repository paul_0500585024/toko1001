CREATE DEFINER=`root`@`%` PROCEDURE `sp_bank_list`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pCurrPage Int unsigned,
        pRecPerPage Int unsigned,
        pDirSort VarChar(4),
        pColumnSort Varchar(50),
        pBankName Varchar(50),
        pBankBranchName Varchar(50),
        pBankAcctNo Varchar(50),
        pBankAcctName Varchar(50),
        pActive Char(1)
    )
BEGIN
    -- Begin variables for Paging
    Declare startRec Int unsigned;
    Declare totalPage Int unsigned;
    Declare totalRec Int unsigned;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";

    If pBankName <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And bank_name like '%" , escape_string(pBankName), "%'");
    End If;
    If pBankBranchName <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And bank_branch_name like '%" , escape_string(pBankBranchName), "%'");
    End If;
    If pBankAcctNo <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And bank_acct_no like '%" , escape_string(pBankAcctNo), "%'");
    End If;
    If pBankAcctName <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And bank_acct_name like '%" , escape_string(pBankAcctName), "%'");
    End If;
    If pActive <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And active = '", pActive ,"'");
    End If;

    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "Select Count(seq) Into @totalRec From m_bank_account";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
        Select
            seq,
			bank_name,
			bank_branch_name,
			bank_acct_no,
			bank_acct_name,
			active,
            created_by,
            created_date,
            modified_by,
            modified_date
        From m_bank_account
    ";

    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);

    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END