CREATE DEFINER=`root`@`%` PROCEDURE `sp_bank_update`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq TinyInt unsigned,
        pBankName VarChar(50),
        pBankBranchName VarChar(50),
        pBankAcctNo VarChar(50),
        pBankAcctName VarChar(50),
        pActive Char(1),
        pLogoImg varchar(45)
    )
BEGIN

update m_bank_account set
    bank_name = pBankName,
    bank_branch_name = pBankBranchName,
    bank_acct_no = pBankAcctNo,
    bank_acct_name = pBankAcctName,
    active = pActive,
    logo_img=pLogoImg,
    modified_by = pUserID,
    modified_date = now()
Where
	seq = pSeq;

END