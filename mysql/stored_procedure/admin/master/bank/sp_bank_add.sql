CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_bank_add`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pBankName VarChar(50),
        pBankBranchName VarChar(50),
        pBankAcctNo VarChar(50),
        pBankAcctName VarChar(50),
        pActive Char(1),
        pLogoImg VARCHAR(45)
    )
BEGIN
	Declare new_seq TinyInt unsigned;

	Select 
		Max(seq) + 1 Into new_seq
	From m_bank_account;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;

	Insert Into m_bank_account(     
		seq,
		bank_name,
        bank_branch_name,
        bank_acct_no,
        bank_acct_name,
		active,
        logo_img,
		created_by,
		created_date,
		modified_by,
		modified_date
	) Values (
		new_seq,
		pBankName,
        pBankBranchName,
        pBankAcctNo,
        pBankAcctName,
		pActive,
        pLogoImg,
		pUserID,
		Now(),
		pUserID,
		Now()
	);

END