CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_bank_by_seq`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq tinyint unsigned
    )
BEGIN

	Select 
        seq,
		bank_name,
        bank_branch_name,
        bank_acct_no,
        bank_acct_name,
		active,
        logo_img
		From m_bank_account
	Where
		seq = pSeq;
END