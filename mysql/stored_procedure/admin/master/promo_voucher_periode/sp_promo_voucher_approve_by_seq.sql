CREATE DEFINER=`root`@`%` PROCEDURE `sp_promo_voucher_approve_by_seq`(
        pUserID VarChar(25),
		pIPAddr VarChar(50),
        pSeq	smallint unsigned
        
        
    )
BEGIN

update m_promo_voucher_period set    
	`status` = 'A',
    auth_by = pUserID,
    auth_date = Now()    
Where
	seq = pSeq AND `status` = 'N';

END