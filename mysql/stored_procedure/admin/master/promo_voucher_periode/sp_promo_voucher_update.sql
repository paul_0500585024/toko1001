CREATE DEFINER=`root`@`%` PROCEDURE `sp_promo_voucher_update`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq smallint unsigned,
        pMemberSeq bigint unsigned,
        pActiveDate date,
        pExpDate date
    )
BEGIN
	UPDATE m_promo_voucher 
SET 
    member_seq = pMemberSeq,
    active_date = pActiveDate,
    exp_date = pExpDate
WHERE
    seq = pSeq;
END