CREATE DEFINER=`root`@`%` PROCEDURE `sp_promo_voucher_period_list`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pCurrPage Int,
        pRecPerPage Int,
        pDirSort VarChar(4),
        pColumnSort Varchar(50),
		pName varchar(50),
        pCode varchar(50),
        pNodeCd char(3),
		pActive char(1),
        pStatus Char(3),
        pDateFrom date,
        pDateTo date
    )
BEGIN
    -- Begin variables for Paging
    Declare startRec Int;
    Declare totalPage Int;
    Declare totalRec Int;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";

    If pName <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And p.name like '%" , escape_string(pName), "%'");
    End If;
    
    If pCode <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And p.code like '%" , escape_string(pCode), "%'");
    End If;
    
    If pNodeCd <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And n.`code` = '" , pNodeCd , "'");
    End If;
    
    If pActive <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And p.active = '", pActive ,"'");
    End If;
	If pStatus <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And status = '" , pStatus, "'");
    End If;
    
    If pDateFrom <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And date_from >='" , pDateFrom , "'");
    End If;
    
    If pDateTo <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And date_to <='" , pDateTo , "'");
    End If;
    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "Select Count(seq) Into @totalRec From m_promo_voucher_period p join s_promo_voucher_node n on n.code = p.node_cd";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
        Select
			p.seq,
			p.date_from,
			p.date_to,
			p.`name`,
			p.`code`,
			n.name as node_name,
			p.`type`,
			p.voucher_count,
			p.nominal,
			p.exp_days,
			p.trx_get_amt,
			p.trx_use_amt,
			p.notes,
			p.`status`,
			p.auth_by,
			p.auth_date,
			p.active
        From m_promo_voucher_period p join s_promo_voucher_node n on
        p.node_cd = n.code
    ";

    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);

    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

SELECT pCurrPage, totalPage, totalRec AS total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END