CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_promo_voucher_period_add`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
    pDateFrom date,
	pDateTo  date,
    pName varchar(50),
    pCode varchar(10),
    pNodeCd varchar(5),
    pType char(1),
    pVoucherCount smallint unsigned,
    pNominal decimal(10,0),
    pExp_days smallint unsigned,
    pTrxGetAmt decimal(10,0),
    pTrxUseAmt decimal(10,0),
    pNotes varchar (1000),
    pActive char(1)
    
)
BEGIN
	Declare new_seq smallint unsigned;
	
	Select 
		Max(seq) + 1 Into new_seq
	From m_promo_voucher_period;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
    
    Insert Into m_promo_voucher_period(
		seq,
        date_from,
        date_to,
        `name`,
        `code`,
        node_cd,
        `type`,
        voucher_count,
        nominal,
        exp_days,
        trx_get_amt,
        trx_use_amt,
        notes,
        `status`,
        auth_by,
        auth_date,
        active,
        created_by,
		created_date,
		modified_by,
		modified_date
    )
    value (
		new_seq,
        pDateFrom,
        pDateTo,
        pName,
        pCode,
        pNodeCd,
        pType,
        pVoucherCount,
        pNominal,
        pExp_days,
        pTrxGetAmt,
        pTrxUseAmt,
        pNotes,
        'N',
        'N',
        NOW(),
        pActive,
        pUserID,
		Now(),
		pUserID,
		Now()
    );
END