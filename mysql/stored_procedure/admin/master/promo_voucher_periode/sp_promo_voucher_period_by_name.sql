CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_promo_voucher_period_by_name`(
      pNodeCd Varchar(10),
      pTotalOrder BigInt unsigned
    )
BEGIN
	Select
		seq,
        date_from,
        date_to,
        `name`,
        `code`,
        node_cd,
        `type`,
		voucher_count,
		nominal,
		exp_days,
		trx_get_amt,
		trx_use_amt,
		notes,
		`status`,
		active
	From m_promo_voucher_period
	Where
		node_cd = pNodeCd AND `status` ='A' AND  Now() BETWEEN date_from AND date_to and
        trx_get_amt <= pTotalOrder
		order by date_from asc limit 1;
END