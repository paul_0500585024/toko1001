CREATE DEFINER=`root`@`%` PROCEDURE `sp_promo_voucher_by_voucher_seq`(
		pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq int unsigned
)
BEGIN
SELECT 
    v.promo_seq,
    v.member_seq,
    v.`code`,
    v.seq as voucher_seq,
    v.nominal,
    v.active_date,
    v.exp_date,
    v.trx_use_amt,
    v.trx_no,
    m.email,
    m.seq,
    m.name,
    vp.type,
    vp.node_cd,
    vp.exp_days
FROM
    m_promo_voucher v
        LEFT JOIN
    m_member m ON v.member_seq = m.seq
		JOIN
	m_promo_voucher_period vp ON v.promo_seq = vp.seq
WHERE
	v.seq = pSeq;
END