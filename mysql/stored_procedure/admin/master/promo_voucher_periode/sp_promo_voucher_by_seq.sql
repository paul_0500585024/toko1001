CREATE DEFINER=`sqladm`@`%` PROCEDURE `sp_promo_voucher_by_seq`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq	int unsigned
    )
BEGIN
select 
	promo_seq,	
	seq ,
    member_seq,
	`code`,
    nominal,
    active_date,
    exp_date,
    trx_use_amt,
    trx_no
from 
	m_promo_voucher
Where
	seq = pSeq ;
END