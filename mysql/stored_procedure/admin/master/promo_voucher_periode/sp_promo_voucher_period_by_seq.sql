CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_promo_voucher_period_by_seq`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq mediumint unsigned
    )
BEGIN

	Select
			pvp.seq,
			pvp.date_from,
			pvp.date_to,
			pvp.`name`,
			pvp.`code`,
			pvp.node_cd,
			pvp.`type`,
			pvp.voucher_count,
			pvp.nominal,
			pvp.exp_days,
			pvp.trx_get_amt,
			pvp.trx_use_amt,
			pvp.notes,
			pvp.`status`,
			pvp.auth_by,
			pvp.auth_date,
			pvp.active,
			pvp.created_by,
			pvp.created_date,
			pvp.modified_by,
			pvp.modified_date,
            pvn.`name` as node_name
		From m_promo_voucher_period pvp join s_promo_voucher_node pvn
			on pvp.node_cd = pvn.code
	Where
		seq = pSeq;
END