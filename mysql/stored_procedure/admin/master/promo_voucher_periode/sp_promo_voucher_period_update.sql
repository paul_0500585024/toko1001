CREATE DEFINER=`root`@`%` PROCEDURE `sp_promo_voucher_period_update`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq smallint(5),
        pDateFrom date,
		pDateTo date,
		pName varchar(50),
		pCode varchar(10),
		pNodeCd varchar(5),
		pType char(1),
		pVoucherCount smallint unsigned,
		pNominal decimal(10,0),
		pExpdays smallint unsigned,
		pTrxGetAmt decimal(10,0),
		pTrxUseAmt decimal(10,0),
		pNotes varchar (1000),
		pActive char(1)
    )
BEGIN

update m_promo_voucher_period set   
	date_from = pDateFrom,
    date_to = pDateTo,
    `name` = pName,
    `code` = pCode,
    node_cd = pNodeCd,
    `type` = pType,
    voucher_count = pVoucherCount,
    nominal = pNominal,
    exp_days = pExpdays,
    trx_get_amt = pTrxGetAmt,
	trx_use_amt = pTrxUseAmt,
    notes = pNotes,
	`status` = 'N',
    active = pActive,
    modified_by = pUserID,
    modified_date = now()
Where
	seq = pSeq;

END