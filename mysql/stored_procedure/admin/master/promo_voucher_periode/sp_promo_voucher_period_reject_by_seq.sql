CREATE DEFINER=`root`@`%` PROCEDURE `sp_promo_voucher_period_reject_by_seq`(
        pUserID VarChar(25),
		pIPAddr VarChar(50),
        pSeq	smallint unsigned
        
    )
BEGIN

update m_promo_voucher_period set    
	`status` = 'R',
	auth_by = pUserID,  
    auth_date = now()
    
Where
	seq = pSeq and `status` = 'N';

END