CREATE DEFINER=`root`@`%` PROCEDURE `sp_promo_voucher_period_delete`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq smallint unsigned
    )
BEGIN

Delete From m_promo_voucher_period Where seq = pSeq;

END