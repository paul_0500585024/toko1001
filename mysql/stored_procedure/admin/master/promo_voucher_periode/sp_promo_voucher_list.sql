CREATE DEFINER=`root`@`%` PROCEDURE `sp_promo_voucher_list`(
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pCurrPage Int,
    pRecPerPage Int,
    pDirSort VarChar(4),
    pColumnSort Varchar(50),
	pPromoSeq smallint unsigned,
    pFrom mediumint unsigned,
    pTo mediumint unsigned
)
BEGIN
    -- Begin variables for Paging
    Declare startRec Int;
    Declare totalPage Int;
    Declare totalRec Int;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
    Set sqlWhere = Concat(sqlWhere, " And promo_seq = '", pPromoSeq ,"'"); 
    
	If pFrom <> 0 Then
        Set sqlWhere = Concat(sqlWhere, " And p.nominal >='" , pFrom , "'");
    End If;
    If pTo <> 0 Then
        Set sqlWhere = Concat(sqlWhere, " And p.nominal <='" , pTo , "'");
    End If;
    -- Begin Paging Info
    Set @sqlCommand = "Select Count(p.seq) Into @totalRec From m_promo_voucher p left join m_member m on m.seq = p.member_seq";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
         Select
            p.promo_seq,
			p.seq,
			p.`code`,
            p.member_seq,
			p.nominal,
			p.active_date,
			p.exp_date,
            p.trx_use_amt,
            p.trx_no,
            m.name as member_name 
        From m_promo_voucher p left join m_member m
			on p.member_seq = m.seq
    ";

    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;
    
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END