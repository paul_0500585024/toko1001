CREATE DEFINER=`root`@`%` PROCEDURE `sp_promo_voucher_add`(
        pUserID VarChar(25),
		pIPAddr VarChar(50),
        pPromoSeq smallint unsigned,
        pNominal DECIMAL(10,0),
        pTrxUseAmt DECIMAL(10,0),
		pCode 	varchar(100)
        
        
    )
BEGIN

Declare new_seq smallint unsigned;
	
	Select 
		Max(seq) + 1 Into new_seq
	From m_promo_voucher;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
    
    Insert Into m_promo_voucher (
    promo_seq,
    seq,
	member_seq,
    `code`,
     nominal,
    active_date,
    exp_date,
	trx_use_amt,
    trx_no,
    created_by ,
    created_date,
    modified_by,
    modified_date
    )
    value (
    pPromoSeq,
	new_seq,
    NULL,
    pCode,
    pNominal,
    DEFAULT,
    DEFAULT,
    pTrxUseAmt,
    trx_no,
    pUserID,
    now(),
    pUserID,
    now()
);

END