CREATE DEFINER=`root`@`%` PROCEDURE `sp_member_approve_by_seq`(
        pUserID VarChar(25),
		pIPAddr VarChar(50),
        pSeq	bigint unsigned,
        pPassword varchar(100)
        
    )
BEGIN

update m_member set    
	`status` = 'A',
    new_password= pPassword,
    modified_by = pUserID,
    modified_date = Now()    
Where
	seq=pSeq AND `status` = 'U';
    
END