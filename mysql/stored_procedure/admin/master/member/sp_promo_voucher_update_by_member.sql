CREATE DEFINER=`root`@`%` PROCEDURE `sp_promo_voucher_update_by_member`(
        pMemberSeq bigint unsigned,
		pActiveDate date,
        pExpDate date
    )
BEGIN
UPDATE m_promo_voucher SET	
    active_date = pActiveDate,
    exp_date = pExpDate
WHERE 
	member_seq =pMemberSeq;
END