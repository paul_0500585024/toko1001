CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_member_by_seq`(
        pUserID VarChar(100),
        pIPAddr VarChar(50),
        pSeq int(10) unsigned
    )
BEGIN

Select
	seq,
    email,
    `name`,
    birthday,
    gender,
    mobile_phone,
    bank_name,
    bank_branch_name,
    bank_acct_no,
    bank_acct_name,
    profile_img,
    deposit_amt,
    status,
    created_by,
    created_date,
    modified_by,
    modified_date
From 
	m_member
Where
	seq = pSeq;
    
END