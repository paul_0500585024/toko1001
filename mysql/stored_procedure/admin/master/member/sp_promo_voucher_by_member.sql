CREATE DEFINER=`root`@`%` PROCEDURE `sp_promo_voucher_by_member`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pMemberSeq	int unsigned
    )
BEGIN
SELECT 
    pv.promo_seq,
    pv.seq,
    pv.member_seq,
    pv.`code`,
    pv.nominal,
    pv.active_date,
    pv.exp_date,
    pv.trx_use_amt,
    pv.trx_no,
    pvp.exp_days
FROM
    m_promo_voucher pv
        JOIN
    m_promo_voucher_period pvp ON pv.promo_seq = pvp.seq
WHERE
    member_seq = pMemberSeq ;

END