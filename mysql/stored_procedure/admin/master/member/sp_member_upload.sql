CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_member_upload`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pEmail VARCHAR(50),
        pName varchar(100),
        pBirthday date,
        pGender char(1),
        pMobilePhone varchar(50),
        pStatus char(1)
    )
BEGIN

    Declare new_seq MediumInt;
    
    Select 
		Max(seq) + 1 Into new_seq
	From m_member;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;

	IF Not Exists (SELECT email FROM m_member WHERE email = pEmail)
    Then
		Insert Into m_member(     
				seq,
				email,
				old_password,
				new_password,
				name,
				birthday,
				gender,
				mobile_phone,
				bank_name,
				bank_branch_name,
				bank_acct_no,
				bank_acct_name,
				profile_img,
				deposit_amt,
				status,
				last_login,
				ip_address,
				created_by,
				created_date,
				modified_by,
				modified_date
		) Values (
				new_seq,
				pEmail,
				' ',
                ' ',
                pName,
                pBirthday,
                pGender,
                pMobilePhone,
                '',
                '',
                '',
                '',
                '',
                '0',
                pStatus,
                Null,
                Null,
                'SYSTEM',
				Now(),
				'SYSTEM',
				Now()
		);
	END IF;
END