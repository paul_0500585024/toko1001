CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_bank_credit_by_seq`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq int unsigned
    )
BEGIN
SELECT 
    bank_seq,
    seq,
    credit_month,
    rate,
    created_by,
    created_date,
    modified_by,
    modified_date
FROM
    m_bank_credit
WHERE
    seq = pSeq;
END