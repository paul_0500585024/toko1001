CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_bank_credit_add`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pBankSeq int unsigned,
        pCreditMonth int unsigned,
        pRate DECIMAL(4,2)
    )
BEGIN
	Declare new_seq Int unsigned;

	SELECT 
		MAX(seq) + 1 INTO new_seq 
	FROM
		m_bank_credit;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;

	Insert Into m_bank_credit(     
		bank_seq,
        seq, 
        credit_month,
        rate, 
        created_by, 
        created_date, 
        modified_by, 
        modified_date
	) Values (
		pBankSeq,
		new_seq,
		pCreditMonth,
        pRate,
		pUserID,
		Now(),
		pUserID,
		Now()
	);

END