CREATE DEFINER=`root`@`%` PROCEDURE `sp_bank_delete_admin`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq int unsigned
    )
BEGIN
DELETE FROM m_bank 
WHERE
    seq = pSeq;
END