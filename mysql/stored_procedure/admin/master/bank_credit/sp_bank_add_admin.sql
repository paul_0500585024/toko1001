CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_bank_add_admin`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pBankName VarChar(200),
        pDescription VarChar(1000)
    )
BEGIN
	Declare new_seq Int unsigned;

	Select 
		Max(seq) + 1 Into new_seq
	From m_bank;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;

	Insert Into m_bank(     
		seq,
		bank_name,
        description,
		created_by,
		created_date,
		modified_by,
		modified_date
	) Values (
		new_seq,
		pBankName,
        pDescription,
		pUserID,
		Now(),
		pUserID,
		Now()
	);

END