CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_bank_by_seq_admin`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq int unsigned
    )
BEGIN
	SELECT 
    seq,
    bank_name,
    description,
    created_by,
    created_date,
    modified_by,
    modified_date
FROM
    m_bank
WHERE
    seq = pSeq;
END