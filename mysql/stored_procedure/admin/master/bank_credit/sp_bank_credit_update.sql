CREATE DEFINER=`root`@`%` PROCEDURE `sp_bank_credit_update`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq Int unsigned,
		pCreditMonth int unsigned,
        pRate DECIMAL(4,2)
    )
BEGIN

UPDATE m_bank_credit
SET 
    credit_month = pCreditMonth,
    rate = pRate,
    modified_by = pUserID,
    modified_date = NOW()
WHERE
    seq = pSeq;

END