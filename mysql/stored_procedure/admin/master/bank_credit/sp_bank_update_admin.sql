CREATE DEFINER=`root`@`%` PROCEDURE `sp_bank_update_admin`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq Int unsigned,
        pBankName VarChar(200),
        pDescription VarChar(1000)
    )
BEGIN

UPDATE m_bank
SET 
    bank_name = pBankName,
    description = pDescription,
    modified_by = pUserID,
    modified_date = NOW()
WHERE
    seq = pSeq;

END