CREATE DEFINER=`root`@`%` PROCEDURE `sp_bank_credit_delete`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq int unsigned
    )
BEGIN
DELETE FROM m_bank_credit
WHERE
    seq = pSeq;
END