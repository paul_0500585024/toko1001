CREATE DEFINER=`root`@`%` PROCEDURE `sp_provider_service_nominal_update`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
    pSeq smallint unsigned,
    pProviderServiceSeq smallint unsigned,
    pNominal int unsigned,
    pSellPrice int unsigned,
    pBuyPrice int unsigned,
    pMappingCode VARCHAR(25),
    pActive char(1)
)
BEGIN

update m_provider_service_nominal set
	`nominal` = pNominal,
    sell_price = pSellPrice,
    buy_price = pBuyPrice,
    mapping_code = pMappingCode,
    active = pActive,
    modified_by = pUserID,
    modified_date = now()
Where
	seq = pSeq AND  provider_service_seq = pProviderServiceSeq;

END