CREATE DEFINER=`root`@`%` PROCEDURE `sp_provider_update`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq Int unsigned,
        pName VarChar(200),
        pLogo VARCHAR(45)
    )
BEGIN

UPDATE m_provider
SET 
    name = pName,
    logo = pLogo,
    modified_by = pUserID,
    modified_date = NOW()
WHERE
    seq = pSeq;

END