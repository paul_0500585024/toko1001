CREATE DEFINER=`root`@`%` PROCEDURE `sp_provider_service_nominal_by_seq`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pProviderServiceSeq smallint unsigned,
        pSeq	smallint unsigned
    )
BEGIN
SELECT 
   seq,
   provider_service_seq,
   nominal,
   sell_price,
   buy_price,
   mapping_code,
   active
FROM
    m_provider_service_nominal
WHERE
   seq=pSeq and provider_service_seq=pProviderServiceSeq;
END