CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_provider_by_seq`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq tinyint unsigned
    )
BEGIN

	Select 
        seq,
		name,
        logo
		From m_provider
	Where
		seq = pSeq;
END