CREATE DEFINER=`root`@`%` PROCEDURE `sp_provider_service_nominal_delete`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq smallint unsigned,
		pProviderServiceSeq smallint unsigned
    )
BEGIN
	DELETE FROM m_provider_service_nominal 
	WHERE
		seq = pSeq
		AND provider_service_seq = pProviderServiceSeq;
END