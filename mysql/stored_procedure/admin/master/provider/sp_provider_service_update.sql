CREATE DEFINER=`root`@`%` PROCEDURE `sp_provider_service_update`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
    pSeq smallint unsigned,
    pProviderSeq smallint unsigned,
    pName varchar(200)
    )
BEGIN

update m_provider_service set
	`name` = pName,
    modified_by = pUserID,
    modified_date = now()
Where
	seq = pSeq AND provider_seq = pProviderSeq;

END