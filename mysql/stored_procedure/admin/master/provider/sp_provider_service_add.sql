CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_provider_service_add`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
    pProviderSeq smallint unsigned,
    pName varchar(200)
)
BEGIN
	Declare new_seq tinyint unsigned;

	Select 
		Max(seq) + 1 Into new_seq
	From m_provider_service;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
    
    Insert Into m_provider_service(
		seq,
        provider_seq,
        name,
        created_by,
        created_date,
        modified_by,
        modified_date
    )
    value (
		new_seq,
        pProviderSeq,
		pName,
        pUserID,
		Now(),
		pUserID,
		Now()
    );
END