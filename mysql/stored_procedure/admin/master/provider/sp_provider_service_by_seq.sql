CREATE DEFINER=`root`@`%` PROCEDURE `sp_provider_service_by_seq`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq	smallint unsigned
    )
BEGIN
SELECT 
    seq,
    provider_seq,
    name,
    created_by,
    created_date,
    modified_by,
    modified_date
FROM
    m_provider_service
WHERE
    seq = pSeq;
END