CREATE DEFINER=`root`@`%` PROCEDURE `sp_provider_service_nominal_list`(
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pCurrPage Int,
    pRecPerPage Int,
    pDirSort VarChar(4),
    pColumnSort Varchar(50),
    pProviderServiceSeq smallint unsigned,
    pFrom mediumint unsigned,
    pTo mediumint unsigned,
    pMappingCode varchar(25)
)
BEGIN
    -- Begin variables for Paging
    Declare startRec Int;
    Declare totalPage Int;
    Declare totalRec Int;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
	Set sqlWhere = Concat(sqlWhere, " AND n.provider_service_seq='", pProviderServiceSeq ,"'"); 
	
    If pFrom <> 0 Then
        Set sqlWhere = Concat(sqlWhere, " And n.nominal >='" , pFrom , "'");
    End If;
    If pTo <> 0 Then
        Set sqlWhere = Concat(sqlWhere, " And n.nominal <='" , pTo , "'");
    End If;
    If pMappingCode <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And n.mapping_code like '%" , escape_string(pMappingCode), "%'");
    End If; 
    
    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "Select Count(n.seq) Into @totalRec From m_provider_service_nominal n
																	JOIN
																m_provider_service s ON n.provider_service_seq = s.seq ";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
			SELECT 
				n.seq,
				n.provider_service_seq,
				n.nominal,
				n.sell_price,
				n.buy_price,
				n.mapping_code,
				n.active,
				n.created_by,
				n.created_date,
				n.modified_by,
				n.modified_date,
				s.`name` AS service_name
			FROM
				m_provider_service_nominal n
					JOIN
				m_provider_service s ON n.provider_service_seq = s.seq
    ";

    If sqlWhere <> "" Then
		Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;
    
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

SELECT pCurrPage, totalPage, totalRec AS total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END