CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_provider_add`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pName VarChar(200),
        pLogo VARCHAR(45)
    )
BEGIN
	Declare new_seq TinyInt unsigned;

	Select 
		Max(seq) + 1 Into new_seq
	From m_provider;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;

	Insert Into m_provider(     
		seq,
		name,
        logo,
		created_by,
		created_date,
		modified_by,
		modified_date
	) Values (
		new_seq,
		pName,
        pLogo,
		pUserID,
		Now(),
		pUserID,
		Now()
	);

END