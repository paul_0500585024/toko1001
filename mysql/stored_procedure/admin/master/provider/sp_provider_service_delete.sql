CREATE DEFINER=`root`@`%` PROCEDURE `sp_provider_service_delete`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq smallint unsigned
    )
BEGIN

Delete From m_provider_service Where seq = pSeq;

END