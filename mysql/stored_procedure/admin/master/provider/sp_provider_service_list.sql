CREATE DEFINER=`root`@`%` PROCEDURE `sp_provider_service_list`(
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pCurrPage Int,
    pRecPerPage Int,
    pDirSort VarChar(4),
    pColumnSort Varchar(50),
    pProviderSeq smallint unsigned,
    pName varchar(200)

)
BEGIN
    -- Begin variables for Paging
    Declare startRec Int;
    Declare totalPage Int;
    Declare totalRec Int;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
	Set sqlWhere = Concat(sqlWhere, " And s.provider_seq = '", pProviderSeq ,"'"); 

    If pName <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And s.name like '%" , escape_string(pName), "%'");
    End If;
    
    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "Select Count(s.seq) Into @totalRec From m_provider_service s JOIN m_provider p ON s.provider_seq = p.seq ";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
       SELECT 
			s.seq,
			s.provider_seq,
			s.name,
			p.name AS provider_name,
			s.created_by,
			s.created_date,
			s.modified_by,
			s.modified_date
		FROM
			m_provider_service s
				JOIN
			m_provider p ON s.provider_seq = p.seq
    ";

    If sqlWhere <> "" Then
		Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;
    
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

SELECT pCurrPage, totalPage, totalRec AS total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END