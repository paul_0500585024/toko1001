CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_provider_service_nominal_add`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
    pProviderServiceSeq smallint unsigned,
    pNominal int unsigned,
    pSellPrice int unsigned,
    pBuyPrice int unsigned,
    pMappingCode VARCHAR(25),
    pActive char(1)
)
BEGIN
	Declare new_seq tinyint unsigned;

	Select 
		Max(seq) + 1 Into new_seq
	From m_provider_service_nominal;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
    
    Insert Into m_provider_service_nominal(
		seq,
        provider_service_seq,
        nominal,
        sell_price,
        buy_price,
        mapping_code,
        active,
        created_by,
        created_date,
        modified_by,
        modified_date
    )
    value (
		new_seq,
        pProviderServiceSeq,
		pNominal,
        pSellPrice,
        pBuyPrice,
        pMappingCode,
        pActive,
        pUserID,
		Now(),
		pUserID,
		Now()
    );
END