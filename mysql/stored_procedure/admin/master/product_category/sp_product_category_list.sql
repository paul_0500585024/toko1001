CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_category_list`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pCurrPage Int,
        pRecPerPage Int,
        pDirSort VarChar(4),
        pColumnSort Varchar(50),
        pName VarChar(50),
        pParentName VarChar(50),
        pActive Char(1),
        pLevel TINYINT unsigned
    )
BEGIN
    -- Begin variables for Paging
    Declare startRec Int;
    Declare totalPage Int;
    Declare totalRec Int;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = " and seq > 0";

    If pName <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And `name` like '%" , escape_string(pName), "%'");
    End If;
     If pParentName <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And parent_name like '%" , escape_string(pParentName), "%'");
    End If;
    If pActive <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And active = '", pActive ,"'");
    End If;    
    If pLevel <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And level = '", pLevel ,"'");
    End If;

    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "Select Count(seq) Into @totalRec From m_product_category left outer join (select seq as parentseq, name as parent_name from m_product_category where seq>0) b on m_product_category.parent_seq=b.parentseq
 ";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
        Select
  seq,
  `name`,
  parent_seq,
  `level`,
  `order`,
  include_ins,
  exp_method,
  variant_seq,
  trx_fee_percent,
  active,
  created_by,
  created_date,
  modified_by,
  modified_date,
  IF(parent_seq = '0', 'ROOT', b.parent_name) parent_name
          From m_product_category
          left outer join (select seq as parentseq, name as parent_name from m_product_category where seq>0) b on m_product_category.parent_seq=b.parentseq
    ";

    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);

    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END