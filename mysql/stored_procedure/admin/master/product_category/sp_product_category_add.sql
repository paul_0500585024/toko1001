CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_product_category_add`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pName VarChar(100),
        pParentSeq MEDIUMINT(5) UNSIGNED,
        pLevel TINYINT(3) UNSIGNED,
        pOrder TINYINT(3) UNSIGNED,
        pIncludeIns Char(1),
        pExpMethod Char(1),
        pVariantSeq MEDIUMINT(5) UNSIGNED,
        pTrxFeePercent DECIMAL(4,2),
        pActive Char(1)
    )
BEGIN
	Declare new_seq Int;
 	
	Select 
		Max(seq) + 1 Into new_seq
	From m_product_category;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
    If pVariantSeq =0 Then
		Set pVariantSeq = NULL;
	End If;
	
	Insert Into m_product_category(  
    seq, `name`, parent_seq, level, `order`, include_ins, exp_method, variant_seq, trx_fee_percent, active,
  created_by, created_date, modified_by, modified_date
	) Values (
		new_seq, pName, pParentSeq, pLevel, pOrder, pIncludeIns, pExpMethod, pVariantSeq, pTrxFeePercent, pActive,
		pUserID, Now(), pUserID, Now()
	);

END