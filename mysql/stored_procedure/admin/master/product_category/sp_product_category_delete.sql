CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_category_delete`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq MEDIUMINT
    )
BEGIN

Delete From m_product_category Where seq = pSeq;


END