CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_product_category_by_seq`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq MediumInt unsigned
    )
BEGIN

Select
  a.seq,
  a.name,
  a.parent_seq,
  a.level,
  a.`order`,
  a.include_ins,
  a.exp_method,
  a.variant_seq,
  a.trx_fee_percent,
  a.active,
  b.name as parent_name
		From m_product_category a
        left outer join m_product_category b on a.parent_seq=b.seq
	Where
		a.seq = pSeq and a.seq > 0;
END