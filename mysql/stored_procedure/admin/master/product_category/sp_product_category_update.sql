CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_category_update`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq MEDIUMINT(5) unsigned,
        pName VarChar(100),
        pParentSeq mediumint(8) unsigned,
        pLevel tinyint(3) unsigned,
        pOrder tinyint(3) unsigned,
        pIncludeIns Char(1),
        pExpMethod Char(1),
        pVariantSeq mediumint(8) unsigned,
        pTrxFeePercent DECIMAL(4,2),
        pActive Char(1)
    )
BEGIN
IF pVariantSeq=0 then
 set pVariantSeq=NULL;
end if;
update m_product_category set
    `name` = pName,
    parent_seq = pParentSeq,
    level = pLevel,
    `order` = pOrder,
    include_ins = pIncludeIns,
    exp_method = pExpMethod,
    variant_seq = pVariantSeq,
    trx_fee_percent = pTrxFeePercent,
    active = pActive,
    modified_by = pUserID,
    modified_date = now()
Where
	seq = pSeq;

END