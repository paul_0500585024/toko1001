DROP PROCEDURE IF EXISTS `sp_partner_down_payment_add`;
DELIMITER $$
CREATE PROCEDURE `sp_partner_down_payment_add`
(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pPartnerLoanSeq SMALLINT UNSIGNED,
        pMinimalLoan VARCHAR(50),
        pMaximalLoan VARCHAR(50),
        pMinDpPercent VARCHAR(50),
        pActive CHAR(1)
    )
BEGIN
	DECLARE new_seq Int UNSIGNED;
	SELECT 
		MAX(seq) + 1 INTO new_seq
	FROM m_partner_loan_dp;
	IF new_seq IS NULL THEN
		SET new_seq = 1;
	END IF;
	INSERT INTO m_partner_loan_dp(     
		seq,
		partner_loan_seq,
		minimal_loan,
		maximal_loan,
		min_dp_percent,
		active,
		created_by,
		created_date,
		modified_by,
		modified_date
	) VALUES (
		new_seq,
		pPartnerLoanSeq,
		pMinimalLoan,
		pMaximalLoan,
		pMinDpPercent,
		pActive,
		pUserID,
		NOW(),
		pUserID,
		NOW()
	);
END$$