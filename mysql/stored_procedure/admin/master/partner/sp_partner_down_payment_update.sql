DROP PROCEDURE IF EXISTS `sp_partner_down_payment_update`;
DELIMITER $$
CREATE PROCEDURE `sp_partner_down_payment_update`
(
  pUserID VARCHAR (25),
  pIPAddr VARCHAR (50),
  pSeq TINYINT UNSIGNED,
  pPartnerLoanSeq SMALLINT UNSIGNED,
  pMinimalLoan VARCHAR (50),
  pMaximalLoan VARCHAR (50),
  pMinDpPercent VARCHAR (50),
  pActive CHAR(1)
)
BEGIN
  UPDATE 
    m_partner_loan_dp
  SET
    partner_loan_seq = pPartnerLoanSeq,
    minimal_loan = pMinimalLoan,
    maximal_loan = pMaximalLoan,
    min_dp_percent = pMinDpPercent,
    active = pActive,
    modified_by = pUserID,
    modified_date = NOW() 
  WHERE seq = pSeq ;
END $$ 
 
