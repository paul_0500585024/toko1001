DROP PROCEDURE IF EXISTS `sp_partner_loan_delete`;
DELIMITER $$
CREATE PROCEDURE `sp_partner_loan_delete`
(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pSeq INT UNSIGNED
    )
BEGIN
DELETE FROM m_partner_loan WHERE seq = pSeq;
END $$  
