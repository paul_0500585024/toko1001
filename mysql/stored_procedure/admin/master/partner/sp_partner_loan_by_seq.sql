DROP PROCEDURE IF EXISTS `sp_partner_loan_by_seq`;
DELIMITER $$
CREATE PROCEDURE `sp_partner_loan_by_seq`
(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pSeq TINYINT UNSIGNED
    )
BEGIN
	SELECT 
		seq,
		product_category_seq,
		partner_seq,
		tenor,
		loan_interest,
		active
		FROM m_partner_loan
	WHERE
		seq = pSeq;
END $$ 
