DROP PROCEDURE IF EXISTS `sp_partner_loan_get_all_tenor_in_product_category_seq_by_seq`;
DELIMITER $$
CREATE PROCEDURE `sp_partner_loan_get_all_tenor_in_product_category_seq_by_seq`
(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pSeq TINYINT UNSIGNED
    )
BEGIN
    DECLARE startRec INT UNSIGNED;
SELECT 
	product_category_seq, partner_seq INTO @product_category_seq,@partner_seq
FROM m_partner_loan
WHERE
	seq = pSeq;
SELECT tenor FROM m_partner_loan WHERE product_category_seq=@product_category_seq AND partner_seq=@partner_seq;
END $$  
