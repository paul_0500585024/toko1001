DROP PROCEDURE IF EXISTS `sp_partner_loan_update`;
DELIMITER $$
CREATE PROCEDURE `sp_partner_loan_update`
(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pSeq TINYINT UNSIGNED,
        pPartnerName VARCHAR(50),
        pCategoryName VARCHAR(50),
        pTenor VARCHAR(50),
        pLoanInterest VARCHAR(50),
        pActive CHAR(1)
    )
BEGIN
UPDATE m_partner_loan SET
    partner_seq = pPartnerName,
    product_category_seq = pCategoryName,
    tenor = pTenor,
    loan_interest = pLoanInterest,
    active = pActive,
    modified_by = pUserID,
    modified_date = NOW()
WHERE
	seq = pSeq;
END $$ 
