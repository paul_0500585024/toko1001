DROP PROCEDURE IF EXISTS `sp_partner_loan_add`;
DELIMITER $$
CREATE PROCEDURE `sp_partner_loan_add`
(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pPartnerName VARCHAR(50),
        pCategoryName VARCHAR(50),
        pTenor VARCHAR(50),
        pLoanInterest VARCHAR(50),
        pActive CHAR(1)
    )
BEGIN
	DECLARE new_seq TINYINT UNSIGNED;
	SELECT 
		MAX(seq) + 1 INTO new_seq
	FROM m_partner_loan;
	IF new_seq IS NULL THEN
		SET new_seq = 1;
	END IF;
	INSERT INTO m_partner_loan(     
		seq,
		product_category_seq,
		partner_seq,
		tenor,
		loan_interest,
		active,
		created_by,
		created_date,
		modified_by,
		modified_date
	) VALUES (
		new_seq,
		pCategoryName,
		pPartnerName,
		pTenor,
		pLoanInterest,
		pActive,
		pUserID,
		NOW(),
		pUserID,
		NOW()
	);
END $$ 
