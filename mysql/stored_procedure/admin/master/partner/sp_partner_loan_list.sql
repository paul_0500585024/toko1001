DROP PROCEDURE IF EXISTS `sp_partner_loan_list`;
DELIMITER $$
CREATE PROCEDURE `sp_partner_loan_list`
(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pCurrPage INT UNSIGNED,
        pRecPerPage INT UNSIGNED,
        pDirSort VARCHAR(4),
        pColumnSort VARCHAR(50),
        pPartnerName VARCHAR(50),
        pCategoryName VARCHAR(50),
        pTenor VARCHAR(50),
        pLoanInterest VARCHAR(50),
        pActive CHAR(1)
    )
BEGIN
    -- Begin variables for Paging
    DECLARE startRec INT UNSIGNED;
    DECLARE totalPage INT UNSIGNED;
    DECLARE totalRec INT UNSIGNED;
    -- End variables for Paging
    -- Begin SQL Where
    DECLARE sqlWhere VARCHAR(500);
	SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    SET sqlWhere = "";
    IF pPartnerName <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And m_p.`seq`= '" , pPartnerName, "'");
    END IF;
    IF pActive <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And m_p_l.`active` = '", pActive ,"'");
    END IF;
    -- End SQL Where
    -- Begin Paging Info
    SET @sqlCommand = "Select Count(m_p_l.`seq`) Into @totalRec FROM m_partner_loan m_p_l
          JOIN m_partner m_p ON m_p.`seq`=m_p_l.`partner_seq`
          JOIN m_product_category m_p_c ON m_p_c.`seq`=m_p_l.`product_category_seq`";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        END IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    -- End Paging Info
    SET @sqlCommand = "
        Select		
			m_p_l.`seq`,
			m_p.`name` AS partner_name,
			m_p_c.`name` AS category_name,
			m_p_l.`tenor`,
			m_p_l.`loan_interest`,
			m_p_l. `active`,
			m_p_l.`created_by`,
			m_p_l.`created_date`,
			m_p_l.`modified_by`,
			m_p_l.`modified_date`
				
	  FROM m_partner_loan m_p_l
          JOIN m_partner m_p ON m_p.`seq`=m_p_l.`partner_seq`
          JOIN m_product_category m_p_c ON m_p_c.`seq`=m_p_l.`product_category_seq`";
    
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    SET @sqlCommand = CONCAT(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @totalRec = NULL;
    SET @sqlCommand = NULL;
END $$ 
