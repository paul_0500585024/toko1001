DROP PROCEDURE IF EXISTS `sp_partner_down_payment_by_seq`;
DELIMITER $$
CREATE PROCEDURE `sp_partner_down_payment_by_seq`
(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pSeq TINYINT UNSIGNED
    )
BEGIN
	SELECT 
			m_p_l.`product_category_seq`,
			m_p_dp.`partner_loan_seq`,
			m_p_dp.`seq`,
			m_p_c.`name` AS category_loan,
			m_p_l.`tenor` AS tenor_loan,
			m_p_dp.`minimal_loan`,
			m_p_dp.`maximal_loan`,
			m_p_dp.`min_dp_percent`,
			m_p_l. `active`,
			m_p_l.`created_by`,
			m_p_l.`created_date`,
			m_p_l.`modified_by`,
			m_p_l.`modified_date`
				
	  FROM m_partner_loan_dp m_p_dp
          JOIN m_partner_loan m_p_l ON m_p_l.`seq`=m_p_dp.`partner_loan_seq`
          JOIN m_product_category m_p_c ON m_p_c.seq=m_p_l.`product_category_seq`
          
	WHERE
		m_p_dp.`seq` = pSeq;
END $$ 

