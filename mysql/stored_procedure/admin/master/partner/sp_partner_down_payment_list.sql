DROP PROCEDURE IF EXISTS `sp_partner_down_payment_list`;
DELIMITER $$
CREATE PROCEDURE `sp_partner_down_payment_list`
(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pCurrPage INT UNSIGNED,
        pRecPerPage INT UNSIGNED,
        pDirSort VARCHAR(4),
        pColumnSort VARCHAR(50),
        pCategoryLoan VARCHAR(50),
        pTenorLoan VARCHAR(50),
        pPartnerSeq TINYINT UNSIGNED,
        pProductCategorySeq TINYINT UNSIGNED,
	pPartnerLoanSeq SMALLINT UNSIGNED,
        pActive CHAR(1)
    )
BEGIN
    -- Begin variables for Paging
    DECLARE startRec INT UNSIGNED;
    DECLARE totalPage INT UNSIGNED;
    DECLARE totalRec INT UNSIGNED;
    -- End variables for Paging
    -- Begin SQL Where
    DECLARE sqlWhere VARCHAR(500);
	SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    SET sqlWhere = "";
    IF pPartnerSeq <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And m_p_l.`partner_seq`= '" , pPartnerSeq, "'");
    END IF;
    IF pProductCategorySeq <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And m_p_l.`product_category_seq`= '" , pProductCategorySeq, "'");
    END IF;
    IF pPartnerLoanSeq <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And m_p_dp.`partner_loan_seq`= '" , pPartnerLoanSeq, "'");
    END IF;
    IF pActive <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And m_p_dp.`active` = '", pActive ,"'");
    END IF;
    -- End SQL Where
    -- Begin Paging Info
    SET @sqlCommand = "Select Count(m_p_l.`seq`) Into @totalRec FROM m_partner_loan_dp m_p_dp
          JOIN m_partner_loan m_p_l ON m_p_l.`seq`=m_p_dp.`partner_loan_seq`
          JOIN m_product_category m_p_c ON m_p_c.seq=m_p_l.`product_category_seq`
          JOIN m_partner m_p ON m_p.seq=m_p_l.partner_seq";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        END IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    -- End Paging Info
    SET @sqlCommand = "
        Select		
			m_p_dp.`seq`,
			m_p.`name` AS partner_name,
			m_p_c.`name` AS category_loan,
			m_p_l.`tenor` AS tenor_loan,
			m_p_dp.`minimal_loan`,
			m_p_dp.`maximal_loan`,
			m_p_dp.`min_dp_percent`,
			m_p_l. `active`,
			m_p_l.`created_by`,
			m_p_l.`created_date`,
			m_p_l.`modified_by`,
			m_p_l.`modified_date`
				
	  FROM m_partner_loan_dp m_p_dp
          JOIN m_partner_loan m_p_l ON m_p_l.`seq`=m_p_dp.`partner_loan_seq`
          JOIN m_product_category m_p_c ON m_p_c.seq=m_p_l.`product_category_seq`
          JOIN m_partner m_p ON m_p.seq=m_p_l.partner_seq
           
          ";
    
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    SET @sqlCommand = CONCAT(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @totalRec = NULL;
    SET @sqlCommand = NULL;
END $$ 
