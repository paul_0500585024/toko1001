CREATE DEFINER=`root`@`%` PROCEDURE `sp_image_slide_show_by_seq`(
	
    pUserID Varchar(25),
    pIPAddress Varchar(50),
    pSeq smallint unsigned

)
BEGIN

select
	seq,
	img,
    img_url,
    `order`,
    active
from 
	m_img_slide_show
where
	seq = pSeq;

END