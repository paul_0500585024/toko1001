CREATE DEFINER=`root`@`%` PROCEDURE `sp_image_slide_show_update_status`(
	pUserID Varchar(25),
    pIPAddress Varchar(50),
    pSeq smallint unsigned,
    pStatus Char(3)
)
BEGIN

update m_img_slide_show set
	status = pStatus,
    auth_by = pUserID,
    auth_date = now()
where
	seq = pSeq;

END