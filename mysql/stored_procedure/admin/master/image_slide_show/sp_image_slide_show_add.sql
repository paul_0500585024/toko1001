CREATE DEFINER=`root`@`%` PROCEDURE `sp_image_slide_show_add`(
	
    pUserID Varchar(25),
    pIPAddress Varchar(50),
    pImg Varchar(100),
    pImgUrl Varchar(100),
    pOrder smallint unsigned,
    pActive Char(1),
    pStatus Char(3)
)
BEGIN

	Declare new_seq TinyInt unsigned;

	Select 
		Max(seq) + 1 Into new_seq
	From m_img_slide_show;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;


	Insert Into m_img_slide_show(
		seq,
        img,
        img_url,
        status,
        `order`,
        active,
        auth_by,
        auth_date,
        created_by,
        created_date,
        modified_by,
        modified_date
	) values (
		new_seq,
        pImg,
        pImgUrl,
        pStatus,
        pOrder,
        pActive,
        '',
        '0000-00-00 00',
        pUserId,
        now(),
        pUserID,
        now()
	);
        

END