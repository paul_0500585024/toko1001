CREATE DEFINER=`root`@`%` PROCEDURE `sp_image_slide_show_delete`(

	pUserID Varchar(25),
    pIPAddres Varchar(50),
    pSeq smallint unsigned
)
BEGIN

update 	m_img_slide_show set
	active = '0',
    modified_by = pUserID, 
    modified_date = now()
where
	seq = pSeq;

END