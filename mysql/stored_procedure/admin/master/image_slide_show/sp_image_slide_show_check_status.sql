CREATE DEFINER=`root`@`%` PROCEDURE `sp_image_slide_show_check_status`(
	
    pUserID Varchar(25),
    pIPAddress Varchar(50),
    pSeq smallint unsigned
)
BEGIN

select
	status
from
	m_img_slide_show
where
	seq = pSeq;

END