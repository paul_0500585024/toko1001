CREATE DEFINER=`root`@`%` PROCEDURE `sp_image_slide_show_update`(
	
    pUserID Varchar(25),
    pIPAddress Varchar(50),
    pSeq smallint unsigned,
    pImg varchar(100),
    pImgUrl Varchar(100),
    pOrder smallint unsigned,
    pActive Char(1)
)
BEGIN

update m_img_slide_show set
	img = pImg,
    img_url = pImgUrl,
    `order` = pOrder,
    active = pActive
where
	seq = pSeq;

END