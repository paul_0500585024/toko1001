CREATE DEFINER=`root`@`%` PROCEDURE `sp_image_slide_show_max_seq`()
BEGIN

Select
	IFNULL(max(seq),0) + 1 as seq
From
	m_img_slide_show;

END