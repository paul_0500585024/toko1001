CREATE DEFINER=`root`@`%` PROCEDURE `sp_expedition_awb_delete`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq MediumInt Unsigned
    )
BEGIN

    Select 
		trx_no
	From 
		m_expedition_awb
	Where seq = pSeq;


	Delete From 
		m_expedition_awb 
	Where seq = pSeq And trx_no = "";

    
END