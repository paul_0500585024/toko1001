CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_expedition_awb_upload`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pExpSeq TinyInt Unsigned,
        pAwbNo VarChar(100)
    )
BEGIN

    Declare new_seq MediumInt;
    
    Select 
		Max(seq) + 1 Into new_seq
	From m_expedition_awb;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;

	IF Not Exists (SELECT awb_no FROM m_expedition_awb WHERE exp_seq = pExpSeq And awb_no = pAwbNo)
    Then
		Insert Into m_expedition_awb(     
			exp_seq,
			seq,
			awb_no,
			trx_no,
			created_by,
			created_date,
			modified_by,
			modified_date
		) Values (
			pExpSeq,
			new_seq,
			pAwbNo,
			'',
			pUserID,
			Now(),
			pUserID,
			Now()
		);
	END IF;
END