CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_expedition_by_seq`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq TinyInt unsigned
    )
BEGIN

	Select
	seq,
	code,
	name,
	logo_img,
    logo_img as old_img,
	awb_method,
	ins_rate_percent,
	notes,
	active
		From m_expedition
	Where
		seq = pSeq;
END