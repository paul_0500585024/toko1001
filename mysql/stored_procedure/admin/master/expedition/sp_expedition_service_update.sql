CREATE DEFINER=`root`@`%` PROCEDURE `sp_expedition_service_update`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq smallint(5) UNSIGNED,
        pCode VarChar(25),
        pName VarChar(50),
        pDefault Char(1),
        pOrder smallint(3) UNSIGNED,
        pActive Char(1)
    )
BEGIN

update m_expedition_service set
    code = pCode,
    name = pName,
    `default` = pDefault,
    `order` = pOrder,
    active = pActive,
    modified_by = pUserID,
    modified_date = now()
Where
	seq = pSeq;

END