CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_expedition_service_by_seq`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq TinyInt UNSIGNED
    )
BEGIN

	Select
  exp_seq,
  seq,
  code,
  name,
  `default`,
  `order`,
  active,
  created_by,
  created_date,
  modified_by,
  modified_date
		From m_expedition_service
	Where
		seq = pSeq;
END