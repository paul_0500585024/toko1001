CREATE DEFINER=`root`@`%` PROCEDURE `sp_expedition_awb_list`(
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pCurrPage Int,
    pRecPerPage Int,
    pDirSort VarChar(4),
    pColumnSort Varchar(50),
    pExpSeq TinyInt unsigned,
    pMerchantInfoSeq TinyInt Unsigned,
    pAwbNo VarChar(100),
    pTrxNo VarChar(100)
    
)
BEGIN
    -- Begin variables for Paging
    Declare startRec Int;
    Declare totalPage Int;
    Declare totalRec Int;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
    
    If pMerchantInfoSeq <> 0 Then
        Set sqlWhere = Concat(sqlWhere, " And om.merchant_info_seq ='" , pMerchantInfoSeq, "'");
    End If;
    
    If pAwbNo <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And ea.awb_no like '%" , escape_string(pAwbNo), "%'");
    End If;
    
    If pTrxNo <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And ea.trx_no like '%" , escape_string(pTrxNo), "%'");
    End If;
    
   -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "Select Count(ea.seq) Into @totalRec 
						From m_expedition_awb ea 
							Join m_expedition e
								On ea.exp_seq = e.seq
							Left Join t_order_merchant om
								On ea.seq = om.awb_seq
							Left Join m_merchant m
								On om.merchant_info_seq = m.seq";
            
	Set @sqlCommand = Concat(@sqlCommand, " Where ea.exp_seq ='" , pExpSeq, "'");
    
	If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
        Select
			ea.seq,
			ea.awb_no,
			ea.trx_no,
			m.name,
			om.ship_date,
			ea.created_by,
			ea.created_date,
			ea.modified_by,
			ea.modified_date
		From m_expedition_awb ea 
			Join m_expedition e
				On ea.exp_seq = e.seq
			Left Join t_order_merchant om
				On ea.seq = om.awb_seq
			Left Join m_merchant m
				On om.merchant_info_seq = m.seq
    ";
    Set @sqlCommand = Concat(@sqlCommand, "Where ea.exp_seq ='" , pExpSeq, "'");
	/*Set @sqlCommand = Concat(@sqlCommand, "Where ea.created_date between '", pCreatedDateFrom, "' and '", pCreatedDateTo, "' ");
    Set @sqlCommand = Concat(@sqlCommand, "Where om.ship_date between '", pShipDateFrom, "' and '", pShipDateTo, "' ");*/

    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END