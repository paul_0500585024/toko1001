CREATE DEFINER=`root`@`%` PROCEDURE `sp_expedition_delete`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq TINYINT(3) unsigned
    )
BEGIN

Delete From m_expedition Where seq = pSeq;


END