CREATE DEFINER=`root`@`%` PROCEDURE `sp_expedition_service_delete`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq TinyInt UNSIGNED
    )
BEGIN

Delete From m_expedition_service Where seq = pSeq;


END