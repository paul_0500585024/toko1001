CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_expedition_add`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pCode VarChar(25),
        pName VarChar(50),
        pLogoImg VarChar(50),
        pAwbMethod Char(1),
        pInsRatePercent Decimal(4,2),
        pNotes VarChar(50),
        pActive Char(1)
    )
BEGIN
	Declare new_seq TinyInt unsigned;

	Select 
		Max(seq) + 1 Into new_seq
	From m_expedition;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;

	Insert Into m_expedition(     
		seq,
		code,
		name,
		logo_img,
		awb_method,
		ins_rate_percent,
		notes,
		active,
		created_by,
		created_date,
		modified_by,
		modified_date
	) Values (
		new_seq,
		pCode,
        pName,
        pLogoImg,
        pAwbMethod,
        pInsRatePercent,
        pNotes,
		pActive,
		pUserID,
		Now(),
		pUserID,
		Now()
	);

END