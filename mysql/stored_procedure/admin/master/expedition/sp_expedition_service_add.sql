CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_expedition_service_add`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pExpSeq smallint(5) UNSIGNED,
        pCode VarChar(25),
        pName VarChar(50),
        pDefault Char(1),
        pOrder smallint(3) UNSIGNED,
        pActive Char(1)
    )
BEGIN
	Declare new_seq TinyInt;

	Select 
		Max(seq) + 1 Into new_seq
	From m_expedition_service;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;

	Insert Into m_expedition_service (     
		exp_seq,
		seq,
		code,
		name,
		`default`,
		`order`,
		active,
		created_by,
		created_date,
		modified_by,
		modified_date
	) Values (
		pExpSeq,
		new_seq,
		pCode,
        pName,
        pDefault,
        pOrder,
		pActive,
		pUserID,
		Now(),
		pUserID,
		Now()
	);

END