CREATE DEFINER=`root`@`%` PROCEDURE `sp_image_update_image_name`(
        pCategorySeq smallint unsigned,
        pSeq smallInt unsigned,
        pImgBanner varchar(50),
        pAdv1Img varchar(50),
        pAdv2Img varchar(50),
        pAdv3Img varchar(50),
        pAdv4Img varchar(50),
        pAdv5Img varchar(50),
        pAdv6Img varchar(50),
		pAdv7Img varchar(50)
)
BEGIN

update m_img_product_category set
    banner_img = pImgBanner,
    adv_1_img = pAdv1Img,
	adv_2_img = pAdv2Img,
    adv_3_img = pAdv3Img,
    adv_4_img = pAdv4Img,
    adv_5_img = pAdv5Img,
    adv_6_img = pAdv6Img,
    adv_7_img = pAdv7Img  
Where
	category_seq = pCategorySeq and
	seq = pSeq;


END