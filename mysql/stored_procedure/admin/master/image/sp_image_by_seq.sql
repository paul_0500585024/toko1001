CREATE DEFINER=`root`@`%` PROCEDURE `sp_image_by_seq`(
       pUserID VarChar(25),
       pIPAddr VarChar(50),
       pCategorySeq smallint unsigned, 
       pSeq smallint unsigned
    )
BEGIN

Select 
	category_seq,
    seq,
    banner_img as img_banner,
    banner_img_url as img_banner_url,
    adv_1_img as img_1,
    adv_1_img_url as img_1_url,
    adv_2_img as img_2,
    adv_2_img_url as img_2_url,
    adv_3_img as img_3,
    adv_3_img_url as img_3_url,
    adv_4_img as img_4,
    adv_4_img_url as img_4_url,
    adv_5_img as img_5,
    adv_5_img_url as img_5_url,
    adv_6_img as img_6,
    adv_6_img_url as img_6_url,
    adv_7_img as img_7,
    adv_7_img_url as img_7_url,
	adv_1_img as old_img_1,
    adv_2_img as old_img_2,
    adv_3_img as old_img_3,
    adv_4_img as old_img_4,
    adv_5_img as old_img_5,
    adv_6_img as old_img_6,
    adv_7_img as old_img_7,
    banner_img as old_img_banner
From 
	m_img_product_category 
Where
	category_seq = pCategorySeq and
	seq = pSeq;
    
END