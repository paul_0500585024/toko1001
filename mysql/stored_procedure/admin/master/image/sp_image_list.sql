CREATE DEFINER=`root`@`%` PROCEDURE `sp_image_list`(
		pUserID VarChar(25),
		pIPAddr VarChar(50),
		pCurrPage Int unsigned,
		pRecPerPage Int unsigned,
		pDirSort VarChar(4),
		pColumnSort Varchar(50),
        pCategorySeq smallint unsigned,
        pStatus	char(1),	
        pActive	char(1)

)
BEGIN
    -- Begin variables for Paging
    Declare startRec Int unsigned;
    Declare totalPage Int unsigned;
    Declare totalRec Int unsigned;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
      
	If pStatus <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And c.status like '%" , pStatus, "%'");
    End If;

    If pCategorySeq <> 0 Then
        Set sqlWhere = Concat(sqlWhere, " And c.category_seq = '" , pCategorySeq , "'");
    End If;
    
	If pActive <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And c.active='" , pActive, "'");
    End If; 
      
    
    -- Begin Paging Info
    Set @sqlCommand = "Select Count(c.seq) Into @totalRec From m_img_product_category c ";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
        Select
			c.category_seq,
			c.seq,
            c.status,
            p.name as category_name,
            c.active,
            c.auth_by,
            c.auth_date,
            c.created_by,
            c.created_date,
            c.modified_by,
            c.modified_date
		From m_img_product_category c
        Join m_product_category p on
            c.category_seq = p.seq

    ";

    If sqlWhere <> "" Then
		Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;
    
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END