CREATE DEFINER=`root`@`%` PROCEDURE `sp_image_update_by_seq`(
        pUserID VarChar(25),
		pIPAddr VarChar(50),
        pCategorySeq smallint unsigned,
        pSeq smallint unsigned,
        pImgBanner varchar(50),
        pImgBannerUrl varchar(200),
        pImg1 varchar(50),
        pImg1Url varchar(200),
        pImg2 varchar(50),
        pImg2url varchar(200),
        pImg3 varchar(50),
        pImg3url varchar(200),
        pImg4 varchar(50),
        pImg4url varchar(200),
        pImg5 varchar(50),
        pImg5url varchar(200),
        pImg6 varchar(50),
        pImg6Url varchar(200),
		pImg7 varchar(50),
        pImg7Url varchar(200)
)
BEGIN

update m_img_product_category set
    category_seq = pCategorySeq,
    seq = pSeq,
    banner_img = pImgBanner,
    banner_img_url = pImgBannerUrl,
    adv_1_img = pImg1 ,
    adv_1_img_url = pImg1Url,
	adv_2_img = pImg2 ,
    adv_2_img_url = pImg2Url,
    adv_3_img = pImg3  ,
    adv_3_img_url = pImg3Url,
    adv_4_img = pImg4 ,
    adv_4_img_url = pImg4Url,
    adv_5_img = pImg5 ,
    adv_5_img_url = pImg5Url,
    adv_6_img = pImg6,
    adv_6_img_url = pImg6Url,       
	adv_7_img = pImg7,
    adv_7_img_url = pImg7Url
Where
	seq = pSeq and 
    category_seq = pCategorySeq;

END