CREATE DEFINER=`root`@`%` PROCEDURE `sp_image_add`(
		pUser_ID VarChar(25),
		pIPAddr VarChar(50),
        pCategorySeq smallint unsigned,
        pImgBanner varchar(50),
        pImgBannerUrl varchar(200),
        pAdv1Img varchar(50),
        pAdv1ImgUrl	varchar(200),
        pAdv2Img varchar(50),
        pAdv2ImgUrl	varchar(200),
        pAdv3Img varchar(50),
        pAdv3ImgUrl	varchar(200),
        pAdv4Img varchar(50),
        pAdv4ImgUrl	varchar(200),
        pAdv5Img varchar(50),
        pAdv5ImgUrl	varchar(200),
        pAdv6Img varchar(50),
        pAdv6ImgUrl	varchar(200),
		pAdv7Img varchar(50),
        pAdv7ImgUrl	varchar(200),
        pStatus	char(1)        
)
BEGIN
Declare new_seq Int;

    Select 
        Max(seq) + 1 Into new_seq
    From m_img_product_category
    where
		category_seq = pCategorySeq;

    If new_seq Is Null Then
        Set new_seq = 1;
    End If;

    Insert Into  m_img_product_category(
		category_seq,
		seq,
        banner_img,
        banner_img_url,
        adv_1_img,
        adv_1_img_url,
        adv_2_img,
        adv_2_img_url,
        adv_3_img,
        adv_3_img_url,
        adv_4_img,
        adv_4_img_url,
        adv_5_img,
        adv_5_img_url,
        adv_6_img,
        adv_6_img_url,
		adv_7_img,
        adv_7_img_url,
        status,
        auth_by,
        auth_date,
        created_by,
        created_date,
        modified_by,
        modified_date
    ) Values (
		pCategorySeq,
        new_seq,
		pImgBanner,
        pImgBannerUrl,
        pAdv1Img ,
        pAdv1ImgUrl,
        pAdv2Img ,
        pAdv2ImgUrl,
        pAdv3Img ,
        pAdv3ImgUrl,
        pAdv4Img ,
        pAdv4ImgUrl,
        pAdv5Img ,
        pAdv5ImgUrl,
        pAdv6Img ,
        pAdv6ImgUrl,
        pAdv7Img,
        pAdv7ImgUrl,
        pStatus,
        '',
        '0000-00-00 00:00:00',
		pUser_ID,
        Now(),
        pUser_ID,
        Now()
    );
    
    select new_seq;
END