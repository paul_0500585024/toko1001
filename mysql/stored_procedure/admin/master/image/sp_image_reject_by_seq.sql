CREATE DEFINER=`root`@`%` PROCEDURE `sp_image_reject_by_seq`(
        pUserID VarChar(25),
		pIPAddr VarChar(50),
        pCategorySeq smallint unsigned,        
        pSeq smallint unsigned        
    )
BEGIN

update m_img_product_category set    
	status = "R",
    auth_by = pUserID,
    auth_date = now(),
    modified_by = pUserID,
    modified_date = Now()    
Where
	seq = pSeq;

END