CREATE DEFINER=`root`@`%` PROCEDURE `sp_image_check_by_seq`(
       pUserID VarChar(25),
        pIPAddr VarChar(50),
        pCategorySeq smallint unsigned,
        pSeq smallint unsigned
    )
BEGIN

Select 
	status,
    adv_1_img,
    adv_2_img,
    adv_3_img,
    adv_4_img,
    adv_5_img,
    adv_6_img,
    adv_7_img,
    banner_img
From m_img_product_category 
Where
	seq = pSeq and 
    category_seq = pCategorySeq;

END