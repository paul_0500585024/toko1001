CREATE DEFINER=`root`@`%` PROCEDURE `sp_image_approve_by_seq`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
	pCategorySeq SmallInt unsigned,
	pSeq SmallInt unsigned        
)
BEGIN

	Update m_img_product_category Set
		status = "A",
		auth_by = pUserID,
		auth_date = now(),
		modified_by = pUserID,
		modified_date = now()        
	Where
		seq = pSeq And
		category_seq = pCategorySeq;

	If Not Exists (Select category_seq From m_product_category_img Where category_seq = pCategorySeq) Then
		Insert Into m_product_category_img(
			category_seq,
			banner_img,
			banner_img_url,
			adv_1_img,
			adv_1_img_url,
			adv_2_img,
			adv_2_img_url,
			adv_3_img,
			adv_3_img_url,
			adv_4_img,
			adv_4_img_url,
			adv_5_img,
			adv_5_img_url,
			adv_6_img,
			adv_6_img_url,
			adv_7_img,
			adv_7_img_url,
			created_by,
			created_date,
			modified_by,
			modified_date
		) Values (
			pCategorySeq,
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			pUserID,
			Now(),
			pUserID,
			Now()
		);
	End If;

	Update m_product_category_img p 
	Join m_img_product_category i On
		i.category_seq = p.category_seq
	Set
		p.banner_img = Case When i.banner_img = "" Then p.banner_img Else i.banner_img End,
		p.banner_img_url = Case When i.banner_img_url = "" Then p.banner_img_url Else i.banner_img_url End,
		p.adv_1_img = Case When i.adv_1_img = "" Then p.adv_1_img Else i.adv_1_img End,
		p.adv_1_img_url = Case When i.adv_1_img_url = "" Then p.adv_1_img_url Else i.adv_1_img_url End,
		p.adv_2_img = Case When i.adv_2_img = "" Then p.adv_2_img Else i.adv_2_img End,
		p.adv_2_img_url = Case When i.adv_2_img_url = "" Then p.adv_2_img_url Else i.adv_2_img_url End,    
		p.adv_3_img = Case When i.adv_3_img = "" Then p.adv_3_img Else i.adv_3_img End,
		p.adv_3_img_url = Case When i.adv_3_img_url = "" Then p.adv_3_img_url Else i.adv_3_img_url End,
		p.adv_4_img = Case When i.adv_4_img = "" Then p.adv_4_img Else i.adv_4_img End,
		p.adv_4_img_url = Case When i.adv_4_img_url = "" Then p.adv_4_img_url Else i.adv_4_img_url End,
		p.adv_5_img = Case When i.adv_5_img = "" Then p.adv_5_img Else i.adv_5_img End,
		p.adv_5_img_url = Case When i.adv_5_img_url = "" Then p.adv_5_img_url Else i.adv_5_img_url End,
		p.adv_6_img = Case When i.adv_6_img = "" Then p.adv_6_img Else i.adv_6_img End,
		p.adv_6_img_url = Case When i.adv_6_img_url = "" Then p.adv_6_img_url Else i.adv_6_img_url End,
		p.adv_7_img = Case When i.adv_7_img = "" Then p.adv_7_img Else i.adv_7_img End,
		p.adv_7_img_url = Case When i.adv_7_img_url = "" Then p.adv_7_img_url Else i.adv_7_img_url End,
		p.modified_by = pUserID,
		p.modified_date = now()
	Where
		i.category_seq = pCategorySeq And
		i.seq = pSeq;

END