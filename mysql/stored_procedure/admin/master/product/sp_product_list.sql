CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_list`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pCurrPage Int,
        pRecPerPage Int,
        pDirSort VarChar(4),
        pColumnSort Varchar(50),
        pName VarChar(150),
        pDesc VarChar(150),
        pMerchantId INT (10) UNSIGNED,
        pMerchantName VARCHAR (100),
        pStatus CHAR(1)
    )
BEGIN
    -- Begin variables for Paging
    Declare startRec Int;
    Declare totalPage Int;
    Declare totalRec Int;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";

    If pDesc <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And a.`description` like '%" , escape_string(pDesc), "%'");
    End If;
    If pName <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And a.`name` like '%" , escape_string(pName), "%'");
    End If;
    If pMerchantId <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And a.`merchant_seq` = '" , pMerchantId, "'");
    End If;
    If pStatus <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And a.`status` = '" , escape_string(pStatus), "'");
    End If;  
    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "Select Count(a.seq) Into @totalRec From `m_product` a ";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
        Select
  a.`merchant_seq`,
  a.`seq`,
  a.`name`,
  a.`include_ins`,
  a.`category_l2_seq`,
  a.`category_ln_seq`,
  a.`notes`,
  a.`description`,
  a.`content`,
  a.`warranty_notes`,
  a.`p_weight_kg`,
  a.`p_length_cm`,
  a.`p_width_cm`,
  a.`p_height_cm`,
  a.`b_weight_kg`,
  a.`b_length_cm`,
  a.`b_width_cm`,
  a.`b_height_cm`,
  a.`status`,
  a.`created_by`,
  a.`created_date`,
  a.`modified_by`,
  a.`modified_date`,
  b.`name` as merchant_name
        From `m_product` a
		left outer join m_merchant b on a.`merchant_seq`=b.seq
    ";

    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);

    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END