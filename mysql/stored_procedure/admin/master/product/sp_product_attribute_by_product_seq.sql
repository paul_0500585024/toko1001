CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_product_attribute_by_product_seq`(
        IN pUserID VARCHAR(25),
        IN pIPAddr VARCHAR(50),
        IN pSeq BIGINT
    )
BEGIN

	Select 
		attribute_value_seq
	From m_product_attribute
	Where
		product_seq = pSeq;
END