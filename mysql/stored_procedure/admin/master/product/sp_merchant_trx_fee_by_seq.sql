CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_merchant_trx_fee_by_seq`(
	pSeq Int unsigned
)
BEGIN

	Select 
		category_l1_seq 
	From `m_merchant_trx_fee`
	Where 
		`merchant_seq` = pSeq And 
		category_l2_seq = 0 ;

END