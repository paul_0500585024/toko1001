CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_product_category_merchant_by_parent`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pMerchantId INT
    )
BEGIN

SELECT 
	t.parent_seq AS Parent , 
    GROUP_CONCAT(t.seq,'~',t.name, '~',t.`parent_seq` ORDER BY parent_seq) AS Children
FROM m_product_category t 
JOIN 
	(select 
		category_l1_seq as seq 
	 from 
		m_merchant_trx_fee 
	where 
		merchant_seq = pMerchantId and 
		category_l2_seq = 0 ) f ON
 t.parent_seq=f.seq  
where 
	t.seq > 0
GROUP BY 
	parent_seq;
END