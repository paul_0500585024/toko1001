CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_product_spec_new_add`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pProductSeq BIGINT(10) UNSIGNED,
        pName VARCHAR(50),
        pValue VARCHAR(50)
    )
BEGIN
Declare new_seq INT;

	Select 
		Max(seq) + 1 Into new_seq
	From `m_product_spec_new` where product_seq=pProductSeq;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;

	Insert Into `m_product_spec_new`(     
		product_seq,
		seq,
        `name`,
        `value`,
		created_by,
		created_date,
		modified_by,
		modified_date
	) Values (
		pProductSeq,
        new_seq,
		pName,
        pValue,
		pUserID,
		Now(),
		pUserID,
		Now()
	);

END