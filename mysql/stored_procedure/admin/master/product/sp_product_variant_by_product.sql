CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_product_variant_by_product`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq BIGINT
    )
BEGIN

	Select `product_seq`,
  `variant_value_seq`,
  `seq`,
  `product_price`,
  `disc_percent`,
  `sell_price`,
  `order`,
  `max_buy`,
  `status`,
  `pic_1_img`,
  `pic_2_img`,
  `pic_3_img`,
  `pic_4_img`,
  `pic_5_img`,
  `pic_6_img`,
  `1star`,
  `2star`,
  `3star`,
  `4star`,
  `5star`,
  `created_by`,
  `created_date`,
  `modified_by`,
  `modified_date`
	From m_product_variant 
	Where
	product_seq = pSeq order by seq;
END