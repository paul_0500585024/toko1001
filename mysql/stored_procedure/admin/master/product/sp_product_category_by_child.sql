CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_category_by_child`(
        pSeq INTEGER
    )
BEGIN
    DECLARE cnt int default 0;
    CREATE temporary table tmpTable 
    (
       `seq` int, `name` varchar(50), `parent_seq` int, path varchar(500)  ,idpath varchar(50)
    )
    engine=memory 
    select seq, `name`, parent_seq, `name` AS 'path', `seq` AS 'idpath' from m_product_category where seq = pSeq;
    select parent_seq into cnt from tmpTable;
    
    while cnt <> 0 do
       Update tmpTable tt, (select seq, `name`, parent_seq, `name` AS 'path', `seq` AS 'idpath' from m_product_category where seq>0) t 
       set tt.parent_seq = t.parent_seq, 
              tt.path = concat(t.name, ' > ', tt.path),
               tt.idpath = concat(t.seq, ',', tt.idpath)
       WHERE tt.parent_seq = t.seq;
       select parent_seq into cnt from tmpTable;
    end while;
    select * from tmpTable;
    drop table tmpTable;
END