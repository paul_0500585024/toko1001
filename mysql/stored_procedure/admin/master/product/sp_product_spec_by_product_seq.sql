CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_product_spec_by_product_seq`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq BIGINT(20) unsigned
    )
BEGIN


	SELECT 
	`product_seq`,
	`seq`,
	`name`,
	`value`,
	`created_by`,
	`created_date`,
	`modified_by`,
	`modified_date`
	FROM 
	`m_product_spec`
	Where 
	product_seq = pSeq 
	order by seq;
	
end