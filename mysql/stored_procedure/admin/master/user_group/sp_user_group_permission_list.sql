CREATE DEFINER=`root`@`%` PROCEDURE `sp_user_group_permission_list`(
	pUserID VarChar(25),
    pIPAddr VarChar(50),
    pCurrPage Int,
    pRecPerPage Int,
    pDirSort VarChar(4),
    pColumnSort Varchar(50),
	pUserGroupSeq tinyint(3)
)
BEGIN

  -- Begin variables for Paging
    Declare startRec Int;
    Declare totalPage Int;
    Declare totalRec Int;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
      
    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "Select Count(m.menu_cd) Into @totalRec From m_user_group_permission u Join s_menu m on m.menu_cd = u.menu_cd ";
	
    Set @sqlCommand = Concat(@sqlCommand, " Where u.user_group_seq = '" , pUserGroupSeq, "'");

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
        Select
			u.menu_cd,
			m.`name`,
			u.can_add,            
			u.can_edit,
            u.can_view,
			u.can_delete,
            u.can_print,
            u.can_auth,
            m.active,
			u.created_by,
			u.created_date,
            u.modified_by,
            u.modified_date
		From m_user_group_permission u Join s_menu m 
			on m.menu_cd = u.menu_cd 
    ";

	Set @sqlCommand = Concat(@sqlCommand, " Where u.user_group_seq ='" , pUserGroupSeq, "'");
    
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;


END