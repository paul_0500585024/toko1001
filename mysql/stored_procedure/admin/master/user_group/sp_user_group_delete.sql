CREATE DEFINER=`root`@`%` PROCEDURE `sp_user_group_delete`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq TinyInt unsigned
    )
BEGIN

Delete From m_user_group Where seq = pSeq;


END