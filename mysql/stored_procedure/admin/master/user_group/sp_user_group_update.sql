CREATE DEFINER=`root`@`%` PROCEDURE `sp_user_group_update`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pSeq smallint(5),
    pName VarChar(50),
    pActive Char(1)
    )
BEGIN

update m_user_group set
    `name` = pName,
    active = pActive,
    modified_by = pUserID,
    modified_date = now()
Where
	seq = pSeq;

END