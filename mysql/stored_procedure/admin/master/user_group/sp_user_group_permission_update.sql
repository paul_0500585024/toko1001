CREATE DEFINER=`root`@`%` PROCEDURE `sp_user_group_permission_update`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pUserGroupSeq TinyInt unsigned,
        pOldMenuCD VarChar(25),
        pMenuCD Varchar(25),
        pCanAdd Char(1),
        pCanEdit Char(1),
        pCanView Char(1),
        pCanDelete Char(1),
        pCanPrint Char(1),
        pCanAuth Char(1)
)
BEGIN

update m_user_group_permission set
	menu_cd = pMenuCD,
	can_add = pCanAdd,
    can_edit = pCanEdit,
    can_view = pCanView,
    can_delete = pCanDelete,
    can_print = pCanPrint,
    can_auth = pCanAuth
where
	user_group_seq = pUserGroupSeq and
    menu_cd = pOldMenuCD;

END