CREATE DEFINER=`root`@`%` PROCEDURE `sp_user_group_add`(
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pName VarChar(50),
    pActive Varchar(1)
)
BEGIN
    Declare new_seq Int;

SELECT 
    MAX(seq) + 1
INTO new_seq FROM
    m_user_group;

    If new_seq Is Null Then
        Set new_seq = 1;
    End If;

    Insert Into m_user_group (
        seq,
        `name`,
        active,
        created_by,
        created_date,
        modified_by,
        modified_date
    ) Values (
        new_seq,
        pName,
        pActive,
        pUserID,
        Now(),
        pUserID,
        Now()
    );
END