CREATE DEFINER=`root`@`%` PROCEDURE `sp_user_group_permission_by_seq`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
	pSeq TinyInt unsigned,
    pMenuCD VarChar(25)
)
BEGIN

Select 
	m.module_seq,
	u.user_group_seq,
    u.menu_cd,
    m.name as menu_name,
    u.can_add,
    u.can_edit,
    u.can_view,
    u.can_auth,
    u.can_delete,
    u.can_print,
    u.created_by,
    u.created_date,
    u.modified_by,
    u.modified_date
from
	m_user_group_permission u 
		join s_menu m on 
			m.menu_cd = u.menu_cd 
		Join s_menu_module mm on
			mm.seq = m.module_seq
Where
	u.user_group_seq = pSeq and
    u.menu_cd = pMenuCD;
    
    
END