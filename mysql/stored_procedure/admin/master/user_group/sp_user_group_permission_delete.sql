CREATE DEFINER=`root`@`%` PROCEDURE `sp_user_group_permission_delete`(
		pUserID VarChar(25),
        pIPAddr VarChar(50),
        pMenuCD VarChar(25)

)
BEGIN

Delete From m_user_group_permission where menu_cd = pMenuCD;

END