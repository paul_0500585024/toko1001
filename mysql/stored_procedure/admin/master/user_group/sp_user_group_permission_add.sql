CREATE DEFINER=`root`@`%` PROCEDURE `sp_user_group_permission_add`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pUserGroupSeq TinyInt unsigned,
        pMenuCD Varchar(25),
        pCanAdd Char(1),
        pCanEdit Char(1),
        pCanView Char(1),
        pCanDelete Char(1),
        pCanPrint Char(1),
        pCanAuth Char(1)
)
BEGIN

Insert Into m_user_group_permission (
	user_group_seq,
    menu_cd,
    can_add,
    can_edit,
    can_view,
    can_delete,
    can_print,
    can_auth,
	created_by,
    created_date,
    modified_by,
    modified_date
) values (
	pUserGroupSeq,
    pMenuCD,
    pCanAdd,
    pCanEdit,
    pCanView,
    pCanDelete,
    pCanPrint,
    pCanAuth,
    pUserID,
    now(),
    pUserID,
    now()
);
END