CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_user_group_by_seq`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
	pSeq TinyInt
)
BEGIN

	Select 
        seq,
		`name`,
		active
	From m_user_group
	Where
		seq = pSeq;
END