CREATE DEFINER=`root`@`%` PROCEDURE `sp_promo_credit_approve_by_seq`(
        pUserID VarChar(25),
		pIPAddr VarChar(50),
        pSeq	int unsigned
        
    )
BEGIN

update m_promo_credit set    
	`status` = 'A',
    modified_by = pUserID,
    modified_date = Now()    
Where
	seq = pSeq AND `status` = 'N';
END