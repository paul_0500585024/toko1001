CREATE DEFINER=`root`@`%` PROCEDURE `sp_promo_credit_bank_add`(
    pUserID VarChar(25),
    pIPAddr VarChar(50),
	pPromoSeq int unsigned,
    pBankSeq int unsigned
)
BEGIN
Declare new_seq smallint unsigned;
	
	Select 
		Max(seq) + 1 Into new_seq
	From m_promo_credit_bank;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
    Insert Into m_promo_credit_bank(
		seq,
		promo_seq, 
		bank_credit_seq, 
		created_by, 
		created_date
    ) Values (
		new_seq,
		pPromoSeq,
		pBankSeq,
        pUserID,
        Now()
    );
END