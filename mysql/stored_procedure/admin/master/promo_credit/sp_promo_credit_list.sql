CREATE DEFINER=`root`@`%` PROCEDURE `sp_promo_credit_list`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pCurrPage Int,
        pRecPerPage Int,
        pDirSort VarChar(4),
        pColumnSort Varchar(50),
        pPromoCreditName varchar(500),
        pPromoCreditPeriodFrom date,
        pPromoCreditPeriodTo date,
        pStatus char(1)
    )
BEGIN
    -- Begin variables for Paging
    Declare startRec Int;
    Declare totalPage Int;
    Declare totalRec Int;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";

    If pPromoCreditName <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And promo_credit_name like '%" , escape_string(pPromoCreditName), "%'");
    End If;
    
    If pStatus <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And status = '" , pStatus, "'");
    End If;
    
    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "Select Count(seq) Into @totalRec From m_promo_credit";
	Set @sqlCommand = Concat(@sqlCommand, " Where (promo_credit_period_from between '" , 
								pPromoCreditPeriodFrom, "' and '", pPromoCreditPeriodTo, "') or (promo_credit_period_to between '",
									pPromoCreditPeriodFrom, "' and '", pPromoCreditPeriodTo, "')");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
        SELECT 
			seq, 
            promo_credit_name,
            promo_credit_period_from,
            promo_credit_period_to,
            minimum_nominal,
            maximum_nominal,
            status, 
            '' AS detail,
            created_by,
            created_date,
            modified_by,
            modified_date
		FROM
			m_promo_credit";

    Set @sqlCommand = Concat(@sqlCommand, " Where (promo_credit_period_from between '" , 
								pPromoCreditPeriodFrom, "' and '", pPromoCreditPeriodTo, "') or (promo_credit_period_to between '",
									pPromoCreditPeriodFrom, "' and '", pPromoCreditPeriodTo, "')");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;

    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);

    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

SELECT pCurrPage, totalPage, totalRec AS total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END