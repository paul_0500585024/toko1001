CREATE DEFINER=`root`@`%` PROCEDURE `sp_promo_credit_reject_by_seq`(
        pUserID VarChar(25),
		pIPAddr VarChar(50),
        pSeq	int unsigned
        
    )
BEGIN

update m_promo_credit set    
	`status` = 'R',
	modified_by = pUserID,  
    modified_date = now()
    
Where
	seq = pSeq and `status` = 'N';

END