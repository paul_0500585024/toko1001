CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_promo_credit_bank_by_seq`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
	pPromoSeq int unsigned
)
BEGIN
SELECT 
    p.promo_seq, 
    c.bank_seq, 
    c.seq,
    b.bank_name,
	CONCAT(b.bank_name,
		' - ',
		c.credit_month,
		' Bulan') as bank_month
FROM
    m_bank_credit c left join m_promo_credit_bank p
		on p.bank_credit_seq = c.seq and
		   p.promo_seq = pPromoseq
					left join m_bank b
		on c.bank_seq = b.seq;					
END