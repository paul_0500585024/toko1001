CREATE DEFINER=`root`@`%` PROCEDURE `sp_promo_credit_update`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq Int unsigned,
		pPromoCreditName VARCHAR(500),
		pPromoCreditPeriodFrom date,
		pPromoCreditPeriodTo date,
		pMinimumNominal DECIMAL(10,0),
		pMaximumNominal DECIMAL(10,0)
    )
BEGIN

update m_promo_credit set
		promo_credit_name = pPromoCreditName,
        promo_credit_period_from = pPromoCreditPeriodFrom,
        promo_credit_period_to = pPromoCreditPeriodTo,
	minimum_nominal = pMinimumNominal,
	maximum_nominal = pMaximumNominal,
        modified_by = pUserID,
        modified_date = now()
Where
	seq = pSeq;

END