CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_promo_credit_product_by_seq`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pPromoCreditSeq int unsigned
    )
BEGIN
SELECT 
    pcp.promo_credit_seq,
    pcp.product_variant_seq,
    p.name AS product_name,
    pcp.created_by,
    pcp.created_date
FROM
    m_promo_credit_product pcp
        JOIN
    m_product_variant pv ON pcp.product_variant_seq = pv.seq
        JOIN
    m_product p ON pv.product_seq = p.seq
WHERE
    promo_credit_seq = pPromoCreditSeq;
END