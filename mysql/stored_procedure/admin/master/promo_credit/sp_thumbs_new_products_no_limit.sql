CREATE DEFINER=`root`@`%` PROCEDURE `sp_thumbs_new_products_no_limit`(
 	IN pWHERE_CONDITION TEXT,
 	IN pORDER_CONDITION TEXT
 )
BEGIN
 IF(pORDER_CONDITION = '') THEN SET @ORDER = 'ORDER BY pv.seq DESC'; ELSE SET @ORDER = pORDER_CONDITION; END IF;
 IF(pWHERE_CONDITION = '') THEN SET @WHERE = 'WHERE pv.active = ''1'' AND pv.status IN (''L'',''C'')'; ELSE SET @WHERE = pWHERE_CONDITION; END IF;
 SET @sql = CONCAT("SELECT SQL_CALC_FOUND_ROWS
 	pv.product_seq,
 	pv.pic_1_img AS image,
 	p.name AS `name`,
 	p.`merchant_seq` AS merchant_seq,
 	vv.seq AS variant_seq,
 	vv.value AS `variant_value`,
 	pv.`product_price` AS `product_price`,
 	pv.`sell_price` AS `sell_price`,
 	pv.`seq` AS `product_variant_seq`,
 	ps.`stock` AS `stock`,
 	ps.`merchant_sku` AS `sku`
 FROM 
 	m_product_variant pv
 LEFT JOIN m_product p 
 	ON pv.product_seq=p.seq
 INNER JOIN m_variant_value vv 
 	ON pv.variant_value_seq=vv.seq 
 LEFT JOIN m_product_stock ps
 	ON ps.`product_variant_seq` = pv.`seq` ",@WHERE," ",@ORDER);
 PREPARE STMT FROM @sql;
 EXECUTE STMT;
 DEALLOCATE PREPARE STMT;
 END