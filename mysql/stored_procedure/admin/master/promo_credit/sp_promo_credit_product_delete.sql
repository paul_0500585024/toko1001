CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_promo_credit_product_delete`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pPromoCreditSeq int unsigned
    )
BEGIN
delete from m_promo_credit_product where  promo_credit_seq = pPromoCreditSeq;
END