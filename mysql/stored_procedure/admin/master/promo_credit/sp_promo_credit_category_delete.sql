DROP PROCEDURE IF EXISTS `sp_promo_credit_category_delete`;
DELIMITER $$
CREATE PROCEDURE `sp_promo_credit_category_delete` (
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pPromoCreditSeq int unsigned
)
BEGIN
DELETE FROM m_promo_credit_category WHERE  promo_credit_seq = pPromoCreditSeq;
END$$
