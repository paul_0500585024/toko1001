CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_promo_credit_product_add`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pPromoCreditSeq int unsigned,
        pProductVariantSeq int unsigned
    )
BEGIN
	Insert Into m_promo_credit_product(     
		promo_credit_seq, 
        product_variant_seq,
        created_by,
        created_date
	) Values (
		pPromoCreditSeq,
        pProductVariantSeq,
		pUserID,
		Now()
	);

END