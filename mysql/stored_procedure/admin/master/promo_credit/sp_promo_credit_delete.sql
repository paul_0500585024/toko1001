CREATE DEFINER=`root`@`%` PROCEDURE `sp_promo_credit_delete`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq int unsigned
    )
BEGIN

Delete From m_promo_credit Where seq = pSeq;

END