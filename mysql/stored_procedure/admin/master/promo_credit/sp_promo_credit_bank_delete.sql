CREATE DEFINER=`root`@`%` PROCEDURE `sp_promo_credit_bank_delete`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pPromoSeq int unsigned
    )
BEGIN

Delete From m_promo_credit_bank Where promo_seq = pPromoSeq;

END