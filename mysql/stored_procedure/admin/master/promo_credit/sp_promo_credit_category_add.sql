DROP PROCEDURE IF EXISTS `sp_promo_credit_category_add`;
DELIMITER $$
CREATE PROCEDURE `sp_promo_credit_category_add` (
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pPromoCreditSeq INT Unsigned,
    pCategorySeq INT Unsigned
)
BEGIN
    Insert Into m_promo_credit_category(
       promo_credit_seq,
       category_seq,
       created_by, 
       created_date
    ) values (
       pPromoCreditSeq,
       pCategorySeq,
       pUserID,
       Now()
    );
END$$
