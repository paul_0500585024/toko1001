CREATE DEFINER=`root`@`%` PROCEDURE `sp_promo_credit_by_seq`(
	  pUserID VarChar(25),
	  pIPAddr VarChar(50),
      pSeq int unsigned
)
BEGIN
SELECT 
    seq,
    promo_credit_name,
    promo_credit_period_from,
    promo_credit_period_to,
    minimum_nominal,
    maximum_nominal,
    status,
    created_by,
    created_date,
    modified_by,
    modified_date
FROM
    m_promo_credit
WHERE
    seq = pSeq;
END