DROP PROCEDURE IF EXISTS `sp_promo_credit_category_by_seq`;
DELIMITER $$
CREATE PROCEDURE `sp_promo_credit_category_by_seq` (
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pPromoCreditSeq int unsigned
)
BEGIN
    SELECT
        pcc.promo_credit_seq,
        pcc.category_seq,
        pcc.created_by, 
        pcc.created_date,
        pc.name,
        pc.seq 
    FROM m_promo_credit_category pcc 
       LEFT JOIN m_product_category pc ON pcc.category_seq = pc.seq
    WHERE pcc.promo_credit_seq = pPromoCreditSeq AND pc.level = '1';
END$$
