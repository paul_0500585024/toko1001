CREATE DEFINER=`root`@`%` PROCEDURE `sp_promo_credit_add`(
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pPromoCreditName VARCHAR(500),
    pPromoCreditPeriodFrom date,
    pPromoCreditPeriodTo date,
    pMinimumNominal DECIMAL(10,0),
    pMaximumNominal DECIMAL(10,0),
    pStatus char(1)
)
BEGIN
    Declare new_seq int Unsigned;

SELECT 
    MAX(seq) + 1
INTO new_seq FROM
    m_promo_credit;

    If new_seq Is Null Then
        Set new_seq = 1;
    End If;

    Insert Into m_promo_credit(
		seq, 
        promo_credit_name,
        promo_credit_period_from,
        promo_credit_period_to,
        minimum_nominal,
        maximum_nominal,
        status, 
        created_by,
        created_date,
        modified_by,
        modified_date
    ) Values (
        new_seq,
        pPromoCreditName,
        pPromoCreditPeriodFrom,
        pPromoCreditPeriodTo,
        pMinimumNominal,
        pMaximumNominal,
        pStatus,
        pUserID,
        Now(),
        pUserID,
        Now()
    );
END