DROP PROCEDURE IF EXISTS sp_redeem_agent_commision_component_add;
DELIMITER $$
CREATE PROCEDURE sp_redeem_agent_commision_component_add (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pSeq INTEGER(10) UNSIGNED,
    pType CHAR(3),
    pMType CHAR(1)
)
BEGIN
    INSERT INTO t_redeem_agent_component(
	redeem_seq,
	partner_seq,
	type,
	mutation_type,
	total,
	created_by,
	created_date,
	modified_by,
	modified_date
	)
    SELECT 
        pSeq, ma.partner_seq, pType, pMtype, SUM(tra.total) total, pUserID, Now(), pUserID, Now() 
    FROM t_redeem_agent tra 
    JOIN m_agent ma ON tra.agent_seq=ma.seq 
    WHERE tra.redeem_seq=pSeq
    GROUP BY ma.partner_seq;
END$$
