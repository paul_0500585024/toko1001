DELIMITER $$
CREATE PROCEDURE sp_redeem_component_by_seq_partner
(
        `pUserID` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pSeq` INTEGER(10) UNSIGNED,
        `pPartnerSeq` INTEGER(10) UNSIGNED
    )
BEGIN
	Select
	`redeem_seq`,
	`partner_seq`,
	`type`,
	`mutation_type`,
	`total`,
	`created_by`,
	`created_date`,
	`modified_by`,
	`modified_date`
	From t_redeem_agent_component
	Where
	redeem_seq = pSeq and partner_seq=pPartnerSeq;
END $$
DELIMITER ;
