DROP PROCEDURE IF EXISTS `sp_agent_refund_reject`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_refund_reject` (
    pUserID VarChar(100),
    pIPAddr VarChar(50),
    pSeq int(10),
    pAgentSeq int(10)
)
BEGIN

UPDATE t_agent_account 
SET `status` = 'R' , 
modified_date = now() , 
modified_by = pUserID 
WHERE seq = pSeq  
AND agent_seq = pAgentSeq;


END$$


