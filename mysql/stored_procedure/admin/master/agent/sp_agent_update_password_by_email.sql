DELIMITER $$
CREATE PROCEDURE sp_agent_update_password_by_email
(
        `pEmail` VARCHAR(100),
        `pNewpass` TEXT
    )
BEGIN
update m_agent set password = pNewpass where email = pEmail;
END $$ 
DELIMITER ; 
