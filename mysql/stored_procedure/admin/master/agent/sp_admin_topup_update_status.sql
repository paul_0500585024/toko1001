DELIMITER $$
CREATE PROCEDURE sp_admin_topup_update_status
(
        `pUserID` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pSeq` BIGINT UNSIGNED,
        `pagentSeq` BIGINT UNSIGNED,
        `pStatus` VARCHAR(1)
    )
BEGIN
update t_agent_account set
	`status` = pStatus,
	modified_by = pUserID,
	modified_date = Now()
Where
	seq=pSeq AND agent_seq=pagentSeq and `status` = 'T';
END $$ 
DELIMITER ; 
