DELIMITER $$
DROP PROCEDURE sp_agent_order_redeem_info $$
CREATE PROCEDURE sp_agent_order_redeem_info
(
        `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pAgentSeq` BIGINT(10)
    )
BEGIN

    SELECT sum(top.qty*top.sell_price*top.commission_fee_percent/100) as pending_commission FROM t_order_product top
    join t_order tor on top.order_seq=tor.seq
    join t_order_merchant tom on top.order_seq=tom.order_seq and top.merchant_info_seq=tom.merchant_info_seq
    where tor.payment_status='P' and top.product_status='R' and tom.order_status in ('R','S') and tor.agent_seq=pAgentSeq;

    SELECT sum(top.qty*top.sell_price*top.commission_fee_percent/100) as commission FROM t_order_product top
    join t_order tor on top.order_seq=tor.seq
    join t_order_merchant tom on top.order_seq=tom.order_seq and top.merchant_info_seq=tom.merchant_info_seq
    where tor.payment_status='P' and top.product_status='R' and tom.order_status = 'D' and tor.agent_seq=pAgentSeq;

END $$
DELIMITER ;
