DROP PROCEDURE IF EXISTS `sp_admin_get_data_withdraw_agent`;
DELIMITER $$
CREATE PROCEDURE `sp_admin_get_data_withdraw_agent` (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pSeq BIGINT(10),
    pAgentSeq BIGINT(10)
)
BEGIN

SELECT
	ma.seq,
    ma.agent_seq,
    m.name,
    m.email,
    m.deposit_amt,
    ma.trx_no,
	ma.deposit_trx_amt,
	ma.non_deposit_trx_amt,
	ma.status
FROM t_agent_account ma
JOIN m_agent m
	ON ma.agent_seq = m.seq
WHERE
	ma.seq = pSeq AND ma.agent_seq = pAgentSeq;

END$$


