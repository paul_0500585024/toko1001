DELIMITER $$
CREATE PROCEDURE sp_agent_order_product_return_list
(
        `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pCurrPage` INTEGER UNSIGNED,
        `pRecPerPage` INTEGER UNSIGNED,
        `pDirSort` VARCHAR(4),
        `pColumnSort` VARCHAR(50),
        `pAgentSeq` BIGINT(10)
    )
BEGIN
    -- Begin variables for Paging
    Declare startRec Int unsigned;
    Declare totalPage Int unsigned;
    Declare totalRec Int unsigned;
    -- End variables for Paging
    -- Begin SQL Where
    Declare sqlWhere VarChar(500);
	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
    
    -- End SQL Where
    -- Begin Paging Info
    Set @sqlCommand = "Select Count(o.seq) Into @totalRec FROM	t_order o
		JOIN
	t_order_merchant om ON o.seq = om.order_seq
		JOIN
	t_order_product op ON om.order_seq = op.order_seq
		AND om.merchant_info_seq = op.merchant_info_seq
		JOIN
	t_order_product_return opt ON op.order_seq = opt.order_seq
		AND op.product_variant_seq = opt.product_variant_seq
		JOIN
	m_product_variant pv ON opt.product_variant_seq = pv.seq
		JOIN
	m_product p ON pv.product_seq = p.seq
		JOIN
	m_variant_value vv ON pv.variant_value_seq = vv.seq
		JOIN
	m_merchant_info mi ON op.merchant_info_seq = mi.seq
		JOIN
	m_merchant m ON mi.merchant_seq = m.seq";
    Set @sqlCommand = Concat(@sqlCommand, " Where o.agent_seq ='" ,pAgentSeq, "'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);
        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;
        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info
    Set @sqlCommand = "
        SELECT 
		opt.seq,
		opt.return_no,
		op.created_date,
		o.order_no,
		opt.awb_member_no,
		pv.pic_1_img AS img_product,
		p.name AS product_name,
		vv.value AS variant_name,
		opt.qty,
		opt.return_status,
		opt.shipment_status,
		opt.review_member,
		opt.review_admin,
		opt.review_merchant,
		o.agent_seq,
		pv.variant_value_seq,
		opt.product_variant_seq,
		m.seq as me_seq
	FROM
		t_order o
			JOIN
		t_order_merchant om ON o.seq = om.order_seq
			JOIN
		t_order_product op ON om.order_seq = op.order_seq
			AND om.merchant_info_seq = op.merchant_info_seq
			JOIN
		t_order_product_return opt ON op.order_seq = opt.order_seq
			AND op.product_variant_seq = opt.product_variant_seq
			JOIN
		m_product_variant pv ON opt.product_variant_seq = pv.seq
			JOIN
		m_product p ON pv.product_seq = p.seq
			JOIN
		m_variant_value vv ON pv.variant_value_seq = vv.seq
			JOIN
		m_merchant_info mi ON op.merchant_info_seq = mi.seq
			JOIN
		m_merchant m ON mi.merchant_seq = m.seq	";
    Set @sqlCommand = Concat(@sqlCommand, " Where o.agent_seq ='" ,pAgentSeq, "'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    Select pCurrPage, totalPage, totalRec as total_rec;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set @totalRec = Null;
    Set @sqlCommand = Null;
END $$ 
DELIMITER ; 
