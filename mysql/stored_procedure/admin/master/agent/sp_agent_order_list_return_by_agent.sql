DELIMITER $$
CREATE PROCEDURE sp_agent_order_list_return_by_agent
(
        `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pAgentSeq` INTEGER UNSIGNED,
        `pOrderStatus` CHAR(1),
        `pFinishDate` SMALLINT(1) UNSIGNED
    )
BEGIN
    select
	o.seq,
	o.agent_seq,
	m.name as agent_name,
	o.order_no,
	o.order_date,
	o.total_order,
	o.payment_status,
	o.paid_date,
	pgm.name as payment_method
		from
			m_agent m
		Join
			t_order o 
			On m.seq = o.agent_seq
		Join 
			m_payment_gateway_method pgm
			On o.pg_method_seq = pgm.seq
	Where 
	o.seq in 
		( select tom.order_seq from t_order_merchant tom join t_order tor on tom.`order_seq`=tor.`seq` where 
		tom.received_date >= date_add( now(),interval - pFinishDate day) and tom.received_date<>'0000-00-00' AND 
		tom.order_status=pOrderStatus and tor.agent_seq= pAgentSeq ) 
	and  o.agent_seq= pAgentSeq ;
END $$ 
DELIMITER ; 
