DELIMITER $$
CREATE PROCEDURE sp_agent_order_list_detail_by_order_no
(
        `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pAgentSeq` BIGINT UNSIGNED,
        `pHSeq` VARCHAR(50)
    )
BEGIN
	Select
	op.order_seq,
	o.order_no,
	o.order_date,
	o.payment_status,
	o.pg_method_seq,
	o.total_payment,
	o.total_order,
	p.name as product_name,
	v.display_name,
	vv.value,
	p.p_weight_kg,
	p.p_length_cm,
	p.p_width_cm,
	p.p_height_cm,
	op.qty,
	op.sell_price,
	op.commission_fee_percent,
	op.product_status,
	op.trx_fee_percent,
	pv.pic_1_img as img_src,
	p.merchant_seq
	From  t_order o
	Join t_order_product op 
		On o.seq = op.order_seq
	Join m_product_variant pv
		On op.product_variant_seq = pv.seq
	Join m_product p
		On pv.product_seq = p.seq
	Join m_variant_value vv
		On op.variant_value_seq = vv.seq 
	Join m_variant v
		On vv.variant_seq = v.seq
        where o.order_no = pHSeq AND o.agent_seq = pAgentSeq
	;
END $$ 
DELIMITER ; 
