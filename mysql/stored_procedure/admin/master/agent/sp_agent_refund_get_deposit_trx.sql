DROP PROCEDURE IF EXISTS `sp_agent_refund_get_deposit_trx`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_refund_get_deposit_trx` (
        pUserID VarChar(100),
	pIPAddr VarChar(50),
	pRef_awb_no varchar(100)
)
BEGIN
SELECT 
	deposit_trx_amt
FROM 
	t_agent_account 
WHERE 
	trx_no = pRef_awb_no
    ;
END$$


