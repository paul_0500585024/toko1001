DELIMITER $$
CREATE PROCEDURE sp_agent_add_signup
(
        `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pEmail` VARCHAR(100),
        `pName` VARCHAR(100),
        `pDate` DATE,
        `pGender` VARCHAR(1),
        `pPhone` VARCHAR(20),
        `pPassword` VARCHAR(100),
        `pImage` VARCHAR(100),
        `pPartnerSeq` INTEGER(10)
    )
BEGIN
Declare new_seq INT Unsigned;
Select
Max(seq) + 1 Into new_seq
    From m_agent;
    If new_seq Is Null Then
        Set new_seq = 1;
    End If;

	Insert into m_agent(
		seq,
		email,
		password,
		`name`,
                partner_seq,
		birthday,
		gender,
		mobile_phone,
		profile_img,
		deposit_amt,
		`status`,
		created_by,
		created_date,
		modified_by,
		modified_date
	) values (
		new_seq,
		pEmail,
		pPassword,
		pName,
                pPartnerSeq,
		pDate,
		pGender,
		pPhone,
		pImage,
		0,
		'A',
		'SYSTEM',
		now(),
		'SYSTEM',
		now()
	);
END $$
DELIMITER ;
