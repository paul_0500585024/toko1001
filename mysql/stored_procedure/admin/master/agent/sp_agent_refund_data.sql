DROP PROCEDURE IF EXISTS `sp_agent_refund_data`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_refund_data` (
    pUserID VarChar(100),
    pIPAddr VarChar(50),
    pSeq int(10),
    pAgentSeq int(10)
)
BEGIN

SELECT
	pgm.name as pgm_method, 
	m.email as email_agent,
    m.`name`, 
    m.deposit_amt , 
    tma.`status` as status_now , 
    tma.agent_seq,
    tma.deposit_trx_amt
    
FROM 
	t_agent_account tma , 
    m_agent m , 
    m_payment_gateway_method pgm 
WHERE 
	tma.seq = pSeq and 
    tma.agent_seq = pAgentSeq and 
    tma.agent_seq = m.seq and 
    tma.pg_method_seq = pgm.seq; 

END$$


