DELIMITER $$
CREATE PROCEDURE sp_redeem_agent_list
(
        `pUserID` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pCurrPage` INTEGER,
        `pRecPerPage` INTEGER,
        `pDirSort` VARCHAR(20),
        `pColumnSort` VARCHAR(50),
        `pFdate` VARCHAR(10),
        `pTdate` VARCHAR(10),
        `pStatus` CHAR(1)
    )
BEGIN
    -- Begin variables for Paging
    Declare startRec Int;
    Declare totalPage Int;
    Declare totalRec Int;
    -- End variables for Paging
    -- Begin SQL Where
    Declare sqlWhere VarChar(500);
	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
    If pFdate <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And a.from_date >= '" , pFdate, "'");
    End If;
    If pTdate <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And a.to_date <= '" , pTdate, "'");
    End If;
    If pStatus <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And a.status= '" , pStatus, "'");
    End If;
    -- End SQL Where
    -- Begin Paging Info
    Set @sqlCommand = "Select Count(seq) Into @totalRec From t_redeem_agent_period a";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);
        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;
        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info
    Set @sqlCommand = "
        Select
	        a.seq,a.from_date,a.to_date,a.status,b.total,a.`created_by`,a.`created_date`,a.`modified_by`,a.`modified_date`
        From t_redeem_agent_period a left outer join (select sum(CASE WHEN c.`mutation_type`= 'D' THEN c.`total` ELSE c.`total` * -1 END) as total,redeem_seq FROM t_redeem_agent_component c group by c.redeem_seq ) b
        on a.seq=b.`redeem_seq`
    ";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    Select pCurrPage, totalPage, totalRec as total_rec;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set @totalRec = Null;
    Set @sqlCommand = Null;
END $$ 
DELIMITER ; 
