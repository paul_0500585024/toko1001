DELIMITER $$
CREATE PROCEDURE sp_deposit_agent_addtract
(
        `pUserID` VARCHAR(25),
        `pIPAddress` VARCHAR(25),
        `pagentSeq` INTEGER UNSIGNED,
        `pDepositAmt` DECIMAL(10,0)
    )
BEGIN
update m_agent set 
	deposit_amt = deposit_amt + pDepositAmt,
	modified_by = pUserID,
	modified_date = now()
where
	seq = pagentSeq;
END $$ 
DELIMITER ; 
