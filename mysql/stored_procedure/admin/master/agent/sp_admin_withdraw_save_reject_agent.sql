DROP PROCEDURE IF EXISTS `sp_admin_withdraw_save_reject_agent`;
DELIMITER $$
CREATE PROCEDURE `sp_admin_withdraw_save_reject_agent` (
        pUserID VARCHAR(25),
	pIPAddr VARCHAR(50),
	pSeq BIGINT(10),
	pAgentSeq BIGINT(10)
)
BEGIN

UPDATE t_agent_account
SET
	`status` = 'R'
WHERE
	seq = pSeq AND agent_seq = pAgentSeq;

END$$


