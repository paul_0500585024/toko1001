DELIMITER $$
CREATE PROCEDURE sp_agent_account_add
(
        `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pagentSeq` BIGINT UNSIGNED,
        `pMutationType` VARCHAR(5),
        `pTrxType` VARCHAR(5),
        `pPaymentMethodSeq` TINYINT UNSIGNED,
        `pTrxNo` VARCHAR(25),
        `pDepositTrxAmt` DECIMAL(10,0),
        `pNonDepositTrxAmt` DECIMAL(10,0),
        `pBankName` VARCHAR(50),
        `pBankBranchName` VARCHAR(50),
        `pBankAcctNo` VARCHAR(50),
        `pBankAcctName` VARCHAR(50),
        `pStatus` VARCHAR(5)
    )
BEGIN
Declare new_seq int unsigned;

	Select
		Max(seq) + 1 Into new_seq
	From t_agent_account
    where
	agent_seq = pagentSeq;
	If new_seq Is Null Then
		Set new_seq = 1;
	End If;

	Insert Into t_agent_account (
	  agent_seq,
	  seq,
	  mutation_type,
	  pg_method_seq,
	  trx_type,
	  trx_no,
	  trx_date,
	  deposit_trx_amt,
	  non_deposit_trx_amt,
	  bank_name,
	  bank_branch_name,
	  bank_acct_no,
	  bank_acct_name,
	  refund_date,
	  `status`,
	  created_by,
	  created_date,
	  modified_by,
	  modified_date
  )
	Value (
	pagentSeq,
	new_seq,
	pMutationType,
	pPaymentMethodSeq,
	pTrxType,
	pTrxNo,
	CURRENT_DATE(),
	pDepositTrxAmt,
	pNonDepositTrxAmt,
	pBankName,
	pBankBranchName,
	pBankAcctNo,
	pBankAcctName,
	'0000-00-00 00:00:00',
	pStatus,
	pagentSeq,
	now(),
	pagentSeq,
	now()
);
END $$
DELIMITER ;