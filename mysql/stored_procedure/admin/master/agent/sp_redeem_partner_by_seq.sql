DROP PROCEDURE IF EXISTS sp_redeem_partner_by_seq;
DELIMITER $$
CREATE PROCEDURE sp_redeem_partner_by_seq (
    `pUserID` VARCHAR(25),
    `pIPAddr` VARCHAR(50),
    `pSeq` INTEGER(10) UNSIGNED,
    `pPartnerSeq` INTEGER(10) UNSIGNED
)
BEGIN
SELECT
    a.`redeem_seq`,
    a.`partner_seq`,
    SUM(CASE WHEN a.`mutation_type`= 'D' THEN a.`total` ELSE a.`total` * -1 END) AS total,
    c.`status`,
    c.`paid_date`,
    b.`name`,
    b.email,
    b.mobile_phone,
    b.bank_name,
    b.bank_branch_name,
    b.bank_acct_no,
    b.bank_acct_name,
    b.profile_img
FROM 
    `t_redeem_agent_component` a 
    JOIN `m_partner` b ON a.`partner_seq` = b.`seq`
    JOIN (
        SELECT tra.redeem_seq, tra.status, tra.paid_date, ma.partner_seq 
        FROM 
            t_redeem_agent tra 
        JOIN m_agent ma ON tra.agent_seq = ma.seq AND ma.`partner_seq` = pPartnerSeq 
        WHERE 
            tra.`redeem_seq`=pSeq 
        GROUP BY tra.redeem_seq, tra.status, tra.paid_date, ma.partner_seq
    ) c ON a.`partner_seq`=c.`partner_seq` 
WHERE
    a.`redeem_seq` = pSeq AND a.`partner_seq`=pPartnerSeq
GROUP BY 
    a.`redeem_seq`, a.`partner_seq`, c.`status`, c.`paid_date`, b.`name`, b.email, b.mobile_phone, b.bank_name, b.bank_branch_name, b.bank_acct_no, b.bank_acct_name, b.profile_img;
END$$