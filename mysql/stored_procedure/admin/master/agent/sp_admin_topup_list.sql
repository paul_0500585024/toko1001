DELIMITER $$
CREATE PROCEDURE sp_agent_delete
(
        `pUserID` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pSeq` BIGINT UNSIGNED
    )
BEGIN
Delete From m_agent Where seq = pSeq and status='N';
END $$
DELIMITER ;
