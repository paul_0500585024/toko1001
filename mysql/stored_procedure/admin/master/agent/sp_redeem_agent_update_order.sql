DROP PROCEDURE IF EXISTS sp_redeem_agent_update_order;
DELIMITER $$
CREATE PROCEDURE sp_redeem_agent_update_order (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pSeq INTEGER(10) UNSIGNED,
    pTdate DATE
)
BEGIN
    UPDATE 
        t_order_merchant tom join t_order tor on tom.order_seq=tor.seq 
    SET
        tom.redeem_agent_seq = pSeq,    
        tom.modified_by = pUserID,
        tom.modified_date = now()
    WHERE
        tom.order_status='D' 
        AND tom.redeem_agent_seq IS NULL 
        AND tom.received_date <= pTdate
        AND tor.member_seq IS NULL;
END$$
