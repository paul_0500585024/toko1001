DROP PROCEDURE IF EXISTS `sp_agent_refund_detail_product_list`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_refund_detail_product_list` (
        pUserID VarChar(100),
        pIPAddr VarChar(50),
        pHseq int(20),
        pMerchant_info_seq int(20)
)
BEGIN

SELECT 
mc.seq AS merchant_seq , 
mc.name AS merchant_name,
mv.value  AS variant_value , 
mp.name AS product_name , 
pv.pic_1_img AS product_image, 
t_order_product.sell_price , 
t_order_product.weight_kg , 
t_order_product.qty , 
t_order_product.sell_price * t_order_product.qty AS sell_total ,  
CEIL(t_order_product.ship_price_charged) * CEIL(t_order_product.weight_kg) * t_order_product.qty AS ship_total 

FROM  
t_order_product , 
m_product_variant pv ,
m_variant_value mv ,
m_product mp ,
m_merchant_info mic ,
m_merchant mc 


WHERE 
t_order_product.product_variant_seq = pv.seq AND
t_order_product.variant_value_seq = mv.seq AND
pv.product_seq = mp.seq AND 
t_order_product.merchant_info_seq = mic.seq AND
mic.merchant_seq = mc.seq  AND t_order_product.product_status = 'X'
AND t_order_product.order_seq = pHseq 
AND t_order_product.merchant_info_seq = pMerchant_info_seq
;

END$$


