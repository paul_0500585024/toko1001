DELIMITER $$
CREATE PROCEDURE sp_admin_get_data_topup_agent
(
        `pUserID` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pSeq` BIGINT(10),
        `pagentSeq` BIGINT(10)
    )
BEGIN
Select
	ma.seq,
	ma.agent_seq,
	m.name,
	m.email,
	m.deposit_amt,
	ma.trx_no,
	ma.deposit_trx_amt,
	ma.non_deposit_trx_amt,
	ma.status
From t_agent_account ma
Join m_agent m
	On ma.agent_seq = m.seq
Where
ma.seq = pSeq And ma.agent_seq = pagentSeq;
END $$
DELIMITER ;
