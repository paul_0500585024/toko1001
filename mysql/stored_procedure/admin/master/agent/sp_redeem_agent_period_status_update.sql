DROP PROCEDURE IF EXISTS sp_redeem_agent_period_status_update;
DELIMITER $$
CREATE PROCEDURE sp_redeem_agent_period_status_update (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pSeq INTEGER(10) UNSIGNED,
    pStatusNew CHAR(1),
    pStatusOld CHAR(1)
)
BEGIN
    UPDATE t_redeem_agent_period 
    SET
        status = pStatusNew,
        modified_by = pUserID,
        modified_date = NOW()
    Where
        seq = pSeq AND status = pStatusOld;
END$$
