DELIMITER $$
CREATE PROCEDURE sp_admin_topup_delete
(
        `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pSeq` BIGINT UNSIGNED,
        `pagentSeq` BIGINT UNSIGNED
    )
BEGIN
Delete From 
	t_agent_account
Where 
	seq = pSeq And
	agent_seq = pagentSeq and status='T';
END $$ 
DELIMITER ; 
