DROP PROCEDURE IF EXISTS sp_redeem_agent_update_by_partner;
DELIMITER $$
CREATE PROCEDURE sp_redeem_agent_update_by_partner (
    `pUserID` VARCHAR(25),
    `pIPAddr` VARCHAR(50),
    `pSeq` INTEGER(10) UNSIGNED,
    `pPartnerSeq` INTEGER(10) UNSIGNED,
    `pPaiddate` DATE
)
BEGIN
UPDATE 
    t_redeem_agent 
SET
    paid_partner_date = pPaiddate,
    `status` = 'T',
    modified_by = pUserID,
    modified_date = NOW() 
WHERE agent_seq IN 
  (SELECT 
    seq 
  FROM
    m_agent 
  WHERE partner_seq = pPartnerSeq) 
  AND `redeem_seq` = pSeq 
  AND `status` = 'U' ;
END$$
