DROP PROCEDURE IF EXISTS `sp_admin_withdraw_list_agent`;
DELIMITER $$
CREATE PROCEDURE `sp_admin_withdraw_list_agent` (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pCurrPage INT UNSIGNED,
    pRecPerPage INT UNSIGNED,
    pDirSort VARCHAR(4),
    pColumnSort VARCHAR(50),
    pAgentEmail VARCHAR(100),
    pStatus CHAR(1)
)
BEGIN

-- Begin variables for Paging
    DECLARE startRec INT UNSIGNED;
    DECLARE totalPage INT UNSIGNED;
    DECLARE totalRec INT UNSIGNED;
    -- End variables for Paging

    -- Begin SQL Where
    DECLARE sqlWhere VARCHAR(500);

	SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    SET sqlWhere = "";
      
    IF pAgentEmail <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And m.email = '" , pAgentEmail , "'");
    END IF;
    
    IF pStatus <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And ma.status = '" , pStatus, "'");
    END IF;
	
    -- End SQL Where

    -- Begin Paging Info
    SET @sqlCommand = "Select Count(ma.seq) Into @totalRec From t_agent_account ma 
														   Join m_agent m On ma.agent_seq = m.seq
                                                           ";
    
    SET @sqlCommand = CONCAT(@sqlCommand, " Where ma.trx_type ='" ,'WDW', "'");
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, sqlWhere);
    END IF;

    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;

    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);

        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        END IF;

        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    -- End Paging Info

    SET @sqlCommand = "
        Select
			ma.seq,
			ma.trx_no,
            ma.trx_date,
            m.seq as agent_seq,
            m.email as agent_email,
            m.deposit_amt,
            ma.deposit_trx_amt,
            ma.non_deposit_trx_amt,
            ma.bank_name,
            ma.bank_acct_no,
            ma.bank_acct_name,
            ma.refund_date,
            ma.status
		From t_agent_account ma
		Join m_agent m
			On ma.agent_seq = m.seq
    ";

    SET @sqlCommand = CONCAT(@sqlCommand, " Where ma.trx_type ='" ,'WDW', "'");
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, sqlWhere);
    END IF;
    
    SET @sqlCommand = CONCAT(@sqlCommand, " order by ", "", pColumnSort, " ", pDirSort);
    
    SET @sqlCommand = CONCAT(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    SELECT pCurrPage, totalPage, totalRec AS total_rec;

    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;

    SET @totalRec = NULL;
    SET @sqlCommand = NULL;

END$$


