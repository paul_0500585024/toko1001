DROP PROCEDURE IF EXISTS `sp_agent_add_deposit`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_add_deposit` (
    pUserID VarChar(100),
    pIPAddr VarChar(50),
    pAgent_seq int(10),
    pDepositadd int(30)
)
BEGIN

UPDATE 
m_agent 
SET `deposit_amt` = pDepositadd 
WHERE seq = pAgent_seq;

END$$


