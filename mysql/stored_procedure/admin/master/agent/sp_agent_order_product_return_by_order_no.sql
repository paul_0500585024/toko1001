DELIMITER $$
CREATE PROCEDURE sp_agent_order_product_return_by_order_no
(
        `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pCurrPage` INTEGER UNSIGNED,
        `pRecPerPage` INTEGER UNSIGNED,
        `pDirSort` VARCHAR(4),
        `pColumnSort` VARCHAR(50),
        `pOrderNo` VARCHAR(50),
        `pAgentSeq` INTEGER UNSIGNED,
        `pProductStatus` CHAR(1),
        `pOrderStatus` CHAR(1),
        `pFinishDate` SMALLINT(1) UNSIGNED
    )
BEGIN
    -- Begin variables for Paging
    Declare startRec Int unsigned;
    Declare totalPage Int unsigned;
    Declare totalRec Int unsigned;
    -- End variables for Paging
    -- Begin SQL Where
    Declare sqlWhere VarChar(500);
	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
    -- End SQL Where
    -- Begin Paging Info
    Set @sqlCommand = "Select Count(o.seq) Into @totalRec From t_order o
		JOIN
	t_order_merchant om ON o.seq = om.order_seq
		JOIN
	t_order_product op ON om.order_seq = op.order_seq AND om.merchant_info_seq = op.merchant_info_seq
		JOIN
	m_product_variant pv ON op.product_variant_seq = pv.seq
		JOIN
	m_product p ON pv.product_seq = p.seq
		JOIN
	m_variant_value vv ON pv.variant_value_seq = vv.seq
		JOIN
	m_merchant_info mi ON op.merchant_info_seq = mi.seq
		JOIN
	m_merchant m ON mi.merchant_seq = m.seq";
    	Set @sqlCommand = Concat(@sqlCommand, " Where om.received_date >= date_add( now(),interval -",pFinishDate," day) and o.order_no ='" ,pOrderNo,"' AND o.agent_seq='",pAgentSeq,"'AND op.product_status='",pProductStatus,"' AND om.order_status='",pOrderStatus,"'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);
        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;
        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info
    Set @sqlCommand = "
	SELECT
	o.order_no,
	o.agent_seq,
	o.seq,
	p.seq as pro_seq,
	pv.pic_1_img AS img,
	pv.variant_value_seq,
	op.product_variant_seq,
	p.name AS product_name,
	vv.value AS variant_name,
	op.qty,
	op.sell_price,
	m.name AS merchant_name,
	m.seq as merchant_seq,
	'' AS checked
	FROM
	t_order o
	JOIN
	t_order_merchant om ON o.seq = om.order_seq
		JOIN
	t_order_product op ON om.order_seq = op.order_seq AND om.merchant_info_seq = op.merchant_info_seq
		JOIN
	m_product_variant pv ON op.product_variant_seq = pv.seq
		JOIN
	m_product p ON pv.product_seq = p.seq
		JOIN
	m_variant_value vv ON pv.variant_value_seq = vv.seq
		JOIN
	m_merchant_info mi ON op.merchant_info_seq = mi.seq
		JOIN
	m_merchant m ON mi.merchant_seq = m.seq";
    	Set @sqlCommand = Concat(@sqlCommand, " Where om.received_date >= date_add( now(),interval -",pFinishDate," day) and o.order_no ='" ,pOrderNo,"' AND o.agent_seq='",pAgentSeq,"' AND op.product_status='",pProductStatus,"' AND om.order_status='",pOrderStatus,"'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
SELECT pCurrPage, totalPage, totalRec AS total_rec;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set @totalRec = Null;
    Set @sqlCommand = Null;
END $$ 
DELIMITER ; 
