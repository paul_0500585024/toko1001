DROP PROCEDURE IF EXISTS sp_redeem_partner_list;
DELIMITER $$
CREATE PROCEDURE sp_redeem_partner_list (
    `pUserID` VARCHAR(25),
    `pIPAddr` VARCHAR(50),
    `pCurrPage` INTEGER,
    `pRecPerPage` INTEGER,
    `pDirSort` VARCHAR(4),
    `pColumnSort` VARCHAR(50),
    `pSeq` INTEGER(10)
)
BEGIN
    -- Begin variables for Paging
    DECLARE startRec INT;
    DECLARE totalPage INT;
    DECLARE totalRec INT;
    -- End variables for Paging
    -- Begin SQL Where
    DECLARE sqlWhere VARCHAR(500);
	SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    SET sqlWhere = "";
    
    IF pSeq <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " AND c.redeem_seq = '" , pSeq, "'");
    END IF;
    -- End SQL Where
    -- Begin Paging Info
    SET @sqlCommand = "SELECT COUNT(c.partner_seq) INTO @totalRec FROM t_redeem_agent_component c ";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " WHERE 1 " , sqlWhere );
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        End IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    -- End Paging Info
    SET @sqlCommand = "
        SELECT
            a.`redeem_seq`,
            a.`partner_seq`,
            SUM(CASE WHEN a.`mutation_type`= 'D' THEN a.`total` ELSE a.`total` * -1 END) AS total,
            c.`status`,
            c.paid_partner_date AS paid_date,
            b.`name`,
            b.email,
            b.mobile_phone,
            b.bank_name,
            b.bank_branch_name,
            b.bank_acct_no,
            b.bank_acct_name,
            b.profile_img
        FROM 
            `t_redeem_agent_component` a 
        JOIN `m_partner` b ON a.`partner_seq`=b.`seq`
        JOIN (SELECT tra.redeem_seq, tra.status, tra.paid_partner_date, ma.partner_seq FROM t_redeem_agent tra JOIN m_agent ma ON tra.agent_seq=ma.seq ";
    IF pSeq <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " WHERE tra.redeem_seq=" , pSeq, " GROUP BY tra.redeem_seq, tra.status, tra.paid_partner_date, ma.partner_seq) c ON a.`partner_seq`= c.`partner_seq` ");
    END IF;    
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " WHERE 1 " , sqlWhere, " AND a.redeem_seq=",pSeq);
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand, " GROUP BY a.`redeem_seq`, a.`partner_seq`, c.`status`, c.`paid_partner_date`, b.`name`, b.email, b.mobile_phone, b.bank_name, b.bank_branch_name, b.bank_acct_no, b.bank_acct_name, b.profile_img");
    SET @sqlCommand = CONCAT(@sqlCommand, " ORDER BY ", pColumnSort, " ", pDirSort);
    SET @sqlCommand = CONCAT(@sqlCommand, " LIMIT ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @totalRec = NULL;
    SET @sqlCommand = NULL;
END$$