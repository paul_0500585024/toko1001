DROP PROCEDURE IF EXISTS `sp_agent_refund_get_data_cancel`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_refund_get_data_cancel` (
        pMerchantId VarChar(25),
        pIPAddr VarChar(50),
        pTrxNo varchar(50),
        pTrxType varchar(10)
)
BEGIN

SELECT  
        tor.order_seq ,
        tor.merchant_info_seq
		from
		t_agent_account tma, 
        m_payment_gateway_method pgm , 
        m_agent m  , 
        t_order_merchant tor      
        where
		tma.pg_method_seq = pgm.seq and 
        tma.agent_seq = m.seq and 
        tma.trx_no = tor.ref_awb_no and 
        tma.trx_type = pTrxType and 
        tma.trx_no = pTrxNo ;

END$$


