DROP PROCEDURE IF EXISTS `sp_admin_withdraw_save_approve_agent`;
DELIMITER $$
CREATE PROCEDURE `sp_admin_withdraw_save_approve_agent` (
    pUserID VARCHAR(25), 
    pIPAddr VARCHAR(50),
    pSeq BIGINT(10),
    pAgentSeq BIGINT(10),
    pDepositTrxAmt DECIMAL(10),
    pNonDepositTrxAmt DECIMAL(10),
    pDepositAmt DECIMAL(10)
)
BEGIN

UPDATE t_agent_account
SET
	`status` = 'A',
    refund_date = DATE_FORMAT(NOW(),"%Y-%m-%d")
WHERE
	seq = pSeq  AND agent_seq = pAgentSeq;
    

SELECT(pDepositAmt - pDepositTrxAmt) INTO @DepositAmt;
UPDATE m_agent
SET deposit_amt = @DepositAmt
WHERE
	seq = pAgentSeq ;
END$$


