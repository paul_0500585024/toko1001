DROP PROCEDURE IF EXISTS `sp_agent_refund_get_product_return`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_refund_get_product_return` (
    pMerchantSeq VarChar(25),
    pIPAddr VarChar(50),
    pReturnNo varchar(50)
)
BEGIN

SELECT 
topr.qty as qty, 
mpv.sell_price  , mpv.pic_1_img as product_image ,  
vl.value as variant_value,
mp.name as product_name , mp.p_weight_kg as weight_kg , 
mpv.sell_price*topr.qty as sell_total,
mc.seq as merchant_seq,mc.name as merchant_name , 0 as ship_total
FROM 
t_order_product_return topr
join m_product_variant mpv on  topr.product_variant_seq = mpv.seq
join m_variant_value vl on mpv.variant_value_seq = vl.seq
join m_product mp on mpv.product_seq = mp.seq
join m_merchant mc on mp.merchant_seq = mc.seq 
WHERE topr.return_no = pReturnNo;

END$$


