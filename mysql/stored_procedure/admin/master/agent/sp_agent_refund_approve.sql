DROP PROCEDURE IF EXISTS `sp_agent_refund_approve`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_refund_approve` (
        pUserID VarChar(100),
	pIPAddr VarChar(50),
	pSeq int(10),
        pAgentSeq int(10)
)
BEGIN

UPDATE 	t_agent_account set 
	`status` = 'A' , 
	refund_date = curdate() , 
	modified_date = now() , 
	modified_by = pUserID 
WHERE 
	seq = pSeq 	and agent_seq = pAgentSeq;

END$$


