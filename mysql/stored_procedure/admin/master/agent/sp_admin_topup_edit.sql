DELIMITER $$
CREATE PROCEDURE sp_admin_topup_edit
(
        `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pSeq` BIGINT UNSIGNED,
        `pTrxNo` VARCHAR(25),
        `pDepositTrxAmt` DECIMAL(10,0)
    )
BEGIN
	update t_agent_account set
	deposit_trx_amt = pDepositTrxAmt,
	modified_by = pUserID,
	modified_date = now()
Where
	seq = pSeq and trx_no=pTrxNo and status='T';
END $$ 
DELIMITER ; 
