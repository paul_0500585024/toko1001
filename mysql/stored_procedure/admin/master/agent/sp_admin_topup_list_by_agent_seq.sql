DELIMITER $$
CREATE PROCEDURE sp_admin_topup_list_by_agent_seq
(
        `pUserID` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pSeq` BIGINT UNSIGNED,
        `pagentSeq` BIGINT UNSIGNED
    )
BEGIN
	Select
		ma.seq,
		ma.trx_no,
		ma.trx_date,
		m.seq as agent_seq,
		m.email as agent_email,
		m.deposit_amt,
		ma.deposit_trx_amt,
		ma.non_deposit_trx_amt,
		ma.bank_name,
		ma.bank_acct_no,
		ma.bank_acct_name,
		ma.refund_date,
		ma.status
	From t_agent_account ma
	Join m_agent m
		On ma.agent_seq = m.seq
		where ma.seq=pSeq and ma.agent_seq=pagentSeq;
        
END $$ 
DELIMITER ; 
