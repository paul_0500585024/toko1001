DROP PROCEDURE IF EXISTS sp_redeem_agent_status;
DELIMITER $$
CREATE PROCEDURE sp_redeem_agent_status (
    `pUserID` VARCHAR(25),
    `pIPAddr` VARCHAR(50),
    `pSeq` INTEGER(10) UNSIGNED
)
BEGIN
    SELECT 
        a.`redeem_seq`,
        a.`agent_seq`,
        a.`total`,
        a.`status`,
        a.`paid_date`,
        a.`created_by`,
        a.`created_date`,
        a.`modified_by`,
        a.`modified_date`, 
        b.`name`,b.`email` 
    FROM 
        `t_redeem_agent` a
    LEFT OUTER JOIN m_agent b on a.`agent_seq` = b.`seq`
    WHERE 
        `redeem_seq` = pSeq;
END$$