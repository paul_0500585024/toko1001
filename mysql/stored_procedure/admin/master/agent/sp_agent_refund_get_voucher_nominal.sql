DROP PROCEDURE IF EXISTS `sp_agent_refund_get_voucher_nominal`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_refund_get_voucher_nominal` (
        pUserID VarChar(100),
        pIPAddr VarChar(50),
        pSeq int(20)
)
BEGIN

SELECT `nominal` 
FROM m_promo_voucher mpv ,
t_order tor 
WHERE mpv.seq = tor.voucher_seq 
AND tor.seq = pSeq;


END$$


