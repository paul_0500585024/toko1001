DELIMITER $$
CREATE PROCEDURE sp_agent_by_seq
(
        pUserID VarChar(50),
        pIPAddr VarChar(50),
        pSeq int(10) unsigned
    )
BEGIN
SELECT
  `seq`,
  `email`,
  `password`,
  `name`,
  `partner_seq`,
  `referral`,
  `birthday`,
  `gender`,
  `mobile_phone`,
  `profile_img`,
  `deposit_amt`,
  `last_login`,
  `ip_address`,
  `created_by`,
  `created_date`,
  `modified_by`,
  `modified_date`
FROM `toko1001_dev`.`m_agent`
Where
   seq = pSeq;
END $$
DELIMITER ;