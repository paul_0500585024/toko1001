DELIMITER $$
CREATE PROCEDURE sp_redeem_agent_update
(
        `pUserID` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pSeq` INTEGER(10) UNSIGNED,
        `pFdate` DATE,
        `pTdate` DATE
    )
BEGIN
update t_redeem_agent_period set
	from_date = pFdate,
	to_date = pTdate,
	modified_by = pUserID,
	modified_date = now()
Where
	seq = pSeq and `status`='O';
END $$ 
DELIMITER ; 
