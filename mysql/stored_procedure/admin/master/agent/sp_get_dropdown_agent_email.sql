DELIMITER $$
CREATE PROCEDURE sp_get_dropdown_agent_email()
BEGIN
Select 
	seq,
	email 
From 
	m_agent
Order By
	email;
END $$ 
DELIMITER ; 
