DELIMITER $$
DROP PROCEDURE sp_order_agent_partner_by_redeem $$
CREATE PROCEDURE toko1001_dev.sp_order_agent_partner_by_redeem
(
        `pUserID` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pPaymentCode` CHAR(10),
        `pSeq` INTEGER(10) UNSIGNED,
        `pPartnerSeq` INTEGER(10) UNSIGNED
    )
BEGIN
    select tor.order_no, tor.total_payment, tol.`total_installment`, tor.order_date from `t_order` tor
    join m_agent ma on tor.agent_seq=ma.seq
    left outer join t_order_loan tol on tor.seq=tol.order_seq
    where tor.`pg_method_seq` in
    (select seq from m_payment_gateway_method where pg_seq in (select seq from m_payment_gateway where `name`=pPaymentCode)) and tor.member_seq is null and ma.partner_seq=pPartnerSeq and tor.seq in
    (select tom.order_seq from t_order_merchant tom where tom.order_status='D' and tom.redeem_agent_seq =pSeq)
    group by tor.order_no;

END $$
DELIMITER ;
