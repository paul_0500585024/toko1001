CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_non_active_by_variant_seq_admin`(
		pUserid VarChar(25),
        pIPAddr VarChar(50),
        pProductVarSeq int unsigned
)
BEGIN

			SELECT pv.seq as product_variant_seq , 
            mc.name as merchant_name,
			p.name as product_name , 
			vv.value as variant_name ,
			p.status as status_product, 
			pv.active as product_active, 
			st.stock as stock_product,
			pv.created_date as tgl_buat,
			pv.modified_date as tgl_ubah 
			FROM m_product_variant pv 
			LEFT JOIN m_product p ON pv.product_seq = p.seq 
			LEFT JOIN m_merchant mc ON p.merchant_seq =mc.seq
			LEFT JOIN m_variant_value vv ON pv.variant_value_seq = vv.seq
			LEFT JOIN m_product_stock st ON pv.seq = st.product_variant_seq	
            where pv.seq = pProductVarSeq  ;
END