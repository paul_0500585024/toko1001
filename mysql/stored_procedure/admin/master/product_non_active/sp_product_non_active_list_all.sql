CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_non_active_list_all`(		
		pUserID VarChar(25),        
        pIPAddr VarChar(50),
        pCurrPage Int,
        pRecPerPage Int,
        pDirSort VarChar(4),
        pColumnSort Varchar(50),
        pMerchantInfoSeq Int unsigned,
        pProductName Varchar(50),
        pStatus Varchar(50),	
        pActive char(1)
        )
BEGIN
 -- Begin variables for Paging
    Declare startRec Int;
    Declare totalPage Int;
    Declare totalRec Int;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";

	If  pMerchantInfoSeq <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And mc.`seq` = '" , escape_string(pMerchantInfoSeq), "'");
    End If;

    If pProductName <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And p.`name` like '%" , escape_string(pProductName), "%'");
    End If;
    
    If pStatus <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And p.`status` like '%" , escape_string(pStatus), "%'");
    End If;
    
    If pActive <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And pv.`active` like '%" , escape_string(pActive), "%'");
    End If;

    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "select Count(p.name) Into @totalRec 
	from m_product_variant pv , m_product p , m_merchant mc , m_variant_value vv, m_product_stock st
	where  pv.product_seq = p.seq 
	and p.merchant_seq = mc.seq
	and pv.variant_value_seq = vv.seq
	and pv.seq = st.product_variant_seq ";
    
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand , sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
			SELECT pv.seq as product_variant_seq ,
            mc.name as merchant_name,
			p.name as product_name , 
			vv.value as variant_name ,
			p.status as status_product, 
			pv.active as product_active, 
			st.stock as stock_product,
			pv.created_date as tgl_buat,
			pv.modified_date as tgl_ubah 
			from m_product_variant pv , m_product p , m_merchant mc , m_variant_value vv, m_product_stock st
			where  pv.product_seq = p.seq 
			and p.merchant_seq = mc.seq
			and pv.variant_value_seq = vv.seq
			and pv.seq = st.product_variant_seq		
            
	     ";

    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;

    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort," ", pDirSort);

    Set @sqlCommand = Concat(@sqlCommand, "  Limit ", startRec, ", ", pRecPerPage); 

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END