CREATE DEFINER=`root`@`%` PROCEDURE `sp_payment_gateway_add`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pName Varchar(50),
    pOrder TinyInt unsigned,
    pNotes VarChar(1000),
    pActive Char (1)
)
BEGIN
Declare new_seq tinyint unsigned;

SELECT 
    MAX(seq) + 1
INTO new_seq FROM
    m_payment_gateway;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;

	Insert Into m_payment_gateway(
		seq,
		`name`,
        `order`,
        notes,
		active,
		created_by,
		created_date,
		modified_by,
		modified_date
	) Values (
		new_seq,
		pName,
        pOrder,
        pNotes,
		pActive,
		pUserID,
		Now(),
		pUserID,
		Now()
	);

END