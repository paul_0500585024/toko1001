CREATE DEFINER=`root`@`%` PROCEDURE `sp_payment_gateway_delete`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pSeq TinyInt unsigned
)
BEGIN

Delete From m_payment_gateway Where seq = pSeq;


END