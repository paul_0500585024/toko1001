CREATE DEFINER=`root`@`%` PROCEDURE `sp_payment_gateway_update`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pSeq tinyint unsigned,
    pName Varchar(50),
    pOrder TinyInt unsigned,
    pNotes VarChar(1000),
    pActive Char (1)
    )
BEGIN

update m_payment_gateway set
	`name` = pName,
    `order` = pOrder,
    notes = pNotes,
    active = pActive,
    modified_by = pUserID,
    modified_date = now()
Where
	seq = pSeq;

END