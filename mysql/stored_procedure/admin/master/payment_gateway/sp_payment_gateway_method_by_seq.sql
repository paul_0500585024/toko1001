CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_payment_gateway_method_by_seq`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
	pSeq TinyInt unsigned
)
BEGIN

	Select 
		pg_seq,
		seq,
		`name`,
        `code`,
        logo_img,
        `order`,
        notes,
		active
	From m_payment_gateway_method
	Where
		seq = pSeq;
END