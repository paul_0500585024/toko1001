CREATE DEFINER=`root`@`%` PROCEDURE `sp_payment_gateway_method_delete`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pSeq TinyInt unsigned
)
BEGIN

Delete From m_payment_gateway_method Where seq = pSeq;


END