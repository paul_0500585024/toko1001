CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_payment_gateway_by_seq`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
	pSeq TinyInt unsigned
)
BEGIN

	Select 
		seq,
		`name`,
        `order`,
        notes,
		active
	From m_payment_gateway
	Where
		seq = pSeq;
END