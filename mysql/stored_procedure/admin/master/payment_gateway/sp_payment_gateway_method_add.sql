CREATE DEFINER=`root`@`%` PROCEDURE `sp_payment_gateway_method_add`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pPg_Seq TinyInt unsigned,
    pName Varchar(50),
    pCode Varchar(10),
    pLogo_img Varchar(50),
    pOrder TinyInt unsigned,
    pNotes VarChar(1000),
    pActive Char (1)
)
BEGIN
Declare new_seq tinyint unsigned;

    Select 
        Max(seq) + 1 Into new_seq
    From m_payment_gateway_method;

    If new_seq Is Null Then
        Set new_seq = 1;
    End If;


	Insert Into m_payment_gateway_method(
		pg_seq,
		seq,
		`name`,
        `code`,
        logo_img,
        `order`,
        notes,
		active,
		created_by,
		created_date,
		modified_by,
		modified_date
	) Values (
		pPg_Seq,
		new_seq,
		pName,
        pCode,
        pLogo_img,
        pOrder,
        pNotes,
		pActive,
		pUserID,
		Now(),
		pUserID,
		Now()
	);

END