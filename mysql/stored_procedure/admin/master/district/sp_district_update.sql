CREATE DEFINER=`root`@`%` PROCEDURE `sp_district_update`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pSeq SmallInt Unsigned,
	pCitySeq SmallInt Unsigned,
    pName VarChar(50),
    pActive Char(1)
    )
BEGIN

update m_district set
	city_seq = pCitySeq,
    `name` = pName,
    active = pActive,
    modified_by = pUserID,
    modified_date = now()
Where
	seq = pSeq;

END