CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_district_add`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
	pCitySeq SmallInt unsigned,
    pName VarChar(50),
    pActive Char(1)
)
BEGIN
	Declare new_seq SmallInt;

	Select 
		Max(seq) + 1 Into new_seq
	From m_district;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;

	Insert Into m_district(
		city_seq,
        seq,
		name,
		active,
		created_by,
		created_date,
		modified_by,
		modified_date
	) Values (
		pCitySeq,
		new_seq,
		pName,
		pActive,
		pUserID,
		Now(),
		pUserID,
		Now()
	);

END