CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_district_by_seq`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq SmallInt unsigned
    )
BEGIN

	Select 
		c.province_seq,
		d.city_seq,
        c.name as city_name,
		d.seq,
		d.name,
		d.active,
        p.name as province_name
	From m_district d join m_city c
    on c.seq= d.city_seq
    join m_province p on c.province_seq=p.seq
	Where
		d.seq = pSeq;
END