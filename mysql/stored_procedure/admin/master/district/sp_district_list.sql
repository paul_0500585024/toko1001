CREATE DEFINER=`root`@`%` PROCEDURE `sp_district_list`(
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pCurrPage Int unsigned,
    pRecPerPage Int unsigned,
    pDirSort VarChar(4),
    pColumnSort Varchar(50),
    pName VarChar(50),
    pProvinceSeq TinyInt unsigned,
    pCitySeq SmallInt unsigned,
	pActive Char(1)
)
BEGIN
    -- Begin variables for Paging
    Declare startRec Int unsigned;
    Declare totalPage Int unsigned;
    Declare totalRec Int unsigned;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
    
	If pName <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And d.name like '%" , escape_string(pName), "%'");
    End If;
    If pProvinceSeq <> 0 Then
        Set sqlWhere = Concat(sqlWhere, " And p.seq = '" , pProvinceSeq, "'");
    End If;
    If pCitySeq <> 0 Then
        Set sqlWhere = Concat(sqlWhere, " And c.seq = '" , pCitySeq, "'");
    End If;
    If pActive <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And d.active = '" , pActive, "'");
    End If; 

    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "
		Select Count(d.seq) Into @totalRec 
			From m_district d  
				join m_city c 
					on d.city_seq = c.seq 
				join m_province P 
					on c.province_seq = p.seq";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "	
        Select
            d.seq,
            p.name as province_name,
            c.name as city_name,
            d.name as district_name,
            d.active,
            d.created_by,
            d.created_date,
            d.modified_by,
            d.modified_date
        From m_district d  join m_city c 
        on d.city_seq = c.seq 
        join m_province P 
        on c.province_seq = p.seq
    ";

    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;
    
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END