CREATE DEFINER=`root`@`%` PROCEDURE `sp_district_delete`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pSeq SmallInt unsigned
)
BEGIN

Delete From m_district Where seq = pSeq;


END