CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_setting_by_seq`(
	pUserID VarChar(100), 
	pIPAddr VarChar(50),
	pSeq TinyInt unsigned
)
BEGIN
	Select
		type_seq,
		seq,
		name,
        value,
        notes,
		active
	From s_setting
	Where
		seq = pSeq;
END