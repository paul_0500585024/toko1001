CREATE DEFINER=`root`@`%` PROCEDURE `sp_setting_delete`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pSeq tinyint unsigned
)
BEGIN

DELETE FROM s_setting WHERE seq = pSeq;


END