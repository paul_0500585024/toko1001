CREATE DEFINER=`root`@`%` PROCEDURE `sp_setting_list`(
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pCurrPage Int,
    pRecPerPage Int,
    pDirSort VarChar(4),
    pColumnSort Varchar(50),
    pTypeSeq TinyInt unsigned,
    pName VarChar(50),
    pValue VarChar(500),
    pActive Char(1)
)
BEGIN
    -- Begin variables for Paging
    Declare startRec Int;
    Declare totalPage Int;
    Declare totalRec Int;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
      
    If pName <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And s.name like '%" , escape_string(pName), "%'");
    End If;

    If pTypeSeq <> 0 Then
        Set sqlWhere = Concat(sqlWhere, " And t.seq = '" , pTypeSeq, "'");
    End If;
    
    If pValue <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And s.value like '%" , escape_string(pValue), "%'");
    End If;
    
	If pActive <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And s.active='" , pActive, "'");
    End If; 
    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "Select Count(s.seq) Into @totalRec From s_setting s Join s_setting_type t On t.seq = s.type_seq";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
        Select
            t.name  as type_name,
            s.seq,
            s.name,
            s.value,
            s.notes,
            s.active,
            s.created_by,
            s.created_date,
            s.modified_by,
            s.modified_date
        From s_setting s
        Join s_setting_type t
			On t.seq = s.type_seq
    ";

    If sqlWhere <> "" Then
		Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;
    
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END