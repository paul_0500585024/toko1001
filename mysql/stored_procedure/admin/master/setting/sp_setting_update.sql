CREATE DEFINER=`root`@`%` PROCEDURE `sp_setting_update`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pType_Seq TinyInt(3),
    pSeq SmallInt(5),
	pName VarChar(50),
    pValue VarChar(500),
    pNotes VarChar(1000),
    pActive Char(1)
    )
BEGIN

update s_setting set
	type_seq = pType_Seq,
    name = pName,
    value = pValue,
    notes = pNotes,
    active = pActive,
    modified_by = pUserID,
    modified_date = now()
Where
	seq = pSeq;

END