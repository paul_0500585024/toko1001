CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_setting_add`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pTypeSeq TinyInt unsigned,
    pName VarChar(50),
    pValue VarChar(500),
    pNotes VarChar(1000),
    pActive Char(1)
)
BEGIN
	Declare new_seq TinyInt unsigned;

	Select 
		Max(seq) + 1 Into new_seq
	From s_setting;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;

	Insert Into s_setting(
		type_seq,
        seq,
		name,
        value,
        notes,
		active,
		created_by,
		created_date,
		modified_by,
		modified_date
	) Values (
		pTypeSeq,
		new_seq,
		pName,
		pValue,
        pNotes,
        pActive,
		pUserID,
		Now(),
		pUserID,
		Now()
	);

END