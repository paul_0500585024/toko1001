CREATE DEFINER=`root`@`%` PROCEDURE `sp_setting_type_delete`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pSeq TinyInt unsigned
)
BEGIN

Delete From s_setting_type Where seq = pSeq;


END