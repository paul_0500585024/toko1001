CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_setting_type_by_seq`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
	pSeq TinyInt unsigned
)
BEGIN

	Select 
		seq,
		name,
        notes,
		active
	From s_setting_type
	Where
		seq = pSeq;
END