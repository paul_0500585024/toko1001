CREATE DEFINER=`root`@`%` PROCEDURE `sp_setting_type_update`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pSeq VarChar(50),
    pName VarChar(50),
    pNotes VarChar(1000),
    pActive Char(1)
    )
BEGIN

update s_setting_type set
	name = pName,
	notes = pNotes,
    active = pActive,
    modified_by = pUserID,
    modified_date = now()
Where
	seq = pSeq;

END