CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_setting_type_add`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pName VarChar(50),
    pNotes VarChar(1000),
    pActive Char(1)
)
BEGIN
	Declare new_seq TinyInt unsigned;

	Select 
		Max(seq) + 1 Into new_seq
	From s_setting_type;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;

	Insert Into s_setting_type(
		seq,
		name,
        notes,
		active,
		created_by,
		created_date,
		modified_by,
		modified_date
	) Values (
		new_seq,
		pName,
        pNotes,
		'1',
		pUserID,
		Now(),
		pUserID,
		Now()
	);

END