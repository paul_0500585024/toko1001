CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_variant_product_by_category`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq smallint unsigned
    )
BEGIN

	Select
        a.display_name,
		a.name,
        c.`seq`,
        c.`value`        
		From `m_variant` a
        inner join `m_product_category` b
        on a.`seq`=b.`variant_seq`
        inner join `m_variant_value` c
        on a.`seq`=c.`variant_seq`
	Where
		b.`seq` = pSeq order by c.`order`,c.`value`;
END