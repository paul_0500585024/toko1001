CREATE DEFINER=`root`@`%` PROCEDURE `sp_variant_value_add`(
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pVariantSeq smallint unsigned,
    pValue VarChar(50),
    pOrder tinyint unsigned,
    pActive Varchar(1)
)
BEGIN
    Declare new_seq smallint unsigned;

SELECT 
    MAX(seq) + 1
INTO new_seq FROM
    m_variant_value;

    If new_seq Is Null Then
        Set new_seq = 1;
    End If;

    Insert Into m_variant_value(
        seq,
        variant_seq,
        `value`,
        `order`,
        active,
        created_by,
        created_date,
        modified_by,
        modified_date
    ) Values (
		new_seq,
        pVariantSeq,
        pValue,
        pOrder,
        pActive,
        pUserID,
        Now(),
        pUserID,
        Now()
    );
END