CREATE DEFINER=`root`@`%` PROCEDURE `sp_variant_product_update`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pSeq smallint unsigned,
    pCatagorySeq tinyint unsigned,
    pName VarChar(50),
    pDisplayName VarChar(50),
    pActive Char(1)
    )
BEGIN

update m_variant set
	category_seq = pCatagorySeq,
	`name` = pName,
    display_name = pDisplayName,
    active = pActive,
    modified_by = pUserID,
    modified_date = now()
Where
	seq = pSeq;

END