CREATE DEFINER=`root`@`%` PROCEDURE `sp_variant_value_update`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pSeq smallint unsigned,
    pValue VarChar(50),
    pOrder tinyint unsigned,
    pActive Varchar(1)
    )
BEGIN

update m_variant_value set
	`value` = pValue,
    `order` = pOrder,
    active = pActive,
    modified_by = pUserID,
    modified_date = now()
Where
	seq = pSeq;

END