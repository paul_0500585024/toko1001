CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_variant_product_by_seq`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq smallint unsigned
    )
BEGIN

	Select
		category_seq,
        seq,
		name,
        display_name,
		active
		From m_variant
	Where
		seq = pSeq;
END