CREATE DEFINER=`root`@`%` PROCEDURE `sp_variant_product_add`(
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pCategorySeq tinyint unsigned,
    pName VarChar(50),
    pDisplayName VarChar(50),
    pActive Varchar(1)
)
BEGIN
    Declare new_seq smallint unsigned;

    Select 
        Max(seq) + 1 Into new_seq
    From m_variant;

    If new_seq Is Null Then
        Set new_seq = 1;
    End If;

    Insert Into m_variant(
        seq,
        category_seq,
        `name`,
        display_name,
        active,
        created_by,
        created_date,
        modified_by,
        modified_date
    ) Values (
		new_seq,
        pCategorySeq,
        pName,
        pDisplayName,
        pActive,
        pUserID,
        Now(),
        pUserID,
        Now()
    );
END