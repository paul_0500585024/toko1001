CREATE DEFINER=`root`@`%` PROCEDURE `sp_variant_value_delete`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq smallint unsigned
    )
BEGIN
Delete From m_variant_value Where seq = pSeq;
END