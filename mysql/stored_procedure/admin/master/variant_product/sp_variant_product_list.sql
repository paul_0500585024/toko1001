CREATE DEFINER=`root`@`%` PROCEDURE `sp_variant_product_list`(
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pCurrPage Int,
    pRecPerPage Int,
    pDirSort VarChar(4),
    pColumnSort Varchar(50),
    pCategorySeq tinyint unsigned,
    pName VarChar(50),
    pDisplayName VarChar(50),
    pActive Char(1)

)
BEGIN
    -- Begin variables for Paging
    Declare startRec Int;
    Declare totalPage Int;
    Declare totalRec Int;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
      
   If pName <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And v.name like '%" ,escape_string( pName), "%'");
    End If;

	If pDisplayName <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And v.display_name like '%" , escape_string(pDisplayName), "%'");
    End If;
    
    If pCategorySeq <> 0 Then
        Set sqlWhere = Concat(sqlWhere, " And c.seq = '" , pCategorySeq , "'");
    End If;
    
	If pActive <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And v.active='" , pActive, "'");
    End If; 
    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "Select Count(v.seq) Into @totalRec 
    		From m_variant v
        Join m_product_category c on
            v.category_seq = c.seq";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
        Select
			v.seq,
            c.name as category_name,
            v.name,
            v.display_name,
            v.active,
            v.created_by,
            v.created_date,
            v.modified_by,
            v.modified_date
		From m_variant v
        Join m_product_category c on
            v.category_seq = c.seq

    ";

    If sqlWhere <> "" Then
		Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;
    
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END