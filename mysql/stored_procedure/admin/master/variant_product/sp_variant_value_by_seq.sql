CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_variant_value_by_seq`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq smallint unsigned
    )
BEGIN

	Select
		variant_seq,
        seq,
		`value`,
        `order`,
		active
		From m_variant_value
	Where
		seq = pSeq;
END