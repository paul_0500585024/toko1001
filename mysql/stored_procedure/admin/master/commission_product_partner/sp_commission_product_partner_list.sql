DROP PROCEDURE IF EXISTS `sp_commission_product_partner_list`;
DELIMITER $$
CREATE PROCEDURE `sp_commission_product_partner_list` (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pCurrPage INT UNSIGNED,
    pRecPerPage INT UNSIGNED,
    pDirSort VARCHAR(4),
    pColumnSort VARCHAR(50),
    pFromDate DATE,
    pToDate DATE
)
BEGIN

 -- Begin variables for Paging
    DECLARE startRec INT UNSIGNED;
    DECLARE totalPage INT UNSIGNED;
    DECLARE totalRec INT UNSIGNED;
    -- End variables for Paging
    -- Begin SQL Where
    DECLARE sqlWhere VARCHAR(500);
	SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    SET sqlWhere = "";
    
    IF pFromDate <> "0000-00-00" THEN
        SET sqlWhere = Concat(sqlWhere, " AND '" , pFromDate , "' BETWEEN period_from AND period_to ");
    END IF;

    IF pToDate <> "0000-00-00" THEN
        SET sqlWhere = Concat(sqlWhere , " OR '" , pToDate , "' BETWEEN period_from AND period_to ");
    END IF;

    SET @sqlCommand = "Select Count(*) Into @totalRec FROM m_partner_product";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        END IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    -- End Paging Info
    SET @sqlCommand = "
        SELECT
            *
        FROM
            m_partner_product
           
          ";
    
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    SET @sqlCommand = CONCAT(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @totalRec = NULL;
    SET @sqlCommand = NULL;

END$$

