DROP PROCEDURE IF EXISTS `sp_save_add_partner_product_commission`;
DELIMITER $$
CREATE PROCEDURE `sp_save_add_partner_product_commission` (
    pUserID varchar(25),
    pIpAddress varchar(25),
    pPartnerProductSeq INT,
    pProductSeq INT,
    pPartnerGrouopSeq INT,
    pNominalCommissionPartner decimal(12,0),
    pNominalCommissionAgent decimal(12,0)
)
BEGIN
    INSERT INTO m_partner_product_commission(
        partner_product_seq,
        product_seq,
        partner_group_seq,
        nominal_commission_partner,
        nominal_commission_agent
    )VALUES(
        pPartnerProductSeq,
        pProductSeq,
        pPartnerGrouopSeq,
        pNominalCommissionPartner,
        pNominalCommissionAgent
    );
END$$

