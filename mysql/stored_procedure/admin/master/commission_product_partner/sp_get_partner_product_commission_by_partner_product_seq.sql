DROP PROCEDURE IF EXISTS `sp_get_partner_product_commission_by_partner_product_seq`;
DELIMITER $$
CREATE PROCEDURE `sp_get_partner_product_commission_by_partner_product_seq` (
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pSeq BIGINT(20) UNSIGNED
)
BEGIN

    SELECT 
        seq,
        period_from,
        period_to,
        notes
    FROM 
    m_partner_product 
        where 
    seq = pSeq;

    SELECT	
        p.name,
        ppc.partner_product_seq,
        CONCAT(ppc.product_seq, '$#' ,p.`name`) as product_seq,
        mpg.partner_group_name,
        ppc.partner_group_seq,
        ppc.nominal_commission_partner,
        ppc.nominal_commission_agent,
        ppc.commission_fee_partner_percent,
        ppc.commission_fee_agent_percent
    FROM 
        m_partner_product_commission ppc 
    JOIN m_product p on ppc.product_seq = p.seq
    JOIN m_partner_group mpg on ppc.partner_group_seq = mpg.partner_group_seq 
    where partner_product_seq = pSeq;
END$$

