DROP PROCEDURE IF EXISTS `sp_commission_product_partner_add`;
DELIMITER $$
CREATE PROCEDURE `sp_commission_product_partner_add` (
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pPeriodFrom datetime,   
    pPeriodTo datetime,
    pNotes varchar(500)
)
BEGIN

 Declare new_seq Int unsigned;
    
    Select 
	Max(seq) + 1 Into new_seq
    From m_partner_product;
    If new_seq Is Null Then
	Set new_seq = 1;
    End If;


INSERT INTO m_partner_product
    (`seq`,
    `period_from`,
    `period_to`,
    `notes`,
    `created_by`,
    `created_date`,
    `modified_by`,
    `modified_date`
)VALUES(
    new_seq,
    pPeriodFrom,
    pPeriodTo,
    pNotes,
    puserID,
    now(),
    puserID,
    now()
);
END$$

