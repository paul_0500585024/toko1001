DROP PROCEDURE IF EXISTS `sp_get_group_partner_commission_list`;
DELIMITER $$
CREATE PROCEDURE `sp_get_group_partner_commission_list` ()
BEGIN
    SELECT 
        concat(partner_group_seq,"'$#'",partner_group_name) as partner_group_seq,
        partner_group_name 
    FROM 
        m_partner_group;
END$$

