DROP PROCEDURE IF EXISTS `sp_get_partner_product_commission_dropdown_product_by_name`;
DELIMITER $$
CREATE PROCEDURE `sp_get_partner_product_commission_dropdown_product_by_name` (
   pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pProductName varchar(150)
)
BEGIN
    SET @sqlCommand = CONCAT("SELECT CONCAT(seq,'$#',name) as id, name as text FROM m_product where name like '%",pProductName,"%' ORDER BY created_date DESC LIMIT 5");
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @totalRec = NULL;
    SET @sqlCommand = NULL;    
END$$

