DROP PROCEDURE IF EXISTS `sp_commission_product_partner_update`;
DELIMITER $$
CREATE PROCEDURE `sp_commission_product_partner_update` (
pUserID VarChar(25),
    pIPAddr VarChar(50),
    pSeq INT,
    pPeriodFrom datetime,   
    pPeriodTo datetime,
    pNotes text
)
BEGIN
    UPDATE m_partner_product
SET
`period_from` = pPeriodFrom,
`period_to` = pPeriodTo,
`notes` = pNotes,
`modified_by` = pUserID,
`modified_date` = now()
WHERE `seq` = pSeq;
END$$

