DROP PROCEDURE IF EXISTS `sp_delete_all_partner_product_commission_by_partner_product_seq`;
DELIMITER $$
CREATE PROCEDURE `sp_delete_all_partner_product_commission_by_partner_product_seq` (
    pUserID varchar(25),
    pIpAddress varchar(25),
    pPartnerProductSeq INT
)
BEGIN
    DELETE FROM m_partner_product_commission WHERE partner_product_seq = pPartnerProductSeq;
END$$

