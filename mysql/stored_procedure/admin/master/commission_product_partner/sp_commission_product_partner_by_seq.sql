DROP PROCEDURE IF EXISTS `sp_commission_product_partner_by_seq`;
DELIMITER $$
CREATE PROCEDURE `sp_commission_product_partner_by_seq` (
       pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pSeq BIGINT(20) UNSIGNED
)
BEGIN
SELECT 
    seq,
    period_from,
    period_to,
    notes,
    created_by,
    created_date,
    modified_by,
    modified_date
FROM 
    m_partner_product
WHERE 
    seq = pSeq;
END$$

