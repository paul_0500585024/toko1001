DROP PROCEDURE IF EXISTS `sp_commission_product_partner_delete`;
DELIMITER $$
CREATE PROCEDURE `sp_commission_product_partner_delete` (
 pUserID VarChar(25),
    pIPAddr VarChar(50),
    pGroupSeq INT
)
BEGIN
     DELETE FROM m_partner_product
    WHERE seq = pGroupSeq;
END$$

