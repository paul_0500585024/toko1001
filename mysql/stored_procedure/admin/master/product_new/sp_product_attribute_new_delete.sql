CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_attribute_new_delete`(
        IN pUserID VARCHAR(25),
        IN pIPAddr VARCHAR(50),
        IN pSeq BIGINT
    )
BEGIN

Delete From `m_product_attribute_new` Where product_seq = pSeq;


END