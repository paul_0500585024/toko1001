CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_product_spec_new_by_product_seq`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq BIGINT(20) unsigned
    )
BEGIN

	Select 
		product_seq,seq,`name`,`value`
	From m_product_spec_new
	Where
		product_seq = pSeq order by seq;
END