CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_product_variant_new_update`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pProductSeq BIGINT(10) UNSIGNED,
        pVarValSeq INTEGER(10) UNSIGNED,
        pProductPrice DECIMAL(10,0),
        pDiscPercent TINYINT(3) UNSIGNED,
        pSalePrice DECIMAL(10,0),
        pOrder TINYINT(3) UNSIGNED,
        pMaxBuy SMALLINT(5) UNSIGNED,
        pPic1 VARCHAR(200),
        pPic2 VARCHAR(200),
        pPic3 VARCHAR(200),
        pPic4 VARCHAR(200),
        pPic5 VARCHAR(200),
        pPic6 VARCHAR(200),
        pSeq BIGINT(10) UNSIGNED
    )
BEGIN

update m_product_variant_new set
	`variant_value_seq`=pVarValSeq,
	`product_price`=pProductPrice,
	`disc_percent`=pDiscPercent,
	`sell_price`=pSalePrice,
	`order`=pOrder,
	`max_buy`=pMaxBuy,
	`pic_1_img`=pPic1,
	`pic_2_img`=pPic2,
	`pic_3_img`=pPic3,
	`pic_4_img`=pPic4,
	`pic_5_img`=pPic5,
	`pic_6_img`=pPic6,
    modified_by = pUserID,
    modified_date = now()
Where
	seq = pSeq and product_seq=pProductSeq;

END