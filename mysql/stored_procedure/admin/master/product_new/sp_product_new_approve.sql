CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_new_approve`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pSeq BIGINT(20) UNSIGNED
    )
BEGIN       

Declare new_seq BIGINT(20) UNSIGNED ;

	Select 
		Max(seq) + 1 Into new_seq
	From m_product;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;   

	insert into m_product
	select 
		`merchant_seq`,
		new_seq,
		`name`,
		`include_ins`,
		`category_l2_seq`,
		`category_ln_seq`,
		`notes`,
		`description`,
		`content`,
		`specification`,
		`warranty_notes`,
		`p_weight_kg`,
		`p_length_cm`,
		`p_width_cm`,
		`p_height_cm`,
		`b_weight_kg`,
		`b_length_cm`,
		`b_width_cm`,
		`b_height_cm`,
		'L',
		pUserID,
		now(),
		`created_by`,
		now(),
		`modified_by`,
		now()
	  from `m_product_new`
	  where seq=pSeq;
  select new_seq;
END