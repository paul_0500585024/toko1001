CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_product_new_add`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pMerchantSeq int(10),
        pName VarChar(150),
        pIncludeIns Char(1),
        pCategoryl2Seq MEDIUMINT(8) UNSIGNED,
        pCategorylnSeq MEDIUMINT(8) UNSIGNED,
        pNotes LONGTEXT,
        pDescription TEXT,
        pContent TEXT,
        pWarrantyNotes VarChar(100),
        pPweightkg DECIMAL(7,2),
        pPlengthcm DECIMAL(7,2),
        pPwidthcm DECIMAL(7,2),
        pPheightcm DECIMAL(7,2),
        pBweightkg DECIMAL(7,2),
        pBlengthcm DECIMAL(7,2),
        pBwidthcm DECIMAL(7,2),
        pBheightcm DECIMAL(7,2),
        pSpec TEXT
    )
BEGIN
	Declare new_seq INT;

	Select 
		Max(seq) + 1 Into new_seq
	From m_product_new;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
if  (pNotes ="") then   
	Insert Into m_product_new(     
		merchant_seq,
		seq,
		`name`,
		include_ins,
		category_l2_seq,
		category_ln_seq,
		notes,
		description,
		content,
		specification,
		warranty_notes,
		p_weight_kg,
		p_length_cm,
		p_width_cm,
		p_height_cm,
		b_weight_kg,
		b_length_cm,
		b_width_cm,
		b_height_cm,
		`status`,
		created_by,
		created_date,
		modified_by,
		modified_date
	) Values (
		pMerchantSeq,
		new_seq,
		pName,
		pIncludeIns,
		pCategoryl2Seq,
		pCategorylnSeq,
		'',
		pDescription,
		pContent,
		pSpec,
		pWarrantyNotes,
		pPweightkg,
		pPlengthcm,
		pPwidthcm,
		pPheightcm,
		pBweightkg,
		pBlengthcm,
		pBwidthcm,
		pBheightcm,
		'N',
		pUserID,
		Now(),
		pUserID,
		Now()
	);
else
	Insert Into m_product_new(     
		merchant_seq,
		seq,
		`name`,
		include_ins,
		category_l2_seq,
		category_ln_seq,
		notes,
		description,
		content,
		specification,
		warranty_notes,
		p_weight_kg,
		p_length_cm,
		p_width_cm,
		p_height_cm,
		b_weight_kg,
		b_length_cm,
		b_width_cm,
		b_height_cm,
		`status`,
		created_by,
		created_date,
		modified_by,
		modified_date
	) Values (
		pMerchantSeq,
		new_seq,
		pName,
		pIncludeIns,
		pCategoryl2Seq,
		pCategorylnSeq,
		pNotes,
		pDescription,
		pContent,
		pSpec,
		pWarrantyNotes,
		pPweightkg,
		pPlengthcm,
		pPwidthcm,
		pPheightcm,
		pBweightkg,
		pBlengthcm,
		pBwidthcm,
		pBheightcm,
		'N',
		pUserID,
		Now(),
		pUserID,
		Now()
	);
end if;
select new_seq;
END