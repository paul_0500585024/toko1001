CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_new_update`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pSeq BIGINT(20) UNSIGNED,
        pMerchant INTEGER(11) UNSIGNED,
        pName VARCHAR(150),
        pIncludeIns CHAR(1),
        pNotes LONGTEXT,
        pDescription TEXT,
        pContent TEXT,
        pWarrantyNotes VARCHAR(100),
        pPweightkg DECIMAL(7,2),
        pPlengthcm DECIMAL(7,2),
        pPwidthcm DECIMAL(7,2),
        pPheightcm DECIMAL(7,2),
        pBweightkg DECIMAL(7,2),
        pBlengthcm DECIMAL(7,2),
        pBwidthcm DECIMAL(7,2),
        pBheightcm DECIMAL(7,2),
        pSpec TEXT
    )
BEGIN

IF(pNotes="") then
update m_product_new set    
	`name`=pName,
	include_ins=pIncludeIns,
	description=pDescription,
	content=pContent,
	specification=pSpec,
	warranty_notes=pWarrantyNotes,
	p_weight_kg=pPweightkg,
	p_length_cm=pPlengthcm,
	p_width_cm=pPwidthcm,
	p_height_cm=pPheightcm,
	b_weight_kg=pBweightkg,
	b_length_cm=pBlengthcm,
	b_width_cm=pBwidthcm,
	b_height_cm=pBheightcm,
	modified_by = pUserID,
	modified_date = now()
Where
	seq = pSeq and merchant_seq=pMerchant;
else
update m_product_new set    
	`name`=pName,
	include_ins=pIncludeIns,
	notes=pNotes,
	description=pDescription,
	content=pContent,
	specification=pSpec,
	warranty_notes=pWarrantyNotes,
	p_weight_kg=pPweightkg,
	p_length_cm=pPlengthcm,
	p_width_cm=pPwidthcm,
	p_height_cm=pPheightcm,
	b_weight_kg=pBweightkg,
	b_length_cm=pBlengthcm,
	b_width_cm=pBwidthcm,
	b_height_cm=pBheightcm,
	modified_by = pUserID,
	modified_date = now()
Where
	seq = pSeq and merchant_seq=pMerchant;
end if;
END