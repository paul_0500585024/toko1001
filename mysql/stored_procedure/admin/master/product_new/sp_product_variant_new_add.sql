CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_product_variant_new_add`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pProductSeq BIGINT(10) UNSIGNED,
        pVarValSeq INTEGER(10) UNSIGNED,
        pProductPrice DECIMAL(10,0),
        pDiscPercent TINYINT(3) UNSIGNED,
        pSalePrice DECIMAL(10,0),
        pOrder TINYINT(3) UNSIGNED,
        pMaxBuy SMALLINT(5) UNSIGNED,
        pPic1 VARCHAR(200),
        pPic2 VARCHAR(200),
        pPic3 VARCHAR(200),
        pPic4 VARCHAR(200),
        pPic5 VARCHAR(200),
        pPic6 VARCHAR(200)
    )
BEGIN
Declare new_seq BIGINT;

	Select 
	Max(seq) + 1 Into new_seq
	From `m_product_variant_new`;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;

	Insert Into `m_product_variant_new`(     
		`product_seq`,
		`variant_value_seq`,
		`seq`,
		`product_price`,
		`disc_percent`,
		`sell_price`,
		`order`,
		`max_buy`,
		`pic_1_img`,
		`pic_2_img`,
		`pic_3_img`,
		`pic_4_img`,
		`pic_5_img`,
		`pic_6_img`,
		`created_by`,
		`created_date`,
		`modified_by`,
		`modified_date`
	) Values (
		pProductSeq,
		pVarValSeq,
		new_seq,
		pProductPrice,
		pDiscPercent,
		pSalePrice,
		pOrder,
		pMaxBuy,
		pPic1,
		pPic2,
		pPic3,
		pPic4,
		pPic5,
		pPic6,
		pUserID,
		Now(),
		pUserID,
		Now()
	);

END