CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_new_reject`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq BIGINT(20)
    )
BEGIN

update m_product_new set
    `status` = 'R'
Where
	seq = pSeq and `status`='N';

END