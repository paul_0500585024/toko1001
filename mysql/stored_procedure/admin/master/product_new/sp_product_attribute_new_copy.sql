CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_attribute_new_copy`(
        IN pUserID VARCHAR(25),
        IN pIPAddr VARCHAR(50),
        IN prdSeq BIGINT(20),
        IN pNewSeq BIGINT(20)
    )
BEGIN       
	insert into `m_product_attribute`
   SELECT 
   pNewSeq,
  `attribute_value_seq`,
  `created_by`,
  `created_date`,
  `modified_by`,
  `modified_date`
FROM 
  `m_product_attribute_new`

  where product_seq=prdSeq;
END