CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_variant_new_delete`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pSeq BIGINT (2)  UNSIGNED
    )
BEGIN

Delete From `m_product_variant_new` Where product_seq = pSeq;


END