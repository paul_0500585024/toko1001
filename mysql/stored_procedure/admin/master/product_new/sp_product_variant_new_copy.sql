CREATE DEFINER=`sqladm`@`%` PROCEDURE `sp_product_variant_new_copy`(

        pUserID VARCHAR(25),

        pIPAddr VARCHAR(50),

        prdSeq BIGINT(20) UNSIGNED,

        pNewSeq BIGINT(20) UNSIGNED

    )
BEGIN     



Declare new_seq BIGINT;



	Select 

	Max(seq) Into new_seq

	From `m_product_variant`;



	If new_seq Is Null Then

		Set new_seq = 0;

	End If;

  Set @num = new_seq;

  insert into `m_product_variant`

  SELECT 
  pNewSeq,
  `variant_value_seq`,
  @num := @num + 1,
  `product_price`,
  `disc_percent`,
  `sell_price`,
  `order`,
  `max_buy`,
   'L',
  `pic_1_img`,
  `pic_2_img`,
  `pic_3_img`,
  `pic_4_img`,
  `pic_5_img`,
  `pic_6_img`,
  '1',
    0,
    0,
    0,
    0,
    0,
  `created_by`,
  now(),
  `modified_by`,
  now()

FROM 
  `m_product_variant_new`
  where product_seq=prdSeq order by seq;

END