CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_product_new_by_seq`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pSeq BIGINT(2)  UNSIGNED
    )
BEGIN

	Select    
  `merchant_seq`,
  `seq`,
  `name`,
  `include_ins`,
  `category_l2_seq`,
  `category_ln_seq`,
  `notes`,
  `description`,
  `content`,
  `specification`,
  `warranty_notes`,
  `p_weight_kg`,
  `p_length_cm`,
  `p_width_cm`,
  `p_height_cm`,
  `b_weight_kg`,
  `b_length_cm`,
  `b_width_cm`,
  `b_height_cm`,
  `status`,
  `created_by`,
  `created_date`,
  `modified_by`,
  `modified_date`
		From `m_product_new`
	Where
		seq = pSeq;
END