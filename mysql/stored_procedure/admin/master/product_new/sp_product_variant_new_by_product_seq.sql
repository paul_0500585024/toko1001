CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_product_variant_new_by_product_seq`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq BIGINT(20) unsigned
    )
BEGIN
	Select 
	a.product_seq,
	a.variant_value_seq,
	a.seq,
	a.product_price,
	a.disc_percent,
	a.sell_price,
	a.`order`,
	a.max_buy,
	a.pic_1_img,
	a.pic_2_img,
	a.pic_3_img,
	a.pic_4_img,
	a.pic_5_img,
	a.pic_6_img,
    b.value
	From m_product_variant_new a
    left outer join `m_variant_value` b
    on a.`variant_value_seq`=b.`seq`
	Where
		product_seq = pSeq order by b.value asc;
END