CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_product_attribute_new_add`(
        IN pUserID VARCHAR(25),
        IN pIPAddr VARCHAR(50),
        IN pProductSeq BIGINT(10),
        IN pAttributeValueSeq INTEGER(10)
    )
BEGIN
	Insert Into m_product_attribute_new(     
		product_seq,
		attribute_value_seq,
		created_by,
		created_date,
		modified_by,
		modified_date
	) Values (
		pProductSeq,
		pAttributeValueSeq,
		pUserID,
		Now(),
		pUserID,
		Now()
	);

END