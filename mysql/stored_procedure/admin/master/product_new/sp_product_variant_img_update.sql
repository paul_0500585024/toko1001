CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_product_variant_img_update`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pSeq BIGINT(20),
        pPic1 VARCHAR(200),
        pPic2 VARCHAR(200),
        pPic3 VARCHAR(200),
        pPic4 VARCHAR(200),
        pPic5 VARCHAR(200),
        pPic6 VARCHAR(200)
    )
BEGIN

update m_product_variant set
	`pic_1_img`=pPic1,
	`pic_2_img`=pPic2,
	`pic_3_img`=pPic3,
	`pic_4_img`=pPic4,
	`pic_5_img`=pPic5,
	`pic_6_img`=pPic6,
    modified_by = pUserID,
    modified_date = now()
Where
	seq = pSeq;

END