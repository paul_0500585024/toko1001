CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_spec_new_copy`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        prdSeq BIGINT(20) UNSIGNED,
        pNewSeq BIGINT(20) UNSIGNED
    )
BEGIN       
insert into m_product_spec
   SELECT 
   pNewSeq,
  `seq`,
  `name`,
  `value`,
  `created_by`,
  `created_date`,
  `modified_by`,
  `modified_date`
FROM 
  `m_product_spec_new`
  where product_seq=prdSeq;
END