CREATE DEFINER=`root`@`%` PROCEDURE `sp_promo_free_fee_city_delete`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq SMALLINT(5) UNSIGNED
    )
BEGIN

Delete From m_promo_free_fee_city Where promo_seq = pSeq;


END