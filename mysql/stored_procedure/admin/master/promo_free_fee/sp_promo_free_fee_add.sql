CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_promo_free_fee_add`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pDateFrom VarChar(10),
        pDateTo VarChar(10),
        pAllOriginCity Char(1),
        pAllDestinationCity Char(1),
        pNotes VarChar(1000),
        pActive Char(1)
    )
BEGIN
	Declare new_seq TinyInt;

	Select 
		Max(seq) + 1 Into new_seq
	From m_promo_free_fee_period;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;

	Insert Into m_promo_free_fee_period(     
		seq,
		date_from,
		date_to,
		all_origin_city,
		all_destination_city,
		notes,
		active,
        status,
        auth_by,
		created_by,
		created_date,
		modified_by,
		modified_date
	) Values (
		new_seq,
        pDateFrom,
        pDateTo,
        pAllOriginCity,
        pAllDestinationCity,
        pNotes,
		pActive,
        'N',
        '',
		pUserID,
		Now(),
		pUserID,
		Now()
	);

END