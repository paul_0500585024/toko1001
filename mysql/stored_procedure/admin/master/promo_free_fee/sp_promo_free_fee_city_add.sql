CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_promo_free_fee_city_add`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pPromoSeq SMALLINT(5) UNSIGNED,
        pType Char(1),
        pCitySeq SMALLINT(5) UNSIGNED
    )
BEGIN
	
	Insert Into m_promo_free_fee_city(     
		promo_seq,
  `type`,
  city_seq,
  created_by,
  created_date,
  modified_by,
  modified_date
	) Values (
		pPromoSeq,
		pType,
        pCitySeq,
		pUserID,
		Now(),
		pUserID,
		Now()
	);

END