CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_promo_free_fee_by_seq`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq SMALLINT(5) UNSIGNED
    )
BEGIN
	Select
		  seq,
		  date_from,
		  date_to,
		  all_origin_city,
		  all_destination_city,
		  notes,
		  `status`,
		  auth_by,
		  auth_date,
		  active,
		  created_by,
		  created_date,
		  modified_by,
		  modified_date
		From m_promo_free_fee_period
	Where
		seq = pSeq;
END