CREATE DEFINER=`root`@`%` PROCEDURE `sp_promo_free_fee_status`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq smallint(5) UNSIGNED,
        pStatus Char(1)
    )
BEGIN

update m_promo_free_fee_period set
    `status` = pStatus,
    auth_by = pUserID,
    auth_date = now()
Where
	seq = pSeq and `status`='N';

END