CREATE DEFINER=`root`@`%` PROCEDURE `sp_promo_free_fee_update`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq smallint(5) UNSIGNED,
        pDateFrom Date,
        pDateTo Date,
        pAllOriginCity Char(1),
        pAllDestinationCity Char(1),
        pNotes VarChar(1000),
        pActive Char(1)
    )
BEGIN

update m_promo_free_fee_period set
    date_from = pDateFrom,
    date_to = pDateTo,
    all_origin_city = pAllOriginCity,
    all_destination_city = pAllDestinationCity,
    notes = pNotes,
    active = pActive,
    modified_by = pUserID,
    modified_date = now()
Where
	seq = pSeq and `status`='N';

END