CREATE DEFINER=`root`@`%` PROCEDURE `sp_promo_free_fee_delete`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq SMALLINT(5) UNSIGNED
    )
BEGIN

Delete From m_promo_free_fee_merchant Where promo_seq = pSeq;
Delete From m_promo_free_fee_city Where promo_seq = pSeq;
Delete From m_promo_free_fee_period Where seq = pSeq and `status`='N';

END