CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_promo_free_fee_merchant_add`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pPromoSeq SMALLINT(5) UNSIGNED,
        pMerchantSeq INT(10) UNSIGNED
    )
BEGIN
	
	Insert Into `m_promo_free_fee_merchant`(     
	`promo_seq`,
	`merchant_seq`,
	created_by,
	created_date,
	modified_by,
	modified_date
	) Values (
		pPromoSeq,
        pMerchantSeq,
		pUserID,
		Now(),
		pUserID,
		Now()
	);

END