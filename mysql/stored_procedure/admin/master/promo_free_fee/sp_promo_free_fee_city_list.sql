CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_promo_free_fee_city_list`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq SMALLINT(5) UNSIGNED
    )
BEGIN

	Select
	a.promo_seq,
  a.`type`,
  a.city_seq, b.city_name
		From m_promo_free_fee_city a left outer join (select seq,`name` as city_name from m_city) b 
        on a.city_seq=b.seq
	Where
		a.promo_seq = pSeq order by a.`type`, b.city_name;
END