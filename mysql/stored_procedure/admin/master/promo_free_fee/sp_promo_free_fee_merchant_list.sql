CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_promo_free_fee_merchant_list`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq SMALLINT(5) UNSIGNED
    )
BEGIN

	Select
	b.promo_seq,
  b.`merchant_seq`,
  a.seq,
  a.`name` AS merchant_name
		From `m_merchant` a left outer join (
        select 
        promo_seq, merchant_seq, created_by, created_date, modified_by, modified_date
        from m_promo_free_fee_merchant Where
		promo_seq = pSeq 
        ) b 
        on a.seq=b.merchant_seq
	 order by a.`name`;
END