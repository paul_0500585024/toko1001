DROP PROCEDURE IF EXISTS `sp_redeem_period_add`;
DELIMITER $$
CREATE PROCEDURE `sp_redeem_period_add` (
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pFdate DATE,
        pTdate DATE,        
        pExpeditionCommission Decimal(5,2)
)
BEGIN

Declare new_seq INT(10) unsigned;

Select 
        Max(seq) + 1 Into new_seq
From t_redeem_period;

If new_seq Is Null Then
        Set new_seq = 1;
End If;

Insert Into t_redeem_period(		
        seq,
        from_date,
        to_date,
        `status`,
        expedition_commission,
        created_by,
        created_date,
        modified_by,
        modified_date
) Values (
        new_seq,
        pFdate,
        pTdate,
        'O',
        pExpeditionCommission,
        pUserID,
        Now(),
        pUserID,
        Now()
);

END$$

