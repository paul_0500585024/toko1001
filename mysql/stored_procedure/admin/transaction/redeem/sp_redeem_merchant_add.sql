CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_redeem_merchant_add`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq int(10) UNSIGNED,
        pMerchantSeq int(10) UNSIGNED,
        pMerchantInfoSeq int(10) UNSIGNED,
        pTotal DECIMAL(10,0),
        pStatus CHAR(1),
        pPaidDate DATE
    )
BEGIN

	Insert Into t_redeem_merchant(		
        redeem_seq,
		merchant_seq,
        merchant_info_seq,
        `total`,
        `status`,
        paid_date,
		created_by,
		created_date,
		modified_by,
		modified_date
	) Values (
		pSeq,
		pMerchantSeq,
		pMerchantInfoSeq,
        pTotal,
        pStatus,
        pPaidDate,
		pUserID,
		Now(),
		pUserID,
		Now()
	);

END