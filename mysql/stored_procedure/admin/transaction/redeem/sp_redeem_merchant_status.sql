CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_redeem_merchant_status`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq int(10) unsigned
    )
BEGIN

	SELECT a.*, b.`name`,b.`email` FROM 
  `t_redeem_merchant` a
  left outer join m_merchant b on a.`merchant_seq`=b.`seq`
	Where
		`redeem_seq` = pSeq;
END