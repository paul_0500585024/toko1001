DROP PROCEDURE IF EXISTS `sp_redeem_merchant_get_order`;
DELIMITER $$
CREATE PROCEDURE `sp_redeem_merchant_get_order`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pToDate date
    )
BEGIN

select t_om.`merchant_seq`, sum(t_op.`qty` * t_op.`sell_price`) as total_barang, sum(t_op.`qty` * t_op.`sell_price` * (t_op.`trx_fee_percent` / 100)) as total_komisi, 
	( 
 	Case When t_om.free_fee_seq > 0 Then
    	Case When t_om.real_expedition_service_seq = 0 Then
        	t_om.total_ship_real * (100-t_om.exp_fee_percent) / 100 
        Else
        	t_om.total_ship_real * t_om.exp_fee_percent / 100 * (-1)
        End
    Else
    	Case When t_om.real_expedition_service_seq = 0 Then
        	t_om.total_ship_real
        Else
        	0 
        End
 	END
 ) as total_ekpedisi
 
from `t_order_product` t_op join 

	(
	select tom.`order_seq`, 
               tom.`merchant_info_seq`, 
               tom.free_fee_seq, 
               qry.total_ship_charged,
               qry.total_ship_real,
               tom.real_expedition_service_seq, 
               mminfo.`exp_fee_percent`, 
               mminfo.`merchant_seq`
        from 
            t_order_merchant tom join m_merchant_info mminfo on tom.`merchant_info_seq`=mminfo.`seq` 
            join (
                select 
                        om.merchant_info_seq,
                        SUM(om.total_ship_charged) as total_ship_charged, 
                        SUM(om.total_ship_real) as total_ship_real 
                from t_order_merchant om 
                where   
                    om.order_status='D' and 
                    om.redeem_seq is null and 
                    om.received_date <= pToDate 
                group by om.merchant_info_seq
                )as qry on qry.merchant_info_seq = tom.merchant_info_seq   
	where 
            tom.order_status='D' and 
            tom.redeem_seq is null and 
            tom.received_date <= pToDate 
	) 

	t_om on t_op.`order_seq`=t_om.`order_seq` and t_op.`merchant_info_seq`=t_om.`merchant_info_seq` 


where t_op.`product_status`='R' group by t_om.`merchant_seq`;

END$$
