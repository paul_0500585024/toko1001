DROP PROCEDURE IF EXISTS sp_invoicing_agent_by_partner;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_agent_by_partner (
    `pUserID` VARCHAR(25),
    `pIPAddr` VARCHAR(50),
    `pSeq` INTEGER(10) UNSIGNED,
    `pPartnerSeq` INTEGER(10) UNSIGNED
)
BEGIN
    SELECT tra.`agent_seq`, tra.`status`, tra.`total`, tra.`paid_date`, tra.`paid_partner_date`, 
    `tra`.`redeem_seq`, ma.name, ma.`email` FROM
    `t_redeem_agent` tra JOIN m_agent ma ON tra.agent_seq=ma.seq
    WHERE `tra`.`redeem_seq`=pSeq AND ma.`partner_seq`=pPartnerSeq;
END$$