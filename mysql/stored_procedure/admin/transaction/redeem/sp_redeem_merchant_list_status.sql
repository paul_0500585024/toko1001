CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_redeem_merchant_list_status`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq int(10) unsigned
    )
BEGIN

	SELECT count(`redeem_seq`) as jumlah FROM 
  `t_redeem_merchant`
	Where
		`redeem_seq` = pSeq and status="U";
END