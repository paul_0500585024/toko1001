CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_redeem_component_by_seq`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq Int(10) UNSIGNED
    )
BEGIN

	Select 
  `redeem_seq`,
  `merchant_seq`,
  `type`,
  `mutation_type`,
  `total`,
  `created_by`,
  `created_date`,
  `modified_by`,
  `modified_date`      
	From t_redeem_component
	Where
		redeem_seq = pSeq;
        
END