CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_merchant_update_ap_amt`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
	pSeq Int unsigned,
	pApAmount Decimal(15,0)
)
BEGIN

	Update m_merchant Set 
		ap_amt = (ap_amt + pApAmount),
		modified_by = pUserID,
		modified_date = now() 
	Where 
		seq = pSeq;

END