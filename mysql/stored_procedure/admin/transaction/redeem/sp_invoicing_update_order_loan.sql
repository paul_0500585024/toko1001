DROP PROCEDURE IF EXISTS sp_invoicing_update_order_loan;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_update_order_loan (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pSeq INTEGER(10) UNSIGNED,
    pTdate DATE
)
BEGIN
    UPDATE 
        t_order_loan `tol` 
        JOIN t_order `tor` ON `tor`.`seq` = `tol`.`order_seq`
        JOIN t_order_merchant `tom` ON `tom`.`order_seq` = `tol`.`order_seq`
    SET
        `tol`.`collect_seq` = pSeq,    
        `tol`.`modified_by` = pUserID,
        `tol`.`modified_date` = NOW()
    WHERE
        `tom`.order_status='D' 
        AND `tol`.status_order = 'P'
        AND `tol`.collect_seq IS NULL 
        AND `tom`.received_date <= pTdate
        AND `tor`.`member_seq` IS NULL;
END$$