CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_redeem_period_get_lastdate`(
        pUserID VarChar(25),
        pIPAddr VarChar(50)
    )
BEGIN

	SELECT DATE_ADD(to_date, INTERVAL 1 DAY) AS from_date,DATE_ADD(to_date, INTERVAL 1 DAY) AS to_date, 0 as status
	From t_redeem_period order by from_date desc limit 1;
END