DROP PROCEDURE IF EXISTS `sp_redeem_period_update`;
DELIMITER $$
CREATE PROCEDURE `sp_redeem_period_update` (
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq int(10) unsigned,
        pFdate DATE,
        pTdate DATE,
        pExpeditionCommission Decimal(5,2)
)
BEGIN

update t_redeem_period set
    from_date = pFdate,
    to_date = pTdate,
    expedition_commission = pExpeditionCommission,
    modified_by = pUserID,
    modified_date = now()
Where
    seq = pSeq and `status`='O';

END$$

