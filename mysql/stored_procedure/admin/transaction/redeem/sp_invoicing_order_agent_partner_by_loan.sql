DROP PROCEDURE IF EXISTS sp_invoicing_order_agent_partner_by_loan;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_order_agent_partner_by_loan (
    `pUserID` VARCHAR(25),
    `pIPAddr` VARCHAR(50),
    `pSeq` INTEGER(10) UNSIGNED,
    `pPartnerSeq` INTEGER(10) UNSIGNED
)
BEGIN
    SELECT tor.seq AS order_seq, tor.order_no, tor.total_payment, tol.`total_installment`, tor.order_date 
    FROM `t_order` tor
    JOIN t_order_merchant tom ON tom.`order_seq` = tor.seq
    JOIN m_agent ma ON tor.agent_seq = ma.seq
    JOIN t_order_loan tol ON tor.seq = tol.order_seq
    WHERE tol.`collect_seq` = pSeq
    AND ma.`partner_seq` = pPartnerSeq
    GROUP BY tor.`order_no`;
END$$