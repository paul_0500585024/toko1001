DROP PROCEDURE IF EXISTS sp_invoicing_period_get_lastdate;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_period_get_lastdate (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50)
)
BEGIN
    SELECT DATE_ADD(to_date, INTERVAL 1 DAY) AS from_date,DATE_ADD(to_date, INTERVAL 1 DAY) AS to_date, 0 AS STATUS
    FROM t_collecting_period ORDER BY from_date DESC LIMIT 1;
END$$