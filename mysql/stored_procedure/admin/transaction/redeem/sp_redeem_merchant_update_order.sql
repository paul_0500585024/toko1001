CREATE DEFINER=`root`@`%` PROCEDURE `sp_redeem_merchant_update_order`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq int(10) unsigned,
        pTdate DATE
    )
BEGIN

update t_order_merchant set
	redeem_seq = pSeq,    
    modified_by = pUserID,
    modified_date = now()
Where
	order_status='D' and redeem_seq is null and received_date <= pTdate;

END