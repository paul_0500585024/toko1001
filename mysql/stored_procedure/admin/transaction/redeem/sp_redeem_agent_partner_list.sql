DROP PROCEDURE IF EXISTS sp_redeem_agent_partner_list;
DELIMITER $$
CREATE PROCEDURE sp_redeem_agent_partner_list (
    `pUserID` VARCHAR(25),
    `pIPAddr` VARCHAR(50),
    `pSeq` INTEGER(10) UNSIGNED
)
BEGIN
    SELECT mp.seq, mp.`email`, mp.`name` 
    FROM `m_partner` mp 
    WHERE mp.`seq` IN (
        SELECT DISTINCT(partner_seq) FROM `t_redeem_agent_component` WHERE redeem_seq = pSeq
    );
END$$