CREATE DEFINER=`root`@`%` PROCEDURE `sp_redeem_merchant_get_expedition_invoice`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq int(10) unsigned
    )
BEGIN

SELECT 
  m_mi.`merchant_seq`,
  sum(t_eia.`ship_price` * (m_mi.`exp_fee_percent`/100)) as total_barang,
  sum(t_eia.`ins_price`) as total_ekpedisi,
  0 as total_komisi  
FROM 
  `t_expedition_invoice` t_ei
  join 
  `t_expedition_invoice_awb` t_eia
  on t_ei.`seq`= t_eia.`invoice_seq`
  join 
  `t_order_merchant` t_om
  on t_eia.`invoice_seq` = t_om.`exp_invoice_seq` and t_eia.`seq` = t_om.`exp_invoice_awb_seq`

  join (
  
  SELECT 
  mmi.`seq`,
  mmi.`merchant_seq`,
  mmi.`pickup_addr`,
  mmi.`pickup_district_seq`,
  mmi.`pickup_zip_code`,
  mmi.`exp_fee_percent`,
  mmi.`ins_fee_percent`,
  mmi.`bank_name`,
  mmi.`bank_branch_name`,
  mmi.`bank_acct_no`,
  mmi.`bank_acct_name`
FROM `m_merchant_info` mmi
INNER JOIN
    (SELECT `merchant_seq`, MAX(seq) AS MaxSeq
    FROM `m_merchant_info`
    GROUP BY merchant_seq) gmmi 
ON mmi.`merchant_seq` = gmmi.`merchant_seq` 
AND mmi.seq = gmmi.MaxSeq
) m_mi
on t_om.`merchant_info_seq`=m_mi.`seq`

  where t_ei.`redeem_seq`=pSeq and t_ei.`status`='V' 
  group by t_om.`merchant_info_seq` ;
  
END