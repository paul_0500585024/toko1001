DROP PROCEDURE IF EXISTS sp_invoicing_partner_by_seq;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_partner_by_seq (
    `pUserID` VARCHAR(25),
    `pIPAddr` VARCHAR(50),
    `pSeq` INTEGER(10) UNSIGNED,
    `pPartnerSeq` INTEGER(10) UNSIGNED
)
BEGIN
IF pSeq <> "" THEN
    SELECT k.status_order, k.paid_date INTO @status,@paid_date 
    FROM t_order_loan k 
    JOIN t_order l ON k.order_seq = l.seq
    JOIN m_agent a ON a.seq = l.agent_seq
    JOIN m_partner p ON p.seq = a.partner_seq
    WHERE k.collect_seq = pSeq LIMIT 1;

    SELECT
        a.`collect_seq`,
        a.`partner_seq`,
        SUM(CASE WHEN a.`mutation_type`= 'D' THEN a.`total` ELSE a.`total` * -1 END) AS total, 
        @status as status,
        @paid_date AS paid_date,
        b.`name`,
        b.email,
        b.mobile_phone,
        b.bank_name,
        b.bank_branch_name,
        b.bank_acct_no,
        b.bank_acct_name,
        b.profile_img,
    FROM `t_collecting_component` a 
    JOIN `m_partner` b ON a.`partner_seq`=b.`seq`
    WHERE a.collect_seq = pSeq AND a.partner_seq = pPartnerSeq
    GROUP BY a.`collect_seq`, a.`partner_seq`;
ELSE
    SELECT
        a.`collect_seq`,
        a.`partner_seq`,
        SUM(CASE WHEN a.`mutation_type`= 'D' THEN a.`total` ELSE a.`total` * -1 END) AS total, 
        NULL as status,
        NULL AS paid_date,
        b.`name`,
        b.email,
        b.mobile_phone,
        b.bank_name,
        b.bank_branch_name,
        b.bank_acct_no,
        b.bank_acct_name,
        b.profile_img
    FROM `t_collecting_component` a 
    JOIN `m_partner` b ON a.`partner_seq`=b.`seq`
    WHERE a.collect_seq = pSeq AND a.partner_seq = pPartnerSeq
    GROUP BY a.`collect_seq`, a.`partner_seq`;    
END IF;
END$$