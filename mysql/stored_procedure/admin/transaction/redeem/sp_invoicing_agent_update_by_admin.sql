DROP PROCEDURE IF EXISTS sp_invoicing_agent_update_by_admin;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_agent_update_by_admin (
    `pUserID` VARCHAR(25),
    `pIPAddr` VARCHAR(50),
    `pSeq` INTEGER(10) UNSIGNED,
    `pPartnerSeq` INTEGER(10) UNSIGNED,
    `pPaidDate` DATETIME
)
BEGIN
    UPDATE t_order_loan tol JOIN t_order `to` ON tol.`order_seq` = `to`.seq JOIN m_agent ma ON `to`.`agent_seq` = ma.`seq`
    SET tol.status_order = 'F', tol.paid_date = pPaidDate
    WHERE tol.status_order = 'D' AND tol.`collect_seq` = pSeq AND ma.partner_seq = pPartnerSeq;
END$$
