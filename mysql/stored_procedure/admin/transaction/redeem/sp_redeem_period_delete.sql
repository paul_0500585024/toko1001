CREATE DEFINER=`root`@`%` PROCEDURE `sp_redeem_period_delete`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq INT(10) UNSIGNED
    )
BEGIN

DELETE FROM t_redeem_period WHERE seq = pSeq and `status`='O';


END