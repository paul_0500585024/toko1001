CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_redeem_merchant_report_by_seq`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq Int(10) UNSIGNED
    )
BEGIN

	SELECT 
  t_o.`seq`,
  t_o.`order_no`,
  t_o.`order_date`,
  t_o.`member_seq`,
  t_o.`receiver_name`,
  t_o.`receiver_address`,
  t_o.`receiver_district_seq`,
  t_o.`receiver_zip_code`,
  t_o.`receiver_phone_no`,

  t_om.`order_seq`,
  t_om.`merchant_info_seq`,
  t_om.`expedition_service_seq`,
  t_om.`real_expedition_service_seq`,
  t_om.`total_merchant`,
  t_om.`total_ins`,
  t_om.`total_ship_real`,
  t_om.`ship_date`,
  t_om.`received_date`,
  t_om.`redeem_seq`,

  t_op.`order_seq`,
  t_op.`merchant_info_seq`,
  t_op.`product_variant_seq`,
  t_op.`variant_value_seq`,
  t_op.`qty`,
  t_op.`sell_price`,
  t_op.`weight_kg`,
  t_op.`ship_price_real`,
  t_op.`ship_price_charged`,
  t_op.`trx_fee_percent`,
  t_op.`ins_rate_percent`,
  t_op.`product_status`

FROM 
  `t_order` t_o  
  join
  `t_order_merchant` t_om
  on t_o.`seq`=t_om.`order_seq`

  join
  `t_order_product` t_op
  on t_o.`seq`=t_op.`order_seq` and  t_om.`merchant_info_seq`= t_op.`merchant_info_seq`

  where t_om.`redeem_seq` = pSeq;
        
END