CREATE DEFINER=`root`@`%` PROCEDURE `sp_redeem_period_status_update`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq int(10) unsigned,
        pStatusNew char(1),
        pStatusOld char(1)
    )
BEGIN

update t_redeem_period set
	`status` = pStatusNew,
    modified_by = pUserID,
    modified_date = now()
Where
	seq = pSeq and `status`=pStatusOld;

END