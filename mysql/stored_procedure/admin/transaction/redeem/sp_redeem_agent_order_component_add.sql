DROP PROCEDURE IF EXISTS sp_redeem_agent_order_component_add;
DELIMITER $$
CREATE PROCEDURE sp_redeem_agent_order_component_add (
    `pUserID` VARCHAR(25),
    `pIPAddr` VARCHAR(50),
    `pSeq` INTEGER(10) UNSIGNED,
    `pType` CHAR(3),
    `pMType` CHAR(1),
    `pPaymentCode` CHAR(10),
    `pTdate` DATE
)
BEGIN
	Insert Into `t_redeem_agent_component`(
        `redeem_seq`,
        `partner_seq`,
        `type`,
        `mutation_type`,
        `total`,
        created_by,
        created_date,
        modified_by,
        modified_date
	)
    select pSeq, ma.partner_seq, pType, pMtype, SUM(`tor`.`total_payment`-tol.`total_installment`) total, pUserID, Now(), pUserID, Now() from
    t_order tor
    left outer join t_order_loan tol on tor.seq=tol.order_seq
    join `m_agent` ma on `tor`.`agent_seq`=ma.`seq`
    where
	tor.seq in (select top.order_seq from `t_order_product` top
    join `t_order_merchant` tom on `top`.`order_seq`=tom.`order_seq` and top.`merchant_info_seq`=tom.`merchant_info_seq`
	where tom.order_status='D' and tom.redeem_agent_seq is null and tom.received_date <= pTdate
    and top.`product_status`='R' ) and tor.member_seq is null and tor.`pg_method_seq` in
    (select seq from m_payment_gateway_method where pg_seq in (select seq from m_payment_gateway where `name`=pPaymentCode))
    group by ma.`partner_seq`;
END$$