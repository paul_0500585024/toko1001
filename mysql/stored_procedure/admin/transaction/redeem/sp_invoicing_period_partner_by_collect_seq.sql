DROP PROCEDURE IF EXISTS `sp_invoicing_period_partner_by_collect_seq`;
DELIMITER $$
CREATE PROCEDURE `sp_invoicing_period_partner_by_collect_seq` (
    `pUserID` VARCHAR(25),
    `pIPAddr` VARCHAR(50),
    `pSeq` INTEGER(10) UNSIGNED
)
BEGIN
    SELECT 
    tor.seq AS order_seq,
    tor.order_no,
    tor.total_payment,
    tol.`total_installment`,
    tor.order_date,
    prod.`name` AS product_name
FROM
    `t_order` tor
        JOIN
    t_order_product op ON tor.seq = op.order_seq
        JOIN
    m_product_variant pv ON op.product_variant_seq = pv.seq
        JOIN
    m_product prod ON pv.product_seq = prod.seq
        JOIN
    t_order_merchant tom ON tom.`order_seq` = tor.seq
        JOIN
    m_agent ma ON tor.agent_seq = ma.seq
        JOIN
    t_order_loan tol ON tor.seq = tol.order_seq
WHERE
    tol.`collect_seq` = pSeq
GROUP BY tor.`order_no`;
END$$

