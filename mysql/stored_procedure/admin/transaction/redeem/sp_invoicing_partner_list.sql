DROP PROCEDURE IF EXISTS sp_invoicing_partner_list;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_partner_list (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pCurrPage INTEGER,
    pRecPerPage INTEGER,
    pDirSort VARCHAR(4),
    pColumnSort VARCHAR(50),
    pSeq INT(10)
)
BEGIN
    DECLARE startRec INT;
    DECLARE totalPage INT;
    DECLARE totalRec INT;
    DECLARE sqlWhere VARCHAR(500);
    DECLARE status_order CHAR(1);
    DECLARE paid_date DATE;
    SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    SET sqlWhere = "";
    IF pSeq <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And a.collect_seq = '" , pSeq, "'");
    END IF;
    SET @sqlCommand = "Select Count(*) Into @totalRec FROM `t_collecting_component` a JOIN `m_partner` b ON a.`partner_seq`=b.`seq`";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        END IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    
    SET @status = NULL;
    SET @paid_date = NULL;

    IF pSeq <> "" THEN
        SELECT k.status_order, k.paid_date INTO @status,@paid_date 
        FROM t_order_loan k 
        JOIN t_order l ON k.order_seq = l.seq
	JOIN m_agent a ON a.seq = l.agent_seq
	JOIN m_partner p ON p.seq = a.partner_seq
	WHERE k.collect_seq = pSeq LIMIT 1;
    END IF;
    SET status_order = @status;  
    SET paid_date = @paid_date;  
    
    SET @sqlCommand = CONCAT("
SELECT
    a.`collect_seq`,
    a.`partner_seq`,
    SUM(CASE WHEN a.`mutation_type`= 'D' THEN a.`total` ELSE a.`total` * -1 END) AS total, ",IFNULL(CONCAT("'",@status,"'"),'NULL')," as status, ",IFNULL(CONCAT("'",@paid_date,"'"),'NULL')," as paid_date,
    b.`name`,
    b.email,
    b.mobile_phone,
    b.bank_name,
    b.bank_branch_name,
    b.bank_acct_no,
    b.bank_acct_name,
    b.profile_img
FROM `t_collecting_component` a 
JOIN `m_partner` b ON a.`partner_seq`=b.`seq`");
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
        SET @sqlCommand = CONCAT(@sqlCommand, " GROUP BY a.collect_seq,a.partner_seq");
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    SET @sqlCommand = CONCAT(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @totalRec = NULL;
    SET @sqlCommand = NULL;
END$$