CREATE DEFINER=`root`@`%` PROCEDURE `sp_redeem_merchant_update`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq int(10) unsigned,
        pMerchantSeq int(10) unsigned,
        pPaiddate DATE
    )
BEGIN

update t_redeem_merchant set
	paid_date = pPaiddate,
    `status` = 'P',
    modified_by = pUserID,
    modified_date = now()
Where
	merchant_seq = pMerchantSeq and `redeem_seq`=pSeq and `status`='U';

END