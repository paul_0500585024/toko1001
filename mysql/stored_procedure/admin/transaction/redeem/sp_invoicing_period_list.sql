DROP PROCEDURE IF EXISTS sp_invoicing_period_list;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_period_list (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pCurrPage INT,
    pRecPerPage INT,
    pDirSort VARCHAR(20),
    pColumnSort VARCHAR(50),
    pFdate VARCHAR(10),
    pTdate VARCHAR(10),
    pStatus CHAR(1)
)
BEGIN
    -- Begin variables for Paging
    DECLARE startRec INT;
    DECLARE totalPage INT;
    DECLARE totalRec INT;
    -- End variables for Paging
    -- Begin SQL Where
    DECLARE sqlWhere VARCHAR(500);
	SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    SET sqlWhere = "";
    IF pFdate <> "" AND pTdate <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And (a.from_date BETWEEN '",pFdate,"' AND '",pTdate,"') OR (a.to_date BETWEEN '",pFdate,"' AND '",pTdate,"')");
    END IF;
    IF pStatus <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And a.status= '" , pStatus, "'");
    END IF;
    -- End SQL Where
    -- Begin Paging Info
    SET @sqlCommand = "Select Count(seq) Into @totalRec From t_collecting_period a";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        END IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    -- End Paging Info
    SET @sqlCommand = "
SELECT 
    a.seq,
    a.from_date,
    a.to_date,
    a.status,
    a.`created_by`,
    a.`created_date`,
    a.`modified_by`,
    a.`modified_date` 
FROM t_collecting_period a";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
        SET @sqlCommand = CONCAT(@sqlCommand, " GROUP BY a.collect_seq,a.partner_seq");
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    SET @sqlCommand = CONCAT(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @totalRec = NULL;
    SET @sqlCommand = NULL;
END$$