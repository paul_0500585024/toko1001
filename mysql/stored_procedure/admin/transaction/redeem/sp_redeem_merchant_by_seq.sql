DELIMITER $$
DROP PROCEDURE sp_redeem_merchant_by_seq $$
CREATE PROCEDURE sp_redeem_merchant_by_seq
(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq int(10) unsigned,
        pMerchantSeq int(10) unsigned
    )
BEGIN
SELECT
	a.`redeem_seq`,
	a.`merchant_seq`,
	a.`merchant_info_seq`,
	a.`total`,
	a.`status`,
	a.`paid_date`,
	a.notif_status,
	b.`bank_name`,
	b.`bank_branch_name`,
	b.`bank_acct_no`,
	b.`bank_acct_name`,
	c.`name`,
        d.expedition_commission
FROM
	`t_redeem_merchant` a join t_redeem_period d on
            d.seq = a.redeem_seq
        left outer join `m_merchant_info` b
	on a.`merchant_seq`=b.`merchant_seq` and a.`merchant_info_seq`=b.`seq`
	left outer join `m_merchant` c
	on a.`merchant_seq`=c.`seq`
	Where
	a.`redeem_seq` = pSeq and a.`merchant_seq`=pMerchantSeq;
END $$
DELIMITER ;
