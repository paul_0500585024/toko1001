DROP PROCEDURE IF EXISTS `sp_order_merchant_by_redeem`;
DELIMITER $$
CREATE PROCEDURE `sp_order_merchant_by_redeem`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pRedeemSeq INT (10) UNSIGNED,
        pMerchantSeq INT (10) UNSIGNED
    )
BEGIN

	SELECT
	t_o.`seq`,
	t_o.`order_no`,
	t_o.`order_date`,
	t_o.`member_seq`,
	t_o.`receiver_name`,
	t_o.`receiver_address`,
	t_o.`receiver_district_seq`,
	t_o.`receiver_zip_code`,
	t_o.`receiver_phone_no`,
	t_o.`payment_status`,
	t_o.`pg_method_seq`,
	t_o.`payment_retry`,
	t_o.`total_order`,
	t_o.`voucher_seq`,
	t_o.`voucher_refunded`,
	t_o.`total_payment`,
	t_o.`conf_pay_type`,
	t_o.`conf_pay_amt_member`,
	t_o.`conf_pay_date`,
	t_o.`conf_pay_note_file`,
	t_o.`conf_pay_bank_seq`,
	t_o.`conf_pay_amt_admin`,
	t_om.`merchant_info_seq`,
	t_om.`expedition_service_seq`,
	t_om.`real_expedition_service_seq`,
	t_om.`total_merchant`,
	t_om.`total_ins`,
	t_om.`total_ship_real`,
	t_om.`total_ship_charged`,
	t_om.`order_status`,
	t_om.`member_notes`,
	t_om.`printed`,
	t_om.`print_date`,
	t_om.`awb_seq`,
	t_om.`awb_no`,
	t_om.`ship_by`,
	t_om.`ship_by_exp_seq`,
	t_om.`ship_date`,
	t_om.`ship_note_file`,
	t_om.`ship_notes`,
	t_om.`received_date`,
	t_om.`received_by`,
	t_om.free_fee_seq,
	t_op.totalorder,
	t_op.totalexpedition,
	t_op.totalins,
	t_op.totalfee,
	m_mi.exp_fee_percent
FROM
  `t_order` t_o

  inner join `t_order_merchant` t_om
  on t_o.`seq`=t_om.`order_seq`
  join (select merchant_seq,seq,exp_fee_percent FROM `m_merchant_info` where merchant_seq=pMerchantSeq) m_mi on t_om.`merchant_info_seq` = m_mi.seq

  join (

	select t_op.order_seq,t_op.merchant_info_seq, sum(t_op.qty*t_op.sell_price) as totalorder,

	(
		Case When t_om.free_fee_seq > 0 Then
		Case When t_om.real_expedition_service_seq = 0 Then
			t_om.total_ship_charged * (100-t_om.exp_fee_percent) / 100 
                Else
                        t_om.total_ship_real* t_om.exp_fee_percent / 100 * (-1) end
	    Else
		Case When t_om.real_expedition_service_seq = 0 Then
			 	t_om.total_ship_real
		Else
			0 End
		END
	 ) as totalexpedition,

	sum(t_op.qty*t_op.sell_price*t_op.ins_rate_percent/100) as totalins,sum(t_op.qty*t_op.sell_price*t_op.trx_fee_percent/100) as totalfee
	from t_order_product t_op

	join

	(
		select tom.`order_seq`,tom.`merchant_info_seq`,tom.free_fee_seq,tom.real_expedition_service_seq,mminfo.`exp_fee_percent`,tom.total_ship_charged ,tom.total_ship_real from t_order_merchant tom
		join m_merchant_info mminfo on tom.`merchant_info_seq`=mminfo.`seq`
		where tom.order_status='D' and tom.redeem_seq =pRedeemSeq
	)

	t_om on t_op.`order_seq`=t_om.`order_seq` and t_op.`merchant_info_seq`=t_om.`merchant_info_seq`


	where t_op.product_status='R' group by t_op.order_seq,t_op.merchant_info_seq



  ) t_op on

  t_om.order_seq=t_op.order_seq and t_om.merchant_info_seq=t_op.merchant_info_seq
  where t_om.`redeem_seq` = pRedeemSeq;

END$$
