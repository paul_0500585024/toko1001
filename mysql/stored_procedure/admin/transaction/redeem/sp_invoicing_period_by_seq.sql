DROP PROCEDURE IF EXISTS sp_invoicing_period_by_seq;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_period_by_seq (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pSeq INT(10) UNSIGNED
)
BEGIN
    SELECT 
        `seq`,
        `from_date`,
        `to_date`,
        `status`,
        `created_by`,
        `created_date`,
        `modified_by`,
        `modified_date` 
    FROM
        t_collecting_period 
    WHERE
        seq = pSeq;
END$$