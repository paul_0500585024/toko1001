CREATE DEFINER=`root`@`%` PROCEDURE `sp_redeem_merchant_list`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pCurrPage INTEGER,
        pRecPerPage INTEGER,
        pDirSort VARCHAR(4),
        pColumnSort VARCHAR(50),
        pSeq INT(10)
    )
BEGIN
    -- Begin variables for Paging
    Declare startRec Int;
    Declare totalPage Int;
    Declare totalRec Int;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
    
    If pSeq <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And a.redeem_seq = '" , pSeq, "'");
    End If;

    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "Select Count(a.redeem_seq) Into @totalRec From t_redeem_merchant a ";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
SELECT
  a.`redeem_seq`,
  a.`merchant_seq`,
  a.`merchant_info_seq`,
  a.`total`,
  a.`status`,
  a.`paid_date`,
  b.`bank_name`,
  b.`bank_branch_name`,
  b.`bank_acct_no`,
  b.`bank_acct_name`,
  c.`name`
FROM 
  `t_redeem_merchant` a left outer join `m_merchant_info` b
  on a.`merchant_seq`=b.`merchant_seq` and a.`merchant_info_seq`=b.`seq` 
  left outer join `m_merchant` c
  on a.`merchant_seq`=c.`seq` 
    ";

    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);

    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END