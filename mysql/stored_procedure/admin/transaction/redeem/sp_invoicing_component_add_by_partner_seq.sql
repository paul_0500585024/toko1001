DROP PROCEDURE IF EXISTS sp_invoicing_component_add_by_partner_seq;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_component_add_by_partner_seq (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pSeq INT(10) UNSIGNED,
    pType CHAR(3),
    pMType CHAR(1),
    pPartnerSeq INT(10) UNSIGNED,    
    pTotal DECIMAL(10,0),
    pTotalInstallment DECIMAL(10,0)
)
BEGIN
    INSERT INTO `t_collecting_component`(
        `collect_seq`,
        `partner_seq`,
        `type`,
        `mutation_type`,
        `total`,
        `created_by`,
        `created_date`,
        `modified_by`,
        `modified_date`
    ) VALUES (
        pSeq,
        pPartnerSeq,
        pType,
        pMType,
        pTotal-pTotalInstallment,
        pUserID,
        NOW(),
        pUserID,
        NOW()
    );
END$$