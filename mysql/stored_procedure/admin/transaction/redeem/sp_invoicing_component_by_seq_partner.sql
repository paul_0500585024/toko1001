DROP PROCEDURE IF EXISTS sp_invoicing_component_by_seq_partner;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_component_by_seq_partner (
    `pUserID` VARCHAR(25),
    `pIPAddr` VARCHAR(50),
    `pSeq` INTEGER(10) UNSIGNED,
    `pPartnerSeq` INTEGER(10) UNSIGNED
)
BEGIN
    SELECT
        `collect_seq`,
        `partner_seq`,
        `type`,
        `mutation_type`,
        `total`,
        `created_by`,
        `created_date`,
        `modified_by`,
        `modified_date`
    FROM t_collecting_component
    WHERE
        collect_seq = pSeq AND partner_seq = pPartnerSeq;
END$$