CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_redeem_component_add`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq int(10) UNSIGNED,
        pType CHAR(3),
        pMType CHAR(1),
        pMerchantSeq int(10) UNSIGNED,    
        pTotal DECIMAL(10,0)
    )
BEGIN
	

	Insert Into `t_redeem_component`(		
        `redeem_seq`,
		`merchant_seq`,
        `type`,
        `mutation_type`,
        `total`,
		created_by,
		created_date,
		modified_by,
		modified_date
	) Values (
		pSeq,
		pMerchantSeq,
		pType,
        pMType,
        pTotal,
		pUserID,
		Now(),
		pUserID,
		Now()
	);

END