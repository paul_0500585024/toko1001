CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_redeem_component_sum_seq`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq Int(10) UNSIGNED
    )
BEGIN

	Select t_rc.merchant_seq, m_mi.merchant_info_seq,
       sum(if(t_rc.mutation_type='C', t_rc.total,(t_rc.total*-1))) as total,
       IF(sum(if(t_rc.mutation_type='C', t_rc.total,(t_rc.total*-1)))>0,'U','P') as status,
       if(sum(if(t_rc.mutation_type='C', t_rc.total,(t_rc.total*-1)))>0,NULL,now()) as paid_date
	From t_redeem_component t_rc
    join (
    
SELECT 
  mmi.`seq` merchant_info_seq,mmi.merchant_seq
FROM `m_merchant_info` mmi
INNER JOIN
    (SELECT `merchant_seq`, MAX(seq) AS MaxSeq
    FROM `m_merchant_info`
    GROUP BY merchant_seq) gmmi 
ON mmi.`merchant_seq` = gmmi.`merchant_seq` 
AND mmi.seq = gmmi.MaxSeq           
    
    )m_mi
    on t_rc.merchant_seq=m_mi.merchant_seq
	Where
		t_rc.redeem_seq = pSeq
        group by t_rc.merchant_seq,t_rc.redeem_seq;
        
END