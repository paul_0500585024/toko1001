DROP PROCEDURE IF EXISTS sp_invoicing_period_add;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_period_add (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pFdate VARCHAR(10),
    pTdate DATE
)
BEGIN
    DECLARE new_seq INT(10) UNSIGNED;
    SELECT 
        MAX(seq) + 1 INTO new_seq
    FROM t_collecting_period;
    IF new_seq IS NULL THEN
        SET new_seq = 1;
    END IF;
    INSERT INTO t_collecting_period(		
        seq,
        from_date,
        to_date,
        `status`,
        created_by,
        created_date,
        modified_by,
        modified_date
    ) VALUES (
        new_seq,
        pFdate,
        pTdate,
        'O',
        pUserID,
        NOW(),
        pUserID,
        NOW()
    );
END$$