DROP PROCEDURE IF EXISTS `sp_redeem_period_by_seq`;
DELIMITER $$
CREATE PROCEDURE `sp_redeem_period_by_seq` (
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq INT(10) unsigned
)
BEGIN

Select
    `seq`,
    `from_date`,
    `to_date`,
    `status`,
     expedition_commission,
    `created_by`,
    `created_date`,
    `modified_by`,
    `modified_date`    
From t_redeem_period
Where
    seq = pSeq;

END$$

