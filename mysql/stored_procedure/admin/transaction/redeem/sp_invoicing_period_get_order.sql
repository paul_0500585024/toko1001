DROP PROCEDURE IF EXISTS sp_invoicing_period_get_order;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_period_get_order (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pToDate DATE
)
BEGIN
    SELECT a.`partner_seq`, SUM(`to`.`total_payment`) AS total_payment, SUM(`tol`.`total_installment`) AS total_installment
    FROM t_order_loan `tol`
    INNER JOIN t_order `to` ON `to`.seq = `tol`.order_seq
    INNER JOIN t_order_merchant `tom` ON `tom`.`order_seq` = `to`.`seq`
    INNER JOIN t_order_product `top` ON `top`.`order_seq` = `to`.`seq`
    INNER JOIN m_agent a ON a.seq = `to`.`agent_seq`
    INNER JOIN m_partner p ON p.`seq`=a.`partner_seq`
    WHERE `top`.`product_status`='R' AND `tom`.order_status='D' AND `tol`.`collect_seq` IS NULL AND `tom`.`received_date` <= pToDate
    GROUP BY a.`partner_seq`;
END$$