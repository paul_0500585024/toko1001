CREATE DEFINER=`root`@`%` PROCEDURE `sp_order_voucher_list`(
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pCurrPage Int,
    pRecPerPage Int,
    pDirSort VarChar(4),
    pColumnSort Varchar(50),
    pOrderNo VARCHAR(50),
    pSDate Varchar(15),
    pEDate Varchar(15),
	pPaymentStatus char(1),
    pPgMethodSeq char(1)
)
BEGIN
    -- Begin variables for Paging
    Declare startRec Int;
    Declare totalPage Int;
    Declare totalRec Int;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
      
   If pOrderNo <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And v.order_no like '%" , escape_string(pOrderNo), "%'");
    End If;
    
	If pSDate <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And v.order_date >='" , pSDate , "'");
    End If;
	
    If pEDate <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And v.order_date <='" , pEDate , "'");
    End If;
    
    If pPaymentStatus <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And v.payment_status like '%" , pPaymentStatus, "%'");
    End If;
    
    If pPgMethodSeq <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And v.pg_method_seq like '%" , pPgMethodSeq, "%'");
    End If;
    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "Select Count(v.seq) Into @totalRec From t_order_voucher v
																JOIN
															m_payment_gateway_method g ON v.pg_method_seq = g.seq
																JOIN
															m_member m ON v.member_seq = m.seq
																JOIN
															m_provider_service_nominal n ON v.provider_nominal_seq = n.seq";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
        SELECT 
			v.seq,
			v.order_no,
			v.order_date,
			v.member_seq,
			v.phone_no,
			v.pg_method_seq,
			v.provider_nominal_seq,
			v.total_payment,
			v.total_order,
			v.payment_status,
			v.order_status,
			v.signature,
			v.created_by,
			v.created_date,
			v.modified_by,
			v.modified_date,
			g.`name` AS payment_name,
			m.`name` AS member_name,
			n.nominal
		FROM
			t_order_voucher v
				JOIN
			m_payment_gateway_method g ON v.pg_method_seq = g.seq
				JOIN
			m_member m ON v.member_seq = m.seq
				JOIN
			m_provider_service_nominal n ON v.provider_nominal_seq = n.seq  ";

    If sqlWhere <> "" Then
		Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;
    
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END