CREATE DEFINER=`root`@`%` PROCEDURE `sp_order_return_save_received`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pReturnNo VarChar(25),
        pShipmentStatus Char(1),
        pStatus Char(1)
    )
BEGIN

Declare pValidStatus Char(1);
Declare Valid Boolean;

Select
	shipment_status Into pValidStatus
From 
	t_order_product_return 
Where 
	return_no = pReturnNo;


IF (pShipmentStatus = pValidStatus) 
Then
	Update t_order_product_return Set
		shipment_status = pStatus,
		admin_received_date = Now(),
		modified_by = pUserID,
		modified_date = Now()
	Where
		return_no = pReturnNo;
        
	Select True as Valid;
Else
	Select False as Valid;
End If;
       

END