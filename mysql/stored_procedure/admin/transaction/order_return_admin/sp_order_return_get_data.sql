CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_order_return_get_data`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pSeq TinyInt Unsigned
)
BEGIN
/*Information Detail*/
	Select
		opr.return_no,
        opr.return_status,
        opr.shipment_status,
        opr.created_date,
        o.order_no,
        o.receiver_address,
        m.`name` as member_name,
        m.email as email_member,
        m.mobile_phone as member_phone,
        d.`name` as district_name,
        c.`name` as city_name,
        p.`name` as province_name,
        i.seq as product_seq,
        opr.admin_received_date,
        opr.merchant_received_date,
        opr.member_received_date,
        (Select e.`name` 
			From t_order_product_return opr
				Join m_expedition e On opr.exp_seq_to_admin = e.seq
			Where opr.seq = pSeq
		) as exp_to_admin,
        (Select e.`name` 
			From t_order_product_return opr
				Join m_expedition e On opr.exp_seq_to_member = e.seq
			Where opr.seq = pSeq
		) as exp_to_member,
        (Select e.`name` 
			From t_order_product_return opr
				Join m_expedition e On opr.exp_seq_to_merchant = e.seq
			Where opr.seq = pSeq
		) as exp_to_merchant,
        opr.awb_admin_no,
        opr.awb_merchant_no,
        opr.awb_member_no,
        opr.review_admin,
        opr.review_merchant,
        opr.review_member,
        opr.ship_to_merchant_date,
        opr.ship_to_member_date,
        opr.modified_date
	From 
		t_order_product_return opr
        Join t_order o
			On opr.order_seq = o.seq
		Join m_member m
			On o.member_seq = m.seq
		Join m_district d
			On o.receiver_district_seq = d.seq
		Join m_city c
			On d.city_seq = c.seq
		Join m_province p
			On c.province_seq = p.seq
		Join m_product_variant pv
			On opr.product_variant_seq = pv.seq
		Join m_product i
			On pv.product_seq = i.seq
    Where
		opr.seq = pSeq;
        
        
/*Product Detail*/
	Select 
        p.`name` as product_name,
        vv.seq as value_seq,
        vv.`value`,
        pv.sell_price,
        opr.qty,
        pv.pic_1_img,
        p.merchant_seq
	From 
		t_order_product_return opr
		Join m_product_variant pv
			On opr.product_variant_seq = pv.seq
		Join m_product p
			On pv.product_seq = p.seq
		Join m_variant_value vv
			On pv.variant_value_seq = vv.seq
    Where
		opr.seq = pSeq;
        
END
