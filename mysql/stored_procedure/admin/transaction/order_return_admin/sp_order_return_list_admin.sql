DROP PROCEDURE IF EXISTS `sp_order_return_list_admin`;
DELIMITER $$
CREATE PROCEDURE `sp_order_return_list_admin`(
        `pUserID` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pCurrPage` INTEGER UNSIGNED,
        `pRecPerPage` INTEGER UNSIGNED,
        `pDirSort` VARCHAR(4),
        `pColumnSort` VARCHAR(50),
        `pReturnNo` VARCHAR(25),
        `pMemberEmail` VARCHAR(100),
        `pDateSearch` CHAR(1),
        `pReturnDateFrom` DATETIME,
        `pReturnDateTo` DATETIME,
        `pReturnStatus` CHAR(1)
    )
BEGIN
    -- Begin variables for Paging
    Declare startRec Int unsigned;
    Declare totalPage Int unsigned;
    Declare totalRec Int unsigned;
    -- End variables for Paging
    -- Begin SQL Where
    Declare sqlWhere VarChar(500);
	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
      
	If pReturnNo <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And opr.return_no = '" , pReturnNo , "'");
    End If;
      
    If pMemberEmail <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And (m.email = '" , pMemberEmail , "' or ma.email = '" , pMemberEmail , "')");
    End If;
    
    If (pDateSearch = "1") Then
        Set sqlWhere = Concat(sqlWhere, " And opr.created_date between '" , pReturnDateFrom , "' And '" , pReturnDateTo , "'");
    End If;
    
    If pReturnStatus <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And opr.return_status = '" , pReturnStatus , "'");
    End If;
	
    -- End SQL Where
    -- Begin Paging Info
    Set @sqlCommand = "Select Count(opr.seq) Into @totalRec From t_order_product_return opr Join t_order o On opr.order_seq = o.seq left outer Join m_member m On o.member_seq = m.seq left outer Join m_agent ma On o.agent_seq = ma.seq";
    
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1 " , sqlWhere);
    End If;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);
        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;
        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info
    Set @sqlCommand = "
        Select
			opr.seq,
            opr.return_status,
			opr.return_no,
            opr.qty,
            opr.awb_member_no,
            opr.shipment_status,
            opr.created_date as return_date,
            p.`name` as product_name,
            m.email as member_email,
			ma.email as agent_email,
            o.order_no,
            '' as action
		From t_order_product_return opr
			Join t_order o On opr.order_seq = o.seq
            Join m_product_variant pv On opr.product_variant_seq = pv.seq
            Join m_product p On pv.product_seq = p.seq
            left outer Join m_member m On o.member_seq = m.seq
            left outer Join m_agent ma On o.agent_seq = ma.seq
    ";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;
    
    Set @sqlCommand = Concat(@sqlCommand, " order by ", " ", pColumnSort, " ", pDirSort);
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    Select pCurrPage, totalPage, totalRec as total_rec;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set @totalRec = Null;
    Set @sqlCommand = Null;
END$$
DELIMITER ;