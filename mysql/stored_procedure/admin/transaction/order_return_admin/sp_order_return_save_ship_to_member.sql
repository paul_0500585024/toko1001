DELIMITER $$
CREATE PROCEDURE sp_order_return_save_ship_to_member
(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pReturnNo VarChar(25),
        pExpSeqToMember TinyInt Unsigned,
        pAwbAdminNo VarChar(25),
        pReviewAdmin Varchar(1000),
        pShipmentStatus Char(1),
        pStatus Char(1)
    )
BEGIN

Declare pValidStatus Char(1);
Declare pValidDate Char(1);
Declare Valid Boolean;
Declare ValidDate Boolean;
Declare pAdminReceivedDate DateTime;
Declare pShipToMemberDate Datetime;

Select
	shipment_status Into pValidStatus
From
	t_order_product_return
Where
	return_no = pReturnNo;

Select
	Now() Into pShipToMemberDate
From
	t_order_product_return
Where
	return_no = pReturnNo;

Select
	admin_received_date Into pAdminReceivedDate
From
	t_order_product_return
Where
	return_no = pReturnNo;


IF (pShipmentStatus = pValidStatus)
Then
	IF (pShipToMemberDate >= pAdminReceivedDate)
	Then
		Update t_order_product_return Set
			shipment_status = pStatus,
			return_status = 'R',
			review_admin = pReviewAdmin,
			ship_to_member_date = pShipToMemberDate,
			exp_seq_to_member = pExpSeqToMember,
			awb_admin_no = pAwbAdminNo
		Where
			return_no = pReturnNo;

		Select
			m.email,
			m.`name`,
			opr.review_admin
		From
			t_order_product_return opr
			Join t_order o
				On opr.order_seq = o.seq
			Join m_member m
				On o.member_seq = m.seq
		Where
			opr.return_no = pReturnNo;

		Select True as Valid, True as ValidDate;
	Else
		Select False as ValidDate;
	End IF;
Else
	Select False as Valid;
End If;


END $$
DELIMITER ;
