CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_order_return_get_data_header`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pReturnNo Varchar(25)
)
BEGIN
Declare pShipmentStatus Char(1);

	Select shipment_status Into pShipmentStatus 
    From t_order_product_return
    Where return_no = pReturnNo;

	Select
		seq,
		return_no,
        shipment_status,
        awb_admin_no,
        review_admin,
        review_member,
        created_date,
        (CASE pShipmentStatus
            WHEN 'S' THEN exp_seq_to_merchant
            WHEN 'T' THEN exp_seq_to_member
            ELSE Null
		END) as expedition_seq,
        now() as `date`
	From t_order_product_return
	Where
		return_no = pReturnNo;
        
        
        
END
