CREATE DEFINER=`root`@`%` PROCEDURE `sp_order_return_save_ship_to_merchant`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pReturnNo VarChar(25),
        pExpSeqToMerchant TinyInt Unsigned,
        pAwbAdminNo VarChar(25),
        pReviewAdmin Varchar(1000),
        pShipmentStatus Char(1),
        pStatus Char(1)
    )
BEGIN

Declare pValidStatus Char(1);
Declare pValidDate Char(1);
Declare Valid Boolean;
Declare ValidDate Boolean;
Declare pAdminReceivedDate DateTime;
Declare pShipToMerchantDate Datetime;

	Select
		shipment_status Into pValidStatus
	From 
		t_order_product_return 
	Where 
		return_no = pReturnNo;

	Select 
		Now() Into pShipToMerchantDate
	From 
		t_order_product_return 
	Where 
		return_no = pReturnNo;    

	Select 
		admin_received_date Into pAdminReceivedDate
	From 
		t_order_product_return 
	Where 
		return_no = pReturnNo;
		
    
IF (pShipmentStatus = pValidStatus) 
Then
	IF (pShipToMerchantDate >= pAdminReceivedDate)
	Then
		Update t_order_product_return Set
			shipment_status = pStatus,
			review_admin = pReviewAdmin,
			ship_to_merchant_date = pShipToMerchantDate,
			exp_seq_to_merchant = pExpSeqToMerchant,
			awb_admin_no = pAwbAdminNo
		Where
			return_no = pReturnNo;

		Select True as Valid, True as ValidDate;
	Else
		Select False as ValidDate;
	End IF;
Else
	Select False as Valid;
End If;

END