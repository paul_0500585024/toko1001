CREATE DEFINER=`root`@`%` PROCEDURE `sp_expedition_merchant_by_redeem`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pRedeemSeq INT (10) UNSIGNED,
        pMerchantSeq INT (10) UNSIGNED
    )
BEGIN

	SELECT t_eia.awb_no order_no,t_eia.awb_date,t_eia.destination,t_eia.ship_price totalorder,t_eia.ins_price
  FROM 
  `t_expedition_invoice_awb` t_eia join `t_expedition_invoice` t_ei
  on t_eia.invoice_seq=t_ei.seq
  join t_order_merchant t_om
  on t_eia.invoice_seq=t_om.`exp_invoice_seq` and t_eia.`seq`=t_om.`exp_invoice_awb_seq`
  		where
			t_ei.`redeem_seq` = pRedeemSeq and t_om.redeem_seq=pRedeemSeq 
            and  t_om.merchant_info_seq in (select seq from m_merchant_info where merchant_seq=pMerchantSeq);

END