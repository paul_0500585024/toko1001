CREATE DEFINER=`root`@`%` PROCEDURE `sp_admin_get_data_withdraw_member`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq BigInt(10),
        pMemberSeq BigInt(10)
    )
BEGIN

Select
	ma.seq,
    ma.member_seq,
    m.name,
    m.email,
    m.deposit_amt,
    ma.trx_no,
	ma.deposit_trx_amt,
	ma.non_deposit_trx_amt,
	ma.status
From t_member_account ma
Join m_member m
	On ma.member_seq = m.seq
Where
	ma.seq = pSeq And ma.member_seq = pMemberSeq;
    

END