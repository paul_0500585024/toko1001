CREATE DEFINER=`root`@`%` PROCEDURE `sp_admin_withdraw_save_approve`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pSeq BigInt(10),
    pMemberSeq BigInt(10),
    pDepositTrxAmt Decimal(10),
    pNonDepositTrxAmt Decimal(10),
    pDepositAmt Decimal(10)
    )
BEGIN

Update t_member_account
Set
	`status` = 'A',
    refund_date = Date_Format(Now(),"%Y-%m-%d")
Where
	seq = pSeq  And member_seq = pMemberSeq;
    

Select(pDepositAmt - pDepositTrxAmt) Into @DepositAmt;
Update m_member
Set deposit_amt = @DepositAmt
Where
	seq = pMemberSeq ;
    
END