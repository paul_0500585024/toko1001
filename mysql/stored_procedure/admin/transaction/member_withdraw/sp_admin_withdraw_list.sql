CREATE DEFINER=`root`@`%` PROCEDURE `sp_admin_withdraw_list`(
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pCurrPage Int unsigned,
    pRecPerPage Int unsigned,
    pDirSort VarChar(4),
    pColumnSort VarChar(50),
	pMemberEmail VarChar(100),
    pStatus Char(1)
)
BEGIN
    -- Begin variables for Paging
    Declare startRec Int unsigned;
    Declare totalPage Int unsigned;
    Declare totalRec Int unsigned;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
      
    If pMemberEmail <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And m.email = '" , pMemberEmail , "'");
    End If;
    
    If pStatus <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And ma.status = '" , pStatus, "'");
    End If;
	
    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "Select Count(ma.seq) Into @totalRec From t_member_account ma 
														   Join m_member m On ma.member_seq = m.seq
                                                           ";
    
    Set @sqlCommand = Concat(@sqlCommand, " Where ma.trx_type ='" ,'WDW', "'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
        Select
			ma.seq,
			ma.trx_no,
            ma.trx_date,
            m.seq as member_seq,
            m.email as member_email,
            m.deposit_amt,
            ma.deposit_trx_amt,
            ma.non_deposit_trx_amt,
            ma.bank_name,
            ma.bank_acct_no,
            ma.bank_acct_name,
            ma.refund_date,
            ma.status
		From t_member_account ma
		Join m_member m
			On ma.member_seq = m.seq
    ";

    Set @sqlCommand = Concat(@sqlCommand, " Where ma.trx_type ='" ,'WDW', "'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    
    Set @sqlCommand = Concat(@sqlCommand, " order by ", "", pColumnSort, " ", pDirSort);
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END