CREATE DEFINER=`root`@`%` PROCEDURE `sp_admin_withdraw_save_reject`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
	pSeq BigInt(10),
	pMemberSeq BigInt(10)
)
BEGIN

Update t_member_account
Set
	`status` = 'R'
Where
	seq = pSeq And member_seq = pMemberSeq;


END