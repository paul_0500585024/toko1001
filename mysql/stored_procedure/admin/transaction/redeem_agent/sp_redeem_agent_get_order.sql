DROP PROCEDURE IF EXISTS `sp_redeem_agent_get_order`;
DELIMITER $$
CREATE PROCEDURE `sp_redeem_agent_get_order` (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pToDate DATE
)
BEGIN

    SELECT 
        t_or.agent_seq,
        SUM(t_op.qty * t_op.sell_price * (t_op.commission_fee_percent / 100)) AS total_komisi
    FROM t_order_product t_op
    JOIN t_order_merchant t_om ON t_om.order_seq=t_op.order_seq AND t_op.merchant_info_seq=t_om.merchant_info_seq
    JOIN t_order t_or  ON t_op.order_seq=t_or.seq
    JOIN m_agent m_a ON t_or.agent_seq=m_a.seq
    WHERE 
        t_om.order_status='D'
        AND t_op.product_status='R'
        AND t_om.received_date <= pToDate
        AND t_om.redeem_agent_seq IS NULL
        AND t_or.member_seq IS NULL
    GROUP BY t_or.agent_seq;

END$$
DELIMITER ;

