DROP PROCEDURE IF EXISTS `sp_redeem_agent_total_partner_by_seq`;
DELIMITER $$
CREATE PROCEDURE `sp_redeem_agent_total_partner_by_seq` (
        pUserID VARCHAR(25),
        pIpAddress VARCHAR(50),
        pReceivedDate DATE,
        pRedemSeq INT UNSIGNED
)
BEGIN
    SELECT 
        SUM(op.nominal_commission_partner * op.qty) as total_partner
    FROM
        t_order_product op
            JOIN
        t_order_merchant om ON om.order_seq = op.order_seq
            AND op.merchant_info_seq = om.merchant_info_seq
            JOIN
        t_order o ON o.seq = op.order_seq
    WHERE
        o.member_seq IS NULL
            AND om.order_status = 'D'
            AND op.product_status = 'R'
            AND om.received_date <= pReceivedDate
            AND om.redeem_agent_seq = pRedemSeq;
END$$
