DROP PROCEDURE IF EXISTS `sp_redeem_agent_by_partner`;
DELIMITER $$
CREATE PROCEDURE `sp_redeem_agent_by_partner` (
        `pUserID` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pSeq` INTEGER(10) UNSIGNED,
        `pPartnerSeq` INTEGER(10) UNSIGNED
)
BEGIN
    select 
        tra.`agent_seq`, 
        tra.`status`, 
        tra.`total`, 
        tra.`paid_date`, 
        tra.`paid_partner_date`, 
        `tra`.`redeem_seq`, 
        ma.name, ma.`email` 
    from
        `t_redeem_agent` tra join m_agent ma 
            on tra.agent_seq=ma.seq
    where 
        `tra`.`redeem_seq`= pSeq and 
        ma.`partner_seq`= pPartnerSeq;
END$$
DELIMITER ;

