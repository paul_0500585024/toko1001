DROP PROCEDURE IF EXISTS `sp_redeem_agent_commission_component_add`;
DELIMITER $$
CREATE PROCEDURE `sp_redeem_agent_commission_component_add` (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pSeq INTEGER(10) UNSIGNED,
    pType CHAR(3),
    pMType CHAR(1),
    pTotalPartner decimal(12,0)
)
BEGIN
     INSERT INTO t_redeem_agent_component(
	redeem_seq,
	partner_seq,
	type,
	mutation_type,
	total,
	created_by,
	created_date,
	modified_by,
	modified_date
    )
    SELECT 
        pSeq, 
        ma.partner_seq, 
        pType, 
        pMtype, 
        pTotalPartner, 
        pUserID, 
        Now(), 
        pUserID, 
        Now() 
    FROM t_redeem_agent_period tra  
    Join t_order_merchant tom ON tom.redeem_agent_seq = tra.seq
    Join t_order_product top ON top.order_seq = tom.order_seq
    Join t_order t on t.seq = tom.order_seq
    JOIN m_agent ma ON t.agent_seq= ma.seq
    WHERE tra.seq=pSeq and top.product_status = 'R'
    GROUP BY ma.partner_seq;


END$$
DELIMITER ;

