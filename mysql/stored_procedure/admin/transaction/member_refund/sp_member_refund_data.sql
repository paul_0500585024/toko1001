CREATE DEFINER=`root`@`%` PROCEDURE `sp_member_refund_data`(
	pUserID VarChar(100),
	pIPAddr VarChar(50),
	pSeq int(10),
    pMemberSeq int(10)
)
BEGIN

select 
	pgm.name as pgm_method, 
	m.email as email_member,
    m.`name`, 
    m.deposit_amt , 
    tma.`status` as status_now , 
    tma.member_seq,
    tma.deposit_trx_amt
    
from 
	t_member_account tma , 
    m_member m , 
    m_payment_gateway_method pgm 
where 
	tma.seq = pSeq and 
    tma.member_seq = pMemberSeq and 
    tma.member_seq = m.seq and 
    tma.pg_method_seq = pgm.seq; 
    
END