CREATE DEFINER=`root`@`%` PROCEDURE `sp_member_refund_approve`(
	pUserID VarChar(100),
	pIPAddr VarChar(50),
	pSeq int(10),
    pMemberSeq int(10)
)
BEGIN

update 	t_member_account set 
	`status` = 'A' , 
	refund_date = curdate() , 
	modified_date = now() , 
	modified_by = pUserID 
where 
	seq = pSeq 	and member_seq = pMemberSeq;

END