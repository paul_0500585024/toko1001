DROP PROCEDURE IF EXISTS `sp_member_refund_get_data_details`;
DELIMITER $$
CREATE PROCEDURE `sp_member_refund_get_data_details` (
        pUserID VARCHAR(100),
        pIPAddr VARCHAR(50),
        pSeq BIGINT(20) UNSIGNED,
        pMerchantInfoSeq BIGINT(20) UNSIGNED,
        pReturnNo VARCHAR(50),
        pProductStatus CHAR(1),
        pType VARCHAR(5)
)
BEGIN
IF (pType = 'RTR') THEN
    SELECT 
        SUM(topr.qty * mpv.sell_price) AS sub_total,
        0 AS ship_price_charged,
        0 AS grand_total_weight,
        0 AS sub_weight
    FROM
        t_order_product_return topr
            JOIN
        m_product_variant mpv ON topr.product_variant_seq = mpv.seq
    WHERE
        topr.return_no = pReturnNo;
ELSE
    SELECT 
        SUM(sell_price * qty) AS sub_total,
       (SELECT  SUM(qty * weight_kg) FROM
                t_order_product
            WHERE
                order_seq = pSeq AND product_status <> pProductStatus AND merchant_info_seq = pMerchantInfoSeq)AS sub_weight,
        ship_price_charged,
        (SELECT 
                SUM(qty * weight_kg)
            FROM
                t_order_product
            WHERE
                order_seq = pSeq
                    AND merchant_info_seq = pMerchantInfoSeq) AS grand_total_weight
    FROM
        t_order_product
    WHERE
        order_seq = pSeq
            AND merchant_info_seq = pMerchantInfoSeq
            AND product_status = pProductStatus;
END IF;
END$$
