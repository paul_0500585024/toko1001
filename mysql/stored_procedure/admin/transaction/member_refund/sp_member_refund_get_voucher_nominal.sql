CREATE DEFINER=`root`@`%` PROCEDURE `sp_member_refund_get_voucher_nominal`(
	pUserID VarChar(100),
        pIPAddr VarChar(50),
        pSeq int(20)
)
BEGIN

select `nominal` 
from m_promo_voucher mpv ,
t_order tor 
where mpv.seq = tor.voucher_seq 
and tor.seq = pSeq;
END