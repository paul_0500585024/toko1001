DROP PROCEDURE IF EXISTS `sp_member_refund_detail_product_list`;
DELIMITER $$
CREATE PROCEDURE `sp_member_refund_detail_product_list`(
		pUserID VarChar(100),
        pIPAddr VarChar(50),
        pHseq int(20),
        pMerchant_info_seq int(20)

)
BEGIN
 SELECT 
    m.seq AS merchant_seq,
    m.name AS merchant_name,
    vv.value AS variant_value,
    p.name AS product_name,
    pv.pic_1_img AS product_image,
    vvz.value AS size_name,
    op.variant_value_seq AS size_seq,
    op.sell_price,
    op.weight_kg,
    op.qty,
    op.sell_price * op.qty AS sell_total,
    op.ship_price_charged
FROM
    t_order_product op
        JOIN
    m_product_variant pv ON op.product_variant_seq = pv.seq
        JOIN
    m_variant_value vv ON pv.variant_value_seq = vv.seq
        JOIN
    m_product p ON pv.product_seq = p.seq
        JOIN
    m_merchant_info mi ON op.merchant_info_seq = mi.seq
        JOIN
    m_merchant m ON mi.merchant_seq = m.seq
        JOIN
    m_variant_value vvz ON vvz.seq = op.variant_value_seq
WHERE
    op.product_status = 'X'
        AND op.order_seq = pHseq
        AND op.merchant_info_seq = pMerchant_info_seq;
END$$