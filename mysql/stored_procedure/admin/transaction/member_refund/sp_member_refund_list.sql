DELIMITER $$
CREATE PROCEDURE sp_member_refund_list
(
        pUserID VarChar(100),
        pIPAddr VarChar(50),
        pCurrPage Int unsigned,
        pRecPerPage Int unsigned,
        pDirSort VarChar(4),
        pColumnSort Varchar(50),
        pTrx_no Varchar(100),
        pPgmethod int unsigned,
        pTrx_type varchar(10),
        pMember_email Varchar(100),
        pStatus Char(1)
    )
BEGIN
    -- Begin variables for Paging
    Declare startRec Int unsigned;
    Declare totalPage Int unsigned;
    Declare totalRec Int unsigned;
    -- End variables for Paging
    -- Begin SQL Where
    Declare sqlWhere VarChar(500);
	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
	If pTrx_no <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And a.trx_no like '%" , pTrx_no, "%'");
    End If;
    If pPgmethod <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And a.pg_method_seq = '" , pPgmethod, "'");
    End If;
    If pMember_email <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And m.email like '%" , pMember_email, "%'");
    End If;
    If pTrx_type <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And a.trx_type in ('" , pTrx_type, "')");
	else
		Set sqlWhere = Concat(sqlWhere, " And a.trx_type in ('CNL','RTR')");
    End If;
    If pStatus <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And a.status = '", pStatus ,"'");
	else
		Set sqlWhere = Concat(sqlWhere);
        End If;
    -- End SQL Where
    -- Begin Paging Info
    -- Set @sqlCommand = "Select Count(t_member_account.seq) as seq Into @totalRec From t_member_account , m_member "; --
    -- Set @sqlCommand = "Select Count(t_member_account.seq) as seq Into @totalRec From t_member_account,m_member,t_order_merchant Where  t_member_account.trx_no = t_order_merchant.ref_awb_no and t_member_account.member_seq = m_member.seq and t_member_account.trx_type = 'CNL'";
    Set @sqlCommand = "
		Select Count(a.seq) as seq Into @totalRec
		from `t_member_account` a
		inner join `m_payment_gateway_method` pgm on a.pg_method_seq = pgm.seq
		inner join `m_member` m on a.member_seq = m.seq
		left outer join `t_order_merchant`  b on a.`trx_no`=b.`ref_awb_no`
		left outer join
		`t_order_product_return`
		c on a.`trx_no`= c.`return_no`

    ";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1 " , sqlWhere);
    End If;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);
        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;
        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info
    set @sqlCommand = "
		select
		pgm.name as pg_name ,
		m.name as member_name,
		a.seq ,
		a.member_seq,
		a.trx_date,
		a.trx_no,
		a.refund_date,
		a.member_seq,
		a.deposit_trx_amt,
		a.created_date,
		a.member_seq,
		a.status,
		a.trx_type
		from `t_member_account` a
		inner join `m_payment_gateway_method` pgm on a.pg_method_seq = pgm.seq
		inner join `m_member` m on a.member_seq = m.seq
		left outer join `t_order_merchant`  b on a.`trx_no`=b.`ref_awb_no`
		left outer join  `t_order_product_return` c on a.`trx_no`= c.`return_no` ";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " where 1 " , sqlWhere);
    End If;
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    Select pCurrPage, totalPage, totalRec as total_rec;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set @totalRec = Null;
    Set @sqlCommand = Null;
END $$
DELIMITER ;
