CREATE DEFINER=`root`@`%` PROCEDURE `sp_member_add_deposit`(
	pUserID VarChar(100),
	pIPAddr VarChar(50),
	pMember_seq int(10),
    pDepositadd int(30)
)
BEGIN

update 
m_member 
set `deposit_amt` = pDepositadd 
where seq = pMember_seq;

END