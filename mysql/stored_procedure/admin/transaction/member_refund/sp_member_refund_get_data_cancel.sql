CREATE DEFINER=`root`@`%` PROCEDURE `sp_member_refund_get_data_cancel`(
pMerchantId VarChar(25),
        pIPAddr VarChar(50),
        pTrxNo varchar(50),
        pTrxType varchar(10)
)
BEGIN

		select  
        tor.order_seq ,
        tor.merchant_info_seq
		from
		t_member_account tma, 
        m_payment_gateway_method pgm , 
        m_member m  , 
        t_order_merchant tor      
        where
		tma.pg_method_seq = pgm.seq and 
        tma.member_seq = m.seq and 
        tma.trx_no = tor.ref_awb_no and 
        tma.trx_type = pTrxType and 
        tma.trx_no = pTrxNo ;


END