CREATE DEFINER=`root`@`%` PROCEDURE `sp_member_refund_get_deposit_trx`(
	pUserID VarChar(100),
	pIPAddr VarChar(50),
	pRef_awb_no varchar(100)
    
)
BEGIN

select 
	deposit_trx_amt
from 
	t_member_account 
where 
	trx_no = pRef_awb_no
    ;
END