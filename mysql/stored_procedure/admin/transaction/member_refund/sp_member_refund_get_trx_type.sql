CREATE DEFINER=`root`@`%` PROCEDURE `sp_member_refund_get_trx_type`(		
        pMerchantSeq VarChar(25),
        pIPAddr VarChar(50),
        pTrxNo varchar(50)
        )
BEGIN

select  
member_seq, 
seq, 
mutation_type, 
pg_method_seq, 
trx_type, 
trx_no, 
trx_date, 
deposit_trx_amt, 
non_deposit_trx_amt, 
refund_date, 
status, 
created_by, 
created_date, 
modified_by, 
modified_date
from t_member_account where trx_no = pTrxNo ;

END