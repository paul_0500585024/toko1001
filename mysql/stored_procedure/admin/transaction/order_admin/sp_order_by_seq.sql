DROP PROCEDURE IF EXISTS `sp_order_by_seq`;
DELIMITER $$
CREATE PROCEDURE `sp_order_by_seq`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
	pSeq smallint unsigned
)
BEGIN
    Select 
        o.seq,
        o.order_no, 
        o.order_date, 
        o.member_seq,
        o.receiver_name, 
        o.receiver_address,
        o.receiver_district_seq,
        o.receiver_zip_code,
        o.receiver_phone_no,
        o.payment_status,
        o.payment_status as order_payment_status,
        o.pg_method_seq,
        o.paid_date,
        o.payment_retry,
        o.total_order,
        o.voucher_seq, 
        o.voucher_refunded,
        o.total_payment,
        o.conf_pay_type,
        o.conf_pay_amt_member,
        o.conf_pay_date, 
        o.conf_pay_note_file, 
        o.conf_pay_bank_seq,
        o.conf_pay_amt_admin,
        case when m.`name` is null then concat(a.name, ' Partner: ', p.name) else m.name end as member_name,
        pg.`name` as pg_name,
        case when v.nominal is null then c.nominal else v.nominal end as nominal,
        b.logo_img,
        b.bank_acct_no,
        b.bank_acct_name,
        m.email email_member,
        pg.`name` as pg_name,
        a.email as email_agent,
        a.seq as agent_seq,
        tl.customer_seq,
        tl.dp,
        tl.admin_fee,
        tl.tenor,
        tl.loan_interest,
        tl.installment,
        tl.total_installment,
        tl.status_order,
        o.agent_seq,
        (select sum(total_ship_charged) from t_order_merchant where order_seq = o.seq) as total_ship
	From t_order o
		left join m_member m on
			o.member_seq = m.seq
                left join m_agent a on 
                        o.`agent_seq`= a.seq
		left join m_promo_voucher v on 
                        o.voucher_seq = v.seq
		left join m_coupon c on 
                        o.coupon_seq = c.seq
		left join m_bank_account b on	
			o.conf_pay_bank_seq = b.seq
		left join m_payment_gateway_method pg on
			o.pg_method_seq = pg.seq
                left join t_order_loan tl on 
			tl.order_seq = o.seq
		left join m_partner p on 
			a.partner_seq = p.seq
	Where
		o.seq = pSeq;
        
END$$