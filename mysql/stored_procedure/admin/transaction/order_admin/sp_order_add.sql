DELIMITER $$
CREATE PROCEDURE sp_order_add
(
        `pUserID` VARCHAR(25),
        `pIPAddress` VARCHAR(25),
        `pMemberSeq` MEDIUMINT UNSIGNED,
        `pOrderNo` VARCHAR(20),
        `pReceiverName` VARCHAR(100),
        `pReceiverAddress` TEXT,
        `pReceiverDistrictSeq` MEDIUMINT UNSIGNED,
        `pReceiverZipCode` VARCHAR(10),
        `pReceiverPhone` VARCHAR(50),
        `pPaymentStatus` CHAR(1),
        `pPaymentMethodSeq` TINYINT UNSIGNED,
        `pVoucherSeq` MEDIUMINT UNSIGNED,
        `pCouponSeq` INTEGER UNSIGNED,
        `pCreditSeq` INTEGER UNSIGNED,
        `pAgentSeq` MEDIUMINT UNSIGNED
    )
BEGIN
Declare new_seq SmallInt Unsigned;
Declare new_coupon_seq SmallInt Unsigned;
Select
	Max(seq) + 1 Into new_seq
From t_order;
If new_seq Is Null Then
	Set new_seq = 1;
End If;
Select
	Max(seq) + 1 Into new_coupon_seq
From m_coupon_trx
where
    coupon_seq = pCouponSeq;
If new_seq Is Null Then
	Set new_seq = 1;
End If;
If new_coupon_seq Is Null Then
	Set new_coupon_seq = 1;
End If;
if pVoucherSeq = 0 then
	set pVoucherSeq = null;
else
	update m_promo_voucher set
		trx_no = pOrderNo
	where
		seq = pVoucherSeq;
end if;
if pCouponSeq = 0 then
	set pCouponSeq = null;
else
    insert m_coupon_trx (
        coupon_seq,
        seq,
        trx_no,
        created_by,
        created_date
    ) values (
        pCouponSeq,
        new_coupon_seq,
        pOrderNo,
        pUserID,
        now()
    );
end if;
if pCreditSeq=0 THEN
	set pCreditSeq=null;
end if;

if pMemberSeq="" THEN
	set pMemberSeq=null;
end if;
if pAgentSeq="" THEN
	set pAgentSeq=null;
end if;
insert into t_order (
	seq,
	order_no,
	order_date,
	member_seq,
	agent_seq,
	receiver_name,
	receiver_address,
	receiver_district_seq,
	receiver_zip_code,
	receiver_phone_no,
	payment_status,
	pg_method_seq,
	voucher_seq,
	coupon_seq,
	promo_credit_seq,
	conf_pay_note_file,
	conf_pay_bank_seq,
	created_by,
	created_date,
	modified_by,
	modified_date
) values (
	new_seq,
	pOrderNo,
	now(),
	pMemberSeq,
	pAgentSeq,
	pReceiverName,
	pReceiverAddress,
	pReceiverDistrictSeq,
	pReceiverZipCode,
	pReceiverPhone,
	pPaymentStatus,
	pPaymentMethodSeq,
	pVoucherSeq,
	pCouponSeq,
	pCreditSeq,
	'',
	null,
	pUserID,
	now(),
	pUserID,
	now()
);
Select new_seq;
END $$
DELIMITER ;