DROP PROCEDURE IF EXISTS sp_order_merchant_by_order;
DELIMITER $$
CREATE PROCEDURE sp_order_merchant_by_order (
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pOrderSeq int(10) unsigned,
    pMerchantId int(10) unsigned
)
BEGIN
    SELECT
        om.order_seq,
        om.ship_note_file,
	o.order_date,
	o.order_no,
        o.payment_status,
        o.member_seq,
        m.email AS merchant_email,
        IFNULL(mb.`name`,a.name) AS member_name,
	om.merchant_info_seq,
	m.`name` AS merchant_name,
	o.pg_method_seq,
	p.`name` AS payment_name,
	om.awb_no,
	om.ship_date,
	o.receiver_address,
	om.order_status,
        om.expedition_service_seq,
        om.real_expedition_service_seq,
        om.received_date,
        exs.seq AS exs_seq,
        exs.name AS exs_name,
        es.seq AS es_seq,
        es.name AS es_name,
        exp.seq AS exp_seq,
        exp.name AS exp_name,
        esp.seq AS expedition_seq,
        esp.name AS esp_name
    FROM t_order_merchant om 
    JOIN t_order o ON om.order_seq = o.seq 
    JOIN m_merchant_info mi ON om.merchant_info_seq = mi.seq
    JOIN m_merchant m ON mi.merchant_seq = m.seq
    LEFT OUTER JOIN m_payment_gateway_method p ON o.pg_method_seq = p.seq
    JOIN m_expedition_service exs ON om.expedition_service_seq = exs.seq
    JOIN m_expedition_service exp ON om.real_expedition_service_seq = exp.seq
    JOIN m_expedition es ON exs.exp_seq = es.seq   
    JOIN m_expedition esp ON exp.exp_seq = esp.seq
    LEFT JOIN m_member mb ON o.member_seq = mb.seq
    LEFT JOIN m_agent a ON o.agent_seq = a.seq
    WHERE 
        om.order_seq = pOrderSeq 
        AND om.merchant_info_seq = pMerchantId;
END$$
