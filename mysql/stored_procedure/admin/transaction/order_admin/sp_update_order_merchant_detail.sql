CREATE DEFINER=`root`@`%` PROCEDURE `sp_update_order_merchant_detail`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pSeq BigInt Unsigned,
    pInvoiceSeq BigInt Unsigned,
    pOrderSeq BigInt Unsigned,
    pMerchantSeq TinyInt Unsigned,
    pOldOrderSeq BigInt Unsigned,
    pOldMerchantInfoSeq TinyInt Unsigned
    )
BEGIN
	
	Update 
		t_order_merchant
    Set 
		exp_invoice_seq = Null, 
		exp_invoice_awb_seq = Null
    Where 
		order_seq = pOldOrderSeq And merchant_info_seq = pOldMerchantInfoSeq;


	Update 
		t_order_merchant
    Set 
		exp_invoice_seq = pInvoiceSeq, 
		exp_invoice_awb_seq = pSeq
    Where 
		order_seq = pOrderSeq And merchant_info_seq In (select seq from m_merchant_info where merchant_seq = pMerchantSeq) ;
        

END