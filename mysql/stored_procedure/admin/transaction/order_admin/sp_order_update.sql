CREATE DEFINER=`root`@`%` PROCEDURE `sp_order_update`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pSeq bigint(20) Unsigned,
    pPaymentStatus Char(1),
    pPaidDate Date,
    pConfPayAmtAdmin DECIMAL(10,0),
    pOrderStatus char(1)
    )
BEGIN

update t_order set
	payment_status = pPaymentStatus,
    paid_date = pPaidDate,
    conf_pay_amt_admin = pConfPayAmtAdmin
Where
	seq = pSeq;
    

UPDATE t_order_merchant 
SET 
    order_status = pOrderStatus
WHERE
    order_seq = pSeq;

SELECT 
    seq
FROM
    t_order
WHERE
   seq = pSeq;
END