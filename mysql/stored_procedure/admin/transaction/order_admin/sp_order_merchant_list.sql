CREATE DEFINER=`root`@`%` PROCEDURE `sp_order_merchant_list`(
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pCurrPage Int,
    pRecPerPage Int,
    pDirSort VarChar(4),
    pColumnSort Varchar(50),
    pOrderNo VARCHAR(50),
    pSDate varchar(15),
    pEDate varchar(15),
	pPaymentName varchar(50),
    pMerchantSeq mediumint unsigned,
    pOrderStatus char(1),
    pPaymentStatus char(1)
)
BEGIN
    -- Begin variables for Paging
    Declare startRec Int;
    Declare totalPage Int;
    Declare totalRec Int;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
      
	If pOrderNo <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And o.order_no like '%" , escape_string(pOrderNo), "%'");
    End If;
    
    If pSDate <> ""Then
        Set sqlWhere = Concat(sqlWhere, " And o.order_date >='" , pSDate , "'");
    End If;
	
    If pEDate <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And o.order_date <='" , pEDate , "'");
    End If;
	
    If pPaymentName <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And o.pg_method_seq like '%" , pPaymentName, "%'");
    End If;
    
	 If pMerchantSeq <> 0 Then
        Set sqlWhere = Concat(sqlWhere, " And m.seq ='" , pMerchantSeq, "'");
    End If;
    
    If pOrderStatus <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And om.order_status like '%" , pOrderStatus, "%'");
    End If;
    
	If pPaymentStatus <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And o.payment_status like '%" , pPaymentStatus, "%'");
    End If;
    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "Select Count(om.order_seq) Into @totalRec 
		From t_order_merchant om join t_order o on
        om.order_seq = o.seq 
        join m_merchant_info mi on
        om.merchant_info_seq = mi.seq
        join m_merchant m on 
        mi.merchant_seq = m.seq
        left outer join m_payment_gateway_method p on
		o.pg_method_seq = p.seq
        join m_expedition_service ex on
        om.expedition_service_seq = ex.seq
        join m_expedition_service exd on 
        om.real_expedition_service_seq = exd.seq";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
        Select
			om.order_seq,
            o.order_date as tgl_order,
            o.order_no as no_order,
            o.payment_status,
            m.`name` as merchant_name,
            m.seq as merchant_seq,
            om.merchant_info_seq,
            o.pg_method_seq,
            p.`name` as payment_name,
            om.awb_no,
            om.ship_date,
            o.receiver_address,
            om.order_status,
            om.expedition_service_seq,
            om.real_expedition_service_seq,
            ex.`name` as expedition_name,
            exd.`name` as expedition_name_real
		From t_order_merchant om join t_order o on
        om.order_seq = o.seq 
        join m_merchant_info mi on
        om.merchant_info_seq = mi.seq
        join m_merchant m on 
        mi.merchant_seq = m.seq
        left outer join m_payment_gateway_method p on
		o.pg_method_seq = p.seq
        join m_expedition_service ex on
        om.expedition_service_seq = ex.seq
        join m_expedition_service exd on 
        om.real_expedition_service_seq = exd.seq
    ";

    If sqlWhere <> "" Then
		Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;
    
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END