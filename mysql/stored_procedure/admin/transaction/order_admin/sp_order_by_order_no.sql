DROP PROCEDURE IF EXISTS `sp_order_by_order_no`;
DELIMITER $$
CREATE PROCEDURE `sp_order_by_order_no` (
    pUserID Varchar(100),
    pIPAddress VarChar(25),
    pOrderNo VarChar(20)
)
BEGIN
    SELECT 
        o.order_no,
        o.order_date,
        o.payment_status,
        o.receiver_name,
        o.receiver_address,
        p.`name` AS province_name,
        c.`name` AS city_name,
        d.`name` AS district_name,
        o.receiver_phone_no,
        m.`name` AS member_name,
        m.email as member_email,
        a.`name` as agent_name,
        a.email as agent_email,
        pgm.name AS payment_name
    FROM
        t_order o
            JOIN
        m_district d ON o.receiver_district_seq = d.seq
            JOIN
        m_city c ON d.city_seq = c.seq
            JOIN
        m_province p ON c.province_seq = p.seq
            LEFT JOIN
        m_member m ON o.member_seq = m.seq
            LEFT JOIN
            m_agent a ON o.agent_seq = a.seq
        JOIN
        m_payment_gateway_method pgm ON o.pg_method_seq = pgm.seq
    WHERE
        order_no = pOrderNo;

    SELECT
            t.order_seq,
            t.merchant_info_seq,
            t.expedition_service_seq,
        e.`name` expedition_name,
        mm.`name` merchant_name,
        mm.email,
        mm.code as merchant_code,
        mm.seq merchant_seq,
            t.real_expedition_service_seq,    
            t.total_merchant,
            t.total_ins,
            t.total_ship_real,
            t.total_ship_charged,
            t.free_fee_seq,
            t.order_status,
            t.member_notes,
            t.printed,
            t.print_date,
            t.awb_seq,
            t.awb_no,
            t.ref_awb_no,
            t.ship_by,
            t.ship_by_exp_seq,
            t.ship_date,
            t.ship_note_file,
            t.ship_notes,
            t.received_date,
            t.received_by,
            t.redeem_seq,
            t.exp_invoice_seq,
            t.exp_invoice_awb_seq
    FROM 
            t_order_merchant t join m_merchant_info m 
                    on m.seq = t.merchant_info_seq 
                                    join m_district d
                    on d.seq = m.pickup_district_seq
                                    join m_city c
                    on c.seq = d.city_seq 
                                    join m_province p 
                    on p.seq = c.province_seq
                                    join m_merchant mm
                    on mm.seq = m.merchant_seq
                                    join m_expedition_service s
                    on s.seq =  t.expedition_service_seq
                                    join m_expedition e
                    on s.exp_seq = e.seq
                                    join t_order o
                    on o.seq = t.order_seq				
    WHERE
                    o.order_no = pOrderNo;

    SELECT 
            t.order_seq,
        mi.merchant_seq,
            t.merchant_info_seq,
            p.`name` as display_name,
        v.seq as product_seq,
        v.pic_1_img as img,
        mv.`value`,
            mv.seq as value_seq,
            mv.`value` as variant_name,
        t.product_status,
            t.qty,
            t.sell_price,
            t.weight_kg,
            t.ship_price_real,
            t.ship_price_charged,
            t.trx_fee_percent,
            t.ins_rate_percent,
            t.product_status,
            t.qty_return,
            t.created_by,
            t.created_date,
            t.modified_by,
            t.modified_date
    FROM
            t_order_product t 
                    Join m_product_variant v			
                            On v.seq = t.product_variant_seq
                    Join m_product p
                            On v.product_seq = p.seq
                    Join m_variant_value vv
                            On vv.seq = t.variant_value_seq
                    Join m_variant_value mv 
                            On mv.seq = v.variant_value_seq
                    Join t_order o
                            On o.seq = t.order_seq
                    Join m_merchant_info mi
                            On mi.seq = t.merchant_info_seq
                    join m_merchant mm 
                            On mm.seq = mi.merchant_seq
    WHERE
            o.order_no = pOrderNo;

    SELECT 
            o.seq,
            o.order_no,
            o.order_date,
            o.member_seq,
        o.signature,
        m.`name` member_name,
        m.email,
        m.mobile_phone,
        a.`name`,
        a.email,
        a.mobile_phone,
        c.`name` city_name,
        case when o.payment_retry <> '0'  Then concat(o.order_no, '-', cast(o.payment_retry as char(2))) Else o.order_no End order_trans_no,	
            o.receiver_name,
            o.receiver_address,
            o.receiver_district_seq,
        d.`name` district_name,
        c.`name` city_name,
        p.`name` province_name,
            o.receiver_zip_code,
            o.receiver_phone_no,
            o.payment_status,
            o.pg_method_seq,
        pg.`code` payment_code,
        pg.name payment_name,
            o.paid_date,
        case when pv.nominal is null then mc.nominal else pv.nominal end as nominal ,
        pv.code as voucher_code,
            o.payment_retry,
            o.total_order,
            o.voucher_seq,
            o.voucher_refunded,
            o.total_payment,
            o.conf_pay_type,
            o.conf_pay_amt_member,
            o.conf_pay_date,
            o.conf_pay_note_file,
            o.conf_pay_bank_seq,
            o.conf_pay_amt_admin
    FROM 
            t_order o JOIN m_district d
                        ON o.receiver_district_seq = d.seq
                    JOIN m_city c 
                        ON c.seq = d.city_seq 
                    JOIN m_province p
                        ON p.seq = c.province_seq
                    JOIN m_payment_gateway_method pg
                        ON pg.seq = o.pg_method_seq
                    LEFT JOIN m_promo_voucher pv 
                        ON o.voucher_seq = pv.seq
                    LEFT JOIN m_coupon mc 
                        on o.coupon_seq = mc.seq
                    LEFT JOIN m_member m
                        on m.seq = o.member_seq
                    LEFT JOIN m_agent a
                        on a.seq = o.agent_seq
            WHERE
                o.order_no = pOrderNo;
END$$