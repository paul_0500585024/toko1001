CREATE DEFINER=`root`@`%` PROCEDURE `sp_order_merchant_by_seq`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pOrderSeq mediumint unsigned
    )
BEGIN
select
	om.order_seq, 
    om.merchant_info_seq,
    om.expedition_service_seq,
    om.real_expedition_service_seq, 
    om.total_merchant, 
    om.total_ins, 
    om.total_ship_real,
    om.total_ship_charged, 
    om.free_fee_seq,
    om.order_status,
    om.member_notes,
    om.printed, 
    om.print_date,
    om.awb_seq, 
    om.awb_no, 
    om.ref_awb_no, 
    om.ship_by, 
    om.ship_by_exp_seq,
    om.ship_date, 
    om.ship_note_file,
    om.ship_notes,
    om.received_date, 
    om.received_by,
    om.redeem_seq,
    om.exp_invoice_seq,
    om.exp_invoice_awb_seq,
    mi.merchant_seq,
    m.email as email_merchant,
    m.`name` as merchant_name,
    m.seq as merchant_seq
    from
		t_order_merchant om
        join m_merchant_info mi on 
		om.merchant_info_seq = mi.seq
        join m_merchant m On
		mi.merchant_seq = m.seq
		where 
			order_seq = pOrderSeq;

END