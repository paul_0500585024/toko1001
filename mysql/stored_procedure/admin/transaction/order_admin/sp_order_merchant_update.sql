CREATE DEFINER=`root`@`%` PROCEDURE `sp_order_merchant_update`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pOrderSeq mediumint Unsigned,
    pOrderStatus char(1),
    pReceiveDate date,
    pMerchantId mediumint unsigned
    )
BEGIN
update t_order_merchant set
	order_status = pOrderStatus,
    received_date = pReceiveDate
Where
	order_seq = pOrderSeq AND
	merchant_info_seq = pMerchantId;
END