DROP PROCEDURE IF EXISTS `sp_get_info_order_loan_admin`;
DELIMITER $$
CREATE PROCEDURE `sp_get_info_order_loan_admin` (
    pIPAddress VARCHAR(25),
    pCreditSeq INTEGER
)
BEGIN

SELECT
    customer_seq,
    dp,
    admin_fee,
    tenor,
    loan_interest,
    installment,
    status_order
FROM
    t_order_loan
WHERE
    order_seq = pCreditSeq;
END$$

