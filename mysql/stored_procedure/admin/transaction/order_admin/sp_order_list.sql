CREATE DEFINER=`root`@`%` PROCEDURE `sp_order_list`(
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pCurrPage Int,
    pRecPerPage Int,
    pDirSort VarChar(4),
    pColumnSort Varchar(50),
    pOrderNo VARCHAR(50),
    pSDate Varchar(15),
    pEDate Varchar(15),
	pPaymentStatus char(1),
    pPgMethodSeq char(1)
)
BEGIN
    -- Begin variables for Paging
    Declare startRec Int;
    Declare totalPage Int;
    Declare totalRec Int;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
      
   If pOrderNo <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And o.order_no like '%" , escape_string(pOrderNo), "%'");
    End If;
    
	If pSDate <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And o.order_date >='" , pSDate , "'");
    End If;
	
    If pEDate <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And o.order_date <='" , pEDate , "'");
    End If;
    
    If pPaymentStatus <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And o.payment_status like '%" , pPaymentStatus, "%'");
    End If;
    
    If pPgMethodSeq <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And o.pg_method_seq like '%" , pPgMethodSeq, "%'");
    End If;
    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "Select Count(o.seq) Into @totalRec 
    		From t_order o join m_member m on m.seq = o.member_seq";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
        Select
            o.seq,
            o.order_no,
            o.order_date,
            o.member_seq,
            m.name as member_name,
            o.pg_method_seq,
            o.total_order,
            o.voucher_seq,
            o.total_payment,
            o.payment_status,
            o.conf_pay_amt_member,
            pg.`name` payment_name,
            pv.nominal as voucher_nominal
		From t_order o join m_member m on
        o.member_seq = m.seq
        join m_payment_gateway_method pg on
        o.pg_method_seq = pg.seq
        left join m_promo_voucher pv on
        o.voucher_seq = pv.seq

    ";

    If sqlWhere <> "" Then
		Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;
    
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END