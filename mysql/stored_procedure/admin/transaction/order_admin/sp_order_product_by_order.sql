DROP PROCEDURE IF EXISTS `sp_order_product_by_order`;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_order_product_by_order`(
            pUserID VarChar(25),
        pIPAddr VarChar(50),
        pOrderSeq BIGINT unsigned
    )
BEGIN
SELECT 
        op.merchant_info_seq,
        m.name AS merchant_name,
        p.`name` AS product_name,
        vv1.value AS variant_name,
        pv.variant_value_seq AS variant_seq,
        op.product_variant_seq,
        op.qty,
        o.order_no,
        o.seq AS order_seq,
        o.receiver_name,
        o.receiver_address,
        o.receiver_phone_no,
        d.name AS district_name,
        c.name AS city_name,
        mp.name AS province_name,
        op.sell_price,
        op.ship_price_real,
        op.ship_price_charged,
        op.weight_kg,
        op.ship_price_charged,
        pv.pic_1_img,
        m.seq AS merchant_seq,
        pv.seq AS product_varian_seq,
        vv2.value AS size_name,
        ps.variant_value_seq AS size_seq,
        (SELECT
        SUM(om.total_ship_charged) as ctotal_ship_charge
            FROM
                t_order o
                    JOIN
                t_order_product op ON o.seq = op.order_seq
                    JOIN
                t_order_merchant om ON om.order_seq = o.seq
                    AND om.merchant_info_seq = op.merchant_info_seq where om.order_seq = pOrderSeq ) as total_ship
            FROM
        t_order o
            JOIN
        t_order_product op ON o.seq = op.order_seq
            JOIN
        m_product_variant pv ON pv.seq = op.product_variant_seq
            JOIN
        m_product_stock ps ON ps.product_variant_seq = pv.seq
            AND op.variant_value_seq = ps.variant_value_seq
            JOIN
        m_variant_value vv1 ON vv1.seq = pv.variant_value_seq
            JOIN
        m_variant_value vv2 ON vv2.seq = ps.variant_value_seq
            JOIN
        t_order_merchant om ON om.order_seq = o.seq
            AND om.merchant_info_seq = op.merchant_info_seq
            JOIN
        m_product p ON p.seq = pv.product_seq
            JOIN
        m_merchant_info mi ON om.merchant_info_seq = mi.seq
            JOIN
        m_merchant m ON mi.merchant_seq = m.seq
            JOIN
        m_district d ON o.receiver_district_seq = d.seq
            JOIN
        m_city c ON d.city_seq = c.seq
            JOIN
        m_province mp ON c.province_seq = mp.seq
    WHERE
        o.seq = pOrderSeq ;
END$$