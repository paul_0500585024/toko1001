CREATE DEFINER=`root`@`%` PROCEDURE `sp_update_order_merchant`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pOrderSeq BigInt Unsigned,
    pMerchantSeq TinyInt Unsigned,
    pInvoiceSeq BigInt Unsigned,
    pAwbSeq BigInt Unsigned
    )
BEGIN

/*Declare pMerchantInfoSeq TinyInt Unsigned;*/


	Update 
		t_order_merchant
    Set 
		exp_invoice_seq = pInvoiceSeq, 
		exp_invoice_awb_seq = pAwbSeq
    Where 
		order_seq = pOrderSeq And merchant_info_seq In (select seq from m_merchant_info where merchant_seq = pMerchantSeq) ;

END