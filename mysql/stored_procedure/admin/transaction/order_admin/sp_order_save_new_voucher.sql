CREATE DEFINER=`root`@`%` PROCEDURE `sp_order_save_new_voucher`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pVoucherRefundSeq Bigint unsigned,
        pSeq  bigint unsigned
    )
BEGIN

update t_order set
    voucher_refund_seq = pVoucherRefundSeq
Where
	seq = pSeq ;
END