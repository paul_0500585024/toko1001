CREATE DEFINER=`root`@`%` PROCEDURE `sp_order_merchant_by_merchant`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
		pOrderSeq bigint(20) unsigned,
        pMerchant bigint(20) unsigned
    )
BEGIN
SELECT 
    o.order_no,
    pv.pic_1_img,
    p.name AS product_name,
    vv.value AS variant_name,
    m.seq AS merchant_seq,
    op.ship_price_charged,
    op.qty,
    op.sell_price,
    op.ship_price_real,
    op.weight_kg,
    m.name AS merchant_name,
    op.product_status,
    pv.seq product_variant_seq
FROM
    t_order_merchant om
        JOIN
    t_order o ON om.order_seq = o.seq
        JOIN
    t_order_product op ON om.merchant_info_seq = op.merchant_info_seq
        AND om.order_seq = op.order_seq
        JOIN
    m_product_variant pv ON op.product_variant_seq = pv.seq
        JOIN
    m_product p ON pv.product_seq = p.seq
        JOIN
    m_variant_value vv ON pv.variant_value_seq = vv.seq
        JOIN
    m_merchant_info mi ON om.merchant_info_seq = mi.seq
        JOIN
    m_merchant m ON mi.merchant_seq = m.seq
WHERE
    om.order_seq = pOrderSeq
        AND om.merchant_info_seq = pMerchant;
END