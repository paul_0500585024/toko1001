CREATE DEFINER=`root`@`%` PROCEDURE `sp_expedition_receipt_save_add_detail`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
	pInvoiceSeq BigInt(10),
    pAwbNo VarChar(50),
    pAwbDate DateTime,
    pDestination VarChar(100),
    pShipPrice Decimal(10),
    pInsPrice Decimal(10),
    pOrderSeq BigInt(10),
    pMerchantInfoSeq TinyInt Unsigned,
    pStatus Char(1)
    )
BEGIN

Declare pExpAwbSeq BigInt;
Declare new_seq BigInt;
	
	Select 
		Max(seq) + 1 Into new_seq
	From t_expedition_invoice_awb;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;

	Insert Into t_expedition_invoice_awb (
		invoice_seq,
		seq, 
		awb_no,
		awb_date, 
		destination, 
		ship_price, 
		ins_price,
		`status`,
		created_by,
		created_date,
		modified_by,
		modified_date
  )	Value (
		pInvoiceSeq,
		new_seq,
		pAwbNo,
		pAwbDate,
		pDestination,
		pShipPrice,
		pInsPrice,
		pStatus,
		pUserID,
		now(),
		pUserID,
		now()
);

Select 
	pInvoiceSeq,
	new_seq;
    

END