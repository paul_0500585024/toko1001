CREATE DEFINER=`root`@`%` PROCEDURE `sp_expedition_receipt_save_approve`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pExpSeq TinyInt Unsigned,
    pSeq MediumInt Unsigned
    )
BEGIN

Update t_expedition_invoice
Set
	`status` = 'V'
Where
	exp_seq = pExpSeq  And seq = pSeq;
        
END