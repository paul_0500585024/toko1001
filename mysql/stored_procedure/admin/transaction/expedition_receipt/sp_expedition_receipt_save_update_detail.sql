CREATE DEFINER=`root`@`%` PROCEDURE `sp_expedition_receipt_save_update_detail`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pInvoiceSeq BigInt(10),
	pSeq Int(10),
    pAwbNo VarChar(50),
    pAwbDate DateTime,
    pDestination VarChar(100),
    pShipPrice Decimal(10),
    pInsPrice Decimal(10),
    pOrderSeq BigInt(10),
    pMerchantSeq Int(10),
    pStatus Char(1)
)
BEGIN

	Select
		order_seq as old_order_seq,
		merchant_info_seq as old_merchant_info_seq    
	From 
		t_order_merchant
	Where
		exp_invoice_seq = pInvoiceSeq And
		exp_invoice_awb_seq = pSeq;
        

	Update t_expedition_invoice_awb set
		awb_no = pAwbNo,
		awb_date = pAwbDate,
		destination = pDestination,
		ship_price = pShipPrice,
		ins_price = pInsPrice,
		`status` = pStatus,
		modified_by = pUserID,
		modified_date = now()
	Where
		seq = pSeq;
	

END