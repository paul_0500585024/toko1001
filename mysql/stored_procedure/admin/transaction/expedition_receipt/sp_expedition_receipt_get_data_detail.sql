CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_expedition_receipt_get_data_detail`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
	pSeq Int(10)
)
BEGIN
	Select
		eia.invoice_seq,
        eia.seq,
        eia.awb_no,
        eia.awb_date,
        eia.destination,
        eia.ship_price,
        eia.ins_price,
        eia.`status`,
        eia.created_by,
        eia.created_date,
        eia.modified_by,
        eia.modified_date,
        o.seq as order_seq,
        o.order_no,
        mi.merchant_seq
	From t_expedition_invoice_awb eia
		Left Join t_order_merchant om On eia.invoice_seq = om.exp_invoice_seq  And eia.seq = om.exp_invoice_awb_seq
		Left Join t_order o On om.order_seq = o.seq
		Left Join m_merchant_info mi On om.merchant_info_seq = mi.seq
	Where
		eia.seq = pSeq;
END