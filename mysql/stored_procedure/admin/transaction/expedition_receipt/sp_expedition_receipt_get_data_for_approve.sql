CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_expedition_receipt_get_data_for_approve`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pExpSeq TinyInt Unsigned,
    pSeq MediumInt Unsigned
)
BEGIN
	
    Select
		ei.exp_seq,
		ei.seq,
		ei.`status` as exp_receipt_status,
        rp.`status` as redeem_status,
        eia.seq as awb_seq
	From 
		t_expedition_invoice ei
	Join t_redeem_period rp
		On ei.redeem_seq = rp.seq
	Left Join t_expedition_invoice_awb eia
		On ei.seq = eia.invoice_seq
	Where
		ei.exp_seq = pExpSeq And ei.seq = pSeq Limit 1;
    
END