CREATE DEFINER=`root`@`%` PROCEDURE `sp_expedition_receipt_check`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pSeq BigInt(10),
    pInvoiceSeq BigInt(10)
)
BEGIN

Update 
	t_expedition_invoice_awb eia Join t_order_merchant om On eia.awb_no = om.awb_no
Set
	om.exp_invoice_seq = eia.invoice_seq, 
	om.exp_invoice_awb_seq = eia.seq,
    eia.`status`= 'V'
Where
	eia.invoice_seq = pInvoiceSeq And eia.status = 'U';

END