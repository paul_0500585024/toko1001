CREATE DEFINER=`root`@`%` PROCEDURE `sp_expedition_receipt_detail_save_delete`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq Int(10)
    )
BEGIN

/*remove from t_order_merchant*/
	Update t_order_merchant om Join t_expedition_invoice_awb eia
		On om.exp_invoice_seq = eia.invoice_seq And om.exp_invoice_awb_seq = eia.seq
    Set
		exp_invoice_seq = Null,
        exp_invoice_awb_seq = Null
	Where
		eia.seq = pSeq;
    
/*then delete t_expedition_invoice_awb*/    
	Delete From t_expedition_invoice_awb 
		Where seq = pSeq;
    
    
END