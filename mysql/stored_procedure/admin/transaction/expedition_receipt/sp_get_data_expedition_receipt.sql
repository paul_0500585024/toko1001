CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_data_expedition_receipt`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pSeq Int(10)
)
BEGIN
	Select
		ei.exp_seq as expedition_seq,
        e.name as expedition_name,
        ei.seq,
        ei.invoice_no,
		CAST(Date_format(ei.invoice_date,'%d-%b-%Y')as Char(25)) as invoice_date,
        ei.redeem_seq as period_seq
	From t_expedition_invoice ei
		Join m_expedition e On ei.exp_seq = e.seq
	Where
		ei.seq = pSeq;
END