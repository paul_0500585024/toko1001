CREATE DEFINER=`root`@`%` PROCEDURE `sp_expedition_receipt_save_update`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pSeq BigInt(10),
    pExpeditionSeq Int(10),
    pInvoiceNo VarChar(25),
    pInvoiceDate DateTime,
    pPeriodSeq BigInt(25)
)
BEGIN
update t_expedition_invoice set
	exp_seq = pExpeditionSeq,
    invoice_no = pInvoiceNo,
    invoice_date = pInvoiceDate,
    redeem_seq = pPeriodSeq
Where
	seq = pSeq;
END