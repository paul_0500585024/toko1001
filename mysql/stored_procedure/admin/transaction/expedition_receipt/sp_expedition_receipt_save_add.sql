CREATE DEFINER=`root`@`%` PROCEDURE `sp_expedition_receipt_save_add`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
	pExpeditionSeq Int(10),
    pInvoiceNo VarChar(25),
    pInvoiceDate DateTime,
    pPeriodSeq BigInt(25),
    pStatus Char(1)
    )
BEGIN
Declare new_seq Int;
	
	Select 
		Max(seq) + 1 Into new_seq
	From t_expedition_invoice;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;

	Insert Into t_expedition_invoice (
		  exp_seq,
		  seq, 
		  invoice_no,
		  invoice_date, 
		  redeem_seq, 
          `status`, 
		  created_by, 
		  created_date,
		  modified_by,
		  modified_date
  )
	Value (
		pExpeditionSeq,
        new_seq,
        pInvoiceNo,
        pInvoiceDate,
        pPeriodSeq,
        pStatus,
        pUserID,
        now(),
        pUserID,
        now()
);

END