CREATE DEFINER=`root`@`%` PROCEDURE `sp_expedition_receipt_list_detail`(
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pCurrPage Int unsigned,
    pRecPerPage Int unsigned,
    pDirSort VarChar(4),
    pColumnSort Varchar(50),
    pHSeq Int(10)
)
BEGIN
    -- Begin variables for Paging
    Declare startRec Int unsigned;
    Declare totalPage Int unsigned;
    Declare totalRec Int unsigned;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(5000);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
    
    /*If pExpeditionSeq <> 0 Then
        Set sqlWhere = Concat(sqlWhere, " And ei.expedition_seq = '" , pExpeditionSeq, "'");
    End If;
    
	If pInvoiceNo <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And ei.invoice_no like '%" , pInvoiceNo, "%'");
    End If;
    
    If pStatus <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And ei.status = '" , pStatus, "'");
    End If;*/
    
    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "
	Select 
		Count(eia.invoice_seq) Into @totalRec 
	From t_expedition_invoice_awb eia
	Left Join t_expedition_invoice ei On 
		ei.seq = eia.invoice_seq				                
	Left Join t_order_merchant om On 
		om.exp_invoice_awb_seq = eia.seq And 
		om.exp_invoice_seq = ei.seq
	Left Join t_order o On
		om.order_seq = o.seq
	Left Join m_merchant_info mi On 
		om.merchant_info_seq = mi.seq
	Left Join m_merchant m On 
		mi.merchant_seq = m.seq";

    Set @sqlCommand = Concat(@sqlCommand, " Where eia.invoice_seq ='" ,pHSeq, "'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    
    /*If sqlWhere <> "" Then
        Set sqlWhere = Concat(sqlWhere, " Where eia.seq ='" , pHSeq , "'");
    End If;*/

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
	Select
		eia.invoice_seq,
		eia.seq,
		eia.awb_no,
		eia.awb_date,
		eia.destination,
		eia.ship_price,
		eia.ins_price,
		(eia.ship_price + eia.ins_price)total_price,
		eia.status,
		o.order_no,
		m.name as merchant_name
	From t_expedition_invoice_awb eia
	Left Join t_expedition_invoice ei On 
		ei.seq = eia.invoice_seq				                
	Left Join t_order_merchant om On
		om.exp_invoice_awb_seq = eia.seq and om.exp_invoice_seq = ei.seq
	Left Join t_order o On
		om.order_seq = o.seq
	Left Join m_merchant_info mi On
		om.merchant_info_seq = mi.seq
	Left Join m_merchant m On 
		mi.merchant_seq = m.seq";

	Set @sqlCommand = Concat(@sqlCommand, " Where eia.invoice_seq ='" ,pHSeq, "'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END