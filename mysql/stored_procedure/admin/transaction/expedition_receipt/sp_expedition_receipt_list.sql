CREATE DEFINER=`root`@`%` PROCEDURE `sp_expedition_receipt_list`(
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pCurrPage Int unsigned,
    pRecPerPage Int unsigned,
    pDirSort VarChar(4),
    pColumnSort Varchar(50),
    pExpeditionSeq TinyInt Unsigned,
    pInvoiceNo VarChar(50),
    pStatus Char(1)
)
BEGIN
    -- Begin variables for Paging
    Declare startRec Int unsigned;
    Declare totalPage Int unsigned;
    Declare totalRec Int unsigned;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(5000);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
    
    If pExpeditionSeq <> 0 Then
        Set sqlWhere = Concat(sqlWhere, " And ei.exp_seq = '" , pExpeditionSeq, "'");
    End If;
    
	If pInvoiceNo <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And ei.invoice_no like '%" , escape_string(pInvoiceNo), "%'");
    End If;
    
    If pStatus <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And ei.status = '" , pStatus, "'");
    End If;
    
    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "
	Select 
		Count(ei.seq) Into @totalRec 
	From t_expedition_invoice ei 
	Join m_expedition e On 
		ei.exp_seq = e.seq
	Left Join (
		Select
			invoice_seq, 
			Sum(ship_price + ins_price) total_amt
		From t_expedition_invoice_awb
		Group By 
			invoice_seq
	) Qry on 
		qry.invoice_seq = ei.seq";
                            
	If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
	Select
		ei.exp_seq as expedition_seq,
		ei.seq,
		e.name as expedition_name,
		ei.invoice_no,
		qry.total_amt,
		Cast(Date_format(ei.invoice_date,'%d-%b-%Y')as Char(25)) as invoice_date,
		ei.status,
		ei.created_by,
		ei.created_date,
		ei.modified_by,
		ei.modified_date
		From t_expedition_invoice ei 
		Join m_expedition e On 
			ei.exp_seq = e.seq
		Left Join (
			Select
				invoice_seq, 
				Sum(ship_price + ins_price) total_amt
			From t_expedition_invoice_awb
			Group By 
				invoice_seq
		) Qry On 
			qry.invoice_seq = ei.seq";

	If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;
    
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END