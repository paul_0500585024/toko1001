CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_data_awb_no`(
	pAwbNo VarChar(50)
)
BEGIN

Select 
	eia.awb_no,
    ei.exp_seq
From 
	t_expedition_invoice_awb eia
Join t_expedition_invoice ei 
	On eia.invoice_seq = ei.seq
Where awb_no = pAwbNo;

END