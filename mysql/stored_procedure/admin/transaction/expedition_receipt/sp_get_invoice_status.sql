CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_invoice_status`(
	pSeq Int(10)
)
BEGIN
Select
	exp_seq,
	`status`
From t_expedition_invoice
Where
	seq = pSeq;
    
    
END