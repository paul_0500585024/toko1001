CREATE DEFINER=`root`@`%` PROCEDURE `sp_expedition_order_update`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pOrderSeq smallint Unsigned,
    pRealExpeditionServiceSeq mediumint unsigned,
    pMerchantId mediumint unsigned
    )
BEGIN
update t_order_merchant set
	real_expedition_service_seq = pRealExpeditionServiceSeq
Where
	order_seq = pOrderSeq AND
	merchant_info_seq = pMerchantId;
END