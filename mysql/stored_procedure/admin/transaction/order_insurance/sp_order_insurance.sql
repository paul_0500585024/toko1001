DROP PROCEDURE IF EXISTS `sp_order_insurance`;
DELIMITER $$
CREATE PROCEDURE `sp_order_insurance` (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pCurrPage INT,
    pRecPerPage INT,
    pDirSort VARCHAR(4),
    pColumnSort VARCHAR(50),
    pOrderNo VARCHAR(50),
    pDateRegStart DATE,
    pDateRegEnd DATE,
    pMemberName VARCHAR(100),
    pPhoneNumber VARCHAR(50),
    pEmailAddress VARCHAR(50),
    pGender VARCHAR(1)
)
BEGIN

    -- Begin variables for Paging
    DECLARE startRec INT;
    DECLARE totalPage INT;
    DECLARE totalRec INT;
    -- End variables for Paging
    -- Begin SQL Where
    DECLARE sqlWhere VARCHAR(500);
	SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    SET sqlWhere = "";
      
    IF pOrderNo <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And to.order_no like '%" , escape_string(pOrderNo), "%'");
    END IF;
    
    IF pDateRegStart <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And DATE(toi.created_date) >='" , pDateRegStart , "'");
    END IF;
	
    IF pDateRegEnd <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And DATE(toi.created_date) <='" , pDateRegEnd , "'");
    END IF;
    
    IF pMemberName <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And toi.member_name like '" , pMemberName, "%'");
    END IF;
    
    IF pPhoneNumber <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And toi.member_phone like '" , pPhoneNumber, "%'");
    END IF;
    
    IF pEmailAddress <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And toi.member_email like '" , pEmailAddress, "%'");
    END IF;
    IF pGender <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And toi.gender = '" , pGender, "'");
    END IF;
    SET sqlWhere = Concat(sqlWhere, " AND member_email <> ''");
    -- End SQL Where
    -- Begin Paging Info
    SET @sqlCommand = "Select Count(*) Into @totalRec 
    		From t_order_insurance `toi` LEFT JOIN t_order `to` ON `toi`.`order_seq`=`to`.seq";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        END IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    -- End Paging Info
    SET @sqlCommand = "
    SELECT `toi`.order_seq,`to`.order_no, to.total_order, `toi`.`created_date`,`toi`.member_name,`toi`.`member_birthday`, `toi`.member_phone,`toi`.member_email,`toi`.`gender` 
    FROM t_order_insurance `toi`
    LEFT JOIN t_order `to` ON `toi`.`order_seq`=`to`.seq";
    IF sqlWhere <> "" THEN
	SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    
    SET @sqlCommand = CONCAT(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    
    SET @sqlCommand = CONCAT(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @totalRec = NULL;
    SET @sqlCommand = NULL;

END$$

