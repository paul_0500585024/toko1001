CREATE DEFINER=`root`@`%` PROCEDURE `sp_change_password`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pNewPassword VarChar(1000)
)
BEGIN
	Update m_user
	Set `password` = pNewPassword
	Where user_id = pUserID;
        
End