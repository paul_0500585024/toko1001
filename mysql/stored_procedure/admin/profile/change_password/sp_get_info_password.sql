CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_info_password`(
	pUserID Varchar(50),
    pIPAddr VarChar(50),
    pOldPassword Varchar(1000),
    pNewPassword Varchar(1000),
    pEncryptOldPassword Varchar(1000),
    pEncryptNewPassword Varchar(1000)
)
BEGIN
SELECT
	`password`
FROM m_user	
WHERE user_id = pUserID AND `password` = pEncryptOldPassword;

END