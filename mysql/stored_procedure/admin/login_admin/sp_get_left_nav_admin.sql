CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_left_nav_admin`(
	pUserID VarChar(25)
)
BEGIN

Select
	m.menu_cd,
    case when m.parent_menu_cd is null then 'ROOT' else parent_menu_cd end as parent_menu_cd,
    m.title_name `name`,
    m.url,
    m.`order`,
    m.icon,
    m.detail
From 
	s_menu m join m_user_group_permission ug
		on ug.menu_cd = m.menu_cd
			 join m_user u 
		on u.user_group_seq = ug.user_group_seq
Where 
	u.user_id = pUserID and m.active = '1' and m.detail = '0'
Order by `order`;

END