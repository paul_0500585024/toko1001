CREATE DEFINER=`root`@`%` PROCEDURE `sp_login_admin`(
    pUserID VarChar(25),
    pPassword VarChar(1000),
    pIPAddress Varchar(50)
)
BEGIN

Declare new_seq Int;

select 
	u.user_id, 
    u.user_name,
    u.password, 
    u.user_group_seq,
    u.last_login,
    g.name 
from m_user u Join m_user_group g
		On g.seq = u.user_group_seq
where 
	u.user_id= pUserID and 
    u.password = pPassword 
    and u.active ='1';

Select 
	m.name as menu_name,
    m.title_name,
	up.menu_cd,
    up.can_add,
    up.can_edit,
    up.can_view,
    up.can_delete,
    up.can_print,
    up.can_auth
from 
	m_user u join m_user_group_permission up
		on up.user_group_seq = u.user_group_seq
			 join s_menu m
		on m.menu_cd = up.menu_cd
Where
	u.user_id = pUserID;    
    
Select 
	Max(seq) + 1 Into new_seq
From m_user_login_log;

If new_seq Is Null Then
	Set new_seq = 1;
End If;    
    
if exists (select user_id from m_user where user_id= pUserID and `password` = pPassword) Then
Begin
    Update m_user set
		last_login = now(),
        ip_address = pIPAddress
	where
		user_id = pUserID;

	insert into m_user_login_log (
		seq,
        user_id,
        ip_address,
        status,
        created_date
	) values (
		new_seq,
        pUserID,
        pIPAddress,
        'S',
        now()
	);
End;
Else
	insert into m_user_login_log (
		seq,
        user_id,
        ip_address,
        status,
        created_date
	) values (
		new_seq,
        pUserID,
        pIPAddress,
        'F',
        now()
	);
End if;	    

END