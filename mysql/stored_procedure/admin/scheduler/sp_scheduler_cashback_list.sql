DROP PROCEDURE IF EXISTS `sp_scheduler_cashback_list`;
DELIMITER $$
CREATE PROCEDURE `sp_scheduler_cashback_list` (
    pUserID VARCHAR(50),
    pIpAdress VARCHAR(50),
    pOrderStatus CHAR(1),
    pSettled CHAR(1),
    pProductStatus CHAR(1)
)
BEGIN
SELECT 
    op.order_seq,
    o.order_no,
    o.agent_seq,
    o.order_date,
    op.product_variant_seq,
    op.variant_value_seq,
    op.commission_fee_percent,
    op.sell_price,
    op.qty,
    op.settled,
    op.product_status,
    om.order_status,
    a.name AS agent_name,
    a.email AS agent_email,
    a.bank_name AS agent_bank_name,
    a.bank_branch_name AS agent_bank_branch_name,
    a.bank_acct_no AS agent_bank_acct_no,
    a.bank_acct_name AS agent_bank_acct_name,
    p.name as product_name,
    op.nominal_commision_agent,
	(select sum(cop.nominal_commision_agent * cop.qty)  FROM
            t_order_product cop
				join
			t_order_merchant com on com.order_seq = cop.order_seq 
					and com.merchant_info_seq = cop.merchant_info_seq
        WHERE
            cop.order_seq = o.seq 
				AND  op.product_status = cop.product_status 
					AND op.settled = cop.settled ) AS total_new
FROM
    t_order_product op
        JOIN
    t_order_merchant om ON op.order_seq = om.order_seq AND op.merchant_info_seq = om.merchant_info_seq
        JOIN
    t_order o ON op.order_seq = o.seq
        JOIN
    m_agent a ON o.agent_seq = a.seq
        JOIN
    m_product_variant pv ON pv.seq = op.product_variant_seq
        JOIN
    m_product p ON p.seq = pv.product_seq
WHERE
    om.order_status = pOrderStatus AND op.settled = pSettled
        AND op.product_status = pProductStatus;
END$$
