CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_rate_expedition_list`()
BEGIN

if Exists(select exp_seq from t_expedition_rate_cache where scheduler = '0') then
Select 
	t.exp_seq,
    t.from_district_code,
    t.to_district_code,
    t.exp_service_seq,
    t.rate,
    t.scheduler,
    e.code as exp_code,
    es.code as service_code
from
	t_expedition_rate_cache t join m_expedition e on
			e.seq = t.exp_seq
							  join m_expedition_service es on
			es.seq = t.exp_service_seq
where	
	scheduler = '0' limit 10;    	
    
else
	Begin
		update t_expedition_rate_cache set
			scheduler = '0';
		
        Select 
			t.exp_seq,
			t.from_district_code,
			t.to_district_code,
			t.exp_service_seq,
			t.rate,
			t.scheduler,
			e.code as exp_code,
			es.code as service_code
		from
			t_expedition_rate_cache t join m_expedition e on
					e.seq = t.exp_seq
									  join m_expedition_service es on
					es.seq = t.exp_service_seq
		where	
			scheduler = '0' limit 10; 
	end;
end if;
END