DROP PROCEDURE IF EXISTS `sp_scheduler_cashback_product_update`;
DELIMITER $$
CREATE PROCEDURE `sp_scheduler_cashback_product_update` (
       pOrderSeq BIGINT(20) Unsigned,
       pProductVariantSeq BIGINT(20) Unsigned,
       pSettled CHAR(1)
)
BEGIN
UPDATE t_order_product 
SET 
    settled = pSettled
WHERE
    order_seq = pOrderSeq
        AND product_variant_seq = pProductVariantSeq;
END$$
