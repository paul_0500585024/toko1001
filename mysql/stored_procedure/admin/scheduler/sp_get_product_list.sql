CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_product_list`()
BEGIN

select 
	v.seq,
    v.variant_value_seq,
    vv.value,
    p.name,
    p.auth_date,
    c.parent_seq
from 
	m_product_variant v join m_product p
		on p.seq = v.product_seq
						join m_variant_value vv
		on vv.seq = v.variant_value_seq
						join m_product_category c
		on c.seq = v.category_l2_seq
where
	v.active = '1';

END