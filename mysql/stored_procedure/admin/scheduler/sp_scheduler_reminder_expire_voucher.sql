DROP PROCEDURE IF EXISTS `sp_reminder_expire_voucher`;
DELIMITER $$
CREATE PROCEDURE `sp_reminder_expire_voucher` (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50)
)
BEGIN
SELECT
m_m.email,
m_m.name,
m_a.email AS email_agent,
m_a.name AS name_agent,
mpv.code,
mpv.exp_date
FROM m_promo_voucher mpv
LEFT JOIN m_member m_m ON mpv.member_seq=m_m.seq
LEFT JOIN m_agent m_a ON mpv.member_seq=m_a.seq
WHERE trx_no = '' 
AND (member_seq IS NOT NULL OR agent_seq IS NOT NULL)
AND DATEDIFF(DATE(NOW()),mpv.exp_date) = -3;
END$$


