DROP PROCEDURE IF EXISTS `sp_reminder_payment_order`;
DELIMITER $$
CREATE PROCEDURE `sp_reminder_payment_order` (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50)
)
BEGIN
SELECT
  m_m.email,
  m_m.name,
  m_a.email AS email_agent,
  m_a.name AS name_agent, 
  t_o.order_no,
  t_o.`order_date`,
  t_o.`receiver_name`, 
  t_o.`receiver_address`, 
  t_o.`total_order`,
  GROUP_CONCAT(mp.name SEPARATOR '<br>') AS order_items
FROM t_order t_o
LEFT JOIN m_member m_m ON m_m.seq=t_o.member_seq
LEFT JOIN m_agent m_a ON m_a.seq=t_o.agent_seq
JOIN t_order_product top ON top.order_seq = t_o.seq
JOIN m_product_variant mpv ON top.product_variant_seq=mpv.seq
JOIN m_product mp ON mpv.product_seq=mp.seq
WHERE payment_status= "U" OR payment_status= "F" OR payment_status= "W"
GROUP BY 
  t_o.order_no;
END$$


