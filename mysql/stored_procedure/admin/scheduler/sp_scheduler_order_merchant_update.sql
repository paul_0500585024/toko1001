CREATE DEFINER = 'root'@'%' PROCEDURE `sp_scheduler_order_merchant_update`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq BigInt unsigned,
        pMerchantInfoSeq Int unsigned,
        pStatus CHAR(1),
        pShipDate DATE,
        pReceiveDate DATETIME,
        pReceiveBy VARCHAR(50)
    )
BEGIN
	Update t_order_merchant Set
    	order_status = pStatus ,
    	ship_date = pShipDate ,
    	received_date = pReceiveDate ,
    	received_by = pReceiveBy ,
		modified_by = pUserID ,
		modified_date = now()
	Where
		order_seq = pSeq And 
		merchant_info_seq = pMerchantInfoSeq;

END;