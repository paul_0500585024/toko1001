CREATE DEFINER=`root`@`%` PROCEDURE `sp_order_list_cancel`(
    pUserID VarChar(25),
    pIPAddr VarChar(50)
)
BEGIN
Select
		o.seq,
		o.order_no,
		o.order_date,
		o.member_seq,
		m.name as member_name,
        m.email as email_member,
		o.pg_method_seq,
		o.total_order,
		o.voucher_seq,
		o.total_payment,
		o.payment_status,
		o.conf_pay_amt_member,
		pg.`name` payment_name,
		pv.nominal as voucher_nominal
From t_order o join m_member m on
	o.member_seq = m.seq
join m_payment_gateway_method pg on
	o.pg_method_seq = pg.seq
left join m_promo_voucher pv on
			o.voucher_seq = pv.seq
where order_date < DATE_SUB(now(),INTERVAL 2 DAY) AND (payment_status = 'U' or payment_status = 'F');
END