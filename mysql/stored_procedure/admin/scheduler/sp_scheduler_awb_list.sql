DROP PROCEDURE `sp_scheduler_awb_list`;
CREATE DEFINER = 'root'@'localhost' PROCEDURE `sp_scheduler_awb_list`(
        pUserID VarChar(50),
        pIPAddr VarChar(50),
        pCode VARCHAR(250)
    )
BEGIN
	Declare sqlWhere VarChar(500);
	Set @sqlCommand = "
	SELECT tom.`order_seq`,tom.`merchant_info_seq`,tom.`awb_no`,mes.`exp_seq`,mex.`code` FROM
	`t_order_merchant`  tom
	join m_expedition_service mes
	on tom.`real_expedition_service_seq`=mes.`seq`
	join (select seq, code from `m_expedition`) mex
	on mes.`exp_seq`=mex.`seq`
	Where tom.`awb_no` <>'' and ( tom.`order_status`='R' or tom.`order_status`='S') and tom.`real_expedition_service_seq`<> 0 ";    
    Set sqlWhere = Concat(@sqlCommand," and
	mex.`code` in (", pCode, ")");
    
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @sqlCommand = Null;
        
END;