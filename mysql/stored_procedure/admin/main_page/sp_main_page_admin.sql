CREATE DEFINER=`root`@`%` PROCEDURE `sp_main_page_admin`(
        pPaymentStatus Char(1),
        pTrxType Char(3),
        pWithdrawStatus Char(1),
        pNewMerchant Char(1),
        pProductStatus Char(1),
        pMerchantStatus Char(1),
        pMemberStatus Char(1)
    )
BEGIN
    
    Select Count(seq) as `order` From t_order Where payment_status = pPaymentStatus;
    Select Count(seq) as withdraw From t_member_account Where trx_type = pTrxType And `status` = pWithdrawStatus;
    Select Count(seq) as new_merchant From m_merchant_new Where `status`= pNewMerchant;
    Select Count(seq) as product From m_product_new Where `status` = pProductStatus;
    Select Count(seq) as total_merchant From m_merchant Where `status` = pMerchantStatus;
    Select Count(seq) as total_member From m_member Where `status` = pMemberStatus;
    
END