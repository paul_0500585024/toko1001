CREATE DEFINER=`root`@`%` PROCEDURE `sp_expedition_rate_cache_update`(
    pUserID Varchar(25),
    pIPAddress Varchar(25),
    pExpSeq Tinyint unsigned,
    pFromDistrictCode Varchar(50),
    pToDistrictCode VarChar(50),
    pExpServiceSeq Tinyint unsigned,
    pRate Decimal(10,0)
)
BEGIN

update t_expedition_rate_cache set
    rate = pRate,
    scheduler = '1',
    modified_by = pUserId,
    modified_date = now()
where
	exp_seq = pExpSeq and
    from_district_code = pFromDistrictCode and
    to_district_code = pToDistrictCode and
    exp_service_seq = pExpServiceSeq;



END