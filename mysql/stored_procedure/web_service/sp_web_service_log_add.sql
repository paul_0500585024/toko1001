CREATE DEFINER=`root`@`%` PROCEDURE `sp_web_service_log_add`(
	pUserID Varchar(25),
    pIPAddress Varchar(25),
    pFunctionName VarChar(50),
    pType char(4),
	pRequestDateTime Datetime
)
BEGIN

    Declare new_seq Int;

	SELECT 
		MAX(seq) + 1
	INTO new_seq  FROM
		t_webservice_log;

    If new_seq Is Null Then
        Set new_seq = 1;
    End If;

	insert into t_webservice_log (
			seq,
            function_name,
			type,
            request,
            response,
			datetime_request,
            datetime_response
	) Values (
			new_seq,
            pFunctionName,
            pType,
			'',
            '',
            pRequestDatetime,
            null
	);
    
    select new_seq;
    
END