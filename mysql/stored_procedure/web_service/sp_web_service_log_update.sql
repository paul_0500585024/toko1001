CREATE DEFINER=`root`@`%` PROCEDURE `sp_web_service_log_update`(
	
    pUserID Varchar(25),
    pIPAddress VarChar(25),
    pWebServiceseq MediumInt,
    pRequest TEXT,
    pResponse TEXT,
    pResponseDateTime Datetime    
)
BEGIN

update t_webservice_log set
	request = pRequest,
	response = pResponse,
    datetime_response = pResponseDateTime
where
	seq = pWebServiceSeq;

END