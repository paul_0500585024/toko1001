DROP PROCEDURE IF EXISTS `sp_change_password_partner`;
DELIMITER $$
CREATE PROCEDURE `sp_change_password_partner`(
	pUserID VARCHAR(25), 
	pIPAddr VARCHAR(50),
    pPartnerSeq TINYINT UNSIGNED,
    pPassword VARCHAR(1000)
)
BEGIN  
	UPDATE
		m_partner
    SET 
		`password` = pPassword,
        modified_by = pUserID,
        modified_date = NOW()
    WHERE seq = pPartnerSeq;
    
END$$