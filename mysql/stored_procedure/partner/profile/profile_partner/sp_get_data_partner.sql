DROP PROCEDURE IF EXISTS `sp_get_data_partner`;
DELIMITER $$
CREATE PROCEDURE `sp_get_data_partner`(
	pIPAddr VARCHAR(50),
    pPartnerSeq BIGINT(10)
)
BEGIN
SELECT
  `seq`,
  `email`,
  `name`,
  `password`,
  `mobile_phone`,
  `profile_img`,
  `status`,
  `bank_name`,
  `bank_branch_name`,
  `bank_acct_no`,
  `bank_acct_name`
FROM 
	m_partner
WHERE 
	seq = pPartnerSeq;    
END$$