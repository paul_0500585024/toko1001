DROP PROCEDURE IF EXISTS `sp_partner_log_security_check`;
DELIMITER $$
CREATE PROCEDURE `sp_partner_log_security_check`(
	pUri VARCHAR(50)
)
BEGIN
	SELECT
		CODE
	FROM m_partner_log_security
	WHERE 
		CODE = pUri;
END$$