DROP PROCEDURE IF EXISTS `sp_partner_log_security_add`;
DELIMITER $$
CREATE PROCEDURE `sp_partner_log_security_add`(
	pUserID VARCHAR(25),
	pIPAddr VARCHAR(50),
	pMseq INT UNSIGNED,
	pType CHAR(1),
	pCode CHAR(20),
	pOldPass VARCHAR(100)
)
BEGIN
	DECLARE new_seq SMALLINT UNSIGNED;
	SELECT 
		MAX(seq) + 1 INTO new_seq
	FROM m_partner_log_security;
	IF new_seq IS NULL THEN
		SET new_seq = 1;
	END IF;
    
	INSERT INTO `m_partner_log_security`(  
		partner_seq,
		seq,
		`type`,
        CODE,
		old_pass,
		ip_addr,
		created_by,
		created_date
	) VALUES (
		pMseq,
		new_seq,
		pType,
        pCode,
		MD5(MD5(pOldPass)),
		pIPAddr ,
		pUserID,
		NOW()
	);
END$$