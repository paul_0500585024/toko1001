DROP PROCEDURE IF EXISTS `sp_partner_email_check`;
DELIMITER $$
CREATE PROCEDURE `sp_partner_email_check`(
	pEmail VARCHAR(50)
)
BEGIN
	SELECT
		seq,
        NAME,
        PASSWORD
	FROM m_partner 
	WHERE 
		email = pEmail;
END$$