DROP PROCEDURE IF EXISTS `sp_email_log_by_code`;
DELIMITER $$
CREATE PROCEDURE `sp_email_log_by_code`(
        pUri VARCHAR(50),
        pEmailCd VARCHAR(50)
    )
BEGIN
SELECT 
  recipient_email,
  recipient_name,
  created_date
FROM 
  `t_email_log` 
WHERE `code` = pUri AND
	   email_cd = pEmailCD;
        
END$$