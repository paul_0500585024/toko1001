DROP PROCEDURE IF EXISTS `sp_partner_update_password_by_email`;
DELIMITER $$
CREATE PROCEDURE `sp_partner_update_password_by_email`(
	pEmail VARCHAR(50),
    pNewpass VARCHAR(100)
)
BEGIN
	UPDATE m_partner SET 
		PASSWORD = pNewpass 
	WHERE 
		email = pEmail;
END$$