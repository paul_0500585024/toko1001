DROP PROCEDURE IF EXISTS `sp_partner_log_data_by_type`;
DELIMITER $$
CREATE PROCEDURE `sp_partner_log_data_by_type`(
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pPartnerSeq INT UNSIGNED
)
BEGIN
SELECT
    seq AS partner_seq,
    'L' AS STATUS,
    `email`,
    `name`,
    `password`,
    `mobile_phone` AS `phone_no`,
    `profile_img` AS `pic1_name`,
    `status`,
    `bank_name`,
    `bank_branch_name`,
    `bank_acct_no`,
    `bank_acct_name`,
    `last_login`,
    `ip_address`,
    `created_by`,
    `created_date`,
    `modified_by`,
    `modified_date`
FROM m_partner m
WHERE seq = pPartnerSeq;
END$$