DROP PROCEDURE IF EXISTS `sp_partner_log_data_update`;
DELIMITER $$
CREATE PROCEDURE `sp_partner_log_data_update`(
	pUserID VARCHAR(25),
	pIPAddr VARCHAR(50), 
	pPartnerSeq SMALLINT UNSIGNED,
	pPhoneNo VARCHAR(50),
	pBankName VARCHAR(50),
	pBankBranchName VARCHAR(50),
	pBankAcctNo VARCHAR(50),
	pBankAcctName VARCHAR(50)
)
BEGIN
    UPDATE `toko1001_dev`.`m_partner`
        SET 
          `mobile_phone` = pPhoneNo,
          `bank_name` = pBankName,
          `bank_branch_name` = pBankBranchName,
          `bank_acct_no` = pBankAcctNo,
          `bank_acct_name` = pBankAcctName
        WHERE `seq` = pPartnerSeq;
END$$