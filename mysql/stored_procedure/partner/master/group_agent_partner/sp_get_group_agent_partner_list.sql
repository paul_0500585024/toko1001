DROP PROCEDURE IF EXISTS `sp_get_group_agent_partner_list`;
DELIMITER $$
CREATE PROCEDURE `sp_get_group_agent_partner_list` (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
-- for paging
    pCurrPage INT UNSIGNED,
    pRecPerPage INT UNSIGNED,
    pDirSort VARCHAR(4),
    pColumnSort VARCHAR(50),
-- for authentication
    pPartnerSeq INT,
-- for search
    pAgentGroupname varchar(500)
)
BEGIN
    -- Begin variables for Paging
    DECLARE startRec INT UNSIGNED;
    DECLARE totalPage INT UNSIGNED;
    DECLARE totalRec INT UNSIGNED;
    -- End variables for Paging

    -- BEGIN SQL WHERE  >> 
    DECLARE sqlWhere VARCHAR(500);
	SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    SET sqlWhere = "";

    IF pAgentGroupname <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And agent_group_name like '%", pAgentGroupname ,"%'");
    END IF;

    SET sqlWhere = CONCAT(sqlWhere, " AND partner_seq = '", pPartnerSeq, "'");
    -- END SQL WHERE

    -- Begin Paging Info
    SET @sqlCommand = "Select Count(*) Into @totalRec FROM m_agent_group ";

    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;

    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        END IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    -- End Paging Info
    SET @sqlCommand = "
        Select		
	partner_seq,
        agent_group_seq,
        agent_group_name,
        commission_fee_percent,
        created_by,
        created_date,
        modified_by,
        modified_date 
        from
        m_agent_group ";
    
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;

    SET @sqlCommand = CONCAT(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    SET @sqlCommand = CONCAT(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @totalRec = NULL;
    SET @sqlCommand = NULL;
END$$
DELIMITER ;

