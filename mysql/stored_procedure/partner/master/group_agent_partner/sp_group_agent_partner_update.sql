DROP PROCEDURE IF EXISTS `sp_group_agent_partner_update`;
DELIMITER $$
CREATE PROCEDURE `sp_group_agent_partner_update` (
    pUserID VARCHAR(50),
    pIpAddress VARCHAR(25),
    pPartnerSeq INT unsigned,
    pAgentGroupSeq INT unsigned,
    pAgentGroupName VARCHAR(500),
    pCommissionFeePercent decimal(4,2)
)
BEGIN
UPDATE m_agent_group
SET
`agent_group_name` = pAgentGroupName,
`commission_fee_percent` = pCommissionFeePercent,
`modified_by` = pUserID,
`modified_date` = now()
WHERE `agent_group_seq` = pAgentGroupSeq AND partner_seq = pPartnerSeq;
END$$

