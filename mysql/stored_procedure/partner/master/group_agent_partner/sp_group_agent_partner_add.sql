DROP PROCEDURE IF EXISTS `sp_group_agent_partner_add`;
DELIMITER $$
CREATE PROCEDURE `sp_group_agent_partner_add` (
    pUserID varchar(25),
    pIpAddress varchar(25),
    pPartnerSeq INT unsigned,
    pAgentGroupName varchar(500),
    pCommissionFeePercent decimal(4,2)
)
BEGIN

declare newSeq INT unsigned;

select 
    max(agent_group_seq) + 1 into newSeq 
from 
    m_agent_group 
where 
    partner_seq = pPartnerSeq;

if newSeq is null then 
    set newSeq = 1;
end if;

INSERT INTO m_agent_group
    (partner_seq,
    agent_group_seq,
    agent_group_name,
    commission_fee_percent,
    created_by,
    created_date,
    modified_by,
    modified_date
)VALUES(
    pPartnerSeq,
    newSeq,
    pAgentGroupName,
    pCommissionFeePercent,
    pUserID,
    now(),
    pUserID,
    now()
);
END$$

