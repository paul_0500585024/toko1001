DROP PROCEDURE IF EXISTS `sp_group_agent_partner_delete`;
DELIMITER $$
CREATE PROCEDURE `sp_group_agent_partner_delete` (
    pUserID VARCHAR(50),
    pIPAddr VARCHAR(50),
    pPartnerSeq INT UNSIGNED,
    pSeq INT UNSIGNED
)
BEGIN

    DELETE FROM m_agent_group WHERE agent_group_seq = pSeq AND partner_seq = pPartnerSeq;

END$$

