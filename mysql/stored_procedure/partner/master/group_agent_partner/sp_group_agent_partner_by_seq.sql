DROP PROCEDURE IF EXISTS `sp_group_agent_partner_by_seq`;
DELIMITER $$
CREATE PROCEDURE `sp_group_agent_partner_by_seq` (
    pUserID VARCHAR(50),
    pIPAddr VARCHAR(50),
    pPartnerSeq INT(10) UNSIGNED,
    pSeq INT(10) UNSIGNED
)
BEGIN
SELECT 
    partner_seq,
    agent_group_seq,
    agent_group_name,
    commission_fee_percent,
    created_by,
    created_date,
    modified_by,
    modified_date
FROM
    m_agent_group
WHERE
    agent_group_seq = pSeq
        AND partner_seq = pPartnerSeq;
END$$

