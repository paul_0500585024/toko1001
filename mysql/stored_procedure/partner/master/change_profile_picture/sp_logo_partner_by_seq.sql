DROP PROCEDURE IF EXISTS `sp_logo_partner_by_seq`;
DELIMITER $$
CREATE PROCEDURE `sp_logo_partner_by_seq`(
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pSeq BIGINT UNSIGNED
)
BEGIN
    SELECT
        seq,
        profile_img,
        `name`
    FROM 
        m_partner
    WHERE
        seq = pSeq;
END$$