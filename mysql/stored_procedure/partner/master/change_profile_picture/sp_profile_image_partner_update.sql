DROP PROCEDURE IF EXISTS `sp_profile_image_partner_update`;
DELIMITER $$
CREATE PROCEDURE `sp_profile_image_partner_update`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pSeq BIGINT UNSIGNED,
        pName VARCHAR(100),
        pProfileImg VARCHAR(50)
    )
BEGIN
UPDATE m_partner SET 
	`name` = pName,
	profile_img = pProfileImg,
	modified_by = pUserID,
	modified_date = NOW() 
WHERE seq = pSeq;
END$$