DROP PROCEDURE IF EXISTS `sp_check_agent_exist`;
DELIMITER $$
CREATE PROCEDURE `sp_check_agent_exist`(
        `pEmail` VARCHAR(50)
    )
BEGIN
SELECT * FROM m_agent WHERE email = pEmail;
END$$