DROP PROCEDURE IF EXISTS `sp_agent_by_seq`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_by_seq`(
        pUserID VARCHAR(50),
        pIPAddr VARCHAR(50),
        pSeq INT(10) UNSIGNED
    )
BEGIN
SELECT
  `seq`,
  `email`,
  `password`,
  `name`,
  `partner_seq`,
  `referral`,
  `birthday`,
  `gender`,
   status,
  `mobile_phone`,
  `profile_img`,
  `deposit_amt`,
  `last_login`,
  `ip_address`,
  `created_by`,
  `created_date`,
  `modified_by`,
  `modified_date`,
    bank_name,
    bank_branch_name,
    bank_acct_no,
    bank_acct_name,
   commission_pro_rate
FROM `m_agent`
WHERE 
   seq = pSeq;
END$$