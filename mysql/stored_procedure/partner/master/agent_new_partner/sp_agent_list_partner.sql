DROP PROCEDURE IF EXISTS `sp_agent_list_partner`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_list_partner`(
        `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pCurrPage` INTEGER UNSIGNED,
        `pRecPerPage` INTEGER UNSIGNED,
        `pDirSort` VARCHAR(4),
        `pColumnSort` VARCHAR(50),
        `pEmail` VARCHAR(100),
        `pName` VARCHAR(100),
        `pStatus` CHAR(3),
        `pPartnerSeq` INTEGER(10)
    )
BEGIN
    
    DECLARE startRec INT UNSIGNED;
    DECLARE totalPage INT UNSIGNED;
    DECLARE totalRec INT UNSIGNED;
    
    
    DECLARE sqlWhere VARCHAR(500);
	SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    SET sqlWhere = "";
      
    IF pEmail <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And email like '%" , escape_string(pEmail), "%'");
    END IF;
    
    IF pName <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And name like '%" , escape_string(pName), "%'");
    END IF;
    
    IF pStatus <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And status = '" , pStatus, "'");
    END IF;

    IF pPartnerSeq <> "" THEN
         SET sqlWhere = CONCAT(sqlWhere, " And partner_seq = '" , pPartnerSeq, "'" );
     END IF;

    SET @sqlCommand = "Select Count(seq) Into @totalRec From m_agent ";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        END IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    
    SET @sqlCommand = "
        Select
			seq as agent_seq,
            email,
            `name`,
            birthday,
            gender,
            mobile_phone,
            profile_img,
            deposit_amt,
            status,
            created_by,
            created_date,
            modified_by,
            modified_date,
            bank_name
		From m_agent
    ";
    IF sqlWhere <> "" THEN
		SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    
    SET @sqlCommand = CONCAT(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    SET @sqlCommand = CONCAT(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @totalRec = NULL;
    SET @sqlCommand = NULL;
END$$