DROP PROCEDURE IF EXISTS `sp_agent_update`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_update`(
        `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pName` VARCHAR(100),
        `pDate` DATE,
        `pGender` VARCHAR(1),
        `pPhone` VARCHAR(20),
        `pPassword` VARCHAR(100),
        `pImage` VARCHAR(100),
        `pPartnerSeq` INTEGER(10),
        `pStatus` CHAR(1),
        `pBankName` VARCHAR(50),
        `pBankBranchName` VARCHAR(50),
        `pBankAcctNo` VARCHAR(50),
        `pBankAcctName` VARCHAR(50),
	`pSeq` INTEGER(10),
        `pCommisionProRate` DECIMAL(3,0)
    )
BEGIN
if pPassword <> ''
then
	update `m_agent`
	set 
	  `password` = pPassword,
	  `name` = pName,
	  `birthday` = pDate,
	  `partner_seq` = pPartnerSeq,
	  `gender` = pGender,
	  `mobile_phone` = pPhone,
	  `profile_img` = pImage,
	  `bank_name` = pBankName,
	  `bank_branch_name` = pBankBranchName,
	  `bank_acct_no` = pBankAcctNo,
	  `bank_acct_name` = pBankAcctName,
	  `status` = pStatus,
	  `modified_by` = pUserID,
	  `modified_date` = NOW(),
          `commission_pro_rate` = pCommisionProRate
	where `seq` = pSeq;
else
	UPDATE `m_agent`
	SET 
	  `name` = pName,
	  `birthday` = pDate,
	  `partner_seq` = pPartnerSeq,
	  `gender` = pGender,
	  `mobile_phone` = pPhone,
	  `profile_img` = pImage,
	  `bank_name` = pBankName,
	  `bank_branch_name` = pBankBranchName,
	  `bank_acct_no` = pBankAcctNo,
	  `bank_acct_name` = pBankAcctName,
	  `status` = pStatus,
	  `modified_by` = pUserID,
	  `modified_date` = NOW(),
          `commission_pro_rate` = pCommisionProRate
	WHERE `seq` = pSeq;	
end if;    
END$$
DELIMITER ;


