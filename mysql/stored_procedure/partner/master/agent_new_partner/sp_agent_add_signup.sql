DROP PROCEDURE IF EXISTS `sp_agent_add_signup`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_add_signup`(
        `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pEmail` VARCHAR(100),
        `pName` VARCHAR(100),
        `pDate` DATE,
        `pGender` VARCHAR(1),
        `pPhone` VARCHAR(20),
        `pPassword` VARCHAR(100),
        `pImage` VARCHAR(100),
        `pPartnerSeq` INTEGER(10)
    )
BEGIN
	DECLARE new_seq INT UNSIGNED;
	SELECT 
        MAX(seq) + 1 INTO new_seq
    FROM m_agent;
    IF new_seq IS NULL THEN
        SET new_seq = 1;
    END IF;
    
	INSERT INTO m_agent(
		seq,
		email,
		PASSWORD,
		`name`,
                partner_seq,
		birthday,
		gender,
		mobile_phone,
		profile_img,
		deposit_amt,
		`status`,
		created_by,
		created_date,
		modified_by,
		modified_date
	) VALUES (
		new_seq,
		pEmail,
		pPassword,
		pName,
                pPartnerSeq,
		pDate,
		pGender,
		pPhone,
		pImage,
		0,
		'A',
		'SYSTEM',
		NOW(),
		'SYSTEM',
		NOW()
	);
END$$