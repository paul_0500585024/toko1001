DROP PROCEDURE IF EXISTS `sp_login_partner`;
DELIMITER $$
CREATE PROCEDURE `sp_login_partner`(
    pUserID VARCHAR(100),
    pPassword VARCHAR(100),
    pIPAddress VARCHAR(50)
)
BEGIN
DECLARE new_seq INT;
DECLARE partner_seq INT;
DECLARE user_group_seq INT;
SELECT MAX(seq) + 1 INTO new_seq FROM m_partner_login_log;
IF new_seq IS NULL THEN SET new_seq = 1; END IF;
SELECT seq INTO user_group_seq FROM m_user_group WHERE `name`='PARTNER';
SELECT 
    u.seq,
    u.email,
    u.`name` AS user_name,
    u.password,
    user_group_seq,
    u.last_login,
    g.`name`,
    u.profile_img
FROM m_partner u
JOIN m_user_group g ON g.seq=user_group_seq
WHERE u.email = pUserID;
		
SELECT 
    m.`name` AS menu_name,
    m.title_name,
    up.menu_cd,
    up.can_add,
    up.can_edit,
    up.can_view,
    up.can_delete,
    up.can_print,
    up.can_auth,
    mp.menu_cd AS parent_menu_cd,
    mp.title_name AS parent_menu_name,
    mp.url AS parent_menu_url,
    m.detail
FROM m_partner u 
JOIN m_user_group_permission up ON up.user_group_seq = user_group_seq
JOIN s_menu m ON m.menu_cd = up.menu_cd 
LEFT JOIN s_menu mp ON m.parent_menu_cd = mp.menu_cd
WHERE u.email = pUserID;
IF EXISTS (SELECT seq FROM m_partner WHERE email = pUserID AND `password` = pPassword) THEN
BEGIN 
    UPDATE m_partner 
    SET last_login = NOW(), ip_address = pIPAddress
    WHERE email = pUserID;
    SELECT seq INTO partner_seq 
    FROM m_partner
    WHERE email = pUserID;
    
    INSERT INTO m_partner_login_log (
        `seq`,
        `partner_seq`,
        `ip_address`,
        `status`,
        `created_date`
    ) VALUES (
        new_seq,
        partner_seq,
        pIPAddress,
        'S',
        NOW()
    );
END;
ELSE
    IF EXISTS(SELECT seq FROM m_partner WHERE email = pUserID) THEN
    BEGIN
        SELECT seq INTO partner_seq 
        FROM m_partner
        WHERE email = pUserID;
        INSERT INTO m_partner_login_log (
            seq,
            partner_seq,
            ip_address,
            `status`,
            created_date
        ) VALUES (
            new_seq,
            partner_seq,
            pIPAddress,
            'F',
            NOW()
        );
    END;
    END IF;
END IF;
    
END$$