DROP PROCEDURE IF EXISTS `sp_get_password_partner`;
DELIMITER $$
CREATE PROCEDURE `sp_get_password_partner`(
    pPartnerSeq TINYINT UNSIGNED
)
BEGIN
	SELECT
		seq,
		email,
		`password`,
		`name`
	FROM m_partner
	WHERE seq = pPartnerSeq;
END$$