DROP PROCEDURE IF EXISTS `sp_get_left_nav_partner`;
DELIMITER $$
CREATE PROCEDURE `sp_get_left_nav_partner`(
    pUserID VARCHAR(50)
)
BEGIN
DECLARE user_group_seq INT;
SELECT seq INTO user_group_seq FROM m_user_group WHERE `name`='PARTNER';
SELECT
	m.menu_cd,
    CASE WHEN m.parent_menu_cd IS NULL THEN 'ROOT' ELSE parent_menu_cd END AS parent_menu_cd,
    m.title_name `name`,
    m.url,
    m.`order`,
    m.icon,
    m.detail
FROM 
	s_menu m JOIN m_user_group_permission ug
		ON ug.menu_cd = m.menu_cd
			 JOIN m_partner u 
		ON ug.user_group_seq = user_group_seq
WHERE 
	u.email = pUserID AND m.active = '1' AND m.detail = '0'
ORDER BY `order`;
END$$
