DROP PROCEDURE IF EXISTS `sp_partner_change_password_log`;
DELIMITER $$
CREATE PROCEDURE `sp_partner_change_password_log`(
	pUserID VARCHAR(25),
	pIPAddr VARCHAR(50),
	pPartnerSeq INT UNSIGNED,
	pOldPassword VARCHAR(1000)
)
BEGIN
	DECLARE new_seq INT UNSIGNED;
	SELECT 
		MAX(seq) + 1 INTO new_seq
	FROM m_partner_log_security;
	IF new_seq IS NULL THEN
		SET new_seq = 1;
	END IF;
    
	INSERT INTO `m_partner_log_security`(  
		partner_seq,
		seq,
		`type`,
        CODE,
		old_pass,
		ip_addr,
		created_by,
		created_date
	) VALUES (
		pPartnerSeq,
		new_seq,
		'1',
        '',
		pOldPassword,
		pIPAddr ,
		pUserID,
		NOW()
	);
END$$