DROP PROCEDURE IF EXISTS `sp_set_redeem_partner_to_agent`;
DELIMITER $$
CREATE PROCEDURE `sp_set_redeem_partner_to_agent` (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pRedeemSeq INTEGER(10) UNSIGNED,
    pAgentSeq INTEGER(10) UNSIGNED,
    pPayDate DATE,
    pStatus char(1)
)
BEGIN

UPDATE
    t_redeem_agent
SET
    paid_date = pPayDate,
    status = 'P'
WHERE
    redeem_seq = pRedeemSeq AND status = pStatus AND agent_seq = pAgentSeq;
END$$

