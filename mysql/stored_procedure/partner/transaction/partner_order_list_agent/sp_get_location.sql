DROP PROCEDURE IF EXISTS `sp_get_location`;
DELIMITER $$
CREATE PROCEDURE `sp_get_location`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pSeq INT,
        cSeq INT,
        dSeq INT
    )
BEGIN
    -- Begin SQL Where
    DECLARE sqlWhere VARCHAR(500);
    SET sqlWhere = "";
    SET @sqlCommand = "
        Select
		  *
        From `v_location`
    ";
	IF pSeq <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " and p_id=" , pSeq);
    END IF;
    IF cSeq <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " and c_id=" , cSeq);
    END IF;
    IF dSeq <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " and d_id=" , dSeq);
    END IF;
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
END$$