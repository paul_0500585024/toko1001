DROP PROCEDURE IF EXISTS `sp_get_dropdown_expedition`;
DELIMITER $$
CREATE PROCEDURE `sp_get_dropdown_expedition`(
)
BEGIN
SELECT 
	seq,
    `code`,
	`name`
FROM 
	m_expedition
WHERE
	active = '1'
ORDER BY 
	`name`;
END$$
