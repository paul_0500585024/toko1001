CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_partner_order_by_status`(      
        IN `pUserID` VARCHAR(20),      
        IN `pIpAddress` VARCHAR(20),
        IN `pPgMethodSeq` VARCHAR(20),
        IN `pPartnerSeq` INT,
        IN `pPaymentStatus` VARCHAR(2)
    )
BEGIN
	Set @sqlCommand = Concat("select t_o.*, m_a.name from t_order t_o join (select order_seq from t_order_loan where status_order='N') t_ol on t_o.seq=t_ol.order_seq join (select * from m_agent where partner_seq=",pPartnerSeq,") m_a on t_o.agent_seq=m_a.seq where t_o.pg_method_seq in (",pPgMethodSeq,") and t_o.payment_status='",pPaymentStatus,"' order by created_date");
Prepare sql_stmt FROM @sqlCommand;
Execute sql_stmt;
Deallocate prepare sql_stmt;    

END