DROP PROCEDURE IF EXISTS `sp_partner_order_list_agent`;
DELIMITER $$
CREATE PROCEDURE `sp_partner_order_list_agent`(
        `pUserID` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pCurrPage` INTEGER UNSIGNED,
        `pRecPerPage` INTEGER UNSIGNED,
        `pDirSort` VARCHAR(4),
        `pColumnSort` VARCHAR(50),
        `pPartnerId` INTEGER(9) UNSIGNED,
        `pOrderNo` VARCHAR(50),
        `pDates` VARCHAR(10),
        `pDatee` VARCHAR(10),
        `pPaymentStatus` CHAR(1),
        `pOrderStatus` CHAR(1)
    )
BEGIN
    -- Begin variables for Paging
    DECLARE startRec INT UNSIGNED;
    DECLARE totalPage INT UNSIGNED;
    DECLARE totalRec INT UNSIGNED;
    -- End variables for Paging
    -- Begin SQL Where
    DECLARE sqlWhere VARCHAR(800);
    DECLARE sqlWhereC VARCHAR(800);
	SET sqlWhere = " WHERE 1";
	SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    IF pOrderNo <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And t_o.order_no like '%" , escape_string(pOrderNo), "%'");
    END IF;    
    IF pDates <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And t_o.order_date >= '" , escape_string(pDates), "'");
    END IF;
    IF pDatee <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And t_o.order_date <= '" , escape_string(pDatee), "'");
    END IF;
    IF pPartnerId <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And t_o.agent_seq in (select seq from m_agent where partner_seq='" , pPartnerId, "')");
    END IF;
    SET sqlWhereC =sqlWhere;
    IF pPaymentStatus <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And t_o.payment_status='" , pPaymentStatus, "')");
    END IF;    
    SET sqlWhereC =sqlWhere;
    IF pOrderStatus <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And t_om.order_status = '" , escape_string(pOrderStatus), "'");
    END IF;
    -- End SQL Where
    -- Begin Paging Info
    SET @sqlCommand = "Select Count(t_o.seq) Into @totalRec From `t_order` t_o inner join `t_order_merchant` t_om on t_o.`seq`=t_om.`order_seq` Left Outer Join `m_district` md ON t_o.`receiver_district_seq` = md.seq
     left outer join (select order_seq,merchant_info_seq,IFNULL(sum(sell_price*qty),0) as total_merchant from t_order_product 
        where product_status='R' group by order_seq,merchant_info_seq)
         t_op on t_om.`order_seq`=t_op.order_seq and t_om.`merchant_info_seq`=t_op.merchant_info_seq";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, sqlWhere);
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        END IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    -- End Paging Info
    SET @sqlCommand = "Select 
  t_o.`seq`,
  t_o.`order_no`,
  t_o.`order_date`,
  t_o.`member_seq`,
  t_o.`agent_seq`,
  t_o.`receiver_name`,
  t_o.`receiver_address`,
  t_o.`receiver_district_seq`,
  t_o.`receiver_zip_code`,
  t_o.`receiver_phone_no`,
  t_o.`payment_status`,
  t_o.`pg_method_seq`,
  t_o.`paid_date`,
  t_o.`payment_retry`,
  t_o.`total_order`,
  t_o.`voucher_seq`,
  t_o.`voucher_refunded`,
  t_o.`voucher_refund_seq`,
  t_o.`coupon_seq`,
  t_o.`promo_credit_seq`,
  t_o.`total_payment`,
  t_o.`conf_pay_type`,
  t_o.`conf_pay_amt_member`,
  t_o.`conf_pay_date`,
  t_o.`conf_pay_note_file`,
  t_o.`conf_pay_bank_seq`,
  t_o.`conf_pay_amt_admin`,
  t_o.`signature`,
  t_o.`payment_code`,
  t_o.`created_by`,
  t_o.`created_date`,
  t_o.`modified_by`,
  t_o.`modified_date`,
  t_om.`order_seq`,
  t_om.`merchant_info_seq`,
  t_om.`expedition_service_seq`,
  t_om.`real_expedition_service_seq`,
  t_om.`total_merchant`,
  t_om.`total_ins`,
  t_om.`total_ship_real`,
  t_om.`total_ship_charged`,
  t_om.`free_fee_seq`,
  t_om.`order_status`,
  t_om.`member_notes`,
  t_om.`printed`,
  t_om.`print_date`,
  t_om.`awb_seq`,
  t_om.`awb_no`,
  t_om.`ref_awb_no`,
  t_om.`ship_by`,
  t_om.`ship_by_exp_seq`,
  t_om.`ship_date`,
  t_om.`ship_note_file`,
  t_om.`ship_notes`,
  t_om.`received_date`,
  t_om.`received_by`,
  t_om.`redeem_seq`,
  t_om.`redeem_agent_seq`,
  t_om.`exp_invoice_seq`,
  t_om.`exp_invoice_awb_seq`,
  t_om.`created_by`,
  t_om.`created_date`,
  t_om.`modified_by`,
  t_om.`modified_date`,
  md.`name` districtname,
  t_op.`total_merchant`
     From `t_order` t_o inner join `t_order_merchant` t_om on t_o.`seq`=t_om.`order_seq` Left Outer Join `m_district` md ON t_o.`receiver_district_seq` = md.seq
     left outer join (select order_seq,merchant_info_seq,IFNULL(sum(sell_price*qty),0) as total_merchant from t_order_product 
        where product_status='R' group by order_seq,merchant_info_seq)
         t_op on t_om.`order_seq`=t_op.order_seq and t_om.`merchant_info_seq`=t_op.merchant_info_seq";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, sqlWhere);
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    SET @sqlCommand = CONCAT(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt; 
    
    -- Begin Qty Status
    SET @sqlCommand = " SELECT COUNT(t_om.order_status) AS qty_status ,t_om.order_status FROM `t_order_merchant` t_om join `t_order` t_o on t_om.`order_seq`=t_o.`seq`";
    IF sqlWhereC <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, sqlWhereC);
        SET @sqlCommand = CONCAT(@sqlCommand, " and t_om.order_status<> 'P'");
    END IF;
	SET @sqlCommand = CONCAT(@sqlCommand, " GROUP BY t_om.order_status");
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    
    SET @totalRec = NULL;
    SET @sqlCommand = NULL;
END$$