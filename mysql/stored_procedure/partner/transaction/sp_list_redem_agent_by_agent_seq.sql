DROP PROCEDURE IF EXISTS `sp_list_redem_agent_by_agent_seq`;
DELIMITER $$
CREATE PROCEDURE `sp_list_redem_agent_by_agent_seq` (
  pUserID VARCHAR(25),
  pIPAddr VARCHAR(50),
  pPartnerSeq INTEGER(10) UNSIGNED,
  pRedeemAgentSeq INTEGER(10) UNSIGNED,
  pCurrPage INTEGER,
  pRecPerPage INTEGER,
  pDirSort VARCHAR(20),
  pColumnSort VARCHAR(50)
)
BEGIN
   
DECLARE startRec INT;
    DECLARE totalPage INT;
    DECLARE totalRec INT;
    
    
    DECLARE sqlWhere VARCHAR(500);
	SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    SET sqlWhere = "";

--     IF pPartnerSeq <> 0 THEN 
--         SET sqlWhere = CONCAT(sqlWhere, " And partner_seq= '" , pPartnerSeq, "'");
--     END IF;

    IF pRedeemAgentSeq <> 0 THEN 
        SET sqlWhere = CONCAT(sqlWhere, " And ra.redeem_seq= '" , pRedeemAgentSeq, "'");
    ELSE
        SET sqlWhere = CONCAT(sqlWhere, " And ra.redeem_seq= 'XXX'");
    END IF;    
    
    
    SET @sqlCommand = "SELECT COUNT(*) INTO @totalRec FROM t_redeem_agent ra LEFT JOIN m_agent a ON ra.agent_seq = a.seq 
        LEFT JOIN t_redeem_agent_period tap  ON tap.seq = ra.redeem_seq ";
    --SET @sqlCommand = CONCAT(@sqlCommand, " INNER JOIN t_redeem_agent_period trap ON trap.seq=",pRedeemAgentSeq);
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    -- SET @sqlCommand = CONCAT(@sqlCommand, " GROUP BY agent_seq");
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        END IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    
    SET @sqlCommand = "SELECT 
        ra. agent_seq,
        a.`name`,
        ra.total,
        ra.status,
        ra.paid_date,
        tap.from_date,
        tap.to_date
    FROM 
        t_redeem_agent ra LEFT JOIN m_agent a ON ra.agent_seq = a.seq
    LEFT JOIN t_redeem_agent_period tap  ON tap.seq = ra.redeem_seq";
    -- SET @sqlCommand = CONCAT(@sqlCommand, " INNER JOIN t_redeem_agent_period trap ON trap.seq=",pRedeemAgentSeq);
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1 " , sqlWhere);
    END IF;
    -- SET @sqlCommand = CONCAT(@sqlCommand, " GROUP BY agent_seq");
    SET @sqlCommand = CONCAT(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    SET @sqlCommand = CONCAT(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @totalRec = NULL;
    SET @sqlCommand = NULL;

END$$

