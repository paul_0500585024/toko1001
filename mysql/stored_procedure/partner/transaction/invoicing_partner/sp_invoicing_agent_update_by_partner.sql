DROP PROCEDURE IF EXISTS sp_invoicing_agent_update_by_partner;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_agent_update_by_partner (
    `pUserID` VARCHAR(25),
    `pIPAddr` VARCHAR(50),
    `pSeq` INTEGER(10) UNSIGNED,
    `pPartnerSeq` INTEGER(10) UNSIGNED,
    `pPaiddate` DATE
)
BEGIN
    UPDATE t_order_loan tol JOIN t_order `to` ON tol.`order_seq` = `to`.seq JOIN m_agent ma ON `to`.`agent_seq` = ma.`seq`
    SET tol.status_order = 'D', tol.`paid_date` = pPaiddate
    WHERE tol.status_order = 'P' AND tol.`collect_seq` = pSeq AND ma.partner_seq = pPartnerSeq;
END$$
