DROP PROCEDURE IF EXISTS sp_invoicing_partner_list_by_partner_seq;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_partner_list_by_partner_seq (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pCurrPage INTEGER,
    pRecPerPage INTEGER,
    pColumnSort VARCHAR(50),
    pDirSort VARCHAR(4),
    pPartnerSeq INT(10),
    pFDate DATE,
    pTDate DATE,
    pStatus CHAR(1)
)
BEGIN
    DECLARE startRec INT;
    DECLARE totalPage INT;
    DECLARE totalRec INT;
    DECLARE sqlWhere VARCHAR(500);
    DECLARE status_order CHAR(1);
    DECLARE paid_date DATE;
    SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    SET sqlWhere = "";
    IF pFdate <> "" AND pTdate <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And ((c.from_date BETWEEN '",pFdate,"' AND '",pTdate,"') OR (c.to_date BETWEEN '",pFdate,"' AND '",pTdate,"'))");
    END IF;
    IF pStatus <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And d.status = '",pStatus,"'");
    END IF;
    IF pPartnerSeq <> "" OR pPartnerSeq <> 0 THEN
        SET sqlWhere = CONCAT(sqlWhere, 
        " And a.partner_seq = '" , pPartnerSeq, "'");
    END IF;
    SET @sqlCommand = CONCAT("Select Count(*) Into @totalRec 
FROM `t_collecting_component` a 
JOIN `t_collecting_period` c ON a.collect_seq = c.seq 
JOIN `m_partner` b ON a.`partner_seq`=b.`seq`
JOIN (SELECT k.collect_seq, a.partner_seq, k.paid_date, k.status_order AS status
FROM t_order_loan k 
JOIN t_order l ON k.order_seq = l.seq
JOIN m_agent a ON a.seq = l.agent_seq 
WHERE a.partner_seq = ",pPartnerSeq," GROUP BY collect_seq, a.partner_seq) d ON a.collect_seq = d.collect_seq AND a.partner_seq = d.partner_seq");
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        END IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    
    SET @sqlCommand = CONCAT("
SELECT
    a.`collect_seq`,
    a.`partner_seq`,
    d.paid_date,
    d.status,
    b.`name`,
    b.email,
    b.mobile_phone,
    b.bank_name,
    b.bank_branch_name,
    b.bank_acct_no,
    b.bank_acct_name,
    b.profile_img,
    c.from_date,
    c.to_date,
    SUM(CASE WHEN a.`mutation_type`= 'D' THEN a.`total` ELSE a.`total` * -1 END) AS total
FROM `t_collecting_component` a
JOIN `t_collecting_period` c ON a.collect_seq = c.seq 
JOIN `m_partner` b ON a.`partner_seq`=b.`seq`
JOIN (SELECT k.collect_seq, a.partner_seq, k.paid_date, k.status_order AS status
FROM t_order_loan k 
JOIN t_order l ON k.order_seq = l.seq
JOIN m_agent a ON a.seq = l.agent_seq WHERE a.partner_seq = ",pPartnerSeq," GROUP BY collect_seq, a.partner_seq) d ON a.collect_seq = d.collect_seq AND a.partner_seq = d.partner_seq
");
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
        SET @sqlCommand = CONCAT(@sqlCommand, " GROUP BY a.collect_seq,a.partner_seq");
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    SET @sqlCommand = CONCAT(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @totalRec = NULL;
    SET @sqlCommand = NULL;
END$$