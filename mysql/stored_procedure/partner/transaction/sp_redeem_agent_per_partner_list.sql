DROP PROCEDURE IF EXISTS `sp_redeem_agent_per_partner_list`;
DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_redeem_agent_per_partner_list`(
        `pUserID` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pPartnerSeq` INTEGER(10) UNSIGNED,
        `pCurrPage` INTEGER,
        `pRecPerPage` INTEGER,
        `pDirSort` VARCHAR(20),
        `pColumnSort` VARCHAR(50),
        `pFdate` VARCHAR(10),
        `pTdate` VARCHAR(10),
        `pStatus` CHAR(1),
        `pPaidDateMonth` TINYINT UNSIGNED,
        `pPaidDateYear` INT UNSIGNED
)
BEGIN
 
    DECLARE startRec INT;
    DECLARE totalPage INT;
    DECLARE totalRec INT;
    
    
    DECLARE sqlWhere VARCHAR(500);
	SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    SET sqlWhere = "";
    
    IF pFdate <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And trap.from_date >= '" , pFdate, "'");
    END IF;
    
    IF pTdate <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And trap.to_date <= '" , pTdate, "'");
    END IF;
    
    IF pStatus <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And tra.status= '" , pStatus, "'");
    END IF;
    
    IF pPartnerSeq <> 0 THEN 
        SET sqlWhere = CONCAT(sqlWhere, " And ma.partner_seq= '" , pPartnerSeq, "'");
    END IF;
    
    IF pPaidDateMonth <> 0 THEN 
        SET sqlWhere = CONCAT(sqlWhere, " And MONTH(tra.paid_partner_date) = '" , pPaidDateMonth, "'");
    END IF;
    
    IF pPaidDateYear <> 0 THEN 
        SET sqlWhere = CONCAT(sqlWhere, " And YEAR(tra.paid_partner_date) = '" , pPaidDateYear, "'");
    END IF;    
    
    
    
    SET @sqlCommand = "SELECT COUNT(DISTINCT(tra.redeem_seq)) Into @totalRec FROM t_redeem_agent tra INNER JOIN t_redeem_agent_period trap ON tra.`redeem_seq`=trap.`seq` INNER JOIN m_agent ma ON ma.`seq`=tra.`agent_seq`";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1 AND tra.status <> 'U'" , sqlWhere);
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        END IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    
    
    SET @sqlCommand = "SELECT trap.`seq`, trap.`from_date`, trap.`to_date`, SUM(tra.`total`) AS total_commission_sum, ma.commission_pro_rate";
    IF pPartnerSeq = 1 THEN
        SET @sqlCommand = CONCAT(@sqlCommand,", tra.`paid_partner_date` AS paid_date");
    ELSE
        SET @sqlCommand = CONCAT(@sqlCommand,", tra.`paid_date` AS paid_date");
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand,", (SELECT SUM(total) FROM t_redeem_agent_component trac WHERE trac.redeem_seq=trap.`seq` AND partner_seq = ",pPartnerSeq," AND mutation_type='C') AS total_commission");
    SET @sqlCommand = CONCAT(@sqlCommand,", (SELECT SUM(`tor`.`total_payment`) as total FROM
    t_order tor JOIN `m_agent` ma ON `tor`.`agent_seq`=ma.`seq`
    WHERE 
	tor.seq IN (SELECT top.order_seq FROM  `t_order_product` top
    JOIN `t_order_merchant` tom ON `top`.`order_seq`=tom.`order_seq` AND top.`merchant_info_seq`=tom.`merchant_info_seq`
	WHERE tom.order_status='D' AND top.`product_status`='R' AND tom.redeem_agent_seq=trap.`seq` ) AND tor.member_seq IS NULL ) AS total_order");
    SET @sqlCommand = CONCAT(@sqlCommand, " FROM t_redeem_agent tra INNER JOIN t_redeem_agent_period trap ON tra.`redeem_seq`=trap.`seq` INNER JOIN m_agent ma ON ma.`seq`=tra.`agent_seq`");
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1 AND tra.status <> 'U'" , sqlWhere);        
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand, " GROUP BY redeem_seq");
    IF pPartnerSeq = 1 THEN
        SET @sqlCommand = CONCAT(@sqlCommand,", tra.`paid_partner_date`");
    ELSE
        SET @sqlCommand = CONCAT(@sqlCommand,", tra.`paid_date`");
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    SET @sqlCommand = CONCAT(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @totalRec = NULL;
    SET @sqlCommand = NULL;         
END$$

