DROP PROCEDURE IF EXISTS `sp_order_voucher_add`;
DELIMITER $$
CREATE PROCEDURE `sp_order_voucher_add` (
    pUserID Varchar(25),
    pOrderNo Varchar(25),
    pPhoneNo Varchar(25),
    pProviderNominalSeq TinyInt unsigned,
    pPgMethodSeq Tinyint unsigned,
    pPaymentStatus Char(1),
    pOrderStatus Char(1),
    pStatus varchar(7)
)

BEGIN
declare new_seq SmallInt Unsigned;
declare pTotalOrder Decimal(12,0);
declare pTotalPayment Decimal(12,0);

SELECT 
    MAX(seq) + 1
INTO new_seq FROM
    t_order_voucher;

If new_seq Is Null Then
	Set new_seq = 1;
End If;


SELECT 
    sell_price
INTO pTotalOrder FROM
    m_provider_service_nominal
WHERE
    seq = pProviderNominalSeq;

set pTotalPayment = pTotalOrder;

IF pStatus = 'member' THEN

insert into t_order_voucher(
	seq,
    order_no,
    order_date,
    member_seq,
    phone_no,
    pg_method_seq,
    provider_nominal_seq,
    total_payment,
    total_order,
    payment_status,
    order_status,
    created_by,
    created_date,
    modified_by,
    modified_date
) values (
	new_seq,
    pOrderNo,
    CURDATE(),
    pUserID,
    pPhoneNo,
    pPGMethodSeq,
    pProviderNominalSeq,
    pTotalPayment,
    pTotalOrder,
    pPaymentStatus,
    pOrderStatus,
    pUserID,
    now(),
    pUserID,
    now()
);
ELSE 
insert into t_order_voucher(
    seq,
    order_no,
    order_date,
    agent_seq,
    phone_no,
    pg_method_seq,
    provider_nominal_seq,
    total_payment,
    total_order,
    payment_status,
    order_status,
    created_by,
    created_date,
    modified_by,
    modified_date
) values (
    new_seq,
    pOrderNo,
    CURDATE(),
    pUserID,
    pPhoneNo,
    pPGMethodSeq,
    pProviderNominalSeq,
    pTotalPayment,
    pTotalOrder,
    pPaymentStatus,
    pOrderStatus,
    pUserID,
    now(),
    pUserID,
    now()
);
END IF;
END$$

