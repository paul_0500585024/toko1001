CREATE DEFINER=`root`@`%` PROCEDURE `sp_thumbs_delivery_promo_city`(
    pMerchantSeq int unsigned
    )
BEGIN
	SELECT 
	mpffm.`promo_seq` AS promo_seq,
	mpffc.`city_seq` AS city_seq,
	mc.`name` AS city_name
	FROM m_promo_free_fee_merchant mpffm
	LEFT JOIN m_promo_free_fee_city mpffc
	ON mpffm.`promo_seq`=mpffc.`promo_seq`
	left join m_promo_free_fee_period mpffp
	on mpffc.promo_seq=mpffp.seq
	LEFT JOIN m_city mc
	ON mpffc.`city_seq` = mc.seq
	WHERE mpffc.type = 'D' AND mpffp.status='A' and Date(now()) between mpffp.date_from and mpffp.date_to
	AND mpffm.merchant_seq = pMerchantSeq;
	
END