CREATE DEFINER=`root`@`%` PROCEDURE `sp_thumbs_category_get_product_seq`(
	IN pAttributeValueSeq TEXT
)
BEGIN
SET @sql_product_attribute = CONCAT('SELECT product_seq from m_product_attribute where attribute_value_seq IN (',pAttributeValueSeq,') GROUP BY product_seq HAVING COUNT(product_seq)>1');
PREPARE pa FROM @sql_product_attribute;
EXECUTE pa;
DEALLOCATE PREPARE pa;
END