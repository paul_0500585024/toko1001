DROP PROCEDURE IF EXISTS `sp_get_credit_product_list`;
DELIMITER $$
CREATE PROCEDURE `sp_get_credit_product_list` (
   IN pSTART INTEGER, 
   IN pLIMIT INTEGER,
   IN pWhere TEXT,
   IN pOrder TEXT
)
BEGIN
IF (pWhere = '') THEN SET @SqlWhere = ''; ELSE SET @SqlWhere = pWhere; END IF;
IF (pOrder = '') THEN SET @SqlOrder = ''; ELSE SET @SqlOrder = pOrder; END IF;
SET @sql = CONCAT("SELECT SQL_CALC_FOUND_ROWS
    pv.product_seq,
    pv.pic_1_img AS image,
    p.name AS `name`,
    p.`merchant_seq` AS merchant_seq,
    vv.seq AS variant_seq,
    vv.value AS `variant_value`,
    pv.`product_price` AS `product_price`,
    pv.`sell_price` AS `sell_price`,
    pv.`seq` AS `product_variant_seq`,
    ps.`stock` AS `stock`,
    ps.`merchant_sku` AS `sku`,
    qry.promo_credit_name,
    qry2.pcredit_name,
    m.name as merchant_name,
    m.code AS merchant_code,
    m.to_date,
    IF((NOW() < m.from_date OR NOW() > m.to_date
            OR (m.from_date = '0000-00-00 00:00:00'
            AND m.to_date = '0000-00-00 00:00:00')),
        'O',
        'C') AS store_status
FROM
    m_product_variant pv
        LEFT JOIN
    m_product p ON pv.product_seq = p.seq
        JOIN
    m_merchant m ON m.seq = p.merchant_seq
        JOIN
    m_product_category mpcat ON mpcat.seq = p.category_l2_seq
        INNER JOIN
    m_variant_value vv ON pv.variant_value_seq = vv.seq
        LEFT JOIN
    m_product_stock ps ON ps.`product_variant_seq` = pv.`seq`
        LEFT JOIN
    (SELECT 
    pc.seq,
        pc.promo_credit_name,
            pc.minimum_nominal,
            pc.maximum_nominal,
            pcp.product_variant_seq
    FROM
        m_promo_credit_product pcp
    JOIN m_promo_credit pc ON pcp.promo_credit_seq = pc.seq
    WHERE
        pc.status = 'A'
            AND CURDATE() BETWEEN pc.promo_credit_period_from AND pc.promo_credit_period_to) AS qry ON qry.product_variant_seq = pv.seq
        AND pv.sell_price BETWEEN qry.minimum_nominal AND qry.maximum_nominal
        LEFT JOIN
    (SELECT 
     pc.seq,
        pc.promo_credit_name AS pcredit_name,
            pc.minimum_nominal AS pc_min_nominal,
            pc.maximum_nominal AS pc_max_nominal,
            mpcc.category_seq
    FROM
        m_promo_credit_category mpcc
    JOIN m_promo_credit pc ON mpcc.promo_credit_seq = pc.seq
    WHERE
        pc.status = 'A'
            AND CURDATE() BETWEEN pc.promo_credit_period_from AND pc.promo_credit_period_to) AS qry2 ON qry2.category_seq = mpcat.parent_seq
        AND pv.sell_price BETWEEN qry2.pc_min_nominal AND qry2.pc_max_nominal
	WHERE qry2.pcredit_name is not null " ,@SqlWhere,
        " group by product_variant_seq ",@SqlOrder," LIMIT ? , ? " );
PREPARE STMT FROM @sql;
SET @START = pSTART; 
SET @LIMIT = pLIMIT; 
EXECUTE STMT USING @START, @LIMIT;
DEALLOCATE PREPARE STMT;



END$$

