DELIMITER $$
DROP PROCEDURE sp_search_home $$
CREATE PROCEDURE sp_search_home
(
       pLimit tinyint unsigned,
       pCategory int(10),
       pSearch varchar(250)
    )
BEGIN
Declare sqlWhere VarChar(1000);
SET @ORDER = "ORDER BY p.name";
SET @WHERE = " WHERE p.seq in (select pv.product_seq from m_product_variant pv INNER JOIN
    m_variant_value vv ON pv.variant_value_seq = vv.seq
        LEFT JOIN
    m_product_stock ps ON ps.`product_variant_seq` = pv.`seq` where pv.active = '1' AND pv.status IN ('L','C') )";
Set sqlWhere = "";

If pSearch <> "" Then
    Set sqlWhere = Concat(sqlWhere, " And p.`name` like '%" , escape_string(pSearch) , "%'");
End If;
If pCategory <>""  Then
    If pCategory >0  Then
        Set sqlWhere = Concat(sqlWhere, " And p.category_l2_seq in ( select seq from m_product_category where parent_seq='" , pCategory , "')");
    End If;
End If;
SET @sql = CONCAT("SELECT p.`name` , CONCAT('kategori?e=',`name`) as links FROM m_product p ",@WHERE, sqlWhere ," ",@ORDER," LIMIT 0,",pLimit);
PREPARE STMT FROM @sql;
EXECUTE STMT  ;
DEALLOCATE PREPARE STMT;

SET @sql = CONCAT("select `name`, CONCAT('merchant/',code) as links from m_merchant where `name` like '%",escape_string(pSearch),"%' LIMIT 0,",pLimit);
PREPARE STMT FROM @sql;
EXECUTE STMT  ;
DEALLOCATE PREPARE STMT;

END $$
DELIMITER ;
