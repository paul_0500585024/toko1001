CREATE DEFINER=`root`@`%` PROCEDURE `sp_thumbs_category_img`(
	pCategorySeq INT UNSIGNED
    )
BEGIN
SELECT 
	banner_img AS banner_image,
	banner_img_url AS banner_image_url
FROM 
	`m_product_category_img` 
WHERE 
	category_seq=pCategorySeq
LIMIT 1;
    
END