CREATE DEFINER=`root`@`%` PROCEDURE `sp_thumbs_floor`()
BEGIN
	select  category_seq,
		banner_img,
		banner_img_url,
		adv_1_img,
		adv_1_img_url,
		adv_2_img,
		adv_2_img_url,
		adv_3_img,
		adv_3_img_url,
		adv_4_img,
		adv_4_img_url,
		adv_5_img,
		adv_5_img_url,
		adv_6_img,
		adv_6_img_url,
		adv_7_img,
		adv_7_img_url,
		created_by,
		created_date,
		modified_by,modified_date
	from m_product_category_img;
END