CREATE DEFINER=`root`@`%` PROCEDURE `sp_thumbs_product_review`(
        pProductVariantSeq INT UNSIGNED,
	IN pSTART INTEGER, 
	IN pLIMIT INTEGER,
	IN pWHERE_CONDITION TEXT,
	IN pORDER_CONDITION TEXT
    )
BEGIN
DECLARE pProductSeq INT UNSIGNED;
IF(pORDER_CONDITION = '') THEN 
	SET @ORDER = 'ORDER BY pr.created_date ASC'; 
ELSE 
	SET @ORDER = pORDER_CONDITION; 
END IF;
IF(pWHERE_CONDITION = '') THEN 	
	IF(pProductVariantSeq IS NULL) THEN
		SET @WHERE = 'WHERE 0'; 
	ELSE
		SET @WHERE = CONCAT("WHERE pr.`product_variant_seq`=",pProductVariantSeq," AND pr.`status` = 'A'"); 
	END IF;
ELSE 
	SET @WHERE = pWHERE_CONDITION; 
END IF;
SET @sql = CONCAT("SELECT SQL_CALC_FOUND_ROWS 
	pr.product_variant_seq AS product_variant_seq, 
	pr.order_seq AS order_seq, 
	pr.rate AS rate, 
	pr.review AS review, 
	pr.review_admin AS review_admin, 
	pr.status AS `status`, 
	pr.created_by AS created_by, 
	pr.created_date AS created_date, 
	pr.modified_by AS modified_by, 
	pr.modified_date AS modified_date, 
	IF(m.name IS NULL, 'Unknown',m.name) AS `name`,
	DATEDIFF(DATE(NOW()),DATE(pr.created_date)) as `days_old`
FROM m_product_review pr 
LEFT JOIN m_member m 
ON pr.`created_by` = m.`seq`  ",@WHERE," ",@ORDER," LIMIT ?,?");
PREPARE STMT FROM @sql;
SET @START = pSTART; 
SET @LIMIT = pLIMIT; 
EXECUTE STMT USING @START, @LIMIT;
DEALLOCATE PREPARE STMT;
END