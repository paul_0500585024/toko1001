CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_dropdown_credit`(
	pProductVariant Bigint Unsigned
)
BEGIN

SELECT 
    pc.seq,
    bc.seq as credit_bank_seq,
    pc.`status`,
    pc.promo_credit_name,
    pc.promo_credit_period_from,
    pc.promo_credit_period_to,
    bc.credit_month,
    bc.rate,
    pv.sell_price,
    b.bank_name
FROM
    m_promo_credit pc
        JOIN
    m_promo_credit_bank pcb ON pcb.promo_seq = pc.seq 
        JOIN
    m_promo_credit_product pcp ON pcp.promo_credit_seq = pc.seq
        JOIN
    m_bank_credit bc ON bc.seq = pcb.bank_credit_seq
        JOIN
    m_product_variant pv ON pv.seq = pcp.product_variant_seq
        JOIN
    m_bank b ON bc.bank_seq = b.seq
WHERE
	pcp.product_variant_seq = pProductVariant and
    pc.status ='A' and
	CURDATE() between pc.promo_credit_period_from and pc.promo_credit_period_to;
END