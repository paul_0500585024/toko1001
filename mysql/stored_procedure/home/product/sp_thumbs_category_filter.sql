CREATE DEFINER=`root`@`%` PROCEDURE `sp_thumbs_category_filter`(
    pCategorySeq TEXT
    )
BEGIN
	IF pCategorySeq <> '\'0\''
	THEN
		SET @sql_attribute = CONCAT('SELECT 
				ac.`category_seq` AS attribute_category_category_seq, 
				ac.`attribute_seq` AS attribute_category_attribute_seq, 
				a.`seq` AS attribute_seq, 
				a.`name` AS attribute_name, 
				a.`display_name` AS attribute_display_name
			FROM m_attribute_category ac
			LEFT JOIN m_attribute AS a
			ON ac.`attribute_seq`=a.`seq`
			WHERE ac.`category_seq` IN (',pCategorySeq,')
			GROUP BY ac.`attribute_seq`,a.`seq`');
		    PREPARE sa FROM @sql_attribute;
		    EXECUTE sa;
		    DEALLOCATE PREPARE sa;
		    
		SET @sql_attribute_value = CONCAT('SELECT 
				ac.`attribute_seq` AS attribute_category_attribute_seq,
				av.seq AS attribute_value_seq, 
				av.value AS attribute_value_value 
			FROM m_attribute_category ac
			LEFT JOIN m_attribute_value av
			ON ac.`attribute_seq`= av.attribute_seq
			WHERE ac.`category_seq` IN (',pCategorySeq,')
			GROUP BY ac.`attribute_seq`,av.seq');
			
		    PREPARE sav FROM @sql_attribute_value;
		    EXECUTE sav;
		    DEALLOCATE PREPARE sav;
	END IF;
END