DROP PROCEDURE IF EXISTS `sp_get_product_info_by_seq`;
DELIMITER $$
CREATE PROCEDURE `sp_get_product_info_by_seq`(
        `pUserID` VARCHAR(25),
        `pIPAddress` VARCHAR(25),
        `pProductVariantSeq` MEDIUMINT UNSIGNED,
        `pExpSeq` TINYINT UNSIGNED,
        `pUser` VARCHAR(10),
	`pAgentSeq` BIGINT
    )
BEGIN
DECLARE commission_agent DECIMAL(5,2);
DECLARE commission_partner DECIMAL(5,2);
Declare partnerGroupSeq MEDIUMINT unsigned;

set commission_agent = 0;
set commission_partner = 0;
set partnerGroupSeq = 0;


SELECT 
    a.commission_pro_rate,
    p.commission_pro_rate,
    p.partner_group_seq
INTO commission_agent , commission_partner , partnerGroupSeq FROM
    m_agent a
        JOIN
    m_partner p ON a.partner_seq = p.seq
WHERE
    a.seq =pAgentSeq;
IF(pUser = 'AGENT') THEN
SELECT 
    p.`name` AS product_name,
    pv.seq AS product_seq,
    pv.product_price,
    pv.disc_percent,
    pv.sell_price,
    pv.max_buy,
    vv.seq AS variant_value,
    vv.value AS variant_name,
    CASE WHEN m.expedition_seq = 0  THEN ep.name ELSE e.name END exp_name,
    pv.pic_1_img,
    p.merchant_seq,
    m.`name` AS merchant_name,
    m.`code` AS merchant_code,
    p.p_weight_kg,
    p.p_length_cm,
    p.p_width_cm,
    p.p_height_cm,
    p.b_weight_kg,
    p.b_length_cm,
    p.b_width_cm,
    p.b_height_cm,
    CASE WHEN m.expedition_seq = 0  THEN ep.volume_divider ELSE e.volume_divider END volume_divider,
    CASE WHEN m.expedition_seq = 0  THEN ep.code ELSE e.code END exp_code,
    qryA.commission_fee,
    qryA.commission_fee_percent,
    qryA.nominal_commission_partner as ppc_nominal_commission_partner,
    qryA.nominal_commission_agent as ppc_nominal_commission_agent,
    qryA.commission_fee_agent_percent as ppc_commission_fee_agent_percent,
    qryA.commission_fee_partner_percent as ppc_commission_fee_partner_percent ,
    qryA.group_commission_fee_percent ,
    qryA.commission_partner as commission_pro_rate_partner,
    qryA.commission_agent as commission_pro_rate_agent
FROM 
	m_product p JOIN m_product_variant pv
		ON pv.product_seq = p.seq
				JOIN m_variant_value vv
		ON vv.seq = pv.variant_value_seq
				JOIN m_merchant m
		ON m.seq = p.merchant_seq
				JOIN m_expedition e
		ON e.seq = m.expedition_seq
				JOIN m_expedition ep
		ON ep.seq = pExpSeq
		LEFT JOIN 
        (SELECT 
        cpv.seq,
         cppc.nominal_commission_partner,
         cppc.nominal_commission_agent,
            cpv.sell_price,
            cppc.commission_fee_agent_percent,
            cppc.commission_fee_partner_percent,
			cpgc.commission_fee_percent as group_commission_fee_percent,
            commission_partner,
            commission_agent,
            CASE
                WHEN
                    (cppc.product_seq IS NOT NULL
                        AND cpp.seq IS NOT NULL)
                THEN
                    CASE
                        WHEN cppc.nominal_commission_partner = 0 THEN (cpv.sell_price * cppc.commission_fee_partner_percent / 100) * (cppc.commission_fee_agent_percent / 100)
                        ELSE (cppc.commission_fee_agent_percent / 100) * cppc.nominal_commission_partner
                    END
                ELSE CASE
                    WHEN (cpgc.category_l2_seq IS NOT NULL) THEN ((cpgc.commission_fee_percent / 100) * cpv.sell_price) * (commission_agent / 100)
                    ELSE ((commission_partner / 100) * cpv.sell_price) * (commission_agent / 100)
                END
            END AS commission_fee,
            CASE
                WHEN
                    (cppc.product_seq IS NOT NULL
                        AND cpp.seq IS NOT NULL)
                THEN
                    CASE
                        WHEN cppc.nominal_commission_partner = 0 THEN (cpv.sell_price * cppc.commission_fee_partner_percent / 100) * (cppc.commission_fee_agent_percent / 100)
                        ELSE (cppc.commission_fee_agent_percent / 100)
                    END
                ELSE CASE
                    WHEN (cpgc.category_l2_seq IS NOT NULL) THEN (commission_agent / 100) * (cpgc.commission_fee_percent)
                    ELSE (commission_partner / 100) * (commission_agent / 100)
                END
            END AS commission_fee_percent
    FROM
        m_product_variant cpv
    JOIN m_product cp ON cp.seq = cpv.product_seq
    JOIN m_product_category cpc ON cpc.seq = cp.category_l2_seq
    LEFT JOIN m_partner_product_commission cppc ON cppc.product_seq = cp.seq
    LEFT JOIN m_partner_product cpp ON cppc.partner_product_seq = cpp.seq
        AND CURDATE() BETWEEN cpp.period_from AND cpp.period_to
    LEFT JOIN m_partner_group_commission cpgc ON cpgc.category_l2_seq = cp.category_l2_seq
        AND cpgc.partner_group_seq = partnerGroupSeq
    LEFT JOIN m_partner_group pg ON pg.partner_group_seq = cpgc.partner_group_seq
        AND pg.partner_group_seq = partnerGroupSeq) AS qryA ON pv.seq = qryA.seq
WHERE
	pv.seq = pProductVariantSeq;

ELSE
Select 
    p.`name` as product_name,
    pv.seq as product_seq,
    pv.product_price,
    pv.disc_percent,
    pv.sell_price,
    pv.max_buy,
    vv.seq as variant_value,
    vv.value as variant_name,
    case when m.expedition_seq = 0  then ep.name else e.name end exp_name,
    pv.pic_1_img,
    p.merchant_seq,
    m.`name` as merchant_name,
    m.`code` as merchant_code,
    p.p_weight_kg,
    p.p_length_cm,
    p.p_width_cm,
    p.p_height_cm,
    p.b_weight_kg,
    p.b_length_cm,
    p.b_width_cm,
    p.b_height_cm,
    case when m.expedition_seq = 0  then ep.volume_divider else e.volume_divider end volume_divider,
    case when m.expedition_seq = 0  then ep.code else e.code end exp_code
from 
	m_product p join m_product_variant pv
		on pv.product_seq = p.seq
				join m_variant_value vv
		on vv.seq = pv.variant_value_seq
				join m_merchant m
		on m.seq = p.merchant_seq
				join m_expedition e
		on e.seq = m.expedition_seq
				join m_expedition ep
		on ep.seq = pExpSeq		
Where
	pv.seq = pProductVariantSeq;
END IF;   
END$$