DROP PROCEDURE IF EXISTS `sp_thumbs_new_products`;
DELIMITER $$
CREATE PROCEDURE `sp_thumbs_new_products`(
        pSTART INTEGER, 
	pLIMIT INTEGER,
	pWHERE_CONDITION TEXT,
	pORDER_CONDITION TEXT,
	pUser VARCHAR(10),
	pAgentSeq BIGINT(20)
)
BEGIN
    
    DECLARE commission_agent decimal(5,2);
    DECLARE commission_partner decimal(5,2);
    Declare partnerGroupSeq MEDIUMINT unsigned;
    -- DECLARE agentCommision decimal(5,2);
    -- DECLARE partnerCommision decimal(5,2);
    set commission_agent = 0 ;
    set commission_partner = 0 ;
    set partnerGroupSeq = 0;

IF(pORDER_CONDITION = '') THEN SET @ORDER = 'ORDER BY pv.seq DESC'; ELSE SET @ORDER = pORDER_CONDITION; END IF;
IF(pWHERE_CONDITION = '') THEN SET @WHERE = 'WHERE pv.active = ''1'' AND pv.status IN (''L'',''C'')'; ELSE SET @WHERE = pWHERE_CONDITION; END IF;

IF(pUser = 'AGENT') THEN
	SELECT 
            a.commission_pro_rate,
            p.commission_pro_rate,
            p.partner_group_seq
        INTO commission_agent , commission_partner , partnerGroupSeq FROM
            m_agent a
        JOIN
            m_partner p ON p.seq = a.partner_seq
        WHERE
            a.seq = pAgentSeq;

	SET @AGENT_TABLE = concat('LEFT JOIN m_merchant_trx_fee mtrx ON mtrx.category_l2_seq = p.category_l2_seq AND mtrx.merchant_seq = p.merchant_seq LEFT JOIN m_product_category pc ON p.category_l2_seq = pc.seq');
    SET @AGENT_SELECT='';      
ELSE
	SET @AGENT_TABLE='';
	SET @AGENT_SELECT='';
	SET @AGENT_SELECTT='';      
END IF;
SET @sql = CONCAT("SELECT SQL_CALC_FOUND_ROWS
    pv.product_seq,	
    pv.pic_1_img AS image,
    p.name AS `name`,
    p.`merchant_seq` AS merchant_seq,
    vv.seq AS variant_seq,
    vv.value AS `variant_value`,
    pv.`product_price` AS `product_price`,
    pv.`sell_price` AS `sell_price`,
    pv.`seq` AS `product_variant_seq`,
    (select case when Count(*) is null || Count(*) = 0 then 0 else 1 end from m_product_stock ps where ps.product_variant_seq = pv.seq and stock > 0 ) as stock,
    '' AS `sku`,
    qry.promo_credit_name,
    qry2.pcredit_name,
    m.code as merchant_code,
    m.name as merchant_name",@AGENT_SELECT,",
    m.to_date,
    qryA.commission_fee,
    qryA.commission_fee_percent,
    if((NOW() < m.from_date OR NOW() > m.to_date OR (m.from_date = '0000-00-00 00:00:00' AND m.to_date = '0000-00-00 00:00:00')),'O','C') as store_status
FROM
    m_product_variant pv
        LEFT JOIN
    m_product p ON pv.product_seq = p.seq
        JOIN
    m_merchant m ON m.seq = p.merchant_seq
        JOIN
    m_product_category mpcat on mpcat.seq=p.category_l2_seq
        INNER JOIN
    m_variant_value vv ON pv.variant_value_seq = vv.seq
        LEFT JOIN
    (select
    pc.promo_credit_name,
    pc.minimum_nominal,
    pc.maximum_nominal,
    pcp.product_variant_seq
    from m_promo_credit_product pcp LEFT JOIN m_promo_credit pc on pcp.promo_credit_seq = pc.seq
    where pc.status = 'A' and
    CURDATE() between pc.promo_credit_period_from and pc.promo_credit_period_to) as qry
    on qry.product_variant_seq = pv.seq and pv.sell_price between qry.minimum_nominal and qry.maximum_nominal
        LEFT JOIN
    (select
    pc.promo_credit_name as pcredit_name,
    pc.minimum_nominal as pc_min_nominal,
    pc.maximum_nominal as pc_max_nominal,
    mpcc.category_seq
    from m_promo_credit_category mpcc LEFT JOIN m_promo_credit pc on mpcc.promo_credit_seq = pc.seq
    where pc.status = 'A' and
    CURDATE() between pc.promo_credit_period_from and pc.promo_credit_period_to) as qry2
    on qry2.category_seq = mpcat.parent_seq and pv.sell_price between qry2.pc_min_nominal and qry2.pc_max_nominal
		Left Join 
	(SELECT 
			cpv.seq,
			CASE
				WHEN
					(cppc.product_seq IS NOT NULL  AND cpp.seq IS NOT NULL)
				THEN
					cppc.nominal_commission_agent
				ELSE 
					CASE
						WHEN (cpgc.category_l2_seq IS NOT NULL) 
					THEN
							((cpgc.commission_fee_percent / 100)* cpv.sell_price ) * (",commission_agent/100,")
						ELSE
							((",commission_partner/100,") * cpv.sell_price ) * (",commission_agent/100,")
					END               
			END as commission_fee,
    
			CASE
				WHEN
					(cppc.product_seq IS NOT NULL AND cpp.seq IS NOT NULL)
				THEN
					CASE
						WHEN cppc.nominal_commission_partner = 0 THEN (cpv.sell_price * cppc.commission_fee_partner_percent / 100) * (cppc.commission_fee_agent_percent / 100)
						ELSE (cppc.commission_fee_agent_percent / 100)
					END
				ELSE 
					CASE
						WHEN (cpgc.category_l2_seq IS NOT NULL) 
					THEN
							(",commission_agent/100,") * (cpgc.commission_fee_percent)
						ELSE
							(" , commission_partner/100 ,")  * (",commission_agent/100,") 
					END
			END as commission_fee_percent
		FROM
			m_product_variant cpv
				JOIN
			m_product cp ON cp.seq = cpv.product_seq
				JOIN
			m_product_category cpc ON cpc.seq = cp.category_l2_seq
				 LEFT JOIN
			m_partner_product_commission cppc ON cppc.product_seq = cp.seq
				 LEFT JOIN
			m_partner_product cpp ON cppc.partner_product_seq = cpp.seq
                                AND CURDATE() BETWEEN cpp.period_from AND cpp.period_to
				LEFT JOIN
			m_partner_group_commission cpgc ON cpgc.category_l2_seq = cp.category_l2_seq
                                 AND cpgc.partner_group_seq = ", partnerGroupSeq ,"
                                 LEFT JOIN
                        m_partner_group pg ON pg.partner_group_seq = cpgc.partner_group_seq
                                 AND pg.partner_group_seq = ", partnerGroupSeq ,"
		) as qryA on pv.seq = qryA.seq

",@AGENT_TABLE," ",@WHERE," ",@ORDER," LIMIT ?,?");
PREPARE STMT FROM @sql;
SET @START = pSTART;
SET @LIMIT = pLIMIT;
EXECUTE STMT USING @START, @LIMIT;
DEALLOCATE PREPARE STMT;
END$$