CREATE DEFINER=`root`@`%` PROCEDURE `sp_thumbs_banner`()
BEGIN
SELECT 
	seq as image_slide_seq, 
	img as image, img_url as image_url 
FROM 
	m_img_slide_show 
where 
	`status`='A' AND `active`='1' ORDER BY `order`;
SELECT 
	banner_img as banner_image, 
	banner_img_url AS banner_image_url, 
	adv_1_img as advertise1_image, 
	adv_1_img_url AS advertise1_image_url, 
	adv_2_img AS advertise2_image, 
	adv_2_img_url AS advertise2_image_url, 
	adv_3_img AS advertise3_image, 
	adv_3_img_url AS advertise3_image_url		
FROM 
	`m_product_category_img` 
WHERE 
	category_seq=0;
END