DROP PROCEDURE IF EXISTS `sp_thumbs_detail`;
DELIMITER $$
CREATE PROCEDURE `sp_thumbs_detail` (
    `pProductVariantSeq` INTEGER UNSIGNED,
    `pUser` VARCHAR(10),
    `pAgentSeq` BIGINT(20)
)
BEGIN
    DECLARE pProductSeq INT UNSIGNED;
    DECLARE agentCommision DECIMAL(4,2);
    DECLARE partnerCommision DECIMAL(4,2);
    Declare partnerGroupSeq MEDIUMINT unsigned;


-- Set Commission Agent, Commission Partner, partner_group_seq
-- @pAgentCommission, @pPartnerCommission, @pPartnerGroupSeq

SELECT 
    a.commission_pro_rate,
    p.commission_pro_rate,
    p.partner_group_seq
INTO agentCommision , partnerCommision , partnerGroupSeq FROM
    m_agent a
        JOIN
    m_partner p ON p.seq = a.partner_seq
WHERE
    a.seq = pAgentSeq;
-- End Set Commission 

SELECT
	product_seq INTO pProductSeq
FROM
	m_product_variant
WHERE
	`active`='1' AND
	seq = pProductVariantSeq;
IF pUser = 'AGENT' THEN
SELECT
    p.name AS `name`,
    p.seq AS product_seq,
    p.`merchant_seq` AS merchant_seq,
    vv.seq AS variant_seq,
    vv.value AS `variant_value`,
    pv.`product_price` AS `product_price`,
    pv.`sell_price` AS `sell_price`,
    pv.`seq` AS `product_variant_seq`,
    p.`description` AS description,
    p.`specification` AS specification,
    p.`warranty_notes` AS guarante,
    p.`p_weight_kg` AS weight,
    p.`p_length_cm` AS dimension_lenght,
    p.`p_width_cm` AS dimension_width,
    p.`p_height_cm` AS dimension_height,
    m.`name` AS merchant_name,
    m.`logo_img`,
    pv.`pic_1_img` AS image1,
    pv.`pic_2_img` AS image2,
    pv.`pic_3_img` AS image3,
    pv.`pic_4_img` AS image4,
    pv.`pic_5_img` AS image5,
    pv.`pic_6_img` AS image6,
    pv.`max_buy`,
    m.`code` AS `code`,
    d.`name` AS merchant_district,
    c.`name` AS merchant_city,
    pr.`name` AS merchant_province,
    p.category_l2_seq AS `category_l2_seq`,
    p.category_ln_seq AS `category_ln_seq`,
    ps.`stock` AS `stock`,
    ps.`merchant_sku` AS `sku`,
    qry.seq AS promo_credit_bank_seq,
    m.to_date,
    CONCAT(' x ',qry.credit_month,' Bulan ',' -- ',qry.bank_name) AS credit_month,
    qry.`status` AS status_credit ,
    qryA.commission_fee,
    qryA.commission_fee_percent,
    mtrx.trx_fee_percent as test1,
    IF((NOW() < m.from_date OR NOW() > m.to_date OR (m.from_date = '0000-00-00 00:00:00' AND m.to_date = '0000-00-00 00:00:00')),'O','C') AS store_status,
    qry2.pc_status, qry2.pc_seq,
    mtrx.trx_fee_percent
FROM
    m_product_variant pv
        LEFT JOIN
    m_product p ON pv.product_seq = p.seq
        INNER JOIN
    m_variant_value vv ON pv.variant_value_seq = vv.seq
        INNER JOIN
    m_merchant m ON p.`merchant_seq` = m.`seq`
        LEFT JOIN
    m_product_stock ps ON ps.`product_variant_seq` = pv.`seq`
        JOIN
    (select * from m_product_category where `level`=2) mpc on mpc.seq = p.category_l2_seq
	JOIN
    m_district d ON d.seq = m.district_seq
        JOIN
    m_city c ON c.seq = d.city_seq
        JOIN
    m_province pr ON pr.seq = c.province_seq
	LEFT JOIN
	(SELECT
	pc.seq,
	pc.promo_credit_name,
        pc.minimum_nominal,
        pc.maximum_nominal,
        pc.status,
        pcp.product_variant_seq,
        b.bank_name,
        bc.credit_month
	FROM
            m_promo_credit_product pcp
        LEFT JOIN
            m_promo_credit pc ON pcp.promo_credit_seq = pc.seq
        LEFT JOIN
            m_promo_credit_bank pcb ON pcb.promo_seq = pc.seq
        LEFT JOIN
            m_bank_credit bc ON bc.seq = pcb.bank_credit_seq
        LEFT JOIN
            m_bank b ON b.seq = bc.bank_seq
	WHERE pc.status = 'A' AND
        CURDATE() BETWEEN pc.promo_credit_period_from AND pc.promo_credit_period_to) AS qry ON qry.product_variant_seq = pv.seq
        and pv.sell_price between qry.minimum_nominal and qry.maximum_nominal

        LEFT JOIN
            (select
            pc.seq pc_seq,
            pc.status as pc_status,
            pc.minimum_nominal as pc_min_nominal,
            pc.maximum_nominal as pc_max_nominal,
            mpcc.category_seq
            from m_promo_credit_category mpcc LEFT JOIN m_promo_credit pc on mpcc.promo_credit_seq = pc.seq
            where pc.status = 'A' and
            CURDATE() between pc.promo_credit_period_from and pc.promo_credit_period_to) as qry2
            on qry2.category_seq = mpc.parent_seq and pv.sell_price between qry2.pc_min_nominal and qry2.pc_max_nominal
        LEFT JOIN (select * from m_agent_trx_commission where agent_seq=pAgentSeq) as matc ON matc.category_l2_seq= p.`category_l2_seq`
        LEFT JOIN m_merchant_trx_fee mtrx ON mtrx.category_l2_seq = p.category_l2_seq AND mtrx.merchant_seq = p.merchant_seq
        LEFT JOIN m_product_category pc ON p.category_l2_seq = pc.seq
        Left Join 
	(SELECT 
            pv.seq as varianSeq,
            CASE WHEN (ppc.product_seq IS NOT NULL AND pp.seq IS NOT NULL) THEN
                    ppc.nominal_commission_agent
                ELSE 
                CASE WHEN (pgc.category_l2_seq IS NOT NULL) 
                    THEN ((pgc.commission_fee_percent / 100)* pv.sell_price ) * (agentCommision/100)
                ELSE
                    ((partnerCommision/100) * pv.sell_price ) * (agentCommision/100)
                END               
            END as commission_fee,
            '' as commission_fee_percent
        FROM
            m_product_variant pv
                JOIN
            m_product p ON p.seq = pv.product_seq
                AND pv.seq = pProductVariantSeq
                LEFT JOIN
            m_partner_product_commission ppc ON ppc.product_seq = p.seq
                AND ppc.partner_group_seq = partnerGroupSeq
                LEFT JOIN
            m_partner_product pp ON ppc.partner_group_seq = pp.seq
                AND CURDATE() BETWEEN pp.period_from AND pp.period_to
                LEFT JOIN
            m_partner_group_commission pgc ON pgc.category_l2_seq = p.category_l2_seq
                AND pgc.partner_group_seq = partnerGroupSeq
                LEFT JOIN
            m_partner_group pg ON pg.partner_group_seq = pgc.partner_group_seq
                AND pg.partner_group_seq = partnerGroupSeq
		) as qryA on pv.seq = qryA.varianSeq
        -- END COMMISSION AGENT
WHERE
	pv.`active`='1' AND
	pv.seq= pProductVariantSeq AND
	pv.status IN ('L','C')
LIMIT 1;
else
SELECT
    p.name AS `name`,
    p.seq AS product_seq,
    p.`merchant_seq` AS merchant_seq,
    vv.seq AS variant_seq,
    vv.value AS `variant_value`,
    pv.`product_price` AS `product_price`,
    pv.`sell_price` AS `sell_price`,
    pv.`seq` AS `product_variant_seq`,
    p.`description` AS description,
    p.`specification` AS specification,
    p.`warranty_notes` AS guarante,
    p.`p_weight_kg` AS weight,
    p.`p_length_cm` AS dimension_lenght,
    p.`p_width_cm` AS dimension_width,
    p.`p_height_cm` AS dimension_height,
    m.`name` AS merchant_name,
    m.`logo_img`,
    pv.`pic_1_img` AS image1,
    pv.`pic_2_img` AS image2,
    pv.`pic_3_img` AS image3,
    pv.`pic_4_img` AS image4,
    pv.`pic_5_img` AS image5,
    pv.`pic_6_img` AS image6,
    pv.`max_buy`,
    m.`code` AS `code`,
    d.`name` AS merchant_district,
    c.`name` AS merchant_city,
    pr.`name` AS merchant_province,
    p.category_l2_seq AS `category_l2_seq`,
    p.category_ln_seq AS `category_ln_seq`,
    ps.`stock` AS `stock`,
    ps.`merchant_sku` AS `sku`,
    qry.seq AS promo_credit_bank_seq,
    m.to_date,
    CONCAT(' x ', qry.credit_month, ' Bulan ',' -- ',qry.bank_name) AS credit_month, qry.`status` AS status_credit,
    qry2.pc_status, qry2.pc_seq,
    IF((NOW() < m.from_date OR NOW() > m.to_date OR (m.from_date = '0000-00-00 00:00:00' AND m.to_date = '0000-00-00 00:00:00')),'O','C') as store_status
FROM
    m_product_variant pv
        LEFT JOIN
    m_product p ON pv.product_seq = p.seq
        JOIN
    m_product_category mpcat on mpcat.seq=p.category_l2_seq
        INNER JOIN
    m_variant_value vv ON pv.variant_value_seq = vv.seq
        INNER JOIN
    m_merchant m ON p.`merchant_seq` = m.`seq`
        LEFT JOIN
    m_product_stock ps ON ps.`product_variant_seq` = pv.`seq`
        JOIN
    m_district d ON d.seq = m.district_seq
        JOIN
    m_city c ON c.seq = d.city_seq
        JOIN
    m_province pr ON pr.seq = c.province_seq
	LEFT JOIN
	(SELECT
		pc.seq,
		pc.promo_credit_name,
        pc.minimum_nominal,
        pc.maximum_nominal,
        pc.status,
        pcp.product_variant_seq,
        b.bank_name,
        bc.credit_month
	FROM
		m_promo_credit_product pcp
        LEFT JOIN m_promo_credit pc ON
		pcp.promo_credit_seq = pc.seq
	LEFT JOIN
            	m_promo_credit_bank pcb ON pcb.promo_seq = pc.seq
        LEFT JOIN
                m_bank_credit bc ON bc.seq = pcb.bank_credit_seq
        LEFT JOIN
                m_bank b ON b.seq = bc.bank_seq
	WHERE pc.status = 'A' AND
        CURDATE() BETWEEN pc.promo_credit_period_from AND pc.promo_credit_period_to) AS qry ON qry.product_variant_seq = pv.seq
        and pv.sell_price between qry.minimum_nominal and qry.maximum_nominal

	LEFT JOIN
		(select
		pc.seq pc_seq,
		pc.status as pc_status,
		pc.minimum_nominal as pc_min_nominal,
		pc.maximum_nominal as pc_max_nominal,
		mpcc.category_seq
		from m_promo_credit_category mpcc LEFT JOIN m_promo_credit pc on mpcc.promo_credit_seq = pc.seq
		where pc.status = 'A' and
		CURDATE() between pc.promo_credit_period_from and pc.promo_credit_period_to) as qry2
		on qry2.category_seq = mpcat.parent_seq and pv.sell_price between qry2.pc_min_nominal and qry2.pc_max_nominal

WHERE
	pv.`active`='1' AND
	pv.seq= pProductVariantSeq AND
	pv.status IN ('L','C')
LIMIT 1;
END IF;
SELECT
     a.`name` AS `name`,
     av.`value` AS `value`
FROM
    `m_product` p
    INNER JOIN `m_product_attribute` pa
        ON (`pa`.`product_seq` = `p`.`seq`)
    INNER JOIN `m_attribute_value` av
        ON (`pa`.`attribute_value_seq` = `av`.`seq`)
    INNER JOIN `m_attribute` a
        ON (`av`.`attribute_seq` = `a`.`seq`)
WHERE
	`p`.`seq`= pProductSeq
UNION ALL
SELECT
	ps.name AS `name`,
	ps.value AS `value`
FROM m_product_spec ps
WHERE ps.product_seq= pProductSeq;
END$$

