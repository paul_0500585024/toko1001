CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_promo_free_fee_city_by_merchant`(
       pMerchantSeq Int UNSIGNED
    )
BEGIN
(select 'SEMUA KOTA' as `name` from `m_promo_free_fee_period` mpffp join `m_promo_free_fee_merchant`
        mpffm on mpffp.`seq`=mpffm.`promo_seq` 
        where mpffp.`active`='1' and mpffp.`status`= 'A' and Date(now()) between mpffp.date_from and mpffp.date_to
        and mpffm.merchant_seq=pMerchantSeq and all_destination_city='1')
        
        union all
        
	(Select 
		mc.name from `m_promo_free_fee_city` mpffc join m_city mc on mpffc.city_seq=mc.`seq`
        where mpffc.`type`='D' and mpffc.`promo_seq` in 
        ( select mpffp.seq from `m_promo_free_fee_period` mpffp join `m_promo_free_fee_merchant`
        mpffm on mpffp.`seq`=mpffm.`promo_seq` 
        where mpffp.`active`='1' and mpffp.`status`= 'A' and Date(now()) between mpffp.date_from and mpffp.date_to
        and mpffm.merchant_seq=pMerchantSeq)
        order by mc.name asc )
;

END