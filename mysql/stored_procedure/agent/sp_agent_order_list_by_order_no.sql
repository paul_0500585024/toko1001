DROP PROCEDURE IF EXISTS `sp_agent_order_list_by_order_no`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_order_list_by_order_no` (
        `pUserID` VARCHAR(100),
        `pIPAddress` VARCHAR(25),
        `pOrderNo` VARCHAR(20)

)
BEGIN
IF EXISTS (SELECT order_no FROM t_order WHERE order_no = pOrderNo) THEN
BEGIN
SELECT 
	o.seq,
	o.order_no,
	o.order_date,
        o.agent_seq as member_seq,
	o.agent_seq,
	o.signature,
	m.`name` member_name,
	m.email,
	m.mobile_phone,
	c.`name` city_name,
	CASE WHEN o.payment_retry <> '0'  THEN CONCAT(o.order_no, '-', CAST(o.payment_retry AS CHAR(2))) ELSE o.order_no END order_trans_no,	
	o.receiver_name,
	o.receiver_address,
	o.receiver_district_seq,
	d.`name` district_name,
	c.`name` city_name,
	p.`name` province_name,
	o.receiver_zip_code,
	o.receiver_phone_no,
	o.payment_status,
	o.pg_method_seq,
	pg.`code` payment_code,
	pg.name payment_name,
	o.paid_date,
	CASE WHEN pv.nominal IS NULL THEN mc.nominal ELSE pv.nominal END AS nominal,
	pv.code AS voucher_code,
	o.payment_retry,
	o.total_order,
	o.voucher_seq,
	o.voucher_refunded,
	o.total_payment,
	o.conf_pay_type,
	o.conf_pay_amt_member,
	o.conf_pay_date,
	o.conf_pay_note_file,
	o.conf_pay_bank_seq,
	o.conf_pay_amt_admin,
	o.promo_credit_seq,
	t_o_l.`customer_seq`,
	t_o_l.`dp`,
	t_o_l.`admin_fee`,
	t_o_l.`tenor`,
	t_o_l.`loan_interest`,
	t_o_l.`installment`,
	t_o_l.`total_installment`,
	t_o_l.`status_order`,
	t_o_l.pg_method_seq as pg_seq_loan,
	t_o_l.auth_date,
	pgm.`name` AS pg_method_name,
        o.apps,
	o.gcm_id,
	o.promo_credit_seq,
	o.created_date
	
FROM 
	t_order o JOIN m_district d
	ON o.receiver_district_seq = d.seq
		JOIN m_city c 
	ON c.seq = d.city_seq 
		JOIN m_province p
	ON p.seq = c.province_seq
		JOIN m_payment_gateway_method pg
	ON pg.seq = o.pg_method_seq
		LEFT JOIN m_promo_voucher pv 
	ON o.voucher_seq = pv.seq
		LEFT JOIN m_coupon mc 
	ON o.coupon_seq = mc.seq
		JOIN m_agent m
	ON m.seq = o.agent_seq
	  LEFT JOIN t_order_loan t_o_l ON t_o_l.order_seq=o.seq
	  LEFT JOIN m_payment_gateway_method pgm ON pgm.seq=t_o_l.pg_method_seq
	  
WHERE
	o.order_no = pOrderNo;
    
SELECT 
    t.order_seq,
    t.merchant_info_seq,
    t.expedition_service_seq,
    e.`name` expedition_name,
    mm.`name` merchant_name,
    mm.email,
    mm.code AS merchant_code,
    mm.seq merchant_seq,
    t.real_expedition_service_seq,
    t.total_merchant,
    t.total_ins,
    t.total_ship_real,
    t.total_ship_charged,
    t.free_fee_seq,
    t.order_status,
    t.member_notes,
    t.printed,
    t.print_date,
    t.awb_seq,
    t.awb_no,
    t.ref_awb_no,
    t.ship_by,
    t.ship_by_exp_seq,
    t.ship_date,
    t.ship_note_file,
    t.ship_notes,
    t.received_date,
    t.received_by,
    t.redeem_seq,
    t.exp_invoice_seq,
    t.exp_invoice_awb_seq,
    (SELECT 
            COUNT(*) total_barang
        FROM
            t_order_product top
                JOIN
            t_order tor ON top.order_seq = tor.seq
        WHERE
            tor.order_no = o.order_no
                AND top.merchant_info_seq = t.merchant_info_seq) AS total_product,
	t.created_date,
    o.paid_date,
    t.`modified_date`,
    (select sum(total_ship_charged) from t_order_merchant where order_seq = t.order_seq) as total_ship
FROM
    t_order_merchant t
        JOIN
    m_merchant_info m ON m.seq = t.merchant_info_seq
        JOIN
    m_district d ON d.seq = m.pickup_district_seq
        JOIN
    m_city c ON c.seq = d.city_seq
        JOIN
    m_province p ON p.seq = c.province_seq
        JOIN
    m_merchant mm ON mm.seq = m.merchant_seq
        JOIN
    m_expedition_service s ON s.seq = t.expedition_service_seq
        JOIN
    m_expedition e ON s.exp_seq = e.seq
        JOIN
    t_order o ON o.seq = t.order_seq
WHERE
    o.order_no = pOrderNo;
            
SELECT 
	t.order_seq,
	mi.merchant_seq,
	t.merchant_info_seq,
	p.`name` AS display_name,
	v.seq AS product_seq,
	v.pic_1_img AS img,
	mv.`value`,
	mv.seq AS value_seq,
	mv.`value` AS variant_name,
	t.product_status,
	t.qty,
	t.sell_price,
	t.weight_kg,
	t.ship_price_real,
	t.ship_price_charged,
	t.trx_fee_percent,
	t.ins_rate_percent,
	t.product_status,
	t.qty_return,
	t.created_by,
	t.created_date,
	t.modified_by,
	t.modified_date,
	t.commission_fee_percent,
        vv.`value`,
        t.variant_value_seq
FROM
	t_order_product t 
		JOIN m_product_variant v			
			ON v.seq = t.product_variant_seq
		JOIN m_product p
			ON v.product_seq = p.seq
		JOIN m_variant_value vv
			ON vv.seq = t.variant_value_seq
		JOIN m_variant_value mv 
			ON mv.seq = v.variant_value_seq
		JOIN t_order o
			ON o.seq = t.order_seq
		JOIN m_merchant_info mi
			ON mi.seq = t.merchant_info_seq
		JOIN m_merchant mm 
			ON mm.seq = mi.merchant_seq
WHERE
	o.order_no = pOrderNo;
    
SELECT 
    'Pesanan telah kami terima dan dalam proses verifikasi' AS history_message,
    created_date 
FROM
    t_order
WHERE
    order_no = pOrderNo
UNION ALL SELECT 
    'Pesanan telah terverifikasi dan akan segera dikirim',
    paid_date 
FROM
    t_order
WHERE
    order_no = pOrderNo 
UNION ALL SELECT 
    CONCAT('Pesanan sedang diproses untuk dikirim oleh ',mm.name),
    t.print_date
FROM
    t_order_merchant t
	JOIN
    m_merchant_info m ON m.seq = t.merchant_info_seq
	JOIN
    m_merchant mm ON mm.seq = m.merchant_seq
        JOIN
    t_order o ON o.seq = t.order_seq
WHERE
    o.order_no = pOrderNo  AND  t.print_date <> "0000-00-00 00:00:00"
UNION ALL SELECT 
    CONCAT('Pesanan telah dikirim oleh ',
	    mm.`name`,
            ' dan melalui ',
            e.`name`,
            ', no.resi ',
            t.awb_no),
    t.ship_date 
FROM
    t_order_merchant t
    	JOIN
    m_merchant_info m ON m.seq = t.merchant_info_seq
	JOIN
    m_merchant mm ON mm.seq = m.merchant_seq
        JOIN
    m_expedition_service s ON s.seq = t.expedition_service_seq
        JOIN
    m_expedition e ON s.exp_seq = e.seq
        JOIN
    t_order o ON o.seq = t.order_seq
WHERE
    o.order_no = pOrderNo AND t.ship_date <> "0000-00-00"
UNION ALL SELECT 
    'Pesanan telah diterima pelanggan',
     t.received_date 
FROM
    t_order_merchant t
        JOIN
    t_order o ON o.seq = t.order_seq
WHERE
    o.order_no = pOrderNo  AND t.received_date <> "0000-00-00"
ORDER BY created_date DESC;    
END;
ELSE
BEGIN
SELECT
	o.seq,
	o.order_no,
	o.order_no AS order_trans_no,
    o.agent_seq,
	o.order_date,
	o.total_order,
	o.total_payment,
	o.payment_status,
	o.order_status,
	o.signature,
	o.pg_method_seq,
	o.phone_no AS phone_number,
	psn.nominal,
	ps.name AS provider_service_name,
	p.name AS provider_name,
	pgm.name AS pg_method_name,
	pgm.code AS payment_code,
    mapping_code
FROM 
	t_order_voucher o JOIN m_provider_service_nominal psn
	ON psn.seq = o.provider_nominal_seq
		JOIN m_provider_service ps
	ON ps.seq = psn.provider_service_seq
		JOIN m_provider p
	ON p.seq = ps.provider_seq
		JOIN m_payment_gateway_method pgm
	ON pgm.seq = o.pg_method_seq
		JOIN m_payment_gateway pg
	ON pg.seq = pgm.pg_seq
WHERE
	order_no = pOrderNo;
END;
END IF;	
END$$
