DELIMITER $$
DROP PROCEDURE sp_agent_order_list $$
CREATE PROCEDURE sp_agent_order_list
(
        `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pagentSeq` BIGINT(10),
        `pCurrPage` INTEGER UNSIGNED,
        `pRecPerPage` INTEGER UNSIGNED,
        `pDirSort` VARCHAR(4),
        `pColumnSort` VARCHAR(50),
        `pOrderNo` VARCHAR(50),
        `pDateSearch` CHAR(1),
        `pOrderDateFrom` DATETIME,
        `pOrderDateTo` DATETIME,
        `pPaymentStatus` CHAR(1)
    )
BEGIN
    -- Begin variables for Paging
    Declare startRec Int unsigned;
    Declare totalPage Int unsigned;
    Declare totalRec Int unsigned;
    -- End variables for Paging
    -- Begin SQL Where
    Declare sqlWhere VarChar(500);
	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";

    If pOrderNo <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And o.order_no like '%" , escape_string(pOrderNo), "%'");
    End If;

    If (pOrderDateFrom <> "") Then
        Set sqlWhere = Concat(sqlWhere, " And o.order_date >='" , pOrderDateFrom, "'");
    End If;
    If (pOrderDateTo <> "") Then
        Set sqlWhere = Concat(sqlWhere, " And o.order_date <='" , pOrderDateTo, "'");
    End If;

    If pPaymentStatus <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And o.payment_status ='" , pPaymentStatus, "'");
    End If;

    -- End SQL Where
    -- Begin Paging Info
    Set @sqlCommand = "Select Count(o.order_no) Into @totalRec
                        From
                        (select order_no, order_date, payment_status, agent_seq from
                        m_agent m Join t_order o On m.seq = o.agent_seq) as o ";
    Set @sqlCommand = Concat(@sqlCommand, " Where o.agent_seq ='" ,pagentSeq, "'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);
        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;
        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info
    Set @sqlCommand = "
        Select
            o.seq,
            o.agent_name,
            o.order_no,
            o.order_date,
            o.total_order,
            o.payment_status,
            o.paid_date,
            o.payment_method,
            '' as detail,
            '' as payment
            From
        (
        select
            o.seq,
            o.agent_seq,
            m.name as agent_name,
            o.order_no,
            o.order_date,
            o.total_order,
            o.payment_status,
            o.paid_date,
            pgm.name as payment_method,
            '' as detail,
            '' as payment
		from
                    m_agent m
		Join
                    t_order o
                            On m.seq = o.agent_seq
		Join
                    m_payment_gateway_method pgm
                            On o.pg_method_seq = pgm.seq
		) as o
		";
    Set @sqlCommand = Concat(@sqlCommand, " Where o.agent_seq ='" ,pagentSeq, "'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;

    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);

    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    Select pCurrPage, totalPage, totalRec as total_rec;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set @totalRec = Null;
    Set @sqlCommand = Null;
END $$
DELIMITER ;
