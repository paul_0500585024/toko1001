DROP PROCEDURE IF EXISTS `sp_change_password_agent`;
DELIMITER $$
CREATE PROCEDURE `sp_change_password_agent` (
        pUserID VARCHAR(200), 
	pIPAddr VARCHAR(50),
	pAgentSeq BIGINT UNSIGNED,
	pNewPassword VARCHAR(1000)
)
BEGIN

UPDATE m_agent SET 
	`password` = pNewPassword,
        modified_by = pUserID,
        modified_date = NOW()
WHERE seq = pAgentSeq;

END$$


