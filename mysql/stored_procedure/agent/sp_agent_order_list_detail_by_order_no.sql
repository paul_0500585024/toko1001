DROP PROCEDURE IF EXISTS `sp_agent_order_list_detail_by_order_no`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_order_list_detail_by_order_no` (
    `pUserID` VARCHAR(100),
    `pIPAddr` VARCHAR(50),
    `pAgentSeq` BIGINT UNSIGNED,
    `pHSeq` VARCHAR(50)
)
BEGIN
    SELECT 
        op.order_seq,
        o.order_no,
        o.order_date,
        o.payment_status,
        o.pg_method_seq,
        o.total_payment,
        o.total_order,
        p.name AS product_name,
        v.display_name,
        vv.value,
        p.p_weight_kg,
        p.p_length_cm,
        p.p_width_cm,
        p.p_height_cm,
        op.qty,
        op.sell_price,
        op.commission_fee_percent,
        op.product_status,
        op.trx_fee_percent,
        pv.pic_1_img AS img_src,
        p.merchant_seq
    FROM
        t_order o
            JOIN
        t_order_product op ON o.seq = op.order_seq
            JOIN
        m_product_variant pv ON op.product_variant_seq = pv.seq
            JOIN
        m_product p ON pv.product_seq = p.seq
            JOIN
        m_variant_value vv ON op.variant_value_seq = vv.seq
            JOIN
        m_variant v ON vv.variant_seq = v.seq
    WHERE
        o.order_no = pHSeq
            AND o.agent_seq = pAgentSeq;
END$$

