CREATE DEFINER=`root`@`%` PROCEDURE `sp_agent_update_status_pg_order_loan`(
	pUserID VARCHAR(25), 
	pIPAddr VARCHAR(50),
	pSeq bigint(20),
	pStatus VARCHAR(1),    
	pPgMethodSeq smallint unsigned
    )
BEGIN
UPDATE t_order_loan SET status_order=pStatus, pg_method_seq=pPgMethodSeq where order_seq=pSeq;
    
END