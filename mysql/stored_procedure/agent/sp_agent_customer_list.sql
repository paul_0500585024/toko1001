CREATE DEFINER=`root`@`%` PROCEDURE `sp_agent_customer_list`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pCurrPage INT UNSIGNED,
        pRecPerPage INT UNSIGNED,
        pDirSort VARCHAR(4),
        pColumnSort VARCHAR(50),
        pAgentSeq INT(9) UNSIGNED,        
        pPicName VARCHAR(50),
        pIdentityNo VARCHAR(50),        
        pEmail VARCHAR(50)
    )
BEGIN
    -- Begin variables for Paging
    DECLARE startRec INT UNSIGNED;
    DECLARE totalPage INT UNSIGNED;
    DECLARE totalRec INT UNSIGNED;
    -- End variables for Paging
    -- Begin SQL Where
    DECLARE sqlWhere VARCHAR(500);
	SET sqlWhere = " where 1 ";
	SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    IF pIdentityNo <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " and identity_no like '%" , pIdentityNo, "%'");
    END IF;
    IF pPicName <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " and pic_name like '%" , pPicName, "%'");
    END IF;
    IF pAgentSeq <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " and agent_seq=" , pAgentSeq );
    END IF;
    IF pEmail <> "" THEN
         SET sqlWhere = CONCAT(sqlWhere, " and email_address like '%" , pEmail, "%'");
    END IF;
    -- End SQL Where
    -- Begin Paging Info
    SET @sqlCommand = "Select Count(*) Into @totalRec From `m_agent_customer` ";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, sqlWhere);
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        END IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    -- End Paging Info
    SET @sqlCommand = "
	Select 
		m_agent_customer.*,
		p.seq as province_seq,
		p.`name` as province_name,
		c.seq as city_seq,
		c.`name` as city_name,
		d.seq as district_seq,
		d.`name` as district_name
	From
		`m_agent_customer` 
        
Left Join m_district d
On d.seq= m_agent_customer.district_seq
Left Join m_city c
On c.seq= d.city_seq
Left Join m_province p
On p.seq= c.province_seq        ";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, sqlWhere);
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    SET @sqlCommand = CONCAT(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @totalRec = NULL;
    SET @sqlCommand = NULL;
END