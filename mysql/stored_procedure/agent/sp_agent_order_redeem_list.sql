DROP PROCEDURE IF EXISTS `sp_agent_order_redeem_list`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_order_redeem_list` (
 `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pAgentSeq` BIGINT(10),
         pCurrPage INT UNSIGNED,
         pRecPerPage INT UNSIGNED,
        `pPaymentStatus` CHAR(1)
)
BEGIN
  -- Begin variables for Paging
    DECLARE startRec INT UNSIGNED;
    DECLARE totalPage INT UNSIGNED;
    DECLARE totalRec INT UNSIGNED;
    -- End variables for Paging
    Declare sqlWhere VarChar(500);
    Set sqlWhere = "";
    SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    If pPaymentStatus = "P" Then
        Set sqlWhere = Concat(sqlWhere, " And tom.order_status = 'D' ");
    End If;
    If pPaymentStatus = "U" Then
        Set sqlWhere = Concat(sqlWhere, " And tom.order_status in ('R','S') ");
    End If;
    -- Begin Paging Info
    SET @sqlCommand = "Select Count(*) Into @totalRec From t_order tor
        join t_order_merchant tom on tor.seq=tom.order_seq
        join t_order_product top on tor.seq=top.order_seq and tom.merchant_info_seq=top.merchant_info_seq and top.order_seq=tom.order_seq ";
    Set @sqlCommand = Concat(@sqlCommand, " Where tor.seq NOT IN (SELECT 
            `to`.seq
        FROM
            t_order `to`
                LEFT JOIN
            t_order_merchant tom ON tom.order_seq = `to`.seq
                LEFT JOIN
            t_redeem_period trp ON trp.seq = tom.redeem_agent_seq
                LEFT JOIN
            t_redeem_agent tra ON tra.redeem_seq = trp.seq
        WHERE
            `to`.agent_seq = ",pAgentSeq," AND tra.status = 'P') AND tor.agent_seq ='" ,pAgentSeq, "' and tor.payment_status='P' and top.product_status='R' ");
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, sqlWhere);
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        END IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    -- End Paging Info
    SET @sqlCommand = "
        Select
            tor.seq,
            tor.agent_seq,
            tor.order_no,
            tor.order_date,
            tor.receiver_name,
            tor.receiver_address,
            tor.receiver_district_seq,
            tor.receiver_zip_code,
            tor.receiver_phone_no,
            tom.order_status,
            tom.received_date,
            top.product_variant_seq,
            top.variant_value_seq,
            top.qty,
            top.sell_price,
            top.weight_kg,
            top.ship_price_real,
            top.commission_fee_percent,
            top.nominal_commision_agent as komisiAgent,
            md.`name` district_name,
            mc.`name` city_name,
            mp.`name` province_name,
            mpr.`name` AS display_name,
            mpr.merchant_seq,
            mpv.seq AS product_seq,
            mpv.pic_1_img AS img,
            mv.`value`,
            mv.seq AS value_seq,
            mv.`value` AS variant_name
       from t_order tor
		join t_order_merchant tom on tor.seq=tom.order_seq
		join t_order_product top on tor.seq=top.order_seq and tom.merchant_info_seq=top.merchant_info_seq and top.order_seq=tom.order_seq
                JOIN m_district md ON tor.receiver_district_seq = md.seq
                JOIN m_city mc ON mc.seq = md.city_seq
                JOIN m_province mp ON mp.seq = mc.province_seq
                JOIN m_product_variant mpv ON mpv.seq = top.product_variant_seq
		JOIN m_product mpr ON mpv.product_seq = mpr.seq
		JOIN m_variant_value vv ON vv.seq = top.variant_value_seq
		JOIN m_variant_value mv ON mv.seq = mpv.variant_value_seq
		";
    Set @sqlCommand = Concat(@sqlCommand, " Where tor.seq NOT IN (SELECT 
            `to`.seq
        FROM
            t_order `to`
                LEFT JOIN
            t_order_merchant tom ON tom.order_seq = `to`.seq
                LEFT JOIN
            t_redeem_period trp ON trp.seq = tom.redeem_agent_seq
                LEFT JOIN
            t_redeem_agent tra ON tra.redeem_seq = trp.seq
        WHERE
            `to`.agent_seq = ",pAgentSeq," AND tra.status = 'P') AND tor.agent_seq ='" ,pAgentSeq, "' and tor.payment_status='P' and top.product_status='R' ");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;

    Set @sqlCommand = Concat(@sqlCommand, " order by tor.order_date desc, tor.seq desc");
    SET @sqlCommand = CONCAT(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set @sqlCommand = Null;
END$$

