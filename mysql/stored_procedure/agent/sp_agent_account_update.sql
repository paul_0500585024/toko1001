DROP PROCEDURE IF EXISTS `sp_agent_account_update`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_account_update` (
    pTrxNo varchar(20)
)
BEGIN
update t_agent_account set
	`status` = 'N'
Where
	trx_no = pTrxNo;
END$$
