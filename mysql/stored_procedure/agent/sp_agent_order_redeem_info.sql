DROP PROCEDURE IF EXISTS `sp_agent_order_redeem_info`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_order_redeem_info` (
    `pUserID` VARCHAR(100),
    `pIPAddr` VARCHAR(50),
    `pAgentSeq` BIGINT(10)
)
BEGIN

SELECT 
    sum(nominal_commision_agent) AS pending_commission
FROM
    t_order tor
        JOIN
    t_order_merchant tom ON tor.seq = tom.order_seq
        JOIN
    t_order_product top ON tor.seq = top.order_seq
        AND tom.merchant_info_seq = top.merchant_info_seq
        AND top.order_seq = tom.order_seq
        JOIN
    m_district md ON tor.receiver_district_seq = md.seq
        JOIN
    m_city mc ON mc.seq = md.city_seq
        JOIN
    m_province mp ON mp.seq = mc.province_seq
        JOIN
    m_product_variant mpv ON mpv.seq = top.product_variant_seq
        JOIN
    m_product mpr ON mpv.product_seq = mpr.seq
        JOIN
    m_variant_value vv ON vv.seq = top.variant_value_seq
        JOIN
    m_variant_value mv ON mv.seq = mpv.variant_value_seq
WHERE
    tor.seq NOT IN (SELECT 
            `to`.seq
        FROM
            t_order `to`
                LEFT JOIN
            t_order_merchant tom ON tom.order_seq = `to`.seq
                LEFT JOIN
            t_redeem_period trp ON trp.seq = tom.redeem_agent_seq
                LEFT JOIN
            t_redeem_agent tra ON tra.redeem_seq = trp.seq
        WHERE
            `to`.agent_seq = pAgentSeq
                AND tra.status = 'P')
        AND tor.agent_seq = pAgentSeq
        AND tor.payment_status = 'P'
        AND top.product_status = 'R'
        And tom.order_status in ('R','S');
SELECT 
    sum((top.qty * top.sell_price) * (top.commission_fee_percent/100)) AS commission
FROM
    t_order tor
        JOIN
    t_order_merchant tom ON tor.seq = tom.order_seq
        JOIN
    t_order_product top ON tor.seq = top.order_seq
        AND tom.merchant_info_seq = top.merchant_info_seq
        AND top.order_seq = tom.order_seq
        JOIN
    m_district md ON tor.receiver_district_seq = md.seq
        JOIN
    m_city mc ON mc.seq = md.city_seq
        JOIN
    m_province mp ON mp.seq = mc.province_seq
        JOIN
    m_product_variant mpv ON mpv.seq = top.product_variant_seq
        JOIN
    m_product mpr ON mpv.product_seq = mpr.seq
        JOIN
    m_variant_value vv ON vv.seq = top.variant_value_seq
        JOIN
    m_variant_value mv ON mv.seq = mpv.variant_value_seq
WHERE
    tor.seq NOT IN (SELECT 
            `to`.seq
        FROM
            t_order `to`
                LEFT JOIN
            t_order_merchant tom ON tom.order_seq = `to`.seq
                LEFT JOIN
            t_redeem_period trp ON trp.seq = tom.redeem_agent_seq
                LEFT JOIN
            t_redeem_agent tra ON tra.redeem_seq = trp.seq
        WHERE
            `to`.agent_seq = pAgentSeq
                AND tra.status = 'P')
        AND tor.agent_seq = pAgentSeq
        AND tor.payment_status = 'P'
        AND top.product_status = 'R'
        And tom.order_status = 'D';
END$$

