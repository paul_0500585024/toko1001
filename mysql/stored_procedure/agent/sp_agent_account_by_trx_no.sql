DROP PROCEDURE IF EXISTS `sp_agent_account_by_trx_no`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_account_by_trx_no` (
	pTrxNo varchar(20)
)
BEGIN
select 
     trx_no,
    `status`
from t_agent_account
where trx_no = pTrxNo;
END$$
