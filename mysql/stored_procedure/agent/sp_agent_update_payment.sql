DROP PROCEDURE IF EXISTS `sp_agent_update_payment`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_update_payment` (
        pIPAddr VarChar(100),
        pAgentSeq BIGINT(20) Unsigned,
        pBankName VarChar(25),
        pBankBranchName VarChar(50),
        pBankAcctNo Varchar(50),
        pBankAcctName Varchar(50)

)
BEGIN
Update m_agent Set
    bank_name = pBankName,
    bank_branch_name = pBankBranchName,
    bank_acct_no = pBankAcctNo,
    bank_acct_name = pBankAcctName,
    modified_by = pIPAddr,
    modified_date = now()
Where
	seq = pAgentSeq;
END$$
