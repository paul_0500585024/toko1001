DROP PROCEDURE IF EXISTS `sp_agent_payment_retry_update`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_payment_retry_update` (
    `pUserID` VARCHAR(100),
    `pIPAddr` VARCHAR(50),
    `pAgentSeq` BIGINT UNSIGNED,
    `pOrderNo` VARCHAR(50),
    `pPaymentStatus` CHAR(1),
    `pPgMethodSeq` BIGINT UNSIGNED
)
BEGIN
    update t_order set
            payment_status= pPaymentStatus,
        pg_method_seq = pPgMethodSeq,
            payment_retry = (payment_retry + 1)
    Where
            agent_seq = pAgentSeq AND order_no = pOrderNo;
END$$

