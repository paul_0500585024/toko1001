CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_merchant_change_password_log`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
	pMerchantSeq Int unsigned,
	pOldPassword VarChar(1000)
)
BEGIN
	Declare new_seq Int unsigned;

	Select 
		Max(seq) + 1 Into new_seq
	From m_merchant_log_security;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
    
	Insert Into `m_merchant_log_security`(  
		merchant_seq,
		seq,
		`type`,
        code,
		old_pass,
		ip_addr,
		created_by,
		created_date
	) Values (
		pMerchantSeq,
		new_seq,
		'1',
        '',
		pOldPassword,
		pIPAddr ,
		pUserID,
		Now()
	);

END