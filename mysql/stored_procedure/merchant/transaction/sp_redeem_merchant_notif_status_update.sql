DROP PROCEDURE IF EXISTS `sp_redeem_merchant_notif_status_update`;
DELIMITER $$
CREATE PROCEDURE `sp_redeem_merchant_notif_status_update`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq int(10) unsigned,
        pMerchantSeq int(10) unsigned
    )
BEGIN
update t_redeem_merchant set
    `notif_status` = 'R',
    modified_by = pUserID,
    modified_date = now()
Where
    merchant_seq = pMerchantSeq and `redeem_seq`=pSeq and `notif_status`='U';
END$$


