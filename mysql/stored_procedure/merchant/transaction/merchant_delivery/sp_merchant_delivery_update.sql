CREATE DEFINER=`root`@`%` PROCEDURE `sp_merchant_delivery_update`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq BigInt (20) unsigned,
        pMerchantId Int(9) unsigned,
        pMnote VarChar(45),
        pStatus Char(1)
    )
BEGIN

	If pStatus <> "X" Then
		Update t_order_merchant set
			ship_notes = pMnote,
			order_status = pStatus,
			modified_by = pUserID,
			modified_date = now()
		Where
			order_seq = pSeq And 
			merchant_info_seq In (Select seq From m_merchant_info Where merchant_seq = pMerchantId) And
			order_status = 'N';
	Else
		Update t_order_merchant Set
			ship_notes = pMnote,
			order_status = 'X',
			modified_by = pUserID,
			modified_date = now()
		Where
			order_seq = pSeq And 
			merchant_info_seq In (Select seq From m_merchant_info Where merchant_seq = pMerchantId) And
			order_status = 'N';
	End If;

END