CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_merchant_delivery_product_by_order`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
	pOrderSeq BigInt unsigned,
	pMerchantId Int unsigned,
	pStatus Char(1)
)
BEGIN

	If pStatus <> "" Then
		Select
			t_op.`order_seq`, 
			t_op.`merchant_info_seq`, 
			t_op.`product_variant_seq`, 
			t_op.`variant_value_seq`, 
			t_op.`qty`, 
			t_op.`sell_price`, 
			t_op.`weight_kg`, 
			t_op.`ship_price_real`, 
			t_op.`ship_price_charged`, 
			t_op.`ins_rate_percent`, 
			t_op.`product_status`, 
			v_pv.`merchant_seq`, 
			v_pv.`seq`, 
			v_pv.`name`, 
			v_pv.`include_ins`, 
			v_pv.`category_l2_seq`, 
			v_pv.`category_ln_seq`, 
			v_pv.`notes`, 
			v_pv.`description`, 
			v_pv.`content`, 
			v_pv.`warranty_notes`, 
			v_pv.`p_weight_kg`, 
			v_pv.`p_length_cm`, 
			v_pv.`p_width_cm`, 
			v_pv.`p_height_cm`, 
			v_pv.`b_weight_kg`, 
			v_pv.`b_length_cm`, 
			v_pv.`b_width_cm`, 
			v_pv.`b_height_cm`, 
			v_pv.`product_seq`, 
			v_pv.`variant_value_seq`, 
			v_pv.`variant_seq`, 
			v_pv.`product_price`, 
			v_pv.`disc_percent`, 
			v_pv.`sell_price` as product_sell_price, 
			v_pv.`order`, 
			v_pv.`max_buy`, 
			v_pv.`status`, 
			v_pv.`pic_1_img`, 
			v_pv.`pic_2_img`, 
			v_pv.`pic_3_img`, 
			v_pv.`pic_4_img`, 
			v_pv.`pic_5_img`, 
			v_pv.`pic_6_img`, 
			v_pv.`1star`, 
			v_pv.`2star`, 
			v_pv.`3star`, 
			v_pv.`4star`, 
			v_pv.`5star`, 
			v_pv.`merchant_sku`, 
			v_pv.`stock`,
            pv.value as variant_name
		From `t_order_product` t_op
		Join (
			Select seq From m_merchant_info Where merchant_seq = pMerchantId
		) mmi On 
			t_op.`merchant_info_seq` = mmi.seq
		Join `v_product_variant` v_pv On 
			t_op.`product_variant_seq` = v_pv.`variant_seq`
		join m_variant_value pv on
			v_pv.variant_value_seq = pv.seq
		Where 
			t_op.`order_seq` = pOrderSeq And 
			t_op.`product_status` = pStatus ;
	Else

		Select
			t_op.`order_seq`, 
			t_op.`merchant_info_seq`, 
			t_op.`product_variant_seq`, 
			t_op.`variant_value_seq`, 
			t_op.`qty`, 
			t_op.`sell_price`, 
			t_op.`weight_kg`, 
			t_op.`ship_price_real`, 
			t_op.`ship_price_charged`, 
			t_op.`ins_rate_percent`, 
			t_op.`product_status`,
			v_pv.`merchant_seq`, 
			v_pv.`seq`, 
			v_pv.`name`, 
			v_pv.`include_ins`, 
			v_pv.`category_l2_seq`, 
			v_pv.`category_ln_seq`, 
			v_pv.`notes`, v_pv.`description`, 
			v_pv.`content`, 
			v_pv.`warranty_notes`, 
			v_pv.`p_weight_kg`, 
			v_pv.`p_length_cm`, 
			v_pv.`p_width_cm`, 
			v_pv.`p_height_cm`, 
			v_pv.`b_weight_kg`, 
			v_pv.`b_length_cm`, 
			v_pv.`b_width_cm`, 
			v_pv.`b_height_cm`, 
			v_pv.`product_seq`, 
			v_pv.`variant_value_seq`, 
			v_pv.`variant_seq`, 
			v_pv.`product_price`, 
			v_pv.`disc_percent`, 
			v_pv.`sell_price` as product_sell_price, 
			v_pv.`order`, 
			v_pv.`max_buy`, 
			v_pv.`status`, 
			v_pv.`pic_1_img`, 
			v_pv.`pic_2_img`, 
			v_pv.`pic_3_img`, 
			v_pv.`pic_4_img`, 
			v_pv.`pic_5_img`, 
			v_pv.`pic_6_img`, 
			v_pv.`1star`, 
			v_pv.`2star`, 
			v_pv.`3star`, 
			v_pv.`4star`, 
			v_pv.`5star`, 
			v_pv.`merchant_sku`, 
			v_pv.`stock`,
			m_vv.`value`
		From `t_order_product` t_op
		Join `v_product_variant` v_pv On 
			t_op.`product_variant_seq` = v_pv.`variant_seq` 
		Join (
			Select seq From m_merchant_info Where merchant_seq = pMerchantId
		) mmi On
			t_op.`merchant_info_seq` = mmi.seq
		Join m_variant_value m_vv On 
			v_pv.variant_seq = m_vv.seq
		Where 
			t_op.`order_seq` = pOrderSeq  ;

	End If;

END