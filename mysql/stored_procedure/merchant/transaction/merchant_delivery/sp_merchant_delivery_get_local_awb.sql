CREATE DEFINER=`root`@`%` PROCEDURE `sp_merchant_delivery_get_local_awb`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pExpSeq INT unsigned
    )
BEGIN
	Select
		* 
	From m_expedition_awb
	Where
		exp_seq = pExpSeq And 
		trx_no = ''
	Order By 
		seq Limit 1;
END