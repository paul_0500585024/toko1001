CREATE DEFINER=`root`@`%` PROCEDURE `sp_merchant_deliver_save_awb_no`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq BigInt (2)unsigned,
        pMerchantId INT(9) unsigned,
        pAwbNo VarChar(100),
        pShipBy VarChar(50),
        pShipByExp TinyInt(3) unsigned,
        pShipDate date,
        pShipNoteFile VarChar(150)
    )
BEGIN
	Update t_order_merchant Set
		awb_no = pAwbNo,
        ship_by = pShipBy,
		ship_by_exp_seq = pShipByExp,
		ship_date = pShipDate,
		ship_note_file = pShipNoteFile,
        order_status = 'S'
	Where
		order_seq = pSeq And 
		merchant_info_seq In (Select seq From m_merchant_info Where merchant_seq = pMerchantId) And
        (order_status = 'R' or order_status = 'S');

END