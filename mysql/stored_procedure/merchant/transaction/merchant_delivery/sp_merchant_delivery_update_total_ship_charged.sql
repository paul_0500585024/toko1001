DROP PROCEDURE IF EXISTS `sp_merchant_delivery_update_total_ship_charged`;
DELIMITER $$
CREATE PROCEDURE `sp_merchant_delivery_update_total_ship_charged` (
    
    pUserID Varchar(50),
    pIPAddress Varchar(25),
    pOrderSeq BigInt unsigned,
    pMerchantSeq Int unsigned
)
BEGIN

Declare total_ship Decimal(10,0);
Declare total_real_ship Decimal(10,0);

select
    CEIL(SUM(op.qty * op.weight_kg)) * Max(op.ship_price_charged),
    CEIL(SUM(op.qty * op.weight_kg)) * Max(op.ship_price_real)
 into 
    total_ship,
    total_real_ship
from
    t_order_merchant om join t_order_product op
        on op.order_seq = om.order_seq and
           op.merchant_info_seq = om.merchant_info_seq 
                        join m_merchant_info mi
        on mi.seq = om.merchant_info_seq
where
    om.order_seq = pOrderSeq and
    mi.merchant_seq = pMerchantSeq and
    op.product_status <>'X';

update 
t_order_merchant om join m_merchant_info mi
    on mi.seq = om.merchant_info_seq 
set
    om.total_ship_real = total_real_ship,
    om.total_ship_charged = total_ship,
    om.modified_by = pUserID,
    om.modified_date = now()
where
    om.order_seq = pOrderSeq and
    mi.merchant_seq = pMerchantSeq;    


END$$
DELIMITER ;

