CREATE DEFINER = 'root'@'%' PROCEDURE `sp_merchant_delivery_list`(
        `pUserID` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pCurrPage` INTEGER UNSIGNED,
        `pRecPerPage` INTEGER UNSIGNED,
        `pDirSort` VARCHAR(4),
        `pColumnSort` VARCHAR(50),
        `pMerchantId` INTEGER(9) UNSIGNED,
        `pOrderNo` VARCHAR(50),
        `pDates` VARCHAR(10),
        `pDatee` VARCHAR(10),
        `pStatus` CHAR(1)
    )
BEGIN
    -- Begin variables for Paging
    Declare startRec Int unsigned;
    Declare totalPage Int unsigned;
    Declare totalRec Int unsigned;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(800);
    Declare sqlWhereC VarChar(800);
	Set sqlWhere = "";
	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;

    If pOrderNo <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And t_o.order_no like '%" , escape_string(pOrderNo), "%'");
    End If;    
    If pDates <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And t_o.order_date >= '" , escape_string(pDates), "'");
    End If;
    If pDatee <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And t_o.order_date <= '" , escape_string(pDatee), "'");
    End If;
    If pMerchantId <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And t_om.merchant_info_seq in (select seq from m_merchant_info where merchant_seq='" , pMerchantId, "')");
    End If;
    Set sqlWhereC =sqlWhere;
    If pStatus <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And t_om.order_status = '" , escape_string(pStatus), "'");
    End If;
    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "Select Count(t_o.seq) Into @totalRec From `t_order` t_o inner join `t_order_merchant` t_om on t_o.`seq`=t_om.`order_seq` where t_o.payment_status='P' ";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
	Select 
		t_o.`seq`,
		t_o.`order_no`,
		t_o.`order_date`,
		t_o.`member_seq`,
		t_o.`agent_seq`,
		t_o.`receiver_name`,
		t_o.`receiver_address`,
		t_o.`receiver_district_seq`,
		t_o.`receiver_zip_code`,
		t_o.`receiver_phone_no`,
		t_o.`payment_status`,
		t_o.`pg_method_seq`,
		t_o.`payment_retry`,
		t_o.`total_order`,
		t_o.`voucher_seq`,
		t_o.`voucher_refunded`,
		t_o.`total_payment`,
		t_o.`conf_pay_type`,
		t_o.`conf_pay_amt_member`,
		t_o.`conf_pay_date`,
		t_o.`conf_pay_note_file`,
		t_o.`conf_pay_bank_seq`,
		t_o.`conf_pay_amt_admin`,
		t_om.`merchant_info_seq`,
		t_om.`expedition_service_seq`,
		t_om.`real_expedition_service_seq`,        
		t_om.`total_ins`,
		t_om.`total_ship_real`,
		t_om.`total_ship_charged`,
		t_om.`order_status`,
		t_om.`member_notes`,
		t_om.`printed`,
		t_om.`print_date`,
		t_om.`awb_seq`,
		t_om.`awb_no`,
		t_om.`ship_by`,
		t_om.`ship_by_exp_seq`,
		t_om.`ship_date`,
		t_om.`ship_note_file`,
		t_om.`ship_notes`,
		t_om.`received_date`,
		t_om.`received_by`,
		md.districtname,
        md.cityname,
        md.provincename,
        t_op.`total_merchant`,
        '' as print_c
	From
		`t_order` t_o
	Inner Join `t_order_merchant` t_om On
		t_o.`seq` = t_om.`order_seq`
	Left Outer Join (select mds.`seq`,mds.name as districtname,mct.`name` as cityname,`mpro`.`name` as provincename from `m_district` mds join m_city mct on mds.`city_seq`=mct.`seq` 
	join `m_province` mpro on mct.`province_seq`=`mpro`.`seq`) md On
		t_o.`receiver_district_seq` = md.seq
        left outer join (select order_seq,merchant_info_seq,IFNULL(sum(sell_price*qty),0) as total_merchant from t_order_product 
        where product_status='R' group by order_seq,merchant_info_seq)
         t_op on t_om.`order_seq`=t_op.order_seq and t_om.`merchant_info_seq`=t_op.merchant_info_seq
	where 
		t_o.payment_status='P'
        ";

    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    Select pCurrPage, totalPage, totalRec as total_rec;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    -- Begin Qty Status
    Set @sqlCommand = " SELECT COUNT(t_om.order_status) AS qty_status ,t_om.order_status FROM `t_order_merchant` t_om join `t_order` t_o on t_om.`order_seq`=t_o.`seq` WHERE t_om.order_status<> 'P' and t_o.payment_status='P'  ";
    If sqlWhereC <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhereC);
    End If;
	Set @sqlCommand = Concat(@sqlCommand, " GROUP BY t_om.order_status");

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
	
	Set @totalRec = Null;
    Set @sqlCommand = Null;
END;