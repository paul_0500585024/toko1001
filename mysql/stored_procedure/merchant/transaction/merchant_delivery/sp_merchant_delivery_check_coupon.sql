CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_merchant_delivery_check_coupon`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pOrderSeq BIGINT (20)unsigned,
        pCoupon INT(11) unsigned,
        pStatus Char(1),
        pMerchantId INT(11) unsigned
    )
BEGIN
Set @sqlCommand = "	
select 
	mc.`seq`,
	mc.`coupon_code`,
	mc.`coupon_name`,
	mc.`coupon_period_from`,
	mc.`coupon_period_to`,
	mc.`coupon_limit`,
	mc.`nominal`,
	mc.`status`,
	mcp.`coupon_seq`,
	mcp.`product_seq`
from `m_coupon` mc inner join `m_coupon_product` mcp on mc.`seq`=mcp.`coupon_seq`
where mcp.`product_seq` in (select mpv.product_seq from `m_product_variant` mpv inner join
	`t_order_product` top on mpv.`seq`=top.`product_variant_seq` inner join 
	`t_order_merchant` tom on top.`order_seq`=tom.`order_seq` and top.`merchant_info_seq`=tom.`merchant_info_seq` inner JOIN
	`t_order` tor on tom.`order_seq`=tor.`seq` 
    " ;
    Set @sqlCommand = Concat(@sqlCommand, " where tor.`coupon_seq` = ",pCoupon," and tor.`seq` = ",pOrderSeq," and tom.`merchant_info_seq` In (Select seq From m_merchant_info Where merchant_seq = ", pMerchantId ," ) " );
    
    If pStatus <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " and top.product_status ='" , pStatus, "' " );
    End If; 
    Set @sqlCommand = Concat(@sqlCommand, " ) and mc.`seq` = ", pCoupon);

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set @sqlCommand = Null;
    
END