CREATE DEFINER=`root`@`%` PROCEDURE `sp_merchant_delivery_ekspedisi_merchant`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
	pSeq BigInt unsigned,
	pMerchantSeq Int unsigned
)
BEGIN

	Select 
		a.`order_seq`,
		a.`merchant_info_seq`,
		a.`expedition_service_seq`,
		a.`real_expedition_service_seq`,
		a.`total_merchant`,
		a.`total_ins`,
		a.`total_ship_real`,
		a.`total_ship_charged`,
		a.`order_status`,
		a.`member_notes`,
		a.`printed`,
		a.`print_date`,
		a.`awb_seq`,
		a.`awb_no`,
		a.`ref_awb_no`,
		a.`ship_by`,
		a.`ship_by_exp_seq`,
		a.`ship_date`,
		a.`ship_note_file`,
		a.`ship_notes`,
		a.`received_date`,
		a.`received_by`,
		b.`exp_seq`,
		b.`code`,
		b.`name` servisname,
		c.code as exp_code,
		c.`name` expedisiname,
		c.`logo_img` expedisilogo,
		c.`awb_method`,
		c.`ins_rate_percent`,
		c.`volume_divider`,
		d.`email`,
		d.`merchantname`,
		d.`address`,
		d.`district_seq`,
		d.`zip_code`,
		d.`phone_no`,
		d.`fax_no`,
		d.`pic1_name`,
		d.`pic1_phone_no`,
		d.`pic2_name`,
		d.`pic2_phone_no`,
		d.`expedition_seq`,
		d.`return_addr_eq`,
		d.`return_addr`,
		d.`return_district_seq`,
		d.`return_zip_code`,
		d.`bank_name`,
		d.`bank_branch_name`,
		d.`bank_acct_no`,
		d.`bank_acct_name`,
		d.`exp_fee_percent`,
		d.`ins_fee_percent`,
		d.`logo_img`,
		d.`banner_img`,
		d.`pickup_addr`,
		d.`pickup_district_seq`,
		d.`pickup_zip_code`,
		e.seq,
		e.order_no,
		e.order_date,
		e.member_seq,
		e.receiver_name,
		e.receiver_address,
		e.receiver_district_seq,
		e.receiver_zip_code,
		e.receiver_phone_no,
		e.payment_status,
		e.pg_method_seq,
		e.paid_date,
		e.payment_retry,
		e.total_order,
		e.voucher_seq,
		e.voucher_refunded,
		e.total_payment,
		e.conf_pay_type,
		e.conf_pay_amt_member,
		e.conf_pay_date,
		e.conf_pay_note_file,
		e.conf_pay_bank_seq,
		e.conf_pay_amt_admin,
		f.city_to,
		g.city_from
	From t_order_merchant a
	Join m_expedition_service b on
		a.`real_expedition_service_seq` = b.`seq`
	Join m_expedition c on     
		b.`exp_seq`=c.`seq`
	Join(
			Select mm.`email`,mm.`name` merchantname, mm.`address`, mm.`district_seq`, mm.`zip_code`, mm.`phone_no`,mm.`fax_no`,mm.`pic1_name`,mm.`pic1_phone_no`,mm.`pic2_name`,mm.`pic2_phone_no`,mm.`expedition_seq`,mm.`return_addr_eq`,mm.`return_addr`,mm.`return_district_seq`,mm.`return_zip_code`,mm.`bank_name`,mm.`bank_branch_name`,mm.`bank_acct_no`,mm.`bank_acct_name`,mm.`exp_fee_percent`,mm.`ins_fee_percent`,mm.`logo_img`,mm.`banner_img`,mmi.seq as m_seq_info,mmi.`pickup_addr`, mmi.`pickup_district_seq`, mmi.`pickup_zip_code` from `m_merchant` mm join m_merchant_info mmi on mm.`seq`=mmi.`merchant_seq` Where mm.`seq` = pMerchantSeq
		) d on
		a.`merchant_info_seq`=d.`m_seq_info`
	Join t_order e on
		a.`order_seq`=e.`seq`
	Left Outer Join(
			Select exp_seq, district_seq, exp_district_code as city_to From m_expedition_district Where active = '1'
		) f On 
		e.`receiver_district_seq` = f.district_seq And 
		d.`expedition_seq`=f.exp_seq
	Left Outer Join(
			Select exp_seq, district_seq, exp_district_code as city_from From m_expedition_district Where active='1'
		) g On 
		d.`pickup_district_seq` = g.district_seq And 
		d.`expedition_seq`=g.exp_seq
	Where
		a.order_seq = pSeq;

END