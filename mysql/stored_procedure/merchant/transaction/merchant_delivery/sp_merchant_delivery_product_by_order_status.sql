DROP PROCEDURE IF EXISTS `sp_merchant_delivery_product_by_order_status`;
DELIMITER $$
CREATE PROCEDURE`sp_merchant_delivery_product_by_order_status`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
	pOrderSeq BigInt unsigned,
	pMerchantId Int unsigned,
	pStatus Char(1)
)
BEGIN

	If pStatus<>"" Then
		Select
			t_op.order_seq,
            t_op.merchant_info_seq,
            t_op.product_variant_seq,
            t_op.variant_value_seq,
            t_op.weight_kg,
            t_op.ship_price_real,
            t_op.ship_price_charged,
            t_op.trx_fee_percent,
            t_op.ins_rate_percent,
            t_op.product_status,
            t_op.qty_return,
            t_op.qty,
            t_op.sell_price,
            t_o.order_no,
            t_o.order_date,
            t_o.receiver_name,
            t_o.receiver_address,
            t_o.receiver_zip_code,
            t_o.receiver_phone_no,
            d.`name` as district_name,
            c.`name` as city_name,
            p.`name` as province_name,
            case when m_m.seq is null then m_a.name else m_m.`name` end as member_name,
            case when m_m.seq is null then m_a.email else m_m.email end as member_email,
            p.`name` as product_name,
            vv.`value` as variant_value
		From `t_order_product` t_op
		Join (Select seq From m_merchant_info Where merchant_seq = pMerchantId) mmi 
			On t_op.`merchant_info_seq`=mmi.seq
		Join t_order t_o 
			On t_op.order_seq = t_o.seq
		Join m_district d
			On t_o.receiver_district_seq = d.seq
		Left Join m_city c
			On d.city_seq = c.seq
		Left Join m_province pr
			On c.province_seq = pr.seq
                Left Join m_member m_m 
			On t_o.member_seq = m_m.seq
                Left Join m_agent m_a
                        on t_o.agent_seq = m_a.seq
                Join m_product_variant pv 
			On t_op.product_variant_seq = pv.seq
                Join m_product p 
			On pv.product_seq = p.seq
                Left Join m_variant_value vv
			On pv.variant_value_seq = vv.seq
		Where
			t_op.`order_seq` = pOrderSeq And 
			t_op.product_status=pStatus;
	Else
		Select
			t_op.*
		From `t_order_product` t_op
		Join (
			Select seq From m_merchant_info Where merchant_seq = pMerchantId
		) mmi On 
			t_op.`merchant_info_seq` = mmi.seq
		Where
			t_op.`order_seq` = pOrderSeq;
	End If;
END$$
DELIMITER ;