DROP PROCEDURE IF EXISTS `sp_merchant_delivery_get_cancel_ship`;
DELIMITER $$
CREATE PROCEDURE `sp_merchant_delivery_get_cancel_ship` (
    pUserID Varchar(50),
    pIPAddress Varchar(25),
    pOrderSeq BigInt unsigned,
    pMerchantSeq Int unsigned
)
BEGIN

Declare total_cancel_ship Decimal(10,0);
Declare total_ship Decimal(10,0);

select
    CEIL(SUM(op.Qty * op.weight_kg)) * Max(op.ship_price_charged) into total_ship
From 
    t_order_merchant om join t_order_product op
        on op.order_seq = om.order_seq and
           op.merchant_info_seq = om.merchant_info_seq 
                        join m_merchant_info mi
        on mi.seq = om.merchant_info_seq
where
    om.order_seq = pOrderSeq and
    mi.merchant_seq = pMerchantSeq and
    op.product_status <> 'X';   

select
    total_ship_charged - total_ship into total_cancel_ship
from 
    t_order_merchant om join m_merchant_info mi
        on mi.seq = om.merchant_info_seq
where
    om.order_seq = pOrderSeq and
    mi.merchant_seq = pMerchantSeq;

select total_cancel_ship;

END$$
DELIMITER ;

