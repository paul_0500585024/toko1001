DROP PROCEDURE IF EXISTS `sp_merchant_deliver_refund_agent_add`;
DELIMITER $$
CREATE PROCEDURE `sp_merchant_deliver_refund_agent_add` (
    
    	pUserID VarChar(25),
	pIPAddr VarChar(50),
	pAgentSeq Int unsigned,
	pConfTopup VarChar(50),
	pMtype Char(1),
	pPgMSeq SmallInt unsigned,
	pTrxNo VarChar(50),
	pDepositTrxAmt Decimal(10,0),
	pNonDeposit Decimal(10,0)

)
BEGIN

    Declare new_seq Int unsigned;

    Select 
            Max(seq) + 1 Into new_seq
    From `t_agent_account` 
    where
            `agent_seq` = pAgentSeq;

    If new_seq Is Null Then
            Set new_seq = 1;
    End If;
    
    Insert Into `t_agent_account`(
		`agent_seq`,
		`seq`,
		`mutation_type`,
		`pg_method_seq`,
		`trx_type`,
		`trx_no`,
		`trx_date`,
		`deposit_trx_amt`,
		`non_deposit_trx_amt`,
		`bank_name`,
		`bank_branch_name`,
		`bank_acct_no`,
		`bank_acct_name`,
		`refund_date`,
                `conf_topup_date_agent`,
                `conf_topup_type_agent`,
                `conf_topup_file_agent`,
                `conf_topup_date_admin`,
                `conf_topup_bank_seq`,
		`status`,
		`created_by`,
		`created_date`,
		`modified_by`,
		`modified_date`
	) values (
		pAgentSeq,
		new_seq,
		pMtype,
		pPgMSeq,
		'CNL',
		pTrxNo,
		now(),
		pDepositTrxAmt,
		pNonDeposit,
		'',
		'',
		'',
		'',
		'0000-00-00',
                '0000-00-00',
                'N',
                '',
                '0000-00-00',
                pConfTopup,
		'N',
		pUserID,
		now(),
		pUserID,
		now()
    );


END$$
DELIMITER ;

