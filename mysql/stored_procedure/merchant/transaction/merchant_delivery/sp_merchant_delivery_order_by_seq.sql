CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_merchant_delivery_order_by_seq`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
	pOrderSeq BigInt unsigned,
	pMerchantId Int unsigned
)
BEGIN
	Select 
		t_om.`order_seq`,
		t_om.`merchant_info_seq`,
		t_om.`expedition_service_seq`,
		t_om.`real_expedition_service_seq`,
		t_om.`total_merchant`,
		t_om.`total_ins`,
		t_om.`total_ship_real`,
		t_om.`total_ship_charged`,
		t_om.`free_fee_seq`,
		t_om.`order_status`,
		t_om.`member_notes`,
		t_om.`printed`,
		t_om.`print_date`,
		t_om.`awb_seq`,
		t_om.`awb_no`,
		t_om.`ref_awb_no`,
		t_om.`ship_by`,
		t_om.`ship_by_exp_seq`,
		t_om.`ship_date`,
		t_om.`ship_note_file`,
		t_om.`ship_notes`,
		t_om.`received_date`,
		t_om.`received_by`,
		t_om.`redeem_seq`,
		t_om.`exp_invoice_seq`,
		t_om.`exp_invoice_awb_seq`,
		t_om.`created_by`,
		t_om.`created_date`,
		t_om.`modified_by`,
		t_om.`modified_date`
	From `t_order_merchant` t_om 
	Join (
		Select seq From m_merchant_info Where merchant_seq = pMerchantId
	) mmi on
		t_om.`merchant_info_seq` = mmi.seq
	Where
		t_om.`order_seq` = pOrderSeq;

END