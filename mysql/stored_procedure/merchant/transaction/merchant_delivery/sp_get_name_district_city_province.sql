CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_name_district_city_province`(
        pDistrictSeq SMALLINT(5)
    )
BEGIN

Select
	p.seq as idp,p.`name` namep,
    c.seq as idc,c.`name` namec,
    d.seq as idd,d.`name` named	
From
	`m_district` d inner join `m_city` c
	 On d.city_seq = c.seq inner join `m_province` p
     on c.province_seq=p.`seq`
Where	
	d.seq = pDistrictSeq And
    d.active = '1' and c.active = '1'  and p.active = '1' ;

END