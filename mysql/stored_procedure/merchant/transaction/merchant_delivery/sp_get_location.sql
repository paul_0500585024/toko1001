CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_location`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq Int,
        cSeq Int,
        dSeq int
    )
BEGIN
    -- Begin SQL Where
    Declare sqlWhere VarChar(500);
    Set sqlWhere = "";
    Set @sqlCommand = "
        Select
		  *
        From `v_location`
    ";
	If pSeq <> "" Then
        Set sqlWhere = Concat(sqlWhere, " and p_id=" , pSeq);
    End If;
    If cSeq <> "" Then
        Set sqlWhere = Concat(sqlWhere, " and c_id=" , cSeq);
    End If;
    If dSeq <> "" Then
        Set sqlWhere = Concat(sqlWhere, " and d_id=" , dSeq);
    End If;
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
END