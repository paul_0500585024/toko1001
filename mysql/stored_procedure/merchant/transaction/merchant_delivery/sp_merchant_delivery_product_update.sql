CREATE DEFINER=`root`@`%` PROCEDURE `sp_merchant_delivery_product_update`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
	pSeq BigInt unsigned,
	pMerchantId Int unsigned,
	pProdVarSeq BigInt unsigned,
	pStatus Char(1)
)
BEGIN

	Update t_order_product Set
		product_status = pStatus,
		modified_by = pUserID,
		modified_date = now()
	Where
		order_seq = pSeq And 
		merchant_info_seq In (
			Select seq From m_merchant_info Where merchant_seq = pMerchantId
		) And 
		product_variant_seq = pProdVarSeq;

END