CREATE DEFINER=`root`@`%` PROCEDURE `sp_merchant_delivery_update_voucher_status`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq BigInt (20) unsigned,
        pVoucherRefSeq BigInt (20) unsigned
    )
BEGIN

	Update t_order Set
	    voucher_refunded = '1',
        voucher_refund_seq=pVoucherRefSeq,
	    modified_by = pUserID,
	    modified_date = now()
	Where
		seq = pSeq;

END