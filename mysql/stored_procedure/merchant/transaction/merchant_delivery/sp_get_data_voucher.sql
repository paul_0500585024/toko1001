DROP PROCEDURE IF EXISTS `sp_get_data_voucher`;
DELIMITER $$
CREATE PROCEDURE `sp_get_data_voucher`(
        pOldVoucherSeq BigInt Unsigned,
        pNewVoucherSeq BigInt Unsigned
    )
BEGIN

	Select 
            (Select `code` From m_promo_voucher Where seq = pOldVoucherSeq) as old_voucher,
            (Select nominal From m_promo_voucher Where seq = pOldVoucherSeq) as nominal_old_voucher,
            (Select `code` From m_promo_voucher Where seq = pNewVoucherSeq) as new_voucher;	
        
END$$
DELIMITER ;