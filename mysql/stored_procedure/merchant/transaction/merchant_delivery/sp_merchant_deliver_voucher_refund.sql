CREATE DEFINER=`root`@`%` PROCEDURE `sp_merchant_deliver_voucher_refund`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq BigInt unsigned,
        pCode varchar(50)
    )
BEGIN

	Declare new_seq BigInt unsigned;
	
	Select 
		Max(seq) + 1 Into new_seq
	From m_promo_voucher;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
    
    Insert Into m_promo_voucher (
		promo_seq,
		seq,
		member_seq,
		code,
		nominal,
		`active_date`,
		`exp_date`,
		`trx_use_amt`,
		trx_no,
		created_by,
		created_date,
		modified_by,
		modified_date
	)
    Select
		0,
		new_seq,
		`member_seq`,
		pCode,
		`nominal`,
		`active_date`,
		`exp_date`,
		`trx_use_amt`,
		'',
		pUserID,
		now(),
		pUserID,
		now()
    From m_promo_voucher
	Where 
		seq = pSeq;
    
    select new_seq;

END