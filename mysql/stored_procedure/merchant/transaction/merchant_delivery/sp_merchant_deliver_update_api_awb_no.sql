CREATE DEFINER=`root`@`%` PROCEDURE `sp_merchant_deliver_update_api_awb_no`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
	pSeq BigInt unsigned,
	pMerchantId Int unsigned,
	pAwbNo VarChar(100)
)
BEGIN
	Update t_order_merchant Set
		awb_no = pAwbNo
	Where
		order_seq = pSeq And 
		merchant_info_seq In (Select seq From m_merchant_info Where merchant_seq = pMerchantId);

END