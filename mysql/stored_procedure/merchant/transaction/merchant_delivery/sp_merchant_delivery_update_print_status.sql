DELIMITER $$
CREATE PROCEDURE sp_merchant_delivery_update_print_status
(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq BigInt unsigned,
        pMerchantId Int unsigned
    )
BEGIN
	Update t_order_merchant Set
		printed = '1',
		print_date = now(),
		order_status = 'R',
		modified_by = pUserID,
		modified_date = now()
	Where
		order_seq = pSeq And
		merchant_info_seq In (Select seq From m_merchant_info Where merchant_seq = pMerchantId);
END $$
DELIMITER ;
