CREATE DEFINER=`root`@`%` PROCEDURE `sp_merchant_delivery_order_merchant_update`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq BigInt unsigned,
        pMerchantId Int unsigned,
        pTotalMerchant Decimal(10,0) unsigned,
        pTotalIns Decimal(10,0) unsigned,
        pShipReal Decimal(10,0) unsigned,
        pTshipCharge Decimal(10,0) unsigned
    )
BEGIN
	Update t_order_merchant Set
		modified_by = pUserID,
		modified_date = now()
	Where
		order_seq = pSeq And 
		merchant_info_seq In (Select seq From m_merchant_info Where merchant_seq = pMerchantId);

END