CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_merchant_deliver_update_awb`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
	pOrderSeq BigInt unsigned,
	pMerchantId Int unsigned,
	pAwbSeq BigInt unsigned,
	pAwbNo VarChar(100)
)
BEGIN

	Update `t_order_merchant` Set 
		awb_seq = pAwbSeq,
		awb_no = pAwbNo,
		modified_by = pUserID,
		modified_date = now() 
	Where
		order_seq = pOrderSeq And 
		merchant_info_seq In (Select seq From m_merchant_info Where merchant_seq = pMerchantId) ;

	Update `m_expedition_awb` m_ea, `t_order` t_o Set 
		m_ea.trx_no = t_o.order_no,
		m_ea.modified_by = pUserID,
		m_ea.modified_date = now() 
	Where
		m_ea.`seq` = pAwbSeq And 
		t_o.`seq` = pOrderSeq;

END