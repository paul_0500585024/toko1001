DROP PROCEDURE IF EXISTS `sp_merchant_delivery_by_seq`;
DELIMITER $$
CREATE PROCEDURE `sp_merchant_delivery_by_seq`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq BIGINT (20)unsigned,
        pMerchantId BIGINT(20) unsigned
    )
BEGIN
	SELECT 
    t_o.`seq`,
    t_o.`order_no`,
    t_o.`order_date`,
    t_o.`member_seq`,
    t_o.`agent_seq`,
    t_o.`receiver_name`,
    t_o.`receiver_address`,
    t_o.`receiver_district_seq`,
    t_o.`receiver_zip_code`,
    t_o.`receiver_phone_no`,
    t_o.`payment_status`,
    t_o.`pg_method_seq`,
    t_o.`paid_date`,
    t_o.`payment_retry`,
    t_o.`total_order`,
    t_o.`voucher_seq`,
    t_o.`voucher_refunded`,
    t_o.`coupon_seq`,
    t_o.`total_payment`,
    t_o.`conf_pay_type`,
    t_o.`conf_pay_amt_member`,
    t_o.`conf_pay_date`,
    t_o.`conf_pay_note_file`,
    t_o.`conf_pay_bank_seq`,
    t_o.`conf_pay_amt_admin`,
    t_om.`merchant_info_seq`,
    t_om.`expedition_service_seq`,
    t_om.`real_expedition_service_seq`,
    t_om.`total_merchant`,
    t_om.`total_ins`,
    t_om.`total_ship_real`,
    t_om.`total_ship_charged`,
    t_om.`order_status`,
    t_om.`member_notes`,
    t_om.`printed`,
    t_om.`print_date`,
    t_om.`awb_seq`,
    t_om.`awb_no`,
    t_om.`ref_awb_no`,
    t_om.`ship_by`,
    t_om.`ship_by_exp_seq`,
    t_om.`ship_date`,
    t_om.`ship_note_file`,
    t_om.`ship_notes`,
    t_om.`received_date`,
    t_om.`received_by`
    FROM 
      `t_order` t_o

      inner join 
      (select tom.* from `t_order_merchant` tom join 
      `m_merchant_info` mmi on tom.`merchant_info_seq`=mmi.`seq`
      where mmi.`merchant_seq`=pMerchantId) t_om
      on t_o.`seq`=t_om.`order_seq`
            Where
                    t_o.`seq` = pSeq       
            ;        

      select 
      t_op.`order_seq`,
      t_op.`merchant_info_seq`,
      t_op.`product_variant_seq`,
      t_op.`variant_value_seq`,
      t_op.`qty`,
      t_op.`sell_price`,
      t_op.`weight_kg`,
      t_op.`ship_price_real`,
      t_op.`ship_price_charged`,
      t_op.`ins_rate_percent`,
      t_op.`product_status`,
      v_pv.`merchant_seq`,
      v_pv.`seq`,
      v_pv.`name`,
      v_pv.`include_ins`,
      v_pv.`category_l2_seq`,
      v_pv.`category_ln_seq`,
      v_pv.`notes`,
      v_pv.`description`,
      v_pv.`content`,
      v_pv.`warranty_notes`,
      v_pv.`p_weight_kg`,
      v_pv.`p_length_cm`,
      v_pv.`p_width_cm`,
      v_pv.`p_height_cm`,
      v_pv.`b_weight_kg`,
      v_pv.`b_length_cm`,
      v_pv.`b_width_cm`,
      v_pv.`b_height_cm`,
      v_pv.`product_seq`,
      v_pv.`variant_seq`,
      v_pv.`product_price`,
      v_pv.`disc_percent`,
      v_pv.`sell_price` as product_sell_price,
      v_pv.`order`,
      v_pv.`max_buy`,
      v_pv.`status`,
      v_pv.`pic_1_img`,
      v_pv.`pic_2_img`,
      v_pv.`pic_3_img`,
      v_pv.`pic_4_img`,
      v_pv.`pic_5_img`,
      v_pv.`pic_6_img`,
      v_pv.`1star`,
      v_pv.`2star`,
      v_pv.`3star`,
      v_pv.`4star`,
      v_pv.`5star`,
      v_pv.`merchant_sku`,
      v_pv.`stock`,
      v_pv.`variant_value`
      from 
     `t_order_product` t_op
     join (select seq,merchant_seq from m_merchant_info where merchant_seq=pMerchantId) mmi
     on t_op.`merchant_info_seq`=mmi.seq
      inner join `v_product_variant` v_pv
      on t_op.`product_variant_seq`=v_pv.`variant_seq`
            Where
                    t_op.`order_seq` = pSeq
        ;        
END$$
DELIMITER ;