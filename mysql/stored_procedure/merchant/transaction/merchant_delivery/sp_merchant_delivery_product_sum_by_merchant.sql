CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_merchant_delivery_product_sum_by_merchant`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
	pSeq BigInt unsigned,
	pMerchantId Int unsigned
)
BEGIN

	Select 
        Sum(qty) as total_qty,
		Sum(sell_price * qty) as total_price,
        Sum(Ceiling(weight_kg * qty)) as total_weight,
        Sum(ship_price_real * Ceiling(weight_kg * qty)) as total_ship_price,
        Sum(sell_price * qty * trx_fee_percent / 100) as total_fee,
        Sum(sell_price * qty * ins_rate_percent / 100) as total_insurance
	From `t_order_product`
	Where
		order_seq= pSeq And 
		merchant_info_seq In (
			Select seq From m_merchant_info Where merchant_seq = pMerchantId
		);
END