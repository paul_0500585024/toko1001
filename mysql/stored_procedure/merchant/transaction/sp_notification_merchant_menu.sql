DELIMITER ##
DROP PROCEDURE sp_notification_merchant_menu ##
CREATE PROCEDURE sp_notification_merchant_menu
(
        `pUserID` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pUserSeq` BIGINT(20)
    )
BEGIN
    SELECT count(tom.order_seq) as count_order from t_order_merchant tom join
    m_merchant_info mmi on tom.merchant_info_seq=mmi.seq
    where tom.order_status in ('N','R','S') and mmi.merchant_seq=pUserSeq;

    SELECT count(topr.seq) as count_order_return from t_order_product_return topr join
    t_order_merchant tom on topr.order_seq=tom.order_seq join
    m_merchant_info mmi on tom.merchant_info_seq=mmi.seq
    where topr.shipment_status in ('S','R','T') and topr.return_status='N'  and mmi.merchant_seq=pUserSeq;

    SELECT count(notif_status) as count_redeem from t_redeem_merchant
    where notif_status='U' and status='P' and merchant_seq=pUserSeq;

END ##
DELIMITER ;
