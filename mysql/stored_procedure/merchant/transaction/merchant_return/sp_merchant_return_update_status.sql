CREATE DEFINER=`root`@`%` PROCEDURE `sp_merchant_return_update_status`(		
        pMerchantID VarChar(25),
        pIPAddr VarChar(50),
        pReturnNo varchar(50),
        pReturnStatus varchar(50),                
        pMerchant_comment text         
        )
BEGIN

update 
t_order_product_return 
set 
`return_status` = pReturnStatus , 
`review_merchant` = pMerchant_comment,
`modified_date` = now(),
`modified_by` = pMerchantID
where `return_no` = pReturnNo;

END