DELIMITER $$
CREATE PROCEDURE sp_merchant_return_detail
(
        `pMerchantSeq` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pReturnNo` VARCHAR(50)
    )
BEGIN
select
topr.seq, topr.return_no,
topr.order_seq, topr.product_variant_seq,
topr.qty, topr.return_status,
topr.shipment_status, topr.review_member,
topr.review_admin, topr.review_merchant,
topr.awb_member_no, topr.admin_received_date,
topr.ship_to_merchant_date, topr.exp_seq_to_merchant,
topr.awb_admin_no, topr.merchant_received_date,
topr.ship_to_member_date, topr.awb_merchant_no,
topr.exp_seq_to_member, topr.created_by,
topr.created_date, topr.modified_by, topr.modified_date,
mc.seq as merchant_seq , mb.email as member_email , mb.`name` as member_name,
ma.email as agent_email , ma.`name` as agent_name , mpv.sell_price as product_sell_price
from t_order_product_return topr
join m_product_variant mpv on topr.product_variant_seq = mpv.seq
join m_product mp on mpv.product_seq = mp.seq
join m_merchant mc on mp.merchant_seq = mc.seq
join t_order tor on topr.order_seq=tor.seq
left outer join m_member mb on tor.member_seq = mb.seq
left outer join m_agent ma on tor.agent_seq = ma.seq
where return_no = pReturnNo ;
END $$
DELIMITER ;
