DELIMITER $$
CREATE PROCEDURE sp_merchant_order_return_list
(
	pUserID VarChar(25),
	pMerchantSeq VarChar(25),
	pIPAddr VarChar(50),
	pCurrPage Int,
	pRecPerPage Int,
	pDirSort VarChar(4),
	pColumnSort Varchar(50),
	pNo_return Varchar(50),
	pDateSearch Char(1),
	pReturnDateFrom DateTime,
	pReturnDateTo DateTime,
	pShip_status char(1),
	pMember_email Varchar(100),
	pNew_Status char(1), -- status N
	pShip_To_member_Status char(1), -- status T
	pShip_From_Member_Status char(1), -- Status M
	pReceived_By_Member_Status char(1), -- Status C
	pReceived_By_Admin_Status char(1), -- Status A
	pRejected_Status char(1) -- Status A */
        )
BEGIN
 -- Begin variables for Paging
    Declare startRec Int;
    Declare totalPage Int;
    Declare totalRec Int;
    -- End variables for Paging
    -- Begin SQL Where
    Declare sqlWhere VarChar(500);
	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
    If pNo_return <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And t_op.`return_no` like '%" , escape_string(pNo_return), "%'");
    End If;

    If (pDateSearch = "1") Then
        Set sqlWhere = Concat(sqlWhere, " And t_op.`created_date` between  '",pReturnDateFrom,"' And  '",pReturnDateTo ,"'");
    End If;

    If pShip_status <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And t_op.`shipment_status` = '" , pShip_status, "'");
    End If;

    -- End SQL Where
    -- Begin Paging Info
    Set @sqlCommand = "SELECT count(t_op.seq) Into @totalRec
		FROM
		t_order_product_return t_op , m_product_variant m_pv , m_product m_p , t_order t_or , m_member mem , m_variant_value m_v
		WHERE
		t_op.product_variant_seq = m_pv.seq
		AND m_pv.product_seq = m_p.seq
		AND t_op.order_seq = t_or.seq
		AND t_or.member_seq = mem.seq
        AND m_pv.variant_value_seq = m_v.seq
        AND t_op.seq not in ( select seq as seq from t_order_product_return where return_status = 'N' and shipment_status = 'T'
		union all
		select seq as seq from t_order_product_return where return_status = 'N' and shipment_status = 'M'
        union all
        select seq as seq from t_order_product_return where return_status = 'N' and shipment_status = 'C'
        union all
         select seq as seq from t_order_product_return where return_status = 'N' and shipment_status = 'A'
         union all
         select seq as seq from t_order_product_return where return_status = 'R' and shipment_status = 'A'
        union all
        select seq as seq from t_order_product_return where return_status = 'R' and shipment_status = 'C' and awb_merchant_no = ''
        )
        ";

    Set @sqlCommand = Concat(@sqlCommand, " And m_p.merchant_seq ='" , pMerchantSeq , "'" );
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);
        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;
        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info
    Set @sqlCommand = "
		SELECT
		t_op.seq as return_seq ,
		mem.`email` as member_email,t_op.return_no ,
		t_op.order_seq , t_or.order_no , m_p.name as product_name , m_v.value as variant_name,m_v.seq as variant_value_seq,
		t_op.qty ,t_op.return_status ,
		t_op.shipment_status,t_op.awb_admin_no,t_op.awb_merchant_no,
		t_op.admin_received_date,t_op.created_date,
		'' as action
		FROM
		t_order_product_return t_op , m_product_variant m_pv , m_product m_p , t_order t_or , m_member mem , m_variant_value m_v
		WHERE
		t_op.product_variant_seq = m_pv.seq
		AND m_pv.product_seq = m_p.seq
		AND t_op.order_seq = t_or.seq
		AND t_or.member_seq = mem.seq
        AND m_pv.variant_value_seq = m_v.seq
        AND t_op.seq not in ( select seq as seq from t_order_product_return where return_status = 'N' and shipment_status = 'T'
		union all
		select seq as seq from t_order_product_return where return_status = 'N' and shipment_status = 'M'
        union all
        select seq as seq from t_order_product_return where return_status = 'N' and shipment_status = 'C'
        union all
         select seq as seq from t_order_product_return where return_status = 'N' and shipment_status = 'A'
         union all
         select seq as seq from t_order_product_return where return_status = 'R' and shipment_status = 'A'
        union all
        select seq as seq from t_order_product_return where return_status = 'R' and shipment_status = 'C' and awb_merchant_no = ''
        )
         ";
         Set @sqlCommand = Concat(@sqlCommand, " AND m_p.merchant_seq ='" , pMerchantSeq , "'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort," ", pDirSort);
    Set @sqlCommand = Concat(@sqlCommand, "  Limit ", startRec, ", ", pRecPerPage);
    Select pCurrPage, totalPage, totalRec as total_rec;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set @totalRec = Null;
    Set @sqlCommand = Null;
END $$
DELIMITER ;
