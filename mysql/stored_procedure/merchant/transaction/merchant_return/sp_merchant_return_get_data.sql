DELIMITER $$
CREATE PROCEDURE sp_merchant_return_get_data
(
 pMerchantSeq varchar(50),
 pIPaddr varchar(60),
 pReturnSeq int unsigned
)
BEGIN
        select
		topr.seq as return_seq , topr.return_no , topr.return_status, topr.shipment_status , topr.qty ,
		topr.review_member , topr.review_admin , topr.review_merchant , topr.created_date , topr.admin_received_date ,
		topr.merchant_received_date , topr.member_received_date , topr.ship_to_merchant_date , topr.ship_to_member_date ,
		topr.awb_member_no , topr.awb_merchant_no , topr.awb_admin_no, topr.modified_date ,
		`order`.seq as order_seq,`order`.receiver_address, `order`.order_no ,
		member.seq  as member_seq, member.email as email_member, member.mobile_phone , member.`name` as member_name ,member.profile_img,
		product_variant.seq as product_variant_seq , variant_value.`value` , product_variant.pic_1_img  , product_variant.sell_price ,
		product.seq as product_seq , product.`name` as product_name , product.merchant_seq,


        (Select e.`name`
			From t_order_product_return opr
				Join m_expedition e On opr.exp_seq_to_admin = e.seq
			Where opr.seq = pReturnSeq
		) as exp_to_admin,
        (Select e.`name`
			From t_order_product_return opr
				Join m_expedition e On opr.exp_seq_to_member = e.seq
			Where opr.seq = pReturnSeq
		) as exp_to_member,
        (Select e.`name`
			From t_order_product_return opr
				Join m_expedition e On opr.exp_seq_to_merchant = e.seq
			Where opr.seq = pReturnSeq
		) as exp_to_merchant,

		district.`name` as district_name ,
		city.`name` as city_name ,province.`name` as province_name,merchant.logo_img
		from t_order_product_return topr
		inner join t_order as `order` on topr.order_seq = `order`.seq
		inner join m_member as member on `order`.member_seq = member.seq
		inner join m_product_variant  as product_variant on topr.product_variant_seq = product_variant.seq
		inner join m_variant_value as variant_value on product_variant.variant_value_seq = variant_value.seq
		inner join m_product as product on product_variant.product_seq = product.seq
		inner join m_district as district on `order`.receiver_district_seq = district.seq
		inner join m_city as city on district.city_seq = city.seq
		inner join m_province as province on city.province_seq = province.seq
		inner join m_merchant as merchant on product.merchant_seq = merchant.seq
        where  topr.seq =  pReturnSeq ;

END $$
DELIMITER ;
