CREATE DEFINER=`root`@`%` PROCEDURE `sp_merchant_return_update_received`(		
        pMerchantSeq VarChar(25),
        pIPAddr VarChar(50),
        pReturnNo varchar(50),
        PTglTerima varchar(50)
        
        )
BEGIN

update 
t_order_product_return 
set 
`shipment_status` = 'R' , 
`merchant_received_date` = pTglTerima  ,
`modified_date` = now()
where `return_no` = pReturnNo;

END