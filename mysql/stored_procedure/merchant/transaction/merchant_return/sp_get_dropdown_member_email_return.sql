DELIMITER $$
CREATE PROCEDURE sp_get_dropdown_member_email_return
(
        `pMerchant_seq` VARCHAR(10)
    )
BEGIN
select distinct(IFNULL(m.email, ma.email)) as email
from t_order_product_return topr
join t_order tor on topr.`order_seq`=tor.`seq`
left outer join m_member m on tor.`member_seq`= m.seq
left outer join m_agent ma on tor.`agent_seq`= ma.seq
join m_product_variant mpv on topr.product_variant_seq = mpv.seq
join m_product mp on mpv.product_seq = mp.seq
join m_merchant mc on mp.merchant_seq = mc.seq
where mc.seq = pMerchant_seq ;
END $$
DELIMITER ;
