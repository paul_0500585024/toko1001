DELIMITER $$
CREATE PROCEDURE sp_merchant_return_add_refund
(
        `pMerchantID` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pReturnNo` VARCHAR(100),
        `pMemberSeq` BIGINT,
        `pPgmethodseq` VARCHAR(100),
        `pTrxDate` VARCHAR(100),
        `pDepositTrxAmt` INTEGER UNSIGNED,
        `pMutationType` VARCHAR(10),
        `pTrxType` VARCHAR(10),
        `pNewStatus` VARCHAR(10),
        `pAgentSeq` BIGINT
    )
BEGIN
Declare new_seq INT unsigned;
if pMemberSeq > 0 then
	Select
		Max(seq) + 1 Into new_seq
	From t_member_account;
	If new_seq Is Null Then
		Set new_seq = 1;
	End If;

    insert into t_member_account (
    member_seq,
    seq,
    mutation_type,
    pg_method_seq,
    trx_type,
    trx_no,
    trx_date,
    deposit_trx_amt,
    non_deposit_trx_amt,
    bank_name,
    bank_branch_name,
    bank_acct_no,
    bank_acct_name,
    refund_date,
    `status`,
    created_by,
    created_date,
    modified_by,
    modified_date
    )
   values
   (
    pMemberSeq,
    new_seq,
    pMutationType,
    pPgmethodseq,
    pTrxType,
    pReturnNo,
    pTrxDate,
    pDepositTrxAmt,
    '0',
    ' ',
    ' ',
    ' ',
    ' ',
    0000-00-00,
    pNewStatus,
    pMerchantID,
    now(),
    pMerchantID,
    now()
   );
else
	Select
		Max(seq) + 1 Into new_seq
	From t_agent_account;
	If new_seq Is Null Then
		Set new_seq = 1;
	End If;

    insert into t_agent_account (
    agent_seq,
    seq,
    mutation_type,
    pg_method_seq,
    trx_type,
    trx_no,
    trx_date,
    deposit_trx_amt,
    non_deposit_trx_amt,
    bank_name,
    bank_branch_name,
    bank_acct_no,
    bank_acct_name,
    refund_date,
    `status`,
    created_by,
    created_date,
    modified_by,
    modified_date
    )
   values
   (
    pAgentSeq,
    new_seq,
    pMutationType,
    pPgmethodseq,
    pTrxType,
    pReturnNo,
    pTrxDate,
    pDepositTrxAmt,
    '0',
    ' ',
    ' ',
    ' ',
    ' ',
    0000-00-00,
    pNewStatus,
    pMerchantID,
    now(),
    pMerchantID,
    now()
   );
end if;
END $$
DELIMITER ;
