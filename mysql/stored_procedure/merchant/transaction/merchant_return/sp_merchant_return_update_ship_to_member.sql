CREATE DEFINER=`root`@`%` PROCEDURE `sp_merchant_return_update_ship_to_member`(		
        pMerchantID VarChar(25),
        pIPAddr VarChar(50),
        pReturnNo varchar(50),
        pTglkirim varchar(50),        
        pExpseq varchar(50),
        pNoAWbMerchant varchar(50),
        pMerchant_comment text         
        )
BEGIN

update 
t_order_product_return 
set 
`shipment_status` = 'T' , 
`exp_seq_to_member` = pExpseq,
`awb_merchant_no` = pNoAWbMerchant,
`ship_to_member_date` = pTglkirim,
`review_merchant` = pMerchant_comment,
`modified_date` = now(),
`modified_by` = pMerchantID
where `return_no` = pReturnNo;

END