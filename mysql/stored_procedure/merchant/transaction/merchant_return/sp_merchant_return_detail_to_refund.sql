DELIMITER $$
CREATE PROCEDURE sp_merchant_return_detail_to_refund
(
        `pMerchantSeq` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pReturnNo` VARCHAR(50)
    )
BEGIN
select o.member_seq,o.agent_seq,
topr.return_no,
o.pg_method_seq,
topr.created_date as trx_date,
pv.sell_price,
topr.qty*pv.sell_price as deposit_trx_amt
from
t_order_product_return topr
join t_order o on topr.order_seq = o.seq
join m_product_variant pv on topr.product_variant_seq = pv.seq
where topr.return_no = pReturnNo ;
END $$
DELIMITER ;
