CREATE DEFINER=`root`@`%` PROCEDURE `sp_merchant_order_list_detail`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pMerchantSeq Int unsigned,
        pCurrPage Int unsigned,
        pRecPerPage Int unsigned,
        pDirSort VarChar(4),
        pColumnSort Varchar(50),
        pHSeq BigInt unsigned
    )
BEGIN
    -- Begin variables for Paging
    Declare startRec Int unsigned;
    Declare totalPage Int unsigned;
    Declare totalRec Int unsigned;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";

    -- Begin Paging Info
    Set @sqlCommand = "Select Count(m.seq) Into @totalRec From  t_order_product op
			JOIN
		m_product_variant pv ON op.product_variant_seq = pv.seq
			JOIN
		m_product p ON pv.product_seq = p.seq
			JOIN
		m_variant_value vv ON pv.variant_value_seq = vv.seq
			JOIN
		m_merchant_info mi ON op.merchant_info_seq = mi.seq
			JOIN
		m_merchant m ON mi.merchant_seq = m.seq";

        Set @sqlCommand = Concat(@sqlCommand, " Where op.order_seq ='" ,pHSeq,"' AND m.seq='",pMerchantSeq,"'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
        SELECT 
			op.order_seq,
			op.merchant_info_seq,
			p.name AS product_name,
			vv.value AS variant_name,
			pv.pic_1_img,
            pv.seq as product_variant_seq,
			p.p_weight_kg,
			p.p_length_cm,
			p.p_width_cm,
			p.p_height_cm,
			op.qty,
			op.sell_price,
			m.seq
            
	FROM
		t_order_product op
			JOIN
		m_product_variant pv ON op.product_variant_seq = pv.seq
			JOIN
		m_product p ON pv.product_seq = p.seq
			JOIN
		m_variant_value vv ON pv.variant_value_seq = vv.seq
			JOIN
		m_merchant_info mi ON op.merchant_info_seq = mi.seq
			JOIN
		m_merchant m ON mi.merchant_seq = m.seq
		";

    Set @sqlCommand = Concat(@sqlCommand, " Where op.order_seq ='" ,pHSeq,"' AND m.seq='",pMerchantSeq,"'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END