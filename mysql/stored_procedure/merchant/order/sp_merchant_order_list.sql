CREATE DEFINER=`root`@`%` PROCEDURE `sp_merchant_order_list`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pCurrPage Int unsigned,
        pRecPerPage Int unsigned,
        pDirSort VarChar(4),
        pColumnSort Varchar(50),
        pSeq Int unsigned,
        pOrderNo VarChar(50),
        pDates VarChar(10),
        pDatee VarChar(10),
        pReceiver Char(50),
        pStatus Char(1),
        pPaymentStatus char(1)
    )
BEGIN
    -- Begin variables for Paging
    Declare startRec Int unsigned;
    Declare totalPage Int unsigned;
    Declare totalRec Int unsigned;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";

    -- End SQL Where
If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
	If pOrderNo <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And o.order_no like '%" , escape_string(pOrderNo), "%'");
    End If;
    If pStatus <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And om.order_status = '" , pStatus, "'");
    End If;
    If pPaymentStatus <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And o.payment_status = '" , pPaymentStatus, "'");
    End If;
    If pDates <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And o.order_date >= '" , pDates, "'");
    End If;
    If pDatee <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And o.order_date <= '" , pDatee, "'");
    End If;
    If pReceiver <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And o.receiver_name like '%" , escape_string(pReceiver), "%'");
    End If;
    -- Begin Paging Info
    Set @sqlCommand = "Select Count(mc.seq) Into @totalRec From m_member m
		Join
			t_order o 
				On m.seq = o.member_seq
		join t_order_merchant om
			 on o.seq = order_seq
		join m_merchant_info mi
			on om.merchant_info_seq = mi.seq
		join m_merchant mc
			on mi.merchant_seq = mc.seq ";
            
    Set @sqlCommand = Concat(@sqlCommand, "Where mc.seq ='" , pSeq , "'");
	If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
        Select
			o.seq,
			m.name as member_name,
			o.order_no,
			o.order_date,
			o.receiver_name,
			o.receiver_address,
			o.payment_status,
			o.paid_date,
            om.merchant_info_seq,
            om.order_status,
            mi.merchant_seq,
            mc.seq as mt_seq,
            '' as produk
		From 
			m_member m
		Join
			t_order o 
				On m.seq = o.member_seq
		join t_order_merchant om
			 on o.seq = om.order_seq
		join m_merchant_info mi
			on om.merchant_info_seq = mi.seq
		join m_merchant mc
			on mi.merchant_seq = mc.seq
    ";

	Set @sqlCommand = Concat(@sqlCommand, "Where mc.seq ='" , pSeq , "'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;

    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);

    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END