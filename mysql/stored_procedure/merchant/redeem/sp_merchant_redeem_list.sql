DELIMITER ##
DROP PROCEDURE sp_merchant_redeem_list ##
CREATE PROCEDURE sp_merchant_redeem_list
(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pCurrPage Int,
        pRecPerPage Int,
        pDirSort VarChar(20),
        pColumnSort Varchar(50),
        pMerchantID int,
        pFdate Varchar(10),
        pTdate VarChar(10)
    )
BEGIN
    -- Begin variables for Paging
    Declare startRec Int;
    Declare totalPage Int;
    Declare totalRec Int;
    -- End variables for Paging
    -- Begin SQL Where
    Declare sqlWhere VarChar(500);
	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;

	Set sqlWhere = " And a.status= 'P'";
    If pFdate <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And a.paid_date >= '" , pFdate, "'");
    End If;
    If pTdate <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And a.paid_date <= '" , pTdate, "'");
    End If;
    If pMerchantID <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And a.merchant_seq= '" , pMerchantID, "'");
    End If;
    -- End SQL Where
    -- Begin Paging Info
    Set @sqlCommand = "Select Count(redeem_seq) Into @totalRec From t_redeem_merchant a";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);
        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;
        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info
    Set @sqlCommand = "
        Select
        a.`redeem_seq`,
        a.`merchant_seq`,
        a.`merchant_info_seq`,
        a.`total`,
        a.`status`,
        a.`paid_date`,
        a.`notif_status`
        FROM t_redeem_merchant a
    ";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    Select pCurrPage, totalPage, totalRec as total_rec;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set @totalRec = Null;
    Set @sqlCommand = Null;
END ##
DELIMITER ;
