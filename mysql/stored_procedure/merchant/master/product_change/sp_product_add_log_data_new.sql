CREATE DEFINER = 'root'@'%' PROCEDURE `sp_product_add_log_data_new`(
        pUser_ID int(25),
        pLog_seq int(10),
        pSeq int(10),
        pName VarChar(150),
        pInclude_ins Char(1),
        pDescription TEXT,
        pSpecification_data TEXT,
        pContent TEXT,
        pWarranty_Notes VarChar(100),
        pP_weight_kg DECIMAL(7,2),
        pP_length_cm DECIMAL(7,2),
        pP_width_cm DECIMAL(7,2),
        pP_height_cm DECIMAL(7,2),
        pB_weight_kg DECIMAL(7,2),
        pB_length_cm DECIMAL(7,2),
        pB_width_cm DECIMAL(7,2),
        pB_height_cm DECIMAL(7,2)
    )
BEGIN

INSERT INTO m_product_log_data(
product_seq, 
log_seq, 
`type`, 
`name`,
include_ins, 
description, 
specification, 
content, 
warranty_notes, 
p_weight_kg, 
p_length_cm, 
p_width_cm, 
p_height_cm, 
b_weight_kg, 
b_length_cm, 
b_width_cm, 
b_height_cm,
created_by, 
created_date, 
modified_by, 
modified_date 
) 

values (
pSeq,
pLog_seq,
'2',
pName, 
pInclude_ins ,
pDescription, 
pSpecification_data,
pContent, 
pWarranty_Notes, 
pP_weight_kg,
pP_length_cm, 
pP_width_cm, 
pP_height_cm, 
pB_weight_kg, 
pB_length_cm, 
pB_width_cm, 
pB_height_cm, 
pUser_ID, 
now(),
pUser_ID, 
now());

END;