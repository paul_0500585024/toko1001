CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_get_spec_seq`(
pUser_id int(10),
pIpaddr varchar(20),
pProdseq int(10)
)
BEGIN
select max(seq) as max_seq from m_product_spec where product_seq = pProdseq;

END