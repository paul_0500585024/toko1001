CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_product_log_add_old_spec`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pProduct_seq BIGINT(10),
        pLogSeq int(10)
        
    )
BEGIN
replace into m_product_log_spec (
product_seq, 
log_seq, 
spec_seq,
`type`,
`name`,
`value`,
created_by, 
created_date, 
modified_by, 
modified_date
)
select 
pProduct_seq as product_seq,
pLogseq as Log_seq,
`seq` as spec_seq,
'1' as `type`,
name as `name`,
value as `value`,
pUserid as created_by,
now() as created_date,
pUserid as modified_by,
now() as modified_date 
from m_product_spec where m_product_spec.product_seq = pProduct_seq; 

END