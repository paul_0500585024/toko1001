CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_product_get_status_by_seq`(
        IN pUserID VARCHAR(25),
        IN pIPAddr VARCHAR(50),
        IN pSeq BIGINT
    )
BEGIN
	Select    
    `status` 
    From `m_product` 
    Where seq = pSeq;
END