CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_merchant_change_status`(
IN pMerchant_seq VARCHAR(25),
IN pSeq_product_last BIGINT(10)
)
BEGIN

update 
m_product set `status` = 'C' ,
`modified_by` = pMerchant_seq,
`modified_date` = now() 
where seq = pSeq_product_last ;
END