CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_get_spec_seq_old`(
pUser_id int(10),
pIpaddr varchar(20),
pOldspecseq int(10),
pProdseq int(10)
)
BEGIN
select 
`seq`,
`name`,`value` 
from m_product_spec 
where 
product_seq = pProdseq 
and seq = pOldspecseq;
END