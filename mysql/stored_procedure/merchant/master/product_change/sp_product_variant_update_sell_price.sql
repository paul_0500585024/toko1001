DROP PROCEDURE IF EXISTS `sp_product_variant_update_sell_price`;
DELIMITER $$
CREATE PROCEDURE `sp_product_variant_update_sell_price` (
    pUserID Varchar(25),
    pIPAddr Varchar(25),
    pMerchantSeq Int unsigned,
    pProductVariantSeq BigInt unsigned,
    pSellPrice Decimal(10,0)
)
BEGIN
declare new_seq int unsigned;
declare old_product_price decimal(10,0);
declare valid boolean;

select
    product_price into old_product_price
from 
    m_product_variant
where
    seq = pProductVariantSeq;    

if (old_product_price < pSellPrice) Then
    set valid = 0;
else
    set valid = 1;
end if;

if (valid) then
begin
update m_product_variant set
    sell_price = pSellPrice,
    disc_percent = ((product_price - pSellPrice) / product_price) * 100,
    modified_by = pUserID,
    modified_date = now()
where
    seq = pProductVariantSeq;

select
    max(seq) + 1 into new_seq
from 
    m_product_variant_price_log
where
    product_variant_seq = pProductVariantSeq;

if new_seq is null Then
   set new_seq = 1;
end if;

insert into m_product_variant_price_log (
    product_variant_seq,
    seq,
    product_price,
    sell_price,
    ip_address,
    created_by,
    created_date
) values (
    pProductVariantSeq,
    new_seq,
    old_product_price,
    pSellPrice,
    pIPAddr,
    pUserID,
    now()
);
end;
end if;

select valid;

END$$

