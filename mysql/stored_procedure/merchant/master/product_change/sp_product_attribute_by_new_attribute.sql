CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_attribute_by_new_attribute`(
	pProduct_seq int(10),
    pNew_attribute int(10)
)
BEGIN
select 
attribute_value_seq 
from 
m_product_attribute 
where 
product_seq = pNew_attribute and 
attribute_value_seq in 
(select seq from m_attribute_value where attribute_value_seq = pProduct_seq );
END