CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_add_log_data_last`(
        pMerchant_seq varchar(25),
        pLog_seq int(10),
        pSeq_product_last int(10)        
)
BEGIN

replace INTO m_product_log_data(
product_seq, 
log_seq, 
`type`, 
`name`,
include_ins, 
description,
content, 
specification, 
warranty_notes, 
p_weight_kg, 
p_length_cm, 
p_width_cm, 
p_height_cm, 
b_weight_kg, 
b_length_cm, 
b_width_cm, 
b_height_cm,
created_by, 
created_date, 
modified_by, 
modified_date 
) 

SELECT 
pSeq_product_last as seq,
pLog_seq as log_seq,
1 as type,
`name` as `name`,
include_ins, 
description, 
content as content, 
specification,
warranty_notes, 
p_weight_kg, 
p_length_cm, 
p_width_cm, 
p_height_cm, 
b_weight_kg, 
b_length_cm, 
b_width_cm, 
b_height_cm, 
created_by, 
created_date, 
modified_by, 
modified_date 
from m_product where merchant_seq = pMerchant_seq and seq = pSeq_product_last;

END