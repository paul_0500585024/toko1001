CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_update_log_attribute`(
		pUserID VarChar(25),
		pAttributeSeq int(10),
		pNewAttribute int(10),
		pOldAttribute int(10),
		pLog_seq int(10),
		pProduct_seq int(10)
)
BEGIN
	
	
        insert into m_product_log_attribute 
        values
        (
        pProduct_seq,
        pLog_seq,
        pOldAttribute,
        pNewAttribute,
        pUserID,
        now(),
        pUserID,
        now()
        );
      
END