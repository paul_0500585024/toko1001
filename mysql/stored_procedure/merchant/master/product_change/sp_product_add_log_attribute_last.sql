CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_add_log_attribute_last`(
		pUser_ID int(25),
        pLog_seq int(10),
        pSeq_product_last int(10),
        pAttribute_old int(10),
        pAttribut int(10)
        
)
BEGIN
REPLACE into m_product_log_attribute (
product_seq, log_seq, old_attribute_value_seq, new_attribute_value_seq, created_by, created_date, modified_by, modified_date
) 
values(
pSeq_product_last,
pLog_seq,
pAttribute_old,
pAttribut,
pUser_ID, now(),pUser_ID, now()
);

END