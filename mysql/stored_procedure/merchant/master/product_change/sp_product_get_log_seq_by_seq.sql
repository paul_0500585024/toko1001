CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_get_log_seq_by_seq`(
		IN pUserID VARCHAR(25),
        IN pIPAddr VARCHAR(50),
        IN pSeq BIGINT
)
BEGIN
	select 
    p.merchant_seq,
    plog.seq  as log_seq 
    from 
    m_product_log plog , 
    m_product p 
    where 
    plog.product_seq = pSeq 
    and 
    p.seq = pSeq 
    and 
    plog.status = 'N';
    -- create by toriq --
END