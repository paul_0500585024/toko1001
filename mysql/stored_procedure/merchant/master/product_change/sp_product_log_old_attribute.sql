CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_log_old_attribute`(
pUserid int(10),
pAttributeSeq int(10),
pProduct_seq int(10)
)
BEGIN
		
select 
attribute_value_seq 
from 
m_product_attribute pa,
m_attribute_value ma 
where 
pa.attribute_value_seq = ma.seq 
and ma.attribute_seq = pAttributeSeq 
and pa.product_seq = pProduct_seq;
    


END