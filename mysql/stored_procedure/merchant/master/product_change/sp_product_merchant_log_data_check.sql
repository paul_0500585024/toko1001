CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_merchant_log_data_check`(	
    PUser_id varchar(10),
    PMerchant_seq varchar(20),
    pSeq int(9)    
)
BEGIN

	
Declare new_seq INT;

	Select 
	Max(seq) + 1 
    Into new_seq
	From m_product_log;

	If new_seq Is Null Then
	Set new_seq = 1;
	End If;

	insert INTO m_product_log
	( product_seq, 
    seq, 
    status, 
    auth_by, 
    auth_date, 
    created_by, 
    created_date, 
    modified_by, 
    modified_date ) 
    VALUES 
	( pSeq,
    new_seq,
    'N',
    pMerchant_seq,
    now(),
    pUser_id,
    now(),
    PMerchant_seq,
    now());

	Select 
    seq as log_seq 
    from 
    m_product_log 
    where product_seq = pSeq 
    and `status` = 'N';
        
	select 
    specification as specification_data 
    from 
    m_product where seq = pSeq;
        
	select 
    count(attribute_value_seq) as jumlah 
    from m_product_attribute 
    where product_seq = pSeq;
        
	select 
    attribute_value_seq 
    from 
    m_product_attribute 
    where product_seq = pSeq;
END