CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_add_log_new_spec`(
		pUserID VARCHAR(25),
		pIpaddr varchar(20),
		pProduct_seq int(10),
		pLog_seq int(10),
		/*pNowSeq int(10),*/
		pSpekname varchar(100),
		pSpekval varchar(100)

)
BEGIN
Declare new_seq TinyInt unsigned;	
		
        Select 
		Max(spec_seq) + 1 Into new_seq
	From `m_product_log_spec` where product_seq=pProduct_Seq and log_seq = pLog_seq   ;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
        
        
/*if (pNowSeq = 0 )
then 
insert into m_product_log_spec values
(pProduct_seq,pLog_seq,'0',pNewseq,'0','0',pSpekname,pSpekval,pUserid,now(),pUserid,now() );
else */
insert into m_product_log_spec (
product_seq, 
log_seq, 
spec_seq,
`type`,
`name`,
`value`,
created_by, 
created_date, 
modified_by, 
modified_date
) values
(pProduct_seq,pLog_seq,new_seq,'2',pSpekname,pSpekval,pUserid,now(),pUserid,now() );


END