DROP PROCEDURE IF EXISTS `sp_logo_merchant_update`;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_logo_merchant_update`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq bigint unsigned,
        pBannerImg	varchar(100),
        pLogoImg	varchar(100),
        pNotes	TEXT,	
        pCloseStoreFrom date,
        pCloseStoreTo date
    )
BEGIN

update m_merchant set 
	logo_img = pLogoImg,
	banner_img = pBannerImg,
	welcome_notes = pNotes,
        from_date = pCloseStoreFrom,
        to_date = pCloseStoreTo,
	modified_by = pUserID,
	modified_date = now() 
where seq = pSeq;

END$$