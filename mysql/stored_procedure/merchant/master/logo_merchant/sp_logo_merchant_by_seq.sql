DROP PROCEDURE IF EXISTS `sp_logo_merchant_by_seq`;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_logo_merchant_by_seq`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq bigint unsigned
    )
BEGIN

	Select
		seq,
		banner_img,
		logo_img,
		welcome_notes as description,
                from_date,
                to_date
	From 
		m_merchant
	Where
		seq = pSeq;

END$$












