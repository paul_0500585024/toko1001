DROP PROCEDURE IF EXISTS `sp_open_store`;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_open_store`(
        pUserID VarChar(25),
        pSeq bigint unsigned
    )
BEGIN

update m_merchant set 
        from_date = '0000-00-00 00:00:00',
        to_date = '0000-00-00 00:00:00',
        modified_by = pUserID,
        modified_date = now()
where seq = pSeq;

END$$