CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_stock_update`(
	pUserID VarChar(50), 
	pIPAddr VarChar(50),
    pProductVariantSeq BigInt Unsigned,
    pValueSeq BigInt Unsigned,
    pMerchantSKU VarChar(50),
    pStock Decimal,
    pStockAdd Decimal
    )
BEGIN

If Exists (
	Select 
		product_variant_seq, 
		variant_value_seq 
	From 
		m_product_stock 
	Where
		product_variant_seq = pProductVariantSeq And variant_value_seq = pValueSeq
)
Then
Update m_product_stock ps
Join m_variant_value vv
	On ps.variant_value_seq = vv.seq
Set
    ps.merchant_sku = pMerchantSKU,
    ps.stock = pStockAdd,
    ps.modified_by = pUserID,
    ps.modified_date = now()
Where
	product_variant_seq = pProductVariantSeq And variant_value_seq = pValueSeq;

Else
Insert Into m_product_stock (
	product_variant_seq,
    variant_value_seq,
    merchant_sku,
    stock,
    created_by,
    created_date,
    modified_by,
    modified_date
)	Values (
	pProductVariantSeq,
    pValueSeq,
    pMerchantSKU,
    pStockAdd,
    pUserID,
    Now(),
    pUserID,
    Now()
);
End If;

END