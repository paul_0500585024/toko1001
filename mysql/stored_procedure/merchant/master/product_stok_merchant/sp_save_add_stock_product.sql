CREATE DEFINER=`root`@`%` PROCEDURE `sp_save_add_stock_product`(
	pUserID VarChar(25),
    pIPAddress VarChar(25),
    pProductVariantSeq MediumInt unsigned,
    pVariantValueSeq smallint unsigned,
    pQty int unsigned

)
BEGIN

update m_product_stock set
	stock = stock + pQty
where
	product_variant_seq = pProductVariantSeq and
    variant_value_seq = pVariantValueSeq;


END