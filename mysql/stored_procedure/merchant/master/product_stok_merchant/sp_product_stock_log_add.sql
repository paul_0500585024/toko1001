CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_product_stock_log_add`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pProductVariantSeq Int(5),
        pVariantValueSeq Int(5),
        pDifferenceStock Decimal,
        pMutationType Char(1),
        pTrxType Char(5),
        pTrxNo Varchar(30)
    )
BEGIN
    Declare new_seq Int;
    
	Select 
		Max(seq) + 1 Into new_seq
	From m_product_stock_log
    where
		product_variant_seq = pProductVariantSeq and
        variant_value_seq = pVariantValueSeq;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;

	Insert Into m_product_stock_log(
		product_variant_seq,
        variant_value_seq,
		seq,
        mutation_type,
        trx_type,
        trx_no,
        trx_date,
        qty,
		created_by,
		created_date
	) Values (
		pProductVariantSeq,
		pVariantValueSeq,
        new_seq,
        pMutationType,
        pTrxType,
        pTrxNo,
		CURDATE(),
        pDifferenceStock,
		pUserID,
		Now()
	);

END