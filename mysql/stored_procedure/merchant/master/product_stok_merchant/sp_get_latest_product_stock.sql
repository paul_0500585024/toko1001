CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_latest_product_stock`(
	
    pUserID Varchar(25),
    pIPAddress VarChar(25),
    pProductVariantSeq BigInt unsigned,
    pVariantValueSeq smallint unsigned
)
BEGIN

declare pStock MediumInt unsigned;

select
	stock into pStock
from 
	m_product_stock 
where
	product_variant_seq = pProductVariantSeq and
    variant_value_seq = pVariantValueSeq;
    
if pStock is null then
    set pStock =0;
end if;

select 
    pStock as stock, 
    m.seq as merchant_seq, 
    m.name as merchant_name,
    p.name as product_name, 
    m.email as merchant_email,
    p.seq as product_seq,
    v.seq as value_seq,
    v.value
from m_product_variant pv join m_product p 
        on p.seq = pv.product_seq
                          join m_merchant m
        on m.seq = p.merchant_seq  
                          join m_variant_value v
        on v.seq = pv.variant_value_seq
where
    pv.seq = pProductVariantSeq;    

END