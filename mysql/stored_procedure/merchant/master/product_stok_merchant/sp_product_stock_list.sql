CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_stock_list`(
        pUserID VarChar(50),
        pMerchantSeq Int Unsigned,
        pIPAddr VarChar(50),
        pCurrPage Int,
        pRecPerPage Int,
        pDirSort VarChar(4),
        pColumnSort Varchar(50),
        pProductName Varchar(50),
        pVariant Varchar(50),
        pMerchantSKU Varchar(50)
    )
BEGIN
    -- Begin variables for Paging
    Declare startRec Int;
    Declare totalPage Int;
    Declare totalRec Int;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";

    If pProductName <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And p.name like '%" , escape_string(pProductName), "%'");
    End If;
    
    If pVariant <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And vv.value like '%" , escape_string(pVariant), "%'");
    End If;
    
    If pMerchantSKU <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And ps.merchant_sku like '%" , escape_string(pMerchantSKU), "%'");
    End If;

    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "Select Count(p.name) Into @totalRec From m_product p
        Left Join m_product_variant pv
			On p.seq = pv.product_seq
        Left Join m_variant_value vv
			On pv.variant_value_seq = vv.seq
		Left Join m_merchant m
			On p.merchant_seq = m.seq
		Left Join m_product_stock ps
				On ps.product_variant_seq = pv.seq
 			";
            
	Set @sqlCommand = Concat(@sqlCommand, "Where m.seq = '", pMerchantSeq , "'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
        Select
			pv.seq as product_variant_seq,
			p.name as product_name,
            (
				Select GROUP_CONCAT(mvv.value, 
									Case when ps.merchant_sku <> '' 
									Then concat(' [', ps.merchant_sku ,'] ')
									Else ''
									End, 
                                    ' = ' , ps.stock  SEPARATOR '; ')
				From m_product_stock s
				Join m_variant_value mvv
					On s.variant_value_seq = mvv.seq
                Where s.product_variant_seq = ps.product_variant_seq
			) as merchant_sku,
            pv.variant_value_seq,
            vv.value
		From m_product p
			Left Join m_product_variant pv
				On p.seq = pv.product_seq
			Left Join m_variant_value vv
				On pv.variant_value_seq = vv.seq
			Left Join m_merchant m
				On p.merchant_seq = m.seq
			Left Join m_product_stock ps
				On ps.product_variant_seq = pv.seq 
 			";

	Set @sqlCommand = Concat(@sqlCommand, "Where m.seq = '", pMerchantSeq , "'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;

    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);

    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END