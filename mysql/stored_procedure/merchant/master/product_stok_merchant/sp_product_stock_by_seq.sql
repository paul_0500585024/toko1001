CREATE DEFINER = 'root'@'%' PROCEDURE `sp_product_stock_by_seq`(
        pUserID VarChar(50),
        pIPAddr VarChar(50),
        pProductVariantSeq BigInt Unsigned,
        pVariantValueSeq BigInt Unsigned
    )
BEGIN

Select 
	pv.seq as product_variant_seq,
	p.`name` as product_name,
    vv.seq as variant_value_seq,
    vv.value
From 
	m_product p Join m_product_variant pv
		On pv.product_seq = p.seq
			    Join m_variant_value vv
		On vv.seq = pv.variant_value_seq
				Join m_merchant m
		On p.merchant_seq = m.seq
				Join m_merchant_info mi
		On m.seq = mi.merchant_seq
Where
	m.email = pUserID And pv.seq = pProductVariantSeq;


Select 
	vv.seq as value_seq,
	vv.value as size,
    ps.merchant_sku,
	Case When ps.stock Is null Then 0 Else ps.stock End As stock,
			(Select SUM(op.qty) 
				From t_order_product op 
				Join t_order_merchant om 
				Where om.order_status ='P'
					And op.product_variant_seq = ps.product_variant_seq
                    And op.variant_value_seq = ps.variant_value_seq
					And om.order_seq = op.order_seq
			) as pending_payment,
            (Select SUM(op.qty) 
				From t_order_product op 
				Join t_order_merchant om 
				Where om.order_status ='N'
					And op.product_variant_seq = ps.product_variant_seq 
                    And op.variant_value_seq = ps.variant_value_seq                    
					And om.order_seq = op.order_seq
			) as pending_delivery
From 
	m_variant_value vv left join m_product_stock ps		
				on ps.variant_value_seq = vv.seq and
				   ps.product_variant_seq = pProductVariantSeq
                
Where	
	vv.variant_seq = '1';


END;