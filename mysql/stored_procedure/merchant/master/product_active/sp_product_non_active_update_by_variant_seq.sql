CREATE DEFINER=`root`@`%` PROCEDURE `sp_product_non_active_update_by_variant_seq`(
		pUserid VarChar(25),
		pMerchantSeq int(10) unsigned,
        pIPAddr VarChar(50),
        pProductVarSeq int unsigned,
        pProductActive Char(1)
)
BEGIN
			update `m_product_variant` 
            set 
            `active` = pProductActive ,
            `modified_by` = pUserid,
            `modified_date` = now()
            where seq = pProductVarSeq;
END