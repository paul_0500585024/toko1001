CREATE DEFINER=`root`@`%` PROCEDURE `sp_main_page_merchant`(
        pNewProduct Char(1),
        pProductChange Char(1),
        pMerchantSeq TinyInt Unsigned,
        pOrderStatus Char(1)
    )
BEGIN

    Select Count(seq) as new_product 
		From m_product_new 
		Where `status` = pNewProduct And merchant_seq = pMerchantSeq;
    Select Count(seq) as product_change 
		From m_product 
		Where `status` = pProductChange And merchant_seq = pMerchantSeq;
    Select Count(stock) as out_of_stock 
		From m_product_stock ps
			Join m_product_variant pv
				On ps.product_variant_seq = pv.seq
			Join m_product p 
				On pv.product_seq = p.seq
        Where stock < '2' And p.merchant_seq = pMerchantSeq;
	Select Count(order_seq) as unprinted 
		From t_order_merchant om
			Join m_merchant_info mi
				On om.merchant_info_seq = mi.seq
        Where om.`order_status` = pOrderStatus And om.printed <> '0' And mi.merchant_seq = pMerchantSeq;
    
END