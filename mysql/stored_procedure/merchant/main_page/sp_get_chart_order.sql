CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_chart_order`(
		pMerchantSeq TinyInt Unsigned,
		pJanuaryFrom Datetime,
        pJanuaryTo Datetime,
        pFebruaryFrom Datetime,
        pFebruaryTo Datetime,
        pMarchFrom Datetime,
        pMarchTo Datetime,
        pAprilFrom Datetime,
        pAprilTo Datetime,
        pMayFrom Datetime,
        pMayTo Datetime,
        pJuneFrom Datetime,
        pJuneTo Datetime,
        pJulyFrom Datetime,
        pJulyTo Datetime,
        pAugustFrom Datetime,
        pAugustTo Datetime,
        pSeptemberFrom Datetime,
        pSeptemberTo Datetime,
        pOctoberFrom Datetime,
        pOctoberTo Datetime,
        pNovemberFrom Datetime,
        pNovemberTo Datetime,
        pDecemberFrom Datetime,
        pDecemberTo Datetime
)
BEGIN

	Select 
		(Select count(seq) 
			From t_order_merchant om
				Join m_merchant_info mi
					On om.merchant_info_seq = mi.seq
            Where (om.created_date Between pJanuaryFrom And pJanuaryTo) And mi.merchant_seq = pMerchantSeq
		) as january,
		(Select count(seq) 
			From t_order_merchant om
				Join m_merchant_info mi
					On om.merchant_info_seq = mi.seq
            Where (om.created_date Between pFebruaryFrom And pFebruaryTo) And mi.merchant_seq = pMerchantSeq
		) as february,
        (Select count(seq) 
			From t_order_merchant om
				Join m_merchant_info mi
					On om.merchant_info_seq = mi.seq
            Where (om.created_date Between pMarchFrom And pMarchTo) And mi.merchant_seq = pMerchantSeq
		) as march,
        (Select count(seq) 
			From t_order_merchant om
				Join m_merchant_info mi
					On om.merchant_info_seq = mi.seq
            Where (om.created_date Between pAprilFrom And pAprilTo) And mi.merchant_seq = pMerchantSeq
		) as april,
        (Select count(seq) 
			From t_order_merchant om
				Join m_merchant_info mi
					On om.merchant_info_seq = mi.seq
            Where (om.created_date Between pMayFrom And pMayTo) And mi.merchant_seq = pMerchantSeq
		) as may,
        (Select count(seq) 
			From t_order_merchant om
				Join m_merchant_info mi
					On om.merchant_info_seq = mi.seq
            Where (om.created_date Between pJuneFrom And pJuneTo) And mi.merchant_seq = pMerchantSeq
		) as june,
        (Select count(seq) 
			From t_order_merchant om
				Join m_merchant_info mi
					On om.merchant_info_seq = mi.seq
            Where (om.created_date Between pJulyFrom And pJulyTo) And mi.merchant_seq = pMerchantSeq
		) as july,
        (Select count(seq) 
			From t_order_merchant om
				Join m_merchant_info mi
					On om.merchant_info_seq = mi.seq
            Where (om.created_date Between pAugustFrom And pAugustTo) And mi.merchant_seq = pMerchantSeq
		) as august,
        (Select count(seq) 
			From t_order_merchant om
				Join m_merchant_info mi
					On om.merchant_info_seq = mi.seq
            Where (om.created_date Between pSeptemberFrom And pSeptemberTo) And mi.merchant_seq = pMerchantSeq
		) as september,
        (Select count(seq) 
			From t_order_merchant om
				Join m_merchant_info mi
					On om.merchant_info_seq = mi.seq
            Where (om.created_date Between pOctoberFrom And pOctoberTo) And mi.merchant_seq = pMerchantSeq
		) as october,
        (Select count(seq) 
			From t_order_merchant om
				Join m_merchant_info mi
					On om.merchant_info_seq = mi.seq
            Where (om.created_date Between pNovemberFrom And pNovemberTo) And mi.merchant_seq = pMerchantSeq
		) as november,
        (Select count(seq) 
			From t_order_merchant om
				Join m_merchant_info mi
					On om.merchant_info_seq = mi.seq
            Where (om.created_date Between pDecemberFrom And pDecemberTo) And mi.merchant_seq = pMerchantSeq
		) as december
    ;

END
