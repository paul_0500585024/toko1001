CREATE DEFINER=`root`@`%` PROCEDURE `sp_merchant_update_password_by_email`(
	pEmail VarChar(50),
    pNewpass VarChar(100)
)
BEGIN

	Update m_merchant Set 
		new_password = pNewpass 
	Where 
		email = pEmail;

END