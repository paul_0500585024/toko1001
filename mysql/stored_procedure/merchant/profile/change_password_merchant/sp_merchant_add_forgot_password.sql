CREATE DEFINER=`root`@`%` PROCEDURE `sp_merchant_add_forgot_password`(
	pEmail VarChar(50),
	pEmail_cd VarChar(50),
	pName VarChar(50),
	pGencode VarChar(50),
	pSubject VarChar(100),
	pContent Text
)
BEGIN
	Declare new_seq BigInt unsigned;

    Select 
        Max(seq) + 1 Into new_seq
    From t_email_log;

    If new_seq Is Null Then
        Set new_seq = 1;
    End If;

	Insert Into  t_email_log(
		email_cd,
		seq,
		code,
		recipient_name,
		recipient_email,
		email_subject,
		email_content,
		sent_status,
		created_by,
		created_date
	) values (
		pEmail_cd,
		new_seq, 
		pGencode, 
		pName,
		pEmail,
		pSubject,
		pContent,
		'1',
		'admin',
		now()
	);

END