CREATE DEFINER=`root`@`%` PROCEDURE `sp_change_password_merchant`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pMerchantSeq TinyInt Unsigned,
    pNewPassword VarChar(1000)
)
BEGIN  
	Update
		m_merchant
    Set 
		new_password = pNewPassword,
        modified_by = pUserID,
        modified_date = Now()
    Where seq = pMerchantSeq;
    
End