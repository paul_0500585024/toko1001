CREATE DEFINER=`root`@`%` PROCEDURE `sp_merchant_log_data_add_old_data`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pMerchantSeq Int unsigned,
    pLogSeq SmallInt unsigned
)
BEGIN

	Insert Into m_merchant_log_data(     
		merchant_seq,
        log_seq,
        `type`,
        address,
        district_seq,
        zip_code,
		phone_no,
		fax_no,
		pic1_name,
		pic1_phone_no,
		pic2_name,
		pic2_phone_no,
		pickup_addr_eq,
		pickup_addr,
		pickup_district_seq,
		pickup_zip_code,
        return_addr_eq,
		return_addr,
		return_district_seq,
		return_zip_code,
        bank_name,
        bank_branch_name,
        bank_acct_no,
        bank_acct_name,
        created_by,
		created_date,
		modified_by,
		modified_date
	)
	Select
		pMerchantSeq,
		pLogSeq,
        '1',
		address,
		district_seq,
		zip_code,
		phone_no,
		fax_no,
		pic1_name,
		pic1_phone_no,
		pic2_name,
		pic2_phone_no,
		pickup_addr_eq,
		pickup_addr,
		pickup_district_seq,
		pickup_zip_code,
        return_addr_eq,
		return_addr,
		return_district_seq,
		return_zip_code,
        bank_name,
        bank_branch_name,
        bank_acct_no,
        bank_acct_name,
        pUserID,
        now(),
        pUserID,
        now() 
	From m_merchant 
	where 
		seq = pMerchantSeq;

END