CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_merchant_log_data_add`(
	pUserID VarChar(25),
	pIPAddr VarChar(50), 
	pMerchantSeq SmallInt unsigned,
	pLogSeq TinyInt unsigned,
	pAddress Text,
	pDistrictSeq SmallInt unsigned,
	pZipCode VarChar(10),
	pPhoneNo VarChar(50),
	pFaxNo VarChar(50),
	pPic1Name VarChar(50),
	pPic1PhoneNo VarChar(50),
	pPic2Name VarChar(50),
	pPic2PhoneNo VarChar(50),
	pPickupAddrEq Char(1),
	pPickupAddr Text,
	pPickupDistrictSeq SmallInt unsigned,
	pPickupZipCode VarChar(10),
	pReturnAddrEq Char(1),
	pReturnAddr Text,
	pReturnDistrictSeq MediumInt unsigned,
	pReturnZipCode VarChar(10),
	pBankName VarChar(50),
	pBankBranchName VarChar(50),
	pBankAcctNo VarChar(50),
	pBankAcctName VarChar(50)
)
BEGIN

	If pPickupAddrEq <> "0" Then
		set pPickupAddr = "";
		set pPickupDistrictSeq = Null;
		set pPickupZipCode = "";    
	End If;

	If pReturnAddrEq <> "0" Then
		set pReturnAddr = "";
		set pReturnDistrictSeq = Null;
		set pReturnZipCode = "";    
	End If;

	Insert Into m_merchant_log_data(     
		merchant_seq,
        log_seq,
        `type`,
        address,
        district_seq,
        zip_code,
		phone_no,
		fax_no,
		pic1_name,
		pic1_phone_no,
		pic2_name,
		pic2_phone_no,
		pickup_addr_eq,
		pickup_addr,
		pickup_district_seq,
		pickup_zip_code,
        return_addr_eq,
		return_addr,
		return_district_seq,
		return_zip_code,
        bank_name,
        bank_branch_name,
        bank_acct_no,
        bank_acct_name,
        created_by,
		created_date,
		modified_by,
		modified_date
	) Values (
		pMerchantSeq,
		pLogSeq,
        '2',
        pAddress,
        pDistrictSeq,
		pZipCode,
		pPhoneNo,
		pFaxNo,
		pPic1Name,
		pPic1PhoneNo,
		pPic2Name,
		pPic2PhoneNo,
		pPickupAddrEq,
		pPickupAddr,
		pPickupDistrictSeq,
		pPickupZipCode,
        pReturnAddrEq,
		pReturnAddr,
		pReturnDistrictSeq,
		pReturnZipCode,
        pBankName,
        pBankBranchName,
        pBankAcctNo,
        pBankAcctName,
		pUserID,
		Now(),
		pUserID,
		Now()
	);
    
	Update m_merchant Set
		`status`='C'
	Where 
		seq = pMerchantSeq;

END