CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_merchant_code`(
        pCode VarChar(100)
    )
BEGIN

	Select
		seq,
		code
	From m_merchant_new
	Where
		code = pCode
	Union All
	Select
		seq,
		code
	From m_merchant
	Where
		code = pCode;

END