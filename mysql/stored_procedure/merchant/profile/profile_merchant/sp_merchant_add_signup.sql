CREATE DEFINER=`root`@`%` PROCEDURE `sp_merchant_add_signup`(
	pUserID VarChar(100),
	pIPAddr VarChar(50),
	pEmail VarChar(100),
	pName VarChar(100),
	pPhone VarChar(50),
	pAddress mediumtext,
    pCode varchar(100)
)
BEGIN
	Declare new_seq int unsigned;

    Select 
        Max(seq) + 1 Into new_seq
    From m_merchant_new;

    If new_seq Is Null Then
        Set new_seq = 1;
    End If;

	Insert Into  m_merchant_new(
		seq,
        `code`,
		email,
		`name`,
		address,
		district_seq,
		zip_code,
		phone_no,
		fax_no,
		pic1_name,
		pic1_phone_no,
		pic2_name,
		pic2_phone_no,
		expedition_seq,
		pickup_addr_eq,
		pickup_addr,
		pickup_district_seq,
		pickup_zip_code,
		return_addr_eq,
		return_addr,
		return_district_seq,
		return_zip_code,
		exp_fee_percent,
		ins_fee_percent,
		`notes`,
		`status`,
		created_by,
		created_date,
		modified_by,
		modified_date
	) values (
		new_seq,
        pCode,
		pEmail, 
		pName , 
		pAddress,
		NULL,
		'',
		pPhone,
		'',
		pName,
		'',
		'',
		'',
		0,
		'1',
		'',
		NULL,
		'',
		'1',
		'',
		NULL,
		'',
		'100',
		'100',
		'',
		'W',
		'SYSTEM',
		now(),
		'SYSTEM',
		now()
	);

END