CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_data_merchant`(
	pIPAddr VarChar(50),
    pMerchantSeq BigInt(10)
)
BEGIN

Select
	seq, 
    user_group_seq, 
   `code`, 
    email, 
    old_password, 
    new_password,
    `name`, 
    address, 
    district_seq, 
    zip_code, 
    phone_no, 
    fax_no, 
    pic1_name, 
    pic1_phone_no, 
    pic2_name, 
    pic2_phone_no, 
    expedition_seq, 
    pickup_addr_eq, 
    pickup_addr, 
    pickup_district_seq, 
    pickup_zip_code, 
    return_addr_eq, 
    return_addr, 
    return_district_seq, 
    return_zip_code, 
    bank_name, 
    bank_branch_name, 
    bank_acct_no, 
    bank_acct_name, 
    exp_fee_percent, 
    ins_fee_percent, 
    logo_img, 
    banner_img, 
    welcome_notes, 
    notes, 
    `status`
From 
	m_merchant
Where 
	seq = pMerchantSeq;
   
    
END