CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_merchant_log_data_by_type`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
	pMerchantSeq Int unsigned
)
BEGIN

	If Exists (Select merchant_seq From m_merchant_log Where status = 'N' And merchant_seq = pMerchantSeq) Then
		Select
			ml.merchant_seq,
			ml.seq,
			ml.`status`,
			mld.`type`,
			mld.address, 
			mld.district_seq, 
			mld.zip_code, 
			mld.phone_no, 
			mld.fax_no, 
			mld.pic1_name, 
			mld.pic1_phone_no, 
			mld.pic2_name, 
			mld.pic2_phone_no,
			mld.pickup_addr_eq,
			mld.pickup_addr,
			mld.pickup_district_seq,
			mld.pickup_zip_code,
			mld.return_addr_eq,
			mld.return_addr,
			mld.return_district_seq,
			mld.return_zip_code,
			mld.bank_name,
			mld.bank_branch_name,
			mld.bank_acct_no,
			mld.bank_acct_name,
            ml.created_date,
            ml.modified_date
		From m_merchant_log ml 
		Join m_merchant_log_data mld On 
			ml.merchant_seq = mld.merchant_seq And 
			ml.seq = mld.log_seq
		Where 
			mld.merchant_seq = pMerchantSeq And 
			mld.type = 2 And 
			ml.status = 'N';
	Else
		Select
			seq as merchant_seq,
			'L' as status,
			address, 
			district_seq, 
			zip_code, 
			phone_no, 
			fax_no, 
			pic1_name, 
			pic1_phone_no, 
			pic2_name, 
			pic2_phone_no,
			pickup_addr_eq,
			pickup_addr,
			pickup_district_seq,
			pickup_zip_code,
			return_addr_eq,
			return_addr,
			return_district_seq,
			return_zip_code,
			bank_name,
			bank_branch_name,
			bank_acct_no,
			bank_acct_name,
			(Select created_date From m_merchant_log Where merchant_seq = m.seq Order By seq Desc Limit 1) as created_date,
			(Select auth_date From m_merchant_log Where merchant_seq = m.seq Order By seq Desc Limit 1) as modified_date
		From m_merchant m
		Where
			seq = pMerchantSeq;
	End If;

END