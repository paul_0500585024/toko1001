CREATE DEFINER=`root`@`%` PROCEDURE `sp_merchant_log_security_check`(
	pUri varchar(50)
)
BEGIN

	Select
		code
	From m_merchant_log_security
	Where 
		code = pUri;

END