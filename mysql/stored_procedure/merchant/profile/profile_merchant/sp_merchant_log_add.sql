CREATE DEFINER=`root`@`%` PROCEDURE `sp_merchant_log_add`(
	pUserID varchar(25), 
	pIPAddr VarChar(50),
    pMerchantSeq Int unsigned
)
BEGIN
	Declare new_seq SmallInt unsigned;

	Select 
		Max(seq) + 1 Into new_seq
	From m_merchant_log where merchant_seq = pMerchantSeq ;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
    
	Insert Into m_merchant_log(
		merchant_seq,
		seq,
		`status`,
		auth_by,
		auth_date,
		created_by,
		created_date,
		modified_by,
		modified_date
	) Values (
		pMerchantSeq,
		new_seq,
		'N',
		"",
		default,
		pUserID,
		now(),
		"",
		default
	);
	
	Select new_seq;

END