CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_password_merchant`(
	pMerchantSeq TinyInt Unsigned
)
BEGIN

	SELECT
		seq,
		email,
		old_password,
		new_password,
		name
	FROM m_merchant
	WHERE seq = pMerchantSeq;

END