CREATE DEFINER=`root`@`%` PROCEDURE `sp_login_merchant`(
    pUserID VARCHAR(100),
    pPassword VARCHAR(100),
    pIPAddress Varchar(50)
)
BEGIN

Declare new_seq Int;
Declare merchant_seq Int;

Select 
	Max(seq) + 1 Into new_seq
From m_merchant_login_log;

If new_seq Is Null Then
	Set new_seq = 1;
End If;

Select 
	u.seq,
	u.email,
    u.`name` as user_name,
    u.old_password,
    u.new_password,
    u.user_group_seq,
    u.last_login,
    g.`name`,
    u.logo_img
From
	m_merchant u
Join 
	m_user_group g On g.seq = u.user_group_seq
Where 
	u.email = pUserID;

Select 
	m.`name` as menu_name,
    m.title_name,
	up.menu_cd,
    up.can_add,
    up.can_edit,
    up.can_view,
    up.can_delete,
    up.can_print,
    up.can_auth
From 
	m_merchant u 
Join 
	m_user_group_permission up on up.user_group_seq = u.user_group_seq
Join
	s_menu m on m.menu_cd = up.menu_cd
Where
	u.email = pUserID;

if exists (select seq from m_merchant where email = pUserID and new_password = pPassword) then
Begin 
	update m_merchant set
		last_login = now(),
        ip_address = pIPAddress
	Where
		email = pUserID;
	
    select 
		seq into merchant_seq 
	from
		m_merchant
	where
		email = pUserID;
    
    insert into m_merchant_login_log (
		seq,
        merchant_seq,
		ip_address,
        status,
        created_date
	) values (
		new_seq,
        merchant_seq,
        pIPAddress,
        'S',
        now()
	);
End;
Else
	if Exists(select seq from m_merchant where email = pUserID) then
	Begin
        select 
			seq into merchant_seq 
		from
			m_merchant
		where
			email = pUserID;
            
		insert into m_merchant_login_log (
			seq,
			merchant_seq,
			ip_address,
			status,
			created_date
		) values (
			new_seq,
			merchant_seq,
			pIPAddress,
			'F',
			now()
		);
	End;
	end if;
End if;
    

END