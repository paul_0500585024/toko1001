CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_tree_view_category`()
BEGIN
	Select
		seq,
        `name`,
        IFNULL(parent_seq, 0) as parent_seq,
        level 
	From
		m_product_category
	Where
		seq> 0 
	Order By 
		`seq` Asc;
    
END