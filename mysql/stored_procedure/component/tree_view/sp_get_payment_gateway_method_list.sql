DROP PROCEDURE IF EXISTS `sp_get_payment_gateway_method_list`;
DELIMITER $$
CREATE PROCEDURE `sp_get_payment_gateway_method_list` (
     pPaymentGatewaySeq int unsigned
)
BEGIN
SELECT 
    pg_seq,
    seq,
    `code`,
    `name`,
    logo_img,
    `order`,
    notes,
    active,
    cancel_pg_method_seq
FROM
    m_payment_gateway_method
WHERE
    pg_seq = pPaymentGatewaySeq AND active = '1';
END$$

