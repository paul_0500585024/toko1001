CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_payment_gateway_list`()
BEGIN

Select
	100 + m.seq parent_seq,
    pg.seq,
    pg.`name`,
	pg.logo_img,
    m.`order`as m_order,
    pg.`order` as pg_order
from 
	m_payment_gateway m join m_payment_gateway_method pg
		on pg.pg_seq = m.seq
Where
	m.active = '1' and pg.active = '1'
union all
Select
	0,
    100 + seq,
    `name`,
    '-',
    `order`,
	0
from
	m_payment_gateway
Where
	active = '1'
order by 
	m_order,
    pg_order;
    
END