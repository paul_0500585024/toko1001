CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_tree_view_menu`(
	pModuleSeq TinyInt unsigned
)
BEGIN

Select
	menu_cd,
    case when parent_menu_cd is null then 'ROOT' else parent_menu_cd end as parent_menu_cd,
    name,
    url,
    `order`,
    icon,
    detail,
    active
From 
	s_menu
Where 
	module_seq = pModuleSeq
Order by `order`;

END