DROP PROCEDURE IF EXISTS `sp_get_payment_gateway_type_list`;
DELIMITER $$
CREATE PROCEDURE `sp_get_payment_gateway_type_list` (
)
BEGIN

SELECT
    seq,
    `name`,
    `order`,
    notes, 
    active,
    logo_img
FROM
    m_payment_gateway
WHERE
    active = '1'
order by `order`;

END$$

