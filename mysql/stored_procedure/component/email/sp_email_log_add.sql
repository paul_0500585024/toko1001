CREATE DEFINER=`root`@`%` PROCEDURE `sp_email_log_add`(
        pUserID VarChar(100),
        pIPAddr VarChar(50),
        pEmailCd VarChar(50),
        pCode VarChar(20),
        pReceiptName VarChar(150),
        pReceiptEmail VarChar(150),
        pEmailSubject VarChar(255),
        pEmailContent TEXT,
        pStatus Char(1)
    )
BEGIN
	Declare new_seq Bigint unsigned;
	Declare user_seq Bigint unsigned;

	SELECT 
    MAX(seq) + 1
INTO new_seq FROM
    `t_email_log`;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;

	SELECT 
		seq INTO user_seq 
	FROM
		m_member
			WHERE
				email = pUserID;
						
		SELECT 
			seq INTO user_seq 
		FROM
			m_merchant
				WHERE
					email = pUserID;
            
	Insert Into `t_email_log`(     
	  `email_cd`,
	  `seq`,
	  `code`,
	  `recipient_name`,
	  `recipient_email`,
	  `email_subject`,
	  `email_content`,
	  `sent_status`,
	  `created_by`,
	  `created_date`
	) Values (
		pEmailCd,
		new_seq,
		pCode,
		pReceiptName,
		pReceiptEmail,
		pEmailSubject,
		pEmailContent,
		pStatus,		
		case when user_seq is null then pUserID else user_seq end,
		Now()
	);
END