CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_email_log_by_code_only`(
        pUserID VarChar(50),
        pIPAddr VarChar(50),
        pUri VARCHAR(50)
    )
BEGIN

SELECT 
  `email_cd`,`seq`,`code`,`recipient_name`,`recipient_email`,
  `email_subject`, `email_content`, `sent_status`,`created_by`,`created_date`
FROM 
  `t_email_log` 
where `code` = pUri;
        
END