CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_email_template_by_code`(
 		pUserID VarChar(50),
        pIPAddr VarChar(50),
        pCode VARCHAR(50)
    )
BEGIN

SELECT 
  `code`,
  `subject`,
  content,
  email_to,
  notes,
  created_by,
  created_date,
  modified_by,
  modified_date
FROM
	m_email_template 
Where 
	`code` = pCode;
        
END