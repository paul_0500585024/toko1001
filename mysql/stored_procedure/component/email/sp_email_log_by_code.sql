CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_email_log_by_code`(
        pUri VARCHAR(50),
        pEmailCd Varchar(50)
    )
BEGIN

SELECT 
  recipient_email,
  recipient_name,
  created_date
FROM 
  `t_email_log` 
where `code` = pUri and
	   email_cd = pEmailCD;
        
END