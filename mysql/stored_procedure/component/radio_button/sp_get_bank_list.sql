CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_bank_list`(
)
BEGIN

	Select
		seq,
		bank_name,
		bank_branch_name,
		bank_acct_no,
		bank_acct_name,
		logo_img
	From m_bank_account
	Where
		active = '1' 
	Order By 
		`order`;

END