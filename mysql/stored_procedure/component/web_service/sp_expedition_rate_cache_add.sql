CREATE DEFINER=`root`@`%` PROCEDURE `sp_expedition_rate_cache_add`(
	
    pUserID Varchar(25),
    pIPAddress Varchar(25),
    pExpSeq Tinyint unsigned,
    pFromDistrictCode Varchar(50),
    pToDistrictCode VarChar(50),
	pExpServiceSeq Tinyint unsigned,
    pRate Decimal(10,0)

)
BEGIN

insert into t_expedition_rate_cache ( 
	exp_seq,
    from_district_code,
    to_district_code,
    exp_service_seq,
    rate,
    modified_by,
    modified_date
) values (
	pExpSeq,
    pFromDistrictCode,
    pToDistrictCode,
    pExpserviceSeq,
    pRate,
    pUserID,
    now()
);


END