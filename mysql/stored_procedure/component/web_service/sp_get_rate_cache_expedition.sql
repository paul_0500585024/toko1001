CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_rate_cache_expedition`(
	
    pUserID VarChar(25),
    pIPAddress VarChar(25),
    pExpSeq tinyint unsigned,
    pFromDistrictCode VarChar(50),
    pToDistrictCode VarChar(50),
	pExpServiceSeq tinyint unsigned
)
BEGIN

	Select 
		rate
	from 
		t_expedition_rate_cache
	where
		exp_seq = pExpSeq and
        from_district_code = pFromDistrictCode and
        to_district_code = pToDistrictCode and
        exp_service_seq = pExpServiceSeq;

END