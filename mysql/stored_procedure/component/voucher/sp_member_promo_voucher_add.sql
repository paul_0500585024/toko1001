CREATE DEFINER=`root`@`%` PROCEDURE `sp_member_promo_voucher_add`(
        pPromoSeq int(10) unsigned,
        pMemberSeq int(10) unsigned,
        pCode varchar(20),
        pActiveDate date,
		pExpDate date
    )
BEGIN
Declare new_seq int unsigned;
SELECT 
    MAX(seq) + 1
INTO new_seq FROM
    m_promo_voucher;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
    
Insert Into m_promo_voucher (
    promo_seq,
    seq,
	member_seq,
    `code`,
	nominal,
    active_date,
    exp_date,
	trx_use_amt,
    trx_no,
    created_by,
    created_date,
    modified_by,
    modified_date
)
Select 
    seq,
	new_seq,
    pMemberSeq,
    pCode,
    nominal,
	pActiveDate,
	pExpDate,
    trx_use_amt,
    '',
    pMemberSeq,
    now(),
    pMemberSeq,
	now()
from m_promo_voucher_period 
where
	seq = pPromoSeq;


END