CREATE DEFINER=`root`@`%` PROCEDURE `sp_merchant_deliver_refund_add`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
	pMemberSeq Int unsigned,
	pMtype Char(1),
	pPgMSeq SmallInt unsigned,
	pTrxNo VarChar(50),
	pDepositTrxAmt Decimal(10,0),
	pNonDeposit Decimal(10,0)
)
BEGIN

	Declare new_seq Int unsigned;
	
	Select 
		Max(seq) + 1 Into new_seq
	From `t_member_account` 
	where
		`member_seq` = pMemberSeq;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
    
    Insert Into `t_member_account`(
		`member_seq`,
		`seq`,
		`mutation_type`,
		`pg_method_seq`,
		`trx_type`,
		`trx_no`,
		`trx_date`,
		`deposit_trx_amt`,
		`non_deposit_trx_amt`,
		`bank_name`,
		`bank_branch_name`,
		`bank_acct_no`,
		`bank_acct_name`,
		`refund_date`,
		`status`,
		`created_by`,
		`created_date`,
		`modified_by`,
		`modified_date`
	) values (
		pMemberSeq,
		new_seq,
		pMtype,
		pPgMSeq,
		'CNL',
		pTrxNo,
		now(),
		pDepositTrxAmt,
		pNonDeposit,
		'',
		'',
		'',
		'',
		'0000-00-00',
		'N',
		pUserID,
		now(),
		pUserID,
		now()
    );

END