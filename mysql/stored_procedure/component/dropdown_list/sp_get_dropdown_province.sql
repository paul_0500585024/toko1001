CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_dropdown_province`()
BEGIN

Select
	seq,
	`name`
From
	m_province
Where
	active = '1'
Order By
	`name`;
    
END