CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_dropdown_product`()
BEGIN
SELECT 
    p.seq, 
    CONCAT(p.`name`, ' | ', m.`name`) as product
FROM
    m_product p
        JOIN
    m_merchant m ON p.merchant_seq = m.seq
ORDER BY p.`name`;
END