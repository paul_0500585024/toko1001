CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_dropdown_attribute`(
)
BEGIN

Select 
	seq,
    `name`
From
	m_attribute
Where
	active = '1'
Order By 
	`name`;

END