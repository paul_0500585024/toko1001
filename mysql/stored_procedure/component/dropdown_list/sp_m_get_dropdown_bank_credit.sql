CREATE DEFINER=`root`@`%` PROCEDURE `sp_m_get_dropdown_bank_credit`(
	pPromoCreditSeq  tinyint unsigned,
    pProductVariantSeq int unsigned
)
BEGIN
	SELECT 
		cb.seq,
		bc.credit_month,
		bc.rate,
		b.bank_name,
		CONCAT(bc.credit_month,
				' Bln',
				' - ',
				b.bank_name) as bank_credit
	FROM
		m_promo_credit_bank cb
			JOIN
		m_promo_credit pc ON pc.seq = cb.promo_seq
			JOIN
		m_promo_credit_product cp ON cp.promo_credit_seq = cb.promo_seq
			JOIN
		m_bank_credit bc ON bc.seq = cb.bank_credit_seq
			JOIN
		m_bank b ON b.seq = bc.bank_seq
	WHERE
		cb.promo_seq = pPromoCreditSeq AND status = 'A' 
			AND cp.product_variant_seq = pProductVariantSeq
			AND NOW() BETWEEN pc.promo_credit_period_from AND pc.promo_credit_period_to
	ORDER BY bc.rate;
END