CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_dropdown_member_address`(
	
    pMemberSeq BigInt unsigned

)
BEGIN

Select 	
	seq,
    alias
From
	m_member_address
Where
	member_seq = pMemberSeq;
    


END