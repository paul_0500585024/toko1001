CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_dropdown_merchant_by_order`(
	pOrderSeq BigInt Unsigned
)
BEGIN

Select 
	m.seq as merchant_seq,
    mi.seq as merchant_info_seq,
    m.`name` as merchant_name
From t_order o
	Join t_order_merchant om 
		On o.seq = om.order_seq
	Join m_merchant_info mi 
		On om.merchant_info_seq = mi.seq
	Join m_merchant m 
		On mi.merchant_seq = m.seq
Where o.seq= pOrderSeq
/* and om.exp_invoice_seq Is Null and om.order_status = 'D';*/
Order By m.`name`;
    
END