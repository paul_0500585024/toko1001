DROP PROCEDURE IF EXISTS `sp_get_dropdown_credit`;
DELIMITER $$
CREATE PROCEDURE `sp_get_dropdown_credit` (
    pProductVariant Bigint Unsigned
)
BEGIN
select * from (
SELECT
    pc.seq,
    pcb.seq as credit_bank_seq,
    pc.`status`,
    pc.promo_credit_name,
    pc.promo_credit_period_from,
    pc.promo_credit_period_to,
    bc.credit_month,
    bc.rate,
    bc.bank_seq,
    pv.sell_price,
    b.bank_name
FROM
    m_promo_credit pc
        JOIN
    m_promo_credit_bank pcb ON pcb.promo_seq = pc.seq
        JOIN
    m_promo_credit_product pcp ON pcp.promo_credit_seq = pc.seq
        JOIN
    m_bank_credit bc ON bc.seq = pcb.bank_credit_seq
        JOIN
    m_product_variant pv ON pv.seq = pcp.product_variant_seq
        JOIN
    m_bank b ON bc.bank_seq = b.seq
WHERE
    pcp.product_variant_seq = pProductVariant and
    pc.status ='A' and
    CURDATE() between pc.promo_credit_period_from and pc.promo_credit_period_to
    and pv.sell_price between pc.minimum_nominal and pc.maximum_nominal
union all
SELECT
    pc.seq,
    pcb.seq as credit_bank_seq,
    pc.`status`,
    pc.promo_credit_name,
    pc.promo_credit_period_from,
    pc.promo_credit_period_to,
    bc.credit_month,
    bc.rate,
	bc.bank_seq,
    pv.sell_price,
    b.bank_name
FROM
    m_promo_credit pc
        JOIN
    m_promo_credit_bank pcb ON pcb.promo_seq = pc.seq
        JOIN
    m_promo_credit_category pcc ON pcc.promo_credit_seq = pc.seq
        JOIN
    m_bank_credit bc ON bc.seq = pcb.bank_credit_seq
        JOIN
    m_bank b ON bc.bank_seq = b.seq
        JOIN
    (select mpc.parent_seq,mpv.sell_price from m_product mp join m_product_category mpc on mp.category_l2_seq=mpc.seq
    join m_product_variant mpv on mp.seq=mpv.product_seq where mpv.seq=pProductVariant) pv
    on pv.parent_seq=pcc.category_seq
WHERE
    pc.status ='A' and
    CURDATE() between pc.promo_credit_period_from and pc.promo_credit_period_to
    and pv.sell_price between pc.minimum_nominal and pc.maximum_nominal
) a order by a.bank_name,a.credit_month
;
END

