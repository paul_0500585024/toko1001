CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_dropdown_redeem_period`(
	pExpSeq tinyint unsigned,
    pSeq int unsigned
)
BEGIN

Select 
	seq as period_seq,
	CONCAT(
	CAST(Date_format(
		from_date,'%d-%M-%Y') as Char(25)),
		' - ',
	CAST(Date_format(
		to_date,'%d-%M-%Y') as Char(25))
	) as period_date,
    `status`
From
	t_redeem_period
Where
	`status` = 'O'
Union
Select 
	t.seq as period_seq,
	CONCAT(
	CAST(Date_format(
		t.from_date,'%d-%M-%Y') as Char(25)),
		' - ',
	CAST(Date_format(
		t.to_date,'%d-%M-%Y') as Char(25))
	) as period_date,
    t.`status`
From
	t_redeem_period t join t_expedition_invoice e
		on e.redeem_seq = t.seq
Where
	e.exp_seq = pExpSeq And
    e.seq = pSeq;



    
END