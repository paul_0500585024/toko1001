CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_dropdown_member_by_name`()
BEGIN
	SELECT 
		seq, CONCAT(`name`, ' | ', `email`) AS member_two_field
	FROM
		m_member
	ORDER BY `name`;
END