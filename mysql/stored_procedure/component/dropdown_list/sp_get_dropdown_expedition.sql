CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_dropdown_expedition`(

)
BEGIN

Select 
	seq,
    `code`,
	`name`
From 
	m_expedition
Where
	active = '1'
Order By 
	`name`;

END