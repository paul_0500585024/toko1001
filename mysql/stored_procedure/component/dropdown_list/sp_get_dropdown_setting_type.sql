CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_dropdown_setting_type`()
BEGIN

Select
	seq,
	`name`
From
	s_setting_type
Where
	active = '1'
Order By
	`name`;
    
END