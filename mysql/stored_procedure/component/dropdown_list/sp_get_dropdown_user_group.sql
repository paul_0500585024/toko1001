CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_dropdown_user_group`()
BEGIN

Select
	seq,
	`name`
From
	m_user_group
Where
	active = '1'
Order By
	`name`;
    
END