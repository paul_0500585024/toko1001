CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_dropdown_district`()
BEGIN

Select
	seq,
	`name`
From
	m_district
Where 
	active = '1'
Order By 
	`name`;
    
END