CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_dropdown_menu_module`(

)
BEGIN

Select 
	seq, 
	`name`
From 
	s_menu_module
Where
	active = '1'
Order By
	`name`;
    
END