CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_dropdown_order_no_by_order`(
	pExpeditionSeq TinyInt Unsigned
)
BEGIN

Select
	o.seq as order_seq,
	o.order_no
From
	t_order_merchant om
    Join m_expedition_service es
		On es.seq = om.real_expedition_service_seq
	Join t_order o 
		On om.order_seq = o.seq
Where
	es.exp_seq = pExpeditionSeq And
	es.active = '1'
Order By 
	o.order_no;
    
END