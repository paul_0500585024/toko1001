CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_dropdown_member_address_by_seq`(
	
    pMemberSeq Bigint unsigned,
    pSeq Smallint unsigned
	
)
BEGIN

Declare pCitySeq SmallInt unsigned;
Declare pProvinceSeq Tinyint unsigned;

select city_seq into pCitySeq 
from 
	m_district d join m_member_address m
		on m.district_seq = d.seq
where
	m.member_seq = pMemberSeq and
    m.seq = pSeq;

select province_seq into pProvinceSeq 
from 
	m_district d join m_member_address m
		on m.district_seq = d.seq
				 join m_city c
		on d.city_seq = c.seq
where
	m.member_seq = pMemberSeq and
    m.seq = pSeq;

Select
	m.pic_name,
    m.phone_no,
    m.zip_code,
    m.address,
    c.province_seq,
    d.city_seq,
    m.district_seq
from
	m_member_address m join m_district d
		on d.seq = m.district_seq
					   join m_city c
		on c.seq = d.city_seq
					   join m_province p
		on p.seq = c.province_seq
where
	m.member_seq = pMemberSeq and
    m.seq = pSeq;

Select
	seq,
	name
from
	m_city
where
	province_seq = pProvinceSeq;

Select
	seq,
    name
from
	m_district
where
	city_seq = pCitySeq;
    
    
	
	

END