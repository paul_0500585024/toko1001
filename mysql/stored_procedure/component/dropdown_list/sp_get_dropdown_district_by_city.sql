CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_dropdown_district_by_city`(
	pCitySeq MediumInt Unsigned
)
BEGIN

Select
	d.seq,
	d.`name`
From m_district d 
	Join m_city c
		On d.city_seq = c.seq
Where	
	d.city_seq = pCitySeq And
    d.active = '1'
Order By
	d.`name`;

END