CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_dropdown_expedition_service`(
	pExpeditionSeq TinyInt Unsigned
)
BEGIN

Select
	c.seq,
	c.`name`
From
	 m_expedition_service c 
     Join m_expedition p
		On p.seq = c.exp_seq
Where	
	c.exp_seq = pExpeditionSeq And 
    c.active = '1'
Order By
	c.`name`;

END