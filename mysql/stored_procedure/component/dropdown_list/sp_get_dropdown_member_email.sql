CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_dropdown_member_email`()
BEGIN

Select 
	seq,
	email 
From 
	m_member
Order By
	email;

END