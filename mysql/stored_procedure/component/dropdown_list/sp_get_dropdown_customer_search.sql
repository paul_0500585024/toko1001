CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_dropdown_customer_search`(
        IN `pKeyword` VARCHAR(100),
        IN `pAgentSeq` VARCHAR(20),
        IN `pCustomerSeq` bigint(20)
    )
BEGIN
if pCustomerSeq=0 then
	Set @sqlCommand = Concat("Select * From m_agent_customer Where identity_no like '%",pKeyword,"%' or pic_name like '%",pKeyword,"%'");
else
	Set @sqlCommand = Concat("Select mac.*, md.name as kecamatan, md.city_seq, mc.name as kota, mc.province_seq From m_agent_customer mac left outer join m_district md on mac.district_seq=md.seq left outer join m_city mc on md.city_seq=mc.seq Where mac.seq = '",pCustomerSeq,"'");
end if;
Prepare sql_stmt FROM @sqlCommand;
Execute sql_stmt;
Deallocate prepare sql_stmt;    

END