CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_dropdown_order_no_by_order_status`(
	pExpeditionSeq TinyInt Unsigned
)
BEGIN

Select distinct
	o.seq as order_seq,
	o.order_no
From
	t_order_merchant om
    Join m_expedition_service es
		On es.seq = om.real_expedition_service_seq
	Join t_order o 
		On om.order_seq = o.seq
Where
	om.order_status = 'D' And
	es.exp_seq = pExpeditionSeq And
	es.active = '1'
Order By 
	o.order_no;
    
END