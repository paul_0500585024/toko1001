CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_dropdown_all_product_category`()
BEGIN

Select
	seq, 
    `name`
From 
	m_product_category 
Where 
    `level` = '1' and 
    active = '1'
Order By
	`name`;

END