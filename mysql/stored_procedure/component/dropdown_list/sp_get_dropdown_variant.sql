CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_dropdown_variant`(
        pCseq TinyInt,
        pTipe char(1)
    )
BEGIN
IF pTipe=0 THEN
  Select seq, display_name From m_variant 
	where active = '1' and category_seq = pCseq
 	order by display_name;
ELSEIF pTipe=1 THEN
  Select seq, display_name From m_variant 
	where active = '1' and category_seq in (select category_seq from m_variant where seq= pCseq)
 	order by display_name;
END If ;
END