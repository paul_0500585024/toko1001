CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_dropdown_city_by_province`(
	pProvinceSeq TinyInt Unsigned
)
BEGIN

Select
	c.seq,
	c.`name`
From
	m_city c Join m_province p
	 On p.seq = c.province_seq
Where
	c.province_seq = pProvinceSeq And 
    c.active = '1'
Order By
	`name`;

END