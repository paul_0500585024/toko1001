CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_dropdown_promo_voucher_node`()
BEGIN

Select
	`code`,
	`name`
From
	s_promo_voucher_node
Order By
	`name`;
    
END