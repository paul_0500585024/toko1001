CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_attribute_list`(
	pSeq MediumInt unsigned,
    pParentSeq MediumInt unsigned
)
BEGIN
	SELECT
		a.seq,
		a.name,
		a.display_name,
		1 as checked
	FROM m_attribute_category c JOIN m_attribute a
		ON c.attribute_seq = a.seq
	WHERE c.category_seq = pSeq
	Union All
	SELECT
		a.seq,
		a.name,
		a.display_name,
		0 as checked
	FROM m_product_category c JOIN m_attribute a
		ON c.seq = a.category_seq
						 Left JOIN m_attribute_category ac
		ON ac.category_seq = pSeq and
		   ac.attribute_seq = a.seq
	WHERE a.category_seq = pParentSeq And ac.category_seq is null
	order by name;
    
END