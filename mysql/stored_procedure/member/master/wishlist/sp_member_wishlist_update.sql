CREATE DEFINER = 'root'@'localhost' PROCEDURE `sp_member_wishlist_update`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pMemberSeq BIGINT unsigned,
        pSeq BIGINT unsigned,
        pStatus char(2)
    )
BEGIN

Declare new_seq int unsigned;	

 If pStatus = 'aw' Then

	Select member_seq Into new_seq From m_member_wishlist where member_seq = pMemberSeq and product_variant_seq=pSeq;

	If new_seq Is Null Then
      INSERT INTO `m_member_wishlist` (`member_seq`, `product_variant_seq`, `created_by`, `created_date`) VALUES (
      pMemberSeq,pSeq,pUserID,now() );
	End If;

 Else

    delete from `m_member_wishlist`	Where member_seq = pMemberSeq and product_variant_seq=pSeq;

 End If;

END;