CREATE DEFINER = 'root'@'localhost' PROCEDURE `sp_member_wishlist_by_seq`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pMemberSeq BIGINT unsigned,
        pSeq BIGINT unsigned
    )
BEGIN

	Select 
		`member_seq`,
		`product_variant_seq`,
		`created_by`,
        `created_date`
	From `m_member_wishlist`
	Where
		member_seq = pMemberSeq and product_variant_seq=pSeq;
END;