CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_member_log_security_add`(
        pUserID VarChar(100),
        pIPAddr VarChar(50),
        pMseq INTEGER(10),
        pType Char(1),
        pCode Char(20),
        pOldPass VarChar(100)
    )
BEGIN
	Declare new_seq int;
	Select 
		Max(seq) + 1 Into new_seq
	From m_member_log_security;
	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
    
	Insert Into `m_member_log_security`(  
		member_seq,
		seq,
		`type`,
        code,
		old_pass,
		ip_addr,
		created_by,
		created_date
	) Values (
		pMseq,
		new_seq,
		pType,
        pCode,
		md5(md5(pOldPass)),
		pIPAddr ,
		pUserID,
		Now()
	);
END