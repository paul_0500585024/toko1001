CREATE DEFINER=`root`@`%` PROCEDURE `sp_member_update_password_by_email`(
	pEmail varchar(100),
    pNewpass text
)
BEGIN
update m_member set new_password = pNewpass where email = pEmail;
END