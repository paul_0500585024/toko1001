CREATE DEFINER=`root`@`%` PROCEDURE `sp_member_add_signup`(
	pUserID VarChar(100),
	pIPAddr VarChar(50),
	pEmail varchar(100),
	pName varchar(100),
	pDate date,
	pGender varchar(1),
	pPhone varchar(20),
	pPassword varchar(100)
)
BEGIN
	Declare new_seq INT Unsigned;

	Select 
        Max(seq) + 1 Into new_seq
    From m_member;

    If new_seq Is Null Then
        Set new_seq = 1;
    End If;
    
	Insert into m_member(
		seq,
		email,
		old_password,
		new_password,
		`name`,
		birthday,
		gender,
		mobile_phone,
		bank_name,
		bank_branch_name,
		bank_acct_no,
		bank_acct_name,
		profile_img,
		deposit_amt,
		`status`,
		created_by,
		created_date,
		modified_by,
		modified_date
	) values (
		new_seq,
		pEmail,
		'',
		pPassword,
		pName,
		pDate,
		pGender,
		pPhone,
		'',
		'',
		'',
		'',
		'',
		0,
		'N',
		'SYSTEM',
		now(),
		'SYSTEM',
		now()
	);
END