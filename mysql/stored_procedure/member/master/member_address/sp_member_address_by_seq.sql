CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_member_address_by_seq`(
	pMemberSeq BigInt Unsigned, 
	pIPAddr VarChar(50),
	pSeq SmallInt Unsigned
)
BEGIN

	Select
		ma.seq as address_seq,
		ma.alias,
        ma.address,
        ma.zip_code,
        ma.pic_name,
        ma.phone_no,
		p.seq as province_seq,
        p.`name` as province_name,
		c.seq as city_seq,
		c.`name` as city_name,
        d.seq as district_seq,
		d.`name` as district_name
	From m_member_address ma 
    Left Join m_district d
		On d.seq= ma.district_seq
	Left Join m_city c
		On c.seq= d.city_seq
    Left Join m_province p
		On p.seq= c.province_seq
	Where
		ma.seq = pSeq And
        ma.member_seq = pMemberSeq;
        
END