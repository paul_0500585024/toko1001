CREATE DEFINER=`root`@`%` PROCEDURE `sp_member_address_delete`(
        pMemberSeq BigInt Unsigned,
        pIPAddr VarChar(50),
        pSeq Tinyint unsigned
    )
BEGIN

Delete From 
	m_member_address 
Where 
	seq = pSeq And
    member_seq = pMemberSeq;


END