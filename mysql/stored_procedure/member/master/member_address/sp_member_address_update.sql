CREATE DEFINER=`root`@`%` PROCEDURE `sp_member_address_update`( 
	pUserID BigInt Unsigned,
    pIPAddr VarChar(50),
	pAddressSeq TinyInt Unsigned,
    pAlias VarChar(50),
    pPicName VarChar(50),
    pAddress VarChar(100),
    pDistrictSeq BigInt Unsigned,
    pZipCode VarChar(15),
    pPhoneNo VarChar(50)
)
BEGIN

	Update 
		m_member_address
    Set
		alias = pAlias,
        address = pAddress,
        district_seq = pDistrictSeq,
        zip_code = pZipCode,
        pic_name = pPicName,
        phone_no = pPhoneNo,
        `default` = '1',
        active = '1',
        created_by = created_by,
        created_date = created_date,
        modified_by = pUserID,
        modified_date = Now()
	Where
		member_seq = pUserID AND seq = pAddressSeq;

END