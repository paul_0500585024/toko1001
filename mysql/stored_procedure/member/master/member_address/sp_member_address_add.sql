CREATE DEFINER=`root`@`%` PROCEDURE `sp_member_address_add`(
    pUserID Varchar(100),
    pIPAddress Varchar(50),
    pMemberSeq Int unsigned,
	pReceiverName VarChar(50),
    pAddress VarChar(500),
    pDistrictSeq smallint unsigned,
    pPhoneNo Varchar(50),
    pPostalCode Varchar(20),
    pAlias Varchar(25)
)
BEGIN

Declare new_seq Int unsigned;

Select 
	Max(seq) + 1 Into new_seq
From m_member_address
where
	member_seq = pMemberSeq;

If new_seq Is Null Then
	Set new_seq = 1;
End If;
   

insert into m_member_address (
	member_seq,
    seq,
    alias,
    address,
    district_seq,
    zip_code,
	pic_name,
    phone_no,
    `default`,
    active,
    created_by,
    created_date,
    modified_by,
    modified_Date
) values (
	pMemberSeq,
    new_seq,
    pAlias,
    pAddress,
    pDistrictSeq,
    pPostalCode,
    pReceiverName,
    pPhoneNo,
    '1',
    '1',
    pUserID,
    now(),
    pUserID,
    now()
);
    
    

END