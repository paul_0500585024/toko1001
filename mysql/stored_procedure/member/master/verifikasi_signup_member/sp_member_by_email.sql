CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_member_by_email`(
        pEmail VARCHAR(100)
    )
BEGIN

SELECT 
		seq,
		email,
		old_password,
		new_password,
		`name`, 
		birthday,
		gender, 
		mobile_phone,
		bank_name,
		bank_branch_name,
		bank_acct_no,
		bank_acct_name,
		profile_img, 
		deposit_amt,
		`status`
  
FROM 
  `m_member` where `email` = pEmail;
        
END