CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_update_member_promo_voucher`(
      pPromoseq bigint(20) unsigned,
      pMemberseq int(10) unsigned,
      pActiveDate date,
      pExpDate date
    )
BEGIN
update m_promo_voucher set
    member_seq = pMemberseq,
    active_date = pActiveDate,
    exp_date = pExpDate,
	modified_by = 'SYSTEM',
    modified_date = now()
where 
	promo_seq = pPromoseq and member_seq IS NULL 
    order by seq asc limit 1;
END