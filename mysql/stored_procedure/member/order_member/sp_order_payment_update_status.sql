DROP PROCEDURE IF EXISTS `sp_order_payment_update_status`;
DELIMITER $$
CREATE PROCEDURE `sp_order_payment_update_status`(
    pUserID VarChar(25),
    pIPAddress VarChar(25),
    pOrderSeq BigInt unsigned,
    pOrderNo Varchar(25),
    pPaymentStatus Char(5),
    pOrderStatus Char(5),
    pPaidDate Datetime
	
)
BEGIN
if exists (select order_no from t_order where order_no = pOrderNo) Then
Begin
update t_order set
	payment_status = pPaymentStatus,
    paid_date = pPaidDate,
    modified_by = pUserID,
    modified_date = now()
where
	seq = pOrderSeq;

update t_order_merchant set
	order_status = pOrderStatus,
    modified_by = pUserID,
    modified_date = now()
where
	order_seq = pOrderSeq;
end;
Else
update t_order_voucher set
	payment_status = pPaymentStatus,
    modified_by = pUserID,
    modified_date = now()
where
	seq = pOrderSeq; 
end if;
END$$
DELIMITER $$