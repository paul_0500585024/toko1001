DROP PROCEDURE IF EXISTS `sp_member_order_list_by_order_no`;
DELIMITER @@
CREATE PROCEDURE sp_member_order_list_by_order_no
(
    pUserID Varchar(100),
    pIPAddress VarChar(25),
    pOrderNo VarChar(20)

)
BEGIN
IF EXISTS (SELECT order_no FROM t_order WHERE order_no = pOrderNo) THEN
BEGIN
Select 
	o.seq,
	o.order_no,
	o.order_date,
	o.member_seq,
	o.signature,
	m.`name` member_name,
	m.email,
	m.mobile_phone,
	c.`name` city_name,
	case when o.payment_retry <> '0'  Then concat(o.order_no, '-', cast(o.payment_retry as char(2))) Else o.order_no End order_trans_no,	
	o.receiver_name,
	o.receiver_address,
	o.receiver_district_seq,
	d.`name` district_name,
	c.`name` city_name,
	p.`name` province_name,
	o.receiver_zip_code,
	o.receiver_phone_no,
	o.payment_status,
	o.pg_method_seq,
	pg.`code` payment_code,
	pg.name payment_name,
	o.paid_date,
	case when pv.nominal is null then mc.nominal else pv.nominal end as nominal,
	pv.code as voucher_code,
	o.payment_retry,
	o.total_order,
	o.voucher_seq,
	o.voucher_refunded,
	o.total_payment,
	o.conf_pay_type,
	o.conf_pay_amt_member,
	o.conf_pay_date,
	o.conf_pay_note_file,
	o.conf_pay_bank_seq,
	o.conf_pay_amt_admin,
        o.apps,
        o.gcm_id,
        o.promo_credit_seq,
        o.created_date
from 
	t_order o join m_district d
		on o.receiver_district_seq = d.seq
			  join m_city c 
		on c.seq = d.city_seq 
			  join m_province p
		on p.seq = c.province_seq
			  join m_payment_gateway_method pg
		on pg.seq = o.pg_method_seq
        	left join m_promo_voucher pv 
		on o.voucher_seq = pv.seq
        	left join m_coupon mc 
		on o.coupon_seq = mc.seq
			  join m_member m
		on m.seq = o.member_seq
where
	o.order_no = pOrderNo;

select
	t.order_seq,
	t.merchant_info_seq,
	t.expedition_service_seq,
	e.`name` expedition_name,
	mm.`name` merchant_name,
	mm.email,
	mm.code as merchant_code,
	mm.seq merchant_seq,
	t.real_expedition_service_seq,    
	t.total_merchant,
	t.total_ins,
	t.total_ship_real,
	t.total_ship_charged,
	t.free_fee_seq,
	t.order_status,
	t.member_notes,
	t.printed,
	t.print_date,
	t.awb_seq,
	t.awb_no,
	t.ref_awb_no,
	t.ship_by,
	t.ship_by_exp_seq,
	t.ship_date,
	t.ship_note_file,
	t.ship_notes,
	t.received_date,
	t.received_by,
	t.redeem_seq,
	t.exp_invoice_seq,
	t.exp_invoice_awb_seq,
        t.modified_date,
        (select count(*) from t_order_product where order_seq = t.order_seq and merchant_info_seq = t.merchant_info_seq) as total_product
        (select sum(total_ship_charged) from t_order_merchant where order_seq = t.order_seq) as total_ship
from 
	t_order_merchant t join m_merchant_info m 
		on m.seq = t.merchant_info_seq 
				join m_district d
		on d.seq = m.pickup_district_seq
				join m_city c
		on c.seq = d.city_seq 
				join m_province p 
		on p.seq = c.province_seq
				join m_merchant mm
		on mm.seq = m.merchant_seq
				join m_expedition_service s
		on s.seq =  t.expedition_service_seq
				join m_expedition e
		on s.exp_seq = e.seq
				join t_order o
		on o.seq = t.order_seq				
where
		o.order_no = pOrderNo;
        
Select 
	t.order_seq,
	mi.merchant_seq,
	t.merchant_info_seq,
	p.`name` as display_name,
	v.seq as product_seq,
	v.pic_1_img as img,
	mv.`value`,
	mv.seq as value_seq,
	mv.`value` as variant_name,
	t.product_status,
	t.qty,
	t.sell_price,
	t.weight_kg,
	t.ship_price_real,
	t.ship_price_charged,
	t.trx_fee_percent,
	t.ins_rate_percent,
	t.product_status,
	t.qty_return,
	t.created_by,
	t.created_date,
	t.modified_by,
	t.modified_date
from
	t_order_product t 
		Join m_product_variant v			
			On v.seq = t.product_variant_seq
		Join m_product p
			On v.product_seq = p.seq
		Join m_variant_value vv
			On vv.seq = t.variant_value_seq
		Join m_variant_value mv 
			On mv.seq = v.variant_value_seq
		Join t_order o
			On o.seq = t.order_seq
		Join m_merchant_info mi
			On mi.seq = t.merchant_info_seq
		join m_merchant mm 
			On mm.seq = mi.merchant_seq
where
	o.order_no = pOrderNo;
END;	
ELSE
BEGIN
SELECT
	o.seq,
	o.order_no,
	o.order_no AS order_trans_no,
	o.member_seq,
	o.order_date,
	o.total_order,
	o.total_payment,
	o.payment_status,
	o.order_status,
	o.signature,
	o.pg_method_seq,
	o.phone_no AS phone_number,
	psn.nominal,
	ps.name AS provider_service_name,
	p.name AS provider_name,
	pgm.name AS pg_method_name,
	pgm.code AS payment_code,
        mapping_code,
        apps, 
        gcm_id
FROM 
	t_order_voucher o JOIN m_provider_service_nominal psn
	ON psn.seq = o.provider_nominal_seq
		JOIN m_provider_service ps
	ON ps.seq = psn.provider_service_seq
		JOIN m_provider p
	ON p.seq = ps.provider_seq
		JOIN m_payment_gateway_method pgm
	ON pgm.seq = o.pg_method_seq
		JOIN m_payment_gateway pg
	ON pg.seq = pgm.pg_seq
WHERE
	order_no = pOrderNo;
END;
END IF;	

END @@ 
DELIMITER ; 
