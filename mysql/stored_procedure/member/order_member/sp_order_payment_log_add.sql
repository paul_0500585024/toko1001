CREATE DEFINER=`root`@`%` PROCEDURE `sp_order_payment_log_add`(
	
    pUserID VarChar(25),
    pIPAddress VarChar(25),
    pOrderSeq BigInt unsigned,
    pOrderNo VarChar(25),
    pPaymentMethodSeq tinyint unsigned,
    pRequest Text,
    pRequestDate Datetime,
    pResponse Text,
    pResponseDate Datetime,
    pSignature VarChar(60)

)
BEGIN

	Declare new_seq int unsigned;

	Select 
		Max(seq) + 1 Into new_seq
	From t_order_payment_log 
    where
		order_seq = pOrderSeq;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
    
    insert into t_order_payment_log (
		order_seq,
		seq,        
        order_no,
        payment_method_seq,        
        request,
        request_date,
        response,        
        response_date,
        created_by,
        created_date,
		signature
	) values (
		pOrderSeq,
		new_seq,
        pOrderNo,
        pPaymentMethodSeq,
        pRequest,        
        pRequestDate,
        pResponse,
        pResponseDate,
        pUserID,
        now(),
        pSignature
	);

END