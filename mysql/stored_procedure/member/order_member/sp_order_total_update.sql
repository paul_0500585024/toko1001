DROP PROCEDURE IF EXISTS `sp_order_total_update`;
DELIMITER $$
CREATE PROCEDURE `sp_order_total_update`(
    pOrderSeq MediumInt
)
BEGIN

Declare pTotalOrder BigInt;

Update t_order_merchant m join (
select 
	order_seq, 
    merchant_info_seq, 
    sum(qty* sell_price) total_merchant,+
    sum(qty * sell_price * ins_rate_percent/100) total_ins, 
    MAX(ship_price_real) * CEIL(sum(qty * weight_kg)) total_ship_real,
    MAX(ship_price_charged) * CEIL(sum(qty * weight_kg)) total_ship_charged
from 
	t_order_product 
Where
	order_seq = pOrderSeq
group by order_seq, merchant_info_seq
) qry on m.order_seq = qry.order_seq and m.merchant_info_seq = qry.merchant_info_seq
set 
	m.total_merchant = qry.total_merchant,
	m.total_ins = qry.total_ins,
    m.total_ship_real = qry.total_ship_real,
    m.total_ship_charged = qry.total_ship_charged
where
	m.order_seq = pOrderSeq;


Select
	qry.total_order - case when v.seq is null then 0 else v.nominal end - case when o.coupon_seq is null then 0 else c.nominal end into pTotalOrder
from 
t_order o join (
select
	order_seq,
    sum(total_merchant + total_ship_charged) total_order
from 
	t_order_merchant
where
	order_seq = pOrderSeq
) qry on qry.order_seq = o.seq
	left join m_promo_voucher v
on v.seq = voucher_seq
        left join m_coupon c
on c.seq = o.coupon_seq;


update t_order o join (
select
	order_seq,
    sum(total_merchant + total_ship_charged) total_order
from 
	t_order_merchant
where
	order_seq = pOrderSeq
) qry on qry.order_seq = o.seq
	left join m_promo_voucher v
on v.seq = voucher_seq
        left join m_coupon c
on c.seq = o.coupon_seq
set
    o.total_order = qry.total_order,
    o.total_payment = case when pTotalOrder < 0 Then 0 Else pTotalOrder End
where
	o.seq = pOrderSeq;

select total_payment from t_order where seq = pOrderSeq;
END$$