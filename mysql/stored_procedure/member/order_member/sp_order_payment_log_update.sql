CREATE DEFINER=`root`@`%` PROCEDURE `sp_order_payment_log_update`(

	pUserID VarChar(25),
    pIPAddress VarChar(25),
	pOrderSeq BigInt unsigned,
    pOrderNo VarChar(25),
    pPaymentMethodSeq tinyint unsigned,
    pResponse Text,
    pResponseDate Datetime,
    pSignature VarChar(60)
	
)
BEGIN

if exists (select signature from t_order_payment_log where signature = pSignature and order_seq = pOrderSeq) then
	update t_order_payment_log set
		response = pResponse,
		response_date = pResponseDate
	Where
		signature = pSignature and 
        order_seq = pOrderSeq;
Else
Begin
	Declare new_seq Int unsigned;

	Select 
		Max(seq) + 1 Into new_seq
	From t_order_payment_log 
    where
		order_seq = pOrderSeq;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
    
    insert into t_order_payment_log (
		order_seq,
		seq,        
        order_no,
        payment_method_seq,        
        request,
        request_date,
        response,        
        response_date,
        created_by,
        created_date,
		signature
	) values (
		pOrderSeq,
		new_seq,
        pOrderNo,
        pPaymentMethodSeq,
        '',        
        '0000-00-00 00:00:00',
        pResponse,
        pResponseDate,
        pUserID,
        now(),
        pSignature
	);
end;
end if;

END