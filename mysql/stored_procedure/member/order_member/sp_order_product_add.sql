DROP PROCEDURE IF EXISTS `sp_order_product_add`;
DELIMITER $$
CREATE PROCEDURE `sp_order_product_add` (
  `pUserID` VARCHAR(25),
  `pIPAddress` VARCHAR(25),
-- for Commission
   pCommissionpartnerPercent DECIMAL(4,2),
   pCommissionPartnerNominal DECIMAL(10,0),
   pCommissionAgentPercent DECIMAL(4,2),
   pCommissionAgentNominal DECIMAL(10,0),
-- End
  `pMerchantSeq` INTEGER UNSIGNED,
  `pOrderSeq` BIGINT(20) UNSIGNED,
  `pProductVariantSeq` MEDIUMINT UNSIGNED,
  `pVariantValueSeq` SMALLINT UNSIGNED,
  `pQty` INTEGER UNSIGNED,
  `pSellPrice` DECIMAL(10,0),
  `pWeightKg` DECIMAL(7,2),
  `pShipPriceReal` DECIMAL(10,0),
  `pShipPriceCharged` DECIMAL(10,0),
  `pProductStatus` CHAR(1),
  `pAgentSeq` BIGINT(20) UNSIGNED)
BEGIN
DECLARE pTrxFeePercent DECIMAL (4,2);
DECLARE pInsRatePercent DECIMAL(4,2);
DECLARE pRateAgent DECIMAL (5,2);
DECLARE pRatePartner DECIMAL(5,2);
DECLARE pMerchantInfoSeq MEDIUMINT UNSIGNED;
DECLARE pTrxCommissionPercent DECIMAL (8,6);
DECLARE pTrxCommissionPartnerPercent DECIMAL (8,6);
SELECT 
  CASE
    WHEN m.trx_fee_percent IS NULL 
    THEN c.trx_fee_percent 
    ELSE m.trx_fee_percent 
  END INTO pTrxFeePercent 
FROM
  m_product p 
  JOIN m_product_variant v 
    ON v.product_seq = p.seq 
  JOIN m_product_category c 
    ON c.seq = p.category_l2_seq 
  LEFT JOIN m_merchant_trx_fee m 
    ON m.category_l2_seq = c.seq 
    AND m.merchant_seq = p.merchant_seq 
WHERE v.seq = pProductVariantSeq ;	
	
SELECT 
  MAX(seq) INTO pMerchantInfoSeq 
FROM
  m_merchant_info 
WHERE merchant_seq = pMerchantSeq ;

SELECT 
  e.ins_rate_percent INTO pInsRatePercent 
FROM
  t_order_merchant m 
  JOIN m_expedition_service s 
    ON s.seq = m.expedition_service_seq 
  JOIN m_expedition e 
    ON e.seq = s.exp_seq 
WHERE m.order_seq = pOrderSeq 
  AND m.merchant_info_seq = pMerchantInfoSeq;

IF pAgentSeq = "" THEN 
  SET pTrxCommissionPercent = 0;
  SET pTrxCommissionPartnerPercent = 0;
ELSE
SELECT 
    a.commission_pro_rate,
    p.commission_pro_rate
INTO
    pRateAgent,
    pRatePartner
FROM 
    m_agent a
JOIN
    m_partner p
ON
    a.partner_seq = p.seq
WHERE 
    a.seq = pAgentSeq;
SELECT
    CASE WHEN mtrx.trx_fee_percent IS NULL THEN (pc.trx_fee_percent/100) * (pRatePartner/100) * (pRateAgent/100) * 100  
        ELSE (mtrx.trx_fee_percent/100) * (pRatePartner/100) * (pRateAgent/100) * 100 
    END as commisionAgent,
    CASE WHEN mtrx.trx_fee_percent IS NULL THEN (pc.trx_fee_percent/100) * (pRatePartner/100) * 100
        ELSE (mtrx.trx_fee_percent/100) * (pRatePartner/100) * 100
    END as commisionPartner
INTO
    pTrxCommissionPercent,
    pTrxCommissionPartnerPercent
FROM 
    m_product_variant pv
JOIN
    m_product p
ON 
    pv.product_seq = p.seq
LEFT JOIN 
    m_merchant_trx_fee mtrx 
ON 
    mtrx.category_l2_seq = p.category_l2_seq 
AND 
    mtrx.merchant_seq = p.merchant_seq
LEFT JOIN 
    m_product_category pc
ON 
    p.category_l2_seq = pc.seq
WHERE 
    pv.seq = pProductVariantSeq;
END IF;
--end create order by

INSERT INTO t_order_product (
  order_seq,
  merchant_info_seq,
  product_variant_seq,
  variant_value_seq,
  qty,
  sell_price,
  weight_kg,
  ship_price_real,
  ship_price_charged,
  trx_fee_percent,
  ins_rate_percent,
  product_status,
  nominal_commission_partner,
  nominal_commision_agent,
  commission_fee_percent,
  commission_fee_partner_percent,
  created_by,
  created_date,
  modified_by,
  modified_date
) 
VALUES
  (
    pOrderSeq,
    pMerchantInfoSeq,
    pProductVariantSeq,
    pVariantValueSeq,
    pQty,
    pSellPrice,
    pWeightKg,
    pShipPriceReal,
    pShipPriceCharged,
    pTrxFeePercent,
    pInsRatePercent,
    pProductStatus,

-- baru tambah
   pCommissionPartnerNominal, 
   pCommissionAgentNominal, 
   pCommissionAgentPercent, 
   pCommissionPartnerPercent,
--end

    pUserID,
    NOW(),
    pUserID,
    NOW()
  ) ;
END$$

