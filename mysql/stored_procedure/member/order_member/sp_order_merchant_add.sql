CREATE DEFINER=`root`@`%` PROCEDURE `sp_order_merchant_add`(
	
    pUserID VarChar(25),
    pIPAddress VarChar(25),
    pOrderSeq BigInt unsigned,
    pMerchantSeq Int unsigned,
    pExpRealServiceSeq tinyint unsigned,
    pExpServiceSeq tinyint unsigned,
    pFreeFeeSeq MediumInt unsigned,
    pOrderStatus char(1),
    pMemberNotes Varchar(100),
    pRefAWBNo varchar(20)    	
)
BEGIN
 
 declare pMerchantInfoSeq MediumInt unsigned;
 declare pRealExpeditionServiceMerchantSeq tinyint unsigned;
 
 select max(seq) into pMerchantInfoSeq
 from 
	m_merchant_info
 where 
	merchant_seq = pMerchantSeq;
 
 select es.seq into pRealExpeditionServiceMerchantSeq
 from
	m_merchant m join m_expedition e
		on e.seq = m.expedition_seq
				 join m_expedition_service es
		on es.exp_seq = e.seq
where
	m.seq = pMerchantSeq limit 1;
 
 if pFreeFeeSeq = 0 then
	set pFreeFeeSeq = null;
 end if;
 
if pRealExpeditionServiceMerchantSeq = 0 Then
	Set  pExpServiceSeq = pRealExpeditionServiceMerchantSeq;
    Set  pExpRealServiceSeq = pRealExpeditionServiceMerchantSeq;
 end if;
 
 insert into t_order_merchant (
	order_seq,
    merchant_info_seq,
    expedition_service_seq,
    real_expedition_service_seq,
    free_fee_seq,
    order_status,
    member_notes,
    awb_no,
    ref_awb_no,
    ship_by,
    ship_by_exp_seq,
    ship_note_file,
    ship_notes,
    received_by,
    created_by,
    created_date,
    modified_by,
    modified_date
) Values (
	pOrderSeq,
    pMerchantInfoSeq,
    pExpServiceSeq,
    pExpRealServiceSeq,
    pFreefeeSeq,
    pOrderStatus,
    pMemberNotes,
    '',
    pRefAWBNo,
    '',
    NULL,
    '',
    '',
    '',
    pUserID,
    now(),
    pUserID,
    now()
);
	


END