CREATE DEFINER=`root`@`%` PROCEDURE `sp_order_update_signature`(
	
    pUserID Varchar(25),
    pIPAddress Varchar(50),
    pOrderSeq Int unsigned,
    pSignature Varchar(100),
    pOrderNo Varchar(25)

)
BEGIN

if exists (select order_no from t_order where order_no = pOrderNo) Then
update t_order set
	signature = pSignature,
    modified_by = pUserID,
    modified_date = now()
where
	seq = pOrderSeq;
else
update t_order_voucher set
	signature = pSignature,
    modified_by = pUserID,
    modified_date = now()
where
	seq = pOrderSeq;
end if;

END