CREATE DEFINER=`root`@`%` PROCEDURE `sp_order_update_payment_code`(
	
    pUserID Varchar(25),
    pIPAddress Varchar(50),
    pOrderNo Varchar(50),
    pPaymentCode Varchar(50)

)
BEGIN

update t_order set
	payment_code = pPaymentCode,
    payment_status = 'T'
Where
	order_no = pOrderNo;


END