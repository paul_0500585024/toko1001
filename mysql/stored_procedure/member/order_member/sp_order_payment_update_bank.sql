CREATE DEFINER=`root`@`%` PROCEDURE `sp_order_payment_update_bank`(
	
    pUserID VarChar(25),
    pIPAddress VarChar(25),
    pOrderNo VarChar(30),
    pPaymentType Char(5),
    pPaymentDate Date,
    pPaymentAmt Decimal(10,0),
    pConfimPayNoteFile Varchar(100),
    pConfirmPayBankSeq Tinyint unsigned,
    pStatus Char(3)
)
BEGIN

update t_order set
	payment_status = pStatus,
	conf_pay_date = pPaymentDate,
    conf_pay_type = pPaymentType,
    conf_pay_amt_member = pPaymentAmt,
	conf_pay_note_file = pConfimPayNoteFile,
    conf_pay_bank_seq = pConfirmPayBankSeq,
    modified_by = pUserID,
    modified_date = now()	
where
	order_no = pOrderNo;

END