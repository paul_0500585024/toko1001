CREATE DEFINER=`root`@`%` PROCEDURE `sp_member_order_product_return_add`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
	pReturnNo VarChar (25),
    pOrderSeq bigint Unsigned,
    pProductVariantSeq int unsigned,
    pVariantValueSeq int unsigned,
    pQty int unsigned,
    pReturnStatus char(1),
    pShipmentStatus char(1),
    pReviewMember text,
    pAwbMemberNo VarChar(50),
    pExpSeqToAdmin int unsigned
)
BEGIN

Declare qty_order Int unsigned;
Declare qty_return Int unsigned;
Declare valid varchar(1);
Declare new_seq bigint unsigned;

	SELECT 
    MAX(seq) + 1
INTO new_seq FROM
    t_order_product_return;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
    
INSERT INTO t_order_product_return(
	seq,
    return_no,
    order_seq, 
    product_variant_seq,
    variant_value_seq,
    qty, 
    return_status,
    shipment_status,
    review_member,
    review_admin,
    review_merchant,
    awb_member_no,
    exp_seq_to_admin,
    admin_received_date,
    ship_to_merchant_date,
    exp_seq_to_merchant,
    awb_admin_no,
    merchant_received_date,
    ship_to_member_date,
    awb_merchant_no,
    exp_seq_to_member,
    member_received_date,
    created_by,
    created_date,
    modified_by, 
    modified_date
)
VALUES(
	new_seq,
    pReturnNo,
    pOrderSeq,
    pProductVariantSeq,
    pVariantValueSeq,
    pQty,
    pReturnStatus,
    pShipmentStatus,
    pReviewMember,
    '',
    '',
    pAwbMemberNo,
    pExpSeqToAdmin,
    '0000-00-00 00:00:00',
    '0000-00-00 00:00:00',
    NULL,
    '',
    '0000-00-00 00:00:00',
    '0000-00-00 00:00:00',
    '',
    NULL,
    '0000-00-00 00:00:00',
    pUserID,
    now(),
    pUserID,
    now()
);

SELECT 
    SUM(qty)
INTO @qty_return FROM
    t_order_product_return
WHERE
    order_seq = pOrderSeq
        AND product_variant_seq = pProductVariantSeq
        AND return_status IN ('I' , 'F');
SELECT 
    qty
INTO @qty_order FROM
    t_order_product
WHERE
    order_seq = pOrderSeq
        AND product_variant_seq = pProductVariantSeq;
        
IF @qty_order >= pQty THEN
		IF @qty_return >= @qty_order Then
			Set valid = '0';
		else
			set valid = '1';
		End If;    
ELSE 
	set valid = '0';	
END IF;

SELECT valid;
END