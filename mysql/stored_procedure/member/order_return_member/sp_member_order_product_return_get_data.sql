CREATE DEFINER=`root`@`%` PROCEDURE `sp_member_order_product_return_get_data`(
    pUserID VarChar(100),
    pIPAddr VarChar(50),
    pReturnNo varchar(25)
)
BEGIN
	SELECT
		shipment_status
	FROM
		t_order_product_return
			WHERE
				return_no = pReturnNo;
END