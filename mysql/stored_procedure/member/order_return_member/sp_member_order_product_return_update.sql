CREATE DEFINER=`root`@`%` PROCEDURE `sp_member_order_product_return_update`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
	pReturnNo VarChar (25),
    pShipmentStatus char(1)
)
BEGIN

	UPDATE t_order_product_return set
		shipment_status = pShipmentStatus,
		member_received_date = now()
	WHERE 
		return_no = pReturnNo;
        
END