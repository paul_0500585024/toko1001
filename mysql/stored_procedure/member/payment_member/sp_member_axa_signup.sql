CREATE DEFINER=`root`@`%` PROCEDURE `sp_member_axa_signup`(
    pUserID VARCHAR(100),
    pIPAddr VARCHAR(50),
    pEmail VARCHAR(100),
    pName VARCHAR(100),
    pDate DATE,
    pGender VARCHAR(1),
    pPhone VARCHAR(20),
    pOrder BIGINT(20)
)
BEGIN
    INSERT INTO t_order_insurance(
        order_seq,
        member_email,
        member_name,
        member_phone,
        member_birthday,
        gender,
        created_date
    ) VALUES (
        pOrder,
        pEmail,	
        pName,
        pPhone,
        pDate,
        pGender,
        NOW()
    );
END