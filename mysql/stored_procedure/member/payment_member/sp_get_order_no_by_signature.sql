CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_order_no_by_signature`(

	pUserID Varchar(25),
    pIPAddress Varchar(50),
    pSignature Varchar(50)

)
BEGIN

Select
	order_no
From
	t_order
where
	signature = pSignature;

END