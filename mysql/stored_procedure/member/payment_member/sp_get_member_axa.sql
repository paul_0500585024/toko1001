CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_member_axa`(
    pUserID VARCHAR(100),
    pIPAddr VARCHAR(50),
    pOrder BIGINT(20)
)
BEGIN
    SELECT order_seq, member_email, member_name, member_phone, member_birthday, gender, created_date 
    from t_order_insurance where order_seq=pOrder;
END