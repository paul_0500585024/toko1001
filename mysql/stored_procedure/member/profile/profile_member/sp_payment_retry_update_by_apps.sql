DROP PROCEDURE IF EXISTS `sp_payment_retry_update_by_apps`;
DELIMITER $$
CREATE PROCEDURE `sp_payment_retry_update_by_apps` (
        pUserID VarChar(100), 
	pIPAddr VarChar(50),
        pOrderNo varchar(50)
)
BEGIN
    UPDATE t_order SET 
        apps = "0"
    WHERE order_no = pOrderNo;
END$$
