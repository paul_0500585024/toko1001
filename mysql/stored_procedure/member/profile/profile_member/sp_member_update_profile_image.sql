CREATE DEFINER=`root`@`%` PROCEDURE `sp_member_update_profile_image`(
        pUserID VarChar(100),
        pIPAddr VarChar(50),
        pMemberSeq BIGINT(20),
        pProfileImg Varchar(50)
    )
BEGIN

Update m_member Set
    profile_img = pProfileImg,
    modified_by = pMemberSeq,
    modified_date = now()
Where
	seq = pMemberSeq;


END