CREATE DEFINER=`root`@`%` PROCEDURE `sp_member_update_settings`(
	pIPAddr VarChar(50),
    pUserID BigInt Unsigned,
    pNewPassword Varchar(1000)
    )
BEGIN
Declare pOldPassword Varchar(1000);

/*Take the old_password*/
Select `new_password` Into pOldPassword 
From m_member 
Where seq = pUserID;


/*Update m_member*/
Update m_member Set
    old_password = pOldPassword,
    new_password = pNewPassword,
    modified_by = pUserID,
    modified_date = now()
Where
	seq = pUserID;


END