CREATE DEFINER = 'root'@'localhost' PROCEDURE `sp_review_product_add`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pPvseq BIGINT UNSIGNED,
        pOrderSeq BIGINT UNSIGNED,
        pRate SMALLINT UNSIGNED,
        pReview TEXT
    )
BEGIN
	Insert Into `m_product_review`(     
	  `product_variant_seq`,
	  `order_seq`,
	  `rate`,
	  `review`,
	  `review_admin`,
	  `status`,
	  `created_by`,
	  `created_date`,
	  `modified_by`,
	  `modified_date`
	) Values (
	pPvseq,
	pOrderSeq,
	pRate,
	pReview,
	'',
	'N',
	pUserID,
	Now(),
	pUserID,
	Now()
	);

END;