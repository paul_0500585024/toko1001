CREATE DEFINER=`root`@`%` PROCEDURE `sp_member_account_list`(
    pIPAddr VarChar(50),    
    pCurrPage Int unsigned,
    pRecPerPage Int unsigned,
    pDirSort VarChar(4),
    pColumnSort Varchar(50),
    pMemberSeq BigInt(20)
)
BEGIN
    -- Begin variables for Paging
    Declare startRec Int unsigned;
    Declare totalPage Int unsigned;
    Declare totalRec Int unsigned;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "
	Select 
		Count(t.member_seq) Into @totalRec 
	From t_member_account t
	Join(
		Select @running_total:=0
	) r  ";

    Set @sqlCommand = Concat(@sqlCommand, " Where t.member_seq ='" ,pMemberSeq, "'");
	
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
	Select
		t.member_seq,
		t.seq,
		t.mutation_type,
		t.pg_method_seq,
		t.trx_type,
		t.trx_no,
		t.trx_date,
		t.deposit_trx_amt,
		t.non_deposit_trx_amt,
		t.bank_name,
		t.bank_branch_name,
		t.bank_acct_no,
		t.bank_acct_name,
		t.refund_date,
		t.`status`,
		Case
			When trx_type In ('WDW','ORD') Then deposit_trx_amt * 1
			Else 0
		End As deposit_Keluar,
		Case
			when trx_type IN ('CNL') then deposit_trx_amt * 1
            Else 0
		End As deposit_Masuk,
		Sum(
			Case
				When mutation_type = 'C' and status = 'A' Then @running_total:=@running_total + t.deposit_trx_amt
                When mutation_type = 'N' and status = 'A' Then @running_total:=@running_total + t.deposit_trx_amt
				When mutation_type = 'D' and status = 'A' Then @running_total:=@running_total - t.deposit_trx_amt
			End
		) As saldo,
		Case 
			When (t.deposit_trx_amt = 0 And t.non_deposit_trx_amt >0 ) Then concat('CC', (t.non_deposit_trx_amt)) 
			Else concat('Deposit ', (t.deposit_trx_amt)) 
		End As nilai_transaksi
	From t_member_account t
	Join(
		Select @running_total:=0
	) r";
    Set @sqlCommand = Concat(@sqlCommand, " Where t.member_seq ='" ,pMemberSeq, "'");
	Set @sqlCommand = Concat(@sqlCommand, " GROUP BY t.member_seq,
											t.seq,
											t.mutation_type,
											t.pg_method_seq,
											t.trx_type,
											t.trx_no,
											t.trx_date,
											t.deposit_trx_amt,
											t.non_deposit_trx_amt,
											t.bank_name,
											t.bank_branch_name,
											t.bank_acct_no,
											t.bank_acct_name,
											t.refund_date,
											t.`status`
											");
    Set @sqlCommand = Concat(@sqlCommand, " order by ", 't.trx_date'" ,"'t.seq', " ", pDirSort);
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

SELECT pCurrPage, totalPage, totalRec AS total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END