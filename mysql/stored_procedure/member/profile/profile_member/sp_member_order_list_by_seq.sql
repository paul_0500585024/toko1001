CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_member_order_list_by_seq`(
        pUserID VarChar(100),
        pIPAddr VarChar(50),
        pHSeq VarChar(50)
    )
BEGIN
	Select
		o.order_no,
		o.order_date,
        o.payment_status,
        o.receiver_name,
        o.receiver_address,
        o.total_order,
        o.total_payment,
        o.pg_method_seq,
        o.voucher_seq,
        pv.trx_use_amt,
        pg.`name` as pg_name
	From  t_order o
	Join t_order_product op 
		On o.seq = op.order_seq
	Left outer join m_promo_voucher pv 
		on o.voucher_seq = pv.seq
	Left Join m_payment_gateway_method pg 
		on o.pg_method_seq = pg.seq
	Where o.seq = pHSeq;
            
END