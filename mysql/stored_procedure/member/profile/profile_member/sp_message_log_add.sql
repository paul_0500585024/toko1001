CREATE DEFINER = 'root'@'%' PROCEDURE `sp_message_log_add`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pContent TEXT,
        pPrevSeq BIGINT(20) unsigned,
        pMemberSeq BIGINT(20) unsigned,
        pMerchantSeq BIGINT(20) unsigned,
        pAdminSeq VARCHAR(25)
    )
BEGIN
	Declare new_seq BIGINT unsigned;
	Select 
		Max(seq) + 1 Into new_seq
	From `t_message_log`;
	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
	If pMemberSeq =0 Then
		Set pMemberSeq = NULL;
	End If;
    If pMerchantSeq =0 Then
		Set pMerchantSeq = NULL;
	End If;
    If pAdminSeq =0 Then
		Set pAdminSeq = NULL;
	End If;
    If pPrevSeq =0 Then
		Set pPrevSeq = NULL;
	End If;


	Insert Into `t_message_log`(
		seq,
        content,
		prev_message_seq,
        member_seq,
		merchant_seq,
        admin_id,
        `status`,
		created_by,
		created_date
	) Values (
		new_seq,
		pContent,
		pPrevSeq,
        pMemberSeq,
		pMerchantSeq,
        pAdminSeq,
        'U',
		pUserID,
		Now()
	);

END;