CREATE DEFINER = 'root'@'%' PROCEDURE `sp_message_log_list`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pCurrPage Int,
        pRecPerPage Int,
        pDirSort VarChar(20),
        pColumnSort Varchar(50),
        pTipe VarChar(20),
        pUserSeq BIGINT(20)
    )
BEGIN
    -- Begin variables for Paging
    Declare startRec Int;
    Declare totalPage Int;
    Declare totalRec Int;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    
	Set sqlWhere = "";
    If pTipe = "member" Then
        Set sqlWhere = Concat(sqlWhere, " And a.member_seq = '" , pUserSeq, "'", " or (a.prev_message_seq in (select seq from t_message_log where member_seq = '" , pUserSeq, "') and a.prev_message_seq > 0)");
    End If;
    If pTipe = "merchant" Then
        Set sqlWhere = Concat(sqlWhere, " And a.merchant_seq = '" , pUserSeq, "'", " or (a.prev_message_seq in (select seq from t_message_log where merchant_seq = '" , pUserSeq, "') and a.prev_message_seq > 0)");
    End If;
    -- End SQL Where

    -- Begin Paging Info
    Set @sqlCommand = "Select Count(seq) Into @totalRec From t_message_log a ";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
        Select
		a.`seq`,
        a.`content`,
		a.`prev_message_seq`,
		a.`member_seq`,
		a.`merchant_seq`,
		a.`admin_id`,
		a.`status`,
		a.created_date,
		a.created_date,
        mb.`name` as member_name,
        mb.`profile_img`,
        mu.`user_name` as admin_name,
        mm.`name` as merchant_name,
        mm.`logo_img`

        FROM t_message_log a left outer join m_member mb on a.`member_seq`=mb.`seq`
        left outer join `m_user` mu on a.`admin_id`=mu.`user_id`
        left outer join `m_merchant` mm on a.`merchant_seq`=mm.`seq`
    ";

    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;

    Set @sqlCommand = Concat(@sqlCommand, " order by IFNULL(a.prev_message_seq , a.seq) desc, a.seq  ");

    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END;