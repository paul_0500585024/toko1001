CREATE DEFINER=`root`@`%` PROCEDURE `sp_member_payment_retry_update`(
	pUserID VarChar(100), 
	pIPAddr VarChar(50),
	pMemberSeq bigint Unsigned,
	pOrderNo varchar(50),
    pPaymentStatus char(1),
    pPgMethodSeq bigint unsigned
    )
BEGIN
update t_order set
	payment_status= pPaymentStatus,
    pg_method_seq = pPgMethodSeq,
	payment_retry = (payment_retry + 1)
Where
	member_seq = pMemberSeq AND order_no = pOrderNo;
END