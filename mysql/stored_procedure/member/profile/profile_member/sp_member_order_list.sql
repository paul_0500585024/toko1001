DELIMITER $$
DROP PROCEDURE sp_member_order_list $$
CREATE PROCEDURE sp_member_order_list
(
    pUserID VarChar(100),
    pIPAddr VarChar(50),
    pMemberSeq BigInt(10),
    pCurrPage Int unsigned,
    pRecPerPage Int unsigned,
    pDirSort VarChar(4),
    pColumnSort Varchar(50),
    pOrderNo VarChar(50),
    pDateSearch Char(1),
    pOrderDateFrom Datetime,
    pOrderDateTo Datetime,
    pPaymentStatus Char(1)

)
BEGIN

    Declare startRec Int unsigned;
    Declare totalPage Int unsigned;
    Declare totalRec Int unsigned;


    Declare sqlWhere VarChar(500);
	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";

    If pOrderNo <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And o.order_no like '%" , escape_string(pOrderNo), "%'");
    End If;
    If (pOrderDateFrom <> "") Then
        Set sqlWhere = Concat(sqlWhere, " And o.order_date >= '" , pOrderDateFrom,"'");
    End If;

    If (pOrderDateTo <> "") Then
        Set sqlWhere = Concat(sqlWhere, " And o.order_date <= '" , pOrderDateTo,"'");
    End If;
    If pPaymentStatus <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And o.payment_status ='" , pPaymentStatus, "'");
    End If;



    Set @sqlCommand = "Select Count(o.order_no) Into @totalRec
            From
            (select order_no, order_date, payment_status, member_seq from m_member m Join t_order o On m.seq = o.member_seq
             union all
             select order_no, order_date, payment_status, member_seq from m_member m Join t_order_voucher o On m.seq = o.member_seq) as o "
        ;
    Set @sqlCommand = Concat(@sqlCommand, " Where o.member_seq ='" ,pMemberSeq, "'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);
        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;
        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;

    Set @sqlCommand = "
        Select
            o.seq,
            o.member_name,
            o.order_no,
            o.order_date,
            o.total_order,
            o.payment_status,
            o.paid_date,
            o.payment_method,
            '' as detail,
            '' as payment
            From
        (
        select
            o.seq,
            o.member_seq,
            m.name as member_name,
            o.order_no,
            o.order_date,
            o.total_order,
            o.payment_status,
            o.paid_date,
            pgm.name as payment_method,
            '' as detail,
            '' as payment
		from
                    m_member m
		Join
                    t_order o On m.seq = o.member_seq
		Join
        	    m_payment_gateway_method pgm On o.pg_method_seq = pgm.seq
		union all
		select
                    o.seq,
                    o.member_seq,
                    m.name as member_name,
                    o.order_no,
                    o.order_date,
                    o.total_order,
                    o.payment_status,
                    o.created_date,
                    pgm.name as payment_method,
                    '' as detail,
                    '' as payment
		from
			m_member m
		Join
			t_order_voucher o On m.seq = o.member_seq
		Join
			m_payment_gateway_method pgm On o.pg_method_seq = pgm.seq
		) as o
		";
    Set @sqlCommand = Concat(@sqlCommand, " Where o.member_seq ='" ,pMemberSeq, "'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;

    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);

    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    Select pCurrPage, totalPage, totalRec as total_rec;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set @totalRec = Null;
    Set @sqlCommand = Null;
END $$
DELIMITER ;
