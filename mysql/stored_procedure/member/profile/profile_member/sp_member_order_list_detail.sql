CREATE DEFINER=`root`@`%` PROCEDURE `sp_member_order_list_detail`(
    pUserID VarChar(100),
    pIPAddr VarChar(50),
    pMemberSeq BigInt(10),
    pCurrPage Int unsigned,
    pRecPerPage Int unsigned,
    pDirSort VarChar(4),
    pColumnSort Varchar(50),
    pHSeq VarChar(50)
    
)
BEGIN
    -- Begin variables for Paging
    Declare startRec Int unsigned;
    Declare totalPage Int unsigned;
    Declare totalRec Int unsigned;
    -- End variables for Paging

    -- Begin SQL Where
    Declare sqlWhere VarChar(500);

	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";

    -- Begin Paging Info
    Set @sqlCommand = "Select Count(op.product_variant_seq) Into @totalRec From  t_order o
		Join t_order_product op 
			On o.seq = op.order_seq
		Join m_product_variant pv
			On op.product_variant_seq = pv.seq
		Join m_product p
			On pv.product_seq = p.seq
		Join m_variant_value vv
			On op.variant_value_seq = vv.seq 
		Join m_variant v
			On vv.variant_seq = v.seq";
    Set @sqlCommand = Concat(@sqlCommand, " Where o.order_no ='" ,pHSeq, "'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);

        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;

        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info

    Set @sqlCommand = "
        Select
			op.order_seq,
            op.product_variant_seq,
            op.qty,
            op.sell_price,
            op.ship_price_charged,
			o.order_no,
			o.order_date,
            om.order_status,
            om.awb_no,
            om.ship_date,
            m.`name` as merchant_name,
            m.`code`,
            p.merchant_seq,
			p.name as product_name,
            p.p_weight_kg,
            v.display_name,
            vv.value,
            pv.pic_1_img as img_src
		From  t_order o
        Join t_order_merchant om
			On o.seq = om.order_seq
		Join t_order_product op 
			On op.order_seq = om.order_seq And op.merchant_info_seq = om.merchant_info_seq
		Join m_product_variant pv
			On op.product_variant_seq = pv.seq
		Join m_product p
			On pv.product_seq = p.seq
		Join m_merchant m
			On p.merchant_seq = m.seq
		Join m_variant_value vv
			On op.variant_value_seq = vv.seq 
		Join m_variant v
			On vv.variant_seq = v.seq
		";

    Set @sqlCommand = Concat(@sqlCommand, " Where o.order_no ='" ,pHSeq, "'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    Select pCurrPage, totalPage, totalRec as total_rec;

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END