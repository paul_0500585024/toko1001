CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_data_member`(
	pIPAddr VarChar(50),
    pMemberSeq BigInt(10)
)
BEGIN

Select
	email,
	new_password as old_password,
	`name` as member_name,
    CAST(Date_format(birthday,'%d-%b-%Y')as Char(25)) as birthday,
	gender,
	mobile_phone,
    profile_img
From 
	m_member
Where 
	seq = pMemberSeq;


Select
	ma.seq as address_seq,
    ma.alias,
	ma.address,
	ma.phone_no,
	d.seq as district_seq,
	d.name as district_name,
	c.seq as city_seq,
	c.name as city_name,
	p.seq as province_seq,
	p.name as province_name,
	ma.zip_code,
	ma.pic_name
From 
	m_member m
Left Join 
	m_member_address ma On m.seq = ma.member_seq
Join 
	m_district d On ma.district_seq = d.seq
Left Join 
	m_city c On d.city_seq = c.seq
Left Join
	m_province p On c.province_seq = p.seq
Where
	m.seq = pMemberSeq;


Select    
	bank_name,
	bank_branch_name,
	bank_acct_no,
	bank_acct_name,
	deposit_amt
From 
	m_member
Where
	seq = pMemberSeq;


Select
	email,
	new_password as old_password
From 
	m_member
Where 
	seq = pMemberSeq;


select
	member_seq,
	trx_type, 
	trx_no, 
	trx_date,
    deposit_trx_amt, 
	non_deposit_trx_amt
From 
	t_member_account
Where 
    member_seq = pMemberSeq;
    
    
Select
	pv.`code`,
    mpv.`code` as code_refund,
    pv.nominal,
    pv.active_date,
    pv.exp_date,
    pv.trx_use_amt,
    pv.trx_no,
    o.payment_status
From 
	m_member m
Join
	m_promo_voucher pv On m.seq = pv.member_seq
left join t_order o on
	m.seq = o.member_seq AND pv.trx_no = o.order_no
left join m_promo_voucher mpv on o.voucher_refund_seq = mpv.seq

Where 
	m.seq = pMemberSeq;
	
    
    
/*Select 
	Count(member_seq) as qty_order_p
From 
	t_order
Where 
	member_seq = pMemberSeq And
    payment_status = "P";
    
    
Select 
	Count(member_seq) as qty_order_u
From 
	t_order
Where 
	member_seq = pMemberSeq And
    payment_status = "U";*/
    
    
END