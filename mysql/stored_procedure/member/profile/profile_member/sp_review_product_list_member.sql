CREATE DEFINER = 'root'@'%' PROCEDURE `sp_review_product_list_member`(
        IN `pUserID` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pSeq` BIGINT UNSIGNED
    )
BEGIN
	Select
		count(top.`product_variant_seq`) as count_msg
		From `t_order_product` top JOIN
		`t_order_merchant` tom on  top.`order_seq`=tom.`order_seq` and top.`merchant_info_seq`=tom.`merchant_info_seq`
		join t_order tor on tom.`order_seq`=tor.`seq`
	Where tor.`payment_status`='P' and tom.`order_status`='D' AND
	tor.`member_seq` = pSeq and (tor.`seq`,top.`product_variant_seq`) not in
	(select order_seq,product_variant_seq from `m_product_review`);
	Select
		t_op.order_seq,
		t_op.merchant_info_seq,
		t_op.product_variant_seq,
		t_op.variant_value_seq,
		t_op.product_status,
		t_op.qty,
		t_op.sell_price,
		t_op.`product_status`,
		t_o.order_no,
		t_o.order_date,
		p.`name` as product_name,
		pv.`pic_1_img`,
		pv.`seq`,
		vv.`value` as variant_value,
		mmi.`merchant_seq`
		From `t_order_product` t_op
		Join `t_order_merchant` t_om
		On t_op.order_seq = t_om.order_seq and t_op.`merchant_info_seq` = t_om.`merchant_info_seq`
		Join m_merchant_info mmi
		On t_op.`merchant_info_seq`=mmi.seq
		Join t_order t_o
		On t_op.order_seq = t_o.seq
		Join m_product_variant pv
		On t_op.product_variant_seq = pv.seq
		Join m_product p
		On pv.product_seq = p.seq
		Join m_variant_value vv
		On pv.variant_value_seq = vv.seq
	Where t_o.`payment_status`='P' and t_om.`order_status`='D' AND
	t_o.`member_seq` = pSeq and (t_o.`seq`,t_op.`product_variant_seq`) not in
	(select order_seq,product_variant_seq from `m_product_review`)
	order by t_o.order_date desc,t_op.order_seq;
END;