CREATE DEFINER=`root`@`%` PROCEDURE `sp_member_update_contact`(
	pUserID BigInt Unsigned, 
	pIPAddr VarChar(50),
    pMemberSeq BigInt Unsigned,
    pMemberName VarChar(50),
    pGender char(1),
    pBirthday Datetime,
    pMobilePhone VarChar(25)
    )
BEGIN

/*Update m_member*/
Update m_member Set
    name = pMemberName,
    birthday = pBirthday,
    gender = pGender,
    mobile_phone = pMobilePhone,
    modified_by = pMemberSeq,
    modified_date = now()
Where
	seq = pMemberSeq;


END