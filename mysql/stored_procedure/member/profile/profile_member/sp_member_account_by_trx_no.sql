CREATE DEFINER=`root`@`%` PROCEDURE `sp_member_account_by_trx_no`(
	pTrxNo varchar(20)
)
BEGIN
select 
	trx_no,
    `status`
from t_member_account
where trx_no = pTrxNo;
END