DROP PROCEDURE IF EXISTS `sp_change_password_member`;
DELIMITER $$
CREATE PROCEDURE `sp_change_password_member` (
    pUserID VarChar(200), 
    pIPAddr VarChar(50),
    pNewPassword VarChar(1000)
)
BEGIN

Update m_member Set 
    `new_password` = pNewPassword 
Where email = pUserID;

END$$

