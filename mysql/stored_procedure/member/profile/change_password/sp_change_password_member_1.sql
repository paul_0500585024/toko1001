CREATE DEFINER=`root`@`%` PROCEDURE `sp_change_password_member`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pNewPassword VarChar(1000)
)
BEGIN  
	Update m_member 
    Set `new_password` = pNewPassword 
    Where email = pUserID;
    
End