CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_member_change_password_log`(
        pUserID VarChar(100),
        pIPAddr VarChar(50),
        pMemberSeq BigInt Unsigned,
        pOldPassword VarChar(1000)
    )
BEGIN
	Declare new_seq Int;

	Select 
		Max(seq) + 1 Into new_seq
	From m_member_log_security;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
    
	Insert Into `m_member_log_security`(  
		member_seq,
		seq,
		`type`,
        code,
		old_pass,
		ip_addr,
		created_by,
		created_date
	) Values (
		pMemberSeq,
		new_seq,
		'1',
        '',
		pOldPassword,
		pIPAddr ,
		pMemberSeq,
		Now()
	);

END