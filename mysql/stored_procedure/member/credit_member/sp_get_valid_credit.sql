CREATE DEFINER=`root`@`%` PROCEDURE `sp_deposit_member_add`(
	
    pUserID VARCHAR(100),
    pIPAddress VARCHAR(25),
    pOrderNo VARCHAR(20)   
	
)
BEGIN

	select pc.seq from m_promo_credit pc inner join 
	m_promo_credit_product pcp on pc.seq=pcp.promo_credit_seq inner join
    `m_promo_credit_bank` pcb on pc.`seq`=pcb.`promo_seq`
    where pc.status='A' and pc.promo_credit_period_from<=CURDATE() and pc.promo_credit_period_to>=CURDATE() 
	and pcp.product_variant_seq=pProductVariantSeq and pcb.seq=pCreditSeq;

    
END