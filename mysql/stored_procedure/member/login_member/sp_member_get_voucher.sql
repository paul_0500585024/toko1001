CREATE DEFINER=`root`@`%` PROCEDURE `sp_member_get_voucher`(
    pMemberSeq BigInt Unsigned,
    pVoucherPromoSeq BigInt Unsigned
)
BEGIN

	 Select
		p.promo_seq,
		p.seq,
		p.`code`,
		p.member_seq,
		p.nominal,
		p.active_date,
		p.exp_date,
		p.trx_use_amt,
		p.trx_no,
		m.name as member_name 
	From m_promo_voucher p 
	Left join m_member m On p.member_seq = m.seq
    Where p.member_seq = pMemberSeq And p.promo_seq = pVoucherPromoSeq;
   
END