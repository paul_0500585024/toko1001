CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_password_member`(
	pUserID Varchar(50)
)
BEGIN
SELECT
	seq,
    email,
    old_password,
    new_password,
    `name`,
    `status`
FROM 
	m_member
WHERE 
	email = pUserID And
    `status` = 'A';

END