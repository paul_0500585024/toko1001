CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_facebook_login_save_add`(
	pUserID VarChar(50),
    pName VarChar(50),
    pBirthday VarChar(50),
    pGender Char(1),
    pPassword VarChar(1000),
    pIPAddress Varchar(50)
)
BEGIN
Declare new_seq BigInt;
Declare member_seq BigInt;

	Select 
		Max(seq) + 1 Into new_seq
	From m_member;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
    
    If Not Exists (Select email From m_member Where email = pUserID) Then 
    Insert Into m_member (
        seq,
        email,
        old_password,
        new_password,
        `name`,
        birthday,
        gender,
        mobile_phone,
        bank_name,
        bank_branch_name,
        bank_acct_no,
        bank_acct_name,
        profile_img,
        deposit_amt,
        `status`,
        last_login,
        ip_address,
        created_by,
        created_date,
        modified_by,
        modified_date
		) Values (
        new_seq,
        pUserID,
        '',
        pPassword,
        pName,
        pBirthday,
        pGender,
        '',
        '',
        '',
        '',
        '',
        '',
        0,
        'A',
        now(),
        pIPAddress,
        'SYSTEM',
		now(),
		'SYSTEM',
		now()
        );
	Else
		Begin
			update m_member set
				last_login = now(),
                ip_address = pIPAddress
			where
				email = pUserID;
			
            select seq into member_seq from m_member where email = pUserID;
            
            insert into m_member_login_log(
				seq,
                member_seq,
                ip_address,
                status,
                created_date
			) values (
				new_seq,
                member_seq,
                pIPAddress,
                'S',
                now()
			);
		end;		
	End If;    
    
    Select * From m_member Where email = pUserID;
           
END