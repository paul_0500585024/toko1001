CREATE DEFINER=`root`@`%` PROCEDURE `sp_login_member`(
    pUserID VarChar(100),
    pPassword Varchar(1000),
    pIPAddress Varchar(50)
)
BEGIN
	Declare new_seq Int;
	Declare member_seq Int;

	Select 
		Max(seq) + 1 Into new_seq
	From m_member_login_log;    
    
    If new_seq Is Null Then
		Set new_seq = 1;
	End If;
    
    Select 
		seq,
		email,
		`name` as user_name,
		old_password,
		new_password,
        profile_img
	From
		m_member
	Where 
		email = pUserID And
        `status` = 'A';
        
if exists (select seq from m_member where email = pUserID and new_password = pPassword And `status`='A') then
    Begin
		update m_member set
			last_login = now(),
			ip_address = pIPAddress
		where
			email = pUserID;
			
		select 
			seq into member_seq
		from 
			m_member
		where
			email = pUserID;
		
        insert into m_member_login_log (
			seq,
            member_seq,
            ip_address,
            status,
            created_date
		) Values (
			new_seq,
            member_seq,
            pIPAddress,
            'S',
            now()
		);
    End;
Else
	if exists (select seq from m_member where email = pUserID) then
		Begin
			select 
				seq into member_seq
			from 
				m_member
			where
				email = pUserID;
			
			insert into m_member_login_log (
				seq,
				member_seq,
				ip_address,
				status,
				created_date
			) Values (
				new_seq,
				member_seq,
				pIPAddress,
				'F',
				now()
			);
            
		End;
	End if;
End if;
        
END