CREATE DEFINER=`root`@`%` PROCEDURE `sp_deposit_member_substract`(
	
	pUserID VarChar(25),    
    pIPAddress VarChar(25),
    pMemberSeq Int unsigned,
    pDepositAmt Decimal(10,0)

)
BEGIN

update m_member set 
	deposit_amt = deposit_amt - pDepositAmt,
    modified_by = pUserID,
    modified_date = now()
where
	seq = pMemberSeq;

END