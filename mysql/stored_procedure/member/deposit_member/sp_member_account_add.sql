CREATE DEFINER=`root`@`%` PROCEDURE `sp_member_account_add`(
	pUserID VarChar(100), 
	pIPAddr VarChar(50),
    pMemberSeq BigInt Unsigned,
    pMutationType VarChar(5),
    pTrxType VarChar(5),
    pPaymentMethodSeq Tinyint unsigned,    
    pTrxNo varchar(25),
    pDepositTrxAmt DECIMAL(10,0),
    pNonDepositTrxAmt DECIMAL(10,0),
    pBankName varchar(50),
    pBankBranchName varchar(50),
    pBankAcctNo varchar(50),
    pBankAcctName varchar(50),
    pStatus VarChar(5)
)
BEGIN
Declare new_seq int unsigned;
	
	Select 
		Max(seq) + 1 Into new_seq
	From t_member_account 
    where
		member_seq = pMemberSeq;

	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
    
	Insert Into t_member_account (
	  member_seq,
	  seq, 
	  mutation_type,
	  pg_method_seq, 
	  trx_type, 
	  trx_no, 
	  trx_date, 
	  deposit_trx_amt, 
	  non_deposit_trx_amt,
	  bank_name,
	  bank_branch_name,
	  bank_acct_no,
	  bank_acct_name, 
	  refund_date,
	  `status`, 
	  created_by, 
	  created_date,
	  modified_by,
	  modified_date
  )
	Value (
		pMemberSeq,
        new_seq,
        pMutationType,
        pPaymentMethodSeq,
        pTrxType,
        pTrxNo,
        CURRENT_DATE(),
        pDepositTrxAmt,
        pNonDepositTrxAmt,
		pBankName,
        pBankBranchName,
        pBankAcctNo,
        pBankAcctName,
        '0000-00-00 00:00:00',
        pStatus,
        pMemberSeq,
        now(),
        pMemberSeq,
        now()
);


END