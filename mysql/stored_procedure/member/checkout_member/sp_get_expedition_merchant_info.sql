CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_expedition_merchant_info`(
	pUserId VarChar(25),
    pIPAddress VarChar(25),
    pMerchantSeq Int unsigned,
    pDistrictSeq SmallInt unsigned,
    pExpSeq TinyInt unsigned
)
BEGIN

declare pOrgirinDistrictSeq SmallInt unsigned;
declare pOriginExpCode varchar(10);
declare pDestinationExpCode varchar(10);

Select  (case when pickup_addr_eq = '1' Then district_seq else pickup_district_seq end)  into pOrgirinDistrictSeq
from 
	m_merchant
where
	seq = pMerchantSeq;

Select  exp_city_code into pOriginExpCode
from 
	m_expedition_district
where
	exp_seq = pExpSeq and
    district_seq = pOrgirinDistrictSeq and
    active = '1';

Select  exp_district_code into pDestinationExpCode
from 
	m_expedition_district
where
	exp_seq = pExpSeq and
    district_seq = pDistrictSeq and
    active = '1';
    
select 
	pOriginExpCode as from_district_code, 
	pDestinationExpCode as to_district_code,
    e.seq as exp_seq,
    (select p.seq from m_province p join m_city c on c.province_seq = p.seq join m_district d on d.city_seq = c.seq Where d.seq = pDistrictSeq) as province_seq,
	pMerchantSeq as merchant_seq,
    e.`code` as exp_code,
    s.seq as exp_service_seq,
    s.`code` as service_code
from 
	m_expedition e join m_expedition_service s
		on s.exp_seq = e.seq
Where
	e.seq = pExpSeq and 
    s.`default` = '1';


END