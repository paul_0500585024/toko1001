CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_code_payment_gateway`(
	
    pUserID VarChar(25),
    pIPAddress VarChar(25),
    pPaymentGatewaySeq Tinyint unsigned
)
BEGIN

select
	`code`,
    name
from
	m_payment_gateway_method 
where
	seq = pPaymentGatewaySeq;


END