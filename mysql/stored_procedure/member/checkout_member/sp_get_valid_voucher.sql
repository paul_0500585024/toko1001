DELIMITER $$
CREATE PROCEDURE sp_get_valid_voucher
(
	pMemberSeq Int unsigned,
	pVoucherSeq BigInt unsigned
)
BEGIN

Select
	seq
from
	m_promo_voucher
where
	exp_date > now() and
	member_seq = pMemberSeq and
	seq = pVoucherSeq and 
	trx_amt = '';
END $$ 
DELIMITER ; 
