CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_valid_coupon`(
	pUserID Varchar(25),
        pIPAddress Varchar(50),        
        pCouponSeq INT unsigned,
        pOrderSeq BIGINT unsigned
)
BEGIN

declare pCountCoupon Int unsigned;

select Exists(
    select 
        c.seq 
    from 
        m_coupon c join m_coupon_product cp
            on cp.coupon_seq = c.seq
                   join t_order t
            on t.coupon_seq = c.seq
                   join t_order_product tp  
            on t.seq = tp.order_seq
                   join m_product_variant pv
            on pv.seq = tp.product_variant_seq and
               cp.product_seq = pv.product_seq
    where
        c.seq = pCouponSeq and
        t.seq = pOrderSeq and 
        CURDATE() between c.coupon_period_from and c.coupon_period_to) as valid;


select 
    coupon_limit into pCountCoupon
from
    m_coupon 
where
    seq = pCouponSeq;

Select Not Exists(
    Select
        count(seq) 
    from
        t_order
    where
        coupon_seq = pCouponSeq
    Having count(seq) > pCountCoupon
) as coupon_count;
        
    
    
END