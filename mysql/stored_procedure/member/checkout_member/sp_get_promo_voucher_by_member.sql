CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_promo_voucher_by_member`(
	
    pUserID VarChar(25),
    pIPAddress Varchar(25),
    pMemberSeq MediumInt unsigned,
    pTotalOrder Decimal(10,0)
)
BEGIN

Select 
	seq,
    `code`,
    nominal
from m_promo_voucher
where
	member_seq = pMemberSeq and
    exp_date >= now() and
    trx_use_amt <= pTotalOrder and
    trx_no = '';
    
END