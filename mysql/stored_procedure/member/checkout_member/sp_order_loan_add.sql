CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_order_loan_add`(IN `pIPAddress` VARCHAR(25), IN `pUserID` VARCHAR(50), IN `pOrderSeq` BIGINT, IN `pCustomerSeq` INT, IN `pDp` DECIMAL(10,0), IN `pAdminFee` DECIMAL(10,0), IN `pTenor` INT, IN `pLoanInterest` DECIMAL(5,2), IN `pInstallment` DECIMAL(10,0), IN `pTotalInstallment` DECIMAL(10,0))
BEGIN
insert into t_order_loan (

	order_seq,

    customer_seq,

    dp,

    admin_fee,

    tenor,

    loan_interest,

    installment,

    total_installment,

    created_by,

    created_date,

    modified_by,

    modified_date

) values (

	pOrderSeq,

    pCustomerSeq,

    pDp,

    pAdminFee,

    pTenor,

    pLoanInterest,

    pInstallment,

    pTotalInstallment,

    pUserID,

    now(),

    pUserID,

    now()

);

END