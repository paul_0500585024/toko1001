DELIMITER $$
CREATE PROCEDURE sp_get_coupon_info
(
	pUserID Varchar(25),
	pIPAddress Varchar(50),
	pProductVariantSeq Varchar(500),
	pCouponCode Varchar(25)
)
BEGIN
Set @sqlCommand = Concat("
Select 
    m.seq,
    m.nominal
from    
m_coupon m join m_coupon_product mp
    on mp.coupon_seq = m.seq           
    join m_product_variant mv
    on mp.product_seq = mv.product_seq  
Where
    mv.seq in (", pProductVariantSeq ,") and
    CURDATE() between m.coupon_period_from and m.coupon_period_to and
    m.status = 'A' and
    m.coupon_code = '", pCouponCode,"';");
Prepare sql_stmt FROM @sqlCommand;
Execute sql_stmt;
Deallocate prepare sql_stmt;
END $$ 
DELIMITER ; 
