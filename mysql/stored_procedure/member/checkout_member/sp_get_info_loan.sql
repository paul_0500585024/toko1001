CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_info_loan`(IN `pProductVariantSeq` BIGINT, IN `pTotal` DECIMAL(10,0), IN `pTenorSeq` SMALLINT, IN `pAgentSeq` BIGINT )
BEGIN
select mpl.*, mp.admin_fee from m_partner_loan mpl join (select ma.partner_seq, mpart.admin_fee from m_agent ma join m_partner mpart on ma.partner_seq=mpart.seq where ma.seq=pAgentSeq) mp on mpl.partner_seq = mp.partner_seq where mpl.active='1' and mpl.product_category_seq in (select p.category_l2_seq from m_product p join m_product_variant pv on p.seq=pv.product_seq where pv.seq=pProductVariantSeq) order by mpl.tenor;
if pTenorSeq > 0 then
	select pld.*, mpl.loan_interest, mpl.tenor from m_partner_loan_dp pld join (select seq, loan_interest, tenor from m_partner_loan where active='1' and partner_seq in (select partner_seq from m_agent where seq=pAgentSeq) and product_category_seq in (select p.category_l2_seq from m_product p join m_product_variant pv on p.seq=pv.product_seq where pv.seq=pProductVariantSeq) and seq=pTenorSeq) mpl on pld.partner_loan_seq=mpl.seq where pld.active='1' and pTotal between pld.minimal_loan and pld.maximal_loan;
end if;
END