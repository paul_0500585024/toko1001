CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_coupon_period`(
	pUserID Varchar(25),
        pIPAddress Varchar(50),
        pProductVariantSeq Varchar(500)
)
BEGIN

Set @sqlCommand = Concat("
Select Exists (
Select 
    m.seq
from    
m_coupon m join m_coupon_product mp
    on mp.coupon_seq = m.seq           
           join m_product_variant mv
    on mp.product_seq = mv.product_seq  
Where
    mv.seq in (", pProductVariantSeq ,") and
    CURDATE() between m.coupon_period_from and m.coupon_period_to and
    m.status = 'A'
) 
as valid");

Prepare sql_stmt FROM @sqlCommand;
Execute sql_stmt;
Deallocate prepare sql_stmt;


END