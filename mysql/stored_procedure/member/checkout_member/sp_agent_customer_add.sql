CREATE DEFINER=`root`@`%` PROCEDURE `sp_agent_customer_add`(
        `pUserID` VARCHAR(100),
        `pIPAddress` VARCHAR(50),
	`pAgentSeq` int(10) unsigned,
	`pIdentityNo` varchar(50),
	`pIdentityAddress` varchar(50),
	`pBirthday` date,
	`pAddress` mediumtext,
	`pEmailAddress` varchar(50),
	`pDistrictSeq` smallint(5) unsigned,
	`pSubDistrict` varchar(150),
	`pZipCode` varchar(10),
	`pPicName` varchar(50),
	`pPhoneNo` varchar(50)        
    )
BEGIN
Declare new_seq Int unsigned;
Select 
	Max(seq) + 1 Into new_seq
From m_agent_customer;
If new_seq Is Null Then
	Set new_seq = 1;
End If;
insert into m_agent_customer (
`agent_seq`,
`seq`,
`identity_no`,
`identity_address`,
`birthday`,
`address`,
`email_address`,
`district_seq`,
`sub_district`,
`zip_code`,
`pic_name`,
`phone_no`,
    created_by,
    created_date,
    modified_by,
    modified_Date
) values (
  pAgentSeq,
    new_seq,
  pIdentityNo,
  pIdentityAddress,
  pBirthday,
  pAddress,
  pEmailAddress,
  pDistrictSeq,
  pSubDistrict,
  pZipCode,
  pPicName,
  pPhoneNo,
    pUserID,
    now(),
    pUserID,
    now()
);
    
select new_seq;
END