CREATE DEFINER=`root`@`%` PROCEDURE `sp_check_free_fee_expedition`(
		pUserID VarChar(25),
		pIPAddress VarChar(25),
		pMerchantSeq int unsigned,
		pCitySeq int    
)
BEGIN

Declare pOriginCitySeq MediumInt;

select case when md.seq is null then d.city_seq else md.city_seq end into pOriginCitySeq
from
	m_district d join m_merchant m
		on m.district_seq = d.seq 
				 left join m_district md
		on md.seq = m.pickup_district_seq
where
	m.seq = pMerchantSeq;
	
select 
	p.seq
From  m_promo_free_fee_period p left join m_promo_free_fee_city c
		on p.seq = c.promo_seq	left Join m_promo_free_fee_city f
        on f.promo_seq = c.promo_seq
								left join m_promo_free_fee_merchant m
		on m.promo_seq = p.seq
where
	Date(now()) between p.date_from and p.date_to And
	status= 'A' And
    active = '1' And
    m.merchant_seq is null and
    ((all_origin_city ='1' And all_destination_city ='1') OR 
    (all_origin_city ='1' and c.city_seq = pCitySeq and c.`type`='D') OR 
    (all_destination_city ='1' and c.city_seq = pOriginCitySeq and c.`type`='O') OR
    ((c.city_seq= pCitySeq And c.`type`='D' And f.city_seq = pOriginCitySeq and f.`type`='O')))
union all
select 
	p.seq
From  m_promo_free_fee_period p left join m_promo_free_fee_city c
		on p.seq = c.promo_seq	left Join m_promo_free_fee_city f
        on f.promo_seq = c.promo_seq
								join m_promo_free_fee_merchant m
		on m.promo_seq = p.seq
where
	Date(now()) between p.date_from and p.date_to And
	status= 'A' And
    active = '1' And
    m.merchant_seq = pMerchantSeq And
    ((all_origin_city ='1' And all_destination_city ='1') OR 
    (all_origin_city ='1' and c.city_seq = pCitySeq and c.`type`='D') OR 
    (all_destination_city ='1' and c.city_seq = pOriginCitySeq and c.`type`='O') OR
    ((c.city_seq= pCitySeq And c.`type`='D' And f.city_seq = pOriginCitySeq and f.`type`='O')));

END