DROP function IF EXISTS `escape_string`;

DELIMITER $$
CREATE DEFINER=`root`@`%` FUNCTION `escape_string`(str VarChar(500)) RETURNS varchar(600) CHARSET utf8
BEGIN
    Return Replace(str, "'", "''");
END$$

DELIMITER ;

