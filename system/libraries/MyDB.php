<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CI_MyDB {

    private $CI, $Data, $mysqli, $ResultSet;

    /**
     * The constructor
     */
    function __construct() {
        $this->CI = & get_instance();
        $this->Data = '';
        $this->ResultSet = array();
        $this->mysqli = $this->CI->db->conn_id;
    }

    public function get_multi_results($sql_command) {
        $k = 0;
        $has_result = true;
        unset($this->Data);
        $this->Data = '';
        
        if (mysqli_multi_query($this->mysqli, $sql_command)) {
            do {
                if ($result = $this->mysqli->store_result()) {
                    $l = 0;
                    while ($row = $result->fetch_object()) {
                        $this->Data[$k][$l] = $row;
                        $l++;
                    }
                    $k++;
                    mysqli_free_result($result);
                }
                if ($this->mysqli->more_results()) {
                    
                } else {
                    $has_result = false;
                }

                if (!$has_result) {
                    break;
                }
            } while ($this->mysqli->next_result());
        }
        
        return $this->Data;
        //        $this->mysqli->close();
    }

}
?>  

