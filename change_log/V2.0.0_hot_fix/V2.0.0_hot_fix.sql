/*
==============================
Change Store Procedure
==============================
*/

DROP PROCEDURE IF EXISTS sp_change_password_member;
DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_change_password_member`(
    pUserID VarChar(200), 
    pIPAddr VarChar(50),
    pNewPassword VarChar(1000)
)
BEGIN  
	Update m_member 
    Set `new_password` = pNewPassword 
    Where email = pUserID;
    
End$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_merchant_order_return_list;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_merchant_order_return_list`(
        `pUserID` VARCHAR(25),
        `pMerchantSeq` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pCurrPage` INTEGER,
        `pRecPerPage` INTEGER,
        `pDirSort` VARCHAR(4),
        `pColumnSort` VARCHAR(50),
        `pNo_return` VARCHAR(50),
        `pDateSearch` CHAR(1),
        `pReturnDateFrom` DATETIME,
        `pReturnDateTo` DATETIME,
        `pShip_status` CHAR(1),
        `pMember_email` VARCHAR(100),
        `pNew_Status` CHAR(1),
        `pShip_To_member_Status` CHAR(1),
        `pShip_From_Member_Status` CHAR(1),
        `pReceived_By_Member_Status` CHAR(1),
        `pReceived_By_Admin_Status` CHAR(1),
        `pRejected_Status` CHAR(1)
    )
BEGIN
 -- Begin variables for Paging
    Declare startRec Int;
    Declare totalPage Int;
    Declare totalRec Int;
    -- End variables for Paging
    -- Begin SQL Where
    Declare sqlWhere VarChar(500);
    Declare sqlWhereC VarChar(500);
	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
    If pNo_return <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And t_op.`return_no` like '%" , escape_string(pNo_return), "%'");
    End If;

    If (pDateSearch = "1") Then
        Set sqlWhere = Concat(sqlWhere, " And t_op.`created_date` between  '",escape_string(pReturnDateFrom),"' And  '",escape_string(pReturnDateTo) ,"'");
    End If;


    If pMember_email <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And (mem.`email` = '" , escape_string(pMember_email), "' or mag.`email` = '" , escape_string(pMember_email), "')");
    End If;

	Set sqlWhereC=sqlWhere;

    If pShip_status <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And t_op.`shipment_status` = '" , escape_string(pShip_status), "'");
    End If;
    -- End SQL Where
    -- Begin Paging Info
    Set @sqlCommand = "SELECT count(t_op.seq) Into @totalRec
		FROM
		t_order_product_return t_op 
        join m_product_variant m_pv on t_op.product_variant_seq = m_pv.seq
        join m_product m_p on m_pv.product_seq = m_p.seq
        join t_order t_or on t_op.order_seq = t_or.seq
        join m_variant_value m_v on m_pv.variant_value_seq = m_v.seq
		left outer join m_member mem on t_or.member_seq = mem.seq
		left outer join m_agent mag on t_or.agent_seq = mag.seq
		WHERE t_op.return_status <> 'R' and t_op.shipment_status in ('S','R','T','C')
        ";

    Set @sqlCommand = Concat(@sqlCommand, " And m_p.merchant_seq ='" , pMerchantSeq , "'" );
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);
        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;
        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info
    Set @sqlCommand = "
		SELECT
		t_op.seq as return_seq , mag.`email` as agent_email,
		mem.`email` as member_email,t_op.return_no ,
		t_op.order_seq , t_or.order_no , m_p.name as product_name , m_v.value as variant_name,m_v.seq as variant_value_seq,
		t_op.qty ,t_op.return_status ,
		t_op.shipment_status,t_op.awb_admin_no,t_op.awb_merchant_no,
		t_op.admin_received_date,t_op.created_date,
        '' as action
		FROM
        t_order_product_return t_op 
        join m_product_variant m_pv on t_op.product_variant_seq = m_pv.seq
        join m_product m_p on m_pv.product_seq = m_p.seq
        join t_order t_or on t_op.order_seq = t_or.seq
        join m_variant_value m_v on m_pv.variant_value_seq = m_v.seq
		left outer join m_member mem on t_or.member_seq = mem.seq
		left outer join m_agent mag on t_or.agent_seq = mag.seq
		WHERE t_op.return_status <> 'R' and t_op.shipment_status in ('S','R','T','C')";
         Set @sqlCommand = Concat(@sqlCommand, " AND m_p.merchant_seq ='" , pMerchantSeq , "'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort," ", pDirSort);
    Set @sqlCommand = Concat(@sqlCommand, "  Limit ", startRec, ", ", pRecPerPage);
    Select pCurrPage, totalPage, totalRec as total_rec;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;


    -- Begin Qty Status
    Set @sqlCommand = " SELECT COUNT(t_op.shipment_status) AS qty_status ,t_op.shipment_status FROM t_order_product_return t_op join t_order t_or on t_op.order_seq = t_or.seq left outer join m_member mem on t_or.member_seq= mem.seq left outer join m_agent mag on t_or.agent_seq= mag.seq join m_product_variant m_pv on t_op.product_variant_seq = m_pv.seq join m_product m_p on m_pv.product_seq = m_p.seq WHERE t_op.return_status <> 'R' and t_op.shipment_status in ('S','R','T','C')";
    Set @sqlCommand = Concat(@sqlCommand, " and m_p.merchant_seq ='" , pMerchantSeq , "'");
    If sqlWhereC <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhereC);
    End If;
	Set @sqlCommand = Concat(@sqlCommand, " GROUP BY t_op.shipment_status");

    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;

    Set @totalRec = Null;
    Set @sqlCommand = Null;
END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS sp_get_password_agent;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_get_password_agent`(
	pUserID Varchar(50)
)
BEGIN
SELECT
	 seq,
	 email,
	 password,
	 name,
	 status
FROM 
	m_agent
WHERE 
	email = pUserID And
    `status` = 'A';
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_change_password_agent;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_change_password_agent`(
	pUserID VarChar(200), 
	pIPAddr VarChar(50),
    pNewPassword VarChar(1000)
)
BEGIN  
Update m_agent
    Set `password` = pNewPassword 
    Where email = pUserID;
    
End$$
DELIMITER ;

/*
======================================
ADD VALUE TABLE
======================================
*/

INSERT INTO `m_email_template` (`code`,`subject`,`content`,`email_to`,`notes`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES ('ORDER_RETURN_MERCHANT_APPROVE','Pengembalian barang dengan nomor [TRX_NO] di [WEB_TITLE]','<div>Halo [RECIPIENT_NAME],</div><br><div> Kami informasikan bahwa pengembalian barang dengan nomor <strong>[TRX_NO]</strong> telah disetujui oleh Merchant dan <br> telah dikirim kembali pada tanggal <strong>[SHIP_TO_MEMBER_DATE]</strong> menggunakan expedisi <strong>[EXPEDITION_NAME]</strong> dengan nomor resi <strong>[AWB_MERCHANT_NO]</strong>. Silahkan anda periksa kembali Klik <a href=\"[LINK]\">disini</a>','Member','Pemberitahuan Pengembalian Barang Disetujui oleh Merchant','admin','0000-00-00 00:00:00','admin','0000-00-00 00:00:00');
INSERT INTO `m_email_template` (`code`,`subject`,`content`,`email_to`,`notes`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES ('ORDER_RETURN_MERCHANT_REFUND','Pengembalian barang dengan nomor [TRX_NO] di [WEB_TITLE]','<div>Halo [RECIPIENT_NAME],</div><br><div> Mohon maaf pengembalian barang dengan nomor <strong>[TRX_NO]</strong> telah dibatalkan oleh Merchant dan <br>telah diajukan sebagai pengembalian dana pada tanggal <strong>[MODIFIED_DATE]</strong> senilai <strong>Rp. [TOTAL]</strong>. <br>Klik <a href=\"[ACCOUNT_LINK]\">disini</a> untuk cek mutasi akun anda.','Member','Pemberitahuan Pengembalian Barang diganti Pengembalian Dana oleh Merchant','admin','0000-00-00 00:00:00','admin','0000-00-00 00:00:00');

/*
================================
ADD FIELD
================================
*/

ALTER TABLE m_partner ADD commission_pro_rate DECIMAL(3,0) NOT NULL DEFAULT 0 AFTER admin_fee;
ALTER TABLE m_agent ADD commission_pro_rate DECIMAL(3,0) NOT NULL DEFAULT 0 AFTER ip_address;
ALTER TABLE t_order_product MODIFY COLUMN commission_fee_partner_percent DECIMAL(8,6) NOT NULL DEFAULT 0;
ALTER TABLE t_order_product MODIFY COLUMN commission_fee_percent DECIMAL(8,6) NOT NULL DEFAULT 0;
ALTER TABLE t_redeem_period ADD expedition_commission DECIMAL(5,2) NOT NULL DEFAULT 0 AFTER status;

/*
=====================================
ADD Stored Procedure
=====================================
*/

DROP PROCEDURE IF EXISTS sp_check_partner_exist;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_check_partner_exist`(
        `pEmail` VARCHAR(50)
    )
BEGIN
SELECT * FROM m_partner WHERE email = pEmail;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS `sp_get_info_order_loan_admin`;
DELIMITER $$
CREATE PROCEDURE `sp_get_info_order_loan_admin` (
    pIPAddress VARCHAR(25),
    pCreditSeq INTEGER
)
BEGIN

SELECT
    customer_seq,
    dp,
    admin_fee,
    tenor,
    loan_interest,
    installment,
    status_order
FROM
    t_order_loan
WHERE
    order_seq = pCreditSeq;
END$$
DELIMITER ;

/*
=====================================
ADD Stored Procedure
=====================================
*/
DROP PROCEDURE IF EXISTS `sp_deposit_agent_substract`;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_deposit_agent_substract`(
        `pUserID` VARCHAR(25),
        `pIPAddress` VARCHAR(25),
        `pagentSeq` INTEGER UNSIGNED,
        `pDepositAmt` DECIMAL(10,0)
    )
BEGIN
update m_agent set 
	deposit_amt = deposit_amt - pDepositAmt,
    modified_by = pUserID,
    modified_date = now()
where
	seq = pagentSeq;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS `sp_redeem_period_add`;
DELIMITER $$
CREATE PROCEDURE `sp_redeem_period_add` (
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pFdate DATE,
        pTdate DATE,        
        pExpeditionCommission Decimal(5,2)
)
BEGIN

Declare new_seq INT(10) unsigned;

Select 
        Max(seq) + 1 Into new_seq
From t_redeem_period;

If new_seq Is Null Then
        Set new_seq = 1;
End If;

Insert Into t_redeem_period(		
        seq,
        from_date,
        to_date,
        `status`,
        expedition_commission,
        created_by,
        created_date,
        modified_by,
        modified_date
) Values (
        new_seq,
        pFdate,
        pTdate,
        'O',
        pExpeditionCommission,
        pUserID,
        Now(),
        pUserID,
        Now()
);

END $$
DELIMITER ;

DROP PROCEDURE IF EXISTS `sp_redeem_period_by_seq`;
DELIMITER $$
CREATE PROCEDURE `sp_redeem_period_by_seq` (
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq INT(10) unsigned
)
BEGIN

Select
    `seq`,
    `from_date`,
    `to_date`,
    `status`,
     expedition_commission,
    `created_by`,
    `created_date`,
    `modified_by`,
    `modified_date`    
From t_redeem_period
Where
    seq = pSeq;

END $$
DELIMITER ;


DROP PROCEDURE IF EXISTS `sp_redeem_period_update`;
DELIMITER $$
CREATE PROCEDURE `sp_redeem_period_update` (
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq int(10) unsigned,
        pFdate DATE,
        pTdate DATE,
        pExpeditionCommission Decimal(5,2)
)
BEGIN

update t_redeem_period set
    from_date = pFdate,
    to_date = pTdate,
    expedition_commission = pExpeditionCommission,
    modified_by = pUserID,
    modified_date = now()
Where
    seq = pSeq and `status`='O';

END $$
DELIMITER ;


DROP PROCEDURE IF EXISTS `sp_thumbs_detail`;
DELIMITER $$
CREATE PROCEDURE `sp_thumbs_detail` (
    `pProductVariantSeq` INTEGER UNSIGNED,
    `pUser` VARCHAR(10),
    `pAgentSeq` BIGINT(20)
)
BEGIN
DECLARE pProductSeq INT UNSIGNED;
DECLARE partnerCommision decimal(5,2);
DECLARE agentCommision decimal(5,2);


SELECT 
    agent.commission_pro_rate,
    partner.commission_pro_rate 
    INTO 
    agentCommision, 
    partnerCommision
FROM 
    m_agent agent
JOIN
    m_partner partner
ON 
    agent.partner_seq = partner.seq
WHERE
    agent.seq = pAgentSeq; 

SELECT
	product_seq INTO pProductSeq
FROM
	m_product_variant
WHERE
	`active`='1' AND
	seq = pProductVariantSeq;
IF pUser = 'AGENT' THEN
SELECT
    p.name AS `name`,
    p.seq AS product_seq,
    p.`merchant_seq` AS merchant_seq,
    vv.seq AS variant_seq,
    vv.value AS `variant_value`,
    pv.`product_price` AS `product_price`,
    pv.`sell_price` AS `sell_price`,
    pv.`seq` AS `product_variant_seq`,
    p.`description` AS description,
    p.`specification` AS specification,
    p.`warranty_notes` AS guarante,
    p.`p_weight_kg` AS weight,
    p.`p_length_cm` AS dimension_lenght,
    p.`p_width_cm` AS dimension_width,
    p.`p_height_cm` AS dimension_height,
    m.`name` AS merchant_name,
    m.`logo_img`,
    pv.`pic_1_img` AS image1,
    pv.`pic_2_img` AS image2,
    pv.`pic_3_img` AS image3,
    pv.`pic_4_img` AS image4,
    pv.`pic_5_img` AS image5,
    pv.`pic_6_img` AS image6,
    pv.`max_buy`,
    m.`code` AS `code`,
    d.`name` AS merchant_district,
    c.`name` AS merchant_city,
    pr.`name` AS merchant_province,
    p.category_l2_seq AS `category_l2_seq`,
    p.category_ln_seq AS `category_ln_seq`,
    ps.`stock` AS `stock`,
    ps.`merchant_sku` AS `sku`,
    qry.seq AS promo_credit_bank_seq,
    m.to_date,
    CONCAT(' x ',qry.credit_month,' Bulan ',' -- ',qry.bank_name) AS credit_month,
    qry.`status` AS status_credit ,
    CASE WHEN 
        mtrx.trx_fee_percent is null THEN (pv.sell_price * pc.trx_fee_percent/100) * ((partnerCommision)/100) * (agentCommision/100)  
        ELSE (pv.sell_price * mtrx.trx_fee_percent/100) * (partnerCommision/100) * ((agentCommision)/100) 
    END as commission_fee,
    CASE WHEN 
        mtrx.trx_fee_percent is null THEN (pc.trx_fee_percent/100) * (partnerCommision/100) * (agentCommision/100) *100
        ELSE (mtrx.trx_fee_percent/100) * (partnerCommision/100) * (agentCommision/100) * 100
    END as commission_fee_percent,
    mtrx.trx_fee_percent as test1,
    IF((NOW() < m.from_date OR NOW() > m.to_date OR (m.from_date = '0000-00-00 00:00:00' AND m.to_date = '0000-00-00 00:00:00')),'O','C') AS store_status,
    qry2.pc_status, qry2.pc_seq,
    mtrx.trx_fee_percent
FROM
    m_product_variant pv
        LEFT JOIN
    m_product p ON pv.product_seq = p.seq
        INNER JOIN
    m_variant_value vv ON pv.variant_value_seq = vv.seq
        INNER JOIN
    m_merchant m ON p.`merchant_seq` = m.`seq`
        LEFT JOIN
    m_product_stock ps ON ps.`product_variant_seq` = pv.`seq`
        JOIN
    (select * from m_product_category where `level`=2) mpc on mpc.seq = p.category_l2_seq
	JOIN
    m_district d ON d.seq = m.district_seq
        JOIN
    m_city c ON c.seq = d.city_seq
        JOIN
    m_province pr ON pr.seq = c.province_seq
	LEFT JOIN
	(SELECT
	pc.seq,
	pc.promo_credit_name,
        pc.minimum_nominal,
        pc.maximum_nominal,
        pc.status,
        pcp.product_variant_seq,
        b.bank_name,
        bc.credit_month
	FROM
            m_promo_credit_product pcp
        LEFT JOIN
            m_promo_credit pc ON pcp.promo_credit_seq = pc.seq
        LEFT JOIN
            m_promo_credit_bank pcb ON pcb.promo_seq = pc.seq
        LEFT JOIN
            m_bank_credit bc ON bc.seq = pcb.bank_credit_seq
        LEFT JOIN
            m_bank b ON b.seq = bc.bank_seq
	WHERE pc.status = 'A' AND
        CURDATE() BETWEEN pc.promo_credit_period_from AND pc.promo_credit_period_to) AS qry ON qry.product_variant_seq = pv.seq
        and pv.sell_price between qry.minimum_nominal and qry.maximum_nominal

        LEFT JOIN
            (select
            pc.seq pc_seq,
            pc.status as pc_status,
            pc.minimum_nominal as pc_min_nominal,
            pc.maximum_nominal as pc_max_nominal,
            mpcc.category_seq
            from m_promo_credit_category mpcc LEFT JOIN m_promo_credit pc on mpcc.promo_credit_seq = pc.seq
            where pc.status = 'A' and
            CURDATE() between pc.promo_credit_period_from and pc.promo_credit_period_to) as qry2
            on qry2.category_seq = mpc.parent_seq and pv.sell_price between qry2.pc_min_nominal and qry2.pc_max_nominal
        LEFT JOIN (select * from m_agent_trx_commission where agent_seq=pAgentSeq) as matc ON matc.category_l2_seq= p.`category_l2_seq`
        LEFT JOIN m_merchant_trx_fee mtrx ON mtrx.category_l2_seq = p.category_l2_seq AND mtrx.merchant_seq = p.merchant_seq
        LEFT JOIN m_product_category pc ON p.category_l2_seq = pc.seq
WHERE
	pv.`active`='1' AND
	pv.seq= pProductVariantSeq AND
	pv.status IN ('L','C')
LIMIT 1;
else
SELECT
    p.name AS `name`,
    p.seq AS product_seq,
    p.`merchant_seq` AS merchant_seq,
    vv.seq AS variant_seq,
    vv.value AS `variant_value`,
    pv.`product_price` AS `product_price`,
    pv.`sell_price` AS `sell_price`,
    pv.`seq` AS `product_variant_seq`,
    p.`description` AS description,
    p.`specification` AS specification,
    p.`warranty_notes` AS guarante,
    p.`p_weight_kg` AS weight,
    p.`p_length_cm` AS dimension_lenght,
    p.`p_width_cm` AS dimension_width,
    p.`p_height_cm` AS dimension_height,
    m.`name` AS merchant_name,
    m.`logo_img`,
    pv.`pic_1_img` AS image1,
    pv.`pic_2_img` AS image2,
    pv.`pic_3_img` AS image3,
    pv.`pic_4_img` AS image4,
    pv.`pic_5_img` AS image5,
    pv.`pic_6_img` AS image6,
    pv.`max_buy`,
    m.`code` AS `code`,
    d.`name` AS merchant_district,
    c.`name` AS merchant_city,
    pr.`name` AS merchant_province,
    p.category_l2_seq AS `category_l2_seq`,
    p.category_ln_seq AS `category_ln_seq`,
    ps.`stock` AS `stock`,
    ps.`merchant_sku` AS `sku`,
    qry.seq AS promo_credit_bank_seq,
    m.to_date,
    CONCAT(' x ', qry.credit_month, ' Bulan ',' -- ',qry.bank_name) AS credit_month, qry.`status` AS status_credit,
    qry2.pc_status, qry2.pc_seq,
    IF((NOW() < m.from_date OR NOW() > m.to_date OR (m.from_date = '0000-00-00 00:00:00' AND m.to_date = '0000-00-00 00:00:00')),'O','C') as store_status
FROM
    m_product_variant pv
        LEFT JOIN
    m_product p ON pv.product_seq = p.seq
        JOIN
    m_product_category mpcat on mpcat.seq=p.category_l2_seq
        INNER JOIN
    m_variant_value vv ON pv.variant_value_seq = vv.seq
        INNER JOIN
    m_merchant m ON p.`merchant_seq` = m.`seq`
        LEFT JOIN
    m_product_stock ps ON ps.`product_variant_seq` = pv.`seq`
        JOIN
    m_district d ON d.seq = m.district_seq
        JOIN
    m_city c ON c.seq = d.city_seq
        JOIN
    m_province pr ON pr.seq = c.province_seq
	LEFT JOIN
	(SELECT
		pc.seq,
		pc.promo_credit_name,
        pc.minimum_nominal,
        pc.maximum_nominal,
        pc.status,
        pcp.product_variant_seq,
        b.bank_name,
        bc.credit_month
	FROM
		m_promo_credit_product pcp
        LEFT JOIN m_promo_credit pc ON
		pcp.promo_credit_seq = pc.seq
	LEFT JOIN
            	m_promo_credit_bank pcb ON pcb.promo_seq = pc.seq
        LEFT JOIN
                m_bank_credit bc ON bc.seq = pcb.bank_credit_seq
        LEFT JOIN
                m_bank b ON b.seq = bc.bank_seq
	WHERE pc.status = 'A' AND
        CURDATE() BETWEEN pc.promo_credit_period_from AND pc.promo_credit_period_to) AS qry ON qry.product_variant_seq = pv.seq
        and pv.sell_price between qry.minimum_nominal and qry.maximum_nominal

	LEFT JOIN
		(select
		pc.seq pc_seq,
		pc.status as pc_status,
		pc.minimum_nominal as pc_min_nominal,
		pc.maximum_nominal as pc_max_nominal,
		mpcc.category_seq
		from m_promo_credit_category mpcc LEFT JOIN m_promo_credit pc on mpcc.promo_credit_seq = pc.seq
		where pc.status = 'A' and
		CURDATE() between pc.promo_credit_period_from and pc.promo_credit_period_to) as qry2
		on qry2.category_seq = mpcat.parent_seq and pv.sell_price between qry2.pc_min_nominal and qry2.pc_max_nominal

WHERE
	pv.`active`='1' AND
	pv.seq= pProductVariantSeq AND
	pv.status IN ('L','C')
LIMIT 1;
END IF;
SELECT
     a.`name` AS `name`,
     av.`value` AS `value`
FROM
    `m_product` p
    INNER JOIN `m_product_attribute` pa
        ON (`pa`.`product_seq` = `p`.`seq`)
    INNER JOIN `m_attribute_value` av
        ON (`pa`.`attribute_value_seq` = `av`.`seq`)
    INNER JOIN `m_attribute` a
        ON (`av`.`attribute_seq` = `a`.`seq`)
WHERE
	`p`.`seq`= pProductSeq
UNION ALL
SELECT
	ps.name AS `name`,
	ps.value AS `value`
FROM m_product_spec ps
WHERE ps.product_seq= pProductSeq;
END $$
DELIMITER ;

DROP PROCEDURE IF EXISTS `sp_thumbs_new_products`;
DELIMITER $$
CREATE PROCEDURE `sp_thumbs_new_products` (
        pSTART INTEGER, 
	pLIMIT INTEGER,
	pWHERE_CONDITION TEXT,
	pORDER_CONDITION TEXT,
	pUser VARCHAR(10),
	pAgentSeq BIGINT(20)
)
BEGIN
    
    DECLARE partnerCommision decimal(5,2);
    DECLARE agentCommision decimal(5,2);


IF(pORDER_CONDITION = '') THEN SET @ORDER = 'ORDER BY pv.seq DESC'; ELSE SET @ORDER = pORDER_CONDITION; END IF;
IF(pWHERE_CONDITION = '') THEN SET @WHERE = 'WHERE pv.active = ''1'' AND pv.status IN (''L'',''C'')'; ELSE SET @WHERE = pWHERE_CONDITION; END IF;

IF(pUser = 'AGENT') THEN
    SELECT 
            agent.commission_pro_rate,
            partner.commission_pro_rate 
            INTO 
            agentCommision, 
            partnerCommision
    FROM 
	m_agent agent
    JOIN
	m_partner partner
    ON 
	agent.partner_seq = partner.seq
    WHERE
	agent.seq = pAgentSeq;      
	SET @AGENT_TABLE = concat('LEFT JOIN m_merchant_trx_fee mtrx ON mtrx.category_l2_seq = p.category_l2_seq AND mtrx.merchant_seq = p.merchant_seq LEFT JOIN m_product_category pc ON p.category_l2_seq = pc.seq');
        SET @AGENT_SELECT = concat(',CASE WHEN mtrx.trx_fee_percent is null THEN (pc.trx_fee_percent * pv.sell_price/100) * ', (partnerCommision/100) ,'*', (agentCommision/100) ,' ELSE (mtrx.trx_fee_percent * pv.sell_price/100) * ', (partnerCommision/100) ,' * ', (agentCommision/100) ,' END AS commission_fee',
        ',CASE WHEN mtrx.trx_fee_percent is null THEN (pc.trx_fee_percent/100) * ', (partnerCommision/100) ,'*', (agentCommision/100) * 100 ,' ELSE (mtrx.trx_fee_percent/100) * ', (partnerCommision/100) ,' * ', (agentCommision/100) * 100 ,' END AS commission_fee_percent');
ELSE
	SET @AGENT_TABLE='';
	SET @AGENT_SELECT='';
        SET @AGENT_SELECTT='';
END IF;
SET @sql = CONCAT("SELECT SQL_CALC_FOUND_ROWS
    pv.product_seq,	
    pv.pic_1_img AS image,
    p.name AS `name`,
    p.`merchant_seq` AS merchant_seq,
    vv.seq AS variant_seq,
    vv.value AS `variant_value`,
    pv.`product_price` AS `product_price`,
    pv.`sell_price` AS `sell_price`,
    pv.`seq` AS `product_variant_seq`,
    ps.`stock` AS `stock`,
    ps.`merchant_sku` AS `sku`,
    qry.promo_credit_name,
    qry2.pcredit_name,
    m.code as merchant_code,
    m.name as merchant_name",@AGENT_SELECT,",
    m.to_date,
    if((NOW() < m.from_date OR NOW() > m.to_date OR (m.from_date = '0000-00-00 00:00:00' AND m.to_date = '0000-00-00 00:00:00')),'O','C') as store_status
FROM
    m_product_variant pv
        LEFT JOIN
    m_product p ON pv.product_seq = p.seq
        JOIN
    m_merchant m ON m.seq = p.merchant_seq
        JOIN
    m_product_category mpcat on mpcat.seq=p.category_l2_seq
        INNER JOIN
    m_variant_value vv ON pv.variant_value_seq = vv.seq
        LEFT JOIN
    m_product_stock ps ON ps.`product_variant_seq` = pv.`seq`
        LEFT JOIN
    (select
    pc.promo_credit_name,
    pc.minimum_nominal,
    pc.maximum_nominal,
    pcp.product_variant_seq
    from m_promo_credit_product pcp LEFT JOIN m_promo_credit pc on pcp.promo_credit_seq = pc.seq
    where pc.status = 'A' and
    CURDATE() between pc.promo_credit_period_from and pc.promo_credit_period_to) as qry
    on qry.product_variant_seq = pv.seq and pv.sell_price between qry.minimum_nominal and qry.maximum_nominal
        LEFT JOIN
    (select
    pc.promo_credit_name as pcredit_name,
    pc.minimum_nominal as pc_min_nominal,
    pc.maximum_nominal as pc_max_nominal,
    mpcc.category_seq
    from m_promo_credit_category mpcc LEFT JOIN m_promo_credit pc on mpcc.promo_credit_seq = pc.seq
    where pc.status = 'A' and
    CURDATE() between pc.promo_credit_period_from and pc.promo_credit_period_to) as qry2
    on qry2.category_seq = mpcat.parent_seq and pv.sell_price between qry2.pc_min_nominal and qry2.pc_max_nominal
",@AGENT_TABLE," ",@WHERE," ",@ORDER," LIMIT ?,?");
PREPARE STMT FROM @sql;
SET @START = pSTART;
SET @LIMIT = pLIMIT;
EXECUTE STMT USING @START, @LIMIT;
DEALLOCATE PREPARE STMT;
END $$
DELIMITER ;

DROP PROCEDURE IF EXISTS `sp_order_product_add`;
DELIMITER $$
CREATE PROCEDURE `sp_order_product_add` (
  `pUserID` VARCHAR(25),
  `pIPAddress` VARCHAR(25),
  `pMerchantSeq` INTEGER UNSIGNED,
  `pOrderSeq` BIGINT(20) UNSIGNED,
  `pProductVariantSeq` MEDIUMINT UNSIGNED,
  `pVariantValueSeq` SMALLINT UNSIGNED,
  `pQty` INTEGER UNSIGNED,
  `pSellPrice` DECIMAL(10,0),
  `pWeightKg` DECIMAL(7,2),
  `pShipPriceReal` DECIMAL(10,0),
  `pShipPriceCharged` DECIMAL(10,0),
  `pProductStatus` CHAR(1),
  `pAgentSeq` BIGINT(20) UNSIGNED)
BEGIN
DECLARE pTrxFeePercent DECIMAL (4,2);
DECLARE pInsRatePercent DECIMAL(4,2);
DECLARE pRateAgent DECIMAL (5,2);
DECLARE pRatePartner DECIMAL(5,2);
DECLARE pMerchantInfoSeq MEDIUMINT UNSIGNED;
DECLARE pTrxCommissionPercent DECIMAL (8,6);
DECLARE pTrxCommissionPartnerPercent DECIMAL (8,6);
SELECT 
  CASE
    WHEN m.trx_fee_percent IS NULL 
    THEN c.trx_fee_percent 
    ELSE m.trx_fee_percent 
  END INTO pTrxFeePercent 
FROM
  m_product p 
  JOIN m_product_variant v 
    ON v.product_seq = p.seq 
  JOIN m_product_category c 
    ON c.seq = p.category_l2_seq 
  LEFT JOIN m_merchant_trx_fee m 
    ON m.category_l2_seq = c.seq 
    AND m.merchant_seq = p.merchant_seq 
WHERE v.seq = pProductVariantSeq ;	
	
SELECT 
  MAX(seq) INTO pMerchantInfoSeq 
FROM
  m_merchant_info 
WHERE merchant_seq = pMerchantSeq ;

SELECT 
  e.ins_rate_percent INTO pInsRatePercent 
FROM
  t_order_merchant m 
  JOIN m_expedition_service s 
    ON s.seq = m.expedition_service_seq 
  JOIN m_expedition e 
    ON e.seq = s.exp_seq 
WHERE m.order_seq = pOrderSeq 
  AND m.merchant_info_seq = pMerchantInfoSeq;

IF pAgentSeq = "" THEN 
  SET pTrxCommissionPercent = 0;
  SET pTrxCommissionPartnerPercent = 0;
ELSE
SELECT 
    a.commission_pro_rate,
    p.commission_pro_rate
INTO
    pRateAgent,
    pRatePartner
FROM 
    m_agent a
JOIN
    m_partner p
ON
    a.partner_seq = p.seq
WHERE 
    a.seq = pAgentSeq;
SELECT
    CASE WHEN mtrx.trx_fee_percent IS NULL THEN (pc.trx_fee_percent/100) * (pRatePartner/100) * (pRateAgent/100) * 100  
        ELSE (mtrx.trx_fee_percent/100) * (pRatePartner/100) * (pRateAgent/100) * 100 
    END as commisionAgent,
    CASE WHEN mtrx.trx_fee_percent IS NULL THEN (pc.trx_fee_percent/100) * (pRatePartner/100) * 100
        ELSE (mtrx.trx_fee_percent/100) * (pRatePartner/100) * 100
    END as commisionPartner
INTO
    pTrxCommissionPercent,
    pTrxCommissionPartnerPercent
FROM 
    m_product_variant pv
JOIN
    m_product p
ON 
    pv.product_seq = p.seq
LEFT JOIN 
    m_merchant_trx_fee mtrx 
ON 
    mtrx.category_l2_seq = p.category_l2_seq 
AND 
    mtrx.merchant_seq = p.merchant_seq
LEFT JOIN 
    m_product_category pc
ON 
    p.category_l2_seq = pc.seq
WHERE 
    pv.seq = pProductVariantSeq;
END IF;

INSERT INTO t_order_product (
  order_seq,
  merchant_info_seq,
  product_variant_seq,
  variant_value_seq,
  qty,
  sell_price,
  weight_kg,
  ship_price_real,
  ship_price_charged,
  trx_fee_percent,
  ins_rate_percent,
  product_status,
  commission_fee_percent,
  commission_fee_partner_percent,
  created_by,
  created_date,
  modified_by,
  modified_date
) 
VALUES
  (
    pOrderSeq,
    pMerchantInfoSeq,
    pProductVariantSeq,
    pVariantValueSeq,
    pQty,
    pSellPrice,
    pWeightKg,
    pShipPriceReal,
    pShipPriceCharged,
    pTrxFeePercent,
    pInsRatePercent,
    pProductStatus,
    pTrxCommissionPercent,
    pTrxCommissionPartnerPercent,
    pUserID,
    NOW(),
    pUserID,
    NOW()
  ) ;
END $$
DELIMITER ;

DROP PROCEDURE IF EXISTS `sp_order_by_order_no`;
DELIMITER $$
CREATE PROCEDURE `sp_order_by_order_no` (
    pUserID Varchar(100),
    pIPAddress VarChar(25),
    pOrderNo VarChar(20)
)
BEGIN

    SELECT 
        o.order_no,
        o.order_date,
        o.payment_status,
        o.receiver_name,
        o.receiver_address,
        p.`name` AS province_name,
        c.`name` AS city_name,
        d.`name` AS district_name,
        o.receiver_phone_no,
        m.`name` AS member_name,
        m.email as member_email,
        a.`name` as agent_name,
        a.email as agent_email,
        pgm.name AS payment_name
    FROM
        t_order o
            JOIN
        m_district d ON o.receiver_district_seq = d.seq
            JOIN
        m_city c ON d.city_seq = c.seq
            JOIN
        m_province p ON c.province_seq = p.seq
            LEFT JOIN
        m_member m ON o.member_seq = m.seq
            LEFT JOIN
            m_agent a ON o.agent_seq = a.seq
        JOIN
        m_payment_gateway_method pgm ON o.pg_method_seq = pgm.seq
    WHERE
        order_no = pOrderNo;
    
select
	t.order_seq,
	t.merchant_info_seq,
	t.expedition_service_seq,
    e.`name` expedition_name,
    mm.`name` merchant_name,
    mm.email,
    mm.code as merchant_code,
    mm.seq merchant_seq,
	t.real_expedition_service_seq,    
	t.total_merchant,
	t.total_ins,
	t.total_ship_real,
	t.total_ship_charged,
	t.free_fee_seq,
	t.order_status,
	t.member_notes,
	t.printed,
	t.print_date,
	t.awb_seq,
	t.awb_no,
	t.ref_awb_no,
	t.ship_by,
	t.ship_by_exp_seq,
	t.ship_date,
	t.ship_note_file,
	t.ship_notes,
	t.received_date,
	t.received_by,
	t.redeem_seq,
	t.exp_invoice_seq,
	t.exp_invoice_awb_seq
from 
	t_order_merchant t join m_merchant_info m 
		on m.seq = t.merchant_info_seq 
				join m_district d
		on d.seq = m.pickup_district_seq
				join m_city c
		on c.seq = d.city_seq 
				join m_province p 
		on p.seq = c.province_seq
				join m_merchant mm
		on mm.seq = m.merchant_seq
				join m_expedition_service s
		on s.seq =  t.expedition_service_seq
				join m_expedition e
		on s.exp_seq = e.seq
				join t_order o
		on o.seq = t.order_seq				
where
		o.order_no = pOrderNo;
        
Select 
	t.order_seq,
    mi.merchant_seq,
	t.merchant_info_seq,
	p.`name` as display_name,
    v.seq as product_seq,
    v.pic_1_img as img,
    mv.`value`,
	mv.seq as value_seq,
	mv.`value` as variant_name,
    t.product_status,
	t.qty,
	t.sell_price,
	t.weight_kg,
	t.ship_price_real,
	t.ship_price_charged,
	t.trx_fee_percent,
	t.ins_rate_percent,
	t.product_status,
	t.qty_return,
	t.created_by,
	t.created_date,
	t.modified_by,
	t.modified_date
from
	t_order_product t 
		Join m_product_variant v			
			On v.seq = t.product_variant_seq
		Join m_product p
			On v.product_seq = p.seq
		Join m_variant_value vv
			On vv.seq = t.variant_value_seq
		Join m_variant_value mv 
			On mv.seq = v.variant_value_seq
		Join t_order o
			On o.seq = t.order_seq
		Join m_merchant_info mi
			On mi.seq = t.merchant_info_seq
		join m_merchant mm 
			On mm.seq = mi.merchant_seq
where
	o.order_no = pOrderNo;
    
    Select 
	o.seq,
	o.order_no,
	o.order_date,
	o.member_seq,
    o.signature,
    m.`name` member_name,
    m.email,
    m.mobile_phone,
    c.`name` city_name,
    case when o.payment_retry <> '0'  Then concat(o.order_no, '-', cast(o.payment_retry as char(2))) Else o.order_no End order_trans_no,	
	o.receiver_name,
	o.receiver_address,
	o.receiver_district_seq,
    d.`name` district_name,
    c.`name` city_name,
    p.`name` province_name,
	o.receiver_zip_code,
	o.receiver_phone_no,
	o.payment_status,
	o.pg_method_seq,
    pg.`code` payment_code,
    pg.name payment_name,
	o.paid_date,
    case when pv.nominal is null then mc.nominal else pv.nominal end as nominal ,
    pv.code as voucher_code,
	o.payment_retry,
	o.total_order,
	o.voucher_seq,
	o.voucher_refunded,
	o.total_payment,
	o.conf_pay_type,
	o.conf_pay_amt_member,
	o.conf_pay_date,
	o.conf_pay_note_file,
	o.conf_pay_bank_seq,
	o.conf_pay_amt_admin
from 
	t_order o join m_district d
		on o.receiver_district_seq = d.seq
			  join m_city c 
		on c.seq = d.city_seq 
			  join m_province p
		on p.seq = c.province_seq
			  join m_payment_gateway_method pg
		on pg.seq = o.pg_method_seq
        	left join m_promo_voucher pv 
		on o.voucher_seq = pv.seq
			left join m_coupon mc 
		on o.coupon_seq = mc.seq
			  join m_member m
		on m.seq = o.member_seq
where
	o.order_no = pOrderNo;

END$$
DELIMITER ;

