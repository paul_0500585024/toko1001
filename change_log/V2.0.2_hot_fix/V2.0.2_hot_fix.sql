/*
========================
ADD COLUMN 
========================
*/
ALTER TABLE m_promo_voucher ADD COLUMN agent_seq BIGINT UNSIGNED NOT NULL after member_seq;
ALTER TABLE t_order_product ADD COLUMN settled ENUM('1','0') NOT NULL DEFAULT 0 after product_status;
ALTER TABLE t_agent_account MODIFY COLUMN trx_type ENUM('CNL','WDW','ORD','RTR','TPU', 'CSH') NOT NULL;

/*
=========================
ADD STORED PROCEDURE
=========================
*/

DROP PROCEDURE IF EXISTS `sp_get_dropdown_credit`;
DELIMITER $$
CREATE PROCEDURE `sp_get_dropdown_credit` (
    pProductVariant Bigint Unsigned
)
BEGIN
select * from (
SELECT
    pc.seq,
    pcb.seq as credit_bank_seq,
    pc.`status`,
    pc.promo_credit_name,
    pc.promo_credit_period_from,
    pc.promo_credit_period_to,
    bc.credit_month,
    bc.rate,
    bc.bank_seq,
    pv.sell_price,
    b.bank_name
FROM
    m_promo_credit pc
        JOIN
    m_promo_credit_bank pcb ON pcb.promo_seq = pc.seq
        JOIN
    m_promo_credit_product pcp ON pcp.promo_credit_seq = pc.seq
        JOIN
    m_bank_credit bc ON bc.seq = pcb.bank_credit_seq
        JOIN
    m_product_variant pv ON pv.seq = pcp.product_variant_seq
        JOIN
    m_bank b ON bc.bank_seq = b.seq
WHERE
    pcp.product_variant_seq = pProductVariant and
    pc.status ='A' and
    CURDATE() between pc.promo_credit_period_from and pc.promo_credit_period_to
    and pv.sell_price between pc.minimum_nominal and pc.maximum_nominal
union all
SELECT
    pc.seq,
    pcb.seq as credit_bank_seq,
    pc.`status`,
    pc.promo_credit_name,
    pc.promo_credit_period_from,
    pc.promo_credit_period_to,
    bc.credit_month,
    bc.rate,
	bc.bank_seq,
    pv.sell_price,
    b.bank_name
FROM
    m_promo_credit pc
        JOIN
    m_promo_credit_bank pcb ON pcb.promo_seq = pc.seq
        JOIN
    m_promo_credit_category pcc ON pcc.promo_credit_seq = pc.seq
        JOIN
    m_bank_credit bc ON bc.seq = pcb.bank_credit_seq
        JOIN
    m_bank b ON bc.bank_seq = b.seq
        JOIN
    (select mpc.parent_seq,mpv.sell_price from m_product mp join m_product_category mpc on mp.category_l2_seq=mpc.seq
    join m_product_variant mpv on mp.seq=mpv.product_seq where mpv.seq=pProductVariant) pv
    on pv.parent_seq=pcc.category_seq
WHERE
    pc.status ='A' and
    CURDATE() between pc.promo_credit_period_from and pc.promo_credit_period_to
    and pv.sell_price between pc.minimum_nominal and pc.maximum_nominal
) a order by a.bank_name,a.credit_month
;
END

DELIMITER ;

DROP PROCEDURE IF EXISTS `sp_promo_credit_add`;
DELIMITER $$
CREATE PROCEDURE `sp_promo_credit_add`(
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pPromoCreditName VARCHAR(500),
    pPromoCreditPeriodFrom date,
    pPromoCreditPeriodTo date,
    pMinimumNominal DECIMAL(10,0),
    pMaximumNominal DECIMAL(10,0),
    pStatus char(1)
)
BEGIN
    Declare new_seq int Unsigned;
SELECT 
    MAX(seq) + 1
INTO new_seq FROM
    m_promo_credit;
    If new_seq Is Null Then
        Set new_seq = 1;
    End If;
    Insert Into m_promo_credit(
		seq, 
        promo_credit_name,
        promo_credit_period_from,
        promo_credit_period_to,
        minimum_nominal,
        maximum_nominal,
        status, 
        created_by,
        created_date,
        modified_by,
        modified_date
    ) Values (
        new_seq,
        pPromoCreditName,
        pPromoCreditPeriodFrom,
        pPromoCreditPeriodTo,
        pMinimumNominal,
        pMaximumNominal,
        pStatus,
        pUserID,
        Now(),
        pUserID,
        Now()
    );
END$$
DELIMITER ;

--------------------------
-- 5 Juni 2017 -- PENDING
--------------------------

/*
================================
ADD & MODIFY COLUMN -- PENDING--
================================
*/

ALTER TABLE t_order ADD apps ENUM('0','1') DEFAULT 0 AFTER signature;
ALTER TABLE t_order ADD gcm_id VARCHAR(200) DEFAULT '' AFTER apps;

/*
---------------------------
ADD STORED PROCEDURE
---------------------------
*/
DROP PROCEDURE IF EXISTS `sp_scheduler_cashback_list`;
DELIMITER $$
CREATE PROCEDURE `sp_scheduler_cashback_list` (
    pUserID VARCHAR(50),
    pIpAdress VARCHAR(50),
    pOrderStatus CHAR(1),
    pSettled CHAR(1),
    pProductStatus CHAR(1)
)
BEGIN
SELECT 
    op.order_seq,
    o.order_no,
    o.agent_seq,
    o.order_date,
    op.product_variant_seq,
    op.variant_value_seq,
    op.commission_fee_percent,
    op.sell_price,
    op.qty,
    op.settled,
    op.product_status,
    om.order_status,
    a.name AS agent_name,
    a.email AS agent_email,
    a.bank_name AS agent_bank_name,
    a.bank_branch_name AS agent_bank_branch_name,
    a.bank_acct_no AS agent_bank_acct_no,
    a.bank_acct_name AS agent_bank_acct_name,
    p.name as product_name,
    (SELECT 
            SUM((sell_price * qty) * commission_fee_percent / 100)
        FROM
            t_order_product
        WHERE
            order_seq = o.seq) AS total
FROM
    t_order_product op
        JOIN
    t_order_merchant om ON op.order_seq = om.order_seq AND op.merchant_info_seq = om.merchant_info_seq
        JOIN
    t_order o ON op.order_seq = o.seq
        JOIN
    m_agent a ON o.agent_seq = a.seq
        JOIN
    m_product_variant pv ON pv.seq = op.product_variant_seq
        JOIN
    m_product p ON p.seq = pv.product_seq
WHERE
    om.order_status = pOrderStatus AND op.settled = pSettled
        AND op.product_status = pProductStatus;
END$$



/*
=====================================================
-- 20 Juni 2017 -- Change Commission Agent Structure
=====================================================
*/

/*
==================
CREATE TABLE
==================
*/
CREATE TABLE `m_partner_group` (
  `partner_group_seq` int(11) unsigned NOT NULL,
  `partner_group_name` varchar(500) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`partner_group_seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `m_partner_group_commission` (
  `partner_group_seq` int(10) unsigned NOT NULL,
  `category_l1_seq` int(11) NOT NULL,
  `category_l2_seq` varchar(45) NOT NULL,
  `commission_fee_percent` DECIMAL(5,2) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`partner_group_seq`,`category_l1_seq`,`category_l2_seq`),
  CONSTRAINT `fk_m_partner_group_m_partner_group_commission` FOREIGN KEY (`partner_group_seq`) REFERENCES `m_partner_group` (`partner_group_seq`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `m_partner_product` (
  `seq` int(10) unsigned NOT NULL,
  `period_from` datetime NOT NULL,
  `period_to` datetime NOT NULL,
  `notes` text NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `m_partner_product_commission` (
  `partner_product_seq` int(10) unsigned NOT NULL,
  `product_seq` bigint(20) unsigned NOT NULL,
  `partner_group_seq` int(11) NOT NULL,
  `nominal_commission_partner` decimal(12,0) NOT NULL DEFAULT '0',
  `nominal_commission_agent` decimal(12,0) NOT NULL DEFAULT '0',
  `commission_fee_partner_percent` decimal(5,2) NOT NULL DEFAULT '0.00',
  `commission_fee_agent_percent` decimal(5,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`partner_product_seq`,`product_seq`,`partner_group_seq`),
  KEY `fk_m_product_m_partner_product_commission_idx` (`product_seq`),
  CONSTRAINT `fk_m_partner_product_m_partner_product_commission` FOREIGN KEY (`partner_product_seq`) REFERENCES `m_partner_product` (`seq`) ON UPDATE CASCADE,
  CONSTRAINT `fk_m_product_m_partner_product_commission` FOREIGN KEY (`product_seq`) REFERENCES `m_product` (`seq`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*
==================
ADD FIELD
==================
*/

ALTER TABLE t_order_product ADD COLUMN nominal_commission_partner DECIMAL(15,0) NOT NULL AFTER trx_fee_percent ;
ALTER TABLE t_order_product ADD COLUMN nominal_commission_agent DECIMAL(15,0) NOT NULL AFTER nominal_commission_partner ;
ALTER TABLE m_partner ADD COLUMN partner_group_seq INT UNSIGNED NULL AFTER email ;


/*
===================
28 July 2017 
===================
*/

ALTER TABLE t_order_voucher ADD COLUMN apps ENUM('1','0') AFTER signature;
ALTER TABLE t_order_voucher ADD COLUMN gcm_id varchar(1000) AFTER apps;


DROP PROCEDURE IF EXISTS `sp_member_order_list_by_order_no`;
DELIMITER $$
CREATE PROCEDURE `sp_member_order_list_by_order_no`(
    pUserID Varchar(100),
    pIPAddress VarChar(25),
    pOrderNo VarChar(20)

)
BEGIN
IF EXISTS (SELECT order_no FROM t_order WHERE order_no = pOrderNo) THEN
BEGIN
Select 
	o.seq,
	o.order_no,
	o.order_date,
	o.member_seq,
	o.signature,
	m.`name` member_name,
	m.email,
	m.mobile_phone,
	c.`name` city_name,
	case when o.payment_retry <> '0'  Then concat(o.order_no, '-', cast(o.payment_retry as char(2))) Else o.order_no End order_trans_no,	
	o.receiver_name,
	o.receiver_address,
	o.receiver_district_seq,
	d.`name` district_name,
	c.`name` city_name,
	p.`name` province_name,
	o.receiver_zip_code,
	o.receiver_phone_no,
	o.payment_status,
	o.pg_method_seq,
	pg.`code` payment_code,
	pg.name payment_name,
	o.paid_date,
	case when pv.nominal is null then mc.nominal else pv.nominal end as nominal,
	pv.code as voucher_code,
	o.payment_retry,
	o.total_order,
	o.voucher_seq,
	o.voucher_refunded,
	o.total_payment,
	o.conf_pay_type,
	o.conf_pay_amt_member,
	o.conf_pay_date,
	o.conf_pay_note_file,
	o.conf_pay_bank_seq,
	o.conf_pay_amt_admin,
        o.apps,
        o.gcm_id,
        o.promo_credit_seq,
        o.created_date
from 
	t_order o join m_district d
		on o.receiver_district_seq = d.seq
			  join m_city c 
		on c.seq = d.city_seq 
			  join m_province p
		on p.seq = c.province_seq
			  join m_payment_gateway_method pg
		on pg.seq = o.pg_method_seq
        	left join m_promo_voucher pv 
		on o.voucher_seq = pv.seq
        	left join m_coupon mc 
		on o.coupon_seq = mc.seq
			  join m_member m
		on m.seq = o.member_seq
where
	o.order_no = pOrderNo;

select
	t.order_seq,
	t.merchant_info_seq,
	t.expedition_service_seq,
	e.`name` expedition_name,
	mm.`name` merchant_name,
	mm.email,
	mm.code as merchant_code,
	mm.seq merchant_seq,
	t.real_expedition_service_seq,    
	t.total_merchant,
	t.total_ins,
	t.total_ship_real,
	t.total_ship_charged,
	t.free_fee_seq,
	t.order_status,
	t.member_notes,
	t.printed,
	t.print_date,
	t.awb_seq,
	t.awb_no,
	t.ref_awb_no,
	t.ship_by,
	t.ship_by_exp_seq,
	t.ship_date,
	t.ship_note_file,
	t.ship_notes,
	t.received_date,
	t.received_by,
	t.redeem_seq,
	t.exp_invoice_seq,
	t.exp_invoice_awb_seq,
        t.modified_date,
        (select count(*) from t_order_product where order_seq = t.order_seq and merchant_info_seq = t.merchant_info_seq) as total_product,
        (select sum(total_ship_charged) from t_order_merchant where order_seq = t.order_seq) as total_ship
from 
	t_order_merchant t join m_merchant_info m 
		on m.seq = t.merchant_info_seq 
				join m_district d
		on d.seq = m.pickup_district_seq
				join m_city c
		on c.seq = d.city_seq 
				join m_province p 
		on p.seq = c.province_seq
				join m_merchant mm
		on mm.seq = m.merchant_seq
				join m_expedition_service s
		on s.seq =  t.expedition_service_seq
				join m_expedition e
		on s.exp_seq = e.seq
				join t_order o
		on o.seq = t.order_seq				
where
		o.order_no = pOrderNo;
        
Select 
	t.order_seq,
	mi.merchant_seq,
	t.merchant_info_seq,
	p.`name` as display_name,
	v.seq as product_seq,
	v.pic_1_img as img,
	mv.`value`,
	mv.seq as value_seq,
	mv.`value` as variant_name,
	t.product_status,
	t.qty,
	t.sell_price,
	t.weight_kg,
	t.ship_price_real,
	t.ship_price_charged,
	t.trx_fee_percent,
	t.ins_rate_percent,
	t.product_status,
	t.qty_return,
	t.created_by,
	t.created_date,
	t.modified_by,
	t.modified_date
from
	t_order_product t 
		Join m_product_variant v			
			On v.seq = t.product_variant_seq
		Join m_product p
			On v.product_seq = p.seq
		Join m_variant_value vv
			On vv.seq = t.variant_value_seq
		Join m_variant_value mv 
			On mv.seq = v.variant_value_seq
		Join t_order o
			On o.seq = t.order_seq
		Join m_merchant_info mi
			On mi.seq = t.merchant_info_seq
		join m_merchant mm 
			On mm.seq = mi.merchant_seq
where
	o.order_no = pOrderNo;
END;	
ELSE
BEGIN
SELECT
	o.seq,
	o.order_no,
	o.order_no AS order_trans_no,
	o.member_seq,
	o.order_date,
	o.total_order,
	o.total_payment,
	o.payment_status,
	o.order_status,
	o.signature,
	o.pg_method_seq,
	o.phone_no AS phone_number,
	psn.nominal,
	ps.name AS provider_service_name,
	p.name AS provider_name,
	pgm.name AS pg_method_name,
	pgm.code AS payment_code,
	mapping_code,
	o.apps,
	o.gcm_id
FROM 
	t_order_voucher o JOIN m_provider_service_nominal psn
	ON psn.seq = o.provider_nominal_seq
		JOIN m_provider_service ps
	ON ps.seq = psn.provider_service_seq
		JOIN m_provider p
	ON p.seq = ps.provider_seq
		JOIN m_payment_gateway_method pgm
	ON pgm.seq = o.pg_method_seq
		JOIN m_payment_gateway pg
	ON pg.seq = pgm.pg_seq
WHERE
	order_no = pOrderNo;
END;
END IF;	

END$$
DELIMITER ;


