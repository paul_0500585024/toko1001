/*
================================================
CREATE TABLE TRUNK V.2.0.0 AGENT & PARTNER ADIRA
================================================
*/

-- MySQL dump 10.13  Distrib 5.6.23, for Win64 (x86_64)
--
-- Host: 192.168.50.5    Database: toko1001_dev
-- ------------------------------------------------------
-- Server version	5.1.73-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `m_agent`
--

DROP TABLE IF EXISTS `m_agent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_agent` (
  `seq` int(10) unsigned NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `partner_seq` int(10) unsigned NOT NULL,
  `referral` int(10) unsigned DEFAULT NULL,
  `birthday` date NOT NULL DEFAULT '0000-00-00',
  `gender` enum('M','F') NOT NULL,
  `mobile_phone` varchar(50) NOT NULL,
  `bank_name` varchar(50) NOT NULL,
  `bank_branch_name` varchar(50) NOT NULL,
  `bank_acct_no` varchar(50) NOT NULL,
  `bank_acct_name` varchar(50) NOT NULL,
  `profile_img` varchar(50) NOT NULL,
  `deposit_amt` decimal(10,0) NOT NULL DEFAULT '0',
  `status` enum('N','A','S','U') NOT NULL COMMENT 'Status merchant ( A : Authenticated, S : Suspended)',
  `last_login` datetime DEFAULT NULL,
  `ip_address` varchar(50) DEFAULT NULL,
  `created_by` varchar(25) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` varchar(25) NOT NULL,
  `modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`seq`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fk_m_agent_m_agent_idx` (`referral`),
  KEY `fk_m_partner_m_agend_idx` (`partner_seq`),
  CONSTRAINT `fk_m_agent_m_agent` FOREIGN KEY (`referral`) REFERENCES `m_agent` (`seq`) ON UPDATE CASCADE,
  CONSTRAINT `fk_m_partner_m_agent` FOREIGN KEY (`partner_seq`) REFERENCES `m_partner` (`seq`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_agent_address`
--

DROP TABLE IF EXISTS `m_agent_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_agent_address` (
  `agent_seq` int(10) unsigned NOT NULL,
  `seq` tinyint(3) unsigned NOT NULL,
  `alias` varchar(50) NOT NULL,
  `address` mediumtext NOT NULL,
  `district_seq` smallint(5) unsigned NOT NULL,
  `zip_code` varchar(10) NOT NULL,
  `pic_name` varchar(50) NOT NULL,
  `phone_no` varchar(50) NOT NULL,
  `default` enum('0','1') NOT NULL DEFAULT '0',
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `created_by` varchar(25) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` varchar(25) NOT NULL,
  `modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`agent_seq`,`seq`),
  CONSTRAINT `fk_m_agent_m_agent_address` FOREIGN KEY (`agent_seq`) REFERENCES `m_agent` (`seq`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
--
-- Table structure for table `m_agent_customer`
--

DROP TABLE IF EXISTS `m_agent_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_agent_customer` (
  `seq` bigint(20) unsigned NOT NULL,
  `agent_seq` int(10) unsigned NOT NULL,
  `identity_no` varchar(50) NOT NULL,
  `identity_address` mediumtext,
  `birthday` date NOT NULL,
  `address` mediumtext NOT NULL,
  `email_address` varchar(50) DEFAULT NULL,
  `district_seq` smallint(5) unsigned NOT NULL,
  `sub_district` varchar(150) NOT NULL,
  `zip_code` varchar(10) NOT NULL,
  `pic_name` varchar(100) NOT NULL,
  `phone_no` varchar(50) NOT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `created_by` varchar(25) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` varchar(25) NOT NULL,
  `modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`seq`,`agent_seq`),
  KEY `agent_seq` (`agent_seq`),
  KEY `district_seq` (`district_seq`),
  CONSTRAINT `m_agent_customer_ibfk_1` FOREIGN KEY (`agent_seq`) REFERENCES `m_agent` (`seq`),
  CONSTRAINT `m_agent_customer_ibfk_2` FOREIGN KEY (`district_seq`) REFERENCES `m_district` (`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_agent_log_security`
--

DROP TABLE IF EXISTS `m_agent_log_security`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_agent_log_security` (
  `agent_seq` int(10) unsigned NOT NULL,
  `seq` smallint(5) unsigned NOT NULL,
  `type` enum('1','2') NOT NULL DEFAULT '1' COMMENT 'Tipe Perubahan (1: Change Password, 2 : Forget Password )',
  `code` varchar(20) DEFAULT NULL,
  `old_pass` varchar(100) NOT NULL,
  `ip_addr` varchar(30) NOT NULL,
  `created_by` varchar(25) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `fk_m_member_t_notif_member_idx` (`agent_seq`),
  CONSTRAINT `fk_m_agent_m_agent_log_security` FOREIGN KEY (`agent_seq`) REFERENCES `m_agent` (`seq`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_agent_login_log`
--

DROP TABLE IF EXISTS `m_agent_login_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_agent_login_log` (
  `seq` bigint(20) unsigned NOT NULL,
  `agent_seq` int(10) unsigned NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `status` enum('S','F') NOT NULL COMMENT 'S = Success, F= Fail',
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`seq`),
  KEY `fk_m_agent_m_agent_login_log_idx` (`agent_seq`),
  CONSTRAINT `fk_m_agent_m_agent_login_log` FOREIGN KEY (`agent_seq`) REFERENCES `m_agent` (`seq`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_agent_trx_commission`
--

DROP TABLE IF EXISTS `m_agent_trx_commission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_agent_trx_commission` (
  `agent_seq` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `category_l1_seq` mediumint(8) unsigned NOT NULL,
  `category_l2_seq` mediumint(8) unsigned DEFAULT NULL,
  `commission_fee_percent` decimal(4,2) unsigned NOT NULL DEFAULT '0.00' COMMENT 'Default commision %',
  `created_by` varchar(25) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` varchar(25) NOT NULL,
  `modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`agent_seq`,`seq`),
  CONSTRAINT `fk_m_agent_m_agent_trx_commission` FOREIGN KEY (`agent_seq`) REFERENCES `m_agent` (`seq`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_agent_trx_commission_log`
--

DROP TABLE IF EXISTS `m_agent_trx_commission_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_agent_trx_commission_log` (
  `agent_seq` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `update_seq` int(11) DEFAULT NULL,
  `category_l1_seq` mediumint(8) unsigned NOT NULL,
  `category_l2_seq` mediumint(8) unsigned DEFAULT NULL,
  `trx_fee_percent` decimal(4,2) unsigned NOT NULL DEFAULT '0.00' COMMENT 'Default commision %',
  `created_by` varchar(25) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` varchar(25) NOT NULL,
  `modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `fk_m_agent_m_agent_trx_commission_log_idx` (`agent_seq`),
  CONSTRAINT `fk_m_agent_m_agent_trx_commission_log` FOREIGN KEY (`agent_seq`) REFERENCES `m_agent` (`seq`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
--
-- Table structure for table `m_partner`
--

DROP TABLE IF EXISTS `m_partner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_partner` (
  `seq` int(10) unsigned NOT NULL,
  `email` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `password` varchar(1000) NOT NULL,
  `mobile_phone` varchar(50) NOT NULL,
  `profile_img` varchar(50) NOT NULL,
  `status` enum('N','A','S','U') NOT NULL COMMENT 'Status merchant ( A : Authenticated, S : Suspended)',
  `bank_name` varchar(50) NOT NULL,
  `bank_branch_name` varchar(50) NOT NULL,
  `bank_acct_no` varchar(50) NOT NULL,
  `bank_acct_name` varchar(50) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `ip_address` varchar(50) DEFAULT NULL,
  `admin_fee` decimal(10,0) NOT NULL,
  `created_by` varchar(25) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` varchar(25) NOT NULL,
  `modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_partner_loan`
--

DROP TABLE IF EXISTS `m_partner_loan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_partner_loan` (
  `seq` int(10) unsigned NOT NULL,
  `product_category_seq` mediumint(8) unsigned NOT NULL,
  `partner_seq` int(10) unsigned NOT NULL,
  `tenor` tinyint(3) unsigned NOT NULL,
  `loan_interest` decimal(5,2) unsigned NOT NULL,
  `active` enum('0','1') NOT NULL,
  `created_by` varchar(25) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` varchar(25) NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`seq`),
  KEY `product_category_seq` (`product_category_seq`),
  KEY `partner_seq` (`partner_seq`),
  CONSTRAINT `m_partner_loan_ibfk_1` FOREIGN KEY (`product_category_seq`) REFERENCES `m_product_category` (`seq`) ON UPDATE CASCADE,
  CONSTRAINT `m_partner_loan_ibfk_2` FOREIGN KEY (`partner_seq`) REFERENCES `m_partner` (`seq`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_partner_loan_dp`
--

DROP TABLE IF EXISTS `m_partner_loan_dp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_partner_loan_dp` (
  `seq` int(10) unsigned NOT NULL,
  `partner_loan_seq` int(10) unsigned NOT NULL,
  `minimal_loan` decimal(10,0) unsigned NOT NULL COMMENT 'price of minimal customer loan',
  `maximal_loan` decimal(10,0) unsigned NOT NULL COMMENT 'price of maximal customer loan',
  `min_dp_percent` decimal(5,2) unsigned NOT NULL,
  `active` enum('0','1') NOT NULL,
  `created_by` varchar(25) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` varchar(25) NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`seq`),
  KEY `partner_loan_seq` (`partner_loan_seq`),
  CONSTRAINT `m_partner_loan_dp_ibfk_1` FOREIGN KEY (`partner_loan_seq`) REFERENCES `m_partner_loan` (`seq`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
--
-- Table structure for table `m_partner_log_security`
--

DROP TABLE IF EXISTS `m_partner_log_security`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_partner_log_security` (
  `partner_seq` int(10) unsigned NOT NULL,
  `seq` smallint(5) unsigned NOT NULL,
  `type` enum('1','2') NOT NULL DEFAULT '1' COMMENT 'Tipe Perubahan (1: Change Password, 2 : Forget Password )',
  `code` varchar(20) DEFAULT NULL,
  `old_pass` varchar(100) NOT NULL,
  `ip_addr` varchar(30) NOT NULL,
  `created_by` varchar(25) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`partner_seq`,`seq`),
  KEY `fk_m_partner_m_partner_log_security_idx` (`partner_seq`),
  CONSTRAINT `fk_m_partner_m_partner_log_security` FOREIGN KEY (`partner_seq`) REFERENCES `m_partner` (`seq`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_partner_login_log`
--

DROP TABLE IF EXISTS `m_partner_login_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_partner_login_log` (
  `seq` bigint(20) unsigned NOT NULL,
  `partner_seq` int(10) unsigned NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `status` enum('S','F') NOT NULL COMMENT 'S = Success, F= Fail',
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`seq`),
  KEY `fk_m_partner_m_partner_login_log_idx` (`partner_seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
--
-- Table structure for table `t_agent_account`
--

DROP TABLE IF EXISTS `t_agent_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_agent_account` (
  `agent_seq` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `mutation_type` enum('D','C','N') NOT NULL COMMENT 'D : Uang keluar, C : Uang masuk, N : Uang tidak ada, hanya keterangan saja',
  `pg_method_seq` smallint(5) unsigned NOT NULL,
  `trx_type` enum('CNL','WDW','ORD','RTR','TPU') NOT NULL COMMENT 'CNL : Batal Kirim dari Merchant, WDW : Agent Withdraw (Agent Tarik), ORD : Pembayaran Order, RTR : Retur Agent, TPU : Top up deposit',
  `trx_no` varchar(50) NOT NULL,
  `trx_date` date NOT NULL DEFAULT '0000-00-00',
  `deposit_trx_amt` decimal(10,0) NOT NULL DEFAULT '0' COMMENT 'Nilai uang transaksi',
  `non_deposit_trx_amt` decimal(10,0) NOT NULL DEFAULT '0' COMMENT 'Nilai belanja untuk dapat menggunakan vourcher',
  `bank_name` varchar(50) NOT NULL,
  `bank_branch_name` varchar(50) NOT NULL,
  `bank_acct_no` varchar(50) NOT NULL,
  `bank_acct_name` varchar(50) NOT NULL,
  `refund_date` date NOT NULL DEFAULT '0000-00-00',
  `conf_topup_date_agent` date NOT NULL DEFAULT '0000-00-00',
  `conf_topup_type_agent` enum('N','C','T') NOT NULL DEFAULT 'N' COMMENT 'Jenis Setoran = N : NA,  C : Cash, T : Transfer',
  `conf_topup_file_agent` varchar(50) NOT NULL COMMENT 'upload file topup',
  `conf_topup_date_admin` date NOT NULL DEFAULT '0000-00-00',
  `conf_topup_bank_seq` tinyint(3) unsigned DEFAULT NULL,
  `status` enum('W','N','A','R','T') NOT NULL COMMENT 'Status account (  W : Request from WEB, N : New, A : Approve, R : Reject, T: Request Top Up )',
  `created_by` varchar(25) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` varchar(25) NOT NULL,
  `modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`agent_seq`,`seq`),
  KEY `fk_m_bank_t_agent_account_idx` (`conf_topup_bank_seq`),
  CONSTRAINT `fk_m_bank_t_agent_account` FOREIGN KEY (`conf_topup_bank_seq`) REFERENCES `m_bank_account` (`seq`) ON UPDATE CASCADE,
  CONSTRAINT `fk_m_agent_t_agent_account` FOREIGN KEY (`agent_seq`) REFERENCES `m_agent` (`seq`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_order_loan`
--

DROP TABLE IF EXISTS `t_order_loan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_order_loan` (
  `order_seq` bigint(20) unsigned NOT NULL,
  `customer_seq` bigint(20) unsigned NOT NULL,
  `dp` decimal(10,0) unsigned NOT NULL,
  `admin_fee` decimal(10,0) unsigned DEFAULT NULL,
  `tenor` tinyint(3) unsigned DEFAULT NULL,
  `loan_interest` decimal(5,2) unsigned DEFAULT NULL,
  `installment` decimal(10,0) unsigned DEFAULT NULL,
  `total_installment` decimal(10,0) unsigned DEFAULT NULL,
  `status_order` enum('N','A','P','D','F','R') DEFAULT 'N' COMMENT 'N=Baru, A=Approved by Partner, P=Paid by Customer, F= Pembayaran full (apporval admin), D= Pembayaran full oleh partner, R= Reject',
  `auth_date` datetime DEFAULT NULL,
  `pg_method_seq` smallint(5) unsigned DEFAULT NULL,
  `po_no` varchar(200) NOT NULL DEFAULT '',
  `created_by` varchar(25) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` varchar(25) NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`order_seq`),
  KEY `customer_seq` (`customer_seq`),
  KEY `fk_m_payment_gateway_method_t_order_loan_idx` (`pg_method_seq`),
  CONSTRAINT `fk_m_payment_gateway_method_t_order_loan` FOREIGN KEY (`pg_method_seq`) REFERENCES `m_payment_gateway_method` (`seq`) ON UPDATE CASCADE,
  CONSTRAINT `t_order_loan_ibfk_1` FOREIGN KEY (`order_seq`) REFERENCES `t_order` (`seq`),
  CONSTRAINT `t_order_loan_ibfk_2` FOREIGN KEY (`customer_seq`) REFERENCES `m_agent_customer` (`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_redeem_agent`
--

DROP TABLE IF EXISTS `t_redeem_agent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_redeem_agent` (
  `redeem_seq` int(10) unsigned NOT NULL,
  `agent_seq` int(10) unsigned NOT NULL,
  `total` decimal(15,0) NOT NULL COMMENT 'sum(t_redeem_component.total) sesuai t_redeem_component.mutation_type',
  `status` enum('U','P','T') NOT NULL COMMENT 'U : Unpaid, T: Paid To Partner , P : Paid',
  `paid_date` date NOT NULL DEFAULT '0000-00-00',
  `paid_partner_date` date NOT NULL DEFAULT '0000-00-00',
  `created_by` varchar(25) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` varchar(25) NOT NULL,
  `modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`redeem_seq`,`agent_seq`),
  KEY `fk_m_agent_t_redeem_agent_idx` (`agent_seq`),
  CONSTRAINT `fk_m_agent_t_redeem_agent` FOREIGN KEY (`agent_seq`) REFERENCES `m_agent` (`seq`) ON UPDATE CASCADE,
  CONSTRAINT `fk_t_redeem_agent_period_t_redeem_agent` FOREIGN KEY (`redeem_seq`) REFERENCES `t_redeem_agent_period` (`seq`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_redeem_agent_component`
--

DROP TABLE IF EXISTS `t_redeem_agent_component`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_redeem_agent_component` (
  `redeem_seq` int(10) unsigned NOT NULL,
  `partner_seq` int(10) unsigned NOT NULL,
  `type` enum('ORD','CFA','RET') NOT NULL COMMENT 'ORD : Order, CFA : Commision Fee Agent, RET : Retur',
  `mutation_type` enum('D','C') NOT NULL COMMENT 'D : Kurang Utang, C : Tambah utang',
  `total` decimal(12,0) unsigned NOT NULL,
  `created_by` varchar(25) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` varchar(25) NOT NULL,
  `modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`redeem_seq`,`partner_seq`,`type`),
  KEY `fk_m_partner_t_redeem_agent_component_idx` (`partner_seq`),
  CONSTRAINT `fk_m_partner_t_redeem_agent_component` FOREIGN KEY (`partner_seq`) REFERENCES `m_partner` (`seq`) ON UPDATE CASCADE,
  CONSTRAINT `fk_t_redeem_agent_period_t_redeem_agent_component` FOREIGN KEY (`redeem_seq`) REFERENCES `t_redeem_agent_period` (`seq`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_redeem_agent_period`
--

DROP TABLE IF EXISTS `t_redeem_agent_period`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_redeem_agent_period` (
  `seq` int(10) unsigned NOT NULL,
  `from_date` date NOT NULL DEFAULT '0000-00-00',
  `to_date` date NOT NULL DEFAULT '0000-00-00',
  `status` enum('O','P','C') NOT NULL COMMENT 'O : Open Period, P : Process, C : Close Period',
  `created_by` varchar(25) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` varchar(25) NOT NULL,
  `modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-03-23 16:05:12


/*
==============================================
ADD FIELD TRUNK V.2.0.0 AGENT & PARTNER ADIRA
==============================================
*/
ALTER TABLE m_product_category ADD commission_fee_percent DECIMAL(4,2) NOT NULL DEFAULT 0.00 AFTER trx_fee_percent;
ALTER TABLE t_order MODIFY COLUMN member_seq INT(10) UNSIGNED NULL; -- 
ALTER TABLE t_order ADD agent_seq INT(10) AFTER member_seq; -- add agent_seq in t_order
ALTER TABLE t_order_product ADD commission_fee_percent DECIMAL(4,2) AFTER trx_fee_percent; -- For redeem calculation purpose
ALTER TABLE t_order_product ADD commission_fee_partner_percent DECIMAL(4,2) AFTER commission_fee_percent;
ALTER TABLE t_order MODIFY COLUMN payment_status ENUM('U','W','C','P','F','X','T','A'); -- add status payment ADIRA;
ALTER TABLE m_payment_gateway ADD logo_img Varchar(50) AFTER active;
ALTER TABLE m_promo_voucher ADD agent_seq INT(10) AFTER member_seq;

/*
==============================================
ADD DATA TRUNK V.2.0.0.0 AGENT & PARTNER ADIRA
==============================================
*/
REPLACE `m_email_template` (`code`,`subject`,`content`,`email_to`,`notes`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES ('AGENT_FORGOT_PASSWORD','Pengajuan \'Forgot Password\' Untuk Agen [RECIPIENT_NAME]','<div>Halo [RECIPIENT_NAME],</div><br>\r <div>Kami informasikan bahwa pengajuan \'Forgot Password\' dari anda sedang kami proses,<br>Klik <a href=\"[LINK]\">disini</a> untuk  mengkonfirmasi.\r Terima Kasih.','Agent','Pengajuan \'Forgot Password\' Agent','ADMIN','2016-11-29 00:00:00','ADMIN','2016-11-29 00:00:00');
REPLACE `m_email_template` (`code`,`subject`,`content`,`email_to`,`notes`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES ('AGENT_FORGOT_PASSWORD_VERIFICATION','Konfirmasi Ubah Password','<div>Halo [RECIPIENT_NAME]</div><br>\r <div>Password login anda telah diubah, <br>silahkan login sebagai agen dengan :<br> email <b>[EMAIL]</b> dan password sementara <b>[PASSWORD]</b>.<br>\r Segera login dan ubah password anda. Terima Kasih.','Agent','Konfirmasi Verifikasi Forgot Password Agen','admin','2016-11-29 00:00:00','admin','2016-11-29 00:00:00');
REPLACE `m_email_template` (`code`,`subject`,`content`,`email_to`,`notes`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES ('AGENT_REDEEM_INFO','Konfirmasi Pembayaran di [WEB_TITLE]','<div>Halo [RECIPIENT_NAME],</div><br>\r <div>Kami informasikan bahwa ada pembayaran invoice periode [PERIODE]. \r <br> Terima Kasih.','Agent','Konfirmasi Pembayaran Redeem per Periode','agus','2016-12-01 00:00:00','agus','2016-12-01 00:00:00');
REPLACE `m_email_template` (`code`,`subject`,`content`,`email_to`,`notes`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES ('AGENT_TOPUP_DEPOSIT','Konfirmasi Topup Deposit [TOPUP_NO] Telah dilakukan','<div>Halo [RECIPIENT_NAME],</div><br>\r\n<div>\r\nKonfirmasi topup dari [AGENT_NAME] untuk nomor topup [TOPUP_NO] sebesar [TOTAL_TRANSFER]<br/><br/> , Silakan Anda melakukan verifikasi atas pembayaran diatas, dan mengupdate status topup dari page Administrator.<br/><br/><br>Klik <a href=\"[LINK]\">disini</a> untuk verfikasi.','Admin','Konfirmasi Topup Telah ditransfer','agent','0000-00-00 00:00:00','agent','0000-00-00 00:00:00');
REPLACE `m_email_template` (`code`,`subject`,`content`,`email_to`,`notes`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES ('AGENT_TOPUP_DEPOSIT_APPROVE','Konfirmasi Topup Deposit [TOPUP_NO] Telah disetujui','<div>Halo [RECIPIENT_NAME],</div><br>\r\n<div>\r\nKonfirmasi topup dari [AGENT_NAME] untuk nomor topup [TOPUP_NO] sebesar [TOTAL_TRANSFER] telah kami setujui<br/><br/>. Terima kasih.','Agent','Konfirmasi Topup Telah disetujui','admin','0000-00-00 00:00:00','admin','0000-00-00 00:00:00');
REPLACE `m_email_template` (`code`,`subject`,`content`,`email_to`,`notes`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES ('AGENT_WITHDRAW','Pengajuan Tarik Deposit','<div>Halo [RECIPIENT_NAME],</div><br>\r\n <div>\r\n Penarikan deposit anda sebesar <strong>Rp. [TOTAL_DEPOSIT]<br/> Klik <a href=\"[LINK]\">disini</a> untuk verifikasi. </div><br>\r\n Terima Kasih.','Agent','Konfirmasi Penarikan Deposit','member','0000-00-00 00:00:00','member','0000-00-00 00:00:00');
REPLACE `m_email_template` (`code`,`subject`,`content`,`email_to`,`notes`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES ('APPROVE_ORDER_BY_PARTNER','Order Dengan Nomor Order [ORDER_NO] di [WEB_TITLE] telah disetujui !','<div>Halo [RECIPIENT_NAME],</div><br>\r\n <div>\r\n  Terima Kasih telah berbelanja  di [WEB_TITLE].<br/> Kami informasikan bahwa order Anda dengan nomor <strong>[ORDER_NO]</strong>, <br/>pada tanggal [ORDER_DATE] <strong>Telah di SETUJUI oleh Partner kami </strong>dengan rincian sebagai berikut :<br/><br/> \r\n     <strong>1. Info Produk</strong><br/>\r\n     [ORDER_ITEMS]\r\n     <br/><br/> <strong>2. Info Penerima</strong><br/>\r\n     [RECIPIENT_ADDRESS]\r\n     <br/><br/>\r\n     [INFO_BANK]\r\n     <br/><br/>\r\n      Silahkan lakukan pembayaran pertama Anda sebesar [TOTAL_INSTALLMENT] sebelum tanggal [EXPIRED_DATE].\r\n     [SMS]\r\n </div>\r\n ','Agent Customer','Pemberitahuan Persetujuan Order Oleh Partner','admin','2017-03-17 14:19:28','admin','2017-03-17 14:19:34');
REPLACE `m_email_template` (`code`,`subject`,`content`,`email_to`,`notes`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES ('ORDER_INFO_PARTNER','Nomor Order [ORDER_NO] di [WEB_TITLE]','<div>Halo [RECIPIENT_NAME],</div><br>\n <div>\n  Terima Kasih telah berbelanja  di [WEB_TITLE].<br/> Kami informasikan bahwa order anda dengan nomor <strong>[ORDER_NO]</strong>, <br/>pada tanggal [ORDER_DATE] dengan rincian sebagai berikut :<br/><br/> <strong>1. Info Produk</strong><br/>\n     [ORDER_ITEMS]\n     <br/><br/> <strong>2. Info Penerima</strong><br/>\n     [RECIPIENT_ADDRESS]\n     <br/><br/>\n     [INFO_BANK]\n     Silahkan klik <a href=[PAYMENT_LINK]>disini</a> untuk melihat status order. \n </div>\n ','Partner','Pemberitahuan Order Baru Member','admin','2016-12-01 13:06:50','admin','2016-12-01 13:06:50');
REPLACE `m_email_template` (`code`,`subject`,`content`,`email_to`,`notes`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES ('ORDER_NEW_PARTNER_TO_MERCHANT','Order Baru Dengan Nomor [ORDER_NO] di [WEB_TITLE]','<div>Halo [RECIPIENT_NAME],</div><br>\r <div>\r     Kami informasikan bahwa ada order baru dengan nomor <strong>[ORDER_NO]</strong> pada tanggal [ORDER_DATE] dengan rincian sebagai berikut :<br/><br/>\r     <strong>1. Info Produk</strong><br/>\r     [ORDER_ITEMS]\r     <br/><br/>\r     <strong>2. Info Penerima</strong><br/>\r     [RECIPIENT_ADDRESS]\r     <br/><br/>\r     Mohon barang tesebut dapat disimpan selama 4x24 jam dari hari ini untuk proses persetujuan.\r\n     Terima Kasih\r </div>\r \r ','Merchant','Pemberitahuan Order Baru untuk penahanan barang selama persetujuan pendanaan','admin','2016-12-01 13:06:50','admin','2016-12-01 13:06:50');
REPLACE `m_email_template` (`code`,`subject`,`content`,`email_to`,`notes`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES ('ORDER_NEW_PARTNER_TO_PARTNER','Pengajuan Kredit Baru Customer Dengan Nomor [ORDER_NO] di [WEB_TITLE]','<div>Halo [RECIPIENT_NAME]</div><br>\r\n <div>\r\n     Terimakasih telah bergabung menjadi partner TOKO1001.<br>\r\n     Kami informasikan bahwa ada pengajuan kredit baru untuk order dengan nomor <strong>[ORDER_NO]</strong> pada tanggal [ORDER_DATE] dengan rincian sebagai berikut :<br/><br/>\r\n     <strong>1. Info Produk</strong><br/>\r\n     [ORDER_ITEMS]\r\n     <br/><br/>\r\n     <strong>2. Info Penerima</strong><br/>\r\n     [RECIPIENT_ADDRESS]\r\n     <br/><br/>\r\n     Mohon segera proses pengajuan kredit tersebut sebelum [EXPIRED_DATE].\r\n     <br/>\r\n     Silahkan klik <a href=[CONFIRM_LINK]>disini</a> untuk memproses pengajuan.\r\n </div>\r\n \r\n ','Partner','Pemberitahuan Pengajuan Pinjaman Baru untuk Partner','admin','0000-00-00 00:00:00','admin','0000-00-00 00:00:00');
REPLACE `m_email_template` (`code`,`subject`,`content`,`email_to`,`notes`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES ('PARTNER_CANCEL_ORDER','Pembatalan Produk Dengan Nomor Order [ORDER_NO] di [WEB_TITLE]','<div>Halo [RECIPIENT_NAME],</div><br>\r <div> Kami informasikan bahwa order anda,<br> degan nomor <strong>[ORDER_NO]</strong> pada tanggal [ORDER_DATE] untuk produk dibawah ini :<br/><br/>\r     <strong>1. Info Produk</strong><br/>\r     [ORDER_ITEMS]\r     <br/><br/>\r     <strong>2. Info Penerima</strong><br/>\r     [RECIPIENT_ADDRESS]\r     <br/><br/>\r     <strong>3. Info Pengembalian Uang</strong><br/>\r     [REFUND_VALUE]\r     <br/><br/>\r     Produk tersebut di atas dibatalkan oleh Partner kami karena <strong>STOK TIDAK TERSEDIA</strong>.<br/><br/>\r     Jumlah sebesar <strong>Rp. [TOTAL_REFUND]</strong> akan dikembalikan kepada anda dengan ketentuan sebagai berikut:<br/>\r     1. Pembayaran menggunakan kartu kredit, akan dikembalikan ke kartu kredit anda.<br>\r     2. Pembayaran selain kartu kredit, akan di <strong>KREDIT ke DEPOSIT</strong> anda. <br>      3. Voucher yang sudah terpakai <strong>[OLD_VOUCHER]</strong> akan dibuatkan voucher pengganti <strong>[NEW_VOUCHER]</strong>. <br>Klik <a href=\"[VOUCHER_LINK]\">disini</a> untuk melihat voucher anda.\r     <br><br>\r     Kami mohon maaf sebesar-besarnya atas ketidaknyamanan ini.\r </div>','Partner','Pembatalan Order untuk barang tertentu oleh Partner','admin','2016-12-01 13:06:06','admin','2016-12-01 13:06:06');
REPLACE `m_email_template` (`code`,`subject`,`content`,`email_to`,`notes`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES ('PARTNER_FORGOT_PASSWORD','Pengajuan \'Forgot Password\' untuk partner [RECIPIENT_NAME]','<div>Halo [RECIPIENT_NAME],</div><br>\r <div>Kami informasikan bahwa pengajuan \'Forgot Password\' dari anda sedang kami proses, <br>Klik <a href=\"[LINK]\">disini</a> untuk  mengkonfirmasi.\r Terima Kasih.','Partner','Pengajuan \'Forgot Password\' Partner','admin','2016-12-01 13:06:06','admin','2016-12-01 13:06:06');
REPLACE `m_email_template` (`code`,`subject`,`content`,`email_to`,`notes`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES ('PARTNER_FORGOT_PASSWORD_VERIFICATION','Konfirmasi Ubah Password','<div>Halo [RECIPIENT_NAME]</div><br>\r <div>Password login anda telah diubah, <br>silahkan login sebagai partner dengan :<br> email <b>[EMAIL]</b> dan password sementara <b>[PASSWORD]</b> .<br>\r Segera login dan ubah password anda. Terima Kasih.','Partner','Konfirmasi Verifikasi Forgot Password Partner','admin','2016-12-01 13:06:06','admin','2016-12-01 13:06:06');
REPLACE `m_email_template` (`code`,`subject`,`content`,`email_to`,`notes`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES ('PARTNER_REDEEM_INFO','Konfirmasi Pembayaran di [WEB_TITLE]','<div>Halo [RECIPIENT_NAME],</div><br>\r <div>Kami informasikan bahwa ada pembayaran Redeem periode [PERIODE]. \r <br> Terima Kasih.','Partner','Konfirmasi Pembayaran Redeem per Periode','agus','2016-12-01 13:06:06','agus','2016-12-01 13:06:06');
REPLACE `m_email_template` (`code`,`subject`,`content`,`email_to`,`notes`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES ('REJECT_ORDER_BY_PARTNER','Penolakan Pengajuan Kredit Dengan Nomor Order [ORDER_NO] di [WEB_TITLE]','<div>Halo [RECIPIENT_NAME],</div><br>\r\n <div> Kami informasikan bahwa produk dibawah ini :<br/><br/>\r\n     <strong>Info Produk</strong><br/>\r\n     [ORDER_ITEMS]\r\n     <br/><br/>\r\n     Produk tersebut sudah dapat digunakan kembali,<br/>\r\n     dikarenakan pengajuan kredit tersebut <strong>DITOLAK</strong> oleh Partner kami.\r\n     <br><br>\r\n     Kami mohon maaf sebesar-besarnya atas ketidaknyamanan ini.\r\n </div>','Merchant','Pemberitahuan Pembatalan Order Oleh Partner','admin','2017-03-09 15:45:45','admin','2017-03-09 15:45:53');
REPLACE `m_email_template` (`code`,`subject`,`content`,`email_to`,`notes`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES ('REJECT_ORDER_BY_PARTNER_TO_AGENT_AND_CUSTOMER','Penolakan Pengajuan Kredit Dengan Nomor Order [ORDER_NO] di [WEB_TITLE]','<div>Halo [RECIPIENT_NAME],</div><br>\r\n <div> Kami informasikan bahwa pengajuan kredit dengan nomor order <strong>[ORDER_NO]</strong> pada tanggal [ORDER_DATE] untuk produk dibawah ini :<br/><br/>\r\n     <strong>1. Info Produk</strong><br/>\r\n     [ORDER_ITEMS]\r\n     <br/><br/>\r\n     <strong>2. Info Penerima</strong><br/>\r\n     [RECIPIENT_ADDRESS]\r\n     <br/><br/>\r\n     Pengajuan Kredit tersebut telah <strong>DITOLAK</strong> oleh partner kami.<br/><br/>\r\n     Kami mohon maaf sebesar-besarnya atas ketidaknyamanan ini.\r\n </div>','Agent','Pemberitahuan Penolakan Pengajuan Kredit Oleh Partner','admin','2017-03-21 19:12:28','admin','2017-03-21 19:12:33');

-- Insert user_group PARTNER
REPLACE `m_user_group` (`seq`,`name`,`active`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (7,'PARTNER','1','w','2016-02-02 13:20:28','w','2016-02-02 13:20:28');

-- insert s_menu_module PARTNER
REPLACE `s_menu_module` (`seq`,`name`,`active`,`created_by`,`created_date`) VALUES (7,'PARTNER','1','admin','2016-12-01 14:48:54');

-- Insert s_menu PARTNER
REPLACE `s_menu` (`module_seq`,`menu_cd`,`parent_menu_cd`,`name`,`title_name`,`url`,`order`,`icon`,`detail`,`level`,`active`,`created_by`,`created_date`) VALUES (7,'PMST00002','PMST','AGENT DATA PARTNER','Data Agent','partner/agent_data',1,'fa-plus','0',2,'1','w','2016-12-06 14:20:47');
REPLACE `s_menu` (`module_seq`,`menu_cd`,`parent_menu_cd`,`name`,`title_name`,`url`,`order`,`icon`,`detail`,`level`,`active`,`created_by`,`created_date`) VALUES (7,'PTRX00001','PTRX','AGENT ORDER PARTNER','Data Order Agent','partner/agent_order',8,'fa-files-o','0',2,'1','a','2015-11-18 10:58:43');
REPLACE `s_menu` (`module_seq`,`menu_cd`,`parent_menu_cd`,`name`,`title_name`,`url`,`order`,`icon`,`detail`,`level`,`active`,`created_by`,`created_date`) VALUES (7,'PCON00001','PCON','CHANGE PASSWORD PARTNER','Password','partner/change_password',2,'fa-dot-circle-o','1',2,'1','a','2015-11-18 13:35:32');
REPLACE `s_menu` (`module_seq`,`menu_cd`,`parent_menu_cd`,`name`,`title_name`,`url`,`order`,`icon`,`detail`,`level`,`active`,`created_by`,`created_date`) VALUES (7,'PCON00002','PCON','CHANGE PICTURE PARTNER','Gambar Profil','partner/change_picture',3,'fa-folder-o','0',2,'0','SYSTEM','2015-11-17 10:18:22');
REPLACE `s_menu` (`module_seq`,`menu_cd`,`parent_menu_cd`,`name`,`title_name`,`url`,`order`,`icon`,`detail`,`level`,`active`,`created_by`,`created_date`) VALUES (7,'PCON',NULL,'CONFIG PARTNER','Konfigurasi','',1,'fa-folder-o','0',1,'0','2016-12-26 11:05:15','2016-12-26 11:05:15');
REPLACE `s_menu` (`module_seq`,`menu_cd`,`parent_menu_cd`,`name`,`title_name`,`url`,`order`,`icon`,`detail`,`level`,`active`,`created_by`,`created_date`) VALUES (7,'PMST00003','PMST','CUSTOMER AGENT DATA PARTNER','Data Customer Agent','partner/customer_agent_data',2,'fa-plus','0',2,'1','w','2017-03-09 11:22:23');
REPLACE `s_menu` (`module_seq`,`menu_cd`,`parent_menu_cd`,`name`,`title_name`,`url`,`order`,`icon`,`detail`,`level`,`active`,`created_by`,`created_date`) VALUES (7,'PCST',NULL,'CUSTOMER LOAN','Customer Loan','',10,'fa-folder-o','0',1,'1','w','2017-03-06 10:20:48');
REPLACE `s_menu` (`module_seq`,`menu_cd`,`parent_menu_cd`,`name`,`title_name`,`url`,`order`,`icon`,`detail`,`level`,`active`,`created_by`,`created_date`) VALUES (7,'PMST',NULL,'MASTER PARTNER','Master','',5,'fa-folder-o','0',1,'1','2016-12-26 11:05:15','2016-12-26 11:05:15');
REPLACE `s_menu` (`module_seq`,`menu_cd`,`parent_menu_cd`,`name`,`title_name`,`url`,`order`,`icon`,`detail`,`level`,`active`,`created_by`,`created_date`) VALUES (7,'PCST00001','PCST','PARTNER LOAN','Partner Loan','partner/partner_loan',11,'fa-files-o','0',2,'1','w','2017-03-06 10:30:31');
REPLACE `s_menu` (`module_seq`,`menu_cd`,`parent_menu_cd`,`name`,`title_name`,`url`,`order`,`icon`,`detail`,`level`,`active`,`created_by`,`created_date`) VALUES (7,'PCST00002','PCST','PARTNER_DOWN_PAYMENT','Partner Down Payment','partner/partner_down_payment',12,'fa-files-','0',2,'1','w','2017-03-06 10:31:49');
REPLACE `s_menu` (`module_seq`,`menu_cd`,`parent_menu_cd`,`name`,`title_name`,`url`,`order`,`icon`,`detail`,`level`,`active`,`created_by`,`created_date`) VALUES (7,'PCON00003','PCON','PROFILE PARTNER','Profil Partner','partner/profile',4,'fa-folder-o','0',2,'0','SYSTEM','2015-11-17 10:18:22');
REPLACE `s_menu` (`module_seq`,`menu_cd`,`parent_menu_cd`,`name`,`title_name`,`url`,`order`,`icon`,`detail`,`level`,`active`,`created_by`,`created_date`) VALUES (7,'PTRX00003','PTRX','REDEEM DETAIL PARTNER','Redeem Data detail Partner','partner/redeem_detail_partner',10,'fa-files-o','1',2,'1','2017-01-16 12:40:38','2017-01-16 12:40:38');
REPLACE `s_menu` (`module_seq`,`menu_cd`,`parent_menu_cd`,`name`,`title_name`,`url`,`order`,`icon`,`detail`,`level`,`active`,`created_by`,`created_date`) VALUES (7,'PTRX00002','PTRX','REDEEM PARTNER','Redeem Data Partner','partner/redeem_partner',9,'fa-files-o','0',2,'1','w','2017-01-12 11:18:28');
REPLACE `s_menu` (`module_seq`,`menu_cd`,`parent_menu_cd`,`name`,`title_name`,`url`,`order`,`icon`,`detail`,`level`,`active`,`created_by`,`created_date`) VALUES (7,'PTRX',NULL,'TRANSACTION PARTNER','Transaksi','',7,'fa-folder-o','0',1,'1','2016-12-26 11:05:15','2016-12-26 11:05:15');

-- insert m_user_group_permission PARTNER
REPLACE `m_user_group_permission` (`user_group_seq`,`menu_cd`,`can_view`,`can_add`,`can_edit`,`can_delete`,`can_auth`,`can_print`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (7,'PCON','1','1','1','1','1','1','w','2015-11-18 10:36:45','2016-12-26 11:10:19','2016-12-26 11:10:19');
REPLACE `m_user_group_permission` (`user_group_seq`,`menu_cd`,`can_view`,`can_add`,`can_edit`,`can_delete`,`can_auth`,`can_print`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (7,'PCON00001','1','1','1','1','1','1','w','2015-11-18 13:40:28','a','2015-11-18 13:40:28');
REPLACE `m_user_group_permission` (`user_group_seq`,`menu_cd`,`can_view`,`can_add`,`can_edit`,`can_delete`,`can_auth`,`can_print`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (7,'PCON00002','1','1','1','1','1','1','w','2016-02-01 11:47:48','SYSTEM','2016-02-01 11:47:48');
REPLACE `m_user_group_permission` (`user_group_seq`,`menu_cd`,`can_view`,`can_add`,`can_edit`,`can_delete`,`can_auth`,`can_print`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (7,'PCON00003','1','1','1','1','1','1','w','2016-09-21 10:37:16','SYSTEM','2016-09-21 10:37:16');
REPLACE `m_user_group_permission` (`user_group_seq`,`menu_cd`,`can_view`,`can_add`,`can_edit`,`can_delete`,`can_auth`,`can_print`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (7,'PCST','1','1','1','1','1','1','w','2017-03-06 10:57:18','w','2017-03-06 10:57:26');
REPLACE `m_user_group_permission` (`user_group_seq`,`menu_cd`,`can_view`,`can_add`,`can_edit`,`can_delete`,`can_auth`,`can_print`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (7,'PCST00001','1','1','1','1','0','0','w','2017-03-06 10:38:10','w','2017-03-06 10:38:10');
REPLACE `m_user_group_permission` (`user_group_seq`,`menu_cd`,`can_view`,`can_add`,`can_edit`,`can_delete`,`can_auth`,`can_print`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (7,'PCST00002','1','1','1','1','0','0','w','2017-03-06 10:38:25','w','2017-03-06 10:38:25');
REPLACE `m_user_group_permission` (`user_group_seq`,`menu_cd`,`can_view`,`can_add`,`can_edit`,`can_delete`,`can_auth`,`can_print`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (7,'PMST','1','1','1','1','1','1','w','2015-11-17 15:33:52','2016-12-26 11:10:19','2016-12-26 11:10:19');
REPLACE `m_user_group_permission` (`user_group_seq`,`menu_cd`,`can_view`,`can_add`,`can_edit`,`can_delete`,`can_auth`,`can_print`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (7,'PMST00002','0','1','1','0','0','0','w','2016-12-06 14:23:18','w','2016-12-06 14:23:18');
REPLACE `m_user_group_permission` (`user_group_seq`,`menu_cd`,`can_view`,`can_add`,`can_edit`,`can_delete`,`can_auth`,`can_print`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (7,'PMST00003','1','0','0','0','0','0','w','2017-03-09 11:23:24','w','2017-03-09 11:23:24');
REPLACE `m_user_group_permission` (`user_group_seq`,`menu_cd`,`can_view`,`can_add`,`can_edit`,`can_delete`,`can_auth`,`can_print`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (7,'PTRX','1','1','1','1','1','1','w','2015-11-17 15:33:59','2016-12-26 11:10:19','2016-12-26 11:10:19');
REPLACE `m_user_group_permission` (`user_group_seq`,`menu_cd`,`can_view`,`can_add`,`can_edit`,`can_delete`,`can_auth`,`can_print`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (7,'PTRX00001','1','0','1','0','0','1','w','2015-11-18 11:00:38','a','2015-11-18 11:00:38');
REPLACE `m_user_group_permission` (`user_group_seq`,`menu_cd`,`can_view`,`can_add`,`can_edit`,`can_delete`,`can_auth`,`can_print`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (7,'PTRX00002','1','0','0','0','0','0','w','2017-01-12 11:29:43','w','2017-01-12 11:29:49');
REPLACE `m_user_group_permission` (`user_group_seq`,`menu_cd`,`can_view`,`can_add`,`can_edit`,`can_delete`,`can_auth`,`can_print`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (7,'PTRX00003','1','0','0','0','0','0','w','2017-01-16 15:01:39','w','2017-01-16 15:01:45');

-- insert m_payment_gateway
REPLACE `m_payment_gateway` (`seq`,`name`,`order`,`notes`,`active`,`logo_img`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (7,'ADIRA',9,'ADIRA','1','adira_payment_img.jpg','agus','2016-12-07 09:32:40','agus','2016-12-07 09:36:11');

-- insert m_payment_gateway_method
REPLACE `m_payment_gateway_method` (`pg_seq`,`seq`,`code`,`name`,`logo_img`,`order`,`notes`,`active`,`cancel_pg_method_seq`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (7,13,'ADIRATB','ADIRA','TMP_new_adira.jpg',2,'ADIRA','1',NULL,'w','2017-03-01 11:37:24','w','2017-03-01 11:37:33');

-- update m_payment_gateway image
REPLACE `m_payment_gateway_method` (`pg_seq`,`seq`,`code`,`name`,`logo_img`,`order`,`notes`,`active`,`cancel_pg_method_seq`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (1,1,'BNK','Via Bank','TMP_VIA_BANK.jpg',1,'-','1',NULL,'w','2015-10-15 16:58:57','finwe','2016-01-16 14:36:40');
REPLACE `m_payment_gateway_method` (`pg_seq`,`seq`,`code`,`name`,`logo_img`,`order`,`notes`,`active`,`cancel_pg_method_seq`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (2,2,'DPT','Deposit Member','TMP_PG_DPT.png',1,'-','1',NULL,'w','2015-10-15 17:01:04','finwe','2016-01-16 14:39:28');
REPLACE `m_payment_gateway_method` (`pg_seq`,`seq`,`code`,`name`,`logo_img`,`order`,`notes`,`active`,`cancel_pg_method_seq`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (3,3,'CC','Visa / Master / JCB','TMP_PG_CC.png',1,'-','1',NULL,'w','2015-10-15 17:01:33','finwe','2016-01-16 14:36:25');
REPLACE `m_payment_gateway_method` (`pg_seq`,`seq`,`code`,`name`,`logo_img`,`order`,`notes`,`active`,`cancel_pg_method_seq`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (4,4,'BKP','BCA Klikpay','bca_klikpay_payment_img.jpg',1,'-','1',NULL,'w','2015-10-15 17:02:13','finwe','2016-01-16 14:37:17');
REPLACE `m_payment_gateway_method` (`pg_seq`,`seq`,`code`,`name`,`logo_img`,`order`,`notes`,`active`,`cancel_pg_method_seq`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (4,5,'MKP','Mandiri Clickpay','mandiri-clickpay.png',2,'-','1',NULL,'w','2015-10-15 17:02:32','finwe','2016-01-16 14:37:50');
REPLACE `m_payment_gateway_method` (`pg_seq`,`seq`,`code`,`name`,`logo_img`,`order`,`notes`,`active`,`cancel_pg_method_seq`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (5,6,'MEC','Mandiri e-cash','mandiriecash.png',2,'-','1',NULL,'w','2015-10-15 17:02:32','finwe','2016-01-16 14:39:13');
REPLACE `m_payment_gateway_method` (`pg_seq`,`seq`,`code`,`name`,`logo_img`,`order`,`notes`,`active`,`cancel_pg_method_seq`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (4,7,'ALF','ALFAMARTKU','logo-groupalfa.png',2,'-','1',NULL,'w','2015-10-15 17:02:32','w','2015-10-15 17:02:32');
REPLACE `m_payment_gateway_method` (`pg_seq`,`seq`,`code`,`name`,`logo_img`,`order`,`notes`,`active`,`cancel_pg_method_seq`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (1,8,'ATM','TRANSFER VIA ATM BERSAMA','TMP_PG_ATM.png',2,'-','1',NULL,'w','2015-10-15 16:58:57','w','2015-10-15 16:58:57');
REPLACE `m_payment_gateway_method` (`pg_seq`,`seq`,`code`,`name`,`logo_img`,`order`,`notes`,`active`,`cancel_pg_method_seq`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (6,9,'QR','PAY BY QR','TMP_PG_QR.jpg',8,'-','1',NULL,'w','2016-10-25 16:19:34','w','0000-00-00 00:00:00');
REPLACE `m_payment_gateway_method` (`pg_seq`,`seq`,`code`,`name`,`logo_img`,`order`,`notes`,`active`,`cancel_pg_method_seq`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (9,12,'CRE','CICILAN','cicilan.png',1,'Cicilan','1',NULL,'agus','2016-12-26 17:02:39','agus','2016-12-26 17:02:43');
REPLACE `m_payment_gateway_method` (`pg_seq`,`seq`,`code`,`name`,`logo_img`,`order`,`notes`,`active`,`cancel_pg_method_seq`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (7,13,'ADIRATB','ADIRA','TMP_new_adira.jpg',2,'ADIRA','1',NULL,'w','2017-03-01 11:37:24','w','2017-03-01 11:37:33');

-- insert s_menu 
REPLACE `s_menu` (`module_seq`,`menu_cd`,`parent_menu_cd`,`name`,`title_name`,`url`,`order`,`icon`,`detail`,`level`,`active`,`created_by`,`created_date`) VALUES (1,'MST09000','MST','MASTER AGENT','Agen','',9,'fa-folder-o','0',2,'1','agus','2016-11-24 13:36:24');
REPLACE `s_menu` (`module_seq`,`menu_cd`,`parent_menu_cd`,`name`,`title_name`,`url`,`order`,`icon`,`detail`,`level`,`active`,`created_by`,`created_date`) VALUES (1,'MST09001','MST09000','MASTER AGENT DATA','Data Agen','admin/master/agent',1,'fa-dot-circle-o','0',3,'0','agus','2016-11-24 13:38:32');
REPLACE `s_menu` (`module_seq`,`menu_cd`,`parent_menu_cd`,`name`,`title_name`,`url`,`order`,`icon`,`detail`,`level`,`active`,`created_by`,`created_date`) VALUES (1,'MST09002','MST09000','TOP UP DEPOSIT','Deposit','admin/master/agent_top_up',2,'fa-dot-circle-o','0',3,'1','agus','2016-11-24 13:42:36');
REPLACE `s_menu` (`module_seq`,`menu_cd`,`parent_menu_cd`,`name`,`title_name`,`url`,`order`,`icon`,`detail`,`level`,`active`,`created_by`,`created_date`) VALUES (1,'MST10000','MST','MASTER PARTNER','Partner','',10,'fa-folder-o','0',2,'1','w','2016-12-05 14:54:34');
REPLACE `s_menu` (`module_seq`,`menu_cd`,`parent_menu_cd`,`name`,`title_name`,`url`,`order`,`icon`,`detail`,`level`,`active`,`created_by`,`created_date`) VALUES (1,'MST10001','MST10000','PARTNER_LIVE','Partner','admin/master/partner_live',1,'fa-dot-circle-o','0',3,'1','w','2016-12-05 15:30:20');
REPLACE `s_menu` (`module_seq`,`menu_cd`,`parent_menu_cd`,`name`,`title_name`,`url`,`order`,`icon`,`detail`,`level`,`active`,`created_by`,`created_date`) VALUES (1,'MST77777','MST05000','PROMO COMMISSION','promo komisi merchant','admin/master/promo_commission',11,'fa-dot-circle-o','0',3,'1','jaka','2016-11-24 13:42:36');
REPLACE `s_menu` (`module_seq`,`menu_cd`,`parent_menu_cd`,`name`,`title_name`,`url`,`order`,`icon`,`detail`,`level`,`active`,`created_by`,`created_date`) VALUES (1,'MST77778','MST05000','PROMO COMMISSION DETAIL','promo komisi merchant detail','admin/master/promo_commission_detail/',12,'fa-dot-circle-o','1',3,'1','jaka','2016-11-24 13:42:36');
REPLACE `s_menu` (`module_seq`,`menu_cd`,`parent_menu_cd`,`name`,`title_name`,`url`,`order`,`icon`,`detail`,`level`,`active`,`created_by`,`created_date`) VALUES (1,'TRX04003','TRX','REDEEM AGENT','Redeem Agen','admin/transaction/redeem_agent',9,'fa-dot-circle-o','0',2,'1','agus','2016-12-01 11:09:33');
REPLACE `s_menu` (`module_seq`,`menu_cd`,`parent_menu_cd`,`name`,`title_name`,`url`,`order`,`icon`,`detail`,`level`,`active`,`created_by`,`created_date`) VALUES (1,'TRX04004','TRX','REDEEM PERIOD AGENT','Periode Redeem Agen','admin/transaction/redeem_period_agent',10,'fa-dot-circle-o','1',2,'1','agus','2016-12-01 11:21:03');

-- insert m_user_group_permission
REPLACE `m_user_group_permission` (`user_group_seq`,`menu_cd`,`can_view`,`can_add`,`can_edit`,`can_delete`,`can_auth`,`can_print`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (2,'MST09000','1','0','0','0','0','0','agus','2016-11-24 13:46:22','agus','2016-11-24 13:46:22');
REPLACE `m_user_group_permission` (`user_group_seq`,`menu_cd`,`can_view`,`can_add`,`can_edit`,`can_delete`,`can_auth`,`can_print`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (2,'MST09001','1','1','1','1','1','0','agus','2016-11-24 13:40:01','agus','2016-11-24 13:40:01');
REPLACE `m_user_group_permission` (`user_group_seq`,`menu_cd`,`can_view`,`can_add`,`can_edit`,`can_delete`,`can_auth`,`can_print`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (2,'MST09002','1','1','1','1','1','1','agus','2016-11-24 13:48:18','agus','2016-11-24 13:48:18');
REPLACE `m_user_group_permission` (`user_group_seq`,`menu_cd`,`can_view`,`can_add`,`can_edit`,`can_delete`,`can_auth`,`can_print`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (2,'MST10000','1','0','0','0','0','0','w','2016-12-05 14:59:13','w','2016-12-05 14:59:13');
REPLACE `m_user_group_permission` (`user_group_seq`,`menu_cd`,`can_view`,`can_add`,`can_edit`,`can_delete`,`can_auth`,`can_print`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (2,'MST10001','1','1','1','1','0','0','w','2016-12-05 15:52:21','w','2016-12-05 15:52:21');
REPLACE `m_user_group_permission` (`user_group_seq`,`menu_cd`,`can_view`,`can_add`,`can_edit`,`can_delete`,`can_auth`,`can_print`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (2,'MST77777','1','1','1','1','1','1','jaka','2015-11-18 11:00:38','jaka','2015-11-18 11:00:38');
REPLACE `m_user_group_permission` (`user_group_seq`,`menu_cd`,`can_view`,`can_add`,`can_edit`,`can_delete`,`can_auth`,`can_print`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (2,'MST77778','1','1','1','1','1','1','jaka','2015-11-18 11:00:38','jaka','2015-11-18 11:00:38');
REPLACE `m_user_group_permission` (`user_group_seq`,`menu_cd`,`can_view`,`can_add`,`can_edit`,`can_delete`,`can_auth`,`can_print`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (2,'TRX04003','1','1','1','1','0','0','agus','2016-12-01 11:24:13','agus','2016-12-01 11:24:13');
REPLACE `m_user_group_permission` (`user_group_seq`,`menu_cd`,`can_view`,`can_add`,`can_edit`,`can_delete`,`can_auth`,`can_print`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (2,'TRX04004','1','0','1','0','0','0','agus','2016-12-01 11:24:42','agus','2016-12-01 11:24:42');
REPLACE `m_user_group_permission` (`user_group_seq`,`menu_cd`,`can_view`,`can_add`,`can_edit`,`can_delete`,`can_auth`,`can_print`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES (2,'TRX07002','0','0','1','0','0','0','a','2016-02-02 11:07:55','a','2016-02-02 11:07:55');
REPLACE `m_email_template` (`code`,`subject`,`content`,`email_to`,`notes`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES ('ORDER_RETURN_MERCHANT_REFUND','Pengembalian barang dengan nomor [TRX_NO] di [WEB_TITLE]','<div>Halo [RECIPIENT_NAME],</div><br><div> Mohon maaf pengembalian barang dengan nomor <strong>[TRX_NO]</strong> telah dibatalkan oleh Merchant dan <br>telah diajukan sebagai pengembalian dana pada tanggal <strong>[MODIFIED_DATE]</strong> senilai <strong>Rp. [TOTAL]</strong>. <br>Klik <a href=\"[ACCOUNT_LINK]\">disini</a> untuk cek mutasi akun anda.','Member','Pemberitahuan Pengembalian Barang diganti Pengembalian Dana oleh Merchant','admin','0000-00-00 00:00:00','admin','0000-00-00 00:00:00');
/*
=======================================================
STORED PROCEDURE TRUNK V.2.0.0.0 AGENT & PARTNER ADIRA
=======================================================
*/

/*!50003 DROP PROCEDURE IF EXISTS sp_admin_topup_list*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_admin_topup_list`(
        `pUserID` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pCurrPage` INTEGER UNSIGNED,
        `pRecPerPage` INTEGER UNSIGNED,
        `pDirSort` VARCHAR(4),
        `pColumnSort` VARCHAR(50),
        `pagentEmail` VARCHAR(100),
        `pStatus` CHAR(1)
    ) 
BEGIN
    -- Begin variables for Paging
    Declare startRec Int unsigned;
    Declare totalPage Int unsigned;
    Declare totalRec Int unsigned;
    -- End variables for Paging
    -- Begin SQL Where
    Declare sqlWhere VarChar(500);
	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
      
    If pagentEmail <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And m.seq = '" , pagentEmail , "'");
    End If;
    
    If pStatus <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And ma.status = '" , pStatus, "'");
    End If;
	
    -- End SQL Where
    -- Begin Paging Info
    Set @sqlCommand = "Select Count(ma.seq) Into @totalRec From t_agent_account ma 
														   Join m_agent m On ma.agent_seq = m.seq
                                                           ";
    
    Set @sqlCommand = Concat(@sqlCommand, " Where ma.trx_type ='" ,'TPU', "'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);
        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;
        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info
    Set @sqlCommand = "
        Select
			ma.seq,
			ma.trx_no,
            ma.trx_date,
            m.seq as agent_seq,
            m.email as agent_email,
            m.deposit_amt,
            ma.deposit_trx_amt,
            ma.non_deposit_trx_amt,
            ma.bank_name,
            ma.bank_acct_no,
            ma.bank_acct_name,
            ma.refund_date,
            ma.status
		From t_agent_account ma
		Join m_agent m
			On ma.agent_seq = m.seq
    ";
    Set @sqlCommand = Concat(@sqlCommand, " Where ma.trx_type ='" ,'TPU', "'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    
    Set @sqlCommand = Concat(@sqlCommand, " order by ", "", pColumnSort, " ", pDirSort);
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    Select pCurrPage, totalPage, totalRec as total_rec;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set @totalRec = Null;
    Set @sqlCommand = Null;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_admin_topup_list_by_agent_seq*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_admin_topup_list_by_agent_seq`(
        `pUserID` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pSeq` BIGINT UNSIGNED,
        `pagentSeq` BIGINT UNSIGNED
    ) 
BEGIN
	Select
		ma.seq,
		ma.trx_no,
		ma.trx_date,
		m.seq as agent_seq,
		m.email as agent_email,
		m.deposit_amt,
		m.name as agent_name,
		ma.deposit_trx_amt,
		ma.non_deposit_trx_amt,
		ma.bank_name,
		ma.bank_acct_no,
		ma.bank_acct_name,
		ma.refund_date,
		ma.status,
		ma.`conf_topup_date_agent`,
		ma.`conf_topup_type_agent`,
		ma.`conf_topup_file_agent`,
		ma.`conf_topup_date_admin`,
		ma.`pg_method_seq`,
		pg.`name` AS pg_name,
		b.logo_img,
		b.bank_acct_no,
		b.bank_acct_name		
	From t_agent_account ma
	Join m_agent m
		On ma.agent_seq = m.seq
	LEFT JOIN m_bank_account b ON
		ma.conf_topup_bank_seq = b.seq
	LEFT JOIN m_payment_gateway_method pg ON
		ma.pg_method_seq = pg.seq		
		where ma.seq=pSeq and ma.agent_seq=pagentSeq;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_agent_account_add*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_agent_account_add`(
        `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pagentSeq` BIGINT UNSIGNED,
        `pMutationType` VARCHAR(5),
        `pTrxType` VARCHAR(5),
        `pPaymentMethodSeq` TINYINT UNSIGNED,
        `pTrxNo` VARCHAR(25),
        `pDepositTrxAmt` DECIMAL(10,0),
        `pNonDepositTrxAmt` DECIMAL(10,0),
        `pBankName` VARCHAR(50),
        `pBankBranchName` VARCHAR(50),
        `pBankAcctNo` VARCHAR(50),
        `pBankAcctName` VARCHAR(50),
        `pStatus` VARCHAR(5),
        `pConfTopupDate` date,
        `pConfTopupType` VARCHAR(1),
        `pConfTopupFile` varchar(50),
        `pConfTopupBankSeq` tinyint(3)
    ) 
BEGIN
Declare new_seq int unsigned;
	
	Select 
		Max(seq) + 1 Into new_seq
	From t_agent_account 
    where
		agent_seq = pagentSeq;
	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
    
    if pConfTopupBankSeq = 0 Then Set pConfTopupBankSeq = null ;
    end if;
    
	INSERT INTO `t_agent_account`
            (`agent_seq`,
             `seq`,
             `mutation_type`,
             `pg_method_seq`,
             `trx_type`,
             `trx_no`,
             `trx_date`,
             `deposit_trx_amt`,
             `non_deposit_trx_amt`,
             `bank_name`,
             `bank_branch_name`,
             `bank_acct_no`,
             `bank_acct_name`,
             `refund_date`,
             `conf_topup_date_agent`,
             `conf_topup_type_agent`,
             `conf_topup_file_agent`,
             `conf_topup_bank_seq`,
             `status`,
             `created_by`,
             `created_date`,
             `modified_by`,
             `modified_date`)
             
	Value (
	pagentSeq,
        new_seq,
        pMutationType,
        pPaymentMethodSeq,
        pTrxType,
        pTrxNo,
        CURRENT_DATE(),
        pDepositTrxAmt,
        pNonDepositTrxAmt,
	pBankName,
        pBankBranchName,
        pBankAcctNo,
        pBankAcctName,
        '0000-00-00 00:00:00',
        pConfTopupDate,
        pConfTopupType,
        pConfTopupFile,
        pConfTopupBankSeq,
        pStatus,
        pagentSeq,
        now(),
        pagentSeq,
        now()
	);
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_agent_account_by_trx_no*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_agent_account_by_trx_no`(
	pTrxNo varchar(20)
) 
BEGIN
select 
     trx_no,
    `status`
from t_agent_account
where trx_no = pTrxNo;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_agent_account_list*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_agent_account_list`(
        `pIPAddr` VARCHAR(50),
        `pCurrPage` INTEGER UNSIGNED,
        `pRecPerPage` INTEGER UNSIGNED,
        `pDirSort` VARCHAR(4),
        `pColumnSort` VARCHAR(50),
        `pagentSeq` BIGINT(20)
    ) 
BEGIN
    -- Begin variables for Paging
    Declare startRec Int unsigned;
    Declare totalPage Int unsigned;
    Declare totalRec Int unsigned;
    -- End variables for Paging
    -- Begin SQL Where
    Declare sqlWhere VarChar(500);
	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
    -- End SQL Where
    -- Begin Paging Info
    Set @sqlCommand = "
	Select 
		Count(t.agent_seq) Into @totalRec 
	From t_agent_account t
	Join(
		Select @running_total:=0
	) r  ";
    Set @sqlCommand = Concat(@sqlCommand, " Where t.agent_seq ='" ,pagentSeq, "'");
	
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);
        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;
        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info
    Set @sqlCommand = "
	Select
		t.agent_seq,
		t.seq,
		t.mutation_type,
		t.pg_method_seq,
		t.trx_type,
		t.trx_no,
		t.trx_date,
		t.deposit_trx_amt,
		t.non_deposit_trx_amt,
		t.bank_name,
		t.bank_branch_name,
		t.bank_acct_no,
		t.bank_acct_name,
		t.refund_date,
		t.`status`,
		Case
			When trx_type In ('WDW','ORD') Then deposit_trx_amt * 1
			Else 0
		End As deposit_Keluar,
		Case
			when trx_type IN ('CNL','TPU') then deposit_trx_amt * 1
            Else 0
		End As deposit_Masuk,
		Sum(
			Case
				When mutation_type = 'C' and status = 'A' Then @running_total:=@running_total + t.deposit_trx_amt
                When mutation_type = 'N' and status = 'A' Then @running_total:=@running_total + t.deposit_trx_amt
				When mutation_type = 'D' and status = 'A' Then @running_total:=@running_total - t.deposit_trx_amt
			End
		) As saldo,
		Case 
			When (t.deposit_trx_amt = 0 And t.non_deposit_trx_amt >0 ) Then concat('CC', (t.non_deposit_trx_amt)) 
			Else concat('Deposit ', (t.deposit_trx_amt)) 
		End As nilai_transaksi
	From t_agent_account t
	Join(
		Select @running_total:=0
	) r";
    Set @sqlCommand = Concat(@sqlCommand, " Where t.agent_seq ='" ,pagentSeq, "'");
	Set @sqlCommand = Concat(@sqlCommand, " GROUP BY t.agent_seq,
											t.seq,
											t.mutation_type,
											t.pg_method_seq,
											t.trx_type,
											t.trx_no,
											t.trx_date,
											t.deposit_trx_amt,
											t.non_deposit_trx_amt,
											t.bank_name,
											t.bank_branch_name,
											t.bank_acct_no,
											t.bank_acct_name,
											t.refund_date,
											t.`status`
											");
    Set @sqlCommand = Concat(@sqlCommand, " order by ", "'t.trx_date'" ,"'t.seq'", " ", pDirSort);
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
SELECT pCurrPage, totalPage, totalRec AS total_rec;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set @totalRec = Null;
    Set @sqlCommand = Null;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_agent_account_update*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_agent_account_update`(
    pTrxNo varchar(20)
) 
BEGIN
update t_agent_account set
	`status` = 'N'
Where
	trx_no = pTrxNo;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_agent_add*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_agent_add`(
        `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pEmail` VARCHAR(50),
        `pName` VARCHAR(100),
        `pDate` DATE,
        `pGender` VARCHAR(1),
        `pPhone` VARCHAR(20),
        `pPassword` VARCHAR(100),
        `pImage` VARCHAR(100),
        `pPartnerSeq` INTEGER(10),
        `pStatus` CHAR(1),
        `pBankName` VARCHAR(50),
        `pBankBranchName` VARCHAR(50),
        `pBankAcctNo` VARCHAR(50),
        `pBankAcctName` VARCHAR(50)
    ) 
BEGIN
	DECLARE new_seq INT UNSIGNED;
	SELECT 
        MAX(seq) + 1 INTO new_seq
    FROM m_agent;
    IF new_seq IS NULL THEN
        SET new_seq = 1;
    END IF;
    
	INSERT INTO m_agent(
		seq,
		email,
		PASSWORD,
		`name`,
                partner_seq,
		birthday,
		gender,
		mobile_phone,
		profile_img,
		deposit_amt,
		bank_name,
		bank_branch_name,
		bank_acct_no,
		bank_acct_name,
		`status`,
		created_by,
		created_date,
		modified_by,
		modified_date
	) VALUES (
		new_seq,
		pEmail,
		pPassword,
		pName,
                pPartnerSeq,
		pDate,
		pGender,
		pPhone,
		pImage,
		0,
		`pBankName`,
		`pBankBranchName`,
		`pBankAcctNo`,
		`pBankAcctName`,
		'A',
		'SYSTEM',
		NOW(),
		'SYSTEM',
		NOW()
	);   
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_agent_address_by_agent_seq*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_agent_address_by_agent_seq`(
        `pUserID` VARCHAR(25),
	pIPAddr VARCHAR(50),
	pAgentSeq SMALLINT UNSIGNED
    ) 
BEGIN
SELECT
	aa.seq AS address_seq,
        aa.alias,
	aa.address,
	aa.phone_no,
	d.seq AS district_seq,
	d.name AS district_name,
	c.seq AS city_seq,
	c.name AS city_name,
	p.seq AS province_seq,
	p.name AS province_name,
	aa.zip_code,
	aa.pic_name
FROM 
	m_agent a
LEFT JOIN 
	m_agent_address aa ON a.seq = aa.agent_seq
JOIN 
	m_district d ON aa.district_seq = d.seq
LEFT JOIN 
	m_city c ON d.city_seq = c.seq
LEFT JOIN
	m_province p ON c.province_seq = p.seq
WHERE
	a.seq = pAgentSeq;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_agent_address_by_seq*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_agent_address_by_seq`(
        pAgentSeq SMALLINT UNSIGNED,
		pIPAddr VarChar(50),
		pSeq SmallInt Unsigned
    ) 
BEGIN
	Select
	ma.seq as address_seq,
	ma.alias,
        ma.address,
        ma.zip_code,
        ma.pic_name,
        ma.phone_no,
	p.seq as province_seq,
        p.`name` as province_name,
	c.seq as city_seq,
	c.`name` as city_name,
        d.seq as district_seq,
	d.`name` as district_name
	From m_agent_address ma 
	Left Join m_district d
		On d.seq= ma.district_seq
	Left Join m_city c
		On c.seq= d.city_seq
	Left Join m_province p
		On p.seq= c.province_seq
	Where
		ma.seq = pSeq And
        ma.agent_seq = pAgentSeq;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_agent_by_seq*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_agent_by_seq`(
        pUserID VarChar(50),
        pIPAddr VarChar(50),
        pSeq int(10) unsigned
    ) 
BEGIN
SELECT
  `seq`,
  `email`,
  `password`,
  `name`,
  `partner_seq`,
  `referral`,
  `birthday`,
  `gender`,
  `status`,
  `mobile_phone`,
  `profile_img`,
  `deposit_amt`,
  `last_login`,
  `ip_address`,
  `created_by`,
  `created_date`,
  `modified_by`,
  `modified_date`,
bank_name,
bank_branch_name,
bank_acct_no,
bank_acct_name
FROM `m_agent`
Where 
   seq = pSeq;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_agent_commission_add*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_agent_commission_add`(
	pUserID VARCHAR(25),
	pIPAddr VARCHAR(50),
	pSeq INT UNSIGNED,
	pCatSeq1 MEDIUMINT UNSIGNED,
	pCatSeq2 MEDIUMINT UNSIGNED,
	pFee DECIMAL(4,2)
) 
BEGIN
    DECLARE new_seq INT;
    SELECT 
        MAX(seq) + 1 INTO new_seq
    FROM m_agent_trx_commission 
	WHERE 
		`agent_seq` = pSeq;
    IF new_seq IS NULL THEN
        SET new_seq = 1;
    END IF;
	
	INSERT INTO m_agent_trx_commission(
		agent_seq,
		seq,
		category_l1_seq,
		category_l2_seq,
		commission_fee_percent,
		created_by,
		created_date,
		modified_by,
		modified_date
	) VALUES (
		pSeq,
		new_seq,
		pCatSeq1,
		pCatSeq2,
		pFee,
		pUserID,
		NOW(),
		pUserID,
		NOW()
	);
    
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_agent_commission_delete*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_agent_commission_delete`(
	pUserID VARCHAR(25),
	pIPAddr VARCHAR(50),
	pSeq TINYINT UNSIGNED
) 
BEGIN
	DELETE FROM m_agent_trx_commission 
	WHERE 
		`agent_seq` = pSeq;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_agent_customer_add*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_agent_customer_add`(
        `pUserID` VARCHAR(100),
        `pIPAddress` VARCHAR(50),
	`pAgentSeq` int(10) unsigned,
	`pIdentityNo` varchar(50),
	`pIdentityAddress` mediumtext,
	`pBirthday` date,
	`pAddress` mediumtext,
	`pEmailAddress` varchar(50),
	`pDistrictSeq` smallint(5) unsigned,
	`pSubDistrict` varchar(150),
	`pZipCode` varchar(10),
	`pPicName` varchar(100),
	`pPhoneNo` varchar(50)        
    ) 
BEGIN
Declare new_seq Int unsigned;
Select 
	Max(seq) + 1 Into new_seq
From m_agent_customer;
If new_seq Is Null Then
	Set new_seq = 1;
End If;
insert into m_agent_customer (
`agent_seq`,
`seq`,
`identity_no`,
`identity_address`,
`birthday`,
`address`,
`email_address`,
`district_seq`,
`sub_district`,
`zip_code`,
`pic_name`,
`phone_no`,
    created_by,
    created_date,
    modified_by,
    modified_Date
) values (
  pAgentSeq,
    new_seq,
  pIdentityNo,
  pIdentityAddress,
  pBirthday,
  pAddress,
  pEmailAddress,
  pDistrictSeq,
  pSubDistrict,
  pZipCode,
  pPicName,
  pPhoneNo,
    pUserID,
    now(),
    pUserID,
    now()
);
    
select new_seq;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_agent_customer_list*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_agent_customer_list`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pCurrPage INT UNSIGNED,
        pRecPerPage INT UNSIGNED,
        pDirSort VARCHAR(4),
        pColumnSort VARCHAR(50),
        pAgentSeq INT(9) UNSIGNED,        
        pPicName VARCHAR(50),
        pIdentityNo VARCHAR(50),        
        pEmail VARCHAR(50)
    ) 
BEGIN
    -- Begin variables for Paging
    DECLARE startRec INT UNSIGNED;
    DECLARE totalPage INT UNSIGNED;
    DECLARE totalRec INT UNSIGNED;
    -- End variables for Paging
    -- Begin SQL Where
    DECLARE sqlWhere VARCHAR(500);
	SET sqlWhere = " where 1 ";
	SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    IF pIdentityNo <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " and identity_no like '%" , pIdentityNo, "%'");
    END IF;
    IF pPicName <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " and pic_name like '%" , pPicName, "%'");
    END IF;
    IF pAgentSeq <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " and agent_seq=" , pAgentSeq );
    END IF;
    IF pEmail <> "" THEN
         SET sqlWhere = CONCAT(sqlWhere, " and email_address like '%" , pEmail, "%'");
    END IF;
    -- End SQL Where
    -- Begin Paging Info
    SET @sqlCommand = "Select Count(*) Into @totalRec From `m_agent_customer` ";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, sqlWhere);
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        END IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    -- End Paging Info
    SET @sqlCommand = "
	Select 
		m_agent_customer.*,
		p.seq as province_seq,
		p.`name` as province_name,
		c.seq as city_seq,
		c.`name` as city_name,
		d.seq as district_seq,
		d.`name` as district_name
	From
		`m_agent_customer` 
        
Left Join m_district d
On d.seq= m_agent_customer.district_seq
Left Join m_city c
On c.seq= d.city_seq
Left Join m_province p
On p.seq= c.province_seq        ";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, sqlWhere);
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    SET @sqlCommand = CONCAT(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @totalRec = NULL;
    SET @sqlCommand = NULL;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_agent_list_export_partner*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_agent_list_export_partner`(
        `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pDirSort` VARCHAR(4),
        `pColumnSort` VARCHAR(50),
        `pEmail` VARCHAR(100),
        `pName` VARCHAR(100),
        `pStatus` CHAR(3),
        `pPartnerSeq` INTEGER(10)
    ) 
BEGIN   
    DECLARE sqlWhere VARCHAR(500);	
    SET sqlWhere = "";
      
    IF pEmail <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And email like '%" , escape_string(pEmail), "%'");
    END IF;
    
    IF pName <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And a.name like '%" , escape_string(pName), "%'");
    END IF;
    
    IF pStatus <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And status = '" , pStatus, "'");
    END IF;
    IF pPartnerSeq <> "" THEN
         SET sqlWhere = CONCAT(sqlWhere, " And partner_seq = '" , pPartnerSeq, "'" );
     END IF;        
    
    SET @sqlCommand = "SELECT 
  a.`seq` as agent_seq,
  a.`email`,
  a.`password`,
  a.`name`,
  a.`partner_seq`,
  a.`referral`,
  a.`birthday`,
  a.`gender`,
  a.`mobile_phone`,
  a.`profile_img`,
  a.`deposit_amt`,
  a.`status`,
  a.`last_login`,
  a.`ip_address`,
  a.`created_by`,
  a.`created_date`,
  a.`modified_by`,
  a.`modified_date`,
  aa.`seq` as agent_address_seq,
  aa.`alias`,
  aa.`address`,
  aa.`district_seq`,
  aa.`zip_code`,
  aa.`pic_name`,
  aa.`phone_no`,
  aa.`default`,
  aa.`active`,
  d.`name` AS district,
  c.`name` AS city,
  p.`name` AS province
FROM `m_agent` AS a
LEFT JOIN ( SELECT `agent_seq`, `seq`, `alias`, `address`, `district_seq`, `zip_code`, `pic_name`, `phone_no`, `default`, `active`, `created_by`, `created_date`, `modified_by`, MAX(`modified_date`) AS `modified_date` FROM `m_agent_address` GROUP BY agent_seq) AS aa
ON a.`seq` = aa.`agent_seq` 
LEFT JOIN m_district d ON aa.district_seq = d.seq 
LEFT JOIN m_city c ON d.city_seq = c.seq
LEFT JOIN m_province p ON c.province_seq = p.seq ";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @sqlCommand = NULL;
    
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_agent_list_partner*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_agent_list_partner`(
        `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pCurrPage` INTEGER UNSIGNED,
        `pRecPerPage` INTEGER UNSIGNED,
        `pDirSort` VARCHAR(4),
        `pColumnSort` VARCHAR(50),
        `pEmail` VARCHAR(100),
        `pName` VARCHAR(100),
        `pStatus` CHAR(3),
        `pPartnerSeq` INTEGER(10)
    ) 
BEGIN
    
    DECLARE startRec INT UNSIGNED;
    DECLARE totalPage INT UNSIGNED;
    DECLARE totalRec INT UNSIGNED;
    
    
    DECLARE sqlWhere VARCHAR(500);
	SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    SET sqlWhere = "";
      
    IF pEmail <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And email like '%" , escape_string(pEmail), "%'");
    END IF;
    
    IF pName <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And name like '%" , escape_string(pName), "%'");
    END IF;
    
    IF pStatus <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And status = '" , pStatus, "'");
    END IF;

    IF pPartnerSeq <> "" THEN
         SET sqlWhere = CONCAT(sqlWhere, " And partner_seq = '" , pPartnerSeq, "'" );
     END IF;

    SET @sqlCommand = "Select Count(seq) Into @totalRec From m_agent ";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        END IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    
    SET @sqlCommand = "
        Select
			seq as agent_seq,
            email,
            `name`,
            birthday,
            gender,
            mobile_phone,
            profile_img,
            deposit_amt,
            status,
            created_by,
            created_date,
            modified_by,
            modified_date,
            bank_name
		From m_agent
    ";
    IF sqlWhere <> "" THEN
		SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    
    SET @sqlCommand = CONCAT(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    SET @sqlCommand = CONCAT(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @totalRec = NULL;
    SET @sqlCommand = NULL;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_agent_list_partner_no_paging*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_agent_list_partner_no_paging`(
        `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pPartnerSeq` INTEGER(10)
    ) 
BEGIN
    
SELECT 
  a.`seq` as agent_seq,
  a.`email`,
  a.`password`,
  a.`name`,
  a.`partner_seq`,
  a.`referral`,
  a.`birthday`,
  a.`gender`,
  a.`mobile_phone`,
  a.`profile_img`,
  a.`deposit_amt`,
  a.`status`,
  a.`last_login`,
  a.`ip_address`,
  a.`created_by`,
  a.`created_date`,
  a.`modified_by`,
  a.`modified_date`
FROM `m_agent` AS a WHERE a.partner_seq=pPartnerSeq;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_agent_order_list*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_agent_order_list`(
        `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pagentSeq` BIGINT(10),
        `pCurrPage` INTEGER UNSIGNED,
        `pRecPerPage` INTEGER UNSIGNED,
        `pDirSort` VARCHAR(4),
        `pColumnSort` VARCHAR(50),
        `pOrderNo` VARCHAR(50),
        `pDateSearch` CHAR(1),
        `pOrderDateFrom` DATETIME,
        `pOrderDateTo` DATETIME,
        `pPaymentStatus` CHAR(1),
        `pIdentityNo` varchar(50)
    ) 
BEGIN
    
    Declare startRec Int unsigned;
    Declare totalPage Int unsigned;
    Declare totalRec Int unsigned;
    
    
    Declare sqlWhere VarChar(500);
	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
    If pOrderNo <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And o.order_no like '%" , escape_string(pOrderNo), "%'");
    End If;
    If (pOrderDateFrom <> "") Then
        Set sqlWhere = Concat(sqlWhere, " And o.order_date >='" , pOrderDateFrom, "'");
    End If;
    If (pOrderDateTo <> "") Then
        Set sqlWhere = Concat(sqlWhere, " And o.order_date <='" , pOrderDateTo, "'");
    End If;
    If pPaymentStatus <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And o.payment_status ='" , pPaymentStatus, "'");
    End If;
    IF pIdentityNo <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And c.identity_no like '%" , escape_string(pIdentityNo), "%'");
    END IF;
    
    
    Set @sqlCommand = "Select Count(o.order_no) Into @totalRec
                        From
                        (select order_no, order_date, payment_status, agent_seq from
                        m_agent m Join t_order o On m.seq = o.agent_seq) as o 
                        LEFT JOIN t_order_loan l ON l.order_seq=o.order_no
			LEFT JOIN m_agent_customer c ON c.seq=l.`customer_seq`
			";
    Set @sqlCommand = Concat(@sqlCommand, " Where o.agent_seq ='" ,pagentSeq, "'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);
        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;
        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    
    Set @sqlCommand = "
        Select
            o.seq,
            o.agent_name,
            o.order_no,
            o.order_date,
            o.total_order,
            o.payment_status,
            o.paid_date,
            o.payment_method,
            '' as detail,
            '' as payment,
	    c.`identity_no`,
	    c.`pic_name`,
	    l.dp,
	    l.admin_fee,
	    l.tenor,
	    l.loan_interest,
	    l.installment,
	    l.total_installment,
	    l.status_order
            From
        (
        select
            o.seq,
            o.agent_seq,
            m.name as agent_name,
            o.order_no,
            o.order_date,
            o.total_order,
            o.payment_status,
            o.paid_date,
            pgm.name as payment_method,
            '' as detail,
            '' as payment
		from
                    m_agent m
		Join
                    t_order o
                            On m.seq = o.agent_seq
		Join
                    m_payment_gateway_method pgm
                            On o.pg_method_seq = pgm.seq
		) as o
        LEFT JOIN t_order_loan l ON l.order_seq=o.seq
        LEFT JOIN m_agent_customer c ON c.seq=l.`customer_seq`
		";
    Set @sqlCommand = Concat(@sqlCommand, " Where o.agent_seq ='" ,pagentSeq, "'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    Select pCurrPage, totalPage, totalRec as total_rec;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set @totalRec = Null;
    Set @sqlCommand = Null;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_agent_order_list_by_order_*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_agent_order_list_by_order_`(
        `pUserID` VARCHAR(100),
        `pIPAddress` VARCHAR(25),
        `pOrderNo` VARCHAR(20)
    ) 
BEGIN
IF EXISTS (SELECT order_no FROM t_order WHERE order_no = pOrderNo) THEN
BEGIN
SELECT 
	o.seq,
	o.order_no,
	o.order_date,
	o.agent_seq,
	o.signature,
	m.`name` member_name,
	m.email,
	m.mobile_phone,
	c.`name` city_name,
	CASE WHEN o.payment_retry <> '0'  THEN CONCAT(o.order_no, '-', CAST(o.payment_retry AS CHAR(2))) ELSE o.order_no END order_trans_no,	
	o.receiver_name,
	o.receiver_address,
	o.receiver_district_seq,
	d.`name` district_name,
	c.`name` city_name,
	p.`name` province_name,
	o.receiver_zip_code,
	o.receiver_phone_no,
	o.payment_status,
	o.pg_method_seq,
	pg.`code` payment_code,
	pg.name payment_name,
	o.paid_date,
	CASE WHEN pv.nominal IS NULL THEN mc.nominal ELSE pv.nominal END AS nominal,
	pv.code AS voucher_code,
	o.payment_retry,
	o.total_order,
	o.voucher_seq,
	o.voucher_refunded,
	o.total_payment,
	o.conf_pay_type,
	o.conf_pay_amt_member,
	o.conf_pay_date,
	o.conf_pay_note_file,
	o.conf_pay_bank_seq,
	o.conf_pay_amt_admin,
    o.promo_credit_seq,
t_o_l.`customer_seq`,
t_o_l.`dp`,
t_o_l.`admin_fee`,
t_o_l.`tenor`,
t_o_l.`loan_interest`,
t_o_l.`installment`,
t_o_l.`total_installment`,
t_o_l.`status_order`    
FROM 
	t_order o JOIN m_district d
	ON o.receiver_district_seq = d.seq
		JOIN m_city c 
	ON c.seq = d.city_seq 
		JOIN m_province p
	ON p.seq = c.province_seq
		JOIN m_payment_gateway_method pg
	ON pg.seq = o.pg_method_seq
		LEFT JOIN m_promo_voucher pv 
	ON o.voucher_seq = pv.seq
		LEFT JOIN m_coupon mc 
	ON o.coupon_seq = mc.seq
		JOIN m_agent m
	ON m.seq = o.agent_seq
	  LEFT JOIN t_order_loan t_o_l ON t_o_l.order_seq=o.seq
WHERE
	o.order_no = pOrderNo;
    
SELECT 
    t.order_seq,
    t.merchant_info_seq,
    t.expedition_service_seq,
    e.`name` expedition_name,
    mm.`name` merchant_name,
    mm.email,
    mm.code AS merchant_code,
    mm.seq merchant_seq,
    t.real_expedition_service_seq,
    t.total_merchant,
    t.total_ins,
    t.total_ship_real,
    t.total_ship_charged,
    t.free_fee_seq,
    t.order_status,
    t.member_notes,
    t.printed,
    t.print_date,
    t.awb_seq,
    t.awb_no,
    t.ref_awb_no,
    t.ship_by,
    t.ship_by_exp_seq,
    t.ship_date,
    t.ship_note_file,
    t.ship_notes,
    t.received_date,
    t.received_by,
    t.redeem_seq,
    t.exp_invoice_seq,
    t.exp_invoice_awb_seq,
    (SELECT 
            COUNT(*) total_barang
        FROM
            t_order_product top
                JOIN
            t_order tor ON top.order_seq = tor.seq
        WHERE
            tor.order_no = o.order_no
                AND top.merchant_info_seq = t.merchant_info_seq) AS total_product,
	t.created_date,
    o.paid_date,
    t.`modified_date`
FROM
    t_order_merchant t
        JOIN
    m_merchant_info m ON m.seq = t.merchant_info_seq
        JOIN
    m_district d ON d.seq = m.pickup_district_seq
        JOIN
    m_city c ON c.seq = d.city_seq
        JOIN
    m_province p ON p.seq = c.province_seq
        JOIN
    m_merchant mm ON mm.seq = m.merchant_seq
        JOIN
    m_expedition_service s ON s.seq = t.expedition_service_seq
        JOIN
    m_expedition e ON s.exp_seq = e.seq
        JOIN
    t_order o ON o.seq = t.order_seq
WHERE
    o.order_no = pOrderNo;
            
SELECT 
	t.order_seq,
	mi.merchant_seq,
	t.merchant_info_seq,
	p.`name` AS display_name,
	v.seq AS product_seq,
	v.pic_1_img AS img,
	mv.`value`,
	mv.seq AS value_seq,
	mv.`value` AS variant_name,
	t.product_status,
	t.qty,
	t.sell_price,
	t.weight_kg,
	t.ship_price_real,
	t.ship_price_charged,
	t.trx_fee_percent,
	t.ins_rate_percent,
	t.product_status,
	t.qty_return,
	t.created_by,
	t.created_date,
	t.modified_by,
	t.modified_date,
	t.commission_fee_percent
FROM
	t_order_product t 
		JOIN m_product_variant v			
			ON v.seq = t.product_variant_seq
		JOIN m_product p
			ON v.product_seq = p.seq
		JOIN m_variant_value vv
			ON vv.seq = t.variant_value_seq
		JOIN m_variant_value mv 
			ON mv.seq = v.variant_value_seq
		JOIN t_order o
			ON o.seq = t.order_seq
		JOIN m_merchant_info mi
			ON mi.seq = t.merchant_info_seq
		JOIN m_merchant mm 
			ON mm.seq = mi.merchant_seq
WHERE
	o.order_no = pOrderNo;
    
SELECT 
    'Pesanan telah kami terima dan dalam proses verifikasi' AS history_message,
    created_date 
FROM
    t_order
WHERE
    order_no = pOrderNo
UNION ALL SELECT 
    'Pesanan telah terverifikasi dan akan segera dikirim',
    paid_date 
FROM
    t_order
WHERE
    order_no = pOrderNo 
UNION ALL SELECT 
    CONCAT('Pesanan sedang diproses untuk dikirim oleh ',mm.name),
    t.print_date
FROM
    t_order_merchant t
	JOIN
    m_merchant_info m ON m.seq = t.merchant_info_seq
	JOIN
    m_merchant mm ON mm.seq = m.merchant_seq
        JOIN
    t_order o ON o.seq = t.order_seq
WHERE
    o.order_no = pOrderNo  AND  t.print_date <> "0000-00-00 00:00:00"
UNION ALL SELECT 
    CONCAT('Pesanan telah dikirim oleh ',
	    mm.`name`,
            ' dan melalui ',
            e.`name`,
            ', no.resi ',
            t.awb_no),
    t.ship_date 
FROM
    t_order_merchant t
    	JOIN
    m_merchant_info m ON m.seq = t.merchant_info_seq
	JOIN
    m_merchant mm ON mm.seq = m.merchant_seq
        JOIN
    m_expedition_service s ON s.seq = t.expedition_service_seq
        JOIN
    m_expedition e ON s.exp_seq = e.seq
        JOIN
    t_order o ON o.seq = t.order_seq
WHERE
    o.order_no = pOrderNo AND t.ship_date <> "0000-00-00"
UNION ALL SELECT 
    'Pesanan telah diterima pelanggan',
     t.received_date 
FROM
    t_order_merchant t
        JOIN
    t_order o ON o.seq = t.order_seq
WHERE
    o.order_no = pOrderNo  AND t.received_date <> "0000-00-00"
ORDER BY created_date DESC;    
END;
ELSE
BEGIN
SELECT
	o.seq,
	o.order_no,
	o.order_no AS order_trans_no,
    o.agent_seq,
	o.order_date,
	o.total_order,
	o.total_payment,
	o.payment_status,
	o.order_status,
	o.signature,
	o.pg_method_seq,
	o.phone_no AS phone_number,
	psn.nominal,
	ps.name AS provider_service_name,
	p.name AS provider_name,
	pgm.name AS pg_method_name,
	pgm.code AS payment_code
FROM 
	t_order_voucher o JOIN m_provider_service_nominal psn
	ON psn.seq = o.provider_nominal_seq
		JOIN m_provider_service ps
	ON ps.seq = psn.provider_service_seq
		JOIN m_provider p
	ON p.seq = ps.provider_seq
		JOIN m_payment_gateway_method pgm
	ON pgm.seq = o.pg_method_seq
		JOIN m_payment_gateway pg
	ON pg.seq = pgm.pg_seq
WHERE
	order_no = pOrderNo;
END;
END IF;	
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_agent_order_list_by_order_no*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_agent_order_list_by_order_no`(
        `pUserID` VARCHAR(100),
        `pIPAddress` VARCHAR(25),
        `pOrderNo` VARCHAR(20)
    ) 
BEGIN
IF EXISTS (SELECT order_no FROM t_order WHERE order_no = pOrderNo) THEN
BEGIN
SELECT 
	o.seq,
	o.order_no,
	o.order_date,
	o.agent_seq,
	o.signature,
	m.`name` member_name,
	m.email,
	m.mobile_phone,
	c.`name` city_name,
	CASE WHEN o.payment_retry <> '0'  THEN CONCAT(o.order_no, '-', CAST(o.payment_retry AS CHAR(2))) ELSE o.order_no END order_trans_no,	
	o.receiver_name,
	o.receiver_address,
	o.receiver_district_seq,
	d.`name` district_name,
	c.`name` city_name,
	p.`name` province_name,
	o.receiver_zip_code,
	o.receiver_phone_no,
	o.payment_status,
	o.pg_method_seq,
	pg.`code` payment_code,
	pg.name payment_name,
	o.paid_date,
	CASE WHEN pv.nominal IS NULL THEN mc.nominal ELSE pv.nominal END AS nominal,
	pv.code AS voucher_code,
	o.payment_retry,
	o.total_order,
	o.voucher_seq,
	o.voucher_refunded,
	o.total_payment,
	o.conf_pay_type,
	o.conf_pay_amt_member,
	o.conf_pay_date,
	o.conf_pay_note_file,
	o.conf_pay_bank_seq,
	o.conf_pay_amt_admin,
	o.promo_credit_seq,
	t_o_l.`customer_seq`,
	t_o_l.`dp`,
	t_o_l.`admin_fee`,
	t_o_l.`tenor`,
	t_o_l.`loan_interest`,
	t_o_l.`installment`,
	t_o_l.`total_installment`,
	t_o_l.`status_order`,
	t_o_l.pg_method_seq as pg_seq_loan,
	t_o_l.auth_date,
	pgm.`name` AS pg_method_name
	
FROM 
	t_order o JOIN m_district d
	ON o.receiver_district_seq = d.seq
		JOIN m_city c 
	ON c.seq = d.city_seq 
		JOIN m_province p
	ON p.seq = c.province_seq
		JOIN m_payment_gateway_method pg
	ON pg.seq = o.pg_method_seq
		LEFT JOIN m_promo_voucher pv 
	ON o.voucher_seq = pv.seq
		LEFT JOIN m_coupon mc 
	ON o.coupon_seq = mc.seq
		JOIN m_agent m
	ON m.seq = o.agent_seq
	  LEFT JOIN t_order_loan t_o_l ON t_o_l.order_seq=o.seq
	  LEFT JOIN m_payment_gateway_method pgm ON pgm.seq=t_o_l.pg_method_seq
	  
WHERE
	o.order_no = pOrderNo;
    
SELECT 
    t.order_seq,
    t.merchant_info_seq,
    t.expedition_service_seq,
    e.`name` expedition_name,
    mm.`name` merchant_name,
    mm.email,
    mm.code AS merchant_code,
    mm.seq merchant_seq,
    t.real_expedition_service_seq,
    t.total_merchant,
    t.total_ins,
    t.total_ship_real,
    t.total_ship_charged,
    t.free_fee_seq,
    t.order_status,
    t.member_notes,
    t.printed,
    t.print_date,
    t.awb_seq,
    t.awb_no,
    t.ref_awb_no,
    t.ship_by,
    t.ship_by_exp_seq,
    t.ship_date,
    t.ship_note_file,
    t.ship_notes,
    t.received_date,
    t.received_by,
    t.redeem_seq,
    t.exp_invoice_seq,
    t.exp_invoice_awb_seq,
    (SELECT 
            COUNT(*) total_barang
        FROM
            t_order_product top
                JOIN
            t_order tor ON top.order_seq = tor.seq
        WHERE
            tor.order_no = o.order_no
                AND top.merchant_info_seq = t.merchant_info_seq) AS total_product,
	t.created_date,
    o.paid_date,
    t.`modified_date`
FROM
    t_order_merchant t
        JOIN
    m_merchant_info m ON m.seq = t.merchant_info_seq
        JOIN
    m_district d ON d.seq = m.pickup_district_seq
        JOIN
    m_city c ON c.seq = d.city_seq
        JOIN
    m_province p ON p.seq = c.province_seq
        JOIN
    m_merchant mm ON mm.seq = m.merchant_seq
        JOIN
    m_expedition_service s ON s.seq = t.expedition_service_seq
        JOIN
    m_expedition e ON s.exp_seq = e.seq
        JOIN
    t_order o ON o.seq = t.order_seq
WHERE
    o.order_no = pOrderNo;
            
SELECT 
	t.order_seq,
	mi.merchant_seq,
	t.merchant_info_seq,
	p.`name` AS display_name,
	v.seq AS product_seq,
	v.pic_1_img AS img,
	mv.`value`,
	mv.seq AS value_seq,
	mv.`value` AS variant_name,
	t.product_status,
	t.qty,
	t.sell_price,
	t.weight_kg,
	t.ship_price_real,
	t.ship_price_charged,
	t.trx_fee_percent,
	t.ins_rate_percent,
	t.product_status,
	t.qty_return,
	t.created_by,
	t.created_date,
	t.modified_by,
	t.modified_date,
	t.commission_fee_percent
FROM
	t_order_product t 
		JOIN m_product_variant v			
			ON v.seq = t.product_variant_seq
		JOIN m_product p
			ON v.product_seq = p.seq
		JOIN m_variant_value vv
			ON vv.seq = t.variant_value_seq
		JOIN m_variant_value mv 
			ON mv.seq = v.variant_value_seq
		JOIN t_order o
			ON o.seq = t.order_seq
		JOIN m_merchant_info mi
			ON mi.seq = t.merchant_info_seq
		JOIN m_merchant mm 
			ON mm.seq = mi.merchant_seq
WHERE
	o.order_no = pOrderNo;
    
SELECT 
    'Pesanan telah kami terima dan dalam proses verifikasi' AS history_message,
    created_date 
FROM
    t_order
WHERE
    order_no = pOrderNo
UNION ALL SELECT 
    'Pesanan telah terverifikasi dan akan segera dikirim',
    paid_date 
FROM
    t_order
WHERE
    order_no = pOrderNo 
UNION ALL SELECT 
    CONCAT('Pesanan sedang diproses untuk dikirim oleh ',mm.name),
    t.print_date
FROM
    t_order_merchant t
	JOIN
    m_merchant_info m ON m.seq = t.merchant_info_seq
	JOIN
    m_merchant mm ON mm.seq = m.merchant_seq
        JOIN
    t_order o ON o.seq = t.order_seq
WHERE
    o.order_no = pOrderNo  AND  t.print_date <> "0000-00-00 00:00:00"
UNION ALL SELECT 
    CONCAT('Pesanan telah dikirim oleh ',
	    mm.`name`,
            ' dan melalui ',
            e.`name`,
            ', no.resi ',
            t.awb_no),
    t.ship_date 
FROM
    t_order_merchant t
    	JOIN
    m_merchant_info m ON m.seq = t.merchant_info_seq
	JOIN
    m_merchant mm ON mm.seq = m.merchant_seq
        JOIN
    m_expedition_service s ON s.seq = t.expedition_service_seq
        JOIN
    m_expedition e ON s.exp_seq = e.seq
        JOIN
    t_order o ON o.seq = t.order_seq
WHERE
    o.order_no = pOrderNo AND t.ship_date <> "0000-00-00"
UNION ALL SELECT 
    'Pesanan telah diterima pelanggan',
     t.received_date 
FROM
    t_order_merchant t
        JOIN
    t_order o ON o.seq = t.order_seq
WHERE
    o.order_no = pOrderNo  AND t.received_date <> "0000-00-00"
ORDER BY created_date DESC;    
END;
ELSE
BEGIN
SELECT
	o.seq,
	o.order_no,
	o.order_no AS order_trans_no,
    o.agent_seq,
	o.order_date,
	o.total_order,
	o.total_payment,
	o.payment_status,
	o.order_status,
	o.signature,
	o.pg_method_seq,
	o.phone_no AS phone_number,
	psn.nominal,
	ps.name AS provider_service_name,
	p.name AS provider_name,
	pgm.name AS pg_method_name,
	pgm.code AS payment_code
FROM 
	t_order_voucher o JOIN m_provider_service_nominal psn
	ON psn.seq = o.provider_nominal_seq
		JOIN m_provider_service ps
	ON ps.seq = psn.provider_service_seq
		JOIN m_provider p
	ON p.seq = ps.provider_seq
		JOIN m_payment_gateway_method pgm
	ON pgm.seq = o.pg_method_seq
		JOIN m_payment_gateway pg
	ON pg.seq = pgm.pg_seq
WHERE
	order_no = pOrderNo;
END;
END IF;	
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_agent_order_redeem_info*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_agent_order_redeem_info`(
        `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pAgentSeq` BIGINT(10)
    ) 
BEGIN

    SELECT sum(top.qty*top.sell_price*top.commission_fee_percent/100) as pending_commission FROM t_order_product top
    join t_order tor on top.order_seq=tor.seq
    join t_order_merchant tom on top.order_seq=tom.order_seq and top.merchant_info_seq=tom.merchant_info_seq
    where tor.payment_status='P' and top.product_status='R' and tom.order_status in ('R','S') and tor.agent_seq=pAgentSeq;

    SELECT sum(top.qty*top.sell_price*top.commission_fee_percent/100) as commission FROM t_order_product top
    join t_order tor on top.order_seq=tor.seq
    join t_order_merchant tom on top.order_seq=tom.order_seq and top.merchant_info_seq=tom.merchant_info_seq
    where tor.payment_status='P' and top.product_status='R' and tom.order_status = 'D' and tor.agent_seq=pAgentSeq;

END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_agent_order_redeem_list*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_agent_order_redeem_list`(
        `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pAgentSeq` BIGINT(10),
         pCurrPage INT UNSIGNED,
         pRecPerPage INT UNSIGNED,
        `pPaymentStatus` CHAR(1)
    ) 
BEGIN
    
    DECLARE startRec INT UNSIGNED;
    DECLARE totalPage INT UNSIGNED;
    DECLARE totalRec INT UNSIGNED;
    
    Declare sqlWhere VarChar(500);
    Set sqlWhere = "";
    SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    If pPaymentStatus = "P" Then
        Set sqlWhere = Concat(sqlWhere, " And tom.order_status = 'D' ");
    End If;
    If pPaymentStatus = "U" Then
        Set sqlWhere = Concat(sqlWhere, " And tom.order_status in ('R','S') ");
    End If;
    
    SET @sqlCommand = "Select Count(*) Into @totalRec From t_order tor
        join t_order_merchant tom on tor.seq=tom.order_seq
        join t_order_product top on tor.seq=top.order_seq and tom.merchant_info_seq=top.merchant_info_seq and top.order_seq=tom.order_seq ";
    Set @sqlCommand = Concat(@sqlCommand, " Where tor.agent_seq ='" ,pAgentSeq, "' and tor.payment_status='P' and top.product_status='R' ");
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, sqlWhere);
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        END IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    
    SET @sqlCommand = "
        Select
            tor.seq,
            tor.agent_seq,
            tor.order_no,
            tor.order_date,
            tor.receiver_name,
            tor.receiver_address,
            tor.receiver_district_seq,
            tor.receiver_zip_code,
            tor.receiver_phone_no,
            tom.order_status,
            tom.received_date,
            top.product_variant_seq,
            top.variant_value_seq,
            top.qty,
            top.sell_price,
            top.weight_kg,
            top.ship_price_real,
            top.commission_fee_percent,
            md.`name` district_name,
            mc.`name` city_name,
            mp.`name` province_name,
            mpr.`name` AS display_name,
            mpr.merchant_seq,
            mpv.seq AS product_seq,
            mpv.pic_1_img AS img,
            mv.`value`,
            mv.seq AS value_seq,
            mv.`value` AS variant_name
       from t_order tor
		join t_order_merchant tom on tor.seq=tom.order_seq
		join t_order_product top on tor.seq=top.order_seq and tom.merchant_info_seq=top.merchant_info_seq and top.order_seq=tom.order_seq
                JOIN m_district md ON tor.receiver_district_seq = md.seq
                JOIN m_city mc ON mc.seq = md.city_seq
                JOIN m_province mp ON mp.seq = mc.province_seq
                JOIN m_product_variant mpv ON mpv.seq = top.product_variant_seq
		JOIN m_product mpr ON mpv.product_seq = mpr.seq
		JOIN m_variant_value vv ON vv.seq = top.variant_value_seq
		JOIN m_variant_value mv ON mv.seq = mpv.variant_value_seq
		";
    Set @sqlCommand = Concat(@sqlCommand, " Where tor.agent_seq ='" ,pAgentSeq, "' and tor.payment_status='P' and top.product_status='R' ");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;

    Set @sqlCommand = Concat(@sqlCommand, " order by tor.order_date desc, tor.seq desc");
    SET @sqlCommand = CONCAT(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set @sqlCommand = Null;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_agent_partner_data_status*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_agent_partner_data_status`(
        `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pCurrPage` INTEGER UNSIGNED,
        `pRecPerPage` INTEGER UNSIGNED,
        `pDirSort` VARCHAR(4),
        `pColumnSort` VARCHAR(50),
        `pEmail` VARCHAR(100),
        `pName` VARCHAR(100),
        `pStatus` CHAR(3),
        `pPartnerSeq` INTEGER(10)
    ) 
BEGIN
    
    DECLARE startRec INT UNSIGNED;
    DECLARE totalPage INT UNSIGNED;
    DECLARE totalRec INT UNSIGNED;    
    
    DECLARE sqlWhere VARCHAR(500);
	SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    SET sqlWhere = "";
      
    IF pEmail <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And email like '%" , escape_string(pEmail), "%'");
    END IF;
    
    IF pName <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And a.name like '%" , escape_string(pName), "%'");
    END IF;
    
--    IF pStatus <> "" THEN
--        SET sqlWhere = CONCAT(sqlWhere, " And status = '" , pStatus, "'");
--    END IF;
    IF pPartnerSeq <> "" THEN
         SET sqlWhere = CONCAT(sqlWhere, " And partner_seq = '" , pPartnerSeq, "'" );
     END IF;
    SET @sqlCommand = "SELECT COUNT(*) AS qty_status, status AS agent_status FROM `m_agent` AS a
LEFT JOIN ( SELECT `agent_seq`, `seq`, `alias`, `address`, `district_seq`, `zip_code`, `pic_name`, `phone_no`, `default`, `active`, `created_by`, `created_date`, `modified_by`, MAX(`modified_date`) AS `modified_date` FROM `m_agent_address` GROUP BY agent_seq) AS aa
ON a.`seq` = aa.`agent_seq` 
LEFT JOIN m_district d ON aa.district_seq = d.seq 
LEFT JOIN m_city c ON d.city_seq = c.seq
LEFT JOIN m_province p ON c.province_seq = p.seq ";
        
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
        SET @sqlCommand = CONCAT(@sqlCommand, " GROUP BY status");
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @totalRec = NULL;
    SET @sqlCommand = NULL;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_agent_partner_order_status*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_agent_partner_order_status`(
        `pUserID` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pCurrPage` INTEGER UNSIGNED,
        `pRecPerPage` INTEGER UNSIGNED,
        `pDirSort` VARCHAR(4),
        `pColumnSort` VARCHAR(50),
        `pPartnerId` INTEGER(9) UNSIGNED,
        `pAgentSeq` INTEGER(10) UNSIGNED,
        `pOrderNo` VARCHAR(50),
        `pDates` VARCHAR(10),
        `pDatee` VARCHAR(10),
        `pPaymentStatus` CHAR(1)
    ) 
BEGIN
    -- Begin variables for Paging
    DECLARE startRec INT UNSIGNED;
    DECLARE totalPage INT UNSIGNED;
    DECLARE totalRec INT UNSIGNED;
    -- End variables for Paging
    -- Begin SQL Where
    DECLARE sqlWhere VARCHAR(800);
    DECLARE sqlWhereC VARCHAR(800);
    SET sqlWhere = " WHERE 1";
    SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    IF pOrderNo <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And t_o.order_no like '%" , escape_string(pOrderNo), "%'");
    END IF;    
    IF pDates <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And t_o.order_date >= '" , escape_string(pDates), "'");
    END IF;
    IF pDatee <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And t_o.order_date <= '" , escape_string(pDatee), "'");
    END IF;
    IF pPartnerId <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And m_a.partner_seq ='" , pPartnerId, "'");
    END IF;
    IF pAgentSeq <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And t_o.agent_seq = '" , pAgentSeq, "'");
    END IF;
--    IF pPaymentStatus <> "" THEN
--        SET sqlWhere = CONCAT(sqlWhere, " And t_o.payment_status='" , pPaymentStatus, "'");
--    END IF;
    -- End SQL Where
    -- Begin Paging Info
    SET @sqlCommand = "SELECT Count(distinct(t_o.seq)) AS qty_status, payment_status FROM `t_order` t_o INNER JOIN m_agent m_a ON t_o.agent_seq = m_a.seq INNER JOIN t_order_merchant t_o_m ON t_o_m.order_seq=t_o.seq INNER JOIN t_order_product t_o_p ON t_o_p.order_seq = t_o.seq AND t_o_p.merchant_info_seq=t_o_m.merchant_info_seq
LEFT JOIN m_district m_d ON m_d.seq=t_o.receiver_district_seq INNER JOIN m_city m_c ON m_d.city_seq=m_c.seq INNER JOIN m_province m_p ON m_c.province_seq = m_p.seq";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, sqlWhere);
        SET @sqlCommand = CONCAT(@sqlCommand, " GROUP BY payment_status");
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_agent_trx_fee_log_add*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_agent_trx_fee_log_add`(
        pUserID VARCHAR(25),
        pIPAddress VARCHAR(25),
        pSeq INT UNSIGNED
    ) 
BEGIN
    DECLARE new_seq INT;
    DECLARE new_update_seq INT;
    SELECT 
        MAX(seq),MAX(update_seq)+1 INTO new_seq,new_update_seq
    FROM `m_agent_trx_commission_log` 
	WHERE 
		`agent_seq` = pSeq;
    IF new_seq IS NULL THEN
        SET new_seq = 0;
        SET new_update_seq=1;
    END IF;
    
    INSERT INTO m_agent_trx_commission_log (
		agent_seq,
		seq,
        update_seq,
		category_l1_seq,
		category_l2_seq,
		trx_fee_percent,
		created_by,
		created_date,
		modified_by,
		modified_date
	)  
		SELECT 
        agent_seq,
		(seq+new_seq),
        new_update_seq,
		category_l1_seq,
		category_l2_seq,
		commission_fee_percent,
		created_by,
		created_date,
		pUserID,
		NOW()
        FROM m_agent_trx_commission
        WHERE 
		`agent_seq` = pSeq 
	;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_agent_update*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_agent_update`(
        `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pName` VARCHAR(100),
        `pDate` DATE,
        `pGender` VARCHAR(1),
        `pPhone` VARCHAR(20),
        `pPassword` VARCHAR(100),
        `pImage` VARCHAR(100),
        `pPartnerSeq` INTEGER(10),
        `pStatus` CHAR(1),
        `pBankName` VARCHAR(50),
        `pBankBranchName` VARCHAR(50),
        `pBankAcctNo` VARCHAR(50),
        `pBankAcctName` VARCHAR(50),
	`pSeq` INTEGER(10)
    ) 
BEGIN
if pPassword <> ''
then
	update `m_agent`
	set 
	  `password` = pPassword,
	  `name` = pName,
	  `birthday` = pDate,
	  `partner_seq` = pPartnerSeq,
	  `gender` = pGender,
	  `mobile_phone` = pPhone,
	  `profile_img` = pImage,
	  `bank_name` = pBankName,
	  `bank_branch_name` = pBankBranchName,
	  `bank_acct_no` = pBankAcctNo,
	  `bank_acct_name` = pBankAcctName,
	  `status` = pStatus,
	  `modified_by` = pUserID,
	  `modified_date` = NOW()
	where `seq` = pSeq;
else
	UPDATE `m_agent`
	SET 
	  `name` = pName,
	  `birthday` = pDate,
	  `partner_seq` = pPartnerSeq,
	  `gender` = pGender,
	  `mobile_phone` = pPhone,
	  `profile_img` = pImage,
	  `bank_name` = pBankName,
	  `bank_branch_name` = pBankBranchName,
	  `bank_acct_no` = pBankAcctNo,
	  `bank_acct_name` = pBankAcctName,
	  `status` = pStatus,
	  `modified_by` = pUserID,
	  `modified_date` = NOW()
	WHERE `seq` = pSeq;	
end if;    
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_agent_update_payment*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_agent_update_payment`(
        pIPAddr VarChar(100),
        pAgentSeq BIGINT(20) Unsigned,
        pBankName VarChar(25),
        pBankBranchName VarChar(50),
        pBankAcctNo Varchar(50),
        pBankAcctName Varchar(50)

) 
BEGIN
Update m_agent Set
    bank_name = pBankName,
    bank_branch_name = pBankBranchName,
    bank_acct_no = pBankAcctNo,
    bank_acct_name = pBankAcctName,
    modified_by = pIPAddr,
    modified_date = now()
Where
	seq = pAgentSeq;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_agent_update_status_order_loan*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_agent_update_status_order_loan`(
	pUserID VARCHAR(25), 
	pIPAddr VARCHAR(50),
	pSeq bigint(20),
	pStatus VARCHAR(1),
	pPoNo VARCHAR(50)
    ) 
BEGIN
UPDATE t_order_loan SET status_order=pStatus, po_no=pPoNo, auth_date=now() where order_seq=pSeq;
if pStatus='R' then 
	UPDATE t_order SET payment_status='F' WHERE seq=pSeq;
	UPDATE t_order_merchant SET order_status='Y' WHERE order_seq=pSeq;
	UPDATE t_order_product SET product_status='X' WHERE order_seq=pSeq;
end if;
    
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_agent_update_status_pg_order_loan*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_agent_update_status_pg_order_loan`(
	pUserID VARCHAR(25), 
	pIPAddr VARCHAR(50),
	pSeq bigint(20),
	pStatus VARCHAR(1),    
	pPgMethodSeq smallint(2) unsigned
    ) 
BEGIN
UPDATE t_order_loan SET status_order=pStatus, pg_method_seq=pPgMethodSeq where order_seq=pSeq;
    
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_bank_delete*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_bank_delete`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq tinyint unsigned
    ) 
BEGIN
Delete From m_bank_account Where seq = pSeq;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_bank_update*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_bank_update`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pSeq TINYINT UNSIGNED,
        pBankName VARCHAR(50),
        pBankBranchName VARCHAR(50),
        pBankAcctNo VARCHAR(50),
        pBankAcctName VARCHAR(50),
        pActive CHAR(1),
        pLogoImg VARCHAR(45)
    ) 
BEGIN
UPDATE m_bank_account SET
    bank_name = pBankName,
    bank_branch_name = pBankBranchName,
    bank_acct_no = pBankAcctNo,
    bank_acct_name = pBankAcctName,
    active = pActive,
    logo_img=pLogoImg,
    modified_by = pUserID,
    modified_date = NOW()
WHERE
	seq = pSeq;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_customer_agent_partner_by_seq*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_customer_agent_partner_by_seq`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pSeq TINYINT UNSIGNED
    ) 
BEGIN
	SELECT 
			m_a_c.`seq`,
			m_a_c.`identity_no`,
			m_a_c.`identity_address`,
			m_a_c.`phone_no`,
			m_a_c.`email_address`,
			m_a_c.`sub_district`,
			m_a_c.`address`,
			m_a_c.`birthday`,
			m_a_c.`zip_code`,
			m_a_c.`active`
			
				
	  FROM m_agent_customer m_a_c
          JOIN m_agent m_a ON m_a.`seq`=m_a_c.`agent_seq`
          
	WHERE
		m_a_c.`seq` = pSeq;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_customer_agent_partner_list*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_customer_agent_partner_list`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pCurrPage INT UNSIGNED,
        pRecPerPage INT UNSIGNED,
        pDirSort VARCHAR(4),
        pColumnSort VARCHAR(50),
        pIdentityNo VARCHAR(50),
        pEmailAddress VARCHAR(50),
        pPartnerSeq INTEGER(10) UNSIGNED,
        pActive CHAR(1)
    ) 
BEGIN
    -- Begin variables for Paging
    DECLARE startRec INT UNSIGNED;
    DECLARE totalPage INT UNSIGNED;
    DECLARE totalRec INT UNSIGNED;
    -- End variables for Paging
    -- Begin SQL Where
    DECLARE sqlWhere VARCHAR(500);
	SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    SET sqlWhere = "";
    IF pIdentityNo <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And m_a_c.`identity_no` like '%" , escape_string(pIdentityNo), "%'");
    END IF;
    IF pEmailAddress <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And m_a_c.`email_address` like '%" , escape_string(pEmailAddress), "%'");
    END IF;
    IF pActive <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And m_a_c.`active` = '", pActive ,"'");
    END IF;
    IF pPartnerSeq <> "" THEN
         SET sqlWhere = CONCAT(sqlWhere, " And m_a.partner_seq = '" , pPartnerSeq, "'" );
     END IF;
    
    -- End SQL Where
    -- Begin Paging Info
    SET @sqlCommand = "Select Count(m_a_c.`seq`) Into @totalRec FROM m_agent_customer m_a_c
          JOIN m_agent m_a ON m_a.`seq`=m_a_c.`agent_seq`";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        END IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    -- End Paging Info
    SET @sqlCommand = "
        Select		
			m_a_c.`seq`,
			m_a_c.`identity_no`,
			m_a_c.`identity_address`,
			m_a_c.`phone_no`,
			m_a_c.`email_address`,
			m_a_c.`sub_district`,
			m_a_c.`address`,
			m_a_c.`birthday`,
			m_a_c.`created_date`
				
	  FROM m_agent_customer m_a_c
          JOIN m_agent m_a ON m_a.`seq`=m_a_c.`agent_seq`
          ";
    
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    SET @sqlCommand = CONCAT(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @totalRec = NULL;
    SET @sqlCommand = NULL;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_get_credit_product_list*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_get_credit_product_list`(
   IN pSTART INTEGER, 
   IN pLIMIT INTEGER,
   IN pWhere TEXT,
   IN pOrder TEXT
) 
BEGIN
IF (pWhere = '') THEN SET @SqlWhere = ''; ELSE SET @SqlWhere = pWhere; END IF;
IF (pOrder = '') THEN SET @SqlOrder = ''; ELSE SET @SqlOrder = pOrder; END IF;
SET @sql = CONCAT("SELECT SQL_CALC_FOUND_ROWS
    pv.product_seq,
    pv.pic_1_img AS image,
    p.name AS `name`,
    p.`merchant_seq` AS merchant_seq,
    vv.seq AS variant_seq,
    vv.value AS `variant_value`,
    pv.`product_price` AS `product_price`,
    pv.`sell_price` AS `sell_price`,
    pv.`seq` AS `product_variant_seq`,
    ps.`stock` AS `stock`,
    ps.`merchant_sku` AS `sku`,
    qry.promo_credit_name,
    qry2.pcredit_name,
    m.name as merchant_name,
    m.code AS merchant_code,
    m.to_date,
    IF((NOW() < m.from_date OR NOW() > m.to_date
            OR (m.from_date = '0000-00-00 00:00:00'
            AND m.to_date = '0000-00-00 00:00:00')),
        'O',
        'C') AS store_status
FROM
    m_product_variant pv
        LEFT JOIN
    m_product p ON pv.product_seq = p.seq
        JOIN
    m_merchant m ON m.seq = p.merchant_seq
        JOIN
    m_product_category mpcat ON mpcat.seq = p.category_l2_seq
        INNER JOIN
    m_variant_value vv ON pv.variant_value_seq = vv.seq
        LEFT JOIN
    m_product_stock ps ON ps.`product_variant_seq` = pv.`seq`
        LEFT JOIN
    (SELECT 
    pc.seq,
        pc.promo_credit_name,
            pc.minimum_nominal,
            pc.maximum_nominal,
            pcp.product_variant_seq
    FROM
        m_promo_credit_product pcp
    JOIN m_promo_credit pc ON pcp.promo_credit_seq = pc.seq
    WHERE
        pc.status = 'A'
            AND CURDATE() BETWEEN pc.promo_credit_period_from AND pc.promo_credit_period_to) AS qry ON qry.product_variant_seq = pv.seq
        AND pv.sell_price BETWEEN qry.minimum_nominal AND qry.maximum_nominal
        LEFT JOIN
    (SELECT 
     pc.seq,
        pc.promo_credit_name AS pcredit_name,
            pc.minimum_nominal AS pc_min_nominal,
            pc.maximum_nominal AS pc_max_nominal,
            mpcc.category_seq
    FROM
        m_promo_credit_category mpcc
    JOIN m_promo_credit pc ON mpcc.promo_credit_seq = pc.seq
    WHERE
        pc.status = 'A'
            AND CURDATE() BETWEEN pc.promo_credit_period_from AND pc.promo_credit_period_to) AS qry2 ON qry2.category_seq = mpcat.parent_seq
        AND pv.sell_price BETWEEN qry2.pc_min_nominal AND qry2.pc_max_nominal
	WHERE qry2.pcredit_name is not null " ,@SqlWhere,
        " group by product_variant_seq ",@SqlOrder," LIMIT ? , ? " );
PREPARE STMT FROM @sql;
SET @START = pSTART; 
SET @LIMIT = pLIMIT; 
EXECUTE STMT USING @START, @LIMIT;
DEALLOCATE PREPARE STMT;



END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_get_customer_agent_list*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_get_customer_agent_list`(
    pCustomerSeq INT
) 
BEGIN
SELECT
    customer.agent_seq,
    customer.identity_no,
    customer.identity_address,
    customer.birthday,
    customer.address,
    customer.email_address,
    customer.district_seq,
    customer.zip_code,
    customer.pic_name,
    customer.phone_no,
    distric.`name` as distric_name,
    city.`name` as city_name,
    province.`name` as province_name,
    customer.sub_district
FROM 
	m_agent_customer customer
LEFT JOIN
	m_district distric
ON
	customer.district_seq = distric.seq
LEFT JOIN 
	m_city city
ON 
	distric.city_seq = city.seq
LEFT JOIN
	m_province province
ON
	city.province_seq = province.seq
Where 
	customer.seq = pCustomerSeq; 
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_get_data_agent*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_get_data_agent`(
        `pIPAddr` VARCHAR(50),
        `pagentSeq` BIGINT(10)
    ) 
BEGIN
Select
	email,
	password as old_password,
	`name` as agent_name,
    CAST(Date_format(birthday,'%d-%b-%Y')as Char(25)) as birthday,
	gender,
	mobile_phone,
    profile_img
From 
	m_agent
Where 
	seq = pagentSeq;
Select
	ma.seq as address_seq,
    ma.alias,
	ma.address,
	ma.phone_no,
	d.seq as district_seq,
	d.name as district_name,
	c.seq as city_seq,
	c.name as city_name,
	p.seq as province_seq,
	p.name as province_name,
	ma.zip_code,
	ma.pic_name
From 
	m_agent m
Left Join 
	m_agent_address ma On m.seq = ma.agent_seq
Join 
	m_district d On ma.district_seq = d.seq
Left Join 
	m_city c On d.city_seq = c.seq
Left Join
	m_province p On c.province_seq = p.seq
Where
	m.seq = pagentSeq;
Select    
	bank_name,
	bank_branch_name,
	bank_acct_no,
	bank_acct_name,
	deposit_amt
From 
	m_agent
Where
	seq = pagentSeq;
Select
	email,
	password as old_password
From 
	m_agent
Where 
	seq = pagentSeq;
select
	agent_seq,
	trx_type, 
	trx_no, 
	trx_date,
    deposit_trx_amt, 
	non_deposit_trx_amt
From 
	t_agent_account
Where 
    agent_seq = pagentSeq;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_get_dropdown_category_name_lvl*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_get_dropdown_category_name_lvl`(
) 
BEGIN
SELECT 
	seq, 
	`name` AS category_name
FROM 
	m_product_category
WHERE 
	`level` = 2
ORDER BY 
	`name`;
    
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_get_dropdown_customer_search*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_get_dropdown_customer_search`(
        IN `pKeyword` VARCHAR(100),
        IN `pAgentSeq` VARCHAR(20),
        IN `pCustomerSeq` bigint(20)
    ) 
BEGIN
if pCustomerSeq=0 then
	Set @sqlCommand = Concat("Select * From m_agent_customer Where identity_no like '%",pKeyword,"%' or pic_name like '%",pKeyword,"%'");
else
	Set @sqlCommand = Concat("Select mac.*, md.name as kecamatan, md.city_seq, mc.name as kota, mc.province_seq, md.name as district_name, mc.name as city_name, mp.name as province_name From m_agent_customer mac left outer join m_district md on mac.district_seq=md.seq left outer join m_city mc on md.city_seq=mc.seq left outer join m_province mp on mc.province_seq = mp.seq Where mac.seq = '",pCustomerSeq,"'");
end if;
Prepare sql_stmt FROM @sqlCommand;
Execute sql_stmt;
Deallocate prepare sql_stmt;    

END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_get_dropdown_partner*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_get_dropdown_partner`(
) 
BEGIN
SELECT 
	distinct `name`
FROM 
	m_partner
ORDER BY 
	`name`;
    
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_get_dropdown_partner_dp_category*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_get_dropdown_partner_dp_category`() 
BEGIN
SELECT m_p_l.seq,
	m_p_l.product_category_seq,
	CONCAT(m_p.`name`,' - ',m_p_c.`name`) AS category_loan
	
	
FROM m_partner_loan m_p_l
JOIN m_product_category m_p_c ON m_p_c.seq=m_p_l.`product_category_seq`
JOIN m_partner m_p ON m_p.seq=m_p_l.partner_seq
GROUP BY m_p_l.product_category_seq
ORDER BY m_p_c.`name`;
    
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_get_dropdown_partner_dp_category_by_seq*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_get_dropdown_partner_dp_category_by_seq`(
	pPartnerSeq TINYINT UNSIGNED) 
BEGIN
SELECT m_p_l.seq,
	m_p_l.product_category_seq,
	m_p_c.`name`AS category_loan
	
	
FROM m_partner_loan m_p_l
JOIN m_product_category m_p_c ON m_p_c.seq=m_p_l.`product_category_seq`
JOIN m_partner m_p ON m_p.seq=m_p_l.partner_seq
WHERE
	partner_seq = pPartnerSeq 
GROUP BY m_p_l.product_category_seq
ORDER BY m_p_c.`name`;
    
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_get_dropdown_partner_dp_category_seq*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_get_dropdown_partner_dp_category_seq`(
	pPartnerSeq TINYINT UNSIGNED) 
BEGIN
SELECT m_p_l.seq,
	m_p_l.product_category_seq,
	m_p_c.`name`AS category_loan
	
	
FROM m_partner_loan m_p_l
JOIN m_product_category m_p_c ON m_p_c.seq=m_p_l.`product_category_seq`
JOIN m_partner m_p ON m_p.seq=m_p_l.partner_seq
WHERE
	partner_seq = pPartnerSeq AND 
    active = '1'
GROUP BY m_p_l.product_category_seq
ORDER BY m_p_c.`name`;
    
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_get_dropdown_partner_dp_get_partner_name*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_get_dropdown_partner_dp_get_partner_name`() 
BEGIN
SELECT m_p_l.seq,
	m_p_l.partner_seq,
	m_p.`name` AS partner_name
	
	
FROM m_partner_loan m_p_l
JOIN m_partner m_p ON m_p.seq=m_p_l.partner_seq
GROUP BY m_p_l.partner_seq
ORDER BY m_p.`name`;
    
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_get_dropdown_partner_dp_tenor*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_get_dropdown_partner_dp_tenor`(
	
) 
BEGIN
SELECT 	seq,
	tenor AS tenor_loan
	
FROM m_partner_loan
GROUP BY seq
ORDER BY 
	tenor;
    
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_get_dropdown_partner_dp_tenor_by_category*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_get_dropdown_partner_dp_tenor_by_category`(
	pProductCategorySeq INT UNSIGNED
) 
BEGIN
SELECT 	seq,
	tenor AS tenor_loan
	
FROM m_partner_loan
WHERE
	product_category_seq = pProductCategorySeq AND 
    active = '1'
ORDER BY 
	tenor;
    
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_get_dropdown_partner_loan*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_get_dropdown_partner_loan`(
) 
BEGIN
Select 
	seq, 
	`loan_interest` AS partner_loan
From 
	m_partner_loan
Order By 
	`loan_interest`;
    
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_get_dropdown_partner_name*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_get_dropdown_partner_name`(
) 
BEGIN
Select 
	seq, 
	`name` AS partner_name
From 
	m_partner
Order By 
	`name`;
    
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_get_info_loan*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_get_info_loan`(IN `pProductVariantSeq` BIGINT, IN `pTotal` DECIMAL(10,0), IN `pTenorSeq` SMALLINT, IN `pAgentSeq` BIGINT ) 
BEGIN
select
 mpl.*, 
 mp.admin_fee,
 cast((mpld.min_dp_percent * pTotal /100) as Unsigned)as min_dp_value
from 
	m_partner_loan mpl join 
		(select 
			ma.partner_seq, 
            mpart.admin_fee 
		 from m_agent ma join m_partner mpart on 
			ma.partner_seq=mpart.seq 
		  where ma.seq=pAgentSeq) mp on mpl.partner_seq = mp.partner_seq join 
	m_partner_loan_dp mpld on 
		mpld.partner_loan_seq = mpl.seq and
        pTotal between mpld.minimal_loan and mpld.maximal_loan
where 
	mpl.active='1' and 
    mpl.product_category_seq in (select 
									p.category_l2_seq 
								 from 
									m_product p join m_product_variant pv on 
                                    p.seq=pv.product_seq 
								 where pv.seq=pProductVariantSeq) 
order by mpl.tenor;
if pTenorSeq > 0 then
	select pld.*, mpl.loan_interest, mpl.tenor from m_partner_loan_dp pld join (select seq, loan_interest, tenor from m_partner_loan where active='1' and partner_seq in (select partner_seq from m_agent where seq=pAgentSeq) and product_category_seq in (select p.category_l2_seq from m_product p join m_product_variant pv on p.seq=pv.product_seq where pv.seq=pProductVariantSeq) and seq=pTenorSeq) mpl on pld.partner_loan_seq=mpl.seq where pld.active='1' and pTotal between pld.minimal_loan and pld.maximal_loan;
end if;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_get_loan_simulation*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_get_loan_simulation`(
	 pUserID VARCHAR(25),
	 pIPAddr VARCHAR(50),
	 pOrderSeq bigint(20)
) 
BEGIN
SELECT
  (SELECT SUM(qty*sell_price) FROM t_order_product WHERE order_seq=pOrderSeq AND product_status='R') AS product_price,
  ((SELECT SUM(qty*sell_price) FROM t_order_product WHERE order_seq=pOrderSeq AND product_status='R') - dp) AS total_loan,
  (SELECT SUM(total_ship_charged) FROM t_order_merchant WHERE order_seq=pOrderSeq) AS total_ship_charged,
  `order_seq`,
  `customer_seq`,
  `dp`,
  `admin_fee`,
  `tenor`,
  `loan_interest`,
  `installment`,
  `total_installment`,
  `status_order`,
  `created_by`,
  `created_date`,
  `modified_by`,
  `modified_date`
FROM `t_order_loan` WHERE order_seq=pOrderSeq;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_get_member_order_adira*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_get_member_order_adira`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pOrderSeq bigint(20)        
    ) 
BEGIN
    -- Begin SQL Where
    DECLARE sqlWhere VARCHAR(500);
    SET sqlWhere = "";
    IF pOrderSeq <> 0 THEN
        SET sqlWhere = CONCAT(sqlWhere, " And o.seq = '", pOrderSeq ,"'");
    END IF;
    SET @sqlCommand = "SELECT
o.seq, o.order_no AS order_no, o.order_date, o.created_date, a.name AS agent_name, ac.pic_name AS customer_name, ac.identity_address, ac.address, ac.birthday, ac.phone_no, ac.email_address, '' AS register, ac.identity_no, ac.zip_code, 
ac.sub_district, IF(pv.variant_value_seq='1',p.name,CONCAT(p.name,'-',v.name)) AS product_name,op.qty*op.sell_price AS product_price,ol.dp,ol.total_installment,ol.tenor
FROM t_order o
INNER JOIN t_order_loan ol ON o.seq=ol.order_seq
INNER JOIN m_agent a ON o.agent_seq = a.seq
INNER JOIN m_agent_customer ac ON ol.customer_seq=ac.seq
LEFT JOIN t_order_product op ON op.order_seq=o.seq
INNER JOIN m_product_variant pv ON op.product_variant_seq = pv.seq
INNER JOIN m_product p ON pv.product_seq = p.seq
LEFT JOIN m_variant v ON pv.variant_value_seq = v.seq";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @sqlCommand = NULL;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_get_merchant_promo_commission_by_seq*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_get_merchant_promo_commission_by_seq`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq SMALLINT(5) UNSIGNED
) 
BEGIN
    Select
	seq,
        period_from,
        period_to,
        all_merchant,
        notes,
        status,
        created_by,
        created_date,
        modified_by,
        modified_date
    From 
        m_promo_commission
    Where
	seq = pSeq;


END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_get_partner_order_by_status*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_get_partner_order_by_status`(      
        IN `pUserID` VARCHAR(20),      
        IN `pIpAddress` VARCHAR(20),
        IN `pPgMethodSeq` VARCHAR(20),
        IN `pPartnerSeq` INT,
        IN `pPaymentStatus` VARCHAR(2)
    ) 
BEGIN
	Set @sqlCommand = Concat("select t_o.*, m_a.name from t_order t_o join (select order_seq from t_order_loan where status_order='N') t_ol on t_o.seq=t_ol.order_seq join (select * from m_agent where partner_seq=",pPartnerSeq,") m_a on t_o.agent_seq=m_a.seq where t_o.pg_method_seq in (",pPgMethodSeq,") and t_o.payment_status='",pPaymentStatus,"' order by created_date");
Prepare sql_stmt FROM @sqlCommand;
Execute sql_stmt;
Deallocate prepare sql_stmt;    

END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_get_payment_gateway_method_list*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_get_payment_gateway_method_list`(
     pPaymentGatewaySeq int unsigned
) 
BEGIN
SELECT 
    pg_seq,
    seq,
    `code`,
    `name`,
    logo_img,
    `order`,
    notes,
    active,
    cancel_pg_method_seq
FROM
    m_payment_gateway_method
WHERE
    pg_seq = pPaymentGatewaySeq AND active = '1';
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_get_payment_gateway_type_list*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_get_payment_gateway_type_list`(
) 
BEGIN

SELECT
    seq,
    `name`,
    `order`,
    notes, 
    active,
    logo_img
FROM
    m_payment_gateway
WHERE
    active = '1'
order by `order`;

END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_login_agent*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_login_agent`(
        `pUserID` VARCHAR(100),
        `pPassword` VARCHAR(1000),
        `pIPAddress` VARCHAR(50)
    ) 
BEGIN
	DECLARE new_seq INT;
	DECLARE agent_seq INT;
	SELECT 
		MAX(seq) + 1 INTO new_seq
	FROM m_agent_login_log;    
    
    IF new_seq IS NULL THEN
		SET new_seq = 1;
	END IF;
    
    SELECT 
	A.seq,
	A.email,
	A.`name` AS user_name,
        A.`password`,
        A.profile_img,
        A.last_login,
        P.`name` AS partner_name,
        A.partner_seq
	FROM m_agent A inner join m_partner P on A.partner_seq=P.seq
	WHERE A.email = pUserID AND A.`status` = 'A';
        
IF EXISTS (SELECT seq FROM m_agent WHERE email = pUserID AND `password` = pPassword AND `status`='A') THEN
    BEGIN
		UPDATE m_agent SET
			last_login = NOW(),
			ip_address = pIPAddress
		WHERE
			email = pUserID;
			
		SELECT 
			seq INTO agent_seq
		FROM 
			m_agent
		WHERE
			email = pUserID;
		
        INSERT INTO m_agent_login_log (
			seq,
            agent_seq,
            ip_address,
            STATUS,
            created_date
		) VALUES (
			new_seq,
            agent_seq,
            pIPAddress,
            'S',
            NOW()
		);
    END;
ELSE
	IF EXISTS (SELECT seq FROM m_agent WHERE email = pUserID) THEN
		BEGIN
			SELECT 
				seq INTO agent_seq
			FROM 
				m_agent
			WHERE
				email = pUserID;
			
			INSERT INTO m_agent_login_log (
				seq,
				agent_seq,
				ip_address,
				STATUS,
				created_date
			) VALUES (
				new_seq,
				agent_seq,
				pIPAddress,
				'F',
				NOW()
			);
            
		END;
	END IF;
END IF;
        
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_member_order_list*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_member_order_list`(
    pUserID VarChar(100),
    pIPAddr VarChar(50),
    pMemberSeq BigInt(10),
    pCurrPage Int unsigned,
    pRecPerPage Int unsigned,
    pDirSort VarChar(4),
    pColumnSort Varchar(50),
    pOrderNo VarChar(50),
    pDateSearch Char(1),
    pOrderDateFrom Datetime,
    pOrderDateTo Datetime,
    pPaymentStatus Char(1)
) 
BEGIN
    
    Declare startRec Int unsigned;
    Declare totalPage Int unsigned;
    Declare totalRec Int unsigned;
    
    
    Declare sqlWhere VarChar(500);
	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
    If pOrderNo <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And o.order_no like '%" , escape_string(pOrderNo), "%'");
    End If;
    If (pOrderDateFrom <> "") Then
        Set sqlWhere = Concat(sqlWhere, " And o.order_date >= '" , pOrderDateFrom,"'");
    End If;
    If (pOrderDateTo <> "") Then
        Set sqlWhere = Concat(sqlWhere, " And o.order_date <= '" , pOrderDateTo,"'");
    End If;
    If pPaymentStatus <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And o.payment_status ='" , pPaymentStatus, "'");
    End If;
    
    
    Set @sqlCommand = "Select Count(o.order_no) Into @totalRec
            From
            (select order_no, order_date, payment_status, member_seq from m_member m Join t_order o On m.seq = o.member_seq
             union all
             select order_no, order_date, payment_status, member_seq from m_member m Join t_order_voucher o On m.seq = o.member_seq) as o "
        ;
    Set @sqlCommand = Concat(@sqlCommand, " Where o.member_seq ='" ,pMemberSeq, "'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);
        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;
        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    
    Set @sqlCommand = "
        Select
            o.seq,
            o.member_name,
            o.order_no,
            o.order_date,
            o.total_order,
            o.payment_status,
            o.paid_date,
            o.payment_method,
            '' as detail,
            '' as payment
            From
        (
        select
            o.seq,
            o.member_seq,
            m.name as member_name,
            o.order_no,
            o.order_date,
            o.total_order,
            o.payment_status,
            o.paid_date,
            pgm.name as payment_method,
            '' as detail,
            '' as payment
		from
                    m_member m
		Join
                    t_order o On m.seq = o.member_seq
		Join
        	    m_payment_gateway_method pgm On o.pg_method_seq = pgm.seq
		union all
		select
                    o.seq,
                    o.member_seq,
                    m.name as member_name,
                    o.order_no,
                    o.order_date,
                    o.total_order,
                    o.payment_status,
                    o.created_date,
                    pgm.name as payment_method,
                    '' as detail,
                    '' as payment
		from
			m_member m
		Join
			t_order_voucher o On m.seq = o.member_seq
		Join
			m_payment_gateway_method pgm On o.pg_method_seq = pgm.seq
		) as o
		";
    Set @sqlCommand = Concat(@sqlCommand, " Where o.member_seq ='" ,pMemberSeq, "'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    Select pCurrPage, totalPage, totalRec as total_rec;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set @totalRec = Null;
    Set @sqlCommand = Null;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_member_order_list_return_by_member*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_member_order_list_return_by_member`(  
        pUserID VarChar(100),
        pIPAddr VarChar(50),
        pMemberSeq int unsigned,
        pOrderStatus char(1),
        pFinishDate SMALLINT(1) unsigned
    ) 
BEGIN
select
    o.seq,
    o.member_seq,
    m.name as member_name,
    o.order_no,
    o.order_date,
    o.total_order,
    o.payment_status,
    o.paid_date,
    pgm.name as payment_method
            from
                    m_member m
            Join
                    t_order o 
                    On m.seq = o.member_seq
            Join 
                    m_payment_gateway_method pgm
                    On o.pg_method_seq = pgm.seq
    Where 
    o.seq in 
            ( select tom.order_seq from t_order_merchant tom join t_order tor on tom.`order_seq`=tor.`seq` where 
            tom.received_date >= date_add( CURDATE(),interval - pFinishDate day) and tom.received_date<>'0000-00-00' AND 
            tom.order_status=pOrderStatus and tor.member_seq= pMemberSeq ) 
    and  o.member_seq= pMemberSeq ;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_m_agent_add*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_m_agent_add`(
        `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pEmail` VARCHAR(50),
        `pName` VARCHAR(100),
        `pDate` DATE,
        `pGender` VARCHAR(1),
        `pPhone` VARCHAR(20),
        `pPassword` VARCHAR(100),
        `pImage` VARCHAR(100),
        `pPartnerSeq` INTEGER(10),
        `pStatus` CHAR(1)
    ) 
BEGIN
	DECLARE new_seq INT UNSIGNED;
	SELECT 
        MAX(seq) + 1 INTO new_seq
    FROM m_agent;
    IF new_seq IS NULL THEN
        SET new_seq = 1;
    END IF;
INSERT INTO m_agent(
    seq,
    email,
    password,
    name,
    partner_seq,
    referral,
    birthday,
    gender,
    mobile_phone,
    bank_name,
    bank_branch_name,
    bank_acct_no,
    bank_acct_name,
    profile_img,
    deposit_amt,
    status,
    last_login,
    ip_address,
    created_by,
    created_date,
    modified_by,
    modified_date
)VALUES(
    new_seq,
    pEmail,
    pPassword,
    pName,
    pPartnerSeq,
    NULL,
    pDate,
    pGender,
    pPhone,
	'',
    '',
    '',
    '',
    pImage,
    '0',
    pStatus,
    NULL,
    NULL,
    pUserID,
    now(),
    pUserID,
    now()
);
select
new_seq;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_m_agent_commission_list*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_m_agent_commission_list`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pAgentSeq BigInt(10),
        pPaymentStatus Char(1),
        pDateSearch Char(1),
		pOrderDateFrom Datetime,
		pOrderDateTo Datetime
) 
BEGIN
  Declare sqlWhere VarChar(500); 
  Set sqlWhere = "";
      If (pDateSearch = "0")  Then
        Set sqlWhere = Concat(sqlWhere, " And tor.order_date between '" , pOrderDateFrom, "' and '" ,pOrderDateTo, "'");
	  End If;
       If pPaymentStatus = "P" Then
        Set sqlWhere = Concat(sqlWhere, " And tom.order_status = 'D' ");
    End If;
    If pPaymentStatus = "U" Then
        Set sqlWhere = Concat(sqlWhere, " And tom.order_status in ('R','S') ");
    End If;
	Set @sqlCommand = "
        SELECT 
			tor.seq,
			tor.agent_seq,
			tor.order_no,
			tor.order_date,
			tor.receiver_name,
			tor.receiver_address,
			tor.receiver_district_seq,
			tor.receiver_zip_code,
			tor.receiver_phone_no,
            tor.order_date,
			tom.order_status,
			tom.received_date,
			top.product_variant_seq,
			top.variant_value_seq,
			top.qty,
			top.sell_price,
			top.weight_kg,
			top.ship_price_real,
			top.commission_fee_percent,
			md.`name` district_name,
			mc.`name` city_name,
			mp.`name` province_name,
			mpr.`name` AS display_name,
			mpr.merchant_seq,
			mpv.seq AS product_seq,
			mpv.pic_1_img AS img,
			mv.`value`,
			mv.seq AS value_seq,
			mv.`value` AS variant_name
		FROM
			t_order tor
				JOIN
			t_order_merchant tom ON tor.seq = tom.order_seq
				JOIN
			t_order_product top ON tor.seq = top.order_seq
				AND tom.merchant_info_seq = top.merchant_info_seq
				AND top.order_seq = tom.order_seq
				JOIN
			m_district md ON tor.receiver_district_seq = md.seq
				JOIN
			m_city mc ON mc.seq = md.city_seq
				JOIN
			m_province mp ON mp.seq = mc.province_seq
				JOIN
			m_product_variant mpv ON mpv.seq = top.product_variant_seq
				JOIN
			m_product mpr ON mpv.product_seq = mpr.seq
				JOIN
			m_variant_value vv ON vv.seq = top.variant_value_seq
				JOIN
			m_variant_value mv ON mv.seq = mpv.variant_value_seq
		";
	Set @sqlCommand = Concat(@sqlCommand, " Where tor.agent_seq ='" ,pAgentSeq, "' and tor.payment_status='P' and top.product_status='R' ");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    Set @sqlCommand = Concat(@sqlCommand, " order by ", "tor.order_date", " DESC" );
    
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set @sqlCommand = Null;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_m_agent_list_partner*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_m_agent_list_partner`(
		`pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pCurrPage` INTEGER UNSIGNED,
        `pRecPerPage` INTEGER UNSIGNED,
        `pEmail` VARCHAR(100),
        `pName` VARCHAR(100),
        `pStatus` CHAR(3),
        `pPartnerSeq` INTEGER(10)
) 
BEGIN
  Declare sqlWhere VarChar(500); 
  Set sqlWhere = "";
	   IF pEmail <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And email like '%" , escape_string(pEmail), "%'");
    END IF;
    
    IF pName <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And a.name like '%" , escape_string(pName), "%'");
    END IF;
    
    IF pStatus <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And status = '" , pStatus, "'");
    END IF;
    IF pPartnerSeq <> "" THEN
         SET sqlWhere = CONCAT(sqlWhere, " And partner_seq = '" , pPartnerSeq, "'" );
     END IF;
	Set @sqlCommand = "
        SELECT 
		  a.`seq` as agent_seq,
		  a.`email`,
		  a.`password`,
		  a.`name`,
		  a.`partner_seq`,
		  a.`referral`,
		  a.`birthday`,
		  a.`gender`,
		  a.`mobile_phone`,
		  a.`profile_img`,
		  a.`deposit_amt`,
		  a.`status`,
		  a.`last_login`,
		  a.`ip_address`,
		  a.`created_by`,
		  a.`created_date`,
		  a.`modified_by`,
		  a.`modified_date`,
		  aa.`seq` as agent_address_seq,
		  aa.`alias`,
		  aa.`address`,
		  aa.`district_seq`,
		  aa.`zip_code`,
		  aa.`pic_name`,
		  aa.`phone_no`,
		  aa.`default`,
		  aa.`active`,
		  d.`name` AS district,
		  c.`name` AS city,
		  p.`name` AS province
		FROM `m_agent` AS a
		LEFT JOIN ( SELECT `agent_seq`, `seq`, `alias`, `address`, `district_seq`, `zip_code`, `pic_name`, `phone_no`, `default`, `active`, `created_by`, `created_date`, `modified_by`, MAX(`modified_date`) AS `modified_date` FROM `m_agent_address` GROUP BY agent_seq) AS aa
		ON a.`seq` = aa.`agent_seq` 
		LEFT JOIN m_district d ON aa.district_seq = d.seq 
		LEFT JOIN m_city c ON d.city_seq = c.seq
		LEFT JOIN m_province p ON c.province_seq = p.seq 
		";
    Set @sqlCommand = Concat(@sqlCommand, " Where a.partner_seq ='" ,pPartnerSeq, "'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    Set @sqlCommand = Concat(@sqlCommand, " order by ", "a.created_date", " DESC" );
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", pCurrPage, ", ", pRecPerPage);
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set @sqlCommand = Null;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_m_agent_order_list*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_m_agent_order_list`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pAgentSeq BigInt(10),
		pCurrPage Int unsigned,
		pRecPerPage Int unsigned,
        pOrderNo VarChar(50), 
        pPaymentStatus Char(1),
        pDateSearch Char(1),
		pOrderDateFrom Datetime,
		pOrderDateTo Datetime,
        pIdentityNo varchar(100)
) 
BEGIN
  Declare sqlWhere VarChar(500); 
  Set sqlWhere = "";
	  If pOrderNo <> "" Then
		   Set sqlWhere = Concat(sqlWhere, " And o.order_no like '%" , escape_string(pOrderNo), "%'");
	  End If;
      If (pDateSearch = "0")  Then
        Set sqlWhere = Concat(sqlWhere, " And o.order_date between '" , pOrderDateFrom, "' and '" ,pOrderDateTo, "'");
	  End If;
	  If pPaymentStatus <> "" Then
		   Set sqlWhere = Concat(sqlWhere, " And o.payment_status ='" , pPaymentStatus, "'");
	  End If;
	IF pIdentityNo <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And o.identity_no like '%" , escape_string(pIdentityNo), "%'");
    END IF;
	Set @sqlCommand = "
         SELECT 
    o.seq,
    o.agent_name,
    o.order_no,
    o.order_date,
    o.total_order,
    o.payment_status,
    o.paid_date,
    o.payment_method,
    '' AS detail,
    '' AS payment,
    o.`identity_no`,
    o.`pic_name`,
    o.dp,
    o.admin_fee,
    o.tenor,
    o.loan_interest,
    o.installment,
    o.total_installment,
    o.status_order
FROM
    (SELECT 
        o.seq,
            o.agent_seq,
            m.name AS agent_name,
            o.order_no,
            o.order_date,
            o.total_order,
            o.payment_status,
            o.paid_date,
            pgm.name AS payment_method,
            '' AS detail,
            '' AS payment,
             c.`identity_no`,
             c.pic_name,
			l.dp,
			l.admin_fee,
			l.tenor,
			l.loan_interest,
			l.installment,
			l.total_installment,
			l.status_order
    FROM
        m_agent m
    JOIN t_order o ON m.seq = o.agent_seq
    JOIN m_payment_gateway_method pgm ON o.pg_method_seq = pgm.seq
    LEFT JOIN t_order_loan l ON l.order_seq = o.seq
    LEFT JOIN m_agent_customer c ON c.seq = l.`customer_seq`) AS o
		";
    Set @sqlCommand = Concat(@sqlCommand, " Where o.agent_seq ='" ,pAgentSeq, "'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    Set @sqlCommand = Concat(@sqlCommand, " order by ", "o.order_date", " DESC" );
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", pCurrPage, ", ", pRecPerPage);
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set @sqlCommand = Null;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_m_agent_order_product_return_by_order_no*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_m_agent_order_product_return_by_order_no`(
    pUserID VarChar(100),
    pIPAddr VarChar(50),
    pAgentSeq int unsigned,
    pProductStatus char(1),
    pOrderStatus char(1),
    pFinishDate int(1) unsigned
) 
BEGIN
SELECT 
    o.order_no,
    o.agent_seq,
    o.seq,
    p.seq AS pro_seq,
    pv.pic_1_img AS img,
    pv.variant_value_seq,
    op.product_variant_seq,
    p.name AS product_name,
    vv.value AS variant_name,
    op.qty,
    pv.max_buy,
    op.sell_price,
    m.name AS merchant_name,
    m.seq AS merchant_seq
FROM
    t_order o
        JOIN
    t_order_merchant om ON o.seq = om.order_seq
        JOIN
    t_order_product op ON om.order_seq = op.order_seq
        AND om.merchant_info_seq = op.merchant_info_seq
        JOIN
    m_product_variant pv ON op.product_variant_seq = pv.seq
        JOIN
    m_product p ON pv.product_seq = p.seq
        JOIN
    m_variant_value vv ON pv.variant_value_seq = vv.seq
        JOIN
    m_merchant_info mi ON op.merchant_info_seq = mi.seq
        JOIN
    m_merchant m ON mi.merchant_seq = m.seq
WHERE
    om.received_date >= DATE_ADD(CURDATE(),
        INTERVAL - pFinishDate DAY)
        AND o.agent_seq = pAgentSeq
        AND op.product_status = pProductStatus
        AND om.order_status = pOrderStatus
 ORDER BY om.received_date DESC;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_m_member_order_product_return_by_order_no*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_m_member_order_product_return_by_order_no`(
    pUserID VarChar(100),
    pIPAddr VarChar(50),
    pMemberSeq int unsigned,
    pOrderNo varchar(50),
    pProductStatus char(1),
    pOrderStatus char(1),
    pFinishDate int(1) unsigned
) 
BEGIN
SELECT 
    o.order_no,
    o.member_seq,
    o.seq,
    p.seq AS pro_seq,
    pv.pic_1_img AS img,
    pv.variant_value_seq,
    op.product_variant_seq,
    p.name AS product_name,
    vv.value AS variant_name,
    op.qty,
    pv.max_buy,
    op.sell_price,
    m.name AS merchant_name,
    m.seq AS merchant_seq
FROM
    t_order o
        JOIN
    t_order_merchant om ON o.seq = om.order_seq
        JOIN
    t_order_product op ON om.order_seq = op.order_seq
        AND om.merchant_info_seq = op.merchant_info_seq
        JOIN
    m_product_variant pv ON op.product_variant_seq = pv.seq
        JOIN
    m_product p ON pv.product_seq = p.seq
        JOIN
    m_variant_value vv ON pv.variant_value_seq = vv.seq
        JOIN
    m_merchant_info mi ON op.merchant_info_seq = mi.seq
        JOIN
    m_merchant m ON mi.merchant_seq = m.seq
WHERE
    om.received_date >= DATE_ADD(CURDATE(),
    INTERVAL - pFinishDate DAY)
    AND o.member_seq = pMemberSeq
    AND op.product_status = pProductStatus
    AND om.order_status = pOrderStatus
 ORDER BY om.received_date DESC;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_order_agent_partner_by_redeem*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_order_agent_partner_by_redeem`(
        `pUserID` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pPaymentCode` CHAR(10),
        `pSeq` INTEGER(10) UNSIGNED,
        `pPartnerSeq` INTEGER(10) UNSIGNED
    ) 
BEGIN
    select tor.order_no, tor.total_payment, tol.`total_installment`, tor.order_date from `t_order` tor
    join m_agent ma on tor.agent_seq=ma.seq
    left outer join t_order_loan tol on tor.seq=tol.order_seq
    where tor.`pg_method_seq` in
    (select seq from m_payment_gateway_method where pg_seq in (select seq from m_payment_gateway where `name`=pPaymentCode)) and tor.member_seq is null and ma.partner_seq=pPartnerSeq and tor.seq in
    (select tom.order_seq from t_order_merchant tom where tom.order_status='D' and tom.redeem_agent_seq =pSeq)
    group by tor.order_no;

END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_order_by_seq*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_order_by_seq`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
	pSeq smallint unsigned
) 
BEGIN
    Select 
        o.seq,
        o.order_no, 
        o.order_date, 
        o.member_seq,
        o.receiver_name, 
        o.receiver_address,
        o.receiver_district_seq,
        o.receiver_zip_code,
        o.receiver_phone_no,
        o.payment_status,
        o.payment_status as order_payment_status,
        o.pg_method_seq,
        o.paid_date,
        o.payment_retry,
        o.total_order,
        o.voucher_seq, 
        o.voucher_refunded,
        o.total_payment,
        o.conf_pay_type,
        o.conf_pay_amt_member,
        o.conf_pay_date, 
        o.conf_pay_note_file, 
        o.conf_pay_bank_seq,
        o.conf_pay_amt_admin,
        case when m.`name` is null then concat(a.name, ' Partner: ', p.name) else m.name end as member_name,
        pg.`name` as pg_name,
        case when v.nominal is null then c.nominal else v.nominal end as nominal,
        b.logo_img,
        b.bank_acct_no,
        b.bank_acct_name,
        m.email email_member,
        pg.`name` as pg_name,
        a.email as email_agent,
        a.seq as agent_seq,
        tl.customer_seq,
        tl.dp,
        tl.admin_fee,
        tl.tenor,
        tl.loan_interest,
        tl.installment,
        tl.total_installment,
        tl.status_order,
        o.agent_seq
	From t_order o
		left join m_member m on
			o.member_seq = m.seq
                left join m_agent a on 
                        o.`agent_seq`= a.seq
		left join m_promo_voucher v on 
                        o.voucher_seq = v.seq
		left join m_coupon c on 
                        o.coupon_seq = c.seq
		left join m_bank_account b on	
			o.conf_pay_bank_seq = b.seq
		left join m_payment_gateway_method pg on
			o.pg_method_seq = pg.seq
                left join t_order_loan tl on 
			tl.order_seq = o.seq
		left join m_partner p on 
			a.partner_seq = p.seq
	Where
		o.seq = pSeq;
        
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_order_list*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_order_list`(
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pCurrPage Int,
    pRecPerPage Int,
    pDirSort VarChar(4),
    pColumnSort Varchar(50),
    pOrderNo VARCHAR(50),
    pSDate Varchar(15),
    pEDate Varchar(15),
	pPaymentStatus char(1),
    pPgMethodSeq char(1)
) 
BEGIN
    -- Begin variables for Paging
    Declare startRec Int;
    Declare totalPage Int;
    Declare totalRec Int;
    -- End variables for Paging
    -- Begin SQL Where
    Declare sqlWhere VarChar(500);
	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
      
   If pOrderNo <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And o.order_no like '%" , escape_string(pOrderNo), "%'");
    End If;
    
	If pSDate <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And o.order_date >='" , pSDate , "'");
    End If;
	
    If pEDate <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And o.order_date <='" , pEDate , "'");
    End If;
    
    If pPaymentStatus <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And o.payment_status like '%" , pPaymentStatus, "%'");
    End If;
    
    If pPgMethodSeq <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And o.pg_method_seq like '%" , pPgMethodSeq, "%'");
    End If;
    -- End SQL Where
    -- Begin Paging Info
    Set @sqlCommand = "Select Count(o.order_no) Into @totalRec 
			from (
            select
				order_no,
                order_date,
                payment_status,
                pg_method_seq
			From t_order o LEFT JOIN m_member m ON
        o.member_seq = m.seq
        LEFT JOIN m_agent a ON o.`agent_seq`=a.seq
            union all
            select
				order_no,
				order_date,
                payment_status,
                pg_method_seq
			from t_order_voucher v join m_member m on m.seq = v.member_seq) as o ";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);
        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;
        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info
    Set @sqlCommand = "
		Select
            o.seq,
            o.order_no,
            o.order_date,
            o.member_seq,
            o.member_name,
            o.pg_method_seq,
            o.total_order,
            o.voucher_seq,
            o.total_payment,
            o.payment_status,
            o.conf_pay_amt_member,
            o.payment_name,
            o.voucher_nominal
        from (        
        Select
            o.seq,
            o.order_no,
            o.order_date,
            o.member_seq,
            case when m.name is null then a.name else m.name end as member_name,
            o.pg_method_seq,
            o.total_order,
            o.voucher_seq,
            o.total_payment,
            o.payment_status,
            o.conf_pay_amt_member,
            pg.`name` payment_name,
            pv.nominal as voucher_nominal
		From t_order o LEFT JOIN m_member m ON
        o.member_seq = m.seq
        LEFT JOIN m_agent a ON o.`agent_seq`=a.seq
        join m_payment_gateway_method pg on
        o.pg_method_seq = pg.seq
        left join m_promo_voucher pv on
        o.voucher_seq = pv.seq
		union all
		select
			p.seq,
            p.order_no,
            p.order_date,
            p.member_seq,
            m.name as member_name,
            p.pg_method_seq,
            p.total_order,
            0,
            p.total_payment,
            p.payment_status,
            p.total_payment,
            pg.`name` payment_name,
            0
		From t_order_voucher p join m_member m on
        p.member_seq = m.seq
        join m_payment_gateway_method pg on
        p.pg_method_seq = pg.seq) as o 
    ";
    If sqlWhere <> "" Then
		Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;
    
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    Select pCurrPage, totalPage, totalRec as total_rec;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set @totalRec = Null;
    Set @sqlCommand = Null;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_order_list_cancel*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_order_list_cancel`(
    pUserID VarChar(25),
    pIPAddr VarChar(50)
) 
BEGIN
Select
		o.seq,
		o.order_no,
		o.order_date,
		o.member_seq,
		CASE WHEN m.`name` IS NULL THEN m_a.`name` ELSE m.`name` END AS member_name,
		CASE WHEN m.`email` IS NULL THEN m_a.`email` ELSE m.`email` END AS email_member,
		o.pg_method_seq,
		o.total_order,
		o.voucher_seq,
		o.total_payment,
		o.payment_status,
		o.conf_pay_amt_member,
		pg.`name` payment_name,
		pv.nominal as voucher_nominal
From t_order o LEFT JOIN m_member m on
	o.member_seq = m.seq
LEFT JOIN m_agent m_a ON 
	o.agent_seq = m_a.seq
join m_payment_gateway_method pg on
	o.pg_method_seq = pg.seq
left join m_promo_voucher pv on
			o.voucher_seq = pv.seq
where order_date ='2016-11-24' AND (payment_status = 'U' or payment_status = 'F') order by order_date limit 1 ;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_order_loan_add*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_order_loan_add`(IN `pIPAddress` VARCHAR(25), IN `pUserID` VARCHAR(50), IN `pOrderSeq` BIGINT, IN `pCustomerSeq` INT, IN `pDp` DECIMAL(10,0), IN `pAdminFee` DECIMAL(10,0), IN `pTenor` INT, IN `pLoanInterest` DECIMAL(5,2), IN `pInstallment` DECIMAL(10,0), IN `pTotalInstallment` DECIMAL(10,0)) 
BEGIN
insert into t_order_loan (

	order_seq,

    customer_seq,

    dp,

    admin_fee,

    tenor,

    loan_interest,

    installment,

    total_installment,

    created_by,

    created_date,

    modified_by,

    modified_date

) values (

	pOrderSeq,

    pCustomerSeq,

    pDp,

    pAdminFee,

    pTenor,

    pLoanInterest,

    pInstallment,

    pTotalInstallment,

    pUserID,

    now(),

    pUserID,

    now()

);

END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_order_merchant_add*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_order_merchant_add`(
	
    pUserID VarChar(25),
    pIPAddress VarChar(25),
    pOrderSeq BigInt unsigned,
    pMerchantSeq Int unsigned,
    pExpRealServiceSeq tinyint unsigned,
    pExpServiceSeq tinyint unsigned,
    pFreeFeeSeq MediumInt unsigned,
    pOrderStatus char(1),
    pMemberNotes Varchar(100),
    pRefAWBNo varchar(52)    	
) 
BEGIN
 
 declare pMerchantInfoSeq MediumInt unsigned;
 declare pRealExpeditionServiceMerchantSeq tinyint unsigned;
 
 select max(seq) into pMerchantInfoSeq
 from 
	m_merchant_info
 where 
	merchant_seq = pMerchantSeq;
 
 select es.seq into pRealExpeditionServiceMerchantSeq
 from
	m_merchant m join m_expedition e
		on e.seq = m.expedition_seq
				 join m_expedition_service es
		on es.exp_seq = e.seq
where
	m.seq = pMerchantSeq limit 1;
 
 if pFreeFeeSeq = 0 then
	set pFreeFeeSeq = null;
 end if;
 
if pRealExpeditionServiceMerchantSeq = 0 Then
	Set  pExpServiceSeq = pRealExpeditionServiceMerchantSeq;
    Set  pExpRealServiceSeq = pRealExpeditionServiceMerchantSeq;
 end if;
 
 insert into t_order_merchant (
	order_seq,
    merchant_info_seq,
    expedition_service_seq,
    real_expedition_service_seq,
    free_fee_seq,
    order_status,
    member_notes,
    awb_no,
    ref_awb_no,
    ship_by,
    ship_by_exp_seq,
    ship_note_file,
    ship_notes,
    received_by,
    created_by,
    created_date,
    modified_by,
    modified_date
) Values (
	pOrderSeq,
    pMerchantInfoSeq,
    pExpServiceSeq,
    pExpRealServiceSeq,
    pFreefeeSeq,
    pOrderStatus,
    pMemberNotes,
    '',
    pRefAWBNo,
    '',
    NULL,
    '',
    '',
    '',
    pUserID,
    now(),
    pUserID,
    now()
);
	
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_order_product_add*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_order_product_add`(
  `pUserID` VARCHAR(25),
  `pIPAddress` VARCHAR(25),
  `pMerchantSeq` INTEGER UNSIGNED,
  `pOrderSeq` BIGINT(20) UNSIGNED,
  `pProductVariantSeq` MEDIUMINT UNSIGNED,
  `pVariantValueSeq` SMALLINT UNSIGNED,
  `pQty` INTEGER UNSIGNED,
  `pSellPrice` DECIMAL(10,0),
  `pWeightKg` DECIMAL(7,2),
  `pShipPriceReal` DECIMAL(10,0),
  `pShipPriceCharged` DECIMAL(10,0),
  `pProductStatus` CHAR(1),
  `pAgentSeq` BIGINT(20) UNSIGNED
) 
BEGIN
DECLARE pTrxFeePercent DECIMAL (4,2);
DECLARE pInsRatePercent DECIMAL(4,2) ;
DECLARE pMerchantInfoSeq MEDIUMINT UNSIGNED;
DECLARE pTrxCommissionPercent DECIMAL (4,2);
DECLARE pTrxCommissionPartnerPercent DECIMAL (4,2);
SELECT 
  CASE
    WHEN m.trx_fee_percent IS NULL 
    THEN c.trx_fee_percent 
    ELSE m.trx_fee_percent 
  END INTO pTrxFeePercent 
FROM
  m_product p 
  JOIN m_product_variant v 
    ON v.product_seq = p.seq 
  JOIN m_product_category c 
    ON c.seq = p.category_l2_seq 
  LEFT JOIN m_merchant_trx_fee m 
    ON m.category_l2_seq = c.seq 
    AND m.merchant_seq = p.merchant_seq 
WHERE v.seq = pProductVariantSeq ;	
	
SELECT 
  MAX(seq) INTO pMerchantInfoSeq 
FROM
  m_merchant_info 
WHERE merchant_seq = pMerchantSeq ;
SELECT 
  e.ins_rate_percent INTO pInsRatePercent 
FROM
  t_order_merchant m 
  JOIN m_expedition_service s 
    ON s.seq = m.expedition_service_seq 
  JOIN m_expedition e 
    ON e.seq = s.exp_seq 
WHERE m.order_seq = pOrderSeq 
  AND m.merchant_info_seq = pMerchantInfoSeq ;
IF pAgentSeq = "" THEN 
  SET pTrxCommissionPercent = 0 ;
  SET pTrxCommissionPartnerPercent = 0 ;
ELSE
SELECT 
  CASE
    WHEN m.commission_fee_percent IS NULL 
    THEN c.commission_fee_percent 
    ELSE m.commission_fee_percent 
  END,
  c.commission_fee_percent
  INTO pTrxCommissionPercent, pTrxCommissionPartnerPercent
FROM
  m_product p 
  JOIN m_product_variant v 
    ON v.product_seq = p.seq 
  JOIN m_product_category c 
    ON c.seq = p.category_l2_seq 
  LEFT JOIN 
    (SELECT 
      commission_fee_percent,
      category_l2_seq,
      agent_seq 
    FROM
      m_agent_trx_commission 
    WHERE agent_seq = pAgentSeq) m 
    ON m.category_l2_seq = c.seq 
WHERE v.seq = pProductVariantSeq ;
END IF;
INSERT INTO t_order_product (
  order_seq,
  merchant_info_seq,
  product_variant_seq,
  variant_value_seq,
  qty,
  sell_price,
  weight_kg,
  ship_price_real,
  ship_price_charged,
  trx_fee_percent,
  ins_rate_percent,
  product_status,
  commission_fee_percent,
  commission_fee_partner_percent,
  created_by,
  created_date,
  modified_by,
  modified_date
) 
VALUES
  (
    pOrderSeq,
    pMerchantInfoSeq,
    pProductVariantSeq,
    pVariantValueSeq,
    pQty,
    pSellPrice,
    pWeightKg,
    pShipPriceReal,
    pShipPriceCharged,
    pTrxFeePercent,
    pInsRatePercent,
    pProductStatus,
    pTrxCommissionPercent,
    pTrxCommissionPartnerPercent,
    pUserID,
    NOW(),
    pUserID,
    NOW()
  ) ;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_order_update_payment_method*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_order_update_payment_method`(
	
    pUserID Varchar(25),
    pIPAddress Varchar(50),
    pOrderNo Varchar(50),
    pPaymentMethodSeq Varchar(50)
) 
BEGIN
update t_order set
	pg_method_seq = pPaymentMethodSeq
Where
	order_no = substring(pOrderNo,1,18) and payment_status in ('W','U');
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_partner_add_signup*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_partner_add_signup`(
    pUserID VARCHAR(100),
    pIPAddr VARCHAR(50),
    pEmail VARCHAR(50),
    pName VARCHAR(100),
    pPassword VARCHAR(1000),
    pMobilePhone VARCHAR(50),
    pAdminFee VARCHAR(50),
    pStatus VARCHAR(1),
    pBankName VARCHAR(50),
    pBankBranchName VARCHAR(50),
    pBankAcctNo VARCHAR(50),
    pBankAcctName VARCHAR(50),
    pProfileImg VARCHAR(50)
) 
BEGIN
    DECLARE new_seq INT UNSIGNED;
    SELECT 
      MAX(seq) + 1 INTO new_seq
    FROM m_partner;
    IF new_seq IS NULL THEN
      SET new_seq = 1;
    END IF;
INSERT INTO `m_partner`
            (`seq`,
             `email`,
             `name`,
             `password`,
             `mobile_phone`,
             `admin_fee`,
             `profile_img`,
             `status`,
             `bank_name`,
             `bank_branch_name`,
             `bank_acct_no`,
             `bank_acct_name`,
             `last_login`,
             `ip_address`,
             `created_by`,
             `created_date`,
             `modified_by`,
             `modified_date`)
VALUES (new_seq,
        pEmail,
        pName,
        pPassword,
        pMobilePhone,
        pAdminFee,
        pProfileImg,
        pStatus,
        pBankName,
        pBankBranchName,
        pBankAcctNo,
        pBankAcctName,
        NULL,
        pIPAddr,
        pUserID,
        NOW(),
        pUserID,
        NOW());
SELECT new_seq;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_partner_commision_data*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_partner_commision_data`(
	pUserID VARCHAR(25),
	pIPAddr VARCHAR(50),
	pSeq INT UNSIGNED
) 
BEGIN
	SELECT
	  a.`seq`,
	  a.`name`,
	  a.`commission_fee_percent`,
	  b.hseq,
	  b.hname,
	  c.mcatlvl1,
	  d.mcatlvl2,
	  d.m_commision  
	FROM `m_product_category` a  
	JOIN (
	  SELECT  
	    `seq` AS hseq,
	    `name` AS hname,
	    `order` AS horder
	  FROM `m_product_category` 
	  WHERE `level` = 1 AND `parent_seq` = 0 AND `seq` > 0
	) b ON a.`parent_seq` = b.hseq
	LEFT OUTER JOIN (
	  SELECT
	    category_l1_seq AS mcatlvl1 
	  FROM `m_agent_trx_commission` 
	  WHERE `agent_seq` = pSeq AND category_l2_seq = 0
	) c ON a.parent_seq=c.mcatlvl1
	LEFT OUTER JOIN (
	  SELECT 
	    category_l2_seq AS mcatlvl2,
	    commission_fee_percent AS m_commision 
	  FROM `m_agent_trx_commission` 
	  WHERE `agent_seq` = pSeq AND category_l2_seq <> 0
	) d ON a.seq = d.mcatlvl2
	WHERE a.`level` = 2
	ORDER BY b.horder, b.hseq, a.`order`, a.`seq`;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_partner_down_payment_add*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_partner_down_payment_add`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pPartnerLoanSeq SMALLINT UNSIGNED,
        pMinimalLoan VARCHAR(50),
        pMaximalLoan VARCHAR(50),
        pMinDpPercent VARCHAR(50),
        pActive CHAR(1)
    ) 
BEGIN
	DECLARE new_seq Int UNSIGNED;
	SELECT 
		MAX(seq) + 1 INTO new_seq
	FROM m_partner_loan_dp;
	IF new_seq IS NULL THEN
		SET new_seq = 1;
	END IF;
	INSERT INTO m_partner_loan_dp(     
		seq,
		partner_loan_seq,
		minimal_loan,
		maximal_loan,
		min_dp_percent,
		active,
		created_by,
		created_date,
		modified_by,
		modified_date
	) VALUES (
		new_seq,
		pPartnerLoanSeq,
		pMinimalLoan,
		pMaximalLoan,
		pMinDpPercent,
		pActive,
		pUserID,
		NOW(),
		pUserID,
		NOW()
	);
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_partner_down_payment_by_seq*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_partner_down_payment_by_seq`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pSeq TINYINT UNSIGNED
    ) 
BEGIN
	SELECT 
			m_p_l.`product_category_seq`,
			m_p_dp.`partner_loan_seq`,
			m_p_dp.`seq`,
			m_p_c.`name` AS category_loan,
			m_p_l.`tenor` AS tenor_loan,
			m_p_dp.`minimal_loan`,
			m_p_dp.`maximal_loan`,
			m_p_dp.`min_dp_percent`,
			m_p_l. `active`,
			m_p_l.`created_by`,
			m_p_l.`created_date`,
			m_p_l.`modified_by`,
			m_p_l.`modified_date`
				
	  FROM m_partner_loan_dp m_p_dp
          JOIN m_partner_loan m_p_l ON m_p_l.`seq`=m_p_dp.`partner_loan_seq`
          JOIN m_product_category m_p_c ON m_p_c.seq=m_p_l.`product_category_seq`
          
	WHERE
		m_p_dp.`seq` = pSeq;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_partner_down_payment_delete*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_partner_down_payment_delete`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pSeq INT UNSIGNED
    ) 
BEGIN
DELETE FROM m_partner_loan_dp WHERE seq = pSeq;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_partner_down_payment_list*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_partner_down_payment_list`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pCurrPage INT UNSIGNED,
        pRecPerPage INT UNSIGNED,
        pDirSort VARCHAR(4),
        pColumnSort VARCHAR(50),
        pCategoryLoan VARCHAR(50),
        pTenorLoan VARCHAR(50),
        pPartnerSeq TINYINT UNSIGNED,
        pProductCategorySeq TINYINT UNSIGNED,
	pPartnerLoanSeq SMALLINT UNSIGNED,
        pActive CHAR(1)
    ) 
BEGIN
    -- Begin variables for Paging
    DECLARE startRec INT UNSIGNED;
    DECLARE totalPage INT UNSIGNED;
    DECLARE totalRec INT UNSIGNED;
    -- End variables for Paging
    -- Begin SQL Where
    DECLARE sqlWhere VARCHAR(500);
	SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    SET sqlWhere = "";
    IF pPartnerSeq <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And m_p_l.`partner_seq`= '" , pPartnerSeq, "'");
    END IF;
    IF pProductCategorySeq <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And m_p_l.`product_category_seq`= '" , pProductCategorySeq, "'");
    END IF;
    IF pPartnerLoanSeq <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And m_p_dp.`partner_loan_seq`= '" , pPartnerLoanSeq, "'");
    END IF;
    IF pActive <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And m_p_dp.`active` = '", pActive ,"'");
    END IF;
    -- End SQL Where
    -- Begin Paging Info
    SET @sqlCommand = "Select Count(m_p_l.`seq`) Into @totalRec FROM m_partner_loan_dp m_p_dp
          JOIN m_partner_loan m_p_l ON m_p_l.`seq`=m_p_dp.`partner_loan_seq`
          JOIN m_product_category m_p_c ON m_p_c.seq=m_p_l.`product_category_seq`
          JOIN m_partner m_p ON m_p.seq=m_p_l.partner_seq";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        END IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    -- End Paging Info
    SET @sqlCommand = "
        Select		
			m_p_dp.`seq`,
			m_p.`name` AS partner_name,
			m_p_c.`name` AS category_loan,
			m_p_l.`tenor` AS tenor_loan,
			m_p_dp.`minimal_loan`,
			m_p_dp.`maximal_loan`,
			m_p_dp.`min_dp_percent`,
			m_p_l. `active`,
			m_p_l.`created_by`,
			m_p_l.`created_date`,
			m_p_l.`modified_by`,
			m_p_l.`modified_date`
				
	  FROM m_partner_loan_dp m_p_dp
          JOIN m_partner_loan m_p_l ON m_p_l.`seq`=m_p_dp.`partner_loan_seq`
          JOIN m_product_category m_p_c ON m_p_c.seq=m_p_l.`product_category_seq`
          JOIN m_partner m_p ON m_p.seq=m_p_l.partner_seq
           
          ";
    
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    SET @sqlCommand = CONCAT(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @totalRec = NULL;
    SET @sqlCommand = NULL;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_partner_down_payment_partner_list*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_partner_down_payment_partner_list`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pCurrPage INT UNSIGNED,
        pRecPerPage INT UNSIGNED,
        pDirSort VARCHAR(4),
        pColumnSort VARCHAR(50),
        pCategoryLoan VARCHAR(50),
        pTenorLoan VARCHAR(50),
        pPartnerSeq TINYINT UNSIGNED,
        pProductCategorySeq TINYINT UNSIGNED,
	pPartnerLoanSeq SMALLINT UNSIGNED,
        pActive CHAR(1)
    ) 
BEGIN
    -- Begin variables for Paging
    DECLARE startRec INT UNSIGNED;
    DECLARE totalPage INT UNSIGNED;
    DECLARE totalRec INT UNSIGNED;
    -- End variables for Paging
    -- Begin SQL Where
    DECLARE sqlWhere VARCHAR(500);
	SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    SET sqlWhere = "";
    IF pPartnerSeq <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And m_p_l.`partner_seq`= '" , pPartnerSeq, "'");
    END IF;
    IF pProductCategorySeq <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And m_p_l.`product_category_seq` = '", pProductCategorySeq ,"'");
    END IF;
    IF pPartnerLoanSeq <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And m_p_dp.`partner_loan_seq`= '" , pPartnerLoanSeq, "'");
    END IF;
    SET sqlWhere = CONCAT(sqlWhere, " And m_p_l.`partner_seq` = '", pPartnerSeq ,"'");
    -- End SQL Where
    -- Begin Paging Info
    SET @sqlCommand = "Select Count(m_p_l.`seq`) Into @totalRec FROM m_partner_loan_dp m_p_dp
          JOIN m_partner_loan m_p_l ON m_p_l.`seq`=m_p_dp.`partner_loan_seq`
          JOIN m_product_category m_p_c ON m_p_c.seq=m_p_l.`product_category_seq`
          JOIN m_partner m_p ON m_p.seq=m_p_l.partner_seq";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        END IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    -- End Paging Info
    SET @sqlCommand = "
        Select		
			m_p_dp.`seq`,
			m_p.`name` AS partner_name,
			m_p_c.`name` AS category_loan,
			m_p_l.`tenor` AS tenor_loan,
			m_p_dp.`minimal_loan`,
			m_p_dp.`maximal_loan`,
			m_p_dp.`min_dp_percent`,
			m_p_l. `active`,
			m_p_l.`created_by`,
			m_p_l.`created_date`,
			m_p_l.`modified_by`,
			m_p_l.`modified_date`
				
	  FROM m_partner_loan_dp m_p_dp
          JOIN m_partner_loan m_p_l ON m_p_l.`seq`=m_p_dp.`partner_loan_seq`
          JOIN m_product_category m_p_c ON m_p_c.seq=m_p_l.`product_category_seq`
          JOIN m_partner m_p ON m_p.seq=m_p_l.partner_seq
          ";
    
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    SET @sqlCommand = CONCAT(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @totalRec = NULL;
    SET @sqlCommand = NULL;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_partner_down_payment_update*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_partner_down_payment_update`(
  pUserID VARCHAR (25),
  pIPAddr VARCHAR (50),
  pSeq TINYINT UNSIGNED,
  pPartnerLoanSeq SMALLINT UNSIGNED,
  pMinimalLoan VARCHAR (50),
  pMaximalLoan VARCHAR (50),
  pMinDpPercent VARCHAR (50),
  pActive CHAR(1)
) 
BEGIN
  UPDATE 
    m_partner_loan_dp
  SET
    partner_loan_seq = pPartnerLoanSeq,
    minimal_loan = pMinimalLoan,
    maximal_loan = pMaximalLoan,
    min_dp_percent = pMinDpPercent,
    active = pActive,
    modified_by = pUserID,
    modified_date = NOW() 
  WHERE seq = pSeq ;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_partner_live_by_seq*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_partner_live_by_seq`(
	 pUserID VARCHAR(25),
	 pIPAddr VARCHAR(50),
	 pSeq TINYINT UNSIGNED
) 
BEGIN
SELECT
  `seq`,
  `email`,
  `name`,
  `password`,
  `mobile_phone`,
  `admin_fee`,
  `profile_img`,
  `status`,
  `bank_name`,
  `bank_branch_name`,
  `bank_acct_no`,
  `bank_acct_name`,
  `last_login`,
  `ip_address`,
  `created_by`,
  `created_date`,
  `modified_by`,
  `modified_date`
FROM `m_partner`
WHERE seq = pSeq;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_partner_live_list*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_partner_live_list`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pCurrPage INT UNSIGNED,
        pRecPerPage INT UNSIGNED,
        pDirSort VARCHAR(4),
        pColumnSort VARCHAR(50),
        pCode VARCHAR(25),
        pPartnerName varchar(100),
        pPartnerEmail Varchar(50),
        pPartnerAdminFee VARCHAR(50)
) 
BEGIN
    -- Begin variables for Paging
    DECLARE startRec INT UNSIGNED;
    DECLARE totalPage INT UNSIGNED;
    DECLARE totalRec INT UNSIGNED;
    -- End variables for Paging
    -- Begin SQL Where
    DECLARE sqlWhere VARCHAR(500);
	SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    SET sqlWhere = "";
	IF pCode <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And code like '%" , escape_string(pCode), "%'");
    END IF;
    IF pPartnerName <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And name = '" , pPartnerName, "'");
    END IF;
    IF pPartnerEmail <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And email like '%" , escape_string(pPartnerEmail), "%'");
    END IF;    
    -- End SQL Where
    -- Begin Paging Info
    SET @sqlCommand = "Select Count(seq) Into @totalRec From m_partner";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        END IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    -- End Paging Info
    SET @sqlCommand = "
        Select
			  seq,
			  email,
			  `name`,
			  admin_fee,
			  created_by,
			  created_date,
			  modified_by,
			  modified_date
        From m_partner
    ";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    SET @sqlCommand = CONCAT(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @totalRec = NULL;
    SET @sqlCommand = NULL;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_partner_live_update*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_partner_live_update`(
	pUserID VARCHAR(25),
	pIPAddr VARCHAR(50),
	pSeq int UNSIGNED,
	pEmail varchar(50),
	pName varchar(100),
	pMobilePhone varchar(50),
	pAdminFee VARCHAR(50),
	pBankName varchar(50),
	pBankBranchName varchar(50),
	pBankAcctNo varchar(50),
	pBankAcctName varchar(50),
	pProfileImg varchar(50)
) 
BEGIN
    
	UPDATE `m_partner`
        SET `email` = pEmail,
          `name` = pName,
          `mobile_phone` = pMobilePhone,
          `admin_fee` = pAdminFee,
          `bank_name` = pBankName,
          `bank_branch_name` = pBankBranchName,
          `bank_acct_no` = pBankAcctNo,
          `bank_acct_name` = pBankAcctName,
          `profile_img` = pProfileImg,
          `ip_address` = pIPAddr,
          `modified_by` = pUserID,
          `modified_date` = now()
	WHERE seq = pSeq;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_partner_loan_add*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_partner_loan_add`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pPartnerName VARCHAR(50),
        pCategoryName VARCHAR(50),
        pTenor VARCHAR(50),
        pLoanInterest VARCHAR(50),
        pActive CHAR(1)
    ) 
BEGIN
	DECLARE new_seq TINYINT UNSIGNED;
	SELECT 
		MAX(seq) + 1 INTO new_seq
	FROM m_partner_loan;
	IF new_seq IS NULL THEN
		SET new_seq = 1;
	END IF;
	INSERT INTO m_partner_loan(     
		seq,
		product_category_seq,
		partner_seq,
		tenor,
		loan_interest,
		active,
		created_by,
		created_date,
		modified_by,
		modified_date
	) VALUES (
		new_seq,
		pCategoryName,
		pPartnerName,
		pTenor,
		pLoanInterest,
		pActive,
		pUserID,
		NOW(),
		pUserID,
		NOW()
	);
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_partner_loan_by_seq*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_partner_loan_by_seq`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pSeq TINYINT UNSIGNED
    ) 
BEGIN
	SELECT 
		seq,
		product_category_seq,
		partner_seq,
		tenor,
		loan_interest,
		active
		FROM m_partner_loan
	WHERE
		seq = pSeq;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_partner_loan_delete*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_partner_loan_delete`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pSeq INT UNSIGNED
    ) 
BEGIN
DELETE FROM m_partner_loan WHERE seq = pSeq;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_partner_loan_get_all_tenor_in_product_category_seq_by_seq*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_partner_loan_get_all_tenor_in_product_category_seq_by_seq`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pSeq TINYINT UNSIGNED
    ) 
BEGIN
DECLARE startRec INT UNSIGNED;
SELECT 
	product_category_seq, partner_seq Into @product_category_seq,@partner_seq
FROM m_partner_loan
WHERE
	seq = pSeq;
select tenor from m_partner_loan where product_category_seq=@product_category_seq and partner_seq=@partner_seq;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_partner_loan_list*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_partner_loan_list`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pCurrPage INT UNSIGNED,
        pRecPerPage INT UNSIGNED,
        pDirSort VARCHAR(4),
        pColumnSort VARCHAR(50),
        pPartnerName VARCHAR(50),
        pCategoryName VARCHAR(50),
        pTenor VARCHAR(50),
        pLoanInterest VARCHAR(50),
        pActive CHAR(1)
    ) 
BEGIN
    -- Begin variables for Paging
    DECLARE startRec INT UNSIGNED;
    DECLARE totalPage INT UNSIGNED;
    DECLARE totalRec INT UNSIGNED;
    -- End variables for Paging
    -- Begin SQL Where
    DECLARE sqlWhere VARCHAR(500);
	SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    SET sqlWhere = "";
    IF pPartnerName <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And m_p.`seq`= '" , pPartnerName, "'");
    END IF;
    IF pActive <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And m_p_l.`active` = '", pActive ,"'");
    END IF;
    -- End SQL Where
    -- Begin Paging Info
    SET @sqlCommand = "Select Count(m_p_l.`seq`) Into @totalRec FROM m_partner_loan m_p_l
          JOIN m_partner m_p ON m_p.`seq`=m_p_l.`partner_seq`
          JOIN m_product_category m_p_c ON m_p_c.`seq`=m_p_l.`product_category_seq`";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        END IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    -- End Paging Info
    SET @sqlCommand = "
        Select		
			m_p_l.`seq`,
			m_p.`name` AS partner_name,
			m_p_c.`name` AS category_name,
			m_p_l.`tenor`,
			m_p_l.`loan_interest`,
			m_p_l. `active`,
			m_p_l.`created_by`,
			m_p_l.`created_date`,
			m_p_l.`modified_by`,
			m_p_l.`modified_date`
				
	  FROM m_partner_loan m_p_l
          JOIN m_partner m_p ON m_p.`seq`=m_p_l.`partner_seq`
          JOIN m_product_category m_p_c ON m_p_c.`seq`=m_p_l.`product_category_seq`";
    
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    SET @sqlCommand = CONCAT(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @totalRec = NULL;
    SET @sqlCommand = NULL;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_partner_loan_partner_add*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_partner_loan_partner_add`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pPartnerSeq SMALLINT UNSIGNED,
        pCategorySeq VARCHAR(50),
        pTenor VARCHAR(50),
        pLoanInterest VARCHAR(50),
        pActive CHAR(1)
    ) 
BEGIN
	DECLARE new_seq Int UNSIGNED;
	SELECT 
		MAX(seq) + 1 INTO new_seq
	FROM m_partner_loan;
	IF new_seq IS NULL THEN
		SET new_seq = 1;
	END IF;
	INSERT INTO m_partner_loan(     
		seq,
		product_category_seq,
		partner_seq,
		tenor,
		loan_interest,
		active,
		created_by,
		created_date,
		modified_by,
		modified_date
	) VALUES (
		new_seq,
		pCategorySeq,
		pPartnerSeq,
		pTenor,
		pLoanInterest,
		pActive,
		pUserID,
		NOW(),
		pUserID,
		NOW()
	);
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_partner_loan_partner_by_seq*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_partner_loan_partner_by_seq`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pSeq TINYINT UNSIGNED
    ) 
BEGIN
	SELECT 
		seq,
		product_category_seq,
		partner_seq,
		tenor,
		loan_interest,
		active
		FROM m_partner_loan
	WHERE
		seq = pSeq;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_partner_loan_partner_list*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_partner_loan_partner_list`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pCurrPage INT UNSIGNED,
        pRecPerPage INT UNSIGNED,
        pDirSort VARCHAR(4),
        pColumnSort VARCHAR(50),
        pPartnerSeq TINYINT UNSIGNED,
        pProductCategorySeq TINYINT UNSIGNED,
        pCategoryName VARCHAR(50),
        pTenor VARCHAR(50),
        pLoanInterest VARCHAR(50),
        pActive CHAR(1)
    ) 
BEGIN
    -- Begin variables for Paging
    DECLARE startRec INT UNSIGNED;
    DECLARE totalPage INT UNSIGNED;
    DECLARE totalRec INT UNSIGNED;
    -- End variables for Paging
    -- Begin SQL Where
    DECLARE sqlWhere VARCHAR(500);
	SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    SET sqlWhere = "";
    IF pProductCategorySeq <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And m_p_l.`product_category_seq` = '", pProductCategorySeq ,"'");
    END IF;
    IF pActive <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And m_p_l.`active` = '", pActive ,"'");
    END IF;
    SET sqlWhere = CONCAT(sqlWhere, " And m_p_l.`partner_seq` = '", pPartnerSeq ,"'");
    -- End SQL Where
    -- Begin Paging Info
    SET @sqlCommand = "Select Count(m_p_l.`seq`) Into @totalRec FROM m_partner_loan m_p_l
          JOIN m_partner m_p ON m_p.`seq`=m_p_l.`partner_seq`
          JOIN m_product_category m_p_c ON m_p_c.`seq`=m_p_l.`product_category_seq`";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        END IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    -- End Paging Info
    SET @sqlCommand = "
        Select		
			m_p_l.`seq`,
			m_p_c.`name` AS category_name,
			m_p_l.`tenor`,
			m_p_l.`loan_interest`,
			m_p_l. `active`,
			m_p_l.`created_by`,
			m_p_l.`created_date`,
			m_p_l.`modified_by`,
			m_p_l.`modified_date`
				
	  FROM m_partner_loan m_p_l
          JOIN m_partner m_p ON m_p.`seq`=m_p_l.`partner_seq`
          JOIN m_product_category m_p_c ON m_p_c.`seq`=m_p_l.`product_category_seq`
          ";
    
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    SET @sqlCommand = CONCAT(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @totalRec = NULL;
    SET @sqlCommand = NULL;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_partner_loan_partner_update*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_partner_loan_partner_update`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pSeq TINYINT UNSIGNED,
        pPartnerSeq VARCHAR(50),
        pRroductCategorySeQ TINYINT UNSIGNED,
        pTenor VARCHAR(50),
        pLoanInterest VARCHAR(50),
        pActive CHAR(1)
    ) 
BEGIN
UPDATE m_partner_loan SET
    partner_seq = pPartnerSeq,
    product_category_seq = pRroductCategorySeQ,
    tenor = pTenor,
    loan_interest = pLoanInterest,
    active = pActive,
    modified_by = pUserID,
    modified_date = NOW()
WHERE
	seq = pSeq;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_partner_loan_update*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_partner_loan_update`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pSeq TINYINT UNSIGNED,
        pPartnerName VARCHAR(50),
        pCategoryName VARCHAR(50),
        pTenor VARCHAR(50),
        pLoanInterest VARCHAR(50),
        pActive CHAR(1)
    ) 
BEGIN
UPDATE m_partner_loan SET
    partner_seq = pPartnerName,
    product_category_seq = pCategoryName,
    tenor = pTenor,
    loan_interest = pLoanInterest,
    active = pActive,
    modified_by = pUserID,
    modified_date = NOW()
WHERE
	seq = pSeq;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_partner_log_data_update*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_partner_log_data_update`(
	pUserID VarChar(25),
	pIPAddr VarChar(50), 
	pPartnerSeq SmallInt unsigned,
	pPhoneNo VarChar(50),
	pBankName VarChar(50),
	pBankBranchName VarChar(50),
	pBankAcctNo VarChar(50),
	pBankAcctName VarChar(50)
) 
BEGIN
    UPDATE `m_partner`
        SET 
          `mobile_phone` = pPhoneNo,
          `bank_name` = pBankName,
          `bank_branch_name` = pBankBranchName,
          `bank_acct_no` = pBankAcctNo,
          `bank_acct_name` = pBankAcctName
        WHERE `seq` = pPartnerSeq;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_partner_order_list_agent*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_partner_order_list_agent`(
        `pUserID` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pCurrPage` INTEGER UNSIGNED,
        `pRecPerPage` INTEGER UNSIGNED,
        `pDirSort` VARCHAR(4),
        `pColumnSort` VARCHAR(50),
        `pPartnerId` INTEGER(9) UNSIGNED,
        `pAgentSeq` INTEGER(10) UNSIGNED,
        `pOrderNo` VARCHAR(50),
        `pDates` VARCHAR(10),
        `pDatee` VARCHAR(10),
        `pPaymentStatus` CHAR(1)
    ) 
BEGIN
    -- Begin variables for Paging
    DECLARE startRec INT UNSIGNED;
    DECLARE totalPage INT UNSIGNED;
    DECLARE totalRec INT UNSIGNED;
    -- End variables for Paging
    -- Begin SQL Where
    DECLARE sqlWhere VARCHAR(800);
    DECLARE sqlWhereC VARCHAR(800);
	SET sqlWhere = " WHERE 1";
	SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    IF pOrderNo <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And t_o.order_no like '%" , escape_string(pOrderNo), "%'");
    END IF;    
    IF pDates <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And t_o.order_date >= '" , escape_string(pDates), "'");
    END IF;
    IF pDatee <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And t_o.order_date <= '" , escape_string(pDatee), "'");
    END IF;
    IF pPartnerId <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And m_a.partner_seq ='" , pPartnerId, "'");
    END IF;
    IF pAgentSeq <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And t_o.agent_seq = '" , pAgentSeq, "'");
    END IF;
    IF pPaymentStatus <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And t_o.payment_status='" , pPaymentStatus, "'");
    END IF;
    -- SET sqlWhere = CONCAT(sqlWhere, " And t_o_p.product_status='R'");
    -- End SQL Where
    -- Begin Paging Info
    SET @sqlCommand = "Select Count(distinct(t_o.seq)) Into @totalRec FROM `t_order` t_o LEFT JOIN m_agent m_a ON t_o.agent_seq = m_a.seq INNER JOIN t_order_merchant t_o_m ON t_o_m.order_seq=t_o.seq INNER JOIN t_order_product t_o_p ON t_o_p.order_seq = t_o.seq AND t_o_p.merchant_info_seq=t_o_m.merchant_info_seq 
LEFT JOIN m_district m_d ON m_d.seq=t_o.receiver_district_seq INNER JOIN m_city m_c ON m_d.city_seq=m_c.seq INNER JOIN m_province m_p ON m_c.province_seq = m_p.seq LEFT JOIN t_order_loan t_o_l ON t_o_l.`order_seq`=t_o.seq JOIN m_payment_gateway_method pgm ON t_o.pg_method_seq = pgm.seq";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, sqlWhere);
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        END IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    -- End Paging Info
    SET @sqlCommand = "SELECT
  t_o.`seq`,
  t_o.`order_no`,
  t_o.`order_date`,
  t_o.`payment_status`,
  t_o.`total_order`,
  m_a.`name` AS agent_name,
  t_o.`receiver_phone_no`,
  t_o.`receiver_address`,  
  t_o_p.commission_fee_percent,
  SUM(t_o_p.commission_fee_percent/100*t_o_p.`sell_price`*qty) AS commission,
  SUM(t_o_p.commission_fee_partner_percent/100*t_o_p.`sell_price`*qty) AS commission_partner,
  t_o_m.total_ship_charged,
  m_d.name AS district,
  m_c.name AS city,
  m_p.name AS province,
  t_o.`receiver_zip_code`,
  t_o.`receiver_name` AS name,
  t_o_l.`customer_seq`,
  t_o_l.`dp`,
  t_o_l.`admin_fee`,
  t_o_l.`tenor`,
  t_o_l.`loan_interest`,
  t_o_l.`installment`,
  t_o_l.`total_installment`,
  t_o_l.`status_order`,
  pgm.`name` AS payment_method
FROM `t_order` t_o LEFT JOIN m_agent m_a ON t_o.agent_seq = m_a.seq INNER JOIN t_order_merchant t_o_m ON t_o_m.order_seq=t_o.seq INNER JOIN t_order_product t_o_p ON t_o_p.order_seq = t_o.seq AND t_o_p.merchant_info_seq=t_o_m.merchant_info_seq 
LEFT JOIN m_district m_d ON m_d.seq=t_o.receiver_district_seq INNER JOIN m_city m_c ON m_d.city_seq=m_c.seq INNER JOIN m_province m_p ON m_c.province_seq = m_p.seq LEFT JOIN t_order_loan t_o_l ON t_o_l.`order_seq`=t_o.seq JOIN m_payment_gateway_method pgm ON t_o.`pg_method_seq` = pgm.`seq`";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, sqlWhere);
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand, " GROUP BY t_o.seq");
    SET @sqlCommand = CONCAT(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    SET @sqlCommand = CONCAT(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_partner_order_list_agent_by_seq*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_partner_order_list_agent_by_seq`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq BIGINT (20)unsigned,
        pPartnerId BIGINT(20) unsigned
    ) 
BEGIN
	SELECT 
t_o.`seq`,
t_o.`order_no`,
t_o.`order_date`,
t_o.`member_seq`,
t_o.`agent_seq`,
t_o.`receiver_name`,
t_o.`receiver_address`,
t_o.`receiver_district_seq`,
t_o.`receiver_zip_code`,
t_o.`receiver_phone_no`,
t_o.`payment_status`,
t_o.`pg_method_seq`,
t_o.`paid_date`,
t_o.`payment_retry`,
t_o.`total_order`,
t_o.`voucher_seq`,
t_o.`voucher_refunded`,
t_o.`coupon_seq`,
t_o.`total_payment`,
t_o.`conf_pay_type`,
t_o.`conf_pay_amt_member`,
t_o.`conf_pay_date`,
t_o.`conf_pay_note_file`,
t_o.`conf_pay_bank_seq`,
t_o.`conf_pay_amt_admin`,
t_o.`agent_seq`,
t_om.`merchant_info_seq`,
t_om.`expedition_service_seq`,
t_om.`real_expedition_service_seq`,
t_om.`total_merchant`,
t_om.`total_ins`,
t_om.`total_ship_real`,
t_om.`total_ship_charged`,
t_om.`order_status`,
t_om.`member_notes`,
t_om.`printed`,
t_om.`print_date`,
t_om.`awb_seq`,
t_om.`awb_no`,
t_om.`ref_awb_no`,
t_om.`ship_by`,
t_om.`ship_by_exp_seq`,
t_om.`ship_date`,
t_om.`ship_note_file`,
t_om.`ship_notes`,
t_om.`received_date`,
t_om.`received_by`,
t_o_l.`customer_seq`,
t_o_l.`dp`,
t_o_l.`admin_fee`,
t_o_l.`tenor`,
t_o_l.`loan_interest`,
t_o_l.`installment`,
t_o_l.`total_installment`,
t_o_l.`status_order`,
t_o_l.`po_no`,
pgm.`name` AS payment_method,
pgm.`seq` AS payment_seq,
m_a.`name` AS agent_name
FROM 
  `t_order` t_o
  inner join 
  t_order_merchant t_om
  on t_o.`seq`=t_om.`order_seq`
  left join
  t_order_loan t_o_l
  on t_o_l.order_seq=t_o.seq
  JOIN m_payment_gateway_method pgm 
  ON t_o.`pg_method_seq` = pgm.`seq`
  JOIN m_agent m_a
  ON t_o.agent_seq = m_a.seq
	Where t_o.`seq` = pSeq AND t_o.agent_seq in (select seq from m_agent where partner_seq=pPartnerId);        
        
  select 
  t_op.`order_seq`,
  t_op.`merchant_info_seq`,
  t_op.`product_variant_seq`,
  t_op.`variant_value_seq`,
  t_op.`qty`,
  t_op.`sell_price`,
  t_op.`weight_kg`,
  t_op.`ship_price_real`,
  t_op.`ship_price_charged`,
  t_op.`ins_rate_percent`,
  t_op.`product_status`,
  v_pv.`merchant_seq`,
  v_pv.`seq`,
  v_pv.`name`,
  v_pv.`include_ins`,
  v_pv.`category_l2_seq`,
  v_pv.`category_ln_seq`,
  v_pv.`notes`,
  v_pv.`description`,
  v_pv.`content`,
  v_pv.`warranty_notes`,
  v_pv.`p_weight_kg`,
  v_pv.`p_length_cm`,
  v_pv.`p_width_cm`,
  v_pv.`p_height_cm`,
  v_pv.`b_weight_kg`,
  v_pv.`b_length_cm`,
  v_pv.`b_width_cm`,
  v_pv.`b_height_cm`,
  v_pv.`product_seq`,
  v_pv.`variant_seq`,
  v_pv.`product_price`,
  v_pv.`disc_percent`,
  v_pv.`sell_price` as product_sell_price,
  v_pv.`order`,
  v_pv.`max_buy`,
  v_pv.`status`,
  v_pv.`pic_1_img`,
  v_pv.`pic_2_img`,
  v_pv.`pic_3_img`,
  v_pv.`pic_4_img`,
  v_pv.`pic_5_img`,
  v_pv.`pic_6_img`,
  v_pv.`1star`,
  v_pv.`2star`,
  v_pv.`3star`,
  v_pv.`4star`,
  v_pv.`5star`,
  v_pv.`merchant_sku`,
  v_pv.`stock`,
  v_pv.`variant_value`,
  t_op.commission_fee_percent/100*t_op.`sell_price` as commission
  from 
 `t_order_product` t_op 
  inner join `v_product_variant` v_pv  
  on t_op.`product_variant_seq`=v_pv.`variant_seq`
  inner join t_order t_o
  on t_op.order_seq=t_o.seq
	Where
		t_op.`order_seq` = pSeq and t_o.agent_seq IN (SELECT seq FROM m_agent WHERE partner_seq=pPartnerId);
		
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_partner_order_list_agent_product*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_partner_order_list_agent_product`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
        pPartnerId INTEGER(9) UNSIGNED
    ) 
BEGIN
  SELECT
    t_o.seq,
    t_o.order_no,
    m_p.`name` AS product_name,
    t_o_m.`order_status`,
    t_o_p.`created_date`,
    m_a.name AS agent_name
  FROM `t_order_product` t_o_p 
  INNER JOIN `t_order_merchant` t_o_m ON t_o_p.`order_seq`=t_o_m.`order_seq`
  INNER JOIN `t_order` t_o ON t_o_p.`order_seq`=t_o.`seq`
  LEFT JOIN m_product_variant m_p_v ON t_o_p.`product_variant_seq`=m_p_v.seq
  INNER JOIN m_product m_p ON m_p_v.product_seq = m_p.seq
  INNER JOIN m_agent m_a ON t_o.`agent_seq`=m_a.`seq`
  where m_a.partner_seq = pPartnerId
  group by t_o.seq,t_o.order_no, m_p.`name`, t_o_m.`order_status`, t_o_p.`created_date`, m_a.name
  ORDER BY t_o_p.`created_date` DESC LIMIT 10;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_payment_gateway_add*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_payment_gateway_add`(
    pUserID VarChar(25), 
    pIPAddr VarChar(50),
    pName Varchar(50),
    pOrder TinyInt unsigned,
    pNotes VarChar(1000),
    pActive Char (1),
    pLogoImg Varchar(50)
) 
BEGIN
Declare new_seq tinyint unsigned;
SELECT 
    MAX(seq) + 1
INTO new_seq FROM
    m_payment_gateway;
	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
	Insert Into m_payment_gateway(
		seq,
		`name`,
        `order`,
        notes,
		active,
        logo_img,
		created_by,
		created_date,
		modified_by,
		modified_date
	) Values (
		new_seq,
		pName,
        pOrder,
        pNotes,
		pActive,
        pLogoImg,
		pUserID,
		Now(),
		pUserID,
		Now()
	);
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_payment_gateway_by_seq*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_payment_gateway_by_seq`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
	pSeq TinyInt unsigned
) 
BEGIN
	Select 
		seq,
		`name`,
        `order`,
        notes,
		active,
        logo_img
	From m_payment_gateway
	Where
		seq = pSeq;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_payment_gateway_update*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_payment_gateway_update`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pSeq tinyint unsigned,
    pName Varchar(50),
    pOrder TinyInt unsigned,
    pNotes VarChar(1000),
    pActive Char (1),
    pLogoimg varchar(50)
    ) 
BEGIN
update m_payment_gateway set
	`name` = pName,
    `order` = pOrder,
    notes = pNotes,
    active = pActive,
    logo_img = pLogoimg,
    modified_by = pUserID,
    modified_date = now()
Where
	seq = pSeq;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_redeem_agent_add*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_redeem_agent_add`(
        `pUserID` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pSeq` INTEGER(10) UNSIGNED,
        `pAgentSeq` INTEGER(10) UNSIGNED,
        `pTotal` DECIMAL(10,0)
    ) 
BEGIN
	Insert Into t_redeem_agent(
        redeem_seq,
		agent_seq,
        `total`,
        `status`,
		created_by,
		created_date,
		modified_by,
		modified_date
	) Values (
		pSeq,
		pAgentSeq,
        pTotal,
        'U',
		pUserID,
		Now(),
		pUserID,
		Now()
	);
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_redeem_agent_get_order*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_redeem_agent_get_order`(
        `pUserID` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pToDate` DATE
    ) 
BEGIN
SELECT t_or.`agent_seq`, 
SUM(t_op.`qty` * t_op.`sell_price` * (t_op.`commission_fee_partner_percent` / 100)) AS total_komisi
FROM `t_order_product` t_op 
JOIN `t_order_merchant` t_om ON t_om.`order_seq`=t_op.`order_seq` AND t_op.`merchant_info_seq`=t_om.`merchant_info_seq`
JOIN `t_order` t_or  ON t_op.`order_seq`=t_or.seq 
JOIN `m_agent` m_a ON t_or.`agent_seq`=m_a.seq
WHERE t_om.`order_status`='D' 
AND t_op.`product_status`='R'
AND t_om.received_date <= pToDate
AND t_om.`redeem_agent_seq` IS NULL
AND t_or.`member_seq` IS NULL
GROUP BY t_or.`agent_seq`;

END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_redeem_agent_list*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_redeem_agent_list`(
        `pUserID` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pCurrPage` INTEGER,
        `pRecPerPage` INTEGER,
        `pDirSort` VARCHAR(20),
        `pColumnSort` VARCHAR(50),
        `pFdate` VARCHAR(10),
        `pTdate` VARCHAR(10),
        `pStatus` CHAR(1)
    ) 
BEGIN
    -- Begin variables for Paging
    Declare startRec Int;
    Declare totalPage Int;
    Declare totalRec Int;
    -- End variables for Paging
    -- Begin SQL Where
    Declare sqlWhere VarChar(500);
	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
    If pFdate <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And a.from_date >= '" , pFdate, "'");
    End If;
    If pTdate <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And a.to_date <= '" , pTdate, "'");
    End If;
    If pStatus <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And a.status= '" , pStatus, "'");
    End If;
    -- End SQL Where
    -- Begin Paging Info
    Set @sqlCommand = "Select Count(seq) Into @totalRec From t_redeem_agent_period a";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);
        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;
        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info
    Set @sqlCommand = "
        Select
	        a.seq,a.from_date,a.to_date,a.status,b.total,a.`created_by`,a.`created_date`,a.`modified_by`,a.`modified_date`
        From t_redeem_agent_period a left outer join (select sum(CASE WHEN c.`mutation_type`= 'D' THEN c.`total` ELSE c.`total` * -1 END) as total,redeem_seq FROM t_redeem_agent_component c group by c.redeem_seq ) b
        on a.seq=b.`redeem_seq`
    ";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    Select pCurrPage, totalPage, totalRec as total_rec;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set @totalRec = Null;
    Set @sqlCommand = Null;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_redeem_agent_order_component_add*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_redeem_agent_order_component_add`(
        `pUserID` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pSeq` INTEGER(10) UNSIGNED,
        `pType` CHAR(3),
        `pMType` CHAR(1),
        `pPaymentCode` CHAR(10),
        `pTdate` DATE
    ) 
BEGIN
	Insert Into `t_redeem_agent_component`(
        `redeem_seq`,
        `partner_seq`,
        `type`,
        `mutation_type`,
        `total`,
        created_by,
        created_date,
        modified_by,
        modified_date
	)
    select pSeq, ma.partner_seq, pType, pMtype, SUM(`tor`.`total_payment`-tol.`total_installment`) total, pUserID, Now(), pUserID, Now() from
    t_order tor
    left outer join t_order_loan tol on tor.seq=tol.order_seq
    join `m_agent` ma on `tor`.`agent_seq`=ma.`seq`
    where
	tor.seq in (select top.order_seq from `t_order_product` top
    join `t_order_merchant` tom on `top`.`order_seq`=tom.`order_seq` and top.`merchant_info_seq`=tom.`merchant_info_seq`
	where tom.order_status='D' and tom.redeem_agent_seq is null and tom.received_date <= pTdate
    and top.`product_status`='R' ) and tor.member_seq is null and tor.`pg_method_seq` in
    (select seq from m_payment_gateway_method where pg_seq in (select seq from m_payment_gateway where `name`=pPaymentCode))
    group by ma.`partner_seq`;

END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_redeem_agent_per_partner_list*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_redeem_agent_per_partner_list`(
        `pUserID` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pPartnerSeq` INTEGER(10) UNSIGNED,
        `pCurrPage` INTEGER,
        `pRecPerPage` INTEGER,
        `pDirSort` VARCHAR(20),
        `pColumnSort` VARCHAR(50),
        `pFdate` VARCHAR(10),
        `pTdate` VARCHAR(10),
        `pStatus` CHAR(1),
        `pPaidDateMonth` TINYINT UNSIGNED,
        `pPaidDateYear` INT UNSIGNED
    ) 
BEGIN
    -- Begin variables for Paging
    DECLARE startRec INT;
    DECLARE totalPage INT;
    DECLARE totalRec INT;
    -- End variables for Paging
    -- Begin SQL Where
    DECLARE sqlWhere VARCHAR(500);
	SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    SET sqlWhere = "";
    IF pFdate <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And trap.from_date >= '" , pFdate, "'");
    END IF;
    IF pTdate <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And trap.to_date <= '" , pTdate, "'");
    END IF;
    IF pStatus <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And tra.status= '" , pStatus, "'");
    END IF;
    IF pPartnerSeq <> 0 THEN 
        SET sqlWhere = CONCAT(sqlWhere, " And ma.partner_seq= '" , pPartnerSeq, "'");
    END IF;    
    IF pPaidDateMonth <> 0 THEN 
        SET sqlWhere = CONCAT(sqlWhere, " And MONTH(tra.paid_date)= '" , pPaidDateMonth, "'");
    END IF;    
    IF pPaidDateYear <> 0 THEN 
        SET sqlWhere = CONCAT(sqlWhere, " And YEAR(tra.paid_date)= '" , pPaidDateYear, "'");
    END IF;    
    
    -- End SQL Where
    -- Begin Paging Info
    SET @sqlCommand = "SELECT COUNT(DISTINCT(tra.redeem_seq)) Into @totalRec FROM t_redeem_agent tra INNER JOIN t_redeem_agent_period trap ON tra.`redeem_seq`=trap.`seq` INNER JOIN m_agent ma ON ma.`seq`=tra.`agent_seq`";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1 AND tra.status<>'U'" , sqlWhere);
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        END IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    
    -- End Paging Info
    SET @sqlCommand = "SELECT trap.`seq`, trap.`from_date`, trap.`to_date`, SUM(tra.`total`) AS total_commission_sum";
    IF pPartnerSeq = 1 THEN
        SET @sqlCommand = CONCAT(@sqlCommand,", tra.`paid_partner_date` AS paid_date");
    ELSE
        SET @sqlCommand = CONCAT(@sqlCommand,", tra.`paid_date` AS paid_date");
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand,", (SELECT SUM(total) FROM t_redeem_agent_component trac WHERE trac.redeem_seq=trap.`seq` AND partner_seq = ",pPartnerSeq," AND mutation_type='C') AS total_commission");
--    SET @sqlCommand = CONCAT(@sqlCommand,", (SELECT SUM(total) FROM t_redeem_agent_component trac WHERE trac.redeem_seq=trap.`seq` AND partner_seq = ",pPartnerSeq," AND mutation_type='D') AS total_order");
    SET @sqlCommand = CONCAT(@sqlCommand,", (SELECT SUM(`tor`.`total_payment`) total FROM
    t_order tor JOIN `m_agent` ma ON `tor`.`agent_seq`=ma.`seq`
    WHERE 
	tor.seq IN (SELECT top.order_seq FROM  `t_order_product` top
    JOIN `t_order_merchant` tom ON `top`.`order_seq`=tom.`order_seq` AND top.`merchant_info_seq`=tom.`merchant_info_seq`
	WHERE tom.order_status='D' AND top.`product_status`='R' AND tom.redeem_agent_seq=trap.`seq` ) AND tor.member_seq IS NULL ) AS total_order");
    SET @sqlCommand = CONCAT(@sqlCommand, " FROM t_redeem_agent tra INNER JOIN t_redeem_agent_period trap ON tra.`redeem_seq`=trap.`seq` INNER JOIN m_agent ma ON ma.`seq`=tra.`agent_seq`");
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1 AND tra.status<>'U'" , sqlWhere);        
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand, " GROUP BY redeem_seq");
    IF pPartnerSeq = 1 THEN
        SET @sqlCommand = CONCAT(@sqlCommand,", tra.`paid_partner_date`");
    ELSE
        SET @sqlCommand = CONCAT(@sqlCommand,", tra.`paid_date`");
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    SET @sqlCommand = CONCAT(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @totalRec = NULL;
    SET @sqlCommand = NULL;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_redeem_agent_per_partner_list_by_agent_seq*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_redeem_agent_per_partner_list_by_agent_seq`(
  `pUserID` VARCHAR(25),
  `pIPAddr` VARCHAR(50),
  `pPartnerSeq` INTEGER(10) UNSIGNED,
  `pRedeemAgentSeq` INTEGER(10) UNSIGNED,
  `pCurrPage` INTEGER,
  `pRecPerPage` INTEGER,
  `pDirSort` VARCHAR(20),
  `pColumnSort` VARCHAR(50)
) 
BEGIN
    -- Begin variables for Paging
    DECLARE startRec INT;
    DECLARE totalPage INT;
    DECLARE totalRec INT;
    -- End variables for Paging
    -- Begin SQL Where
    DECLARE sqlWhere VARCHAR(500);
	SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    SET sqlWhere = "";
    IF pPartnerSeq <> 0 THEN 
        SET sqlWhere = CONCAT(sqlWhere, " And ma.partner_seq= '" , pPartnerSeq, "'");
    END IF;
    IF pRedeemAgentSeq <> 0 THEN 
        SET sqlWhere = CONCAT(sqlWhere, " And tom.`redeem_agent_seq`= '" , pRedeemAgentSeq, "'");
    END IF;    
    -- End SQL Where
    -- Begin Paging Info
    SET @sqlCommand = "Select COUNT(*) Into @totalRec 
FROM t_order_product top
INNER JOIN t_order_merchant tom ON tom.`order_seq` = top.`order_seq` AND tom.merchant_info_seq = top.`merchant_info_seq`
INNER JOIN t_order `to` ON `to`.`seq` = tom.`order_seq` 
LEFT JOIN m_product_variant mpv ON mpv.`seq` = top.`product_variant_seq`
INNER JOIN m_product mp ON mp.`seq` = mpv.`product_seq` 
INNER JOIN m_variant_value mvv ON mpv.`variant_value_seq`=mvv.seq
LEFT JOIN m_agent ma ON ma.`seq`=`to`.`agent_seq`";
    SET @sqlCommand = CONCAT(@sqlCommand, " INNER JOIN t_redeem_agent_period trap ON trap.seq=",pRedeemAgentSeq);
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1 AND top.`product_status`='R' AND tom.`order_status`='D' AND `to`.`payment_status`='P'" , sqlWhere);
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        END IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    -- End Paging Info
    SET @sqlCommand = "
SELECT 
  top.order_seq,
  top.qty, 
  `to`.order_no,
  ma.`name`,
  `to`.`total_order`,
  tom.`total_ship_charged`,
  `to`.total_payment,
  top.`sell_price`, top.`commission_fee_percent`, 
  (top.`qty` * top.`sell_price` * top.`commission_fee_percent`/100) AS total_commission,
  IF(mvv.seq=1,mp.`name`,CONCAT(mp.`name`,' - ',mvv.value)) AS product_name,
  tom.`created_date`, tom.`redeem_agent_seq`,
  trap.from_date, trap.to_date
FROM t_order_product top
INNER JOIN t_order_merchant tom ON tom.`order_seq` = top.`order_seq` AND tom.merchant_info_seq = top.`merchant_info_seq`
INNER JOIN t_order `to` ON `to`.`seq` = tom.`order_seq` 
LEFT JOIN m_product_variant mpv ON mpv.`seq` = top.`product_variant_seq`
INNER JOIN m_product mp ON mp.`seq` = mpv.`product_seq` 
INNER JOIN m_variant_value mvv ON mpv.`variant_value_seq`=mvv.seq
LEFT JOIN m_agent ma ON ma.`seq`=`to`.`agent_seq`";
    SET @sqlCommand = CONCAT(@sqlCommand, " INNER JOIN t_redeem_agent_period trap ON trap.seq=",pRedeemAgentSeq);
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1 AND top.`product_status`='R' AND tom.`order_status`='D' AND `to`.`payment_status`='P'" , sqlWhere);
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    SET @sqlCommand = CONCAT(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @totalRec = NULL;
    SET @sqlCommand = NULL;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_redeem_agent_per_partner_list_by_agent_seq_no_limit*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_redeem_agent_per_partner_list_by_agent_seq_no_limit`(
  `pUserID` VARCHAR(25),
  `pIPAddr` VARCHAR(50),
  `pPartnerSeq` INTEGER(10) UNSIGNED,
  `pRedeemAgentSeq` INTEGER(10) UNSIGNED,
  `pDirSort` VARCHAR(20),
  `pColumnSort` VARCHAR(50)
) 
BEGIN
    DECLARE sqlWhere VARCHAR(500);
    SET sqlWhere = "";
    IF pPartnerSeq <> 0 THEN 
        SET sqlWhere = CONCAT(sqlWhere, " And ma.partner_seq= '" , pPartnerSeq, "'");
    END IF;
    IF pRedeemAgentSeq <> 0 THEN 
        SET sqlWhere = CONCAT(sqlWhere, " And tom.`redeem_agent_seq`= '" , pRedeemAgentSeq, "'");
    END IF;    
    SET @sqlCommand = "
SELECT 
  top.order_seq,
  top.qty, 
  `to`.order_no,
  ma.`name`,
  `to`.`total_order`,
  tom.`total_ship_charged`,
  `to`.total_payment,
  top.`sell_price`, top.`commission_fee_percent`, 
  (top.`qty` * top.`sell_price` * top.`commission_fee_percent`/100) AS total_commission,
  IF(mvv.seq=1,mp.`name`,CONCAT(mp.`name`,' - ',mvv.value)) AS product_name,
  tom.`created_date`, tom.`redeem_agent_seq`,
  trap.from_date, trap.to_date
FROM t_order_product top
INNER JOIN t_order_merchant tom ON tom.`order_seq` = top.`order_seq` AND tom.merchant_info_seq = top.`merchant_info_seq`
INNER JOIN t_order `to` ON `to`.`seq` = tom.`order_seq` 
LEFT JOIN m_product_variant mpv ON mpv.`seq` = top.`product_variant_seq`
INNER JOIN m_product mp ON mp.`seq` = mpv.`product_seq` 
INNER JOIN m_variant_value mvv ON mpv.`variant_value_seq`=mvv.seq
LEFT JOIN m_agent ma ON ma.`seq`=`to`.`agent_seq`";
    SET @sqlCommand = CONCAT(@sqlCommand, " INNER JOIN t_redeem_agent_period trap ON trap.seq=",pRedeemAgentSeq);
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1 AND top.`product_status`='R' AND tom.`order_status`='D' AND `to`.`payment_status`='P'" , sqlWhere);
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @sqlCommand = NULL;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_redeem_agent_per_partner_list_no_limit*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_redeem_agent_per_partner_list_no_limit`(
        `pUserID` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pPartnerSeq` INTEGER(10) UNSIGNED,
        `pDirSort` VARCHAR(20),
        `pColumnSort` VARCHAR(50),
        `pFdate` VARCHAR(10),
        `pTdate` VARCHAR(10),
        `pStatus` CHAR(1),
        `pPaidDateMonth` TINYINT UNSIGNED,
        `pPaidDateYear` INT UNSIGNED
    ) 
BEGIN
    -- Begin SQL Where
    DECLARE sqlWhere VARCHAR(500);
   SET sqlWhere = "";
    IF pFdate <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And trap.from_date >= '" , pFdate, "'");
    END IF;
    IF pTdate <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And trap.to_date <= '" , pTdate, "'");
    END IF;
    IF pStatus <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And tra.status= '" , pStatus, "'");
    END IF;
    IF pPartnerSeq <> 0 THEN 
        SET sqlWhere = CONCAT(sqlWhere, " And ma.partner_seq= '" , pPartnerSeq, "'");
    END IF;    
    IF pPaidDateMonth <> 0 THEN 
        SET sqlWhere = CONCAT(sqlWhere, " And MONTH(tra.paid_date)= '" , pPaidDateMonth, "'");
    END IF;    
    IF pPaidDateYear <> 0 THEN 
        SET sqlWhere = CONCAT(sqlWhere, " And YEAR(tra.paid_date)= '" , pPaidDateYear, "'");
    END IF;
    -- End SQL Where
    
    SET @sqlCommand = "SELECT trap.`seq`, trap.`from_date`, trap.`to_date`, SUM(tra.`total`) AS total_commission_sum";
    IF pPartnerSeq = 1 THEN
        SET @sqlCommand = CONCAT(@sqlCommand,", tra.`paid_partner_date` AS paid_date");
    ELSE
        SET @sqlCommand = CONCAT(@sqlCommand,", tra.`paid_date` AS paid_date");
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand,", (SELECT SUM(total) FROM t_redeem_agent_component trac WHERE trac.redeem_seq=trap.`seq` AND partner_seq = ",pPartnerSeq," AND mutation_type='C') AS total_commission");
    SET @sqlCommand = CONCAT(@sqlCommand,", (SELECT SUM(total) FROM t_redeem_agent_component trac WHERE trac.redeem_seq=trap.`seq` AND partner_seq = ",pPartnerSeq," AND mutation_type='D') AS total_order");
    SET @sqlCommand = CONCAT(@sqlCommand, " FROM t_redeem_agent tra INNER JOIN t_redeem_agent_period trap ON tra.`redeem_seq`=trap.`seq` INNER JOIN m_agent ma ON ma.`seq`=tra.`agent_seq`");
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1 AND tra.status<>'U'" , sqlWhere);        
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand, " GROUP BY redeem_seq");
    IF pPartnerSeq = 1 THEN
        SET @sqlCommand = CONCAT(@sqlCommand,", tra.`paid_partner_date`");
    ELSE
        SET @sqlCommand = CONCAT(@sqlCommand,", tra.`paid_date`");
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @totalRec = NULL;
    SET @sqlCommand = NULL;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_save_delete_agent_address_by_agent_seq*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_save_delete_agent_address_by_agent_seq`(
        `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pAgent_Seq` INTEGER(10)
    ) 
BEGIN
    delete from `m_agent_address` where `agent_seq`=pAgent_Seq;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_thumbs_new_products*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_thumbs_new_products`(
	pSTART INTEGER, 
	pLIMIT INTEGER,
	pWHERE_CONDITION TEXT,
	pORDER_CONDITION TEXT,
	pUser VARCHAR(10),
	pAgentSeq BIGINT(20)
) 
BEGIN
IF(pORDER_CONDITION = '') THEN SET @ORDER = 'ORDER BY pv.seq DESC'; ELSE SET @ORDER = pORDER_CONDITION; END IF;
IF(pWHERE_CONDITION = '') THEN SET @WHERE = 'WHERE pv.active = ''1'' AND pv.status IN (''L'',''C'')'; ELSE SET @WHERE = pWHERE_CONDITION; END IF;
IF(pUser = 'AGENT') THEN
	SET @AGENT_TABLE = concat('LEFT JOIN (SELECT * FROM m_agent_trx_commission WHERE agent_seq=',pAgentSeq,') AS matc ON matc.category_l2_seq= p.`category_l2_seq`',' JOIN (SELECT * FROM m_product_category WHERE `level`=2) mpc ON mpc.seq = p.category_l2_seq');
	SET @AGENT_SELECT = ',IF(matc.commission_fee_percent IS NULL,CEIL(mpc.commission_fee_percent * pv.`sell_price`/100),CEIL(matc.commission_fee_percent * pv.`sell_price`/100)) AS commission_fee,
        IF(matc.commission_fee_percent IS NULL,mpc.commission_fee_percent,matc.commission_fee_percent) AS commission_fee_percent';
ELSE
	SET @AGENT_TABLE='';
	SET @AGENT_SELECT='';
END IF;
SET @sql = CONCAT("SELECT SQL_CALC_FOUND_ROWS
    pv.product_seq,
    pv.pic_1_img AS image,
    p.name AS `name`,
    p.`merchant_seq` AS merchant_seq,
    vv.seq AS variant_seq,
    vv.value AS `variant_value`,
    pv.`product_price` AS `product_price`,
    pv.`sell_price` AS `sell_price`,
    pv.`seq` AS `product_variant_seq`,
    ps.`stock` AS `stock`,
    ps.`merchant_sku` AS `sku`,
    qry.promo_credit_name,
    qry2.pcredit_name,
    m.code as merchant_code,
    m.name as merchant_name",@AGENT_SELECT,",
    m.to_date,
    if((NOW() < m.from_date OR NOW() > m.to_date OR (m.from_date = '0000-00-00 00:00:00' AND m.to_date = '0000-00-00 00:00:00')),'O','C') as store_status
FROM
    m_product_variant pv
        LEFT JOIN
    m_product p ON pv.product_seq = p.seq
        JOIN
    m_merchant m ON m.seq = p.merchant_seq
        JOIN
    m_product_category mpcat on mpcat.seq=p.category_l2_seq
        INNER JOIN
    m_variant_value vv ON pv.variant_value_seq = vv.seq
        LEFT JOIN
    m_product_stock ps ON ps.`product_variant_seq` = pv.`seq`
        LEFT JOIN
    (select
    pc.promo_credit_name,
    pc.minimum_nominal,
    pc.maximum_nominal,
    pcp.product_variant_seq
    from m_promo_credit_product pcp LEFT JOIN m_promo_credit pc on pcp.promo_credit_seq = pc.seq
    where pc.status = 'A' and
    CURDATE() between pc.promo_credit_period_from and pc.promo_credit_period_to) as qry
    on qry.product_variant_seq = pv.seq and pv.sell_price between qry.minimum_nominal and qry.maximum_nominal
        LEFT JOIN
    (select
    pc.promo_credit_name as pcredit_name,
    pc.minimum_nominal as pc_min_nominal,
    pc.maximum_nominal as pc_max_nominal,
    mpcc.category_seq
    from m_promo_credit_category mpcc LEFT JOIN m_promo_credit pc on mpcc.promo_credit_seq = pc.seq
    where pc.status = 'A' and
    CURDATE() between pc.promo_credit_period_from and pc.promo_credit_period_to) as qry2
    on qry2.category_seq = mpcat.parent_seq and pv.sell_price between qry2.pc_min_nominal and qry2.pc_max_nominal
",@AGENT_TABLE," ",@WHERE," ",@ORDER," LIMIT ?,?");
PREPARE STMT FROM @sql;
SET @START = pSTART;
SET @LIMIT = pLIMIT;
EXECUTE STMT USING @START, @LIMIT;
DEALLOCATE PREPARE STMT;
END $$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_login_partner`(
    pUserID VARCHAR(100),
    pPassword VARCHAR(100),
    pIPAddress Varchar(50)
)
BEGIN
Declare new_seq Int;
Declare partner_seq Int;
declare user_group_seq int;
Select Max(seq) + 1 Into new_seq From m_partner_login_log;
If new_seq Is Null Then Set new_seq = 1; End If;
select seq into user_group_seq from m_user_group where `name`='PARTNER';
SELECT 
    u.seq,
    u.email,
    u.`name` AS user_name,
    u.password,
    user_group_seq,
    u.last_login,
    g.`name`,
    u.profile_img
FROM m_partner u
JOIN m_user_group g ON g.seq=user_group_seq
WHERE u.email = pUserID;
		
Select 
    m.`name` as menu_name,
    m.title_name,
    up.menu_cd,
    up.can_add,
    up.can_edit,
    up.can_view,
    up.can_delete,
    up.can_print,
    up.can_auth,
    mp.menu_cd as parent_menu_cd,
    mp.title_name as parent_menu_name,
    mp.url as parent_menu_url,
    m.detail
From m_partner u 
Join m_user_group_permission up on up.user_group_seq = user_group_seq
Join s_menu m on m.menu_cd = up.menu_cd 
left join s_menu mp on m.parent_menu_cd = mp.menu_cd
Where u.email = pUserID;
if exists (select seq from m_partner where email = pUserID and `password` = pPassword) then
Begin 
    update m_partner 
    set last_login = now(), ip_address = pIPAddress
    Where email = pUserID;
    select seq into partner_seq 
    from m_partner
    where email = pUserID;
    
    insert into m_partner_login_log (
        `seq`,
        `partner_seq`,
        `ip_address`,
        `status`,
        `created_date`
    ) values (
        new_seq,
        partner_seq,
        pIPAddress,
        'S',
        now()
    );
End;
Else
    if Exists(select seq from m_partner where email = pUserID) then
    Begin
        select seq into partner_seq 
        from m_partner
        where email = pUserID;
        insert into m_partner_login_log (
            seq,
            partner_seq,
            ip_address,
            `status`,
            created_date
        ) values (
            new_seq,
            partner_seq,
            pIPAddress,
            'F',
            now()
        );
    End;
    end if;
End if;
    
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_get_left_nav_partner;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_get_left_nav_partner`(
	pUserID VarChar(50)
)
BEGIN
DECLARE user_group_seq INT;
SELECT seq INTO user_group_seq FROM m_user_group WHERE `name`='PARTNER';
Select
	m.menu_cd,
    case when m.parent_menu_cd is null then 'ROOT' else parent_menu_cd end as parent_menu_cd,
    m.title_name `name`,
    m.url,
    m.`order`,
    m.icon,
    m.detail
From 
	s_menu m join m_user_group_permission ug
		on ug.menu_cd = m.menu_cd
			 join m_partner u 
		on ug.user_group_seq = user_group_seq
Where 
	u.email = pUserID and m.active = '1' and m.detail = '0'
Order by `order`;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_get_password_partner;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_get_password_partner`(
	pPartnerSeq TinyInt Unsigned
)
BEGIN
	SELECT
		seq,
		email,
		`password`,
		`name`
	FROM m_partner
	WHERE seq = pPartnerSeq;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_change_password_partner;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_change_password_partner`(
	pUserID VarChar(25), 
	pIPAddr VarChar(50),
    pPartnerSeq TinyInt Unsigned,
    pPassword VarChar(1000)
)
BEGIN  
	Update
		m_partner
    Set 
		`password` = pPassword,
        modified_by = pUserID,
        modified_date = Now()
    Where seq = pPartnerSeq;
    
End$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_partner_change_password_log;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_partner_change_password_log`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
	pPartnerSeq Int unsigned,
	pOldPassword VarChar(1000)
)
BEGIN
	Declare new_seq Int unsigned;
	Select 
		Max(seq) + 1 Into new_seq
	From m_partner_log_security;
	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
    
	Insert Into `m_partner_log_security`(  
		partner_seq,
		seq,
		`type`,
        code,
		old_pass,
		ip_addr,
		created_by,
		created_date
	) Values (
		pPartnerSeq,
		new_seq,
		'1',
        '',
		pOldPassword,
		pIPAddr ,
		pUserID,
		Now()
	);
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_partner_log_data_by_type;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_partner_log_data_by_type`(
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pPartnerSeq Int unsigned
)
BEGIN
Select
    seq as partner_seq,
    'L' as status,
    `email`,
    `name`,
    `password`,
    `mobile_phone` as `phone_no`,
    `profile_img` as `pic1_name`,
    `status`,
    `bank_name`,
    `bank_branch_name`,
    `bank_acct_no`,
    `bank_acct_name`,
    `last_login`,
    `ip_address`,
    `created_by`,
    `created_date`,
    `modified_by`,
    `modified_date`
From m_partner m
Where seq = pPartnerSeq;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_logo_partner_by_seq;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_logo_partner_by_seq`(
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pSeq bigint unsigned
)
BEGIN
    Select
        seq,
        profile_img,
        `name`
    From 
        m_partner
    Where
        seq = pSeq;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_profile_image_partner_update;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_profile_image_partner_update`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq bigint unsigned,
        pName varchar(100),
        pProfileImg varchar(50)
    )
BEGIN
update m_partner set 
	`name` = pName,
	profile_img = pProfileImg,
	modified_by = pUserID,
	modified_date = now() 
where seq = pSeq;
END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS sp_check_agent_exist;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_check_agent_exist`(
        `pEmail` VARCHAR(50)
    )
BEGIN
select * from m_agent where email = pEmail;
END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS sp_review_product_list_agent;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_review_product_list_agent`(
       `pUserID` VARCHAR(25),
       `pIPAddr` VARCHAR(50),
       `pSeq` BIGINT UNSIGNED
    )
BEGIN
	Select 
		count(top.`product_variant_seq`) as count_msg
		From `t_order_product` top JOIN
		`t_order_merchant` tom on  top.`order_seq`=tom.`order_seq` and top.`merchant_info_seq`=tom.`merchant_info_seq`
		join t_order tor on tom.`order_seq`=tor.`seq`
	Where tor.`payment_status`='P' and tom.`order_status`='D' AND
	tor.`agent_seq` = pSeq and (tor.`seq`,top.`product_variant_seq`) not in
	(select order_seq,product_variant_seq from `m_product_review`);                
	Select
		t_op.order_seq,
		t_op.merchant_info_seq,
		t_op.product_variant_seq,
		t_op.variant_value_seq,
		t_op.product_status,
		t_op.qty,
		t_op.sell_price,
		t_op.`product_status`,
		t_o.order_no,
		t_o.order_date,
		p.`name` as product_name,
		pv.`pic_1_img`,
		pv.`seq`,
		vv.`value` as variant_value,
		mmi.`merchant_seq`
		From `t_order_product` t_op
		Join `t_order_merchant` t_om 
		On t_op.order_seq = t_om.order_seq and t_op.`merchant_info_seq` = t_om.`merchant_info_seq`
		Join m_merchant_info mmi 
		On t_op.`merchant_info_seq`=mmi.seq
		Join t_order t_o 
		On t_op.order_seq = t_o.seq
		Join m_product_variant pv 
		On t_op.product_variant_seq = pv.seq
		Join m_product p 
		On pv.product_seq = p.seq
		Join m_variant_value vv
		On pv.variant_value_seq = vv.seq
	Where t_o.`payment_status`='P' and t_om.`order_status`='D' AND
	t_o.`agent_seq` = pSeq and (t_o.`seq`,t_op.`product_variant_seq`) not in
	(select order_seq,product_variant_seq from `m_product_review`)
	order by t_o.order_date desc,t_op.order_seq;        
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_agent_order_product_return_list;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_agent_order_product_return_list`(
        `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pCurrPage` INTEGER UNSIGNED,
        `pRecPerPage` INTEGER UNSIGNED,
        `pDirSort` VARCHAR(4),
        `pColumnSort` VARCHAR(50),
        `pAgentSeq` BIGINT(10)
    )
BEGIN
    -- Begin variables for Paging
    Declare startRec Int unsigned;
    Declare totalPage Int unsigned;
    Declare totalRec Int unsigned;
    -- End variables for Paging
    -- Begin SQL Where
    Declare sqlWhere VarChar(500);
	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
    
    -- End SQL Where
    -- Begin Paging Info
    Set @sqlCommand = "Select Count(o.seq) Into @totalRec FROM	t_order o
		JOIN
	t_order_merchant om ON o.seq = om.order_seq
		JOIN
	t_order_product op ON om.order_seq = op.order_seq
		AND om.merchant_info_seq = op.merchant_info_seq
		JOIN
	t_order_product_return opt ON op.order_seq = opt.order_seq
		AND op.product_variant_seq = opt.product_variant_seq
		JOIN
	m_product_variant pv ON opt.product_variant_seq = pv.seq
		JOIN
	m_product p ON pv.product_seq = p.seq
		JOIN
	m_variant_value vv ON pv.variant_value_seq = vv.seq
		JOIN
	m_merchant_info mi ON op.merchant_info_seq = mi.seq
		JOIN
	m_merchant m ON mi.merchant_seq = m.seq";
    Set @sqlCommand = Concat(@sqlCommand, " Where o.agent_seq ='" ,pAgentSeq, "'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);
        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;
        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info
    Set @sqlCommand = "
        SELECT 
			opt.seq,
			opt.return_no,
			op.created_date,
			o.order_no,
			opt.awb_member_no,
			pv.pic_1_img AS img_product,
			p.name AS product_name,
			vv.value AS variant_name,
			opt.qty,
			opt.return_status,
			opt.shipment_status,
			opt.review_member,
			opt.review_admin,
			opt.review_merchant,
			o.agent_seq,
			pv.variant_value_seq,
			opt.product_variant_seq,
			m.seq as me_seq
		FROM
			t_order o
				JOIN
			t_order_merchant om ON o.seq = om.order_seq
				JOIN
			t_order_product op ON om.order_seq = op.order_seq
				AND om.merchant_info_seq = op.merchant_info_seq
				JOIN
			t_order_product_return opt ON op.order_seq = opt.order_seq
				AND op.product_variant_seq = opt.product_variant_seq
				JOIN
			m_product_variant pv ON opt.product_variant_seq = pv.seq
				JOIN
			m_product p ON pv.product_seq = p.seq
				JOIN
			m_variant_value vv ON pv.variant_value_seq = vv.seq
				JOIN
			m_merchant_info mi ON op.merchant_info_seq = mi.seq
				JOIN
			m_merchant m ON mi.merchant_seq = m.seq	";
    Set @sqlCommand = Concat(@sqlCommand, " Where o.agent_seq ='" ,pAgentSeq, "'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    Select pCurrPage, totalPage, totalRec as total_rec;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set @totalRec = Null;
    Set @sqlCommand = Null;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_agent_order_list;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_agent_order_list`(
        `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pagentSeq` BIGINT(10),
        `pCurrPage` INTEGER UNSIGNED,
        `pRecPerPage` INTEGER UNSIGNED,
        `pDirSort` VARCHAR(4),
        `pColumnSort` VARCHAR(50),
        `pOrderNo` VARCHAR(50),
        `pDateSearch` CHAR(1),
        `pOrderDateFrom` DATETIME,
        `pOrderDateTo` DATETIME,
        `pPaymentStatus` CHAR(1),
        `pIdentityNo` varchar(50)
    )
BEGIN
    
    Declare startRec Int unsigned;
    Declare totalPage Int unsigned;
    Declare totalRec Int unsigned;
    
    
    Declare sqlWhere VarChar(500);
	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
    If pOrderNo <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And o.order_no like '%" , escape_string(pOrderNo), "%'");
    End If;
    If (pOrderDateFrom <> "") Then
        Set sqlWhere = Concat(sqlWhere, " And o.order_date >='" , pOrderDateFrom, "'");
    End If;
    If (pOrderDateTo <> "") Then
        Set sqlWhere = Concat(sqlWhere, " And o.order_date <='" , pOrderDateTo, "'");
    End If;
    If pPaymentStatus <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And o.payment_status ='" , pPaymentStatus, "'");
    End If;
    IF pIdentityNo <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And c.identity_no like '%" , escape_string(pIdentityNo), "%'");
    END IF;
    
    
    Set @sqlCommand = "Select Count(o.order_no) Into @totalRec
                        From
                        (select order_no, order_date, payment_status, agent_seq from
                        m_agent m Join t_order o On m.seq = o.agent_seq) as o 
                        LEFT JOIN t_order_loan l ON l.order_seq=o.order_no
			LEFT JOIN m_agent_customer c ON c.seq=l.`customer_seq`
			";
    Set @sqlCommand = Concat(@sqlCommand, " Where o.agent_seq ='" ,pagentSeq, "'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);
        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;
        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    
    Set @sqlCommand = "
        Select
            o.seq,
            o.agent_name,
            o.order_no,
            o.order_date,
            o.total_order,
            o.payment_status,
            o.paid_date,
            o.payment_method,
            '' as detail,
            '' as payment,
	    c.`identity_no`,
	    c.`pic_name`,
	    l.dp,
	    l.admin_fee,
	    l.tenor,
	    l.loan_interest,
	    l.installment,
	    l.total_installment,
	    l.status_order
            From
        (
        select
            o.seq,
            o.agent_seq,
            m.name as agent_name,
            o.order_no,
            o.order_date,
            o.total_order,
            o.payment_status,
            o.paid_date,
            pgm.name as payment_method,
            '' as detail,
            '' as payment
		from
                    m_agent m
		Join
                    t_order o
                            On m.seq = o.agent_seq
		Join
                    m_payment_gateway_method pgm
                            On o.pg_method_seq = pgm.seq
		) as o
        LEFT JOIN t_order_loan l ON l.order_seq=o.seq
        LEFT JOIN m_agent_customer c ON c.seq=l.`customer_seq`
		";
    Set @sqlCommand = Concat(@sqlCommand, " Where o.agent_seq ='" ,pagentSeq, "'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    Select pCurrPage, totalPage, totalRec as total_rec;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set @totalRec = Null;
    Set @sqlCommand = Null;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_get_info_password_agent;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_get_info_password_agent`(
        `pUserID` VARCHAR(50),
        `pIPAddr` VARCHAR(50),
        `pOldPassword` VARCHAR(1000),
        `pNewPassword` VARCHAR(1000),
        `pEncryptOldPassword` VARCHAR(1000),
        `pEncryptNewPassword` VARCHAR(1000)
    )
BEGIN
SELECT
	`password` as `old_password`
FROM m_agent
WHERE email = pUserID;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_agent_update_settings;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_agent_update_settings`(
        `pIPAddr` VARCHAR(50),
        `pUserID` BIGINT UNSIGNED,
        `pNewPassword` VARCHAR(1000)
    )
BEGIN
/*Update m_agent*/
Update m_agent Set
    password = pNewPassword,
    modified_by = pUserID,
    modified_date = now()
Where
	seq = pUserID;
END$$
DELIMITER ;

-- if exists drop
DROP PROCEDURE IF EXISTS sp_agent_change_password_log;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_agent_change_password_log`(
       `pUserID` VARCHAR(100),
       `pIPAddr` VARCHAR(50),
       `pagentSeq` BIGINT UNSIGNED,
       `pOldPassword` VARCHAR(1000)
    )
BEGIN
	Declare new_seq Int;
	Select 
		Max(seq) + 1 Into new_seq
	From m_agent_log_security;
	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
    
	Insert Into `m_agent_log_security`(  
		agent_seq,
		seq,
		`type`,
        code,
		old_pass,
		ip_addr,
		created_by,
		created_date
	) Values (
		pagentSeq,
		new_seq,
		'1',
        '',
		pOldPassword,
		pIPAddr ,
		pagentSeq,
		Now()
	);
END$$
DELIMITER ;

-- if exists drop
DROP PROCEDURE IF EXISTS sp_thumbs_detail;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_thumbs_detail`(
        `pProductVariantSeq` INTEGER UNSIGNED,
        `pUser` VARCHAR(10),
        `pAgentSeq` BIGINT(20)
    )
BEGIN
DECLARE pProductSeq INT UNSIGNED;
SELECT
	product_seq INTO pProductSeq
FROM
	m_product_variant
WHERE
	`active`='1' AND
	seq = pProductVariantSeq;
IF pUser = 'AGENT' THEN
SELECT
    p.name AS `name`,
    p.seq AS product_seq,
    p.`merchant_seq` AS merchant_seq,
    vv.seq AS variant_seq,
    vv.value AS `variant_value`,
    pv.`product_price` AS `product_price`,
    pv.`sell_price` AS `sell_price`,
    pv.`seq` AS `product_variant_seq`,
    p.`description` AS description,
    p.`specification` AS specification,
    p.`warranty_notes` AS guarante,
    p.`p_weight_kg` AS weight,
    p.`p_length_cm` AS dimension_lenght,
    p.`p_width_cm` AS dimension_width,
    p.`p_height_cm` AS dimension_height,
    m.`name` AS merchant_name,
    m.`logo_img`,
    pv.`pic_1_img` AS image1,
    pv.`pic_2_img` AS image2,
    pv.`pic_3_img` AS image3,
    pv.`pic_4_img` AS image4,
    pv.`pic_5_img` AS image5,
    pv.`pic_6_img` AS image6,
    pv.`max_buy`,
    m.`code` AS `code`,
    d.`name` AS merchant_district,
    c.`name` AS merchant_city,
    pr.`name` AS merchant_province,
    p.category_l2_seq AS `category_l2_seq`,
    p.category_ln_seq AS `category_ln_seq`,
    ps.`stock` AS `stock`,
    ps.`merchant_sku` AS `sku`,
    qry.seq AS promo_credit_bank_seq,
    m.to_date,
    CONCAT(' x ',qry.credit_month,' Bulan ',' -- ',qry.bank_name) AS credit_month,
    qry.`status` AS status_credit ,
    IF(matc.commission_fee_percent is null,CEIL(mpc.commission_fee_percent * pv.`sell_price`/100),CEIL(matc.commission_fee_percent * pv.`sell_price`/100)) AS commission_fee,
    IF(matc.commission_fee_percent IS NULL,mpc.commission_fee_percent,matc.commission_fee_percent) as commission_fee_percent,
    IF((NOW() < m.from_date OR NOW() > m.to_date OR (m.from_date = '0000-00-00 00:00:00' AND m.to_date = '0000-00-00 00:00:00')),'O','C') AS store_status,
    qry2.pc_status, qry2.pc_seq
FROM
    m_product_variant pv
        LEFT JOIN
    m_product p ON pv.product_seq = p.seq
        INNER JOIN
    m_variant_value vv ON pv.variant_value_seq = vv.seq
        INNER JOIN
    m_merchant m ON p.`merchant_seq` = m.`seq`
        LEFT JOIN
    m_product_stock ps ON ps.`product_variant_seq` = pv.`seq`
        JOIN
    (select * from m_product_category where `level`=2) mpc on mpc.seq = p.category_l2_seq
	JOIN
    m_district d ON d.seq = m.district_seq
        JOIN
    m_city c ON c.seq = d.city_seq
        JOIN
    m_province pr ON pr.seq = c.province_seq
	LEFT JOIN
	(SELECT
	pc.seq,
	pc.promo_credit_name,
        pc.minimum_nominal,
        pc.maximum_nominal,
        pc.status,
        pcp.product_variant_seq,
        b.bank_name,
        bc.credit_month
	FROM
            m_promo_credit_product pcp
        LEFT JOIN
            m_promo_credit pc ON pcp.promo_credit_seq = pc.seq
        LEFT JOIN
            m_promo_credit_bank pcb ON pcb.promo_seq = pc.seq
        LEFT JOIN
            m_bank_credit bc ON bc.seq = pcb.bank_credit_seq
        LEFT JOIN
            m_bank b ON b.seq = bc.bank_seq
	WHERE pc.status = 'A' AND
        CURDATE() BETWEEN pc.promo_credit_period_from AND pc.promo_credit_period_to) AS qry ON qry.product_variant_seq = pv.seq
        and pv.sell_price between qry.minimum_nominal and qry.maximum_nominal

        LEFT JOIN
            (select
            pc.seq pc_seq,
            pc.status as pc_status,
            pc.minimum_nominal as pc_min_nominal,
            pc.maximum_nominal as pc_max_nominal,
            mpcc.category_seq
            from m_promo_credit_category mpcc LEFT JOIN m_promo_credit pc on mpcc.promo_credit_seq = pc.seq
            where pc.status = 'A' and
            CURDATE() between pc.promo_credit_period_from and pc.promo_credit_period_to) as qry2
            on qry2.category_seq = mpc.parent_seq and pv.sell_price between qry2.pc_min_nominal and qry2.pc_max_nominal
        LEFT JOIN (select * from m_agent_trx_commission where agent_seq=pAgentSeq) as matc ON matc.category_l2_seq= p.`category_l2_seq`
WHERE
	pv.`active`='1' AND
	pv.seq= pProductVariantSeq AND
	pv.status IN ('L','C')
LIMIT 1;
else
SELECT
    p.name AS `name`,
    p.seq AS product_seq,
    p.`merchant_seq` AS merchant_seq,
    vv.seq AS variant_seq,
    vv.value AS `variant_value`,
    pv.`product_price` AS `product_price`,
    pv.`sell_price` AS `sell_price`,
    pv.`seq` AS `product_variant_seq`,
    p.`description` AS description,
    p.`specification` AS specification,
    p.`warranty_notes` AS guarante,
    p.`p_weight_kg` AS weight,
    p.`p_length_cm` AS dimension_lenght,
    p.`p_width_cm` AS dimension_width,
    p.`p_height_cm` AS dimension_height,
    m.`name` AS merchant_name,
    m.`logo_img`,
    pv.`pic_1_img` AS image1,
    pv.`pic_2_img` AS image2,
    pv.`pic_3_img` AS image3,
    pv.`pic_4_img` AS image4,
    pv.`pic_5_img` AS image5,
    pv.`pic_6_img` AS image6,
    pv.`max_buy`,
    m.`code` AS `code`,
    d.`name` AS merchant_district,
    c.`name` AS merchant_city,
    pr.`name` AS merchant_province,
    p.category_l2_seq AS `category_l2_seq`,
    p.category_ln_seq AS `category_ln_seq`,
    ps.`stock` AS `stock`,
    ps.`merchant_sku` AS `sku`,
    qry.seq AS promo_credit_bank_seq,
    m.to_date,
    CONCAT(' x ', qry.credit_month, ' Bulan ',' -- ',qry.bank_name) AS credit_month, qry.`status` AS status_credit,
    qry2.pc_status, qry2.pc_seq,
    IF((NOW() < m.from_date OR NOW() > m.to_date OR (m.from_date = '0000-00-00 00:00:00' AND m.to_date = '0000-00-00 00:00:00')),'O','C') as store_status
FROM
    m_product_variant pv
        LEFT JOIN
    m_product p ON pv.product_seq = p.seq
        JOIN
    m_product_category mpcat on mpcat.seq=p.category_l2_seq
        INNER JOIN
    m_variant_value vv ON pv.variant_value_seq = vv.seq
        INNER JOIN
    m_merchant m ON p.`merchant_seq` = m.`seq`
        LEFT JOIN
    m_product_stock ps ON ps.`product_variant_seq` = pv.`seq`
        JOIN
    m_district d ON d.seq = m.district_seq
        JOIN
    m_city c ON c.seq = d.city_seq
        JOIN
    m_province pr ON pr.seq = c.province_seq
	LEFT JOIN
	(SELECT
		pc.seq,
		pc.promo_credit_name,
        pc.minimum_nominal,
        pc.maximum_nominal,
        pc.status,
        pcp.product_variant_seq,
        b.bank_name,
        bc.credit_month
	FROM
		m_promo_credit_product pcp
        LEFT JOIN m_promo_credit pc ON
		pcp.promo_credit_seq = pc.seq
	LEFT JOIN
            	m_promo_credit_bank pcb ON pcb.promo_seq = pc.seq
        LEFT JOIN
                m_bank_credit bc ON bc.seq = pcb.bank_credit_seq
        LEFT JOIN
                m_bank b ON b.seq = bc.bank_seq
	WHERE pc.status = 'A' AND
        CURDATE() BETWEEN pc.promo_credit_period_from AND pc.promo_credit_period_to) AS qry ON qry.product_variant_seq = pv.seq
        and pv.sell_price between qry.minimum_nominal and qry.maximum_nominal

	LEFT JOIN
		(select
		pc.seq pc_seq,
		pc.status as pc_status,
		pc.minimum_nominal as pc_min_nominal,
		pc.maximum_nominal as pc_max_nominal,
		mpcc.category_seq
		from m_promo_credit_category mpcc LEFT JOIN m_promo_credit pc on mpcc.promo_credit_seq = pc.seq
		where pc.status = 'A' and
		CURDATE() between pc.promo_credit_period_from and pc.promo_credit_period_to) as qry2
		on qry2.category_seq = mpcat.parent_seq and pv.sell_price between qry2.pc_min_nominal and qry2.pc_max_nominal

WHERE
	pv.`active`='1' AND
	pv.seq= pProductVariantSeq AND
	pv.status IN ('L','C')
LIMIT 1;
END IF;
SELECT
     a.`name` AS `name`,
     av.`value` AS `value`
FROM
    `m_product` p
    INNER JOIN `m_product_attribute` pa
        ON (`pa`.`product_seq` = `p`.`seq`)
    INNER JOIN `m_attribute_value` av
        ON (`pa`.`attribute_value_seq` = `av`.`seq`)
    INNER JOIN `m_attribute` a
        ON (`av`.`attribute_seq` = `a`.`seq`)
WHERE
	`p`.`seq`= pProductSeq
UNION ALL
SELECT
	ps.name AS `name`,
	ps.value AS `value`
FROM m_product_spec ps
WHERE ps.product_seq= pProductSeq;
END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_get_dropdown_agent_address`(
        `pagentSeq` BIGINT UNSIGNED
    )
BEGIN
Select 	
	seq,
    alias
From
	m_agent_address
Where
	agent_seq = pagentSeq;
    
END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS sp_order_add;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_order_add`(
        `pUserID` VARCHAR(25),
        `pIPAddress` VARCHAR(25),
        `pMemberSeq` MEDIUMINT UNSIGNED,
        `pOrderNo` VARCHAR(20),
        `pReceiverName` VARCHAR(100),
        `pReceiverAddress` TEXT,
        `pReceiverDistrictSeq` MEDIUMINT UNSIGNED,
        `pReceiverZipCode` VARCHAR(10),
        `pReceiverPhone` VARCHAR(50),
        `pPaymentStatus` CHAR(1),
        `pPaymentMethodSeq` TINYINT UNSIGNED,
        `pVoucherSeq` MEDIUMINT UNSIGNED,
        `pCouponSeq` INTEGER UNSIGNED,
        `pCreditSeq` INTEGER UNSIGNED,
        `pAgentSeq` MEDIUMINT UNSIGNED
    )
BEGIN
Declare new_seq SmallInt Unsigned;
Declare new_coupon_seq SmallInt Unsigned;
Select 
	Max(seq) + 1 Into new_seq
From t_order;
If new_seq Is Null Then
	Set new_seq = 1;
End If;
Select 
	Max(seq) + 1 Into new_coupon_seq
From m_coupon_trx
where
    coupon_seq = pCouponSeq;
If new_seq Is Null Then
	Set new_seq = 1;
End If;
If new_coupon_seq Is Null Then
	Set new_coupon_seq = 1;
End If;
if pVoucherSeq = 0 then
	set pVoucherSeq = null;
else
	update m_promo_voucher set
		trx_no = pOrderNo
	where
		seq = pVoucherSeq;
end if;
if pCouponSeq = 0 then
	set pCouponSeq = null;
else
    insert m_coupon_trx (
        coupon_seq,
        seq,
        trx_no,
        created_by,
        created_date
    ) values (
        pCouponSeq,
        new_coupon_seq,
        pOrderNo,
        pUserID,
        now()
    );
end if;
if pCreditSeq=0 THEN
	set pCreditSeq=null;
end if;

if pMemberSeq="" THEN
	set pMemberSeq=null;
end if;
if pAgentSeq="" THEN
	set pAgentSeq=null;
end if;
insert into t_order (
	seq,
    order_no,
    order_date,
    member_seq,
    agent_seq,
    receiver_name,
    receiver_address,
    receiver_district_seq,
    receiver_zip_code,
    receiver_phone_no,
    payment_status,
    pg_method_seq,
    voucher_seq,
    coupon_seq,
    promo_credit_seq,
    conf_pay_note_file,
    conf_pay_bank_seq,
    created_by,
    created_date,
    modified_by,
    modified_date
) values (
	new_seq,
    pOrderNo,
    now(),
    pMemberSeq,
    pAgentSeq,
    pReceiverName,
    pReceiverAddress,
    pReceiverDistrictSeq,
    pReceiverZipCode,
    pReceiverPhone,
    pPaymentStatus,
    pPaymentMethodSeq,
    pVoucherSeq,
    pCouponSeq,
    pCreditSeq,
    '',
    null,
    pUserID,
    now(),
    pUserID,
    now()
);
Select new_seq;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_agent_address_add`(
        `pUserID` VARCHAR(100),
        `pIPAddress` VARCHAR(50),
        `pagentSeq` INTEGER UNSIGNED,
        `pReceiverName` VARCHAR(50),
        `pAddress` VARCHAR(500),
        `pDistrictSeq` SMALLINT UNSIGNED,
        `pPhoneNo` VARCHAR(50),
        `pPostalCode` VARCHAR(20),
        `pAlias` VARCHAR(25)
    )
BEGIN
Declare new_seq Int unsigned;
Select 
	Max(seq) + 1 Into new_seq
From m_agent_address
where
	agent_seq = pagentSeq;
If new_seq Is Null Then
	Set new_seq = 1;
End If;
   
insert into m_agent_address (
	agent_seq,
    seq,
    alias,
    address,
    district_seq,
    zip_code,
	pic_name,
    phone_no,
    `default`,
    active,
    created_by,
    created_date,
    modified_by,
    modified_Date
) values (
	pagentSeq,
    new_seq,
    pAlias,
    pAddress,
    pDistrictSeq,
    pPostalCode,
    pReceiverName,
    pPhoneNo,
    '1',
    '1',
    pUserID,
    now(),
    pUserID,
    now()
);
    
    
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_get_location;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_get_location`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq Int,
        cSeq Int,
        dSeq int
    )
BEGIN
    -- Begin SQL Where
    Declare sqlWhere VarChar(500);
    Set sqlWhere = "";
    Set @sqlCommand = "
        Select
		  *
        From `v_location`
    ";
	If pSeq <> "" Then
        Set sqlWhere = Concat(sqlWhere, " and p_id=" , pSeq);
    End If;
    If cSeq <> "" Then
        Set sqlWhere = Concat(sqlWhere, " and c_id=" , cSeq);
    End If;
    If dSeq <> "" Then
        Set sqlWhere = Concat(sqlWhere, " and d_id=" , dSeq);
    End If;
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1" , sqlWhere);
    End If;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS sp_login_merchant;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_login_merchant`(
    pUserID VARCHAR(100),
    pPassword VARCHAR(100),
    pIPAddress Varchar(50)
)
BEGIN
Declare new_seq Int;
Declare merchant_seq Int;
Select 
	Max(seq) + 1 Into new_seq
From m_merchant_login_log;
If new_seq Is Null Then
	Set new_seq = 1;
End If;
Select 
	u.seq,
	u.email,
    u.`name` as user_name,
    u.old_password,
    u.new_password,
    u.user_group_seq,
    u.last_login,
    g.`name`,
    u.logo_img
From
	m_merchant u
Join 
	m_user_group g On g.seq = u.user_group_seq
Where 
	u.email = pUserID;
Select 
	m.`name` as menu_name,
    m.title_name,
	up.menu_cd,
    up.can_add,
    up.can_edit,
    up.can_view,
    up.can_delete,
    up.can_print,
    up.can_auth,
    mp.menu_cd as parent_menu_cd,
    mp.title_name as parent_menu_name,
    mp.url as parent_menu_url,
    m.detail
From 
	m_merchant u 
Join 
	m_user_group_permission up on up.user_group_seq = u.user_group_seq
Join
	s_menu m on m.menu_cd = up.menu_cd 
left join 
    s_menu mp on m.parent_menu_cd = mp.menu_cd
Where
	u.email = pUserID;
if exists (select seq from m_merchant where email = pUserID and new_password = pPassword) then
Begin 
	update m_merchant set
		last_login = now(),
        ip_address = pIPAddress
	Where
		email = pUserID;
	
    select 
		seq into merchant_seq 
	from
		m_merchant
	where
		email = pUserID;
    
    insert into m_merchant_login_log (
		seq,
        merchant_seq,
		ip_address,
        status,
        created_date
	) values (
		new_seq,
        merchant_seq,
        pIPAddress,
        'S',
        now()
	);
End;
Else
	if Exists(select seq from m_merchant where email = pUserID) then
	Begin
        select 
			seq into merchant_seq 
		from
			m_merchant
		where
			email = pUserID;
            
		insert into m_merchant_login_log (
			seq,
			merchant_seq,
			ip_address,
			status,
			created_date
		) values (
			new_seq,
			merchant_seq,
			pIPAddress,
			'F',
			now()
		);
	End;
	end if;
End if;
    
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_get_dropdown_agent_email;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_get_dropdown_agent_email`()
BEGIN
Select 
	seq,
	email 
From 
	m_agent
Order By
	email;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_agent_update_profile_image;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_agent_update_profile_image`(
        `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pagentSeq` BIGINT(20),
        `pProfileImg` VARCHAR(50)
    )
BEGIN
Update m_agent Set
    profile_img = pProfileImg,
    modified_by = pagentSeq,
    modified_date = now()
Where
	seq = pagentSeq;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_agent_address_update;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_agent_address_update`(
        `pUserID` BIGINT UNSIGNED,
        `pIPAddr` VARCHAR(50),
        `pAddressSeq` TINYINT UNSIGNED,
        `pAlias` VARCHAR(50),
        `pPicName` VARCHAR(50),
        `pAddress` VARCHAR(100),
        `pDistrictSeq` BIGINT UNSIGNED,
        `pZipCode` VARCHAR(15),
        `pPhoneNo` VARCHAR(50)
    )
BEGIN
	Update 
		m_agent_address
    Set
		alias = pAlias,
        address = pAddress,
        district_seq = pDistrictSeq,
        zip_code = pZipCode,
        pic_name = pPicName,
        phone_no = pPhoneNo,
        `default` = '1',
        active = '1',
        created_by = created_by,
        created_date = created_date,
        modified_by = pUserID,
        modified_date = Now()
	Where
		agent_seq = pUserID AND seq = pAddressSeq;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_agent_address_delete;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_agent_address_delete`(
        `pagentSeq` BIGINT UNSIGNED,
        `pIPAddr` VARCHAR(50),
        `pSeq` TINYINT UNSIGNED
    )
BEGIN
Delete From 
	m_agent_address 
Where 
	seq = pSeq And
    agent_seq = pagentSeq;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_agent_update_contact;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_agent_update_contact`(
         `pUserID` VARCHAR(50),
         `pIPAddr` VARCHAR(50),
         `pagentSeq` BIGINT UNSIGNED,
         `pagentName` VARCHAR(50),
         `pGender` CHAR(1),
         `pBirthday` DATETIME,
         `pMobilePhone` VARCHAR(25)
    )
BEGIN
/*Update m_agent*/
Update m_agent Set
    name = pagentName,
    birthday = pBirthday,
    gender = pGender,
    mobile_phone = pMobilePhone,
    modified_by = pUserID,
    modified_date = now()
Where
	seq = pagentSeq;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_get_dropdown_agent_address_by_seq;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_get_dropdown_agent_address_by_seq`(
        `pagentSeq` BIGINT UNSIGNED,
        `pSeq` SMALLINT UNSIGNED
    )
BEGIN
Declare pCitySeq SmallInt unsigned;
Declare pProvinceSeq Tinyint unsigned;
	select city_seq into pCitySeq
	from
		m_district d join m_agent_address m
			on m.district_seq = d.seq
	where
		m.agent_seq = pagentSeq and
		m.seq = pSeq;
	select province_seq into pProvinceSeq
	from
		m_district d join m_agent_address m
			on m.district_seq = d.seq
					 join m_city c
			on d.city_seq = c.seq
	where
		m.agent_seq = pagentSeq and
		m.seq = pSeq;
	Select
		m.pic_name,
		m.phone_no,
		m.zip_code,
		m.address,
		c.province_seq,
		d.city_seq,
		m.district_seq
	from
		m_agent_address m join m_district d
			on d.seq = m.district_seq
						   join m_city c
			on c.seq = d.city_seq
						   join m_province p
			on p.seq = c.province_seq
	where
		m.agent_seq = pagentSeq and
		m.seq = pSeq;
	Select
		seq,
		name
	from
		m_city
	where
		province_seq = pProvinceSeq;
	Select
		seq,
		name
	from
		m_district
	where
		city_seq = pCitySeq;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_get_valid_voucher;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_get_valid_voucher`(
        `pUserSeq` INTEGER UNSIGNED,
        `pVoucherSeq` BIGINT UNSIGNED,
        `pVoucherNode` CHAR(3),
        `pUser` VARCHAR(10)
    )
BEGIN

	IF Exists(Select pv.seq from m_promo_voucher pv join m_promo_voucher_period pvp on 
    pvp.seq = pv.promo_seq where pv.seq = pVoucherSeq and pv.member_seq is null and pv.agent_seq is null 
    and pvp.node_cd = pVoucherNode) Then
		IF pUser = 'agent' THEN 
			update 
			    m_promo_voucher pv join m_promo_voucher_period pvp
			    on pvp.seq = pv.promo_seq
			Set
			    pv.agent_seq = pUserSeq,
			    pv.active_date = now(),
			    pv.exp_date = pvp.date_to,
			    pv.modified_by = pUserSeq,
			    pv.modified_date = now()
			Where
			    pv.seq = pVoucherSeq;
		else
			update 
			    m_promo_voucher pv join m_promo_voucher_period pvp
			    on pvp.seq = pv.promo_seq
			Set
			    pv.member_seq = pUserSeq,
			    pv.active_date = now(),
			    pv.exp_date = pvp.date_to,
			    pv.modified_by = pUserSeq,
			    pv.modified_date = now()
			Where
			    pv.seq = pVoucherSeq;
		END IF;
	End If;
		
	IF pUser = 'agent' THEN 

		Select
			seq
		from
			m_promo_voucher
		where
		    exp_date >= CURDATE() and
		    agent_seq = pUserSeq and
		    seq = pVoucherSeq and trx_no ='';

	Else
		Select
			seq
		from
			m_promo_voucher
		where
		    exp_date >= CURDATE() and
		    member_seq = pUserSeq and
		    seq = pVoucherSeq and trx_no ='';

	End If;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_merchant_delivery_by_seq;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_merchant_delivery_by_seq`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pSeq BIGINT (20)unsigned,
        pMerchantId BIGINT(20) unsigned
    )
BEGIN
	SELECT 
t_o.`seq`,
t_o.`order_no`,
t_o.`order_date`,
t_o.`member_seq`,
t_o.`agent_seq`,
t_o.`receiver_name`,
t_o.`receiver_address`,
t_o.`receiver_district_seq`,
t_o.`receiver_zip_code`,
t_o.`receiver_phone_no`,
t_o.`payment_status`,
t_o.`pg_method_seq`,
t_o.`paid_date`,
t_o.`payment_retry`,
t_o.`total_order`,
t_o.`voucher_seq`,
t_o.`voucher_refunded`,
t_o.`coupon_seq`,
t_o.`total_payment`,
t_o.`conf_pay_type`,
t_o.`conf_pay_amt_member`,
t_o.`conf_pay_date`,
t_o.`conf_pay_note_file`,
t_o.`conf_pay_bank_seq`,
t_o.`conf_pay_amt_admin`,
t_om.`merchant_info_seq`,
t_om.`expedition_service_seq`,
t_om.`real_expedition_service_seq`,
t_om.`total_merchant`,
t_om.`total_ins`,
t_om.`total_ship_real`,
t_om.`total_ship_charged`,
t_om.`order_status`,
t_om.`member_notes`,
t_om.`printed`,
t_om.`print_date`,
t_om.`awb_seq`,
t_om.`awb_no`,
t_om.`ref_awb_no`,
t_om.`ship_by`,
t_om.`ship_by_exp_seq`,
t_om.`ship_date`,
t_om.`ship_note_file`,
t_om.`ship_notes`,
t_om.`received_date`,
t_om.`received_by`
FROM 
  `t_order` t_o
  
  inner join 
  (select tom.* from `t_order_merchant` tom join 
  `m_merchant_info` mmi on tom.`merchant_info_seq`=mmi.`seq`
  where mmi.`merchant_seq`=pMerchantId) t_om
  on t_o.`seq`=t_om.`order_seq`
	Where
		t_o.`seq` = pSeq       
        ;        
        
  select 
  t_op.`order_seq`,
  t_op.`merchant_info_seq`,
  t_op.`product_variant_seq`,
  t_op.`variant_value_seq`,
  t_op.`qty`,
  t_op.`sell_price`,
  t_op.`weight_kg`,
  t_op.`ship_price_real`,
  t_op.`ship_price_charged`,
  t_op.`ins_rate_percent`,
  t_op.`product_status`,
  v_pv.`merchant_seq`,
  v_pv.`seq`,
  v_pv.`name`,
  v_pv.`include_ins`,
  v_pv.`category_l2_seq`,
  v_pv.`category_ln_seq`,
  v_pv.`notes`,
  v_pv.`description`,
  v_pv.`content`,
  v_pv.`warranty_notes`,
  v_pv.`p_weight_kg`,
  v_pv.`p_length_cm`,
  v_pv.`p_width_cm`,
  v_pv.`p_height_cm`,
  v_pv.`b_weight_kg`,
  v_pv.`b_length_cm`,
  v_pv.`b_width_cm`,
  v_pv.`b_height_cm`,
  v_pv.`product_seq`,
  v_pv.`variant_seq`,
  v_pv.`product_price`,
  v_pv.`disc_percent`,
  v_pv.`sell_price` as product_sell_price,
  v_pv.`order`,
  v_pv.`max_buy`,
  v_pv.`status`,
  v_pv.`pic_1_img`,
  v_pv.`pic_2_img`,
  v_pv.`pic_3_img`,
  v_pv.`pic_4_img`,
  v_pv.`pic_5_img`,
  v_pv.`pic_6_img`,
  v_pv.`1star`,
  v_pv.`2star`,
  v_pv.`3star`,
  v_pv.`4star`,
  v_pv.`5star`,
  v_pv.`merchant_sku`,
  v_pv.`stock`,
  v_pv.`variant_value`
  from 
 `t_order_product` t_op
 join (select seq,merchant_seq from m_merchant_info where merchant_seq=pMerchantId) mmi
 on t_op.`merchant_info_seq`=mmi.seq
  inner join `v_product_variant` v_pv
  on t_op.`product_variant_seq`=v_pv.`variant_seq`
	Where
		t_op.`order_seq` = pSeq
        ;        
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_member_order_list_by_order_no;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_member_order_list_by_order_no`(
        `pUserID` VARCHAR(100),
        `pIPAddress` VARCHAR(25),
        `pOrderNo` VARCHAR(20)
    )
BEGIN
IF EXISTS (SELECT order_no FROM t_order WHERE order_no = pOrderNo) THEN
BEGIN
SELECT 
	o.seq,
	o.order_no,
	o.order_date,
	o.member_seq,
	o.agent_seq,
    o.signature,
    m.`name` member_name,
    m.email,
    m.mobile_phone,
    c.`name` city_name,
    CASE WHEN o.payment_retry <> '0'  THEN CONCAT(o.order_no, '-', CAST(o.payment_retry AS CHAR(2))) ELSE o.order_no END order_trans_no,	
	o.receiver_name,
	o.receiver_address,
	o.receiver_district_seq,
    d.`name` district_name,
    c.`name` city_name,
    p.`name` province_name,
	o.receiver_zip_code,
	o.receiver_phone_no,
	o.payment_status,
	o.pg_method_seq,
    pg.`code` payment_code,
    pg.name payment_name,
	o.paid_date,
    CASE WHEN pv.nominal IS NULL THEN mc.nominal ELSE pv.nominal END AS nominal,
    pv.code AS voucher_code,
	o.payment_retry,
	o.total_order,
	o.voucher_seq,
	o.voucher_refunded,
	o.total_payment,
	o.conf_pay_type,
	o.conf_pay_amt_member,
	o.conf_pay_date,
	o.conf_pay_note_file,
	o.conf_pay_bank_seq,
	o.conf_pay_amt_admin,
    o.promo_credit_seq
FROM 
	t_order o JOIN m_district d
		ON o.receiver_district_seq = d.seq
			  JOIN m_city c 
		ON c.seq = d.city_seq 
			  JOIN m_province p
		ON p.seq = c.province_seq
			  JOIN m_payment_gateway_method pg
		ON pg.seq = o.pg_method_seq
        	LEFT JOIN m_promo_voucher pv 
		ON o.voucher_seq = pv.seq
        	LEFT JOIN m_coupon mc 
		ON o.coupon_seq = mc.seq
		left JOIN m_member m
		ON m.seq = o.member_seq
		left join m_agent a
		on a.seq = o.agent_seq
WHERE
	o.order_no = pOrderNo;
    
SELECT 
    t.order_seq,
    t.merchant_info_seq,
    t.expedition_service_seq,
    e.`name` expedition_name,
    mm.`name` merchant_name,
    mm.email,
    mm.code AS merchant_code,
    mm.seq merchant_seq,
    t.real_expedition_service_seq,
    t.total_merchant,
    t.total_ins,
    t.total_ship_real,
    t.total_ship_charged,
    t.free_fee_seq,
    t.order_status,
    t.member_notes,
    t.printed,
    t.print_date,
    t.awb_seq,
    t.awb_no,
    t.ref_awb_no,
    t.ship_by,
    t.ship_by_exp_seq,
    t.ship_date,
    t.ship_note_file,
    t.ship_notes,
    t.received_date,
    t.received_by,
    t.redeem_seq,
    t.exp_invoice_seq,
    t.exp_invoice_awb_seq,
    (SELECT 
            COUNT(*) total_barang
        FROM
            t_order_product top
                JOIN
            t_order tor ON top.order_seq = tor.seq
        WHERE
            tor.order_no = o.order_no
                AND top.merchant_info_seq = t.merchant_info_seq) AS total_product,
	t.created_date,
    o.paid_date,
    t.`modified_date`
FROM
    t_order_merchant t
        JOIN
    m_merchant_info m ON m.seq = t.merchant_info_seq
        JOIN
    m_district d ON d.seq = m.pickup_district_seq
        JOIN
    m_city c ON c.seq = d.city_seq
        JOIN
    m_province p ON p.seq = c.province_seq
        JOIN
    m_merchant mm ON mm.seq = m.merchant_seq
        JOIN
    m_expedition_service s ON s.seq = t.expedition_service_seq
        JOIN
    m_expedition e ON s.exp_seq = e.seq
        JOIN
    t_order o ON o.seq = t.order_seq
WHERE
    o.order_no = pOrderNo;
            
SELECT 
	t.order_seq,
    mi.merchant_seq,
	t.merchant_info_seq,
	p.`name` AS display_name,
    v.seq AS product_seq,
    v.pic_1_img AS img,
    mv.`value`,
	mv.seq AS value_seq,
	mv.`value` AS variant_name,
    t.product_status,
	t.qty,
	t.sell_price,
	t.weight_kg,
	t.ship_price_real,
	t.ship_price_charged,
	t.trx_fee_percent,
	t.ins_rate_percent,
	t.product_status,
	t.qty_return,
	t.created_by,
	t.created_date,
	t.modified_by,
	t.modified_date
FROM
	t_order_product t 
		JOIN m_product_variant v			
			ON v.seq = t.product_variant_seq
		JOIN m_product p
			ON v.product_seq = p.seq
		JOIN m_variant_value vv
			ON vv.seq = t.variant_value_seq
		JOIN m_variant_value mv 
			ON mv.seq = v.variant_value_seq
		JOIN t_order o
			ON o.seq = t.order_seq
		JOIN m_merchant_info mi
			ON mi.seq = t.merchant_info_seq
		JOIN m_merchant mm 
			ON mm.seq = mi.merchant_seq
WHERE
	o.order_no = pOrderNo;
    
SELECT 
    'Pesanan telah kami terima dan dalam proses verifikasi' AS history_message,
    created_date 
FROM
    t_order
WHERE
    order_no = pOrderNo
UNION ALL SELECT 
    'Pesanan telah terverifikasi dan akan segera dikirim',
    paid_date 
FROM
    t_order
WHERE
    order_no = pOrderNo 
UNION ALL SELECT 
    CONCAT('Pesanan sedang diproses untuk dikirim oleh ',mm.name),
    t.print_date
FROM
    t_order_merchant t
		JOIN
    m_merchant_info m ON m.seq = t.merchant_info_seq
		JOIN
    m_merchant mm ON mm.seq = m.merchant_seq
        JOIN
    t_order o ON o.seq = t.order_seq
WHERE
    o.order_no = pOrderNo  AND  t.print_date <> "0000-00-00 00:00:00"
UNION ALL SELECT 
    CONCAT('Pesanan telah dikirim oleh ',
			mm.`name`,
            ' dan melalui ',
            e.`name`,
            ', no.resi ',
            t.awb_no),
    t.ship_date 
FROM
    t_order_merchant t
    	JOIN
    m_merchant_info m ON m.seq = t.merchant_info_seq
		JOIN
    m_merchant mm ON mm.seq = m.merchant_seq
        JOIN
    m_expedition_service s ON s.seq = t.expedition_service_seq
        JOIN
    m_expedition e ON s.exp_seq = e.seq
        JOIN
    t_order o ON o.seq = t.order_seq
WHERE
    o.order_no = pOrderNo AND t.ship_date <> "0000-00-00"
UNION ALL SELECT 
    'Pesanan telah diterima pelanggan',
     t.received_date 
FROM
    t_order_merchant t
        JOIN
    t_order o ON o.seq = t.order_seq
WHERE
    o.order_no = pOrderNo  AND t.received_date <> "0000-00-00"
ORDER BY created_date DESC;    
END;
ELSE
BEGIN
SELECT
	o.seq,
	o.order_no,
    o.order_no AS order_trans_no,
    o.order_date,
    o.member_seq,
    o.total_order,
    o.total_payment,
    o.payment_status,
    o.order_status,
	o.signature,
	o.pg_method_seq,
    o.phone_no AS phone_number,
    psn.nominal,
    ps.name AS provider_service_name,
    p.name AS provider_name,
    pgm.name AS pg_method_name,
    pgm.code AS payment_code
FROM 
	t_order_voucher o JOIN m_provider_service_nominal psn
		ON psn.seq = o.provider_nominal_seq
					  JOIN m_provider_service ps
		ON ps.seq = psn.provider_service_seq
					  JOIN m_provider p
		ON p.seq = ps.provider_seq
					  JOIN m_payment_gateway_method pgm
		ON pgm.seq = o.pg_method_seq
					  JOIN m_payment_gateway pg
		ON pg.seq = pgm.pg_seq
WHERE
	order_no = pOrderNo;
END;
END IF;	
END$$
DELIMITER ;

/*
==========================================================================
-- STORED PROCEDURE MOBILE 
==========================================================================
*/

/*!50003 DROP PROCEDURE IF EXISTS sp_m_agent_account_list*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_m_agent_account_list`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pAgentSeq int unsigned,
		IN pSTART INTEGER, 
		IN pLIMIT INTEGER
) 
BEGIN
SET @sql = CONCAT("SELECT
		agent_seq,
        seq,
        mutation_type,
        pg_method_seq,
        trx_type, 
        trx_no,
        trx_date,
        deposit_trx_amt,
        non_deposit_trx_amt,
        bank_name, 
        bank_branch_name, 
        bank_acct_no,
        bank_acct_name, 
        refund_date, 
        status
	From 
		t_agent_account
	where 
		agent_seq = ",pAgentSeq ," ORDER BY created_date DESC
		LIMIT ?,?");
PREPARE STMT FROM @sql;
SET @START = pSTART; 
SET @LIMIT = pLIMIT; 
EXECUTE STMT USING @START, @LIMIT;
DEALLOCATE PREPARE STMT;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_m_agent_add*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_m_agent_add`(
        `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pEmail` VARCHAR(50),
        `pName` VARCHAR(100),
        `pDate` DATE,
        `pGender` VARCHAR(1),
        `pPhone` VARCHAR(20),
        `pPassword` VARCHAR(100),
        `pImage` VARCHAR(100),
        `pPartnerSeq` INTEGER(10),
        `pStatus` CHAR(1)
    ) 
BEGIN
	DECLARE new_seq INT UNSIGNED;
	SELECT 
        MAX(seq) + 1 INTO new_seq
    FROM m_agent;
    IF new_seq IS NULL THEN
        SET new_seq = 1;
    END IF;
INSERT INTO m_agent(
    seq,
    email,
    password,
    name,
    partner_seq,
    referral,
    birthday,
    gender,
    mobile_phone,
    bank_name,
    bank_branch_name,
    bank_acct_no,
    bank_acct_name,
    profile_img,
    deposit_amt,
    status,
    last_login,
    ip_address,
    created_by,
    created_date,
    modified_by,
    modified_date
)VALUES(
    new_seq,
    pEmail,
    pPassword,
    pName,
    pPartnerSeq,
    NULL,
    pDate,
    pGender,
    pPhone,
	'',
    '',
    '',
    '',
    pImage,
    '0',
    pStatus,
    NULL,
    NULL,
    pUserID,
    now(),
    pUserID,
    now()
);
select
new_seq;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_m_agent_address_list*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_m_agent_address_list`(
	pUserID VarChar(50), 
	pIPAddr VarChar(50),
	pAgentSeq int Unsigned,
    IN pSTART INTEGER, 
	IN pLIMIT INTEGER
) 
BEGIN
SET @sql = CONCAT("SELECT 
    ma.seq AS address_seq,
    ma.alias,
    ma.address,
    ma.zip_code,
    ma.pic_name,
    ma.phone_no,
    p.seq AS province_seq,
    p.`name` AS province_name,
    c.seq AS city_seq,
    c.`name` AS city_name,
    d.seq AS district_seq,
    d.`name` AS district_name
FROM
    m_agent_address ma
        LEFT JOIN
    m_district d ON d.seq = ma.district_seq
        LEFT JOIN
    m_city c ON c.seq = d.city_seq
        LEFT JOIN
    m_province p ON p.seq = c.province_seq
	where 
		ma.agent_seq = ",pAgentSeq ," ORDER BY ma.created_date DESC
		LIMIT ?,?");
PREPARE STMT FROM @sql;
SET @START = pSTART; 
SET @LIMIT = pLIMIT; 
EXECUTE STMT USING @START, @LIMIT;
DEALLOCATE PREPARE STMT;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_m_agent_commission_list*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_m_agent_commission_list`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pAgentSeq BigInt(10),
        pPaymentStatus Char(1),
        pDateSearch Char(1),
		pOrderDateFrom Datetime,
		pOrderDateTo Datetime
) 
BEGIN
  Declare sqlWhere VarChar(500); 
  Set sqlWhere = "";
      If (pDateSearch = "0")  Then
        Set sqlWhere = Concat(sqlWhere, " And tor.order_date between '" , pOrderDateFrom, "' and '" ,pOrderDateTo, "'");
	  End If;
       If pPaymentStatus = "P" Then
        Set sqlWhere = Concat(sqlWhere, " And tom.order_status = 'D' ");
    End If;
    If pPaymentStatus = "U" Then
        Set sqlWhere = Concat(sqlWhere, " And tom.order_status in ('R','S') ");
    End If;
	Set @sqlCommand = "
        SELECT 
			tor.seq,
			tor.agent_seq,
			tor.order_no,
			tor.order_date,
			tor.receiver_name,
			tor.receiver_address,
			tor.receiver_district_seq,
			tor.receiver_zip_code,
			tor.receiver_phone_no,
            tor.order_date,
			tom.order_status,
			tom.received_date,
			top.product_variant_seq,
			top.variant_value_seq,
			top.qty,
			top.sell_price,
			top.weight_kg,
			top.ship_price_real,
			top.commission_fee_percent,
			md.`name` district_name,
			mc.`name` city_name,
			mp.`name` province_name,
			mpr.`name` AS display_name,
			mpr.merchant_seq,
			mpv.seq AS product_seq,
			mpv.pic_1_img AS img,
			mv.`value`,
			mv.seq AS value_seq,
			mv.`value` AS variant_name
		FROM
			t_order tor
				JOIN
			t_order_merchant tom ON tor.seq = tom.order_seq
				JOIN
			t_order_product top ON tor.seq = top.order_seq
				AND tom.merchant_info_seq = top.merchant_info_seq
				AND top.order_seq = tom.order_seq
				JOIN
			m_district md ON tor.receiver_district_seq = md.seq
				JOIN
			m_city mc ON mc.seq = md.city_seq
				JOIN
			m_province mp ON mp.seq = mc.province_seq
				JOIN
			m_product_variant mpv ON mpv.seq = top.product_variant_seq
				JOIN
			m_product mpr ON mpv.product_seq = mpr.seq
				JOIN
			m_variant_value vv ON vv.seq = top.variant_value_seq
				JOIN
			m_variant_value mv ON mv.seq = mpv.variant_value_seq
		";
	Set @sqlCommand = Concat(@sqlCommand, " Where tor.agent_seq ='" ,pAgentSeq, "' and tor.payment_status='P' and top.product_status='R' ");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    Set @sqlCommand = Concat(@sqlCommand, " order by ", "tor.order_date", " DESC" );
    
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set @sqlCommand = Null;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_m_agent_list_partner*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_m_agent_list_partner`(
		`pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pCurrPage` INTEGER UNSIGNED,
        `pRecPerPage` INTEGER UNSIGNED,
        `pEmail` VARCHAR(100),
        `pName` VARCHAR(100),
        `pStatus` CHAR(3),
        `pPartnerSeq` INTEGER(10)
) 
BEGIN
  Declare sqlWhere VarChar(500); 
  Set sqlWhere = "";
	   IF pEmail <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And email like '%" , escape_string(pEmail), "%'");
    END IF;
    
    IF pName <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And a.name like '%" , escape_string(pName), "%'");
    END IF;
    
    IF pStatus <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And status = '" , pStatus, "'");
    END IF;
    IF pPartnerSeq <> "" THEN
         SET sqlWhere = CONCAT(sqlWhere, " And partner_seq = '" , pPartnerSeq, "'" );
     END IF;
	Set @sqlCommand = "
        SELECT 
		  a.`seq` as agent_seq,
		  a.`email`,
		  a.`password`,
		  a.`name`,
		  a.`partner_seq`,
		  a.`referral`,
		  a.`birthday`,
		  a.`gender`,
		  a.`mobile_phone`,
		  a.`profile_img`,
		  a.`deposit_amt`,
		  a.`status`,
		  a.`last_login`,
		  a.`ip_address`,
		  a.`created_by`,
		  a.`created_date`,
		  a.`modified_by`,
		  a.`modified_date`,
		  aa.`seq` as agent_address_seq,
		  aa.`alias`,
		  aa.`address`,
		  aa.`district_seq`,
		  aa.`zip_code`,
		  aa.`pic_name`,
		  aa.`phone_no`,
		  aa.`default`,
		  aa.`active`,
		  d.`name` AS district,
		  c.`name` AS city,
		  p.`name` AS province
		FROM `m_agent` AS a
		LEFT JOIN ( SELECT `agent_seq`, `seq`, `alias`, `address`, `district_seq`, `zip_code`, `pic_name`, `phone_no`, `default`, `active`, `created_by`, `created_date`, `modified_by`, MAX(`modified_date`) AS `modified_date` FROM `m_agent_address` GROUP BY agent_seq) AS aa
		ON a.`seq` = aa.`agent_seq` 
		LEFT JOIN m_district d ON aa.district_seq = d.seq 
		LEFT JOIN m_city c ON d.city_seq = c.seq
		LEFT JOIN m_province p ON c.province_seq = p.seq 
		";
    Set @sqlCommand = Concat(@sqlCommand, " Where a.partner_seq ='" ,pPartnerSeq, "'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    Set @sqlCommand = Concat(@sqlCommand, " order by ", "a.created_date", " DESC" );
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", pCurrPage, ", ", pRecPerPage);
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set @sqlCommand = Null;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_m_agent_order_list*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_m_agent_order_list`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pAgentSeq BigInt(10),
		pCurrPage Int unsigned,
		pRecPerPage Int unsigned,
        pOrderNo VarChar(50), 
        pPaymentStatus Char(1),
        pDateSearch Char(1),
		pOrderDateFrom Datetime,
		pOrderDateTo Datetime,
        pIdentityNo varchar(100)
) 
BEGIN
  Declare sqlWhere VarChar(500); 
  Set sqlWhere = "";
	  If pOrderNo <> "" Then
		   Set sqlWhere = Concat(sqlWhere, " And o.order_no like '%" , escape_string(pOrderNo), "%'");
	  End If;
      If (pDateSearch = "0")  Then
        Set sqlWhere = Concat(sqlWhere, " And o.order_date between '" , pOrderDateFrom, "' and '" ,pOrderDateTo, "'");
	  End If;
	  If pPaymentStatus <> "" Then
		   Set sqlWhere = Concat(sqlWhere, " And o.payment_status ='" , pPaymentStatus, "'");
	  End If;
	IF pIdentityNo <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And o.identity_no like '%" , escape_string(pIdentityNo), "%'");
    END IF;
	Set @sqlCommand = "
         SELECT 
    o.seq,
    o.agent_name,
    o.order_no,
    o.order_date,
    o.total_order,
    o.payment_status,
    o.paid_date,
    o.payment_method,
    '' AS detail,
    '' AS payment,
    o.`identity_no`,
    o.`pic_name`,
    o.dp,
    o.admin_fee,
    o.tenor,
    o.loan_interest,
    o.installment,
    o.total_installment,
    o.status_order
FROM
    (SELECT 
        o.seq,
            o.agent_seq,
            m.name AS agent_name,
            o.order_no,
            o.order_date,
            o.total_order,
            o.payment_status,
            o.paid_date,
            pgm.name AS payment_method,
            '' AS detail,
            '' AS payment,
             c.`identity_no`,
             c.pic_name,
			l.dp,
			l.admin_fee,
			l.tenor,
			l.loan_interest,
			l.installment,
			l.total_installment,
			l.status_order
    FROM
        m_agent m
    JOIN t_order o ON m.seq = o.agent_seq
    JOIN m_payment_gateway_method pgm ON o.pg_method_seq = pgm.seq
    LEFT JOIN t_order_loan l ON l.order_seq = o.seq
    LEFT JOIN m_agent_customer c ON c.seq = l.`customer_seq`) AS o
		";
    Set @sqlCommand = Concat(@sqlCommand, " Where o.agent_seq ='" ,pAgentSeq, "'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    Set @sqlCommand = Concat(@sqlCommand, " order by ", "o.order_date", " DESC" );
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", pCurrPage, ", ", pRecPerPage);
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set @sqlCommand = Null;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_m_agent_order_product_return_by_order_no*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_m_agent_order_product_return_by_order_no`(
    pUserID VarChar(100),
    pIPAddr VarChar(50),
    pAgentSeq int unsigned,
    pProductStatus char(1),
    pOrderStatus char(1),
    pFinishDate int(1) unsigned
) 
BEGIN
SELECT 
    o.order_no,
    o.agent_seq,
    o.seq,
    p.seq AS pro_seq,
    pv.pic_1_img AS img,
    pv.variant_value_seq,
    op.product_variant_seq,
    p.name AS product_name,
    vv.value AS variant_name,
    op.qty,
    pv.max_buy,
    op.sell_price,
    m.name AS merchant_name,
    m.seq AS merchant_seq
FROM
    t_order o
        JOIN
    t_order_merchant om ON o.seq = om.order_seq
        JOIN
    t_order_product op ON om.order_seq = op.order_seq
        AND om.merchant_info_seq = op.merchant_info_seq
        JOIN
    m_product_variant pv ON op.product_variant_seq = pv.seq
        JOIN
    m_product p ON pv.product_seq = p.seq
        JOIN
    m_variant_value vv ON pv.variant_value_seq = vv.seq
        JOIN
    m_merchant_info mi ON op.merchant_info_seq = mi.seq
        JOIN
    m_merchant m ON mi.merchant_seq = m.seq
WHERE
    om.received_date >= DATE_ADD(CURDATE(),
        INTERVAL - pFinishDate DAY)
        AND o.agent_seq = pAgentSeq
        AND op.product_status = pProductStatus
        AND om.order_status = pOrderStatus
 ORDER BY om.received_date DESC;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_m_agent_order_product_return_list*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_m_agent_order_product_return_list`(
    pUserID VarChar(100),
    pIPAddr VarChar(50),
	pCurrPage Int unsigned,
	pRecPerPage Int unsigned,
	pAgentSeq BigInt(10)
) 
BEGIN
Declare sqlWhere VarChar(500);
	Set sqlWhere = "";
	Set @sqlCommand = "
			SELECT 
				opt.seq,
				opt.return_no,
				opt.created_date,
				o.order_no,
				opt.awb_member_no,
				pv.pic_1_img AS img_product,
				p.name AS product_name,
				vv.value AS variant_name,
				opt.qty,
				opt.return_status,
				opt.shipment_status,
				opt.review_member,
				opt.review_admin,
				opt.review_merchant,
				o.member_seq,
				pv.variant_value_seq,
                pv.max_buy,
				opt.product_variant_seq,
				m.seq as me_seq
			FROM
				t_order o
					JOIN
				t_order_merchant om ON o.seq = om.order_seq
					JOIN
				t_order_product op ON om.order_seq = op.order_seq
					AND om.merchant_info_seq = op.merchant_info_seq
					JOIN
				t_order_product_return opt ON op.order_seq = opt.order_seq
					AND op.product_variant_seq = opt.product_variant_seq
					JOIN
				m_product_variant pv ON opt.product_variant_seq = pv.seq
					JOIN
				m_product p ON pv.product_seq = p.seq
					JOIN
				m_variant_value vv ON pv.variant_value_seq = vv.seq
					JOIN
				m_merchant_info mi ON op.merchant_info_seq = mi.seq
					JOIN
				m_merchant m ON mi.merchant_seq = m.seq";
		Set @sqlCommand = Concat(@sqlCommand, " Where o.agent_seq ='" ,pAgentSeq, "'");
		If sqlWhere <> "" Then
			Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
		End If;
		Set @sqlCommand = Concat(@sqlCommand, " order by ", "opt.created_date", " DESC" );
		Set @sqlCommand = Concat(@sqlCommand, " Limit ", pCurrPage, ", ", pRecPerPage);
		Prepare sql_stmt FROM @sqlCommand;
		Execute sql_stmt;
		Deallocate prepare sql_stmt;
		Set @sqlCommand = Null;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_m_get_data_bank_credit*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_m_get_data_bank_credit`(
	pPromoCreditSeq  tinyint unsigned,
    pPromoCreditBankSeq tinyint unsigned,
    pProductVariantSeq int unsigned
) 
BEGIN
SELECT 
    cb.seq,
    bc.credit_month,
    bc.rate,
    b.bank_name,
    CONCAT(bc.credit_month,
            ' Bln',
            ' - ',
            b.bank_name) AS bank_credit
FROM
    m_promo_credit_bank cb
        JOIN
    m_promo_credit pc ON pc.seq = cb.promo_seq
        JOIN
    m_promo_credit_product cp ON cp.promo_credit_seq = cb.promo_seq
        JOIN
    m_bank_credit bc ON bc.seq = cb.bank_credit_seq
        JOIN
    m_bank b ON b.seq = bc.bank_seq
WHERE
    cb.promo_seq = pPromoCreditSeq
        AND status = 'A'
        AND cp.product_variant_seq = pProductVariantSeq
        AND cb.seq = pPromoCreditBankSeq
        AND NOW() BETWEEN pc.promo_credit_period_from AND pc.promo_credit_period_to;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_m_get_dropdown_bank_credit*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_m_get_dropdown_bank_credit`(
	pPromoCreditSeq  tinyint unsigned,
    pProductVariantSeq int unsigned
) 
BEGIN
	SELECT 
		cb.seq,
		bc.credit_month,
		bc.rate,
		b.bank_name,
		CONCAT(bc.credit_month,
				' Bln',
				' - ',
				b.bank_name) as bank_credit
	FROM
		m_promo_credit_bank cb
			JOIN
		m_promo_credit pc ON pc.seq = cb.promo_seq
			JOIN
		m_promo_credit_product cp ON cp.promo_credit_seq = cb.promo_seq
			JOIN
		m_bank_credit bc ON bc.seq = cb.bank_credit_seq
			JOIN
		m_bank b ON b.seq = bc.bank_seq
	WHERE
		cb.promo_seq = pPromoCreditSeq AND status = 'A' 
			AND cp.product_variant_seq = pProductVariantSeq
			AND NOW() BETWEEN pc.promo_credit_period_from AND pc.promo_credit_period_to
	ORDER BY bc.rate;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_m_get_voucher_member_by_seq*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_m_get_voucher_member_by_seq`(
		pMemberSeq int unsigned,
		IN pSTART INTEGER, 
		IN pLIMIT INTEGER
) 
BEGIN
SET @sql = CONCAT("SELECT
		pv.code,
		mpv.code as code_refund,
		pv.nominal,
		pv.active_date,
		pv.exp_date,
		pv.trx_use_amt,
		pv.trx_no,
		o.payment_status
	From 
		m_member m
        Join
		m_promo_voucher pv On m.seq = pv.member_seq
		left join t_order o on
		m.seq = o.member_seq AND pv.trx_no = o.order_no
		left join m_promo_voucher mpv on o.voucher_refund_seq = mpv.seq
	where 
		m.seq = ",pMemberSeq ,"       
		LIMIT ?,?");
PREPARE STMT FROM @sql;
SET @START = pSTART; 
SET @LIMIT = pLIMIT; 
EXECUTE STMT USING @START, @LIMIT;
DEALLOCATE PREPARE STMT;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_m_member_account_list*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_m_member_account_list`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pMemberSeq int unsigned,
		IN pSTART INTEGER, 
		IN pLIMIT INTEGER
) 
BEGIN
SET @sql = CONCAT("SELECT
		member_seq,
        seq,
        mutation_type,
        pg_method_seq,
        trx_type, 
        trx_no,
        trx_date,
        deposit_trx_amt,
        non_deposit_trx_amt,
        bank_name, 
        bank_branch_name, 
        bank_acct_no,
        bank_acct_name, 
        refund_date, 
        status
	From 
		t_member_account
	where 
		member_seq = ",pMemberSeq ," ORDER BY created_date DESC
		LIMIT ?,?");
PREPARE STMT FROM @sql;
SET @START = pSTART; 
SET @LIMIT = pLIMIT; 
EXECUTE STMT USING @START, @LIMIT;
DEALLOCATE PREPARE STMT;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_m_member_address_list*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_m_member_address_list`(
    pUserID VarChar(50), 
    pIPAddr VarChar(50),
    pMemberSeq int Unsigned,
    IN pSTART INTEGER, 
    IN pLIMIT INTEGER
) 
BEGIN
SET @sql = CONCAT("SELECT 
    ma.seq AS address_seq,
    ma.alias,
    ma.address,
    ma.zip_code,
    ma.pic_name,
    ma.phone_no,
    p.seq AS province_seq,
    p.`name` AS province_name,
    c.seq AS city_seq,
    c.`name` AS city_name,
    d.seq AS district_seq,
    d.`name` AS district_name
FROM
    m_member_address ma
        LEFT JOIN
    m_district d ON d.seq = ma.district_seq
        LEFT JOIN
    m_city c ON c.seq = d.city_seq
        LEFT JOIN
    m_province p ON p.seq = c.province_seq
	where 
		ma.member_seq = ",pMemberSeq ," ORDER BY ma.created_date DESC
		LIMIT ?,?");
PREPARE STMT FROM @sql;
SET @START = pSTART; 
SET @LIMIT = pLIMIT; 
EXECUTE STMT USING @START, @LIMIT;
DEALLOCATE PREPARE STMT;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_m_member_order_list*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_m_member_order_list`(
        pUserID VarChar(25),
        pIPAddr VarChar(50),
        pMemberSeq BigInt(10),
		pCurrPage Int unsigned,
		pRecPerPage Int unsigned,
        pOrderNo VarChar(50), 
        pPaymentStatus Char(1),
        pDateSearch Char(1),
		pOrderDateFrom Datetime,
		pOrderDateTo Datetime
) 
BEGIN
  Declare sqlWhere VarChar(500); 
  Set sqlWhere = "";
	  If pOrderNo <> "" Then
		   Set sqlWhere = Concat(sqlWhere, " And o.order_no like '%" , escape_string(pOrderNo), "%'");
	  End If;
      If (pDateSearch = "0")  Then
        Set sqlWhere = Concat(sqlWhere, " And o.order_date between '" , pOrderDateFrom, "' and '" ,pOrderDateTo, "'");
	  End If;
	  If pPaymentStatus <> "" Then
		   Set sqlWhere = Concat(sqlWhere, " And o.payment_status ='" , pPaymentStatus, "'");
	  End If;
	Set @sqlCommand = "
        Select
			o.seq,
			o.member_name,
			o.order_no,
			o.order_date,
            o.total_order,
			o.payment_status,
			o.paid_date,
            o.payment_method,
            o.code,
            '' as detail,
            '' as payment
		From 
        (
        select
			o.seq,
            o.member_seq,
			m.name as member_name,
			o.order_no,
			o.order_date,
            o.total_order,
			o.payment_status,
			o.paid_date,
            pgm.name as payment_method,
            pgm.code,
            '' as detail,
            '' as payment
		from
			m_member m
		Join
			t_order o 
				On m.seq = o.member_seq
		Join 
			m_payment_gateway_method pgm
				On o.pg_method_seq = pgm.seq
		union all 
		select
			o.seq,
            o.member_seq,
			m.name as member_name,
			o.order_no,
			o.order_date,
            o.total_order,
			o.payment_status,
			o.created_date,
            pgm.name as payment_method,
            pgm.code,
            '' as detail,
            '' as payment
		from
			m_member m
		Join
			t_order_voucher o 
				On m.seq = o.member_seq
		Join 
			m_payment_gateway_method pgm
				On o.pg_method_seq = pgm.seq
		) as o
		";
    Set @sqlCommand = Concat(@sqlCommand, " Where o.member_seq ='" ,pMemberSeq, "'");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;
    Set @sqlCommand = Concat(@sqlCommand, " order by ", "o.order_date", " DESC" );
    
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", pCurrPage, ", ", pRecPerPage);
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set @sqlCommand = Null;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_m_member_order_product_return_by_order_no*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_m_member_order_product_return_by_order_no`(
    pUserID VarChar(100),
    pIPAddr VarChar(50),
    pMemberSeq int unsigned,
    pOrderNo varchar(50),
    pProductStatus char(1),
    pOrderStatus char(1),
    pFinishDate int(1) unsigned
) 
BEGIN
SELECT 
    o.order_no,
    o.member_seq,
    o.seq,
    p.seq AS pro_seq,
    pv.pic_1_img AS img,
    pv.variant_value_seq,
    op.product_variant_seq,
    p.name AS product_name,
    vv.value AS variant_name,
    op.qty,
    pv.max_buy,
    op.sell_price,
    m.name AS merchant_name,
    m.seq AS merchant_seq
FROM
    t_order o
        JOIN
    t_order_merchant om ON o.seq = om.order_seq
        JOIN
    t_order_product op ON om.order_seq = op.order_seq
        AND om.merchant_info_seq = op.merchant_info_seq
        JOIN
    m_product_variant pv ON op.product_variant_seq = pv.seq
        JOIN
    m_product p ON pv.product_seq = p.seq
        JOIN
    m_variant_value vv ON pv.variant_value_seq = vv.seq
        JOIN
    m_merchant_info mi ON op.merchant_info_seq = mi.seq
        JOIN
    m_merchant m ON mi.merchant_seq = m.seq
WHERE
    om.received_date >= DATE_ADD(CURDATE(),
    INTERVAL - pFinishDate DAY)
    AND o.member_seq = pMemberSeq
    AND op.product_status = pProductStatus
    AND om.order_status = pOrderStatus
 ORDER BY om.received_date DESC;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_m_member_order_product_return_list*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_m_member_order_product_return_list`(
    pUserID VarChar(100),
    pIPAddr VarChar(50),
	pCurrPage Int unsigned,
	pRecPerPage Int unsigned,
	pMemberSeq BigInt(10)
) 
BEGIN
Declare sqlWhere VarChar(500);
	Set sqlWhere = "";
	Set @sqlCommand = "
			SELECT 
				opt.seq,
				opt.return_no,
				opt.created_date,
				o.order_no,
				opt.awb_member_no,
				pv.pic_1_img AS img_product,
				p.name AS product_name,
				vv.value AS variant_name,
				opt.qty,
				opt.return_status,
				opt.shipment_status,
				opt.review_member,
				opt.review_admin,
				opt.review_merchant,
				o.member_seq,
				pv.variant_value_seq,
                pv.max_buy,
				opt.product_variant_seq,
				m.seq as me_seq
			FROM
				t_order o
					JOIN
				t_order_merchant om ON o.seq = om.order_seq
					JOIN
				t_order_product op ON om.order_seq = op.order_seq
					AND om.merchant_info_seq = op.merchant_info_seq
					JOIN
				t_order_product_return opt ON op.order_seq = opt.order_seq
					AND op.product_variant_seq = opt.product_variant_seq
					JOIN
				m_product_variant pv ON opt.product_variant_seq = pv.seq
					JOIN
				m_product p ON pv.product_seq = p.seq
					JOIN
				m_variant_value vv ON pv.variant_value_seq = vv.seq
					JOIN
				m_merchant_info mi ON op.merchant_info_seq = mi.seq
					JOIN
				m_merchant m ON mi.merchant_seq = m.seq";
		Set @sqlCommand = Concat(@sqlCommand, " Where o.member_seq ='" ,pMemberSeq, "'");
		If sqlWhere <> "" Then
			Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
		End If;
		Set @sqlCommand = Concat(@sqlCommand, " order by ", "opt.created_date", " DESC" );
		Set @sqlCommand = Concat(@sqlCommand, " Limit ", pCurrPage, ", ", pRecPerPage);
		Prepare sql_stmt FROM @sqlCommand;
		Execute sql_stmt;
		Deallocate prepare sql_stmt;
		Set @sqlCommand = Null;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_m_review_product_list*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_m_review_product_list`(
        pUserID VARCHAR(25),
        pIPAddr VARCHAR(50),
		pMemberSeq BIGINT UNSIGNED,
        pCurrPage INTEGER,
        pRecPerPage INTEGER
    ) 
BEGIN
Declare sqlWhere VarChar(500);
	Set sqlWhere = "";
    Set @sqlCommand = "
		Select
		t_op.order_seq,
		t_op.merchant_info_seq,
		t_op.product_variant_seq,
		t_op.variant_value_seq,
		t_op.product_status,
		t_op.qty,
		t_op.sell_price,
		t_op.`product_status`,
		t_o.order_no,
		t_o.order_date,
		p.`name` as product_name,
		pv.`pic_1_img`,
		pv.`seq`,
		vv.`value` as variant_value,
		mmi.`merchant_seq`,
        mpr.`created_by`,
        mpr.`rate`,
        mpr.`review`,
        mpr.`review_admin`,
        mpr.`status`,
        mpr.`created_by`,
        mpr.`created_date`,
        mpr.`modified_by`,
        mpr.`modified_date`
		From `t_order_product` t_op
		Join `t_order_merchant` t_om 
		On t_op.order_seq = t_om.order_seq and t_op.`merchant_info_seq` = t_om.`merchant_info_seq`
		Join m_merchant_info mmi 
		On t_op.`merchant_info_seq`=mmi.seq
		Join t_order t_o 
		On t_op.order_seq = t_o.seq
		Join m_product_variant pv 
		On t_op.product_variant_seq = pv.seq
		Join m_product p 
		On pv.product_seq = p.seq
		Left Join m_variant_value vv
		On pv.variant_value_seq = vv.seq
		join m_product_review mpr on
		t_o.`seq`=mpr.order_seq and t_op.`product_variant_seq`=mpr.product_variant_seq
    ";
		Set @sqlCommand = Concat(@sqlCommand, " Where t_o.member_seq ='" ,pMemberSeq, "'");
		If sqlWhere <> "" Then
			Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
		End If;
		
		Set @sqlCommand = Concat(@sqlCommand, " Limit ", pCurrPage, ", ", pRecPerPage);
		Prepare sql_stmt FROM @sqlCommand;
		Execute sql_stmt;
		Deallocate prepare sql_stmt;
		Set @sqlCommand = Null;
END $$
DELIMITER ;

/*!50003 DROP PROCEDURE IF EXISTS sp_m_thumbs_product_review*/;	
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
 DELIMITER $$
CREATE PROCEDURE `sp_m_thumbs_product_review`(
	pProductVariantSeq int unsigned
) 
BEGIN
SELECT 
    pr.product_variant_seq AS product_variant_seq,
    pr.order_seq AS order_seq,
    pr.rate AS rate,
    pr.review AS review,
    pr.review_admin AS review_admin,
    pr.status AS `status`,
    pr.created_by AS created_by,
    pr.created_date AS created_date,
    pr.modified_by AS modified_by,
    pr.modified_date AS modified_date,
    m.profile_img,
    IF(m.name IS NULL, 'Unknown', m.name) AS `name`,
    DATEDIFF(DATE(NOW()), DATE(pr.created_date)) AS `days_old`
FROM
    m_product_review pr
        LEFT JOIN
    m_member m ON pr.`created_by` = m.`seq`
WHERE pr.product_variant_seq = pProductVariantSeq;
END $$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_get_product_info_by_seq;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_get_product_info_by_seq`(
        `pUserID` VARCHAR(25),
        `pIPAddress` VARCHAR(25),
        `pProductVariantSeq` MEDIUMINT UNSIGNED,
        `pExpSeq` TINYINT UNSIGNED,
        `pUser` VARCHAR(10),
	`pAgentSeq` BIGINT
    )
BEGIN
IF(pUser = 'AGENT') THEN
SELECT 
    p.`name` AS product_name,
    pv.seq AS product_seq,
    pv.product_price,
    pv.disc_percent,
    pv.sell_price,
    pv.max_buy,
    vv.seq AS variant_value,
    vv.value AS variant_name,
    CASE WHEN m.expedition_seq = 0  THEN ep.name ELSE e.name END exp_name,
    pv.pic_1_img,
    p.merchant_seq,
    m.`name` AS merchant_name,
    m.`code` AS merchant_code,
    p.p_weight_kg,
    p.p_length_cm,
    p.p_width_cm,
    p.p_height_cm,
    p.b_weight_kg,
    p.b_length_cm,
    p.b_width_cm,
    p.b_height_cm,
    CASE WHEN m.expedition_seq = 0  THEN ep.volume_divider ELSE e.volume_divider END volume_divider,
    CASE WHEN m.expedition_seq = 0  THEN ep.code ELSE e.code END exp_code
    ,CEIL(matc.commission_fee_percent * pv.`sell_price`/100) AS commission_fee, matc.commission_fee_percent
FROM 
	m_product p JOIN m_product_variant pv
		ON pv.product_seq = p.seq
				JOIN m_variant_value vv
		ON vv.seq = pv.variant_value_seq
				JOIN m_merchant m
		ON m.seq = p.merchant_seq
				JOIN m_expedition e
		ON e.seq = m.expedition_seq
				JOIN m_expedition ep
		ON ep.seq = pExpSeq
		LEFT JOIN (
		Select
			case when magt.commission_fee_percent is null then mpc.commission_fee_percent else magt.commission_fee_percent end as commission_fee_percent,prd.category_l2_seq
		From m_product prd join m_product_variant mpv on mpv.product_seq = prd.seq
		Join m_product_category mpc on mpc.seq = prd.category_l2_seq
		left Join (select commission_fee_percent,category_l2_seq,agent_seq from m_agent_trx_commission where agent_seq=pAgentSeq) magt
		on magt.category_l2_seq = mpc.seq
		where 
		mpv.seq = pProductVariantSeq				
		) matc ON matc.category_l2_seq= p.`category_l2_seq`	
WHERE
	pv.seq = pProductVariantSeq;
ELSE
Select 
    p.`name` as product_name,
    pv.seq as product_seq,
    pv.product_price,
    pv.disc_percent,
    pv.sell_price,
    pv.max_buy,
    vv.seq as variant_value,
    vv.value as variant_name,
    case when m.expedition_seq = 0  then ep.name else e.name end exp_name,
    pv.pic_1_img,
    p.merchant_seq,
    m.`name` as merchant_name,
    m.`code` as merchant_code,
    p.p_weight_kg,
    p.p_length_cm,
    p.p_width_cm,
    p.p_height_cm,
    p.b_weight_kg,
    p.b_length_cm,
    p.b_width_cm,
    p.b_height_cm,
    case when m.expedition_seq = 0  then ep.volume_divider else e.volume_divider end volume_divider,
    case when m.expedition_seq = 0  then ep.code else e.code end exp_code
from 
	m_product p join m_product_variant pv
		on pv.product_seq = p.seq
				join m_variant_value vv
		on vv.seq = pv.variant_value_seq
				join m_merchant m
		on m.seq = p.merchant_seq
				join m_expedition e
		on e.seq = m.expedition_seq
				join m_expedition ep
		on ep.seq = pExpSeq		
Where
	pv.seq = pProductVariantSeq;
END IF;   
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_agent_log_security_check;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_agent_log_security_check`(
        `pUri` VARCHAR(50)
    )
BEGIN
select code from m_agent_log_security where code = pUri;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_agent_email_check;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_agent_email_check`(
        `pEmail` VARCHAR(100)
    )
BEGIN
select 
	seq,
    `name`,
    password    
from 
	m_agent 
where email = pEmail;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_agent_update_password_by_email;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_agent_update_password_by_email`(
        `pEmail` VARCHAR(100),
        `pNewpass` TEXT
    )
BEGIN
update m_agent set password = pNewpass where email = pEmail;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_agent_log_security_add;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_agent_log_security_add`(
        `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pMseq` INTEGER(10),
        `pType` CHAR(1),
        `pCode` CHAR(20),
        `pOldPass` VARCHAR(100)
    )
BEGIN
	Declare new_seq int;
	Select 
		Max(seq) + 1 Into new_seq
	From m_agent_log_security;
	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
    
	Insert Into `m_agent_log_security`(  
		agent_seq,
		seq,
		`type`,
        code,
		old_pass,
		ip_addr,
		created_by,
		created_date
	) Values (
		pMseq,
		new_seq,
		pType,
        pCode,
		md5(md5(pOldPass)),
		pIPAddr ,
		pUserID,
		Now()
	);
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_merchant_return_add_refund;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_merchant_return_add_refund`(
        `pMerchantID` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pReturnNo` VARCHAR(100),
        `pMemberSeq` BIGINT,
        `pPgmethodseq` VARCHAR(100),
        `pTrxDate` VARCHAR(100),
        `pDepositTrxAmt` INTEGER UNSIGNED,
        `pMutationType` VARCHAR(10),
        `pTrxType` VARCHAR(10),
        `pNewStatus` VARCHAR(10),
        `pAgentSeq` BIGINT
    )
BEGIN
Declare new_seq INT unsigned;
if pMemberSeq > 0 then
	Select 
		Max(seq) + 1 Into new_seq
	From t_member_account;
	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
    
    insert into t_member_account (
    member_seq,     
    seq, 
    mutation_type,     
    pg_method_seq, 
    trx_type,    
    trx_no,     
    trx_date,     
    deposit_trx_amt, 
    non_deposit_trx_amt,     
    bank_name, 
    bank_branch_name,     
    bank_acct_no, 
    bank_acct_name, 
    refund_date, 
    `status`, 
    created_by, 
    created_date, 
    modified_by, 
    modified_date
    )
   values
   (
    pMemberSeq,
    new_seq,
    pMutationType,
    pPgmethodseq,
    pTrxType,
    pReturnNo,
    pTrxDate,
    pDepositTrxAmt,
    '0',
    ' ',
    ' ',
    ' ',
    ' ',
    0000-00-00,
    pNewStatus,
    pMerchantID,
    now(),
    pMerchantID,
    now()    
   );
else
	Select 
		Max(seq) + 1 Into new_seq
	From t_agent_account;
	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
    
    insert into t_agent_account (
    agent_seq,     
    seq, 
    mutation_type,     
    pg_method_seq, 
    trx_type,    
    trx_no,     
    trx_date,     
    deposit_trx_amt, 
    non_deposit_trx_amt,     
    bank_name, 
    bank_branch_name,     
    bank_acct_no, 
    bank_acct_name, 
    refund_date, 
    `status`, 
    created_by, 
    created_date, 
    modified_by, 
    modified_date
    )
   values
   (
    pAgentSeq,
    new_seq,
    pMutationType,
    pPgmethodseq,
    pTrxType,
    pReturnNo,
    pTrxDate,
    pDepositTrxAmt,
    '0',
    ' ',
    ' ',
    ' ',
    ' ',
    0000-00-00,
    pNewStatus,
    pMerchantID,
    now(),
    pMerchantID,
    now()    
   );
end if;   
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_merchant_return_detail_to_refund;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_merchant_return_detail_to_refund`(
        `pMerchantSeq` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pReturnNo` VARCHAR(50)
    )
BEGIN
select o.member_seq,o.agent_seq,
topr.return_no,
o.pg_method_seq,
topr.created_date as trx_date,
pv.sell_price,
topr.qty*pv.sell_price as deposit_trx_amt
from 
t_order_product_return topr 
join t_order o on topr.order_seq = o.seq  
join m_product_variant pv on topr.product_variant_seq = pv.seq
where topr.return_no = pReturnNo ;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_get_product_info_by_seq;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_get_product_info_by_seq`(
        `pUserID` VARCHAR(25),
        `pIPAddress` VARCHAR(25),
        `pProductVariantSeq` MEDIUMINT UNSIGNED,
        `pExpSeq` TINYINT UNSIGNED,
        `pUser` VARCHAR(10),
	`pAgentSeq` BIGINT
    )
BEGIN
IF(pUser = 'AGENT') THEN
SELECT 
    p.`name` AS product_name,
    pv.seq AS product_seq,
    pv.product_price,
    pv.disc_percent,
    pv.sell_price,
    pv.max_buy,
    vv.seq AS variant_value,
    vv.value AS variant_name,
    CASE WHEN m.expedition_seq = 0  THEN ep.name ELSE e.name END exp_name,
    pv.pic_1_img,
    p.merchant_seq,
    m.`name` AS merchant_name,
    m.`code` AS merchant_code,
    p.p_weight_kg,
    p.p_length_cm,
    p.p_width_cm,
    p.p_height_cm,
    p.b_weight_kg,
    p.b_length_cm,
    p.b_width_cm,
    p.b_height_cm,
    CASE WHEN m.expedition_seq = 0  THEN ep.volume_divider ELSE e.volume_divider END volume_divider,
    CASE WHEN m.expedition_seq = 0  THEN ep.code ELSE e.code END exp_code
    ,CEIL(matc.commission_fee_percent * pv.`sell_price`/100) AS commission_fee, matc.commission_fee_percent
FROM 
	m_product p JOIN m_product_variant pv
		ON pv.product_seq = p.seq
				JOIN m_variant_value vv
		ON vv.seq = pv.variant_value_seq
				JOIN m_merchant m
		ON m.seq = p.merchant_seq
				JOIN m_expedition e
		ON e.seq = m.expedition_seq
				JOIN m_expedition ep
		ON ep.seq = pExpSeq
		LEFT JOIN (
		Select
			case when magt.commission_fee_percent is null then mpc.commission_fee_percent else magt.commission_fee_percent end as commission_fee_percent,prd.category_l2_seq
		From m_product prd join m_product_variant mpv on mpv.product_seq = prd.seq
		Join m_product_category mpc on mpc.seq = prd.category_l2_seq
		left Join (select commission_fee_percent,category_l2_seq,agent_seq from m_agent_trx_commission where agent_seq=pAgentSeq) magt
		on magt.category_l2_seq = mpc.seq
		where 
		mpv.seq = pProductVariantSeq				
		) matc ON matc.category_l2_seq= p.`category_l2_seq`	
WHERE
	pv.seq = pProductVariantSeq;
ELSE
Select 
    p.`name` as product_name,
    pv.seq as product_seq,
    pv.product_price,
    pv.disc_percent,
    pv.sell_price,
    pv.max_buy,
    vv.seq as variant_value,
    vv.value as variant_name,
    case when m.expedition_seq = 0  then ep.name else e.name end exp_name,
    pv.pic_1_img,
    p.merchant_seq,
    m.`name` as merchant_name,
    m.`code` as merchant_code,
    p.p_weight_kg,
    p.p_length_cm,
    p.p_width_cm,
    p.p_height_cm,
    p.b_weight_kg,
    p.b_length_cm,
    p.b_width_cm,
    p.b_height_cm,
    case when m.expedition_seq = 0  then ep.volume_divider else e.volume_divider end volume_divider,
    case when m.expedition_seq = 0  then ep.code else e.code end exp_code
from 
	m_product p join m_product_variant pv
		on pv.product_seq = p.seq
				join m_variant_value vv
		on vv.seq = pv.variant_value_seq
				join m_merchant m
		on m.seq = p.merchant_seq
				join m_expedition e
		on e.seq = m.expedition_seq
				join m_expedition ep
		on ep.seq = pExpSeq		
Where
	pv.seq = pProductVariantSeq;
END IF;   
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_deposit_agent_addtract;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_deposit_agent_addtract`(
        `pUserID` VARCHAR(25),
        `pIPAddress` VARCHAR(25),
        `pagentSeq` INTEGER UNSIGNED,
        `pDepositAmt` DECIMAL(10,0)
    )
BEGIN
update m_agent set 
	deposit_amt = deposit_amt + pDepositAmt,
    modified_by = pUserID,
    modified_date = now()
where
	seq = pagentSeq;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_admin_topup_update_status;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_admin_topup_update_status`(
        `pUserID` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pSeq` BIGINT UNSIGNED,
        `pagentSeq` BIGINT UNSIGNED,
        `pStatus` VARCHAR(1)
    )
BEGIN
update t_agent_account set
	`status` = pStatus,
    modified_by = pUserID,
    modified_date = Now()
Where
	seq=pSeq AND agent_seq=pagentSeq and `status` = 'T';
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_partner_log_security_check;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_partner_log_security_check`(
	pUri varchar(50)
)
BEGIN
	Select
		code
	From m_partner_log_security
	Where 
		code = pUri;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_partner_email_check;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_partner_email_check`(
	pEmail varchar(50)
)
BEGIN
	Select
		seq,
        name,
        password
	From m_partner 
	Where 
		email = pEmail;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_partner_update_password_by_email;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_partner_update_password_by_email`(
	pEmail VarChar(50),
    pNewpass VarChar(100)
)
BEGIN
	Update m_partner Set 
		password = pNewpass 
	Where 
		email = pEmail;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_partner_log_security_add;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_partner_log_security_add`(
	pUserID VarChar(25),
	pIPAddr VarChar(50),
	pMseq Int unsigned,
	pType Char(1),
	pCode Char(20),
	pOldPass VarChar(100)
)
BEGIN
	Declare new_seq SmallInt unsigned;
	Select 
		Max(seq) + 1 Into new_seq
	From m_partner_log_security;
	If new_seq Is Null Then
		Set new_seq = 1;
	End If;
    
	Insert Into `m_partner_log_security`(  
		partner_seq,
		seq,
		`type`,
        code,
		old_pass,
		ip_addr,
		created_by,
		created_date
	) Values (
		pMseq,
		new_seq,
		pType,
        pCode,
		md5(md5(pOldPass)),
		pIPAddr ,
		pUserID,
		Now()
	);
END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS sp_m_member_order_product_return_by_order_no;
DELIMITER $$
CREATE DEFINER=`tk1001`@`%` PROCEDURE `sp_m_member_order_product_return_by_order_no`(
    pUserID VarChar(100),
    pIPAddr VarChar(50),
    pMemberSeq int unsigned,
    pProductStatus char(1),
    pOrderStatus char(1),
    pFinishDate int(1) unsigned
)
BEGIN
SELECT 
    o.order_no,
    o.member_seq,
    o.seq,
    p.seq AS pro_seq,
    pv.pic_1_img AS img,
    pv.variant_value_seq,
    op.product_variant_seq,
    p.name AS product_name,
    vv.value AS variant_name,
    op.qty,
    pv.max_buy,
    op.sell_price,
    m.name AS merchant_name,
    m.seq AS merchant_seq
FROM
    t_order o
        JOIN
    t_order_merchant om ON o.seq = om.order_seq
        JOIN
    t_order_product op ON om.order_seq = op.order_seq
        AND om.merchant_info_seq = op.merchant_info_seq
        JOIN
    m_product_variant pv ON op.product_variant_seq = pv.seq
        JOIN
    m_product p ON pv.product_seq = p.seq
        JOIN
    m_variant_value vv ON pv.variant_value_seq = vv.seq
        JOIN
    m_merchant_info mi ON op.merchant_info_seq = mi.seq
        JOIN
    m_merchant m ON mi.merchant_seq = m.seq
WHERE
    om.received_date >= DATE_ADD(CURDATE(),
    INTERVAL - pFinishDate DAY)
    AND o.member_seq = pMemberSeq
    AND op.product_status = pProductStatus
    AND om.order_status = pOrderStatus
 ORDER BY om.received_date DESC;
END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS sp_redeem_agent_get_lastdate;
DELIMITER $$
CREATE PROCEDURE sp_redeem_agent_get_lastdate (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50)
)
BEGIN
    SELECT DATE_ADD(to_date, INTERVAL 1 DAY) AS from_date,DATE_ADD(to_date, INTERVAL 1 DAY) AS to_date, 0 AS status
    FROM t_redeem_agent_period
    ORDER BY from_date DESC 
    LIMIT 1;
END$$

DROP PROCEDURE IF EXISTS sp_redeem_agent_period_add;
DELIMITER $$
CREATE PROCEDURE sp_redeem_agent_period_add (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pFdate VARCHAR(10),
    pTdate DATE
)
BEGIN
    DECLARE new_seq INT(10) UNSIGNED;
    SELECT MAX(seq) + 1 INTO new_seq
    FROM t_redeem_agent_period;
    IF new_seq IS NULL 
        THEN Set new_seq = 1;
    END IF;
    INSERT INTO t_redeem_agent_period(
        seq, from_date, to_date, status, created_by, created_date, modified_by, modified_date
    ) VALUES (
        new_seq, pFdate, pTdate, 'O', pUserID, NOW(), pUserID, NOW()
    );
END$$

DROP PROCEDURE IF EXISTS sp_redeem_agent_by_seq;
DELIMITER $$
CREATE PROCEDURE sp_redeem_agent_by_seq (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pSeq INTEGER(10) UNSIGNED
)
BEGIN
    SELECT
        seq, from_date, to_date, status, created_by, created_date, modified_by, modified_date
    FROM t_redeem_agent_period
    WHERE seq = pSeq;
END$$

DROP PROCEDURE IF EXISTS sp_redeem_agent_get_order;
DELIMITER $$
CREATE PROCEDURE sp_redeem_agent_get_order (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pToDate DATE
)
BEGIN
    SELECT 
        t_or.agent_seq,
        SUM(t_op.qty * t_op.sell_price * (t_op.commission_fee_partner_percent / 100)) AS total_komisi
    FROM t_order_product t_op
    JOIN t_order_merchant t_om ON t_om.order_seq=t_op.order_seq AND t_op.merchant_info_seq=t_om.merchant_info_seq
    JOIN t_order t_or  ON t_op.order_seq=t_or.seq
    JOIN m_agent m_a ON t_or.agent_seq=m_a.seq
    WHERE 
        t_om.order_status='D'
        AND t_op.product_status='R'
        AND t_om.received_date <= pToDate
        AND t_om.redeem_agent_seq IS NULL
        AND t_or.member_seq IS NULL
    GROUP BY t_or.agent_seq;
END$$

--Tambah field
ALTER TABLE t_order_merchant ADD COLUMN redeem_agent_seq INT(10) UNSIGNED NULL AFTER redeem_seq; -- Untuk perhitungan redeem agent

DROP PROCEDURE IF EXISTS sp_redeem_agent_update_order;
DELIMITER $$
CREATE PROCEDURE sp_redeem_agent_update_order (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pSeq INTEGER(10) UNSIGNED,
    pTdate DATE
)
BEGIN
    UPDATE 
        t_order_merchant tom join t_order tor on tom.order_seq=tor.seq 
    SET
        tom.redeem_agent_seq = pSeq,    
        tom.modified_by = pUserID,
        tom.modified_date = now()
    WHERE
        tom.order_status='D' 
        AND tom.redeem_agent_seq IS NULL 
        AND tom.received_date <= pTdate
        AND tor.member_seq IS NULL;
END$$

DROP PROCEDURE IF EXISTS sp_redeem_agent_period_status_update;
DELIMITER $$
CREATE PROCEDURE sp_redeem_agent_period_status_update (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pSeq INTEGER(10) UNSIGNED,
    pStatusNew CHAR(1),
    pStatusOld CHAR(1)
)
BEGIN
    UPDATE t_redeem_agent_period 
    SET
        status = pStatusNew,
        modified_by = pUserID,
        modified_date = NOW()
    Where
        seq = pSeq AND status = pStatusOld;
END$$

DROP PROCEDURE IF EXISTS sp_order_merchant_by_order;
DELIMITER $$
CREATE PROCEDURE sp_order_merchant_by_order (
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pOrderSeq int(10) unsigned,
    pMerchantId int(10) unsigned
)
BEGIN
    SELECT
        om.order_seq,
        om.ship_note_file,
	o.order_date,
	o.order_no,
        o.payment_status,
        o.member_seq,
        m.email AS merchant_email,
        IFNULL(mb.`name`,a.name) AS member_name,
	om.merchant_info_seq,
	m.`name` AS merchant_name,
	o.pg_method_seq,
	p.`name` AS payment_name,
	om.awb_no,
	om.ship_date,
	o.receiver_address,
	om.order_status,
        om.expedition_service_seq,
        om.real_expedition_service_seq,
        om.received_date,
        exs.seq AS exs_seq,
        exs.name AS exs_name,
        es.seq AS es_seq,
        es.name AS es_name,
        exp.seq AS exp_seq,
        exp.name AS exp_name,
        esp.seq AS expedition_seq,
        esp.name AS esp_name
    FROM t_order_merchant om 
    JOIN t_order o ON om.order_seq = o.seq 
    JOIN m_merchant_info mi ON om.merchant_info_seq = mi.seq
    JOIN m_merchant m ON mi.merchant_seq = m.seq
    LEFT OUTER JOIN m_payment_gateway_method p ON o.pg_method_seq = p.seq
    JOIN m_expedition_service exs ON om.expedition_service_seq = exs.seq
    JOIN m_expedition_service EXP ON om.real_expedition_service_seq = exp.seq
    JOIN m_expedition es ON exs.exp_seq = es.seq   
    JOIN m_expedition esp ON exp.exp_seq = esp.seq
    LEFT JOIN m_member mb ON o.member_seq = mb.seq
    LEFT JOIN m_agent a ON o.agent_seq = a.seq
    WHERE 
        om.order_seq = pOrderSeq 
        AND om.merchant_info_seq = pMerchantId;
END$$

DROP PROCEDURE IF EXISTS sp_redeem_agent_commision_component_add;
DELIMITER $$
CREATE PROCEDURE sp_redeem_agent_commision_component_add (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pSeq INTEGER(10) UNSIGNED,
    pType CHAR(3),
    pMType CHAR(1)
)
BEGIN
    INSERT INTO t_redeem_agent_component(
	redeem_seq,
	partner_seq,
	type,
	mutation_type,
	total,
	created_by,
	created_date,
	modified_by,
	modified_date
	)
    SELECT 
        pSeq, ma.partner_seq, pType, pMtype, SUM(tra.total) total, pUserID, Now(), pUserID, Now() 
    FROM t_redeem_agent tra 
    JOIN m_agent ma ON tra.agent_seq=ma.seq 
    WHERE tra.redeem_seq=pSeq
    GROUP BY ma.partner_seq;
END$$


DROP PROCEDURE IF EXISTS sp_redeem_partner_list;
DELIMITER $$
CREATE PROCEDURE sp_redeem_partner_list (
    `pUserID` VARCHAR(25),
    `pIPAddr` VARCHAR(50),
    `pCurrPage` INTEGER,
    `pRecPerPage` INTEGER,
    `pDirSort` VARCHAR(4),
    `pColumnSort` VARCHAR(50),
    `pSeq` INTEGER(10)
)
BEGIN
    -- Begin variables for Paging
    DECLARE startRec INT;
    DECLARE totalPage INT;
    DECLARE totalRec INT;
    -- End variables for Paging
    -- Begin SQL Where
    DECLARE sqlWhere VARCHAR(500);
	SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    SET sqlWhere = "";
    
    IF pSeq <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " AND c.redeem_seq = '" , pSeq, "'");
    END IF;
    -- End SQL Where
    -- Begin Paging Info
    SET @sqlCommand = "SELECT COUNT(c.partner_seq) INTO @totalRec FROM t_redeem_agent_component c ";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " WHERE 1 " , sqlWhere );
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        End IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    -- End Paging Info
    SET @sqlCommand = "
        SELECT
            a.`redeem_seq`,
            a.`partner_seq`,
            SUM(CASE WHEN a.`mutation_type`= 'D' THEN a.`total` ELSE a.`total` * -1 END) AS total,
            c.`status`,
            c.paid_partner_date AS paid_date,
            b.`name`,
            b.email,
            b.mobile_phone,
            b.bank_name,
            b.bank_branch_name,
            b.bank_acct_no,
            b.bank_acct_name,
            b.profile_img
        FROM 
            `t_redeem_agent_component` a 
        JOIN `m_partner` b ON a.`partner_seq`=b.`seq`
        JOIN (SELECT tra.redeem_seq, tra.status, tra.paid_partner_date, ma.partner_seq FROM t_redeem_agent tra JOIN m_agent ma ON tra.agent_seq=ma.seq ";
    IF pSeq <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " WHERE tra.redeem_seq=" , pSeq, " GROUP BY tra.redeem_seq, tra.status, tra.paid_partner_date, ma.partner_seq) c ON a.`partner_seq`= c.`partner_seq` ");
    END IF;    
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " WHERE 1 " , sqlWhere, " AND a.redeem_seq=",pSeq);
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand, " GROUP BY a.`redeem_seq`, a.`partner_seq`, c.`status`, c.`paid_partner_date`, b.`name`, b.email, b.mobile_phone, b.bank_name, b.bank_branch_name, b.bank_acct_no, b.bank_acct_name, b.profile_img");
    SET @sqlCommand = CONCAT(@sqlCommand, " ORDER BY ", pColumnSort, " ", pDirSort);
    SET @sqlCommand = CONCAT(@sqlCommand, " LIMIT ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @totalRec = NULL;
    SET @sqlCommand = NULL;
END$$

DROP PROCEDURE IF EXISTS sp_redeem_partner_by_seq;
DELIMITER $$
CREATE PROCEDURE sp_redeem_partner_by_seq (
    `pUserID` VARCHAR(25),
    `pIPAddr` VARCHAR(50),
    `pSeq` INTEGER(10) UNSIGNED,
    `pPartnerSeq` INTEGER(10) UNSIGNED
)
BEGIN
SELECT
    a.`redeem_seq`,
    a.`partner_seq`,
    SUM(CASE WHEN a.`mutation_type`= 'D' THEN a.`total` ELSE a.`total` * -1 END) AS total,
    c.`status`,
    c.`paid_date`,
    b.`name`,
    b.email,
    b.mobile_phone,
    b.bank_name,
    b.bank_branch_name,
    b.bank_acct_no,
    b.bank_acct_name,
    b.profile_img
FROM 
    `t_redeem_agent_component` a 
    JOIN `m_partner` b ON a.`partner_seq` = b.`seq`
    JOIN (
        SELECT tra.redeem_seq, tra.status, tra.paid_date, ma.partner_seq 
        FROM 
            t_redeem_agent tra 
        JOIN m_agent ma ON tra.agent_seq = ma.seq AND ma.`partner_seq` = pPartnerSeq 
        WHERE 
            tra.`redeem_seq`=pSeq 
        GROUP BY tra.redeem_seq, tra.status, tra.paid_date, ma.partner_seq
    ) c ON a.`partner_seq`=c.`partner_seq` 
WHERE
    a.`redeem_seq` = pSeq AND a.`partner_seq`=pPartnerSeq
GROUP BY 
    a.`redeem_seq`, a.`partner_seq`, c.`status`, c.`paid_date`, b.`name`, b.email, b.mobile_phone, b.bank_name, b.bank_branch_name, b.bank_acct_no, b.bank_acct_name, b.profile_img;
END$$

DROP PROCEDURE IF EXISTS sp_redeem_agent_status;
DELIMITER $$
CREATE PROCEDURE sp_redeem_agent_status (
    `pUserID` VARCHAR(25),
    `pIPAddr` VARCHAR(50),
    `pSeq` INTEGER(10) UNSIGNED
)
BEGIN
    SELECT 
        a.`redeem_seq`,
        a.`agent_seq`,
        a.`total`,
        a.`status`,
        a.`paid_date`,
        a.`created_by`,
        a.`created_date`,
        a.`modified_by`,
        a.`modified_date`, 
        b.`name`,b.`email` 
    FROM 
        `t_redeem_agent` a
    LEFT OUTER JOIN m_agent b on a.`agent_seq` = b.`seq`
    WHERE 
        `redeem_seq` = pSeq;
END$$

DROP PROCEDURE IF EXISTS sp_redeem_agent_partner_list;
DELIMITER $$
CREATE PROCEDURE sp_redeem_agent_partner_list (
    `pUserID` VARCHAR(25),
    `pIPAddr` VARCHAR(50),
    `pSeq` INTEGER(10) UNSIGNED
)
BEGIN
    SELECT mp.seq, mp.`email`, mp.`name` 
    FROM `m_partner` mp 
    WHERE mp.`seq` IN (
        SELECT DISTINCT(partner_seq) FROM `t_redeem_agent_component` WHERE redeem_seq = pSeq
    );
END$$

DROP PROCEDURE IF EXISTS sp_redeem_agent_update_by_partner;
DELIMITER $$
CREATE PROCEDURE sp_redeem_agent_update_by_partner (
    `pUserID` VARCHAR(25),
    `pIPAddr` VARCHAR(50),
    `pSeq` INTEGER(10) UNSIGNED,
    `pPartnerSeq` INTEGER(10) UNSIGNED,
    `pPaiddate` DATE
)
BEGIN
UPDATE 
    t_redeem_agent 
SET
    paid_partner_date = pPaiddate,
    `status` = 'T',
    modified_by = pUserID,
    modified_date = NOW() 
WHERE agent_seq IN 
  (SELECT 
    seq 
  FROM
    m_agent 
  WHERE partner_seq = pPartnerSeq) 
  AND `redeem_seq` = pSeq 
  AND `status` = 'U' ;
END$$
