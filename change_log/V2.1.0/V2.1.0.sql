/*
============
ADD COLUMN 
============
*/

ALTER TABLE m_product_category ADD variant_additional_seq MEDIUMINT UNSIGNED AFTER variant_seq;
ALTER TABLE m_product_category ADD CONSTRAINT fk_m_variant_m_product_category  FOREIGN KEY (variant_additional_seq) REFERENCES m_variant(seq) ON DELETE RESTRICT ON UPDATE CASCADE;