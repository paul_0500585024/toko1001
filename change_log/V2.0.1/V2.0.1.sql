/*
=================================
Create Table
=================================
*/

CREATE TABLE `t_collecting_period` (
  `seq` int(11) unsigned NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `status` enum('O','P','C') NOT NULL COMMENT 'O = Open ; P = Proccess ; C = Close;',
  `created_by` varchar(25) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` varchar(25) NOT NULL,
  `modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `t_collecting_component` (
  `collect_seq` int(10) unsigned NOT NULL,
  `partner_seq` int(10) unsigned NOT NULL,
  `type` enum('ORD','RET') NOT NULL COMMENT 'ORD = Order ; RET = Retur;',
  `mutation_type` enum('D','C') NOT NULL COMMENT 'D : Kurang Utang, C : Tambah utang',
  `total` decimal(12,0) unsigned NOT NULL,
  `created_by` varchar(25) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` varchar(25) NOT NULL,
  `modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`collect_seq`,`partner_seq`),
  KEY `fk_m_partner_t_collecting_period_idx` (`partner_seq`),
  CONSTRAINT `fk_t_collecting_period_t_collecting_component` FOREIGN KEY (`collect_seq`) REFERENCES `t_collecting_period` (`seq`) ON UPDATE CASCADE,
  CONSTRAINT `fk_m_partner_t_collecting_period` FOREIGN KEY (`partner_seq`) REFERENCES `m_partner` (`seq`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='	';

/*
=================================
Add Data
=================================
*/
INSERT INTO `s_menu` (`module_seq`,`menu_cd`,`parent_menu_cd`,`name`,`title_name`,`url`,`order`,`icon`,`detail`,`level`,`active`,`created_by`,`created_date`) VALUES (1,'MST09005','MST09000','AGENT WITHDRAW','Agent Tarik Dana','admin/master/agent_withdraw_admin',3,'fa fa-dot-circle-o','0',3,'1','w','2017-04-04 18:10:42');
INSERT INTO `s_menu` (`module_seq`,`menu_cd`,`parent_menu_cd`,`name`,`title_name`,`url`,`order`,`icon`,`detail`,`level`,`active`,`created_by`,`created_date`) VALUES (1,'TRX10001','TRX','INVOICING PERIOD','Periode Invoicing','admin/transaction/invoicing_period',11,'fa-dot-circle-o','0',2,'1','w','2017-04-06 11:43:44');
INSERT INTO `s_menu` (`module_seq`,`menu_cd`,`parent_menu_cd`,`name`,`title_name`,`url`,`order`,`icon`,`detail`,`level`,`active`,`created_by`,`created_date`) VALUES (1,'TRX10002','TRX','INVOICING PERIOD PARTNER','Periode Invoicing Partner','admin/transaction/invoicing_period_partner',12,'fa-dot-circle-o','1',2,'1','w','2017-04-06 11:24:21');
INSERT INTO `m_email_template` (`code`,`subject`,`content`,`email_to`,`notes`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES ('AGENT_WITHDRAW_FAIL','Konfimasi Tarik Dana Gagal!','<div>Halo <b> [RECIPIENT_NAME] </b>,</div><br><div>Penarikan deposit senilai <b>Rp. [TOTAL_WITHDRAW]</b> tidak dapat dilakukan.<br></div>\r\n<div>Dengan nomor transaksi <b>[TRX_NO]</b>.</div><div>Untuk informasi lebih lanjut, silahkan hubungi <b>Administrator</b> kami.</div>','Agent','Konfirmasi Penarikan Dana Gagal','admin','2017-04-05 10:21:04','admin','2017-04-05 10:21:09');
INSERT INTO `m_email_template` (`code`,`subject`,`content`,`email_to`,`notes`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES ('AGENT_WITHDRAW_SUCCESS','Konfirmasi Tarik Dana Berhasil!','<div>Halo <b> [RECIPIENT_NAME] </b>,</div><br><div>Penarikan deposit senilai <b>Rp. [TOTAL_WITHDRAW] </b> telah disetujui.<br></div>\r\n<div>Dengan nomor transaksi <b>[TRX_NO]</b>.</div><div>Silahkan cek <a href=\"[LINK]\">disini</a> untuk  melihat saldo anda.</div>','Agent','Konfirmasi Penarikan Dana Berhasil','admin','2017-04-05 10:19:11','admin','2017-04-05 10:19:17');
INSERT INTO `m_email_template` (`code`,`subject`,`content`,`email_to`,`notes`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES ('RESET_PASSWORD_AGENT','Perubahan Password Agent oleh Partner [WEB_TITLE]','Halo <b> [RECIPIENT_NAME]</b></center><br>\r\n <center>Password login anda telah diubah, silahkan login sebagai Agent dengan email <br> <b>[EMAIL]</b> <br>dan password sementara <br><b style=\"font-size:20px;color:#007dc6;\">[PASSWORD]</b><br>\r\n \r\n Segera login dan ubah password anda. Terima Kasih.<br><br>\r\n<a href=\"[BASE_URL]agent/login\" target=\"_blank\"> Login Sebagai Agent</a>\r\n','Agent','Generate Password oleh Partner','admin','2017-04-04 12:02:17','admin','2017-04-04 12:02:19');
INSERT INTO `m_email_template` (`code`,`subject`,`content`,`email_to`,`notes`,`created_by`,`created_date`,`modified_by`,`modified_date`) VALUES ('RESET_PASSWORD_PARTNER','Perubahan Password Partner oleh Admin [WEB_TITLE]','Halo <b> [RECIPIENT_NAME]</b></center><br>\r\n <center>Password login anda telah diubah, silahkan login sebagai Partner dengan email <br> <b>[EMAIL]</b> <br>dan password sementara <br><b style=\"font-size:20px;color:#007dc6;\">[PASSWORD]</b><br>\r\n \r\n Segera login dan ubah password anda. Terima Kasih.<br><br>\r\n<a href=\"[BASE_URL]partner/login\" target=\"_blank\"> Login Sebagai Partner</a>\r\n','Partner','Generate Password oleh admin','admin','2017-04-04 11:38:54','admin','2017-04-04 11:38:56');
REPLACE `s_menu` (`module_seq`, `menu_cd`, `parent_menu_cd`, `name`, `title_name`, `url`, `order`, `icon`, `detail`, `level`, `active`, `created_by`, `created_date`) values('1','TRX10001','TRX','INVOICING PERIOD','Periode Invoicing','admin/transaction/invoicing_period','11','fa-dot-circle-o','0','2','1','w','2017-04-06 11:43:44');
REPLACE `s_menu` (`module_seq`, `menu_cd`, `parent_menu_cd`, `name`, `title_name`, `url`, `order`, `icon`, `detail`, `level`, `active`, `created_by`, `created_date`) values('1','TRX10002','TRX','INVOICING PERIOD PARTNER','Periode Invoicing Partner','admin/transaction/invoicing_period_partner','12','fa-dot-circle-o','1','2','1','w','2017-04-06 11:24:21');
REPLACE `s_menu` (`module_seq`, `menu_cd`, `parent_menu_cd`, `name`, `title_name`, `url`, `order`, `icon`, `detail`, `level`, `active`, `created_by`, `created_date`) values('7','PTRX00004','PTRX','INVOICING PARTNER','Invoicing Data Partner','partner/invoicing_period ','11','fa-files-o','0','2','1','SYSTEM','0000-00-00 00:00:00');
REPLACE `m_user_group_permission` (`user_group_seq`, `menu_cd`, `can_view`, `can_add`, `can_edit`, `can_delete`, `can_auth`, `can_print`, `created_by`, `created_date`, `modified_by`, `modified_date`) values('2','TRX10001','1','1','1','0','0','0','w','2017-04-06 11:52:24','w','2017-04-06 11:52:24');
REPLACE `m_user_group_permission` (`user_group_seq`, `menu_cd`, `can_view`, `can_add`, `can_edit`, `can_delete`, `can_auth`, `can_print`, `created_by`, `created_date`, `modified_by`, `modified_date`) values('2','TRX10002','1','1','1','0','0','0','w','2017-04-06 11:52:46','w','2017-04-06 11:52:46');
REPLACE `m_user_group_permission` (`user_group_seq`, `menu_cd`, `can_view`, `can_add`, `can_edit`, `can_delete`, `can_auth`, `can_print`, `created_by`, `created_date`, `modified_by`, `modified_date`) values('7','PTRX00004','1','1','1','1','1','1','w','2017-04-11 13:32:59','w','2017-04-11 13:33:06');

/*
==================================
Modify Column Field
==================================
*/

ALTER TABLE t_order_merchant ADD redeem_agent_seq INT UNSIGNED NULL AFTER redeem_seq;  
ALTER TABLE t_order_loan ADD paid_date DATETIME NULL AFTER auth_date;
ALTER TABLE t_order_loan ADD collect_seq INT UNSIGNED NULL AFTER po_no;

/*
=================================
Add Stored Procedure
=================================
*/
DROP PROCEDURE IF EXISTS `sp_admin_get_data_withdraw_agent`;
DELIMITER $$
CREATE PROCEDURE `sp_admin_get_data_withdraw_agent` (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pSeq BIGINT(10),
    pAgentSeq BIGINT(10)
)
BEGIN

SELECT
	ma.seq,
    ma.agent_seq,
    m.name,
    m.email,
    m.deposit_amt,
    ma.trx_no,
	ma.deposit_trx_amt,
	ma.non_deposit_trx_amt,
	ma.status
FROM t_agent_account ma
JOIN m_agent m
	ON ma.agent_seq = m.seq
WHERE
	ma.seq = pSeq AND ma.agent_seq = pAgentSeq;

END$$ DELIMITER ;


DROP PROCEDURE IF EXISTS `sp_admin_withdraw_list_agent`;
DELIMITER $$
CREATE PROCEDURE `sp_admin_withdraw_list_agent` (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pCurrPage INT UNSIGNED,
    pRecPerPage INT UNSIGNED,
    pDirSort VARCHAR(4),
    pColumnSort VARCHAR(50),
    pAgentEmail VARCHAR(100),
    pStatus CHAR(1)
)
BEGIN

-- Begin variables for Paging
    DECLARE startRec INT UNSIGNED;
    DECLARE totalPage INT UNSIGNED;
    DECLARE totalRec INT UNSIGNED;
    -- End variables for Paging

    -- Begin SQL Where
    DECLARE sqlWhere VARCHAR(500);

	SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    SET sqlWhere = "";
      
    IF pAgentEmail <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And m.email = '" , pAgentEmail , "'");
    END IF;
    
    IF pStatus <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And ma.status = '" , pStatus, "'");
    END IF;
	
    -- End SQL Where

    -- Begin Paging Info
    SET @sqlCommand = "Select Count(ma.seq) Into @totalRec From t_agent_account ma 
														   Join m_agent m On ma.agent_seq = m.seq
                                                           ";
    
    SET @sqlCommand = CONCAT(@sqlCommand, " Where ma.trx_type ='" ,'WDW', "'");
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, sqlWhere);
    END IF;

    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;

    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);

        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        END IF;

        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    -- End Paging Info

    SET @sqlCommand = "
        Select
			ma.seq,
			ma.trx_no,
            ma.trx_date,
            m.seq as agent_seq,
            m.email as agent_email,
            m.deposit_amt,
            ma.deposit_trx_amt,
            ma.non_deposit_trx_amt,
            ma.bank_name,
            ma.bank_acct_no,
            ma.bank_acct_name,
            ma.refund_date,
            ma.status
		From t_agent_account ma
		Join m_agent m
			On ma.agent_seq = m.seq
    ";

    SET @sqlCommand = CONCAT(@sqlCommand, " Where ma.trx_type ='" ,'WDW', "'");
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, sqlWhere);
    END IF;
    
    SET @sqlCommand = CONCAT(@sqlCommand, " order by ", "", pColumnSort, " ", pDirSort);
    
    SET @sqlCommand = CONCAT(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);

    SELECT pCurrPage, totalPage, totalRec AS total_rec;

    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;

    SET @totalRec = NULL;
    SET @sqlCommand = NULL;

END$$ DELIMITER ;


DROP PROCEDURE IF EXISTS `sp_admin_withdraw_save_approve_agent`;
DELIMITER $$
CREATE PROCEDURE `sp_admin_withdraw_save_approve_agent` (
    pUserID VARCHAR(25), 
    pIPAddr VARCHAR(50),
    pSeq BIGINT(10),
    pAgentSeq BIGINT(10),
    pDepositTrxAmt DECIMAL(10),
    pNonDepositTrxAmt DECIMAL(10),
    pDepositAmt DECIMAL(10)
)
BEGIN

UPDATE t_agent_account
SET
	`status` = 'A',
    refund_date = DATE_FORMAT(NOW(),"%Y-%m-%d")
WHERE
	seq = pSeq  AND agent_seq = pAgentSeq;
    

SELECT(pDepositAmt - pDepositTrxAmt) INTO @DepositAmt;
UPDATE m_agent
SET deposit_amt = @DepositAmt
WHERE
	seq = pAgentSeq ;
END$$ DELIMITER ;


DROP PROCEDURE IF EXISTS `sp_admin_withdraw_save_reject_agent`;
DELIMITER $$
CREATE PROCEDURE `sp_admin_withdraw_save_reject_agent` (
        pUserID VARCHAR(25),
	pIPAddr VARCHAR(50),
	pSeq BIGINT(10),
	pAgentSeq BIGINT(10)
)
BEGIN

UPDATE t_agent_account
SET
	`status` = 'R'
WHERE
	seq = pSeq AND agent_seq = pAgentSeq;

END$$ DELIMITER ;


DROP PROCEDURE IF EXISTS `sp_agent_add_deposit`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_add_deposit` (
    pUserID VarChar(100),
    pIPAddr VarChar(50),
    pAgent_seq int(10),
    pDepositadd int(30)
)
BEGIN

UPDATE 
m_agent 
SET `deposit_amt` = pDepositadd 
WHERE seq = pAgent_seq;

END$$ DELIMITER ;


DROP PROCEDURE IF EXISTS `sp_agent_refund_approve`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_refund_approve` (
        pUserID VarChar(100),
	pIPAddr VarChar(50),
	pSeq int(10),
        pAgentSeq int(10)
)
BEGIN

UPDATE 	t_agent_account set 
	`status` = 'A' , 
	refund_date = curdate() , 
	modified_date = now() , 
	modified_by = pUserID 
WHERE 
	seq = pSeq 	and agent_seq = pAgentSeq;

END$$ DELIMITER ;


DROP PROCEDURE IF EXISTS `sp_agent_refund_data`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_refund_data` (
    pUserID VarChar(100),
    pIPAddr VarChar(50),
    pSeq int(10),
    pAgentSeq int(10)
)
BEGIN

SELECT
	pgm.name as pgm_method, 
	m.email as email_agent,
    m.`name`, 
    m.deposit_amt , 
    tma.`status` as status_now , 
    tma.agent_seq,
    tma.deposit_trx_amt
    
FROM 
	t_agent_account tma , 
    m_agent m , 
    m_payment_gateway_method pgm 
WHERE 
	tma.seq = pSeq and 
    tma.agent_seq = pAgentSeq and 
    tma.agent_seq = m.seq and 
    tma.pg_method_seq = pgm.seq; 

END$$ DELIMITER ;


DROP PROCEDURE IF EXISTS `sp_agent_refund_detail_product_list`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_refund_detail_product_list` (
        pUserID VarChar(100),
        pIPAddr VarChar(50),
        pHseq int(20),
        pMerchant_info_seq int(20)
)
BEGIN

SELECT 
mc.seq AS merchant_seq , 
mc.name AS merchant_name,
mv.value  AS variant_value , 
mp.name AS product_name , 
pv.pic_1_img AS product_image, 
t_order_product.sell_price , 
t_order_product.weight_kg , 
t_order_product.qty , 
t_order_product.sell_price * t_order_product.qty AS sell_total ,  
CEIL(t_order_product.ship_price_charged) * CEIL(t_order_product.weight_kg) * t_order_product.qty AS ship_total 

FROM  
t_order_product , 
m_product_variant pv ,
m_variant_value mv ,
m_product mp ,
m_merchant_info mic ,
m_merchant mc 


WHERE 
t_order_product.product_variant_seq = pv.seq AND
t_order_product.variant_value_seq = mv.seq AND
pv.product_seq = mp.seq AND 
t_order_product.merchant_info_seq = mic.seq AND
mic.merchant_seq = mc.seq  AND t_order_product.product_status = 'X'
AND t_order_product.order_seq = pHseq 
AND t_order_product.merchant_info_seq = pMerchant_info_seq
;

END$$ DELIMITER ;


DROP PROCEDURE IF EXISTS `sp_agent_refund_get_data_cancel`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_refund_get_data_cancel` (
        pMerchantId VarChar(25),
        pIPAddr VarChar(50),
        pTrxNo varchar(50),
        pTrxType varchar(10)
)
BEGIN

SELECT  
        tor.order_seq ,
        tor.merchant_info_seq
		from
		t_agent_account tma, 
        m_payment_gateway_method pgm , 
        m_agent m  , 
        t_order_merchant tor      
        where
		tma.pg_method_seq = pgm.seq and 
        tma.agent_seq = m.seq and 
        tma.trx_no = tor.ref_awb_no and 
        tma.trx_type = pTrxType and 
        tma.trx_no = pTrxNo ;

END$$ DELIMITER ;


DROP PROCEDURE IF EXISTS `sp_agent_refund_get_deposit_trx`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_refund_get_deposit_trx` (
        pUserID VarChar(100),
	pIPAddr VarChar(50),
	pRef_awb_no varchar(100)
)
BEGIN
SELECT 
	deposit_trx_amt
FROM 
	t_agent_account 
WHERE 
	trx_no = pRef_awb_no
    ;
END$$ DELIMITER ;


DROP PROCEDURE IF EXISTS `sp_agent_refund_get_product_return`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_refund_get_product_return` (
    pMerchantSeq VarChar(25),
    pIPAddr VarChar(50),
    pReturnNo varchar(50)
)
BEGIN

SELECT 
topr.qty as qty, 
mpv.sell_price  , mpv.pic_1_img as product_image ,  
vl.value as variant_value,
mp.name as product_name , mp.p_weight_kg as weight_kg , 
mpv.sell_price*topr.qty as sell_total,
mc.seq as merchant_seq,mc.name as merchant_name , 0 as ship_total
FROM 
t_order_product_return topr
join m_product_variant mpv on  topr.product_variant_seq = mpv.seq
join m_variant_value vl on mpv.variant_value_seq = vl.seq
join m_product mp on mpv.product_seq = mp.seq
join m_merchant mc on mp.merchant_seq = mc.seq 
WHERE topr.return_no = pReturnNo;

END$$ DELIMITER ;


DROP PROCEDURE IF EXISTS `sp_agent_refund_get_trx_type`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_refund_get_trx_type` (
        pMerchantSeq VarChar(25),
        pIPAddr VarChar(50),
        pTrxNo varchar(50)
)
BEGIN
SELECT 
        agent_seq, 
        seq, 
        mutation_type, 
        pg_method_seq, 
        trx_type, 
        trx_no, 
        trx_date, 
        deposit_trx_amt, 
        non_deposit_trx_amt, 
        refund_date, 
        status, 
        created_by, 
        created_date, 
        modified_by, 
        modified_date
        from t_agent_account where trx_no = pTrxNo ;

END$$ DELIMITER ;


DROP PROCEDURE IF EXISTS `sp_agent_refund_get_voucher_nominal`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_refund_get_voucher_nominal` (
        pUserID VarChar(100),
        pIPAddr VarChar(50),
        pSeq int(20)
)
BEGIN

SELECT `nominal` 
FROM m_promo_voucher mpv ,
t_order tor 
WHERE mpv.seq = tor.voucher_seq 
AND tor.seq = pSeq;


END$$ DELIMITER ;


DROP PROCEDURE IF EXISTS `sp_agent_refund_list`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_refund_list` (
        pUserID VarChar(100),
        pIPAddr VarChar(50),
        pCurrPage Int unsigned,
        pRecPerPage Int unsigned,
        pDirSort VarChar(4),
        pColumnSort Varchar(50),
        pTrx_no Varchar(100),
        pPgmethod int unsigned,
        pTrx_type varchar(10),
        pAgent_email Varchar(100),
        pStatus Char(1)
)
BEGIN

    -- Begin variables for Paging
    Declare startRec Int unsigned;
    Declare totalPage Int unsigned;
    Declare totalRec Int unsigned;
    -- End variables for Paging
    -- Begin SQL Where
    Declare sqlWhere VarChar(500);
	Set pCurrpage = (pCurrpage / pRecPerPage) + 1;
    Set sqlWhere = "";
	If pTrx_no <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And a.trx_no like '%" , pTrx_no, "%'");
    End If;
    If pPgmethod <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And a.pg_method_seq = '" , pPgmethod, "'");
    End If;
    If pAgent_email <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And m.email like '%" , pAgent_email, "%'");
    End If;
    If pTrx_type <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And a.trx_type in ('" , pTrx_type, "')");
	else
		Set sqlWhere = Concat(sqlWhere, " And a.trx_type in ('CNL','RTR')");
    End If;
    If pStatus <> "" Then
        Set sqlWhere = Concat(sqlWhere, " And a.status = '", pStatus ,"'");
	else
		Set sqlWhere = Concat(sqlWhere);
        End If;
    -- End SQL Where
    -- Begin Paging Info
    -- Set @sqlCommand = "Select Count(t_agent_account.seq) as seq Into @totalRec From t_agent_account , m_agent "; --
    -- Set @sqlCommand = "Select Count(t_agent_account.seq) as seq Into @totalRec From t_agent_account,m_agent,t_order_merchant Where  t_agent_account.trx_no = t_order_merchant.ref_awb_no and t_agent_account.agent_seq = m_agent.seq and t_agent_account.trx_type = 'CNL'";
    Set @sqlCommand = "
		Select Count(a.seq) as seq Into @totalRec
		from `t_agent_account` a
		inner join `m_payment_gateway_method` pgm on a.pg_method_seq = pgm.seq
		inner join `m_agent` m on a.agent_seq = m.seq
		left outer join `t_order_merchant`  b on a.`trx_no`=b.`ref_awb_no`
		left outer join
		`t_order_product_return`
		c on a.`trx_no`= c.`return_no`

    ";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " Where 1 " , sqlWhere);
    End If;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set totalRec = @totalRec;
    If totalRec = 0 Then
        Set totalPage = 0;
        Set pCurrPage = 1;
        Set startRec = 0;
    Else
        Set totalPage = Ceiling(totalRec / pRecPerPage);
        If pCurrPage <= 0 Then
            Set pCurrPage = 1;
        ElseIf pCurrPage > totalPage Then
            Set pCurrPage = totalPage;
        End If;
        Set startRec = (pCurrPage - 1) * pRecPerPage;
    End If;
    -- End Paging Info
    set @sqlCommand = "
		select
		pgm.name as pg_name ,
		m.name as agent_name,
		a.seq ,
		a.agent_seq,
		a.trx_date,
		a.trx_no,
		a.refund_date,
		a.agent_seq,
		a.deposit_trx_amt,
		a.created_date,
		a.agent_seq,
		a.status,
		a.trx_type
		from `t_agent_account` a
		inner join `m_payment_gateway_method` pgm on a.pg_method_seq = pgm.seq
		inner join `m_agent` m on a.agent_seq = m.seq
		left outer join `t_order_merchant`  b on a.`trx_no`=b.`ref_awb_no`
		left outer join  `t_order_product_return` c on a.`trx_no`= c.`return_no` ";
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, " where 1 " , sqlWhere);
    End If;
    Set @sqlCommand = Concat(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    Set @sqlCommand = Concat(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    Select pCurrPage, totalPage, totalRec as total_rec;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set @totalRec = Null;
    Set @sqlCommand = Null;

END$$ DELIMITER ;


DROP PROCEDURE IF EXISTS `sp_agent_refund_reject`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_refund_reject` (
    pUserID VarChar(100),
    pIPAddr VarChar(50),
    pSeq int(10),
    pAgentSeq int(10)
)
BEGIN

UPDATE t_agent_account 
SET `status` = 'R' , 
modified_date = now() , 
modified_by = pUserID 
WHERE seq = pSeq  
AND agent_seq = pAgentSeq;


END$$ DELIMITER ;


DROP PROCEDURE IF EXISTS sp_invoicing_agent_by_partner;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_agent_by_partner (
    `pUserID` VARCHAR(25),
    `pIPAddr` VARCHAR(50),
    `pSeq` INTEGER(10) UNSIGNED,
    `pPartnerSeq` INTEGER(10) UNSIGNED
)
BEGIN
    SELECT tra.`agent_seq`, tra.`status`, tra.`total`, tra.`paid_date`, tra.`paid_partner_date`, 
    `tra`.`redeem_seq`, ma.name, ma.`email` FROM
    `t_redeem_agent` tra JOIN m_agent ma ON tra.agent_seq=ma.seq
    WHERE `tra`.`redeem_seq`=pSeq AND ma.`partner_seq`=pPartnerSeq;
END$$ DELIMITER ;
DROP PROCEDURE IF EXISTS sp_invoicing_agent_update_by_admin;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_agent_update_by_admin (
    `pUserID` VARCHAR(25),
    `pIPAddr` VARCHAR(50),
    `pSeq` INTEGER(10) UNSIGNED,
    `pPartnerSeq` INTEGER(10) UNSIGNED
)
BEGIN
    UPDATE t_order_loan tol JOIN t_order `to` ON tol.`order_seq` = `to`.seq JOIN m_agent ma ON `to`.`agent_seq` = ma.`seq`
    SET tol.status_order = 'F'
    WHERE tol.status_order = 'D' AND tol.`collect_seq` = pSeq AND ma.partner_seq = pPartnerSeq;
END$$ DELIMITER ;
DROP PROCEDURE IF EXISTS `sp_order_by_order_no`;
DELIMITER $$
CREATE PROCEDURE `sp_order_by_order_no` (
    pUserID Varchar(100),
    pIPAddress VarChar(25),
    pOrderNo VarChar(20)
)
BEGIN
    SELECT 
        o.order_no,
        o.order_date,
        o.payment_status,
        o.receiver_name,
        o.receiver_address,
        p.`name` AS province_name,
        c.`name` AS city_name,
        d.`name` AS district_name,
        o.receiver_phone_no,
        m.`name` AS member_name,
        m.email as member_email,
        a.`name` as agent_name,
        a.email as agent_email,
        pgm.name AS payment_name
    FROM
        t_order o
            JOIN
        m_district d ON o.receiver_district_seq = d.seq
            JOIN
        m_city c ON d.city_seq = c.seq
            JOIN
        m_province p ON c.province_seq = p.seq
            LEFT JOIN
        m_member m ON o.member_seq = m.seq
            LEFT JOIN
            m_agent a ON o.agent_seq = a.seq
        JOIN
        m_payment_gateway_method pgm ON o.pg_method_seq = pgm.seq
    WHERE
        order_no = pOrderNo;

    SELECT
            t.order_seq,
            t.merchant_info_seq,
            t.expedition_service_seq,
        e.`name` expedition_name,
        mm.`name` merchant_name,
        mm.email,
        mm.code as merchant_code,
        mm.seq merchant_seq,
            t.real_expedition_service_seq,    
            t.total_merchant,
            t.total_ins,
            t.total_ship_real,
            t.total_ship_charged,
            t.free_fee_seq,
            t.order_status,
            t.member_notes,
            t.printed,
            t.print_date,
            t.awb_seq,
            t.awb_no,
            t.ref_awb_no,
            t.ship_by,
            t.ship_by_exp_seq,
            t.ship_date,
            t.ship_note_file,
            t.ship_notes,
            t.received_date,
            t.received_by,
            t.redeem_seq,
            t.exp_invoice_seq,
            t.exp_invoice_awb_seq
    FROM 
            t_order_merchant t join m_merchant_info m 
                    on m.seq = t.merchant_info_seq 
                                    join m_district d
                    on d.seq = m.pickup_district_seq
                                    join m_city c
                    on c.seq = d.city_seq 
                                    join m_province p 
                    on p.seq = c.province_seq
                                    join m_merchant mm
                    on mm.seq = m.merchant_seq
                                    join m_expedition_service s
                    on s.seq =  t.expedition_service_seq
                                    join m_expedition e
                    on s.exp_seq = e.seq
                                    join t_order o
                    on o.seq = t.order_seq				
    WHERE
                    o.order_no = pOrderNo;

    SELECT 
            t.order_seq,
        mi.merchant_seq,
            t.merchant_info_seq,
            p.`name` as display_name,
        v.seq as product_seq,
        v.pic_1_img as img,
        mv.`value`,
            mv.seq as value_seq,
            mv.`value` as variant_name,
        t.product_status,
            t.qty,
            t.sell_price,
            t.weight_kg,
            t.ship_price_real,
            t.ship_price_charged,
            t.trx_fee_percent,
            t.ins_rate_percent,
            t.product_status,
            t.qty_return,
            t.created_by,
            t.created_date,
            t.modified_by,
            t.modified_date
    FROM
            t_order_product t 
                    Join m_product_variant v			
                            On v.seq = t.product_variant_seq
                    Join m_product p
                            On v.product_seq = p.seq
                    Join m_variant_value vv
                            On vv.seq = t.variant_value_seq
                    Join m_variant_value mv 
                            On mv.seq = v.variant_value_seq
                    Join t_order o
                            On o.seq = t.order_seq
                    Join m_merchant_info mi
                            On mi.seq = t.merchant_info_seq
                    join m_merchant mm 
                            On mm.seq = mi.merchant_seq
    WHERE
            o.order_no = pOrderNo;

    SELECT 
            o.seq,
            o.order_no,
            o.order_date,
            o.member_seq,
        o.signature,
        m.`name` member_name,
        m.email,
        m.mobile_phone,
        a.`name`,
        a.email,
        a.mobile_phone,
        c.`name` city_name,
        case when o.payment_retry <> '0'  Then concat(o.order_no, '-', cast(o.payment_retry as char(2))) Else o.order_no End order_trans_no,	
            o.receiver_name,
            o.receiver_address,
            o.receiver_district_seq,
        d.`name` district_name,
        c.`name` city_name,
        p.`name` province_name,
            o.receiver_zip_code,
            o.receiver_phone_no,
            o.payment_status,
            o.pg_method_seq,
        pg.`code` payment_code,
        pg.name payment_name,
            o.paid_date,
        case when pv.nominal is null then mc.nominal else pv.nominal end as nominal ,
        pv.code as voucher_code,
            o.payment_retry,
            o.total_order,
            o.voucher_seq,
            o.voucher_refunded,
            o.total_payment,
            o.conf_pay_type,
            o.conf_pay_amt_member,
            o.conf_pay_date,
            o.conf_pay_note_file,
            o.conf_pay_bank_seq,
            o.conf_pay_amt_admin
    FROM 
            t_order o JOIN m_district d
                        ON o.receiver_district_seq = d.seq
                    JOIN m_city c 
                        ON c.seq = d.city_seq 
                    JOIN m_province p
                        ON p.seq = c.province_seq
                    JOIN m_payment_gateway_method pg
                        ON pg.seq = o.pg_method_seq
                    LEFT JOIN m_promo_voucher pv 
                        ON o.voucher_seq = pv.seq
                    LEFT JOIN m_coupon mc 
                        on o.coupon_seq = mc.seq
                    LEFT JOIN m_member m
                        on m.seq = o.member_seq
                    LEFT JOIN m_agent a
                        on a.seq = o.agent_seq
            WHERE
                o.order_no = pOrderNo;
END$$ DELIMITER ;
DROP PROCEDURE IF EXISTS sp_order_merchant_by_order;
DELIMITER $$
CREATE PROCEDURE sp_order_merchant_by_order (
    pUserID VarChar(25),
    pIPAddr VarChar(50),
    pOrderSeq int(10) unsigned,
    pMerchantId int(10) unsigned
)
BEGIN
    SELECT
        om.order_seq,
        om.ship_note_file,
	o.order_date,
	o.order_no,
        o.payment_status,
        o.member_seq,
        m.email AS merchant_email,
        IFNULL(mb.`name`,a.name) AS member_name,
	om.merchant_info_seq,
	m.`name` AS merchant_name,
	o.pg_method_seq,
	p.`name` AS payment_name,
	om.awb_no,
	om.ship_date,
	o.receiver_address,
	om.order_status,
        om.expedition_service_seq,
        om.real_expedition_service_seq,
        om.received_date,
        exs.seq AS exs_seq,
        exs.name AS exs_name,
        es.seq AS es_seq,
        es.name AS es_name,
        exp.seq AS exp_seq,
        exp.name AS exp_name,
        esp.seq AS expedition_seq,
        esp.name AS esp_name
    FROM t_order_merchant om 
    JOIN t_order o ON om.order_seq = o.seq 
    JOIN m_merchant_info mi ON om.merchant_info_seq = mi.seq
    JOIN m_merchant m ON mi.merchant_seq = m.seq
    LEFT OUTER JOIN m_payment_gateway_method p ON o.pg_method_seq = p.seq
    JOIN m_expedition_service exs ON om.expedition_service_seq = exs.seq
    JOIN m_expedition_service exp ON om.real_expedition_service_seq = exp.seq
    JOIN m_expedition es ON exs.exp_seq = es.seq   
    JOIN m_expedition esp ON exp.exp_seq = esp.seq
    LEFT JOIN m_member mb ON o.member_seq = mb.seq
    LEFT JOIN m_agent a ON o.agent_seq = a.seq
    WHERE 
        om.order_seq = pOrderSeq 
        AND om.merchant_info_seq = pMerchantId;
END$$ DELIMITER ;
DROP PROCEDURE IF EXISTS sp_redeem_agent_by_seq;
DELIMITER $$
CREATE PROCEDURE sp_redeem_agent_by_seq (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pSeq INTEGER(10) UNSIGNED
)
BEGIN
    SELECT
        seq, from_date, to_date, status, created_by, created_date, modified_by, modified_date
    FROM t_redeem_agent_period
    WHERE seq = pSeq;
END$$ DELIMITER ;
DROP PROCEDURE IF EXISTS `sp_redeem_agent_commission_component_add`;
DELIMITER $$
CREATE PROCEDURE `sp_redeem_agent_commission_component_add` (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pSeq INTEGER(10) UNSIGNED,
    pType CHAR(3),
    pMType CHAR(1)
)
BEGIN
    INSERT INTO t_redeem_agent_component(
	redeem_seq,
	partner_seq,
	type,
	mutation_type,
	total,
	created_by,
	created_date,
	modified_by,
	modified_date
    )
    SELECT 
        pSeq, 
        ma.partner_seq, 
        pType, 
        pMtype, 
        SUM(top.qty * top.sell_price * commission_fee_partner_percent /100) total, 
        pUserID, 
        Now(), 
        pUserID, 
        Now() 
    FROM t_redeem_agent_period tra  
    Join t_order_merchant tom ON tom.redeem_agent_seq = tra.seq
    Join t_order_product top ON top.order_seq = tom.order_seq
    Join t_order t on t.seq = tom.order_seq
    JOIN m_agent ma ON t.agent_seq= ma.seq
    WHERE tra.seq=pSeq and top.product_status = 'R'
    GROUP BY ma.partner_seq;


END$$
DELIMITER ;
DROP PROCEDURE IF EXISTS sp_redeem_agent_get_lastdate;
DELIMITER $$
CREATE PROCEDURE sp_redeem_agent_get_lastdate (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50)
)
BEGIN
    SELECT DATE_ADD(to_date, INTERVAL 1 DAY) AS from_date,DATE_ADD(to_date, INTERVAL 1 DAY) AS to_date, 0 AS status
    FROM t_redeem_agent_period
    ORDER BY from_date DESC 
    LIMIT 1;
END$$ DELIMITER ;
DROP PROCEDURE IF EXISTS sp_redeem_agent_get_order;
DELIMITER $$
CREATE PROCEDURE sp_redeem_agent_get_order (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pToDate DATE
)
BEGIN
    SELECT 
        t_or.agent_seq,
        SUM(t_op.qty * t_op.sell_price * (t_op.commission_fee_partner_percent / 100)) AS total_komisi
    FROM t_order_product t_op
    JOIN t_order_merchant t_om ON t_om.order_seq=t_op.order_seq AND t_op.merchant_info_seq=t_om.merchant_info_seq
    JOIN t_order t_or  ON t_op.order_seq=t_or.seq
    JOIN m_agent m_a ON t_or.agent_seq=m_a.seq
    WHERE 
        t_om.order_status='D'
        AND t_op.product_status='R'
        AND t_om.received_date <= pToDate
        AND t_om.redeem_agent_seq IS NULL
        AND t_or.member_seq IS NULL
    GROUP BY t_or.agent_seq;
END$$ DELIMITER ;
DROP PROCEDURE IF EXISTS sp_redeem_agent_period_add;
DELIMITER $$
CREATE PROCEDURE sp_redeem_agent_period_add (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pFdate VARCHAR(10),
    pTdate DATE
)
BEGIN
    DECLARE new_seq INT(10) UNSIGNED;
    SELECT MAX(seq) + 1 INTO new_seq
    FROM t_redeem_agent_period;
    IF new_seq IS NULL 
        THEN Set new_seq = 1;
    END IF;
    INSERT INTO t_redeem_agent_period(
        seq, from_date, to_date, status, created_by, created_date, modified_by, modified_date
    ) VALUES (
        new_seq, pFdate, pTdate, 'O', pUserID, NOW(), pUserID, NOW()
    );
END$$ DELIMITER ;
DROP PROCEDURE IF EXISTS sp_redeem_agent_period_status_update;
DELIMITER $$
CREATE PROCEDURE sp_redeem_agent_period_status_update (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pSeq INTEGER(10) UNSIGNED,
    pStatusNew CHAR(1),
    pStatusOld CHAR(1)
)
BEGIN
    UPDATE t_redeem_agent_period 
    SET
        status = pStatusNew,
        modified_by = pUserID,
        modified_date = NOW()
    Where
        seq = pSeq AND status = pStatusOld;
END$$ DELIMITER ;
DROP PROCEDURE IF EXISTS sp_redeem_agent_status;
DELIMITER $$
CREATE PROCEDURE sp_redeem_agent_status (
    `pUserID` VARCHAR(25),
    `pIPAddr` VARCHAR(50),
    `pSeq` INTEGER(10) UNSIGNED
)
BEGIN
    SELECT 
        a.`redeem_seq`,
        a.`agent_seq`,
        a.`total`,
        a.`status`,
        a.`paid_date`,
        a.`created_by`,
        a.`created_date`,
        a.`modified_by`,
        a.`modified_date`, 
        b.`name`,b.`email` 
    FROM 
        `t_redeem_agent` a
    LEFT OUTER JOIN m_agent b on a.`agent_seq` = b.`seq`
    WHERE 
        `redeem_seq` = pSeq;
END$$ DELIMITER ;

DROP PROCEDURE IF EXISTS sp_redeem_agent_update_by_partner;
DELIMITER $$
CREATE PROCEDURE sp_redeem_agent_update_by_partner (
    `pUserID` VARCHAR(25),
    `pIPAddr` VARCHAR(50),
    `pSeq` INTEGER(10) UNSIGNED,
    `pPartnerSeq` INTEGER(10) UNSIGNED,
    `pPaiddate` DATE
)
BEGIN
UPDATE 
    t_redeem_agent 
SET
    paid_partner_date = pPaiddate,
    `status` = 'T',
    modified_by = pUserID,
    modified_date = NOW() 
WHERE agent_seq IN 
  (SELECT 
    seq 
  FROM
    m_agent 
  WHERE partner_seq = pPartnerSeq) 
  AND `redeem_seq` = pSeq 
  AND `status` = 'U' ;
END$$ DELIMITER ;
DROP PROCEDURE IF EXISTS sp_redeem_agent_update_order;
DELIMITER $$
CREATE PROCEDURE sp_redeem_agent_update_order (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pSeq INTEGER(10) UNSIGNED,
    pTdate DATE
)
BEGIN
    UPDATE 
        t_order_merchant tom join t_order tor on tom.order_seq=tor.seq 
    SET
        tom.redeem_agent_seq = pSeq,    
        tom.modified_by = pUserID,
        tom.modified_date = now()
    WHERE
        tom.order_status='D' 
        AND tom.redeem_agent_seq IS NULL 
        AND tom.received_date <= pTdate
        AND tor.member_seq IS NULL;
END$$ DELIMITER ;
DROP PROCEDURE IF EXISTS sp_redeem_partner_by_seq;
DELIMITER $$
CREATE PROCEDURE sp_redeem_partner_by_seq (
    `pUserID` VARCHAR(25),
    `pIPAddr` VARCHAR(50),
    `pSeq` INTEGER(10) UNSIGNED,
    `pPartnerSeq` INTEGER(10) UNSIGNED
)
BEGIN
SELECT
    a.`redeem_seq`,
    a.`partner_seq`,
    SUM(CASE WHEN a.`mutation_type`= 'D' THEN a.`total` ELSE a.`total` * -1 END) AS total,
    c.`status`,
    c.`paid_date`,
    b.`name`,
    b.email,
    b.mobile_phone,
    b.bank_name,
    b.bank_branch_name,
    b.bank_acct_no,
    b.bank_acct_name,
    b.profile_img
FROM 
    `t_redeem_agent_component` a 
    JOIN `m_partner` b ON a.`partner_seq` = b.`seq`
    JOIN (
        SELECT tra.redeem_seq, tra.status, tra.paid_date, ma.partner_seq 
        FROM 
            t_redeem_agent tra 
        JOIN m_agent ma ON tra.agent_seq = ma.seq AND ma.`partner_seq` = pPartnerSeq 
        WHERE 
            tra.`redeem_seq`=pSeq 
        GROUP BY tra.redeem_seq, tra.status, tra.paid_date, ma.partner_seq
    ) c ON a.`partner_seq`=c.`partner_seq` 
WHERE
    a.`redeem_seq` = pSeq AND a.`partner_seq`=pPartnerSeq
GROUP BY 
    a.`redeem_seq`, a.`partner_seq`, c.`status`, c.`paid_date`, b.`name`, b.email, b.mobile_phone, b.bank_name, b.bank_branch_name, b.bank_acct_no, b.bank_acct_name, b.profile_img;
END$$ DELIMITER ;

DROP PROCEDURE IF EXISTS sp_redeem_partner_list;
DELIMITER $$
CREATE PROCEDURE sp_redeem_partner_list (
    `pUserID` VARCHAR(25),
    `pIPAddr` VARCHAR(50),
    `pCurrPage` INTEGER,
    `pRecPerPage` INTEGER,
    `pDirSort` VARCHAR(4),
    `pColumnSort` VARCHAR(50),
    `pSeq` INTEGER(10)
)
BEGIN
    -- Begin variables for Paging
    DECLARE startRec INT;
    DECLARE totalPage INT;
    DECLARE totalRec INT;
    -- End variables for Paging
    -- Begin SQL Where
    DECLARE sqlWhere VARCHAR(500);
	SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    SET sqlWhere = "";
    
    IF pSeq <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " AND c.redeem_seq = '" , pSeq, "'");
    END IF;
    -- End SQL Where
    -- Begin Paging Info
    SET @sqlCommand = "SELECT COUNT(c.partner_seq) INTO @totalRec FROM t_redeem_agent_component c ";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " WHERE 1 " , sqlWhere );
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        End IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    -- End Paging Info
    SET @sqlCommand = "
        SELECT
            a.`redeem_seq`,
            a.`partner_seq`,
            SUM(CASE WHEN a.`mutation_type`= 'D' THEN a.`total` ELSE a.`total` * -1 END) AS total,
            c.`status`,
            c.paid_partner_date AS paid_date,
            b.`name`,
            b.email,
            b.mobile_phone,
            b.bank_name,
            b.bank_branch_name,
            b.bank_acct_no,
            b.bank_acct_name,
            b.profile_img
        FROM 
            `t_redeem_agent_component` a 
        JOIN `m_partner` b ON a.`partner_seq`=b.`seq`
        JOIN (SELECT tra.redeem_seq, tra.status, tra.paid_partner_date, ma.partner_seq FROM t_redeem_agent tra JOIN m_agent ma ON tra.agent_seq=ma.seq ";
    IF pSeq <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " WHERE tra.redeem_seq=" , pSeq, " GROUP BY tra.redeem_seq, tra.status, tra.paid_partner_date, ma.partner_seq) c ON a.`partner_seq`= c.`partner_seq` ");
    END IF;    
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " WHERE 1 " , sqlWhere, " AND a.redeem_seq=",pSeq);
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand, " GROUP BY a.`redeem_seq`, a.`partner_seq`, c.`status`, c.`paid_partner_date`, b.`name`, b.email, b.mobile_phone, b.bank_name, b.bank_branch_name, b.bank_acct_no, b.bank_acct_name, b.profile_img");
    SET @sqlCommand = CONCAT(@sqlCommand, " ORDER BY ", pColumnSort, " ", pDirSort);
    SET @sqlCommand = CONCAT(@sqlCommand, " LIMIT ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @totalRec = NULL;
    SET @sqlCommand = NULL;
END$$ DELIMITER ;

DROP PROCEDURE IF EXISTS sp_invoicing_agent_by_partner;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_agent_by_partner (
    `pUserID` VARCHAR(25),
    `pIPAddr` VARCHAR(50),
    `pSeq` INTEGER(10) UNSIGNED,
    `pPartnerSeq` INTEGER(10) UNSIGNED
)
BEGIN
    SELECT tra.`agent_seq`, tra.`status`, tra.`total`, tra.`paid_date`, tra.`paid_partner_date`, 
    `tra`.`redeem_seq`, ma.name, ma.`email` FROM
    `t_redeem_agent` tra JOIN m_agent ma ON tra.agent_seq=ma.seq
    WHERE `tra`.`redeem_seq`=pSeq AND ma.`partner_seq`=pPartnerSeq;
END$$ DELIMITER ;

DROP PROCEDURE IF EXISTS sp_invoicing_agent_update_by_admin;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_agent_update_by_admin (
    `pUserID` VARCHAR(25),
    `pIPAddr` VARCHAR(50),
    `pSeq` INTEGER(10) UNSIGNED,
    `pPartnerSeq` INTEGER(10) UNSIGNED,
    `pPaidDate` DATETIME
)
BEGIN
    UPDATE t_order_loan tol JOIN t_order `to` ON tol.`order_seq` = `to`.seq JOIN m_agent ma ON `to`.`agent_seq` = ma.`seq`
    SET tol.status_order = 'F', tol.paid_date = pPaidDate
    WHERE tol.status_order = 'D' AND tol.`collect_seq` = pSeq AND ma.partner_seq = pPartnerSeq;
END$$ DELIMITER ;


DROP PROCEDURE IF EXISTS sp_invoicing_component_add_by_partner_seq;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_component_add_by_partner_seq (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pSeq INT(10) UNSIGNED,
    pType CHAR(3),
    pMType CHAR(1),
    pPartnerSeq INT(10) UNSIGNED,    
    pTotal DECIMAL(10,0),
    pTotalInstallment DECIMAL(10,0)
)
BEGIN
    INSERT INTO `t_collecting_component`(
        `collect_seq`,
        `partner_seq`,
        `type`,
        `mutation_type`,
        `total`,
        `created_by`,
        `created_date`,
        `modified_by`,
        `modified_date`
    ) VALUES (
        pSeq,
        pPartnerSeq,
        pType,
        pMType,
        pTotal-pTotalInstallment,
        pUserID,
        NOW(),
        pUserID,
        NOW()
    );
END$$ DELIMITER ;

DROP PROCEDURE IF EXISTS sp_invoicing_component_by_seq_partner;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_component_by_seq_partner (
    `pUserID` VARCHAR(25),
    `pIPAddr` VARCHAR(50),
    `pSeq` INTEGER(10) UNSIGNED,
    `pPartnerSeq` INTEGER(10) UNSIGNED
)
BEGIN
    SELECT
        `collect_seq`,
        `partner_seq`,
        `type`,
        `mutation_type`,
        `total`,
        `created_by`,
        `created_date`,
        `modified_by`,
        `modified_date`
    FROM t_collecting_component
    WHERE
        collect_seq = pSeq AND partner_seq = pPartnerSeq;
END$$ DELIMITER ;

DROP PROCEDURE IF EXISTS sp_invoicing_order_agent_partner_by_loan;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_order_agent_partner_by_loan (
    `pUserID` VARCHAR(25),
    `pIPAddr` VARCHAR(50),
    `pSeq` INTEGER(10) UNSIGNED,
    `pPartnerSeq` INTEGER(10) UNSIGNED
)
BEGIN
    SELECT tor.seq AS order_seq, tor.order_no, tor.total_payment, tol.`total_installment`, tor.order_date 
    FROM `t_order` tor
    JOIN t_order_merchant tom ON tom.`order_seq` = tor.seq
    JOIN m_agent ma ON tor.agent_seq = ma.seq
    JOIN t_order_loan tol ON tor.seq = tol.order_seq
    WHERE tol.`collect_seq` = pSeq
    AND ma.`partner_seq` = pPartnerSeq
    GROUP BY tor.`order_no`;
END$$ DELIMITER ;

DROP PROCEDURE IF EXISTS sp_invoicing_partner_list;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_partner_list (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pCurrPage INTEGER,
    pRecPerPage INTEGER,
    pDirSort VARCHAR(4),
    pColumnSort VARCHAR(50),
    pSeq INT(10)
)
BEGIN
    DECLARE startRec INT;
    DECLARE totalPage INT;
    DECLARE totalRec INT;
    DECLARE sqlWhere VARCHAR(500);
    DECLARE status_order CHAR(1);
    DECLARE paid_date DATE;
    SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    SET sqlWhere = "";
    IF pSeq <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And a.collect_seq = '" , pSeq, "'");
    END IF;
    SET @sqlCommand = "Select Count(*) Into @totalRec FROM `t_collecting_component` a JOIN `m_partner` b ON a.`partner_seq`=b.`seq`";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        END IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    
    SET @status = NULL;
    SET @paid_date = NULL;

    IF pSeq <> "" THEN
        SELECT k.status_order, k.paid_date INTO @status,@paid_date 
        FROM t_order_loan k 
        JOIN t_order l ON k.order_seq = l.seq
	JOIN m_agent a ON a.seq = l.agent_seq
	JOIN m_partner p ON p.seq = a.partner_seq
	WHERE k.collect_seq = pSeq LIMIT 1;
    END IF;
    SET status_order = @status;  
    SET paid_date = @paid_date;  
    
    SET @sqlCommand = CONCAT("
SELECT
    a.`collect_seq`,
    a.`partner_seq`,
    SUM(CASE WHEN a.`mutation_type`= 'D' THEN a.`total` ELSE a.`total` * -1 END) AS total, ",IFNULL(CONCAT("'",@status,"'"),'NULL')," as status, ",IFNULL(CONCAT("'",@paid_date,"'"),'NULL')," as paid_date,
    b.`name`,
    b.email,
    b.mobile_phone,
    b.bank_name,
    b.bank_branch_name,
    b.bank_acct_no,
    b.bank_acct_name,
    b.profile_img
FROM `t_collecting_component` a 
JOIN `m_partner` b ON a.`partner_seq`=b.`seq`");
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
        SET @sqlCommand = CONCAT(@sqlCommand, " GROUP BY a.collect_seq,a.partner_seq");
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    SET @sqlCommand = CONCAT(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @totalRec = NULL;
    SET @sqlCommand = NULL;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_invoicing_period_add;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_period_add (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pFdate VARCHAR(10),
    pTdate DATE
)
BEGIN
    DECLARE new_seq INT(10) UNSIGNED;
    SELECT 
        MAX(seq) + 1 INTO new_seq
    FROM t_collecting_period;
    IF new_seq IS NULL THEN
        SET new_seq = 1;
    END IF;
    INSERT INTO t_collecting_period(		
        seq,
        from_date,
        to_date,
        `status`,
        created_by,
        created_date,
        modified_by,
        modified_date
    ) VALUES (
        new_seq,
        pFdate,
        pTdate,
        'O',
        pUserID,
        NOW(),
        pUserID,
        NOW()
    );
END$$ DELIMITER ;

DROP PROCEDURE IF EXISTS sp_invoicing_period_by_seq;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_period_by_seq (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pSeq INT(10) UNSIGNED
)
BEGIN
    SELECT 
        `seq`,
        `from_date`,
        `to_date`,
        `status`,
        `created_by`,
        `created_date`,
        `modified_by`,
        `modified_date` 
    FROM
        t_collecting_period 
    WHERE
        seq = pSeq;
END$$ DELIMITER ;

DROP PROCEDURE IF EXISTS sp_invoicing_period_get_lastdate;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_period_get_lastdate (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50)
)
BEGIN
    SELECT DATE_ADD(to_date, INTERVAL 1 DAY) AS from_date,DATE_ADD(to_date, INTERVAL 1 DAY) AS to_date, 0 AS STATUS
    FROM t_collecting_period ORDER BY from_date DESC LIMIT 1;
END$$

DELIMITER ;

DROP PROCEDURE IF EXISTS sp_invoicing_period_get_order;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_period_get_order (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pToDate DATE
)
BEGIN
    SELECT a.`partner_seq`, SUM(`to`.`total_payment`) AS total_payment, SUM(`tol`.`total_installment`) AS total_installment
    FROM t_order_loan `tol`
    INNER JOIN t_order `to` ON `to`.seq = `tol`.order_seq
    INNER JOIN t_order_merchant `tom` ON `tom`.`order_seq` = `to`.`seq`
    INNER JOIN t_order_product `top` ON `top`.`order_seq` = `to`.`seq`
    INNER JOIN m_agent a ON a.seq = `to`.`agent_seq`
    INNER JOIN m_partner p ON p.`seq`=a.`partner_seq`
    WHERE `top`.`product_status`='R' AND `tom`.order_status='D' AND `tol`.`collect_seq` IS NULL AND `tom`.`received_date` <= pToDate
    GROUP BY a.`partner_seq`;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_invoicing_period_list;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_period_list (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pCurrPage INT,
    pRecPerPage INT,
    pDirSort VARCHAR(20),
    pColumnSort VARCHAR(50),
    pFdate VARCHAR(10),
    pTdate VARCHAR(10),
    pStatus CHAR(1)
)
BEGIN
    -- Begin variables for Paging
    DECLARE startRec INT;
    DECLARE totalPage INT;
    DECLARE totalRec INT;
    -- End variables for Paging
    -- Begin SQL Where
    DECLARE sqlWhere VARCHAR(500);
	SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    SET sqlWhere = "";
    IF pFdate <> "" AND pTdate <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And (a.from_date BETWEEN '",pFdate,"' AND '",pTdate,"') OR (a.to_date BETWEEN '",pFdate,"' AND '",pTdate,"')");
    END IF;
    IF pStatus <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And a.status= '" , pStatus, "'");
    END IF;
    -- End SQL Where
    -- Begin Paging Info
    SET @sqlCommand = "Select Count(seq) Into @totalRec From t_collecting_period a";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        END IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    -- End Paging Info
    SET @sqlCommand = "
SELECT 
    a.seq,
    a.from_date,
    a.to_date,
    a.status,
    a.`created_by`,
    a.`created_date`,
    a.`modified_by`,
    a.`modified_date` 
FROM t_collecting_period a";
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
        SET @sqlCommand = CONCAT(@sqlCommand, " GROUP BY a.collect_seq,a.partner_seq");
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    SET @sqlCommand = CONCAT(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @totalRec = NULL;
    SET @sqlCommand = NULL;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_invoicing_period_status_update;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_period_status_update (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pSeq INTEGER(10) UNSIGNED,
    pStatusNew CHAR(1),
    pStatusOld CHAR(1)
)
BEGIN
    UPDATE t_collecting_period 
    SET
        status = pStatusNew,
        modified_by = pUserID,
        modified_date = NOW()
    WHERE
        seq = pSeq AND status = pStatusOld;
END$$

DELIMITER ;

DROP PROCEDURE IF EXISTS sp_invoicing_update_order_loan;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_update_order_loan (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pSeq INTEGER(10) UNSIGNED,
    pTdate DATE
)
BEGIN
    UPDATE 
        t_order_loan `tol` 
        JOIN t_order `tor` ON `tor`.`seq` = `tol`.`order_seq`
        JOIN t_order_merchant `tom` ON `tom`.`order_seq` = `tol`.`order_seq`
    SET
        `tol`.`collect_seq` = pSeq,    
        `tol`.`modified_by` = pUserID,
        `tol`.`modified_date` = NOW()
    WHERE
        `tom`.order_status='D' 
        AND `tol`.status_order = 'P'
        AND `tol`.collect_seq IS NULL 
        AND `tom`.received_date <= pTdate
        AND `tor`.`member_seq` IS NULL;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_invoicing_agent_update_by_partner;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_agent_update_by_partner (
    `pUserID` VARCHAR(25),
    `pIPAddr` VARCHAR(50),
    `pSeq` INTEGER(10) UNSIGNED,
    `pPartnerSeq` INTEGER(10) UNSIGNED,
    `pPaiddate` DATE
)
BEGIN
    UPDATE t_order_loan tol JOIN t_order `to` ON tol.`order_seq` = `to`.seq JOIN m_agent ma ON `to`.`agent_seq` = ma.`seq`
    SET tol.status_order = 'D', tol.`paid_date` = pPaiddate
    WHERE tol.status_order = 'P' AND tol.`collect_seq` = pSeq AND ma.partner_seq = pPartnerSeq;
END$$

DELIMITER ;

DROP PROCEDURE IF EXISTS sp_invoicing_partner_list_by_partner_seq;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_partner_list_by_partner_seq (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pCurrPage INTEGER,
    pRecPerPage INTEGER,
    pColumnSort VARCHAR(50),
    pDirSort VARCHAR(4),
    pPartnerSeq INT(10),
    pFDate DATE,
    pTDate DATE,
    pStatus CHAR(1)
)
BEGIN
    DECLARE startRec INT;
    DECLARE totalPage INT;
    DECLARE totalRec INT;
    DECLARE sqlWhere VARCHAR(500);
    DECLARE status_order CHAR(1);
    DECLARE paid_date DATE;
    SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    SET sqlWhere = "";
    IF pFdate <> "" AND pTdate <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And ((c.from_date BETWEEN '",pFdate,"' AND '",pTdate,"') OR (c.to_date BETWEEN '",pFdate,"' AND '",pTdate,"'))");
    END IF;
    IF pStatus <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And d.status = '",pStatus,"'");
    END IF;
    IF pPartnerSeq <> "" OR pPartnerSeq <> 0 THEN
        SET sqlWhere = CONCAT(sqlWhere, 
        " And a.partner_seq = '" , pPartnerSeq, "'");
    END IF;
    SET @sqlCommand = CONCAT("Select Count(*) Into @totalRec 
FROM `t_collecting_component` a 
JOIN `t_collecting_period` c ON a.collect_seq = c.seq 
JOIN `m_partner` b ON a.`partner_seq`=b.`seq`
JOIN (SELECT k.collect_seq, a.partner_seq, k.paid_date, k.status_order AS status
FROM t_order_loan k 
JOIN t_order l ON k.order_seq = l.seq
JOIN m_agent a ON a.seq = l.agent_seq 
WHERE a.partner_seq = ",pPartnerSeq," GROUP BY collect_seq, a.partner_seq) d ON a.collect_seq = d.collect_seq AND a.partner_seq = d.partner_seq");
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        END IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    
    SET @sqlCommand = CONCAT("
SELECT
    a.`collect_seq`,
    a.`partner_seq`,
    d.paid_date,
    d.status,
    b.`name`,
    b.email,
    b.mobile_phone,
    b.bank_name,
    b.bank_branch_name,
    b.bank_acct_no,
    b.bank_acct_name,
    b.profile_img,
    c.from_date,
    c.to_date,
    SUM(CASE WHEN a.`mutation_type`= 'D' THEN a.`total` ELSE a.`total` * -1 END) AS total
FROM `t_collecting_component` a
JOIN `t_collecting_period` c ON a.collect_seq = c.seq 
JOIN `m_partner` b ON a.`partner_seq`=b.`seq`
JOIN (SELECT k.collect_seq, a.partner_seq, k.paid_date, k.status_order AS status
FROM t_order_loan k 
JOIN t_order l ON k.order_seq = l.seq
JOIN m_agent a ON a.seq = l.agent_seq WHERE a.partner_seq = ",pPartnerSeq," GROUP BY collect_seq, a.partner_seq) d ON a.collect_seq = d.collect_seq AND a.partner_seq = d.partner_seq
");
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
        SET @sqlCommand = CONCAT(@sqlCommand, " GROUP BY a.collect_seq,a.partner_seq");
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    SET @sqlCommand = CONCAT(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @totalRec = NULL;
    SET @sqlCommand = NULL;
END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS `sp_redeem_component_by_seq_partner`;
DELIMITER $$
CREATE PROCEDURE `sp_redeem_component_by_seq_partner` (
        `pUserID` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pSeq` INTEGER(10) UNSIGNED,
        `pPartnerSeq` INTEGER(10) UNSIGNED
)
BEGIN

Select 
    `redeem_seq`, 
    `partner_seq`, 
    `type`, 
    `mutation_type`, 
    `total`, 
    `created_by`, 
    `created_date`, 
    `modified_by`, 
    `modified_date`
From 
    t_redeem_agent_component
Where
    redeem_seq = pSeq and partner_seq=pPartnerSeq;

END$$ 
DELIMITER ;

DROP PROCEDURE IF EXISTS `sp_redeem_agent_by_partner`;
DELIMITER $$
CREATE PROCEDURE `sp_redeem_agent_by_partner` (
        `pUserID` VARCHAR(25),
        `pIPAddr` VARCHAR(50),
        `pSeq` INTEGER(10) UNSIGNED,
        `pPartnerSeq` INTEGER(10) UNSIGNED
)
BEGIN
    select 
        tra.`agent_seq`, 
        tra.`status`, 
        tra.`total`, 
        tra.`paid_date`, 
        tra.`paid_partner_date`, 
        `tra`.`redeem_seq`, 
        ma.name, ma.`email` 
    from
        `t_redeem_agent` tra join m_agent ma 
            on tra.agent_seq=ma.seq
    where 
        `tra`.`redeem_seq`= pSeq and 
        ma.`partner_seq`= pPartnerSeq;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_redeem_agent_partner_list;
DELIMITER $$
CREATE PROCEDURE sp_redeem_agent_partner_list (
    `pUserID` VARCHAR(25),
    `pIPAddr` VARCHAR(50),
    `pSeq` INTEGER(10) UNSIGNED
)
BEGIN
    SELECT mp.seq, mp.`email`, mp.`name` 
    FROM `m_partner` mp 
    WHERE mp.`seq` IN (
        SELECT DISTINCT(partner_seq) FROM `t_redeem_agent_component` WHERE redeem_seq = pSeq
    );
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_invoicing_agent_update_by_partner;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_agent_update_by_partner (
    `pUserID` VARCHAR(25),
    `pIPAddr` VARCHAR(50),
    `pSeq` INTEGER(10) UNSIGNED,
    `pPartnerSeq` INTEGER(10) UNSIGNED,
    `pPaiddate` DATE
)
BEGIN
    UPDATE t_order_loan tol JOIN t_order `to` ON tol.`order_seq` = `to`.seq JOIN m_agent ma ON `to`.`agent_seq` = ma.`seq`
    SET tol.status_order = 'D', tol.`paid_date` = pPaiddate
    WHERE tol.status_order = 'P' AND tol.`collect_seq` = pSeq AND ma.partner_seq = pPartnerSeq;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_invoicing_partner_list_by_partner_seq;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_partner_list_by_partner_seq (
    pUserID VARCHAR(25),
    pIPAddr VARCHAR(50),
    pCurrPage INTEGER,
    pRecPerPage INTEGER,
    pColumnSort VARCHAR(50),
    pDirSort VARCHAR(4),
    pPartnerSeq INT(10),
    pFDate DATE,
    pTDate DATE,
    pStatus CHAR(1)
)
BEGIN
    DECLARE startRec INT;
    DECLARE totalPage INT;
    DECLARE totalRec INT;
    DECLARE sqlWhere VARCHAR(500);
    DECLARE status_order CHAR(1);
    DECLARE paid_date DATE;
    SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    SET sqlWhere = "";
    IF pFdate <> "" AND pTdate <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And ((c.from_date BETWEEN '",pFdate,"' AND '",pTdate,"') OR (c.to_date BETWEEN '",pFdate,"' AND '",pTdate,"'))");
    END IF;
    IF pStatus <> "" THEN
        SET sqlWhere = CONCAT(sqlWhere, " And d.status = '",pStatus,"'");
    END IF;
    IF pPartnerSeq <> "" OR pPartnerSeq <> 0 THEN
        SET sqlWhere = CONCAT(sqlWhere, 
        " And a.partner_seq = '" , pPartnerSeq, "'");
    END IF;
    SET @sqlCommand = CONCAT("Select Count(*) Into @totalRec 
FROM `t_collecting_component` a 
JOIN `t_collecting_period` c ON a.collect_seq = c.seq 
JOIN `m_partner` b ON a.`partner_seq`=b.`seq`
JOIN (SELECT k.collect_seq, a.partner_seq, k.paid_date, k.status_order AS status
FROM t_order_loan k 
JOIN t_order l ON k.order_seq = l.seq
JOIN m_agent a ON a.seq = l.agent_seq 
WHERE a.partner_seq = ",pPartnerSeq," GROUP BY collect_seq, a.partner_seq) d ON a.collect_seq = d.collect_seq AND a.partner_seq = d.partner_seq");
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        END IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    
    SET @sqlCommand = CONCAT("
SELECT
    a.`collect_seq`,
    a.`partner_seq`,
    d.paid_date,
    d.status,
    b.`name`,
    b.email,
    b.mobile_phone,
    b.bank_name,
    b.bank_branch_name,
    b.bank_acct_no,
    b.bank_acct_name,
    b.profile_img,
    c.from_date,
    c.to_date,
    SUM(CASE WHEN a.`mutation_type`= 'D' THEN a.`total` ELSE a.`total` * -1 END) AS total
FROM `t_collecting_component` a
JOIN `t_collecting_period` c ON a.collect_seq = c.seq 
JOIN `m_partner` b ON a.`partner_seq`=b.`seq`
JOIN (SELECT k.collect_seq, a.partner_seq, k.paid_date, k.status_order AS status
FROM t_order_loan k 
JOIN t_order l ON k.order_seq = l.seq
JOIN m_agent a ON a.seq = l.agent_seq WHERE a.partner_seq = ",pPartnerSeq," GROUP BY collect_seq, a.partner_seq) d ON a.collect_seq = d.collect_seq AND a.partner_seq = d.partner_seq
");
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, " Where 1" , sqlWhere);
        SET @sqlCommand = CONCAT(@sqlCommand, " GROUP BY a.collect_seq,a.partner_seq");
    END IF;
    SET @sqlCommand = CONCAT(@sqlCommand, " order by ", pColumnSort, " ", pDirSort);
    SET @sqlCommand = CONCAT(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET @totalRec = NULL;
    SET @sqlCommand = NULL;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_invoicing_partner_by_seq;
DELIMITER $$
CREATE PROCEDURE sp_invoicing_partner_by_seq (
    `pUserID` VARCHAR(25),
    `pIPAddr` VARCHAR(50),
    `pSeq` INTEGER(10) UNSIGNED,
    `pPartnerSeq` INTEGER(10) UNSIGNED
)
BEGIN
IF pSeq <> "" THEN
    SELECT k.status_order, k.paid_date INTO @status,@paid_date 
    FROM t_order_loan k 
    JOIN t_order l ON k.order_seq = l.seq
    JOIN m_agent a ON a.seq = l.agent_seq
    JOIN m_partner p ON p.seq = a.partner_seq
    WHERE k.collect_seq = pSeq LIMIT 1;

    SELECT
        a.`collect_seq`,
        a.`partner_seq`,
        SUM(CASE WHEN a.`mutation_type`= 'D' THEN a.`total` ELSE a.`total` * -1 END) AS total, 
        @status as status,
        @paid_date AS paid_date,
        b.`name`,
        b.email,
        b.mobile_phone,
        b.bank_name,
        b.bank_branch_name,
        b.bank_acct_no,
        b.bank_acct_name,
        b.profile_img
    FROM `t_collecting_component` a 
    JOIN `m_partner` b ON a.`partner_seq`=b.`seq`
    WHERE a.collect_seq = pSeq AND a.partner_seq = pPartnerSeq
    GROUP BY a.`collect_seq`, a.`partner_seq`;
ELSE
    SELECT
        a.`collect_seq`,
        a.`partner_seq`,
        SUM(CASE WHEN a.`mutation_type`= 'D' THEN a.`total` ELSE a.`total` * -1 END) AS total, 
        NULL as status,
        NULL AS paid_date,
        b.`name`,
        b.email,
        b.mobile_phone,
        b.bank_name,
        b.bank_branch_name,
        b.bank_acct_no,
        b.bank_acct_name,
        b.profile_img
    FROM `t_collecting_component` a 
    JOIN `m_partner` b ON a.`partner_seq`=b.`seq`
    WHERE a.collect_seq = pSeq AND a.partner_seq = pPartnerSeq
    GROUP BY a.`collect_seq`, a.`partner_seq`;    
END IF;
END$$
DELIMITER ;