/*
==========================
ADD & MODIFY COLUMN
==========================
*/

ALTER TABLE t_order_voucher ADD agent_seq INT UNSIGNED NULL AFTER member_seq; 
ALTER TABLE t_order_voucher MODIFY COLUMN member_seq INT UNSIGNED NULL;

ALTER TABLE m_provider_service_nominal CHANGE COLUMN buy_price buy_price_1 DECIMAL(12,0) NOT NULL;
ALTER TABLE m_provider_service_nominal CHANGE COLUMN sell_price buy_price DECIMAL(12,0) NOT NULL;
ALTER TABLE m_provider_service_nominal CHANGE COLUMN buy_price_1 sell_price DECIMAL(12,0) NOT NULL;


/*
======================
ADD STORED PROCEDURE
======================
*/
DROP PROCEDURE IF EXISTS `sp_agent_order_list_by_order_no`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_order_list_by_order_no` (
    `pUserID` VARCHAR(100),
    `pIPAddress` VARCHAR(25),
    `pOrderNo` VARCHAR(20)
)
BEGIN
IF EXISTS (SELECT order_no FROM t_order WHERE order_no = pOrderNo) THEN
BEGIN
SELECT 
	o.seq,
	o.order_no,
	o.order_date,
	o.agent_seq,
	o.signature,
	m.`name` member_name,
	m.email,
	m.mobile_phone,
	c.`name` city_name,
	CASE WHEN o.payment_retry <> '0'  THEN CONCAT(o.order_no, '-', CAST(o.payment_retry AS CHAR(2))) ELSE o.order_no END order_trans_no,	
	o.receiver_name,
	o.receiver_address,
	o.receiver_district_seq,
	d.`name` district_name,
	c.`name` city_name,
	p.`name` province_name,
	o.receiver_zip_code,
	o.receiver_phone_no,
	o.payment_status,
	o.pg_method_seq,
	pg.`code` payment_code,
	pg.name payment_name,
	o.paid_date,
	CASE WHEN pv.nominal IS NULL THEN mc.nominal ELSE pv.nominal END AS nominal,
	pv.code AS voucher_code,
	o.payment_retry,
	o.total_order,
	o.voucher_seq,
	o.voucher_refunded,
	o.total_payment,
	o.conf_pay_type,
	o.conf_pay_amt_member,
	o.conf_pay_date,
	o.conf_pay_note_file,
	o.conf_pay_bank_seq,
	o.conf_pay_amt_admin,
	o.promo_credit_seq
FROM 
	t_order o JOIN m_district d
	ON o.receiver_district_seq = d.seq
		JOIN m_city c 
	ON c.seq = d.city_seq 
		JOIN m_province p
	ON p.seq = c.province_seq
		JOIN m_payment_gateway_method pg
	ON pg.seq = o.pg_method_seq
		LEFT JOIN m_promo_voucher pv 
	ON o.voucher_seq = pv.seq
		LEFT JOIN m_coupon mc 
	ON o.coupon_seq = mc.seq
		JOIN m_agent m
	ON m.seq = o.agent_seq
WHERE
	o.order_no = pOrderNo;
    
SELECT 
    t.order_seq,
    t.merchant_info_seq,
    t.expedition_service_seq,
    e.`name` expedition_name,
    mm.`name` merchant_name,
    mm.email,
    mm.code AS merchant_code,
    mm.seq merchant_seq,
    t.real_expedition_service_seq,
    t.total_merchant,
    t.total_ins,
    t.total_ship_real,
    t.total_ship_charged,
    t.free_fee_seq,
    t.order_status,
    t.member_notes,
    t.printed,
    t.print_date,
    t.awb_seq,
    t.awb_no,
    t.ref_awb_no,
    t.ship_by,
    t.ship_by_exp_seq,
    t.ship_date,
    t.ship_note_file,
    t.ship_notes,
    t.received_date,
    t.received_by,
    t.redeem_seq,
    t.exp_invoice_seq,
    t.exp_invoice_awb_seq,
    (SELECT 
            COUNT(*) total_barang
        FROM
            t_order_product top
                JOIN
            t_order tor ON top.order_seq = tor.seq
        WHERE
            tor.order_no = o.order_no
                AND top.merchant_info_seq = t.merchant_info_seq) AS total_product,
	t.created_date,
    o.paid_date,
    t.`modified_date`
FROM
    t_order_merchant t
        JOIN
    m_merchant_info m ON m.seq = t.merchant_info_seq
        JOIN
    m_district d ON d.seq = m.pickup_district_seq
        JOIN
    m_city c ON c.seq = d.city_seq
        JOIN
    m_province p ON p.seq = c.province_seq
        JOIN
    m_merchant mm ON mm.seq = m.merchant_seq
        JOIN
    m_expedition_service s ON s.seq = t.expedition_service_seq
        JOIN
    m_expedition e ON s.exp_seq = e.seq
        JOIN
    t_order o ON o.seq = t.order_seq
WHERE
    o.order_no = pOrderNo;
            
SELECT 
	t.order_seq,
	mi.merchant_seq,
	t.merchant_info_seq,
	p.`name` AS display_name,
	v.seq AS product_seq,
	v.pic_1_img AS img,
	mv.`value`,
	mv.seq AS value_seq,
	mv.`value` AS variant_name,
	t.product_status,
	t.qty,
	t.sell_price,
	t.weight_kg,
	t.ship_price_real,
	t.ship_price_charged,
	t.trx_fee_percent,
	t.ins_rate_percent,
	t.product_status,
	t.qty_return,
	t.created_by,
	t.created_date,
	t.modified_by,
	t.modified_date,
	t.commission_fee_percent
FROM
	t_order_product t 
		JOIN m_product_variant v			
			ON v.seq = t.product_variant_seq
		JOIN m_product p
			ON v.product_seq = p.seq
		JOIN m_variant_value vv
			ON vv.seq = t.variant_value_seq
		JOIN m_variant_value mv 
			ON mv.seq = v.variant_value_seq
		JOIN t_order o
			ON o.seq = t.order_seq
		JOIN m_merchant_info mi
			ON mi.seq = t.merchant_info_seq
		JOIN m_merchant mm 
			ON mm.seq = mi.merchant_seq
WHERE
	o.order_no = pOrderNo;
    
SELECT 
    'Pesanan telah kami terima dan dalam proses verifikasi' AS history_message,
    created_date 
FROM
    t_order
WHERE
    order_no = pOrderNo
UNION ALL SELECT 
    'Pesanan telah terverifikasi dan akan segera dikirim',
    paid_date 
FROM
    t_order
WHERE
    order_no = pOrderNo 
UNION ALL SELECT 
    CONCAT('Pesanan sedang diproses untuk dikirim oleh ',mm.name),
    t.print_date
FROM
    t_order_merchant t
	JOIN
    m_merchant_info m ON m.seq = t.merchant_info_seq
	JOIN
    m_merchant mm ON mm.seq = m.merchant_seq
        JOIN
    t_order o ON o.seq = t.order_seq
WHERE
    o.order_no = pOrderNo  AND  t.print_date <> "0000-00-00 00:00:00"
UNION ALL SELECT 
    CONCAT('Pesanan telah dikirim oleh ',
	    mm.`name`,
            ' dan melalui ',
            e.`name`,
            ', no.resi ',
            t.awb_no),
    t.ship_date 
FROM
    t_order_merchant t
    	JOIN
    m_merchant_info m ON m.seq = t.merchant_info_seq
	JOIN
    m_merchant mm ON mm.seq = m.merchant_seq
        JOIN
    m_expedition_service s ON s.seq = t.expedition_service_seq
        JOIN
    m_expedition e ON s.exp_seq = e.seq
        JOIN
    t_order o ON o.seq = t.order_seq
WHERE
    o.order_no = pOrderNo AND t.ship_date <> "0000-00-00"
UNION ALL SELECT 
    'Pesanan telah diterima pelanggan',
     t.received_date 
FROM
    t_order_merchant t
        JOIN
    t_order o ON o.seq = t.order_seq
WHERE
    o.order_no = pOrderNo  AND t.received_date <> "0000-00-00"
ORDER BY created_date DESC;    
END;
ELSE
BEGIN
SELECT
	o.seq,
	o.order_no,
	o.order_no AS order_trans_no,
	o.agent_seq,
	o.order_date,
	o.total_order,
	o.total_payment,
	o.payment_status,
	o.order_status,
	o.signature,
	o.pg_method_seq,
	o.phone_no AS phone_number,
	psn.nominal,
	ps.name AS provider_service_name,
	p.name AS provider_name,
	pgm.name AS pg_method_name,
	pgm.code AS payment_code,
        mapping_code
FROM 
	t_order_voucher o JOIN m_provider_service_nominal psn
	ON psn.seq = o.provider_nominal_seq
		JOIN m_provider_service ps
	ON ps.seq = psn.provider_service_seq
		JOIN m_provider p
	ON p.seq = ps.provider_seq
		JOIN m_payment_gateway_method pgm
	ON pgm.seq = o.pg_method_seq
		JOIN m_payment_gateway pg
	ON pg.seq = pgm.pg_seq
WHERE
	order_no = pOrderNo;
END;
END IF;	
END $$ 
DELIMITER ;

DROP PROCEDURE IF EXISTS `sp_member_order_list_by_order_no`;
DELIMITER @@
CREATE PROCEDURE sp_member_order_list_by_order_no
(
    pUserID Varchar(100),
    pIPAddress VarChar(25),
    pOrderNo VarChar(20)

)
BEGIN
IF EXISTS (SELECT order_no FROM t_order WHERE order_no = pOrderNo) THEN
BEGIN
Select 
	o.seq,
	o.order_no,
	o.order_date,
	o.member_seq,
	o.signature,
	m.`name` member_name,
	m.email,
	m.mobile_phone,
	c.`name` city_name,
	case when o.payment_retry <> '0'  Then concat(o.order_no, '-', cast(o.payment_retry as char(2))) Else o.order_no End order_trans_no,	
	o.receiver_name,
	o.receiver_address,
	o.receiver_district_seq,
	d.`name` district_name,
	c.`name` city_name,
	p.`name` province_name,
	o.receiver_zip_code,
	o.receiver_phone_no,
	o.payment_status,
	o.pg_method_seq,
	pg.`code` payment_code,
	pg.name payment_name,
	o.paid_date,
	case when pv.nominal is null then mc.nominal else pv.nominal end as nominal,
	pv.code as voucher_code,
	o.payment_retry,
	o.total_order,
	o.voucher_seq,
	o.voucher_refunded,
	o.total_payment,
	o.conf_pay_type,
	o.conf_pay_amt_member,
	o.conf_pay_date,
	o.conf_pay_note_file,
	o.conf_pay_bank_seq,
	o.conf_pay_amt_admin
from 
	t_order o join m_district d
		on o.receiver_district_seq = d.seq
			  join m_city c 
		on c.seq = d.city_seq 
			  join m_province p
		on p.seq = c.province_seq
			  join m_payment_gateway_method pg
		on pg.seq = o.pg_method_seq
        	left join m_promo_voucher pv 
		on o.voucher_seq = pv.seq
        	left join m_coupon mc 
		on o.coupon_seq = mc.seq
			  join m_member m
		on m.seq = o.member_seq
where
	o.order_no = pOrderNo;

select
	t.order_seq,
	t.merchant_info_seq,
	t.expedition_service_seq,
	e.`name` expedition_name,
	mm.`name` merchant_name,
	mm.email,
	mm.code as merchant_code,
	mm.seq merchant_seq,
	t.real_expedition_service_seq,    
	t.total_merchant,
	t.total_ins,
	t.total_ship_real,
	t.total_ship_charged,
	t.free_fee_seq,
	t.order_status,
	t.member_notes,
	t.printed,
	t.print_date,
	t.awb_seq,
	t.awb_no,
	t.ref_awb_no,
	t.ship_by,
	t.ship_by_exp_seq,
	t.ship_date,
	t.ship_note_file,
	t.ship_notes,
	t.received_date,
	t.received_by,
	t.redeem_seq,
	t.exp_invoice_seq,
	t.exp_invoice_awb_seq
from 
	t_order_merchant t join m_merchant_info m 
		on m.seq = t.merchant_info_seq 
				join m_district d
		on d.seq = m.pickup_district_seq
				join m_city c
		on c.seq = d.city_seq 
				join m_province p 
		on p.seq = c.province_seq
				join m_merchant mm
		on mm.seq = m.merchant_seq
				join m_expedition_service s
		on s.seq =  t.expedition_service_seq
				join m_expedition e
		on s.exp_seq = e.seq
				join t_order o
		on o.seq = t.order_seq				
where
		o.order_no = pOrderNo;
        
Select 
	t.order_seq,
	mi.merchant_seq,
	t.merchant_info_seq,
	p.`name` as display_name,
	v.seq as product_seq,
	v.pic_1_img as img,
	mv.`value`,
	mv.seq as value_seq,
	mv.`value` as variant_name,
	t.product_status,
	t.qty,
	t.sell_price,
	t.weight_kg,
	t.ship_price_real,
	t.ship_price_charged,
	t.trx_fee_percent,
	t.ins_rate_percent,
	t.product_status,
	t.qty_return,
	t.created_by,
	t.created_date,
	t.modified_by,
	t.modified_date
from
	t_order_product t 
		Join m_product_variant v			
			On v.seq = t.product_variant_seq
		Join m_product p
			On v.product_seq = p.seq
		Join m_variant_value vv
			On vv.seq = t.variant_value_seq
		Join m_variant_value mv 
			On mv.seq = v.variant_value_seq
		Join t_order o
			On o.seq = t.order_seq
		Join m_merchant_info mi
			On mi.seq = t.merchant_info_seq
		join m_merchant mm 
			On mm.seq = mi.merchant_seq
where
	o.order_no = pOrderNo;
END;	
ELSE
BEGIN
SELECT
	o.seq,
	o.order_no,
	o.order_no AS order_trans_no,
	o.member_seq,
	o.order_date,
	o.total_order,
	o.total_payment,
	o.payment_status,
	o.order_status,
	o.signature,
	o.pg_method_seq,
	o.phone_no AS phone_number,
	psn.nominal,
	ps.name AS provider_service_name,
	p.name AS provider_name,
	pgm.name AS pg_method_name,
	pgm.code AS payment_code,
        mapping_code
FROM 
	t_order_voucher o JOIN m_provider_service_nominal psn
	ON psn.seq = o.provider_nominal_seq
		JOIN m_provider_service ps
	ON ps.seq = psn.provider_service_seq
		JOIN m_provider p
	ON p.seq = ps.provider_seq
		JOIN m_payment_gateway_method pgm
	ON pgm.seq = o.pg_method_seq
		JOIN m_payment_gateway pg
	ON pg.seq = pgm.pg_seq
WHERE
	order_no = pOrderNo;
END;
END IF;	
END @@ 
DELIMITER ; 


DROP PROCEDURE IF EXISTS `sp_order_voucher_add`;
DELIMITER $$
CREATE PROCEDURE `sp_order_voucher_add` (
    pUserID Varchar(25),
    pOrderNo Varchar(25),
    pPhoneNo Varchar(25),
    pProviderNominalSeq TinyInt unsigned,
    pPgMethodSeq Tinyint unsigned,
    pPaymentStatus Char(1),
    pOrderStatus Char(1),
    pStatus varchar(7)
)

BEGIN
declare new_seq SmallInt Unsigned;
declare pTotalOrder Decimal(12,0);
declare pTotalPayment Decimal(12,0);

SELECT 
    MAX(seq) + 1
INTO new_seq FROM
    t_order_voucher;

If new_seq Is Null Then
	Set new_seq = 1;
End If;


SELECT 
    sell_price
INTO pTotalOrder FROM
    m_provider_service_nominal
WHERE
    seq = pProviderNominalSeq;

set pTotalPayment = pTotalOrder;

IF pStatus = 'member' THEN

insert into t_order_voucher(
	seq,
    order_no,
    order_date,
    member_seq,
    phone_no,
    pg_method_seq,
    provider_nominal_seq,
    total_payment,
    total_order,
    payment_status,
    order_status,
    created_by,
    created_date,
    modified_by,
    modified_date
) values (
	new_seq,
    pOrderNo,
    CURDATE(),
    pUserID,
    pPhoneNo,
    pPGMethodSeq,
    pProviderNominalSeq,
    pTotalPayment,
    pTotalOrder,
    pPaymentStatus,
    pOrderStatus,
    pUserID,
    now(),
    pUserID,
    now()
);
ELSE 
insert into t_order_voucher(
    seq,
    order_no,
    order_date,
    agent_seq,
    phone_no,
    pg_method_seq,
    provider_nominal_seq,
    total_payment,
    total_order,
    payment_status,
    order_status,
    created_by,
    created_date,
    modified_by,
    modified_date
) values (
    new_seq,
    pOrderNo,
    CURDATE(),
    pUserID,
    pPhoneNo,
    pPGMethodSeq,
    pProviderNominalSeq,
    pTotalPayment,
    pTotalOrder,
    pPaymentStatus,
    pOrderStatus,
    pUserID,
    now(),
    pUserID,
    now()
);
END IF;
END$$

DELIMITER ;

DROP PROCEDURE IF EXISTS `sp_agent_order_list_detail_by_order_no`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_order_list_detail_by_order_no` (
    `pUserID` VARCHAR(100),
    `pIPAddr` VARCHAR(50),
    `pAgentSeq` BIGINT UNSIGNED,
    `pHSeq` VARCHAR(50)
)
BEGIN
    SELECT 
        op.order_seq,
        o.order_no,
        o.order_date,
        o.payment_status,
        o.pg_method_seq,
        o.total_payment,
        o.total_order,
        p.name AS product_name,
        v.display_name,
        vv.value,
        p.p_weight_kg,
        p.p_length_cm,
        p.p_width_cm,
        p.p_height_cm,
        op.qty,
        op.sell_price,
        op.commission_fee_percent,
        op.product_status,
        op.trx_fee_percent,
        pv.pic_1_img AS img_src,
        p.merchant_seq
    FROM
        t_order o
            JOIN
        t_order_product op ON o.seq = op.order_seq
            JOIN
        m_product_variant pv ON op.product_variant_seq = pv.seq
            JOIN
        m_product p ON pv.product_seq = p.seq
            JOIN
        m_variant_value vv ON op.variant_value_seq = vv.seq
            JOIN
        m_variant v ON vv.variant_seq = v.seq
    WHERE
        o.order_no = pHSeq
            AND o.agent_seq = pAgentSeq;
END$$

DELIMITER ;

DROP PROCEDURE IF EXISTS `sp_agent_payment_retry_update`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_payment_retry_update` (
    `pUserID` VARCHAR(100),
    `pIPAddr` VARCHAR(50),
    `pAgentSeq` BIGINT UNSIGNED,
    `pOrderNo` VARCHAR(50),
    `pPaymentStatus` CHAR(1),
    `pPgMethodSeq` BIGINT UNSIGNED
)
BEGIN
    update t_order set
            payment_status= pPaymentStatus,
        pg_method_seq = pPgMethodSeq,
            payment_retry = (payment_retry + 1)
    Where
            agent_seq = pAgentSeq AND order_no = pOrderNo;
END$$


DROP PROCEDURE IF EXISTS `sp_agent_order_redeem_info`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_order_redeem_info` (
    `pUserID` VARCHAR(100),
    `pIPAddr` VARCHAR(50),
    `pAgentSeq` BIGINT(10)
)
BEGIN

SELECT 
    sum((top.qty * top.sell_price) * (top.commission_fee_percent/100)) AS pending_commission
FROM
    t_order tor
        JOIN
    t_order_merchant tom ON tor.seq = tom.order_seq
        JOIN
    t_order_product top ON tor.seq = top.order_seq
        AND tom.merchant_info_seq = top.merchant_info_seq
        AND top.order_seq = tom.order_seq
        JOIN
    m_district md ON tor.receiver_district_seq = md.seq
        JOIN
    m_city mc ON mc.seq = md.city_seq
        JOIN
    m_province mp ON mp.seq = mc.province_seq
        JOIN
    m_product_variant mpv ON mpv.seq = top.product_variant_seq
        JOIN
    m_product mpr ON mpv.product_seq = mpr.seq
        JOIN
    m_variant_value vv ON vv.seq = top.variant_value_seq
        JOIN
    m_variant_value mv ON mv.seq = mpv.variant_value_seq
WHERE
    tor.seq NOT IN (SELECT 
            `to`.seq
        FROM
            t_order `to`
                LEFT JOIN
            t_order_merchant tom ON tom.order_seq = `to`.seq
                LEFT JOIN
            t_redeem_period trp ON trp.seq = tom.redeem_agent_seq
                LEFT JOIN
            t_redeem_agent tra ON tra.redeem_seq = trp.seq
        WHERE
            `to`.agent_seq = pAgentSeq
                AND tra.status = 'P')
        AND tor.agent_seq = pAgentSeq
        AND tor.payment_status = 'P'
        AND top.product_status = 'R'
        And tom.order_status in ('R','S');
SELECT 
    sum((top.qty * top.sell_price) * (top.commission_fee_percent/100)) AS commission
FROM
    t_order tor
        JOIN
    t_order_merchant tom ON tor.seq = tom.order_seq
        JOIN
    t_order_product top ON tor.seq = top.order_seq
        AND tom.merchant_info_seq = top.merchant_info_seq
        AND top.order_seq = tom.order_seq
        JOIN
    m_district md ON tor.receiver_district_seq = md.seq
        JOIN
    m_city mc ON mc.seq = md.city_seq
        JOIN
    m_province mp ON mp.seq = mc.province_seq
        JOIN
    m_product_variant mpv ON mpv.seq = top.product_variant_seq
        JOIN
    m_product mpr ON mpv.product_seq = mpr.seq
        JOIN
    m_variant_value vv ON vv.seq = top.variant_value_seq
        JOIN
    m_variant_value mv ON mv.seq = mpv.variant_value_seq
WHERE
    tor.seq NOT IN (SELECT 
            `to`.seq
        FROM
            t_order `to`
                LEFT JOIN
            t_order_merchant tom ON tom.order_seq = `to`.seq
                LEFT JOIN
            t_redeem_period trp ON trp.seq = tom.redeem_agent_seq
                LEFT JOIN
            t_redeem_agent tra ON tra.redeem_seq = trp.seq
        WHERE
            `to`.agent_seq = pAgentSeq
                AND tra.status = 'P')
        AND tor.agent_seq = pAgentSeq
        AND tor.payment_status = 'P'
        AND top.product_status = 'R'
        And tom.order_status = 'D';
END$$

DELIMITER ;

DROP PROCEDURE IF EXISTS `sp_agent_order_redeem_list`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_order_redeem_list` (
 `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pAgentSeq` BIGINT(10),
         pCurrPage INT UNSIGNED,
         pRecPerPage INT UNSIGNED,
        `pPaymentStatus` CHAR(1)
)
BEGIN
  -- Begin variables for Paging
    DECLARE startRec INT UNSIGNED;
    DECLARE totalPage INT UNSIGNED;
    DECLARE totalRec INT UNSIGNED;
    -- End variables for Paging
    Declare sqlWhere VarChar(500);
    Set sqlWhere = "";
    SET pCurrpage = (pCurrpage / pRecPerPage) + 1;
    If pPaymentStatus = "P" Then
        Set sqlWhere = Concat(sqlWhere, " And tom.order_status = 'D' ");
    End If;
    If pPaymentStatus = "U" Then
        Set sqlWhere = Concat(sqlWhere, " And tom.order_status in ('R','S') ");
    End If;
    -- Begin Paging Info
    SET @sqlCommand = "Select Count(*) Into @totalRec From t_order tor
        join t_order_merchant tom on tor.seq=tom.order_seq
        join t_order_product top on tor.seq=top.order_seq and tom.merchant_info_seq=top.merchant_info_seq and top.order_seq=tom.order_seq ";
    Set @sqlCommand = Concat(@sqlCommand, " Where tor.seq NOT IN (SELECT 
            `to`.seq
        FROM
            t_order `to`
                LEFT JOIN
            t_order_merchant tom ON tom.order_seq = `to`.seq
                LEFT JOIN
            t_redeem_period trp ON trp.seq = tom.redeem_agent_seq
                LEFT JOIN
            t_redeem_agent tra ON tra.redeem_seq = trp.seq
        WHERE
            `to`.agent_seq = ",pAgentSeq," AND tra.status = 'P') AND tor.agent_seq ='" ,pAgentSeq, "' and tor.payment_status='P' and top.product_status='R' ");
    IF sqlWhere <> "" THEN
        SET @sqlCommand = CONCAT(@sqlCommand, sqlWhere);
    END IF;
    PREPARE sql_stmt FROM @sqlCommand;
    EXECUTE sql_stmt;
    DEALLOCATE PREPARE sql_stmt;
    SET totalRec = @totalRec;
    IF totalRec = 0 THEN
        SET totalPage = 0;
        SET pCurrPage = 1;
        SET startRec = 0;
    ELSE
        SET totalPage = CEILING(totalRec / pRecPerPage);
        IF pCurrPage <= 0 THEN
            SET pCurrPage = 1;
        ELSEIF pCurrPage > totalPage THEN
            SET pCurrPage = totalPage;
        END IF;
        SET startRec = (pCurrPage - 1) * pRecPerPage;
    END IF;
    -- End Paging Info
    SET @sqlCommand = "
        Select
            tor.seq,
            tor.agent_seq,
            tor.order_no,
            tor.order_date,
            tor.receiver_name,
            tor.receiver_address,
            tor.receiver_district_seq,
            tor.receiver_zip_code,
            tor.receiver_phone_no,
            tom.order_status,
            tom.received_date,
            top.product_variant_seq,
            top.variant_value_seq,
            top.qty,
            top.sell_price,
            top.weight_kg,
            top.ship_price_real,
            top.commission_fee_percent,
            (top.qty * top.sell_price) * (top.commission_fee_percent/100) as komisiAgent,
            md.`name` district_name,
            mc.`name` city_name,
            mp.`name` province_name,
            mpr.`name` AS display_name,
            mpr.merchant_seq,
            mpv.seq AS product_seq,
            mpv.pic_1_img AS img,
            mv.`value`,
            mv.seq AS value_seq,
            mv.`value` AS variant_name
       from t_order tor
		join t_order_merchant tom on tor.seq=tom.order_seq
		join t_order_product top on tor.seq=top.order_seq and tom.merchant_info_seq=top.merchant_info_seq and top.order_seq=tom.order_seq
                JOIN m_district md ON tor.receiver_district_seq = md.seq
                JOIN m_city mc ON mc.seq = md.city_seq
                JOIN m_province mp ON mp.seq = mc.province_seq
                JOIN m_product_variant mpv ON mpv.seq = top.product_variant_seq
		JOIN m_product mpr ON mpv.product_seq = mpr.seq
		JOIN m_variant_value vv ON vv.seq = top.variant_value_seq
		JOIN m_variant_value mv ON mv.seq = mpv.variant_value_seq
		";
    Set @sqlCommand = Concat(@sqlCommand, " Where tor.seq NOT IN (SELECT 
            `to`.seq
        FROM
            t_order `to`
                LEFT JOIN
            t_order_merchant tom ON tom.order_seq = `to`.seq
                LEFT JOIN
            t_redeem_period trp ON trp.seq = tom.redeem_agent_seq
                LEFT JOIN
            t_redeem_agent tra ON tra.redeem_seq = trp.seq
        WHERE
            `to`.agent_seq = ",pAgentSeq," AND tra.status = 'P') AND tor.agent_seq ='" ,pAgentSeq, "' and tor.payment_status='P' and top.product_status='R' ");
    If sqlWhere <> "" Then
        Set @sqlCommand = Concat(@sqlCommand, sqlWhere);
    End If;

    Set @sqlCommand = Concat(@sqlCommand, " order by tor.order_date desc, tor.seq desc");
    SET @sqlCommand = CONCAT(@sqlCommand, " Limit ", startRec, ", ", pRecPerPage);
    SELECT pCurrPage, totalPage, totalRec AS total_rec;
    Prepare sql_stmt FROM @sqlCommand;
    Execute sql_stmt;
    Deallocate prepare sql_stmt;
    Set @sqlCommand = Null;
END$$

DELIMITER ;

DROP PROCEDURE IF EXISTS `sp_agent_add`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_add` (
    `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pEmail` VARCHAR(50),
        `pName` VARCHAR(100),
        `pDate` DATE,
        `pGender` VARCHAR(1),
        `pPhone` VARCHAR(20),
        `pPassword` VARCHAR(100),
        `pImage` VARCHAR(100),
        `pPartnerSeq` INTEGER(10),
        `pStatus` CHAR(1),
        `pBankName` VARCHAR(50),
        `pBankBranchName` VARCHAR(50),
        `pBankAcctNo` VARCHAR(50),
        `pBankAcctName` VARCHAR(50),
        `pCommisionProRate` DECIMAL(5,2)
)
BEGIN

    DECLARE new_seq INT UNSIGNED;
	SELECT 
        MAX(seq) + 1 INTO new_seq
    FROM m_agent;
    IF new_seq IS NULL THEN
        SET new_seq = 1;
    END IF;
    
	INSERT INTO m_agent(
	seq,
	email,
	PASSWORD,
	`name`,
        partner_seq,
	birthday,
	gender,
	mobile_phone,
	profile_img,
	deposit_amt,
	bank_name,
	bank_branch_name,
	bank_acct_no,
	bank_acct_name,
	`status`,
        commission_pro_rate,
	created_by,
	created_date,
	modified_by,
	modified_date
	) VALUES (
	new_seq,
	pEmail,
	pPassword,
	pName,
        pPartnerSeq,
	pDate,
	pGender,
	pPhone,
	pImage,
	0,
	`pBankName`,
	`pBankBranchName`,
	`pBankAcctNo`,
	`pBankAcctName`,
	'A',
        pCommisionProRate,
	pUserID,
	NOW(),
	pUserID,
	NOW()
	);   

END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS `sp_agent_by_seq`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_by_seq`(
        pUserID VARCHAR(50),
        pIPAddr VARCHAR(50),
        pSeq INT(10) UNSIGNED
    )
BEGIN
SELECT
  `seq`,
  `email`,
  `password`,
  `name`,
  `partner_seq`,
  `referral`,
  `birthday`,
  `gender`,
   status,
  `mobile_phone`,
  `profile_img`,
  `deposit_amt`,
  `last_login`,
  `ip_address`,
  `created_by`,
  `created_date`,
  `modified_by`,
  `modified_date`,
    bank_name,
    bank_branch_name,
    bank_acct_no,
    bank_acct_name,
   commission_pro_rate
FROM `m_agent`
WHERE 
   seq = pSeq;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS `sp_agent_update`;
DELIMITER $$
CREATE PROCEDURE `sp_agent_update`(
        `pUserID` VARCHAR(100),
        `pIPAddr` VARCHAR(50),
        `pName` VARCHAR(100),
        `pDate` DATE,
        `pGender` VARCHAR(1),
        `pPhone` VARCHAR(20),
        `pPassword` VARCHAR(100),
        `pImage` VARCHAR(100),
        `pPartnerSeq` INTEGER(10),
        `pStatus` CHAR(1),
        `pBankName` VARCHAR(50),
        `pBankBranchName` VARCHAR(50),
        `pBankAcctNo` VARCHAR(50),
        `pBankAcctName` VARCHAR(50),
	`pSeq` INTEGER(10),
        `pCommisionProRate` DECIMAL(3,0)
    )
BEGIN
if pPassword <> ''
then
	update `m_agent`
	set 
	  `password` = pPassword,
	  `name` = pName,
	  `birthday` = pDate,
	  `partner_seq` = pPartnerSeq,
	  `gender` = pGender,
	  `mobile_phone` = pPhone,
	  `profile_img` = pImage,
	  `bank_name` = pBankName,
	  `bank_branch_name` = pBankBranchName,
	  `bank_acct_no` = pBankAcctNo,
	  `bank_acct_name` = pBankAcctName,
	  `status` = pStatus,
	  `modified_by` = pUserID,
	  `modified_date` = NOW(),
          `commission_pro_rate` = pCommisionProRate
	where `seq` = pSeq;
else
	UPDATE `m_agent`
	SET 
	  `name` = pName,
	  `birthday` = pDate,
	  `partner_seq` = pPartnerSeq,
	  `gender` = pGender,
	  `mobile_phone` = pPhone,
	  `profile_img` = pImage,
	  `bank_name` = pBankName,
	  `bank_branch_name` = pBankBranchName,
	  `bank_acct_no` = pBankAcctNo,
	  `bank_acct_name` = pBankAcctName,
	  `status` = pStatus,
	  `modified_by` = pUserID,
	  `modified_date` = NOW(),
          `commission_pro_rate` = pCommisionProRate
	WHERE `seq` = pSeq;	
end if;    
END$$
DELIMITER ;