function JsonDecode(str){
    var retval;
    try{
        retval = jQuery.parseJSON(str);
    } catch (e) {
        retval = null;
    }
    return retval;
}
function isSessionExpired(error){
    error_object = JsonDecode(error);
    var retval = false;
    try{
        url = error_object.url;
        message = error_object.message;
        code = error_object.code;
        if(url != undefined){
            retval = true;
        }
    }catch (e){
        retval = false;
    }
    return retval;
}
function json_decode(json){
    try{
        json_object = JsonDecode(json);
    }catch (e){
        json_object =  undefined;
    }
    return json_object;
}
function error_response(code){
    var error_message = '';
    if(code == '0'){
        error_message = 'Sambungan internet tidak dapat menjangkau server';
    }
    else if(code == '100'){
        error_message = 'Melanjutkan';
    }else if(code == '101'){
        error_message = 'Memilih Protokol';
    }else if(code == '102'){
        error_message = 'Memproses';
    }else if(code == '200'){
        error_message = 'Session Terputus. Mohon login kembali';
    }else if(code == '201'){
        error_message = 'Request Berhasil dibuat';
    }else if(code == '202'){
        error_message = 'Request berhasil diterima';
    }else if(code == '203'){
        error_message = 'Non-Authoritative Information';
    }else if(code == '204'){
        error_message = 'Tanpa Konten';
    }else if(code == '205'){
        error_message = 'Reset Content';
    }else if(code == '206'){
        error_message = 'Partial Content';
    }else if(code == '207'){
        error_message = 'Multi-Status';
    }else if(code == '208'){
        error_message = 'Already Reported';
    }else if(code == '226'){
        error_message = 'IM Used';
    }else if(code == '300'){
        error_message = 'Multiple Choices';
    }else if(code == '301'){
        error_message = 'Dipindah Permanen';
    }else if(code == '302'){
        error_message = 'Ditemukan';
    }else if(code == '303'){
        error_message = 'Lihat Lainnya';
    }else if(code == '304'){
        error_message = 'Not Modified';
    }else if(code == '305'){
        error_message = 'Use Proxy';
    }else if(code == '306'){
        error_message = 'Switch Proxy';
    }else if(code == '307'){
        error_message = 'Temporary Redirect';
    }else if(code == '308'){
        error_message = 'Permanent Redirect';
    }else if(code == '400'){
        error_message = 'Bad Request';
    }else if(code == '401'){
        error_message = 'Unauthorized';
    }else if(code == '402'){
        error_message = 'Payment Required';
    }else if(code == '403'){
        error_message = 'Forbidden';
    }else if(code == '404'){
        error_message = 'Not Found';
    }else if(code == '405'){
        error_message = 'Method Not Allowed';
    }else if(code == '406'){
        error_message = 'Not Acceptable';
    }else if(code == '407'){
        error_message = 'Proxy Authentication Required';
    }else if(code == '408'){
        error_message = 'Request Timeout';
    }else if(code == '409'){
        error_message = 'Conflict';
    }else if(code == '410'){
        error_message = 'Gone';
    }else if(code == '411'){
        error_message = 'Length Required';
    }else if(code == '412'){
        error_message = 'Precondition Failed';
    }else if(code == '413'){
        error_message = 'Payload Too Large';
    }else if(code == '414'){
        error_message = 'URI Too Long';
    }else if(code == '415'){
        error_message = 'Unsupported Media Type';
    }else if(code == '416'){
        error_message = 'Range Not Satisfiable';
    }else if(code == '417'){
        error_message = 'Expectation Failed';
    }else if(code == '418'){
        error_message = "I'm a teapot";
    }else if(code == '419'){
        error_message = 'Authentication Timeout';
    }else if(code == '420'){
        error_message = 'Method Failure';
    }else if(code == '421'){
        error_message = 'Misdirected Request';
    }else if(code == '422'){
        error_message = 'Unprocessable Entity';
    }else if(code == '423'){
        error_message = 'Locked';
    }else if(code == '424'){
        error_message = 'Failed Dependency';
    }else if(code == '426'){
        error_message = 'Upgrade Required';
    }else if(code == '428'){
        error_message = 'Precondition Required';
    }else if(code == '429'){
        error_message = 'Too Many Requests';
    }else if(code == '431'){
        error_message = 'Request Header Fields Too Large';
    }else if(code == '440'){
        error_message = 'Login Timeout';
    }else if(code == '444'){
        error_message = 'No Response';
    }else if(code == '449'){
        error_message = 'Retry With';
    }else if(code == '450'){
        error_message = 'Blocked by Windows Parental Controls';
    }else if(code == '451'){
        error_message = 'Unavailable For Legal Reasons';
    }else if(code == '494'){
        error_message = 'Request Header Too Large';
    }else if(code == '495'){
        error_message = 'Cert Error';
    }else if(code == '496'){
        error_message = 'No Cert';
    }else if(code == '497'){
        error_message = 'HTTP to HTTPS';
    }else if(code == '498'){
        error_message = 'Token expired/invalid';
    }else if(code == '499'){
        error_message = 'Client Closed Request';
    }else if(code == '500'){
        error_message = 'Internal Server Error';
    }else if(code == '501'){
        error_message = 'Not Implemented';
    }else if(code == '502'){
        error_message = 'Bad Gateway';
    }else if(code == '503'){
        error_message = 'Service Unavailable';
    }else if(code == '504'){
        error_message = 'Gateway Timeout';
    }else if(code == '505'){
        error_message = 'HTTP Version Not Supported';
    }else if(code == '506'){
        error_message = 'Variant Also Negotiates';
    }else if(code == '507'){
        error_message = 'Insufficeient Storage';
    }else if(code == '508'){
        error_message = 'Loop Detected';
    }else if(code == '509'){
        error_message = 'Bandwidth Limit Exceeded';
    }else if(code == '510'){
        error_message = 'Not Extended';
    }else if(code == '511'){
        error_message = 'Network Authentication Required';
    }else if(code == '520'){
        error_message = 'Origin Error';
    }else if(code == '521'){
        error_message = 'Web server is down';
    }else if(code == '522'){
        error_message = 'Connection timed out';
    }else if(code == '523'){
        error_message = 'Proxy Declined Request';
    }else if(code == '524'){
        error_message = 'A timeout occured';
    }else if(code == '598'){
        error_message = 'Network read timeout error';
    }else if(code == '599'){
        error_message = 'Network connect timeout error';
    }else{
        error_message = 'error';
    }
    return error_message;
}
