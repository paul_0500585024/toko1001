var elevatezoom_index = 1;
function create_captcha() {
    $.ajax({
        url: base_url + "member/c_signup_member/captcha",
        success: function(response) { 
            if (isSessionExpired(response)) {
                response_object = json_decode(response);
                url = response_object.url;
                location.href = url;
            } else {
                $('#captcha_img_member').html(response);
            }
        },
        error: function(request, error) {
            alert(error_response(request.status));
        }
    });
}

$(document).on("click", "#loginregister", function() {
    var myData = $(this).data('id');
    $('.form-group').removeClass('has-error');
    $('.form-group').find('span.help-block').hide();
    $('#content-message').empty();
    $.fn.modal.Constructor.prototype.enforceFocus = function() {
    };
    if (myData == 1) {
        $(".modal-body #tablogin").addClass('active');
        $(".modal-body #login").addClass('active');

        $(".modal-body #tabregister").removeClass('active');

        $(".modal-body #daftar").removeClass('active');

    }
    else if (myData == 0) {
        create_captcha();
        $(".modal-body #tabregister").addClass('active');
        $(".modal-body #daftar").addClass('active');

        $(".modal-body #tablogin").removeClass('active');

        $(".modal-body #login").removeClass('active');
    }
});

function findBootstrapEnvironment() {
    var envValues = ["xs", "sm", "md", "lg"];

    var $el = $('<div>');
    $el.appendTo($('body'));

    for (var i = envValues.length - 1; i >= 0; i--) {
        var envVal = envValues[i];

        $el.addClass('hidden-' + envVal);
        if ($el.is(':hidden')) {
            $el.remove();
            return envValues[i]
        }
    }
    ;
}

$(window).resize(function() {
    $("#id_detail_big_thumb" + elevatezoom_index).trigger('click');
});


$(document).on("click", "#goto_chart", function() {
    window.location = "cart";
});
/*$(document).on("click", "#pengirimangratis", function() {
 $('#pengirimangratis').popover();
 });*/

function create_star(num_star, total_star) {
    num_star = typeof num_star !== 'undefined' ? num_star : 0;
    total_star = typeof total_star !== 'undefined' ? total_star : 5;
    star_created = '';
    if (num_star >= 0 && total_star > 0 && num_star <= total_star) {
        for (counter = 0; counter < num_star; counter++) {
            star_created += '<span class="fa fa-star"></span>';
        }
        for (counter = num_star; counter < total_star; counter++) {
            star_created += '<span class="fa fa-star-o"></span>';
        }
    }
    return star_created;
}

function getParameterByName(name, url) {
    if (!url)
        url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
    if (!results)
        return null;
    if (!results[2])
        return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
;

function ulasan_success(response) {
    response_object = json_decode(response);
    product_review = response_object.product_review;
    total_product_review = response_object.total_product_review;
    pagination = response_object.pagination;
    html_display_table_review_open = '<div class="table-responsive"><table class="table"><tr class="info"><th>Rating</th><th>Komentar</th> <th>User</th><th>Waktu</th></tr>';
    html_display_product_review = '';
    $.each(product_review, function(key, value) {
        html_display_product_review += '<tr>' +
                '<td>' + create_star(value.rate) + '</td>' +
                '<td>' + value.review_admin + '</td>' +
                '<td>' + value.name + '</td>' +
                '<td>' + value.days_old + ' hari lalu' + '</td>' +
                '</tr>';
    });
    html_display_product_review += pagination;
    html_display_table_review_close = '</table></div>';
    $('#display_product_review').html(html_display_table_review_open + html_display_product_review + html_display_table_review_close);
    $('ul#pagination_review li a').click(function(event) {
        href = $(this).attr('href');
        if (href != '#') {
            start = getParameterByName('start', href);
            start_val = (start != null) ? start : '0';
            create_ulasan(start_val);
        }
        return false;
    });
}

function create_ulasan(start, limit, where, order) {
    start = typeof start !== 'undefined' ? start : '0';
    limit = typeof limit !== 'undefined' ? limit : '3';
    where = typeof where !== 'undefined' ? where : '';
    order = typeof order !== 'undefined' ? order : '';
    $.ajax({
        url: base_url + "home/c_detail/product_review",
        type: 'get',
        data: 'product_variant_seq=' + product_variant_seq +
                '&start=' + start + '&limit=' + limit + '&where=' + where +
                '&order=' + order,
        beforeSend: function(xhr) {
            $('#display_product_review').html("<img src='" + base_url + "assets/img/home/loading.gif' class='img-responsive center-block'/>");
        },
        success: function(response) {
            ulasan_success(response);
        },
        error: function(request, error) {
            alert(error_response(request.status));
        }
    });
}

$("ul#sidebar li a[data-toggle=\"tab\"]").on("shown.bs.tab", function(e) {
    var tab_name = ($(this).attr('href'));
    if (tab_name == '#daftar') {
        create_captcha();
    } else if (tab_name == '#ulasan') {
        create_ulasan();
    }
});


$(document).ready(function(e) {
    $('#maxOption2_lg,#maxOption2_md').selectpicker({});

    $('#product_related').carousel({
        interval: 10000
    })

    $('.carousel_product_related .item_product_related').each(function() {
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        if (next.next().length > 0) {
            next.next().children(':first-child').clone().appendTo($(this));
        }
        else {
            $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
        }
    });
    $('[data-toggle="popover"]').popover();
    var banner = $('#banner')[0];
//    if (banner != null) {
//        $('#topnavbar').affix({
//            offset: {
//                top: $('#banner').height()
//            }
//        });
//    } else {
//        $('#topnavbar').affix({   
//            offset: {
//                top: 0
//            }
//        });
//    }
    /* BOOTSTRAP SLIDER */
    $('.slider').slider();
    $("#sliderprice").on("slide", function(slideEvt) {
        $("#pricerange").val(slideEvt.value);
    });

    $(' #da-thumbs > li ').each(function() {
        $(this).hoverdir();
    });
    var menuLeft = $('#cbp-spmenu-s1')[0],
            showLeft = $('#showLeft')[0],
            closeMenu = $('#closeMenu')[0];

    var loginRight = $('#cbp-spmenu-login')[0],
            showRight = $('#user_profile')[0],
            closeLogin = $('#closeLogin')[0];

    if (showLeft != null) {
        showLeft.onclick = function() {
            classie.toggle(this, 'active');
            classie.toggle(menuLeft, 'cbp-spmenu-open');
            $('#cbp-spmenu-login').removeClass('cbp-spmenu-open');
        };
    }

    if (closeMenu != null) {
        closeMenu.onclick = function() {
            classie.toggle(this, 'active');
            classie.toggle(menuLeft, 'cbp-spmenu-open');
            disableOther('showLeft');
        };
    }

    if (showRight != null) {
        showRight.onclick = function() {
            classie.toggle(this, 'active');
            classie.toggle(loginRight, 'cbp-spmenu-open');
            disableOther('showRight');
            $('#cbp-spmenu-s1').removeClass('cbp-spmenu-open');
//            $(".cbp-spmenu-left").slideToggle();
        };
    }

    if (closeLogin != null) {
        closeLogin.onclick = function() {
            classie.toggle(this, 'active');
            classie.toggle(loginRight, 'cbp-spmenu-open');
            disableOther('showRight');
        };
    }

    function disableOther(button) {
        if (button !== 'showLeft') {
            classie.toggle(showLeft, 'disabled');
        }
    }


    $('.pagi-container label:nth-child(3)>img').trigger('click');

    $('#li-metode-btn').click(function()
    {
        $('#li-metode').addClass('active');
        $('#li-info').removeClass('active');

    })

    $('#detil-transaksi-btn').click(function()
    {
        $('#li-metode').removeClass('active');
        $('#li-detil').addClass('active');
    });

    $("[id^='id_detail_big_thumb']").click(function() {
        var get_num_image = this.id.replace("id_detail_big_thumb", "");
        $("input[id^='input_detail_big_thumb" + get_num_image + "']").trigger('click');
        $("img[id^='id_data_small_thumb']").removeAttr('style');
        $('#active_img_zoom_container_number').val(get_num_image);
        if (findBootstrapEnvironment() == 'xs') {
            $('.zoomContainer').remove();
        } else {
            $("img[id^='id_data_small_thumb" + get_num_image + "']").elevateZoom({zoomType: "lens", lensShape: "round", lensSize: 150, scrollZoom: true, loadingIcon: base_url + 'assets/img/home/loading.gif'});
        }
        var img_xs = $("#id_xs_data_small_thumb" + get_num_image).attr("src");
        $("#id_display_xs_detail_big_thumb").html('<center><img src="' + img_xs + '"  id="preview" class="img-responsive"  style="text-align: center"></center>');
        elevatezoom_index = get_num_image;
    });
    /*    $("input[id^='id_detail_big_thumb_xs']").click(function() {
     var get_num_image = this.id.replace("id_detail_big_thumb_xs", "");
     var img_xs = $("#id_xs_data_small_thumb" + get_num_image).attr("src");
     $("#id_display_xs_detail_big_thumb").html('<img src="' + img_xs + '"  id="preview" class="img-responsive"  style="text-align: center">');
     });*/
    setTimeout(clickdetail, 500);
    function clickdetail() {
        $("#id_detail_big_thumb1").trigger('click')
    }
//    $("#id_detail_big_thumb_xs1").trigger('click');

    //Datemask dd/mm/yyyy
//    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    $("[data-mask]").inputmask({"placeholder": ""});

    //iCheck for checkbox and radio inputs


    /*    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
     checkboxClass: 'icheckbox_minimal-blue',
     radioClass: 'iradio_minimal-blue'
     });*/

    var valid_code = true;
    var valid_email = true;
    $("#email_merchant").blur(function() {
        $.ajax({
            type: "POST",
            url: base_url + "merchant/sign_up",
            data: {email: $("#email_merchant").val(), btnApprove: "true"},
            success: function(msgemail) {
                if (isSessionExpired(msgemail)) {
                    response_object = json_decode(msgemail);
                    url = response_object.url;
                    location.href = url;
                } else {
                    if (msgemail.trim() == "true")
                    {
                        if ($("#email_merchant").val() == "") {
                            $("span#email_span").addClass("glyphicon-envelope");
                            $("#img_yes").hide();
                            $("#img_no").hide();
                        } else {
                            $("span#email_span").removeClass("glyphicon-envelope");
                            $("span#email_span").html("<img id='img_yes' src='" + base_url + "assets/img/yes.png' />");
                        }
                        valid_email = true;
                    }
                    else
                    {
                        valid_email = false;
                        if ($("#email_merchant").val() == "") {
                            $("span#email_span").addClass("glyphicon-envelope");
                            $("#img_yes").hide();
                            $("#img_no").hide();
                        } else {
                            $("span#email_span").removeClass("glyphicon-envelope");
                            $("span#email_span").html("<img id='img_no' src='" + base_url + "assets/img/no.png' />");
                        }
                    }
                }
            },
            error: function(request, error) {
                alert(error_response(request.status));
            }
        });
    });

    var isValidSignUp = $('#frmSignUpMerchant').validate({
        ignore: 'input[type=hidden]',
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.hasClass('select2')) {
                error.insertAfter(element.next('span'));
            } else {
                error.insertAfter(element);
            }
        }
    });

    var success_html = '<div class="row text-green" >';
    var error_html = '<div class="row text-red" >';
    $('form#frmSignUpMerchant').submit(function() {
        var get_data = $(this).serialize();
        if (isValidSignUp.valid()) {
            $('input[type="text"]').each(function() {
                if ($(this).hasClass('auto') || $(this).hasClass('auto_int') || $(this).hasClass('auto_dec'))
                {
                    var v = $(this).autoNumeric('get');
                    $(this).autoNumeric('destroy');
                    $(this).val(v);
                }
                $.ajax({
                    url: base_url + "merchant/sign_up/register_merchant_check",
                    data: get_data,
                    dataType: "json",
                    type: "POST",
                    success: function(response) {
                        if (isSessionExpired(response)) {
                            response_object = json_decode(response);
                            url = response_object.url;
                            location.href = url;
                        } else {
                            if (response[0].trim() != "true") {
                                $('#captcha_img_merchant').html(response[1][0]);
                                $('#content-message_merchant').html(error_html + response[0].trim() + "</div>")

                            } else {
                                $('#content-message_merchant').html(success_html + "Terima kasih telah mendaftar sebagai Merchant di Toko1001. Harap cek email anda </div>")
                                $('#email_merchant').val('');
                                $('#name_merchant').val('');
                                $('#merchant_name').val('');
                                $('#phone_merchant').val('');
                                $('#merchant_address').val('');
                                $('#code_captcha_merchant').val('');
                                $("span#email_span").addClass("glyphicon-envelope");
                                $("#img_yes").hide();
                                $("#img_no").hide();
                                $('#captcha_img_merchant').html(response[1][0]);
                            }
                            ;
                        }
                    },
                    error: function(request, error) {
                        $('#content-message_merchant').html(error_html + "Data Tidak Berhasil Diproses !</div>")
                    }

                });
                return false;
            });
        }
        return false;
    });

    var isValidSignUpMember = $('#frmSignupMember').validate({
        ignore: 'input[type=hidden]',
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.hasClass('select2')) {
                error.insertAfter(element.next('span'));
            } else {
                error.insertAfter(element);
            }
        }
    });

    $('form#frmSignupMember').submit(function() {
        var get_data_member = $(this).serialize();
        if (isValidSignUpMember.valid()) {
            $('input[type="text"]').each(function() {
                if ($(this).hasClass('auto') || $(this).hasClass('auto_int') || $(this).hasClass('auto_dec'))
                {
                    var v = $(this).autoNumeric('get');
                    $(this).autoNumeric('destroy');
                    $(this).val(v);
                }
                $.ajax({
                    url: base_url + "member/c_signup_member/check_member",
                    data: get_data_member,
                    dataType: "json",
                    type: "POST",
                    success: function(response) {
                        if (isSessionExpired(response)) {
                            response_object = json_decode(response);
                            url = response_object.url;
                            location.href = url;
                        } else {
                            if (response[0].trim() != "true") {
                                $('#captcha_img_member').html(response[1][0]);
                                $('#content-message').html(error_html + response[0].trim() + "</div>")
                            } else {
                                $('#content-message').html(success_html + "Terima kasih telah mendaftar sebagai Member di Toko1001, harap cek email anda untuk verifikasi.</div>")
                                $('#member_email').val('');
                                $("#img_no").hide();
                                $("#img_yes").hide();
                                $("span#email_span_member").addClass("glyphicon-envelope");
                                $('#member_name').val('');
                                $('#member_birthday').val('');
                                $('#gender_member').val('');
                                $('#member_phone').val('');
                                $('#password1').val('');
                                $('#password2').val('');
                                $('#captcha_member').val('');
                                $('#member_requirement').iCheck('uncheck');
                                $('#captcha_img_member').html(response[1][0]);
                            }
                            ;
                        }
                    },
                    error: function(request, error) {
                        $('#content-message').html(error_html + " Data Tidak Berhasil Diproses ! </div>")
                    }

                });
                return false;
            });
        }
        return false;
    });

    var isValid = $('#frmMain').validate({
        ignore: 'input[type=hidden]',
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.hasClass('select2')) {
                error.insertAfter(element.next('span'));
            } else {
                error.insertAfter(element);
            }
        }
    });
    $.validator.messages.required = "Data wajib diisi !";
    $.validator.messages.maxlength = "Harap diisi tidak lebih dari {0} karakter !";
    $.validator.messages.minlength = "Harap diisi dengan minimal {0} karakter !";
    $.validator.messages.email = "Harap diisi dengan alamat email dengan benar !";
    $.validator.messages.date = "Harap diisi dengan tanggal !";
    $.validator.messages.dateiso = "Harap diisi dengan format tanggal yang benar !";
    $.validator.messages.number = "Harap diisi dengan angka !";

    $(document).on('submit', '#frmMain', function(e) {
        if (isValid.valid()) {
            $('input[type="text"]').each(function() {
                if ($(this).hasClass('auto') || $(this).hasClass('auto_int') || $(this).hasClass('auto_dec'))
                {
                    var v = $(this).autoNumeric('get');
                    $(this).autoNumeric('destroy');
                    $(this).val(v);
                }
            });
        }
    });

    $('input[type="text"], input[type="file"], input[type="password"], input[type="radio"], textarea').each(function() {
        var validate = $(this).attr('validate');
        switch (validate) {
            case  "required[]" :
                $(this).rules('add', {
                    required: true});
                break;
            case  "email[]" :
                $(this).rules('add', {
                    required: true,
                    email: true});
                break;
            case  "password[]" :
                $(this).rules('add', {
                    required: true,
                    minlength: 8,
                    maxlength: 20});
                break;
            case  "email-not-required[]" :
                $(this).rules('add', {
                    email: true});
                break;
            case  "date[]" :
                $(this).rules('add', {
                    required: true,
                    date: true});
                break;
            case  "date-not-required[]" :
                $(this).rules('add', {
                    date: true
                });
                break;
            case  "num[]" :
                $(this).rules('add', {
                    required: true,
                    number: true
                });
                break;
            case  "num-not-required[]" :
                $(this).rules('add', {
                    number: true
                });
                break;
        }
        ;
    });

    $('div#deskripsi').each(function() {
        var $this = $(this);
        new_html = $this.html().replace(/&lt;iframe/g, '<iframe').replace(/&gt;&lt;\/iframe&gt;/g, '></iframe>');
        $this.html(new_html);
        $this.find('iframe').addClass("embed-responsive-item");
        $this.find('iframe').wrap('<div class="embed-responsive embed-responsive-16by9"></div>');
        $this.find('img').addClass("img-responsive");
    });

    $('select').each(function() {
        if ($(this).attr('validate') == "required[]")
        {
            $(this).rules('add', {
                required: true
            });
        }
    });

    $('.select2').on('change', function() {
        $(this).valid();
    });

});

$('.select2').select2();

$('.auto').autoNumeric('init', {vMin: 0});
$('.auto_int').autoNumeric('init', {vMin: 0, mDec: 0});
$('.auto_dec').autoNumeric('init', {vMin: 0, mDec: 2});
//$(document).ready(function(){
// if ($('#floordiv').height() > 54) {
//    $('#floorimg').css("margin-top","15px");
//}   
//});
$("#register_merchant").click(function() {
//    $('.form-group').removeClass('has-error');
//    $('.form-group').find('span.help-block').hide();
    $('.form-group').removeClass('has-error');
    $('.form-group').find('span.help-block').hide();
    $('#content-message_merchant').empty();
    $.ajax({
        url: base_url + "merchant/master/c_signup_merchant/get_cap",
        success: function(response) {
            if (isSessionExpired(response)) {
                response_object = json_decode(response);
                url = response_object.url;
                location.href = url;
            } else {
                $('#captcha_img_merchant').html(response);
            }
        },
        error: function(request, error) {
            alert(error_response(request.status));
        }
    });
});

var valid_code_member = true;
var valid_email_member = true;
$("#member_email").blur(function() {
    $.ajax({
        type: "POST",
        url: base_url + "member/sign_up",
        data: {member_email: $("#member_email").val(), btnemail: "true"},
        success: function(response) {
            if (isSessionExpired(response)) {
                response_object = json_decode(response);
                url = response_object.url;
                location.href = url;
            } else {
                if (response.trim() == "true")
                {
                    if ($("#member_email").val() == "") {
                        $("span#email_span_member").addClass("glyphicon-envelope");
                        $("#img_yes").hide();
                        $("#img_no").hide();
                    } else {
                        $("span#email_span_member").removeClass("glyphicon-envelope");
                        $("span#email_span_member").html("<img id='img_yes' src='" + base_url + "assets/img/yes.png' />");
                    }
                    valid_code_member = true;
                }
                else
                {
                    valid_code_member = false;
                    if ($("#member_email").val() == "") {
                        $("span#email_span_member").addClass("glyphicon-envelope");
                        $("#img_yes").hide();
                        $("#img_no").hide();
                    } else {
                        $("span#email_span_member").removeClass("glyphicon-envelope");
                        $("span#email_span_member").html("<img id='img_no' src='" + base_url + "assets/img/no.png' />");
                    }
                }
            }
        },
        error: function(request, error) {
            alert(error_response(request.status));
        }
    });
});
$('.payment_type').on('click', function() {
    var id = $(this).find('input').attr('id');
    $('.saldo-div').hide();
    $('.transfer-bank-div').hide();
    $('.kartu-kredit-div').hide();
    $('.internet-banking-div').hide();
    $('.emoney-div').hide();
    $('.docu-alfamart-div').hide()
    $('.saldo-div *').attr('disabled', true);
    $('.transfer-bank-div *').attr('disabled', true);
    $('.kartu-kredit-div *').attr('disabled', true);
    $('.internet-banking-div *').attr('disabled', true);
    $('.emoney-div *').attr('disabled', true);
    $('.docu-alfamart-div *').attr('disabled', true);

    $('.' + id + '-div').show();
    $('.' + id + '-div *').attr('disabled', false);
});

$('#maxOption2_general, #maxOption2_md, maxOption2_sm, #maxOption2_lg').on('change', function() {
    var selected = $(this).find('option:selected');
    var extra = selected.data('url');
    /*    $('select[name=selValue]').val(1);
     $('.selectpicker').selectpicker('refresh')*/
    window.location = extra;
});


$('input[date_type="date"]').daterangepicker({
    locale: {
        format: 'DD-MMM-YYYY'
    },
    singleDatePicker: true,
    showDropdowns: true,
    maxDate: new Date(),
    minDate: '1-Jan-' + (new Date().getFullYear() - 60)
});
/*register*/
$('#member_birthday').val('');
$('#member_birthday').on('show.daterangepicker', function(ev, picker) {
    $(".daterangepicker").css("position", "fixed");
    $(".daterangepicker").css("top", "278px");
});
/*user member*/
$('#datepicker').on('show.daterangepicker', function(ev, picker) {
    $(".daterangepicker").css("z-index", "997");
});
/*
 $('input[date_type="date"]').daterangepicker({
 locale: {
 format: 'DD-MMM-YYYY'
 },
 singleDatePicker: true,
 showDropdowns: true,
 maxDate: new Date(),
 minDate: '1-Jan-' + (new Date().getFullYear() - 60),
 
 });*/

/*start blazy image*/
(function() {
    // Initialize
    var bLazy = new Blazy({
        success: function(element) {
            var parent = element.parentNode;
            parent.className = parent.className.replace(/\load_img\b/, '');
        }
    });
})();
/*end blazy image*/


$(document).ready(function() {
    $('.subMegaMenu').slimScroll({
        color: '#000',
        size: '5px',
        height: '600px',
        alwaysVisible: true
    });
    $('.cst-scrool').slimScroll({
        color: '#000',
        size: '3px',
        height: '113',
        alwaysVisible: false,
        distance: '0px'
    });


    $('#table_product').slimScroll({
        color: '#000',
        size: '3px',
        alwaysVisible: false,
        distance: '0px',
        height: '400px',
        alwaysVisible: true
    });


});