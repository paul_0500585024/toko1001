///* =========================================================
// * bootstrap-tabdrop.js
// * http://www.eyecon.ro/bootstrap-tabdrop
// * =========================================================
// * Copyright 2012 Stefan Petre
// * Copyright 2013 Jenna Schabdach
// * Copyright 2014 Jose Ant. Aranda
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// * http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// * ========================================================= */
//
//!function ($) {
//
//	var WinResizer = (function () {
//		var registered = [];
//		var inited = false;
//		var timer;
//		var resize = function () {
//			clearTimeout(timer);
//			timer = setTimeout(notify, 100);
//		};
//		var notify = function () {
//			for (var i = 0, cnt = registered.length; i < cnt; i++) {
//				registered[i].apply();
//			}
//		};
//		return {
//			register: function (fn) {
//				registered.push(fn);
//				if (inited === false) {
//					$(window).bind('resize', resize);
//					inited = true;
//				}
//			},
//			unregister: function (fn) {
//				var registeredFnIndex = registered.indexOf(fn);
//				if (registeredFnIndex > -1) {
//					registered.splice(registeredFnIndex, 1);
//				}
//			}
//		}
//	}());
//
//	var TabDrop = function (element, options) {
//		this.element = $(element);
//		this.options = options;
//
//		if (options.align === "left")
//			this.dropdown = $('<li class="dropdown hide pull-left tabdrop" id="tabdrop-menu"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="display-tab"></span><b class="caret"></b></a><ul class="dropdown-menu"></ul></li>');
//		else
//			this.dropdown = $('<li class="dropdown hide pull-right tabdrop" id="tabdrop-menu"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="display-tab"></span><b class="caret"></b></a><ul class="dropdown-menu"></ul></li>');
//
//		this.dropdown.prependTo(this.element);
//		if (this.element.parent().is('.tabs-below')) {
//			this.dropdown.addClass('dropup');
//		}
//
//		var boundLayout = $.proxy(this.layout, this);
//
//		WinResizer.register(boundLayout);
//		this.element.on('click', 'li:not(.tabdrop)', boundLayout);
//
//		this.teardown = function () {
//			WinResizer.unregister(boundLayout);
//			this.element.off('click', 'li:not(.tabdrop)', boundLayout);
//		};
//
//		this.layout();
//	};
//
//	TabDrop.prototype = {
//		constructor: TabDrop,
//
//		layout: function () {
//			var self = this;
//			var collection = [];
//      var isUsingFlexbox = function(el){
//        return el.element.css('display').indexOf('flex') > 0;
//      };
//
//			function setDropdownText(text) {
//				self.dropdown.find('').html(text);
//			}
//
//			function setDropdownDefaultText(collection) {
//				var text;
//				if (jQuery.isFunction(self.options.text)) {
//					text = self.options.text(collection);
//				} else {
//					text = self.options.text;
//				}
//				setDropdownText(text);
//			}
//
//      // Flexbox support
//      function handleFlexbox(){
//        if (isUsingFlexbox(self)){
//          if (self.element.find('li.tabdrop').hasClass('pull-right')){
//          	self.element.find('li.tabdrop').css({position: 'absolute', right: 0});
//						self.element.css('padding-right', self.element.find('.tabdrop').outerWidth(true));
//          }
//        }  
//      }
//
//			function checkOffsetAndPush(recursion) {
//				self.element.find('> li:not(.tabdrop)')
//					.each(function () {
//						if (this.offsetTop > self.options.offsetTop) {
//							collection.push(this);
//						}
//					});
//
//				if (collection.length > 0) {
//					if (!recursion) {
//						self.dropdown.removeClass('hide');
//						self.dropdown.find('ul').empty();
//					}
//					self.dropdown.find('ul').prepend(collection);
//					
//					if (self.dropdown.find('.active').length == 1) {
//						self.dropdown.addClass('active');
//						setDropdownText(self.dropdown.find('.active > a').html());
//					} else {
//						self.dropdown.removeClass('active');
//						setDropdownDefaultText(collection);
//					}
//          handleFlexbox();
//					collection = [];
//					checkOffsetAndPush(true);
//				} else {
//					if (!recursion) {
//						self.dropdown.addClass('hide');
//					}
//				}
//			}
//    
//			self.element.append(self.dropdown.find('li'));
//			checkOffsetAndPush();
//		}
//	};
//
//	$.fn.tabdrop = function (option) {
//		return this.each(function () {
//			var $this = $(this),
//				data = $this.data('tabdrop'),
//				options = typeof option === 'object' && option;
//			if (!data) {
//				options = $.extend({}, $.fn.tabdrop.defaults, options);
//				data = new TabDrop(this, options);
//				$this.data('tabdrop', data);
//			}
//			if (typeof option == 'string') {
//				data[option]();
//			}
//		})
//	};
//
//	$.fn.tabdrop.defaults = {
//		text: '<i class="fa fa-align-justify"></i>',
//		offsetTop: 0
//	};
//
//	$.fn.tabdrop.Constructor = TabDrop;
//
//}(window.jQuery);
if ($('#tab-contact').hasClass("active"))
{
    $('#title-menu').html("<i class='fa fa-user'></i>&nbsp; Informasi Member");
}

function tabactivecontact() {
    $('.tab-pane').removeClass("active fade in");
    $('#contact').addClass("active fade in");
    $('li').removeClass("active");
    $('#title-menu').html("<i class='fa fa-user'></i>&nbsp; Informasi Member");
    $('#tab-contact').addClass("active");

}
function tabactiveaddress() {
    $('.tab-pane').removeClass("active fade in");
    $('#address').addClass("active fade in");
    $('li').removeClass("active");
    $('#title-menu').html("<i class='fa fa-home'></i>&nbsp; Alamat");
    $('#tab-address').addClass("active");
    //$('#tab-address-xs').addClass("active");
}
function tabactivebank() {
    $('.tab-pane').removeClass("active fade in");
    $('#bank').addClass("active fade in");
    $('li').removeClass("active");
    $('#title-menu').html("<i class='fa fa-university'></i>&nbsp; Info Bank");
    $('#tab-bank').addClass("active");
    //$('#tab-bank-xs').addClass("active");
}
function tabactivevoucher() {
    $('.tab-pane').removeClass("active fade in");
    $('#voucher').addClass("active fade in");
    $('li').removeClass("active");
    $('#title-menu').html("<i class='fa fa-dollar'></i>&nbsp; Voucher");
    $('#tab-voucher').addClass("active");
    // $('#tab-voucher-xs').addClass("active");
}
function tabactivedeposit() {
    $('.tab-pane').removeClass("active fade in");
    $('#deposit').addClass("active fade in");
    $('li').removeClass("active");
    $('#title-menu').html("<i class='fa fa-folder-open'></i>&nbsp; Penarikan Deposit");
    $('#tab-deposit').addClass("active");
    //$('#tab-deposit-xs').addClass("active");
}
function tabactiveorder() {
    $('.tab-pane').removeClass("active fade in");
    $('#order').addClass("active fade in");
    $('li').removeClass("active");
    $('#title-menu').html("<i class='fa fa-shopping-cart'></i>&nbsp; Order");
    $('#tab-order').addClass("active");
    //$('#tab-order-xs').addClass("active");
}
function tabactiveconfig() {
    $('.tab-pane').removeClass("active fade in");
    $('#configuration').addClass("active fade in");
    $('li').removeClass("active");
    $('#title-menu').html("<i class='fa fa-gear'></i>&nbsp; Konfigurasi");
    $('#tab-config').addClass("active");
    //$('#tab-config-xs').addClass("active");
}
function tabactivereturn() {
    $('.tab-pane').removeClass("active fade in");
    $('#return').addClass("active fade in");
    $('li').removeClass("active");
    $('#title-menu').html("<i class='fa fa-cubes'></i>&nbsp;Pengembalian");
    $('#tab-return').addClass("active");
    //$('#tab-config-xs').addClass("active");
}

function tabactiveview() {
    $('.tab-pane').removeClass("active fade in");
    $('#review').addClass("active fade in");
    $('li').removeClass("active");
    $('#title-menu').html("<i class='fa fa-comments-o'></i>&nbsp; Ulasan Produk" + count_msg);
    $('#tab-review').addClass("active");
    //$('#tab-config-xs').addClass("active");
}
                    