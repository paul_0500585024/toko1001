$(document).ready(function () {
    
    setTimeout(function () {
        $('body').fadeIn('slow');
    }, 500);

    $('#' + menu).css('background', '#ecf0f5');
    $('#' + menu).attr("value", "test");

    $('.sidebar-toggle').click(function () {
        $('.control-sidebar-dark').removeClass("control-sidebar-open");
    });
    $('.control-search').click(function () {
        $('.skin-blue').removeClass("sidebar-open");
    });
    $('#user-options').click(function () {
        $('.skin-blue').removeClass("sidebar-open");
        $('.control-sidebar-dark').removeClass("control-sidebar-open");
    });
    $("#search").click(function () {
        listtable(1);
    });
});
function view(id) {
    $("#key").val(id);
    $("<input>").attr("type", "hidden").attr("name", "<?php echo CONTROL_VIEW_NAME; ?>").attr("value", "view").appendTo("#frmAuth");
    var hashloc = $("#frmSearch1").serialize();
    history.pushState({state: 1}, "<?php echo $formauthtitle; ?>", "<?php echo get_base_url() . $formauthurl; ?>?" + hashloc);
    $("#frmAuth").submit();
}
function edit(id) {
    $("#key").val(id);
    $("<input>").attr("type", "hidden").attr("name", "<?php echo CONTROL_EDIT_NAME; ?>").attr("value", "edit").appendTo("#frmAuth");
    var hashloc = $("#frmSearch1").serialize();
    history.pushState({state: 1}, "<?php echo $formauthtitle; ?>", "<?php echo get_base_url() . $formauthurl ?>?" + hashloc);
    $("#frmAuth").submit();
}
function addnew() {
    $("<input>").attr("type", "hidden").attr("name", "<?php echo CONTROL_ADD_NAME ?>").attr("value", "add").appendTo("#frmAuth");
    var hashloc = $("#frmSearch1").serialize();
    history.pushState({state: 1}, "<?php echo $formauthtitle; ?>", "<?php echo get_base_url() . $formauthurl; ?>?" + hashloc);
    $("#frmAuth").submit();
}
function paging(recorddata, idf, functioname) {
    var lengbar = $("#length" + idf).val();
    var halaman = $("#start" + idf).val();
    var clas = '';
    var pagings = '';
    if (Number(recorddata - 1) > Number(lengbar)) {
        pagings = '<ul class="pagination pagination-sm no-margin pull-right">';
        y = 0;
        for (i = 1; i < recorddata; i += Number(lengbar)) {
            y++;
            if (i == halaman) {
                clas = ' class="text-red text-bold"';
            } else {
                clas = '';
            }
            pagings += "<li><a href='javascript:gopage(" + i + "," + idf + "," + functioname + ")' " + clas + ">" + y + "</a></li>";
        }
        pagings += "</ul>";
    }
    return pagings;
}

function gopage(hal, idf, functioname) {
    $("#start" + idf).val(hal);
    functioname(idf);
}
var windowHeight = ($(window).height() - 90);

$(window).on('resize', function () {
    var windowSize = $(window).width();
    if (windowSize > 1199) {
        $('aside').css('position', 'fixed');
        $('nav').css('position', 'fixed');
        $('body').css('padding-top', '50px');
    } else {
        $('aside').css('position', 'absolute');
        $('nav').css('position', 'relative');
        $('body').css('padding-top', '0');
    }
});

$(document).ready(function () {
    $('.content-wrapper').css('min-height', windowHeight + 'px');
});


